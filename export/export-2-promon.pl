#!/usr/bin/perl
#
# Script to convert the default export format for APMS into
# something more or less in line with what Jacks Point have
# requested as an import format.
#
use strict;

use Text::CSV_XS;   # libtext-csv-perl in Debian


my @in;

my $csv = Text::CSV_XS->new();

while( <> ) {
  $csv->parse($_);
  my ( $batchcode,$doc,$transactioncode,$entitytype,$entitycode,$accountcode,$date,$monthcode,$amount,$reference,$description,$consequenceof,$entityname,$orderno,$invoiceref ) = $csv->fields();

  next if ( $batchcode == 'BatchCode' );
  # next if ( $reference !~ /^VCHR/ );

  my $coding = sprintf("%02d.%7.2lf", $entitycode % 100, $accountcode ) if ( $consequenceof == 0 && $entitytype eq "L" );

  $in[$doc]{'no'}          = $doc;
  if ( defined($coding) && !defined($in[$doc]{'et'}) ) {
    $in[$doc]{'et'} = $entitytype;
    $in[$doc]{'ec'} = $entitycode;
  }
  $in[$doc]{'coding'}      = $coding if ( defined($coding) && !defined($in[$doc]{'coding'}) );
  $in[$doc]{'date'}        = $date;
  $in[$doc]{'period'}      = $monthcode - 546;
  $in[$doc]{'reference'}   = $reference;
  $in[$doc]{'invoiceref'}  = $invoiceref;
  $in[$doc]{'orderno'}     = $orderno;
  $in[$doc]{'description'} = $description if ( !defined($in[$doc]{'description'}) );

  my $txn = $transactioncode;
  $txn = $consequenceof if ( $consequenceof != 0 ) ;
  $in[$doc]{'lines'}[$txn]{'accountcode'} = $coding;
  $in[$doc]{'lines'}[$txn]{'amount'}      = $amount;
  $in[$doc]{'lines'}[$txn]{'description'} = $description;
  $in[$doc]{'lines'}[$txn]{'entityname'}  = $entityname;
  $in[$doc]{'lines'}[$txn]{'no'}          = $txn;
  $in[$doc]{'lines'}[$txn]{'coding'}      = $coding if ( !defined($in[$doc]{'lines'}[$txn]{'coding'}) );

  if ( $entitytype eq "C" ) {
    $in[$doc]{'otherparty'}  = $entitytype . sprintf('%05d', $entitycode );
    $in[$doc]{'entityname'}  = $entityname;
  }
  elsif ( $entitytype eq "T" ) {
    $in[$doc]{'otherparty'}  = $entitytype . sprintf('%05d', $entitycode );
    $in[$doc]{'entityname'}  = $entityname;
  }

  $in[$doc]{'total'} += $amount if ( $amount > 0 && $entitytype eq "L" );

  # printf( qq(%d\t%s\t%s\t%s\t%s\t%.2lf\t%d\n), $doc, $in[$doc]{'accountcode'},
            # $date, $reference, $entitytype, $amount, $in[$doc]{'period'} );
}

print "CompanyID,InvoiceDate,Period,InvoiceNumber,Description,InvoiceTotal,BudgetCode,Subtotal,ItemNumber,ItemDescription,APMSReference,SupplierReference,OtherPartyName\n";


foreach my $tx ( @in ) {
  next if ( $tx->{'no'} == 0 );
  $tx->{'otherparty'} = sprintf('%s%05d', $tx->{'et'}, $tx->{'ec'}) if ( !defined($tx->{'otherparty'}) );
  my $lno = 1;
  my @lines = @{$tx->{'lines'}};
  foreach my $vline ( @lines ) {
    next if ( $vline->{'amount'} <= 0 );

    my $entityname = ( defined($vline->{'entityname'}) ? $vline->{'entityname'} : $tx->{'entityname'});

    printf( qq("%s","%s",%d,"%s","%s",%.2lf,"%s",%.2lf,%d,"%s","%s","%s","%s"\n),
        $tx->{'otherparty'},
        $tx->{'date'},
        $tx->{'period'},
        $tx->{'orderno'},
        $tx->{'description'},
        $tx->{'total'},
        $tx->{'coding'},
        $vline->{'amount'},
        $lno++,
        $vline->{'description'},
        $tx->{'reference'},
        $tx->{'invoiceref'},
        $entityname
      );
  }
}
