/* include the one from adm-row-available, part of an "IF"
 * around it to give the appropriate table
 */
source-table = "{2}" THEN DO:
  {src/adm/template/row-{1}.i "{2}"}
END.
