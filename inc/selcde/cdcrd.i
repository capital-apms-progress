/* cdcrd.i */
/* Code fill-in for Creditor records */

{inc/selcde/cdmst.i &ExtTable="Creditor"
                    &ExtKey  ="CreditorCode"
                    &ConvFn  ="INT"
                    &FillIn  ="{1}"}
