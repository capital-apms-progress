/*--------------------------------------------------------------------------
    File        : cdmst.i
    Purpose     : Value-changed trigger for all select code fill-ins

    Parameters  : &ExtTable - The external table
                  &ExtKey   - The key code of the external table
                  
    Description : This will update the select fill-in and display
                  the appropriate external table value.

    Author(s)   : Tyrone McAuley
    Notes       : The private-data of this fill-in stores the value of the
                  ROWID for the current external table record that the
                  internal code points to
                  
  ------------------------------------------------------------------------*/

&SCOPED-DEFINE N {&SEQUENCE}   /* Helps uniquely identify all variables for
                                  multiple references within the same tigger */

IF {&SELF-NAME}:MODIFIED THEN
DO:

/* cdmst.i */
/* Code fill-in master file for all records */

DEF VAR old-val{&N}  AS CHAR NO-UNDO.
DEF VAR curr-val{&N} AS CHAR NO-UNDO.
DEF VAR old-id{&N}   AS ROWID NO-UNDO.

DEF BUFFER tmp_{&ExtTable} FOR {&ExtTable}.

old-val{&N}  = ENTRY( 2, {&SELF-NAME}:PRIVATE-DATA ).
curr-val{&N} = {&SELF-NAME}:SCREEN-VALUE.
old-id{&N}   = TO-ROWID( ENTRY( 1, {&SELF-NAME}:PRIVATE-DATA ) ).

IF old-val{&N} <> curr-val{&N} THEN
DO WITH FRAME {&FRAME-NAME}:

  FIND FIRST tmp_{&ExtTable} WHERE tmp_{&ExtTable}.{&ExtKey} = {&ConvFn} ( curr-val{&N} )
    NO-LOCK NO-ERROR.

  IF NOT AVAILABLE tmp_{&ExtTable} THEN
  DO:
    
    MESSAGE "There is no {&ExtTable} available with code " {&SELF-NAME}:SCREEN-VALUE
      VIEW-AS ALERT-BOX ERROR.
      
    FIND FIRST tmp_{&ExtTable} WHERE ROWID( tmp_{&ExtTable} ) = old-id{&N} NO-LOCK NO-ERROR.
    
    IF AVAILABLE tmp_{&ExtTable} THEN
      DISPLAY tmp_{&ExtTable}.{&ExtKey} @ {&SELF-NAME}.
    ELSE
      DISPLAY {&SELF-NAME}.    
          
  END.
  ELSE
  DO:
    
    DEF VAR spd{&N} AS CHAR NO-UNDO.
    spd{&N} = {&SELF-NAME}:PRIVATE-DATA.    
    ENTRY( 1, spd{&N} ) = STRING( ROWID( tmp_{&ExtTable} ) ).
    ENTRY( 2, spd{&N} ) = {&SELF-NAME}:SCREEN-VALUE.
    {&SELF-NAME}:PRIVATE-DATA = spd{&N}.
    
    {&FillIn}:PRIVATE-DATA    = STRING( ROWID( tmp_{&ExtTable} ) ).

    DEF VAR enabled{&N} AS LOGI NO-UNDO.
    enabled{&N} = {&FillIn}:SENSITIVE.

    {&FillIn}:SENSITIVE = TRUE.
    APPLY 'U2':U TO {&FillIn}.
    {&FillIn}:SENSITIVE = enabled{&N}.

    /* Tab past the select button for valid codes if tab was pressed*/
    IF LOOKUP( LAST-EVENT:LABEL, 'TAB,ENTER':U ) <> 0 THEN
    DO:
      DEF VAR next-tab{&N} AS HANDLE NO-UNDO.
      
      next-tab{&N} = {&SELF-NAME}:NEXT-TAB-ITEM.
      DO WHILE NOT next-tab{&N}:SENSITIVE:
        next-tab{&N} = next-tab{&N}:NEXT-TAB-ITEM.
      END.
      
      IF VALID-HANDLE( next-tab{&N} ) AND next-tab{&N}:TYPE = 'BUTTON':U
         AND next-tab{&N}:DYNAMIC THEN
      DO: 
        APPLY 'TAB':U TO next-tab{&N}.
        RETURN NO-APPLY.
      END.
    END.
  
  END.
  
END.

END. /* Of check if modified */
