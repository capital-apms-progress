/* cdnbc.i */
/* Code fill-in for New Batch records */

{inc/selcde/cdmst.i &ExtTable="NewBatch"
                    &ExtKey  ="BatchCode"
                    &ConvFn  ="INT"
                    &FillIn  ="{1}"}
