/* cdfyr.i */
/* Code fill-in for Financial Year records */

{inc/selcde/cdmst.i &ExtTable="FinancialYear"
                    &ExtKey  ="FinancialYearCode"
                    &ConvFn  ="INT"
                    &FillIn  ="{1}"}
