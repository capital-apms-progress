/* Include to get the user's name */

&IF DEFINED(BQ-MGR) &THEN
  IF VALID-HANDLE(bq-mgr) THEN RUN get-username IN bq-mgr ( OUTPUT {1} ).
&ENDIF

&IF DEFINED(SYS-MGR) &THEN
  IF VALID-HANDLE(sys-mgr) AND ( {1} = ? OR {1} = "" ) THEN
    RUN get-username IN sys-mgr ( OUTPUT {1} ).
&ENDIF

IF {1} = ? OR {1} = "" THEN {1} = OS-GETENV("PROPUSER":U).
IF {1} = ? OR {1} = "" THEN {1} = "Unknown".

DEFINE VARIABLE title-ledger-name AS CHARACTER FORMAT "X(20)" INITIAL '' NO-UNDO.
{inc/ofc-this.i}
FIND OfficeSetting OF Office WHERE OfficeSetting.SetName = 'LedgerName' NO-LOCK NO-ERROR.
IF AVAILABLE(OfficeSetting) THEN DO:
    title-ledger-name = OfficeSetting.SetValue.
    {1} = {1} + " " + title-ledger-name.
END.

