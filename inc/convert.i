&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*--------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = .08
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE amount-to-word Include 
PROCEDURE amount-to-word :
/*------------------------------------------------------------------------------
  Purpose:     Converts a decimal monetary amount to words
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT  PARAMETER amount AS DEC NO-UNDO.
  DEF OUTPUT PARAMETER words  AS CHAR NO-UNDO.

  DEF VAR dollars AS INT NO-UNDO.
  DEF VAR cents   AS INT NO-UNDO.

  DEF VAR dwords AS CHAR NO-UNDO.
  DEF VAR cwords AS CHAR NO-UNDO.
    
  dollars = TRUNCATE( amount, 0 ).
  cents   = (amount - dollars) * 100.

  RUN num-to-word( dollars, OUTPUT dwords ).
  RUN num-to-word( cents,   OUTPUT cwords ).
  
  words = "".
  IF dwords <> "" THEN
  DO:
    words = dwords + " dollar" + IF dollars = 1 THEN "" ELSE "s".
    IF cwords <> "" THEN
      words = words + " and " + cwords + " cent" + IF cents = 1 THEN "" ELSE "s".
    words = words + " only".
  END.
  ELSE
  DO:
    IF cwords <> "" THEN
      words = cwords + " cent" + IF cents = 1 THEN "" ELSE "s".
    words = words + " only".
  END.
  
  words = TRIM( words ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE date-to-word Include 
PROCEDURE date-to-word :
/*------------------------------------------------------------------------------
  Purpose:     Converts a date to a word
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT  PARAMETER in-date AS DATE NO-UNDO.
  DEF OUTPUT PARAMETER words AS CHAR NO-UNDO.

  DEF VAR dd AS INT NO-UNDO.
  DEF VAR mm AS INT NO-UNDO.
  DEF VAR yy AS INT NO-UNDO.
  
  DEF VAR dd-suffix AS CHAR NO-UNDO.
  DEF VAR mword     AS CHAR NO-UNDO.

  dd = DAY( in-date ).
  mm = MONTH( in-date ).
  yy = YEAR( in-date ).
  
  dd-suffix =
    IF dd = 11 OR dd = 12 OR dd = 13 THEN "th" ELSE
    IF dd MODULO 10 = 1 THEN "st" ELSE
    IF dd MODULO 10 = 2 THEN "nd" ELSE
    IF dd MODULO 10 = 3 THEN "rd" ELSE "th".
    
  CASE mm:
    WHEN 1  THEN mword = "January".
    WHEN 2  THEN mword = "February".
    WHEN 3  THEN mword = "March".
    WHEN 4  THEN mword = "April".
    WHEN 5  THEN mword = "May".
    WHEN 6  THEN mword = "June".
    WHEN 7  THEN mword = "July".
    WHEN 8  THEN mword = "August".
    WHEN 9  THEN mword = "September".
    WHEN 10 THEN mword = "October".
    WHEN 11 THEN mword = "November".
    WHEN 12 THEN mword = "December".
  END CASE.

  words = STRING( dd ) + dd-suffix + ' ' + mword + ', ' + STRING( yy ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE num-to-word Include 
PROCEDURE num-to-word :
/*------------------------------------------------------------------------------
  Purpose:     Converts a number to a word
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT  PARAMETER number AS INT NO-UNDO.
  DEF OUTPUT PARAMETER words  AS CHAR NO-UNDO.

  DEF VAR ones      AS INT NO-UNDO.
  DEF VAR hundreds  AS INT NO-UNDO.
  DEF VAR thousands AS INT NO-UNDO.
  DEF VAR millions  AS INT NO-UNDO.

  DEF VAR oword     AS CHAR NO-UNDO.
  DEF VAR hword     AS CHAR NO-UNDO.
  DEF VAR tword     AS CHAR NO-UNDO.
  DEF VAR mword     AS CHAR NO-UNDO.
  
  IF number <=0 THEN RETURN.
    
  ones      = number MODULO 100.
  hundreds  = INT( DEC(number) / 100 - 0.5 ) MODULO 10.
  thousands = INT( DEC(number) / 1000 - 0.5 ) MODULO 1000.
  millions  = INT( DEC(number) / 1000000  - 0.5 ).
  
  words = "".
  IF millions > 0 THEN         
  DO:
    RUN num-to-word( millions, OUTPUT mword ).
    words = mword + " million ".
  END.
  
  IF thousands > 0 THEN         
  DO:
    RUN num-to-word( thousands, OUTPUT tword ).
    words = words + tword + " thousand ".
  END.

  IF hundreds > 0 THEN         
  DO:
    RUN num-to-word( hundreds, OUTPUT hword ).
    words = words + hword + " hundred ".
  END.

  IF ones > 0 THEN         
  DO:
    IF number > 100 THEN words = words + "and ".  
    DEF VAR digit AS INT NO-UNDO.
    oword = "".
    digit = INT( DEC(ones) / 10 - 0.5 ).
    
    oword = 
      IF digit = 2 THEN "twenty "  ELSE
      IF digit = 3 THEN "thirty "  ELSE
      IF digit = 4 THEN "forty "   ELSE
      IF digit = 5 THEN "fifty "   ELSE
      IF digit = 6 THEN "sixty "   ELSE
      IF digit = 7 THEN "seventy " ELSE
      IF digit = 8 THEN "eighty "  ELSE
      IF digit = 9 THEN "ninety "  ELSE "".

    digit = IF digit = 1 THEN ones ELSE ones MODULO 10.
    
    CASE digit:
      WHEN 1  THEN oword = oword + "one".
      WHEN 2  THEN oword = oword + "two".
      WHEN 3  THEN oword = oword + "three".
      WHEN 4  THEN oword = oword + "four".
      WHEN 5  THEN oword = oword + "five".
      WHEN 6  THEN oword = oword + "six".
      WHEN 7  THEN oword = oword + "seven".
      WHEN 8  THEN oword = oword + "eight".
      WHEN 9  THEN oword = oword + "nine".
      WHEN 10 THEN oword = oword + "ten".
      WHEN 11 THEN oword = oword + "eleven".
      WHEN 12 THEN oword = oword + "twelve".
      WHEN 13 THEN oword = oword + "thirteen".
      WHEN 14 THEN oword = oword + "fourteen".
      WHEN 15 THEN oword = oword + "fifteen".
      WHEN 16 THEN oword = oword + "sixteen".
      WHEN 17 THEN oword = oword + "seventeen".
      WHEN 18 THEN oword = oword + "eighteen".
      WHEN 19 THEN oword = oword + "nineteen".
    END CASE.

    words = words + oword.
    
  END.

  words = TRIM( words ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE word-wrap Include 
PROCEDURE word-wrap :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER in-word    AS CHAR NO-UNDO.
DEF INPUT  PARAMETER wrap-width AS INT NO-UNDO.
DEF OUTPUT PARAMETER result   AS CHAR NO-UNDO.
  
  DEF VAR words-left AS CHAR NO-UNDO.
  DEF VAR idx        AS INT NO-UNDO.
  DEF VAR idx2       AS INT NO-UNDO.

  words-left = in-word.
  DO WHILE LENGTH( words-left ) > wrap-width :
    idx = R-INDEX( words-left, " ", wrap-width ).
    IF idx = 0 THEN idx = wrap-width.
    idx2 = INDEX( words-left, "~n").

    IF idx2 <> 0 AND idx2 < idx THEN DO:
      idx = idx2.
      result = result + SUBSTR( words-left, 1, idx).
    END.
    ELSE
      result = result + TRIM(SUBSTR( words-left, 1, idx - 1)) + "~n".

    words-left = SUBSTR( words-left, idx + 1).
  END.
  result = result + words-left.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


