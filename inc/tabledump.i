&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-convert-cha) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD convert-cha Method-Library 
FUNCTION convert-cha RETURNS CHAR
  ( INPUT in-text AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-dat) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD convert-dat Method-Library 
FUNCTION convert-dat RETURNS CHAR
  ( INPUT in-date AS DATE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-dec) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD convert-dec Method-Library 
FUNCTION convert-dec RETURNS CHAR
  ( INPUT in-dec AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-int) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD convert-int Method-Library 
FUNCTION convert-int RETURNS CHAR
  ( INPUT in-int AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-log) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD convert-log Method-Library 
FUNCTION convert-log RETURNS CHARACTER
  ( INPUT in-log AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-raw) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD convert-raw Method-Library 
FUNCTION convert-raw RETURNS CHAR
  ( INPUT in-text AS RAW )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-rec) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD convert-rec Method-Library 
FUNCTION convert-rec RETURNS CHAR
  ( INPUT in-int AS RECID )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-row) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD convert-row Method-Library 
FUNCTION convert-row RETURNS CHAR
  ( INPUT in-int AS ROWID )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .67
         WIDTH              = 38.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

RUN DumpTableData.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-convert-cha) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION convert-cha Method-Library 
FUNCTION convert-cha RETURNS CHAR
  ( INPUT in-text AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert a character string to a string for SQL import
------------------------------------------------------------------------------*/
  IF in-text = ? THEN RETURN "NULL".

  in-text = REPLACE( in-text, "'", "''").
  in-text = REPLACE( in-text, "\", "\\").
  RETURN "'" + in-text + "'".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-dat) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION convert-dat Method-Library 
FUNCTION convert-dat RETURNS CHAR
  ( INPUT in-date AS DATE ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert a date to a string generically for SQL import
------------------------------------------------------------------------------*/
  IF in-date = ? THEN RETURN "NULL".

  RETURN "'" + STRING(in-date, "99-99-9999") + "'".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-dec) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION convert-dec Method-Library 
FUNCTION convert-dec RETURNS CHAR
  ( INPUT in-dec AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert a decimal to a string generically for SQL import
------------------------------------------------------------------------------*/
  IF in-dec = ? THEN RETURN "NULL".

  RETURN RIGHT-TRIM( RIGHT-TRIM( TRIM(STRING(in-dec, "->>>>>>>>>>>>>>>9.99999999999")), "0"), ".").

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-int) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION convert-int Method-Library 
FUNCTION convert-int RETURNS CHAR
  ( INPUT in-int AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert an Int to a string generically for SQL import
------------------------------------------------------------------------------*/
  IF in-int = ? THEN RETURN "NULL".

  RETURN TRIM(STRING(in-int, "->>>>>>>>>>9")).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-log) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION convert-log Method-Library 
FUNCTION convert-log RETURNS CHARACTER
  ( INPUT in-log AS LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  IF in-log = ? THEN RETURN "NULL".

  RETURN STRING( in-log, "TRUE/FALSE").

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-raw) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION convert-raw Method-Library 
FUNCTION convert-raw RETURNS CHAR
  ( INPUT in-text AS RAW ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert a character string to a string for SQL import
------------------------------------------------------------------------------*/
DEF VAR out-str AS CHAR NO-UNDO.

  IF in-text = ? THEN RETURN "NULL".

  out-str = REPLACE( STRING(in-text), "'", "''").
  RETURN "'" + out-str + "'".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-rec) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION convert-rec Method-Library 
FUNCTION convert-rec RETURNS CHAR
  ( INPUT in-int AS RECID ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert an Int to a string generically for SQL import
------------------------------------------------------------------------------*/

  IF in-int = ? THEN RETURN "NULL".
  RETURN TRIM(STRING(in-int)).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-row) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION convert-row Method-Library 
FUNCTION convert-row RETURNS CHAR
  ( INPUT in-int AS ROWID ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert an Int to a string generically for SQL import
------------------------------------------------------------------------------*/

  IF in-int = ? THEN RETURN "NULL".
  RETURN TRIM(STRING(in-int)).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

