/* inc/ranges.i - find appropriate value in range */

&SCOPED-DEFINE N {&SEQUENCE}

{inc/ofc-set.i "{&insetname}" "{&insetname}-{&N}" "ERROR"}

{&lowvalue} = INT( ENTRY( 1, OfficeSettings.SetValue, "-")).
{&highvalue} = INT( ENTRY( 2, OfficeSettings.SetValue, "-")).
