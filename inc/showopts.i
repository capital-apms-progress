&IF DEFINED(BQ-MGR) &THEN
  IF NOT VALID-HANDLE(bq-mgr) THEN DO:
&ENDIF

DEF VAR opts-user-name-msg AS CHAR NO-UNDO.
{inc/username.i "opts-user-name-msg"}
IF CAN-FIND( UsrGroupMember WHERE UsrGroupMember.UserName = opts-user-name-msg 
                AND UsrGroupMember.GroupName = "Programmer") THEN DO:
  MESSAGE {1}.
END.

&IF DEFINED(BQ-MGR) &THEN
  END.
&ENDIF
