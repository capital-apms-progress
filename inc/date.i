&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*--------------------------------------------------------------------------
    Purpose     : Date manipulation routines
  ------------------------------------------------------------------------*/


DEF VAR list-of-month-names AS CHAR NO-UNDO
                INITIAL "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD add-date Include 
FUNCTION add-date RETURNS DATE
  ( INPUT d AS DATE, INPUT yy AS INT, INPUT mm AS INT, INPUT dd AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD add-freq Include 
FUNCTION add-freq RETURNS DATE
  ( INPUT d AS DATE, INPUT freq AS CHAR, INPUT n AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD add-months Include 
FUNCTION add-months RETURNS DATE
  ( INPUT d AS DATE, INPUT n-months AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD date-diff Include 
FUNCTION date-diff RETURNS DATE
  ( INPUT d-start AS DATE, INPUT d-end AS DATE, OUTPUT yy AS INT, OUTPUT mm AS INT, OUTPUT dd AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD diff-months Include 
FUNCTION diff-months RETURNS INTEGER
  ( INPUT a AS DATE, INPUT b AS DATE)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD display-date-with-time Include 
FUNCTION display-date-with-time RETURNS CHARACTER
  ( INPUT dt AS DATE, INPUT tm AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD first-of-month Include 
FUNCTION first-of-month RETURNS DATE
  ( INPUT d AS DATE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-freq-days Include 
FUNCTION get-freq-days RETURNS INTEGER
  ( INPUT freq AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-freq-months Include 
FUNCTION get-freq-months RETURNS INTEGER
  ( INPUT freq AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD int-to-date Include 
FUNCTION int-to-date RETURNS DATE
  ( INPUT int-date AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD int2time Include 
FUNCTION int2time RETURNS CHARACTER
  ( INPUT secs AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD last-of-month Include 
FUNCTION last-of-month RETURNS DATE
  ( INPUT d AS DATE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD month-days Include 
FUNCTION month-days RETURNS INTEGER
  ( INPUT d AS DATE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD month-desc Include 
FUNCTION month-desc RETURNS CHARACTER
 ( INPUT month-date AS DATE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD month-name Include 
FUNCTION month-name RETURNS CHARACTER
  ( INPUT mm AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD next-date-after Include 
FUNCTION next-date-after RETURNS DATE
  ( INPUT d-from AS DATE, INPUT after AS DATE, INPUT units AS CHAR, INPUT n AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD time2int Include 
FUNCTION time2int RETURNS INTEGER
  ( INPUT tm AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = .05
         WIDTH              = 29.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Include 
/* ************************* Included-Libraries *********************** */

{inc/math.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION add-date Include 
FUNCTION add-date RETURNS DATE
  ( INPUT d AS DATE, INPUT yy AS INT, INPUT mm AS INT, INPUT dd AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  DEF VAR new-date AS DATE NO-UNDO.
  new-date = DATE( MONTH( d ), 1, YEAR( d ) + yy ).
  new-date = DATE( MONTH( new-date ), MIN( month-days( new-date ), DAY( d ) ), YEAR( new-date ) ).
  new-date = add-months( new-date, mm ) + dd.
  
  RETURN new-date.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION add-freq Include 
FUNCTION add-freq RETURNS DATE
  ( INPUT d AS DATE, INPUT freq AS CHAR, INPUT n AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Add a particular frequency interval to a date ( n times )
------------------------------------------------------------------------------*/
DEF VAR units AS INT NO-UNDO.

  units = get-freq-months( freq ).
  IF units <> ? THEN RETURN add-months( d, units * n ).

  units = get-freq-days( freq ).
  IF units <> ? THEN RETURN d + (FrequencyType.UnitCount * n).

  RETURN ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION add-months Include 
FUNCTION add-months RETURNS DATE
  ( INPUT d AS DATE, INPUT n-months AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR dd AS INT NO-UNDO.
DEF VAR mm AS INT NO-UNDO.
DEF VAR yy AS INT NO-UNDO.

  dd = DAY(d).
  mm = MONTH(d) + n-months.
  yy = YEAR(d).
  IF mm < 1 THEN DO:
    yy = yy -  1 + TRUNCATE( mm / 12, 0).
    mm = mm + 12 - (TRUNCATE( mm / 12, 0) * 12).
  END.
  ELSE IF mm > 12 THEN DO:
    yy = yy + TRUNCATE( (mm - 1) / 12, 0).
    mm = ((mm - 1) MOD 12) + 1.
  END.

  IF dd > month-days( DATE( mm, 1, yy) ) OR dd = month-days(d) THEN
    RETURN last-of-month( DATE( mm, 1, yy) ).
  ELSE
    RETURN DATE( mm, dd, yy).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION date-diff Include 
FUNCTION date-diff RETURNS DATE
  ( INPUT d-start AS DATE, INPUT d-end AS DATE, OUTPUT yy AS INT, OUTPUT mm AS INT, OUTPUT dd AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Return the difference between two dates
    Notes:  We add one to the end date because the range is 'inclusive',
            i.e. 18/m/y -> 17/m/y+1 is a date range of exactly one year.
            Mostly its something like 1/m/y -> end/m-1/y+n of course.
------------------------------------------------------------------------------*/
  d-end = d-end + 1.

  mm = diff-months( d-start, d-end ).
  dd = DAY(d-end) - DAY(d-start).
  IF dd < 0 THEN DO:
    dd = dd + month-days( d-start ).
    mm = mm - 1  .
  END.
  yy = TRUNCATE( mm / 12, 0 ).
  mm = mm - (yy * 12).

  RETURN d-end - INT(d-start) + INT( DATE(1,1,-1)).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION diff-months Include 
FUNCTION diff-months RETURNS INTEGER
  ( INPUT a AS DATE, INPUT b AS DATE) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN( (YEAR(b) * 12 + MONTH(b)) - (YEAR(a) * 12 + MONTH(a)) ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION display-date-with-time Include 
FUNCTION display-date-with-time RETURNS CHARACTER
  ( INPUT dt AS DATE, INPUT tm AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Display a 99/99/9999, HH:MM date and time
------------------------------------------------------------------------------*/
DEF VAR dwt AS CHAR NO-UNDO.

  dwt = STRING( dt, '99/99/9999').
  IF dwt = ? THEN dwt = ''.
  dwt = dwt + ', ' + int2time(tm).

  RETURN dwt.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION first-of-month Include 
FUNCTION first-of-month RETURNS DATE
  ( INPUT d AS DATE ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN DATE( MONTH( d ), 1, YEAR( d ) ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-freq-days Include 
FUNCTION get-freq-days RETURNS INTEGER
  ( INPUT freq AS CHAR ) :

  FIND FrequencyType WHERE FrequencyType.FrequencyCode = freq NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(FrequencyType) THEN          RETURN ?.
  IF FrequencyType.RepeatUnits BEGINS "M" THEN  RETURN ?.
  IF FrequencyType.RepeatUnits BEGINS "D" THEN  RETURN FrequencyType.UnitCount.

  DEF VAR fract     AS DEC NO-UNDO.
  RUN process/calcfreq.p( freq, OUTPUT fract ).
  RETURN INT(fract * 365).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-freq-months Include 
FUNCTION get-freq-months RETURNS INTEGER
  ( INPUT freq AS CHAR ) :

  FIND FrequencyType WHERE FrequencyType.FrequencyCode = freq NO-LOCK NO-ERROR.
  IF NOT AVAILABLE  FrequencyType THEN RETURN ?.

  IF FrequencyType.RepeatUnits BEGINS "D" THEN   RETURN ?.
  IF FrequencyType.RepeatUnits BEGINS "M" THEN RETURN FrequencyType.UnitCount.

  DEF VAR fract     AS DEC NO-UNDO.
  RUN process/calcfreq.p( freq, OUTPUT fract ).
  RETURN INT(fract * 12).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION int-to-date Include 
FUNCTION int-to-date RETURNS DATE
  ( INPUT int-date AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert an integer back into a date
------------------------------------------------------------------------------*/

  RETURN (DATE(1,1,1) + (int-date - INT(DATE(1,1,1)))).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION int2time Include 
FUNCTION int2time RETURNS CHARACTER
  ( INPUT secs AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR hh AS INT NO-UNDO.
DEF VAR mm AS INT NO-UNDO.
DEF VAR ss AS INT NO-UNDO.
DEF VAR tm AS CHAR NO-UNDO.

  ASSIGN hh = TRUNCATE(secs / 3600, 0) NO-ERROR.
  ASSIGN mm = TRUNCATE((secs MODULO 3600) / 60, 0) NO-ERROR.
  ASSIGN ss = (secs MODULO 60) NO-ERROR.
  IF ( hh = ? ) THEN hh = 0.
  IF ( mm = ? ) THEN mm = 0.
  IF ( ss = ? ) THEN ss = 0.
  
  tm = TRIM(STRING( hh, '>>>>>99')) + ':' +  TRIM(STRING( mm, '>>>99')).
  /*
  tm = tm + ':' +  TRIM(STRING( ss, '>>>99'))
  */
  RETURN tm.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION last-of-month Include 
FUNCTION last-of-month RETURNS DATE
  ( INPUT d AS DATE ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN DATE( MONTH( d ), month-days( d ), YEAR( d ) ).
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION month-days Include 
FUNCTION month-days RETURNS INTEGER
  ( INPUT d AS DATE ) :
/*------------------------------------------------------------------------------
  Purpose:  How many days are there in the month which contains 'd' day?
    Notes:  Yes, it's year 2000 compliant... :-)  It's even year 2100 compliant
            and year 3000 compliant.  I'll let someone else worry about whether
            it's Y10K compliant!
------------------------------------------------------------------------------*/

  DEF VAR leap-year AS LOGI NO-UNDO.

  CASE MONTH( d ):
    WHEN 2 THEN DO:
      leap-year = YEAR( d ) MOD 4 = 0.
      IF leap-year THEN DO:
        IF YEAR( d ) MOD 1000 = 0 THEN leap-year = Yes. ELSE
        IF YEAR( d ) MOD 100  = 0 THEN leap-year = No.
      END.
      RETURN ( IF leap-year THEN 29 ELSE 28 ).
    END.
    WHEN 4 OR WHEN 6 OR WHEN 9 OR WHEN 11 THEN RETURN 30.
    OTHERWISE RETURN 31.
  END CASE.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION month-desc Include 
FUNCTION month-desc RETURNS CHARACTER
 ( INPUT month-date AS DATE ) :

  RETURN month-name( MONTH(month-date) ) + " " + STRING( YEAR( month-date ), "9999" ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION month-name Include 
FUNCTION month-name RETURNS CHARACTER
  ( INPUT mm AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  IF mm > 0 AND mm <= NUM-ENTRIES( list-of-month-names ) THEN
    RETURN ENTRY( mm, list-of-month-names ).
  ELSE
   RETURN "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION next-date-after Include 
FUNCTION next-date-after RETURNS DATE
  ( INPUT d-from AS DATE, INPUT after AS DATE, INPUT units AS CHAR, INPUT n AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  From 'd-from' date, using 'n' intervals of 'units' this
            function returns the first date after the input date 'after'.
  Notes:    Valid repeat units begin with 'D' or 'M'
------------------------------------------------------------------------------*/

  IF d-from >= after THEN RETURN d-from.  /* that was easy! */
  
  IF SUBSTRING(units, 1, 1) = "M" THEN DO:
    DEF VAR diff   AS DEC NO-UNDO.
    diff = diff-months( d-from, after ) + (IF DAY(d-from) < DAY(after) THEN 1 ELSE 0).
    RETURN add-months( d-from, n * ceiling( DEC(diff) / n ) ).
  END.

  RETURN d-from + n * ( ceiling( DEC( after - d-from) / n ) ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION time2int Include 
FUNCTION time2int RETURNS INTEGER
  ( INPUT tm AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR hh AS INT NO-UNDO.
DEF VAR mm AS INT NO-UNDO.
DEF VAR ss AS INT NO-UNDO.

  ASSIGN hh = INT(ENTRY( 1, tm, ":" )) NO-ERROR.
  ASSIGN mm = INT(ENTRY( 2, tm, ":" )) NO-ERROR.
  ASSIGN ss = INT(ENTRY( 3, tm, ":" )) NO-ERROR.
  IF ( hh = ? ) THEN hh = 0.
  IF ( mm = ? ) THEN mm = 0.
  IF ( ss = ? ) THEN ss = 0.
  
  ss = 3600 * hh + 60 * mm + ss.

  IF INDEX( tm, "p" ) > 0 THEN ss = ss + 43200 .
/*  MESSAGE "Converted" tm "to" ss. */

  RETURN ss.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

