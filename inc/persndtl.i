&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

DEF VAR office-country-code LIKE PhoneDetail.cCountryCode NO-UNDO.
DEF VAR office-std-code     LIKE PhoneDetail.cSTDCode     NO-UNDO.
DEF VAR office-isd-prefix   AS CHAR NO-UNDO INITIAL "00".
DEF VAR office-country-name AS CHAR NO-UNDO INITIAL "".

DEF VAR other-countries AS CHAR NO-UNDO INITIAL
    "Australia,USA, U.S.A.,UK,U.K.,United Kingdom,France,New Zealand".

DEF VAR street-types AS CHAR NO-UNDO INITIAL
    "st,tce,cr,pl".

DEF VAR city-suffixes AS CHAR NO-UNDO INITIAL
    "bay".

{inc/ofc-this.i}
{inc/ofc-set.i "PhoneType-Priority" "phone-types"}
IF NOT AVAILABLE(OfficeSetting) THEN phone-types = "BUS,FAX,MOB,EMAL,DDI,HOME".
DEF VAR no-phone-types AS INT NO-UNDO.
no-phone-types = NUM-ENTRIES(phone-types).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD combine-name Include 
FUNCTION combine-name RETURNS CHARACTER
  ( INPUT p-title AS CHAR, INPUT p-first AS CHAR, INPUT p-middle AS CHAR, INPUT p-last AS CHAR, INPUT p-suffix AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-phone-nos Include 
FUNCTION get-phone-nos RETURNS CHARACTER
  ( INPUT person-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = .1
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

RUN get-office-phone-defaults.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE combine-fullname Include 
PROCEDURE combine-fullname :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER first-name AS CHAR NO-UNDO.
DEF INPUT  PARAMETER last-name  AS CHAR NO-UNDO.
  
  DEF OUTPUT PARAMETER full-name  AS CHAR NO-UNDO.
  full-name = TRIM( first-name + ' ' + last-name ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE combine-phone Include 
PROCEDURE combine-phone :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER c-country LIKE PhoneDetail.cCountryCode NO-UNDO.
DEF INPUT  PARAMETER c-std     LIKE PhoneDetail.cSTDCode     NO-UNDO.
DEF INPUT  PARAMETER phone-no  LIKE PhoneDetail.Number       NO-UNDO.
  
DEF OUTPUT PARAMETER full-phone AS CHAR NO-UNDO.

  DEF VAR country-part AS CHAR NO-UNDO INITIAL "".
  DEF VAR std-part     AS CHAR NO-UNDO INITIAL "".
  
  /* If the phone number contains an @ then only put it in phone-no and return */
  IF INDEX(phone-no, "@") > 0 THEN DO:
    full-phone = phone-no.
    RETURN.
  END.

  IF c-country <> "" AND c-country <> ? AND c-country <> office-country-code THEN
    country-part = "+" + c-country + " ".

  IF c-std = ? THEN c-std = "".
  IF c-country <> office-country-code OR c-std <> office-std-code THEN
    std-part = "(" + c-std + ") ".

  full-phone = (IF country-part <> "" THEN country-part ELSE "")
             + (IF std-part <> "" THEN std-part ELSE "")
             + phone-no .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-office-phone-defaults Include 
PROCEDURE get-office-phone-defaults :
/*------------------------------------------------------------------------------
  Purpose:     FInd the country code and std code for this office
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  {inc/ofc-this.i}

  FIND OfficeSettings OF Office WHERE OfficeSettings.SetName = "Country-Code" NO-LOCK NO-ERROR.
  IF AVAILABLE OfficeSettings THEN office-country-code = OfficeSettings.SetValue.

  FIND OfficeSettings OF Office WHERE OfficeSettings.SetName = "STD-Code" NO-LOCK NO-ERROR.
  IF AVAILABLE OfficeSettings THEN office-std-code = OfficeSettings.SetValue.

  FIND OfficeSettings OF Office WHERE OfficeSettings.SetName = "ISD-Prefix" NO-LOCK NO-ERROR.
  IF AVAILABLE(OfficeSettings) THEN office-isd-prefix = OfficeSettings.SetValue.

  FIND OfficeSettings OF Office WHERE OfficeSettings.SetName = "Country" NO-LOCK NO-ERROR.
  IF AVAILABLE(OfficeSettings) THEN office-country-name = OfficeSettings.SetValue.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE split-address Include 
PROCEDURE split-address :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER address AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER str-address AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER city AS CHAR NO-UNDO           INITIAL "".
DEF OUTPUT PARAMETER state AS CHAR NO-UNDO          INITIAL "".
DEF OUTPUT PARAMETER zip AS CHAR NO-UNDO            INITIAL "".
DEF OUTPUT PARAMETER country AS CHAR NO-UNDO        INITIAL "".

DEF VAR n AS INT NO-UNDO.
DEF VAR m AS INT NO-UNDO.
  address = TRIM(address).
  n = NUM-ENTRIES( address, "~n" ).

  /* country is probably the last, but we really have to verify it against a list */
  country = TRIM(ENTRY( n, address, "~n" )).
  IF country BEGINS "ATT" THEN DO:
    /* an Attention: line... */
    address = country + "~n" + TRIM( SUBSTRING( address, 1, LENGTH(address) - LENGTH(ENTRY( n, address, "~n" )))).
    country = TRIM(ENTRY( n, address, "~n" )).
  END.
  IF LOOKUP( country, other-countries) = 0 THEN
    country = office-country-name.
  ELSE DO:
    address = TRIM( SUBSTRING( address, 1, LENGTH(address) - LENGTH(ENTRY( n, address, "~n" )))).
    n = n - 1.
  END.

  /* "city, state zip" is the standard format... */
  city = TRIM(ENTRY( n, address, "~n" )).
  city = TRIM(city, ",").
  m = NUM-ENTRIES( city, " ").

  zip = TRIM( ENTRY( m, city, " ") ).
  /* if it has a digit in it, itis most likely to be a "ZIP" */
  IF (INDEX( zip, "0") + INDEX( zip, "1") + INDEX( zip, "2") + INDEX( zip, "3") + INDEX( zip, "4") + INDEX( zip, "5") + INDEX( zip, "6") + INDEX( zip, "7") + INDEX( zip, "8") + INDEX( zip, "9")) > 0 THEN DO:
    city = TRIM( SUBSTRING( city, 1, LENGTH(city) - LENGTH(ENTRY( m, city, " " )))).
    ENTRY(n,address,"~n") = city.
    m = m - 1.
  END.
  ELSE DO:
    zip = "".
  END.

  /* state is probably a field after a "," on the line, otherwise if the last   */
  /* field is <= 4 characters we will assume it's a state                       */
  city = TRIM(city, ",").
  IF INDEX( city, ",") > 0 THEN DO:
    state = TRIM( SUBSTRING( city, R-INDEX( city, ",") + 1) ).
    city = TRIM( SUBSTRING( city, 1, R-INDEX( city, ",") - 1) ).
    address = TRIM( SUBSTRING( address, 1, LENGTH(address) - LENGTH(ENTRY( n, address, "~n" )))).
    n = n - 1.
  END.
  ELSE IF m > 1 THEN DO:
    state = TRIM( SUBSTRING( city, R-INDEX( city, " ") + 1) ).
    IF LOOKUP( TRIM(state,"."), street-types ) > 0 THEN DO:
      city = "".
      state = "".
    END.
    ELSE DO:
      IF LENGTH(city) < 3 OR LOOKUP( state, city-suffixes) > 0 THEN
        state = "".
      ELSE IF LENGTH(state) < 4 THEN
        city = TRIM( SUBSTRING( city, 1, R-INDEX( city, " ") - 1) ).
      ELSE
        state = "".
      address = TRIM( SUBSTRING( address, 1, LENGTH(address) - LENGTH(ENTRY( n, address, "~n" )))).
      n = n - 1.
    END.
  END.
  ELSE IF n > 1 THEN DO:
    address = TRIM( SUBSTRING( address, 1, LENGTH(address) - LENGTH(ENTRY( n, address, "~n" )))).
    n = n - 1.
  END.

  str-address = address.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE split-fullname Include 
PROCEDURE split-fullname :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER full-name    AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER first-name   AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER last-name    AS CHAR NO-UNDO.

DEF VAR next-part AS CHAR NO-UNDO.
  IF NUM-ENTRIES( full-name, " " ) <= 1 THEN ASSIGN
    first-name = ""
    last-name  = full-name.
  ELSE DO:
    first-name = TRIM( ENTRY( 1, full-name, " " ) ).
    last-name  = TRIM( SUBSTR( full-name, LENGTH( first-name ) + 1 ) ).

    /* skip any title or so forth */
    IF LOOKUP( first-name, "Mr,Mrs,Miss,Ms,Sir,Mast,Mstr,Master") > 0 THEN DO:
      /* skip it */
      first-name = TRIM( ENTRY( 1, last-name, " " ) ).
      last-name  = TRIM( SUBSTR( last-name, LENGTH( first-name ) + 1 ) ).
    END.

    /* move any middle initials and stuff to be part of the first name */
    next-part = ENTRY( 1, last-name, " ").
    DO WHILE next-part <> last-name AND LENGTH(TRIM(next-part,".")) = 1 :
      first-name = first-name + " " + next-part.
      last-name = TRIM( SUBSTR( last-name, LENGTH( next-part ) + 1 ) ).
      next-part = ENTRY( 1, last-name, " ").
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE split-name Include 
PROCEDURE split-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER full-name AS CHAR NO-UNDO.

DEF OUTPUT PARAMETER p-title   AS CHAR NO-UNDO  INITIAL "".
DEF OUTPUT PARAMETER p-first   AS CHAR NO-UNDO  INITIAL "".
DEF OUTPUT PARAMETER p-middle  AS CHAR NO-UNDO  INITIAL "".
DEF OUTPUT PARAMETER p-last    AS CHAR NO-UNDO  INITIAL "".
DEF OUTPUT PARAMETER p-suffix  AS CHAR NO-UNDO  INITIAL "".

DEF VAR next-part AS CHAR NO-UNDO.
  IF NUM-ENTRIES( full-name, " " ) <= 1 THEN DO:
    p-first = full-name.
    p-last  = "".
    RETURN.
  END.

  p-first = TRIM( ENTRY( 1, full-name, " " ), " ." ).
  p-last  = TRIM( SUBSTR( full-name, INDEX(full-name, " ") + 1 ) ).

  /* extract any title */
  IF LOOKUP( p-first, "Mr,Mrs,Miss,Ms,Sir,Mast,Mstr,Master,Dr,Prof,Dame,(none)") > 0 THEN DO:
    p-title = p-first.
    /* and then skip it */
    p-first = TRIM( ENTRY( 1, p-last, " " ), "." ).
    p-last  = TRIM( SUBSTR( p-last, INDEX( p-last, " " ) + 1 ) ).
  END.

  /* extract any suffix */
  IF NUM-ENTRIES( p-last, " ") > 1 THEN DO:
    p-suffix = TRIM(ENTRY( NUM-ENTRIES( p-last, " "), p-last, " "), " .").
    IF LOOKUP( p-suffix, "Jr,Snr,I,II,III,IV,V,VI,VII,VIII,IX,X,XI,XII,XIII,XIV,XV,XVI,XVII,XVIII,XIX,XX") > 0 THEN
      p-last  = TRIM( SUBSTR( p-last, 1, R-INDEX(p-last, " ") - 1 ) ).
    ELSE
      p-suffix = "".
  END.

  /* split out any middle names */
  IF NUM-ENTRIES( p-last, " ") > 1 THEN DO:
    next-part = ENTRY( 1, p-last, " ").
    DO WHILE next-part <> p-last:
      IF LOOKUP( next-part, "de,van,der,mc,mac,o,di,des,en,st,te") > 0 THEN LEAVE.
      p-middle = p-middle + next-part + " ".
      p-last = TRIM( SUBSTR( p-last, INDEX( p-last, " ") + 1 ) ).
      next-part = ENTRY( 1, p-last, " ").
    END.
    p-middle = TRIM(p-middle).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE split-phone Include 
PROCEDURE split-phone :
/*------------------------------------------------------------------------------
  Purpose:     Parses/Splits a single string phone number into
               country code, STD code, phone number
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER full-phone   AS CHAR NO-UNDO.

DEF OUTPUT PARAMETER c-country    LIKE PhoneDetail.cCountryCode NO-UNDO INITIAL "".
DEF OUTPUT PARAMETER c-std        LIKE PhoneDetail.cSTDCode     NO-UNDO INITIAL "".
DEF OUTPUT PARAMETER phone-no     LIKE PhoneDetail.Number       NO-UNDO INITIAL "".

  /* Parse the phone number ! */
  DEF VAR part-parse    AS CHAR NO-UNDO.
  DEF VAR ch            AS CHAR NO-UNDO.
  DEF VAR extra-phone   AS CHAR NO-UNDO.
  DEF VAR i             AS INT  NO-UNDO.
  DEF VAR n             AS INT  NO-UNDO.

  /* If the phone number contains an @ then only put it in phone-no and return */
  IF INDEX(full-phone, "@") > 0 THEN DO:
    phone-no = full-phone.
    RETURN.
  END.

  /* always replace "-" with " " first for improved parsing! */
  full-phone = REPLACE( TRIM(full-phone), "-", " " ).

  /* only parse up to the last 'phone-number' character */
  n = LENGTH(full-phone).
  DO i = 1 TO n:
    ch = SUBSTR(full-phone, i, 1).
    IF INDEX( "0123456789+() ", ch) = 0 THEN DO:
      extra-phone = SUBSTR( full-phone, i).
      full-phone = SUBSTR( full-phone, 1, i - 1).
      n = i.
    END.
  END.

  /* assume the last two are usually the base phone number, but if we   */
  /* detect sufficient structure we can override that choice            */
  n = MAXIMUM( NUM-ENTRIES( full-phone, " ") - 2, 0).
  IF n < 1 AND SUBSTR( full-phone, 1, 1) = "+" THEN n = n + 1.
  IF n < 1 AND SUBSTR( full-phone, 1, LENGTH(office-isd-prefix) ) = office-isd-prefix THEN n = n + 1.
  IF n < 2 AND INDEX( full-phone, "(") <> 0 THEN n = n + 1.
  IF n < 1 AND SUBSTR( full-phone, 1, 1) = "0" THEN n = 1.
  n = MINIMUM( NUM-ENTRIES( full-phone, " "), n).

  DO i = 1 TO n:
      
    part-parse = ENTRY( i, full-phone, " " ).

    IF c-country <> "" AND c-std <> "" THEN
      /* no point picking country/std information twice */
      phone-no = phone-no + part-parse + " ".
    ELSE IF c-country <> "" AND c-std = "" THEN ASSIGN
      /* since we have a country, but no STD, next bit must be STD */
      c-std = part-parse
      c-std = REPLACE( c-std, "(", "" )
      c-std = REPLACE( c-std, ")", "" ).
    ELSE IF SUBSTR( part-parse, 1, 1 ) = "+" THEN DO:
      /* definitely a country */
      c-country = REPLACE( part-parse, "+", "" ).
      IF INDEX( c-country, "(") <> 0 THEN DO:
        /* also runs straight into STD without spaces */
        c-std = ENTRY( 2, c-country, "(" ).
        c-country = ENTRY( 1, c-country, "(" ).

        /* in case it runs straight into start of phone number as well */
        c-std = REPLACE( c-std, ")", " " ).
        c-std = c-std + " ".
        phone-no = SUBSTR( c-std, INDEX( c-std, " ") + 1 ).
        c-std = ENTRY( 1, c-std, " ").
      END.
    END.
    ELSE IF c-country = "" AND SUBSTR( part-parse, 1, LENGTH(office-isd-prefix) ) = office-isd-prefix THEN DO:
      c-country = TRIM( REPLACE( part-parse, office-isd-prefix, "" ) ).
      IF c-country = "" THEN ASSIGN
        i = i + 1
        part-parse = ENTRY( i, full-phone, " " )
        c-country = part-parse.
      IF LENGTH(c-country) > 2 AND LOOKUP( SUBSTR( c-country, 1, 2), "44,61,64") <> 0 THEN DO:
      ASSIGN
        /* numbers of the style 00612 9223 5601 (quite common really) */
        c-std = SUBSTR( c-country, 3)
        c-country = SUBSTR( c-country, 1, 2).
       END.
    END.
    ELSE IF SUBSTR( part-parse, 1, 1 ) = "(" THEN DO:
      c-std = part-parse.
      c-std = REPLACE( c-std, "(", "" ).

      /* in case it runs straight into start of phone number as well */
      c-std = REPLACE( c-std, ")", " " ).
      c-std = c-std + " ".
      phone-no = SUBSTR( c-std, INDEX( c-std, " ") + 1 ).
      c-std = ENTRY( 1, c-std, " ").
    END.
    ELSE IF c-std = "" AND SUBSTR( part-parse, 1, 1 ) = "0" THEN
      c-std = SUBSTR( part-parse, 2 ).
    ELSE
      phone-no = phone-no + part-parse + " ".
      
  END.
  DO WHILE i <= NUM-ENTRIES( full-phone, " "):
    phone-no = phone-no + ENTRY( i, full-phone, " ") + " ".
    i = i + 1.
  END.
  c-country = TRIM( c-country ).
  c-std = TRIM( c-std ).
  phone-no = TRIM(phone-no)
           + (IF extra-phone = "" THEN "" ELSE " " + extra-phone).

  IF c-country <> "" AND c-std = "" 
        AND c-country <> office-country-code
        AND LOOKUP( c-country, "65,852") = 0
  THEN ASSIGN
    c-std = SUBSTRING( c-country, LENGTH(c-country), 1)
    c-country = SUBSTRING( c-country, 1, LENGTH(c-country) - 1).

  IF c-country = "" OR c-country = office-country-code THEN DO:
    c-country = office-country-code.
    IF c-std = "" AND LOOKUP( ENTRY( 1, phone-no, " "), "3,4,9" ) <> 0 THEN ASSIGN
      c-std = ENTRY( 1, phone-no, " ")
      phone-no = SUBSTR( phone-no, 3 ).
    ELSE IF c-std = "" THEN
      c-std = office-std-code.
  END.

  /* not even Hobart has a preceding 0 on their STD code now */
  c-std = LEFT-TRIM( c-std, "0").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION combine-name Include 
FUNCTION combine-name RETURNS CHARACTER
  ( INPUT p-title AS CHAR, INPUT p-first AS CHAR, INPUT p-middle AS CHAR, INPUT p-last AS CHAR, INPUT p-suffix AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR full-name  AS CHAR NO-UNDO.

  IF p-title = ? THEN p-title = "".
  IF p-first = ? THEN p-first = "".
  IF p-middle = ? THEN p-middle = "".
  IF p-last = ? THEN p-last = "".
  IF p-suffix = ? THEN p-suffix = "".

  full-name = TRIM( p-title )   + ' ' + TRIM( p-first ).
  full-name = TRIM( full-name ) + ' ' + TRIM( p-middle ).
  full-name = TRIM( full-name ) + ' ' + TRIM( p-last ).
  full-name = TRIM(TRIM( full-name ) + ' ' + TRIM( p-suffix )).

  RETURN full-name.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-phone-nos Include 
FUNCTION get-phone-nos RETURNS CHARACTER
  ( INPUT person-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR no-found AS INT NO-UNDO INITIAL 0.
DEF VAR phone-nos AS CHAR NO-UNDO INITIAL "".
DEF VAR this-phone AS CHAR NO-UNDO INITIAL "".

DEF VAR this-type AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
  DO i = 1 TO no-phone-types:
    this-type = ENTRY( i, phone-types).
    FIND PhoneDetail WHERE PhoneDetail.PersonCode = person-code
                       AND PhoneDetail.PhoneType = this-type NO-LOCK NO-ERROR.
    IF AVAILABLE(PhoneDetail) THEN DO:
      no-found = no-found + 1.
      RUN combine-phone( PhoneDetail.cCountryCode, PhoneDetail.cSTDCode, PhoneDetail.Number, OUTPUT this-phone ).
      phone-nos = phone-nos + this-type + ":" + this-phone + ", ".
    END.
  END.

  phone-nos = TRIM(phone-nos, ", ").

  RETURN phone-nos.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

