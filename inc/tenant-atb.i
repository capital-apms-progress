&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

{inc/ofc-this.i}
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .4
         WIDTH              = 40.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-get-aged-balances) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-aged-balances Method-Library 
PROCEDURE get-aged-balances :
/*------------------------------------------------------------------------------
  Purpose:  Calculate the aged balances for the current tenant
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER tenant-code AS INT NO-UNDO.
DEF INPUT PARAMETER current-month-code AS INT NO-UNDO.
DEF OUTPUT PARAMETER b0 AS DEC NO-UNDO INITIAL 0.
DEF OUTPUT PARAMETER b1 AS DEC NO-UNDO INITIAL 0.
DEF OUTPUT PARAMETER b2 AS DEC NO-UNDO INITIAL 0.
DEF OUTPUT PARAMETER b3 AS DEC NO-UNDO INITIAL 0.

DEF VAR i AS INT NO-UNDO.
DEF BUFFER Trn FOR AcctTran.
DEF BUFFER m0 FOR Month.
DEF BUFFER m1 FOR Month.
DEF BUFFER m2 FOR Month.
DEF BUFFER m3 FOR Month.

  FIND m0 WHERE m0.MonthCode = current-month-code NO-LOCK.
  FIND LAST m1 WHERE m1.MonthCode < m0.MonthCode NO-LOCK.
  FIND LAST m2 WHERE m2.MonthCode < m1.MonthCode NO-LOCK.
  FIND LAST m3 WHERE m3.MonthCode < m2.MonthCode NO-LOCK.

  FOR EACH Trn NO-LOCK WHERE Trn.EntityType   = 'T'
                AND Trn.EntityCode   = tenant-code
                AND Trn.AccountCode  = sundry-debtors
/*                AND Trn.MonthCode   <= current-month-code */
                AND Trn.ClosedState <> "F":
    IF Trn.MonthCode <= m3.MonthCode THEN
      b3 = b3 + Trn.Amount.
    ELSE IF Trn.MonthCode = m2.MonthCode THEN
      b2 = b2 + Trn.Amount.
    ELSE IF Trn.MonthCode = m1.MonthCode THEN
      b1 = b1 + Trn.Amount.
    ELSE
      b0 = b0 + Trn.Amount.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

