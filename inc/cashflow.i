&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

DEF VAR cf-split-date AS CHAR NO-UNDO.
DEF VAR cf-split-info AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD cf-add-to-bucket Include 
FUNCTION cf-add-to-bucket RETURNS CHARACTER
  ( INPUT bucket-id AS CHAR, INPUT amount AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD cf-find-bucket Include 
FUNCTION cf-find-bucket RETURNS CHARACTER
  ( INPUT d-on AS DATE, INPUT d-1 AS DATE, INPUT d-n AS DATE, INPUT mthly AS LOGICAL, INPUT units AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD end-of-bucket Include 
FUNCTION end-of-bucket RETURNS DATE
  ( INPUT ends AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD flow-on-1st Include 
FUNCTION flow-on-1st RETURNS LOGICAL
  ( INPUT cf-type AS CHAR, cf-freq AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD start-of-bucket Include 
FUNCTION start-of-bucket RETURNS DATE
  ( INPUT ends AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = .33
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Include 
/* ************************* Included-Libraries *********************** */

{inc/string.i}
{inc/date.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE dump-cash-flow Include 
PROCEDURE dump-cash-flow :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER names AS CHAR NO-UNDO.
DEF INPUT PARAMETER flows AS CHAR NO-UNDO.
DEF INPUT PARAMETER changes AS CHAR NO-UNDO.

DEF VAR line AS CHAR NO-UNDO.

  line = STRING( CashFlow.ScenarioCode, ">>>9") + " "
       + CashFlow.EntityType + STRING( CashFlow.EntityCode, "99999") + "-" + STRING( CashFlow.AccountCode, "9999.99")
       + CashFlow.CFChangeType + " " + CashFlow.CashFlowType + " " + CashFlow.FrequencyCode  + " "
       + (IF CashFlow.StartDate = ? THEN "?" ELSE STRING( CashFlow.StartDate, "99/99/9999")) + " "
       + (IF CashFlow.EndDate = ? THEN "?" ELSE STRING( CashFlow.EndDate, "99/99/9999")) + " "
       + STRING( CashFlow.Amount, "->>,>>>,>>9.99") + " "
       + CashFlow.RelatedKey + "   Change:" + changes.
  debug-event( line ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE split-cash-flow Include 
PROCEDURE split-cash-flow :
/*------------------------------------------------------------------------------
  Purpose:  Split the cash flow into the occurrences during the period.
------------------------------------------------------------------------------*/
DEF INPUT PARAM fc-1   AS DATE NO-UNDO.     /* fc = forecast */
DEF INPUT PARAM fc-n   AS DATE NO-UNDO.
DEF INPUT PARAM fc-fq  AS CHAR NO-UNDO.
DEF INPUT PARAM fw-1   AS DATE NO-UNDO.     /* fw = cash flow */
DEF INPUT PARAM fw-n   AS DATE NO-UNDO.
DEF INPUT PARAM fw-fq  AS CHAR NO-UNDO.
DEF INPUT PARAM fw-amt AS DEC  NO-UNDO.
DEF INPUT PARAM fw-1st AS LOGI NO-UNDO.
DEF OUTPUT PARAM split-dates AS CHAR NO-UNDO.
DEF OUTPUT PARAM split-flow AS CHAR NO-UNDO.

  cf-split-date = "". cf-split-info = "".
  IF fw-1 = ? THEN fw-1 = fc-1.
  IF fw-n = ? THEN fw-n = fc-n.

  /* find out about the periods we're dealing with */
  DEF VAR fc-units   AS INT  NO-UNDO.   DEF VAR fc-mthly   AS LOGI NO-UNDO.
  DEF VAR fw-units   AS INT  NO-UNDO.   DEF VAR fw-mthly   AS LOGI NO-UNDO.
  fc-units = get-freq-months( fc-fq ).
  fc-mthly = (fc-units <> 0 AND fc-units <> ?).
  IF NOT fc-mthly THEN fc-units = get-freq-days( fc-fq ).

  fw-units = get-freq-months( fw-fq ).
  fw-mthly = (fw-units <> 0 AND fw-units <> ?).
  IF NOT fw-mthly THEN fw-units = get-freq-days( fw-fq ).

  IF fc-units = 0 OR fw-units = 0 OR fc-units = ? OR fw-units = ? THEN DO:
    MESSAGE fw-fq "seems to be an ill-defined frequency!"
                VIEW-AS ALERT-BOX INFORMATION.
    RETURN.
  END.
  IF fc-mthly THEN ASSIGN   fc-1 = first-of-month( fc-1 )
                            fc-n = last-of-month( fc-n ).

  /* Split the cash flow into the appropriate buckets */
  DEF VAR bucket AS CHAR NO-UNDO.
  DEF VAR pay-no AS INT NO-UNDO.
  DEF VAR pay-1  AS DATE NO-UNDO.
  DEF VAR pay-d  AS DATE NO-UNDO.
  pay-1 = next-date-after( fw-1, fc-1, (IF fw-mthly THEN "M" ELSE "D"), fw-units).
  IF fw-1st THEN pay-1 = first-of-month(pay-1).

DEF VAR b-1 AS DATE NO-UNDO.
DEF VAR b-n AS DATE NO-UNDO.
DEF VAR part-period AS DEC NO-UNDO INITIAL 1.

&IF DEFINED(DEBUG-END) &THEN
  debug-event("Splitting Flow: fc-1=" + STRING( fc-1, "99/99/9999")
                        + ",   fc-n=" + STRING( fc-n, "99/99/9999")
                        + ",   fc-fq=" + fc-fq
                        + " = " + STRING(fc-units) + (IF fc-mthly THEN "M" ELSE "D")
                        + ",   fw-1=" + STRING( fw-1, "99/99/9999")
                        + ",   fw-n=" + STRING( fw-n, "99/99/9999")
                        + ",   fw-fq=" + fw-fq
                        + " = " + STRING(fw-units) + (IF fw-mthly THEN "M" ELSE "D")
                        + ",  fw-1st=" + STRING( fw-1st, "Yes/No")
                        + ",  fw-amt=" + STRING( fw-amt ) ).
&ENDIF

  pay-d = pay-1.
  DO WHILE pay-d <= fc-n AND pay-d <= fw-n:

    /* add this pay into the appropriate bucket */
    bucket = cf-find-bucket( pay-d, fc-1, fc-n, fc-mthly, fc-units ).
    b-1 = start-of-bucket(bucket).
    b-n = end-of-bucket(bucket).
    IF (fw-1 > b-1 OR fw-n < b-n)
            AND fw-mthly AND fc-mthly AND fc-units >= fw-units THEN DO:
      part-period = (MIN(fw-n, b-n) - MAX(fw-1, b-1) + 1) / (b-n - b-1 + 1).

&IF DEFINED(DEBUG-END) &THEN
      debug-event("Part period: fw-1=" + STRING( fw-1, "99/99/9999")
                         + ",   fw-n=" + STRING( fw-n, "99/99/9999")
                         + ",   b-1="  + STRING( b-1, "99/99/9999")
                         + ",   b-n="  + STRING( b-n, "99/99/9999")
                         + ",  pay-d=" + STRING( pay-d, "99/99/9999")
                         + ",  part="  + STRING( part-period ) ).
&ENDIF

      IF part-period > 1 THEN part-period = 1.
      IF part-period > 0 THEN
        cf-add-to-bucket( bucket, fw-amt * part-period ).
    END.
    ELSE
      cf-add-to-bucket( bucket, fw-amt ).

    /* Get the next pay date */
    pay-no = pay-no + 1.
    IF fw-mthly THEN pay-d = add-months( pay-1, fw-units * pay-no ).
                ELSE pay-d = pay-d + fw-units.
  END.

  split-dates = cf-split-date.
  split-flow = cf-split-info.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION cf-add-to-bucket Include 
FUNCTION cf-add-to-bucket RETURNS CHARACTER
  ( INPUT bucket-id AS CHAR, INPUT amount AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  DEF VAR list-idx AS INT NO-UNDO.

&IF DEFINED(DEBUG-END) &THEN
  debug-event( "Adding flow to " + bucket-id + " of " + STRING(amount)).
&ENDIF

  list-idx = LOOKUP( bucket-id, cf-split-date ).
  IF list-idx = 0 THEN DO:
    add-to-list( cf-split-date, bucket-id ).
    add-to-list( cf-split-info, STRING(amount) ).
  END.
  ELSE
    ENTRY(list-idx, cf-split-info) = STRING( DEC(ENTRY(list-idx, cf-split-info)) + amount ).

  RETURN "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION cf-find-bucket Include 
FUNCTION cf-find-bucket RETURNS CHARACTER
  ( INPUT d-on AS DATE, INPUT d-1 AS DATE, INPUT d-n AS DATE, INPUT mthly AS LOGICAL, INPUT units AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR b-gin AS DATE NO-UNDO.
DEF VAR b-end AS DATE NO-UNDO.

  /* find the first start-date after this one */
  b-gin = next-date-after( d-1, d-on + 1, (IF mthly THEN "M" ELSE "D"), units).
  b-end = b-gin - 1.
  IF mthly THEN b-gin = add-months(b-gin, - units).
           ELSE b-gin = b-gin - units.

  RETURN STRING(b-gin, "99/99/9999") + "-" + STRING(b-end, "99/99/9999").


END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION end-of-bucket Include 
FUNCTION end-of-bucket RETURNS DATE
  ( INPUT ends AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN DATE( ENTRY(2,ends,"-")).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION flow-on-1st Include 
FUNCTION flow-on-1st RETURNS LOGICAL
  ( INPUT cf-type AS CHAR, cf-freq AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Decide whether the cashflow is forced to the 1st of the month
    Notes:  basically, monthly rentals are done this way.
------------------------------------------------------------------------------*/

  RETURN cf-freq = "MNTH" AND
                 (cf-type = "RENT" OR cf-type = "MRNT"
               OR cf-type = "OPEX" OR cf-type = "MOPX" ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION start-of-bucket Include 
FUNCTION start-of-bucket RETURNS DATE
  ( INPUT ends AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN DATE( ENTRY(1,ends,"-")).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


