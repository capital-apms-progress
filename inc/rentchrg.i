&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    File        : inc/rentchrg.i
    Purpose     : Common functions for handling rent charge structures

    Person              Date    Change
    ======           =========  ============================================
    Tyrone McAuley   5/12/1997  Written
    Andrew McMillan  15/5/1998  Severely hacked until it actually works!
    Andrew McMillan  16/7/1998  Added RentCharge-Outgoings functionality for AmTrust
    Andrew McMillan  9/9/1998   Added 'rent-charge-one-period' option which is used
                                by some callers to tell the program to charge a single
                                month ignoring whether it's already charged.
    Andrew McMillan  12/4/2000  Added 'show-override-descriptions' option which is
                                used by the rental advice program to get all descriptions.
    Andrew McMillan  12/4/2000  Added 'use-past-charges' option which lets you calculate
                                charges as they were As At some time past.
  ------------------------------------------------------------------------*/

DEF VAR annual-outgoings-charges AS DEC NO-UNDO.
DEF VAR rent-charge-one-period AS LOGI NO-UNDO INITIAL No.
DEF VAR allow-past-leases AS LOGI NO-UNDO INITIAL No.
DEF VAR show-override-descriptions AS LOGI NO-UNDO INITIAL No.
DEF VAR check-lease-inclusion AS LOGI NO-UNDO INITIAL No.
DEF VAR use-past-charges AS LOGI NO-UNDO INITIAL No.

DEF TEMP-TABLE ChargeDetail NO-UNDO
  FIELD Sequence      AS INT
  FIELD TenantCode    LIKE Tenant.TenantCode
  FIELD ChargeType    LIKE RentChargeType.RentChargeType
  FIELD Description   LIKE RentCharge.Description
  FIELD ChargeSeq     LIKE RentCharge.Sequence
  FIELD ChargeStart   LIKE RentChargeLine.StartDate
  FIELD ChargeAmount  LIKE RentChargeLine.Amount
  FIELD AnnualAmount  LIKE RentChargeLine.Amount
  FIELD PeriodFraction  AS DECIMAL
  FIELD TaxableAmount LIKE RentChargeLine.Amount
  FIELD EntityType    LIKE AcctTran.EntityType
  FIELD EntityCode    LIKE AcctTran.EntityCode
  FIELD AccountCode   LIKE AcctTran.AccountCode
  FIELD AreaDescription LIKE TenancyLease.AreaDescription
  FIELD TenancyLeaseCode LIKE TenancyLease.TenancyLeaseCode
  FIELD ChargedFrom   AS DATE
  FIELD ChargedUpTo   AS DATE
  FIELD ChargeEnd     LIKE RentChargeLine.EndDate

  INDEX ChargeDetail IS PRIMARY
    TenantCode
    TenancyLeaseCode
    ChargeType
    EntityType
    EntityCode
    AccountCode
    Sequence /*
  INDEX XAKRentCharge IS UNIQUE
    TenancyLeaseCode
    ChargeSeq */ .

DEF VAR charge-detail-seq AS INT NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set.i "RentCharge-Outgoings" "og-method"}
IF NOT AVAILABLE(OfficeSetting) THEN og-method = "".
{inc/ofc-set.i "RentCharge-OG-By-Lease" "charge-og-lease"}
IF NOT AVAILABLE(OfficeSetting) THEN charge-og-lease = "Yes".
IF NOT AVAILABLE(OfficeSetting) THEN og-method = "".
{inc/ofc-set-l.i "RentCharge-PartMonth-Annual" "part-month-annual"}
IF NOT AVAILABLE(OfficeSetting) THEN part-month-annual = Yes.

{inc/ofc-set.i "GST-Types-Never" "gst-types-never"}
{inc/ofc-set.i "GST-Types-Always" "gst-types-always"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Method-Library
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-can-commit-charge) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD can-commit-charge Method-Library 
FUNCTION can-commit-charge RETURNS LOGICAL
  ( INPUT lease-code AS INT, INPUT sequence-code AS INT, INPUT start-date AS DATE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-annual-outgoings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-annual-outgoings Method-Library 
FUNCTION get-annual-outgoings RETURNS DECIMAL
  ( INPUT lease-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-not-can-action-rent-review) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD not-can-action-rent-review Method-Library 
FUNCTION not-can-action-rent-review RETURNS LOGICAL
  ( INPUT lease-code AS INT, INPUT sequence-code AS INT, INPUT start-date AS DATE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = 20.45
         WIDTH              = 35.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/date.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-add-charge-detail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-charge-detail Method-Library 
PROCEDURE add-charge-detail :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER tenant-code    LIKE Tenant.TenantCode             NO-UNDO.
DEF INPUT PARAMETER charge-type    LIKE RentChargeType.RentChargeType NO-UNDO.
DEF INPUT PARAMETER entity-type    LIKE AcctTran.EntityType           NO-UNDO.
DEF INPUT PARAMETER entity-code    LIKE AcctTran.EntityCode           NO-UNDO.
DEF INPUT PARAMETER account-code   LIKE AcctTran.AccountCode          NO-UNDO.
DEF INPUT PARAMETER description    LIKE RentCharge.Description        NO-UNDO.
DEF INPUT PARAMETER lease-code     LIKE TenancyLease.TenancyLeaseCode NO-UNDO.
DEF INPUT PARAMETER area-description LIKE RentCharge.Description      NO-UNDO.
DEF INPUT PARAMETER charge-fraction AS DECIMAL                        NO-UNDO.
DEF INPUT PARAMETER taxable         AS LOGICAL                        NO-UNDO.
DEF INPUT PARAMETER charged-from   AS DATE                            NO-UNDO.
DEF INPUT PARAMETER charged-to     AS DATE                            NO-UNDO.
DEF INPUT PARAMETER freq-units     AS DEC                             NO-UNDO.
DEF INPUT PARAMETER freq-type      AS CHAR                            NO-UNDO.

DEF VAR annual-amount AS DEC NO-UNDO.
DEF VAR period-amount AS DEC NO-UNDO.

  IF show-override-descriptions AND AVAILABLE(RentCharge) THEN DO:
    IF RentCharge.Description <> "" AND RentCharge.Description <> ? THEN
      description = /* RentChargeType.Description + " - " + */ RentCharge.Description.
  END.
  IF CAN-DO( gst-types-always, charge-type ) THEN taxable = YES.
  ELSE IF CAN-DO( gst-types-never, charge-type ) THEN taxable = NO.

  IF og-method <> "" AND og-method = RentCharge.RentChargeType 
                     AND charge-og-lease <> "Charge" 
  THEN ASSIGN
    annual-amount = annual-outgoings-charges
    period-amount = ((annual-amount * freq-units) / (IF freq-type = "D" THEN 365 ELSE 12)).
  ELSE ASSIGN
    period-amount = RentChargeLine.Amount
    annual-amount = ((period-amount / freq-units) * (IF freq-type = "D" THEN 365 ELSE 12)).

  CREATE  ChargeDetail.
  ASSIGN  ChargeDetail.Sequence    = charge-detail-seq
          ChargeDetail.TenantCode  = tenant-code
          ChargeDetail.ChargeType  = charge-type
          ChargeDetail.ChargeSeq   = RentCharge.Sequence
          ChargeDetail.ChargeStart = RentChargeLine.StartDate
          ChargeDetail.Description = description
          ChargeDetail.AreaDescription = area-description
          ChargeDetail.EntityType  = entity-type
          ChargeDetail.EntityCode  = entity-code
          ChargeDetail.AccountCode = account-code
          ChargeDetail.TenancyLeaseCode = lease-code
          ChargeDetail.ChargeAmount  = ROUND( charge-fraction * period-amount, 2)
          ChargeDetail.TaxableAmount = (IF taxable THEN ChargeDetail.ChargeAmount ELSE 0)
          ChargeDetail.PeriodFraction= charge-fraction
          ChargeDetail.AnnualAmount  = annual-amount
          ChargeDetail.ChargedFrom   = charged-from
          ChargeDetail.ChargedUpTo   = charged-to
          ChargeDetail.ChargeEnd     = RentChargeLine.EndDate.

  charge-detail-seq = charge-detail-seq + 1.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-build-daily-charges) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-daily-charges Method-Library 
PROCEDURE build-daily-charges :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER tenant-code AS INT NO-UNDO.
DEF INPUT PARAMETER tenant-et AS CHAR NO-UNDO.
DEF INPUT PARAMETER tenant-ec AS INT NO-UNDO.
DEF INPUT PARAMETER lease-code AS INT NO-UNDO.
DEF INPUT PARAMETER lease-desc AS CHAR NO-UNDO.
DEF INPUT PARAMETER lease-tax AS LOGI NO-UNDO.
DEF INPUT PARAMETER start-day AS DATE NO-UNDO.
DEF INPUT PARAMETER stop-day  AS DATE NO-UNDO.

DEF VAR period-fraction  AS DEC NO-UNDO.
DEF VAR taxable-amount AS DEC NO-UNDO.
DEF VAR rent-end-day  AS DATE NO-UNDO.
DEF VAR freq-units AS INT  NO-UNDO.
DEF VAR paid-up-to AS DATE NO-UNDO.
DEF VAR this-pay-date AS DATE NO-UNDO.
DEF VAR next-pay-date AS DATE NO-UNDO.
DEF VAR amount-multiplier AS INT NO-UNDO    INITIAL 1.

  freq-units = get-freq-days( RentChargeLine.FrequencyCode ).
  IF start-day > stop-day THEN DO:
    /* rent-end-day is used as temp variable, but later it's value is correct! */
    rent-end-day = start-day.
    start-day = stop-day.
    stop-day = rent-end-day.
    amount-multiplier = -1.
  END.
  ELSE
    rent-end-day = (IF RentChargeLine.EndDate = ? THEN TODAY + 1000000 ELSE RentChargeLine.EndDate).

  this-pay-date = start-day.
  DO WHILE this-pay-date <= stop-day:
    next-pay-date = next-date-after( RentChargeLine.StartDate, this-pay-date + 1, "D", freq-units ).
    paid-up-to = MINIMUM( next-pay-date - 1, rent-end-day).
  
    /* This should work right for part-period charges */
    period-fraction = (((paid-up-to - this-pay-date) + 1) / freq-units).
  
    RUN add-charge-detail( tenant-code, RentCharge.RentChargeType,
            (IF RentCharge.EntityType <> "" THEN RentCharge.EntityType ELSE tenant-et ),
            (IF RentCharge.EntityType <> "" THEN RentCharge.EntityCode ELSE tenant-ec ),
            (IF CAN-FIND( FIRST ChartOfAccount OF RentCharge ) THEN RentCharge.AccountCode ELSE RentChargeType.AccountCode),
            (IF RentCharge.RentChargeType <> "S" THEN RentChargeType.Description ELSE RentCharge.Description ),
            lease-code, lease-desc, amount-multiplier * period-fraction,
            lease-tax, this-pay-date, paid-up-to, freq-units, "D" ).

    this-pay-date = next-pay-date.

    IF rent-charge-one-period THEN this-pay-date = stop-day + 1.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-build-monthly-charges) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-monthly-charges Method-Library 
PROCEDURE build-monthly-charges :
/*------------------------------------------------------------------------------
  Purpose:  Build monthly charges
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER tenant-code AS INT NO-UNDO.
DEF INPUT PARAMETER tenant-et AS CHAR NO-UNDO.
DEF INPUT PARAMETER tenant-ec AS INT NO-UNDO.
DEF INPUT PARAMETER lease-code AS INT NO-UNDO.
DEF INPUT PARAMETER lease-desc AS CHAR NO-UNDO.
DEF INPUT PARAMETER lease-tax AS LOGI NO-UNDO.
DEF INPUT PARAMETER start-day AS DATE NO-UNDO.
DEF INPUT PARAMETER stop-day  AS DATE NO-UNDO.

DEF VAR period-fraction AS DEC NO-UNDO.
DEF VAR taxable-amount AS DEC NO-UNDO.
DEF VAR rent-end-day  AS DATE NO-UNDO.
DEF VAR freq-units AS INT  NO-UNDO.
DEF VAR description AS CHAR NO-UNDO.
DEF VAR paid-up-to AS DATE NO-UNDO.
DEF VAR this-pay-date AS DATE NO-UNDO.
DEF VAR next-pay-date AS DATE NO-UNDO.
DEF VAR amount-multiplier AS INT NO-UNDO    INITIAL 1.

  freq-units = get-freq-months( RentChargeLine.FrequencyCode ).
  IF start-day > stop-day THEN DO:
    /* rent-end-day is used as temp variable, but later it's value is correct! */
    rent-end-day = start-day.
    start-day = stop-day.
    stop-day = rent-end-day.
    amount-multiplier = -1.
  END.
  ELSE
    rent-end-day = (IF RentChargeLine.EndDate = ? THEN TODAY + 1000000 ELSE RentChargeLine.EndDate).

  this-pay-date = start-day.
  DO WHILE this-pay-date <= stop-day:
    IF freq-units = 1 THEN
      next-pay-date = next-date-after( DATE(MONTH(this-pay-date), 1, YEAR(this-pay-date)), this-pay-date + 1, "M", freq-units ).
    ELSE
      next-pay-date = next-date-after( this-pay-date, this-pay-date + 1, "M", freq-units ).

    paid-up-to = MINIMUM( next-pay-date - 1, rent-end-day).

    IF part-month-annual THEN
      period-fraction = ((paid-up-to - this-pay-date + 1) / 365 ) * 12.
    ELSE
      period-fraction = ((paid-up-to - this-pay-date + 1) / month-days(paid-up-to) ).

    IF period-fraction < 0 THEN        period-fraction = 0.
    ELSE IF period-fraction > 1 THEN   period-fraction = 1.
    IF DAY(this-pay-date) = 1 AND paid-up-to = last-of-month(paid-up-to) THEN period-fraction = 1.

    IF freq-units > 1 THEN DO:
      period-fraction = 1.
    END.
    ELSE IF period-fraction <> 1 AND freq-units > 1 AND freq-units < 12 THEN DO:
/* NOTE: We are trying to handle a part-period correctly where the   */
/* frequency is non-monthly.  It should work out the number of whole */
/* months and charge a monthly amount for each, then work out the    */
/* part month charge as a fraction of that month.                    */
    DEF VAR date-m1 AS DATE NO-UNDO.
    DEF VAR date-m2 AS DATE NO-UNDO.

      period-fraction = 0.
      date-m1 = this-pay-date.
      DO WHILE date-m1 < paid-up-to:
        date-m2 = next-date-after( this-pay-date, date-m1 + 1, "M", 1).
        IF date-m2 > paid-up-to THEN DO:
          period-fraction = period-fraction + ((paid-up-to - date-m1 + 1) / (date-m2 - date-m1)).
        END.
        ELSE
          period-fraction = period-fraction + 1 .
        date-m1 = date-m2.
      END.
      period-fraction = period-fraction / freq-units.
    END.
/*
    MESSAGE start-day stop-day rent-end-day SKIP
            period-fraction RentChargeLine.Amount
            VIEW-AS ALERT-BOX .
*/
  description = (IF RentCharge.RentChargeType <> "S" THEN RentChargeType.Description ELSE RentCharge.Description).

    RUN add-charge-detail( tenant-code, RentCharge.RentChargeType,
            (IF RentCharge.EntityType <> "" THEN RentCharge.EntityType ELSE tenant-et ),
            (IF RentCharge.EntityType <> "" THEN RentCharge.EntityCode ELSE tenant-ec ),
            (IF CAN-FIND( FIRST ChartOfAccount OF RentCharge ) THEN RentCharge.AccountCode ELSE RentChargeType.AccountCode),
            description,
            lease-code, lease-desc, amount-multiplier * period-fraction,
            lease-tax, this-pay-date, paid-up-to, freq-units, "M" ).
    this-pay-date = next-pay-date.

    IF rent-charge-one-period THEN this-pay-date = stop-day + 1.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-build-tenant-charges) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-tenant-charges Method-Library 
PROCEDURE build-tenant-charges :
/*------------------------------------------------------------------------------
  Purpose:     Build a list of the current tenant rental charges.
               The list ( a temp table ) can be processed in a second pass
               for charging to several accounts or a single account and/or
               for reporting purposes.
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER tenant-code LIKE Tenant.TenantCode NO-UNDO.
DEF INPUT PARAMETER period-start-date AS DATE NO-UNDO.
DEF INPUT PARAMETER period-end-date AS DATE NO-UNDO.

DEF BUFFER Lease FOR TenancyLease.
DEF BUFFER L-Tenant FOR Tenant.
DEF BUFFER OtherLine FOR RentChargeLine.

  RUN clear-charge-details.

/* First pass - accumulate payable charegs into buckets */
DEF VAR start-day AS DATE NO-UNDO.
DEF VAR stop-day  AS DATE NO-UNDO.
DEF VAR rent-end-day  AS DATE NO-UNDO.

  FIND L-Tenant WHERE L-Tenant.TenantCode = tenant-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(L-Tenant) THEN DO:
    MESSAGE "Could not find tenant " + STRING(tenant-code).
    RETURN.
  END.
  IF L-Tenant.EntityType <> "P" THEN RETURN.
  FOR EACH Lease NO-LOCK WHERE Lease.PropertyCode = L-Tenant.EntityCode
                           AND Lease.TenantCode = tenant-code
                           AND (Lease.LeaseStatus <> "PAST" OR allow-past-leases):

    IF check-lease-inclusion THEN
      IF NOT include-lease(Lease.TenancyLeaseCode) THEN NEXT.

    IF og-method = "" OR charge-og-lease = "Charge" THEN
      annual-outgoings-charges = 0.
    ELSE
      annual-outgoings-charges = get-annual-outgoings(Lease.TenancyLeaseCode).

rent-charge-loop:
    FOR EACH RentCharge OF Lease NO-LOCK:

      FIND RentChargeType OF RentCharge NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(RentChargeType) THEN DO:
        MESSAGE "Tenant" Lease.TenantCode "has a rent charge of type ~"" RentCharge.RentChargeType "~" which is not set up properly - ignored".
        NEXT rent-charge-loop.
      END.

      /* Calculate the current charges payable for the period */
rent-charge-line-loop:
      FOR EACH RentChargeLine OF RentCharge NO-LOCK
        WHERE RentChargeLine.StartDate <= period-end-date:

        IF use-past-charges THEN DO:
          IF RentChargeLine.EndDate < period-start-date THEN NEXT rent-charge-line-loop.
          /* IF RentChargeLine.StartDate > period-end-date THEN NEXT rent-charge-line-loop. */
          IF RentChargeLine.RentChargeLineStatus <> "C" 
                      AND RentChargeLine.RentChargeLineStatus <> "P" THEN
            NEXT rent-charge-line-loop.
        END.
        ELSE IF RentChargeLine.RentChargeLineStatus <> "C" THEN
          NEXT rent-charge-line-loop.
        
        IF NOT(rent-charge-one-period) THEN DO:
          IF RentChargeLine.LastChargedDate <> ?
              AND RentChargeLine.LastChargedDate = RentChargeLine.EndDate THEN DO:
            IF NOT( CAN-FIND( FIRST OtherLine OF RentCharge WHERE OtherLine.RentChargeLineStatus = "C"
                                    AND OtherLine.StartDate <= period-end-date
                                    AND RECID(OtherLine) <> RECID(RentChargeLine) ))
            THEN NEXT rent-charge-line-loop.
          END.

          IF RentChargeLine.EndDate = ? AND RentChargeLine.LastChargedDate <> ?
                AND RentChargeLine.LastChargedDate >= period-end-date THEN DO:
            NEXT rent-charge-line-loop.
          END.

          IF RentChargeLine.EndDate <> ? THEN DO:
            IF RentChargeLine.EndDate < period-start-date
                  AND RentChargeLine.LastChargedDate < period-start-date THEN
              NEXT rent-charge-line-loop.
            IF RentChargeLine.EndDate >= period-end-date
                  AND RentChargeLine.LastChargedDate >= period-end-date THEN
              NEXT rent-charge-line-loop.
          END.
        END.

        start-day = period-start-date.
        stop-day  = period-end-date.
        IF RentChargeLine.StartDate       <> ? THEN start-day = MAXIMUM( start-day, RentChargeLine.StartDate ).

        IF RentChargeLine.LastChargedDate <> ? THEN start-day = MAXIMUM( start-day, (RentChargeLine.LastChargedDate + 1) ).
        ELSE IF RentChargeLine.StartDate  <> ? THEN start-day = MINIMUM( start-day, RentChargeLine.StartDate ).

        IF RentChargeLine.EndDate         <> ? THEN ASSIGN
          stop-day  = MINIMUM( stop-day,  RentChargeLine.EndDate ).

        IF RentChargeLine.EndDate < RentChargeLine.LastChargedDate THEN DO:
          /* This means that something that was charged should be part credited */
          stop-day = RentChargeLine.EndDate + 1.
          start-day = RentChargeLine.LastChargedDate.
/*           MESSAGE "Start:" start-day " to " stop-day RentChargeLine.Amount RentChargeLine.StartDate RentChargeLine.LastChargedDate RentChargeLine.EndDate. */
        END.
        ELSE DO:
          IF start-day = stop-day THEN NEXT rent-charge-line-loop.
        END.

        IF rent-charge-one-period THEN DO:
          IF RentChargeLine.EndDate <> ? AND RentChargeLine.EndDate < period-end-date
                AND CAN-FIND( LAST OtherLine OF RentCharge WHERE OtherLine.RentChargeLineStatus = "C"
                                  AND OtherLine.StartDate <= period-end-date
                                  AND OtherLine.EndDate >= period-end-date )
          THEN NEXT rent-charge-line-loop.
          start-day = period-start-date.
          stop-day = period-end-date.
        END.

        IF get-freq-months( RentChargeLine.FrequencyCode ) <> ? THEN
          RUN build-monthly-charges( tenant-code, L-Tenant.EntityType, L-Tenant.EntityCode,
                                    Lease.TenancyLeaseCode, Lease.AreaDescription, Lease.TaxApplies,
                                    start-day, stop-day ).
        ELSE IF get-freq-days( RentChargeLine.FrequencyCode ) <> ? THEN
          RUN build-daily-charges( tenant-code, L-Tenant.EntityType, L-Tenant.EntityCode,
                                    Lease.TenancyLeaseCode, Lease.AreaDescription, Lease.TaxApplies,
                                    start-day, stop-day ).
        ELSE
          MESSAGE "Bad frequency" RentChargeLine.FrequencyCode.

      END.  /* Each Rent Charge Line */
    END.    /* Each Rent Charge */
  END.      /* Each Lease */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-clear-charge-details) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-charge-details Method-Library 
PROCEDURE clear-charge-details :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH ChargeDetail: DELETE ChargeDetail. END.
  charge-detail-seq = 0.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-commit-charge-entry) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE commit-charge-entry Method-Library 
PROCEDURE commit-charge-entry :
/*------------------------------------------------------------------------------
  Purpose:     Commits a charge entry for charging:
                 RentChargeLineStatus = C - Chargeable
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER lease-code    LIKE RentChargeLine.TenancyLeaseCode NO-UNDO.
  DEF INPUT PARAMETER sequence-code LIKE RentChargeLine.SequenceCode     NO-UNDO.
  DEF INPUT PARAMETER start-date    LIKE RentChargeLine.StartDate        NO-UNDO.

  DEF BUFFER ThisChargeEntry FOR RentChargeLine.
  DEF BUFFER LastChargeEntry FOR RentChargeLine.

  IF NOT can-commit-charge( lease-code, sequence-code, start-date ) THEN RETURN "FAIL".
  
  FIND ThisChargeEntry
  WHERE ThisChargeEntry.TenancyLeaseCode = lease-code
    AND ThisChargeEntry.SequenceCode     = sequence-code
    AND ThisChargeEntry.StartDate        = start-date
    EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE ThisChargeEntry THEN RETURN "FAIL".

  /* Set the end date of the last existing chargeable line */
  
  FIND FIRST LastChargeEntry
  WHERE LastChargeEntry.TenancyLeaseCode = lease-code
    AND LastChargeEntry.SequenceCode     = sequence-code
    AND LastChargeEntry.RentChargeLineStatus = "C"
    AND LastChargeEntry.EndDate = ?
    AND ROWID( LastChargeEntry ) <> ROWID( ThisChargeEntry )
    EXCLUSIVE-LOCK NO-ERROR.

  IF AVAILABLE LastChargeEntry THEN DO:
  
    IF ThisChargeEntry.StartDate > LastChargeEntry.StartDate THEN DO:
      ASSIGN LastChargeEntry.EndDate =
        MAXIMUM( LastChargeEntry.StartDate, ThisChargeEntry.StartDate - 1 ).
    END.
    ELSE DO:
      IF ThisChargeEntry.EndDate = ? THEN DO:
        IF LastChargeEntry.LastChargedDate = ? THEN
          ASSIGN LastChargeEntry.RentChargeLineStatus = "A". /* Aborted */
        ELSE
          ASSIGN
            LastChargeEntry.EndDate = LastChargeEntry.LastChargedDate.
      END.
      ELSE DO:
        /* Nothing */
      END.
    END.
  END.

  ASSIGN
    ThisChargeEntry.DateCommitted = TODAY
    ThisChargeEntry.RentChargeLineStatus = "C".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-create-charge-entry) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-charge-entry Method-Library 
PROCEDURE create-charge-entry :
/*------------------------------------------------------------------------------
  Purpose:     Schedule a charging entry for the given charge
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER lease-code    LIKE RentChargeLine.TenancyLeaseCode NO-UNDO.
  DEF INPUT PARAMETER sequence-code LIKE RentChargeLine.SequenceCode NO-UNDO.

  DEF INPUT PARAMETER start-date    LIKE RentChargeLine.StartDate NO-UNDO.
  DEF INPUT PARAMETER end-date      LIKE RentChargeLine.EndDate NO-UNDO.
  DEF INPUT PARAMETER freq-code     LIKE RentChargeLine.FrequencyCode NO-UNDO.
  DEF INPUT PARAMETER charge-amount LIKE RentChargeLine.Amount NO-UNDO.

  FIND TenancyLease WHERE TenancyLease.TenancyLeaseCode = lease-code
    NO-LOCK NO-ERROR.
  IF NOT AVAILABLE TenancyLease THEN RETURN.
  
  FIND RentCharge
  WHERE RentCharge.TenancyLeaseCode = lease-code
    AND RentCharge.SequenceCode     = sequence-code
    NO-LOCK NO-ERROR.
    
  IF NOT AVAILABLE RentCharge THEN RETURN "FAIL".
  
  CREATE RentChargeLine.
  ASSIGN
    RentChargeLine.TenancyLeaseCode     = lease-code
    RentChargeLine.SequenceCode         = sequence-code
    RentChargeLine.RentChargeLineStatus = "I"
    RentChargeLine.StartDate            = start-date
    RentChargeLine.EndDate              = end-date
    RentChargeLine.FrequencyCode  = 
      IF freq-code <> ?         THEN freq-code ELSE
      IF AVAILABLE TenancyLease THEN TenancyLease.PaymentFrequency 
         ELSE "MNTH"
    RentChargeLine.Amount           = charge-amount.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-make-blurb) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE make-blurb Method-Library 
PROCEDURE make-blurb :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER descriptive-word AS CHAR NO-UNDO.
DEF INPUT PARAMETER period-d1 AS DATE NO-UNDO.
DEF INPUT PARAMETER period-dn AS DATE NO-UNDO.
DEF OUTPUT PARAMETER inv-blurb AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER inv-amount AS DEC NO-UNDO.
DEF OUTPUT PARAMETER inv-tax AS DEC NO-UNDO.
DEF OUTPUT PARAMETER inv-total AS DEC NO-UNDO.

  inv-blurb = "Rental " + descriptive-word
                + " for the period from " + STRING( period-d1, "99/99/9999")
                + " to " + STRING( period-dn, "99/99/9999") + "~n".

  inv-total = 0.
  inv-tax   = 0.
  FOR EACH ChargeDetail:
    inv-total = inv-total + ChargeDetail.ChargeAmount.
    inv-tax   = inv-tax   + ChargeDetail.TaxableAmount.
    inv-blurb = inv-blurb + "~n"
                  + SUBSTR( STRING( ChargeDetail.ChargedFrom, "99/99/9999"), 1, 5)
                  + " - "
                  + SUBSTR( STRING( ChargeDetail.ChargedUpTo, "99/99/9999"), 1, 5) + " "
                  + STRING( ChargeDetail.AreaDescription, "X(33)") + " "
                  + STRING( (IF ChargeDetail.PeriodFraction < 0 THEN "Reverse " ELSE "")
                            + ChargeDetail.Description
                            + " " + "@ "
                            + TRIM(STRING( ChargeDetail.AnnualAmount, ">>>,>>>,>>9.99CR" ))
                            + " p.a.", "X(35)")
                  + STRING( ChargeDetail.ChargeAmount, "(>,>>>,>>9.99)" ).
  END.

  inv-tax    = inv-tax * (Office.GST / 100).
  inv-amount = inv-total + inv-tax.
  inv-blurb = inv-blurb + "~n~n~n".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-can-commit-charge) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION can-commit-charge Method-Library 
FUNCTION can-commit-charge RETURNS LOGICAL
  ( INPUT lease-code AS INT, INPUT sequence-code AS INT, INPUT start-date AS DATE ) :
/*------------------------------------------------------------------------------
  Purpose:  Detremines whether or not a charge entry can be committed for
            charging
    Notes:  
------------------------------------------------------------------------------*/

  IF start-date = ? THEN RETURN No.
  
  DEF BUFFER ChargeEntry FOR RentChargeLine.
  
  FIND ChargeEntry WHERE ChargeEntry.TenancyLeaseCode = lease-code
                     AND ChargeEntry.SequenceCode     = sequence-code
                     AND ChargeEntry.StartDate        = start-date
                     NO-LOCK NO-ERROR.
  IF NOT AVAILABLE ChargeEntry THEN RETURN No.

  RETURN( ChargeEntry.Amount <> 0 AND ChargeEntry.FrequencyCode <> "" ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-annual-outgoings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-annual-outgoings Method-Library 
FUNCTION get-annual-outgoings RETURNS DECIMAL
  ( INPUT lease-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Calculate the estimated annual outgoings charges
    Notes:  
------------------------------------------------------------------------------*/
DEF BUFFER Lse FOR TenancyLease.
DEF BUFFER Prp FOR Property.
DEF BUFFER Pog FOR PropertyOutgoing.
DEF BUFFER Tnt FOR Tenant.
DEF BUFFER Tog FOR TenancyOutgoing.

  IF og-method = "" THEN RETURN 0.0.
  FIND Lse WHERE Lse.TenancyLeaseCode = lease-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Lse) THEN DO:
    MESSAGE "Lease " + STRING(lease-code) + " not on file." .
    RETURN ?.
  END.

  IF charge-og-lease BEGINS "Y" THEN RETURN Lse.OutgoingsBudget.
  ELSE IF Lse.RecoveryType = "F" THEN RETURN Lse.OutgoingsBudget.
  ELSE IF Lse.RecoveryType = "A" THEN RETURN 0.0 .
  ELSE IF Lse.RecoveryType <> "B" THEN DO:
    MESSAGE "Invalid recovery type '" Lse.RecoveryType "'for lease " + STRING(lease-code).
    RETURN 0.0 .
  END.

DEF VAR fixed AS DEC NO-UNDO.
DEF VAR percent AS DEC NO-UNDO.
DEF VAR amount AS DEC NO-UNDO   INITIAL 0.0 .
  FOR EACH Pog WHERE Pog.PropertyCode = Lse.PropertyCode NO-LOCK:
    FIND Tog WHERE Tog.TenancyLeaseCode = lease-code
                AND Tog.AccountCode = Pog.AccountCode NO-LOCK NO-ERROR.
    percent = 0.
    fixed = 0.
    IF AVAILABLE(Tog) THEN DO:
      IF Tog.FixedAmount > 0 THEN
        fixed = ROUND(Tog.FixedAmount / 12, 2).
      ELSE
        percent = Tog.Percentage.
    END.
    ELSE
      percent = Lse.OutgoingsRate.

    IF percent = ? THEN percent = 0.
    IF Pog.BudgetAmount = ? THEN DO:
      MESSAGE Pog.BudgetAmount "is not a valid budget!".
      NEXT.
    END.
    amount = amount + fixed + ROUND((percent * Pog.BudgetAmount) / 1200, 2).
  END.

  FOR EACH Tog WHERE Tog.TenancyLeaseCode = lease-code
                 AND Tog.FixedAmount > 0
                 AND NOT CAN-FIND( Pog WHERE Pog.PropertyCode = Lse.PropertyCode
                                         AND Tog.AccountCode = Pog.AccountCode )
                 NO-LOCK:
    fixed = ROUND(Tog.FixedAmount / 12, 2).
    amount = amount + fixed.
  END.

  RETURN amount * 12.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-not-can-action-rent-review) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION not-can-action-rent-review Method-Library 
FUNCTION not-can-action-rent-review RETURNS LOGICAL
  ( INPUT lease-code AS INT, INPUT sequence-code AS INT, INPUT start-date AS DATE ) :
/*------------------------------------------------------------------------------
  Purpose:  Detremines whether or not a rent review can be committed for
            charging
    Notes:  
------------------------------------------------------------------------------*/

  IF start-date = ? THEN RETURN No.
  
  DEF BUFFER Review FOR RentReview.
  
  FIND Review
  WHERE Review.TenancyLeaseCode = lease-code
/*    AND Review.SequenceCode     = sequence-code */
/*    AND Review.StartDate        = start-date */
    NO-LOCK NO-ERROR.
  IF NOT AVAILABLE Review THEN RETURN No.
  
  RETURN( 
/*    Review.Amount <> 0 AND */
/*    Review.DateCommitted = ? AND */
    Review.DateComplete <> ? AND
    Review.EstimateBasis <> "" AND
/*    Review.FrequencyCode <> "" AND */
/*     Review.LastChargedDate = ? AND */
    Review.ReviewStatus <> "C" AND
    Review.ReviewStatus <> "" AND
    Review.ReviewType <> ""

/*  Review.DateDue <> ?
    Review.Latest
    Review.Earliest */
    
  ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

