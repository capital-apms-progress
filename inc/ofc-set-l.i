/* ----------------------------------------------------------------------------

  Purpose:    Search for an office setting with a Logical value

  Parameters: {1} - The name of the office setting
              {2} - The name of the logical variable to define and store the
                    value in
              {3} - (Optional) If the setting is not found then display in a
                    message alert-box of type {3}

  Notes:      Office must be available

-------------------------------------------------------------------------------*/

DEF VAR {2} AS LOGI NO-UNDO.

FIND OfficeSetting OF Office WHERE OfficeSetting.SetName = "{1}"
  NO-LOCK NO-ERROR.

{2} = IF AVAILABLE OfficeSetting THEN (TRIM(OfficeSetting.SetValue) BEGINS "Y")
                                 ELSE ?.

IF "{3}" <> "" AND NOT AVAILABLE OfficeSetting THEN
  MESSAGE {2} VIEW-AS ALERT-BOX {3}.
