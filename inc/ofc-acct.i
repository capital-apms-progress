FIND OfficeControlAccount NO-LOCK OF Office 
            WHERE OfficeControlAccount.Name = "{1}" NO-ERROR.
IF NOT AVAILABLE(OfficeControlAccount) THEN DO:
  MESSAGE   "The {1} control account is not set up for" SKIP
            "the " + Office.Name + " office." SKIP(1)
            "Please ask your system administrator to set it up"
            VIEW-AS ALERT-BOX ERROR.
  RETURN ERROR.
END.
DEF VAR {2} LIKE OfficeControlAccount.AccountCode NO-UNDO.
{2} = OfficeControlAccount.AccountCode.
