&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : 
    Purpose     : Use this when you want to log things

    Syntax      :

    Description :

    Author(s)   : Andrew McMillan
    Created     : 23 June 1998
    Notes       :
  ------------------------------------------------------------------------*/

DEFINE STREAM log-stream.
DEF VAR log-depth AS INT NO-UNDO        INITIAL 10.
DEF VAR log-filename AS CHAR NO-UNDO    INITIAL "default.log".

log-filename = SESSION:TEMP-DIRECTORY + log-filename.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-log) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD log Method-Library 
FUNCTION log RETURNS CHARACTER
  ( INPUT depth AS INT, INPUT entry-text AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = 14.45
         WIDTH              = 39.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-log) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION log Method-Library 
FUNCTION log RETURNS CHARACTER
  ( INPUT depth AS INT, INPUT entry-text AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  write output formatted as a log entry
------------------------------------------------------------------------------*/

  IF depth > log-depth THEN RETURN "".

DEF VAR log-text AS CHAR NO-UNDO.
  log-text = STRING( YEAR(TODAY), "9999") + "-" + STRING( MONTH(TODAY), "99") + "-" + STRING( DAY(TODAY), "99")
           + " " + STRING( TIME, "HH:MM:SS")
           + " " + STRING( depth, ">>9")
           + " " + (IF entry-text = ? THEN "?" ELSE entry-text).

  OUTPUT STREAM log-stream TO VALUE(log-filename) KEEP-MESSAGES APPEND.
  PUT STREAM log-stream UNFORMATTED log-text SKIP.
  OUTPUT STREAM log-stream CLOSE.

  RETURN log-text.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

