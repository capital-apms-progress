&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

/* If called from a drill button, these variables must be set
   for correct operation of this drill window */

/* Other variables */

DEF VAR btn     AS HANDLE EXTENT 50 NO-UNDO.
DEF VAR splash  AS HANDLE NO-UNDO.

DEF VAR user-name AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .08
         WIDTH              = 37.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-mstwin.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-default-layout) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE default-layout Method-Library 
PROCEDURE default-layout :
/*------------------------------------------------------------------------------
  Purpose:     Default widget layout for this window
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN get-default-groups.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-default-groups) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-default-groups Method-Library 
PROCEDURE get-default-groups :
/*------------------------------------------------------------------------------
  Purpose:     Defines the default widget groups for this window
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR btn-list AS CHAR NO-UNDO.

  RUN get-btn-list IN sys-mgr( THIS-PROCEDURE, OUTPUT btn-list ).
  RUN pack( btn-list, "btn-group", "L", No ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pre-destroy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy Method-Library 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
  IF NOT VALID-HANDLE( {&WINDOW-NAME} ) THEN RETURN.
  IF NOT VALID-HANDLE( {&WINDOW-NAME}:HANDLE ) THEN RETURN.
  IF {&WINDOW-NAME}:X < 1 OR {&WINDOW-NAME}:Y < 1 OR
     {&WINDOW-NAME}:X + {&WINDOW-NAME}:WIDTH-PIXELS  > (SESSION:WIDTH-PIXELS - 20) OR
     {&WINDOW-NAME}:Y + {&WINDOW-NAME}:HEIGHT-PIXELS > (SESSION:HEIGHT-PIXELS - 15) THEN
    RETURN.  /* window position is invalid (probably minimised)- don't save it */

  FIND RP EXCLUSIVE-LOCK WHERE RP.ReportID = "Menu-" + {&WINDOW-NAME}:TITLE 
                           AND RP.UserName = user-name NO-ERROR.
  IF NOT AVAILABLE(RP) THEN CREATE RP.
  ASSIGN    RP.ReportID = "Menu-" + {&WINDOW-NAME}:TITLE
            RP.UserName = user-name
            RP.Int1 = {&WINDOW-NAME}:X
            RP.Int2 = {&WINDOW-NAME}:Y.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-user-position-window) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE user-position-window Method-Library 
PROCEDURE user-position-window :
/*------------------------------------------------------------------------------
  Purpose:     If the window position is outside the screen then center it
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 IF NOT VALID-HANDLE( {&WINDOW-NAME}:HANDLE ) THEN RETURN.

  FIND RP WHERE RP.ReportID = "Menu-" + {&WINDOW-NAME}:TITLE 
            AND RP.UserName = user-name NO-LOCK NO-ERROR.
  IF AVAILABLE(RP) THEN ASSIGN
      {&WINDOW-NAME}:X = RP.Int1
      {&WINDOW-NAME}:Y = RP.Int2 .

 IF {&WINDOW-NAME}:X < 1 OR {&WINDOW-NAME}:Y < 1 OR
    {&WINDOW-NAME}:X + {&WINDOW-NAME}:WIDTH-PIXELS  > (SESSION:WIDTH-PIXELS - 20) OR
    {&WINDOW-NAME}:Y + {&WINDOW-NAME}:HEIGHT-PIXELS > (SESSION:HEIGHT-PIXELS - 15) THEN
 DO:
 
    {&WINDOW-NAME}:X = ( SESSION:WIDTH-PIXELS - {&WINDOW-NAME}:WIDTH-PIXELS - 20 ) / 2.
    {&WINDOW-NAME}:Y = ( SESSION:HEIGHT-PIXELS - {&WINDOW-NAME}:HEIGHT-PIXELS - 15) / 2.
 
 END.
 
 IF {&WINDOW-NAME}:X < 1 THEN {&WINDOW-NAME}:X = 1.  
 IF {&WINDOW-NAME}:Y < 1 THEN {&WINDOW-NAME}:Y = 1.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

