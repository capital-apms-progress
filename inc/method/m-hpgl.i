&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : m-hpgl.i
    Purpose     : Methods for using HPGL/2 with PCL
    Author(s)   : Andrew McMillan
    Notes       : Generally included along with m-txtrep.i
  ------------------------------------------------------------------------*/

DEF VAR hpgl-codes AS CHAR INITIAL "" NO-UNDO.
DEF VAR hpgl-pen-is-down AS LOGICAL INITIAL ? NO-UNDO.
DEF VAR hpgl-last-font AS CHAR NO-UNDO INITIAL ?.
DEF VAR hpgl-initialise-codes AS CHAR NO-UNDO INITIAL "".
DEF VAR hpgl-x-scaling-factor AS DEC NO-UNDO INITIAL 1.0 .
DEF VAR hpgl-y-scaling-factor AS DEC NO-UNDO INITIAL 1.0.
DEF VAR hpgl-output-mode AS CHAR INITIAL "hpgl" NO-UNDO.

/* If in PDF mode, should a logo image be used */
DEF VAR do-an-image-logo AS LOGICAL INITIAL YES NO-UNDO.

&SCOP HPGL-X-OFFSET 6
&SCOP HPGL-Y-OFFSET 12

{inc/ofc-this.i}
{inc/ofc-set.i "Corporate-Logo-Routine" "corporate-logo-routine"}
IF NOT AVAILABLE(OfficeSetting) THEN DO:
  IF LOOKUP( Office.OfficeCode, "SDNY" ) <> 0 THEN
    corporate-logo-routine = "process/logo/agp.p".
  ELSE
    corporate-logo-routine = "process/logo/ttpl.p".
END.

/* Dont use an image for the logo if there is no appropriate office setting */
{inc/ofc-set.i "PDF-logo-image" "pdf-logo-image"}
IF NOT AVAILABLE(OfficeSetting) THEN
    do-an-image-logo = NO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-hpgl-esc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD hpgl-esc Method-Library 
FUNCTION hpgl-esc RETURNS CHARACTER
  ( INPUT s1 AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-hpgl) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD hpgl-hpgl Method-Library 
FUNCTION hpgl-hpgl RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-pcl) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD hpgl-pcl Method-Library 
FUNCTION hpgl-pcl RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .05
         WIDTH              = 30.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/string.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-agp-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE agp-address Method-Library 
PROCEDURE agp-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

/* save page position in PCL :-)  - sneaky! */
RUN hpgl-append( CHR(27) + "%1A" + CHR(27) + "%1B" ).

RUN hpgl-move-relative( -53, -2.5 ).
RUN hpgl-text( 'helvetica,point,5,proportional', "ACN 003 354 443" ).

RUN hpgl-move-relative( -37.5, -2 ).
RUN hpgl-text( hpgl-last-font,
  "LEVEL 11, ATANASKOVIC HARTNELL HOUSE, 75-85 ELIZABETH STREET, SYDNEY NSW 2000 AUSTRALIA" ).

RUN hpgl-move-relative( 9.5, -2 ).
RUN hpgl-text( hpgl-last-font,
  "GPO BOX 4079, SYDNEY 2001, TELEPHONE 61-2-9223 5601, FACSIMILE 61-2-9223 5561" ).

/* restore page position from PCL :-)  - sneaky! */
RUN hpgl-append( CHR(27) + "%0A" + CHR(27) + "%1B" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-agp-logo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE agp-logo Method-Library 
PROCEDURE agp-logo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

/* save current page position in PCL :-)  - sneaky! */
RUN hpgl-append( CHR(27) + "%1A" + CHR(27) + "%1B" ).
RUN hpgl-moveto( 105, 272 ).
RUN sea-logo.
RUN hpgl-move-relative( -38, -6).
RUN hpgl-text( 'helvetica,point,12,proportional,bold',
  "AUSTRALIAN GROWTH PROPERTIES LIMITED").

/* restore page position from PCL :-)  - sneaky! */
RUN hpgl-append( CHR(27) + "%0A" + CHR(27) + "%1B" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-client-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE client-address Method-Library 
PROCEDURE client-address :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER entity-type AS CHARACTER NO-UNDO.
DEF INPUT PARAMETER entity-code AS INTEGER NO-UNDO.
DEF INPUT PARAMETER client-code LIKE Client.ClientCode NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
  IF NOT do-an-image-logo THEN
    RUN client-routine( entity-type, entity-code, client-code, "Address":U ).
END.
ELSE
  RUN client-routine( entity-type, entity-code, client-code, "Address":U ).

  RUN hpgl-moveto( 0, 250 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-client-logo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE client-logo Method-Library 
PROCEDURE client-logo :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER entity-type AS CHARACTER NO-UNDO.
DEF INPUT PARAMETER entity-code AS INTEGER NO-UNDO.
DEF INPUT PARAMETER client-code LIKE Client.ClientCode NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
  IF do-an-image-logo THEN DO:
    /* The logo and address usually pushes the text down the page, so simulate that */
    RUN hpgl-append( CHR(10) ).
    RUN hpgl-append( CHR(10) ).
    RUN hpgl-append( CHR(10) ).
    RUN hpgl-append( CHR(10) ).
    RUN hpgl-append( CHR(10) ).
    RUN hpgl-append( CHR(10) ).
    RUN hpgl-append( CHR(10) ).
    RUN hpgl-append( CHR(10) ).
    RUN hpgl-append( CHR(10) ).
    RUN hpgl-append( '[-pdf-]>IMAGE:' ).
    RUN hpgl-append( ENTRY( 1, pdf-logo-image, "|") ).
    RUN hpgl-append( ',' ).
    RUN hpgl-append( ENTRY( 2, pdf-logo-image, "|") ).
    RUN hpgl-append( ',' ).
    RUN hpgl-append( ENTRY( 3, pdf-logo-image, "|") ).
    RUN hpgl-append( '[-pdf-]' ).
  END.
  ELSE
    RUN client-routine( entity-type, entity-code, client-code, "Logo":U ) NO-ERROR.
END.
ELSE
  RUN client-routine( entity-type, entity-code, client-code, "Logo":U ) NO-ERROR.

  RUN hpgl-moveto( 0, 250 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-client-routine) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE client-routine Method-Library 
PROCEDURE client-routine :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER entity-type AS CHARACTER NO-UNDO.
DEF INPUT PARAMETER entity-code AS INTEGER NO-UNDO.
DEF INPUT PARAMETER client-code LIKE Client.ClientCode NO-UNDO.
DEF INPUT PARAMETER routine-name AS CHAR NO-UNDO.

DEF BUFFER AltClient FOR Client.
FIND AltClient WHERE AltClient.ClientCode = client-code NO-LOCK NO-ERROR.

IF AVAILABLE(AltClient) AND AltClient.LogoRoutine <> "" AND AltClient.LogoRoutine <> ? THEN
  RUN VALUE(AltClient.LogoRoutine) ( entity-type, entity-code, routine-name, hpgl-output-mode ) NO-ERROR.
ELSE
  RUN VALUE(corporate-logo-routine) ( entity-type, entity-code, routine-name, hpgl-output-mode ) NO-ERROR.

RUN hpgl-append( RETURN-VALUE ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-append) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-append Method-Library 
PROCEDURE hpgl-append :
/*------------------------------------------------------------------------------
  Purpose:  Append a command to the hpgl code string
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER codes AS CHAR NO-UNDO.

  hpgl-codes = hpgl-codes + codes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-append-from-file) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-append-from-file Method-Library 
PROCEDURE hpgl-append-from-file :
/*------------------------------------------------------------------------------
  Purpose:  Append HPGL code input from a file.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER code-filename AS CHAR NO-UNDO.

DEF VAR in-line AS CHAR NO-UNDO.

  INPUT FROM VALUE(code-filename).
  REPEAT:
    IMPORT UNFORMATTED in-line .
    hpgl-codes = hpgl-codes + in-line.
  END.
  INPUT CLOSE.

  /* don't know what effect it has had */
  hpgl-pen-is-down = ?.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-arc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-arc Method-Library 
PROCEDURE hpgl-arc :
/*------------------------------------------------------------------------------
  Purpose:  Draw an absolute arc
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER x AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER y AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER degrees AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER chord-angle AS DECIMAL NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
    RUN hpgl-pen-down.
    RUN hpgl-append( "[-pdf-]>AA:" ).
    RUN hpgl-mm-plu( x - {&HPGL-X-OFFSET} ).
    RUN hpgl-append( "," ).
    RUN hpgl-mm-plu( y - {&HPGL-Y-OFFSET} ).
    RUN hpgl-append( "," ).
    RUN hpgl-decimal-value( degrees ).
    IF chord-angle <> ? THEN DO:
      RUN hpgl-append( "," ).
      RUN hpgl-decimal-value( chord-angle ).
    END.
    RUN hpgl-append( "[-pdf-]" ).

END.
ELSE DO:
  RUN hpgl-pen-down.
  RUN hpgl-append( "AA" ).         /* Arc Absolute */
  RUN hpgl-mm-plu( x - {&HPGL-X-OFFSET} ).
  RUN hpgl-append( "," ).
  RUN hpgl-mm-plu( y - {&HPGL-Y-OFFSET} ).
  RUN hpgl-append( "," ).
  RUN hpgl-decimal-value( degrees ).
  IF chord-angle <> ? THEN DO:
    RUN hpgl-append( "," ).
    RUN hpgl-decimal-value( chord-angle ).
    RUN hpgl-append( ";" ).
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-arc-relative) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-arc-relative Method-Library 
PROCEDURE hpgl-arc-relative :
/*------------------------------------------------------------------------------
  Purpose:  Draw a relative arc
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER x AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER y AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER degrees AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER chord-angle AS DECIMAL NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
    RUN hpgl-pen-down.
    RUN hpgl-append( "[-pdf-]>AR:" ).         /* Arc Relative */
    RUN hpgl-mm-plu( x * hpgl-x-scaling-factor ).
    RUN hpgl-append( "," ).
    RUN hpgl-mm-plu( y * hpgl-y-scaling-factor ).
    RUN hpgl-append( "," ).
    RUN hpgl-decimal-value( degrees ).
    IF chord-angle <> ? THEN DO:
        RUN hpgl-append( "," ).
        RUN hpgl-decimal-value( chord-angle ).
    END.
    RUN hpgl-append( "[-pdf-]" ).
END.
ELSE DO:
    RUN hpgl-pen-down.
    RUN hpgl-append( "AR" ).         /* Arc Relative */
    RUN hpgl-mm-plu( x * hpgl-x-scaling-factor ).
    RUN hpgl-append( "," ).
    RUN hpgl-mm-plu( y * hpgl-y-scaling-factor ).
    RUN hpgl-append( "," ).
    RUN hpgl-decimal-value( degrees ).
    IF chord-angle <> ? THEN DO:
        RUN hpgl-append( "," ).
        RUN hpgl-decimal-value( chord-angle ).
        RUN hpgl-append( ";" ).
    END.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-box) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-box Method-Library 
PROCEDURE hpgl-box :
/*------------------------------------------------------------------------------
  Purpose:  draw a rectangle at the coordinates specified
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER x1 AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER y1 AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER x2 AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER y2 AS DECIMAL NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
    RUN hpgl-moveto( x1, y1).
    RUN hpgl-box-relative( x2 - x1, y2 - y1 ).
END.
ELSE DO:
    RUN hpgl-moveto( x1, y1).
    RUN hpgl-box-relative( x2 - x1, y2 - y1 ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-box-relative) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-box-relative Method-Library 
PROCEDURE hpgl-box-relative :
/*------------------------------------------------------------------------------
  Purpose:  draw a relative rectangle to the coordinates specified
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER x AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER y AS DECIMAL NO-UNDO.

  RUN hpgl-line-relative( 0, y).
  RUN hpgl-line-relative( x, 0).
  RUN hpgl-line-relative( 0, - y).
  RUN hpgl-line-relative( - x, 0).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-circle) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-circle Method-Library 
PROCEDURE hpgl-circle :
/*------------------------------------------------------------------------------
  Purpose:  Draw a circle around the current pen position
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER radius AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER chord-angle AS DECIMAL NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
    RUN hpgl-pen-down.
    RUN hpgl-append( "[-pdf-]>CI:" ).
    RUN hpgl-mm-plu( radius ).
    IF chord-angle <> ? THEN DO:
        RUN hpgl-append( "," ).
        RUN hpgl-decimal-value( chord-angle ).
    END.
    RUN hpgl-append( "[-pdf-]" ).
END.
ELSE DO:
    RUN hpgl-pen-down.
    RUN hpgl-append( "CI" ).
    RUN hpgl-mm-plu( radius ).
    IF chord-angle <> ? THEN DO:
        RUN hpgl-append( "," ).
        RUN hpgl-decimal-value( chord-angle ).
        RUN hpgl-append( ";" ).
    END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-clear) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-clear Method-Library 
PROCEDURE hpgl-clear :
/*------------------------------------------------------------------------------
  Purpose:  Clear the hpgl ready for new codes
------------------------------------------------------------------------------*/
  hpgl-codes = "".
  hpgl-pen-is-down = ?.
  hpgl-last-font = ?.
  hpgl-initialise-codes = "".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-copywatermark) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-copywatermark Method-Library 
PROCEDURE hpgl-copywatermark :
/*------------------------------------------------------------------------------
  Purpose: Create a "COPY" watermark on the page.  
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

IF hpgl-output-mode = "pdf" THEN DO:
    RUN hpgl-append( "[-pdf-]>WATERMARK:COPY[-pdf-]" ).
END.
ELSE DO:
    RUN hpgl-moveto( 70, 130 ).
    RUN hpgl-append( "SD1,21,2,1,4,100,5,0,6,3,7,4148SS" ).
    RUN hpgl-append( "FT10," ).
    RUN hpgl-decimal-value( DECIMAL( 20.00 ) ).
    RUN hpgl-append( "CF2DT*DI2,1LBCOPY" + CHR(13) + "*" ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-decimal-value) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-decimal-value Method-Library 
PROCEDURE hpgl-decimal-value :
/*------------------------------------------------------------------------------
  Purpose:  Convert decimal value to a string
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER decimal-value AS DECIMAL NO-UNDO.

DEF VAR decimal-string AS CHAR NO-UNDO.

  decimal-string = TRIM( STRING( decimal-value, "->>>>>9.9999"), " 0").
  decimal-string = RIGHT-TRIM( decimal-string, ".").
  IF decimal-string = "" OR decimal-string = ? THEN decimal-string = "0".
  RUN hpgl-append( decimal-string ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-fill-polygon) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-fill-polygon Method-Library 
PROCEDURE hpgl-fill-polygon :
/*------------------------------------------------------------------------------
  Purpose:  Fill the polygon
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER density AS INTEGER NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
    IF ( density <> ? ) THEN DO:
      RUN hpgl-append( "[-pdf-]>FT:10," ).
      RUN hpgl-decimal-value( DECIMAL( density ) ).
    END.
    RUN hpgl-append( "[-pdf-]" ).
END.
ELSE DO:
    IF ( density <> ? ) THEN DO:
        RUN hpgl-append( "FT10," ).
        RUN hpgl-decimal-value( DECIMAL( density ) ).
    END.
    RUN hpgl-append( "FP" ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-finish-polygon) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-finish-polygon Method-Library 
PROCEDURE hpgl-finish-polygon :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF hpgl-output-mode = "pdf" THEN
      RUN hpgl-append( "[-pdf-]>ENDPOLY:[-pdf-]" ).
  ELSE
      RUN hpgl-append( "PM2" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-get-codes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-get-codes Method-Library 
PROCEDURE hpgl-get-codes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER keep-pcl-cursor  AS LOGICAL NO-UNDO.
DEF INPUT PARAMETER keep-hpgl-cursor AS LOGICAL NO-UNDO.
DEF OUTPUT PARAMETER out-hpgl-codes AS CHAR NO-UNDO.

  IF hpgl-output-mode = "pdf" THEN DO:
    out-hpgl-codes = "[-pdf-]>INITHPGL:"
                     + (IF keep-pcl-cursor THEN "keepcursor" ELSE "")
                     + hpgl-codes + "[-pdf-]>ENDHPGL:"
                     + (IF keep-hpgl-cursor THEN "keepcursor" ELSE "")
                     + "[-pdf-]".
  END.
  ELSE DO:
    out-hpgl-codes = hpgl-initialise-codes
                   + CHR(27) + (IF keep-pcl-cursor THEN  "%1B" ELSE "%0B")
                   + hpgl-codes
                   + CHR(27) + (IF keep-hpgl-cursor THEN "%1A" ELSE "%0A").
  END.

  hpgl-codes = "".
  hpgl-initialise-codes = "".
  hpgl-pen-is-down = ?.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-initialize) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-initialize Method-Library 
PROCEDURE hpgl-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Set the HPGL code to initialize
------------------------------------------------------------------------------*/
  hpgl-initialise-codes = "" /* CHR(27) + "*p00Y" + CHR(27) + "*c0t5590x8060Y" */.
  IF hpgl-output-mode <> "pdf" THEN RUN hpgl-append( "INSP1" ).  /* initialize, select pen 1 */
  RUN hpgl-pen-up.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-integer-value) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-integer-value Method-Library 
PROCEDURE hpgl-integer-value :
/*------------------------------------------------------------------------------
  Purpose:  Convert decimal value to an integer and string it
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER decimal-value AS DECIMAL NO-UNDO.

  RUN hpgl-decimal-value( ROUND(decimal-value, 0) ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-line-relative) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-line-relative Method-Library 
PROCEDURE hpgl-line-relative :
/*------------------------------------------------------------------------------
  Purpose:  Draw a line to a location, relative to the current position
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER x AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER y AS DECIMAL NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
    RUN hpgl-pen-down.
    RUN hpgl-append( "[-pdf-]>PR:" ).         /* Plot relative */
    RUN hpgl-mm-plu( x * hpgl-x-scaling-factor ).
    RUN hpgl-append( "," ).
    RUN hpgl-mm-plu( y * hpgl-y-scaling-factor ).
    RUN hpgl-append( "[-pdf-]" ).
END.
ELSE DO:
  RUN hpgl-pen-down.
  RUN hpgl-append( "PR" ).         /* Plot relative */
  RUN hpgl-mm-plu( x * hpgl-x-scaling-factor ).
  RUN hpgl-append( "," ).
  RUN hpgl-mm-plu( y * hpgl-y-scaling-factor ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-lineto) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-lineto Method-Library 
PROCEDURE hpgl-lineto :
/*------------------------------------------------------------------------------
  Purpose:  Draw a line to a location
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER x AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER y AS DECIMAL NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
  RUN hpgl-pen-down.
  RUN hpgl-append( "[-pdf-]>PA:" ).         /* Plot absolute */
  RUN hpgl-mm-plu( x - {&HPGL-X-OFFSET} ).
  RUN hpgl-append( "," ).
  RUN hpgl-mm-plu( y - {&HPGL-Y-OFFSET} ).
  RUN hpgl-append( "[-pdf-]" ).
END.
ELSE DO:
  RUN hpgl-pen-down.
  RUN hpgl-append( "PA" ).         /* Plot absolute */
  RUN hpgl-mm-plu( x - {&HPGL-X-OFFSET} ).
  RUN hpgl-append( "," ).
  RUN hpgl-mm-plu( y - {&HPGL-Y-OFFSET} ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-mm-plu) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-mm-plu Method-Library 
PROCEDURE hpgl-mm-plu :
/*------------------------------------------------------------------------------
  Purpose:  Convert millimetres to 'plu's'
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER mm AS DECIMAL NO-UNDO.

  RUN hpgl-integer-value( mm * 40.0 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-move-relative) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-move-relative Method-Library 
PROCEDURE hpgl-move-relative :
/*------------------------------------------------------------------------------
  Purpose:  Set codes to move to a location
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER x AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER y AS DECIMAL NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
  RUN hpgl-pen-up.
  RUN hpgl-append( "[-pdf-]>PR:" ).         /* Plot relative */
  RUN hpgl-mm-plu( x * hpgl-x-scaling-factor ).
  RUN hpgl-append( "," ).
  RUN hpgl-mm-plu( y * hpgl-y-scaling-factor ).
  RUN hpgl-append( "[-pdf-]" ).
END.
ELSE DO:
  RUN hpgl-pen-up.
  RUN hpgl-append( "PR" ).         /* Plot relative */
  RUN hpgl-mm-plu( x * hpgl-x-scaling-factor ).
  RUN hpgl-append( "," ).
  RUN hpgl-mm-plu( y * hpgl-y-scaling-factor ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-moveto) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-moveto Method-Library 
PROCEDURE hpgl-moveto :
/*------------------------------------------------------------------------------
  Purpose:  Set codes to move to a location
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER x AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER y AS DECIMAL NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
    RUN hpgl-pen-up.
    RUN hpgl-append( "[-pdf-]>PA:" ).          /* Plot absolute */
    RUN hpgl-mm-plu( x - {&HPGL-X-OFFSET} ).
    RUN hpgl-append( "," ).
    RUN hpgl-mm-plu( y - {&HPGL-Y-OFFSET} ).
    RUN hpgl-append( "[-pdf-]" ).
END.
ELSE DO:
  RUN hpgl-pen-up.
  RUN hpgl-append( "PA" ).          /* Plot absolute */
  RUN hpgl-mm-plu( x - {&HPGL-X-OFFSET} ).
  RUN hpgl-append( "," ).
  RUN hpgl-mm-plu( y - {&HPGL-Y-OFFSET} ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-output-mode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-output-mode Method-Library 
PROCEDURE hpgl-output-mode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER mode AS CHAR NO-UNDO.

hpgl-output-mode = mode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-pen-down) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-pen-down Method-Library 
PROCEDURE hpgl-pen-down :
/*------------------------------------------------------------------------------
  Purpose:  Puts the pen down (if it isn't already)
------------------------------------------------------------------------------*/
  IF hpgl-pen-is-down <> yes THEN DO:
      IF hpgl-output-mode = "pdf" THEN RUN hpgl-append( "[-pdf-]>PD:[-pdf-]" ).
      ELSE RUN hpgl-append( "PD" ).
  END.
  hpgl-pen-is-down = yes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-pen-up) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-pen-up Method-Library 
PROCEDURE hpgl-pen-up :
/*------------------------------------------------------------------------------
  Purpose:  Puts the pen down (if it isn't already)
------------------------------------------------------------------------------*/
  IF hpgl-pen-is-down <> no THEN DO:
    IF hpgl-output-mode = "pdf" THEN RUN hpgl-append( "[-pdf-]>PU:[-pdf-]" ).
    ELSE RUN hpgl-append( "PU" ).
  END.
  hpgl-pen-is-down = no.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-polygon) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-polygon Method-Library 
PROCEDURE hpgl-polygon :
/*------------------------------------------------------------------------------
  Purpose:  Draw a polygon based on a list of point pairs.
    The point pairs should be <X>,<Y> and the list should have each
    point pair as <pair 1>;<pair 2>;<pair 3>;...;<pair n>
    I.e. x1,y1;x2,y2;x3,y3;...;xn,yn
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER point-list AS CHAR NO-UNDO.
DEF INPUT PARAMETER fill-percent AS DEC NO-UNDO.

DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.
DEF VAR point AS CHAR NO-UNDO.
DEF VAR x AS DEC NO-UNDO.
DEF VAR y AS DEC NO-UNDO.

  RUN hpgl-start-polygon.

  n = NUM-ENTRIES(point-list, ";").
  DO i = 1 TO n:
    point = ENTRY( i, point-list, ";").
    x = DEC( TRIM( ENTRY( 1, point) ) ).
    y = DEC( TRIM( ENTRY( 2, point) ) ).
    RUN hpgl-line-relative( x, y).
  END.

  RUN hpgl-finish-polygon.
  RUN hpgl-fill-polygon(fill-percent).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-replace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-replace Method-Library 
PROCEDURE hpgl-replace :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEF INPUT PARAMETER from-string AS CHAR NO-UNDO.
DEF INPUT PARAMETER to-string   AS CHAR NO-UNDO.

  IF from-string = ? THEN RETURN.
  hpgl-codes = REPLACE( hpgl-codes, from-string, (IF to-string = ? THEN "?" ELSE to-string) ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-set-font) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-set-font Method-Library 
PROCEDURE hpgl-set-font :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER font-string AS CHAR NO-UNDO.

DEF VAR font-codes AS CHAR NO-UNDO.
DEF VAR cols AS INT NO-UNDO.  /* ignored */
DEF VAR rows AS INT NO-UNDO.  /* ignored */

  hpgl-last-font = font-string.

  IF hpgl-output-mode = "pdf" THEN DO:
    RUN hpgl-append("[-pdf-]>FONT:" + font-string + "[-pdf-]" ).
  END.
  ELSE DO:
    RUN make-control-string( "PCL", font-string, OUTPUT font-codes, OUTPUT cols, OUTPUT rows).
    RUN hpgl-append( font-codes ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-set-line-type) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-set-line-type Method-Library 
PROCEDURE hpgl-set-line-type :
/*------------------------------------------------------------------------------
  Purpose:  Set the line characteristics
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER line-type AS INT NO-UNDO.
DEF INPUT PARAMETER pattern-length AS DECIMAL NO-UNDO.
DEF INPUT PARAMETER line-mode AS DECIMAL NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
  RUN hpgl-append( "[-pdf-]>LT:" ).
  RUN hpgl-decimal-value( DECIMAL(line-type) ).
  IF pattern-length <> ? THEN DO:
    RUN hpgl-append( "," ).
    RUN hpgl-decimal-value( pattern-length ).
    IF line-mode <> ? THEN DO:
      RUN hpgl-append( "," ).
      RUN hpgl-decimal-value( line-mode ).
    END.
  END.
  RUN hpgl-append( "[-pdf-]" ).
END.
ELSE DO:
  RUN hpgl-append( "LT" ).
  RUN hpgl-decimal-value( DECIMAL(line-type) ).
  IF pattern-length <> ? THEN DO:
    RUN hpgl-append( "," ).
    RUN hpgl-decimal-value( pattern-length ).
    IF line-mode <> ? THEN DO:
      RUN hpgl-append( "," ).
      RUN hpgl-decimal-value( line-mode ).
    END.
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-set-line-width) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-set-line-width Method-Library 
PROCEDURE hpgl-set-line-width :
/*------------------------------------------------------------------------------
  Purpose:  Set the line width
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER line-width AS DECIMAL NO-UNDO.

IF hpgl-output-mode = "pdf" THEN DO:
  RUN hpgl-append( "[-pdf-]>PW:" ).
  RUN hpgl-decimal-value( line-width ).
  RUN hpgl-append( "[-pdf-]" ).
END.
ELSE DO:
  RUN hpgl-append( "PW" ).
  RUN hpgl-decimal-value( line-width ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-set-orientation) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-set-orientation Method-Library 
PROCEDURE hpgl-set-orientation :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER orientation AS CHAR NO-UNDO.

DEF VAR prcodes AS CHAR NO-UNDO.
DEF VAR cols AS INT NO-UNDO.  /* ignored */
DEF VAR rows AS INT NO-UNDO.  /* ignored */

  IF hpgl-output-mode = "pdf" THEN DO:
     IF orientation = "Landscape" THEN
         RUN make-control-string( "PCL", orientation, OUTPUT prcodes, OUTPUT cols, OUTPUT rows).
     RUN hpgl-append( "[-pdf-]>ORIENTATION:" + ORIENTATION + "[-pdf-]" ).
  END.
  ELSE DO:
    IF orientation = "Landscape" THEN
      RUN make-control-string( "PCL", orientation, OUTPUT prcodes, OUTPUT cols, OUTPUT rows).
    ELSE
      RUN make-control-string( "PCL", "portrait", OUTPUT prcodes, OUTPUT cols, OUTPUT rows).

    RUN hpgl-append ( hpgl-pcl() ).
    RUN hpgl-append( prcodes ).
    RUN hpgl-append ( hpgl-hpgl() ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-set-scale) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-set-scale Method-Library 
PROCEDURE hpgl-set-scale :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-x-scale AS DEC NO-UNDO.
DEF INPUT PARAMETER new-y-scale AS DEC NO-UNDO.

   hpgl-x-scaling-factor = new-x-scale.
   hpgl-y-scaling-factor = new-y-scale.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-shaded-text) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-shaded-text Method-Library 
PROCEDURE hpgl-shaded-text :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER this-font AS CHAR NO-UNDO.
DEF INPUT PARAMETER text-string AS CHAR NO-UNDO.
DEF INPUT PARAMETER density AS DEC NO-UNDO.

  IF hpgl-output-mode = "pdf" THEN DO:
     RUN hpgl-append( "[-pdf-]>TEXT:" + this-font + "," ).
     RUN hpgl-decimal-value(density).
     RUN hpgl-append( "," + text-string + "[-pdf-]" ).
  END.
  ELSE DO:
    RUN hpgl-append( hpgl-pcl() ).

    /* set shading */
    RUN hpgl-append( hpgl-esc("*v0N") + hpgl-esc("*v0O") + hpgl-esc("*c") ).
    RUN hpgl-decimal-value(density).
    RUN hpgl-append( "G" + hpgl-esc("*v2T") ).

    IF hpgl-last-font <> this-font THEN RUN hpgl-set-font( this-font ).
    RUN hpgl-append( text-string ).

    /* return to black */
    RUN hpgl-append( hpgl-esc("*v0N") + hpgl-esc("*v0O") + hpgl-esc("*c0G") + hpgl-esc("*v0T") ).

    RUN hpgl-append( hpgl-hpgl() ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-start-polygon) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-start-polygon Method-Library 
PROCEDURE hpgl-start-polygon :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF hpgl-output-mode = "pdf" THEN RUN hpgl-append( "[-pdf-]>POLY:" ).
  ELSE RUN hpgl-append( "PM" ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-text) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-text Method-Library 
PROCEDURE hpgl-text :
/*------------------------------------------------------------------------------
  Purpose:  Put text in the current font, at the current position
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER this-font AS CHAR NO-UNDO.
DEF INPUT PARAMETER text-string AS CHAR NO-UNDO.

  IF hpgl-output-mode = "pdf" THEN DO:
     IF hpgl-last-font <> this-font THEN DO:
        RUN hpgl-set-font( this-font ).
     END.
     RUN hpgl-append( "[-pdf-]>TEXT:" + text-string + "[-pdf-]" ).
  END.
  ELSE DO:
    RUN hpgl-append ( hpgl-pcl() ).
    IF hpgl-last-font <> this-font THEN DO:
      RUN hpgl-set-font( this-font ).
    END.
    RUN hpgl-append( text-string ).
    RUN hpgl-append( hpgl-hpgl() ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-wrap-lines) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hpgl-wrap-lines Method-Library 
PROCEDURE hpgl-wrap-lines :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER this-font AS CHAR NO-UNDO.
DEF INPUT PARAMETER s1 AS CHAR NO-UNDO.
DEF INPUT PARAMETER cols AS INT NO-UNDO.
DEF INPUT PARAMETER down-by AS DEC NO-UNDO.

DEF VAR wrapped AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.

  RUN hpgl-append ( hpgl-pcl() ).
  IF hpgl-last-font <> this-font THEN DO:
    RUN hpgl-set-font( this-font ).
  END.
  wrapped = (IF s1 = ? THEN "" ELSE WRAP(s1, cols) ).
  n = NUM-ENTRIES( wrapped, "~n" ).
  DO i = 1 TO n:
    RUN hpgl-append( ENTRY( i, wrapped, "~n" ) ).
    RUN hpgl-append( hpgl-hpgl() ).
    IF i < n THEN DO:
      RUN hpgl-move-relative( 0, down-by ).
      RUN hpgl-append( hpgl-pcl() ).
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-office-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE office-address Method-Library 
PROCEDURE office-address :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
RUN VALUE(corporate-logo-routine) ( "Address":U, hpgl-output-mode ) NO-ERROR.

RUN hpgl-append( RETURN-VALUE ).
RUN hpgl-moveto( 0, 260 ).

IF hpgl-output-mode = "pdf" THEN DO:
    RUN hpgl-append( "[-pdf-]PCLTOHPGL:[-pdf-]" ).
END.
ELSE DO:
    /* force PCL position to HPGL position */
    RUN hpgl-append( CHR(27) + "%1A" + CHR(27) + "%1B" ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-office-logo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE office-logo Method-Library 
PROCEDURE office-logo :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
RUN VALUE(corporate-logo-routine) ( "Logo":U, hpgl-output-mode ) NO-ERROR.
RUN hpgl-append( RETURN-VALUE ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-sea-logo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sea-logo Method-Library 
PROCEDURE sea-logo :
/*------------------------------------------------------------------------------
  Purpose:  Print the "SEA" logo
------------------------------------------------------------------------------*/

RUN hpgl-start-polygon.
RUN hpgl-line-relative( -5, 0 ).
RUN hpgl-line-relative( 0, 5 ).
RUN hpgl-line-relative( 2, 0 ).
RUN hpgl-line-relative( 0, -3 ).
RUN hpgl-line-relative( 3, 0 ).
RUN hpgl-line-relative( 0, -2 ).
RUN hpgl-finish-polygon.
RUN hpgl-fill-polygon( 100 ).

RUN hpgl-move-relative( 0.5, 0).
RUN hpgl-start-polygon.
RUN hpgl-line-relative( 2, 2 ).
RUN hpgl-line-relative( 0, 5.5 ).
RUN hpgl-line-relative( -5.5, 0 ).
RUN hpgl-line-relative( -2, -2 ).
RUN hpgl-line-relative( 5.5, 0 ).
RUN hpgl-line-relative( 0, -5.5 ).
RUN hpgl-finish-polygon.
RUN hpgl-fill-polygon( 100 ).

RUN hpgl-move-relative( -0.5, 4).
RUN hpgl-start-polygon.
RUN hpgl-line-relative( -1, 0 ).
RUN hpgl-line-relative( 0, 1 ).
RUN hpgl-line-relative( 1, 0 ).
RUN hpgl-line-relative( 0, -1 ).
RUN hpgl-finish-polygon.
RUN hpgl-fill-polygon( 100 ).

RUN hpgl-move-relative( -1, 4).
RUN hpgl-start-polygon.
RUN hpgl-line-relative( 0, 1.5 ).
RUN hpgl-line-relative( 2, 2 ).
RUN hpgl-line-relative( 0, -3.5 ).
RUN hpgl-line-relative( -2, 0 ).
RUN hpgl-finish-polygon.
RUN hpgl-fill-polygon( 100 ).

RUN hpgl-move-relative( 2.5, 1.5).
RUN hpgl-start-polygon.
RUN hpgl-line-relative( 3, 0 ).
RUN hpgl-line-relative( 0, -3 ).
RUN hpgl-line-relative( 2, 0 ).
RUN hpgl-line-relative( 0, 5 ).
RUN hpgl-line-relative( -5, 0 ).
RUN hpgl-line-relative( 0, -2 ).
RUN hpgl-finish-polygon.
RUN hpgl-fill-polygon( 100 ).

RUN hpgl-move-relative( 1.5, -3.5).
RUN hpgl-start-polygon.
RUN hpgl-line-relative( 3.5, 0 ).
RUN hpgl-line-relative( -2, -2 ).
RUN hpgl-line-relative( -1.5, 0 ).
RUN hpgl-line-relative( 0, 2 ).
RUN hpgl-finish-polygon.
RUN hpgl-fill-polygon( 100 ).

RUN hpgl-move-relative( -8, -6).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ttpl-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ttpl-address Method-Library 
PROCEDURE ttpl-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

/* save page position in PCL :-)  - sneaky! */
RUN hpgl-append( CHR(27) + "%1A" + CHR(27) + "%1B" ).

RUN hpgl-move-relative( -54, -2.5 ).
RUN hpgl-text( 'helvetica,point,5,proportional',
  "LEVEL 3, CITIBANK CENTRE, 23 CUSTOMS STREET EAST, PRIVATE BAG 92029, AUCKLAND").
RUN hpgl-move-relative( 10, -2 ).
RUN hpgl-text( hpgl-last-font,
  "DX CP29018, TELEPHONE 64-9-303 3800, FACSIMILE 64-9-377 7895").


IF hpgl-output-mode = "pdf" THEN DO:
    RUN hpgl-append( "[-pdf-]PCLTOHPGL:[-pdf-]" ).
END.
ELSE DO:
    /* restore page position from PCL :-)  - sneaky! */
    RUN hpgl-append( CHR(27) + "%0A" + CHR(27) + "%1B" ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ttpl-logo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ttpl-logo Method-Library 
PROCEDURE ttpl-logo :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
/* save current page position in PCL :-)  - sneaky! */
RUN hpgl-append( CHR(27) + "%1A" + CHR(27) + "%1B" ).
RUN hpgl-moveto( 105, 272 ).
RUN sea-logo.
RUN hpgl-move-relative( -18, -6).
RUN hpgl-text( 'helvetica,point,18,proportional,bold',"T").
RUN hpgl-move-relative( 3.3, 0).
RUN hpgl-text( 'helvetica,point,15,proportional,bold',"RANS").
RUN hpgl-move-relative( 18, 0).
RUN hpgl-text( 'helvetica,point,18,proportional,bold',"T").
RUN hpgl-move-relative( 3.3, 0).
RUN hpgl-text( 'helvetica,point,15,proportional,bold',"ASMAN").
RUN hpgl-move-relative( -17, -2).
RUN hpgl-text( 'helvetica,point,5,proportional',"P R O P E R T I E S    L I M I T E D").


IF hpgl-output-mode = "pdf" THEN DO:
    RUN hpgl-append( "[-pdf-]PCLTOHPGL:[-pdf-]" ).
END.
ELSE DO:
    /* restore page position from PCL :-)  - sneaky! */
    RUN hpgl-append( CHR(27) + "%0A" + CHR(27) + "%1B" ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-hpgl-esc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION hpgl-esc Method-Library 
FUNCTION hpgl-esc RETURNS CHARACTER
  ( INPUT s1 AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  IF hpgl-output-mode = "pdf" THEN DO:
    RETURN "".
  END.
  ELSE DO:
    RETURN CHR(27) + s1 .
  END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-hpgl) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION hpgl-hpgl Method-Library 
FUNCTION hpgl-hpgl RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  IF hpgl-output-mode = "pdf" THEN DO:
    RETURN "".
  END.
  ELSE DO:
    RETURN CHR(27) + "%0B".
  END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hpgl-pcl) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION hpgl-pcl Method-Library 
FUNCTION hpgl-pcl RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  IF hpgl-output-mode = "pdf" THEN DO:
    RETURN "".
  END.
  ELSE DO:
    RETURN CHR(27) + "%1A".
  END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

