&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

{inc/DEBUG.i "m-drlwin.i definitions begin"}

/* If called from a drill button, these variables must be set
   for correct operation of this drill window */

/* Other variables */

DEF VAR btn      AS HANDLE EXTENT 50 NO-UNDO.
DEF VAR opt-pnl  AS HANDLE EXTENT 10 NO-UNDO.

{inc/DEBUG.i "m-drlwin.i definitions end"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .35
         WIDTH              = 41.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-mstwin.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

{inc/DEBUG.i "m-drlwin.i main end"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-default-layout) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE default-layout Method-Library 
PROCEDURE default-layout :
/*------------------------------------------------------------------------------
  Purpose:     Default widget layout for this window
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{inc/DEBUG.i "m-drlwin.i default-layout begin"}
  RUN get-default-groups.
{inc/DEBUG.i "m-drlwin.i default-layout got groups"}
  RUN stack( "opt-group", "mst-group", "T", No ).
{inc/DEBUG.i "m-drlwin.i default-layout stacked 1"}
  RUN stack( STRING( curr-viewer ) + ",note-group,btn-group", "mst-group", "T", Yes ).
{inc/DEBUG.i "m-drlwin.i default-layout end"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-default-groups) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-default-groups Method-Library 
PROCEDURE get-default-groups :
/*------------------------------------------------------------------------------
  Purpose:     Defines the default widget groups for this window
------------------------------------------------------------------------------*/

{inc/DEBUG.i "m-drlwin.i get-default-groups begin"}
  
  DEF VAR btn-list AS CHAR NO-UNDO.
  DEF VAR pnl-list AS CHAR NO-UNDO.
  DEF VAR flt-list AS CHAR NO-UNDO.
  DEF VAR nte-list AS CHAR NO-UNDO.

  DEF VAR h-ABC1   AS HANDLE NO-UNDO.
  DEF VAR h-ABC2   AS HANDLE NO-UNDO.
  DEF VAR h-Search AS HANDLE NO-UNDO.
  DEF VAR h-Note   AS HANDLE NO-UNDO.
  DEF VAR h-Temp   AS HANDLE NO-UNDO.
  DEF VAR browse-handle  AS HANDLE NO-UNDO.

  RUN get-attribute IN curr-viewer ( 'adm-object-handle':U ).
  browse-handle = WIDGET-HANDLE( RETURN-VALUE ).

  /* Do all filter type panels */
  
  RUN get-attribute( 'Filter-Panel' ).
  IF RETURN-VALUE <> ? THEN DO:
    flt-list = flt-list + "," + RETURN-VALUE.
    h-ABC1 = WIDGET-HANDLE(RETURN-VALUE).
    RUN set-size IN h-ABC1 ( 1, browse-handle:width-chars ).
  END.

  RUN get-attribute( 'Filter2-Panel' ).
  IF RETURN-VALUE <> ? THEN DO:
    flt-list = flt-list + "," + RETURN-VALUE.
    h-ABC2 = WIDGET-HANDLE(RETURN-VALUE).
    RUN set-size IN h-ABC2 ( 1, browse-handle:width-chars ).
  END.

  /* Do all option type panels */

  RUN get-attribute ( 'FilterBy-Panel' ).
  IF RETURN-VALUE <> ? THEN pnl-list = pnl-list + "," + RETURN-VALUE.
  RUN get-attribute ( 'SortBy-Panel' ).
  IF RETURN-VALUE <> ? THEN pnl-list = pnl-list + "," + RETURN-VALUE.
  RUN get-attribute( 'SearchBy-Panel' ).
  IF RETURN-VALUE <> ? THEN pnl-list = pnl-list + "," + RETURN-VALUE.
  RUN get-attribute( 'Option-Panel' ).
  IF RETURN-VALUE <> ? THEN pnl-list = pnl-list + "," + RETURN-VALUE.

  RUN get-attribute( 'Notes-Handle' ).
  IF RETURN-VALUE <> ? THEN DO:
    nte-list = nte-list + "," + RETURN-VALUE.
    h-Note = WIDGET-HANDLE(RETURN-VALUE).

    RUN get-attribute IN curr-viewer( "Note-Font":U ).
    IF RETURN-VALUE <> "" THEN
      RUN set-attribute-list IN h-Note ( 'Font = ':U + RETURN-VALUE ).

    DEF VAR note-height AS DECIMAL NO-UNDO.
    RUN get-attribute IN curr-viewer( "Note-Height":U ).
    note-height = IF RETURN-VALUE = ? THEN 5 ELSE DECIMAL(RETURN-VALUE).

    DEF VAR note-width AS DECIMAL NO-UNDO.
    RUN get-attribute IN curr-viewer( "Note-Width":U ).
    IF RETURN-VALUE = ? THEN
      note-width = browse-handle:width-chars.
    ELSE
      note-width = DECIMAL(RETURN-VALUE).

    RUN set-size IN h-Note ( note-height, note-width ).
  END.

  /* Do all link panels */
  DEF VAR i AS INT NO-UNDO.
  DEF VAR j AS INT NO-UNDO.
  DEF VAR direction  AS CHAR NO-UNDO.

  DO i = 1 TO 2:
    direction = ( IF i = 1 THEN "From" ELSE "To" ).
    DO j = 1 TO 3:
      RUN get-attribute( "Link-" + direction + "-" + STRING( j ) ).
      IF RETURN-VALUE <> ? THEN DO:
        h-Temp = WIDGET-HANDLE(RETURN-VALUE).
        RUN advise-panel-location IN h-Temp NO-ERROR.
        CASE RETURN-VALUE:
          WHEN "first" THEN         pnl-list = STRING(h-Temp) + "," + pnl-list .
          WHEN "middle" THEN        nte-list = STRING(h-Temp) + "," + nte-list .
          WHEN "bottom" THEN        nte-list = nte-list + "," + STRING(h-Temp) .
          OTHERWISE /* second */    pnl-list = pnl-list + "," + STRING(h-Temp) .
        END CASE.
      END.
    END.
  END.

  RUN pre-pack IN curr-viewer ( THIS-PROCEDURE ) NO-ERROR .

  pnl-list = TRIM( pnl-list, "," ).
  flt-list = TRIM( flt-list, "," ).
  nte-list = TRIM( nte-list, "," ).

  RUN pack( pnl-list + "," + flt-list, "opt-group", "L", No ).
  RUN stack( nte-list, "note-group", "T", No ).
    
  RUN get-btn-list IN sys-mgr( THIS-PROCEDURE, OUTPUT btn-list ).
  RUN pack( btn-list, "btn-group", "L", No ).

  {inc/DEBUG.i "m-drlwin.i get-default-groups end"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-sub1-create-objects) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sub1-create-objects Method-Library 
PROCEDURE sub1-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Master create objects for drill windows
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{inc/DEBUG.i "m-drlwin.i sub1-create-objects begin"}
  RUN sub2-create-objects  IN THIS-PROCEDURE NO-ERROR.
{inc/DEBUG.i "m-drlwin.i sub1-create-objects end"}
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

