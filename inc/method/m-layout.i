&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Define the temporaray table used for widget layout */
&GLOBAL-DEFINE GP 6
&GLOBAL-DEFINE LayoutItem Yes

&IF DEFINED( LayoutItem ) &THEN

DEFINE TEMP-TABLE LayoutItem NO-UNDO
  FIELD group-id  AS CHAR  /* Identifier of parent group (if none then = ?) */
  FIELD sequence  AS INT   /* Sequence of the group/item within it's parent */
  FIELD bounding  AS LOGI  /* Does this item's size bound others in the group */
  FIELD type      AS CHAR  /* ? = single item, P = Pack Group, S = Stack Group */
  FIELD id        AS CHAR  /* group name/widget-handle string */ 
  FIELD alignment AS CHAR  /* Alignment of the group/item
                              For Pack  Groups: L = Left, R = Right,  C = Centre
                              For Stack Groups: T = Top , B = Bottom, C = Centre */
  FIELD drawn  AS LOGI
  FIELD x AS INT
  FIELD y AS INT
  FIELD w AS INT
  FIELD h AS INT
                              
  INDEX child-sequence IS UNIQUE PRIMARY group-id sequence ASCENDING
  INDEX child-align    IS UNIQUE group-id alignment sequence   ASCENDING
  INDEX group-child    IS UNIQUE group-id id
  INDEX group-bound    group-id bounding
  INDEX id id.

&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .25
         WIDTH              = 34.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-draw-window) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE draw-window Method-Library 
PROCEDURE draw-window :
/*------------------------------------------------------------------------------
  Purpose:     Position the groups
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN predraw-group(?).
  RUN resize-window.
  RUN rec-draw(?, {&GAP}, {&GAP}).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-widget-size) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-widget-size Method-Library 
PROCEDURE get-widget-size :
/*------------------------------------------------------------------------------
  Purpose:     Set the size of a non-group layout item ( widget )
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER item-id AS CHAR NO-UNDO.

  DEF BUFFER item FOR LayoutItem.
  FIND FIRST item WHERE item.id = item-id.
    
  DEF VAR widget AS HANDLE NO-UNDO.
  widget = WIDGET-HANDLE( item.id ).

  IF CAN-QUERY( widget, 'TYPE':U ) AND widget:TYPE = 'PROCEDURE':U THEN
  DO:
    RUN get-attribute IN widget( 'adm-object-handle':U ).
    widget = WIDGET-HANDLE( RETURN-VALUE ).
  END.

  IF VALID-HANDLE( widget ) THEN
    ASSIGN
      item.w = widget:width-pixels
      item.h = widget:height-pixels
      item.drawn = yes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-group-widgets) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE group-widgets Method-Library 
PROCEDURE group-widgets :
/*------------------------------------------------------------------------------
  Purpose:     Group layout items
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER typ   AS CHAR NO-UNDO.
DEF INPUT PARAMETER items AS CHAR NO-UNDO.
DEF INPUT PARAMETER grp   AS CHAR NO-UNDO.
DEF INPUT PARAMETER align AS CHAR NO-UNDO.
DEF INPUT PARAMETER bound AS LOGI NO-UNDO.

  DEF VAR msg-prefix  AS CHAR NO-UNDO.
  DEF VAR jst-options AS CHAR NO-UNDO.
  DEF VAR seq         AS INT INIT 0 NO-UNDO.
  DEF VAR i           AS INT NO-UNDO.
  DEF VAR item        AS CHAR NO-UNDO.

  jst-options = IF typ = "S" THEN 'T,C,B':U ELSE 'L,C,R':U.

  /* check if the group already exists with a different type */

  IF CAN-FIND( FIRST LayoutItem WHERE LayoutItem.id = grp AND LayoutItem.Type <> typ ) THEN
  DO:
    MESSAGE "The layout item group " grp " has already been defined!" SKIP
      VIEW-AS ALERT-BOX ERROR TITLE "Error grouping widgets".
    RETURN.
  END.
  
  /* Create the group if it doesn't exist */
  
  IF NOT CAN-FIND( FIRST LayoutItem WHERE LayoutItem.id = grp ) THEN DO:
    CREATE LayoutItem.
    ASSIGN
      group-id  = ?
      sequence  = ?
      type      = typ
      id        = grp
      alignment = ?
      bound     = No.
  END.
  ELSE DO:
    DEF BUFFER LastItem FOR LayoutItem.
    FIND LAST LastItem WHERE LastItem.group-id = grp NO-LOCK NO-ERROR.
    IF AVAILABLE LastItem THEN seq = LastItem.Sequence.
  END.
  
  /* Add the given items to the group */
  DEF VAR is-alpha  AS LOGI NO-UNDO.
  DEF VAR dummy-int AS INT NO-UNDO.
  
  DO i = 1 TO NUM-ENTRIES( items ):
    
    item = ENTRY( i, items ).
    
    /* Check to see if the item is an integer - then it must be a widget-handle */
    ASSIGN dummy-int = INT( item ) NO-ERROR.
    is-alpha = ERROR-STATUS:ERROR.
    
    IF CAN-FIND( FIRST LayoutItem WHERE LayoutItem.id = item ) THEN DO:
      seq = seq + 1.
      FIND FIRST LayoutItem WHERE LayoutItem.id = item.
      ASSIGN
        LayoutItem.group-id  = grp
        LayoutItem.sequence  = seq
        LayoutItem.bound     = bound
        LayoutItem.alignment = IF LOOKUP( align, jst-options ) <> 0 THEN align ELSE ENTRY( 1, jst-options ).
    END.
    ELSE IF NOT is-alpha AND VALID-HANDLE( WIDGET-HANDLE( item ) ) THEN DO:
      seq = seq + 1.
      CREATE LayoutItem.
      ASSIGN
        LayoutItem.group-id  = grp
        LayoutItem.sequence  = seq
        LayoutItem.type      = ?
        LayoutItem.id        = item
        LayoutItem.bound     = bound.
        LayoutItem.alignment = IF LOOKUP( align, jst-options ) <> 0 THEN align ELSE ENTRY( 1, jst-options ).
      bound = no.
    END.
    ELSE.
      /* The widget or group does not exist - just ignore */
  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-init-layout) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE init-layout Method-Library 
PROCEDURE init-layout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH LayoutItem: DELETE LayoutItem. END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pack) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pack Method-Library 
PROCEDURE pack :
/*------------------------------------------------------------------------------
  Purpose:     Pack items into a pack group
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER items AS CHAR NO-UNDO.
  DEF INPUT PARAMETER grp   AS CHAR NO-UNDO.
  DEF INPUT PARAMETER align AS CHAR NO-UNDO.
  DEF INPUT PARAMETER bound AS LOGI NO-UNDO.

  RUN group-widgets( "P", items, grp, align, bound ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-predraw-bounding-item) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE predraw-bounding-item Method-Library 
PROCEDURE predraw-bounding-item :
/*------------------------------------------------------------------------------
  Purpose:     Return the id of  the bounding item for the given group
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF PARAMETER BUFFER grp FOR LayoutItem.
  DEF OUTPUT PARAMETER bnd-id AS CHAR NO-UNDO.
  
  IF grp.group-id = ? THEN RETURN.
  
  FIND FIRST LayoutItem WHERE LayoutItem.group-id = grp.group-id AND LayoutItem.Bounding
    NO-LOCK NO-ERROR.

  IF NOT AVAILABLE LayoutItem THEN  
  DO:
    FIND FIRST LayoutItem WHERE LayoutItem.group-id = grp.group-id NO-ERROR.
    ASSIGN LayoutItem.Bounding = Yes.
  END.

  IF NOT AVAILABLE LayoutItem OR LayoutItem.id = grp.id THEN RETURN.
  bnd-id = LayoutItem.id.
  IF LayoutItem.Drawn THEN RETURN.
  
  IF LayoutItem.Type <> ? THEN
    RUN predraw-group( LayoutItem.id ).
  ELSE
    RUN get-widget-size( LayoutItem.id ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-predraw-group) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE predraw-group Method-Library 
PROCEDURE predraw-group :
/*------------------------------------------------------------------------------
  Purpose:     Draw a widget group
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER grp-id AS CHAR NO-UNDO.

  DEF VAR bnd-id   AS CHAR NO-UNDO.
  DEF BUFFER grp   FOR LayoutItem.
  DEF BUFFER bnd   FOR LayoutItem.
  DEF BUFFER child FOR LayoutItem.
  
  DEF VAR line-x    AS INT NO-UNDO.  DEF VAR line-y AS INT NO-UNDO.
  DEF VAR line-size AS INT NO-UNDO.  DEF VAR line-n    AS INT NO-UNDO.
  DEF VAR bnd-w     AS INT NO-UNDO.  DEF VAR bnd-h  AS INT NO-UNDO.
  DEF VAR gap       AS INT NO-UNDO.
  DEF VAR x-gap     AS INT NO-UNDO.
  DEF VAR y-gap     AS INT NO-UNDO.

  IF grp-id = ? THEN FIND grp WHERE group-id = ? NO-ERROR.
                ELSE FIND grp WHERE id = grp-id  NO-ERROR.
                
  IF NOT AVAILABLE grp THEN RETURN.

  RUN predraw-bounding-item( BUFFER grp, OUTPUT bnd-id ).
  FIND FIRST bnd WHERE bnd.id = bnd-id NO-LOCK NO-ERROR.
  bnd-w = IF AVAILABLE bnd THEN bnd.w ELSE INT(SESSION:WIDTH-PIXELS * 0.75).
  bnd-h = IF AVAILABLE bnd THEN bnd.h ELSE INT(SESSION:HEIGHT-PIXELS * 0.80).

  x-gap = IF grp.id = "btn-group" THEN {&BTN-X-GAP} ELSE {&GAP}.
  y-gap = IF grp.id = "btn-group" THEN {&BTN-Y-GAP} ELSE {&GAP}.

  FOR EACH child WHERE child.group-id = grp.id NO-LOCK.

    IF NOT child.Drawn THEN
      IF child.Type = ? THEN
        RUN get-widget-size( child.id ).
      ELSE
        RUN predraw-group( child.id ).

    IF Grp.Type = "S" THEN DO:
      gap = IF line-n = 0 THEN 0 ELSE y-gap.
      IF bnd-h > 0 AND line-n > 0 AND
         line-y + gap + child.h > bnd-h THEN
      DO:
        line-x = line-x + line-size + x-gap.
        ASSIGN
          line-y = 0
          line-n = 0
          line-size = 0
          gap    = 0.
      END.

      ASSIGN
        child.X = line-x
        child.Y = gap + line-y.
      line-y = child.Y + child.h.
      
      IF line-y > grp.h THEN grp.h = line-y.
      IF child.w > line-size THEN line-size = child.w.
      IF line-x + line-size > grp.w THEN grp.w = line-x + line-size.

    END.
    ELSE IF Grp.Type = "P" THEN
    DO:
      
      gap = IF line-n = 0 THEN 0 ELSE x-gap.
      IF bnd-w > 0 AND line-n > 0 AND
        line-x + gap + child.w > bnd-w THEN
      DO:
        line-y = line-y + line-size + y-gap.
        ASSIGN
          line-x = 0
          line-n = 0
          line-size = 0
          gap = 0.
      END.
      
      child.X = gap + line-x.
      child.Y = line-y.
      line-x = child.X + child.w.
      
      IF line-x > grp.w THEN grp.w = line-x.
      IF child.h > line-size THEN line-size = child.h.
      IF line-y + line-size > grp.h THEN grp.h = line-y + line-size.
      
    END.
    line-n = line-n + 1.  

  END.    
  grp.Drawn = Yes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-rec-draw) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rec-draw Method-Library 
PROCEDURE rec-draw :
/*------------------------------------------------------------------------------
  Purpose:     Recurisvely draw and position widgets from the given group
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER grp-id AS CHAR NO-UNDO.
  DEF INPUT PARAMETER x-off  AS INT NO-UNDO.
  DEF INPUT PARAMETER y-off  AS INT NO-UNDO.
  
  DEF BUFFER grp   FOR LayoutItem.
  DEF BUFFER child FOR LayoutItem.
  
  IF grp-id = ? THEN FIND grp WHERE group-id = ? NO-ERROR.
                ELSE FIND grp WHERE id = grp-id  NO-ERROR.
  
  IF NOT AVAILABLE grp THEN RETURN.

  x-off = x-off + grp.x. y-off = y-off + grp.y.
  FOR EACH child WHERE child.group-id = grp.id NO-LOCK.
    IF child.Type = ? THEN
      RUN set-widget-pos( child.id, x-off, y-off ).
    ELSE
      RUN rec-draw( child.id, x-off, y-off ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-resize-window) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resize-window Method-Library 
PROCEDURE resize-window :
/*------------------------------------------------------------------------------
  Purpose:     Size the parent object accdording to the size of the master group
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF BUFFER mst FOR LayoutItem.
  FIND mst WHERE mst.group-id = ? NO-LOCK NO-ERROR.

  IF NOT AVAILABLE mst THEN RETURN.
  
  DEF VAR w AS INT NO-UNDO.
  DEF VAR h AS INT NO-UNDO.

  w = mst.w + {&GAP} * 2.
  h = mst.h + {&GAP} * 2.   
 
  DEF VAR frm AS HANDLE NO-UNDO. frm = FRAME {&FRAME-NAME}:HANDLE.
  DEF VAR win AS HANDLE NO-UNDO. win = {&WINDOW-NAME}:HANDLE.

DO WITH FRAME {&FRAME-NAME}:
  frm:SCROLLABLE = No.
  win:WIDTH-PIXELS  = w.
  win:HEIGHT-PIXELS = h.
  ASSIGN frm:WIDTH-PIXELS  = w NO-ERROR.
  ASSIGN frm:HEIGHT-PIXELS = h NO-ERROR.
END.

  RUN post-resize-window IN THIS-PROCEDURE NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-widget-pos) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-widget-pos Method-Library 
PROCEDURE set-widget-pos :
/*------------------------------------------------------------------------------
  Purpose:     Set a widget position relative to the given offsets
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER item-id AS CHAR NO-UNDO.
  DEF INPUT PARAMETER x-off   AS INT NO-UNDO.
  DEF INPUT PARAMETER y-off   AS INT NO-UNDO.
  
  DEF BUFFER item FOR LayoutItem.
  FIND item WHERE item.id = item-id NO-LOCK.
    
  DEF VAR widget AS HANDLE NO-UNDO.
  widget = WIDGET-HANDLE( item.id ).

  IF CAN-QUERY( widget, 'TYPE':U ) AND widget:TYPE = 'PROCEDURE':U THEN
  DO:
    RUN get-attribute IN widget( 'adm-object-handle':U ).
    widget = WIDGET-HANDLE( RETURN-VALUE ).
  END.

  IF VALID-HANDLE( widget ) THEN
    ASSIGN
      widget:X = x-off + item.x
      widget:Y = y-off + item.y
      widget:HIDDEN = No.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-stack) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE stack Method-Library 
PROCEDURE stack :
/*------------------------------------------------------------------------------
  Purpose:     Stack items into a stack group
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER items AS CHAR NO-UNDO.
  DEF INPUT PARAMETER grp   AS CHAR NO-UNDO.
  DEF INPUT PARAMETER align AS CHAR NO-UNDO.
  DEF INPUT PARAMETER bound AS LOGI NO-UNDO.

  RUN group-widgets( "S", items, grp, align, bound ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

