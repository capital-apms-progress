&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r11
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEFINE NEW SHARED STREAM dir.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .08
         WIDTH              = 39.
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

PROCEDURE sndPlaySound EXTERNAL "mmsystem.dll":
  DEF INPUT  PARAMETER ic  AS CHAR NO-UNDO.
  DEF INPUT  PARAMETER ish AS SHORT NO-UNDO.
  DEF RETURN PARAMETER osh AS SHORT NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-find-sibling) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE find-sibling Method-Library 
PROCEDURE find-sibling :
/*------------------------------------------------------------------------------
  Purpose:     Recursively descend the widget hierarchy for a given
               widget and find a sibling whose type or name is the
               same as that given.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT  PARAMETER widge   AS HANDLE NO-UNDO.
  DEF INPUT  PARAMETER name    AS CHAR NO-UNDO.
  DEF OUTPUT PARAMETER sibling AS HANDLE NO-UNDO.

  IF widge:name = name OR widge:type = name THEN
  DO:
    sibling = widge.
    RETURN.
  END.

  IF CAN-QUERY( widge, "first-child":U) AND
     VALID-HANDLE( widge:first-child ) THEN
    RUN find-sibling( INPUT widge:first-child,
                      INPUT name,
                      OUTPUT sibling ).
                      
  IF VALID-HANDLE( sibling ) THEN RETURN.
    
  IF CAN-QUERY( widge, "next-sibling":U) AND 
      VALID-HANDLE( widge:next-sibling) THEN
    RUN find-sibling( INPUT widge:next-sibling,
                      INPUT name,
                      OUTPUT sibling ).


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-child) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-child Method-Library 
PROCEDURE get-child :
/*------------------------------------------------------------------------------
  Purpose:     Get the first child of the given procedure that
               are of the specified type.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER proc AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER type AS CHAR NO-UNDO.
  
  DEF VAR ch           AS CHAR   NO-UNDO.
  DEF VAR child        AS HANDLE NO-UNDO.
  DEF VAR child-name   AS CHAR   NO-UNDO.
  DEF VAR child-type   AS CHAR   NO-UNDO.
  DEF VAR i            AS INT  INIT 1 NO-UNDO.
  
  RUN get-link-handle IN adm-broker-hdl ( INPUT proc,
                                          INPUT 'Container-Target':U,
                                          OUTPUT ch ).
  DO WHILE i <= NUM-ENTRIES(ch):

    child = WIDGET-HANDLE( ENTRY( i , ch ) ).
    IF VALID-HANDLE(child) THEN DO:
      child-name = child:file-name.    
      RUN get-attribute IN child ( 'TYPE':U ).
      child-type = RETURN-VALUE.
      IF type = child-type OR INDEX( child-name, type ) <> 0 THEN RETURN ENTRY( i, ch).
    END.
    i = i + 1.
  
  END.
  
  RETURN "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PlaySound) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PlaySound Method-Library 
PROCEDURE PlaySound :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER wave-name AS CHAR NO-UNDO.
  DEF VAR play-status AS INT NO-UNDO.
  IF SEARCH( wave-name ) <> ? THEN
    RUN sndPlaySound ( wave-name, 2, OUTPUT play-status ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-verify-prog) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-prog Method-Library 
PROCEDURE verify-prog :
/*------------------------------------------------------------------------------
  Purpose:     Verify that the given program name is present in
               the propath. The given name can include or exclude
               the '.w' or '.r' suffix.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT  PARAMETER in_name  AS CHAR NO-UNDO.
  DEF OUTPUT PARAMETER out_name AS CHAR NO-UNDO.

  out_name =
    IF SEARCH( in_name )        <> ? THEN in_name        ELSE
    IF SEARCH( in_name + ".r" ) <> ? THEN in_name + ".r" ELSE
    IF SEARCH( in_name + ".w" ) <> ? THEN in_name + ".w" ELSE
    IF SEARCH( in_name + ".p" ) <> ? THEN in_name + ".p" ELSE "".
  
  IF out_name = "" THEN DO:
    MESSAGE "Program [" in_name "] not found." 
      VIEW-AS ALERT-BOX WARNING TITLE "Error: " + THIS-PROCEDURE:FILE-NAME.
    RETURN "FAIL".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

