&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/******************* N O T E !!!! ******************

The custom trigger code below for executing a local
event procedure when the value of a browse changes
will only work properly if this method library is
included at the start of the main block section of
the including program! ( or you delete the standard 
VALUE-CHANGED trigger for the browse ).
This is so the below trigger will execute after any 
other triggers that are defined

****************************************************/

&IF DEFINED (BROWSE-NAME) NE 0 AND "{&PROCEDURE-TYPE}" EQ "SmartBrowser" &THEN

/* The ADM code does not raise any adm events in the current procedure
   for a value-change of a browse.
   This trigger will catch all value-changed events for the current browse
   and dispatch drl-value-changed. */

ON "VALUE-CHANGED":U OF {&BROWSE-NAME}
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
  RUN drl-value-changed.
END.

&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .1
         WIDTH              = 43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-mstvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-drl-value-changed) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE drl-value-changed Method-Library 
PROCEDURE drl-value-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LOOKUP( 'inst-value-changed', THIS-PROCEDURE:INTERNAL-ENTRIES ) <> 0 THEN
    RUN inst-value-changed.
  ELSE
  DO:

    &IF DEFINED (FIRST-TABLE-IN-QUERY-{&BROWSE-NAME}) NE 0 &THEN

    RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, "Maintain,delete-record", "SENSITIVE = " +
      IF AVAILABLE {&FIRST-TABLE-IN-QUERY-{&BROWSE-NAME}} THEN "Yes" ELSE "No" ).

    &ENDIF
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-open-query) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query Method-Library 
PROCEDURE local-open-query :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  RUN pre-open-query IN THIS-PROCEDURE NO-ERROR.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query':U ) .
  RUN inst-open-query IN THIS-PROCEDURE NO-ERROR.
  RUN drl-value-changed.

  /* Code placed here will execute AFTER standard behavior.    */
  RUN post-open-query IN THIS-PROCEDURE NO-ERROR.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-row-changed) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-changed Method-Library 
PROCEDURE local-row-changed :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-changed':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  &IF DEFINED(BROWSE-NAME) NE 0 AND "{&PROCEDURE-TYPE}" = "SmartBrowser" &THEN

    /* If there is custom color code in the row-display trigger
       for the browser, then this will call it */

    IF AVAILABLE {&FIRST-TABLE-IN-QUERY-{&BROWSE-NAME}} THEN
      IF BROWSE {&BROWSE-NAME}:REFRESH() THEN.
    RUN drl-value-changed.

  &ENDIF
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-sub1-create-objects) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sub1-create-objects Method-Library 
PROCEDURE sub1-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Master create objects for all drill viewers
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN sub2-create-objects IN THIS-PROCEDURE NO-ERROR.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-sub1-display-fields) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sub1-display-fields Method-Library 
PROCEDURE sub1-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Master display fields for Drill viewers
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN sub2-display-fields IN THIS-PROCEDURE NO-ERROR.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-sub1-initialize) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sub1-initialize Method-Library 
PROCEDURE sub1-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN sub2-initialize IN THIS-PROCEDURE NO-ERROR.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

