&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : inc/method/m-excel.i
    Purpose     : functions/procedures to help drive excel 95/97

    Author(s)   : Andrew McMillan
    Created     : 18/7/98
    Notes       : Based on "dlc/src/samples/activex/excelgraphs/oleauto.p"
  ------------------------------------------------------------------------*/

DEFINE VARIABLE chExcelApplication      AS COM-HANDLE.
DEFINE VARIABLE chWorkbook              AS COM-HANDLE.
DEFINE VARIABLE chWorksheet             AS COM-HANDLE.
DEFINE VARIABLE chChart                 AS COM-HANDLE.
DEFINE VARIABLE chWorksheetRange        AS COM-HANDLE.

&GLOB xlAlignLeft 2
&GLOB xlAlignCenter 3
&GLOB xlAlignRight 4

/* Only empirically discovered xlPageBreakManual so far */
&GLOB xlPageBreakAutomatic 0
&GLOB xlPageBreakManual 1
&GLOB xlPageBreakNone -1

/* Only empirically discovered xlLandscape so far */
&GLOB xlPortrait 1
&GLOB xlLandscape 2

/* Only empirically discovered xlEdgeTop so far */
&GLOB xlEdgeTop 3
&GLOB xlEdgeBottom 4
&GLOB xlEdgeLeft 1
&GLOB xlEdgeRight 2

/* Only empirically discovered xlContinuous so far */
&GLOB xlContinuous 1
&GLOB xlDash 2
&GLOB xlDashDot 3
&GLOB xlDashDotDot 4
&GLOB xlDot 5
&GLOB xlDouble 6

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-create-workbook) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD create-workbook Method-Library 
FUNCTION create-workbook RETURNS COM-HANDLE
  ( /* no parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-excel-date) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD excel-date Method-Library 
FUNCTION excel-date RETURNS DECIMAL
  ( INPUT dt AS DATE, INPUT secs AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-range) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-range Method-Library 
FUNCTION get-range RETURNS COM-HANDLE
  ( INPUT range-spec AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-int-to-column) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD int-to-column Method-Library 
FUNCTION int-to-column RETURNS CHARACTER
  ( INPUT column-no AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-int-to-range) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD int-to-range Method-Library 
FUNCTION int-to-range RETURNS CHARACTER
  ( INPUT x1 AS INT, INPUT y1 AS INT, INPUT x2 AS INT, INPUT y2 AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-release-excel) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD release-excel Method-Library 
FUNCTION release-excel RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-select-sheet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD select-sheet Method-Library 
FUNCTION select-sheet RETURNS COM-HANDLE
  ( INPUT sheet-number AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-auto-calculate) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-auto-calculate Method-Library 
FUNCTION set-auto-calculate RETURNS INT
  ( INPUT on-off AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-bold) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-bold Method-Library 
FUNCTION set-bold RETURNS CHARACTER
  ( INPUT range-def AS CHAR, INPUT on-off AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-column-width) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-column-width Method-Library 
FUNCTION set-column-width RETURNS COM-HANDLE
  ( INPUT column-ref AS CHAR, INPUT col-width AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-excel-visible) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-excel-visible Method-Library 
FUNCTION set-excel-visible RETURNS COM-HANDLE
  ( INPUT visible-flag AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-fast-mode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-fast-mode Method-Library 
FUNCTION set-fast-mode RETURNS CHARACTER
  ( INPUT on-off AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-interactive) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-interactive Method-Library 
FUNCTION set-interactive RETURNS LOGICAL
  ( INPUT on-off AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-value) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-value Method-Library 
FUNCTION set-value RETURNS COM-HANDLE
  ( INPUT x AS INT, INPUT y AS INT, INPUT cell-value AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-start-excel) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD start-excel Method-Library 
FUNCTION start-excel RETURNS COM-HANDLE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .75
         WIDTH              = 38.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-create-workbook) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION create-workbook Method-Library 
FUNCTION create-workbook RETURNS COM-HANDLE
  ( /* no parameter-definitions */ ) :

  /* create a new Workbook */
  chWorkbook = chExcelApplication:Workbooks:Add().

  RETURN chWorkBook.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-excel-date) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION excel-date Method-Library 
FUNCTION excel-date RETURNS DECIMAL
  ( INPUT dt AS DATE, INPUT secs AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert a date/time into excel format (decimal days since 1/1/1900).
    Notes:  
------------------------------------------------------------------------------*/

  RETURN ((dt - DATE( 1, 1, 1900)) + 2.0 ) + (DECIMAL(secs) / 86400.0) .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-range) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-range Method-Library 
FUNCTION get-range RETURNS COM-HANDLE
  ( INPUT range-spec AS CHAR ) :

  RETURN chWorkSheet:Range( range-spec ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-int-to-column) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION int-to-column Method-Library 
FUNCTION int-to-column RETURNS CHARACTER
  ( INPUT column-no AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR a AS INT NO-UNDO.

  a = ASC("A").
  column-no = column-no - 1.
  RETURN (IF column-no > 25 THEN CHR( (a - 1) + INT(TRUNCATE( column-no / 26, 0)))
                            ELSE "")
         + CHR( a + (column-no MOD 26)) .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-int-to-range) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION int-to-range Method-Library 
FUNCTION int-to-range RETURNS CHARACTER
  ( INPUT x1 AS INT, INPUT y1 AS INT, INPUT x2 AS INT, INPUT y2 AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR range-spec AS CHAR NO-UNDO.

  range-spec = int-to-column( x1 ) + STRING( y1 ).

  IF x1 <> x2 OR y1 <> y2 THEN
    range-spec = range-spec + ":" + int-to-column(x2) + STRING( y2 ) .

  RETURN range-spec.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-release-excel) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION release-excel Method-Library 
FUNCTION release-excel RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Make sure we close excel when we've finished!
    Notes:  
------------------------------------------------------------------------------*/

  RELEASE OBJECT chExcelApplication NO-ERROR.
  RELEASE OBJECT chWorkbook NO-ERROR.
  RELEASE OBJECT chWorksheet NO-ERROR.
  RELEASE OBJECT chChart NO-ERROR.
  RELEASE OBJECT chWorksheetRange NO-ERROR.

  RETURN "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-select-sheet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION select-sheet Method-Library 
FUNCTION select-sheet RETURNS COM-HANDLE
  ( INPUT sheet-number AS INT ) :

  /* get the active Worksheet */
  chWorkSheet = chExcelApplication:Sheets:Item(sheet-number).

  RETURN chWorkSheet .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-auto-calculate) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-auto-calculate Method-Library 
FUNCTION set-auto-calculate RETURNS INT
  ( INPUT on-off AS LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
/* 1 = Automatic
 * 2 = Automatic except tables
 * 3 = Manual
 */
DEF VAR old-calc AS INT NO-UNDO.

  old-calc = chExcelApplication:Calculation .
  chExcelApplication:Calculation = (IF on-off THEN 1 ELSE 3 ).
  RETURN old-calc.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-bold) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-bold Method-Library 
FUNCTION set-bold RETURNS CHARACTER
  ( INPUT range-def AS CHAR, INPUT on-off AS LOGICAL ) :

  chWorkSheet:Range( range-def ):Font:Bold = on-off.

  RETURN range-def.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-column-width) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-column-width Method-Library 
FUNCTION set-column-width RETURNS COM-HANDLE
  ( INPUT column-ref AS CHAR, INPUT col-width AS DEC ) :

  chWorkSheet:Columns( column-ref ):ColumnWidth = col-width .
  RETURN chWorkSheet:Columns( column-ref ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-excel-visible) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-excel-visible Method-Library 
FUNCTION set-excel-visible RETURNS COM-HANDLE
  ( INPUT visible-flag AS LOGICAL ) :

  /* launch Excel so it is visible to the user */
  chExcelApplication:Visible = visible-flag.

  RETURN chExcelApplication.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-fast-mode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-fast-mode Method-Library 
FUNCTION set-fast-mode RETURNS CHARACTER
  ( INPUT on-off AS LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  chExcelApplication:Interactive = NOT(on-off).
  chExcelApplication:DisplayAlerts = NOT(on-off).
  chExcelApplication:ScreenUpdating = NOT(on-off).

  /* There needs to be a worksheet available before the calculate on/off works */
  IF VALID-HANDLE(chWorkSheet) THEN
    chExcelApplication:Calculation = (IF on-off THEN 3 ELSE 1 ).

  RETURN "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-interactive) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-interactive Method-Library 
FUNCTION set-interactive RETURNS LOGICAL
  ( INPUT on-off AS LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
/* Yes  = Interactive
 * No   = Non-interactive
 */
DEF VAR old-interactive AS LOGI NO-UNDO.

  old-interactive = chExcelApplication:Interactive.
  chExcelApplication:Interactive = on-off.
  RETURN old-interactive.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-value) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-value Method-Library 
FUNCTION set-value RETURNS COM-HANDLE
  ( INPUT x AS INT, INPUT y AS INT, INPUT cell-value AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Set the value in a particular spreadsheet cell
------------------------------------------------------------------------------*/
DEF VAR cRange AS CHAR NO-UNDO.

  cRange = int-to-column(x) + STRING(y).
  chWorkSheet:Range( cRange ):Value = cell-value.
  RETURN chWorkSheet:Range( cRange ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-start-excel) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION start-excel Method-Library 
FUNCTION start-excel RETURNS COM-HANDLE
  ( /* parameter-definitions */ ) :

  /* create a new Excel Application object */
  CREATE "Excel.Application" chExcelApplication.

  RETURN chExcelApplication .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

