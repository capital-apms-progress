&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : inc/method/m-wizvwr.i
    Purpose     : Include for viewers which are destined to be components of
                  a wizard process.

    Description : Doesn't do a lot, except spoof the function calls to routines
                  in the container window.

    Author(s)   : Andrew McMillan
    Created     : 1/11/97
    Notes       :
  ------------------------------------------------------------------------*/

DEF VAR wizard-hdl AS HANDLE NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-add-step-link) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD add-step-link Method-Library 
FUNCTION add-step-link RETURNS LOGICAL
  ( INPUT step-from AS CHAR, INPUT step-to   AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-can-finish) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD can-finish Method-Library 
FUNCTION can-finish RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-clear-results) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clear-results Method-Library 
FUNCTION clear-results RETURNS LOGICAL
  ( /* no parameters */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-del-step-link) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD del-step-link Method-Library 
FUNCTION del-step-link RETURNS LOGICAL
  ( INPUT delete-step AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-result) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-result Method-Library 
FUNCTION get-result RETURNS CHARACTER
  ( INPUT result-name AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-result) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-result Method-Library 
FUNCTION set-result RETURNS LOGICAL
  ( INPUT result-name AS CHAR, INPUT result-value AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-validated) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-validated Method-Library 
FUNCTION set-validated RETURNS LOGICAL
  ( INPUT valid AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .15
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

FUNCTION add-step-link RETURNS LOGICAL
    ( INPUT step-from AS CHAR, INPUT step-to AS CHAR )
    MAP TO add-step-link IN wizard-hdl.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-use-Wizard-Handle) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Wizard-Handle Method-Library 
PROCEDURE use-Wizard-Handle :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-handle AS CHAR NO-UNDO.
/*
  IF LOOKUP( "override-use-Wizard-Handle", THIS-PROCEDURE:INTERNAL-ENTRIES ) > 0 THEN
    RUN override-use-Wizard-Handle( new-handle ) NO-ERROR.
  ELSE
*/
    wizard-hdl = WIDGET-HANDLE(new-handle).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-add-step-link) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION add-step-link Method-Library 
FUNCTION add-step-link RETURNS LOGICAL
  ( INPUT step-from AS CHAR, INPUT step-to   AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  RETURN DYNAMIC-FUNCTION( "add-step-link" IN wizard-hdl, step-from, step-to ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-can-finish) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION can-finish Method-Library 
FUNCTION can-finish RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  RETURN DYNAMIC-FUNCTION( "can-finish" IN wizard-hdl ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-clear-results) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clear-results Method-Library 
FUNCTION clear-results RETURNS LOGICAL
  ( /* no parameters */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  RETURN DYNAMIC-FUNCTION( "clear-results" IN wizard-hdl ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-del-step-link) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION del-step-link Method-Library 
FUNCTION del-step-link RETURNS LOGICAL
  ( INPUT delete-step AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  RETURN DYNAMIC-FUNCTION( "del-step-link" IN wizard-hdl, delete-step ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-result) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-result Method-Library 
FUNCTION get-result RETURNS CHARACTER
  ( INPUT result-name AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
/*  MESSAGE "Getting:" SKIP result-name "=" DYNAMIC-FUNCTION( "get-result" IN wizard-hdl, result-name ).*/
&IF DEFINED(USE-DEBUG) &THEN
DEF VAR answer AS CHAR NO-UNDO.
  answer = DYNAMIC-FUNCTION( "get-result" IN wizard-hdl, result-name ).
  IF debug THEN MESSAGE "Got" result-name "=" answer "=".
  RETURN answer.
&ELSE
  RETURN DYNAMIC-FUNCTION( "get-result" IN wizard-hdl, result-name ).
&ENDIF
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-result) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-result Method-Library 
FUNCTION set-result RETURNS LOGICAL
  ( INPUT result-name AS CHAR, INPUT result-value AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
/*  MESSAGE "Setting:" SKIP result-name "=" result-value.*/
&IF DEFINED(USE-DEBUG) &THEN
DEF VAR answer AS LOGI NO-UNDO.
  answer = DYNAMIC-FUNCTION( "set-result" IN wizard-hdl, result-name, result-value ).
  IF debug THEN MESSAGE "Put" result-name "=" result-value "=".
  RETURN answer.
&ELSE
  RETURN DYNAMIC-FUNCTION( "set-result" IN wizard-hdl, result-name, result-value ).
&ENDIF
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-validated) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-validated Method-Library 
FUNCTION set-validated RETURNS LOGICAL
  ( INPUT valid AS LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  Set the current step as validated
------------------------------------------------------------------------------*/
  RETURN DYNAMIC-FUNCTION( "set-validated" IN wizard-hdl, ?, valid ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

