&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : inc/method/m-bqmgr.i
    Purpose     : Get the handle to the batch queue manager into a local variable

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

&GLOBAL-DEFINE BQ-MGR 1

DEF VAR bq-mgr AS HANDLE NO-UNDO.
RUN get-bq-routine.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = 13.35
         WIDTH              = 40.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-get-bq-routine) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-bq-routine Method-Library 
PROCEDURE get-bq-routine :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR wh AS HANDLE NO-UNDO.

  bq-mgr = ?.

  IF SESSION:BATCH-MODE THEN DO:
    ASSIGN bq-mgr = WIDGET-HANDLE(SESSION:CONTEXT-HELP-FILE) NO-ERROR.
    IF VALID-HANDLE(bq-mgr) THEN LEAVE.
  END.
  
  wh = SESSION:FIRST-CHILD.
  
  IF VALID-HANDLE(wh) AND ENTRY( 1, wh:PRIVATE-DATA) = "apms-queue-manager" THEN DO:
    bq-mgr = WIDGET-HANDLE( ENTRY( 2, wh:PRIVATE-DATA) ).
    LEAVE.
  END.

  wh = SESSION:FIRST-PROCEDURE.
  IF NOT VALID-HANDLE(wh) THEN DO:
    wh = THIS-PROCEDURE.
  END.

  DO WHILE VALID-HANDLE(wh):
    IF LOOKUP( 'apms-queue-manager', wh:INTERNAL-ENTRIES ) > 0 THEN DO:
      bq-mgr = wh.
      LEAVE.
    END.
    wh = wh:NEXT-SIBLING.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

