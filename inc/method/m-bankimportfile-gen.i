&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*------------------------------------------------------------------------
    Library     : 1st tier Bank Import File Generation Method Library
    Purpose     : For Direct Payments
                  Generates a file suitable for import by online banking
                  by including bank specific procedures and calling them appropriately.
    Description : Generic library which calls an appropriate bank specific library
    Author(s)   : William Stokes
    Created     : 13/04/2004
    Notes       : 
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEFINE NEW GLOBAL SHARED STREAM s-bi-output.
DEFINE NEW GLOBAL SHARED STREAM s-bi-err.

/* Import File Generation */
DEFINE VARIABLE v-bankimport-filename AS CHARACTER INITIAL '' NO-UNDO. 
DEFINE VARIABLE v-bankimport-error-filename AS CHARACTER INITIAL '' NO-UNDO.
DEFINE VARIABLE v-file-name AS CHARACTER INITIAL '' NO-UNDO.
DEFINE VARIABLE v-file-extn AS CHARACTER INITIAL '.txt' NO-UNDO.
DEFINE VARIABLE v-bankimport-payer-payername AS CHARACTER FORMAT "X(20)" INITIAL '' NO-UNDO.
/* Not currently used but field specified */
DEFINE VARIABLE v-bankimport-payer-custnum AS INTEGER FORMAT "999999" INITIAL 0 NO-UNDO.

/* Miscellaneous Variables */
DEFINE VARIABLE v-bankimport-initialized AS LOGICAL INITIAL FALSE NO-UNDO.
DEFINE VARIABLE v-bankimport-outputlines AS INTEGER INITIAL 0 NO-UNDO.
DEFINE VARIABLE v-bankimport-errors AS INTEGER INITIAL 0 NO-UNDO.

/* Set in initialise only if the country is australia */
DEFINE VARIABLE v-de-bankcode AS CHARACTER NO-UNDO.
DEFINE VARIABLE v-de-username AS CHARACTER NO-UNDO.
DEFINE VARIABLE v-de-userid AS CHARACTER NO-UNDO.

/* Include WestpacTrust DeskBank routines */
{inc/method/m-deskbank.i}
{inc/method/m-ababank.i}

DEF TEMP-TABLE BankImportError NO-UNDO
    FIELD ChequeNo AS INTEGER
    FIELD ErrorMessage AS CHARACTER
    INDEX XPKBankImportErrors IS PRIMARY ChequeNo ASCENDING .

{inc/ofc-this.i}
{inc/ofc-set-l.i "BankFileRefIsPayer" "v-bankimport-payer-is-reference"}

/* Used to determine if the account number is in AUS format */
{inc/ofc-set.i "Country" "v-bank-country"}
IF NOT AVAILABLE OfficeSetting THEN
    v-bank-country = 'New Zealand'.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = 14.9
         WIDTH              = 76.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-bankimport-finish) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE bankimport-finish Method-Library 
PROCEDURE bankimport-finish :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

/* Call appropriate 2nd tier routine */
CASE v-bank-country:
    WHEN 'New Zealand' THEN DO:
        RUN deskbank-finish.
    END.
    WHEN 'Australia' THEN DO:
        RUN aba-balancing-entry.
        RUN aba-finish.
    END.
END CASE.

FOR EACH BankImportError NO-LOCK BY BankImportError.ChequeNo:
    IF BankImportError.ChequeNo = 0 THEN DO:
        PUT STREAM s-bi-err UNFORMATTED
            BankImportError.ErrorMessage + "~n".
        v-bankimport-errors = v-bankimport-errors + 1.
    END.
    ELSE DO:
        PUT STREAM s-bi-err UNFORMATTED
            'Cheque: ' + STRING(BankImportError.ChequeNo, "999999") + ' Not Printed: ' + BankImportError.ErrorMessage + "~n".
        v-bankimport-errors = v-bankimport-errors + 1.
    END.

END.

OUTPUT STREAM s-bi-output CLOSE.
OUTPUT STREAM s-bi-err CLOSE.

/* Output File cleanup ... */
IF v-bankimport-outputlines = 0 THEN
    OS-DELETE VALUE(v-bankimport-filename).

IF v-bankimport-errors = 0 THEN DO:
    OS-DELETE VALUE(v-bankimport-error-filename).
END.

/* MESSAGE User via ALERT-BOX to direct them to error file */
IF v-bankimport-errors > 0 THEN DO:
    MESSAGE "Errors occured during generation of Bank Import File.  View file:~n"
         + v-bankimport-error-filename + "~n for details."
        VIEW-AS ALERT-BOX INFORMATION TITLE 'Errors During Bank Import File Generation'.
END.
ELSE IF v-bankimport-outputlines > 0 THEN DO:
    MESSAGE "Bank Import File generation to:~n" + v-file-name + "~ncomplete."
        VIEW-AS ALERT-BOX INFORMATION TITLE 'Bank Import File Generated'.
END.

RETURN 'OK'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-bankimport-initialize) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE bankimport-initialize Method-Library 
PROCEDURE bankimport-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  INPUT bank-account-number CHARACTER
               INPUT output-file CHARACTER
  Notes:       
------------------------------------------------------------------------------*/

DEFINE INPUT PARAMETER p-output-directory AS CHARACTER INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payer-bankaccount-rid AS ROWID NO-UNDO.

/* Find Bank Account Record */
FIND BankAccount WHERE ROWID(BankAccount) = p-payer-bankaccount-rid NO-LOCK.

/* Ensure \ is replaced with / for compatibility */
p-output-directory = REPLACE ( p-output-directory , "\" , "/" ).

IF NOT( LENGTH(p-output-directory) > 0) THEN DO:
/* Check v-directory is specified and has \ on the end */
/* IF R-INDEX(p-output-directory, "/") = ? OR  R-INDEX(p-output-directory, "/") = 0 THEN DO: */
  p-output-directory = SESSION:TEMP-DIRECTORY.
END.

/* Ensure the directory ends with a / */
p-output-directory = RIGHT-TRIM( p-output-directory, "/").
p-output-directory = p-output-directory + "/".

FILE-INFO:FILE-NAME = p-output-directory.
/*
MESSAGE "Bank Payments File Is Being Generated In:~n" + FILE-INFO:FULL-PATHNAME
    VIEW-AS ALERT-BOX INFORMATION TITLE "Bank Payments File Location".
*/

/* Name Files appropriately */
v-file-name = BankAccount.BankAccount + '-Payments-' +
    STRING( SUBSTRING( STRING( YEAR(TODAY) ), 3, 2), "99" ) + '-' +
    STRING( MONTH(TODAY), "99" ) + '-' +
    STRING( DAY(TODAY), "99" ) + '-' +
    REPLACE( STRING( TIME, "HH:MM:SS" ), ':', '').

v-bankimport-filename = p-output-directory + v-file-name + v-file-extn.
v-bankimport-error-filename = p-output-directory + v-file-name + '-errors' + v-file-extn.

OUTPUT STREAM s-bi-output TO VALUE ( v-bankimport-filename ) NO-CONVERT.
OUTPUT STREAM s-bi-err TO VALUE ( v-bankimport-error-filename ) NO-CONVERT.

MESSAGE "Bank Payments file is:~n   " + v-bankimport-filename
    VIEW-AS ALERT-BOX INFORMATION TITLE "Bank Payments File Location".

FILE-INFO:FILE-NAME = v-bankimport-filename.

IF FILE-INFO:FILE-TYPE <> 'FRW' THEN DO:
    MESSAGE "Failed Writing Bank Payments File" VIEW-AS ALERT-BOX ERROR TITLE "Error Outputting File".
    RETURN "FAIL".
END.

/* Find the direct credit details for the ABA file */
IF v-bank-country = "Australia" THEN DO:
    FIND FIRST DirectEntryDetail WHERE DirectEntryDetail.DEUserId = BankAccount.DEUserId.
    IF AVAILABLE DirectEntryDetail THEN
        ASSIGN
            v-de-username = DirectEntryDetail.DEUserName
            v-de-userid   = DirectEntryDetail.DEUserId
            v-de-bankcode = DirectEntryDetail.DEInstitution.
    ELSE DO:
        MESSAGE "Could not find a DC detail record for the chosen bank account"
            VIEW-AS ALERT-BOX ERROR TITLE "Error finding details".
        RETURN "FAIL".
    END.
END.

v-bankimport-payer-payername = BankAccount.AccountName.

/* Call appropriate initialise routine from 2nd tier */
CASE v-bank-country:
    WHEN 'Australia' THEN DO:
        RUN aba-initialize (
            v-bankimport-filename,
            v-bankimport-error-filename,
            BankAccount.BankAccount,
            v-bankimport-payer-payername,
            v-bankimport-payer-custnum,
            v-de-bankcode,
            v-de-username,
            v-de-userid
       ).
       IF RETURN-VALUE <> 'OK' THEN DO:
           CREATE BankImportError.
           ASSIGN BankImportError.ChequeNo = 0
                  BankImportError.ErrorMessage = RETURN-VALUE.
           RETURN 'FAIL'.
       END.
    END.
    WHEN 'New Zealand' THEN DO:
        RUN deskbank-initialize (
            v-bankimport-filename,
            v-bankimport-error-filename,
            BankAccount.BankAccount,
            v-bankimport-payer-payername,
            v-bankimport-payer-custnum
       ).
       IF RETURN-VALUE <> 'OK' THEN DO:
           CREATE BankImportError.
           ASSIGN BankImportError.ChequeNo = 0
                  BankImportError.ErrorMessage = RETURN-VALUE.
           RETURN 'FAIL'.
       END.
    END.
    OTHERWISE DO:
        CREATE BankImportError.
        ASSIGN BankImportError.ChequeNo = 0
               BankImportError.ErrorMessage = "Unknown country when determining output style - see Country in office settings".
        RETURN 'FAIL'.
    END.
END CASE.
IF RETURN-VALUE = 'OK' THEN DO:
    v-bankimport-initialized = TRUE.
    RETURN 'OK'.
END.
ELSE DO:
    RETURN 'FAIL'.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-bankimport-payment-detailline) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE bankimport-payment-detailline Method-Library 
PROCEDURE bankimport-payment-detailline :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: FORMAT Phrase in Define statements is for refrence only.
         If you wish to alter output line format then alter STRING(..., FORMAT Phrase) too.
------------------------------------------------------------------------------*/

DEFINE INPUT PARAMETER p-cheque-rid AS ROWID NO-UNDO.
DEFINE INPUT PARAMETER p-creditor-rid AS ROWID NO-UNDO.
DEFINE INPUT PARAMETER p-voucher-rid AS ROWID NO-UNDO.

DEF VAR their-reference-1 AS CHAR NO-UNDO.
DEF VAR their-reference-2 AS CHAR NO-UNDO.
DEF VAR their-reference-3 AS CHAR NO-UNDO.

FIND Cheque WHERE ROWID(Cheque) = p-cheque-rid NO-LOCK.
FIND Creditor WHERE ROWID(Creditor) = p-creditor-rid NO-LOCK NO-ERROR.
FIND Voucher WHERE ROWID(Voucher) = p-voucher-rid NO-LOCK NO-ERROR.

IF NOT AVAILABLE Creditor OR NOT AVAILABLE Voucher THEN DO:
    CREATE BankImportError.
    ASSIGN BankImportError.ChequeNo = Cheque.ChequeNo
           BankImportError.ErrorMessage = 'Cannot find required records of Cheque to output Bank Import Line for ChequeNo ' + STRING(Cheque.ChequeNo, "999999").
    RETURN 'FAIL'.
END.

/* CHECK Payee/Creditor is DirectPayment Enabled */
IF NOT Creditor.EnableDirectPayment THEN DO:
    CREATE BankImportError.
    ASSIGN BankImportError.ChequeNo = Cheque.ChequeNo
           BankImportError.ErrorMessage = 'Creditor ' + STRING(Creditor.CreditorCode) + ' is not DirectPayment enabled '.
    RETURN 'FAIL'.
END.

IF Voucher.PaymentStyle <> 'DC' THEN DO:
    CREATE BankImportError.
    ASSIGN BankImportError.ChequeNo = Cheque.ChequeNo
           BankImportError.ErrorMessage = 'Voucher Payment Style Not Direct Debit'.
    RETURN 'FAIL'.
END.

their-reference-1 = Voucher.Description.
their-reference-2 = Voucher.OurOrderNo.
their-reference-3 = Voucher.InvoiceReference.

IF v-bankimport-payer-is-reference THEN DO:
    their-reference-1 = STRING(v-bankimport-payer-payername,'X(12)').
    IF LENGTH(v-bankimport-payer-payername) > 12 THEN DO:
      their-reference-2 = STRING(SUBSTR(v-bankimport-payer-payername,13,12),'X(12)').
    END.
END.

IF Creditor.DcStatementText <> ? AND Creditor.DcStatementText <> "" THEN DO:
  their-reference-3 = STRING(Creditor.DcStatementText,'X(12)').
END.

/* Call appropriate 2nd tier routine */
CASE v-bank-country:
    WHEN 'Australia' THEN DO:
        RUN aba-payment-detailline(
            Cheque.ChequeNo,
            Creditor.BankDetails,
            Creditor.PayeeName,
            their-reference-1,
            their-reference-2,
            their-reference-3,
            Cheque.Amount
        ).
    END.
    WHEN 'New Zealand' THEN DO:
        RUN deskbank-payment-detailline(
            Cheque.ChequeNo,
            Creditor.BankDetails,
            Creditor.PayeeName,
            their-reference-1,
            their-reference-2,
            their-reference-3,
            Cheque.Amount
        ).
    END.
END CASE.
IF RETURN-VALUE <> 'OK' THEN DO:
    CREATE BankImportError.
    ASSIGN BankImportError.ChequeNo = Cheque.ChequeNo
           BankImportError.ErrorMessage = RETURN-VALUE.
    RETURN 'FAIL'.
END.

v-bankimport-outputlines = v-bankimport-outputlines + 1.
RETURN 'OK'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-bankimport-payment-headerline) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE bankimport-payment-headerline Method-Library 
PROCEDURE bankimport-payment-headerline :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEFINE INPUT PARAMETER p-sequence AS INTEGER FORMAT "999999" INITIAL 0 NO-UNDO.
DEFINE INPUT PARAMETER p-description AS CHARACTER FORMAT "X(20)" INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-due-date AS DATE INITIAL '' NO-UNDO.

/* Call appropriate 2nd tier routine */
CASE v-bank-country:
    WHEN 'Australia' THEN DO:
        RUN aba-payment-headerline(
            p-sequence,
            p-description,
            p-due-date
         ).
    END.
    WHEN 'New Zealand' THEN DO:
        RUN deskbank-payment-headerline(
            p-sequence,
            p-description,
            p-due-date
         ).
    END.
END CASE.
IF RETURN-VALUE <> 'OK' THEN DO:
    CREATE BankImportError.
    ASSIGN BankImportError.ChequeNo = 0
           BankImportError.ErrorMessage = RETURN-VALUE.
    RETURN 'FAIL'.
END.

v-bankimport-outputlines = v-bankimport-outputlines + 1.
RETURN 'OK'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-bankbranchaccountsuffix) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-bankbranchaccountsuffix Method-Library 
PROCEDURE get-bankbranchaccountsuffix :
/*------------------------------------------------------------------------------
  Purpose: To break out bank branch account and suffix segments of a bank account number
  Parameters: INPUT BankAccount Number CHARACTER
              OUTPUT bank    INTEGER
              OUTPUT branch  INTEGER
              OUTPUT account INTEGER
              OUTPUT suffix  INTEGER
  RETURN-VALUE: 'OK' if bank branch account and suffix were all split out
   and converted to INTEGER OK and splitting managed to get a suffix
   and didn't end up with too many segments, otherwise 'FAIL'.
  Notes: If p-bank-account has no hyphens it will be formatted like "99-9999-9999999-99"
         Coincidentally this is the default format string for the BankAccount.BankAccount field.
------------------------------------------------------------------------------*/

DEFINE INPUT PARAMETER p-bank-account AS CHARACTER INITIAL '' NO-UNDO.
DEFINE OUTPUT PARAMETER op-bank AS INTEGER INITIAL 0 NO-UNDO.
DEFINE OUTPUT PARAMETER op-branch AS INTEGER INITIAL 0 NO-UNDO.
DEFINE OUTPUT PARAMETER op-account AS INTEGER INITIAL 0 NO-UNDO.
DEFINE OUTPUT PARAMETER op-suffix AS INTEGER INITIAL 0 NO-UNDO.

DEFINE VARIABLE v-start-strpos AS INTEGER INITIAL 1 NO-UNDO.
DEFINE VARIABLE v-end-strpos AS INTEGER INITIAL 1 NO-UNDO.
DEFINE VARIABLE v-secpos AS INTEGER INITIAL 1 NO-UNDO.
DEFINE VARIABLE v-split2suffix AS LOGICAL INITIAL FALSE NO-UNDO.

p-bank-account = TRIM(p-bank-account).
/* Do we have a Bank Account Number With spaces instead of hyphens? */
IF INDEX( p-bank-account, ' ', v-start-strpos ) > 0 THEN DO:
  p-bank-account = REPLACE( p-bank-account, ' ', '-').
END.

/* Do we have a Bank Account Number With or Without Hyphen(-)s ? */
IF INDEX( p-bank-account, '-', v-start-strpos ) = 0 THEN
    p-bank-account = STRING( p-bank-account, "99-9999-9999999-99").

REPEAT WHILE v-end-strpos < LENGTH(p-bank-account):
    v-end-strpos = INDEX( p-bank-account, '-', v-start-strpos ).
    
    /* Break Condition - Matched to the End of p-bank-account */
    IF v-end-strpos = 0 THEN DO:
         v-end-strpos = LENGTH(p-bank-account) + 1.
    END.

    CASE v-secpos:
        WHEN 1 THEN DO:
            op-bank = INTEGER( SUBSTRING( p-bank-account, v-start-strpos, v-end-strpos - v-start-strpos) ) NO-ERROR.
            IF ERROR-STATUS:ERROR AND v-bankimport-initialized THEN DO:
                /* PUT STREAM s-deskbanker UNFORMATTED
                     'Cannot Get Bank Code Of Bank Account Number ' + p-bank-account + ".~n".*/
                RETURN 'FAIL'.
            END.
        END.
        WHEN 2 THEN DO:
            op-branch = INTEGER( SUBSTRING( p-bank-account, v-start-strpos, v-end-strpos - v-start-strpos) ) NO-ERROR.
            IF ERROR-STATUS:ERROR AND v-bankimport-initialized THEN DO:
                /* PUT STREAM s-deskbanker UNFORMATTED
                    'Cannot Get Branch Code Of Bank Account Number ' + p-bank-account + ".~n".*/
                RETURN 'FAIL'.
            END.
        END.
        WHEN 3 THEN DO:
            op-account = INTEGER( SUBSTRING( p-bank-account, v-start-strpos, v-end-strpos - v-start-strpos) ) NO-ERROR.
            IF ERROR-STATUS:ERROR AND v-bankimport-initialized THEN DO:
                /* PUT STREAM s-deskbanker UNFORMATTED
                    'Cannot Get Account Code Of Bank Account Number ' + p-bank-account + ".~n".*/
                RETURN 'FAIL'.
            END.
        END.
        WHEN 4 THEN DO:
            op-suffix = INTEGER( SUBSTRING( p-bank-account, v-start-strpos, v-end-strpos - v-start-strpos) ) NO-ERROR.
            IF ERROR-STATUS:ERROR AND v-bankimport-initialized THEN DO:
                /* PUT STREAM s-deskbanker UNFORMATTED
                    'Cannot Get Suffix Code Of Bank Account Number ' + p-bank-account + ".~n".*/
                RETURN 'FAIL'.
            END.
            ELSE
                v-split2suffix = TRUE.
        END.
    END CASE.

    v-secpos = v-secpos + 1.
    v-start-strpos = v-end-strpos + 1.
END.

IF v-split2suffix AND v-secpos = 5 THEN
    RETURN 'OK'.
ELSE DO:
    /* Bugger, Reset Payer Account Variables */
    op-bank = 0. op-branch = 0. op-account = 0.
    RETURN 'FAIL'.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-bsbaccount) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-bsbaccount Method-Library 
PROCEDURE get-bsbaccount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT PARAMETER v-account-number AS CHARACTER NO-UNDO.
DEFINE OUTPUT PARAMETER v-bsb-bankstate AS CHARACTER NO-UNDO.
DEFINE OUTPUT PARAMETER v-bsb-branch AS CHARACTER NO-UNDO.
DEFINE OUTPUT PARAMETER v-bsb-account AS CHARACTER NO-UNDO.

v-account-number = REPLACE( v-account-number, "-", "" ).
v-account-number = REPLACE( v-account-number, " ", "" ).

DO ON ERROR UNDO, RETURN 'FAIL':
    v-bsb-bankstate = SUBSTRING( v-account-number, 1, 3 ).
    v-bsb-branch = SUBSTRING( v-account-number, 4, 3 ).
    v-bsb-account = SUBSTRING( v-account-number, 7, ( LENGTH( v-account-number ) - 6 ) ).
END.

RETURN 'OK'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

