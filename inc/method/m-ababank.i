&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : 
    Purpose     : Export an .aba standard file

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEFINE VAR v-aba-payer-bankstate AS CHARACTER NO-UNDO.
DEFINE VAR v-aba-payer-branch AS CHARACTER NO-UNDO.
DEFINE VAR v-aba-payer-account AS CHARACTER NO-UNDO.

DEFINE VAR v-aba-record-count AS INTEGER INIT 0 NO-UNDO.
DEFINE VAR v-aba-total-amount AS DECIMAL INIT 0.0 NO-UNDO.

DEFINE VAR v-aba-filename AS CHARACTER NO-UNDO.
DEFINE VAR v-aba-error-filename AS CHARACTER NO-UNDO.
DEFINE VAR v-aba-payer-payername AS CHARACTER NO-UNDO.
DEFINE VAR v-aba-payer-custnum AS INTEGER NO-UNDO.

DEFINE VARIABLE v-aba-de-bankcode AS CHARACTER NO-UNDO.
DEFINE VARIABLE v-aba-de-username AS CHARACTER NO-UNDO.
DEFINE VARIABLE v-aba-de-userid AS CHARACTER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 1.85
         WIDTH              = 41.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE aba-balancing-entry Include 
PROCEDURE aba-balancing-entry :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEFINE VAR bsb-bankstate AS CHARACTER NO-UNDO.
DEFINE VAR bsb-branch AS CHARACTER NO-UNDO.
DEFINE VAR bsb-account AS CHARACTER NO-UNDO.

DEFINE VAR v-length AS INTEGER NO-UNDO.
DEFINE VAR v-number AS INTEGER NO-UNDO.
DEFINE VAR v-count AS INTEGER NO-UNDO.
DEFINE VAR v-amount AS CHARACTER NO-UNDO.

/* Record type */
PUT STREAM s-bi-output UNFORMATTED "1".

/* Bank, State, Branch */
PUT STREAM s-bi-output UNFORMATTED v-aba-payer-bankstate + '-' + v-aba-payer-branch.

/* Credit/Debit account number */
/* (right justified, blank filled, with leading zeros if part of number) */
v-length = LENGTH( v-aba-payer-account ).
IF v-length < 9 THEN DO:
    v-number = 9 - v-length.
    DO v-count = 1 TO v-number:
        PUT STREAM s-bi-output UNFORMATTED " ".
    END.
END.
ELSE DO:
    IF v-length > 9 THEN DO:
        RETURN 'Could not fit the account number"' + v-aba-payer-account + '" into 9 characters'.
    END.
END.
PUT STREAM s-bi-output UNFORMATTED v-aba-payer-account.

/* Indicator */
PUT STREAM s-bi-output UNFORMATTED " ".

/* Transaction code - 13, 50, 51, 52, 53, 54, 55, 56, 57 */
PUT STREAM s-bi-output UNFORMATTED "13".

/* Amount */
v-amount = STRING( v-aba-total-amount * 100 ).
v-length = LENGTH( v-amount ).
IF v-length < 10 THEN DO:
    v-number = 10 - v-length.
    DO v-count = 1 TO v-number:
        PUT STREAM s-bi-output UNFORMATTED "0".
    END.
END.
PUT STREAM s-bi-output UNFORMATTED v-amount.

PUT STREAM s-bi-output v-aba-payer-payername FORMAT "X(32)".

/* Lodgement ref (18) */
PUT STREAM s-bi-output "                  " FORMAT "X(18)".

/* Trace BSD */
PUT STREAM s-bi-output UNFORMATTED v-aba-payer-bankstate + '-' + v-aba-payer-branch.

/* Trace account number */
/* (right justified, blank filled, with leading zeros if part of number) */
v-length = LENGTH( v-aba-payer-account ).
IF v-length < 9 THEN DO:
    v-number = 9 - v-length.
    DO v-count = 1 TO v-number:
        PUT STREAM s-bi-output UNFORMATTED " ".
    END.
END.
ELSE DO:
    IF v-length > 9 THEN DO:
        RETURN 'Could not fit the trace account number"' + v-aba-payer-account + '" into 9 characters'.
    END.
END.
PUT STREAM s-bi-output UNFORMATTED v-aba-payer-account.

/* Name of remitter (16) */
PUT STREAM s-bi-output v-aba-payer-payername FORMAT "X(16)".

/* Amount of witholding tax (never used hopefully) */
PUT STREAM s-bi-output UNFORMATTED "00000000".

/* PUT STREAM s-bi-output UNFORMATTED CHR(13). */
PUT STREAM s-bi-output UNFORMATTED CHR(10).

/* A count of these detail lines for the footer */
v-aba-record-count = v-aba-record-count + 1.

RETURN 'OK'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE aba-finish Include 
PROCEDURE aba-finish :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEFINE VAR v-total-amount-cents AS CHARACTER NO-UNDO.
DEFINE VAR v-count AS INTEGER NO-UNDO.
DEFINE VAR v-length AS INTEGER NO-UNDO.
DEFINE VAR v-number AS INTEGER NO-UNDO.

v-total-amount-cents = STRING( v-aba-total-amount * 100 ).

/* Record type */
PUT STREAM s-bi-output UNFORMATTED "7".

/* Bank, State, Branch */
PUT STREAM s-bi-output UNFORMATTED "999-999".

/* Gap (12) */
PUT STREAM s-bi-output UNFORMATTED "            ".

/* File net total - Always 0 */
PUT STREAM s-bi-output UNFORMATTED "0000000000". /* $10.80 */

IF LENGTH( STRING( v-total-amount-cents ) ) > 10 THEN
    RETURN 'The total value is more than $100,000,000, exceeding the record format'.

/* File credit/debit totals */
PUT STREAM s-bi-output UNFORMATTED STRING(INT(v-total-amount-cents),"9999999999").
PUT STREAM s-bi-output UNFORMATTED STRING(INT(v-total-amount-cents),"9999999999").

/* Gap (24) */
PUT STREAM s-bi-output UNFORMATTED "                        ".

/* Number of type 1 records */
IF LENGTH( STRING( v-aba-record-count ) ) > 6 THEN
    RETURN 'The record count exceeds a 6 digit number, the bank will reject this file'.

PUT STREAM s-bi-output UNFORMATTED STRING( v-aba-record-count, "999999" ).

/* Gap (40) */
PUT STREAM s-bi-output UNFORMATTED "                                        ".

/* PUT STREAM s-bi-output UNFORMATTED CHR(13). */
PUT STREAM s-bi-output UNFORMATTED CHR(10).

RETURN 'OK'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE aba-initialize Include 
PROCEDURE aba-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEFINE INPUT PARAMETER p-output-filename AS CHARACTER INITIAL '' NO-UNDO. 
DEFINE INPUT PARAMETER p-output-error-filename AS CHARACTER INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payer-bank-details AS CHARACTER INITIAL '' NO-UNDO. 
DEFINE INPUT PARAMETER p-payer-payername AS CHARACTER INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payer-custnum AS INTEGER INITIAL 0 NO-UNDO.
DEFINE INPUT PARAMETER p-de-bankcode AS CHARACTER NO-UNDO.
DEFINE INPUT PARAMETER p-de-username AS CHARACTER NO-UNDO.
DEFINE INPUT PARAMETER p-de-userid AS CHARACTER NO-UNDO.

RUN get-bsbaccount(
    p-payer-bank-details,
    OUTPUT v-aba-payer-bankstate,
    OUTPUT v-aba-payer-branch,
    OUTPUT v-aba-payer-account
).
IF RETURN-VALUE = 'FAIL' THEN DO:
    RETURN 'Cannot Make Sense of Payer Bank Account Number ' + p-payer-bank-details.
END.

ASSIGN
    v-aba-filename        = p-output-filename
    v-aba-error-filename  = p-output-error-filename
    v-aba-payer-payername = p-payer-payername
    v-aba-payer-custnum   = p-payer-custnum
    v-aba-de-bankcode     = p-de-bankcode
    v-aba-de-username     = p-de-username
    v-aba-de-userid       = p-de-userid.

IF v-aba-de-bankcode = "" OR v-aba-de-bankcode = ? THEN
    RETURN 'The direct entry bank code is empty for the selected bank account'.

IF v-aba-de-username = "" OR v-aba-de-username = ? THEN
    RETURN 'The direct entry user name is empty for the selected bank account'.

IF v-aba-de-userid = "" OR v-aba-de-userid = ? THEN
    RETURN 'The direct entry user id is empty for the selected bank account'.

/*
Run this from here, as its not called from within remittance advice and I dont want
to risk stuffing NZ bank exports by putting it in.
*/
RUN aba-payment-headerline(
    0,
    "APMSEXPORT",
    TODAY
).
IF RETURN-VALUE <> 'OK' THEN
    RETURN 'Running the header line failed: ' + RETURN-VALUE.

RETURN 'OK'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE aba-payment-detailline Include 
PROCEDURE aba-payment-detailline :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEFINE INPUT PARAMETER p-sequence AS INTEGER INITIAL 0 NO-UNDO.
DEFINE INPUT PARAMETER p-payee-bank-account LIKE BankAccount.BankAccount INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payee-name AS CHARACTER INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payee-particulars AS CHARACTER INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payee-analysis-code AS CHARACTER INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payee-reference AS CHARACTER INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-amount AS DECIMAL DECIMALS 2 INITIAL 0 NO-UNDO.

DEFINE VAR bsb-bankstate AS CHARACTER NO-UNDO.
DEFINE VAR bsb-branch AS CHARACTER NO-UNDO.
DEFINE VAR bsb-account AS CHARACTER NO-UNDO.

DEFINE VAR v-length AS INTEGER NO-UNDO.
DEFINE VAR v-number AS INTEGER NO-UNDO.
DEFINE VAR v-count AS INTEGER NO-UNDO.
DEFINE VAR v-amount AS CHARACTER NO-UNDO.

/* Assign Variables - Payee Bank Account Details */
RUN get-bsbaccount(
    p-payee-bank-account,
    OUTPUT bsb-bankstate,
    OUTPUT bsb-branch,
    OUTPUT bsb-account
).

/* Record type */
PUT STREAM s-bi-output UNFORMATTED "1".

/* Bank, State, Branch */
PUT STREAM s-bi-output UNFORMATTED bsb-bankstate + '-' + bsb-branch.

/* Credit/Debit account number */
/* (right justified, blank filled, with leading zeros if part of number) */
v-length = LENGTH( bsb-account ).
IF v-length < 9 THEN DO:
    v-number = 9 - v-length.
    DO v-count = 1 TO v-number:
        PUT STREAM s-bi-output UNFORMATTED " ".
    END.
END.
ELSE DO:
    IF v-length > 9 THEN DO:
        RETURN 'Could not fit the account number"' + bsb-account + '" into 9 characters'.
    END.
END.
PUT STREAM s-bi-output UNFORMATTED bsb-account.

/* Indicator */
PUT STREAM s-bi-output UNFORMATTED " ".

/* Transaction code - 13, 50, 51, 52, 53, 54, 55, 56, 57 */
PUT STREAM s-bi-output UNFORMATTED "50".

/* Amount */
v-aba-total-amount = v-aba-total-amount + p-amount.
v-amount = STRING( p-amount * 100 ).
v-length = LENGTH( v-amount ).
IF v-length < 10 THEN DO:
    v-number = 10 - v-length.
    DO v-count = 1 TO v-number:
        PUT STREAM s-bi-output UNFORMATTED "0".
    END.
END.
PUT STREAM s-bi-output UNFORMATTED v-amount. /* $110.05 */

/* Account name (32) */
/* v-length = LENGTH( p-payee-name ).                    */
/* IF v-length < 32 THEN DO:                             */
/*     PUT STREAM s-bi-output UNFORMATTED p-payee-name.  */
/*     v-number = 32 - v-length.                         */
/*     DO v-count = 1 TO v-number:                       */
/*         PUT STREAM s-bi-output UNFORMATTED " ".       */
/*     END.                                              */
/* END.                                                  */
/* ELSE                                                  */
PUT STREAM s-bi-output p-payee-name FORMAT "X(32)".

/* Lodgement ref (18) */
PUT STREAM s-bi-output p-payee-reference FORMAT "X(18)".

/* Trace BSD */
PUT STREAM s-bi-output UNFORMATTED v-aba-payer-bankstate + '-' + v-aba-payer-branch.

/* Trace account number */
/* (right justified, blank filled, with leading zeros if part of number) */
v-length = LENGTH( v-aba-payer-account ).
IF v-length < 9 THEN DO:
    v-number = 9 - v-length.
    DO v-count = 1 TO v-number:
        PUT STREAM s-bi-output UNFORMATTED " ".
    END.
END.
ELSE DO:
    IF v-length > 9 THEN DO:
        RETURN 'Could not fit the trace account number"' + v-aba-payer-account + '" into 9 characters'.
    END.
END.
PUT STREAM s-bi-output UNFORMATTED v-aba-payer-account.

/* Name of remitter (16) */
PUT STREAM s-bi-output v-aba-payer-payername FORMAT "X(16)".

/* Amount of witholding tax (never used hopefully) */
PUT STREAM s-bi-output UNFORMATTED "00000000".

/* PUT STREAM s-bi-output UNFORMATTED CHR(13). */
PUT STREAM s-bi-output UNFORMATTED CHR(10).

/* A count of these detail lines for the footer */
v-aba-record-count = v-aba-record-count + 1.

RETURN 'OK'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE aba-payment-headerline Include 
PROCEDURE aba-payment-headerline :
/*------------------------------------------------------------------------------
  Purpose: Print an .ABA format header
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEFINE INPUT PARAMETER p-sequence AS INTEGER INITIAL 0 NO-UNDO.
DEFINE INPUT PARAMETER p-description AS CHARACTER INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-due-date AS DATE INITIAL '' NO-UNDO.

/* Record type */
PUT STREAM s-bi-output UNFORMATTED "0".

/* Gap (17) */
PUT STREAM s-bi-output UNFORMATTED "                 ".

/* Reel sequence number */
PUT STREAM s-bi-output UNFORMATTED "01".

/* Financial institution */
PUT STREAM s-bi-output UNFORMATTED v-aba-de-bankcode.

/* Gap (7) */
PUT STREAM s-bi-output UNFORMATTED "       ".

/* Direct entry username */
PUT STREAM s-bi-output UNFORMATTED STRING( v-aba-de-username, "X(26)" ).

/* Direct entry userid */
PUT STREAM s-bi-output UNFORMATTED STRING( INT( v-aba-de-userid ), "999999" ).

/* File description */
PUT STREAM s-bi-output p-description FORMAT "X(12)".

/* Value date DDMMYY */
PUT STREAM s-bi-output UNFORMATTED
    STRING( DAY( p-due-date ), "99" )
    STRING( MONTH( p-due-date ), "99" )
    SUBSTRING( STRING( YEAR( p-due-date ), "9999" ), 3, 2 ).

/* Gap (40) */
PUT STREAM s-bi-output UNFORMATTED "                                        ".

/* PUT STREAM s-bi-output UNFORMATTED CHR(13). */
PUT STREAM s-bi-output UNFORMATTED CHR(10).

RETURN 'OK'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

