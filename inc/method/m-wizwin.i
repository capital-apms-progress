&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     :  Wizard Library
    Purpose     :

    Description :  Wizard Methods for Smart Windows

    Author(s)   :  Tyrone McAuley
    Created     :  30/10/1997
  ------------------------------------------------------------------------*/

DEF TEMP-TABLE Step NO-UNDO
  FIELD StepViewer   AS CHAR
  FIELD StepHelp     AS CHAR
  FIELD ObjectHandle AS HANDLE
  FIELD Mandatory    AS LOGI
  FIELD Valid        AS LOGI
  
  INDEX StepViewer IS UNIQUE PRIMARY
    StepViewer.

DEF TEMP-TABLE StepLink NO-UNDO
  FIELD ViewerFrom AS CHAR
  FIELD ViewerTo   AS CHAR

  INDEX StepFrom IS UNIQUE PRIMARY ViewerFrom
  INDEX StepTo IS UNIQUE           ViewerTo.

DEF TEMP-TABLE WizardResults NO-UNDO
  FIELD ResultName AS CHAR
  FIELD Answer AS CHAR

  INDEX ResultNames IS UNIQUE PRIMARY ResultName.

DEF VAR current-step        LIKE Step.StepViewer NO-UNDO.
DEF VAR current-step-handle AS HANDLE NO-UNDO.
DEF VAR current-step-no     AS INT NO-UNDO.
DEF VAR n-steps             AS INT NO-UNDO.
DEF VAR wizard-name         AS CHAR NO-UNDO.

DEF VAR h-btn-cancel AS HANDLE NO-UNDO.
DEF VAR h-btn-back   AS HANDLE NO-UNDO.
DEF VAR h-btn-next   AS HANDLE NO-UNDO.
DEF VAR h-btn-finish AS HANDLE NO-UNDO.
DEF VAR h-cmb-step   AS HANDLE NO-UNDO.

DEF VAR use-jump-combo AS LOGI NO-UNDO.

DEF VAR disable-viewing AS LOGICAL NO-UNDO INITIAL No.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-add-step) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD add-step Method-Library 
FUNCTION add-step RETURNS LOGICAL
  ( INPUT step-viewer AS CHAR,
    INPUT step-help   AS CHAR,
    INPUT mandatory   AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-add-step-link) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD add-step-link Method-Library 
FUNCTION add-step-link RETURNS LOGICAL
  ( INPUT step-from AS CHAR, INPUT step-to   AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-can-finish) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD can-finish Method-Library 
FUNCTION can-finish RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-clear-results) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clear-results Method-Library 
FUNCTION clear-results RETURNS LOGICAL
  ( /* no parameters */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-del-step-link) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD del-step-link Method-Library 
FUNCTION del-step-link RETURNS LOGICAL
  ( INPUT delete-step AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-result) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-result Method-Library 
FUNCTION get-result RETURNS CHARACTER
  ( INPUT result-name AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-sensitise) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD sensitise Method-Library 
FUNCTION sensitise RETURNS LOGICAL
  ( INPUT wh AS HANDLE, INPUT sens AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-result) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-result Method-Library 
FUNCTION set-result RETURNS LOGICAL
  ( INPUT result-name AS CHAR, INPUT result-value AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-validated) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-validated Method-Library 
FUNCTION set-validated RETURNS LOGICAL
  ( INPUT step-vwr AS CHAR, INPUT valid AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .15
         WIDTH              = 38.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-mstwin.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-back-step) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE back-step Method-Library 
PROCEDURE back-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND StepLink WHERE StepLink.ViewerTo = current-step NO-ERROR.
  IF NOT AVAILABLE(StepLink) THEN RETURN.

  FIND Step WHERE Step.StepViewer = StepLink.ViewerFrom NO-ERROR.
  IF NOT AVAILABLE Step THEN RETURN.

  current-step-no = current-step-no - 1.
  RUN view-step ( Step.StepViewer ).
  RUN update-node IN sys-mgr( THIS-PROCEDURE ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-cancel-process) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-process Method-Library 
PROCEDURE cancel-process :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LOOKUP( "local-cancel-process", THIS-PROCEDURE:INTERNAL-ENTRIES ) <> 0 THEN
    RUN local-cancel-process.
  ELSE DO:
    MESSAGE
      "Are you sure you want to quit the" SKIP
      "wizard and cancel all your changes?" SKIP
      VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO
      TITLE "Confirm Cancel" UPDATE cancel-it AS LOGI.
    IF cancel-it THEN RUN dispatch( 'exit' ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-default-layout) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE default-layout Method-Library 
PROCEDURE default-layout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN init-layout.
  RUN get-default-groups.
  RUN stack( "top-group,btn-group", "mst-group", "T", No ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-finish-process) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE finish-process Method-Library 
PROCEDURE finish-process :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR last-current AS CHAR NO-UNDO.

  IF LOOKUP( "local-finish-process", THIS-PROCEDURE:INTERNAL-ENTRIES ) <> 0 THEN
    RUN local-finish-process.
  ELSE DO:
    RUN get-attribute( 'Wizard-Finish':U ).
    IF RETURN-VALUE <> ? THEN DO:
      last-current = current-step.
      current-step = RETURN-VALUE.
      FIND Step WHERE Step.StepViewer = current-step NO-ERROR.
      IF NOT AVAILABLE(Step) THEN
        add-step( current-step, "Final finishing process for Wizard", No ).

      RUN view-step( current-step ).
      IF RETURN-VALUE = "FAIL" THEN
        RUN view-step( last-current ).
      ELSE DO:
        RUN finish-wizard IN current-step-handle NO-ERROR.
        IF RETURN-VALUE = "" THEN RUN dispatch( 'exit':U ).
      END.
    END.
    ELSE
      MESSAGE "There is no procedure defined to finish this" SKIP
              "wizard!" SKIP(1)
              "Inform your developer"
              VIEW-AS ALERT-BOX ERROR TITLE "Huh!?!".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-first-step) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE first-step Method-Library 
PROCEDURE first-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER OtherLink FOR StepLink.

  FIND StepLink WHERE StepLink.ViewerFrom = ? NO-ERROR.
  IF AVAILABLE(StepLink) THEN
    FIND Step WHERE Step.StepViewer = StepLink.ViewerTo NO-ERROR.
  ELSE DO:
    FIND FIRST StepLink WHERE
            NOT CAN-FIND( FIRST OtherLink WHERE OtherLink.ViewerTo = StepLink.ViewerFrom)
            NO-ERROR.
    IF NOT AVAILABLE(StepLink) THEN DO:
      MESSAGE "Can't find any link first step" VIEW-AS ALERT-BOX ERROR TITLE "No initial step in wizard!".
      FOR EACH StepLink:
        MESSAGE StepLink.ViewerFrom SKIP StepLink.ViewerTo .
      END.
      RETURN.
    END.
    FIND Step WHERE Step.StepViewer = StepLink.ViewerFrom NO-ERROR.
  END.

  IF NOT AVAILABLE Step THEN DO:
    MESSAGE "Can't find first step in list" SKIP(1)
            "A dicey wizard definition"
            VIEW-AS ALERT-BOX ERROR
            TITLE "Bad Wizard Definition".
    RETURN.
  END.

  current-step-no = 1.
  RUN view-step( Step.Stepviewer ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-default-groups) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-default-groups Method-Library 
PROCEDURE get-default-groups :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR goto-cmb-pack AS CHAR NO-UNDO.
  
  /* Include the step jump combo if requested */
  RUN get-attribute( 'GoTo-Combo' ).
  IF RETURN-VALUE = "Yes" THEN goto-cmb-pack = STRING( h-cmb-step ) + ",".
  
  RUN pre-pack IN curr-viewer ( THIS-PROCEDURE ) NO-ERROR .

  RUN pack( STRING( current-step-handle ), "top-group", "L", No ).
  RUN pack( STRING( h-btn-cancel ) + "," + STRING( h-btn-back ) + "," +
            STRING( h-btn-next )   + "," + goto-cmb-pack +
            STRING( h-btn-finish ), "btn-group", "L", No ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-goto-first-error) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE goto-first-error Method-Library 
PROCEDURE goto-first-error :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR wizard-finish AS CHAR NO-UNDO.

  RUN get-attribute( 'Wizard-Finish' ). wizard-finish = RETURN-VALUE.

  disable-viewing = Yes.
  RUN first-step.
  DO WHILE NOT(can-finish()) AND AVAILABLE(Step)
                             AND (Step.Valid OR NOT Step.Mandatory)
                             AND Step.StepViewer <> wizard-finish:
    disable-viewing = Yes.
    RUN next-step.
  END.
  RUN view-step ( Step.StepViewer ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-goto-step) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE goto-step Method-Library 
PROCEDURE goto-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF BUFFER SL FOR StepLink.
  DEF BUFFER ST FOR Step.
  
  DEF VAR goto-step-no AS INT NO-UNDO.
  DEF VAR this-step-no AS INT NO-UNDO.
  DEF VAR this-step AS CHAR NO-UNDO.
  
  goto-step-no = INT( h-cmb-step:SCREEN-VALUE ).
  this-step-no = current-step-no.
  this-step    = current-step.
  
  IF goto-step-no = this-step-no OR goto-step-no = ? OR
     goto-step-no < 0 OR goto-step-no > n-steps THEN RETURN.
     
  IF goto-step-no < this-step-no THEN
  DO WHILE this-step-no <> goto-step-no:

    FIND SL WHERE SL.ViewerTo = this-step NO-ERROR.
    IF NOT AVAILABLE SL THEN RETURN.
    FIND ST WHERE ST.StepViewer = SL.ViewerFrom NO-ERROR.
    IF NOT AVAILABLE ST THEN RETURN.
    this-step    = ST.StepViewer.
    this-step-no = this-step-no - 1.
    
  END.
  ELSE IF goto-step-no > this-step-no THEN
  DO WHILE this-step-no <> goto-step-no:
  
    FIND SL WHERE SL.ViewerFrom = this-step NO-ERROR.
    IF NOT AVAILABLE SL THEN RETURN.
    FIND ST WHERE ST.StepViewer = SL.ViewerTo.
    IF NOT AVAILABLE ST THEN RETURN.
    this-step    = ST.StepViewer.
    this-step-no = this-step-no + 1.
  
  END.

  current-step-no = goto-step-no.
  RUN view-step ( this-step ).
  RUN update-node IN sys-mgr( THIS-PROCEDURE ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-initialize-wizard) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialize-wizard Method-Library 
PROCEDURE initialize-wizard :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Create the wizard widgets */
  DEF VAR frm-hdl AS HANDLE NO-UNDO.
  RUN get-attribute( 'Frame-Handle' ).
  frm-hdl = WIDGET-HANDLE( RETURN-VALUE ).
  
  CREATE BUTTON h-btn-cancel
  ASSIGN FRAME = frm-hdl   FONT  = 10   LABEL = "&Cancel"   SENSITIVE = Yes
         WIDTH-C = 10 HEIGHT-C = 1.15
  TRIGGERS:
    ON CHOOSE PERSISTENT RUN cancel-process IN THIS-PROCEDURE.
  END TRIGGERS.

  CREATE BUTTON h-btn-back
  ASSIGN FRAME = frm-hdl   FONT  = 10   LABEL = "<  &Back"   SENSITIVE = Yes
         WIDTH-C = 10 HEIGHT-C = 1.15
  TRIGGERS:
    ON CHOOSE PERSISTENT RUN back-step IN THIS-PROCEDURE.
  END TRIGGERS.

  CREATE BUTTON h-btn-next
  ASSIGN FRAME = frm-hdl   FONT  = 10   LABEL = "&Next  >"   SENSITIVE = Yes
         WIDTH-C = 10 HEIGHT-C = 1.15 DEFAULT = Yes
  TRIGGERS:
    ON CHOOSE PERSISTENT RUN next-step IN THIS-PROCEDURE.
  END TRIGGERS.
  frm-hdl:DEFAULT-BUTTON = h-btn-next.

  CREATE COMBO-BOX h-cmb-step
  ASSIGN FRAME = frm-hdl   FONT  = 10    SENSITIVE = Yes
         WIDTH-C = 6 INNER-LINES = 10    HIDDEN = NOT use-jump-combo
  TRIGGERS:
    ON VALUE-CHANGED PERSISTENT RUN goto-step IN THIS-PROCEDURE.
  END TRIGGERS.

  CREATE BUTTON h-btn-finish
  ASSIGN FRAME = frm-hdl   FONT  = 10   LABEL = "&Finish"    SENSITIVE = Yes
         WIDTH-C = 10 HEIGHT-C = 1.15
  TRIGGERS:
    ON CHOOSE PERSISTENT RUN finish-process IN THIS-PROCEDURE.
  END TRIGGERS.

  IF LOOKUP( "local-wizard-definition", THIS-PROCEDURE:INTERNAL-ENTRIES ) <> 0 THEN
    RUN local-wizard-definition.
  ELSE
    RUN wizard-definition.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-next-step) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE next-step Method-Library 
PROCEDURE next-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FIND StepLink WHERE StepLink.ViewerFrom = current-step NO-ERROR.
  IF NOT AVAILABLE(StepLink) THEN DO:
    IF can-finish() THEN RUN finish-process.
    RETURN.
  END.
    
  FIND Step WHERE Step.StepViewer = StepLink.ViewerTo.
  IF NOT AVAILABLE Step THEN RETURN.
  
  current-step-no = current-step-no + 1.
  RUN view-step ( Step.StepViewer ).
  RUN update-node IN sys-mgr( THIS-PROCEDURE ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-post-apply-entry) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE post-apply-entry Method-Library 
PROCEDURE post-apply-entry :
/*------------------------------------------------------------------------------
  Purpose: 
  Notes:       
------------------------------------------------------------------------------*/

  IF VALID-HANDLE(current-step-handle) THEN
    RUN dispatch IN current-step-handle ( INPUT 'apply-entry':U ) .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reset-step-combo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-step-combo Method-Library 
PROCEDURE reset-step-combo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-step-status-changed) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE step-status-changed Method-Library 
PROCEDURE step-status-changed :
/*------------------------------------------------------------------------------
  Purpose:     Sensitise the wizard navigation buttons
  Notes:       Checks to:
                 - see if there is a prior step
                 - see if there is a next step
                 - see if it is now valid to 'finish'
                 - sensitise the buttons appropriately
                 
------------------------------------------------------------------------------*/
DEF VAR prev-step        AS LOGI NO-UNDO.
DEF VAR next-step        AS LOGI NO-UNDO.
DEF VAR finishing-state  AS LOGI NO-UNDO.
DEF VAR wizard-finish    AS CHAR NO-UNDO.
DEF VAR current-finished AS LOGI NO-UNDO.
DEF BUFFER Nextlink    FOR StepLink.
  
  IF NOT VALID-HANDLE( current-step-handle ) THEN RETURN.

  /* Check to see if there is a previous step */
  prev-step = CAN-FIND( StepLink WHERE StepLink.ViewerTo = current-step ).

  /* Check to see if we are in a finishing state
                i.e. all mandatory steps have been completed */
  finishing-state = can-finish().
  
  /* Check to see if there is a next step */
  /* If the next step is the finish and we are not in a finishing state then
     we cannot go next
       OR
     we are not using a jump combo ( sequential steps ) and the current step has not
     been completed */
  
  current-finished = CAN-FIND( Step WHERE Step.StepViewer = current-step AND
                                  ( Step.Valid OR NOT Step.Mandatory ) ).

  RUN get-attribute( 'Wizard-Finish':U ).
  wizard-finish = RETURN-VALUE.
  FIND FIRST NextLink WHERE NextLink.ViewerFrom = current-step 
    NO-LOCK NO-ERROR.
  
  IF NOT use-jump-combo /* Must complete steps sequentially */ THEN
    next-step = AVAILABLE NextLink AND current-finished.
  ELSE /* Not necessarily to complete sequentially */
    next-step = AVAILABLE NextLink .
    
  /* sensitise( h-btn-cancel, Yes ). */
  sensitise( h-btn-back,   prev-step ).
  sensitise( h-btn-next,   next-step ).
  sensitise( h-btn-finish, finishing-state ).

  RETURN.

DEF VAR msg AS CHAR NO-UNDO.
  FOR EACH Step:
    msg = msg + Step.StepViewer
        + "  Mand: " + STRING(Step.Mandatory)
        + " Valid: " + STRING(Step.Valid) + "~n" .
  END.
  MESSAGE msg.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-sub1-create-objects) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sub1-create-objects Method-Library 
PROCEDURE sub1-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  RUN initialize-wizard.
  RUN first-step.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-use-GoTo-Combo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-GoTo-Combo Method-Library 
PROCEDURE use-GoTo-Combo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER use-combo AS CHAR NO-UNDO.
  use-jump-combo = use-combo = "Yes".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-use-Wizard-Name) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Wizard-Name Method-Library 
PROCEDURE use-Wizard-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-name AS CHAR NO-UNDO.
  wizard-name = new-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-view-step) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE view-step Method-Library 
PROCEDURE view-step :
/*------------------------------------------------------------------------------
  Purpose:  Hide the current step and view a different one
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-step AS CHAR NO-UNDO.

DEF VAR run-viewer AS CHAR NO-UNDO.
DEF BUFFER LastStep FOR Step.
DEF BUFFER NextStep FOR Step.
  
  /* Hide the current step if necessary */
  IF VALID-HANDLE( current-step-handle ) THEN DO:
    RUN validate-step IN current-step-handle NO-ERROR.
    set-validated( ?, RETURN-VALUE <> "FAIL").
    RUN dispatch IN current-step-handle( 'hide' ).
  END.

  FIND NextStep WHERE NextStep.StepViewer = new-step NO-ERROR.
  IF NOT AVAILABLE(NextStep) THEN DO:
    MESSAGE "No step defined for" new-step
            VIEW-AS ALERT-BOX WARNING TITLE "Poor Wizard Definition".
    add-step( new-step, "No help available", No ).
    FIND NextStep WHERE NextStep.StepViewer = new-step NO-ERROR.
  END.

  IF AVAILABLE(NextStep) THEN DO:
    IF NOT VALID-HANDLE( NextStep.ObjectHandle ) THEN DO:
      RUN verify-prog( NextStep.StepViewer, OUTPUT run-viewer ).
      IF RETURN-VALUE = "FAIL" THEN RETURN.
      RUN init-object IN THIS-PROCEDURE ( run-viewer,  FRAME {&FRAME-NAME}:HANDLE,
              'Hide-On-Init = Yes,Wizard-Handle =':U + STRING(THIS-PROCEDURE),
              OUTPUT NextStep.ObjectHandle ).
      RUN dispatch IN NextStep.ObjectHandle ( 'Initialize':U ).
    END.

    current-step        = new-step.
    current-step-handle = NextStep.ObjectHandle.

    IF NOT disable-viewing THEN DO:
      h-cmb-step:SCREEN-VALUE = STRING( current-step-no ).
      h-cmb-step:PRIVATE-DATA = STRING( current-step-no ).

      {&WINDOW-NAME}:TITLE = wizard-name + " - Step " +
                          STRING( current-step-no ) + " of " + STRING( n-steps ).
      RUN window-layout.
      RUN dispatch IN current-step-handle ( 'View' ).
      RUN display-step IN current-step-handle NO-ERROR.
      RUN step-status-changed.
      RUN dispatch IN current-step-handle ( 'apply-entry':U ) .
    END.
    ELSE DO:
      RUN validate-step IN current-step-handle NO-ERROR.
      RUN step-status-changed.
      disable-viewing = false.
    END.
  END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-wizard-definition) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE wizard-definition Method-Library 
PROCEDURE wizard-definition :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.
DEF VAR viewer-def AS CHAR NO-UNDO.
DEF VAR def-line AS CHAR NO-UNDO.
DEF VAR major-delim AS CHAR NO-UNDO INITIAL "~n".
DEF VAR minor-delim AS CHAR NO-UNDO INITIAL "|".

  RUN get-attribute( "Major-Delim":U ).
  IF RETURN-VALUE <> ? THEN major-delim = RETURN-VALUE.
  RUN get-attribute( "Minor-Delim":U ).
  IF RETURN-VALUE <> ? THEN minor-delim = RETURN-VALUE.

  RUN get-attribute( "Wizard-Viewers":U ).
  viewer-def = RETURN-VALUE.
  n = NUM-ENTRIES( viewer-def, major-delim).
  DO i = 1 TO n:
    def-line = ENTRY( i, viewer-def, major-delim).
    add-step( ENTRY(1, def-line, minor-delim), ENTRY(2, def-line, minor-delim),
                         (SUBSTRING(ENTRY(3,def-line,minor-delim), 1,1) = "Y") ).
  END.

  RUN get-attribute( "Viewer-Links":U ).
  viewer-def = RETURN-VALUE.
  n = NUM-ENTRIES( viewer-def, major-delim).
  
  DO i = 1 TO n:
    def-line = ENTRY( i, viewer-def, major-delim).
    add-step-link( ENTRY(1, def-line, minor-delim), ENTRY(2, def-line, minor-delim) ).
  END.
  
  RUN get-attribute( "Wizard-Results":U ).
  viewer-def = RETURN-VALUE.
  n = NUM-ENTRIES( viewer-def, major-delim).
  DO i = 1 TO n:
    def-line = ENTRY( i, viewer-def, major-delim).
    set-result( ENTRY(1, def-line, minor-delim),
                      SUBSTRING( def-line, LENGTH(ENTRY(1,def-line, minor-delim)) + 2 ) ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-add-step) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION add-step Method-Library 
FUNCTION add-step RETURNS LOGICAL
  ( INPUT step-viewer AS CHAR,
    INPUT step-help   AS CHAR,
    INPUT mandatory   AS LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  CREATE Step.
  ASSIGN Step.StepViewer = step-viewer
         Step.StepHelp   = step-help
         Step.Mandatory  = mandatory
         Step.Valid      = No
         n-steps         = n-steps + 1.
  IF VALID-HANDLE( h-cmb-step ) AND h-cmb-step:ADD-LAST( STRING( n-steps ) ) THEN.

/*  MESSAGE "Added step" SKIP Step.StepViewer SKIP step-help SKIP mandatory SKIP n-steps. */

  RETURN TRUE .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-add-step-link) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION add-step-link Method-Library 
FUNCTION add-step-link RETURNS LOGICAL
  ( INPUT step-from AS CHAR, INPUT step-to   AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR old-end AS CHAR NO-UNDO INITIAL ?.

  /* validate the parameters */
  IF NOT CAN-FIND(Step WHERE Step.StepViewer = step-from ) THEN RETURN No.
  IF NOT CAN-FIND(Step WHERE Step.StepViewer = step-to   ) THEN RETURN No.

  FIND StepLink WHERE StepLink.ViewerFrom = step-from NO-ERROR.
  IF AVAILABLE(StepLink) THEN DO:
    ASSIGN old-end = StepLink.ViewerTo
           StepLink.ViewerTo = step-to.

    CREATE StepLink.
    ASSIGN StepLink.ViewerFrom = step-to
           StepLink.ViewerTo   = old-end.
  END.
  ELSE DO:
    FIND StepLink WHERE StepLink.ViewerTo = step-to NO-ERROR.
    IF AVAILABLE(StepLink) THEN DO:
      ASSIGN old-end = StepLink.ViewerFrom
             StepLink.ViewerFrom = step-from.

      CREATE StepLink.
      ASSIGN StepLink.ViewerFrom = old-end
             StepLink.ViewerTo   = step-from.
    END.
    ELSE DO:
      CREATE StepLink.
      ASSIGN StepLink.ViewerFrom = step-from
             StepLink.ViewerTo   = step-to.
    END.
  END.

  RETURN Yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-can-finish) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION can-finish Method-Library 
FUNCTION can-finish RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Returns Yes if there are steps in error
    Notes:  
------------------------------------------------------------------------------*/
DEF BUFFER E-Step FOR Step.

  RETURN NOT CAN-FIND( FIRST E-Step WHERE E-Step.Mandatory AND NOT E-Step.Valid ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-clear-results) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clear-results Method-Library 
FUNCTION clear-results RETURNS LOGICAL
  ( /* no parameters */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  FOR EACH WizardResults: DELETE WizardResults. END.
  RETURN Yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-del-step-link) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION del-step-link Method-Library 
FUNCTION del-step-link RETURNS LOGICAL
  ( INPUT delete-step AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR old-from AS CHAR NO-UNDO INITIAL ?.
DEF VAR old-to AS CHAR NO-UNDO INITIAL ?.

  FIND StepLink WHERE StepLink.ViewerFrom = delete-step NO-ERROR.
  IF AVAILABLE(StepLink) THEN DO:
    old-to = StepLink.ViewerTo.
    DELETE StepLink.
  END.

  FIND StepLink WHERE StepLink.ViewerTo = delete-step NO-ERROR.
  IF AVAILABLE(StepLink) THEN DO:
    old-from = StepLink.ViewerFrom.
    DELETE StepLink.
  END.

  IF old-from <> ? AND old-to <> ? THEN DO:
    CREATE StepLink.
    ASSIGN StepLink.ViewerFrom = old-from
           StepLink.ViewerTo   = old-to.
  END.

  RETURN Yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-result) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-result Method-Library 
FUNCTION get-result RETURNS CHARACTER
  ( INPUT result-name AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  FIND WizardResults WHERE WizardResults.ResultName = result-name NO-ERROR.

  RETURN IF AVAILABLE(WizardResults) THEN WizardResults.Answer ELSE ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-sensitise) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION sensitise Method-Library 
FUNCTION sensitise RETURNS LOGICAL
  ( INPUT wh AS HANDLE, INPUT sens AS LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  IF VALID-HANDLE(wh) AND CAN-SET( wh, "SENSITIVE") THEN wh:SENSITIVE = sens.
    
  RETURN Yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-result) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-result Method-Library 
FUNCTION set-result RETURNS LOGICAL
  ( INPUT result-name AS CHAR, INPUT result-value AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  FIND WizardResults WHERE WizardResults.ResultName = result-name NO-ERROR.
  IF NOT AVAILABLE(WizardResults) THEN CREATE WizardResults.

  ASSIGN    WizardResults.ResultName    = result-name
            WizardResults.Answer        = result-value.

  RETURN Yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-validated) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-validated Method-Library 
FUNCTION set-validated RETURNS LOGICAL
  ( INPUT step-vwr AS CHAR, INPUT valid AS LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  Set the current step as validated
------------------------------------------------------------------------------*/
DEF BUFFER V-Step FOR Step.

DEF VAR result AS LOGI NO-UNDO INITIAL Yes.

  IF step-vwr = ? THEN step-vwr = current-step.
  FIND V-Step WHERE V-Step.StepViewer = step-vwr NO-ERROR.
  IF NOT AVAILABLE(V-Step) THEN
    result = No.
  ELSE DO:
    V-Step.Valid = valid.
    /* IF NOT valid THEN V-Step.Mandatory = Yes. */
  END.

  RUN step-status-changed.
  RETURN result.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

