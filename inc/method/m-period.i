&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

&GLOBAL-DEFINE VALID-REPORTS "VCNT,LEXP,NLSE,RVWO,RVWR,CAPX"
&GLOBAL-DEFINE MAX-BREAKS 3

DEF VAR delim AS CHAR NO-UNDO.
delim = "|".
DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}

DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR report-preview AS LOGICAL NO-UNDO INITIAL No.

DEF VAR brk-val AS CHAR EXTENT {&MAX-BREAKS} NO-UNDO INITIAL ?.

{inc/ofc-this.i}
{inc/ofc-set-l.i "Mgmt-Contract-Rents" "use-contract-rents"}
IF NOT AVAILABLE(OfficeSetting) THEN use-contract-rents = No.
{inc/ofc-set-l.i "Mgmt-Display-TenantCode" "display-tenantcode"}
IF NOT AVAILABLE(OfficeSetting) THEN display-tenantcode = No.

/***************************************************************
 Generation stage definitions
***************************************************************/

DEFINE WORK-TABLE Portfolio NO-UNDO
  FIELD PersonCode   LIKE Person.PersonCode
  FIELD Portfolio    AS CHAR
  FIELD PropertyList AS CHAR
  FIELD AreaSize     AS DEC
  FIELD NoParks      AS INT
  FIELD Rental       AS DEC EXTENT 3.
/*    
  INDEX Portfolio IS UNIQUE PRIMARY
    Portfolio.
*/

/* Variables updated through firt passes to add-to-portfolio */
DEF VAR total-area AS DEC          NO-UNDO.
DEF VAR total-parks AS INT         NO-UNDO.
DEF VAR total-rntl AS DEC EXTENT 3 NO-UNDO.


/* PeriodicDetail Line Variables */
DEF VAR dtl     AS CHAR NO-UNDO.
DEF VAR id      AS CHAR NO-UNDO INITIAL ?.


/* Variables based on report record selection criteria */
/* i.e - only vacant space, space on new leases etc.... */

DEF VAR brk-area AS DEC EXTENT 4  NO-UNDO.
DEF VAR brk-rntl AS DEC EXTENT 12 NO-UNDO.
DEF VAR brk-cost AS DEC EXTENT 4  NO-UNDO.

/* Property Information Table */

DEF WORK-TABLE PropInfo NO-UNDO
  FIELD PropertyCode LIKE Property.PropertyCode
  FIELD Manager        AS CHAR
  FIELD LeasedArea     AS DEC
  FIELD VacantArea     AS DEC
  FIELD TotalArea      AS DEC
  FIELD TotalParks     AS INT
  FIELD LeaseAreaRent  AS DEC
  FIELD LeaseParkRent  AS DEC
  FIELD VacantAreaRent AS DEC
  FIELD VacantParkRent AS DEC
  FIELD MarketRental   AS DEC
  FIELD MarketCarpark  AS DEC
  FIELD floor-rate     AS DEC
  FIELD park-rate      AS DEC
  FIELD opex-psm       AS DEC
  FIELD opex           AS DEC
  FIELD VacantRate     AS DEC
  FIELD TotalRent      AS DEC.   /* This is the Budget EAR for vacant space plus
                                    the contracted rent for leased space  */
/*  
  INDEX PropertyCode IS UNIQUE PRIMARY
    PropertyCode.
*/

/* Lease Information Table */

DEF WORK-TABLE LeaseInfo NO-UNDO
  FIELD TenancyLeaseCode LIKE TenancyLease.TenancyLeaseCode
  FIELD area-name    AS CHAR
  FIELD TotalArea    AS DEC
  FIELD Manager      AS CHAR
  FIELD TotalParks   AS DEC
  FIELD Carpark      AS LOGI
  FIELD c-area       AS CHAR
  FIELD c-parks      AS CHAR
  FIELD opex         AS DEC
  FIELD c-opex       AS CHAR
  FIELD AreaRental   AS DEC  EXTENT 3
  FIELD ParkRental   AS DEC  EXTENT 3
  FIELD Rental       AS DEC  EXTENT 3
  FIELD c-rntl       AS CHAR EXTENT 3
  FIELD ear-total    AS DEC
  FIELD c-ear-total  AS CHAR
  FIELD rent-psm     AS DEC  EXTENT 3
  FIELD c-rent-psm   AS CHAR EXTENT 3
  FIELD gross        AS LOGICAL
  FIELD c-gross      AS CHAR
  FIELD c-start      AS CHAR
  FIELD c-finish     AS CHAR
  FIELD c-change     AS CHAR
  FIELD change-type  AS CHAR
  FIELD c-remaining  AS CHAR
  FIELD c-term       AS CHAR
  FIELD prev-rent    AS DEC  EXTENT 3
  FIELD c-prev-rent  AS CHAR EXTENT 3
  FIELD prev-psm     AS DEC  EXTENT 3
  FIELD c-prev-psm   AS CHAR EXTENT 3
  FIELD c-notice     AS CHAR.

DEFINE WORK-TABLE AreaInfo NO-UNDO
  FIELD area-name   AS CHAR FORMAT "X(45)"
  FIELD floor-space AS CHAR FORMAT "X(5)"
  FIELD area-size   AS DEC 
  FIELD c-area      AS CHAR 
  FIELD area-parks  AS INT 
  FIELD c-parks     AS CHAR 
  FIELD opex-pc     AS DEC
  FIELD c-opex-pc   AS CHAR
  FIELD area-opex   AS DEC 
  FIELD c-opex      AS CHAR
  FIELD rntl        AS DEC  EXTENT 3 
  FIELD c-rntl      AS CHAR EXTENT 3 
  FIELD rate        AS DEC  EXTENT 3
  FIELD c-rate      AS CHAR EXTENT 3 
  FIELD cost        AS DEC 
  FIELD c-cost      AS CHAR 
  FIELD cost-psm    AS DEC 
  FIELD c-cost-psm  AS CHAR 
  FIELD vacated     AS DATE 
  FIELD c-vacated   AS CHAR
  FIELD area-percent AS DEC
  FIELD c-area-percent AS CHAR
  FIELD rent-percent AS DEC
  FIELD c-rent-percent AS CHAR
  .


{inc/convert.i}


/***************************************************************
 Output stage definitions
***************************************************************/
/* Report counters */
DEF VAR ln AS DEC INIT 0.00 NO-UNDO.

/* Line definitions */
DEF VAR rows AS DEC NO-UNDO.
DEF VAR cols AS DEC NO-UNDO.

DEF VAR page-no               AS INT INIT 0 NO-UNDO.
DEF VAR reset-page            AS CHAR NO-UNDO.
DEF VAR half-line             AS CHAR NO-UNDO.
DEF VAR title-font            AS CHAR NO-UNDO.
DEF VAR title2-font           AS CHAR NO-UNDO.
DEF VAR time-font             AS CHAR NO-UNDO.
DEF VAR break-font            AS CHAR EXTENT {&MAX-BREAKS} NO-UNDO.
DEF VAR column-font           AS CHAR NO-UNDO.
DEF VAR body-font             AS CHAR NO-UNDO.
DEF VAR summary-font          AS CHAR NO-UNDO.
DEF VAR line-printer          AS CHAR NO-UNDO.
DEF VAR report-asat-date      AS DATE NO-UNDO.

DEF VAR report-title  AS CHAR NO-UNDO.
DEF VAR field-list    AS CHAR NO-UNDO.
DEF VAR xlate-list    AS CHAR NO-UNDO.
DEF VAR align-list    AS CHAR NO-UNDO.
DEF VAR wrap-list     AS CHAR NO-UNDO.
DEF VAR format-list   AS CHAR NO-UNDO.
DEF VAR gap-list      AS CHAR NO-UNDO.
DEF VAR total-list    AS CHAR NO-UNDO.
DEF VAR total-formats AS CHAR NO-UNDO.
DEF VAR n-breaks      AS INT  NO-UNDO.
DEF VAR break-type    AS INT  NO-UNDO.
DEF VAR n-columns     AS INT  NO-UNDO.

DEF VAR lines-per-page  AS INT NO-UNDO.

DEF VAR brk-lines AS INT  NO-UNDO.
DEF VAR now       AS CHAR NO-UNDO.
DEF VAR brk-tot   AS CHAR EXTENT {&MAX-BREAKS} NO-UNDO  INITIAL ?.
DEF VAR grd-tot   AS CHAR NO-UNDO.

DEF BUFFER Currentdetail FOR PeriodicDetail.
/* Buffer for the most recent detail record of the report */

{inc/ofc-this.i}
{inc/ofc-set.i "Organisation-Name" "organisation-name"}
{inc/ofc-set-l.i "Use-Rent-Charges" "use-rent-charges"}
{inc/ofc-set-l.i "Property-MarketPerUnit" "market-per-unit"}

DEF VAR break-4-by   AS CHAR NO-UNDO.
DEF VAR break-4-run  AS CHAR NO-UNDO INITIAL "".
DEF VAR break-4-direction AS CHAR NO-UNDO INITIAL "Up".
DEFINE TEMP-TABLE Alt-Index NO-UNDO
  FIELD BreakValue AS CHAR
  FIELD ShowValue AS CHAR
  FIELD Row-ID AS ROWID

  INDEX XPKAlt-Index IS PRIMARY BreakValue ASCENDING.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-to-annual) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD to-annual Method-Library 
FUNCTION to-annual RETURNS DECIMAL
  ( INPUT freq-code AS CHAR, INPUT period-amount AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .05
         WIDTH              = 40.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}
{inc/method/m-lease-rentals.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-add-rsp-portfolio) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-rsp-portfolio Method-Library 
PROCEDURE add-rsp-portfolio :
/*------------------------------------------------------------------------------
  Purpose:  Used to calculate the portfolio totals for comparison
------------------------------------------------------------------------------*/
DEF VAR rntl      AS DEC EXTENT 3 NO-UNDO.
DEF VAR area-size AS DEC NO-UNDO.
DEF VAR area-parks AS INT NO-UNDO.
DEF VAR area-opex AS DEC NO-UNDO.
DEF VAR opex-pc AS DEC NO-UNDO.
DEF VAR i    AS INT NO-UNDO.

  RUN get-property-info.
  area-size  = 0.
  area-parks = 0.
  RUN test-floor-space( RentalSpace.AreaType, RentalSpace.AreaSize).
  IF RETURN-VALUE = "Yes" THEN
    area-size = RentalSpace.AreaSize.
  ELSE IF RETURN-VALUE = "Park" THEN ASSIGN
    area-parks = RentalSpace.AreaSize.

  opex-pc = (IF RentalSpace.OutgoingsPercentage <> ? THEN RentalSpace.OutgoingsPercentage ELSE 
            (IF area-size > 0 THEN PropInfo.TotalArea / area-size ELSE 0)).

  IF AVAILABLE(TenancyLease) AND TenancyLease.GrossLease THEN ASSIGN
    area-opex = 0.
  ELSE IF RentalSpace.AreaStatus = "V" THEN ASSIGN
    area-opex = PropInfo.opex * (opex-pc / 100).
  ELSE ASSIGN
    area-opex = PropInfo.opex * (opex-pc / 100).

  rntl[1] = RentalSpace.MarketRental.
  rntl[2] = RentalSpace.ContractedRental.
  rntl[3] = area-opex + rntl[2].

  DO i = 1 TO 2: IF rntl[i] = ? THEN rntl[i] = 0.00. END.
  IF rntl[3] = ? THEN rntl[3] = rntl[2].

  /* Add to the portfolio */
  FIND FIRST Portfolio WHERE Portfolio.PersonCode = Property.Manager NO-ERROR.
    
  IF NOT AVAILABLE Portfolio THEN
  DO:
    FIND Person WHERE Person.PersonCode = Property.Manager NO-LOCK NO-ERROR.
    CREATE Portfolio.
    ASSIGN Portfolio.PersonCode = Property.Manager
           Portfolio.Portfolio = (IF AVAILABLE(Person) THEN Person.Department ELSE "Other").
  END.

  Portfolio.AreaSize = Portfolio.AreaSize + area-size.
  Portfolio.NoParks  = Portfolio.NoParks + area-parks.
  total-area = total-area + area-size.
  total-parks = total-parks + area-parks.

  IF LOOKUP( STRING( Property.PropertyCode) , Portfolio.PropertyList ) = 0 THEN
  DO:
    DEF VAR property-rntl AS DEC NO-UNDO.
    Portfolio.PropertyList = Portfolio.PropertyList + IF Portfolio.PropertyList = "" THEN "" ELSE ",".
    Portfolio.PropertyList = Portfolio.PropertyList + STRING( Property.PropertyCode ).
    property-rntl = IF Property.MarketRental <> ? THEN Property.MarketRental ELSE 0.
    Portfolio.Rental[1] = Portfolio.Rental[1] + property-rntl.
    total-rntl[1] = total-rntl[1] + property-rntl.
  END.

  DO i = 2 TO 3:
    Portfolio.Rental[i] = Portfolio.Rental[i] + rntl[i].
    total-rntl[i] = total-rntl[i] + rntl[i].  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-add-to-totals) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-to-totals Method-Library 
PROCEDURE add-to-totals :
/*------------------------------------------------------------------------------
  Purpose:  Add the values in data onto the totals column
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-level AS INT NO-UNDO.
DEF INPUT PARAMETER data AS CHAR NO-UNDO.

DEF VAR j AS INT NO-UNDO.
DEF VAR tot AS DEC NO-UNDO.
DEF VAR val AS DEC NO-UNDO.
DEF VAR totals AS CHAR NO-UNDO.

DEF VAR no-totals AS INT NO-UNDO.
DEF VAR no-vals AS INT NO-UNDO.

  no-totals = NUM-ENTRIES(total-list, delim).
  no-vals   = NUM-ENTRIES( data, delim).
  IF no-vals < no-totals THEN no-totals = no-vals.

  totals = IF new-level > 0 THEN brk-tot[new-level] ELSE grd-tot.
  DO j = 1 TO no-totals:
    IF ENTRY( j, total-list, delim) = "Y" THEN DO:
      tot = DECIMAL( ENTRY( j, totals, delim ) ) NO-ERROR.
      IF tot = ? THEN tot = 0.
      ASSIGN val = DECIMAL( TRIM( ENTRY( j, data, delim ) ) ) NO-ERROR.
      IF val = ? THEN val = 0.
      tot = tot + val.
      ENTRY( j, totals, delim ) = STRING(tot).
    END.
  END.

  IF new-level > 0 THEN
    brk-tot[new-level] = totals.
  ELSE
    grd-tot = totals.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-basic-report) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE basic-report Method-Library 
PROCEDURE basic-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER report-date AS DATE NO-UNDO.
DEF VAR i AS INT NO-UNDO.

  IF file-name = "Printer" THEN RUN get-control-strings.
  IF n-breaks > {&MAX-BREAKS} THEN n-breaks = {&MAX-BREAKS}.

  /* trim total list to end at last 'Y' */
  i = R-INDEX( total-list, "Y").
  total-list = SUBSTRING( total-list, 1, i).
  DO i = 0 TO n-breaks:
    RUN clear-totals( i ).
    IF i > 0 THEN brk-val[i] = ?.
  END.
  n-columns = NUM-ENTRIES( xlate-list ).

  FIND LAST CurrentDetail WHERE CurrentDetail.ReportID   = report-id
            AND CurrentDetail.Date <= report-date
            AND CurrentDetail.DetailType = "D"
            NO-LOCK NO-ERROR.

  /* so that we skip to the next page as soon as we start */
  ln = lines-per-page + 1.
  page-no = 0.
  RUN skip-line(0).

  IF break-type = 4 THEN        RUN break-by-4.
  ELSE IF break-type = 3 THEN   RUN break-by-3.
  ELSE IF break-type = 2 THEN   RUN break-by-2.
  ELSE                          RUN break-by-1.

  DO i = n-breaks TO 1 BY -1:
    RUN break-end( i ).
  END.
  RUN break-end( 0 ).

  RUN page-footer.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-break-begin) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE break-begin Method-Library 
PROCEDURE break-begin :
/*------------------------------------------------------------------------------
  Purpose:  Called when a new break level is starting
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER break-level AS INT NO-UNDO.

  RUN clear-totals(break-level).
  RUN pre-break-begin IN THIS-PROCEDURE ( break-level ) NO-ERROR.
  IF RETURN-VALUE = "NO" THEN RETURN.
  IF file-name <> "Printer" THEN RETURN.

  IF file-name = "Printer" AND ln + 3 >= lines-per-page THEN RUN skip-line(3).
                                                        ELSE RUN skip-line(0).
  PUT CONTROL break-font[break-level].
  PUT UNFORMATTED brk-val[break-level].
  PUT CONTROL line-printer.
  RUN skip-line( IF break-level > 1 THEN 1 ELSE 1.5 ).

  RUN post-break-begin IN THIS-PROCEDURE ( break-level ) NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-break-by-1) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE break-by-1 Method-Library 
PROCEDURE break-by-1 :
/*------------------------------------------------------------------------------
  Purpose:  Report using first break-by
------------------------------------------------------------------------------*/

  IF AVAILABLE CurrentDetail THEN DO:
    FOR EACH PeriodicDetail NO-LOCK WHERE
      PeriodicDetail.ReportId   = report-id AND
      PeriodicDetail.Date       = CurrentDetail.Date AND
      PeriodicDetail.DetailType = "D"
      BY PeriodicDetail.ReportId  BY PeriodicDetail.Date
      BY PeriodicDetail.DetailType BY PeriodicDetail.BreakValue1:

      RUN each-detail IN THIS-PROCEDURE(PeriodicDetail.BreakValue1).
    END.

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-break-by-2) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE break-by-2 Method-Library 
PROCEDURE break-by-2 :
/*------------------------------------------------------------------------------
  Purpose:  Report using second break-by
------------------------------------------------------------------------------*/

  IF AVAILABLE CurrentDetail THEN DO:
    FOR EACH PeriodicDetail NO-LOCK WHERE
      PeriodicDetail.ReportId   = report-id AND
      PeriodicDetail.Date       = CurrentDetail.Date AND
      PeriodicDetail.DetailType = "D"
      BY PeriodicDetail.ReportId  BY PeriodicDetail.Date
      BY PeriodicDetail.DetailType BY PeriodicDetail.BreakValue2:

      RUN each-detail IN THIS-PROCEDURE(PeriodicDetail.BreakValue2).
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-break-by-3) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE break-by-3 Method-Library 
PROCEDURE break-by-3 :
/*------------------------------------------------------------------------------
  Purpose:  Report using third break-by
------------------------------------------------------------------------------*/

  IF AVAILABLE CurrentDetail THEN DO:
    FOR EACH PeriodicDetail NO-LOCK WHERE
      PeriodicDetail.ReportId   = report-id AND
      PeriodicDetail.Date       = CurrentDetail.Date AND
      PeriodicDetail.DetailType = "D"
      BY PeriodicDetail.ReportId  BY PeriodicDetail.Date
      BY PeriodicDetail.DetailType BY PeriodicDetail.BreakValue3:

      RUN each-detail IN THIS-PROCEDURE(PeriodicDetail.BreakValue3).
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-break-by-4) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE break-by-4 Method-Library 
PROCEDURE break-by-4 :
/*------------------------------------------------------------------------------
  Purpose:  Report using sneakily using alternative break value
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR n-brk AS INT NO-UNDO.
DEF VAR brk AS CHAR NO-UNDO.

n-brk = NUM-ENTRIES( break-4-by ).

  FOR EACH Alt-Index: DELETE Alt-Index.  END.
  IF AVAILABLE(CurrentDetail) THEN DO:
    FOR EACH PeriodicDetail NO-LOCK WHERE
      PeriodicDetail.ReportId   = report-id AND
      PeriodicDetail.Date       = CurrentDetail.Date AND
      PeriodicDetail.DetailType = "D":

      CREATE Alt-Index.
      Alt-Index.Row-Id = ROWID(PeriodicDetail).
      IF break-4-run <> "" THEN
        RUN VALUE(break-4-run).
      ELSE DO:
        DO i = 1 TO n-brk:
          brk = (IF i = 1 THEN "" ELSE brk + delim)
              + ENTRY( INT(ENTRY( i, break-4-by)), PeriodicDetail.Data, delim).
        END.
        ASSIGN Alt-Index.BreakValue = brk
               Alt-Index.ShowValue  = brk.
      END.
    END.

    IF break-4-direction = "Up" THEN DO:
      FOR EACH Alt-Index:
        FIND PeriodicDetail WHERE ROWID(PeriodicDetail) = Alt-Index.Row-Id.
        RUN each-detail IN THIS-PROCEDURE(Alt-Index.ShowValue).
      END.
    END.
    ELSE DO:
      FOR EACH Alt-Index BY Alt-Index.Breakvalue DESCENDING:
        FIND PeriodicDetail WHERE ROWID(PeriodicDetail) = Alt-Index.Row-Id.
        RUN each-detail IN THIS-PROCEDURE(Alt-Index.ShowValue).
      END.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-break-end) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE break-end Method-Library 
PROCEDURE break-end :
/*------------------------------------------------------------------------------
  Purpose:  Called when a break level is ending
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER break-level AS INT  NO-UNDO.

DEF BUFFER SummaryDetail FOR PeriodicDetail.
DEF VAR brk-ln     AS INT  NO-UNDO.
DEF VAR total-line AS CHAR NO-UNDO.
DEF VAR break-value AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR break-processing AS CHAR NO-UNDO.

  IF break-level > n-breaks THEN RETURN.
  break-value = IF break-level = 0 THEN "Grand Total" ELSE brk-val[break-level].
  IF break-value = ? THEN RETURN.

  RUN pre-break-end IN THIS-PROCEDURE ( break-level ) NO-ERROR.
  break-processing = RETURN-VALUE.
  IF break-processing = "NO" THEN DO:
    IF break-level > 0 THEN RUN add-to-totals( break-level - 1, brk-tot[break-level] ).
    RETURN.
  END.
  IF file-name <> "Printer" THEN RETURN.

  IF LOOKUP( "Y", total-list, delim ) = 0 THEN
    RUN skip-line(2).
  ELSE
  DO:

    IF break-processing <> "" OR break-level <> n-breaks OR brk-lines > 1 THEN
    DO:
      IF LOOKUP( "no-separator", break-processing) = 0 THEN RUN total-separator.
      total-line = "".
      DO i = 1 TO NUM-ENTRIES( total-list, delim ):
        IF ENTRY( i, total-list, delim ) = "Y" THEN
          total-line = total-line + STRING( DECIMAL( ENTRY( i, (IF break-level = 0 THEN grd-tot ELSE brk-tot[break-level]), delim)),
                                            ENTRY( i, total-formats, delim) )
                                  + delim.
        ELSE IF ENTRY( i, total-list, delim ) = "B" THEN
          total-line = total-line + break-value + delim.
        ELSE
          total-line = total-line + delim.
      END.
      IF file-name = "Printer" THEN RUN line-to-printer( No, total-line + delim ).
                               ELSE RUN line-to-file( total-line + delim ).
    END.

    FIND FIRST SummaryDetail WHERE
      SummaryDetail.ReportID   = report-id AND
      SummaryDetail.Date       = PeriodicDetail.Date AND
      SummaryDetail.EntityID   = break-value AND
      SummaryDetail.DetailType = "S" + STRING( break-level )
    NO-LOCK NO-ERROR.

    IF AVAILABLE SummaryDetail AND NOT ( break-level = n-breaks AND brk-lines <= 1 ) THEN
    DO:
      DO brk-ln = 1 TO NUM-ENTRIES( SummaryDetail.Data, CHR(10) ):
        PUT CONTROL summary-font.
        PUT UNFORMATTED ENTRY( brk-ln, SummaryDetail.Data, CHR(10) ).
        PUT CONTROL line-printer.
        RUN skip-line(1.5).
      END.
    END.

    IF LOOKUP( "no-blank", break-processing) = 0 THEN RUN skip-line(1).
    brk-lines = 0.
  
  END.

  IF break-level > 0 THEN RUN add-to-totals( break-level - 1, brk-tot[break-level] ).

  RUN post-break-end IN THIS-PROCEDURE ( break-level ) NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-calc-date-diff) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calc-date-diff Method-Library 
PROCEDURE calc-date-diff :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEF INPUT PARAMETER start-date AS DATE NO-UNDO.
DEF INPUT PARAMETER end-date   AS DATE NO-UNDO.
DEF OUTPUT PARAMETER yy AS INT NO-UNDO.
DEF OUTPUT PARAMETER mm AS INT NO-UNDO.
DEF OUTPUT PARAMETER dd AS INT NO-UNDO.

DEF VAR dpm AS INT NO-UNDO.
DEF VAR start-yy AS INT NO-UNDO.
DEF VAR start-mm AS INT NO-UNDO.

  dd = DAY( end-date ).  mm = MONTH( end-date ).  yy = YEAR( end-date ).
  RUN get-number-of-days( yy, mm, OUTPUT dpm).

  dd = 1 + dd - DAY( start-date ).
  start-mm = MONTH( start-date ).
  start-yy = YEAR( start-date ).
  mm = mm - start-mm.
  yy = yy - start-yy.
  IF dd = dpm THEN
    ASSIGN     dd = 0      mm = mm + 1  .
  ELSE DO:
    IF dd < 0 THEN DO:
      RUN get-number-of-days( start-yy, start-mm, OUTPUT dpm).
      ASSIGN      dd = dd + dpm       mm = mm - 1  .
    END.
  END.
  IF mm = 12 THEN ASSIGN mm = 0 yy = yy + 1.
  IF mm < 0 THEN ASSIGN mm = mm + 12      yy = yy - 1.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-clear-totals) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-totals Method-Library 
PROCEDURE clear-totals :
/*------------------------------------------------------------------------------
  Purpose:  Clear the break totals for a break level
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER break-level AS INT NO-UNDO.

  IF break-level = 0 THEN
    grd-tot = FILL( "0" + delim, NUM-ENTRIES(total-list, delim) ).
  ELSE
    brk-tot[break-level] = FILL( "0" + delim, NUM-ENTRIES(total-list, delim) ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-create-detail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-detail Method-Library 
PROCEDURE create-detail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER type AS CHAR NO-UNDO.
DEF INPUT PARAMETER e-id   AS CHAR NO-UNDO.
DEF INPUT PARAMETER dtl  AS CHAR NO-UNDO.
  
DEF VAR id-property AS CHAR NO-UNDO.
DEF VAR pd-rowid AS ROWID NO-UNDO.

  /* If this is for a carpark, check to see if we have a record for this
      property already  */
  IF merge-parks AND type = "D" AND
     ENTRY(2,dtl,delim) = "Park" THEN DO:
     /* Find a periodic detail record for the same property number but not the same
        rentalspacecode, where the second part of detail is park and add these car
        parks to it.  */
     /* MESSAGE "merge - A - 0".    */
     id-property = ENTRY(1,e-id,",").
     /* MESSAGE "merge - A - 1". */
     pd-rowid = ?.
     FOR EACH PeriodicDetail WHERE PeriodicDetail.Date = TODAY
          AND   PeriodicDetail.ReportID = report-id
          AND   PeriodicDetail.EntityID BEGINS id-property
          NO-LOCK:
       IF ( NUM-ENTRIES(PeriodicDetail.Data,delim) > 1
             AND ENTRY(2, PeriodicDetail.Data,delim) = "Park" ) THEN DO:
         pd-rowid = ROWID(PeriodicDetail).
         LEAVE.
       END.
     END.
     IF pd-rowid <> ? THEN
       FIND PeriodicDetail WHERE ROWID(PeriodicDetail) = pd-rowid
               EXCLUSIVE-LOCK NO-ERROR.
     /* MESSAGE "merge - A - 2".    */
   END.

   /* MESSAGE "create-detail " type dtl. */
   IF merge-parks AND type = "D" AND
      ENTRY(2,dtl,delim) = "Park" AND
      AVAILABLE PeriodicDetail THEN DO:
        /* pull all the data out, add this one and put it back
            1 - area description
            4 - car parks
            7 - rate 1
            9 - rate 2
            11 - rate 3 */
     ENTRY(1,PeriodicDetail.Data,delim) = "Carpark".
     ENTRY(4,PeriodicDetail.Data,delim) = STRING(INTEGER(ENTRY(4,PeriodicDetail.Data,delim)) +
                                                       INTEGER(ENTRY(4,dtl,delim))).
     ENTRY(7,PeriodicDetail.Data,delim) = STRING(DECIMAL(ENTRY(7,PeriodicDetail.Data,delim)) +
                                                      DECIMAL(ENTRY(7,dtl,delim)),"->>,>>>,>>9.99").
     ENTRY(9,PeriodicDetail.Data,delim) = STRING(DECIMAL(ENTRY(9,PeriodicDetail.Data,delim)) +
                                                      DECIMAL(ENTRY(9,dtl,delim)),"->>,>>>,>>9.99").
     ENTRY(11,PeriodicDetail.Data,delim) = STRING(DECIMAL(ENTRY(11,PeriodicDetail.Data,delim)) +
                                                       DECIMAL(ENTRY(11,dtl,delim)),"->>,>>>,>>9.99").

  END.
  ELSE DO:

    CREATE PeriodicDetail.
    ASSIGN
      PeriodicDetail.Date        = TODAY
      PeriodicDetail.ReportID    = report-id
      PeriodicDetail.EntityID    = e-id
      PeriodicDetail.DetailType  = type
      PeriodicDetail.Data        = dtl
      PeriodicDetail.BreakValue1 = brk-val[1]
      PeriodicDetail.BreakValue2 = brk-val[2]
      PeriodicDetail.BreakValue3 = brk-val[3].
      
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-create-lease-detail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-lease-detail Method-Library 
PROCEDURE create-lease-detail :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR p-short-name AS CHAR NO-UNDO.
DEF VAR e-id AS CHAR NO-UNDO.
DEF VAR tenant-name AS CHAR NO-UNDO.

  p-short-name = Property.ShortName.
  IF p-short-name = ? OR TRIM(p-short-name) = "" THEN
    p-short-name = SUBSTR( Property.Name, 1, 12).

  IF display-tenantcode THEN
      tenant-name = (IF AVAILABLE(Tenant) THEN STRING(Tenant.TenantCode) + " " + Tenant.Name ELSE "").
  ELSE
      tenant-name = (IF AVAILABLE(Tenant) THEN Tenant.Name ELSE "").

  dtl = p-short-name + delim +                /*  1   */
        tenant-name  + delim +
        LeaseInfo.area-name + delim +         /*  3   */
        LeaseInfo.c-gross + delim +           /*  4   */
        LeaseInfo.c-area + delim +            /*  5   */
        LeaseInfo.c-parks + delim +           /*  6   */
        LeaseInfo.c-opex + delim +            /*  7   */
        LeaseInfo.c-rntl[1] + delim +         /*  8   */
        LeaseInfo.c-rent-psm[1] + delim +     /*  9   */
        LeaseInfo.c-rntl[2] + delim +         /*  10  */
        LeaseInfo.c-rent-psm[2] + delim +     /*  11  */
        LeaseInfo.c-rntl[3] + delim +         /*  12  */
        LeaseInfo.c-rent-psm[3] + delim +     /*  13  */
        LeaseInfo.c-start + delim +           /*  14  */
        LeaseInfo.c-finish + delim +          /*  15  */
        LeaseInfo.c-remaining + delim +       /*  16  */
        LeaseInfo.c-term + delim +            /*  17  */
        LeaseInfo.c-prev-rent[2] + delim +    /*  18  */
        LeaseInfo.c-prev-psm[2] + delim +     /*  19  */
        LeaseInfo.c-prev-rent[3] + delim +    /*  20  */
        LeaseInfo.c-prev-psm[3] + delim +     /*  21  */
        LeaseInfo.c-ear-total + delim         /*  22  */
        + LeaseInfo.c-change + delim          /*  23  */
        + LeaseInfo.change-type + delim       /*  24  */
        + LeaseInfo.Manager + delim           /*  25  */
        + LeaseInfo.c-notice + delim          /*  26  */
        .

  IF id = ? THEN
    e-id = STRING( TenancyLease.TenancyLeaseCode ).
  ELSE
    e-id = id.

  RUN create-detail( "D", e-id, dtl ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-create-rentalspace-detail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-rentalspace-detail Method-Library 
PROCEDURE create-rentalspace-detail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR e-id AS CHAR NO-UNDO.

  FIND Person WHERE Person.PersonCode = Property.Manager NO-LOCK NO-ERROR.
  FIND Region WHERE Region.Region = Property.Region NO-LOCK NO-ERROR.
 
  brk-val[1] = PropInfo.Manager
             + delim + STRING( Property.PropertyCode, "99999" ) + " - " + (IF Property.Name = ? THEN "* * Unknown * *" ELSE Property.Name)
             + delim + STRING( RentalSpace.Level, "+9999" ) + STRING( RentalSpace.LevelSequence, "+9999").
  brk-val[2] = delim + STRING( Property.PropertyCode, "99999" ) + " - " + (IF Property.Name = ? THEN "* * Unknown * *" ELSE Property.Name)
             + delim + STRING( RentalSpace.Level + 1000, "9999" ) + STRING( RentalSpace.LevelSequence, "9999").
  brk-val[3] = Property.Region + " - " + (IF AVAILABLE(Region) THEN Region.Name ELSE "Unknown")

             + delim + STRING( Property.PropertyCode, "99999" ) + " - " + (IF Property.Name = ? THEN "* * Unknown * *" ELSE Property.Name)
             + delim + STRING( RentalSpace.Level + 1000, "9999" ) + STRING( RentalSpace.LevelSequence, "9999").

  dtl =
    AreaInfo.area-name     + delim +        /*  1   */
    AreaInfo.floor-space   + delim +        /*  2   */
    AreaInfo.c-area        + delim +        /*  3   */
    AreaInfo.c-parks       + delim +        /*  4   */
    AreaInfo.c-opex        + delim +        /*  5   */
    AreaInfo.c-opex-pc     + delim +        /*  6   */
    AreaInfo.c-rntl[1]     + delim +        /*  7   */
    AreaInfo.c-rate[1]     + delim +        /*  8   */
    AreaInfo.c-rntl[2]     + delim +        /*  9   */
    AreaInfo.c-rate[2]     + delim +        /*  10  */
    AreaInfo.c-rntl[3]     + delim +        /*  11  */
    AreaInfo.c-rate[3]     + delim +        /*  12  */
    AreaInfo.c-cost        + delim +        /*  13  */
    AreaInfo.c-cost-psm    + delim +        /*  14  */
    AreaInfo.c-vacated     + delim +        /*  15  */
    AreaInfo.c-area-percent + delim +       /*  16  */
    AreaInfo.c-rent-percent.                /*  17  */
    

  IF id = ? THEN
    e-id = STRING( RentalSpace.PropertyCode ) + "," + STRING( RentalSpace.RentalSpaceCode ).
  ELSE
    e-id = id.

  RUN create-detail( "D", e-id, dtl ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-delete-todays-report) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-todays-report Method-Library 
PROCEDURE delete-todays-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH PeriodicDetail WHERE
    PeriodicDetail.ReportID   = report-id AND
    PeriodicDetail.Date       = TODAY     AND
    PeriodicDetail.DetailType <> "C" EXCLUSIVE-LOCK.

    DELETE PeriodicDetail.
    
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-detail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-detail Method-Library 
PROCEDURE each-detail :
/*------------------------------------------------------------------------------
  Purpose:  Process each detail line
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER break-value AS CHAR NO-UNDO.
DEF BUFFER CommentDetail FOR PeriodicDetail.
DEF VAR i AS INT NO-UNDO.
DEF VAR this-value AS CHAR INITIAL "" EXTENT {&MAX-BREAKS} NO-UNDO.
DEF VAR data      AS CHAR NO-UNDO.
DEF VAR brk-max   AS INT NO-UNDO INITIAL 999.

  RUN pre-detail-line IN THIS-PROCEDURE ( INPUT-OUTPUT break-value ) NO-ERROR.
  IF RETURN-VALUE = "NO" THEN RETURN.

  /* find the maximum break level */
  DO i = n-breaks TO 1 BY -1:
    this-value[i] = ENTRY( i, break-value, delim ).
    IF brk-val[i] <> this-value[i] THEN brk-max = i.
  END.

  /* close the previous break sets */
  DO i = n-breaks TO 1 BY -1:
    IF i >= brk-max THEN RUN break-end IN THIS-PROCEDURE( i ).
  END.

  /* open the new break sets */
  DO i = 1 TO n-breaks:
    IF i >= brk-max THEN DO:
      brk-val[i] = this-value[i].
      RUN break-begin IN THIS-PROCEDURE( i ).
    END.
  END.

  FIND LAST CommentDetail WHERE
    CommentDetail.ReportID   = PeriodicDetail.ReportID AND
    CommentDetail.EntityID   = PeriodicDetail.EntityID AND
    CommentDetail.DetailType = "C"
    NO-LOCK NO-ERROR.

  data = "".
  DO i = 1 TO n-columns:
    IF ENTRY( i, xlate-list ) = "C" THEN
      data = data + (IF i > 1 THEN delim ELSE "") + (IF AVAILABLE CommentDetail AND (TRIM(CommentDetail.Data) <> "") THEN TRIM(CommentDetail.Data) + "    --(" + STRING( CommentDetail.Date, "99/99/9999" ) + ")--" ELSE "").
    ELSE
      data = data + (IF i > 1 THEN delim ELSE "") + ENTRY( INTEGER(ENTRY( i,xlate-list)), PeriodicDetail.Data, delim).
  END.

  RUN add-to-totals IN THIS-PROCEDURE( n-breaks, data ).

  RUN pre-output-line IN THIS-PROCEDURE ( break-value ) NO-ERROR.
  IF RETURN-VALUE = "NO" THEN RETURN.

/*
  MESSAGE "PD: " PeriodicDetail.Data.
  MESSAGE "Dt: " data.
*/
  IF file-name = "Printer" THEN RUN line-to-printer IN THIS-PROCEDURE( Yes, data ).
                           ELSE RUN line-to-file IN THIS-PROCEDURE( data ).

  RUN post-detail-line IN THIS-PROCEDURE ( break-value ) NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-area-info) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-area-info Method-Library 
PROCEDURE get-area-info :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       RentalSpace, Property and Portfolio must be available  - and PropInfo
------------------------------------------------------------------------------*/
DEF VAR i       AS INT NO-UNDO.
DEF VAR brk     AS INT NO-UNDO.

  RUN get-property-info.
  CREATE AreaInfo.
  AreaInfo.area-name = RentalSpace.Description .
  RUN test-floor-space( RentalSpace.AreaType, RentalSpace.AreaSize).
  AreaInfo.floor-space = RETURN-VALUE.
  AreaInfo.area-size  = IF AreaInfo.floor-space = "Yes"  THEN RentalSpace.AreaSize ELSE 0.00.
  AreaInfo.area-percent = IF PropInfo.TotalArea > 0 THEN ((AreaInfo.area-size / PropInfo.TotalArea) * 100) ELSE 0.00.
  AreaInfo.area-parks = IF AreaInfo.floor-space = "Park" THEN RentalSpace.AreaSize ELSE 0.
  AreaInfo.c-area =  IF AreaInfo.area-size = 0  THEN "" ELSE TRIM( STRING( AreaInfo.area-size, "->>,>>>,>>9.99" )).
  AreaInfo.c-area-percent =  IF AreaInfo.area-percent = 0  THEN "0.00" ELSE TRIM( STRING( AreaInfo.area-percent, ">>9.99" )).
  AreaInfo.c-parks = IF AreaInfo.area-parks = 0 THEN "" ELSE TRIM( STRING( AreaInfo.area-parks, "->>>,>>9")).
  AreaInfo.opex-pc = (IF RentalSpace.OutgoingsPercentage = ? THEN 0 ELSE 
                     (IF RentalSpace.OutgoingsPercentage <> 0 THEN RentalSpace.OutgoingsPercentage ELSE 
                     (IF AreaInfo.area-size > 0 THEN (AreaInfo.area-size / PropInfo.TotalArea * 100) ELSE 0))).
  AreaInfo.c-opex-pc = (IF AreaInfo.opex-pc <> 0 THEN TRIM( STRING( AreaInfo.opex-pc, "->>>9.99")) ELSE "").

  IF AVAILABLE(TenancyLease) AND TenancyLease.GrossLease THEN ASSIGN
    AreaInfo.area-opex = 0.
  ELSE IF RentalSpace.AreaStatus = "V" THEN ASSIGN
    AreaInfo.area-opex = PropInfo.opex * (AreaInfo.opex-pc / 100).
  ELSE ASSIGN
    AreaInfo.area-opex = PropInfo.opex * (AreaInfo.opex-pc / 100).

  AreaInfo.rntl[1] = RentalSpace.MarketRental.
  IF NOT(use-contract-rents) AND use-rent-charges THEN
    AreaInfo.rntl[2] = get-area-rental( Property.PropertyCode, RentalSpace.RentalSpaceCode ).
  ELSE
    AreaInfo.rntl[2] = RentalSpace.ContractedRental .
  AreaInfo.rntl[3] = AreaInfo.area-opex + AreaInfo.rntl[2].
  IF market-per-unit THEN DO:
    IF AreaInfo.rntl[1] = ? THEN AreaInfo.rntl[1] = 100.0 .
    AreaInfo.rntl[1] = (AreaInfo.rntl[1] / 100.0).
    IF AreaInfo.floor-space = "Park" THEN
      AreaInfo.rntl[1] = AreaInfo.rntl[1] * PropInfo.MarketCarpark * AreaInfo.area-size * 52.
    ELSE IF AreaInfo.floor-space = "Yes" THEN
      AreaInfo.rntl[1] = AreaInfo.rntl[1] * PropInfo.MarketRental * AreaInfo.area-size.
    ELSE
      AreaInfo.rntl[1] = AreaInfo.rntl[2].
  END.

  IF AreaInfo.rntl[1] = 0 OR AreaInfo.rntl[1] = ? THEN DO:
    IF AreaInfo.floor-space = "Yes" THEN 
      AreaInfo.rntl[1] = PropInfo.floor-rate * AreaInfo.area-size.
    ELSE IF AreaInfo.floor-space = "Park" THEN 
      AreaInfo.rntl[1] = PropInfo.park-rate * AreaInfo.area-size.
  END.

  DO i = 1 TO 3:
    IF AreaInfo.rntl[i] = ? THEN AreaInfo.rntl[i] = 0.

    AreaInfo.c-rntl[i] = TRIM( STRING( AreaInfo.rntl[i], "->>,>>>,>>9.99" )).
    AreaInfo.c-rate[i] = (IF AreaInfo.floor-space = "Park" THEN
                    TRIM( STRING( AreaInfo.rntl[i] / ( 52 * AreaInfo.area-size ), "->>>,>>9.99" ) + "pw")
                ELSE (IF AreaInfo.floor-space = "Yes" THEN
                    ( TRIM( STRING( AreaInfo.rntl[i] / AreaInfo.area-size, "->>>,>>9.99" )) + "  " )
                ELSE "")).

    IF AreaInfo.c-rate[i] = ? THEN AreaInfo.c-rate[i] = "".

  END.
  AreaInfo.rent-percent = IF PropInfo.TotalRent > 0 THEN ((AreaInfo.rntl[1] / (AreaInfo.rntl[1] + PropInfo.TotalRent)) * 100) ELSE 0.00.
  IF AreaInfo.rent-percent > 999.99 THEN AreaInfo.rent-percent = 999.99.
  AreaInfo.c-rent-percent =  IF AreaInfo.rent-percent = 0  THEN "" ELSE TRIM( STRING( AreaInfo.rent-percent, ">>9.99" )).

  AreaInfo.cost = 0.        AreaInfo.cost-psm = 0.
  AreaInfo.c-cost = "".     AreaInfo.c-cost-psm = "".
  AreaInfo.vacated = ?.     AreaInfo.c-vacated = "".
  IF RentalSpace.AreaStatus = "V" THEN DO:
    IF RentalSpace.VacationDate <> ? THEN ASSIGN
      AreaInfo.vacated = RentalSpace.VacationDate
      AreaInfo.c-vacated = STRING( RentalSpace.VacationDate, "99/99/9999").

    AreaInfo.cost = IF RentalSpace.VacantCosts = ? THEN 0 ELSE RentalSpace.VacantCosts .

    IF AreaInfo.cost <> 0 THEN ASSIGN
      AreaInfo.cost-psm = AreaInfo.cost / AreaInfo.area-size
      AreaInfo.c-cost = TRIM( STRING( AreaInfo.cost, "->>,>>>,>>9.99" )).
      AreaInfo.c-cost-psm = (IF AreaInfo.cost-psm = ? OR AreaInfo.cost-psm = 0 THEN "" ELSE TRIM( STRING( AreaInfo.cost-psm, "->>>>9.99" ))).
  END.

/*
DEFINE WORK-TABLE AreaInfo NO-UNDO
  FIELD area-name   AS CHAR
  FIELD floor-space AS CHAR
  FIELD area-size   AS DEC 
  FIELD c-area      AS CHAR 
  FIELD area-parks  AS INT 
  FIELD c-parks     AS CHAR 
  FIELD opex-pc     AS DEC
  FIELD c-opex-pc   AS CHAR
  FIELD area-opex   AS DEC 
  FIELD c-opex      AS CHAR
  FIELD rntl        AS DEC  EXTENT 3 
  FIELD c-rntl      AS CHAR EXTENT 3 
  FIELD rate        AS DEC  EXTENT 3
  FIELD c-rate      AS CHAR EXTENT 3 

  FIELD cost        AS DEC 
  FIELD c-cost      AS CHAR 
  FIELD cost-psm    AS DEC 
  FIELD c-cost-psm  AS CHAR 
  FIELD vacated     AS DATE 
  FIELD c-vacated   AS CHAR 
*/
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-control-strings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-control-strings Method-Library 
PROCEDURE get-control-strings :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
/* for discarded row & column values */
DEF VAR rx AS INT NO-UNDO.
DEF VAR cx AS INT NO-UNDO.

  lines-per-page = 85.

  RUN make-control-string( "PCL", (IF LOOKUP( report-id, "NLSE,RVWO,RVWR" ) <> 0
                                        THEN "reset,landscape,a4,tm,0,lm,0"
                                        ELSE "reset,landscape,a4,tm,0,lm,0"),
                          OUTPUT reset-page, OUTPUT rx, OUTPUT cx ).

  RUN make-control-string( "PCL", "fixed,courier,cpi,20,lpi,11",
                    OUTPUT line-printer, OUTPUT rows, OUTPUT cols ).

  IF report-preview THEN DO:
    reset-page = reset-page + line-printer.
    line-printer = "".
    RETURN.
  END.
  half-line = CHR(27) + "=".
  body-font = line-printer.

  RUN make-control-string( "PCL", "Proportional,Helvetica,Bold,Point,18",
                  OUTPUT title-font, OUTPUT rx, OUTPUT cx ).

  RUN make-control-string( "PCL", "Proportional,Helvetica,Bold,Point,11",
                  OUTPUT title2-font, OUTPUT rx, OUTPUT cx ).

  RUN make-control-string( "PCL", "Proportional,Helvetica,Bold,Point,6",
                  OUTPUT time-font, OUTPUT rx, OUTPUT cx ).

  RUN make-control-string( "PCL", "Proportional,Helvetica,Bold,Point,8",
                  OUTPUT column-font, OUTPUT rx, OUTPUT cx ).

  RUN make-control-string( "PCL", "Proportional,Helvetica,Bold,Point,8",
                  OUTPUT summary-font, OUTPUT rx, OUTPUT cx ).

  RUN make-control-string( "PCL", "Proportional,Helvetica,Bold,Point,12",
                  OUTPUT break-font[1], OUTPUT rx, OUTPUT cx ).

  RUN make-control-string( "PCL", "Proportional,Helvetica,Bold,Point,9",
                  OUTPUT break-font[2], OUTPUT rx, OUTPUT cx ).

  RUN make-control-string( "PCL", "Proportional,Helvetica,Bold,Point,8",
                  OUTPUT break-font[3], OUTPUT rx, OUTPUT cx ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-lease-info) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-lease-info Method-Library 
PROCEDURE get-lease-info :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE TenancyLease THEN RETURN.
  
  FIND FIRST LeaseInfo WHERE LeaseInfo.TenancyLeaseCode = TenancyLease.TenancyLeaseCode NO-ERROR.
  IF AVAILABLE LeaseInfo THEN RETURN.

  CREATE LeaseInfo.

  DEF VAR i AS INT  NO-UNDO.
  ASSIGN LeaseInfo.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
         LeaseInfo.area-name = ?
         LeaseInfo.Carpark = ? .

  RUN get-property-info.
  LeaseInfo.Manager = PropInfo.Manager.

  FOR EACH RentalSpace OF TenancyLease NO-LOCK WHERE RentalSpace.AreaStatus <> "V":
    RUN get-area-info.
    LeaseInfo.TotalArea  = LeaseInfo.TotalArea  + AreaInfo.area-size.
    LeaseInfo.TotalParks = LeaseInfo.TotalParks + AreaInfo.area-parks.
    DO i = 1 TO 2:
      LeaseInfo.Rental[i] = LeaseInfo.Rental[i] + AreaInfo.rntl[i].
      IF AreaInfo.floor-space = "Yes" THEN
        LeaseInfo.AreaRental[i] = LeaseInfo.AreaRental[i] + AreaInfo.rntl[i].
      ELSE IF AreaInfo.floor-space = "Park" THEN
        LeaseInfo.ParkRental[i] = LeaseInfo.ParkRental[i] + AreaInfo.rntl[i].
    END.
    IF LeaseInfo.Carpark = ? AND AreaInfo.floor-space = "Park" THEN
      LeaseInfo.Carpark = Yes.
    ELSE IF AreaInfo.floor-space <> "Park" THEN
      LeaseInfo.Carpark = No.

    IF RentalSpace.RentalSpaceCode = Tenancylease.PrimarySpace THEN
      LeaseInfo.area-name = AreaInfo.area-name .
    DELETE AreaInfo.
  END.
  LeaseInfo.opex = TenancyLease.OutgoingsBudget.
  IF LeaseInfo.opex <> 0   THEN DO:
    LeaseInfo.c-opex = STRING( LeaseInfo.opex, "->>,>>>,>>9.99" ).
    LeaseInfo.Rental[3] = LeaseInfo.Rental[2] + LeaseInfo.opex.
    LeaseInfo.AreaRental[3] = LeaseInfo.AreaRental[2] + LeaseInfo.opex.
    Leaseinfo.ear-total = Leaseinfo.Rental[1] + LeaseInfo.opex.
  END.
  ELSE ASSIGN
    LeaseInfo.Rental[3]     = LeaseInfo.Rental[2]
    LeaseInfo.AreaRental[3] = LeaseInfo.AreaRental[2].

  IF LeaseInfo.Carpark = ? THEN LeaseInfo.Carpark = No.

  IF TenancyLease.AreaDescription <> "" AND TenancyLease.AreaDescription <> ? THEN
    LeaseInfo.area-name = TenancyLease.AreaDescription .
  IF LeaseInfo.area-name = ? THEN DO:
    FIND FIRST RentalSpace NO-LOCK OF TenancyLease NO-ERROR.
    IF AVAILABLE(RentalSpace) THEN
      LeaseInfo.area-name = RentalSpace.Description.
  END.
  IF LeaseInfo.area-name = ? THEN LeaseInfo.area-name = "Unknown Rental Space!!!".

  IF LeaseInfo.TotalArea <> 0  THEN LeaseInfo.c-area  = STRING( LeaseInfo.TotalArea, "->>>>>9.99" ).
  IF LeaseInfo.TotalParks <> 0 THEN LeaseInfo.c-parks = STRING( LeaseInfo.TotalParks, ">>>>9").

  LeaseInfo.c-ear-total     = TRIM( STRING( LeaseInfo.ear-total, "->>,>>>,>>9.99" )).
  IF LeaseInfo.c-ear-total  = ?     THEN LeaseInfo.c-ear-total = "" .
  DO i = 1 TO 3:
    LeaseInfo.c-rntl[i]     = TRIM( STRING( LeaseInfo.Rental[i], "->>,>>>,>>9.99" )).
    IF LeaseInfo.c-rntl[i]  = ?     THEN LeaseInfo.c-rntl[i] = "" .
    IF LeaseInfo.Carpark THEN DO:
      LeaseInfo.rent-psm[i]   = LeaseInfo.Rental[i] / Leaseinfo.TotalParks.
      LeaseInfo.c-rent-psm[i]     = TRIM( STRING( LeaseInfo.rent-psm[i], "->>,>>>,>>9.99" )).
      IF LeaseInfo.c-rent-psm[i]  = ?     THEN LeaseInfo.c-rent-psm[i] = "" .
    END.
    ELSE DO:
      LeaseInfo.rent-psm[i]   = LeaseInfo.AreaRental[i] / Leaseinfo.TotalArea.
      LeaseInfo.c-rent-psm[i]     = TRIM( STRING( LeaseInfo.rent-psm[i], "->>,>>>,>>9.99" )).
      IF LeaseInfo.c-rent-psm[i]  = ?     THEN LeaseInfo.c-rent-psm[i] = "" .
    END.
  END.

  Leaseinfo.gross = IF TenancyLease.GrossLease THEN Yes ELSE No.
  Leaseinfo.c-gross = STRING( LeaseInfo.gross, "G/N").

  LeaseInfo.c-start = STRING( TenancyLease.LeaseStartDate, "99/99/9999" ).
  IF LeaseInfo.c-start = ? THEN LeaseInfo.c-start = "".
  IF TenancyLease.LeaseEndDate = ? THEN ASSIGN
    LeaseInfo.c-finish = "Monthly"
    LeaseInfo.c-remaining = LeaseInfo.c-finish
    LeaseInfo.c-term = LeaseInfo.c-finish.
/*
  ELSE IF TenancyLease.LeaseEndDate < TODAY THEN ASSIGN
    LeaseInfo.c-finish = STRING( TenancyLease.LeaseEndDate, "99/99/9999" )STRING( TenancyLease.LeaseEndDate, "99/99/9999" )
    LeaseInfo.c-remaining = LeaseInfo.c-finish
    LeaseInfo.c-term = LeaseInfo.c-finish.
 */
  ELSE DO:
    LeaseInfo.c-finish = STRING( TenancyLease.LeaseEndDate, "99/99/9999" ).
    RUN get-term-remaining( TODAY, TenancyLease.LeaseEndDate, OUTPUT LeaseInfo.c-remaining).
    RUN get-term-remaining( TenancyLease.LeaseStartDate, TenancyLease.LeaseEndDate, OUTPUT LeaseInfo.c-term).
    IF LeaseInfo.c-remaining <> "" THEN LeaseInfo.c-remaining = LeaseInfo.c-remaining + " remaining".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-number-of-days) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-number-of-days Method-Library 
PROCEDURE get-number-of-days :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER yy AS INT NO-UNDO.
DEF INPUT PARAMETER mm AS INT NO-UNDO.
DEF OUTPUT PARAMETER days AS INT NO-UNDO INITIAL 31.

  CASE mm:
    WHEN  2 THEN DO:
      days = 28.
      IF (yy MODULO 4) = 0 THEN days = days + 1.
    END.
    WHEN  4 THEN days = 30.
    WHEN  6 THEN days = 30.
    WHEN  9 THEN days = 30.
    WHEN 11 THEN days = 30.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-previous-rent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-previous-rent Method-Library 
PROCEDURE get-previous-rent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       TenancyLease must be available
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER from-date     AS DATE NO-UNDO.
DEF OUTPUT PARAMETER prev-grs-pa   AS DEC  NO-UNDO.
DEF OUTPUT PARAMETER prev-net-pa   AS DEC  NO-UNDO.

  DEF VAR lease-type  AS CHAR NO-UNDO.
  DEF VAR renew-opex  AS DEC NO-UNDO    INITIAL 0.
  DEF VAR renew-rent  AS DEC NO-UNDO    INITIAL 0.
  DEF VAR area-opex   AS DEC NO-UNDO    INITIAL 0.
  DEF VAR area-rent   AS DEC NO-UNDO    INITIAL 0.
  DEF VAR rsp-list    AS CHAR NO-UNDO   INITIAL "".


  FOR EACH RentSpaceHistory OF TenancyLease WHERE
    RentSpaceHistory.DateChanged >= from-date NO-LOCK
    BY RentSpaceHistory.DateChanged:

    IF LOOKUP( STRING( RentSpaceHistory.RentalSpaceCode ), rsp-list ) = 0 THEN
    DO:
      rsp-list = rsp-list + STRING( RentSpaceHistory.RentalSpaceCode ) + ",".

      area-opex = (PropInfo.opex * (RentSpaceHistory.OutgoingsPercentage / 100)) .
      IF area-opex <> ? THEN renew-opex = renew-opex + area-opex .

      area-rent = RentSpaceHistory.ContractedRental.
      IF area-rent <> ? THEN renew-rent = renew-rent + area-rent.
    END.
  END.

  lease-type = TenancyLease.LeaseType.
/*  IF AVAILABLE(LeaseHistory) THEN lease-type = LeaseHistory.LeaseType. */

  prev-net-pa = renew-rent.
  IF (lease-type = "GROS") THEN prev-net-pa = prev-net-pa - renew-opex .
  prev-grs-pa = prev-net-pa + renew-opex.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-property-info) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-property-info Method-Library 
PROCEDURE get-property-info :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  DEF BUFFER RSP FOR RentalSpace.
  
  IF NOT AVAILABLE Property THEN RETURN.
  IF AVAILABLE(PropInfo) AND PropInfo.PropertyCode = Property.PropertyCode THEN RETURN.
  
  FIND FIRST PropInfo WHERE PropInfo.PropertyCode = Property.PropertyCode NO-ERROR.
  IF AVAILABLE PropInfo THEN RETURN.  
  
  IF use-rent-charges THEN RUN build-lease-rentals.

  CREATE PropInfo.
  ASSIGN PropInfo.PropertyCode = Property.PropertyCode.

  FIND Person WHERE Person.PersonCode = Property.Manager NO-LOCK NO-ERROR.
  IF AVAILABLE(Person) THEN DO:
/*    PropInfo.Manager = Person.Department.
    IF PropInfo.Manager = ? OR PropInfo.Manager = "" THEN
      PropInfo.Manager = Person.JobTitle.
    IF PropInfo.Manager = ? OR PropInfo.Manager = "" THEN */
      PropInfo.Manager = Person.FirstName + " " + Person.LastName.
  END.
  ELSE
    PropInfo.Manager = "No manager for P" + STRING( Property.PropertyCode, "99999").

  DEF VAR rsp-area  AS DEC NO-UNDO.
  DEF VAR rsp-parks AS INT NO-UNDO.
  DEF VAR rsp-rent  AS DEC NO-UNDO.
  DEF VAR vacant-parks AS INT NO-UNDO INITIAL 0.
  DEF VAR leased-parks AS INT NO-UNDO INITIAL 0.
  DEF VAR vacant-cost AS DEC NO-UNDO INITIAL 0.
  DEF VAR vacant-with-cost AS DEC NO-UNDO INITIAL 0.

  FOR EACH RSP OF Property NO-LOCK:
    ASSIGN rsp-area = 0     rsp-parks = 0   rsp-rent = 0 .
    RUN test-floor-space( RSP.AreaType, RSP.AreaSize ).
    IF RETURN-VALUE = "Yes" THEN ASSIGN
      rsp-area  = RSP.AreaSize.
    ELSE IF RETURN-VALUE = "Park" THEN ASSIGN
      rsp-parks = RSP.AreaSize.

    IF use-rent-charges THEN
      rsp-rent = get-area-rental( Property.PropertyCode, RSP.RentalSpaceCode ).
    ELSE
      rsp-rent = RSP.ContractedRental .

    IF RSP.AreaStatus = "V" THEN ASSIGN
      PropInfo.VacantArea = PropInfo.VacantArea + rsp-area
      vacant-cost = vacant-cost + (IF RSP.VacantCosts > 0 THEN RSP.VacantCosts ELSE 0)
      vacant-with-cost = vacant-with-cost + (IF RSP.VacantCosts > 0 THEN rsp-area ELSE 0)
      vacant-parks = vacant-parks + rsp-parks
      PropInfo.LeaseAreaRent = PropInfo.LeaseAreaRent + (IF rsp-area  = 0 THEN 0 ELSE RSP.MarketRental)
      PropInfo.LeaseParkRent = PropInfo.LeaseParkRent + (IF rsp-parks = 0 THEN 0 ELSE RSP.MarketRental) 
      PropInfo.TotalRent = PropInfo.TotalRent + RSP.MarketRental.
    ELSE ASSIGN
      PropInfo.LeasedArea = PropInfo.LeasedArea + rsp-area
      PropInfo.LeaseAreaRent = PropInfo.LeaseAreaRent + (IF rsp-area  = 0 THEN 0 ELSE RSP.ContractedRental)
      PropInfo.LeaseParkRent = PropInfo.LeaseParkRent + (IF rsp-parks = 0 THEN 0 ELSE RSP.ContractedRental)
      leased-parks = leased-parks + rsp-parks
      PropInfo.TotalRent = PropInfo.TotalRent + RSP.ContractedRental.
      
   
  END.  /* for each RSP  */

  PropInfo.TotalArea    = PropInfo.VacantArea + PropInfo.LeasedArea.
  PropInfo.TotalParks   = vacant-parks + leased-parks .

  PropInfo.floor-rate   = PropInfo.LeaseAreaRent / PropInfo.LeasedArea.
  IF PropInfo.floor-rate = ? OR PropInfo.floor-rate = 0 THEN 
    PropInfo.floor-rate = PropInfo.VacantAreaRent / PropInfo.VacantArea.

  PropInfo.park-rate    = PropInfo.LeaseParkRent / leased-parks.
  IF PropInfo.park-rate = ? OR PropInfo.park-rate = 0 THEN 
    PropInfo.park-rate = PropInfo.VacantParkRent / vacant-parks.

  PropInfo.opex         = IF Property.OpexEstimate = ? THEN 0.00 ELSE Property.OpexEstimate.
  PropInfo.opex-psm     = PropInfo.opex / PropInfo.TotalArea.

  IF vacant-cost > 0 THEN
    PropInfo.VacantRate   = vacant-cost / vacant-with-cost .

  PropInfo.MarketRental  = IF Property.MarketRental = ? THEN 0.00 ELSE Property.MarketRental.
  PropInfo.MarketCarpark = IF Property.MarketCarpark = ? THEN 0.00 ELSE Property.MarketCarpark.

/*
DEF WORK-TABLE PropInfo NO-UNDO
  FIELD PropertyCode LIKE Property.PropertyCode
  FIELD LeasedArea     AS DEC
  FIELD VacantArea     AS DEC
  FIELD TotalArea      AS DEC
  FIELD TotalParks     AS INT
  FIELD LeaseAreaRent  AS DEC
  FIELD LeaseParkRent  AS DEC
  FIELD VacantAreaRent AS DEC
  FIELD VacantParkRent AS DEC
  FIELD MarketRental   AS DEC
  FIELD floor-rate     AS DEC
  FIELD park-rate      AS DEC
  FIELD opex-psm       AS DEC
  FIELD opex           AS DEC
  FIELD VacantRate     AS DEC.
*/      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-term) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-term Method-Library 
PROCEDURE get-term :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER c-term AS CHAR NO-UNDO.
  
  IF TenancyLease.TermYears <> ? AND TenancyLease.TermYears <> 0 THEN
    c-term = c-term + STRING( TenancyLease.TermYears ) + "y".

  IF TenancyLease.TermMonths <> ? AND TenancyLease.TermMonths <> 0 THEN
    c-term = c-term + STRING( TenancyLease.TermMonths ) + "m".
 
  IF TenancyLease.TermDays <> ? AND TenancyLease.TermDays <> 0 THEN
    c-term = c-term + STRING( TenancyLease.TermDays ) + "d".
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-term-remaining) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-term-remaining Method-Library 
PROCEDURE get-term-remaining :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER  now      AS DATE NO-UNDO.
DEF INPUT PARAMETER  term-end AS DATE NO-UNDO.
DEF OUTPUT PARAMETER c-term   AS CHAR NO-UNDO.
  
  DEF VAR yy AS INT NO-UNDO.
  DEF VAR mm AS INT NO-UNDO.
  DEF VAR dd AS INT NO-UNDO.
  
  RUN calc-date-diff( now, term-end, OUTPUT yy, OUTPUT mm, OUTPUT dd ).

  IF yy <> ? AND yy <> 0 THEN c-term = c-term + STRING( yy ) + "y".
  IF mm <> ? AND mm <> 0 THEN c-term = c-term + STRING( mm ) + "m".
  IF dd <> ? AND dd <> 0 THEN c-term = c-term + STRING( dd ) + "d".
  IF c-term = ? THEN c-term = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-line-to-file) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE line-to-file Method-Library 
PROCEDURE line-to-file :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER data AS CHAR NO-UNDO.
DEF VAR i           AS INT  NO-UNDO.
DEF VAR detail-str  AS CHAR NO-UNDO.
DEF VAR i-max AS INT NO-UNDO.
i-max = MINIMUM( n-columns, NUM-ENTRIES( data, delim)).

  DO i = 1 TO n-breaks:
    IF detail-str <> "" THEN detail-str = detail-str + ",".
    detail-str = detail-str + '"' + brk-val[i] + '"'.
  END.
  
  DO i = 1 TO i-max:
    IF detail-str <> "" THEN detail-str = detail-str + ",".
    detail-str = detail-str + '"' + TRIM( ENTRY( i, data, delim )) + '"'.
  END.

  PUT UNFORMATTED detail-str SKIP.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-line-to-printer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE line-to-printer Method-Library 
PROCEDURE line-to-printer :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER is-detail AS LOGI NO-UNDO.
DEF INPUT PARAMETER data      AS CHAR NO-UNDO.

DEF VAR space-before AS INT  NO-UNDO.
DEF VAR space-after  AS INT  NO-UNDO.
DEF VAR fmt-length   AS INT  NO-UNDO.
DEF VAR item         AS CHAR NO-UNDO.
DEF VAR item-wrap    AS CHAR NO-UNDO.
DEF VAR list-wrap    AS CHAR NO-UNDO.
DEF VAR sub-ln       AS INT  NO-UNDO INIT 0.
DEF VAR w            AS INT  NO-UNDO.
DEF VAR still-lines  AS LOGI NO-UNDO INIT Yes.
DEF VAR no-lines     AS INT  NO-UNDO INIT 1.
DEF VAR i AS INT NO-UNDO.
DEF VAR i-max AS INT NO-UNDO.
i-max = MINIMUM( n-columns, NUM-ENTRIES( data, delim)).

  list-wrap = FILL( delim, n-columns).
  DO sub-ln = 1 TO no-lines:

    DO i = 1 TO i-max:

      item         = IF sub-ln = 1 THEN ENTRY( i, data, delim ) ELSE "".
      fmt-length   = LENGTH( STRING( "", ENTRY( i, format-list, delim ) ) ).

      IF ENTRY( i, wrap-list, delim ) = "Y"
         OR (NOT is-detail AND INDEX( item, "~n") <> 0 )
         OR ENTRY( i, list-wrap, delim ) <> "" THEN
      DO:
        IF sub-ln = 1 THEN DO:
          IF is-detail THEN RUN word-wrap IN THIS-PROCEDURE( item, fmt-length, OUTPUT item-wrap ).
                       ELSE item-wrap = item.
          ENTRY( i, list-wrap, delim ) = item-wrap.
          no-lines = MAXIMUM( no-lines, NUM-ENTRIES( item-wrap, "~n" )).
          ASSIGN item = ENTRY( sub-ln, item-wrap, "~n" ) NO-ERROR.
        END.
        ELSE DO:
          ASSIGN item = ENTRY( sub-ln, ENTRY( i, list-wrap, delim ), "~n" ) NO-ERROR.
        END.
        IF item = ? THEN item = "".
      END.

      IF LENGTH( item ) > fmt-length THEN item = SUBSTR( item, 1, fmt-length ).

      CASE ENTRY( i, align-list, delim ):
        WHEN "L" THEN space-before = 0.
        WHEN "C" THEN space-before = ( fmt-length - LENGTH( item ) ) / 2.
        WHEN "R" THEN space-before = fmt-length - LENGTH( item ).
      END CASE.

      space-after  = fmt-length - LENGTH( item ) - space-before.
    
      item = FILL( ' ', space-before ) + item + FILL( ' ', space-after ).

      PUT UNFORMATTED 
        STRING( item, ENTRY( i, format-list, delim ) )
        SPACE( INT( ENTRY( i, gap-list ) ) ).
        
    END.

    RUN skip-line IN THIS-PROCEDURE(1).
    
  END.

  IF is-detail THEN brk-lines = brk-lines + 1.  
  RUN skip-line IN THIS-PROCEDURE( 0.5 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-page-footer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE page-footer Method-Library 
PROCEDURE page-footer :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF file-name <> "Printer" THEN RETURN.
  page-no = page-no + 1.
  IF page-no < 2 THEN RETURN.
  PUT CONTROL CHR(12).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-page-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE page-header Method-Library 
PROCEDURE page-header :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF BUFFER HeaderDetail FOR PeriodicDetail.

DEF VAR i AS INT NO-UNDO.

  IF page-no < 2 THEN PUT CONTROL reset-page.
  ln = 0.

  PUT CONTROL line-printer.
  IF file-name = "Printer" THEN RUN skip-line(2).
  
  PUT CONTROL time-font.
  RUN skip-line(1.5).
  PUT UNFORMATTED SPACE(300)
    STRING(
      "Printed: " + now + " for " + user-name,
      "X(100)" ).
  RUN skip-line(1).    
  PUT UNFORMATTED SPACE(300)
      STRING( "Page: " + STRING( page-no ), "X(20)" ).
  RUN skip-line(1).

  PUT CONTROL title-font.
  DO i = 1 TO NUM-ENTRIES( report-title, "~n" ):
    IF i > 1 THEN   PUT CONTROL title2-font.
    PUT UNFORMATTED ENTRY( i, report-title, "~n").
    RUN skip-line(1.5).  
  END.

  PUT CONTROL time-font.
  PUT UNFORMATTED SPACE(300) organisation-name.
  PUT CONTROL line-printer.
  RUN skip-line(2).

  IF file-name = "Printer" THEN RUN line-to-printer( No, field-list ).
                           ELSE RUN line-to-file( field-list ).
  
  RUN skip-line(1).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reset-totals) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-totals Method-Library 
PROCEDURE reset-totals :
/*------------------------------------------------------------------------------

  Sometimes used in generation phase - can probably be deleted now.
  
  Purpose:  Reset the totals for a particular level, adding them to the next.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER break-level AS INT NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR bl AS INT NO-UNDO.

  bl = IF break-level = 0 THEN 4 ELSE break-level.

  brk-area[bl] = 0.00.
  brk-cost[bl] = 0.00.
  DO i = 1 TO 3:
    brk-rntl[ (bl - 1) * 3 + i ] = 0.00.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-lease-fields) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-lease-fields Method-Library 
PROCEDURE set-lease-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.

DEF VAR s-name AS CHAR NO-UNDO.
DEF VAR p-name AS CHAR NO-UNDO.
DEF VAR tot-it AS CHAR NO-UNDO.
DEF VAR fmt-it AS CHAR NO-UNDO.
DEF VAR wrpp AS CHAR NO-UNDO.
DEF VAR algn AS CHAR NO-UNDO.
DEF VAR tot-fmt AS CHAR NO-UNDO.

  n = NUM-ENTRIES( xlate-list ).
  field-list  = FILL( delim, n).
  total-list  = field-list.
  total-formats= field-list.
  wrap-list   = field-list.
  align-list  = field-list.
  format-list = field-list.
  gap-list    = FILL( "1,", n).

  DO i = 1 TO n.
    tot-fmt = "".
    CASE ENTRY( i, xlate-list ):
      WHEN "1" THEN ASSIGN p-name = "Property"              s-name = "Property-Short-Name"
                           fmt-it = "X(12)"   tot-it = "N"  wrpp = "N"  algn = "L".
      WHEN "2" THEN ASSIGN p-name = "Tenant Name"           s-name = "Tenant-Name"
                           fmt-it = "X(30)"   tot-it = "N"  wrpp = "Y"  algn = "L".
      WHEN "3" THEN ASSIGN p-name = "Lease Description"     s-name = "Lease-Description"
                           fmt-it = "X(20)"   tot-it = "N"  wrpp = "Y"  algn = "L".
      WHEN "4" THEN ASSIGN p-name = "Gross/~nNet"          s-name = "Gross-Lease"
                           fmt-it = "X(5)"    tot-it = "N"  wrpp = "N"  algn = "C".
      WHEN "5" THEN ASSIGN p-name = "Area"                  s-name = "Area"
                           fmt-it = "X(10)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = ">>>,>>9.99".
      WHEN "6" THEN ASSIGN p-name = "Parks"                 s-name = "Parks"
                           fmt-it = "X(6)"    tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = ">,>>9".
      WHEN "7" THEN ASSIGN p-name = "Budget~nOPEX P.A"      s-name = "OPEX-Budget-PA"
                           fmt-it = "X(14)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "8" THEN ASSIGN p-name = "EAR Base~nRent P.A"   s-name = "EAR-Base-Rent-PA"
                           fmt-it = "X(14)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "9" THEN ASSIGN p-name = "EAR Base~nRent PSM"   s-name = "EAR-Base-Rent-PSM"
                           fmt-it = "X(9)"    tot-it = "N"  wrpp = "N"  algn = "R".
      WHEN "10" THEN ASSIGN p-name = "Base~nRent P.A"       s-name = "Base-Rent-PA"
                           fmt-it = "X(14)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "11" THEN ASSIGN p-name = "Base~nRent PSM"       s-name = "Base-Rent-PSM"
                           fmt-it = "X(9)"    tot-it = "N"  wrpp = "N"  algn = "R".
      WHEN "12" THEN ASSIGN p-name = "Gross~nRent P.A"      s-name = "Total-Rent-PA"
                           fmt-it = "X(14)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "13" THEN ASSIGN p-name = "Total~nRent PSM"      s-name = "Total-Rent-PSM"
                           fmt-it = "X(9)"    tot-it = "N"  wrpp = "N"  algn = "R".
      WHEN "14" THEN ASSIGN p-name = "Lease Start"          s-name = "Lease-Start"
                           fmt-it = "X(11)"   tot-it = "N"  wrpp = "N"  algn = "C".
      WHEN "15" THEN ASSIGN p-name = "Lease End"            s-name = "Lease-End"
                           fmt-it = "X(10)"   tot-it = "N"  wrpp = "N"  algn = "C".
      WHEN "16" THEN ASSIGN p-name = "Remaining"            s-name = "Remaining-Term"
                           fmt-it = "X(10)"   tot-it = "N"  wrpp = "N"  algn = "L".
      WHEN "17" THEN ASSIGN p-name = "Term"                 s-name = "Full-Term"
                           fmt-it = "X(10)"   tot-it = "N"  wrpp = "N"  algn = "L".
      WHEN "18" THEN ASSIGN p-name = "Previous~nBase P.A"   s-name = "Prev-Base-Rent"
                           fmt-it = "X(14)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "19" THEN ASSIGN p-name = "Previous~nBase PSM"   s-name = "Prev-Base-PSM"
                           fmt-it = "X(9)"    tot-it = "N"  wrpp = "N"  algn = "R".
      WHEN "20" THEN ASSIGN p-name = "Previous~nTotal P.A"  s-name = "Prev-Total-Rent"
                           fmt-it = "X(14)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "21" THEN ASSIGN p-name = "Previous~nTotal PSM"  s-name = "Prev-Total-PSM"
                           fmt-it = "X(9)"    tot-it = "N"  wrpp = "N"  algn = "R".
      WHEN "22" THEN ASSIGN p-name = "EAR Gross~nRent P.A"  s-name = "EAR-Total-Rent-PA"
                           fmt-it = "X(14)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "23" THEN ASSIGN p-name = "Change Date"          s-name = "Change-Date"
                           fmt-it = "X(11)"   tot-it = "N"  wrpp = "N"  algn = "C".
      WHEN "24" THEN ASSIGN p-name = "Change~nType"         s-name = "Change-Type"
                           fmt-it = "X(10)"    tot-it = "N"  wrpp = "N"  algn = "C".
      WHEN "25" THEN ASSIGN p-name = "Manager"              s-name = "Manager"
                           fmt-it = "X(20)"   tot-it = "N"  wrpp = "Y"  algn = "L".
      WHEN "26" THEN ASSIGN p-name = "Notice Due"          s-name = "Notice-Due"
                           fmt-it = "X(11)"   tot-it = "N"  wrpp = "N"  algn = "C".
      WHEN "C" THEN ASSIGN p-name = "Comment"               s-name = "Comment"
                           fmt-it = "X(50)"   tot-it = "N"  wrpp = "Y"  algn = "L".
    END CASE.

    IF file-name = "Printer" THEN
      ENTRY( i, field-list, delim) = p-name.
    ELSE
      ENTRY( i, field-list, delim) = s-name.

    ENTRY( i, total-list, delim)   = tot-it.
    ENTRY( i, total-formats, delim)= tot-fmt.
    ENTRY( i, wrap-list, delim)    = wrpp.
    ENTRY( i, align-list, delim)   = algn.
    ENTRY( i, format-list, delim)  = fmt-it.
  END.

/*
  MESSAGE "field-list " field-list.
  MESSAGE "total-list " total-list.
  MESSAGE "wrap-list " wrap-list.
  MESSAGE "align-list " align-list.
  MESSAGE "total-formats " total-formats.
  MESSAGE "format-list " format-list.
*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-rentalspace-fields) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-rentalspace-fields Method-Library 
PROCEDURE set-rentalspace-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.

DEF VAR s-name AS CHAR NO-UNDO.
DEF VAR p-name AS CHAR NO-UNDO.
DEF VAR tot-it AS CHAR NO-UNDO.
DEF VAR fmt-it AS CHAR NO-UNDO.
DEF VAR wrpp AS CHAR NO-UNDO.
DEF VAR algn AS CHAR NO-UNDO.
DEF VAR tot-fmt AS CHAR NO-UNDO.

  n = NUM-ENTRIES( xlate-list ).
  field-list  = FILL( delim, n).
  total-list  = field-list.
  total-formats= field-list.
  wrap-list   = field-list.
  align-list  = field-list.
  format-list = field-list.
  gap-list    = FILL( "2,", n).

  DO i = 1 TO n.
    tot-fmt = "".
    CASE ENTRY( i, xlate-list ):
      WHEN "1" THEN ASSIGN p-name = "Space Description"     s-name = "SpaceDescription"
                           fmt-it = "X(50)"   tot-it = "N"  wrpp = "Y"  algn = "L".
      WHEN "2" THEN ASSIGN p-name = "Floor~nSpace"          s-name = "FloorSpace"
                           fmt-it = "X(5)"    tot-it = "N"  wrpp = "N"  algn = "C".
      WHEN "3" THEN ASSIGN p-name = "Area"                  s-name = "Area"
                           fmt-it = "X(10)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = ">>>,>>9.99".
      WHEN "4" THEN ASSIGN p-name = "Parks"                 s-name = "Parks"
                           fmt-it = "X(6)"    tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = ">,>>9".
      WHEN "5" THEN ASSIGN p-name = "Budget~nOPEX P.A"      s-name = "OPEX-Budget-PA"
                           fmt-it = "X(14)"    tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "6" THEN ASSIGN p-name = "O/G %"                 s-name = "Outgoings-Percent"
                           fmt-it = "X(7)"    tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>9.99".
      WHEN "7" THEN ASSIGN p-name = "EAR Total~nRent P.A"   s-name = "EAR-Total-Rent-PA"
                           fmt-it = "X(14)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "8" THEN ASSIGN p-name = "EAR Total~nRent PSM"   s-name = "EAR-Total-Rent-PSM"
                           fmt-it = "X(9)"    tot-it = "N"  wrpp = "N"  algn = "R".
      WHEN "9" THEN ASSIGN p-name = "Base~nRent P.A"        s-name = "Base-Rent-PA"
                           fmt-it = "X(14)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "10" THEN ASSIGN p-name = "Base~nRent PSM"       s-name = "Base-Rent-PSM"
                           fmt-it = "X(9)"    tot-it = "N"  wrpp = "N"  algn = "R".
      WHEN "11" THEN ASSIGN p-name = "Total~nRent P.A"      s-name = "Total-Rent-PA"
                           fmt-it = "X(14)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "12" THEN ASSIGN p-name = "Total~nRent PSM"      s-name = "Total-Rent-PSM"
                           fmt-it = "X(9)"    tot-it = "N"  wrpp = "N"  algn = "R".
      WHEN "13" THEN ASSIGN p-name = "Holding~nCosts P.A"   s-name = "Holding-Costs-PA"
                           fmt-it = "X(14)"   tot-it = "Y"  wrpp = "N"  algn = "R"   tot-fmt = "->>,>>>,>>9.99".
      WHEN "14" THEN ASSIGN p-name = "Holding~nCosts PSM"   s-name = "Holding-Costs-PSM"
                           fmt-it = "X(9)"    tot-it = "N"  wrpp = "N"  algn = "R".
      WHEN "15" THEN ASSIGN p-name = "Vacant~nSince"        s-name = "Vacant-Since"
                           fmt-it = "X(10)"   tot-it = "N"  wrpp = "N"  algn = "C".
      WHEN "16" THEN ASSIGN p-name = "% Area~nBldg"   s-name = "%-Area-Bldg"
                           fmt-it = "X(6)"   tot-it = "Y"  wrpp = "N"  algn = "R" tot-fmt = ">>9.99".  
      WHEN "17" THEN ASSIGN p-name = "% Rent~nBldg"   s-name = "%-Rent-Bldg"
                           fmt-it = "X(6)"   tot-it = "Y"  wrpp = "N"  algn = "R" tot-fmt = ">>9.99".  
      WHEN "C" THEN ASSIGN p-name = "Comment"               s-name = "Comment"
                           fmt-it = "X(60)"   tot-it = "N"  wrpp = "Y"  algn = "L".
    END CASE.

    IF file-name = "Printer" THEN
      ENTRY( i, field-list, delim) = p-name.
    ELSE
      ENTRY( i, field-list, delim) = s-name.

    ENTRY( i, total-list, delim)   = tot-it.
    ENTRY( i, total-formats, delim)= tot-fmt.
    ENTRY( i, wrap-list, delim)    = wrpp.
    ENTRY( i, align-list, delim)   = algn.
    ENTRY( i, format-list, delim)  = fmt-it.
  END.

/*
 *   MESSAGE "field-list " field-list.
 *   MESSAGE "total-list " total-list.
 *   MESSAGE "wrap-list " wrap-list.
 *   MESSAGE "align-list " align-list.
 *   MESSAGE "total-formats " total-formats.
 *   MESSAGE "format-list " format-list.
 * */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-skip-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE skip-line Method-Library 
PROCEDURE skip-line :
/*------------------------------------------------------------------------------
  Purpose:  Skip forward a number of lines, or to new page.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER n AS DEC NO-UNDO.

  IF file-name = "Printer" AND ln + n >= lines-per-page THEN
  DO:
    RUN page-footer IN THIS-PROCEDURE.
    RUN page-header IN THIS-PROCEDURE.
    RETURN.
  END.

  DEF VAR int-part AS INT NO-UNDO.
  DEF VAR dec-part AS DEC NO-UNDO.
  
  int-part = TRUNCATE( n, 0 ).
  IF int-part < 0 THEN RETURN.
  dec-part = n - int-part.
  IF int-part = 0 AND dec-part = 0 THEN RETURN.

  /* Need to have this like the following - do not touch */  
  IF int-part = 1 THEN PUT " " SKIP.
  ELSE IF int-part > 1 THEN PUT SKIP(int-part).
  IF dec-part <> 0 THEN PUT CONTROL half-line.
    
  ln = ln + n.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-test-floor-space) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE test-floor-space Method-Library 
PROCEDURE test-floor-space :
/*------------------------------------------------------------------------------
  Purpose:  Decide if this is actual floor space
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER type AS CHAR NO-UNDO.
DEF INPUT PARAMETER area AS DECIMAL NO-UNDO.

DEF BUFFER LocAreaType FOR AreaType.

  IF area = ? THEN RETURN "No".
  FIND LocAreaType WHERE LocAreaType.AreaType = type NO-LOCK NO-ERROR.
  IF AVAILABLE(LocAreaType) THEN DO:
    IF LocAreaType.IsCarPark THEN RETURN "Park".
    IF LocAreaType.IsFloorArea THEN RETURN "Yes".
    RETURN "No".
  END.

  CASE type:
    WHEN "C" THEN RETURN "Park".
    WHEN "O" THEN RETURN "Yes".
    WHEN "R" THEN RETURN "Yes".
    WHEN "W" THEN RETURN "Yes".
    WHEN "N" THEN RETURN "No".
    OTHERWISE
      IF area > 20 OR (area <> INTEGER(area)) THEN RETURN "Yes".
  END CASE.

  RETURN "No".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-total-separator) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE total-separator Method-Library 
PROCEDURE total-separator :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF VAR fmt-length AS INT NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR tot-fmt AS CHAR NO-UNDO.

  DO i = 1 TO NUM-ENTRIES( total-list, delim ):
    tot-fmt = ENTRY( i, format-list, delim).
    fmt-length = LENGTH( STRING( "", tot-fmt ) ).
    PUT UNFORMATTED 
      FILL( (IF ENTRY( i, total-list, delim ) = "Y" THEN '-' ELSE ' '), fmt-length )
      SPACE( INT( ENTRY( i, gap-list ) ) ).
  END.
 
  RUN skip-line(1).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-to-annual) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION to-annual Method-Library 
FUNCTION to-annual RETURNS DECIMAL
  ( INPUT freq-code AS CHAR, INPUT period-amount AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert period amount to annual figure
    Notes:  
------------------------------------------------------------------------------*/
  /* short circuit for most cases */
  IF freq-code = "MNTH" THEN RETURN (period-amount * 12.0).

  FIND FrequencyType WHERE FrequencyType.FrequencyCode = freq-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(FrequencyType) THEN RETURN (period-amount * 12.0). /* assume monthly! */

  IF FrequencyType.RepeatUnits BEGINS "M" THEN
    RETURN ((period-amount / DEC(FrequencyType.UnitCount)) * 12.0 ).

  RETURN ((period-amount / DEC(FrequencyType.UnitCount)) * 365.0 ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

