&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*------------------------------------------------------------------------
    Library     : MIME EMail
    Purpose     : Send MIME encoded email (email with attachments)
    Description : Generate a MIME encoded email stream, and send it to an SMTP server
                  for delivery. WARNING: NO SSL SUPPORT
    Author(s)   : Chris Eade
    Created     : 15/10/2007 - Copyright GPL v3 or any later version
    Notes       : Requires: m-base64.i
    
    How to use this include..
       
    1) CREATE NEW MESSAGE:
        
        MIME_New( "someone.else@other.com", "sender@foo.bar.com", "This is a test message subject" ).
        
    2) ATTACH SOME PLAIN TEXT CONTENT:
    
        MIME_Body( "This is the body of a multipart test message", "plain" ).

    3) OR SOME HTML CONTENT:
    
        MIME_Body( "<h3>The heading</h3><p>Paragraph</p>", "html" ).

    4) OR SOME IMAGES:
    
        MIME_AttachImage( "C:\foo.png", "png", "attached.png", "" ).
    
    5) OR SOMETHING ELSE:
    
        MIME_AttachFile( C:\foo.pdf", 'application/octet-stream' ).
        
    6) SEND EMAIL WITHOUT SMTP AUTHENTICATION:
        
        RUN MIME_Send( "unauth.bar.com", 25 ).

    7) OR SEND EMAIL WITH SMTP AUTHENTICATION:
    
        MIME_Auth( "bob@bar.com", "mypass33" ).
        RUN MIME_Send( "semi-secure.bar.com", 25 ).

------------------------------------------------------------------------*/
    
/* Definitions */

/* Maximum recipient addresses and maximum attachments */
&SCOPED-DEFINE SMTP-MAX-ADDR 20
&SCOPED-DEFINE SMTP-MAX-ATTCH 10

DEF TEMP-TABLE SMTPAddress NO-UNDO
    FIELD MailCode AS INTEGER
    FIELD Address AS CHARACTER INIT ""
    FIELD RecipType AS CHARACTER INIT "".

DEF TEMP-TABLE SMTPAttach NO-UNDO
    FIELD MailCode AS INTEGER
    FIELD AttType AS CHARACTER
    FIELD AttEncoding AS CHARACTER
    FIELD AttExternalFile AS LOGICAL
    FIELD AttFileOrChar AS CHARACTER
    FIELD AttParameters AS CHARACTER.

/* This is available for inspection apon error */
DEF VAR smtp-internal-error AS CHARACTER NO-UNDO.

/* The current working message identifier, and message count */
DEF VAR smtp-internal-wm AS INTEGER INIT 0 NO-UNDO.
DEF VAR smtp-internal-mcount AS INTEGER INIT 0 NO-UNDO.

/* Set by MIME_New. These end up being a CSV list for each MailCode */
DEF VAR smtp-internal-from AS CHARACTER INIT "" NO-UNDO.
DEF VAR smtp-internal-subject AS CHARACTER INIT "" NO-UNDO.

/* Flags for if auth is supported after sending EHLO */
DEF VAR smtp-internal-auth-login AS LOGICAL NO-UNDO.
DEF VAR smtp-internal-auth-plain AS LOGICAL NO-UNDO.

/* Auth details store */
DEF VAR smtp-internal-try-auth AS LOGICAL NO-UNDO.
DEF VAR smtp-internal-auth-username AS CHARACTER NO-UNDO.
DEF VAR smtp-internal-auth-password AS CHARACTER NO-UNDO.

/* Misc */
DEF VAR smtp-internal-dow AS CHARACTER INIT "Mon,Tue,Wed,Thu,Fri,Sat,Sun" NO-UNDO.
DEF VAR smtp-internal-moy AS CHARACTER INIT "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec" NO-UNDO.
DEF VAR smtp-internal-timezone AS CHARACTER NO-UNDO.

/* Attachments */
DEF VAR boundry AS CHARACTER NO-UNDO.
DEF STREAM emailfile.
DEF STREAM incomingfile.
DEF STREAM connectlog.

/* TCP socket for connecting to SMTP server */
DEFINE VARIABLE mailSocket AS HANDLE.
CREATE SOCKET mailSocket.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-MIME_Add_Bcc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MIME_Add_Bcc Method-Library 
FUNCTION MIME_Add_Bcc RETURNS LOGICAL
  ( to-address AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_Add_Cc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MIME_Add_Cc Method-Library 
FUNCTION MIME_Add_Cc RETURNS LOGICAL
  ( to-address AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_Add_To) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MIME_Add_To Method-Library 
FUNCTION MIME_Add_To RETURNS LOGICAL
  ( to-address AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_AttachFile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MIME_AttachFile Method-Library 
FUNCTION MIME_AttachFile RETURNS LOGICAL
(
    file-location AS CHARACTER,
    type-string AS CHARACTER,
    client-file-name AS CHARACTER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_AttachImage) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MIME_AttachImage Method-Library 
FUNCTION MIME_AttachImage RETURNS LOGICAL
(
    file-location AS CHARACTER,
    image-type AS CHARACTER,
    client-file-name AS CHARACTER,
    image-id AS CHARACTER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_Auth) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MIME_Auth Method-Library 
FUNCTION MIME_Auth RETURNS LOGICAL
(
    smtp-user AS CHARACTER,
    smtp-pass AS CHARACTER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_Body) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MIME_Body Method-Library 
FUNCTION MIME_Body RETURNS LOGICAL
(
    body-string AS CHARACTER,
    body-type AS CHARACTER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_New) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MIME_New Method-Library 
FUNCTION MIME_New RETURNS INTEGER
(
    to-address AS CHARACTER,
    from-address AS CHARACTER,
    subject-text AS CHARACTER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_Reset) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MIME_Reset Method-Library 
FUNCTION MIME_Reset RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_SetTimezone) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MIME_SetTimezone Method-Library 
FUNCTION MIME_SetTimezone RETURNS LOGICAL
  ( input-string AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_SwitchMessage) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD MIME_SwitchMessage Method-Library 
FUNCTION MIME_SwitchMessage RETURNS LOGICAL
  ( message-id AS INTEGER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-append) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD smtp-internal-append Method-Library 
FUNCTION smtp-internal-append RETURNS CHARACTER
(
    new-string AS CHARACTER,
    dest-string AS CHARACTER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-attach) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD smtp-internal-attach Method-Library 
FUNCTION smtp-internal-attach RETURNS LOGICAL
(
    type-string AS CHARACTER,
    parameter-string AS CHARACTER,
    as-file AS LOGICAL,
    content-params AS CHARACTER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = 30.32
         WIDTH              = 76.6.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{O:\inc\method\m-base64.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-MIME_Send) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE MIME_Send Method-Library 
PROCEDURE MIME_Send :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER server-address AS CHARACTER NO-UNDO.
DEF INPUT PARAMETER server-port AS INTEGER NO-UNDO.

DEF VAR connect-message AS CHARACTER NO-UNDO.

/* Check a server was supplied */
IF server-address = ? OR server-address = "" THEN DO:
    smtp-internal-error = "No SMTP server address defined".
    RETURN.
END.

/* Default to port 25 */
IF server-port = ? THEN
    server-port = 25.

/* Connect and parse server options */
RUN smtp-internal-connect( INPUT server-address, INPUT server-port ).

/* If there was a problem connecting then return */
IF smtp-internal-error <> "" THEN
    RETURN.

/* Attempt logins if the server supports it */
IF smtp-internal-auth-username <> ? AND smtp-internal-auth-username <> "" THEN DO:
    IF smtp-internal-auth-plain THEN DO:
        RUN smtp-internal-login( INPUT "P", INPUT smtp-internal-auth-username, INPUT smtp-internal-auth-password ).
    END.
    ELSE IF smtp-internal-auth-login THEN DO:
        RUN smtp-internal-login( INPUT "L", INPUT smtp-internal-auth-username, INPUT smtp-internal-auth-password ).
    END.
END.

RUN smtp-internal-build-email.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE smtp-internal-address Method-Library 
PROCEDURE smtp-internal-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER text-string AS CHARACTER NO-UNDO.
DEF VAR response-string AS CHARACTER NO-UNDO.

RUN smtp-internal-write( text-string ).
RUN smtp-internal-read( OUTPUT response-string ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-build-email) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE smtp-internal-build-email Method-Library 
PROCEDURE smtp-internal-build-email :
/*------------------------------------------------------------------------------
  Purpose: Build up the emails and send them to the SMTP server   
  Parameters:  <none>
  Notes:   Assumes that a connection has been established and that authentication
           has been done (if required)    
------------------------------------------------------------------------------*/
DEF VAR v-count AS INTEGER NO-UNDO.
DEF VAR param-count AS INTEGER NO-UNDO.
DEF VAR message-count AS INTEGER NO-UNDO.
DEF VAR incoming-line AS CHARACTER NO-UNDO.
DEF VAR response-code AS CHARACTER NO-UNDO.
DEF VAR date-string AS CHARACTER NO-UNDO.

OUTPUT STREAM emailfile TO VALUE( "C:\emailfile.txt" ).

/* For each message */
DO message-count = 1 TO smtp-internal-mcount:

    /* Construct a MIME boundry */
    boundry = 'BAGGINS' + STRING( TODAY, "99999999" ) + STRING( TIME ) + STRING( message-count ).

    /* --- SMTP Envelope --- */
    RUN smtp-internal-put(
        INPUT "MAIL FROM: " + ENTRY( message-count, smtp-internal-from ) + "~r~n",
        OUTPUT response-code
    ).
    IF response-code <> "250" THEN DO:
        smtp-internal-error = 'From address "' + smtp-internal-from + '" not accepted'.
        RETURN.
    END.
    FOR EACH SMTPAddress WHERE SMTPAddress.MailCode = message-count:
        RUN smtp-internal-put(
            INPUT "RCPT TO: " + SMTPAddress.Address + "~n",
            OUTPUT response-code
        ).
        IF response-code <> "250" THEN DO:
            smtp-internal-error = 'Recipient address "' + SMTPAddress.Address + '" not accepted'.
            RETURN.
        END.
    END.
    RUN smtp-internal-put(
        INPUT "DATA~n",
        OUTPUT response-code
    ).
    IF response-code <> "354" THEN DO:
        smtp-internal-error = 'Message start command not accepted'.
        RETURN.
    END.
    /* --- Envelope end --- */


    /* Put the Date */
    date-string = ENTRY( WEEKDAY( TODAY ), smtp-internal-dow, ',' ) + ', ' +
        STRING( DAY( TODAY ) ) + ' ' +
        ENTRY( MONTH( TODAY ), smtp-internal-moy, ',' ) + ' ' +
        STRING( YEAR( TODAY ) ) + ' ' +
        STRING( TIME, "HH:MM:SS" ).
    IF smtp-internal-timezone <> '' THEN
        date-string = date-string + ' ' + smtp-internal-timezone.

    RUN smtp-internal-write( INPUT "Date: " + date-string ).

    /* Put the From address */
    RUN smtp-internal-write( INPUT "From: " + ENTRY( message-count, smtp-internal-from ) + "~n" ).

    /* Put the recipient addresses */
    FOR EACH SMTPAddress WHERE SMTPAddress.MailCode = message-count AND
        SMTPAddress.RecipType <> "Bcc":
        RUN smtp-internal-write( INPUT SMTPAddress.RecipType + ": " + SMTPAddress.Address + "~n" ).
    END.

    /* Put the Subject line */
    RUN smtp-internal-write( INPUT "Subject: " + ENTRY( message-count, smtp-internal-subject ) + "~n" ).

    /* MIME header and client identifier */
    RUN smtp-internal-write( INPUT "MIME-version: 1.0~n" ).
    RUN smtp-internal-write( INPUT "Great-Progress-Emailer: 0.001~n" ).
    RUN smtp-internal-write( INPUT 'Content-type: multipart/mixed; boundary="' + boundry + '"~n' ).
    RUN smtp-internal-write( INPUT "~n" ).
    RUN smtp-internal-write( INPUT "This is a multi-part message in MIME format.~n" ).

    /* Attachments */
    FOR EACH SMTPAttach WHERE SMTPAttach.MailCode = message-count:

        /* Write the header for this component */
        RUN smtp-internal-write( INPUT "--" + boundry + "~n" ).
        RUN smtp-internal-write( "Content-type: " + SMTPAttach.AttType + "~n" ).

        IF SMTPAttach.AttEncoding <> "" THEN
            RUN smtp-internal-write( INPUT "Content-transfer-encoding: " + SMTPAttach.AttEncoding + "~n" ).

        IF SMTPAttach.AttParameters <> "" THEN DO:
            DO param-count = 1 TO NUM-ENTRIES( SMTPAttach.AttParameters ):
                RUN smtp-internal-write( INPUT ENTRY( param-count, SMTPAttach.AttParameters, ',' ) + "~n" ).
            END.
        END.

        RUN smtp-internal-write( "~n" ).

        IF SMTPAttach.AttExternalFile THEN DO:
            /* Stream the file to the server */
            FILE-INFO:FILE-NAME = SMTPAttach.AttFileOrChar.
            IF FILE-INFO:FILE-MOD-DATE = ? THEN DO:
                RETURN.
            END.

            INPUT STREAM incomingfile FROM VALUE( SMTPAttach.AttFileOrChar ).
            REPEAT:
                IMPORT STREAM incomingfile UNFORMATTED incoming-line.
                RUN smtp-internal-write( INPUT incoming-line + "~n" ).
            END.
            INPUT STREAM incomingfile CLOSE.
        END.
        ELSE DO:
            /* Send the text to the server */
            RUN smtp-internal-write( INPUT SMTPAttach.AttFileOrChar + "~n" ).
        END.
    END.
    /* This message is finished, so write the boundry */
    RUN smtp-internal-write( INPUT "--" + boundry + "--~n" ).

    /* Put the single dot on a line to signal the end of the message */
    RUN smtp-internal-put(
        INPUT ".~n",
        OUTPUT response-code
    ).
    IF response-code <> "250" THEN DO:
        smtp-internal-error = "Message data not terminated correctly".
        RETURN.
    END.
END.

RUN smtp-internal-write( INPUT "QUIT~r~n" ).

OUTPUT STREAM emailfile CLOSE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-connect) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE smtp-internal-connect Method-Library 
PROCEDURE smtp-internal-connect :
/*------------------------------------------------------------------------------
  Purpose: Make a connection to the mail server and send EHLO   
  Parameters: host-name <string> and port number <int>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER host-name AS CHARACTER.
DEF INPUT PARAMETER port-number AS INTEGER.
DEF VAR connect-message AS CHARACTER NO-UNDO.
DEF VAR return-code AS CHARACTER NO-UNDO.

DEF VAR v-count AS INTEGER NO-UNDO.
DEF VAR v-line AS CHARACTER NO-UNDO.

OUTPUT STREAM emailfile TO VALUE( "C:\connectionlog.txt" ).

mailSocket:CONNECT( "-H " + host-name + " -S " + STRING( port-number ) ).
IF mailSocket:CONNECTED() THEN DO:

    /* Read the connect header */
    MESSAGE "Reading header".
    RUN smtp-internal-read( OUTPUT connect-message ).
    return-code = SUBSTRING( connect-message, 1, 3 ).
    IF return-code <> '220' THEN
        smtp-internal-error = "Connected to server, but did not receive a 220".

    /* Get the ESMTP options */
    MESSAGE "Getting options".
    RUN smtp-internal-write( INPUT "EHLO localhost~r~n" ).

    /* Read in the response to EHLO */
    RUN smtp-internal-read( OUTPUT connect-message ).

    /* Parse the response (set global flags like smtp-internal-support-login */
    RUN smtp-internal-parse-esmtp( INPUT connect-message ).
END.
ELSE DO:
    smtp-internal-error = "Could not connect to the server".
    DELETE OBJECT mailSocket.
END.

OUTPUT STREAM emailfile CLOSE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-login) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE smtp-internal-login Method-Library 
PROCEDURE smtp-internal-login :
/*------------------------------------------------------------------------------
  Purpose: Authenticate to the server 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER auth-type AS CHARACTER NO-UNDO.
DEF INPUT PARAMETER username AS CHARACTER NO-UNDO.
DEF INPUT PARAMETER password AS CHARACTER NO-UNDO.

DEF VAR auth-string AS CHARACTER NO-UNDO.
DEF VAR response-code AS CHARACTER NO-UNDO.

auth-type = SUBSTRING( LC( auth-type ), 1, 1 ).

CASE auth-type:
    WHEN "p" THEN DO:
        /* AUTH PLAIN */
        auth-string = "~000" + username + "~000" + password.
        auth-string = base64-encode-string( auth-string ).
        RUN smtp-internal-put(
            INPUT "AUTH PLAIN " + auth-string + "~n",
            OUTPUT response-code
        ).
        IF response-code <> "235" THEN DO:
            smtp-internal-error = "Plain login failed".
            RETURN.
        END.
    END.
    WHEN "l" THEN DO:
        /* AUTH LOGIN */
        RUN smtp-internal-put(
            INPUT "AUTH LOGIN~n",
            OUTPUT response-code
        ).
        IF response-code <> "334" THEN DO:
            smtp-internal-error = "Login mechanism not supported".
        END.
        auth-string = base64-encode-string( username ).
        RUN smtp-internal-put(
            INPUT auth-string + "~n",
            OUTPUT response-code
        ).
        IF response-code <> "235" THEN DO:
            smtp-internal-error = "Invalid username".
            RETURN.
        END.
        auth-string = base64-encode-string( password ).
        RUN smtp-internal-put(
            INPUT auth-string + "~n",
            OUTPUT response-code
        ).
        IF response-code <> "235" THEN DO:
            smtp-internal-error = "Invalid password".
            RETURN.
        END.
    END.
END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-parse-esmtp) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE smtp-internal-parse-esmtp Method-Library 
PROCEDURE smtp-internal-parse-esmtp :
/*------------------------------------------------------------------------------
  Purpose: Parse the ESMTP response from the server and set some flags  
  Parameters: CHAR - the response string from an EHLO command
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER response-string AS CHARACTER NO-UNDO.
DEF VAR newline-count AS INTEGER NO-UNDO.
DEF VAR v-count AS INTEGER NO-UNDO.
DEF VAR o-count AS INTEGER NO-UNDO.
DEF VAR options-string AS CHARACTER NO-UNDO.
DEF VAR current-line AS CHARACTER NO-UNDO.

DEF VAR supported-options AS CHARACTER NO-UNDO.
supported-options = "SIZE,AUTH".

newline-count = NUM-ENTRIES( response-string, "~n" ).

/* For each line of the response */
DO v-count = 1 TO NUM-ENTRIES( response-string, "~n" ):
    current-line = ENTRY( v-count, response-string, "~n" ).

    /* If this line contains either a space or a dash at the 4th char then consider it a valid line */
    IF SUBSTRING( current-line, 4, 1 ) = " " OR SUBSTRING( current-line, 4, 1 ) = "-" THEN DO:
        options-string = SUBSTRING( current-line, 5, LENGTH( current-line ) - 5 ).

        /* Each line has one or more options, separated by spaces */
        IF ENTRY( 1, options-string, " " ) = "AUTH" THEN DO:
            DO o-count = 2 TO NUM-ENTRIES( options-string, " " ):
                CASE ENTRY( o-count, options-string, " " ):
                    WHEN "LOGIN" THEN
                        smtp-internal-auth-login = YES.
                    WHEN "PLAIN" THEN
                        smtp-internal-auth-plain = YES.
                END CASE.
            END.
        END.
    END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-put) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE smtp-internal-put Method-Library 
PROCEDURE smtp-internal-put :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER input-chars AS CHARACTER NO-UNDO.
DEF OUTPUT PARAMETER response-string AS CHARACTER NO-UNDO.

RUN smtp-internal-write( INPUT input-chars ).
RUN smtp-internal-read( OUTPUT response-string ).

response-string = ENTRY( 1, response-string, " " ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-read) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE smtp-internal-read Method-Library 
PROCEDURE smtp-internal-read :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER response-string AS CHARACTER NO-UNDO.
DEF VAR response-buffer AS MEMPTR.
DEF VAR bytes-available AS INTEGER NO-UNDO.

WAIT-FOR READ-RESPONSE OF mailSocket.

bytes-available = mailSocket:GET-BYTES-AVAILABLE().
IF bytes-available > 255 THEN
    smtp-internal-error = "Read response is greater than 255 bytes - ignoring".
ELSE DO:
    SET-SIZE( response-buffer ) = bytes-available.
    mailSocket:READ( response-buffer, 1, bytes-available ).

    response-string = GET-STRING( response-buffer,1 ).

    PUT STREAM emailfile UNFORMATTED 'SERVER>>>~n' + response-string + "<<<~n".
END.

SET-SIZE( response-buffer ) = 0.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-write) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE smtp-internal-write Method-Library 
PROCEDURE smtp-internal-write :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER input-chars AS CHARACTER.

DEF VAR string-pointer AS MEMPTR.
DEF VAR byte-length AS INTEGER NO-UNDO.
DEF VAR response-buffer AS MEMPTR.
DEF VAR cString AS CHARACTER.

byte-length = LENGTH( input-chars, "RAW" ).

/* Fucken retarded PROGRESS - just leave the 10 there */
SET-SIZE( string-pointer ) = byte-length.

/* Write the string to the port  */
PUT-STRING( string-pointer, 1, byte-length ) = input-chars.
mailSocket:WRITE( string-pointer, 1, byte-length ).

PUT STREAM emailfile UNFORMATTED 'CLIENT>>>' + input-chars + "<<<~n".

/* Release memory */
SET-SIZE( string-pointer ) = 0.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-MIME_Add_Bcc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MIME_Add_Bcc Method-Library 
FUNCTION MIME_Add_Bcc RETURNS LOGICAL
  ( to-address AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose: Add a Bcc recipient to the email list
    Notes:  
------------------------------------------------------------------------------*/

/* Create a new address record */
CREATE SMTPAddress.
ASSIGN
    MailCode  = smtp-internal-wm
    Address   = to-address
    RecipType = "Bcc".

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_Add_Cc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MIME_Add_Cc Method-Library 
FUNCTION MIME_Add_Cc RETURNS LOGICAL
  ( to-address AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose: Add a Cc recipient to the address list
    Notes:  
------------------------------------------------------------------------------*/

/* Create a new address record */
CREATE SMTPAddress.
ASSIGN
    MailCode  = smtp-internal-wm
    Address   = to-address
    RecipType = "Cc".

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_Add_To) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MIME_Add_To Method-Library 
FUNCTION MIME_Add_To RETURNS LOGICAL
  ( to-address AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose: Add a To recipient to the email list
    Notes:  
------------------------------------------------------------------------------*/

/* Create a new address record */
CREATE SMTPAddress.
ASSIGN
    MailCode  = smtp-internal-wm
    Address   = to-address
    RecipType = "To".

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_AttachFile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MIME_AttachFile Method-Library 
FUNCTION MIME_AttachFile RETURNS LOGICAL
(
    file-location AS CHARACTER,
    type-string AS CHARACTER,
    client-file-name AS CHARACTER
) :
/*------------------------------------------------------------------------------
  Purpose: Attach a file to an email
    Notes:  
    
       MIME_AttachFile(
          <file name>     => Location of the image in the file system
          <content type>  => Type of file (eg: "application/octet-stream")
        * <client name>   => The name of the attachment that the client sees
                            
        * = Can be blank
------------------------------------------------------------------------------*/

/* Attach the file */
smtp-internal-attach(
    type-string,
    file-location,
    YES,
    ""
).

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_AttachImage) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MIME_AttachImage Method-Library 
FUNCTION MIME_AttachImage RETURNS LOGICAL
(
    file-location AS CHARACTER,
    image-type AS CHARACTER,
    client-file-name AS CHARACTER,
    image-id AS CHARACTER
) :
/*------------------------------------------------------------------------------
  Purpose: Attach an image to the email
    Notes:
    
       MIME_AttachImage(
          <file name>    => Location of the image in the file system
          <image type>   => The type of image (jpg, gif, png)
        * <client name>  => The name of the attachment that the client sees
        * <image id>     => A Content-ID for the image - can be used to view
                            attached images in inline HTML: src="CID:<image id>"
                            
        * = Can be blank
------------------------------------------------------------------------------*/
DEF VAR type-string AS CHARACTER NO-UNDO.
DEF VAR content-params AS CHARACTER NO-UNDO.

image-type = LC( image-type ).

CASE SUBSTRING( image-type, 1, 3 ):
    WHEN "jpg" THEN
        type-string = "image/jpeg".
    OTHERWISE
        type-string = "image/" + image-type.
END CASE.

IF image-id <> "" THEN
    content-params = smtp-internal-append( 'Content-ID: <' + image-id + '>', content-params ).

IF client-file-name <> "" THEN
    content-params = smtp-internal-append( 'Content-Disposition: attachment; filename="' + client-file-name + '"', content-params ).

/* Attach the file */
smtp-internal-attach(
    type-string,
    file-location,
    YES,
    content-params
).

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_Auth) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MIME_Auth Method-Library 
FUNCTION MIME_Auth RETURNS LOGICAL
(
    smtp-user AS CHARACTER,
    smtp-pass AS CHARACTER
) :
/*------------------------------------------------------------------------------
  Purpose: Flag the system to attempt auth
    Notes:  
------------------------------------------------------------------------------*/

smtp-internal-try-auth = YES.
smtp-internal-auth-username = smtp-user.
smtp-internal-auth-password = smtp-pass.

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_Body) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MIME_Body Method-Library 
FUNCTION MIME_Body RETURNS LOGICAL
(
    body-string AS CHARACTER,
    body-type AS CHARACTER
) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:
     
       MIME_Body(
          <content>       => The content string
          <content type>  => Type of text, "html" or "plain"
          
------------------------------------------------------------------------------*/

/* Attach the file */
smtp-internal-attach(
    "text/" + body-type,
    body-string,
    NO,
    ""
).

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_New) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MIME_New Method-Library 
FUNCTION MIME_New RETURNS INTEGER
(
    to-address AS CHARACTER,
    from-address AS CHARACTER,
    subject-text AS CHARACTER
) :
/*------------------------------------------------------------------------------
  Purpose: Initiate a new message
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR v-count AS INTEGER NO-UNDO.

smtp-internal-wm = smtp-internal-wm + 1.
smtp-internal-mcount = smtp-internal-mcount + 1.

/* Create a new address record */
CREATE SMTPAddress.
ASSIGN
    MailCode  = smtp-internal-wm
    Address   = to-address
    RecipType = "To".

/* Add this from address to the list */
IF smtp-internal-from = "" THEN DO:
    smtp-internal-from = smtp-internal-from + from-address.
END.
ELSE DO:
    smtp-internal-from = smtp-internal-from + "," + from-address.
END.

/* Add this subject to the list */
IF smtp-internal-subject = "" THEN DO:
    smtp-internal-subject = smtp-internal-subject + subject-text.
END.
ELSE DO:
    smtp-internal-subject = smtp-internal-subject + "," + subject-text.
END.

RETURN smtp-internal-wm.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_Reset) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MIME_Reset Method-Library 
FUNCTION MIME_Reset RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose: Delete all the messages
    Notes:  
------------------------------------------------------------------------------*/
FOR EACH SMTPAddress:
    DELETE SMTPAddress.
END.

FOR EACH SMTPAttach:
    DELETE SMTPAttach.
END.

smtp-internal-error = "".
smtp-internal-from = "".
smtp-internal-subject = "".
smtp-internal-wm = 0.
smtp-internal-mcount = 0.

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_SetTimezone) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MIME_SetTimezone Method-Library 
FUNCTION MIME_SetTimezone RETURNS LOGICAL
  ( input-string AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose: Set the timezone for the date of the message
    Notes:  
------------------------------------------------------------------------------*/

smtp-internal-timezone = input-string.

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MIME_SwitchMessage) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION MIME_SwitchMessage Method-Library 
FUNCTION MIME_SwitchMessage RETURNS LOGICAL
  ( message-id AS INTEGER ) :
/*------------------------------------------------------------------------------
  Purpose: Switch the current mail context
    Notes:  
------------------------------------------------------------------------------*/

IF message-id > smtp-internal-mcount THEN DO:
    smtp-internal-error = "Message ID does not exist".
    RETURN FALSE.
END.

smtp-internal-wm = message-id.

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-append) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION smtp-internal-append Method-Library 
FUNCTION smtp-internal-append RETURNS CHARACTER
(
    new-string AS CHARACTER,
    dest-string AS CHARACTER
) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

IF dest-string <> "" THEN
    dest-string = dest-string + ','.

dest-string = dest-string + new-string.

RETURN dest-string.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-smtp-internal-attach) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION smtp-internal-attach Method-Library 
FUNCTION smtp-internal-attach RETURNS LOGICAL
(
    type-string AS CHARACTER,
    parameter-string AS CHARACTER,
    as-file AS LOGICAL,
    content-params AS CHARACTER
) :
/*------------------------------------------------------------------------------
  Purpose: Attach a thing to the list of attachments 
    Notes:
           type-string is the content type of the attachment (eg: text/plain)
           parameter-string is either a block of text, or a file name
           as-file determines if the parameter-string is interpreted as a file name
           or a character string of text
------------------------------------------------------------------------------*/
DEF VAR type-group AS CHARACTER NO-UNDO.
DEF VAR type-sub AS CHARACTER NO-UNDO.
DEF VAR att-type AS CHARACTER NO-UNDO.
DEF VAR att-encoding AS CHARACTER NO-UNDO.
DEF VAR encoded-file-name AS CHARACTER NO-UNDO.

type-string = LC( type-string ).

IF as-file THEN DO:
    /* Encode the file in base64 to a temp file */
    encoded-file-name = base64-encode-file( parameter-string ).

    /* Create a new file attachment */
    CREATE SMTPAttach.
    ASSIGN
        MailCode        = smtp-internal-wm
        AttType         = type-string
        AttEncoding     = "base64"
        AttExternalFile = YES
        AttFileOrChar   = encoded-file-name
        AttParameters   = content-params.
END.
ELSE DO:
    /* Create a new inline attachment */
    CREATE SMTPAttach.
    ASSIGN
        MailCode        = smtp-internal-wm
        AttType         = type-string
        AttEncoding     = ""
        AttExternalFile = NO
        AttFileOrChar   = parameter-string
        AttParameters   = content-params.
END.

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

