&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

/* Preprocessor definitions */

&GLOBAL-DEFINE GAP              4   /* Gap between widgets - init-layout */
&GLOBAL-DEFINE BTN-X-GAP        2   /* up-down gap for buttons */
&GLOBAL-DEFINE BTN-Y-GAP        3   /* left-right gap for buttons */
&GLOBAL-DEFINE MAX-DRILL-BTNS   30
&GLOBAL-DEFINE MAX-MAINT-BTNS   5
&GLOBAL-DEFINE MAX-SELCT-BTNS   20
&GLOBAL-DEFINE MAX-BTNS         50

/* Other Window variables */

DEF VAR curr-viewer AS HANDLE NO-UNDO.
DEF VAR this-file   AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .15
         WIDTH              = 36.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-sysmgr.i}
{inc/method/m-utils.i}
{inc/method/m-layout.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/*--- Set the current procedure's file name ---*/

this-file = REPLACE( SEARCH( THIS-PROCEDURE:FILE-NAME ), "\", "/" ).
this-file = REPLACE( this-file, "./", "" ).


RUN set-attribute-list (
  "frame-handle = " + STRING( FRAME {&FRAME-NAME}:HANDLE ) + "," +
  "filename = "     + this-file ).

RUN set-window-icon IN sys-mgr( {&WINDOW-NAME} ).

{inc/wintrig.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-get-viewer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-viewer Method-Library 
PROCEDURE get-viewer :
/*------------------------------------------------------------------------------
  Purpose:     Gets the handle to the current viewer.
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER viewer AS HANDLE NO-UNDO.

  IF VALID-HANDLE(curr-viewer) THEN DO:
    viewer = curr-viewer.
    RETURN.
  END.

  curr-viewer = THIS-PROCEDURE.  
  RUN set-attribute-list ( 'Viewer = ' + STRING( curr-viewer ) ).
  viewer = curr-viewer.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-window-handle) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-window-handle Method-Library 
PROCEDURE get-window-handle :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RETURN STRING( {&WINDOW-NAME}:HANDLE ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-apply-entry) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-apply-entry Method-Library 
PROCEDURE local-apply-entry :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  IF LOOKUP( "override-apply-entry", THIS-PROCEDURE:INTERNAL-ENTRIES) > 0 THEN DO:
    RUN post-apply-entry IN THIS-PROCEDURE NO-ERROR.
    RETURN.
  END.
  /* Code placed here will execute PRIOR to standard behavior. */
  RUN pre-apply-entry IN THIS-PROCEDURE NO-ERROR.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'apply-entry':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN post-apply-entry IN THIS-PROCEDURE NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-create-objects) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-create-objects Method-Library 
PROCEDURE local-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* The TTP drill window approach is based on window page 0
     so for pages other than 1 dispatch adm-create-objects */
  RUN get-attribute( 'CURRENT-PAGE':U ).
  IF INTEGER(RETURN-VALUE) <> 0 THEN RUN adm-create-objects IN THIS-PROCEDURE.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-destroy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy Method-Library 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  RUN pre-destroy IN THIS-PROCEDURE NO-ERROR.
  RUN notify-viewers IN THIS-PROCEDURE ( "pre-destroy" ) NO-ERROR.

  IF VALID-HANDLE( sys-mgr ) THEN 
    RUN remove-node IN sys-mgr( THIS-PROCEDURE ).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN inst-destroy IN THIS-PROCEDURE NO-ERROR.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-initialize) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize Method-Library 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
  /* Code placed here will execute PRIOR to standard behavior. */

  RUN pre-initialize     IN THIS-PROCEDURE NO-ERROR.
  RUN pre-create-objects IN THIS-PROCEDURE NO-ERROR.

  RUN adm-create-objects IN THIS-PROCEDURE.
  IF VALID-HANDLE( sys-mgr ) THEN 
    RUN add-node IN sys-mgr( THIS-PROCEDURE ).
  RUN sub1-create-objects IN THIS-PROCEDURE NO-ERROR.
  RUN inst-create-objects IN THIS-PROCEDURE NO-ERROR. 
  RUN post-create-objects IN THIS-PROCEDURE NO-ERROR.

  RUN window-layout IN THIS-PROCEDURE.

  /* Code placed here will execute AFTER standard behavior.    */
  RUN sub1-initialize IN THIS-PROCEDURE NO-ERROR.
  RUN inst-initialize IN THIS-PROCEDURE NO-ERROR.
  RUN post-initialize IN THIS-PROCEDURE NO-ERROR.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-notify-viewers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE notify-viewers Method-Library 
PROCEDURE notify-viewers :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER notify-event AS CHAR NO-UNDO.

  RUN notify ( notify-event + ", CONTAINER-TARGET":U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-position-window) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE position-window Method-Library 
PROCEDURE position-window :
/*------------------------------------------------------------------------------
  Purpose:     If the window pisition is outside the screen then center it
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
 IF {&WINDOW-NAME}:X < 1 OR {&WINDOW-NAME}:Y < 1 OR
    {&WINDOW-NAME}:X + {&WINDOW-NAME}:WIDTH-PIXELS  > (SESSION:WIDTH-PIXELS - 20) OR
    {&WINDOW-NAME}:Y + {&WINDOW-NAME}:HEIGHT-PIXELS > (SESSION:HEIGHT-PIXELS - 15) THEN
 DO:
 
    {&WINDOW-NAME}:X = ( SESSION:WIDTH-PIXELS - {&WINDOW-NAME}:WIDTH-PIXELS - 20 ) / 2.
    {&WINDOW-NAME}:Y = ( SESSION:HEIGHT-PIXELS - {&WINDOW-NAME}:HEIGHT-PIXELS - 15) / 2.
 
 END.
 
 IF {&WINDOW-NAME}:X < 1 THEN {&WINDOW-NAME}:X = 1.  
 IF {&WINDOW-NAME}:Y < 1 THEN {&WINDOW-NAME}:Y = 1.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reset-topmost) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-topmost Method-Library 
PROCEDURE reset-topmost :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR result-code AS INT NO-UNDO.

  RUN adecomm/_topmost.r( {&WINDOW-NAME}:HWND, No, OUTPUT result-code ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-busy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-busy Method-Library 
PROCEDURE set-busy :
/*------------------------------------------------------------------------------
  Purpose:     Set the current window into a busy state
------------------------------------------------------------------------------*/
  RUN adecomm/_setcurs.p( "WAIT" ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-idle) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-idle Method-Library 
PROCEDURE set-idle :
/*------------------------------------------------------------------------------
  Purpose:     Set the current window into an idle state
------------------------------------------------------------------------------*/
  RUN adecomm/_setcurs.p( "" ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-prompt-font) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-prompt-font Method-Library 
PROCEDURE set-prompt-font :
/*------------------------------------------------------------------------------
  Purpose:     Set the prompt fonts for the frame.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN set-widget-font( FRAME {&FRAME-NAME}:HANDLE ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-topmost) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-topmost Method-Library 
PROCEDURE set-topmost :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR result-code AS INT NO-UNDO.

  RUN adecomm/_topmost.r( {&WINDOW-NAME}:HWND, Yes, OUTPUT result-code ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-widget-font) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-widget-font Method-Library 
PROCEDURE set-widget-font :
/*------------------------------------------------------------------------------
  Purpose:     Set the prompt fonts for the given widget.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER wh AS HANDLE NO-UNDO.

  IF wh:TYPE = 'FRAME':U THEN DO:
    DEF VAR h-child AS HANDLE NO-UNDO.
    h-child = wh:CURRENT-ITERATION.
    h-child = h-child:FIRST-CHILD.
    
    DO WHILE VALID-HANDLE( h-child ):
      IF h-child:TYPE = 'FRAME' THEN
      DO:
        h-child:FONT = 10.
        RUN set-widget-font( h-child ).
      END.
    
      h-child = h-child:NEXT-SIBLING.
      
    END.
  
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-use-Viewer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Viewer Method-Library 
PROCEDURE use-Viewer :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-viewer AS CHAR NO-UNDO.

  ASSIGN curr-viewer = WIDGET-HANDLE( new-viewer ) NO-ERROR.

DEF VAR viewer-list AS CHAR NO-UNDO INIT "SmartBrowser,SmartViewer,SmartFrame":U.
DEF VAR i AS INT INIT 1 NO-UNDO.

  IF NOT VALID-HANDLE( curr-viewer ) THEN
    viewer-list = new-viewer + "," + viewer-list.

  DO WHILE NOT VALID-HANDLE( curr-viewer ) AND
     i <= NUM-ENTRIES( viewer-list ):

    RUN get-child ( THIS-PROCEDURE , ENTRY( i, viewer-list ) ).
    curr-viewer = WIDGET-HANDLE( RETURN-VALUE ).
    i = i + 1.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-window-layout) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE window-layout Method-Library 
PROCEDURE window-layout :
/*------------------------------------------------------------------------------
  Purpose:     Set the widget layout and initialize the window
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN set-prompt-font.

  IF LOOKUP( 'user-layout':U, THIS-PROCEDURE:INTERNAL-ENTRIES ) <> 0 THEN
    RUN user-layout.
  ELSE
    RUN default-layout.

  RUN draw-window.

  IF LOOKUP( 'user-position-window':U, THIS-PROCEDURE:INTERNAL-ENTRIES ) <> 0 THEN
    RUN user-position-window.
  ELSE
    RUN position-window.

  RUN adm-initialize IN THIS-PROCEDURE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

