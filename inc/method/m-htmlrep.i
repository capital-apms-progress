&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : m-htmlrep.i
    Purpose     : Methods for producing HTML-based reports
    Author(s)   : Andrew McMillan
    Notes       : This should be a replacement for m-txtrep.i, except
                  that it probably _won't_ work on the batch queue.
                  and lot's of other changes will be needed to work
                  for a different metaphor really.
  ------------------------------------------------------------------------*/

DEF VAR htmlrep-print-file AS CHAR NO-UNDO.
DEF VAR htmlrep-header-file AS CHAR NO-UNDO.
DEF VAR htmlrep-filename AS CHAR NO-UNDO.
DEF VAR htmlrep-prefix AS CHAR INITIAL "R" NO-UNDO.

DEF VAR htmlrep-headers AS CHAR NO-UNDO INITIAL "<html>~n".
DEF VAR htmlrep-body AS CHAR NO-UNDO INITIAL "</head>~n".
DEF VAR htmlrep-hdr-sent AS LOGI NO-UNDO INITIAL No.
DEF VAR htmlrep-current-tag AS CHAR NO-UNDO INITIAL "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-html-entities) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD html-entities Method-Library 
FUNCTION html-entities RETURNS CHARACTER
  ( INPUT base-text AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-tag) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD tag Method-Library 
FUNCTION tag RETURNS CHARACTER
  ( INPUT tagtype AS CHAR, INPUT tagtext AS CHAR, INPUT attributes AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .1
         WIDTH              = 39.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-bqmgr.i}
{src/adm/method/attribut.i}
{inc/string.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

RUN htmlrep-initialise.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-htmlrep-initialise) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE htmlrep-initialise Method-Library 
PROCEDURE htmlrep-initialise :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the important parameters
------------------------------------------------------------------------------*/
DEF VAR temp-dir AS CHAR NO-UNDO.

  temp-dir = OS-GETENV("TEMP":U).
  IF temp-dir = ? OR temp-dir = "" THEN temp-dir = SESSION:TEMP-DIRECTORY .
  temp-dir = temp-dir + "\".

  htmlrep-print-file = temp-dir + "HB" + STRING( TIME, "99999") + ".TMP".
  htmlrep-header-file = temp-dir + "HH" + STRING( TIME, "99999") + ".TMP".
  htmlrep-filename = temp-dir + "H" + STRING( TIME, "99999") + ".html" .

  DO WHILE INDEX( htmlrep-print-file, "/" ) > 0:
    SUBSTRING( htmlrep-print-file, INDEX( htmlrep-print-file, "/" ), 1) = "\".
  END.
  DO WHILE INDEX( htmlrep-header-file, "/" ) > 0:
    SUBSTRING( htmlrep-header-file, INDEX( htmlrep-header-file, "/" ), 1) = "\".
  END.
  DO WHILE INDEX( htmlrep-filename, "/" ) > 0:
    SUBSTRING( htmlrep-filename, INDEX( htmlrep-filename, "/" ), 1) = "\".
  END.

  htmlrep-headers = "<html>~n<head>~n".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-htmlrep-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE htmlrep-line Method-Library 
PROCEDURE htmlrep-line :
/*------------------------------------------------------------------------------
  Purpose:  Print a line in a selected font
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER line-text AS CHAR NO-UNDO.

  IF line-text = ? THEN line-text = "".
  
  DEF VAR i AS INT NO-UNDO.

  htmlrep-body = htmlrep-body + line-text + "~n".

/*  PUT UNFORMATTED line-text SKIP. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pclrep-start) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pclrep-start Method-Library 
PROCEDURE pclrep-start :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER preview AS LOGICAL NO-UNDO.

  htmlrep-headers = "<html>~n".
  htmlrep-body    = "</head>~n".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-title) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-title Method-Library 
PROCEDURE set-title :
/*------------------------------------------------------------------------------
  Purpose:  Set the title in the report viewer window.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER title-text AS CHAR NO-UNDO.

  htmlrep-headers = htmlrep-headers
                  + "<title>" + html-entities(title-text) + "</title>~n".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-view-output-file) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE view-output-file Method-Library 
PROCEDURE view-output-file :
/*------------------------------------------------------------------------------
  Purpose:  Close the output file now that we've finished with it.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER txtrep-is-preview AS LOGI NO-UNDO.

  OUTPUT TO VALUE(html-filename) .
  PUT UNFORMATTED htmlrep-headers htmlrep-body .
  OUTPUT CLOSE.

  IF txtrep-is-preview THEN DO:
    OS-COMMAND "iexplore " + htmlrep-filename .
  END.
  ELSE DO:
    /* do nothing */
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-html-entities) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION html-entities Method-Library 
FUNCTION html-entities RETURNS CHARACTER
  ( INPUT base-text AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert awkward characters to HTML entities
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR replaced AS CHAR NO-UNDO.

  replaced = REPLACE( base-text, "&", "&amp;" ).
  replaced = REPLACE( replaced, "<", "&lt;" ).
  replaced = REPLACE( replaced, ">", "&gt;" ).

  RETURN replaced.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-tag) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION tag Method-Library 
FUNCTION tag RETURNS CHARACTER
  ( INPUT tagtype AS CHAR, INPUT tagtext AS CHAR, INPUT attributes AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Enclose the tagtext value in quotes
    Notes:  
------------------------------------------------------------------------------*/

  RETURN "<" + tagtype + " " + attributes + ">"
       + tagtext
       + "</" + tagtype + ">".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

