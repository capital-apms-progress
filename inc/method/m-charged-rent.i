&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : inc/method/m-charged-rent.i
    Purpose     : calculate charged rent split for rental spaces.

    Description :

    Author(s)   : Andrew McMillan
    Created     : 7/5/1999
    Notes       : Depends on the split of contracted rent being maintained
  ------------------------------------------------------------------------*/

{inc/ofc-this.i}
{inc/ofc-set.i "RentCharge-Outgoings" "chgrnt-og-type"}
IF NOT AVAILABLE(OfficeSetting) THEN chgrnt-og-type = "".

DEF TEMP-TABLE chgrnt-LeaseCharge NO-UNDO
    FIELD TenancyLeaseCode AS INT
    FIELD AreaType AS CHAR
    FIELD TotalCharged AS DEC
    FIELD TotalContract AS DEC
    INDEX XPKchgrent-LeaseCharge IS UNIQUE TenancyLeaseCode AreaType.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-get-charged-rent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-charged-rent Method-Library 
FUNCTION get-charged-rent RETURNS DECIMAL
  ( INPUT space-charged AS DEC, INPUT space-contract AS DEC, INPUT area-type AS CHAR, INPUT lease-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .55
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-get-charged-rent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-charged-rent Method-Library 
FUNCTION get-charged-rent RETURNS DECIMAL
  ( INPUT space-charged AS DEC, INPUT space-contract AS DEC, INPUT area-type AS CHAR, INPUT lease-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF lease-code = 0 OR lease-code = ? THEN RETURN 0.0 .
  IF use-rent-charges <> Yes THEN DO:
    RETURN (IF space-charged <> ? THEN space-charged ELSE 0).
  END.

DEF BUFFER TestSpace FOR RentalSpace.
DEF BUFFER TestCharge FOR RentCharge.
DEF VAR total-contract AS DEC NO-UNDO INITIAL 0.0.
DEF VAR total-charge AS DEC NO-UNDO INITIAL 0.0.
  FIND chgrnt-LeaseCharge WHERE chgrnt-LeaseCharge.TenancyLeaseCode = lease-code
                            AND chgrnt-LeaseCharge.AreaType = area-type NO-ERROR.
  IF AVAILABLE(chgrnt-LeaseCharge) THEN DO:
    total-charge = chgrnt-LeaseCharge.TotalCharged.
    total-contract = chgrnt-LeaseCharge.TotalContract.
  END.
  ELSE DO:
    ON FIND OF RentalSpace OVERRIDE DO: END.
    FOR EACH TestSpace WHERE TestSpace.TenancyLeaseCode = lease-code
                            AND TestSpace.AreaType = area-type NO-LOCK:
      IF TestSpace.AreaStatus <> "L" THEN NEXT.
      total-contract = total-contract + TestSpace.ContractedRental .
    END.
    ON FIND OF RentalSpace REVERT.

    FOR EACH TestCharge WHERE TestCharge.TenancyLeaseCode = lease-code
                          AND TestCharge.RentChargeType = area-type
                          AND TestCharge.RentChargeType <> chgrnt-og-type NO-LOCK:
      total-charge = total-charge + TestCharge.CurrentAnnualRental .
    END.

    CREATE chgrnt-LeaseCharge.
    chgrnt-LeaseCharge.TenancyLeaseCode = lease-code.
    chgrnt-LeaseCharge.AreaType = area-type.
    chgrnt-LeaseCharge.TotalCharged = total-charge.
    chgrnt-LeaseCharge.TotalContract = total-contract.

  END.

  IF total-contract = 0.0 THEN DO:
    RETURN 0.0 .
  END.

  RETURN ROUND( (space-contract / total-contract) * total-charge, 2) .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

