&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

DEF VAR sec-mgr AS HANDLE NO-UNDO.
DEF VAR sys-mgr AS HANDLE NO-UNDO.

&IF DEFINED(UIB_Is_Running) NE 0 &THEN
  DEF VAR uib-operational-mode AS LOGICAL INITIAL Yes NO-UNDO.
&ELSE
  DEF VAR uib-operational-mode AS LOGICAL INITIAL No NO-UNDO.
&ENDIF

RUN get-sys-mgr.

&GLOBAL-DEFINE SYS-MGR

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .17
         WIDTH              = 37.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-get-sys-mgr) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-sys-mgr Method-Library 
PROCEDURE get-sys-mgr :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR wh AS HANDLE NO-UNDO.
DEF VAR i AS INT NO-UNDO INITIAL 0.

  wh = SESSION:FIRST-PROCEDURE.
  sys-mgr = ?.
  DO WHILE VALID-HANDLE(wh):
    i = i + 1.
    IF INDEX( wh:FILE-NAME, "uib" ) <> 0 THEN uib-operational-mode = Yes.
    IF LOOKUP( 'apms-system-manager':U, wh:INTERNAL-ENTRIES ) > 0 THEN DO:
      sys-mgr = wh.
      LEAVE.
    END.
    wh = wh:NEXT-SIBLING.
  END.

  IF NOT VALID-HANDLE( sys-mgr ) THEN RUN start-sys-mgr.

  sec-mgr = sys-mgr.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-start-sys-mgr) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE start-sys-mgr Method-Library 
PROCEDURE start-sys-mgr :
/*------------------------------------------------------------------------------
  Purpose:     Start the system manager.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR sys-mgr-name AS CHAR NO-UNDO.

  IF VALID-HANDLE( sys-mgr ) THEN RETURN.
  sys-mgr-name = SEARCH( "sysmgr.r" ).
  IF sys-mgr-name = ? THEN sys-mgr-name = SEARCH( "sysmgr.w" ).

  RUN VALUE( sys-mgr-name ) PERSISTENT SET sys-mgr.
  IF uib-operational-mode THEN RUN dispatch IN sys-mgr( 'set-uib-modus-operandi':U ).
  RUN dispatch IN sys-mgr( 'Initialize':U ).

  sec-mgr = sys-mgr.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

