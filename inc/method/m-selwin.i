&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

/* If called from a drill button, these variables must be set
   for correct operation of this drill window */

/* Other variables */

DEF VAR btn     AS HANDLE EXTENT 50 NO-UNDO.
DEF VAR opt-pnl AS HANDLE EXTENT 5  NO-UNDO.

DEF VAR sel-btn-ok     AS HANDLE NO-UNDO.
DEF VAR sel-btn-cancel AS HANDLE NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .1
         WIDTH              = 37.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-mstwin.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-create-confirm-buttons) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-confirm-buttons Method-Library 
PROCEDURE create-confirm-buttons :
/*------------------------------------------------------------------------------
  Purpose:     Create the confirm buttons for a select window
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR frm-handle AS HANDLE NO-UNDO.
  frm-handle = FRAME {&FRAME-NAME}:HANDLE.

  CREATE BUTTON sel-btn-ok
  
    ASSIGN
      X            = 1
      Y            = 1
      FONT         = 9
      LABEL        = '&OK':U
      FRAME        = frm-handle
      DEFAULT      = True
      VISIBLE      = True
      SENSITIVE    = True

    TRIGGERS:
      ON CHOOSE PERSISTENT RUN select-ok IN THIS-PROCEDURE.
    END TRIGGERS.

  FRAME {&FRAME-NAME}:DEFAULT-BUTTON = sel-btn-ok.

  CREATE BUTTON sel-btn-cancel
  
    ASSIGN
      X = 1
      Y            = 1
      FONT         = 9
      LABEL        = '&Cancel':U
      FRAME        = frm-handle
      VISIBLE      = True
      SENSITIVE    = True

    TRIGGERS:
      ON CHOOSE PERSISTENT RUN select-cancel IN THIS-PROCEDURE.
    END TRIGGERS.

  FRAME {&FRAME-NAME}:CANCEL-BUTTON = sel-btn-cancel.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-default-layout) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE default-layout Method-Library 
PROCEDURE default-layout :
/*------------------------------------------------------------------------------
  Purpose:     Default widget layout for this window
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN get-default-groups.
  RUN stack( "opt-group", "mst-group", "T", No ).
  RUN stack( STRING( curr-viewer ) + ",btn-group", "mst-group", "T", Yes ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-default-groups) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-default-groups Method-Library 
PROCEDURE get-default-groups :
/*------------------------------------------------------------------------------
  Purpose:     Defines the default widget groups for this window
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR btn-list AS CHAR NO-UNDO.
DEF VAR pnl-list AS CHAR NO-UNDO.
DEF VAR flt-list AS CHAR NO-UNDO INITIAL "".

DEF VAR h_ABC-Panel1 AS HANDLE NO-UNDO.
DEF VAR h_ABC-Panel2 AS HANDLE NO-UNDO.
DEF VAR browse-handle AS HANDLE NO-UNDO.

  RUN get-attribute IN curr-viewer ( 'adm-object-handle':U ).
  browse-handle = WIDGET-HANDLE( RETURN-VALUE ).

  /* filter panels */
  RUN get-attribute( 'Filter-Panel' ).
  IF RETURN-VALUE <> ? THEN DO:
    flt-list = RETURN-VALUE + ",".
    h_ABC-Panel1 = WIDGET-HANDLE(RETURN-VALUE).
    RUN set-size IN h_ABC-Panel1 ( 0.8, browse-handle:width-chars ).
  END.

  RUN get-attribute( 'Filter2-Panel' ).
  IF RETURN-VALUE <> ? THEN DO:
    flt-list = flt-list + RETURN-VALUE + ",".
    h_ABC-Panel2 = WIDGET-HANDLE(RETURN-VALUE).
    RUN set-size IN h_ABC-Panel2 ( 0.8, browse-handle:width-chars ).
  END.

  /* option type panels */
  RUN get-attribute ( 'FilterBy-Panel' ).
  IF RETURN-VALUE <> ? THEN pnl-list = pnl-list + RETURN-VALUE + ",".
  RUN get-attribute ( 'SortBy-Panel' ).
  IF RETURN-VALUE <> ? THEN pnl-list = pnl-list + RETURN-VALUE + ",".
  RUN get-attribute( 'SearchBy-Panel' ).
  IF RETURN-VALUE <> ? THEN pnl-list = pnl-list + RETURN-VALUE + ",".
  RUN get-attribute( 'Option-Panel' ).
  IF RETURN-VALUE <> ? THEN pnl-list = pnl-list + RETURN-VALUE + ",".

  /* Do all link panels */
  DEF VAR i AS INT NO-UNDO.
  DEF VAR j AS INT NO-UNDO.
  DEF VAR direction  AS CHAR NO-UNDO.

  DO i = 1 TO 2:
    direction = ( IF i = 1 THEN "From" ELSE "To" ).
    DO j = 1 TO 3:
      RUN get-attribute( "Link-" + direction + "-" + STRING( j ) ).
      IF RETURN-VALUE <> ? THEN pnl-list = pnl-list + RETURN-VALUE + ",".
    END.
  END.

  RUN get-btn-list IN sys-mgr( THIS-PROCEDURE, OUTPUT btn-list ).
  RUN pack( STRING( sel-btn-ok ) + "," + STRING( sel-btn-cancel ), "btn-group", "L", No ).
  RUN pack( btn-list, "btn-group", "L", No ).

  pnl-list = TRIM( pnl-list, "," ).
  RUN pack( pnl-list, "opt1-group", "L", No ).
  RUN stack( "opt1-group," + TRIM(flt-list,","), "opt-group", "T", No).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-select-cancel) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-cancel Method-Library 
PROCEDURE select-cancel :
/*------------------------------------------------------------------------------
  Purpose:     Cancel the selection in this window
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  APPLY 'WINDOW-CLOSE':U TO {&WINDOW-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-select-ok) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-ok Method-Library 
PROCEDURE select-ok :
/*------------------------------------------------------------------------------
  Purpose:     Confirm the selection in this window
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN select-ok IN sys-mgr ( THIS-PROCEDURE ).
  APPLY 'WINDOW-CLOSE':U TO {&WINDOW-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-sub1-create-objects) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sub1-create-objects Method-Library 
PROCEDURE sub1-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Master create objects for drill windows
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN create-confirm-buttons IN THIS-PROCEDURE.
  RUN sub2-create-objects IN THIS-PROCEDURE NO-ERROR.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

