&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

&SCOPED-DEFINE GAP 4

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .05
         WIDTH              = 43.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-apply-event) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE apply-event Method-Library 
PROCEDURE apply-event :
/*------------------------------------------------------------------------------
  Purpose:     Applies an event to all widgets matching the given
               criteria in the current frame
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER event  AS CHAR NO-UNDO.
DEF INPUT PARAMETER prefix AS CHAR NO-UNDO.
DEF INPUT PARAMETER type-list   AS CHAR NO-UNDO.
  
  DEF VAR enabled AS LOGI NO-UNDO.
  DEF VAR wh AS HANDLE NO-UNDO.
  wh = FRAME {&FRAME-NAME}:CURRENT-ITERATION.
  wh = wh:FIRST-CHILD.

  DO WHILE VALID-HANDLE( wh ):
  
    IF wh:NAME BEGINS prefix AND LOOKUP( wh:TYPE, type-list) > 0 THEN
    DO:
      enabled = wh:SENSITIVE.
      wh:SENSITIVE = Yes. 
      APPLY event TO wh.
      wh:SENSITIVE = enabled.
    END.
  
    wh = wh:NEXT-SIBLING.

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-assign-select-combos) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-select-combos Method-Library 
PROCEDURE assign-select-combos :
/*------------------------------------------------------------------------------
  Purpose:     Assign the values of all select combo boxes
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN apply-event( 'U2':U, 'cmb', 'COMBO-BOX,SELECTION-LIST':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-assign-select-fill-ins) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-select-fill-ins Method-Library 
PROCEDURE assign-select-fill-ins :
/*------------------------------------------------------------------------------
  Purpose:     Assign the values of all select fill-ins
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN apply-event( 'U3':U, 'fil', 'FILL-IN':U ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-display-select-combos) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-select-combos Method-Library 
PROCEDURE display-select-combos :
/*------------------------------------------------------------------------------
  Purpose:     Intialise and display all select combos
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN apply-event( 'U1':U, 'cmb', 'COMBO-BOX,SELECTION-LIST':U ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-display-select-fill-ins) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-select-fill-ins Method-Library 
PROCEDURE display-select-fill-ins :
/*------------------------------------------------------------------------------
  Purpose:     Intialise and display all select fill-ins
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN apply-event( 'U1':U, 'fil', 'FILL-IN':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-add-record) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record Method-Library 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  IF INDEX( THIS-PROCEDURE:INTERNAL-ENTRIES, 'override-add-record') > 0 THEN
  DO:
    RUN override-add-record IN THIS-PROCEDURE NO-ERROR.
  END.
  ELSE DO:
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .
    RUN inst-add-record IN THIS-PROCEDURE NO-ERROR.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-assign-record) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record Method-Library 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
  RUN inst-assign-record IN THIS-PROCEDURE NO-ERROR.

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-assign-statement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement Method-Library 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  RUN pre-assign-statement IN THIS-PROCEDURE NO-ERROR.
  RUN assign-select-fill-ins.
  RUN assign-select-combos.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ) .
  RUN inst-assign-statement IN THIS-PROCEDURE NO-ERROR.
  
  /* Code placed here will execute AFTER standard behavior.    */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-delete-record) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record Method-Library 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  
  IF INDEX( THIS-PROCEDURE:INTERNAL-ENTRIES, 'override-delete-record') > 0 THEN
    RUN override-delete-record IN THIS-PROCEDURE NO-ERROR.
  ELSE DO:
    RUN inst-delete-record IN THIS-PROCEDURE NO-ERROR.
    IF RETURN-VALUE = "FAIL":U THEN RETURN ERROR.

    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

    /* Code placed here will execute AFTER standard behavior.    */
    CLEAR FRAME {&FRAME-NAME} ALL NO-PAUSE.

    RUN post-delete-record IN THIS-PROCEDURE NO-ERROR.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-destroy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy Method-Library 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  RUN pre-destroy  IN THIS-PROCEDURE NO-ERROR.
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch     IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  RUN inst-destroy IN THIS-PROCEDURE NO-ERROR.
  IF VALID-HANDLE( sys-mgr ) THEN 
    RUN remove-node  IN sys-mgr( THIS-PROCEDURE ).
  
  /* Code placed here will execute AFTER standard behavior.    */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-disable-fields) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields Method-Library 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
  RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, "Viewer", "SENSITIVE = No" ).
  RUN inst-disable-fields IN THIS-PROCEDURE NO-ERROR.

  /* Code placed here will execute AFTER standard behavior.    */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-sub2-create-objects) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sub2-create-objects Method-Library 
PROCEDURE sub2-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Master create objects for all maintain viewers
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF VALID-HANDLE( sys-mgr ) THEN 
    RUN add-node IN sys-mgr ( THIS-PROCEDURE ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-sub2-display-fields) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sub2-display-fields Method-Library 
PROCEDURE sub2-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Master display fields for maintenance viewers
  Notes:       
------------------------------------------------------------------------------*/

  RUN display-select-fill-ins.
  RUN display-select-combos.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

