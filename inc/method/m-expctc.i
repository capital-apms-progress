&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : 
    Purpose     : Common functionality for Contact Exports

    Syntax      :

    Description :

    Author(s)   : Andrew McMillan
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/
DEF VAR include-no-mailout AS LOGI NO-UNDO INITIAL No.
DEF VAR make-unique AS LOGI NO-UNDO INITIAL No.
DEF VAR clean-addresses AS LOGI NO-UNDO INITIAL No.
DEF VAR split-addresses AS LOGI NO-UNDO INITIAL No.

DEF VAR this-contact-type AS CHAR NO-UNDO.


DEFINE TEMP-TABLE Exported
     FIELD PersonCode LIKE Contact.PersonCode
     FIELD FirstName AS CHAR
     FIELD LastName AS CHAR
     FIELD Company AS CHAR
     INDEX XPKEx IS UNIQUE PRIMARY PersonCode
     INDEX XAK1Ex IS UNIQUE Company LastName FirstName.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-clean-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clean-address Method-Library 
FUNCTION clean-address RETURNS CHARACTER
  ( INPUT in-address AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .15
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/contacts.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

RUN parse-parameters.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-export-contact) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE export-contact Method-Library 
PROCEDURE export-contact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR person-address AS CHAR NO-UNDO.
DEF VAR phone-no       AS CHAR EXTENT 4 NO-UNDO.
DEF VAR phone-list     AS CHAR NO-UNDO INIT "HOME,BUS,MOB,FAX".
DEF VAR i              AS INT  NO-UNDO.  

  IF NOT AVAILABLE(Person) THEN RETURN.
  IF NOT(include-no-mailout OR Person.MailOut) THEN RETURN.

  IF make-unique THEN DO:
    FIND FIRST Exported WHERE Exported.PersonCode = Person.PersonCode NO-ERROR.
    IF AVAILABLE(Exported)  THEN DO:
        /* MESSAGE "Person is not unique " + TRIM(STRING(Person.PersonCode,">>>>>9")) VIEW-AS ALERT-BOX . */
        RETURN.
    END.
    FIND FIRST Exported WHERE Exported.FirstName = Person.FirstName
           AND Exported.LastName = Person.LastName
           AND Exported.Company = Person.Company NO-ERROR.
    IF AVAILABLE(Exported)  THEN DO:
      /* MESSAGE "Firstname / Lastname / Company is not unique " + Person.FirstName + "/" + Person.LastName + "/" + Person.Company VIEW-AS ALERT-BOX . */
      RETURN.
    END.
    CREATE Exported NO-ERROR.
    ASSIGN Exported.PersonCode = Person.PersonCode 
           Exported.FirstName = Person.FirstName
           Exported.LastName = Person.LastName
           Exported.Company = Person.Company NO-ERROR.
  END.
  /* PUT UNFORMATTED TRIM(STRING(Person.PersonCode,">>>>>>9")) + ",". */

  IF split-addresses THEN DO:
    RUN export-split-addresses.
    RETURN.
  END.

  RUN process/getaddr.p( "PERSON", Person.PersonCode, pty-list, OUTPUT person-address ).
  IF clean-addresses THEN person-address = clean-address( person-address ).

  DO i = 1 TO NUM-ENTRIES( phone-list ):
    phone-no[i] = "".
    FIND PhoneDetail WHERE PhoneDetail.PersonCode = Person.PersonCode
                     AND PhoneDetail.PhoneType  = ENTRY( i, phone-list )
                     NO-LOCK NO-ERROR.
    IF AVAILABLE( PhoneDetail ) THEN
      phone-no[i] = get-phone-no( Person.PersonCode, ENTRY( i, phone-list ) ).
  END.

  DO i = 1 TO NUM-ENTRIES( pty-list ):
    FIND PostalDetail WHERE PostalDetail.PersonCode = Person.PersonCode AND
                            PostalDetail.PostalType = ENTRY( i, pty-list )
                            NO-LOCK NO-ERROR.
    IF AVAILABLE(PostalDetail) THEN LEAVE.
  END.

  EXPORT DELIMITER ","
    this-contact-type
    Person.PersonTitle
    Person.FirstName
    Person.LastName
    Person.JobTitle
    Person.Company
    TRIM(REPLACE(person-address, "~r", ""))
    (IF AVAILABLE(PostalDetail) THEN PostalDetail.City ELSE "")
    (IF AVAILABLE(PostalDetail) THEN PostalDetail.Country ELSE "")
    phone-no[1]
    phone-no[2]
    phone-no[3]
    phone-no[4]
    {&EXTRA-FIELDS}
    .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-export-contact-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE export-contact-header Method-Library 
PROCEDURE export-contact-header :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
IF split-addresses THEN
  EXPORT DELIMITER ","
    "Type"
    "Title"
    "FirstName"
    "LastName"
    "JobTitle"
    "Company"
    "Address-1"
    "Address-2"
    "Address-3"
    "Address-4"
    "Address-5"
    "Address-6"
    "City"
    "Country"
    "HomePhone"
    "WorkPhone"
    "MobilePhone"
    "Facsimile"
    {&EXTRA-HEADERS}
    .

ELSE
  EXPORT DELIMITER ","
    "Type"
    "Title"
    "FirstName"
    "LastName"
    "JobTitle"
    "Company"
    "Address"
    "City"
    "Country"
    "HomePhone"
    "WorkPhone"
    "MobilePhone"
    "Facsimile"
    {&EXTRA-HEADERS}
    .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-export-split-addresses) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE export-split-addresses Method-Library 
PROCEDURE export-split-addresses :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR person-address AS CHAR NO-UNDO.
DEF VAR phone-no       AS CHAR EXTENT 4 NO-UNDO INITIAL "".
DEF VAR address-line   AS CHAR EXTENT 6 NO-UNDO INITIAL "".
DEF VAR phone-list     AS CHAR NO-UNDO INIT "HOME,BUS,MOB,FAX".
DEF VAR i              AS INT  NO-UNDO.  

  IF NOT AVAILABLE(Person) THEN RETURN.
  RUN process/getaddr.p( "PERSON", Person.PersonCode, pty-list, OUTPUT person-address ).
  IF clean-addresses THEN person-address = clean-address( person-address ).

  person-address = TRIM(REPLACE(person-address, "~r", "")).
  DO i = 1 TO NUM-ENTRIES( person-address, "~n" ):
    address-line[i] = ENTRY(i, person-address, "~n").
  END.

  DO i = 1 TO NUM-ENTRIES( phone-list ):
    phone-no[i] = "".
    FIND PhoneDetail WHERE PhoneDetail.PersonCode = Person.PersonCode
                     AND PhoneDetail.PhoneType  = ENTRY( i, phone-list )
                     NO-LOCK NO-ERROR.
    IF AVAILABLE( PhoneDetail ) THEN
      phone-no[i] = get-phone-no( Person.PersonCode, ENTRY( i, phone-list ) ).
  END.

  DO i = 1 TO NUM-ENTRIES( pty-list ):
    FIND PostalDetail WHERE PostalDetail.PersonCode = Person.PersonCode AND
                            PostalDetail.PostalType = ENTRY( i, pty-list )
                            NO-LOCK NO-ERROR.
    IF AVAILABLE(PostalDetail) THEN LEAVE.
  END.

  EXPORT DELIMITER ","
    this-contact-type
    Person.PersonTitle
    Person.FirstName
    Person.LastName
    Person.JobTitle
    Person.Company
    address-line[1]
    address-line[2]
    address-line[3]
    address-line[4]
    address-line[5]
    address-line[6]
    (IF AVAILABLE(PostalDetail) THEN PostalDetail.City ELSE "")
    (IF AVAILABLE(PostalDetail) THEN PostalDetail.Country ELSE "")
    phone-no[1]
    phone-no[2]
    phone-no[3]
    phone-no[4]
    {&EXTRA-FIELDS}
    .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-clean-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clean-address Method-Library 
FUNCTION clean-address RETURNS CHARACTER
  ( INPUT in-address AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Clean the company name (and possibly other things) from the address
------------------------------------------------------------------------------*/
DEF VAR out-address AS CHAR NO-UNDO.
DEF VAR position AS INT NO-UNDO.
DEF VAR line AS CHAR NO-UNDO.

  position = INDEX( in-address, Person.Company ).
  IF position > 0 THEN DO:
    in-address = SUBSTRING( in-address, 1, position - 1) + SUBSTRING( in-address, position + LENGTH(Person.Company)).
    out-address = "".
    DO position = 1 TO NUM-ENTRIES(in-address, "~n"):
      line = LEFT-TRIM( ENTRY( position, in-address, "~n"), ", ").
      IF line <> "" THEN out-address = out-address
                                     + (IF out-address = "" THEN "" ELSE "~n")
                                     + line.
    END.
  END.
  ELSE
    out-address = in-address.

  RETURN out-address.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

