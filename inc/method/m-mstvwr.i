&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF VAR have-records AS LOGI INIT NO NO-UNDO.
DEF VAR this-file    AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-find-parent-key) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD find-parent-key Method-Library 
FUNCTION find-parent-key RETURNS CHARACTER
  ( INPUT key-name AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .05
         WIDTH              = 44.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-sysmgr.i}
{inc/method/m-utils.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */
this-file = REPLACE( SEARCH( THIS-PROCEDURE:FILE-NAME ), "\", "/" ).
this-file = REPLACE( this-file, "./", "" ).

RUN set-attribute-list (
  "frame-handle = " + STRING( FRAME {&FRAME-NAME}:HANDLE ) + "," +
  "filename = "     + this-file ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-get-topic-desc) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-topic-desc Method-Library 
PROCEDURE get-topic-desc :
/*------------------------------------------------------------------------------
  Purpose:     Get the description of the topic for this
               viewer.
------------------------------------------------------------------------------*/

  DEF OUTPUT PARAMETER topic-desc AS CHAR NO-UNDO.

  &IF DEFINED( TOPIC-TABLE ) NE 0 AND
      DEFINED( TOPIC-FIELD ) NE 0 &THEN

    topic-desc = IF AVAILABLE {&TOPIC-TABLE}
      THEN {&TOPIC-PREFIX} + " " + STRING({&TOPIC-FIELD})
      ELSE "{&TOPIC-TABLE}".   
    topic-desc = TRIM( topic-desc ).
    
  &ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-display-fields) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields Method-Library 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  RUN pre-display-fields  IN THIS-PROCEDURE NO-ERROR.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN sub1-display-fields IN THIS-PROCEDURE NO-ERROR.
  RUN inst-display-fields IN THIS-PROCEDURE NO-ERROR.
  RUN post-display-fields IN THIS-PROCEDURE NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-enable-fields) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields Method-Library 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF INDEX( THIS-PROCEDURE:INTERNAL-ENTRIES, 'override-enable-fields') > 0 THEN DO:
    RUN override-enable-fields IN THIS-PROCEDURE NO-ERROR.
    RETURN.
  END.
  RUN pre-enable-fields IN THIS-PROCEDURE NO-ERROR.

  /* Dispatch standard ADM method.                             */
  IF RETURN-VALUE = "LEAVE" THEN RETURN.
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .
  RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, "ALL", "SENSITIVE = Yes" ).
  RUN inst-enable-fields IN THIS-PROCEDURE NO-ERROR.
  
  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-initialize) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize Method-Library 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
  /* Code placed here will execute PRIOR to standard behavior. */

  RUN pre-initialize      IN THIS-PROCEDURE NO-ERROR.
  RUN pre-create-objects  IN THIS-PROCEDURE NO-ERROR.
  RUN sub1-create-objects IN THIS-PROCEDURE NO-ERROR.
  RUN inst-create-objects IN THIS-PROCEDURE NO-ERROR.
  RUN post-create-objects IN THIS-PROCEDURE NO-ERROR.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  RUN sub1-initialize IN THIS-PROCEDURE NO-ERROR.
  RUN inst-initialize IN THIS-PROCEDURE NO-ERROR.
  RUN post-initialize IN THIS-PROCEDURE NO-ERROR.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-local-row-available) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available Method-Library 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  IF INDEX( THIS-PROCEDURE:INTERNAL-ENTRIES, 'override-row-available') > 0 THEN DO:
    RUN override-row-available IN THIS-PROCEDURE NO-ERROR.
    RETURN.
  END.
  /* If this viewer has been destroyed or is hidden then just return */

  RUN get-attribute IN THIS-PROCEDURE( 'adm-object-handle':U ).
  IF NOT VALID-HANDLE( WIDGET-HANDLE( RETURN-VALUE ) ) THEN RETURN.
  RUN get-attribute IN THIS-PROCEDURE( 'container-hidden' ).
  IF RETURN-VALUE = "Yes" THEN RETURN.

  RUN sub1-pre-row-available IN THIS-PROCEDURE NO-ERROR.
  RUN pre-row-available      IN THIS-PROCEDURE NO-ERROR.

  /* Static link between linked viewers */
  
  RUN get-attribute IN THIS-PROCEDURE ( 'Initialized':U ).
  
  IF RETURN-VALUE = "Yes" AND NOT have-records THEN
  DO:
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .
    RUN inst-row-available IN THIS-PROCEDURE NO-ERROR.
    RUN sub1-row-available IN THIS-PROCEDURE NO-ERROR.
    RUN post-row-available IN THIS-PROCEDURE NO-ERROR.
    have-records = Yes.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refresh-topic) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-topic Method-Library 
PROCEDURE refresh-topic :
/*------------------------------------------------------------------------------
  Purpose:  Refresh the topic for record-targets
------------------------------------------------------------------------------*/
DEF VAR new-topic AS CHAR NO-UNDO.

  RUN get-topic-desc( OUTPUT new-topic ).
  RETURN new-topic.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-busy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-busy Method-Library 
PROCEDURE set-busy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-idle) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-idle Method-Library 
PROCEDURE set-idle :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-test-modified) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE test-modified Method-Library 
PROCEDURE test-modified :
/*------------------------------------------------------------------------------
  Purpose:  Test to see if the current crop of fields is modified
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER record-changed AS LOGICAL NO-UNDO INITIAL No.

  /* This block of code was developed from src/adm/method/tableio.i - check-modified */
  IF VALID-HANDLE(FRAME {&FRAME-NAME}:HANDLE)
&IF DEFINED(adm-first-enabled-table) = 0 &THEN
            AND AVAILABLE({&adm-first-enabled-table})
&ENDIF
  THEN DO:
  DEF VAR curr-widget AS HANDLE NO-UNDO.

    ASSIGN curr-widget = FRAME {&FRAME-NAME}:FIRST-CHILD. /* Field group */
    ASSIGN curr-widget = curr-widget:FIRST-CHILD. /* First field */
    DO WHILE NOT record-changed AND VALID-HANDLE (curr-widget):
      record-changed = LOOKUP (curr-widget:TYPE, "FILL-IN,COMBO-BOX,EDITOR,RADIO-SET,SELECTION-LIST,SLIDER,TOGGLE-BOX":U) NE 0
                     AND curr-widget:MODIFIED.
      ASSIGN curr-widget = curr-widget:NEXT-SIBLING. /* next field */
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-find-parent-key) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION find-parent-key Method-Library 
FUNCTION find-parent-key RETURNS CHARACTER
  ( INPUT key-name AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  DEF VAR key-value AS CHAR   NO-UNDO.
  DEF VAR curr-hdl  AS HANDLE NO-UNDO.
  DEF VAR done      AS LOGI   NO-UNDO.
  DEF VAR c-rec-src AS CHAR   NO-UNDO.
  
  curr-hdl = THIS-PROCEDURE.
  
  DO WHILE NOT done:

    RUN get-link-handle IN adm-broker-hdl( curr-hdl, 'RECORD-SOURCE':U, OUTPUT c-rec-src ).
    curr-hdl = ?.
    ASSIGN curr-hdl = WIDGET-HANDLE( c-rec-src ) NO-ERROR.

    IF VALID-HANDLE( curr-hdl ) THEN DO:

      RUN get-attribute IN curr-hdl( 'Keys-Supplied' ).
      IF LOOKUP( key-name, RETURN-VALUE ) <> 0 THEN DO:
        RUN send-key IN curr-hdl( key-name, OUTPUT key-value ) NO-ERROR.
        done = NOT ERROR-STATUS:ERROR AND key-value <> ?.
      END.
    END.
    ELSE done = Yes.

  END.

  RETURN key-value.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

