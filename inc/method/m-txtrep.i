&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : m-txtrep.i
    Purpose     : Methods for producing text-based reports
    Author(s)   : Andrew McMillan
    Notes       : m-rbrun.i is included to get the correct printer name.
  ------------------------------------------------------------------------*/

DEF VAR txtrep-print-file AS CHAR NO-UNDO.
DEF VAR txtrep-control-file AS CHAR NO-UNDO.
DEF VAR txtrep-file-suffix AS CHAR NO-UNDO INITIAL ".TMP".
DEF VAR txtrep-prefix AS CHAR INITIAL "REP" NO-UNDO.
DEF VAR txtrep-preview-window AS HANDLE NO-UNDO.
DEF VAR txtrep-printer AS CHAR NO-UNDO.
DEF VAR txtrep-delim AS CHAR INITIAL "~\" NO-UNDO.
DEF VAR txtrep-output-mode AS CHAR INITIAL "txtrep" NO-UNDO.
DEF VAR txtrep-email-address AS CHAR NO-UNDO.

DEF VAR pclrep-delimiter    AS CHAR NO-UNDO INITIAL "|".
DEF VAR pclrep-font-text    AS CHAR NO-UNDO.
DEF VAR pclrep-font-code    AS CHAR NO-UNDO.
DEF VAR pclrep-font-height  AS CHAR NO-UNDO.
DEF VAR pclrep-font-cols    AS CHAR NO-UNDO.
DEF VAR pclrep-current-font AS INT NO-UNDO INITIAL ?.
DEF VAR pclrep-current-font-height AS DEC NO-UNDO.

DEF VAR pclrep-page-number   AS INT NO-UNDO INITIAL 0.
DEF VAR pclrep-page-position AS DEC NO-UNDO INITIAL 0.
DEF VAR pclrep-page-height   AS DEC NO-UNDO.
DEF VAR pclrep-preview-mode  AS LOGICAL NO-UNDO.
DEF VAR pclrep-between-pages AS LOGICAL NO-UNDO INITIAL No.

DEF VAR pclrep-footer-height AS DEC NO-UNDO.

DEF VAR htmlrep-browser      AS CHAR NO-UNDO INITIAL 'START iexplore.exe'.
DEF VAR htmlrep-css-file     AS CHAR NO-UNDO.

DEF VAR wine-printing AS LOGICAL NO-UNDO INITIAL NO.
DEF VAR apms-spool AS CHAR NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set-l.i "Windows-Printing" "windows-printing"}

IF OPSYS = "UNIX" THEN DO:
  txtrep-delim = "/".
  windows-printing = No.
END.

apms-spool = OS-GETENV( "APMSSPOOL" ).
IF apms-spool NE ? AND apms-spool NE "" THEN DO:
    wine-printing = YES.
    htmlrep-browser = 'iexplore.exe'.
END.

{inc/ofc-set.i "PDF-Output-Directory" "pdf-output-directory"}
IF NOT AVAILABLE(OfficeSetting) THEN
    pdf-output-directory = "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-htmlrep-style) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD htmlrep-style Method-Library 
FUNCTION htmlrep-style RETURNS CHARACTER
  ( INPUT sname AS CHAR, INPUT sformat AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pclrep-test-bottom) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pclrep-test-bottom Method-Library 
FUNCTION pclrep-test-bottom RETURNS LOGICAL
  ( INPUT lines AS DECIMAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-tag) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD tag Method-Library 
FUNCTION tag RETURNS CHARACTER
  ( INPUT tagtype AS CHAR, INPUT tagtext AS CHAR, INPUT attributes AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .05
         WIDTH              = 30.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-bqmgr.i}
{src/adm/method/attribut.i}
{inc/string.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

RUN txtrep-initialise.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-copy-to-printer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copy-to-printer Method-Library 
PROCEDURE copy-to-printer :
/*------------------------------------------------------------------------------
  Purpose:  Run a command to copy the report to the printer.
------------------------------------------------------------------------------*/
DEF VAR temp-bat AS CHAR INITIAL "." NO-UNDO.
DEF VAR dir-name AS CHAR NO-UNDO.
DEF VAR control-name AS CHAR NO-UNDO.
DEF VAR print-name AS CHAR NO-UNDO.

DEF VAR cmd AS CHAR NO-UNDO.
DEF VAR prt-ctrl AS CHAR NO-UNDO.
DEF VAR rows   AS INT NO-UNDO.
DEF VAR cols AS INT NO-UNDO.

  IF windows-printing THEN DO:
    OUTPUT TO PRINTER PAGE-SIZE 0.
    INPUT FROM VALUE(txtrep-control-file) BINARY.
    /* Read from control file and output to printer */
    REPEAT:
      IMPORT UNFORMATTED prt-ctrl.
      PUT CONTROL prt-ctrl.
    END.
    INPUT CLOSE.
    INPUT FROM VALUE(txtrep-print-file) BINARY.
    /* Read from print file and output to printer */
    REPEAT:
      IMPORT UNFORMATTED prt-ctrl.
      PUT CONTROL prt-ctrl.
    END.
    INPUT CLOSE.
  END.
  ELSE DO:
    /* Make sure that the printer is reset after the job */
    OUTPUT TO VALUE(txtrep-print-file) APPEND.
    RUN make-control-string( "PCL", 'reset,portrait,a4', OUTPUT prt-ctrl, OUTPUT rows, OUTPUT cols ).
    PUT CONTROL prt-ctrl.
    OUTPUT CLOSE.

    /* calculate name of temporary batch file */
    IF SESSION:TEMP-DIRECTORY <> ? THEN temp-bat = SESSION:TEMP-DIRECTORY.
    IF LENGTH(temp-bat) <> R-INDEX( temp-bat, txtrep-delim) THEN temp-bat = temp-bat + txtrep-delim.
    temp-bat = temp-bat + "PrgPrint.BAT".

    dir-name = SUBSTRING( txtrep-print-file, 1, R-INDEX(txtrep-print-file, txtrep-delim) ).
    control-name = SUBSTRING( txtrep-control-file, R-INDEX(txtrep-control-file, txtrep-delim) + 1 ).
    print-name = SUBSTRING( txtrep-print-file, R-INDEX(txtrep-print-file, txtrep-delim) + 1 ).

    IF OPSYS = "UNIX" THEN DO:
      /* dir-name = REPLACE( dir-name, txtrep-delim, "/" ). */
      OS-COMMAND SILENT VALUE( 
            "cat " + dir-name + control-name + " " + dir-name + print-name
                   + " | tee /tmp/lastrep.pcl "
                   + " | apms_lpr " + txtrep-printer ).
    END.
    ELSE DO:
      cmd = 'PP "' + txtrep-printer + '" ' + dir-name + ' ' + control-name + " " + print-name.
      /* MESSAGE cmd VIEW-AS ALERT-BOX. */

      OUTPUT TO VALUE(temp-bat).
      IF txtrep-output-mode <> "pdf" THEN DO:
          IF wine-printing THEN DO:
              OS-RENAME VALUE(txtrep-control-file) VALUE(apms-spool + control-name).
              OS-RENAME VALUE(txtrep-print-file) VALUE(apms-spool + print-name).
              OUTPUT TO VALUE(apms-spool + "print.list") APPEND.
              PUT UNFORMATTED control-name " " + print-name + "~n".
              OUTPUT CLOSE.
          END.
          ELSE DO:
              PUT UNFORMATTED cmd + "~n".
              PUT UNFORMATTED "COPY /B " + dir-name + control-name + " + " + dir-name + print-name + " " + dir-name + 'lastrep.pcl~n'.
              OUTPUT CLOSE.

              OS-COMMAND SILENT VALUE(temp-bat).
          END.
      END.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-current-printer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-current-printer Method-Library 
PROCEDURE get-current-printer :
/*------------------------------------------------------------------------------
  Purpose:     Set the value of txtrep-printer to reflect the current printer.
------------------------------------------------------------------------------*/
DEF BUFFER CurrPrinter FOR RP.

  DEF VAR user-name AS CHAR NO-UNDO.
  {inc/username.i "user-name"}

  FIND FIRST CurrPrinter WHERE
    CurrPrinter.UserName = user-name AND
    CurrPrinter.ReportID = "Current Printer" NO-LOCK NO-ERROR.
    
  IF NOT AVAILABLE CurrPrinter THEN
  DO:
    DEF VAR printer-name AS CHAR NO-UNDO.  /* we don't use this value */
    DEF VAR success AS LOGI NO-UNDO.

    printer-name = SESSION:PRINTER-NAME.
    txtrep-printer = SESSION:PRINTER-PORT.

    CREATE CurrPrinter.
    ASSIGN
      CurrPrinter.UserName = user-name
      CurrPrinter.ReportID = "Current Printer"
      CurrPrinter.Char1    = printer-name
      CurrPrinter.Char2    = txtrep-printer.

    FIND CURRENT CurrPrinter NO-LOCK.
  END.

  /* Set the value of the port (more useful than printer name for copying to it) */
  txtrep-printer = CurrPrinter.Char2.
&IF DEFINED(BQ-MGR) &THEN
  IF VALID-HANDLE(bq-mgr) AND CurrPrinter.Char4 <> "" THEN
    txtrep-printer = CurrPrinter.Char4.
&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-htmlrep-body) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE htmlrep-body Method-Library 
PROCEDURE htmlrep-body :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
PUT CONTROL "</head>~n<body>~n".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-htmlrep-finish) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE htmlrep-finish Method-Library 
PROCEDURE htmlrep-finish :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR dir-name AS CHAR NO-UNDO.
dir-name = SUBSTRING( txtrep-print-file, 1, R-INDEX(txtrep-print-file, txtrep-delim) ).

/* OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES APPEND PAGE-SIZE 0. */
PUT CONTROL "</body~n</html>~n~n".
OUTPUT CLOSE.

IF htmlrep-css-file NE ? AND htmlrep-css-file NE "" THEN DO:
  htmlrep-css-file = REPLACE( htmlrep-css-file, '/', txtrep-delim).
  OS-COMMAND SILENT VALUE( 'COPY /Y "' + htmlrep-css-file + '" "' + dir-name + '"~n').
END.
/* MESSAGE '"' + htmlrep-browser + '" ' + txtrep-print-file . */

IF apms-spool EQ ? OR apms-spool EQ "" THEN DO:
  OS-COMMAND VALUE( htmlrep-browser + ' "' + txtrep-print-file + '"' ).
END.

/* Save the file so we can see it later */
OS-DELETE VALUE(  dir-name + 'lastrep.html' )  NO-ERROR.
OS-COMMAND SILENT VALUE( 'COPY /Y "' + txtrep-print-file + '" "' + dir-name + 'lastrep.html"~n').

OS-DELETE VALUE( txtrep-control-file )  NO-ERROR.
/*
OS-DELETE VALUE( txtrep-print-file )    NO-ERROR.
*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-htmlrep-start) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE htmlrep-start Method-Library 
PROCEDURE htmlrep-start :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER page-title AS CHAR NO-UNDO.

PUT CONTROL '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">~n'
  + '<html>~n<head>~n<title>' + page-title + "</title>~n"
  + '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">~n'.

    txtrep-output-mode   = "htmlrep".
    pclrep-preview-mode  = TRUE.
    pclrep-font-text     = "".
    pclrep-font-code     = "".
    pclrep-font-height   = "0" .
    pclrep-current-font-height = 0.
    pclrep-font-cols     = "1".
    pclrep-current-font  = 1.
    pclrep-page-number   = 0.
    pclrep-page-position = 0.
    pclrep-between-pages = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-make-control-string) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE make-control-string Method-Library 
PROCEDURE make-control-string :
/*------------------------------------------------------------------------------
  Purpose:  Build the control string for the printer.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER txtrep-language AS CHAR NO-UNDO. /* currently must be "PCL" */
DEF INPUT PARAMETER txtrep-commands AS CHAR NO-UNDO.  /* comma separated list */
DEF OUTPUT PARAMETER txtrep-ctrl AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER txtrep-rows AS INT NO-UNDO.
DEF OUTPUT PARAMETER txtrep-cols AS INT NO-UNDO.

DEF VAR page-height AS DEC NO-UNDO.
DEF VAR line-height AS DEC NO-UNDO.

  CASE txtrep-language:
    WHEN "PCL" THEN DO:
      RUN pcl-control-string( txtrep-commands, OUTPUT txtrep-ctrl,
                     OUTPUT txtrep-cols, OUTPUT line-height, OUTPUT page-height ).
      txtrep-rows = INTEGER( (page-height / line-height) - 2.5 ).
/*      MESSAGE line-height SKIP page-height. */
    END.
    OTHERWISE DO:
      MESSAGE "Printer language " + txtrep-language + " is not supported."
            VIEW-AS ALERT-BOX ERROR TITLE "Bad Printer Language".
    END.
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-output-control-file) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE output-control-file Method-Library 
PROCEDURE output-control-file :
/*------------------------------------------------------------------------------
  Purpose:  Send the output to a file.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER txtrep-printer-control AS CHAR NO-UNDO.

  OUTPUT TO VALUE(txtrep-control-file) PAGE-SIZE 0.
  PUT CONTROL txtrep-printer-control.
  OUTPUT CLOSE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pcl-control-string) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pcl-control-string Method-Library 
PROCEDURE pcl-control-string :
/*------------------------------------------------------------------------------
  Purpose:  Control string for PCL
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER txtrep-commands AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER txtrep-ctrl AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER txtrep-cols AS INT NO-UNDO.
DEF OUTPUT PARAMETER line-height AS DEC NO-UNDO.
DEF OUTPUT PARAMETER page-height AS DEC NO-UNDO INITIAL 11.25.

DEF VAR i AS INT NO-UNDO.
DEF VAR command AS CHAR NO-UNDO.
DEF VAR no-commands AS INT NO-UNDO.
no-commands = NUM-ENTRIES( txtrep-commands ).

&SCOP NUM-RESULTS 9
DEF VAR result AS CHAR EXTENT {&NUM-RESULTS} INITIAL "" NO-UNDO.

DEF VAR page-width AS DECIMAL INITIAL 7.75 NO-UNDO.
DEF VAR width AS LOGICAL INITIAL yes NO-UNDO.
DEF VAR cpi AS DECIMAL INITIAL 10 NO-UNDO.
DEF VAR lpi AS DECIMAL INITIAL 6 NO-UNDO.
DEF VAR lm AS INTEGER INITIAL 0 NO-UNDO.
DEF VAR tm AS INTEGER INITIAL 0 NO-UNDO.


&IF DEFINED(DEBUG-END) &THEN
  debug-event("Printer control command is: " + txtrep-commands ).
&ENDIF

  /****  For each command in the txtrep-commands we build up the various components
         of the control string  *****************************************/
  DO i = 1 TO no-commands:
    command = ENTRY( i, txtrep-commands ).
    CASE command:
      WHEN 'eject'      THEN result[1] = result[1] + "0h".

      WHEN 'reset'      THEN ASSIGN
        result[2] = "e"
        result[4] = result[4] + "0l".

      WHEN 'lm'        THEN DO:
        i = i + 1.
        lm = INTEGER( ENTRY( i, txtrep-commands ) ).
        result[5] = result[5] + ENTRY( i, txtrep-commands ) + "l".
      END.


      WHEN 'a4'         THEN result[4] = result[4] + "26a".
      WHEN 'portrait'   THEN ASSIGN
            result[4] = result[4] + "0o"      page-width =  7.75    page-height = 11.25 .
      WHEN 'landscape'  THEN ASSIGN
            result[4] = result[4] + "1o"      page-width = 11.25    page-height =  7.75 .
      WHEN 'duplex'     THEN result[4] = result[4] + "1s".
      WHEN 'simplex'    THEN result[4] = result[4] + "0s".
      WHEN 'short'      THEN result[4] = result[4] + "2s".
      WHEN 'bin'        THEN DO:
        i = i + 1.
        result[4] = result[4] + ENTRY( i, txtrep-commands ) + "h".
      END.
      WHEN 'lpi'        THEN DO:
        i = i + 1.
        lpi = INTEGER( ENTRY( i, txtrep-commands ) ) .
        /* set vertical motion index */
        result[4] = result[4] + STRING( (48 / lpi) ) + "c".
      END.
      WHEN 'tm'        THEN DO:
        i = i + 1.
        tm = INTEGER( ENTRY( i, txtrep-commands ) ).
        result[4] = result[4] + ENTRY( i, txtrep-commands ) + "e".
      END.



      WHEN 'courier'    THEN ASSIGN
            result[7] = "10U"           result[9] = result[9] + "4099t" .
      WHEN 'lineprinter' THEN ASSIGN
            result[7] = "10U"
            result[8] = result[8] + "0p16.67h8.5v0s0b"
            result[9] = result[9] + "0t"    cpi = 16.66     lpi = 8.5 .
      WHEN 'times'      THEN ASSIGN
            result[7] = "8U"            result[9] = result[9] + "4101t" width = no .
      WHEN 'univers'  THEN ASSIGN
            result[7] = "8U"            result[9] = result[9] + "4148t" width = no .
      WHEN 'helvetica'  THEN ASSIGN
            result[7] = "8U"            result[9] = result[9] + "4148t" width = no .
      WHEN 'garamond'  THEN ASSIGN
            result[7] = "8U"            result[9] = result[9] + "4197t" width = no .
      WHEN 'antiqueolive'  THEN ASSIGN
            result[8] = result[8] + "1p0s0b"
            result[7] = "8U"            result[9] = result[9] + "4168t" width = no .
      WHEN 'coronet'  THEN ASSIGN
            result[8] = result[8] + "1p1s0b"
            result[7] = "8U"            result[9] = result[9] + "4116t" width = no .
      WHEN 'marigold'  THEN ASSIGN
            result[7] = "8U"            result[9] = result[9] + "4297t" width = no .
      WHEN 'albertus'  THEN ASSIGN
            result[7] = "8U"            result[9] = result[9] + "4362t" width = no .
      WHEN 'omega'  THEN ASSIGN
            result[7] = "8U"            result[9] = result[9] + "4113t" width = no .
      WHEN 'fixed'          THEN ASSIGN result[8] = result[8] + "0p"   width = yes .
      WHEN 'proportional'   THEN ASSIGN result[8] = result[8] + "1p"   width = no .
      WHEN 'expanded'       THEN result[8] = result[8] + "24s".
      WHEN 'outline'        THEN result[8] = result[8] + "32s".
      WHEN 'black'          THEN result[8] = result[8] + "5b".
      WHEN 'thin'           THEN result[8] = result[8] + "-5b".
      WHEN 'italic'         THEN result[8] = result[8] + "1s".
      WHEN 'bold'           THEN result[8] = result[8] + "3b".
      WHEN 'light'          THEN result[8] = result[8] + "-3b".
      WHEN 'normal'         THEN result[8] = result[8] + "0b0s".
      WHEN 'point'     THEN DO:
        i = i + 1.
        ASSIGN
          result[8] = result[8] + ENTRY( i, txtrep-commands ) + "v"
          /* 137 is an approximation - w:h scale is dependent on font */
          cpi = 137 / INTEGER( ENTRY( i, txtrep-commands ) )
          /* 72 is good because that's how 'point' is defined */
          lpi = 72 / INTEGER( ENTRY( i, txtrep-commands ) ).
      END.
      WHEN 'cpi'     THEN DO:
        i = i + 1.
        ASSIGN
          result[8] = result[8] + ENTRY( i, txtrep-commands ) + "h"
          cpi = INTEGER( ENTRY( i, txtrep-commands ) ) .
      END.

    END CASE.

  END.


  /**** Finally!  We build the printer control string ********************/
  txtrep-ctrl = "".
  result[8] = result[8] + result[9].
  result[9] = "".
  DO i = 1 TO {&NUM-RESULTS}:
    IF result[i] <> "" THEN DO:
      /* Convert trailing letter to caps as end of sequence */
      SUBSTRING( result[i], LENGTH(result[i]), 1) =
                 CAPS( SUBSTRING( result[i], LENGTH(result[i]), 1) ).
      txtrep-ctrl = txtrep-ctrl + "~033".
      CASE i:
        WHEN 1 THEN txtrep-ctrl = txtrep-ctrl + "&l".   /* eject */
        WHEN 2 THEN /* no prefix applies */.    /* reset */
        WHEN 3 THEN txtrep-ctrl = txtrep-ctrl + "".     /* not used */
        WHEN 4 THEN txtrep-ctrl = txtrep-ctrl + "&l".   /* page characteristics */
        WHEN 5 THEN txtrep-ctrl = txtrep-ctrl + "&a".   /* margins (left only) */
        WHEN 6 THEN txtrep-ctrl = txtrep-ctrl + "".     /* not used */
        WHEN 7 THEN txtrep-ctrl = txtrep-ctrl + "(".    /* symbol set */
        WHEN 8 THEN txtrep-ctrl = txtrep-ctrl + "(s".   /* font characteristics */
        WHEN 9 THEN /* added onto '4' */ .      /* font */
      END CASE.

      txtrep-ctrl = txtrep-ctrl + result[i].
    END.
  END.

  /**** Calculate the cols (sometimes this will be garbage!) */
  /**** The line-height and page-height is more dependable   */
  ASSIGN
    txtrep-cols = INT( IF width THEN (cpi * page-width - 0.5) - lm ELSE 9999 )
    line-height = 25.4 / lpi
    page-height = (page-height * 25.4) - ((tm + 2) * line-height)
  .

  IF txtrep-output-mode = "pdf" THEN DO:
    txtrep-ctrl = "[-pdf-]>CONTROL:" + txtrep-commands + "[-pdf-]".
  END.


&IF DEFINED(DEBUG-END) &THEN
  debug-event("Printer control string is: " + txtrep-ctrl ).
  debug-event("line-height=" + STRING(line-height)
         + ",  page-height=" + STRING(page-height)
         + ",  txtrep-cols=" + STRING(txtrep-cols)
         + ",  cpi=" + STRING(cpi) + ",  lpi=" + STRING(lpi)
         + ",  lm=" + STRING(lm) + ",  tm=" + STRING(tm) ).
&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pcl-move-relative) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pcl-move-relative Method-Library 
PROCEDURE pcl-move-relative :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER rows AS DEC NO-UNDO.
  DEF INPUT PARAMETER cols AS DEC NO-UNDO.
  
  IF txtrep-output-mode = "pdf" THEN DO:
    PUT CONTROL "[-pdf-]>MOVEREL:" + TRIM(STRING( rows, "+>>>>9.99" ) ) + ",".
    pclrep-page-position = pclrep-page-position + ( rows * pclrep-current-font-height ).
    IF cols <> ? THEN PUT CONTROL TRIM( STRING( cols, "+>>>>9.99" ) ).
    PUT CONTROL "[-pdf-]".
  END.
  ELSE DO:
    PUT CONTROL CHR(27) + "&a" + TRIM( STRING( rows, "+>>>>9.99" ) ) + "R".
    pclrep-page-position = pclrep-page-position + ( rows * pclrep-current-font-height ).

    IF cols = ? THEN
      PUT CONTROL CHR(12).
    ELSE
      PUT CONTROL CHR(27) + "&a" + TRIM( STRING( cols, "+>>>>9.99" ) ) + "C".
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pcl-moveto) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pcl-moveto Method-Library 
PROCEDURE pcl-moveto :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER rows AS DEC NO-UNDO.
  DEF INPUT PARAMETER cols AS DEC NO-UNDO.
  
  IF txtrep-output-mode = "pdf" THEN DO:
    PUT CONTROL "[-pdf-]>MOVEABS:" + TRIM(STRING( rows, "+>>>>9.99" ) ) + ","
                           + TRIM( STRING( cols, "+>>>>9.99" ) ) + "[-pdf-]".
  END.
  ELSE DO:
    PUT CONTROL CHR(27) + "&a" + TRIM( STRING( rows, "->>>>9.99" ) ) + "R".
    PUT CONTROL CHR(27) + "&a" + TRIM( STRING( cols, "->>>>9.99" ) ) + "C".
  END.
  pclrep-page-position = rows * pclrep-current-font-height .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pclrep-down-by) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pclrep-down-by Method-Library 
PROCEDURE pclrep-down-by :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER rows AS DEC NO-UNDO.

  IF pclrep-preview-mode THEN DO:
    rows = ROUND( rows, 0).
    IF NOT(pclrep-between-pages) AND pclrep-test-bottom( rows ) THEN
      RUN pclrep-page-break.        /* footer half */
    ELSE IF rows >= 1 THEN DO:
      IF NOT(pclrep-between-pages) THEN DO:
        IF pclrep-page-position <= 0 THEN RUN pclrep-page-break.   /* header half */
      END.
      RUN pclrep-line( "", FILL("~n", INT(rows)) ).
    END.
  END.
  ELSE IF NOT(pclrep-between-pages) AND pclrep-test-bottom( rows ) THEN
    RUN pclrep-page-break.          /* footer half */
  ELSE DO:
    IF NOT(pclrep-between-pages) THEN DO:
      IF pclrep-page-position <= 0 THEN RUN pclrep-page-break.   /* header half */
    END.
    PUT CONTROL CHR(27) + "&a" + TRIM( STRING( rows, "+>>>>9.999" ), " 9" ) + "R".
    pclrep-page-position = pclrep-page-position + ( rows * pclrep-current-font-height ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pclrep-finish) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pclrep-finish Method-Library 
PROCEDURE pclrep-finish :
/*------------------------------------------------------------------------------
  Purpose:  Finish running a pclrep report
------------------------------------------------------------------------------*/

 RUN pclrep-page-break.
 RUN view-output-file ( pclrep-preview-mode ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pclrep-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pclrep-line Method-Library 
PROCEDURE pclrep-line :
/*------------------------------------------------------------------------------
  Purpose:  Print a line in a selected font
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER font-text AS CHAR NO-UNDO.
DEF INPUT PARAMETER line-text AS CHAR NO-UNDO.

  IF font-text = ? THEN font-text = "".
  IF line-text = ? THEN line-text = "".
  
  DEF VAR position-increment AS DEC NO-UNDO.
  DEF VAR i AS INT NO-UNDO.

/*  MESSAGE "pclrep-line" pclrep-between-pages pclrep-page-position . */
  /* initialisation, or first half of page break, sets page-position to 0 */
  IF NOT(pclrep-between-pages) THEN DO:
    IF pclrep-page-position <= 0 THEN RUN pclrep-page-break.   /* header half */
  END.

  RUN pclrep-select-font( font-text ).
  position-increment = (IF line-text = "" THEN 1 ELSE NUM-ENTRIES(line-text, "~n")).
    
  IF NOT(pclrep-between-pages) THEN DO:
    /* If we're not in a page break we might be over the end of the page */
    IF pclrep-test-bottom(position-increment) THEN DO:
      RUN pclrep-page-break.        /* footer half */
      RUN pclrep-page-break.        /* header half */
      RUN pclrep-select-font( font-text ).
    END.
  END.

  IF TRIM(line-text, " ") = "" THEN PUT SKIP(1).
  ELSE PUT UNFORMATTED line-text SKIP.
  pclrep-page-position = pclrep-page-position + pclrep-current-font-height * position-increment.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pclrep-page-break) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pclrep-page-break Method-Library 
PROCEDURE pclrep-page-break :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  /* This variable turns off the pagination logic so we don't get too recursive */
 /* MESSAGE 'page break' pclrep-page-position. */
  IF pclrep-page-position > 0 THEN DO:
    /* footer half, since we're somewhere on the page... */
    pclrep-between-pages = Yes.
    RUN inst-page-footer IN THIS-PROCEDURE NO-ERROR.
    pclrep-between-pages = No.
    PUT CONTROL CHR(12).
    pclrep-page-position = 0.
    RETURN.
  END.

  /* otherwise it must be the header half */
  pclrep-page-number = pclrep-page-number + 1.

  pclrep-between-pages = Yes.
  RUN inst-page-header IN THIS-PROCEDURE NO-ERROR.
  pclrep-between-pages = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pclrep-paragraph) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pclrep-paragraph Method-Library 
PROCEDURE pclrep-paragraph :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER this-font AS CHAR NO-UNDO.
DEF INPUT PARAMETER s1 AS CHAR NO-UNDO.
DEF INPUT PARAMETER cols AS INT NO-UNDO.
DEF INPUT PARAMETER lm AS INT NO-UNDO.

DEF VAR wrapped AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.

  wrapped = WRAP( s1, cols  ).
  n = NUM-ENTRIES( wrapped, "~n" ).
  DO i = 1 TO n:
    RUN pclrep-line( this-font, FILL(" ",lm) + ENTRY( i, wrapped, "~n" ) ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pclrep-select-font) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pclrep-select-font Method-Library 
PROCEDURE pclrep-select-font :
/*------------------------------------------------------------------------------
  Purpose:  Select a particular font for this (and subsequent) lines.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER font-text AS CHAR NO-UNDO.

  IF pclrep-preview-mode OR font-text = "" OR font-text = ? THEN RETURN.

  IF font-text = ENTRY( pclrep-current-font, pclrep-font-text, pclrep-delimiter) THEN
    RETURN.     /* quick check results in nothing to do */

  pclrep-current-font = LOOKUP( font-text, pclrep-font-text, pclrep-delimiter).

  IF pclrep-current-font = 0 THEN
  DO:

    /* add into the list */
    DEF VAR code AS CHAR NO-UNDO.
    DEF VAR cols  AS DEC NO-UNDO.
    DEF VAR height AS DEC NO-UNDO.
    DEF VAR ignored AS DEC NO-UNDO.

    pclrep-current-font = NUM-ENTRIES( pclrep-font-text, pclrep-delimiter ) + 1.
    RUN pcl-control-string ( font-text, OUTPUT code, OUTPUT cols,
                                                   OUTPUT height, OUTPUT ignored).
    IF height < 0.01 THEN height = 1.   /* just to catch wierd conditions */
    ASSIGN
        pclrep-font-text   = pclrep-font-text   + pclrep-delimiter + font-text
        pclrep-font-code   = pclrep-font-code   + pclrep-delimiter + code
        pclrep-font-height = pclrep-font-height + pclrep-delimiter + STRING( height )
        pclrep-current-font-height = height
        pclrep-font-cols   = pclrep-font-cols   + pclrep-delimiter + STRING( cols ).
        
  END.
  ELSE
    pclrep-current-font-height = DEC(ENTRY( pclrep-current-font, pclrep-font-height, pclrep-delimiter )).

  /* Change the font */
  IF txtrep-output-mode = "pdf" THEN DO:
    PUT CONTROL "[-pdf-]>CONTROL:" + font-text + "[-pdf-]".
  END.
  ELSE DO:
    PUT CONTROL ENTRY( pclrep-current-font, pclrep-font-code, pclrep-delimiter ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pclrep-skip-to) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pclrep-skip-to Method-Library 
PROCEDURE pclrep-skip-to :
/*------------------------------------------------------------------------------
  Purpose:  Skip to a position on the page
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-pos AS DEC NO-UNDO.

/*
DEF VAR y-pos AS DECIMAL NO-UNDO.
  y-pos = new-pos - DECIMAL( ENTRY(pclrep-current-font,pclrep-font-height,pclrep-delimiter)).
*/

  DO WHILE pclrep-page-position <= new-pos:
    RUN pclrep-line( "", "").
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pclrep-start) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pclrep-start Method-Library 
PROCEDURE pclrep-start :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER preview AS LOGICAL NO-UNDO.
DEF INPUT PARAMETER page-reset AS CHAR NO-UNDO.

DEF VAR prt-ctrl AS CHAR NO-UNDO.
DEF VAR line-height AS DEC NO-UNDO.
DEF VAR cols AS DEC NO-UNDO.

  RUN pcl-control-string ( "reset," + page-reset, OUTPUT prt-ctrl, OUTPUT cols,
                        OUTPUT line-height, OUTPUT pclrep-page-height ).
  RUN output-control-file ( prt-ctrl ).

  ASSIGN
    pclrep-preview-mode  = preview
    pclrep-font-text     = page-reset
    pclrep-font-code     = prt-ctrl
    pclrep-font-height   = STRING( line-height )
    pclrep-current-font-height = line-height
    pclrep-font-cols     = STRING( cols )
    pclrep-current-font  = 1
    pclrep-page-number   = 0
    pclrep-page-position = 0
    pclrep-between-pages = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-send-to-printer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-to-printer Method-Library 
PROCEDURE send-to-printer :
/*------------------------------------------------------------------------------
  Purpose:  Send the report to the printer via Windows.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER font-num AS INTEGER NO-UNDO.
DEF INPUT PARAMETER landscape AS LOGICAL NO-UNDO.
DEF INPUT PARAMETER show-dialog AS LOGICAL NO-UNDO.

DEF VAR lsuccess AS LOGI INITIAL No NO-UNDO.
DEF VAR pr-flags AS INT NO-UNDO.

  pr-flags = (IF show-dialog  THEN 1 ELSE 0)
           + (IF landscape    THEN 2 ELSE 0).

  RUN adecomm/_osprint.p ( CURRENT-WINDOW, txtrep-print-file, font-num, pr-flags,
                           0, 0, OUTPUT lsuccess ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-viewer-title) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-viewer-title Method-Library 
PROCEDURE set-viewer-title :
/*------------------------------------------------------------------------------
  Purpose:  Set the title in the report viewer window.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER title-text AS CHAR NO-UNDO.

  RUN set-title IN txtrep-preview-window ( title-text ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-txtrep-do-email) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE txtrep-do-email Method-Library 
PROCEDURE txtrep-do-email :
/*------------------------------------------------------------------------------
  Purpose: 
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER email-string AS CHAR NO-UNDO.
DEF INPUT PARAMETER email-subject AS CHAR NO-UNDO.
DEF INPUT PARAMETER email-template AS CHAR NO-UNDO.

IF email-string <> "" AND email-template <> "" THEN DO:
  IF txtrep-output-mode = "pdf" THEN
    PUT CONTROL "[-pdf-]>EMAIL:" + email-string + "|" + email-subject + "|" + email-template + "[-pdf-]".
END.
ELSE
  MESSAGE "Email and email template parameters required"
    VIEW-AS ALERT-BOX ERROR TITLE "Email or template blank".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-txtrep-initialise) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE txtrep-initialise Method-Library 
PROCEDURE txtrep-initialise :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the important parameters
------------------------------------------------------------------------------*/
DEF VAR temp-dir AS CHAR NO-UNDO.

  temp-dir = OS-GETENV("APMSTEMP":U).
  IF temp-dir = ? OR temp-dir = "" THEN temp-dir = OS-GETENV("TMP":U).
  IF temp-dir = ? OR temp-dir = "" THEN temp-dir = OS-GETENV("TEMP":U).
  IF temp-dir = ? OR temp-dir = "" THEN temp-dir = SESSION:TEMP-DIRECTORY .
  temp-dir = temp-dir + txtrep-delim.

  txtrep-print-file = temp-dir + txtrep-prefix + STRING( TIME, "99999") + txtrep-file-suffix.
  txtrep-control-file = temp-dir + "CTL" + STRING( TIME, "99999") + txtrep-file-suffix.

  IF txtrep-delim NE "/" THEN DO:
    DO WHILE INDEX( txtrep-print-file, "/" ) > 0:
      SUBSTRING( txtrep-print-file, INDEX( txtrep-print-file, "/" ), 1) = txtrep-delim.
    END.
    DO WHILE INDEX( txtrep-control-file, "/" ) > 0:
      SUBSTRING( txtrep-control-file, INDEX( txtrep-control-file, "/" ), 1) = txtrep-delim.
    END.
  END.

  /* Set the 'txtrep-printer' to printer port */
  RUN get-current-printer.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-txtrep-output-mode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE txtrep-output-mode Method-Library 
PROCEDURE txtrep-output-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER mode AS CHAR NO-UNDO.

txtrep-output-mode = mode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-txtrep-pdf-filename) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE txtrep-pdf-filename Method-Library 
PROCEDURE txtrep-pdf-filename :
/*------------------------------------------------------------------------------
  Purpose:     Indicate that the output will be a PDF and the filename
  Parameters:  filename (without the extension)
  Notes:
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER pdf-file-name AS CHAR NO-UNDO.

txtrep-print-file = pdf-output-directory + '/' + pdf-file-name + '.protopdf'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-view-output-file) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE view-output-file Method-Library 
PROCEDURE view-output-file :
/*------------------------------------------------------------------------------
  Purpose:  Close the output file now that we've finished with it.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER txtrep-is-preview AS LOGI NO-UNDO.

  IF txtrep-is-preview THEN DO:
    RUN win/w-report.w PERSISTENT SET txtrep-preview-window.
    RUN dispatch IN txtrep-preview-window ( 'initialize':U ).
    RUN load-editor IN txtrep-preview-window ( txtrep-print-file, txtrep-control-file ).
    WAIT-FOR CLOSE OF txtrep-preview-window.
  END.
  ELSE DO:
    RUN copy-to-printer.
DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
    IF CAN-FIND( UsrGroupMember WHERE UsrGroupMember.UserName = user-name AND UsrGroupMember.GroupName = "Programmer") THEN PAUSE 5 NO-MESSAGE.
    OS-DELETE VALUE( txtrep-control-file )  NO-ERROR.
    IF txtrep-output-mode <> "pdf" THEN
        OS-DELETE VALUE( txtrep-print-file ) NO-ERROR.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-vs-output-file) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE vs-output-file Method-Library 
PROCEDURE vs-output-file :
/*------------------------------------------------------------------------------
  Purpose:  Close the output file now that we've finished with it.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER txtrep-is-preview AS LOGI NO-UNDO.
DEF INPUT PARAMETER font-num AS INTEGER NO-UNDO.
DEF INPUT PARAMETER landscape AS LOGICAL NO-UNDO.
DEF INPUT PARAMETER show-dialog AS LOGICAL NO-UNDO.

  IF txtrep-is-preview THEN DO:
    RUN win/w-report.w PERSISTENT SET txtrep-preview-window.
    RUN dispatch IN txtrep-preview-window ( 'initialize':U ).
    RUN load-editor IN txtrep-preview-window ( txtrep-print-file, txtrep-control-file ).
    RUN set-print-details IN txtrep-preview-window ( font-num, landscape, show-dialog ) NO-ERROR.
  END.
  ELSE DO:
    RUN send-to-printer( font-num, landscape, show-dialog ).
    OS-DELETE VALUE( txtrep-control-file ).
    OS-DELETE VALUE( txtrep-print-file ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-htmlrep-style) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION htmlrep-style Method-Library 
FUNCTION htmlrep-style RETURNS CHARACTER
  ( INPUT sname AS CHAR, INPUT sformat AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Since Progress deals less handily with braces...
------------------------------------------------------------------------------*/

  RETURN "    " + sname + "~{" + sformat + "~}~n".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pclrep-test-bottom) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pclrep-test-bottom Method-Library 
FUNCTION pclrep-test-bottom RETURNS LOGICAL
  ( INPUT lines AS DECIMAL ) :
/*---------------------------------------------------------------------------
  Returns 'TRUE' if current position + 'lines' would be off the bottom.
---------------------------------------------------------------------------*/
DEF VAR test-pos AS DEC NO-UNDO.
DEF VAR text-end AS DEC NO-UNDO.

  test-pos = (pclrep-page-position + (lines * pclrep-current-font-height)).
  text-end = (pclrep-page-height - pclrep-footer-height).

/*
&IF DEFINED(DEBUG-END) &THEN
    debug-event( "Page position " + STRING(pclrep-page-position)
           + ",   Test position " + STRING(test-pos)
           + ",   Text ends at " + STRING(text-end)
           + ",   Lines " + STRING(lines)
           + ",   Pg Height " + STRING(pclrep-page-height)
           + ",   Ftr Height " + STRING(pclrep-footer-height)
           + ",   Font Height " + STRING(pclrep-current-font-height)).
&ENDIF
*/

  RETURN test-pos >= text-end .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-tag) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION tag Method-Library 
FUNCTION tag RETURNS CHARACTER
  ( INPUT tagtype AS CHAR, INPUT tagtext AS CHAR, INPUT attributes AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Enclose the tagtext value in quotes
    Notes:  
------------------------------------------------------------------------------*/
IF tagtype = ? THEN tagtype = 'p'.
IF tagtext = ? THEN tagtext = ''.
IF attributes = ? THEN attributes = ''.  
IF attributes <> '' THEN attributes = ' ' + attributes.
  
RETURN "<" + tagtype + attributes + ">"
       + tagtext
       + "</" + tagtype + ">".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

