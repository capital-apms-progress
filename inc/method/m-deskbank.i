&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*------------------------------------------------------------------------
    Library     : m-deskbank.i
    Purpose     :Methods for producing output file suitable for Import
                        by Westpac DeskBank
    Syntax      :
    Description :
    Author(s)   : William Stokes
    Created     : 30/03/2004
    Notes       :
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEFINE VARIABLE v-deskbank-filename AS CHARACTER INITIAL '' NO-UNDO. 
DEFINE VARIABLE v-deskbank-error-filename AS CHARACTER INITIAL '' NO-UNDO.
DEFINE VARIABLE v-deskbank-payer-bank AS INTEGER FORMAT "99" INITIAL 0 NO-UNDO. 
DEFINE VARIABLE v-deskbank-payer-branch AS INTEGER FORMAT "9999" INITIAL 0 NO-UNDO. 
DEFINE VARIABLE v-deskbank-payer-account AS INTEGER FORMAT "99999999" INITIAL 0 NO-UNDO. 
DEFINE VARIABLE v-deskbank-payer-suffix AS INTEGER FORMAT "9999" INITIAL 0 NO-UNDO. 
DEFINE VARIABLE v-deskbank-payer-payername AS CHARACTER FORMAT "X(20)" INITIAL '' NO-UNDO.
/* Not currently used but field specified */
DEFINE VARIABLE v-deskbank-payer-custnum AS INTEGER FORMAT "999999" INITIAL 0 NO-UNDO.
/* Miscellaneous Variables */
DEFINE VARIABLE v-deskbank-initialized AS LOGICAL INITIAL FALSE NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = 11.16
         WIDTH              = 77.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-deskbank-finish) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deskbank-finish Method-Library 
PROCEDURE deskbank-finish :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

IF NOT v-deskbank-initialized THEN RETURN 'FAIL'.

RETURN 'OK'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-deskbank-initialize) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deskbank-initialize Method-Library 
PROCEDURE deskbank-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEFINE INPUT PARAMETER p-output-filename AS CHARACTER INITIAL '' NO-UNDO. 
DEFINE INPUT PARAMETER p-output-error-filename AS CHARACTER INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payer-bank-details AS CHARACTER INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payer-payername AS CHARACTER FORMAT "X(20)" INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payer-custnum AS INTEGER FORMAT "999999" INITIAL 0 NO-UNDO.

/* Assign Global Variables - Payer Bank Account Details */
RUN get-bankbranchaccountsuffix (
    p-payer-bank-details,
    OUTPUT v-deskbank-payer-bank,
    OUTPUT v-deskbank-payer-branch,
    OUTPUT v-deskbank-payer-account,
    OUTPUT v-deskbank-payer-suffix
).
IF RETURN-VALUE = 'FAIL' THEN DO:
    RETURN 'Cannot Make Sense of Payer Bank Account Number ' + p-payer-bank-details.
END.

/* Assign Parameters to Method Variables */
ASSIGN
    v-deskbank-filename = p-output-filename
    v-deskbank-error-filename = p-output-error-filename
    v-deskbank-payer-payername = p-payer-payername
    v-deskbank-payer-custnum = p-payer-custnum.

/* Have We Got Payer Bank Account */
IF v-deskbank-payer-bank <> 03 THEN DO:
    RETURN 'Payer Account ' + BankAccount.BankAccount + ' is not a Westpac Bank Account.'.
END.

v-deskbank-initialized = TRUE.
RETURN 'OK'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-deskbank-payment-detailline) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deskbank-payment-detailline Method-Library 
PROCEDURE deskbank-payment-detailline :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: FORMAT Phrase in Define statements is for refrence only.
         If you wish to alter output line format then alter STRING(..., FORMAT Phrase) too.
------------------------------------------------------------------------------*/

DEFINE INPUT PARAMETER p-sequence AS INTEGER FORMAT "999999" INITIAL 0 NO-UNDO.
DEFINE INPUT PARAMETER p-payee-bank-account LIKE BankAccount.BankAccount INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payee-name AS CHARACTER FORMAT "X(20)" INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payee-particulars AS CHARACTER FORMAT "X(12)" INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payee-analysis-code AS CHARACTER FORMAT "X(12)" INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-payee-reference AS CHARACTER FORMAT "X(12)" INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-amount AS DECIMAL FORMAT "9999999999999.99" DECIMALS 2 INITIAL 0 NO-UNDO.

DEFINE VARIABLE v-payee-bank AS INTEGER FORMAT "99" INITIAL 0 NO-UNDO. 
DEFINE VARIABLE v-payee-branch AS INTEGER FORMAT "9999" INITIAL 0 NO-UNDO. 
DEFINE VARIABLE v-payee-account AS INTEGER FORMAT "99999999" INITIAL 0 NO-UNDO. 
DEFINE VARIABLE v-payee-suffix AS INTEGER FORMAT "9999" INITIAL 0 NO-UNDO. 
DEFINE VARIABLE v-amount AS INTEGER FORMAT "999999999999999" INITIAL 0 NO-UNDO.

/* Payment Constants */
DEFINE VARIABLE v-transaction-code AS INTEGER FORMAT "99" INITIAL 50 NO-UNDO.
DEFINE VARIABLE v-mts-source AS CHARACTER FORMAT "X(2)" INITIAL 'DC' NO-UNDO.

IF NOT v-deskbank-initialized THEN RETURN 'Generation did not initialize'.

/* 'ReMove' Decimal Point */
v-amount = INTEGER(p-amount * 100).

/* Assign Variables - Payee Bank Account Details */
RUN get-bankbranchaccountsuffix (
    p-payee-bank-account,
    OUTPUT v-payee-bank,
    OUTPUT v-payee-branch,
    OUTPUT v-payee-account,
    OUTPUT v-payee-suffix
 ).
IF RETURN-VALUE = 'FAIL' THEN DO:
    RETURN 'Cannot make sense of Payee Bank Account Number' + STRING(p-payee-bank-account).
END.

PUT STREAM s-bi-output UNFORMATTED
    'D'
    STRING( p-sequence, "999999" )
    STRING( v-payee-bank, "99" )
    STRING( v-payee-branch, "9999" )
    STRING( v-payee-account, "99999999" )
    STRING( v-payee-suffix, "9999" )
    STRING( v-transaction-code, "99" )
    STRING( v-mts-source, "X(2)" )
    STRING( v-amount, "999999999999999" )
    STRING( p-payee-name, "X(20)" )
    STRING( p-payee-particulars, "X(12)" )
    STRING( p-payee-analysis-code, "X(12)" )
    STRING( p-payee-reference, "X(12)" )
    STRING( v-deskbank-payer-bank, "99" )
    STRING( v-deskbank-payer-branch, "9999" )
    STRING( v-deskbank-payer-account, "99999999" )
    STRING( v-deskbank-payer-suffix, "9999" )
    STRING( v-deskbank-payer-payername, "X(20)" )
    STRING( '', "X(42)" )
    CHR(13) CHR(10).

RETURN "OK".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-deskbank-payment-headerline) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deskbank-payment-headerline Method-Library 
PROCEDURE deskbank-payment-headerline :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: FORMAT Phrase in Define statements is for refrence only.
         If you wish to alter output line format then alter STRING(..., FORMAT Phrase) too.
------------------------------------------------------------------------------*/

DEFINE INPUT PARAMETER p-sequence AS INTEGER FORMAT "999999" INITIAL 0 NO-UNDO.
DEFINE INPUT PARAMETER p-description AS CHARACTER FORMAT "X(20)" INITIAL '' NO-UNDO.
DEFINE INPUT PARAMETER p-due-date AS DATE INITIAL '' NO-UNDO.

IF NOT v-deskbank-initialized THEN RETURN 'Generation did not initialize'.

IF p-due-date = ? OR p-due-date < TODAY THEN p-due-date = TODAY.

PUT STREAM s-bi-output UNFORMATTED
    'A'
    STRING( p-sequence, "999999" )
    STRING( v-deskbank-payer-bank, "99" )
    STRING( v-deskbank-payer-branch, "9999" )
    STRING( v-deskbank-payer-payername, "X(20)" )
    STRING( v-deskbank-payer-custnum, "999999" )
    STRING( p-description, "X(20)" )
    STRING( DAY(p-due-date), "99" )
    STRING( MONTH(p-due-date), "99" )
    STRING( SUBSTRING( STRING( YEAR(p-due-date) ), 3, 2), "99" )
    STRING( '', "X(105)" )
    CHR(13) CHR(10).

RETURN "OK".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

