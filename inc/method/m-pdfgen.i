&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*------------------------------------------------------------------------
    Library     : Generate PDF's from scratch
    Purpose     :

    Syntax      :

    Description : This library allows PDF's to be created without external
                  libraries or COM objects. Synopsis:
                  
    {inc\m-pdfgen.i}
    PPDF_Open( 'A4', 'P' ).
    PPDF_AddPage().
    PPDF_SetFont( "helvetica", "B", 12 ).
    PPDF_Text( 100, 100, "Hello" ).
    PPDF_Output( 'C:\test.pdf' ).

    Author(s)   : Chris Eade
    Created     : September 2007
    Notes       : Licenced under the GPL v3 or later
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF VAR pdf-internal-state AS INTEGER INIT 0 NO-UNDO.              /* State tracking */
DEF VAR pdf-internal-buffer AS CHARACTER NO-UNDO.                  /* Character buffer */
DEF VAR pdf-internal-pages AS CHARACTER EXTENT 20 NO-UNDO.         /* The PDF pages */
DEF VAR pdf-internal-current-page AS INTEGER INIT 0 NO-UNDO.       /* The current working page index */
DEF VAR pdf-internal-current-font-family AS CHARACTER NO-UNDO.
DEF VAR pdf-internal-current-font-style AS CHARACTER NO-UNDO.
DEF VAR pdf-internal-current-font-key AS CHARACTER NO-UNDO.
DEF VAR pdf-internal-current-font-size AS DECIMAL INIT 12 NO-UNDO.
DEF VAR pdf-internal-current-font-index AS INTEGER NO-UNDO.
DEF VAR pdf-internal-cached-fonts AS CHARACTER NO-UNDO.
DEF VAR pdf-internal-font-count AS INTEGER NO-UNDO.
DEF VAR pdf-internal-font-object AS CHARACTER NO-UNDO.
DEF VAR pdf-internal-compress AS CHARACTER NO-UNDO.
DEF VAR pdf-internal-image-list AS CHARACTER NO-UNDO.
DEF VAR pdf-internal-image-count AS INTEGER NO-UNDO.
DEF VAR pdf-internal-image-object AS CHARACTER NO-UNDO.

DEF VAR pdf-internal-offsets AS INTEGER EXTENT 30 NO-UNDO.
DEF VAR pdf-internal-object-count AS INTEGER INIT 2 NO-UNDO.

DEF VAR pdf-internal-page-width AS DECIMAL NO-UNDO.
DEF VAR pdf-internal-page-height AS DECIMAL NO-UNDO.

/* Defines a "hash table" of key, value pairs accessed using the ENTRY function */
DEF VAR pdf-internal-font-keys AS CHARACTER NO-UNDO.
DEF VAR pdf-internal-font-vals AS CHARACTER NO-UNDO.

pdf-internal-font-keys = "courier,courierB,courierI,courierBI,helvetica,helveticaB,helveticaI,helveticaBI".
pdf-internal-font-keys = pdf-internal-font-keys + ",times,timesB,timesI,timesBI,symbol,zapfdingbats".

pdf-internal-font-vals = "Courier,Courier-Bold,Courier-Oblique,Courier-BoldOblique,Helvetica,Helvetica-Bold".
pdf-internal-font-vals = pdf-internal-font-vals + ",Helvetica-Oblique,Helvetica-BoldOblique,Times-Roman".
pdf-internal-font-vals = pdf-internal-font-vals + ",Times-Bold,Times-Italic,Times-BoldItalic,Symbol,ZapfDingbats".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-pdf_internal_cache_font) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_cache_font Method-Library 
FUNCTION pdf_internal_cache_font RETURNS LOGICAL
(
    font-family AS CHARACTER,
    font-style AS CHARACTER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_cache_image) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_cache_image Method-Library 
FUNCTION pdf_internal_cache_image RETURNS CHARACTER
( image-file AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_close) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_close Method-Library 
FUNCTION pdf_internal_close RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_escape) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_escape Method-Library 
FUNCTION pdf_internal_escape RETURNS CHARACTER
  ( input-string AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_hash_val) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_hash_val Method-Library 
FUNCTION pdf_internal_hash_val RETURNS CHARACTER
  ( font-key AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_new_object) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_new_object Method-Library 
FUNCTION pdf_internal_new_object RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_out) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_out Method-Library 
FUNCTION pdf_internal_out RETURNS LOGICAL
  ( input-string AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_put_fonts) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_put_fonts Method-Library 
FUNCTION pdf_internal_put_fonts RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_put_images) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_put_images Method-Library 
FUNCTION pdf_internal_put_images RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_put_pages) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_put_pages Method-Library 
FUNCTION pdf_internal_put_pages RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_put_resources) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_put_resources Method-Library 
FUNCTION pdf_internal_put_resources RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_put_stream) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf_internal_put_stream Method-Library 
FUNCTION pdf_internal_put_stream RETURNS LOGICAL
  ( input-string AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_AddImage) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD PPDF_AddImage Method-Library 
FUNCTION PPDF_AddImage RETURNS LOGICAL
(
    image-file AS CHARACTER,
    image-translation AS CHARACTER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_AddPage) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD PPDF_AddPage Method-Library 
FUNCTION PPDF_AddPage RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_Open) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD PPDF_Open Method-Library 
FUNCTION PPDF_Open RETURNS LOGICAL
(
    size-of-page AS CHARACTER,
    page-orientation AS CHARACTER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_Output) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD PPDF_Output Method-Library 
FUNCTION PPDF_Output RETURNS LOGICAL
  ( filename-string AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_SetFont) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD PPDF_SetFont Method-Library 
FUNCTION PPDF_SetFont RETURNS LOGICAL
(
    font-family AS CHARACTER,
    font-style AS CHARACTER,
    font-size AS INTEGER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_SetFontSize) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD PPDF_SetFontSize Method-Library 
FUNCTION PPDF_SetFontSize RETURNS LOGICAL
  ( font-size AS INTEGER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_Text) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD PPDF_Text Method-Library 
FUNCTION PPDF_Text RETURNS LOGICAL
(
    x-loc AS DECIMAL,
    y-loc AS DECIMAL,
    input-string AS CHARACTER
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = 28.32
         WIDTH              = 76.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-pdf_internal_cache_font) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_cache_font Method-Library 
FUNCTION pdf_internal_cache_font RETURNS LOGICAL
(
    font-family AS CHARACTER,
    font-style AS CHARACTER
) :
/*------------------------------------------------------------------------------
  Purpose: Store a list of fonts that have been used already, so repeated calls
           don't result in a repitition of font resource declarations
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR font-index AS INTEGER NO-UNDO.
DEF VAR font-key AS CHARACTER NO-UNDO.
DEF VAR lc-font-family AS CHARACTER NO-UNDO.

lc-font-family = LC( font-family ).

IF lc-font-family = 'arial' THEN
    font-family = 'Helvetica'.
ELSE IF lc-font-family = 'symbol' OR lc-font-family = 'zapfdingbats' THEN
    font-style = ''.

/* Switch the order of the style if required */
font-style = CAPS( font-style ).
IF font-style = 'IB' THEN
    font-style = 'BI'.

/* Set the real font style as understood by the PDF spec */
CASE font-style:
    WHEN "B" THEN
        font-style = "Bold".
    WHEN "I" THEN
        font-style = "Italic".
    WHEN "BI" THEN
        font-style = "BoldItalic".
END CASE.

/* Generate the font name */
font-key = font-family + '-' + font-style.
    
pdf-internal-current-font-index = LOOKUP( font-key, pdf-internal-cached-fonts, ',' ).

IF pdf-internal-current-font-index = 0 THEN DO:
    pdf-internal-font-count = pdf-internal-font-count + 1.
    IF pdf-internal-cached-fonts <> "" THEN
        pdf-internal-cached-fonts = pdf-internal-cached-fonts + ','.
    pdf-internal-cached-fonts = pdf-internal-cached-fonts + font-key.

    pdf-internal-current-font-index = pdf-internal-font-count.
END.

/* The font was already cached */
RETURN FALSE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_cache_image) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_cache_image Method-Library 
FUNCTION pdf_internal_cache_image RETURNS CHARACTER
( image-file AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose: Registes the image for later inclusion in resources
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR image-index AS INTEGER NO-UNDO.
DEF VAR image-name AS CHARACTER NO-UNDO.

/* The image name in the PDF is an encoded version of the file name */
image-name = ENCODE( image-file ).
image-index = LOOKUP( image-name, pdf-internal-image-list, ',' ).

IF image-index = 0 THEN DO:
    pdf-internal-image-count = pdf-internal-image-count + 1.
    IF pdf-internal-image-list <> "" THEN
        pdf-internal-image-list = pdf-internal-image-list + ','.
    pdf-internal-image-list = pdf-internal-image-list + image-file + '=' + image-name.
END.

RETURN image-name.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_close) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_close Method-Library 
FUNCTION pdf_internal_close RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose: Assemble the PDF file and finish
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR v-count AS INTEGER NO-UNDO.
DEF VAR cross-ref-length AS INTEGER NO-UNDO.

IF NOT pdf-internal-current-page > 0 THEN
    PPDF_AddPage().

pdf-internal-state = 1.

/* Pages and resources. */
pdf_internal_put_pages().
pdf_internal_put_resources().

/* Print some document info. */
pdf_internal_new_object().
pdf_internal_out( '<<' ).
pdf_internal_out( '/Producer (Catalyst PROGRESS PDF creator)' ).
pdf_internal_out( '/CreationDate (D:' + STRING( TODAY ) + ')' ). /* date('YmdHis') */
pdf_internal_out( '>>' ).
pdf_internal_out( 'endobj' ).

/* Print catalog. */
pdf_internal_new_object().
pdf_internal_out( '<<' ).
pdf_internal_out( '/Type /Catalog' ).
pdf_internal_out( '/Pages 1 0 R' ).
pdf_internal_out( '/OpenAction [3 0 R /FitH null]' ).
pdf_internal_out( '/PageLayout /OneColumn' ).
pdf_internal_out( '>>' ).
pdf_internal_out( 'endobj' ).

/* Print cross reference. */
cross-ref-length = LENGTH( pdf-internal-buffer ).
/* $start_xref = strlen( $this->_buffer ). */
pdf_internal_out( 'xref' ).
pdf_internal_out( '0 ' + STRING( pdf-internal-object-count + 1 ) ).
pdf_internal_out( '0000000000 65535 f ' ).

DO v-count = 1 TO pdf-internal-object-count:
    pdf_internal_out( STRING( pdf-internal-offsets[v-count], "9999999999" ) + ' 00000 n ' ).
END.

/* for ($i = 1; $i <= $this->_n; $i++) {                             */
/*     $this->_out(sprintf('%010d 00000 n ', $this->_offsets[$i]));  */
/* }                                                                 */

/* Print trailer. */
pdf_internal_out( 'trailer' ).
pdf_internal_out( '<<' ).

/* The total number of objects. */
pdf_internal_out( '/Size ' + STRING( pdf-internal-object-count + 1 ) ).

/* The root object. */
pdf_internal_out( '/Root ' + STRING( pdf-internal-object-count ) + ' 0 R' ).

/* The document information object. */
pdf_internal_out( '/Info ' + STRING( pdf-internal-object-count - 1 ) + ' 0 R' ).
pdf_internal_out( '>>' ).
pdf_internal_out( 'startxref' ).
pdf_internal_out( STRING( cross-ref-length ) ).  /* Where to find the xref */
pdf_internal_out( '%%EOF' ).
pdf-internal-state = 3.

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_escape) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_escape Method-Library 
FUNCTION pdf_internal_escape RETURNS CHARACTER
  ( input-string AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose: Escape particular chars
    Notes:  
------------------------------------------------------------------------------*/
    
input-string = REPLACE( input-string, '\\', '\\\\' ).
input-string = REPLACE( input-string, '(', '\\(' ).
input-string = REPLACE( input-string, ')', '\\)' ).

RETURN input-string.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_hash_val) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_hash_val Method-Library 
FUNCTION pdf_internal_hash_val RETURNS CHARACTER
  ( font-key AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose: Return the value of pdf-internal-font-vals using a key
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR text-index AS INTEGER NO-UNDO.

text-index = LOOKUP( font-key, pdf-internal-font-keys, ',' ).

IF text-index = 0 THEN
    RETURN "".
ELSE
    RETURN ENTRY( text-index, pdf-internal-font-vals, ',' ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_new_object) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_new_object Method-Library 
FUNCTION pdf_internal_new_object RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose: Create a new object block
    Notes:  
------------------------------------------------------------------------------*/

pdf-internal-object-count = pdf-internal-object-count + 1.
pdf-internal-offsets[pdf-internal-object-count] = LENGTH( pdf-internal-buffer ).

pdf_internal_out( STRING( pdf-internal-object-count ) + ' 0 obj' ).

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_out) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_out Method-Library 
FUNCTION pdf_internal_out RETURNS LOGICAL
  ( input-string AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose: Append content to a page, or a buffer
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR input-memptr AS MEMPTR.
DEF VAR input-length AS INTEGER NO-UNDO.

/* Write the input string into a memptr */
input-length = LENGTH( input-string, "RAW" ) + 1000.
SET-SIZE( input-memptr ) = input-length.
PUT-STRING( input-memptr, 1 ) = input-string.

IF pdf-internal-state = 2 THEN
    pdf-internal-pages[pdf-internal-current-page] =
        pdf-internal-pages[pdf-internal-current-page] + input-string + CHR(10).
ELSE
    pdf-internal-buffer = pdf-internal-buffer + input-string + CHR(10).

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_put_fonts) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_put_fonts Method-Library 
FUNCTION pdf_internal_put_fonts RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose: Output the font definitions
    Notes: Fonts are stored in the cache line this:
            "helvetica=bold,helvetica=italic,times=,times=bold,times=..."
           So the 1st ENTRY (split on "=") of the <n>th entry (split on ",") will
           give the font name. 
------------------------------------------------------------------------------*/
DEF VAR v-count AS INTEGER NO-UNDO.
DEF VAR v-num-fonts AS INTEGER NO-UNDO.
DEF VAR font-name AS CHARACTER NO-UNDO.
DEF VAR font-style AS CHARACTER NO-UNDO.

/* Print out font details. */
v-num-fonts = NUM-ENTRIES( pdf-internal-cached-fonts, ',' ).

DO v-count = 1 TO v-num-fonts:
    font-name = ENTRY( v-count, pdf-internal-cached-fonts, ',' ).

    /* Create a new object for this font */
    pdf_internal_new_object().

    /* Save the object reference for later */
    IF pdf-internal-font-object <> "" THEN DO:
        pdf-internal-font-object = pdf-internal-font-object + ','.
    END.
    pdf-internal-font-object = pdf-internal-font-object + STRING( pdf-internal-object-count ).

    /* Output the font object definition */
    pdf_internal_out( '<</Type /Font' ).
    pdf_internal_out( '/BaseFont /' + font-name ).
    pdf_internal_out( '/Subtype /Type1' ).
    IF font-name <> "Symbol" AND font-name <> "ZapfDingbats" THEN
        pdf_internal_out( '/Encoding /WinAnsiEncoding' ).
    pdf_internal_out( '>>' ).
    pdf_internal_out( 'endobj' ).
END.

/* foreach ($this->_fonts as $k => $font) {                     */
/*         $this->_newobj();                                    */
/*         $this->_fonts[$k]['n'] = $this->_n;                  */
/*         $name = $font['name'];                               */
/*         $this->_out('<</Type /Font');                        */
/*         $this->_out('/BaseFont /' . $name);                  */
/*         $this->_out('/Subtype /Type1');                      */
/*         if ($name != 'Symbol' && $name != 'ZapfDingbats') {  */
/*             $this->_out('/Encoding /WinAnsiEncoding');       */
/*         }                                                    */
/*         $this->_out('>>');                                   */
/*         $this->_out('endobj');                               */
/*     }                                                        */
/* }                                                            */

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_put_images) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_put_images Method-Library 
FUNCTION pdf_internal_put_images RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR v-count AS INTEGER NO-UNDO.
DEF VAR v-num-images AS INTEGER NO-UNDO.
DEF VAR image-name AS CHARACTER NO-UNDO.
DEF VAR image-file AS CHARACTER NO-UNDO.
DEF VAR image-length AS INTEGER NO-UNDO.

/* Print out font details. */
v-num-images = NUM-ENTRIES( pdf-internal-image-list, ',' ).

DO v-count = 1 TO v-num-images:
    image-name = ENTRY( 2, ENTRY( v-count, pdf-internal-image-list, ',' ), ',' ).

    /* Create a new object for this image */
    pdf_internal_new_object().

    /* Save the object reference for later */
    IF pdf-internal-image-object <> "" THEN DO:
        pdf-internal-image-object = pdf-internal-image-object + ','.
    END.
    pdf-internal-image-object = pdf-internal-image-object + STRING( pdf-internal-object-count ).

    /* Output the image object definition */
    pdf_internal_out( '<<' ).
    pdf_internal_out( '/Type /XObject' ).
    pdf_internal_out( '/SubType /Image' ).
    pdf_internal_out( '/Height ' + '' ).
    pdf_internal_out( '/Width ' + '' ).
    pdf_internal_out( '/ColorSpace /DeviceRGB' ).
    pdf_internal_out( '/Filter [ /DCTDecode ]' ).
    pdf_internal_out( '/Length ' + STRING( image-length ) ).
    pdf_internal_out( '/Name ' + image-name ).
    pdf_internal_out( '>>' ).
/*     pdf_internal_put_raw_stream( image-memptr ).  */
    pdf_internal_out( 'endobj' ).
END.

RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_put_pages) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_put_pages Method-Library 
FUNCTION pdf_internal_put_pages RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose: Output the page blocks
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR v-count AS INTEGER NO-UNDO.
DEF VAR text-filter AS CHARACTER NO-UNDO.
DEF VAR kids-string AS CHARACTER NO-UNDO.
DEF VAR kids-index AS INTEGER NO-UNDO.

/* Set encoding as flat text unless the compress type is set */
IF pdf-internal-compress <> '' THEN
    text-filter = pdf-internal-compress.
ELSE
    text-filter = '/Filter /FlateDecode '.

text-filter = ''.

/* Go through all the pages and add them as new page objects */
DO v-count = 1 TO pdf-internal-current-page:
    pdf_internal_new_object().
    pdf_internal_out( '<</Type /Page' ).
    pdf_internal_out( '/Parent 1 0 R' ).
    pdf_internal_out( '/Resources 2 0 R' ).
    pdf_internal_out( '/Contents ' + STRING( pdf-internal-object-count + 1 ) + ' 0 R>>' ).
    pdf_internal_out( 'endobj' ).

    /* If compression required gzcompress() the page content. */
    /* $p = ($this->_compress) ? gzcompress($this->_pages[$n]) : $this->_pages[$n];  */

    /* Tell the length of the page stream */
    pdf_internal_new_object().
    pdf_internal_out( '<<' + text-filter + '/Length ' + STRING(
        LENGTH( pdf-internal-pages[v-count] ) ) + '>>'
    ).

    /* Output the page object as a stream */
    pdf_internal_put_stream( pdf-internal-pages[v-count] ).
    pdf_internal_out( 'endobj' ).
END.

/* Save the byte start of the page parent object */
pdf-internal-offsets[1] = LENGTH( pdf-internal-buffer ).

/* Now output the page parent object */
pdf_internal_out( '1 0 obj' ).
pdf_internal_out( '<</Type /Pages' ).

kids-string = '/Kids ['.
DO v-count = 1 TO pdf-internal-current-page:
    kids-index = v-count - 1.
    kids-string = kids-string + STRING( 3 + 2 * kids-index ) + ' 0 R '.
END.
kids-string = kids-string + ']'.
pdf_internal_out( kids-string ).

/* The number of child pages this page parent object has */
pdf_internal_out( '/Count ' + STRING( pdf-internal-current-page ) ).

/* The mediabox for these child pages */
pdf_internal_out( '/MediaBox [0 0 ' +
    STRING( ROUND( pdf-internal-page-width, 2 ) ) + ' ' +
    STRING( ROUND( pdf-internal-page-height, 2 ) ) + ']'
).
pdf_internal_out( '>>' ).
pdf_internal_out( 'endobj' ).

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_put_resources) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_put_resources Method-Library 
FUNCTION pdf_internal_put_resources RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose: Output the resources
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR v-count AS INTEGER NO-UNDO.
DEF VAR image-name AS CHARACTER NO-UNDO.

/* Put the font resource objects */
pdf_internal_put_fonts().

pdf-internal-offsets[2] = LENGTH( pdf-internal-buffer ).
pdf_internal_out( '2 0 obj' ).
pdf_internal_out( '<<' ).
pdf_internal_out( '/ProcSet [/PDF /Text]' ).
pdf_internal_out( '/Font <<' ).
DO v-count = 1 TO pdf-internal-font-count:
    pdf_internal_out(
        '/F' + STRING( v-count ) + ' ' +
        ENTRY( v-count, pdf-internal-font-object, ',' ) + ' 0 R'
    ).
END.
pdf_internal_out( '>>' ).
pdf_internal_out( '/XObject <<' ).
DO v-count = 1 TO pdf-internal-image-count:
    image-name = ENTRY( 2, ENTRY( v-count, pdf-internal-image-list, ',' ), '=' ).
    pdf_internal_out(
        '/' + image-name + ' ' +
        ENTRY( v-count, pdf-internal-image-object, ',' ) + ' 0 R'
    ).
END.
pdf_internal_out( '>>' ).

pdf_internal_out( '>>' ).
pdf_internal_out( 'endobj' ).

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pdf_internal_put_stream) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf_internal_put_stream Method-Library 
FUNCTION pdf_internal_put_stream RETURNS LOGICAL
  ( input-string AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
pdf_internal_out( 'stream' ).
pdf_internal_out( input-string ).
pdf_internal_out( 'endstream' ).

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_AddImage) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION PPDF_AddImage Method-Library 
FUNCTION PPDF_AddImage RETURNS LOGICAL
(
    image-file AS CHARACTER,
    image-translation AS CHARACTER
) :
/*------------------------------------------------------------------------------
  Purpose: Add an image to the page 
    Notes: Parameters:

      image-file         => The filename of the image
      image-translation  => A specially coded string defining the translation
                            do be done on the image:
                            
               <xpos>, <ypos>, <width>, <height>, <rotation>
            
               Note: <xpos> and <ypos> are manditory
------------------------------------------------------------------------------*/
DEF VAR xpos AS DECIMAL NO-UNDO.
DEF VAR ypos AS DECIMAL NO-UNDO.
DEF VAR image-name AS CHARACTER NO-UNDO.

xpos = DECIMAL( ENTRY( 1, image-translation, ',' ) ).
ypos = DECIMAL( ENTRY( 2, image-translation, ',' ) ).

image-name = pdf_internal_cache_image( image-file ).

IF pdf-internal-current-page > 0 THEN DO:
    pdf_internal_out( 'q' ).
    pdf_internal_out( '50 0 0 50 ' + STRING( xpos ) + ' ' + STRING( ypos ) + ' cm' ).
    pdf_internal_out( '/' + image-name + ' Do' ).
    pdf_internal_out( 'Q' ).
END.

RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_AddPage) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION PPDF_AddPage Method-Library 
FUNCTION PPDF_AddPage RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose: Add a new page to the document
    Notes:  
------------------------------------------------------------------------------*/
    
/* Increment the page count */
pdf-internal-current-page = pdf-internal-current-page + 1.

/* Clear the page buffer */
pdf-internal-pages[pdf-internal-current-page] = ''.

/* Set the state to page */
pdf-internal-state = 2.

/* if ($this->_font_family) {                                                       */
/*     $this->setFont($this->_font_family, $this->_font_style, $this->_font_size);  */
/* }                                                                                */

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_Open) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION PPDF_Open Method-Library 
FUNCTION PPDF_Open RETURNS LOGICAL
(
    size-of-page AS CHARACTER,
    page-orientation AS CHARACTER
) :
/*------------------------------------------------------------------------------
  Purpose: PDF Header
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR temp-width AS DECIMAL NO-UNDO.

pdf-internal-state = 1.
pdf_internal_out( '%PDF-1.3' ).

size-of-page = LC( size-of-page ).
page-orientation = LC( page-orientation ).

CASE size-of-page:
    WHEN "a3" THEN DO:
        pdf-internal-page-width = 841.89.
        pdf-internal-page-height = 1190.55.
    END.
    WHEN "a4" THEN DO:
        pdf-internal-page-width = 595.28.
        pdf-internal-page-height = 841.89.
    END.
    WHEN "a5" THEN DO:
        pdf-internal-page-width = 420.94.
        pdf-internal-page-height = 595.28.
    END.
    WHEN "letter" THEN DO:
        pdf-internal-page-width = 612.
        pdf-internal-page-height = 792.
    END.
    WHEN "legal" THEN DO:
        pdf-internal-page-width = 612.
        pdf-internal-page-height = 1008.
    END.
END CASE.

/* Flip the measurements if its landscape */
IF page-orientation = "l" OR page-orientation = "landscape" THEN DO:
    temp-width = pdf-internal-page-width.
    pdf-internal-page-width = pdf-internal-page-height.
    pdf-internal-page-height = temp-width.
END.

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_Output) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION PPDF_Output Method-Library 
FUNCTION PPDF_Output RETURNS LOGICAL
  ( filename-string AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose: Write the PDF document to a file
    Notes:  
------------------------------------------------------------------------------*/

IF pdf-internal-state < 3 THEN
    pdf_internal_close().

OUTPUT TO VALUE( filename-string ) KEEP-MESSAGES PAGE-SIZE 0.
PUT CONTROL pdf-internal-buffer.
OUTPUT CLOSE.

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_SetFont) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION PPDF_SetFont Method-Library 
FUNCTION PPDF_SetFont RETURNS LOGICAL
(
    font-family AS CHARACTER,
    font-style AS CHARACTER,
    font-size AS INTEGER
) :
/*------------------------------------------------------------------------------
  Purpose: Set the current font, and output font definition if we are on a page
    Notes:  
------------------------------------------------------------------------------*/
IF font-size = ? THEN
    font-size = pdf-internal-current-font-size.

IF font-family = pdf-internal-current-font-family AND
    font-style  = pdf-internal-current-font-style AND
    font-size   = pdf-internal-current-font-size THEN RETURN TRUE.

/* This will update pdf-internal-current-font-index regardless */
pdf_internal_cache_font( font-family, font-style ).

/* Store current font information. */
pdf-internal-current-font-family = font-family.
pdf-internal-current-font-style  = font-style.
pdf-internal-current-font-size   = font-size.

/* Output font information if at least one page has been defined. */
IF pdf-internal-current-page > 0 THEN
    pdf_internal_out(
        'BT /F' + STRING( pdf-internal-current-font-index ) + ' ' +
        STRING( ROUND( pdf-internal-current-font-size, 2) ) +
        ' Tf ET'
    ).

/* sprintf('BT /F%d %.2f Tf ET', $this->_current_font['i'], $this->_font_size)); */

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_SetFontSize) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION PPDF_SetFontSize Method-Library 
FUNCTION PPDF_SetFontSize RETURNS LOGICAL
  ( font-size AS INTEGER ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

IF font-size = pdf-internal-current-font-size THEN RETURN TRUE.

pdf-internal-current-font-size = font-size.
    
IF pdf-internal-current-page > 0 THEN
    pdf_internal_out(
        'BT /F' + STRING( pdf-internal-current-font-index ) + ' ' +
        STRING( ROUND( pdf-internal-current-font-size, 2 ) ) +
        ' Tf ET'
    ).
  
RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PPDF_Text) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION PPDF_Text Method-Library 
FUNCTION PPDF_Text RETURNS LOGICAL
(
    x-loc AS DECIMAL,
    y-loc AS DECIMAL,
    input-string AS CHARACTER
) :
/*------------------------------------------------------------------------------
  Purpose: Put some text at a location
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR y-from-top AS DECIMAL.

input-string = pdf_internal_escape( input-string ).

y-from-top = pdf-internal-page-height - y-loc.

pdf_internal_out(
    'BT ' + STRING( ROUND( x-loc, 2 ) ) + ' ' + STRING( ROUND( y-from-top, 2 ) ) +
    ' Td (' + input-string + ') Tj ET'
).

RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

