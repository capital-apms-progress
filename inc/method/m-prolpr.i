&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : m-prolpr.i
    Purpose     : Methods for producing reports using PROLPR.DLL interface
    Author(s)   : Andrew McMillan
  ------------------------------------------------------------------------*/

DEF VAR txtrep-print-file AS CHAR NO-UNDO.
DEF VAR txtrep-control-file AS CHAR NO-UNDO.
DEF VAR txtrep-prefix AS CHAR INITIAL "REP" NO-UNDO.
DEF VAR txtrep-preview-window AS HANDLE NO-UNDO.
DEF VAR txtrep-printer AS CHAR NO-UNDO.
DEF VAR lpr-printer-name AS CHAR NO-UNDO.

DEF VAR lpr-delimiter    AS CHAR NO-UNDO INITIAL "|".
DEF VAR lpr-font-text    AS CHAR NO-UNDO.
DEF VAR lpr-font-code    AS CHAR NO-UNDO.
DEF VAR lpr-font-height  AS CHAR NO-UNDO.
DEF VAR lpr-font-cols    AS CHAR NO-UNDO.
DEF VAR lpr-current-font AS INT NO-UNDO INITIAL ?.
DEF VAR lpr-current-font-height AS DEC NO-UNDO.

DEF VAR lpr-page-number   AS INT NO-UNDO INITIAL 0.
DEF VAR lpr-page-position AS DEC NO-UNDO INITIAL 0.
DEF VAR lpr-page-height   AS DEC NO-UNDO.
DEF VAR lpr-preview-mode  AS LOGICAL NO-UNDO.
DEF VAR lpr-between-pages AS LOGICAL NO-UNDO INITIAL No.

DEF VAR lpr-footer-height AS DEC NO-UNDO.
DEF VAR lp_result AS INT NO-UNDO.

&GLOB PROLPR "D:\DEV\C-CODE\PROLPR\DEBUG\PROLPR.DLL"

PROCEDURE lp_open EXTERNAL {&PROLPR} CDECL:
  DEF INPUT  PARAMETER printer-name AS CHAR NO-UNDO.
  DEF INPUT  PARAMETER output-name  AS CHAR NO-UNDO.
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_close EXTERNAL {&PROLPR} CDECL:
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_put_text EXTERNAL {&PROLPR} CDECL:
  DEF INPUT  PARAMETER text-string AS CHAR NO-UNDO.
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_set_font EXTERNAL {&PROLPR} CDECL:
  DEF INPUT  PARAMETER font-name AS CHAR NO-UNDO.
  DEF INPUT  PARAMETER point-size AS DOUBLE NO-UNDO.
  DEF INPUT  PARAMETER fixed-font AS LONG NO-UNDO.
  DEF INPUT  PARAMETER boldness AS LONG NO-UNDO.
  DEF INPUT  PARAMETER italic AS LONG NO-UNDO.
  DEF INPUT  PARAMETER u-line AS LONG NO-UNDO.
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_moveto EXTERNAL {&PROLPR} CDECL:
  DEF INPUT  PARAMETER x AS DOUBLE NO-UNDO.
  DEF INPUT  PARAMETER y AS DOUBLE NO-UNDO.
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_moverel EXTERNAL {&PROLPR} CDECL:
  DEF INPUT  PARAMETER x AS DOUBLE NO-UNDO.
  DEF INPUT  PARAMETER y AS DOUBLE NO-UNDO.
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_pushto EXTERNAL {&PROLPR} CDECL:
  DEF INPUT  PARAMETER x AS DOUBLE NO-UNDO.
  DEF INPUT  PARAMETER y AS DOUBLE NO-UNDO.
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_pushrel EXTERNAL {&PROLPR} CDECL:
  DEF INPUT  PARAMETER x AS DOUBLE NO-UNDO.
  DEF INPUT  PARAMETER y AS DOUBLE NO-UNDO.
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_lineto EXTERNAL {&PROLPR} CDECL:
  DEF INPUT  PARAMETER x AS DOUBLE NO-UNDO.
  DEF INPUT  PARAMETER y AS DOUBLE NO-UNDO.
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_pushline EXTERNAL {&PROLPR} CDECL:
  DEF INPUT  PARAMETER x AS DOUBLE NO-UNDO.
  DEF INPUT  PARAMETER y AS DOUBLE NO-UNDO.
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_linerel EXTERNAL {&PROLPR} CDECL:
  DEF INPUT  PARAMETER x AS DOUBLE NO-UNDO.
  DEF INPUT  PARAMETER y AS DOUBLE NO-UNDO.
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_pushlinerel EXTERNAL {&PROLPR} CDECL:
  DEF INPUT  PARAMETER x AS DOUBLE NO-UNDO.
  DEF INPUT  PARAMETER y AS DOUBLE NO-UNDO.
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_popto EXTERNAL {&PROLPR} CDECL:
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_popline EXTERNAL {&PROLPR} CDECL:
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_get_xpos EXTERNAL {&PROLPR} CDECL:
  DEF RETURN PARAMETER xpos AS DOUBLE NO-UNDO.
END PROCEDURE.

PROCEDURE lp_get_ypos EXTERNAL {&PROLPR} CDECL:
  DEF RETURN PARAMETER ypos AS DOUBLE NO-UNDO.
END PROCEDURE.

PROCEDURE lp_end_page EXTERNAL {&PROLPR} CDECL:
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

PROCEDURE lp_start_page EXTERNAL {&PROLPR} CDECL:
  DEF RETURN PARAMETER result AS LONG NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-lpr-down-by) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD lpr-down-by Method-Library 
FUNCTION lpr-down-by RETURNS DECIMAL
  ( INPUT rows AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-down-to) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD lpr-down-to Method-Library 
FUNCTION lpr-down-to RETURNS DECIMAL
  ( INPUT new-pos AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-finish) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD lpr-finish Method-Library 
FUNCTION lpr-finish RETURNS CHARACTER
  ( /* no parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-get-xpos) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD lpr-get-xpos Method-Library 
FUNCTION lpr-get-xpos RETURNS DECIMAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-get-ypos) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD lpr-get-ypos Method-Library 
FUNCTION lpr-get-ypos RETURNS DECIMAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD lpr-line Method-Library 
FUNCTION lpr-line RETURNS DECIMAL
  ( INPUT font-text AS CHAR, INPUT line-text AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-moveto) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD lpr-moveto Method-Library 
FUNCTION lpr-moveto RETURNS DECIMAL
  ( INPUT x AS DEC, INPUT y AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-page-break) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD lpr-page-break Method-Library 
FUNCTION lpr-page-break RETURNS LOGICAL
  ( /* no parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-select-font) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD lpr-select-font Method-Library 
FUNCTION lpr-select-font RETURNS DECIMAL
  ( INPUT font-text AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-start) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD lpr-start Method-Library 
FUNCTION lpr-start RETURNS DECIMAL
  ( INPUT preview AS LOGICAL, INPUT page-reset AS CHAR, INPUT base-font AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-test-bottom) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD lpr-test-bottom Method-Library 
FUNCTION lpr-test-bottom RETURNS LOGICAL
  ( INPUT lines AS DECIMAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .08
         WIDTH              = 39.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

{inc/method/m-bqmgr.i}
{src/adm/method/attribut.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

RUN lpr-initialise.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-get-current-printer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-current-printer Method-Library 
PROCEDURE get-current-printer :
/*------------------------------------------------------------------------------
  Purpose:     Set the value of txtrep-printer to reflect the current printer.
------------------------------------------------------------------------------*/
DEF BUFFER CurrPrinter FOR RP.

  DEF VAR user-name AS CHAR NO-UNDO.
  {inc/username.i "user-name"}

  FIND FIRST CurrPrinter WHERE
    CurrPrinter.UserName = user-name AND
    CurrPrinter.ReportID = "Current Printer" NO-LOCK NO-ERROR.
    
  IF NOT AVAILABLE CurrPrinter THEN
  DO:
    DEF VAR success AS LOGI NO-UNDO.

    lpr-printer-name = SESSION:PRINTER-NAME.
    txtrep-printer = SESSION:PRINTER-PORT.

    CREATE CurrPrinter.
    ASSIGN
      CurrPrinter.UserName = user-name
      CurrPrinter.ReportID = "Current Printer"
      CurrPrinter.Char1    = lpr-printer-name
      CurrPrinter.Char2    = txtrep-printer.

    FIND CURRENT CurrPrinter NO-LOCK.
  END.

  /* Set the value of the port (more useful than printer name for copying to it) */
  txtrep-printer = CurrPrinter.Char2.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-initialise) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE lpr-initialise Method-Library 
PROCEDURE lpr-initialise :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the important parameters
------------------------------------------------------------------------------*/
DEF VAR temp-dir AS CHAR NO-UNDO.

  temp-dir = OS-GETENV("TEMP":U).
  IF temp-dir = ? OR temp-dir = "" THEN temp-dir = SESSION:TEMP-DIRECTORY .
  temp-dir = temp-dir + "\".

  txtrep-print-file = temp-dir + txtrep-prefix + STRING( TIME, "99999") + ".TMP".
  txtrep-control-file = temp-dir + "CTL" + STRING( TIME, "99999") + ".TMP".

  DO WHILE INDEX( txtrep-print-file, "/" ) > 0:
    SUBSTRING( txtrep-print-file, INDEX( txtrep-print-file, "/" ), 1) = "\".
  END.
  DO WHILE INDEX( txtrep-control-file, "/" ) > 0:
    SUBSTRING( txtrep-control-file, INDEX( txtrep-control-file, "/" ), 1) = "\".
  END.

  /* Set the 'txtrep-printer' to printer port */
  RUN get-current-printer.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-viewer-title) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-viewer-title Method-Library 
PROCEDURE set-viewer-title :
/*------------------------------------------------------------------------------
  Purpose:  Set the title in the report viewer window.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER title-text AS CHAR NO-UNDO.

  RUN set-title IN txtrep-preview-window ( title-text ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-view-output-file) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE view-output-file Method-Library 
PROCEDURE view-output-file :
/*------------------------------------------------------------------------------
  Purpose:  Close the output file now that we've finished with it.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER txtrep-is-preview AS LOGI NO-UNDO.

  IF txtrep-is-preview THEN DO:
    RUN win/w-report.w PERSISTENT SET txtrep-preview-window.
    RUN dispatch IN txtrep-preview-window ( 'initialize':U ).
    RUN load-editor IN txtrep-preview-window ( txtrep-print-file, txtrep-control-file ).
  END.
  ELSE DO:
    /* should all have been done already */
    RUN lp_close( OUTPUT lp_result ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-lpr-down-by) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION lpr-down-by Method-Library 
FUNCTION lpr-down-by RETURNS DECIMAL
  ( INPUT rows AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Move the cursor down by a specific amount
------------------------------------------------------------------------------*/

  IF lpr-preview-mode THEN DO:
    rows = ROUND( rows, 0).
    IF NOT(lpr-between-pages) AND lpr-test-bottom( rows ) THEN
      lpr-page-break().        /* footer half */
    ELSE IF rows >= 1 THEN DO:
      IF NOT(lpr-between-pages) THEN DO:
        IF lpr-page-position <= 0 THEN lpr-page-break().   /* header half */
      END.
      lpr-line( "", FILL("~n", INT(rows)) ).
    END.
  END.
  ELSE IF NOT(lpr-between-pages) AND lpr-test-bottom( rows ) THEN
    lpr-page-break().          /* footer half */
  ELSE DO:
    IF NOT(lpr-between-pages) THEN DO:
      IF lpr-page-position <= 0 THEN lpr-page-break().   /* header half */
    END.
    lpr-page-position = lpr-page-position + ( rows * lpr-current-font-height ).
    RUN lp_moveto( lpr-get-xpos(), lpr-page-position, OUTPUT lp_result ).
  END.

  RETURN lpr-page-position.  

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-down-to) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION lpr-down-to Method-Library 
FUNCTION lpr-down-to RETURNS DECIMAL
  ( INPUT new-pos AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Skip to a position on the page
------------------------------------------------------------------------------*/

  RUN lp_moveto( lpr-get-xpos(), new-pos, OUTPUT lp_result ).
  RETURN new-pos.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-finish) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION lpr-finish Method-Library 
FUNCTION lpr-finish RETURNS CHARACTER
  ( /* no parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Finish running a pclrep report
------------------------------------------------------------------------------*/

 lpr-page-break().
 RUN view-output-file ( lpr-preview-mode ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-get-xpos) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION lpr-get-xpos Method-Library 
FUNCTION lpr-get-xpos RETURNS DECIMAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR result AS DEC NO-UNDO.

  RUN lp_get_xpos( OUTPUT result ).
  RETURN result.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-get-ypos) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION lpr-get-ypos Method-Library 
FUNCTION lpr-get-ypos RETURNS DECIMAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR result AS DEC NO-UNDO.

  RUN lp_get_ypos( OUTPUT result ).
  RETURN result.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION lpr-line Method-Library 
FUNCTION lpr-line RETURNS DECIMAL
  ( INPUT font-text AS CHAR, INPUT line-text AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Print a line in a selected font
------------------------------------------------------------------------------*/

  IF font-text = ? THEN font-text = "".
  IF line-text = ? THEN line-text = "".
  
  DEF VAR position-increment AS DEC NO-UNDO.
  DEF VAR i AS INT NO-UNDO.

/*  MESSAGE "lpr-line" lpr-between-pages lpr-page-position . */
  /* initialisation, or first half of page break, sets page-position to 0 */
  IF NOT(lpr-between-pages) THEN DO:
    IF lpr-page-position <= 0 THEN lpr-page-break().   /* header half */
  END.

  lpr-select-font( font-text ).
  position-increment = (IF line-text = "" THEN 1 ELSE NUM-ENTRIES(line-text, "~n")).
    
  IF NOT(lpr-between-pages) THEN DO:
    /* If we're not in a page break we might be over the end of the page */
    IF lpr-test-bottom(position-increment) THEN DO:
      lpr-page-break().        /* footer half */
      lpr-page-break().        /* header half */
      lpr-select-font( font-text ).
    END.
  END.

  IF lpr-preview-mode THEN DO:
    IF TRIM(line-text) = "" THEN PUT SKIP(1).
    ELSE PUT UNFORMATTED line-text SKIP.
  END.
  ELSE DO:
    RUN lp_put_text( line-text, OUTPUT lp_result ).
    RUN lp_moverel( 0.0, lpr-current-font-height, OUTPUT lp_result ).
  END.
  lpr-page-position = lpr-page-position + lpr-current-font-height * position-increment.

  RETURN lpr-page-position.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-moveto) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION lpr-moveto Method-Library 
FUNCTION lpr-moveto RETURNS DECIMAL
  ( INPUT x AS DEC, INPUT y AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Move to a particular location
    Notes:  If X or Y are ? then the current position will be used
------------------------------------------------------------------------------*/

  IF x = ? THEN   RUN lp_get_xpos( OUTPUT x ).
  IF y = ? THEN   RUN lp_get_ypos( OUTPUT y ).

  RUN lp_moveto( x, y, OUTPUT lp_result ).
  RETURN x.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-page-break) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION lpr-page-break Method-Library 
FUNCTION lpr-page-break RETURNS LOGICAL
  ( /* no parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
  /* This variable turns off the pagination logic so we don't get too recursive */
 /* MESSAGE 'page break' lpr-page-position. */
  IF lpr-page-position > 0 THEN DO:
    /* footer half, since we're somewhere on the page... */
    lpr-between-pages = Yes.
    RUN inst-page-footer IN THIS-PROCEDURE NO-ERROR.
    lpr-between-pages = No.
    RUN lp_end_page( OUTPUT lp_result ).
    lpr-page-position = 0.
    RETURN  No.
  END.

  /* otherwise it must be the header half */
  RUN lp_start_page( OUTPUT lp_result ).
  lpr-page-number = lpr-page-number + 1.

  lpr-between-pages = Yes.
  RUN inst-page-header IN THIS-PROCEDURE NO-ERROR.
  lpr-between-pages = No.

  RETURN Yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-select-font) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION lpr-select-font Method-Library 
FUNCTION lpr-select-font RETURNS DECIMAL
  ( INPUT font-text AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Select a particular font for this (and subsequent) lines.
------------------------------------------------------------------------------*/

  IF lpr-preview-mode OR font-text = "" OR font-text = ? THEN
    RETURN lpr-current-font-height.

  IF font-text = ENTRY( lpr-current-font, lpr-font-text, lpr-delimiter) THEN
    RETURN lpr-current-font-height.     /* quick check results in nothing to do */

  lpr-current-font = LOOKUP( font-text, lpr-font-text, lpr-delimiter).

  IF lpr-current-font = 0 THEN DO:
    DEF VAR height AS DEC NO-UNDO.

    height = (25.4 * DEC( ENTRY(2,font-text))) / 72.
    lpr-current-font = NUM-ENTRIES( lpr-font-text, lpr-delimiter ) + 1.
    IF height < 0.01 THEN height = 1.   /* just to catch wierd conditions */

    lpr-font-text   = lpr-font-text   + lpr-delimiter + font-text.
    lpr-font-height = lpr-font-height + lpr-delimiter + STRING( height ).
    lpr-current-font-height = height.
  END.
  ELSE
    lpr-current-font-height = DEC(ENTRY( lpr-current-font, lpr-font-height, lpr-delimiter )).

  /* Change the font name, points, fix=1, bold = +ve, italic=1, underline=1 */
  RUN lp_set_font( ENTRY(1,font-text), DEC(ENTRY(2,font-text)), INT(ENTRY(3,font-text)),
                    INT(ENTRY(4,font-text)), INT(ENTRY(5,font-text)), INT(ENTRY(6,font-text)),
                    OUTPUT lp_result ).

  RETURN lpr-current-font-height.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-start) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION lpr-start Method-Library 
FUNCTION lpr-start RETURNS DECIMAL
  ( INPUT preview AS LOGICAL, INPUT page-reset AS CHAR, INPUT base-font AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Prepare to output a report
------------------------------------------------------------------------------*/

  ASSIGN
    lpr-preview-mode  = preview
    lpr-font-text     = base-font
    lpr-current-font-height = (IF lpr-preview-mode THEN 1 ELSE (25.4 * DEC( ENTRY(2,base-font))) / 72)
    lpr-font-height   = STRING( lpr-current-font-height )
    lpr-current-font  = 1
    lpr-page-number   = 0
    lpr-page-position = 0
    lpr-between-pages = No.

  IF NOT preview THEN DO:
    RUN lp_open( lpr-printer-name, "Capital APMS printout", OUTPUT lp_result ).
    RUN lp_set_font( base-font, OUTPUT lp_result ).
  END.

  RETURN lpr-current-font-height.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-lpr-test-bottom) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION lpr-test-bottom Method-Library 
FUNCTION lpr-test-bottom RETURNS LOGICAL
  ( INPUT lines AS DECIMAL ) :
/*---------------------------------------------------------------------------
  Returns 'TRUE' if current position + 'lines' would be off the bottom.
---------------------------------------------------------------------------*/
DEF VAR test-pos AS DEC NO-UNDO.
DEF VAR text-end AS DEC NO-UNDO.

  test-pos = (lpr-page-position + (lines * lpr-current-font-height)).
  text-end = (lpr-page-height - lpr-footer-height).

/*
&IF DEFINED(DEBUG-END) &THEN
    debug-event( "Page position " + STRING(lpr-page-position)
           + ",   Test position " + STRING(test-pos)
           + ",   Text ends at " + STRING(text-end)
           + ",   Lines " + STRING(lines)
           + ",   Pg Height " + STRING(lpr-page-height)
           + ",   Ftr Height " + STRING(lpr-footer-height)
           + ",   Font Height " + STRING(lpr-current-font-height)).
&ENDIF
*/

  RETURN test-pos >= text-end .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

