&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

{inc/ofc-this.i}
{inc/ofc-acct.i "CREDITORS" "sundry-creditors"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .15
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

{inc/ofc-this.i}
{inc/ofc-set.i "Voucher-Style" "voucher-style" "WARNING"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-cancel-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-voucher Method-Library 
PROCEDURE cancel-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DEF VAR yes-do-it AS LOGICAL INITIAL no NO-UNDO.
  IF Voucher.VoucherStatus = "C" THEN DO:
    MESSAGE "The voucher is already cancelled!"
        VIEW-AS ALERT-BOX ERROR TITLE "Voucher Already Cancelled".
    RETURN.
  END.

  IF NOT( CAN-DO( "A,H,U,Q", Voucher.VoucherStatus ) ) THEN DO:
    MESSAGE "You can only change Unapproved, Approved, Queried" SKIP
            "or Held vouchers to cancelled!" VIEW-AS ALERT-BOX ERROR
            TITLE "Not Applicable Error".
    RETURN.
  END.


  MESSAGE "Are you sure you want to cancel this voucher?"
        VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL TITLE "Cancel Voucher"
        UPDATE yes-do-it.

  IF NOT yes-do-it THEN RETURN.

  IF Voucher.VoucherStatus = "U" THEN DO:
    RUN change-voucher-status( "C" ).
  END.
  ELSE IF CAN-DO( "A,H", Voucher.VoucherStatus ) THEN
    RUN really-cancel-voucher.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-change-voucher-status) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE change-voucher-status Method-Library 
PROCEDURE change-voucher-status :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER v-status LIKE Voucher.VoucherStatus NO-UNDO.

  IF NOT AVAILABLE Voucher THEN RETURN.

DEF VAR errmsg AS CHAR NO-UNDO INITIAL "".
  
  IF LOOKUP( Voucher.VoucherStatus, "N,P" ) <> 0 THEN
    errmsg = "You cannot change the status~nof Not paid or Paid Vouchers!".
  ELSE IF v-status = "H" AND Voucher.VoucherStatus <> "A" THEN
    errmsg = "You can only hold approved vouchers".
  ELSE IF v-status = "A" AND Voucher.VoucherStatus <> "H" THEN
    errmsg = "You can only un-hold held vouchers".
  ELSE IF v-status = "Q" AND Voucher.VoucherStatus <> "U" THEN
    errmsg = "You can only query unapproved vouchers".
  ELSE IF v-status = "U" AND NOT CAN-DO("C,Q", Voucher.VoucherStatus ) THEN
    errmsg = "Only cancelled or Queried vouchers~ncan be changed to unapproved".

  IF errmsg <> "" THEN DO:
    MESSAGE errmsg VIEW-AS ALERT-BOX ERROR TITLE "Update Error".
    RETURN.
  END.

  DO TRANSACTION:
    FIND CURRENT Voucher EXCLUSIVE-LOCK.
    ASSIGN Voucher.VoucherStatus = v-status.
    FIND CURRENT Voucher EXCLUSIVE-LOCK.
  END.

  RUN dispatch( 'display-fields':U ).
  APPLY 'ROW-DISPLAY' TO {&BROWSE-NAME} IN FRAME {&FRAME-NAME}.
  RUN drl-value-changed IN THIS-PROCEDURE NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-hold-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hold-voucher Method-Library 
PROCEDURE hold-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN change-voucher-status( "H" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-inst-value-changed-not) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-value-changed-not Method-Library 
PROCEDURE inst-value-changed-not :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN set-link-attributes IN sys-mgr(
    THIS-PROCEDURE, "hold-voucher,unhold-voucher",
    "SENSITIVE = " + IF AVAILABLE Voucher AND LOOKUP( Voucher.VoucherStatus, "A,H" ) <> 0 THEN
      "Yes" ELSE "No"
  ).

  RUN set-link-attributes IN sys-mgr(
    THIS-PROCEDURE, "cancel-voucher,uncancel-voucher",
    "SENSITIVE = " + IF AVAILABLE(Voucher) AND LOOKUP( Voucher.VoucherStatus, "U,C,A,H" ) <> 0 THEN
      "Yes" ELSE "No"
  ).

  RUN set-link-attributes IN sys-mgr(
    THIS-PROCEDURE, "Maintain",
    "SENSITIVE = " + IF AVAILABLE Voucher AND LOOKUP( Voucher.VoucherStatus, "U" ) <> 0 THEN
      "Yes" ELSE "No"
  ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-query-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE query-voucher Method-Library 
PROCEDURE query-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN change-voucher-status( "Q" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-really-cancel-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE really-cancel-voucher Method-Library 
PROCEDURE really-cancel-voucher :
/*------------------------------------------------------------------------------
  Purpose:  Really Cancel the voucher, creating a reversal batch.
------------------------------------------------------------------------------*/
DEF VAR yes-do-it AS LOGI NO-UNDO INITIAL No.
DEF VAR voucher-month LIKE Month.MonthCode INITIAL ? NO-UNDO.
DEF VAR closing-group LIKE AcctTran.ClosingGroup INITIAL ? NO-UNDO.
DEF VAR docref-code AS INT NO-UNDO.

  DEF VAR voucher-string AS CHAR NO-UNDO.
  voucher-string = "VCHR" + TRIM( STRING( Voucher.VoucherSeq )).

  FIND FIRST AcctTran WHERE AcctTran.BatchCode = Voucher.BatchCode
                        AND AcctTran.DocumentCode = Voucher.DocumentCode
                        AND AcctTran.ConsequenceOf = 0 NO-LOCK NO-ERROR.
  IF NOT (AVAILABLE(Voucher) AND AVAILABLE(AcctTran)) THEN DO:
    MESSAGE "No transactions related to the voucher can be found!" SKIP(2)
            "The voucher batch may not yet have been updated yet." SKIP(2)
            "This voucher will not be cancelled."
          VIEW-AS ALERT-BOX ERROR TITLE "Allocations for Vouchers of Voucher could not be found!".
    RETURN.
    
  END.

  FIND Document WHERE Document.BatchCode = Voucher.BatchCode
                  AND Document.DocumentCode = Voucher.DocumentCode
                  NO-LOCK NO-ERROR.

  IF AVAILABLE(Document) THEN ASSIGN docref-code = INTEGER(SUBSTRING(Document.Reference,5)) NO-ERROR.
  IF AVAILABLE(Document) AND docref-code = Voucher.VoucherSeq THEN DO:
    FIND FIRST AcctTran OF Document NO-LOCK NO-ERROR.
    voucher-month = AcctTran.MonthCode.
  END.
  ELSE DO:
    FOR EACH AcctTran NO-LOCK WHERE AcctTran.EntityType = "C"
             AND AcctTran.EntityCode = Voucher.CreditorCode
             AND AcctTran.AccountCode = sundry-creditors,
        FIRST Document OF AcctTran NO-LOCK WHERE Document.Reference = voucher-string:
      voucher-month = AcctTran.MonthCode.
      LEAVE.
    END.
  END.

  cancel-block:
  DO TRANSACTION:

    CREATE NewBatch.
    ASSIGN NewBatch.BatchType = "NORM"
           NewBatch.Description = "Cancel voucher " + voucher-string.

    CREATE NewDocument.
    ASSIGN NewDocument.BatchCode = NewBatch.BatchCode
           NewDocument.DocumentCode = 1
           NewDocument.Reference = voucher-string
           NewDocument.Description = "CANCEL Voucher " + voucher-string.

    /* unapprove and unallocate the voucher */
    IF Voucher.BatchCode > 0 AND Voucher.DocumentCode > 0 THEN DO:
      FOR EACH AcctTran WHERE AcctTran.BatchCode = Voucher.BatchCode
                          AND AcctTran.DocumentCode = Voucher.DocumentCode
                          AND AcctTran.ConsequenceOf = 0 NO-LOCK:
        CREATE NewAcctTrans.
        ASSIGN NewAcctTrans.BatchCode = NewBatch.BatchCode
               NewAcctTrans.DocumentCode = 1
               NewAcctTrans.Reference = ""
               NewAcctTrans.EntityType = AcctTran.EntityType
               NewAcctTrans.EntityCode = AcctTran.EntityCode
               NewAcctTrans.AccountCode = AcctTran.AccountCode
               NewAcctTrans.Date = AcctTran.Date
               NewAcctTrans.Amount = - AcctTran.Amount
               NewAcctTrans.Description = "CANCEL voucher - " + Voucher.Description.
      END.
    END.

    FIND CURRENT Voucher EXCLUSIVE-LOCK.
    ASSIGN Voucher.VoucherStatus = "C"
           Voucher.BatchCode = ?
           Voucher.DocumentCode = ? .
    FIND CURRENT Voucher NO-LOCK.
  END.

  RUN dispatch( 'display-fields':U ).
  APPLY 'ROW-DISPLAY' TO {&BROWSE-NAME} IN FRAME {&FRAME-NAME}.
  RUN drl-value-changed IN THIS-PROCEDURE NO-ERROR.

  MESSAGE "Voucher cancelled" SKIP(1)
          "You will need to post batch" NewBatch.BatchCode "which has been created."
          VIEW-AS ALERT-BOX INFORMATION
          TITLE "Voucher Cancelled".
  RUN dispatch( 'row-available':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reprint-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reprint-voucher Method-Library 
PROCEDURE reprint-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Voucher THEN RETURN.

DEF VAR voucher-list AS CHAR NO-UNDO.
  voucher-list = STRING( Voucher.VoucherSeq ).

  CASE voucher-style:
    WHEN "1" THEN RUN process/report/vchrappr.p( ?, ?, voucher-list ).
    WHEN "2" THEN RUN process/report/vchrapr2.p( ?, ?, voucher-list ).
    OTHERWISE     RUN process/report/vchrappr.p( ?, ?, voucher-list ).
  END CASE.
    
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-uncancel-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE uncancel-voucher Method-Library 
PROCEDURE uncancel-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN change-voucher-status( "U" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-unhold-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE unhold-voucher Method-Library 
PROCEDURE unhold-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN change-voucher-status( "A" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-unquery-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE unquery-voucher Method-Library 
PROCEDURE unquery-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN change-voucher-status( "U" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-delete-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-voucher Method-Library
PROCEDURE delete-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR yes-do-it AS LOG INIT NO.

  IF Voucher.VoucherStatus <> "U" THEN DO:
    MESSAGE "You can only delete an unapproved voucher as there may be~n"
            "transactions associated with other types of vouchers."
    VIEW-AS ALERT-BOX ERROR TITLE "Can not delete a voucher with transactions".
    RETURN.
  END.

  FIND FIRST AcctTran WHERE AcctTran.BatchCode = Voucher.BatchCode
                        AND AcctTran.DocumentCode = Voucher.DocumentCode
                      /* AND AcctTran.ConsequenceOf = 0 */
                        NO-LOCK NO-ERROR.

  IF AVAILABLE(AcctTran) THEN DO:
    MESSAGE "You have selected to delete an unapproved voucher, but for some reason~n"
            "there are transactions associated with this vouchers batch. I am not~n"
            "sure how this happened, but to be safe I cant delete the voucher."
    VIEW-AS ALERT-BOX ERROR TITLE "Can not delete a voucher with transactions".
    RETURN.
  END.

  MESSAGE "Are you sure you want to delete this voucher?"
    VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL TITLE "Delete Voucher"
    UPDATE yes-do-it.

  IF yes-do-it THEN DO:
    DO TRANSACTION:
      FIND CURRENT Voucher EXCLUSIVE-LOCK.
      DELETE Voucher.
    END.
    RUN dispatch( 'open-query':U ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

