&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : inc/method/m-impcsv.i
    Purpose     : Implement functions for importing CSV files

    Author(s)   : Andrew McMillan
    Created     : 3/5/98
  ------------------------------------------------------------------------*/

DEFINE TEMP-TABLE ImportField NO-UNDO
            FIELD RecordNo AS INT
            FIELD FieldNo AS INT
            FIELD FieldData AS CHAR
            INDEX xpkIF IS UNIQUE PRIMARY RecordNo FieldNo
            INDEX xakIF FieldNo RecordNo.

DEF VAR import-record-no AS INT NO-UNDO INITIAL 0.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-csv-clear-fields) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD csv-clear-fields Method-Library 
FUNCTION csv-clear-fields RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-csv-find-record) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD csv-find-record Method-Library 
FUNCTION csv-find-record RETURNS CHARACTER
  ( INPUT record-no AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-csv-get-field) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD csv-get-field Method-Library 
FUNCTION csv-get-field RETURNS CHARACTER
  ( INPUT field-no AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-csv-import-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD csv-import-line Method-Library 
FUNCTION csv-import-line RETURNS CHARACTER
  ( /* no parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-csv-split-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD csv-split-line Method-Library 
FUNCTION csv-split-line RETURNS INTEGER
  ( INPUT in-line AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-to-number) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD to-number Method-Library 
FUNCTION to-number RETURNS DECIMAL
  ( INPUT txtnum AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .5
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-csv-clear-fields) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION csv-clear-fields Method-Library 
FUNCTION csv-clear-fields RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Clear the current fields ready for a new one
    Notes:  
------------------------------------------------------------------------------*/
  FOR EACH ImportField:     DELETE ImportField.     END.
  RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-csv-find-record) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION csv-find-record Method-Library 
FUNCTION csv-find-record RETURNS CHARACTER
  ( INPUT record-no AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Find the first field of the specified record
    Notes:  
------------------------------------------------------------------------------*/
  FIND FIRST ImportField WHERE ImportField.RecordNo = record-no
                           AND ImportField.FieldNo = 1 NO-LOCK NO-ERROR.

  IF AVAILABLE(ImportField) THEN
    RETURN ImportField.FieldData.
  ELSE
    RETURN ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-csv-get-field) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION csv-get-field Method-Library 
FUNCTION csv-get-field RETURNS CHARACTER
  ( INPUT field-no AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Return field<x> from the current record
------------------------------------------------------------------------------*/
DEFINE BUFFER I-F FOR ImportField.

  IF AVAILABLE(ImportField) THEN
    FIND I-F WHERE I-F.RecordNo = ImportField.RecordNo
                    AND I-F.FieldNo = field-no NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(I-F) THEN
    FIND LAST I-F WHERE I-F.FieldNo = field-no NO-LOCK NO-ERROR.

  IF AVAILABLE(I-F) THEN
    RETURN I-F.FieldData.
  ELSE
    RETURN ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-csv-import-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION csv-import-line Method-Library 
FUNCTION csv-import-line RETURNS CHARACTER
  ( /* no parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Import a line and split it into fields
    Notes:  Returns the unformatted import line in it's entirety
------------------------------------------------------------------------------*/

DEF VAR in-line AS CHAR NO-UNDO.

  /* We need to make sure we preserve the error action so normal    */
  /* import loops behave as we would expect them to.                */
  IMPORT UNFORMATTED in-line NO-ERROR.
  IF ERROR-STATUS:ERROR THEN RETURN ERROR ?.

  csv-split-line( in-line ).

  RETURN in-line.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-csv-split-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION csv-split-line Method-Library 
FUNCTION csv-split-line RETURNS INTEGER
  ( INPUT in-line AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  split the csv fields in the line
    Notes:  Returns the number of fields found.
------------------------------------------------------------------------------*/

DEF VAR p1 AS INT NO-UNDO.
DEF VAR p2 AS INT NO-UNDO.
DEF VAR fld-no AS INT NO-UNDO.
DEF VAR txt-length AS INT NO-UNDO.
DEF VAR search-for AS CHAR NO-UNDO.
DEF VAR fld-val AS CHAR NO-UNDO.
DEF VAR this-char AS CHAR NO-UNDO.

  import-record-no = import-record-no + 1.
  p2 = 0.
  fld-no = 1.
  txt-length = LENGTH(in-line).

  parse-text-loop:
  DO WHILE p2 < txt-length:
    p1 = p2 + 1.            /* point to first char of new field */
    search-for = ",".       /* default to search for next comma */
    IF SUBSTRING( in-line, p1, 1) = '"' THEN DO:
      p1 = p1 + 1.          /* OK, starts with a quote, so we skip that */
      search-for = '"'.     /* searching for the closing quote */
    END.

    fld-val = "".
    p2 = p1.
    scan-field-loop:
    DO WHILE p2 <= txt-length:
      this-char = SUBSTRING(in-line,p2,1).
      IF this-char = search-for THEN DO:
        IF search-for = '"' THEN DO:
          p2 = p2 + 1.  /* two-quotes implies a single quote so skip an extra character */
          IF SUBSTRING( in-line, p2, 1) = "," OR SUBSTRING( in-line, p2, 1) = "" THEN
            LEAVE scan-field-loop.  /* searching for a quote and found one followed by a comma or null */
        END.
        ELSE
          LEAVE scan-field-loop.  /* searching a comma and found one */
      END.
      fld-val = fld-val + this-char.
      p2 = p2 + 1.
    END.

    /* assign the field value to a newly created field */
    CREATE ImportField.
    ASSIGN  ImportField.RecordNo  = import-record-no
            ImportField.FieldNo   = fld-no
            ImportField.FieldData = fld-val
            fld-no = fld-no + 1.
  END.


  RETURN (fld-no - 1).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-to-number) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION to-number Method-Library 
FUNCTION to-number RETURNS DECIMAL
  ( INPUT txtnum AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert a text value into a number
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR result AS DEC NO-UNDO.
DEF VAR bare   AS CHAR NO-UNDO.
DEF VAR negative AS LOGI NO-UNDO INITIAL No.

  bare = REPLACE(txtnum, '"', "").
  bare = REPLACE(bare, '$', "").
  bare = REPLACE(bare, ',', "").
  bare = REPLACE(bare, '%', "").
  bare = REPLACE(bare, ';', "").
  bare = REPLACE(bare, ' ', "").
  bare = REPLACE(bare, '<', "").
  bare = REPLACE(bare, 'C', "").
  bare = REPLACE(bare, 'R', "").
  bare = REPLACE(bare, 'D', "").
  bare = REPLACE(bare, 'B', "").
  bare = REPLACE(bare, '-', "").
  bare = REPLACE(bare, '(', "").
  bare = REPLACE(bare, ')', "").

  /* replace numeric-looking letters with numbers! */
  bare = REPLACE(bare, 'O', "0").
  bare = REPLACE(bare, 'L', "1").
  bare = REPLACE(bare, 'Z', "2").

  IF (INDEX(txtnum, "CR") > 0) THEN negative = Yes.
  IF (INDEX(txtnum, "DB") > 0) THEN negative = No.
  IF (INDEX(txtnum, "-") > 0) THEN  negative = Yes.
  IF (INDEX(txtnum, "<") > 0) THEN  negative = Yes.
  IF (INDEX(txtnum, "(") > 0) THEN  negative = Yes.
  ASSIGN result = DEC(TRIM(bare)) NO-ERROR.
  IF result = ? THEN RETURN ?.
  IF negative THEN result = result * -1 .
  IF INDEX( txtnum, "%") > 0 THEN result = result / 100.

/*  MESSAGE txtnum negative bare result . */

  RETURN result.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

