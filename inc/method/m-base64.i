&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*------------------------------------------------------------------------
    Library     : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Definitions */
DEF STREAM base64stream.
DEF STREAM incomingfile.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-base64-encode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD base64-encode Method-Library 
FUNCTION base64-encode RETURNS CHARACTER
(
    input-stream AS MEMPTR,
    is-a-string AS LOGICAL,
    to-file AS LOGICAL
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-base64-encode-file) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD base64-encode-file Method-Library 
FUNCTION base64-encode-file RETURNS CHARACTER
( file-param AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-base64-encode-string) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD base64-encode-string Method-Library 
FUNCTION base64-encode-string RETURNS CHARACTER
( input-string AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "MethodLibraryCues" Method-Library _INLINE
/* Actions: adecomm/_so-cue.w ? adecomm/_so-cued.p ? adecomm/_so-cuew.p */
/* Method Library,uib,70080
Destroy on next read */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-base64-encode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION base64-encode Method-Library 
FUNCTION base64-encode RETURNS CHARACTER
(
    input-stream AS MEMPTR,
    is-a-string AS LOGICAL,
    to-file AS LOGICAL
) :
/*------------------------------------------------------------------------------
  Purpose: Encode a mem pointer binary stream into a base64 encoded string
  
    Notes: The following description of the base64 encoding process..

    * Divide the input bytes stream into blocks of 3 bytes.
    * Divide the 24 bits of a 3-byte block into 4 groups of 6 bits.
    * Map each group of 6 bits to 1 printable character, based on the 6-bit value.
    * If the last 3-byte block has only 1 byte of input data, pad 2 bytes of zero
      (\x0000). After encoding it as a normal block, override the last 2 characters
      with 2 equal signs (==), so the decoding process knows 2 bytes of zero were
      padded.
    * If the last 3-byte block has only 2 bytes of input data, pad 1 byte of zero
      (\x00). After encoding it as a normal block, override the last 1 character
      with 1 equal signs (=), so the decoding process knows 1 byte of zero was
      padded.
    * Carriage return (\r) and new line (\n) are inserted into the output character
      stream. They will be ignored by the decoding process.
      
------------------------------------------------------------------------------*/
DEF VAR Codes64 AS CHARACTER NO-UNDO.
DEF VAR output-string AS CHARACTER NO-UNDO.
DEF VAR v-count AS INTEGER NO-UNDO.
DEF VAR v-index AS INTEGER NO-UNDO.
DEF VAR grand-count AS INTEGER INIT 0 NO-UNDO.

DEF VAR stream-length AS INTEGER NO-UNDO.

DEF VAR first-int AS INTEGER NO-UNDO.
DEF VAR second-int AS INTEGER NO-UNDO.
DEF VAR third-int AS INTEGER NO-UNDO.
DEF VAR last-block AS LOGICAL INIT NO NO-UNDO.

DEF VAR integer-array AS INTEGER EXTENT 4 NO-UNDO.

/* Temp file vars */
DEF VAR temp-file-name AS CHARACTER NO-UNDO.

/* Encoding string */
Codes64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'.

/* If the output is set to go to a file */
IF to-file THEN DO:
    temp-file-name = SESSION:TEMP-DIRECTORY + 'BASE64' +
        STRING( TODAY, "99999999" ) +
        STRING( TIME ) +
        STRING( RANDOM( 1, 200 ) ) + '.tmp'.
    OUTPUT STREAM base64stream TO VALUE( temp-file-name ).
END.

/* If this is a CHAR stream, then ignore the last byte as it will be a NULL */
stream-length = GET-SIZE( input-stream ).
IF is-a-string THEN
    stream-length = stream-length - 1.

DO v-count = 1 TO stream-length BY 3:

    IF v-count + 3 >= stream-length THEN
        last-block = YES.

    integer-array[1] = 0.
    integer-array[2] = 0.
    integer-array[3] = 0.
    integer-array[4] = 0.

    /* Fetch the next three chars */
    first-int = GET-BYTE( input-stream, v-count ).
    second-int = GET-BYTE( input-stream, v-count + 1 ).
    third-int = GET-BYTE( input-stream, v-count + 2 ).

    /* Pack the three 8 bit integers into 4 "6 bit" integers */
    PUT-BITS( integer-array[1], 1, 6 ) = GET-BITS( first-int, 3, 6 ).
    PUT-BITS( integer-array[2], 5, 2 ) = GET-BITS( first-int, 1, 2 ).
    PUT-BITS( integer-array[2], 1, 4 ) = GET-BITS( second-int, 5, 4 ).
    PUT-BITS( integer-array[3], 3, 4 ) = GET-BITS( second-int, 1, 4 ).
    PUT-BITS( integer-array[3], 1, 2 ) = GET-BITS( third-int, 7, 2 ).
    PUT-BITS( integer-array[4], 1, 6 ) = GET-BITS( third-int, 1, 6 ).

    DO v-index = 1 TO 4:
        /* Wrap the string every 76 characters */
        IF grand-count <> 0 AND grand-count MODULO 72 = 0 THEN
            output-string = output-string + CHR(13) + CHR(10).

        /* Check if the last two characters exist */
        IF v-index = 3 OR v-index = 4 THEN DO:
            IF last-block AND integer-array[v-index] = 0 THEN
                output-string = output-string + '='.
            ELSE
                output-string = output-string + SUBSTRING( Codes64, integer-array[v-index] + 1, 1 ).
        END.
        ELSE
            output-string = output-string + SUBSTRING( Codes64, integer-array[v-index] + 1, 1 ).

        /* Count the chars for use in the MODULO */
        grand-count = grand-count + 1.
    END.

    /* If this data is going to a file then write the data and clear the string buffer */
    IF to-file THEN DO:
        PUT STREAM base64stream UNFORMATTED output-string.
        output-string = ''.
    END.
END.

/* Return the temp-file name or the string */
IF to-file THEN DO:
    PUT STREAM base64stream UNFORMATTED CHR(10).
    OUTPUT STREAM base64stream CLOSE.
    RETURN temp-file-name.
END.
ELSE
    RETURN output-string.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-base64-encode-file) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION base64-encode-file Method-Library 
FUNCTION base64-encode-file RETURNS CHARACTER
( file-param AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose: Base64 encode a file
    Notes:
------------------------------------------------------------------------------*/
DEF VAR fileptr AS MEMPTR.
DEF VAR encoded-file-name AS CHARACTER NO-UNDO.

FILE-INFO:FILE-NAME = file-param.
IF FILE-INFO:FILE-MOD-DATE = ? THEN DO:
    RETURN "".
END.

/* Slurp the file in */
SET-SIZE( fileptr ) = FILE-INFO:FILE-SIZE.
INPUT STREAM incomingfile FROM VALUE( file-param ) BINARY.
IMPORT STREAM incomingfile fileptr.
INPUT STREAM incomingfile CLOSE.

/*
Encode the file in base64. NO because not a string, YES because into a temp file.
The function returns the temp file location of the encoded data
*/
encoded-file-name = base64-encode( fileptr, NO, YES ).

RETURN encoded-file-name.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-base64-encode-string) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION base64-encode-string Method-Library 
FUNCTION base64-encode-string RETURNS CHARACTER
( input-string AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose: Base64 encode an arbritrary string
    Notes:
------------------------------------------------------------------------------*/
DEF VAR string-pointer AS MEMPTR.
SET-SIZE( string-pointer ) = LENGTH( input-string ) + 1.

PUT-STRING( string-pointer, 1 ) = input-string.

/*
YES because this function should ignore the terminating NULL, and NO
because the output should not go to a file.
*/
RETURN base64-encode( string-pointer, YES, NO ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

