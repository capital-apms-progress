&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : inc/method/m-lease-rentals.i
    Purpose     : Used by reports which report on rentals for leases / areas

    Author(s)   : Andrew McMillan
    Created     : 2/2/1999
    Notes       : Cut from Sydney Schedule.
  ------------------------------------------------------------------------*/
DEF VAR show-future-rentals AS LOGI NO-UNDO INITIAL No.
DEF VAR all-future-rentals AS LOGI NO-UNDO INITIAL No.

DEF TEMP-TABLE LeaseTotal NO-UNDO
    FIELD LeaseCode AS INT
    FIELD AnnualRent AS DEC INITIAL 0.0 
    FIELD ContractRent AS DEC INITIAL 0.0
    FIELD AnnualOutgoings AS DEC INITIAL 0.0 
    FIELD AnnualCleaning AS DEC INITIAL 0.0 
    FIELD AreaSize AS DEC INITIAL 0.0 
    FIELD AreaCount AS INT INITIAL 0
    INDEX XPKLeaseTotals IS UNIQUE PRIMARY LeaseCode.

DEF TEMP-TABLE LeaseRental NO-UNDO
    FIELD LeaseCode AS INT
    FIELD AreaType AS CHAR
    FIELD AnnualRent AS DEC INITIAL 0.0 
    FIELD AnnualOutgoings AS DEC INITIAL 0.0 
    FIELD AnnualCleaning AS DEC INITIAL 0.0 
    FIELD ContractRent AS DEC INITIAL 0.0
    FIELD AreaSize AS DEC INITIAL 0.0 
    FIELD AreaCount AS INT INITIAL 0
    FIELD Future AS LOGI INITIAL No
    INDEX XPKLeaseRentals IS UNIQUE PRIMARY LeaseCode AreaType.

DEF TEMP-TABLE AreaRental NO-UNDO
    FIELD PropertyCode AS INT
    FIELD RentalSpaceCode AS INT
    FIELD LeaseCode AS INT
    FIELD AreaType AS CHAR
    FIELD AnnualRent AS DEC INITIAL 0.0
    FIELD AnnualCleaning AS DEC INITIAL 0.0
    FIELD AnnualOutgoings AS DEC INITIAL 0.0 
    FIELD ContractRent AS DEC INITIAL 0.0
    FIELD AreaSize AS DEC INITIAL 0.0
    INDEX XPKLeaseRentals IS UNIQUE PRIMARY PropertyCode RentalSpaceCode .

{inc/ofc-this.i}
{inc/ofc-set.i "RentCharge-Outgoings" "outgoings-type-list"}
{inc/ofc-set.i "RentCharge-Cleaning" "cleaning-type-list"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-get-area-cleaning) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-area-cleaning Method-Library 
FUNCTION get-area-cleaning RETURNS DECIMAL
  ( INPUT property-code AS INT, INPUT area-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-area-outgoings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-area-outgoings Method-Library 
FUNCTION get-area-outgoings RETURNS DECIMAL
  ( INPUT property-code AS INT, INPUT area-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-area-rental) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-area-rental Method-Library 
FUNCTION get-area-rental RETURNS DECIMAL
  ( INPUT property-code AS INT, INPUT area-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = 8.4
         WIDTH              = 45.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-build-lease-rentals) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-lease-rentals Method-Library 
PROCEDURE build-lease-rentals :
/*------------------------------------------------------------------------------
  Purpose:  Build tables of rental for each type for each lease
            and then build allocate them by the area type for each LseSpace.
------------------------------------------------------------------------------*/
DEF BUFFER Lease FOR TenancyLease.
DEF BUFFER LseCharge FOR RentCharge.
DEF BUFFER LseSpace FOR RentalSpace.
DEF VAR no-current-rentals AS LOGI NO-UNDO INITIAL NO.
DEF VAR this-rental AS DEC NO-UNDO.

  FOR EACH Lease OF Property WHERE Lease.LeaseStatus <> "PAST" NO-LOCK:
    CREATE LeaseTotal.
    LeaseTotal.LeaseCode = Lease.TenancyLeaseCode.
    no-current-rentals = NOT CAN-FIND( FIRST LseCharge OF Lease WHERE CAN-FIND( FIRST RentChargeLine OF LseCharge WHERE RentChargeLine.RentChargeLineStatus = "C" AND RentChargeLine.StartDate <= TODAY)).
    FOR EACH LseCharge OF Lease WHERE CAN-FIND( FIRST RentChargeLine OF LseCharge WHERE RentChargeLine.RentChargeLineStatus = "C"
                                                 OR RentChargeLine.RentChargeLineStatus = "I") NO-LOCK:
      FIND LeaseRental WHERE LeaseRental.LeaseCode = Lease.TenancyLeaseCode
                         AND LeaseRental.AreaType = LseCharge.RentChargeType
                         NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(LeaseRental) THEN DO:
        CREATE LeaseRental.
        LeaseRental.LeaseCode = Lease.TenancyLeaseCode.
        LeaseRental.AreaType = LseCharge.RentChargeType.
      END.

      IF (show-future-rentals AND (Lease.LeaseStartDate > TODAY)) OR no-current-rentals THEN DO:
        FIND FIRST RentChargeLine OF LseCharge WHERE RentChargeLine.RentChargeLineStatus = "C"
                                    AND (no-current-rentals OR RentChargeLine.StartDate > TODAY)
                                               NO-LOCK NO-ERROR.
        IF NOT AVAILABLE(RentChargeLine) OR (RentChargeLine.EndDate = RentChargeLine.LastChargedDate 
            AND RentChargeLine.LastChargedDate <> ?) THEN
          NEXT.
        this-rental =  to-annual( RentChargeLine.FrequencyCode, RentChargeLine.Amount ) .
        IF RentChargeLine.StartDate > TODAY THEN LeaseRental.Future = Yes.
      END.
      ELSE DO:
        this-rental = LseCharge.CurrentAnnualRental .
      END.
      IF CAN-DO( outgoings-type-list, LseCharge.RentChargeType ) THEN DO:
        LeaseTotal.AnnualOutgoings = LeaseTotal.AnnualOutgoings + this-rental.
      END.
      ELSE IF CAN-DO( cleaning-type-list, LseCharge.RentChargeType ) THEN DO:
        LeaseTotal.AnnualCleaning = LeaseTotal.AnnualCleaning + this-rental.
      END.
      ELSE DO:
        LeaseTotal.AnnualRent = LeaseTotal.AnnualRent + this-rental.
      END.
      LeaseRental.AnnualRent = LeaseRental.AnnualRent + this-rental.
    END.

    FOR EACH LseSpace OF Lease WHERE LseSpace.AreaStatus = "L" NO-LOCK:
      CREATE AreaRental.
      AreaRental.PropertyCode = LseSpace.PropertyCode.
      AreaRental.RentalSpaceCode = LseSpace.RentalSpaceCode.
      AreaRental.LeaseCode = Lease.TenancyLeaseCode.
      AreaRental.AreaSize = LseSpace.AreaSize.
      AreaRental.AreaType = LseSpace.AreaType.
      AreaRental.ContractRent = LseSpace.ContractedRental.
      FIND LeaseRental WHERE LeaseRental.LeaseCode = Lease.TenancyLeaseCode
                         AND LeaseRental.AreaType = LseSpace.AreaType
                         NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(LeaseRental) THEN DO:
        CREATE LeaseRental.
        LeaseRental.LeaseCode = Lease.TenancyLeaseCode.
        LeaseRental.AreaType = LseSpace.AreaType.
      END.
      LeaseRental.AreaSize = LeaseRental.AreaSize + LseSpace.AreaSize.
      LeaseRental.AreaCount = LeaseRental.AreaCount + 1.
      LeaseRental.ContractRent = LeaseRental.ContractRent + LseSpace.ContractedRental.
      
      FIND AreaType WHERE AreaType.AreaType = LeaseRental.AreaType NO-LOCK NO-ERROR.
      IF AVAILABLE(AreaType) AND AreaType.IsFloorArea THEN DO:
        LeaseTotal.AreaSize     = LeaseTotal.AreaSize + LseSpace.AreaSize.
        LeaseTotal.AreaCount    = LeaseTotal.AreaCount + 1.
        LeaseTotal.ContractRent = LeaseTotal.ContractRent + LseSpace.ContractedRental.
      END.
    END.

DEF BUFFER NextAreaRent FOR AreaRental.
DEF VAR i AS INT NO-UNDO.
DEF VAR j AS INT NO-UNDO.
DEF VAR rent-ratio AS DEC NO-UNDO.
DEF VAR area-ratio AS DEC NO-UNDO.
    FOR EACH LeaseRental WHERE LeaseRental.LeaseCode = Lease.TenancyLeaseCode
                           AND LeaseRental.AnnualRent <> 0:
      i = 1.
      j = 1.
      FIND AreaType WHERE AreaType.AreaType = LeaseRental.AreaType NO-LOCK NO-ERROR.
      
      FOR EACH AreaRental WHERE AreaRental.LeaseCode = LeaseRental.LeaseCode
                            AND AreaRental.AreaType = LeaseRental.AreaType:
        IF LeaseRental.AreaCount < 2 THEN
          AreaRental.AnnualRent = LeaseRental.AnnualRent.
        ELSE IF AreaRental.ContractRent <> 0 AND LeaseRental.ContractRent <> 0 THEN
          AreaRental.AnnualRent = LeaseRental.AnnualRent * (AreaRental.ContractRent / LeaseRental.ContractRent).
        ELSE IF LeaseRental.AreaSize <> 0 THEN
          AreaRental.AnnualRent = LeaseRental.AnnualRent * (AreaRental.AreaSize / LeaseRental.AreaSize) .
        ELSE IF i = 1 THEN
          AreaRental.AnnualRent = LeaseRental.AnnualRent.
        
        IF AVAILABLE(AreaType) AND AreaType.IsFloorArea THEN DO:
          area-ratio = AreaRental.AreaSize / LeaseTotal.AreaSize.
          IF LeaseTotal.AreaCount < 2 THEN DO:
            AreaRental.AnnualOutgoings = LeaseTotal.AnnualOutgoings.
            AreaRental.AnnualCleaning = LeaseTotal.AnnualCleaning.
          END.
          ELSE IF AreaRental.ContractRent <> 0 AND LeaseTotal.ContractRent <> 0 THEN DO:
            rent-ratio = (AreaRental.ContractRent / LeaseTotal.ContractRent).
            AreaRental.AnnualOutgoings = LeaseTotal.AnnualOutgoings * rent-ratio.
            AreaRental.AnnualCleaning = LeaseTotal.AnnualCleaning * area-ratio.
          END.
          ELSE IF LeaseTotal.AreaSize <> 0 THEN DO:
            AreaRental.AnnualOutgoings = LeaseTotal.AnnualOutgoings * area-ratio.
            AreaRental.AnnualCleaning = LeaseTotal.AnnualCleaning * area-ratio.
          END.
          ELSE IF j = 1 THEN DO:
            AreaRental.AnnualOutgoings = LeaseTotal.AnnualOutgoings.
            AreaRental.AnnualCleaning = LeaseTotal.AnnualCleaning.
          END.
          j = j + 1.
        END.

        i = i + 1.
      END.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-get-area-cleaning) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-area-cleaning Method-Library 
FUNCTION get-area-cleaning RETURNS DECIMAL
  ( INPUT property-code AS INT, INPUT area-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Get the rental for this area
    Notes:  
------------------------------------------------------------------------------*/
  FIND AreaRental WHERE AreaRental.PropertyCode = property-code
                    AND AreaRental.RentalSpaceCode = area-code NO-ERROR.

  RETURN (IF AVAILABLE(AreaRental) THEN AreaRental.AnnualCleaning ELSE 0.0 ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-area-outgoings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-area-outgoings Method-Library 
FUNCTION get-area-outgoings RETURNS DECIMAL
  ( INPUT property-code AS INT, INPUT area-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Get the rental for this area
    Notes:  
------------------------------------------------------------------------------*/
  FIND AreaRental WHERE AreaRental.PropertyCode = property-code
                    AND AreaRental.RentalSpaceCode = area-code NO-ERROR.

  RETURN (IF AVAILABLE(AreaRental) THEN AreaRental.AnnualOutgoings ELSE 0.0 ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-area-rental) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-area-rental Method-Library 
FUNCTION get-area-rental RETURNS DECIMAL
  ( INPUT property-code AS INT, INPUT area-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Get the rental for this area
    Notes:  
------------------------------------------------------------------------------*/
  FIND AreaRental WHERE AreaRental.PropertyCode = property-code
                    AND AreaRental.RentalSpaceCode = area-code NO-ERROR.

  RETURN (IF AVAILABLE(AreaRental) THEN AreaRental.AnnualRent ELSE 0.0 ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

