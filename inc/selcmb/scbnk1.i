/* scbnk1.i */
/* selection combo for an BankAccount */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="BankAccount"
                     &ExtKey  ="BankAccountCode"
                     &Clause  ="BankAccount.Active"
                     &DV      ="STRING(BankAccount.BankAccountCode,'X(4)') ~
			      + ' -' + STRING(BankAccount.CompanyCode, '>>999') + '/' + STRING(BankAccount.AccountCode,'9999.99') ~
                              + ' - ' + BankAccount.AccountName" }
