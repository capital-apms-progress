/* sccft1.i */
/* selection combo for an CashFlowTypes */


{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="CashFlowType"
                     &ExtKey  ="CashFlowType"
                     &Clause  ="(mode = 'View' OR NOT CashFlowType.SystemGenerated)"
                     &DV      ="STRING( CashFlowType.CashFlowType, 'X(4)' ) + ' - ' + CashFlowType.Description"}
