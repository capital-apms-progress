/*--------------------------------------------------------------------------
    File        : scmst1.i
    Purpose     : Initialise and display routine for all select combo boxes

    Parameters  : &IntTable - The internal table
                  &IntCode  - The internal code for the external table
                  &ExtTable - The external table
                  &ExtKey   - The key code of the external table
                  &Clause    - (Optional) The WHERE part of the external
                              table lookup.
                              i.e FOR EACH &ExtTable WHERE &Clause NO-LOCK: ....                   
                                    
                  Either of the following:
                  &ExtField - The external field to display in the combo
                  &DV       - The display value to display in the combo
                  
    Description : This will initialise the select combo and display
                  the appropriate external table value.

    Author(s)   : Tyrone McAuley
    Notes       : The private-data of this combo is organised as follows
                  ROWID1,ROWID2,.......,ROWID
                  
  ------------------------------------------------------------------------*/

&SCOPED-DEFINE N {&SEQUENCE}   /* Helps uniquely identify all variables for
                                  multiple references within the same tigger */

&IF "{&IntTable}" <> "?" &THEN
  &SCOPED-DEFINE IntTC {&IntTable}.{&IntCode}
&ELSE
  &SCOPED-DEFINE IntTC {&IntCode}
&ENDIF

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item{&N}       AS CHAR NO-UNDO.
  DEF VAR item-id{&N}    AS CHAR NO-UNDO.
  DEF VAR rowid-list{&N} AS CHAR NO-UNDO.

  {&SELF-NAME}:LIST-ITEMS = "".
  {&SELF-NAME}:DELIMITER = CHR(7).

  &IF DEFINED(Clause) NE 0 &THEN
    FOR EACH {&ExtTable} WHERE {&Clause} NO-LOCK:
  &ELSE
    FOR EACH {&ExtTable} NO-LOCK:
  &ENDIF

    /*--- Add items to the list ---*/
    
    &IF     DEFINED(DV)       NE 0 &THEN item{&N} = {&DV}.
    &ELSEIF DEFINED(ExtField) NE 0 &THEN item{&N} = {&ExtTable}.{&ExtField}.
    &ENDIF 

    IF LENGTH( TRIM( item{&N} ) ) <= 1 THEN NEXT.
    
    IF {&SELF-NAME}:ADD-LAST(item{&N}) THEN
    ASSIGN      
      item-id{&N}    = STRING( ROWID( {&ExtTable} ) )
      rowid-list{&N} = rowid-list{&N} + IF NUM-ENTRIES( rowid-list{&N} ) = 0 THEN
                item-id{&N} ELSE "," + item-id{&N}.

    
    /*--- Set combo screen value if correct record ---*/
    &IF "{&IntTC}" <> "{&IntCode}" &THEN
      IF AVAILABLE {&IntTable} AND {&IntTC} = {&ExtTable}.{&ExtKey} THEN
                       {&SELF-NAME}:SCREEN-VALUE = item{&N}.
    &ELSE
      IF {&IntTC} = {&ExtTable}.{&ExtKey} THEN
                       {&SELF-NAME}:SCREEN-VALUE = item{&N}.
    &ENDIF
    
  END.

  {&SELF-NAME}:PRIVATE-DATA = rowid-list{&N}.

END.
