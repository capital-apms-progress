/*--------------------------------------------------------------------------
    File        : scpmgr2.i
    Purpose     : Assignment routine for related person select combos

    Parameters  : &IntTable - The internal table
                  &IntCode  - The PersonCode field on the internal table
                                    
    Description : This will assign the appropriate Person.PersonCode
                  to the &IntTable.&IntCode

    Author(s)   : Andrew McMillan
    Notes       : The private-data of this combo is organised as follows
                  ROWID1,ROWID2,.......,ROWIDn
                  
  ------------------------------------------------------------------------*/

&SCOPED-DEFINE N {&SEQUENCE}   /* Helps uniquely identify all variables for
                                  multiple references within the same tigger */

&IF "{&IntTable}" <> "?" &THEN
  &SCOPED-DEFINE IntTC {&IntTable}.{&IntCode}
&ELSE
  &SCOPED-DEFINE IntTC {&IntCode}
&ENDIF

&IF "{&IntTable}" <> "?" &THEN
IF AVAILABLE {&IntTable} THEN
&ENDIF

DO WITH FRAME {&FRAME-NAME}:

  DEF BUFFER tmp_Person{&N} FOR Person.
  DEF VAR item-index{&N} AS INT NO-UNDO.
  DEF VAR item-id{&N}    AS CHAR NO-UNDO.

  item-index{&N} = {&SELF-NAME}:LOOKUP( {&SELF-NAME}:SCREEN-VALUE ).
  item-id{&N}    = ENTRY( item-index{&N}, {&SELF-NAME}:PRIVATE-DATA ).
    
  FIND FIRST tmp_Person{&N} WHERE ROWID(tmp_Person{&N}) = TO-ROWID( item-id{&N} )
      NO-LOCK NO-ERROR.
  
  IF AVAILABLE( tmp_Person{&N} ) THEN DO:
&IF "{&IntTable}" <> "?" &THEN
    FIND CURRENT {&IntTable} EXCLUSIVE-LOCK.
&ENDIF
    ASSIGN {&IntTC} = tmp_Person{&N}.PersonCode NO-ERROR.
    IF ERROR-STATUS:ERROR THEN DO:
      MESSAGE ERROR-STATUS:GET-MESSAGE(1).
    END.
  END.
END.
