/* scvaltyp1.i */
/* selection combo for an ValuationType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="ValuationType"
                     &ExtKey  ="ValuationType"
                     &DV      ="ValuationType.ValuationType + ' - ' + ValuationType.Description"}
