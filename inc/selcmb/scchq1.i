/* scbnk1.i */
/* selection combo for a Cheque BankAccount */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="BankAccount"
                     &ExtKey  ="BankAccountCode"
                     &Clause  ="(BankAccount.Active AND BankAccount.ChequeAccount)"
                     &DV      ="STRING(BankAccount.BankAccountCode,'X(4)') ~
			      + ' -' + STRING(BankAccount.CompanyCode, '>>999') + '/' + STRING(BankAccount.AccountCode,'9999.99') ~
                              + ' - ' + BankAccount.AccountName" }
