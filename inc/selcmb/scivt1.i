/* scivt1.i */
/* selection combo for an Invoice Terms */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="InvoiceTerms"
                     &ExtKey  ="TermsCode"
                     &DV      ="InvoiceTerms.TermsCode + ' - ' + InvoiceTerms.Description"}
