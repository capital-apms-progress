/*--------------------------------------------------------------------------
    File        : sel-mst1.i
    Purpose     : Initialise and display routine for all selection lists

    Parameters  : &IntTable - The internal table
                  &IntCode  - The internal code for the external table
                  &ExtTable - The external table
                  &ExtKey   - The key code of the external table
                  &Clause    - (Optional) The WHERE part of the external
                              table lookup.
                              i.e FOR EACH &ExtTable WHERE &Clause NO-LOCK: ....                   
                                    
                  &Order     - (Optional) The sequencing part of the external
                              table lookup.
                              i.e FOR EACH &ExtTable WHERE &Clause NO-LOCK {&Order}: ....
                  Either of the following:
                  &ExtField - The external field to display in the list
                  &DV       - The display value to display in the list
                  
    Description : This will initialise the selection list and display
                  the appropriate external table value.

    Author(s)   : Andrew McMillan
    Notes       : The private-data of this list is organised as follows
                  ROWID1,ROWID2,.......,ROWID
                  
  ------------------------------------------------------------------------*/

&SCOPED-DEFINE N {&SEQUENCE}   /* Helps uniquely identify all variables for
                                  multiple references within the same tigger */

&IF "{&IntTable}" <> "?" &THEN
  &SCOPED-DEFINE IntTC {&IntTable}.{&IntCode}
&ELSE
  &SCOPED-DEFINE IntTC {&IntCode}
&ENDIF

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item{&N}       AS CHAR NO-UNDO.
  DEF VAR item-id{&N}    AS CHAR NO-UNDO.
  DEF VAR rowid-list{&N} AS CHAR NO-UNDO.
  DEF VAR scr-val{&N}    AS CHAR NO-UNDO INITIAL "".

  {&SELF-NAME}:LIST-ITEMS = "".

  FOR EACH {&ExtTable}
&IF DEFINED(Clause) NE 0 &THEN
            WHERE {&Clause}
&ENDIF
            NO-LOCK
&IF DEFINED(Order) NE 0 &THEN
            {&Order}
&ENDIF
    :

    /*--- Add items to the list ---*/
    
    &IF     DEFINED(DV)       NE 0 &THEN item{&N} = {&DV}.
    &ELSEIF DEFINED(ExtField) NE 0 &THEN item{&N} = {&ExtTable}.{&ExtField}.
    &ENDIF 

    IF LENGTH( TRIM( item{&N} ) ) <= 1 THEN NEXT.
    
    IF {&SELF-NAME}:ADD-LAST(item{&N}) THEN
    ASSIGN      
      item-id{&N}    = STRING( ROWID( {&ExtTable} ) )
      rowid-list{&N} = rowid-list{&N} + IF NUM-ENTRIES( rowid-list{&N} ) = 0 THEN
                item-id{&N} ELSE "," + item-id{&N}.


    /*--- Set list screen value if correct record ---*/
    IF /* rest of statement follows conditional directives */
&IF "{&IntTC}" <> "{&IntCode}" &THEN
          AVAILABLE {&IntTable} AND 
&ENDIF
      /* IF... */ LOOKUP( STRING({&ExtTable}.{&ExtKey}), {&IntTC} ) > 0 THEN
         scr-val{&N} = scr-val{&N}
                     + (IF scr-val{&N} = "" THEN "" ELSE {&SELF-NAME}:DELIMITER)
                     + item{&N}.
    
  END.

  {&SELF-NAME}:PRIVATE-DATA = rowid-list{&N}.
  {&SELF-NAME}:SCREEN-VALUE = scr-val{&N}.
END.
