/* scfyr1.i */
/* selection combo for an FinancialYear */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="FinancialYear"
                     &ExtKey  ="FinancialYearCode"
                     &DV      ="STRING( FinancialYear.FinancialYearCode, '9999') + ' - ' + FinancialYear.Description" }
