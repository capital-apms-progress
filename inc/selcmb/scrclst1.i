/* scrclst1.i */
/* selection combo for a Rent Charge Line Status */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="RentChargeLineStatus"
                     &ExtKey  ="RentChargeLineStatus"
                     &DV      ="RentChargeLineStatus.RentChargeLineStatus + ' - ' + RentChargeLineStatus.Description"}
