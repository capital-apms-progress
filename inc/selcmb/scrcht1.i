/* scrcht1.i */
/* selection combo for a RentCharge Type */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="RentChargeType"
                     &ExtKey  ="RentChargeType"
                     &DV      ="RentChargeType.RentChargeType + ' - ' + RentChargeType.Description"}
