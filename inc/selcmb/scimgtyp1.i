/* scimgtyp1.i */
/* selection combo for a Image Type */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="ImageType"
                     &ExtKey  ="ImageType"
                     &DV      ="STRING(ImageType.ImageType,'X(5)') + '- ' + ImageType.Description"}
