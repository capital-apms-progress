/* scels1.i */
/* selection combo for an EntityList */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="EntityList"
                     &ExtKey  ="ListCode"
                     &DV      ="EntityList.ListCode + ' - ' + EntityList.Description + ' (' + EntityList.ListType + ')'}
