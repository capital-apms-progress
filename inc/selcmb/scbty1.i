/* scbty1.i */
/* selection combo for a Building Type */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="BuildingType"
                     &ExtKey  ="BuildingType"
                     &DV      ="BuildingType.BuildingType + ' - ' + BuildingType.Description"}
