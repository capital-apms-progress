/* scpbdg1.i */
/* selection combo for an ProjectBudget */
/* note: 3rd parameter used as project code for WHERE clause */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="ProjectBudget"
                     &ExtKey  ="AccountCode"
                     &Clause  ="ProjectBudget.ProjectCode = {3}"
                     &DV="STRING( ProjectBudget.AccountCode, '9999.99' ) + ' - ' + ProjectBudget.Description }
