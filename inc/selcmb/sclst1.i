/* sclst1.i */
/* selection combo for a Lease Status */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="LeaseStatus"
                     &ExtKey  ="LeaseStatus"
                     &DV      ="LeaseStatus.LeaseStatus + ' - ' + LeaseStatus.Description"}
