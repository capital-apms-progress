/* scaty1.i */
/* selection combo for an AreaType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="AreaType"
                     &ExtKey  ="AreaType"
                     &DV      ="AreaType.AreaType + ' - ' + AreaType.Description"}
