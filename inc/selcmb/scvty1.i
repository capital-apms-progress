/* scvty1.i */
/* selection combo for an VariationType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="VariationType"
                     &ExtKey  ="VariationType"
                     &DV      ="VariationType.VariationType + ' - ' + VariationType.Description"}
