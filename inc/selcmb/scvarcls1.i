/* scvarcls1.i */
/* selection combo for an VarianceClassification */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="VarianceClassification"
                     &ExtKey  ="VarianceClassification"
                     &DV      ="VarianceClassification.VarianceClassification + ' - ' + VarianceClassification.Description"}
