/*--------------------------------------------------------------------------
    File        : sel-mst2.i
    Purpose     : Assignment routine for all select selection lists

    Parameters  : &IntTable - The internal table
                  &IntCode  - The internal code for the external table
                  &ExtTable - The external table
                  &ExtKey   - The key code of the external table
                                    
    Description : This will assign the appropriate external table key
                  to the &IntTable.&IntCode

    Author(s)   : Andrew McMillan
    Notes       : The private-data of this list is organised as follows
                  ROWID1,ROWID2,.......,ROWIDn
                  
  ------------------------------------------------------------------------*/

&SCOPED-DEFINE N {&SEQUENCE}   /* Helps uniquely identify all variables for
                                  multiple references within the same tigger */

&IF "{&IntTable}" <> "?" &THEN
  &SCOPED-DEFINE IntTC {&IntTable}.{&IntCode}
&ELSE
  &SCOPED-DEFINE IntTC {&IntCode}
&ENDIF

&IF "{&IntTable}" <> "?" &THEN
IF AVAILABLE {&IntTable} THEN
&ENDIF

DO WITH FRAME {&FRAME-NAME}:

  DEF BUFFER tmp_{&ExtTable}{&N} FOR {&ExtTable}.
  DEF VAR no-items{&N}   AS INT NO-UNDO.
  DEF VAR item-index{&N} AS INT NO-UNDO.
  DEF VAR item-id{&N}    AS CHAR NO-UNDO.
  DEF VAR key-list{&N}   AS CHAR NO-UNDO INITIAL "".

  no-items{&N} = NUM-ENTRIES( {&SELF-NAME}:LIST-ITEMS, {&SELF-NAME}:DELIMITER ).
  DO item-index{&N} = 1 TO no-items{&N}:
    IF NOT {&SELF-NAME}:IS-SELECTED(item-index{&N}) THEN NEXT.
    item-id{&N} = ENTRY( item-index{&N}, {&SELF-NAME}:PRIVATE-DATA ).
    
    FIND FIRST tmp_{&ExtTable}{&N} WHERE ROWID(tmp_{&ExtTable}{&N}) = TO-ROWID( item-id{&N} )
        NO-LOCK NO-ERROR.
    key-list{&N} = key-list{&N} + (IF key-list{&N} <> "" THEN "," ELSE "")
                 + STRING(tmp_{&ExtTable}{&N}.{&ExtKey}).
  END.  

  IF AVAILABLE( tmp_{&ExtTable}{&N} ) THEN DO:
&IF "{&IntTable}" <> "?" &THEN
    FIND CURRENT {&IntTable} EXCLUSIVE-LOCK.
&ENDIF
    ASSIGN {&IntTC} = key-list{&N} NO-ERROR.
    IF ERROR-STATUS:ERROR THEN DO:
      MESSAGE ERROR-STATUS:GET-MESSAGE(1).
    END.
  END.
END.
