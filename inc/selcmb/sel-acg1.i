/* sel-acg1.i */
/* selection list for an AccountGroup */

/* The clause is non-selective, it just controls the record order */
{inc/selcmb/sel-mst1.i &IntTable="{1}"
                       &IntCode ="{2}"
                       &ExtTable="AccountGroup"
                       &ExtKey  ="AccountGroupCode"
                       &Order   ="BY AccountGroup.Sequence BY AccountGroup.AccountGroupCode"
                       &DV      ="STRING( AccountGroup.AccountGroupCode, 'X(6)' ) + ' - ' + AccountGroup.Name"}
