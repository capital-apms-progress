/* scinsp1.i */
/* selection combo for a Inspector */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="Inspector"
                     &ExtKey  ="InspectorCode"
                     &Clause  ="TRUE NO-LOCK, FIRST Person WHERE Person.PersonCode = Inspector.PersonCode"
                     &DV      ="STRING(Inspector.InspectorCode) + ' - ' + Person.FirstName + ' ' + Person.LastName"}
