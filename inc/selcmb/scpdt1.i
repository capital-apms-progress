/* scpdt1.i */
/* selection combo for a PostalType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="PostalType"
                     &ExtKey  ="PostalType"
                     &DV      ="PostalType.PostalType + ' - ' + PostalType.Description"}
