/* scbldsys2.i */
/* selection combo for a Building System Type */

{inc/selcmb/scmst2.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="BuildingSystemType"
                     &ExtKey  ="BuildingSystemType"}
