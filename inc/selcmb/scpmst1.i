/*--------------------------------------------------------------------------
    File        : scpmgr1.i
    Purpose     : Initialise and display routine for selecting people
                  referenced through a table (e.g property manager)

    Parameters  : &IntTable - The internal table
                  &IntCode  - The internal code for the external table
                  &ExtTable - The external table which references the person
                  &ExtKey   - The key code of the external table
                  &ExtField - The external field which references the person

      Optionally:-
                  &DV       - The display value to display in the combo
                  &Clause   - The WHERE clause for the &ExtTable
                  
    Description : This will initialise the select combo and display
                  the appropriate external table value.

    Author(s)   : Andrew McMillan
    Notes       : The private-data of this combo is organised as follows
                  ROWID1,ROWID2,.......,ROWID
                  
  ------------------------------------------------------------------------*/

&SCOPED-DEFINE N {&SEQUENCE}   /* Helps uniquely identify all variables for
                                  multiple references within the same tigger */

&IF "{&IntTable}" <> "?" &THEN
  &SCOPED-DEFINE IntTC {&IntTable}.{&IntCode}
&ELSE
  &SCOPED-DEFINE IntTC {&IntCode}
&ENDIF

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item{&N}       AS CHAR NO-UNDO.
  DEF VAR item-id{&N}    AS CHAR NO-UNDO.
  DEF VAR rowid-list{&N} AS CHAR NO-UNDO.

  {&SELF-NAME}:LIST-ITEMS = "".
  {&SELF-NAME}:DELIMITER = CHR(7).

  &IF DEFINED(Clause) NE 0 &THEN
    FOR EACH {&ExtTable} WHERE {&Clause} NO-LOCK:
  &ELSE
    FOR EACH {&ExtTable} NO-LOCK:
  &ENDIF

    /*--- Find the related Person record ---*/
    FIND Person WHERE Person.PersonCode = {&ExtTable}.{&ExtField} NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(Person) THEN NEXT.
    item-id{&N} = STRING( ROWID(Person) ).
    
    /*--- Add items to the list ---*/
    &IF DEFINED(DV) NE 0 &THEN item{&N} = {&DV}.
    &ELSE                      item{&N} = Person.FirstName + " " + Person.LastName .
    &ENDIF 

    IF LENGTH( TRIM( item{&N} ) ) <= 1 THEN NEXT.
    IF LOOKUP( item-id{&N}, rowid-list{&N} ) > 0 THEN NEXT. /* already in */
    
    IF {&SELF-NAME}:ADD-LAST(item{&N}) THEN
      rowid-list{&N} = rowid-list{&N}
                     + (IF rowid-list{&N} = "" THEN "" ELSE ",")
                     + item-id{&N}.

    
    /*--- Set combo screen value if correct record ---*/
    &IF "{&IntTC}" <> "{&IntCode}" &THEN
      IF AVAILABLE {&IntTable} AND {&IntTC} = Person.PersonCode THEN
                       {&SELF-NAME}:SCREEN-VALUE = item{&N}.
    &ELSE
      IF {&IntTC} = Person.PersonCode THEN
                       {&SELF-NAME}:SCREEN-VALUE = item{&N}.
    &ENDIF
    
  END.

  {&SELF-NAME}:PRIVATE-DATA = rowid-list{&N}.

END.
