/* sccls1.i */
/* selection combo for an ConsolidationList */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="ConsolidationList"
                     &ExtKey  ="Name"
                     &DV      ="ConsolidationList.Name + ' - ' + ConsolidationList.Description"}
