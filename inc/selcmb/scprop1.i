/* scprop1.i */
/* selection combo for a Property */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="Property"
                     &ExtKey  ="PropertyCode"
                     &Clause  ="Property.Region = {3}"
                     &DV      ="STRING(Property.PropertyCode, '99999') + ' - ' + Property.Name "}
