/* scpxct1.i */
/* selection combo for an ProjectExpenseCategory */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="ProjectExpenseCategory"
                     &ExtKey  ="ProjectExpenseCategory"
                     &DV="STRING( ProjectExpenseCategory.ProjectExpenseCategory, 'X(4)' ) + ' - ' + ProjectExpenseCategory.Description }
