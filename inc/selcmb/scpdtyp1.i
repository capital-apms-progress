/* scpdtyp1.i */
/* selection combo for an PersonDetailType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="PersonDetailType"
                     &ExtKey  ="PersonDetailType"
                     &DV      ="PersonDetailType.PersonDetailType + ' - ' + PersonDetailType.Description"}
