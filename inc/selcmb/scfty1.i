/* scfty1.i */
/* selection combo for an FrequencyTypes */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="Frequencytype"
                     &ExtKey  ="FrequencyCode"
                     &DV      ="STRING( Frequencytype.FrequencyCode, 'X(4)' ) + ' - ' + FrequencyType.Description"}
