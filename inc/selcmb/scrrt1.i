/* scrrt1.i */
/* selection combo for a Rent Review Type */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="ReviewType"
                     &ExtKey  ="ReviewType"
                     &DV      ="ReviewType.ReviewType + ' - ' + ReviewType.Description"}
