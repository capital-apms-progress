/* scfyr2.i */
/* selection combo for an FinancialYear */

{inc/selcmb/scmst2.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="FinancialYear"
                     &ExtKey  ="FinancialYearCode"}
