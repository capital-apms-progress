/* scpjt1.i */
/* selection combo for a ProjectType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="ProjectType"
                     &ExtKey  ="ProjectType"
                     &DV      ="STRING( ProjectType.ProjectType, 'X(4)' ) + ' - ' + ProjectType.Description"}
