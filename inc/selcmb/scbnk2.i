/* scbnk2.i */
/* selection combo for an BankAccount */

{inc/selcmb/scmst2.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="BankAccount"
                     &ExtKey  ="BankAccountCode"}
