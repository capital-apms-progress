/* scsst1.i */
/* selection combo for an ScenarioStatus */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="ScenarioStatus"
                     &ExtKey  ="ScenarioStatus"
                     &DV      ="STRING( ScenarioStatus.Description, 'X(4)' ) + ' - ' + ScenarioStatus.Description"}
