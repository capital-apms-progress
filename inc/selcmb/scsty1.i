/* scsty1.i */
/* selection combo for an ServiceType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="ServiceType"
                     &ExtKey  ="ServiceType"
                     &DV      ="ServiceType.ServiceType + ' - ' + ServiceType.Description"}
