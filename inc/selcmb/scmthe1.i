/* scmthe1.i */
/* selection combo for a Month Ends */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="Month"
                     &ExtKey  ="MonthCode"
                     &DV      ="STRING(Month.EndDate, '99/99/9999')"}
