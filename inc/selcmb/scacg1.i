/* scacg1.i */
/* selection combo for an AccountGroups */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="AccountGroup"
                     &ExtKey  ="AccountGroupCode"
                     &DV      ="STRING( AccountGroup.AccountGroupCode, 'X(6)' ) + ' - ' + AccountGroup.Name"}
