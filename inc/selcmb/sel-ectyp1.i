/* scectyp1.i */
/* selection list for an EntityContactType */

{inc/selcmb/sel-mst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="EntityContactType"
                     &ExtKey  ="EntityContactType"
                     &DV      ="EntityContactType.EntityContactType + ' - ' + EntityContactType.Description"}
