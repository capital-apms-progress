/* scrrst1.i */
/* selection combo for a Rent Review Status */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="ReviewStatus"
                     &ExtKey  ="ReviewStatus"
                     &DV      ="ReviewStatus.ReviewStatus + ' - ' + ReviewStatus.Description"}
