/* sclty1.i */
/* selection combo for a Lease Type */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="LeaseType"
                     &ExtKey  ="LeaseType"
                     &DV      ="LeaseType.LeaseType + ' - ' + LeaseType.Description"}
