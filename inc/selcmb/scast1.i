/* scast1.i */
/* selection combo for an AreaStatus */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="AreaStatus"
                     &ExtKey  ="AreaStatus"
                     &DV      ="AreaStatus.AreaStatus + ' - ' + AreaStatus.Description"}
