/* scety1.i */
/* selection combo for an EntityType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="EntityType"
                     &ExtKey  ="EntityType"
                     &DV      ="EntityType.EntityType + ' - ' + EntityType.Description"}
