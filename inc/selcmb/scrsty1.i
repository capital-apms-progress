/* scpsty1.i */
/* selection combo for a Payment (Receipt) Style */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="PaymentStyle"
                     &ExtKey  ="PaymentStyle"
                     &Clause  ="PaymentStyle.Receipts"
                     &DV      ="STRING(PaymentStyle.PaymentStyle, 'X(5)') + '- ' + PaymentStyle.Description "}
