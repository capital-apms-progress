/* scmths1.i */
/* selection combo for a Month Starts */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="Month"
                     &ExtKey  ="MonthCode"
                     &DV      ="STRING(Month.StartDate, '99/99/9999' )"}
