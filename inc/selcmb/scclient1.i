/* scclient1.i */
/* selection combo for a Client */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="Client"
                     &ExtKey  ="ClientCode"
                     &DV      ="STRING( Client.ClientCode, 'X(6)' ) + ' - ' + Client.Name"}
