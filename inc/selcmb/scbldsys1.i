/* scbldsys1.i */
/* selection combo for a Building System Type */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="BuildingSystemType"
                     &ExtKey  ="BuildingSystemType"
                     &DV      ="STRING(BuildingSystemType.BuildingSystemType) + ' - ' + BuildingSystemType.Description"}
