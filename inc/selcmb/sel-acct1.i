/* sel-acct1.i */
/* selection list for an Account selection */

{inc/selcmb/sel-mst1.i &IntTable="{1}"
                       &IntCode ="{2}"
                       &ExtTable="ChartOfAccount"
                       &ExtKey  ="AccountCode"
                       &Clause  ="ChartOfAccount.AccountGroupCode = {3}"
                       &DV      ="STRING( ChartOfAccount.AccountCode, '9999.99' ) + ' - ' + ChartOfAccount.Name"}
