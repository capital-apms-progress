/* sccfct1.i */
/* selection combo for an CFChangeType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="CFChangeType"
                     &ExtKey  ="CFChangeType"
                     &DV="STRING( CFChangeType.CFChangeType, 'X(4)' ) + ' - ' + CFChangeType.Description }
