/* scectyp1.i */
/* selection combo for an EntityContactType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="EntityContactType"
                     &ExtKey  ="EntityContactType"
                     &DV      ="EntityContactType.EntityContactType + ' - ' + EntityContactType.Description"}
