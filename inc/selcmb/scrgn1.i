/* scrgn1.i */
/* selection combo for a Region */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="Region"
                     &ExtKey  ="Region"
                     &DV      ="STRING( Region.Region, 'X(5)' ) + '- ' + Region.Name"}
