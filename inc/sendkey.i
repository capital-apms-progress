DEF INPUT PARAMETER key-name AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER key-value AS CHAR NO-UNDO INITIAL ?.

  RUN get-attribute( 'Key-Name':U ).
  IF RETURN-VALUE = key-name AND NOT(key-name BEGINS "Parent") THEN DO:
    RUN get-attribute( 'Key-Value':U ).
    key-value =  RETURN-VALUE.
  END.
  IF key-value = ? THEN DO:
    /* Dispatch standard ADM method.                             */
    RUN normal-send-key( key-name, OUTPUT key-value ) NO-ERROR.
  END.
