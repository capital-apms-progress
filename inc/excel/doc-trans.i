&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*--------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 13.55
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/*
 * inc/excel/doc-trans.i
 *
 * Include for spreadsheet export of browser records
 *
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-headings Include 
PROCEDURE assign-headings :
/*------------------------------------------------------------------------------
  Purpose:  Assign the column headings of the spreadsheet
------------------------------------------------------------------------------*/

ASSIGN
  chWorkSheet:Cells(1,1):Value = "Closed"
  chWorkSheet:Cells(1,2):Value = "Date"
  chWorkSheet:Cells(1,3):Value = "Reference"
  chWorkSheet:Cells(1,4):Value = "Amount"
  chWorkSheet:Cells(1,5):Value = "Description"
  chWorkSheet:Cells(1,6):Value = "Flag"
  chWorkSheet:Cells(1,7):Value = "Flagged By"
  chWorkSheet:Cells(1,8):Value = "EntityCode"
  chWorkSheet:Cells(1,9):Value = "EntityName"
  chWorkSheet:Columns("A"):ColumnWidth = 5
  chWorkSheet:Columns("B"):ColumnWidth = 10
  chWorkSheet:Columns("C"):ColumnWidth = 12
  chWorkSheet:Columns("D"):ColumnWidth = 14
  chWorkSheet:Columns("E"):ColumnWidth = 50
  chWorkSheet:Columns("F"):ColumnWidth = 4
  chWorkSheet:Columns("G"):ColumnWidth = 14
  chWorkSheet:Columns("H"):ColumnWidth = 14
  chWorkSheet:Columns("I"):ColumnWidth = 50
  chWorkSheet:Range("B:B"):NumberFormat = /* (IF SESSION:DATE-FORMAT = "dmy" THEN */ "d/mm/yy" /* ELSE "m/dd/yy") */
  chWorkSheet:Range("D:D"):NumberFormat = "#,##0.00_);[Red](#,##0.00)"
  chWorkSheet:Range("A:A"):HorizontalAlignment = 3  /* Center */
  chWorkSheet:Range("B:B"):HorizontalAlignment = 3  /* Center */
  chWorkSheet:Range("C:C"):HorizontalAlignment = 2  /* Left   */
  chWorkSheet:Range("D:D"):HorizontalAlignment = 4  /* Right  */
  chWorkSheet:Range("H:H"):HorizontalAlignment = 2  /* Left   */
  chWorkSheet:Range("I:I"):HorizontalAlignment = 2  /* Left   */
  chWorkSheet:Range("1:1"):Font:Bold = Yes
  NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-row Include 
PROCEDURE assign-row :
/*------------------------------------------------------------------------------
  Purpose:  Assign a row of the spreadsheet from queried records
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER r AS INT NO-UNDO.

ASSIGN
  chWorkSheet:Cells(r,1):Value = AcctTran.ClosedState
  chWorkSheet:Cells(r,2):Value = excel-date(AcctTran.Date, 0.0)
  chWorkSheet:Cells(r,3):Value = (IF AcctTran.Reference = '' THEN Document.Reference ELSE AcctTran.Reference)
  chWorkSheet:Cells(r,4):Value = AcctTran.Amount
  chWorkSheet:Cells(r,5):Value = (IF AcctTran.Description = '' THEN Document.Description ELSE AcctTran.Description)
  chWorkSheet:Cells(r,6):Value = AcctTran.FlagAttention
  chWorkSheet:Cells(r,7):Value = (IF AcctTran.FlaggedBy > 0 THEN Person.FirstName ELSE '')
  NO-ERROR.

    CASE AcctTran.EntityType:
        WHEN 'L' THEN DO:
            FIND FIRST Company WHERE Company.CompanyCode = AcctTran.EntityCode NO-LOCK.
            ASSIGN
                chWorkSheet:Cells(r,8):VALUE = AcctTran.EntityType + STRING( AcctTran.EntityCode, "99999" )
                chWorkSheet:Cells(r,9):VALUE = STRING( Company.LegalName, "X(50)" )
                NO-ERROR.
        END.
        WHEN 'P' THEN DO:
            FIND FIRST Property WHERE Property.PropertyCode = AcctTran.EntityCode NO-LOCK.
            ASSIGN
                chWorkSheet:Cells(r,8):VALUE = AcctTran.EntityType + STRING( AcctTran.EntityCode, "99999" )
                chWorkSheet:Cells(r,9):VALUE = STRING( Property.Name, "X(50)" )
                NO-ERROR.
        END.
        WHEN 'J' THEN DO:
            FIND FIRST Project WHERE Project.ProjectCode = AcctTran.EntityCode NO-LOCK.
            ASSIGN
                chWorkSheet:Cells(r,8):VALUE = AcctTran.EntityType + STRING( AcctTran.EntityCode, "99999" )
                chWorkSheet:Cells(r,9):VALUE = STRING( Project.Name, "X(50)" )
                NO-ERROR.
        END.
        WHEN 'T' THEN DO:
            FIND FIRST Tenant WHERE Tenant.TenantCode = AcctTran.EntityCode NO-LOCK.
            ASSIGN
                chWorkSheet:Cells(r,8):VALUE = AcctTran.EntityType + STRING( AcctTran.EntityCode, "99999" )
                chWorkSheet:Cells(r,9):VALUE = STRING( Tenant.Name, "X(50)" )
                NO-ERROR.
        END.
        WHEN 'C' THEN DO:
            FIND FIRST Creditor WHERE Creditor.CreditorCode = AcctTran.EntityCode NO-LOCK.
            ASSIGN
                chWorkSheet:Cells(r,8):VALUE = AcctTran.EntityType + STRING( AcctTran.EntityCode, "99999" )
                chWorkSheet:Cells(r,9):VALUE = STRING( Creditor.Name, "X(50)" )
                NO-ERROR.
        END.
    END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

