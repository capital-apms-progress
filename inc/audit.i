/* Include for Auditing */

DEF VAR now-date AS DATE NO-UNDO.
DEF VAR now-time AS INTEGER NO-UNDO.
DEF VAR now-user AS CHAR NO-UNDO.
RUN sec/audit.p ( "WRITE", NEW {1}, "{1}",
                  OUTPUT now-date, OUTPUT now-time, OUTPUT now-user).
ASSIGN
  {1}.LastModifiedDate = now-date
  {1}.LastModifiedTime = now-time
  {1}.LastModifiedUser = now-user
.
