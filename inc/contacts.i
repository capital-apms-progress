&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

DEF VAR ctc-office-country-code LIKE PhoneDetail.cCountryCode NO-UNDO.
DEF VAR ctc-office-std-code     LIKE PhoneDetail.cSTDCode     NO-UNDO.
DEF VAR ctc-office-isd-prefix   AS CHAR NO-UNDO INITIAL "00".
DEF VAR ctc-office-country-name AS CHAR NO-UNDO INITIAL "".

{inc/ofc-this.i}

FIND OfficeSettings OF Office WHERE OfficeSettings.SetName = "Country-Code" NO-LOCK NO-ERROR.
IF AVAILABLE OfficeSettings THEN ctc-office-country-code = OfficeSettings.SetValue.

FIND OfficeSettings OF Office WHERE OfficeSettings.SetName = "STD-Code" NO-LOCK NO-ERROR.
IF AVAILABLE OfficeSettings THEN ctc-office-std-code = OfficeSettings.SetValue.

FIND OfficeSettings OF Office WHERE OfficeSettings.SetName = "ISD-Prefix" NO-LOCK NO-ERROR.
IF AVAILABLE(OfficeSettings) THEN ctc-office-isd-prefix = OfficeSettings.SetValue.

FIND OfficeSettings OF Office WHERE OfficeSettings.SetName = "Country" NO-LOCK NO-ERROR.
IF AVAILABLE(OfficeSettings) THEN ctc-office-country-name = OfficeSettings.SetValue.

{inc/ofc-set.i "Address-Priority" "ctc-address-priority-list"}
{inc/ofc-set.i "Phone-Priority" "ctc-phone-priority-list"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-address Include 
FUNCTION get-address RETURNS CHARACTER
  ( INPUT person-code AS INT, INPUT postal-type AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-best-address Include 
FUNCTION get-best-address RETURNS CHARACTER
  ( INPUT person-code AS INT, INPUT type-list AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-best-phone Include 
FUNCTION get-best-phone RETURNS CHARACTER
  ( INPUT person-code AS INT, INPUT type-list AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-fullname Include 
FUNCTION get-fullname RETURNS CHARACTER
  ( INPUT person-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-phone-no Include 
FUNCTION get-phone-no RETURNS CHARACTER
  ( INPUT person-code AS INT, INPUT phone-type AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD non-null Include 
FUNCTION non-null RETURNS CHARACTER
  ( INPUT s AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = .1
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-address Include 
FUNCTION get-address RETURNS CHARACTER
  ( INPUT person-code AS INT, INPUT postal-type AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Return a formatted address string for the address of the
            specified type.
------------------------------------------------------------------------------*/
DEF VAR address-line AS CHAR NO-UNDO INITIAL "".
DEF VAR address AS CHAR NO-UNDO INITIAL "".

DEF BUFFER PD FOR PostalDetail.
  FIND PD WHERE PD.PersonCode = person-code
            AND PD.PostalType = postal-type NO-LOCK NO-ERROR.
  IF NOT AVAILABLE PD THEN RETURN ?.

  address-line = non-null(PD.State).
  address-line = TRIM( address-line + (IF PD.Zip = ? OR PD.ZIp = "" THEN "" ELSE " " + PD.Zip)).
  IF address-line <> "" THEN
    address-line = TRIM( non-null( PD.City ) + ", " + address-line, ", ") .
  ELSE
    address-line = non-null( PD.City ).

  address = address + non-null(  TRIM(PD.Address) ).
  address = address + non-null(  "~n" + address-line ).

  address-line = "".
  IF PD.Country <> ctc-office-country-name THEN address-line = PD.Country.

  address = address + non-null(  "~n" + TRIM(address-line) ).

  /* replace any bizarro stuff we got from doing a dump and reload */
  address = TRIM(REPLACE(address, "~r", "")).
  DO WHILE INDEX( address, ",~n") > 0:
    address = REPLACE( address, ",~n", "~n").
  END.
  DO WHILE INDEX( address, "~n ") > 0:
    address = REPLACE( address, "~n ", "~n").
  END.
  DO WHILE INDEX( address, " ~n") > 0:
    address = REPLACE( address, " ~n", "~n").
  END.
  DO WHILE INDEX( address, "~n~n") > 0:
    address = REPLACE( address, "~n~n", "~n").
  END.

  RETURN address.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-best-address Include 
FUNCTION get-best-address RETURNS CHARACTER
  ( INPUT person-code AS INT, INPUT type-list AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR n AS INT NO-UNDO.
DEF VAR i AS INT NO-UNDO.

  IF type-list = ? OR type-list = "" THEN type-list = ctc-address-priority-list.
  n = NUM-ENTRIES( type-list ).
  DO i = 1 TO n:
    FIND PostalDetail WHERE PostalDetail.PersonCode = person-code
                       AND PostalDetail.PostalType = ENTRY(i,type-list)
                       NO-LOCK NO-ERROR.
    IF AVAILABLE(PostalDetail) THEN
      RETURN get-address( person-code, PostalDetail.PostalType ).
  END.

  FIND FIRST PostalDetail WHERE PostalDetail.PersonCode = person-code NO-LOCK NO-ERROR.
  IF AVAILABLE(PostalDetail) THEN
    RETURN get-address( person-code, PostalDetail.PostalType ).

  RETURN "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-best-phone Include 
FUNCTION get-best-phone RETURNS CHARACTER
  ( INPUT person-code AS INT, INPUT type-list AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR n AS INT NO-UNDO.
DEF VAR i AS INT NO-UNDO.

  IF type-list = ? OR type-list = "" THEN type-list = ctc-phone-priority-list.
  n = NUM-ENTRIES( type-list ).
  DO i = 1 TO n:
    FIND PhoneDetail WHERE PhoneDetail.PersonCode = person-code
                       AND PhoneDetail.PhoneType = ENTRY(i,type-list)
                       NO-LOCK NO-ERROR.
    IF AVAILABLE(PhoneDetail) THEN
      RETURN get-phone-no( person-code, PhoneDetail.PhoneType ).
  END.

  FIND FIRST PhoneDetail WHERE PhoneDetail.PersonCode = person-code NO-LOCK NO-ERROR.
  IF AVAILABLE(PhoneDetail) THEN
    RETURN get-phone-no( person-code, PhoneDetail.PhoneType ).

  RETURN "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-fullname Include 
FUNCTION get-fullname RETURNS CHARACTER
  ( INPUT person-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  IF AVAILABLE(Person) AND Person.PersonCode = person-code THEN
    RETURN TRIM( non-null(Person.FirstName) + ' ' + non-null(Person.LastName) ).

DEF BUFFER AltPerson FOR Person.
  FIND AltPerson WHERE AltPerson.PersonCode = person-code NO-LOCK NO-ERROR.
  IF AVAILABLE(AltPerson) THEN
    RETURN TRIM( non-null(AltPerson.FirstName) + ' ' + non-null(AltPerson.LastName) ).

  RETURN ?. /* no such person */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-phone-no Include 
FUNCTION get-phone-no RETURNS CHARACTER
  ( INPUT person-code AS INT, INPUT phone-type AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  return a formatted phone number of this type for this person
------------------------------------------------------------------------------*/
DEF VAR country-part AS CHAR NO-UNDO INITIAL "".
DEF VAR std-part     AS CHAR NO-UNDO INITIAL "".
DEF VAR c-country    AS CHAR NO-UNDO.
DEF VAR c-std        AS CHAR NO-UNDO.
DEF VAR phone-no     AS CHAR NO-UNDO.

  /* might avoid unnecessary read */
  IF AVAILABLE(PhoneDetail) AND  PhoneDetail.PersonCode = person-code
                            AND PhoneDetail.PhoneType = phone-type THEN ASSIGN
    c-country = non-null(PhoneDetail.cCountryCode)
    c-std     = non-null(PhoneDetail.cSTDCode)
    phone-no  = non-null(PhoneDetail.Number).
  ELSE DO:
    DEF BUFFER AltPD FOR PhoneDetail.
    FIND AltPD WHERE AltPD.PersonCode = person-code
                 AND AltPD.PhoneType = phone-type NO-LOCK NO-ERROR.
    IF AVAILABLE(AltPD) THEN ASSIGN
      c-country = non-null(AltPD.cCountryCode)
      c-std     = non-null(AltPD.cSTDCode)
      phone-no  = non-null(AltPD.Number).
    ELSE
      RETURN ?.
  END.

  IF c-country <> "" AND c-country <> ctc-office-country-code THEN
    country-part = "+" + c-country + " ".

  IF c-country <> ctc-office-country-code OR c-std <> ctc-office-std-code THEN
    std-part = "(" + c-std + ") ".

  RETURN country-part + std-part + phone-no .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION non-null Include 
FUNCTION non-null RETURNS CHARACTER
  ( INPUT s AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Return a null-string if the input is ?
------------------------------------------------------------------------------*/
  IF s <> ? THEN RETURN s.
  RETURN "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


