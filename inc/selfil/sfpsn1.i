/* sfpsn1.i */
/* selection fill-in for a person */

{inc/selfil/sfmst1.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="Contact"
                     &ExtKey  ="PersonCode"
                     &DV="Person.FirstName   + ' ' +
                          Person.LastName"
                     &DVFind="FIND Person WHERE Person.PersonCode ="
                     }
