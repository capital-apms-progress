/*--------------------------------------------------------------------------
    File        : sfmst3.i
    Purpose     : Update the record value from the select fill-ins

    Parameters  : &IntTable - The internal table
                  &IntCode  - The internal code for the external table
                  &ExtTable - The external table
                  &ExtKey   - The key code of the external table
                  
    Description : This will initialise the select fill-in and display
                  the appropriate external table value.

    Author(s)   : Tyrone McAuley
    Notes       : The private-data of this fill-in stores the value of the
                  ROWID for the current external table record that the
                  internal code points to
                  
  ------------------------------------------------------------------------*/

&SCOPED-DEFINE N {&SEQUENCE}   /* Helps uniquely identify all variables for
                                  multiple references within the same tigger */

&IF "{&IntTable}" <> "?" &THEN
  &SCOPED-DEFINE IntTC {&IntTable}.{&IntCode}
&ELSE
  &SCOPED-DEFINE IntTC {&IntCode}
&ENDIF

&IF "{&IntTable}" <> "?" &THEN
IF AVAILABLE {&IntTable} THEN
&ENDIF

DO WITH FRAME {&FRAME-NAME}:
  
  DEF BUFFER tmp_{&ExtTable} FOR {&ExtTable}.
  DEF VAR    curr-id{&N} AS ROWID NO-UNDO.
  curr-id{&N} = TO-ROWID( {&SELF-NAME}:PRIVATE-DATA ).

  FIND tmp_{&ExtTable} WHERE ROWID(tmp_{&ExtTable}) = curr-id{&N}
    NO-LOCK NO-ERROR.

 IF AVAILABLE tmp_{&ExtTable} THEN
   ASSIGN {&IntTC} = tmp_{&ExtTable}.{&ExtKey}.
       
END.
