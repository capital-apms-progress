/* sfmtr3.i */
/* selection fill-in for Supply Meters */

{inc/selfil/sfmst3.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="SupplyMeter"
                     &ExtKey  ="MeterCode"}
