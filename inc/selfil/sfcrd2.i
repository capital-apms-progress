/* sfcrd2.i */
/* selection fill-in for a Creditor */

{inc/selfil/sfmst2.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="Creditor"
                     &ExtKey  ="CreditorCode"
                     &ExtField="Name"}
