/* sfinv1.i */
/* selection fill-in for an Invoice */

{inc/selfil/sfmst1.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="Invoice"
                     &ExtKey  ="InvoiceNo"
                     &ExtField="ToDetail"}
