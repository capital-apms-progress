/* sfmtr1.i */
/* selection fill-in for Supply Meters */

{inc/selfil/sfmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="SupplyMeter"
                     &ExtKey  ="MeterCode"
                     &ExtField="Description"}
