/* sfcmp1.i */
/* selection fill-in for a Company */

{inc/selfil/sfmst1.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="Company"
                     &ExtKey  ="CompanyCode"
                     &ExtField="LegalName"}
