/* sfscn2.i */
/* selection fill-in for a Scenario */

{inc/selfil/sfmst2.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="Scenario"
                     &ExtKey  ="ScenarioCode"
                     &ExtField="Name"}
