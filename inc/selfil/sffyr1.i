/* sffyr1.i */
/* selection fill-in for a FinancialYear */

{inc/selfil/sfmst1.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="FinancialYear"
                     &ExtKey  ="FinancialYearCode"
                     &ExtField="Description"}
