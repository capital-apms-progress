/* sfinv2.i */
/* selection fill-in for an Invoice */

{inc/selfil/sfmst2.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="Invoice"
                     &ExtKey  ="InvoiceNo"
                     &ExtField="ToDetail"}
