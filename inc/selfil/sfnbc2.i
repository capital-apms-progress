/* sfnbc2.i */
/* selection fill-in for a New Batch */

{inc/selfil/sfmst2.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable ="NewBatch"
                     &ExtKey   ="BatchCode"
                     &ExtField ="Description"}
