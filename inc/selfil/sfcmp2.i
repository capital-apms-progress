/* sfcmp2.i */
/* selection fill-in for a Company */

{inc/selfil/sfmst2.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="Company"
                     &ExtKey  ="CompanyCode"
                     &ExtField="LegalName"}
