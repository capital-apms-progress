/* sftnt1.i */
/* selection fill-in for a Tenant */

{inc/selfil/sfmst1.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="Tenant"
                     &ExtKey  ="TenantCode"
                     &ExtField="Name"}
