/* sfapp3.i */
/* selection fill-in for an Approver */

{inc/selfil/sfmst3.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="Approver"
                     &ExtKey  ="ApproverCode"}
