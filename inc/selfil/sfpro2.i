/* sfpro2.i */
/* selection fill-in for a Property */

{inc/selfil/sfmst2.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="Property"
                     &ExtKey  ="PropertyCode"
                     &ExtField="Name"}
