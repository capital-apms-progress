/*--------------------------------------------------------------------------
    File        : sfmst1.i
    Purpose     : Initialise and display routine for all select fill-ins

    Parameters  : &IntTable - The internal table
                  &IntCode  - The internal code for the external table
                  &ExtTable - The external table
                  &ExtKey   - The key code of the external table
                                    
                  Either of the following:
                  &ExtField - The external field to display in the fill in
                  &DV       - The display value to display in the fill in
                  &DVFind   - The first part (FIND x WHERE x.x =) of the
                              find statement to find the table containing
                              the DV value.
                  
    Description : This will initialise the select fill-in and display
                  the appropriate external table value.

    Author(s)   : Tyrone McAuley
    Notes       : The private-data of this fill-in stores the value of the
                  ROWID for the current external table record that the
                  internal code points to
                  
  ------------------------------------------------------------------------*/

&SCOPED-DEFINE N {&SEQUENCE}   /* Helps uniquely identify all variables for
                                  multiple references within the same tigger */

/* Initialise the ROWID and clear the screen-value */

  {&SELF-NAME}:PRIVATE-DATA = "".
  {&SELF-NAME}:SCREEN-VALUE = "".


/* --- Try to find the external table  ---
       using the internal code             */

&IF "{&IntTable}" <> "?" &THEN
  &SCOPED-DEFINE IntTC {&IntTable}.{&IntCode}
&ELSE
  &SCOPED-DEFINE IntTC {&IntCode}
&ENDIF

&IF "{&IntTable}" <> "?" &THEN
IF AVAILABLE {&IntTable} THEN
&ENDIF

DO WITH FRAME {&FRAME-NAME}:

  /* If there is an associated code field in the current frame then
     intialise it's private-data */
     
  DEF VAR wh{&N} AS HANDLE NO-UNDO.
  RUN find-sibling ( FRAME {&FRAME-NAME}:HANDLE , "{&IntCode}", OUTPUT wh{&N} ).
  IF VALID-HANDLE( wh{&N} ) THEN wh{&N}:PRIVATE-DATA = "," + wh{&N}:SCREEN-VALUE.
  

  DEF BUFFER tmp_{&ExtTable} FOR {&ExtTable}.

  
  FIND FIRST tmp_{&ExtTable} WHERE
    tmp_{&ExtTable}.{&ExtKey} = {&IntTC} NO-LOCK NO-ERROR.
      
  IF AVAILABLE tmp_{&ExtTable} THEN
  DO:

    &IF DEFINED(DVFind) NE 0 &THEN
       {&DVFind} tmp_{&ExtTable}.{&ExtKey} NO-LOCK NO-ERROR.
    &ENDIF
    &IF DEFINED(ExtField) NE 0 &THEN
      DISPLAY tmp_{&ExtTable}.{&ExtField} @ {&SELF-NAME}.
    &ELSEIF DEFINED(DV) NE 0 &THEN
      DISPLAY {&DV} @ {&SELF-NAME}.
    &ENDIF 

    {&SELF-NAME}:PRIVATE-DATA = STRING( ROWID( tmp_{&ExtTable} ) ).

    IF VALID-HANDLE( wh{&N} ) THEN
    DO:
      DEF VAR whpd{&N} AS CHAR INIT "," NO-UNDO.
      ENTRY( 1, whpd{&N} ) = STRING( ROWID( tmp_{&ExtTable} ) ).
      ENTRY( 2, whpd{&N} ) = wh{&N}:SCREEN-VALUE.
      wh{&N}:PRIVATE-DATA  = whpd{&N}.
      wh{&N}:MODIFIED      = No.
    END.
      
  END.
     
END.

/* Must include return because default behaviour is strange */
RETURN NO-APPLY.
