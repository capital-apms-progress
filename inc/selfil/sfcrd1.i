/* sfcrd1.i */
/* selection fill-in for a Creditor */

{inc/selfil/sfmst1.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="Creditor"
                     &ExtKey  ="CreditorCode"
                     &ExtField="Name"}
