/* sfpro1.i */
/* selection fill-in for a Property */

{inc/selfil/sfmst1.i &IntTable ="{1}"
                     &IntCode  ="{2}"
                     &ExtTable="Property"
                     &ExtKey  ="PropertyCode"
                     &ExtField="Name"}
