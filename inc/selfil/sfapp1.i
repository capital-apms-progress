/* sfapp1.i */
/* selection fill-in for an approver */

FIND FIRST Approver WHERE Approver.ApproverCode = {1}.{2}
  NO-LOCK NO-ERROR.
  
FIND FIRST Person WHERE Person.PersonCode = Approver.PersonCode
  NO-LOCK NO-ERROR.

{inc/selfil/sfmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="Approver"
                     &ExtKey  ="ApproverCode"
                     &DV="IF AVAILABLE Person THEN Person.FirstName   + ' ' +
                                       Person.LastName ELSE ''"}
