/* sfapp2.i */
/* selection fill-in for an approver */

FIND FIRST Approver WHERE ROWID(Approver) = TO-ROWID( {&SELF-NAME}:PRIVATE-DATA )
  NO-LOCK NO-ERROR.
  
FIND FIRST Person WHERE Person.PersonCode = Approver.PersonCode
  NO-LOCK NO-ERROR.

{inc/selfil/sfmst2.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="Approver"
                     &ExtKey  ="ApproverCode"
                     &DV="IF AVAILABLE Person THEN Person.FirstName   + ' ' +
                                                   Person.LastName ELSE ''"}
