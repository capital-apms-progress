&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
    Library     : inc/entity.i
    Purpose     : Deal with getting things associated with accounting entities

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-get-entity-account) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-entity-account Method-Library 
FUNCTION get-entity-account RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-entity-client) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-entity-client Method-Library 
FUNCTION get-entity-client RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-entity-ledger) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-entity-ledger Method-Library 
FUNCTION get-entity-ledger RETURNS INTEGER
    ( INPUT et AS CHAR, INPUT ec AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-entity-name) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-entity-name Method-Library 
FUNCTION get-entity-name RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-entity-type-name) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-entity-type-name Method-Library 
FUNCTION get-entity-type-name RETURNS CHARACTER
  ( INPUT et AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-parent-entity) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-parent-entity Method-Library 
FUNCTION get-parent-entity RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .4
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Method-Library 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-get-entity-account) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-entity-account Method-Library 
FUNCTION get-entity-account RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR name AS CHAR NO-UNDO INITIAL "* * * Unknown * * *".

DEF BUFFER tmp_COA FOR ChartOfAccount.
DEF BUFFER tmp_ProjectBudget FOR ProjectBudget.

  IF et = "J" THEN DO:
    FIND tmp_ProjectBudget WHERE tmp_ProjectBudget.ProjectCode = ec AND tmp_ProjectBudget.AccountCode = ac NO-LOCK NO-ERROR.
    IF AVAILABLE(tmp_ProjectBudget) THEN RETURN tmp_ProjectBudget.Description.
  END.

  FIND tmp_COA WHERE tmp_COA.AccountCode = ac NO-LOCK NO-ERROR.
  IF AVAILABLE(tmp_COA) THEN name = tmp_COA.Name.

  RETURN name.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-entity-client) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-entity-client Method-Library 
FUNCTION get-entity-client RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Find the client for this entity type / entity code.
------------------------------------------------------------------------------*/
DEF VAR entity-code AS CHAR NO-UNDO.

DEF BUFFER tmp_Company FOR Company.
  DO WHILE et <> "L":
    entity-code = get-parent-entity( et, ec ).
    et = SUBSTRING( entity-code, 1, 1).
    ec = INT( SUBSTRING( entity-code, 2) ).
  END.

  FIND tmp_Company WHERE tmp_Company.CompanyCode = ec NO-LOCK NO-ERROR.
  RETURN tmp_Company.ClientCode.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-entity-ledger) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-entity-ledger Method-Library 
FUNCTION get-entity-ledger RETURNS INTEGER
    ( INPUT et AS CHAR, INPUT ec AS INT ) :
  /*------------------------------------------------------------------------------
    Purpose:  Find the ultimate ledger parent for this entity type / entity code.
  ------------------------------------------------------------------------------*/
  DEF VAR entity-code AS CHAR NO-UNDO.

  DO WHILE et <> "L":
    entity-code = get-parent-entity( et, ec ).
    et = SUBSTRING( entity-code, 1, 1).
    ec = INT( SUBSTRING( entity-code, 2) ).
  END.

  RETURN ec.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-entity-name) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-entity-name Method-Library 
FUNCTION get-entity-name RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR name AS CHAR NO-UNDO.

  CASE et:
    WHEN "T" THEN DO:
      DEF BUFFER tmp_Tenant FOR Tenant.
      FIND tmp_Tenant WHERE tmp_Tenant.TenantCode = ec NO-LOCK NO-ERROR.
      name = IF AVAILABLE(tmp_Tenant) THEN tmp_Tenant.Name ELSE "* * * Unknown * * *".
    END.
    WHEN "C" THEN DO:
      DEF BUFFER tmp_Creditor FOR Creditor.
      FIND tmp_Creditor WHERE tmp_Creditor.CreditorCode = ec NO-LOCK NO-ERROR.
      name = IF AVAILABLE(tmp_Creditor) THEN tmp_Creditor.Name ELSE "* * * Unknown * * *".
    END.
    WHEN "P" THEN DO:
      DEF BUFFER tmp_Property FOR Property.
      FIND tmp_Property WHERE tmp_Property.PropertyCode = ec NO-LOCK NO-ERROR.
      name = IF AVAILABLE(tmp_Property) THEN tmp_Property.Name ELSE "* * * Unknown * * *".
    END.
    WHEN "L" THEN DO:
      DEF BUFFER tmp_Company FOR Company.
      FIND tmp_Company WHERE tmp_Company.CompanyCode = ec NO-LOCK NO-ERROR.
      name = IF AVAILABLE(tmp_Company) THEN tmp_Company.LegalName ELSE "* * * Unknown * * *".
    END.
    WHEN "J" THEN DO:
      DEF BUFFER tmp_Project FOR Project.
      FIND tmp_Project WHERE tmp_Project.ProjectCode = ec NO-LOCK NO-ERROR.
      name = IF AVAILABLE(tmp_Project) THEN tmp_Project.Name ELSE "* * * Unknown * * *".
    END.
    WHEN "F" THEN DO:
      DEF BUFFER tmp_FixedAsset FOR FixedAsset.
      FIND tmp_FixedAsset WHERE tmp_FixedAsset.AssetCode = ec NO-LOCK NO-ERROR.
      name = IF AVAILABLE(tmp_FixedAsset) THEN tmp_FixedAsset.Description ELSE "* * * Unknown * * *".
    END.
  END CASE.

  RETURN name.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-entity-type-name) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-entity-type-name Method-Library 
FUNCTION get-entity-type-name RETURNS CHARACTER
  ( INPUT et AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  CASE et:
    WHEN 'P' THEN   RETURN "Property".
    WHEN "J" THEN   RETURN "Project".
    WHEN "C" THEN   RETURN "Creditor".
    WHEN "L" THEN   RETURN "Company".
    WHEN "T" THEN   RETURN "Tenant".
    WHEN "F" THEN   RETURN "Fixed Asset".
  END CASE.

  RETURN ?.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-parent-entity) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-parent-entity Method-Library 
FUNCTION get-parent-entity RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR parent-entity AS CHAR NO-UNDO.

  CASE et:
    WHEN "J" THEN DO:
      DEF BUFFER tmp_Project FOR Project.
      FIND tmp_Project WHERE tmp_Project.ProjectCode = ec NO-LOCK NO-ERROR.
      parent-entity = tmp_Project.EntityType + STRING( tmp_Project.EntityCode, "99999").
    END.
    WHEN "P" THEN DO:
      DEF BUFFER tmp_Property FOR Property.
      FIND tmp_Property WHERE tmp_Property.PropertyCode = ec NO-LOCK NO-ERROR.
      parent-entity = "L" + STRING( tmp_Property.CompanyCode, "99999").
    END.
    WHEN "T" THEN DO:
      DEF BUFFER tmp_Tenant FOR Tenant.
      FIND tmp_Tenant WHERE tmp_Tenant.TenantCode = ec NO-LOCK NO-ERROR.
      parent-entity = tmp_Tenant.EntityType + STRING( tmp_Tenant.EntityCode, "99999").
    END.
    WHEN "C" THEN DO:
      DEF BUFFER tmp_Creditor FOR Creditor.
      FIND tmp_Creditor WHERE tmp_Creditor.CreditorCode = ec NO-LOCK NO-ERROR.
      parent-entity = "L" + STRING( tmp_Creditor.CompanyCode, "99999").
    END.
    OTHERWISE DO:
      parent-entity = "L00001".
    END.
  END CASE.

  RETURN parent-entity.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

