/*--------------------------------------------------------------------------
    File        : ent-chg.i
    Purpose     : Generic trigger code for viwers with entity-type entity-code

    Parameters  : &1 - The name of the entity type field
                  &2 - List of accepted entity type - "ALL" for all entity types
                  &3 - Run function
                  &4 - Optional flag Yes/No, Default Yes
                       Yes - TAB to next field if entered value is OK
                       No  - Stay in given entity field if OK
                  
    Description : This will immediately validate the entity type field
                  if the value is ok:
                    the function &Function will be run
                    focus will be passed to the next enabled field ( depending
                    on the value of &4.
                  otherwise:
                    an error is displayed

    Author(s)   : Tyrone McAuley
                  
  ------------------------------------------------------------------------*/


ON "ANY-PRINTABLE", "BACKSPACE", "DELETE-CHARACTER" OF {1}
DO:

  IF CHR( LASTKEY ) = {1}:SCREEN-VALUE THEN
    RETURN NO-APPLY.
  
  DEF VAR entity-list AS CHAR NO-UNDO.
  
  IF "{2}" = "All" THEN
    FOR EACH EntityType NO-LOCK:
      entity-list = TRIM( entity-list + "," + EntityType.EntityType, "," ).
    END.
  ELSE
    entity-list = "{2}".
        
  IF LOOKUP( CHR( LASTKEY ), entity-list ) = 0 THEN
  DO:
    MESSAGE
      "The Entity Type must be one of: " SKIP
        "    " entity-list
      VIEW-AS ALERT-BOX ERROR
      TITLE "Invalid Entity Type - " + CAPS( CHR( LASTKEY ) ).
    SELF:AUTO-ZAP = Yes.
    APPLY 'ENTRY':U TO SELF.
  END.
  ELSE
  DO:
    APPLY CAPS( CHR( LASTKEY) ) TO SELF.
    RUN {3} NO-ERROR.
    IF "{4}" <> "No" THEN APPLY 'TAB':U TO SELF.
    ELSE
    DO:
      SELF:AUTO-ZAP = Yes.
      APPLY 'ENTRY':U TO SELF.
    END.
  END.

  RETURN NO-APPLY.

END.


PROCEDURE verify-type:

END.
