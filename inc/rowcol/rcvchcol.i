    CASE Voucher.VoucherStatus:
      WHEN "U" THEN ASSIGN bcolor = 15 fcolor = 1.
      WHEN "Q" THEN ASSIGN bcolor = 15 fcolor = 13.
      WHEN "C" THEN ASSIGN bcolor = 15 fcolor = 4.
      WHEN "A" THEN ASSIGN bcolor = 15 fcolor = 2.
      WHEN "H" THEN ASSIGN bcolor = 15 fcolor = 12.
      WHEN "P" THEN ASSIGN bcolor = 15 fcolor = 0.
      OTHERWISE     ASSIGN bcolor = ?  fcolor = ?.
    END CASE.
