/* rcactamt.i */
/* row-display trigger code for voucher browsers */

AcctTran.Amount:FGCOLOR IN BROWSE {&BROWSE-NAME}
                = IF AcctTran.Amount < 0 THEN 12 ELSE 0.
