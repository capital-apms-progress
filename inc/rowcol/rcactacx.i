/* rcactacx.i */
/* row-display trigger code for account transaction of X browser */

  IF AVAILABLE(AcctTran) THEN DO WITH FRAME {&FRAME-NAME}:
    DEFINE VARIABLE fcolor AS INTEGER INITIAL ? NO-UNDO.

    IF AcctTran.ClosedState = "P" THEN fcolor = 3.
    ELSE IF AcctTran.ClosedState = "F" THEN fcolor = 2.

    ASSIGN
      d-date:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
      ClosedState:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
      Reference:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
      Description:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
      FirstName:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
      BatchCode:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
    .
  END.
