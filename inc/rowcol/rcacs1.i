/* rcacs1.i */
/* row-display trigger code for account summary browsers */

AccountSummary.Balance:FGCOLOR IN BROWSE {&BROWSE-NAME}
                = IF AccountSummary.Balance < 0 THEN 12 ELSE 0.
