/* rcinv1.i */
/* row-display trigger code for Invoice browsers */
/* Row colour is determined by the InvoiceStatus field */

  IF AVAILABLE Invoice THEN DO:

    DEF VAR bcolor AS INT NO-UNDO.
    DEF VAR fcolor AS INT NO-UNDO.
    
    CASE Invoice.InvoiceStatus:
      WHEN "U" THEN ASSIGN bcolor = 15 fcolor = 1.
      OTHERWISE     ASSIGN bcolor = 8  fcolor = 0.
    END CASE.
    
    ASSIGN
      Invoice.InvoiceNo:BGCOLOR 
              IN BROWSE {&BROWSE-NAME}  = bcolor
      Invoice.InvoiceDate:BGCOLOR       = bcolor
      Tenant:BGCOLOR                    = bcolor
      Invoice.ToDetail:BGCOLOR          = bcolor
      Invoice.Total:BGCOLOR             = bcolor

      Invoice.InvoiceNo:FGCOLOR         = fcolor
      Invoice.InvoiceDate:FGCOLOR       = fcolor
      Tenant:FGCOLOR                    = fcolor
      Invoice.ToDetail:FGCOLOR          = fcolor
      Invoice.Total:FGCOLOR             = IF Total < 0 THEN 12 ELSE fcolor
    NO-ERROR.
    
  END.
