/* rcvch1.i */
/* row-display trigger code for voucher browsers */
/* Row colour is determined by the VoucherStatus field */

  IF AVAILABLE Voucher THEN DO:

    DEF VAR fcolor AS INT NO-UNDO.
    DEF VAR bcolor AS INT NO-UNDO.
    
    {inc/rowcol/rcvchcol.i}
    
    ASSIGN
      Voucher.VoucherSeq:BGCOLOR
              IN BROWSE {&BROWSE-NAME} = bcolor
      Voucher.VoucherStatus:BGCOLOR    = bcolor
      Voucher.ApproverCode:BGCOLOR     = bcolor
      Voucher.CreditorCode:BGCOLOR     = bcolor
      Voucher.Date:BGCOLOR             = bcolor
      Voucher.Description:BGCOLOR      = bcolor
      Voucher.InvoiceReference:BGCOLOR = bcolor
      Voucher.GoodsValue:BGCOLOR       = bcolor
      Voucher.TaxValue:BGCOLOR         = bcolor
      total-value:BGCOLOR              = bcolor
      date-due:BGCOLOR                 = bcolor
      cheque-detail:BGCOLOR            = bcolor
      update-detail:BGCOLOR            = bcolor

      Voucher.VoucherSeq:FGCOLOR       = fcolor
      Voucher.VoucherStatus:FGCOLOR    = fcolor
      Voucher.ApproverCode:FGCOLOR     = fcolor
      Voucher.CreditorCode:FGCOLOR     = fcolor
      Voucher.Date:FGCOLOR             = fcolor
      Voucher.Description:FGCOLOR      = fcolor
      Voucher.InvoiceReference:FGCOLOR = fcolor
      Voucher.GoodsValue:FGCOLOR       = fcolor
      Voucher.TaxValue:FGCOLOR         = fcolor
      total-value:FGCOLOR              = fcolor
      date-due:FGCOLOR                 = fcolor
      cheque-detail:FGCOLOR            = fcolor
      update-detail:FGCOLOR            = fcolor
    NO-ERROR.
    
  END.
