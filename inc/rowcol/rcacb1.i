/* rcacb1.i */
/* row-display trigger code for account balance browsers */

AccountBalance.Balance:FGCOLOR IN BROWSE {&BROWSE-NAME}
                = IF AccountBalance.Balance < 0 THEN 12 ELSE 0.
AccountBalance.Budget:FGCOLOR IN BROWSE {&BROWSE-NAME}
                = IF AccountBalance.Budget < 0 THEN 12 ELSE 0.
AccountBalance.RevisedBudget:FGCOLOR IN BROWSE {&BROWSE-NAME}
                = IF AccountBalance.RevisedBudget < 0 THEN 12 ELSE 0.
