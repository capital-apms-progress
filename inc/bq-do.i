/****************************************************/
/* Filename:    inc/bq-do.i                         */
/* Notes:       Run process on the batch queue      */
/* Parameters:                                      */
/*      1   The full pathname of the process to run */
/*      2   The parameters of the process to run    */
/*      3   "run locally" logical value             */
/****************************************************/

IF {3} THEN DO:
  RUN make-bq-entry IN sys-mgr( "{1}", {2}, ?, ? ).
END.
ELSE DO:
  RUN notify( 'set-busy,container-source':U ).
  RUN {1} ( {2} ).
  RUN notify( 'set-idle,container-source':U ).
END.

