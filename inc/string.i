&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Method-Library 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-add-to-list) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD add-to-list Method-Library 
FUNCTION add-to-list RETURNS CHARACTER
  ( INPUT-OUTPUT str AS CHAR, INPUT item AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-add-to-list-delim) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD add-to-list-delim Method-Library 
FUNCTION add-to-list-delim RETURNS CHARACTER
  ( INPUT-OUTPUT str AS CHAR, INPUT item AS CHAR, INPUT delim AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SPC) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SPC Method-Library 
FUNCTION SPC RETURNS CHARACTER
  ( INPUT n AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-WRAP) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD WRAP Method-Library 
FUNCTION WRAP RETURNS CHARACTER
  ( INPUT unwrapped AS CHAR, INPUT wrap-width AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Method-Library
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Method-Library ASSIGN
         HEIGHT             = .08
         WIDTH              = 38.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Method-Library 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-add-to-list) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION add-to-list Method-Library 
FUNCTION add-to-list RETURNS CHARACTER
  ( INPUT-OUTPUT str AS CHAR, INPUT item AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  str = str + (IF str = "" THEN "" ELSE ",") + item.

END FUNCTION.

&GLOB EXCLUDE-add-to-list 1

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-add-to-list-delim) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION add-to-list-delim Method-Library 
FUNCTION add-to-list-delim RETURNS CHARACTER
  ( INPUT-OUTPUT str AS CHAR, INPUT item AS CHAR, INPUT delim AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  str = str + (IF str = "" THEN "" ELSE delim) + item.

END FUNCTION.

&GLOB EXCLUDE-add-to-list-delim 1

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SPC) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SPC Method-Library 
FUNCTION SPC RETURNS CHARACTER
  ( INPUT n AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN FILL( " ", n ).   /* Function return value. */

END FUNCTION.

&GLOB EXCLUDE-SPC 1

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-WRAP) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION WRAP Method-Library 
FUNCTION WRAP RETURNS CHARACTER
  ( INPUT unwrapped AS CHAR, INPUT wrap-width AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Wrap 'unwrapped' to the specified width
------------------------------------------------------------------------------*/
DEF VAR result     AS CHAR NO-UNDO INITIAL "".
DEF VAR words-left AS CHAR NO-UNDO.
DEF VAR idx        AS INT NO-UNDO.
DEF VAR idx2       AS INT NO-UNDO.

  words-left = unwrapped.
  DO WHILE LENGTH( words-left ) > wrap-width :
    idx = R-INDEX( words-left, " ", wrap-width ).
    IF idx = 0 THEN idx = wrap-width.
    idx2 = INDEX( words-left, "~n").
    IF idx2 <> 0 AND idx2 < idx THEN DO:
      idx = idx2.
      result = result + SUBSTR( words-left, 1, idx).
    END.
    ELSE
      result = result + TRIM(SUBSTR( words-left, 1, idx - 1)) + "~n".

    words-left = SUBSTR( words-left, idx + 1).
  END.
  result = result + words-left.

  RETURN result.

END FUNCTION.

&GLOB EXCLUDE-WRAP 1

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

