/* Generic topic description preprocessors for Cheque */

&GLOBAL-DEFINE TOPIC-TABLE  Cheque
&GLOBAL-DEFINE TOPIC-FIELD  (Cheque.BankAccountCode + "/" + STRING(Cheque.ChequeNo,"999999"))
&GLOBAL-DEFINE TOPIC-PREFIX "Cheque"
