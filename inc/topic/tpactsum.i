/* Generic topic description preprocessors for AccountSummary */

&GLOBAL-DEFINE TOPIC-TABLE  AccountSummary

&GLOBAL-DEFINE TOPIC-FIELD  ~
( AccountSummary.EntityType + "-" + ~
  STRING(AccountSummary.EntityCode, "99999") + "-" + ~
  STRING(AccountSummary.AccountCode,"9999.99") ~
)

&GLOBAL-DEFINE TOPIC-PREFIX "Account"
