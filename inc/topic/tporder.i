/* Generic topic description preprocessors for Orders */

&GLOBAL-DEFINE TOPIC-TABLE  Order
&GLOBAL-DEFINE TOPIC-FIELD  (STRING(Order.ProjectCode) + "/" + STRING(Order.OrderCode) + " - " + REPLACE(SUBSTRING(Order.Description,1,50),"~~n",", "))
&GLOBAL-DEFINE TOPIC-PREFIX "Order"
