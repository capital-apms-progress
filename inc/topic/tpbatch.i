/* Generic topic description preprocessors for Batches */

&GLOBAL-DEFINE TOPIC-TABLE  Batch
&GLOBAL-DEFINE TOPIC-FIELD  STRING(Batch.BatchCode) + ' - ' + Batch.Description
&GLOBAL-DEFINE TOPIC-PREFIX "Posted Batch"
