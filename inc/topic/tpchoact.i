/* Generic topic description preprocessors for Accounts */

&GLOBAL-DEFINE TOPIC-TABLE  ChartOfAccount
&GLOBAL-DEFINE TOPIC-FIELD  STRING(ChartOfAccount.AccountCode, "9999.99") ~
                            + " - " + ChartOfAccount.Name
&GLOBAL-DEFINE TOPIC-PREFIX "Account"

