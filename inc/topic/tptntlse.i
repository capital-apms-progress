/* Generic topic description preprocessors for Tenancy Leases*/

&GLOBAL-DEFINE TOPIC-TABLE  TenancyLease

&GLOBAL-DEFINE TOPIC-FIELD ~
(~
  (IF AVAILABLE(Tenant) THEN ("by " + Tenant.Name) ~
                        ELSE STRING(TenancyLease.TenancyLeaseCode)) + ~
  (IF TenancyLease.LeaseStatus = "PAST" THEN " (past)" ~
                                        ELSE "") ~
)
&GLOBAL-DEFINE TOPIC-PREFIX "Lease"
