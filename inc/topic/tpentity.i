/* Generic topic description preprocessors for Entities */
/* {1} is a variable referring to the entity type */
/* {2} is a variable referring to the entity code */

{inc/entity.i}

&GLOBAL-DEFINE TOPIC-TABLE
&GLOBAL-DEFINE TOPIC-FIELD  get-entity-name( {1}, {2} )
&GLOBAL-DEFINE TOPIC-PREFIX get-entity-type-name( {1} )
