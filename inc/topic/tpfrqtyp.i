/* Generic topic description preprocessors for FrequencyTypes */

&GLOBAL-DEFINE TOPIC-TABLE  FrequencyType
&GLOBAL-DEFINE TOPIC-FIELD  FrequencyType.FrequencyCode
&GLOBAL-DEFINE TOPIC-PREFIX "Frequency Type"
