/* Generic topic description preprocessors for Batches */

&GLOBAL-DEFINE TOPIC-TABLE  NewBatch
&GLOBAL-DEFINE TOPIC-FIELD  STRING(NewBatch.BatchCode) + ' - ' + NewBatch.Description
&GLOBAL-DEFINE TOPIC-PREFIX "Unposted Batch"
