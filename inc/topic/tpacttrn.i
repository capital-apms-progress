/* Generic topic description preprocessors for Account Transactions */

&GLOBAL-DEFINE TOPIC-TABLE  AcctTran
&GLOBAL-DEFINE TOPIC-FIELD  (STRING(AcctTran.BatchCode) + "-" + ~
                             STRING(AcctTran.DocumentCode) + "-" + ~
                             STRING(AcctTran.TransactionCode) + "  " + ~
                             AcctTran.EntityType + ~
                             STRING(AcctTran.EntityCode,"99999") + "-" + ~
                             STRING(AcctTran.AccountCode,"99999") + "  " + ~
                             STRING(AcctTran.Date,"99/99/9999") + "  " + ~
                             AcctTran.Reference)
&GLOBAL-DEFINE TOPIC-PREFIX "Transaction"
