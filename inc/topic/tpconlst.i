/* Generic topic description preprocessors for Consolidation Lists */

&GLOBAL-DEFINE TOPIC-TABLE  ConsolidationList
&GLOBAL-DEFINE TOPIC-FIELD  ConsolidationList.Name
&GLOBAL-DEFINE TOPIC-PREFIX "Consolidation List"
