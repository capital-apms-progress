/* Generic topic description preprocessors for BuildingSystem */

&GLOBAL-DEFINE TOPIC-TABLE  BuildingSystem
&GLOBAL-DEFINE TOPIC-FIELD  ("P" + STRING(BuildingSystem.PropertyCode) + "/" + STRING(BuildingSystem.BuildingSystemType))
&GLOBAL-DEFINE TOPIC-PREFIX "Building/System"
