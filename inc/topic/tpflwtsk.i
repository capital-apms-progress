/* Generic topic description preprocessors for FlowTask */

&GLOBAL-DEFINE TOPIC-TABLE  FlowTask
&GLOBAL-DEFINE TOPIC-FIELD  (FlowTask.EntityType + STRING(FlowTask.EntityCode) ~
                             + " " + STRING(FlowTask.DueDate,"99/99/9999") ~
                             + " " + FlowTask.FlowTaskType ~
                             + " - " + FlowTask.Description)
&GLOBAL-DEFINE TOPIC-PREFIX "FlowTask"
