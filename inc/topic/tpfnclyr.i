/* Generic topic description preprocessors for AccountGroups */

&GLOBAL-DEFINE TOPIC-TABLE  FinancialYear
&GLOBAL-DEFINE TOPIC-FIELD  (FinancialYear.Description + ' (' + STRING(FinancialYear.FinancialYearCode,'9999') + ')')
&GLOBAL-DEFINE TOPIC-PREFIX ""
