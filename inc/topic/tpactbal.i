/* Generic topic description preprocessors for AccountBalances */

&GLOBAL-DEFINE TOPIC-TABLE  AccountBalance

&GLOBAL-DEFINE TOPIC-FIELD  ~
( AccountBalance.EntityType + "-" + ~
  STRING(AccountBalance.EntityCode, "99999") + "-" + ~
  STRING(AccountBalance.AccountCode,"9999.99") + ~
  IF AVAILABLE(Month) THEN ~
    ( "  " + ~
      STRING( Month.MonthName, "9999") + " " + ~
      STRING( Month.FinancialYearCode, "9999") ~
    ) ~
  ELSE ~
    "" ~
)

&GLOBAL-DEFINE TOPIC-PREFIX "Account Balance"
