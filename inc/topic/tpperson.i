/* Generic topic description preprocessors for Persons */

&GLOBAL-DEFINE TOPIC-TABLE  Person
&GLOBAL-DEFINE TOPIC-FIELD  ( IF TRIM( Person.FirstName + ' ' + Person.LastName ) <> "" THEN ( Person.FirstName + ' ' + Person.LastName ) ELSE Person.Company )
&GLOBAL-DEFINE TOPIC-PREFIX ""
