/* Generic topic description preprocessors for Projects */

&GLOBAL-DEFINE TOPIC-TABLE  ProjectBudget
&GLOBAL-DEFINE TOPIC-FIELD  ("J" + STRING(ProjectBudget.ProjectCode) + "/" + STRING(ProjectBudget.AccountCode,"9999.99"))
&GLOBAL-DEFINE TOPIC-PREFIX "Account"
