/* Generic topic description preprocessors for Rental Spaces */

&GLOBAL-DEFINE TOPIC-TABLE  RentalSpace
&GLOBAL-DEFINE TOPIC-FIELD  ~
(~
  "P" + STRING(RentalSpace.PropertyCode ) ~
  + ", Lvl " + STRING( RentalSpace.Level ) + "-" + STRING(RentalSpace.LevelSequence) ~
  + ", " + SUBSTRING(RentalSpace.Description, 1, 20) ~
)

&GLOBAL-DEFINE TOPIC-PREFIX "RentalSpace"
