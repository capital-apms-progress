/* Generic topic description preprocessors for Tenants */

&GLOBAL-DEFINE TOPIC-TABLE  Tenant
&GLOBAL-DEFINE TOPIC-FIELD  (Tenant.Name + " (T" + STRING(Tenant.TenantCode,"99999") + "/" + Tenant.EntityType + ~
 STRING(Tenant.EntityCode, (IF Tenant.EntityType = "P" THEN "99999" ELSE "999") ) + ")" )
&GLOBAL-DEFINE TOPIC-PREFIX ""
