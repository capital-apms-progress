/* Generic topic description preprocessors for Property Titles */

&GLOBAL-DEFINE TOPIC-TABLE  PropertyTitle
&GLOBAL-DEFINE TOPIC-FIELD  PropertyTitle.TitleCode
&GLOBAL-DEFINE TOPIC-PREFIX "Property title"
