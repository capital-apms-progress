/* Generic topic description preprocessors for BankAccounts */

&GLOBAL-DEFINE TOPIC-TABLE  BankAccount
&GLOBAL-DEFINE TOPIC-FIELD  BankAccount.BankAccountCode
&GLOBAL-DEFINE TOPIC-PREFIX "Bank Account"
