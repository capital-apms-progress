/* Triggers for all windows */

ON 'F12':U OF {&WINDOW-NAME} ANYWHERE DO:
  IF VALID-HANDLE( sys-mgr ) THEN
    RUN focus-this IN sys-mgr( THIS-PROCEDURE ).
END.

ON 'SHIFT-F12':U OF {&WINDOW-NAME} ANYWHERE DO:
  IF VALID-HANDLE( sys-mgr ) THEN
    RUN focus-root IN sys-mgr.
END.

ON 'CTRL-F12':U OF {&WINDOW-NAME} ANYWHERE DO:
  IF VALID-HANDLE( sys-mgr ) THEN
    RUN focus-parent IN sys-mgr( THIS-PROCEDURE ).
END.
