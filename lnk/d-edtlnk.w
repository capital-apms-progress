&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialog 
/*------------------------------------------------------------------------

  File: 
  Author: Andrew McMillan

  Created: 
------------------------------------------------------------------------*/
DEF INPUT PARAMETER link-code LIKE ProgramLink.LinkCode NO-UNDO.

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

FIND ProgramLink WHERE ProgramLink.LinkCode = link-code NO-LOCK NO-ERROR.
IF NOT AVAILABLE(ProgramLink) THEN DO:
  MESSAGE "Program Link not found".
  RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialog

&Scoped-define ADM-CONTAINER DIALOG-BOX

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME D-Dialog

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ProgramLink

/* Definitions for DIALOG-BOX D-Dialog                                  */
&Scoped-define FIELDS-IN-QUERY-D-Dialog ProgramLink.Description ~
ProgramLink.ButtonLabel ProgramLink.LinkType ProgramLink.CreateViewer ~
ProgramLink.Viewer 
&Scoped-define ENABLED-FIELDS-IN-QUERY-D-Dialog ProgramLink.Description ~
ProgramLink.ButtonLabel ProgramLink.LinkType ProgramLink.CreateViewer ~
ProgramLink.Viewer 
&Scoped-define ENABLED-TABLES-IN-QUERY-D-Dialog ProgramLink
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-D-Dialog ProgramLink

&Scoped-define FIELD-PAIRS-IN-QUERY-D-Dialog~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}~
 ~{&FP1}ButtonLabel ~{&FP2}ButtonLabel ~{&FP3}~
 ~{&FP1}LinkType ~{&FP2}LinkType ~{&FP3}~
 ~{&FP1}CreateViewer ~{&FP2}CreateViewer ~{&FP3}~
 ~{&FP1}Viewer ~{&FP2}Viewer ~{&FP3}
&Scoped-define OPEN-QUERY-D-Dialog OPEN QUERY D-Dialog FOR EACH ProgramLink SHARE-LOCK.
&Scoped-define TABLES-IN-QUERY-D-Dialog ProgramLink
&Scoped-define FIRST-TABLE-IN-QUERY-D-Dialog ProgramLink


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS ProgramLink.Description ~
ProgramLink.ButtonLabel ProgramLink.LinkType ProgramLink.CreateViewer ~
ProgramLink.Viewer 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}~
 ~{&FP1}ButtonLabel ~{&FP2}ButtonLabel ~{&FP3}~
 ~{&FP1}LinkType ~{&FP2}LinkType ~{&FP3}~
 ~{&FP1}CreateViewer ~{&FP2}CreateViewer ~{&FP3}~
 ~{&FP1}Viewer ~{&FP2}Viewer ~{&FP3}
&Scoped-define ENABLED-TABLES ProgramLink
&Scoped-define FIRST-ENABLED-TABLE ProgramLink
&Scoped-Define ENABLED-OBJECTS RECT-1 edt_Function Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-FIELDS ProgramLink.Description ~
ProgramLink.ButtonLabel ProgramLink.LinkType ProgramLink.CreateViewer ~
ProgramLink.Viewer 
&Scoped-Define DISPLAYED-OBJECTS edt_Function 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 8 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 8.57 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE edt_Function AS CHARACTER 
     VIEW-AS EDITOR NO-WORD-WRAP SCROLLBAR-HORIZONTAL SCROLLBAR-VERTICAL LARGE
     SIZE 72.57 BY 8.2.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 85.14 BY 14.2.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY D-Dialog FOR 
      ProgramLink SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialog
     ProgramLink.Description AT ROW 1.4 COL 11.57 COLON-ALIGNED FORMAT "X(1024)"
          VIEW-AS FILL-IN 
          SIZE 72.57 BY 1.05
     ProgramLink.ButtonLabel AT ROW 2.6 COL 11.57 COLON-ALIGNED FORMAT "X(1024)"
          VIEW-AS FILL-IN 
          SIZE 42.29 BY 1.05
     ProgramLink.LinkType AT ROW 3.8 COL 11.57 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 4.57 BY 1.05
     ProgramLink.CreateViewer AT ROW 3.8 COL 34.43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 2.14 BY 1.05
     ProgramLink.Viewer AT ROW 5.4 COL 11.57 COLON-ALIGNED FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 72.57 BY 1.05
     edt_Function AT ROW 6.8 COL 13.57 NO-LABEL
     Btn_OK AT ROW 15.6 COL 1.57
     Btn_Cancel AT ROW 15.6 COL 11.29
     RECT-1 AT ROW 1.2 COL 1.57
     "Function:" VIEW-AS TEXT
          SIZE 6.86 BY .65 AT ROW 7 COL 6.72
     SPACE(73.70) SKIP(9.04)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 10
         TITLE "Programlink Editor"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialog
   Allow: Basic,Browse,DB-Fields,Query,Smart
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX D-Dialog
                                                                        */
ASSIGN 
       FRAME D-Dialog:SCROLLABLE       = FALSE
       FRAME D-Dialog:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN ProgramLink.ButtonLabel IN FRAME D-Dialog
   EXP-FORMAT                                                           */
/* SETTINGS FOR FILL-IN ProgramLink.Description IN FRAME D-Dialog
   EXP-FORMAT                                                           */
ASSIGN 
       edt_Function:RETURN-INSERTED IN FRAME D-Dialog  = TRUE.

/* SETTINGS FOR FILL-IN ProgramLink.Viewer IN FRAME D-Dialog
   EXP-FORMAT                                                           */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialog
/* Query rebuild information for DIALOG-BOX D-Dialog
     _TblList          = "TTPL.ProgramLink"
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialog */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialog 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON WINDOW-CLOSE OF FRAME D-Dialog /* Programlink Editor */
DO:  
  /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK D-Dialog
ON CHOOSE OF Btn_OK IN FRAME D-Dialog /* OK */
DO:
  RUN assign-changes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialog 


/* ***************************  Main Block  *************************** */

edt_Function = REPLACE( ProgramLink.Function , ",", "~n" ).

{src/adm/template/dialogmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialog _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialog _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-changes D-Dialog 
PROCEDURE assign-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:


  FIND CURRENT ProgramLink EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE(ProgramLink) THEN DO:
    ASSIGN {&ENABLED-FIELDS}.
    ProgramLink.Function = REPLACE( INPUT edt_Function, "~n", "," ).
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialog _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialog.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialog _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY edt_Function 
      WITH FRAME D-Dialog.
  IF AVAILABLE ProgramLink THEN 
    DISPLAY ProgramLink.Description ProgramLink.ButtonLabel ProgramLink.LinkType 
          ProgramLink.CreateViewer ProgramLink.Viewer 
      WITH FRAME D-Dialog.
  ENABLE RECT-1 ProgramLink.Description ProgramLink.ButtonLabel 
         ProgramLink.LinkType ProgramLink.CreateViewer ProgramLink.Viewer 
         edt_Function Btn_OK Btn_Cancel 
      WITH FRAME D-Dialog.
  VIEW FRAME D-Dialog.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialog}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialog _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ProgramLink"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialog 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


