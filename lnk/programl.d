4 "DRL" "Browse the  Accounts of this Company (General Ledger)" 4 3 "Accounts" "vwr/drl/b-actsum.w" yes "Key-Name = CompanyCode,NotesViewer = Yes" "" "" no no
8 "DRL" "View all transactions in the same document as the current one" 7 44 "Relate by Document" "vwr/drl/b-actdoc.w" yes "Key-Name = BatchCode" "" "" no no
10 "DRL" "Browse all Service Contracts" 8 5 "Service Contracts" "vwr/drl/b-contrt.w" yes "SortPanel = Yes,NotesViewer = Yes,FilterPanel = Yes" "" "" yes yes
12 "DRL" "Browse all Properties" 8 11 "Properties" "vwr/drl/b-proper.w" yes "SortPanel = Yes,NotesViewer = Yes,FilterPanel = Yes,AlphabetPanel = Yes" "" "" yes yes
13 "DRL" "Browse Vacant Rental Spaces by various criteria" 8 13 "Browse Vacant Space" "vwr/drl/b-vcntsp.w" yes "SortPanel = Yes,NotesViewer = Yes" "" "" yes yes
15 "DRL" "Browse all Tenants" 8 14 "Tenants" "vwr/drl/b-tenant.w" yes "FilterPanel = Yes,SearchPanel = Yes,NotesViewer = Yes,SortPanel = Yes,AlphabetPanel = Yes" "" "" yes yes
17 "DRL" "Browse Companies" 8 4 "Companies" "vwr/drl/b-compny.w" yes "SortPanel = Yes,FilterPanel = Yes,AlphabetPanel = Yes" "" "" no no
18 "DRL" "Browse Creditors" 8 6 "Creditors" "vwr/drl/b-crdtor.w" yes "SortPanel = Yes,SearchPanel = Yes,AlphabetPanel = Yes,AlphabetPanel2 = Yes,FilterPanel = Yes" "" "" yes yes
20 "DRL" "Browse records defining Approvers of Vouchers and Cheques" 8 17 "Approvers" "vwr/drl/b-apprvr.w" yes "FilterPanel = Yes" "" "" no no
21 "DRL" "Maintain the current batch as a batch of journals" 9 42 "Journals" "" no "" "" "" no no
24 "DRL" "Browse Contracts of this Property" 11 5 "Service Contracts" "vwr/drl/b-contrt.w" yes "SortPanel = Yes,NotesViewer = Yes,FilterPanel = Yes,Key-Name = PropertyCode" "" "" yes yes
26 "DRL" "Browse Records of Title for this Property" 11 12 "Property Titles" "vwr/drl/b-ptipro.w" yes "" "" "" no no
28 "DRL" "Browse Leases for this Property" 11 74 "Leases" "vwr/drl/b-tntlse.w" yes "Key-Name = PropertyCode,SortPanel = Yes,NotesViewer = Yes,FilterBy-Case = Current leases only" "" "" no yes
29 "DRL" "Browse Accounts of this Property" 11 3 "Accounts" "vwr/drl/b-actsum.w" yes "Key-Name = PropertyCode,NotesViewer = Yes" "" "" no no
35 "DRL" "Browse Chart of Account Groupings" 8 45 "Account Groups" "vwr/drl/b-actgrp.w" yes "SortPanel = Yes" "" "" yes no
36 "DRL" "Add an approver" 17 52 "Add" "vwr/mnt/v-apprvr.w" yes "mode = Add" "" "" no no
41 "MNT" "Browse (and Maintain) Types of Documents allowed in the system (system function)" 8 177 "Doc Types" "vwr/drl/b-doctyp.w" yes "" "" "" no no
42 "DRL" "Browse unposted batches" 8 9 "Select Batch" "vwr/drl/b-newbch.w" yes "SortPanel = Yes,FilterPanel = Yes" "" "" yes yes
43 "DRL" "Browse Month definitions" 8 40 "Months" "vwr/drl/b-month.w" yes "" "" "" no no
44 "DRL" "Browse Financial Years definitions" 8 47 "Financial Years" "vwr/drl/b-fnclyr.w" yes "" "" "" no no
45 "DRL" "Browse Office records" 8 267 "Offices" "vwr/drl/b-office.w" yes "" "" "" no no
46 "MNT" "Maintain the details of the currently selected Property" 11 57 "Maintain" "vwr/mnt/v-proper.w" yes "Mode = Maintain" "" "" no no
47 "MNT" "Maintain the current Property Title record" 12 65 "Maintain" "vwr/mnt/v-protit.w" yes "" "" "" no no
49 "MNT" "Maintain the current Approver" 18 52 "Maintain" "vwr/mnt/v-apprvr.w" yes "mode = Maintain" "" "" no no
50 "DRL" "Maintain this Company" 19 59 "Maintain" "vwr/mnt/v-compny.w" yes "mode = Maintain" "" "" no no
51 "MNT" "Maintain this Account definition record" 20 54 "Maintain" "vwr/mnt/v-choact.w" yes "mode = Maintain" "" "" no no
54 "MNT" "Maintain this Property" 23 57 "&Maintain" "vwr/mnt/v-proper.w" yes "" "" "" no no
57 "DRL" "Maintain Tenancy Lease" 74 62 "Maintain" "vwr/mnt/v-tntlse.w" yes "Mode = Maintain" "" "" no no
60 "SEL" "Select the Parent Company for this Company" 28 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes,Fill-In = fil_Parent,Field = ParentCode" "fil_Parent" "ParentCode" no no
61 "SEL" "Select an alternative Contact Person for this Creditor" 29 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = ST   - Suppliers: Trade" "fil_OtherContact" "" no no
67 "SEL" "Select a Person as manager for this property" 32 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = SF   - Staff" "fil_Manager" "" no no
68 "SEL" "Select a Person as backup manager for this property" 32 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = SF   - Staff" "fil_Administrator" "Administrator" no no
69 "SEL" "Select a company as the owner of this property" 32 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_Company" "CompanyCode" no no
71 "SEL" "Select a property as the one that contains this Rental Space" 33 23 "" "vwr/sel/b-selpro.w" yes "" "fil_Property" "PropertyCode" no no
74 "SEL" "Select a tenant who is party to this lease" 34 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes, SearchPanel = Yes" "fil_Tenant" "TenantCode" no no
80 "SEL" "Select a person who is this user record" 37 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = SF   - Staff" "fil_Person" "" no no
83 "DRL" "Browse Invoices to Debtors" 8 67 "Invoices" "vwr/drl/b-ivoice.w" yes "SortPanel = Yes,FilterPanel = Yes" "" "" yes yes
84 "DRL" "Maintain Invoices" 67 68 "Maintain" "vwr/mnt/v-ivoice.w" yes "mode = Maintain" "" "" no no
94 "SEL" "Select the Account for this Bank Account" 81 20 "" "vwr/sel/b-selcoa.w" yes "SortPanel = Yes,FilterPanel = Yes,Fill-In = fil_Account,Field = AccountCode" "fil_Account" "AccountCode" yes yes
95 "SEL" "Select the Company for this Bank Account" 81 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes,Fill-In = fil_Company,Field = CompanyCode" "fil_Company" "CompanyCode" yes yes
97 "DRL" "Maintain the details for this company" 4 59 "Maintain" "vwr/mnt/v-compny.w" yes "mode = Maintain" "" "" no no
98 "SEL" "Select a Property for a Service Contract" 83 23 "" "vwr/sel/b-selpro.w" yes "Fill-In = fil_Property,Field = PropertyCode" "fil_Property" "PropertyCode" no no
99 "MNT" "Browse Chart Of Accounts records" 8 100 "Chart of Accounts" "vwr/drl/b-choact.w" yes "SortPanel = Yes,OptionPanel = Active,SearchPanel = Yes,FilterPanel = Yes" "" "" yes yes
100 "DRL" "Maintain the records which define links between the different functions in the system" 8 84 "Program Links" "" no "" "" "" no no
104 "DRL" "Report which prints a Rental Advice form advising a tenant of a change in their rental" 8 134 "Rental Advice" "vwr/mnt/v-rntadv.w" yes "" "" "" no no
105 "MNT" "Browse (and Maintain) System Autotext Entries" 8 177 "Autotext" "vwr/drl/b-autext.w" yes "" "" "" no no
106 "DRL" "Browse the monthly balances of this Account" 3 1 "by Month" "vwr/drl/b-actbal.w" yes "NotesViewer = Yes,Key-Name = EntityCode" "" "" no no
107 "SEL" "Select Tenant for a Rental Advice" 93 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes, SearchPanel = Yes" "fil_Tenant" "Int1" no no
108 "SEL" "Select a Person for a Directorship" 91 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = SF   - Staff,Fill-In = fil_Director" "fil_Director" "" no no
109 "SEL" "Select a Person for a Share Holder" 92 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = SH   - ShareHolders" "fil_ShareHolder" "" no no
110 "SEL" "Contact for a Service Contract" 83 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = ST   - Suppliers: Trade,Fill-In = fil_contact,Field = Contact" "fil_contact" "Contact" no no
111 "SEL" "Select a Creditor for a Service Contract" 83 21 "" "vwr/sel/b-selcrd.w" yes "FilterPanel = Yes,SearchPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,AlphabetPanel2 = Yes,Fill-In = fil_Creditor,Field = CreditorCode" "fil_Creditor" "CreditorCode" no no
115 "DRL" "Browse Contacts records" 8 108 "Contacts" "vwr/drl/b-person.w" yes "SortPanel = Yes,SearchPanel = Yes,FilterPanel = Yes,AlphabetPanel = Yes" "" "" yes yes
116 "MNU" "Menu of Accounting processes and data-entry screens" 8 97 "Accounting Menu" "" no "" "" "" no no
117 "MNU" "Menu of Processes and Functions used by the Property Management department" 8 96 "Property Menu" "" no "" "" "" no no
118 "MNU" "Menu of Administrative and System Maintenance functions" 8 98 "Administration" "" no "" "" "" no no
119 "DRL" "Browse (and Maintain) types of Services provided by contractors" 8 99 "Service Types" "vwr/drl/b-srvtyp.w" yes "" "" "" no no
120 "DRL" "Browse (and Maintain) records which define the possible repeat frequencies used by the system" 8 101 "Frequency Types" "vwr/drl/b-frqtyp.w" yes "" "" "" no no
121 "DRL" "Frequency Type Maintenance" 101 102 "Maintain" "vwr/mnt/v-frqtyp.w" yes "mode = Maintain" "" "" no no
122 "DRL" "Accounts Summary by Account" 100 3 "Balances" "vwr/drl/b-actsum.w" yes "Key-Name = AccountCode,NotesViewer = Yes" "" "" no no
123 "DRL" "Add a new account to the Chart of Accounts" 100 54 "Add" "vwr/mnt/v-choact.w" yes "mode = Add" "" "" no no
136 "DRL" "Browse the transactions of this account" 3 7 "Transactions" "vwr/drl/b-actacx.w" yes "FilterPanel = Yes,Key-Name = AccountSummary" "" "" no no
140 "DRL" "Browse the Transactions for this Tenant" 14 7 "Transactions" "vwr/drl/b-actacx.w" yes "SortPanel = Yes,FilterPanel = Yes" "" "" no no
148 "DRL" "Report showing Vouchers information - usually used to see how many vouchers each approver has outstanding" 8 150 "Voucher Report" "vwr/mnt/v-vchenq.w" yes "" "" "" no no
149 "DRL" "Resequence Account Groups" 45 88 "&Resequence" "" no "" "" "" no no
150 "DRL" "View totals of accounts" 3 112 "Totals" "vwr/drl/v-totals.w" yes "" "" "" no no
151 "DRL" "Register Invoices" 67 68 "Register" "vwr/mnt/v-ivoice.w" yes "mode = Register" "" "" no no
152 "DRL" "Display Total for Ledger Accounts" 4 112 "Ledger Total" "vwr/drl/v-totals.w" yes "" "" "" no no
154 "DRL" "Total for PROPEX Accounts of Property" 11 112 "Expense Totals" "vwr/drl/v-totals.w" yes "" "" "" no no
155 "DRL" "View Portfolio Totals for one Account" 100 112 "Portfolio Total" "vwr/drl/v-totals.w" yes "" "" "" no no
159 "DRL" "Browse the leases for this Tenant" 14 74 "Leases" "vwr/drl/b-tntlse.w" yes "Key-Name = TenantCode,NotesViewer = Yes,FilterBy-Case = All leases" "" "" no no
160 "DRL" "Maintain the details for this Tenant record" 14 116 "Maintain" "vwr/mnt/v-tenant.w" yes "mode = Maintain" "" "" no no
165 "SEL" "Select Related Entity for Tenant" 118 23 "" "vwr/sel/b-selpro.w" yes "" "fil_Entity" "EntityCode" no no
167 "MNU" "Menu of Accounting Reports" 8 121 "Accounting Reports" "" no "" "" "" no no
169 "DRL" "Report of transactions for a tenant account" 8 119 "Tenant Trans" "vwr/mnt/v-tr-t.w" yes "" "" "" no no
170 "SEL" "Select first tenant" 123 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes, SearchPanel = Yes" "fil_tenant1" "Int1" no no
171 "SEL" "Select other tenant" 123 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes, SearchPanel = Yes" "fil_tenant2" "Int2" no no
174 "SEL" "Select first company" 123 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_comp1" "Int5" no no
175 "SEL" "Select other company" 123 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_comp2" "Int6" no no
178 "DRL" "Report which shows transactions against Creditor accounts" 8 124 "Creditor Trans" "vwr/mnt/v-tr-c.w" yes "" "" "" no no
179 "SEL" "Creditor1 for Trans Report" 125 21 "" "vwr/sel/b-selcrd.w" yes "FilterPanel = Yes,SearchPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,AlphabetPanel2 = Yes,Fill-In = fil_cred1,Field = Int1" "fil_cred1" "Int1" no no
180 "SEL" "Creditor2 for Trans Report" 125 21 "" "vwr/sel/b-selcrd.w" yes "FilterPanel = Yes,SearchPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,AlphabetPanel2 = Yes,Fill-In = fil_cred2,Field = Int2" "fil_cred2" "Int2" no no
181 "DRL" "Maintain the current batch as a batch of receipts" 9 126 "Receipts" "" no "mode = Receipt" "" "" no no
182 "DRL" "Maintain the current batch as a batch of payments" 9 126 "Payments" "" no "mode = Payment" "" "" no no
183 "DRL" "Report of transactions for GL Accounts (many options)" 8 127 "GL Trans" "vwr/mnt/v-tr-l.w" yes "" "" "" no no
184 "SEL" "Select first company" 128 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_comp1" "Int1" no no
185 "SEL" "Select other company" 128 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_comp2" "Int2" no no
186 "SEL" "Select first account" 128 20 "" "vwr/sel/b-selcoa.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_account1" "Dec1" no no
187 "SEL" "Select other account" 128 20 "" "vwr/sel/b-selcoa.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_account2" "Dec2" no no
188 "DRL" "Update the current batch of transactions" 9 129 "Update" "vwr/mnt/v-kcktru.w" yes "" "" "" no no
190 "DRL" "Print the current batch of transactions" 9 131 "Print" "vwr/mnt/v-kckprt.w" yes "" "" "" no no
191 "MSG" "Create a new batch record" 9 1 "&Add" "" no "Message = add-record" "" "" no no
192 "DRL" "Add a new frequency type" 101 102 "Add" "vwr/mnt/v-frqtyp.w" yes "mode = Add" "" "" no no
193 "DRL" "Approve Invoices" 67 134 "Approval" "vwr/mnt/v-invapp.w" yes "" "" "" no no
195 "MSG" "Add a new office control account" 135 1 "&Add" "" no "Message = add-record" "" "" no no
197 "MSG" "Add a new office sequence record" 136 1 "&Add" "" no "Message = add-record" "" "" no no
198 "MSG" "Delete an office sequence" 136 1 "Delete" "" no "Message = delete-record" "" "" no no
199 "MSG" "Delete an office control" 135 1 "Delete" "" no "Message = delete-record" "" "" no no
202 "MSG" "Delete the current batch of transactions" 9 1 "Delete" "" no "Message = delete-record" "" "" no no
203 "MSG" "Modify the batch type and description for the current batch" 9 1 "Modify" "" no "Message = reset-record" "" "" no no
206 "DRL" "Browse the transactions making up this account balance" 1 7 "Transactions" "vwr/drl/b-actacx.w" yes "FilterPanel = Yes" "" "" no no
207 "MSG" "Refresh the records in this browser" 9 1 "Refresh" "" no "Message = open-query" "" "" no no
212 "MSG" "Confirm changes to an approver" 52 1 "&OK" "" no "Message = confirm-changes" "" "" no no
213 "MSG" "Cancel changes to an Approver" 52 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
215 "DRL" "Lets you choose which printer your report output should go to" 8 137 "Select Printer" "vwr/mnt/v-selprt.w" yes "" "" "" no no
217 "MNU" "Programs controlling the setup of menu options" 8 138 "Menu Administration" "" yes "" "" "" no no
218 "DRL" "Browse the month by month account balances for this creditor" 6 1 "Balances" "vwr/drl/b-actbal.w" yes "NotesViewer = Yes,Key-Name = CreditorCode" "" "" no no
219 "DRL" "Browse user groups" 8 139 "User Groups" "sec/b-usrgrp.w" yes "SortPanel = Yes" "" "" no no
220 "MSG" "Add User Group" 139 1 "&Add" "" no "Message = add-record" "" "" no no
221 "MSG" "Delete User Group" 139 1 "Delete" "" no "Message = delete-record" "" "" no no
222 "DRL" "Browse transactions against the current Creditor" 6 7 "Transactions" "vwr/drl/b-actacx.w" yes "SortPanel = Yes,FilterPanel = Yes" "" "" no no
223 "DRL" "Report showing Contacts of selected types" 8 140 "Contact Listing" "vwr/mnt/v-ctlst.w" yes "" "" "" no no
224 "DRL" "Process which prints Cheques for Approved Vouchers" 8 141 "Cheque Run" "vwr/mnt/v-chqrun.w" yes "" "" "" no no
225 "DRL" "Listing of basic tenant details" 8 134 "Tenant Listing" "vwr/mnt/v-tntlst.w" yes "" "" "" no no
226 "DRL" "Documents of batch" 142 144 "Documents" "vwr/drl/b-docbch.w" yes "" "" "" no no
227 "DRL" "Browse Batches which have been updated" 8 142 "Posted Batches" "vwr/drl/b-batch0.w" yes "" "" "" no no
229 "DRL" "Report which lists Company Records" 8 134 "Company Listing" "vwr/mnt/v-cmplst.w" yes "" "" "" no no
230 "DRL" "Report listing Chart of Accounts definitions" 8 134 "COA Listing" "vwr/mnt/v-coalst.w" yes "" "" "" no no
232 "DRL" "Report showing transactions for Property Accounts" 8 127 "Prop Trans" "vwr/mnt/v-tr-p.w" yes "" "" "" no no
233 "SEL" "Select first account" 145 20 "" "vwr/sel/b-selcoa.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = PROPEX - Property Expenses" "fil_account1" "Dec1" no no
234 "SEL" "Select other account" 145 20 "" "vwr/sel/b-selcoa.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = PROPEX - Property Expenses" "fil_account2" "Dec2" no no
235 "SEL" "Select first property" 145 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop1" "Int1" no no
236 "SEL" "Select other link" 145 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop2" "Int2" no no
239 "DRL" "View the details for this company" 4 148 "View" "vwr/mnt/v-compny.w" yes "" "" "" no no
240 "DRL" "Electricity Invoice Registration" 67 134 "Electricity Invoices" "vwr/mnt/v-elcinv.w" yes "" "" "" no no
241 "SEL" "Select a meter for an electricity invoice" 149 147 "" "vwr/sel/b-selmtr.w" yes "Fill-In = fil_meter,Key-Name = PropertyCode,Field = loc_meter" "fil_meter" "loc_meter" no no
243 "DRL" "Reports and Exports of GL balances summarised by account group" 8 150 "TB by Group" "vwr/mnt/v-tbgrpt.w" yes "" "" "" no no
244 "SEL" "Select company for report" 151 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_comp1" "Int1" no no
245 "DRL" "Transactions of Document" 144 44 "Transactions" "vwr/drl/b-actdoc.w" yes "Key-Name = BatchCode" "" "" no no
246 "DRL" "Browse balances by month for the account this transaction is against" 44 1 "Entity by Month" "vwr/drl/b-actbal.w" yes "NotesViewer = Yes,Key-Name = EntityCode" "" "" no no
247 "DRL" "Add a New Creditor" 6 153 "Add" "vwr/mnt/v-crdtor.w" yes "mode = Add" "" "" no no
248 "SEL" "Select First Property" 123 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop1" "Int5" no no
249 "SEL" "Select Other Property" 123 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop2" "Int6" no no
250 "SEL" "Select Tenant for an Invoice" 69 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes, SearchPanel = Yes" "fil_Tenant" "EntityCode" no no
251 "DRL" "Browse all Documents for the batch this transaction updated with" 44 144 "Relate by Batch" "vwr/drl/b-docact.w" yes "" "" "" no no
253 "DRL" "Add a new creditor" 21 153 "Add" "vwr/mnt/v-crdtor.w" yes "mode = Add" "" "" no no
254 "DRL" "Maintain details for the current Creditor" 6 153 "Maintain" "vwr/mnt/v-crdtor.w" yes "mode = Maintain" "" "" no no
257 "MSG" "OK Creditor Changes" 153 1 "&OK" "" no "Message = confirm-changes" "" "" no no
258 "MSG" "Cancel creditor changes" 153 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
259 "DRL" "Close Transactions for this Tenant" 14 155 "Transaction Closing" "" no "" "" "" no no
260 "DRL" "Close Transactions for this Creditor" 6 155 "Tran. Closing" "" no "" "" "" no no
261 "DRL" "Process which automatically closes all transactions where tenant or creditor balance is zero." 8 134 "Auto Close" "vwr/mnt/v-autcls.w" yes "" "" "" no no
262 "SEL" "Select a Property for a Statement" 157 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property,Field = Int1" "fil_Property" "Int1" no no
263 "DRL" "Report showing unallocated, or partially allocated tenant transactions" 8 150 "Debtors Report" "vwr/mnt/v-dbtrpt.w" yes "" "" "" no no
265 "MSG" "OK - Tenant Maintenance" 116 1 "&OK" "" no "Message = confirm-changes" "" "" no no
266 "MSG" "Cancel - Tenant Maintenance" 116 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
267 "DRL" "Report(s) providing details for a property of all of the leases and rental areas" 8 150 "Property Schedule" "vwr/mnt/v-schdul.w" yes "" "" "" no no
268 "SEL" "Select FROM Property for a Property Schedule" 160 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop1" "Int1" no no
269 "SEL" "Select TO Property for a Property Schedule" 160 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop2" "Int2" no no
273 "DRL" "Maintain this Creditor" 21 153 "Maintain" "vwr/mnt/v-crdtor.w" yes "mode = Maintain" "" "" no no
274 "DRL" "Process which generates a batch of transactions of rent and/or outgoings charges" 8 134 "Rent + Outgoings" "vwr/mnt/v-rntchg.w" yes "" "" "" no no
275 "DRL" "Browse all Cheques" 8 161 "Cheques" "vwr/drl/b-cheque.w" yes "FilterPanel = Yes,SearchPanel = Yes,Key-Name = " "" "" no yes
276 "DRL" "Browse Rental Spaces of this Property" 11 13 "Rental Spaces" "vwr/drl/b-rntspc.w" yes "Key-Name = PropertyCode,FilterBy-Case = All,NotesViewer = Yes,SortPanel = Yes,FilterPanel = Yes" "" "" yes yes
277 "DRL" "Add a Rental Space" 13 58 "Add" "vwr/mnt/v-rntspc.w" yes "mode = Add" "" "" no no
278 "DRL" "Maintain the current Rental Space record" 13 58 "Maintain" "vwr/mnt/v-rntspc.w" yes "Mode = Maintain" "" "" no no
279 "DRL" "Add a Tenant" 14 116 "Add" "vwr/mnt/v-tenant.w" yes "mode = Add" "" "" no no
280 "DRL" "Add a TenancyLease" 74 62 "Add" "vwr/mnt/v-tntlse.w" yes "mode = Add" "" "" no no
281 "DRL" "Browse Rental Spaces of TenancyLease" 74 13 "Rental Spaces" "vwr/drl/b-rntspc.w" yes "Key-Name = TenancyLeaseCode,NotesViewer = Yes,FilterBy-Case = All" "" "" no no
282 "MSG" "OK - Rental Space Maintain" 58 1 "&OK" "" no "Message = confirm-changes" "" "" no no
283 "MSG" "Cancel - Rental Space Maintenance" 58 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
284 "MSG" "OK - Tenancy Lease Maintenance" 62 1 "&OK" "" no "Message = confirm-changes" "" "" no no
285 "MSG" "Cancel - TenancyLease Maintenance" 62 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
286 "DRL" "Report summarising (or detailing) Rent/Outgoings payable" 8 150 "Rental Summary" "vwr/mnt/v-rntsum.w" yes "" "" "" no no
287 "MSG" "Delete this Rental Space" 13 1 "Delete" "" no "Message = delete-record" "" "" no no
290 "DRL" "Sub-Leases of a Lease" 74 164 "Sub Leases" "vwr/drl/b-subtls.w" yes "" "" "" no no
291 "DRL" "Browse records of Street Frontages for this Property" 11 165 "Street Frontages" "vwr/drl/b-stfrnt.w" yes "" "" "" no no
293 "DRL" "Add a Rent Review" 162 202 "Add" "vwr/mnt/v-rntrvw.w" yes "Mode = Add" "" "" no no
294 "MSG" "Delete a Rent Review" 162 1 "Delete" "" yes "Message = delete-record" "" "" no no
297 "MSG" "Add a Sublease for this Lease" 164 1 "&Add" "" yes "Message = add-record" "" "" no no
298 "MSG" "Delete a SubLease from this Lease" 164 1 "&Delete" "" yes "Message = delete-record" "" "" no no
299 "MSG" "Add a Street Frontage" 165 1 "&Add" "" yes "Message = add-record" "" "" no no
300 "MSG" "Delete a Street Frontage" 165 1 "&Delete" "" yes "Message = delete-record" "" "" no no
301 "DRL" "Reprint Vouchers for approval where the voucher is missing" 8 150 "Reprint Vouchers" "vwr/mnt/v-reprnt.w" yes "Reprint = Voucher" "" "" no no
302 "DRL" "Reprint Invoice Approval Forms for a range of invoices" 8 150 "Reprint Invoices" "vwr/mnt/v-reprnt.w" yes "Reprint = Invoice Approval" "" "" no no
303 "DRL" "Report showing the Weighted Average Lease Life for each property" 8 150 "W.A.L.L." "vwr/mnt/v-wall.w" yes "" "" "" no no
304 "DRL" "Report on Tenant Balances, split according to age of debt" 8 150 "Aged Balance" "vwr/mnt/v-tntbal.w" yes "" "" "" no no
305 "DRL" "Reprint Posted Batch" 142 150 "Reprint" "vwr/mnt/v-treprt.w" yes "" "" "" no no
306 "MSG" "OK - Property Maintenance" 57 1 "&OK" "" no "Message = confirm-changes" "" "" no no
307 "MSG" "Cancel - Property Maintenance" 57 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
308 "MSG" "Delete a TenancyLease" 74 1 "Delete" "" yes "Message = delete-record" "" "" no no
309 "MSG" "Refresh the records in this browser" 74 1 "Refresh" "" no "Message = open-query" "" "" no no
310 "MSG" "Refresh the records in this browser" 13 1 "Refresh" "" no "Message = open-query" "" "" no no
311 "DRL" "Browse Invoices for this Tenant" 14 67 "Invoices" "vwr/drl/b-ivoice.w" yes "FilterPanel = Yes,Key-Name = TenantCode" "" "" yes yes
312 "MNT" "Add a new Property" 11 57 "Add" "vwr/mnt/v-proper.w" yes "mode = Add" "" "" no no
313 "MSG" "Add a meter" 147 1 "&Add" "" no "Message = add-record" "" "" no no
314 "MSG" "Delete a meter" 147 1 "&Delete" "" no "Message = delete-record" "" "" no no
315 "DRL" "Process which allows you to apply system changes from an update file." 8 134 "System Update" "vwr/mnt/v-implnk.w" yes "" "" "" no no
316 "DRL" "Report showing Rent Reviews not yet complete as well as those due" 8 134 "Rent Reviews" "vwr/mnt/v-rrvrpt.w" yes "" "" "" no no
317 "DRL" "Report detailing vacant rental space" 8 134 "Vacant Spaces Report (old)" "vwr/mnt/v-genrpt.w" yes "ReportTitle = Vacant Spaces Report,ReportName = vcntspce.p" "" "" no no
318 "DRL" "Report detail a tenants outstanding transactions in a presentable form" 8 150 "Statement of Account" "vwr/mnt/v-statmt.w" yes "" "" "" no no
319 "SEL" "Select Tenant for a Stement of Account" 157 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes, SearchPanel = Yes" "fil_Tenant" "Int2" no no
320 "DRL" "Report showing O/G percentages applying to the various tenants of a property (obsolete)" 8 150 "Outgoings %ages" "vwr/mnt/v-ogperc.w" yes "" "" "" no no
321 "DRL" "Report summarising the types of areas rented by region" 8 134 "Rental Area" "vwr/mnt/v-genrpt.w" yes "ReportTitle = Rental Area Breakdown, ReportName = rntlarea.p" "" "" no no
322 "DRL" "Report on Leases Expiring over a future period" 8 134 "Leases Expiring" "vwr/mnt/v-lsxpry.w" yes "" "" "" no no
324 "MNU" "Menu of reports generally related to Property Management" 8 166 "Property Reports" "" no "" "" "" no no
325 "DRL" "Report summarising building income and expense categorised and grouped by account" 8 134 "Building Income Stmt" "vwr/mnt/v-bldinc.w" yes "" "" "" no no
326 "SEL" "Select the starting property for the building income report" 158 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_PropertyFrom,Field = Int1" "fil_PropertyFrom" "Int1" yes yes
327 "SEL" "Select the finishing prpoerty for the building income report" 158 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_PropertyTo,Field = Int2" "fil_PropertyTo" "Int2" yes yes
328 "DRL" "Reprint previously approved Tax Invoices" 8 150 "Reprint Tax Invoices" "vwr/mnt/v-reprnt.w" yes "Reprint = Tax Invoice" "" "" no no
329 "DRL" "Add a Company" 4 59 "Add" "vwr/mnt/v-compny.w" yes "mode = Add" "" "" no no
330 "DRL" "Browse Bank Account definitions" 8 167 "Bank Accounts" "vwr/drl/b-bnkact.w" yes "FilterPanel = Yes" "" "" no no
331 "DRL" "Maintain Bank Accounts" 167 80 "Maintain" "vwr/mnt/v-bnkact.w" yes "mode = Maintain" "" "" no no
333 "MSG" "Confirm changes to a COA" 54 1 "&OK" "" no "Message = confirm-changes" "" "" no no
334 "MSG" "Cancel changes to a COA" 54 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
335 "DRL" "Add a Person" 108 168 "&Add" "vwr/mnt/v-person.w" yes "mode = Add" "" "" no no
336 "DRL" "Maintain a Person" 108 168 "Maintain" "vwr/mnt/v-person.w" yes "mode = Maintain, Key-Name = PersonCode" "" "" no no
337 "MSG" "Confirm changes to a Person" 168 1 "&OK" "" no "Message = confirm-changes" "" "" no no
338 "MSG" "Cancel changes to a Person" 168 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
340 "DRL" "Report listing basic Property information" 8 134 "Property Listing" "vwr/mnt/v-prplst.w" yes "" "" "" no no
344 "DRL" "Guarantees of TenancyLease" 74 171 "Guarantees" "vwr/drl/b-gtrtls.w" yes "" "" "" no no
345 "DRL" "Export the current menu and window definition records for transfer to another site" 8 134 "Export Links" "vwr/mnt/v-explnk.w" yes "" "" "" no no
346 "DRL" "Add a Bank Account" 167 80 "Add" "vwr/mnt/v-bnkact.w" yes "mode = Add" "" "" no no
347 "MSG" "Confirm changes to a bank account" 80 1 "&OK" "" no "Message = confirm-changes" "" "" no no
348 "MSG" "Cancel Changes to a Bank Account" 80 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
349 "DRL" "Maintain a Guarantor" 171 202 "Maintain" "vwr/mnt/v-garntr.w" yes "mode = Maintain" "" "" no no
350 "DRL" "Add a Guarantor" 171 202 "Add" "vwr/mnt/v-garntr.w" yes "mode = Add" "" "" no no
351 "MSG" "Confirm changes to a Guarantor" 172 1 "&OK" "" no "Message = confirm-changes" "" "" no no
352 "MSG" "Cancel changes to a Guarantor" 172 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
353 "DRL" "Tool for modifying and copying the transactions of this batch in various ways" 9 134 "Batch tool" "vwr/mnt/v-nbctol.w" yes "" "" "" no no
354 "DRL" "Register Vouchers (with allocations)" 15 63 "Register" "vwr/mnt/v-vouchr.w" yes "mode = Register" "" "" no no
355 "DRL" "Maintain this Voucher (and it's allocations)" 15 63 "Maintain" "vwr/mnt/v-vouchr.w" yes "mode = Maintain" "" "" no no
358 "DRL" "Voucher Approval (Allocations at Registration stage)" 15 134 "Multi-Approval" "vwr/mnt/v-vchap2.w" yes "" "" "" no no
359 "DRL" "Report showing the deposits in the current batch, to accompany actual deposit" 9 150 "Deposit Report" "vwr/mnt/v-depsit.w" yes "" "" "" no no
360 "DRL" "Maintain this Approver" 17 52 "Maintain" "vwr/mnt/v-apprvr.w" yes "mode = Maintain" "" "" no no
361 "DRL" "Browse records of Consolidation Lists" 8 174 "Consolidation Lists" "vwr/drl/b-conlst.w" yes "" "" "" no no
362 "DRL" "Add a Consolidation List" 174 175 "Add" "vwr/mnt/v-conlst.w" yes "mode = Add" "" "" no no
363 "DRL" "Maintain a Consolidation List" 174 175 "Maintain" "vwr/mnt/v-conlst.w" yes "mode = Maintain" "" "" no no
364 "MSG" "Confirm changes to a consolidation List" 175 1 "&OK" "" no "Message = confirm-changes" "" "" no no
365 "MSG" "Cancel changes to a Consolidation List" 175 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
366 "MSG" "Delete a Consolidation List" 174 1 "Delete" "" no "Message = delete-record" "" "" no no
367 "DRL" "Batch Tool" 142 134 "Batch Tool" "vwr/mnt/v-bchtol.w" yes "" "" "" no no
368 "MSG" "Delete this Approver" 17 1 "Delete" "" no "Message = delete-record" "" "" no no
369 "MSG" "Confirm changes to a frequency type" 102 1 "&OK" "" no "Message = confirm-changes" "" "" no no
370 "MSG" "Cancel changes to a Frequency Type" 102 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
371 "DRL" "Add a Contract" 5 55 "Add" "vwr/mnt/v-contrt.w" yes "mode = Add" "" "" no no
372 "DRL" "Maintain the details of this contract" 5 55 "Maintain" "vwr/mnt/v-contrt.w" yes "mode = Maintain" "" "" no no
373 "MSG" "Confirm changes to a service contract" 55 1 "&OK" "" no "Message = confirm-changes" "" "" no no
374 "MSG" "Cancel changes to a service contract" 55 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
375 "DRL" "Browse (and Maintain) Area Types allowed for Rental Areas" 8 176 "Area Types" "vwr/drl/b-aretyp.w" yes "" "" "" no no
376 "MSG" "Add an Area type" 176 1 "&Add" "" no "Message = add-record" "" "" no no
377 "MSG" "Delete an Area Type" 176 1 "Delete" "" no "Message = delete-record" "" "" no no
378 "DRL" "Import Contact information from a Microsoft Schedule+ (*.SC2) export file" 8 134 "Import Contacts" "vwr/mnt/v-impctc.w" yes "" "" "" no no
385 "DRL" "Browse (and Maintain) Types of Contacts" 8 178 "Contact Types" "vwr/drl/b-ctctyp.w" yes "" "" "" no no
386 "MSG" "Add a Contact Type" 178 1 "&Add" "" no "Message = add-record" "" "" no no
387 "MSG" "Delete a Contact Type" 178 1 "Delete" "" no "Message = delete-record" "" "" no no
388 "DRL" "Process which allows you to reprint selected Cheques" 8 141 "Renumber Cheques" "vwr/mnt/v-chqrep.w" yes "" "" "" no no
389 "DRL" "Browse the cheques issued to this creditor" 6 161 "Cheques" "vwr/drl/b-cheque.w" yes "Key-Name = CreditorCode, FilterPanel=Yes" "" "" no no
390 "DRL" "Browse Vouchers of A Cheque" 161 15 "Vouchers" "vwr/drl/b-vouchr.w" yes "Key-Name = ChequeNo" "" "" no no
391 "DRL" "Cheque Viewing / Maintenance" 161 179 "View" "vwr/mnt/v-cheque.w" yes "" "" "" no no
392 "MSG" "Cancel this cheque if all transactions have been updated" 179 1 "Reverse Cheque" "" yes "Message = cancel-cheque" "" "" no no
393 "DRL" "Report showing Service Contracts by building or by contractor" 8 134 "Contracts Listing" "vwr/mnt/v-ctrlst.w" yes "" "" "" no no
394 "DRL" "Maintain Outgoings of a Tenancy Lease" 74 180 "Outgoings" "vwr/mnt/v-tntls2.w" no "" "" "" no no
395 "DRL" "Report on Creditor Transactions where the payment is in a different period to the charge" 8 150 "Cross-Period Report" "vwr/mnt/v-xperod.w" yes "" "" "" no no
396 "DRL" "Maintain Budgets for Property or Company (GL) Accounts" 8 134 "Budgets" "vwr/mnt/v-budget.w" yes "" "" "" no no
397 "SEL" "Select other company in range for report" 151 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_comp2" "Int2" no no
398 "SEL" "Select a property which this lease is for areas in" 34 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_property" "PropertyCode" no no
400 "DRL" "Export core contact information to a file for importing into other applications" 8 134 "Export Contacts" "vwr/mnt/v-expctc.w" yes "" "" "" no no
401 "DRL" "Maintain the comments appearing on the various management reports" 8 134 "Mgmt Rpt Comment" "vwr/mnt/v-prdcom.w" yes "" "" "" no no
402 "DRL" "Management Report - New Leases" 8 150 "New Leases" "vwr/mnt/v-mgnlse.w" yes "" "" "" no no
403 "MNU" "Menu of Miscellaneous Reports" 8 181 "General Exports" "" no "" "" "" no no
404 "DRL" "Process a file downloaded from the bank, marking presented cheques appropriately" 8 134 "Process Bank File" "vwr/mnt/v-impanz.w" yes "" "" "" no no
405 "MSG" "Delete a Person" 108 1 "Delete" "" no "Message = delete-record" "" "" no no
406 "MSG" "Delete a Contract" 5 1 "Delete" "" no "Message = delete-record" "" "" no no
407 "DRL" "Process which runs a program by name" 8 134 "Run Program" "vwr/mnt/v-runprg.w" yes "" "" "" no no
408 "DRL" "Report listing cheques generated but not yet presented" 8 134 "Unpresented Cheques" "vwr/mnt/v-unpchq.w" yes "" "" "" no no
409 "MSG" "Cancel this Voucher" 15 1 "Cancel Voucher" "" no "Message = cancel-voucher" "" "" no no
410 "MSG" "Hold Payment on a voucher" 15 1 "Hold Payment" "" yes "Message = hold-voucher" "" "" no no
411 "MSG" "Change Held Voucher back to Approved" 15 1 "Un-Hold" "" yes "Message = unhold-voucher" "" "" no no
412 "DRL" "Process which generates transfers to retained earnings at year end" 8 134 "Year End Routine" "vwr/mnt/v-yrend.w" yes "" "" "" no no
413 "DRL" "Add a Project" 183 182 "Add" "vwr/mnt/v-projct.w" yes "mode = Add" "" "" no no
414 "DRL" "Maintain a Project" 183 182 "Maintain" "vwr/mnt/v-projct.w" yes "mode = Maintain" "" "" no no
415 "DRL" "Browse records of Projects and approved expenditure" 8 183 "Projects" "vwr/drl/b-projct.w" yes "SortPanel = Yes,OptionPanel = Select,FilterPanel = Yes,AlphabetPanel = Yes" "" "" yes yes
417 "MSG" "Confirm changes to a Project" 182 1 "&OK" "" yes "Message = confirm-changes" "" "" no no
418 "MSG" "Cancel changes to a project" 182 1 "&Cancel" "" yes "Message = cancel-changes" "" "" no no
419 "SEL" "Select a Proposer for a Project" 184 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = SF   - Staff" "fil_Proposer" "Proposer" no no
422 "SEL" "Select the first Approver for a Project" 184 18 "" "vwr/sel/b-selapp.w" yes "" "fil_Approver1" "FirstApprover" no no
423 "SEL" "Select the second Approver for a Project" 184 18 "" "vwr/sel/b-selapp.w" yes "" "fil_Approver2" "SecondApprover" no no
425 "MSG" "Refresh the records in this browser" 17 1 "Refresh" "" no "Message = open-query" "" "" no no
426 "DRL" "Export of Contacts for Property" 8 134 "Export Prop. Contact" "vwr/mnt/v-expct2.w" yes "" "" "" no no
427 "DRL" "Browse the service contracts for this Creditor" 6 5 "Serv. Contracts" "vwr/drl/b-contrt.w" yes "SortPanel = Yes,NotesViewer = Yes,FilterPanel = Yes,Key-Name = CreditorCode" "" "" yes yes
428 "DRL" "Report showing budgets for the accounts of a Property or Company" 8 150 "Entity Budget" "vwr/mnt/v-budrpt.w" yes "" "" "" no no
429 "DRL" "Report which summarises the items being paid by a range of cheques" 8 150 "Payment Summary" "vwr/mnt/v-pmtsum.w" yes "" "" "" no no
432 "MSG" "Add a valuation" 187 1 "&Add-b" "" no "Message = add-record" "" "" no no
434 "DRL" "Browse Valuations of Property" 11 187 "Valuations" "vwr/drl/b-valutn.w" yes "Key-Name = PropertyCode,FilterPanel = Yes" "" "" no no
435 "DRL" "Viewer which shows Net Property Income and valuation related information" 11 134 "NPI Thumbnail" "vwr/drl/v-ross.w" yes "" "" "" no no
436 "DRL" "Orders of a Project" 183 188 "Orders" "vwr/drl/b-ordprj.w" yes "Key-Name = ProjectCode,FilterPanel = Yes" "" "" no no
437 "DRL" "Add an Order" 188 189 "Add" "vwr/mnt/v-order.w" yes "mode = Add" "" "" no no
438 "DRL" "Maintain an Order" 188 189 "Maintain" "vwr/mnt/v-order.w" yes "mode = Maintain" "" "" no no
439 "MSG" "Confirm changes to an Order" 189 1 "&OK" "" yes "Message = confirm-changes" "" "" no no
440 "MSG" "Cancel changes to an order" 189 1 "&Cancel" "" yes "Message = cancel-changes" "" "" no no
441 "SEL" "Select Approver for an Order" 191 18 "" "vwr/sel/b-selapp.w" yes "Fill-In = fil_Approver1,Field = FirstApprover" "fil_Approver1" "FirstApprover" no no
442 "SEL" "Select Creditor for an Order" 191 21 "" "vwr/sel/b-selcrd.w" yes "FilterPanel = Yes,SearchPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,AlphabetPanel2 = Yes,Fill-In = fil_Creditor,Field = CreditorCode" "fil_Creditor" "CreditorCode" no no
443 "DRL" "Budgets of a Project" 183 192 "Budgets" "vwr/drl/b-prjbdg.w" yes "Key-Name = ProjectCode" "" "" no no
444 "DRL" "Management Report - Vacant Space" 8 150 "Vacant Space" "vwr/mnt/v-mgvcnt.w" yes "" "" "" no no
445 "MNU" "Menu of reports which list system codes" 8 194 "Code Listings" "" no "" "" "" no no
446 "MNU" "Menu of Budget Reports" 8 186 "Budget Reports" "" no "" "" "" no no
447 "MNT" "Menu of Management Reports" 8 193 "Management Reports" "" no "" "" "" no no
448 "DRL" "Add a Project Budget" 192 195 "Add" "vwr/mnt/v-prjbdg.w" yes "mode = Add" "" "" no no
449 "DRL" "Maintain a Project Budget" 192 195 "Maintain" "vwr/mnt/v-prjbdg.w" yes "mode = Maintain" "" "" no no
450 "MSG" "Confirm changes to a Project Budget" 195 1 "&OK" "" yes "Message = confirm-changes" "" "" no no
451 "MSG" "Cancel changes to a project Budget" 195 1 "&Cancel" "" yes "Message = cancel-changes" "" "" no no
452 "SEL" "Select an Entity for a Project Budget" 196 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_Entity" "EntityCode" no no
453 "SEL" "Select the Account for a Project Budget" 196 20 "" "vwr/sel/b-selcoa.w" yes "SortPanel = Yes,FilterPanel = Yes" "Description" "AccountCode" no no
454 "SEL" "Select Entity Account for a Project Budget" 196 20 "" "vwr/sel/b-selcoa.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_EAccount" "EntityAccount" no no
455 "SEL" "Select an Entity for a Project" 184 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_Entity" "EntityCode" no no
456 "DRL" "Modify Project Budget Cash Flows" 192 197 "Cash Flows" "vwr/mnt/v-prjcsh.w" yes "" "" "" no no
457 "DRL" "View various valuations for this property" 57 198 "View Valuations" "vwr/mnt/v-proval.w" yes "mode = View,Key-Name = PropertyCode" "" "" no no
459 "DRL" "Browse Ground Leases of this Property" 11 201 "Ground Leases" "vwr/drl/b-grdlse.w" yes "Key-Name = PropertyCode" "" "" no no
460 "SEL" "Select a Creditor for a Ground Lease" 199 21 "" "vwr/sel/b-selcrd.w" yes "FilterPanel = Yes,SearchPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,AlphabetPanel2 = Yes,Fill-In = fil_Creditor,Field = Lessor" "fil_Creditor" "Lessor" no no
461 "DRL" "Add a Valuation" 187 202 "Add" "vwr/mnt/v-valuation.w" yes "mode = Add" "" "" no no
462 "DRL" "Add a Ground Lease" 201 202 "Add" "vwr/mnt/v-grdlse.w" yes "mode = Add" "" "" no no
463 "DRL" "Maintain a Ground Lease" 201 202 "Maintain" "vwr/mnt/v-grdlse.w" yes "mode = Maintain" "" "" no no
464 "MSG" "OK - Confirm Changes" 202 1 "&OK" "" no "Message = confirm-changes" "" "" no no
465 "MSG" "Cancel - Cancel changes" 202 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
466 "DRL" "Maintain Sale/Purchase Details for Property" 57 202 "Sale/Purchase" "vwr/mnt/v-prospd.w" yes "mode = Maintain" "" "" no no
467 "DRL" "Report on a Project, Tracking actual expenditure against expected values" 8 134 "Project Tracking" "vwr/mnt/v-prjtrk.w" yes "" "" "" no no
468 "SEL" "Select first project for the tracking report" 203 190 "" "vwr/sel/b-selprj.w" yes "Fill-In = fil_Project1,Field = Int1" "fil_Project1" "Int1" no no
469 "DRL" "Report on the Cash Flows for a Project" 8 150 "Project Cash Flows" "vwr/mnt/v-prjcfl.w" yes "" "" "" no no
470 "SEL" "Select a Project for the Report" 204 190 "" "vwr/sel/b-selprj.w" yes "Fill-In = fil_Project1,Field = Int1" "fil_Project1" "Int1" no no
471 "SEL" "Select the Contact record for this Approver" 205 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = SF   - Staff,Fill-In = fil_Person,Field = PersonCode,Fill-In = fil_Person,Field = PersonCode,Fill-In = fil_Person,Field = PersonCode,Fill-In = fil_Person,Field = PersonCode,Fill-In = fil_Person,Field = PersonCode,Fill-In = fil_Person,Field = PersonCode,Fill-In = fil_Person,Field = PersonCode,Fill-In = fil_Person,Field = PersonCode,Fill-In = fil_Person,Field = PersonCode" "fil_Person" "PersonCode" yes yes
472 "DRL" "Add an Approver" 18 52 "Add" "vwr/mnt/v-apprvr.w" yes "mode = Add" "" "" no no
473 "DRL" "Add a Contact Person" 24 168 "&Add" "vwr/mnt/v-person.w" yes "mode = Add" "" "" no no
474 "DRL" "Maintain this Contact Person" 24 168 "Maintain" "vwr/mnt/v-person.w" yes "mode = Maintain, Key-Name = PersonCode" "" "" no no
476 "DRL" "Process which creates a batch of transactions allocating tenants share of FID Charges (Aust.)" 8 150 "FID Charges" "vwr/mnt/v-fidchg.w" yes "" "" "" no no
477 "SEL" "Select first property for FID charges" 206 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_Property-1" "Int1" no no
478 "SEL" "Select other property for FID charges" 206 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_Property-2" "Int2" no no
479 "MSG" "Delete a Project" 183 1 "Delete" "" no "Message = delete-record" "" "" no no
480 "MSG" "Delete an Order" 188 1 "Delete" "" no "Message = delete-record" "" "" no no
481 "MSG" "Delete the current creditor" 6 1 "Delete" "" no "Message = delete-record" "" "" no no
482 "DRL" "Add a tenant" 64 116 "Add" "vwr/mnt/v-tenant.w" yes "mode = Add" "" "" no no
483 "DRL" "Maintain a Tenant" 64 116 "Maintain" "vwr/mnt/v-tenant.w" yes "mode = Maintain" "" "" no no
484 "DRL" "Management Report - Leases Expiring" 8 150 "Lease Expiries" "vwr/mnt/v-mglexp.w" yes "" "" "" no no
485 "DRL" "Browse months of the current financial year to open or close them" 9 207 "Open/Close Months" "vwr/drl/b-month2.w" yes "FilterPanel = Yes" "" "" no no
486 "DRL" "Browse Region definitions" 8 208 "Regions" "vwr/drl/b-region.w" yes "" "" "" no no
487 "MSG" "Add a region" 208 1 "&Add" "" no "Message = add-record" "" "" no no
488 "MSG" "Delete a region" 208 1 "&Delete" "" no "Message = delete-record" "" "" no no
489 "DRL" "User Group Menus" 139 212 "Menus" "sec/v-usgmnu.w" yes "Key-Name = GroupName" "" "" no no
490 "DRL" "Browse (and Maintain) records about users of the system" 8 210 "User Administration" "sec/b-user.w" yes "" "" "" no no
491 "MSG" "Delete a User" 210 1 "Delete" "" no "Message = delete-record" "" "" no no
492 "DRL" "Add a User" 210 211 "Add" "sec/v-user.w" yes "Key-Name = UserName, mode = Add" "" "" no no
493 "DRL" "Maintain a User" 210 211 "Maintain" "sec/v-user.w" yes "Key-Name = UserName,mode = Maintain" "" "" no no
494 "MSG" "Confirm changes to a User" 211 1 "&OK" "" no "Message = confirm-changes" "" "" no no
495 "MSG" "Cancel changes to a user" 211 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
496 "DRL" "Group Memberships for a user" 210 213 "Groups" "sec/v-usgmem.w" yes "Key-Name = UserName" "" "" no no
497 "DRL" "Produce a Property Schedule printout for this property" 11 150 "Schedule" "vwr/mnt/v-schdul.w" yes "external-key = PropertyCode" "" "" no no
498 "DRL" "Add a new Account Group" 45 202 "&Add" "vwr/mnt/v-actgrp.w" yes "mode = Add" "" "" no no
499 "DRL" "Maintain this Account Group" 45 202 "&Maintain" "vwr/mnt/v-actgrp.w" yes "mode = Maintain" "" "" no no
500 "MSG" "Delete this Account Group" 45 1 "&Delete" "" no "Message = delete-record" "" "" no no
501 "MSG" "Add a new month" 40 1 "&Add" "" no "Message = add-record" "" "" no no
502 "MSG" "Add a new financial year" 47 1 "&Add" "" no "Message = add-record" "" "" no no
503 "DRL" "Modify Contact Types of a Person" 168 134 "Contact Types" "vwr/mnt/v-ctypsn.w" yes "Key-Name = PersonCode" "" "" no no
504 "DRL" "Management Report - Approved Expenditure" 8 150 "Capex Approvals" "vwr/mnt/v-mgcapx.w" yes "" "" "" no no
505 "DRL" "User Group Rights" 139 214 "Rights" "vwr/drl/b-usgrts.w" yes "Key-Name = GroupName" "" "" no no
506 "MSG" "Add a UsrGroupRights record" 214 1 "&Add" "" no "Message = add-record" "" "" no no
507 "MSG" "Delete a UsrGroupRights record" 214 1 "&Delete" "" no "Message = delete-record" "" "" no no
508 "MSG" "Confirm changes to a Company" 59 1 "&OK" "" no "Message = confirm-changes" "" "" no no
509 "MSG" "cancel changes to a Company" 59 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
510 "SEL" "Select the Contact Person who is Secretary for this Company" 28 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = SF   - Staff,Fill-In = fil_Secretary" "fil_Secretary" "" no no
511 "DRL" "Browse the Directors of this Company" 4 215 "Directors" "vwr/drl/b-dirctr.w" yes "Key-Name = CompanyCode" "" "" no no
512 "DRL" "Add a director" 215 216 "Add" "vwr/mnt/v-dirctr.w" yes "mode = Add" "" "" no no
513 "DRL" "Maintain a Director" 215 216 "Maintain" "vwr/mnt/v-dirctr.w" yes "mode = Maintain" "" "" no no
514 "MSG" "Delete a director" 215 1 "Delete" "" no "Message = delete-record" "" "" no no
515 "MSG" "Confirm changes to a directorship" 216 1 "&OK" "" no "Message = confirm-changes" "" "" no no
516 "MSG" "Cancel changes to a directorship" 216 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
517 "SEL" "Select Company for a Directorship" 91 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_Company" "" no no
518 "SEL" "Select a Company for a Share Holder" 92 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_Company" "" no no
519 "DRL" "Browse the list of shareholders of this Company" 4 217 "Share Holders" "vwr/drl/b-shrhld.w" yes "Key-Name = CompanyCode" "" "" no no
520 "DRL" "Add a Shareholder" 217 218 "Add" "vwr/mnt/v-shrhld.w" yes "mode = Add" "" "" no no
521 "DRL" "Maintain a Share Holder" 217 218 "Maintain" "vwr/mnt/v-shrhld.w" yes "mode = Maintain" "" "" no no
522 "MSG" "Delete a Share Holder" 217 1 "Delete" "" no "Message = delete-record" "" "" no no
523 "MSG" "Confirm changes to a shareholder" 218 1 "&OK" "" no "Message = confirm-changes" "" "" no no
524 "MSG" "Cancel changes to a Share Holder" 218 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
525 "SEL" "Select Person as Insurance Broker" 219 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_Broker" "InsuranceBroker" no no
526 "DRL" "Management Report - Outstanding Rent Reviews" 8 150 "O/S Reviews" "vwr/mnt/v-mgrvwo.w" yes "" "" "" no no
527 "DRL" "Browse Vouchers of Creditor" 6 15 "Vouchers" "vwr/drl/b-vouchr.w" yes "SortPanel = Yes,FilterPanel = Yes,Key-Name = CreditorCode" "" "" no no
528 "DRL" "Browse all Vouchers (records of invoices from Creditors)" 8 15 "Vouchers" "vwr/drl/b-vouchr.w" yes "SortPanel = Yes,SearchPanel = Yes,FilterPanel = Yes,Key-Name = " "" "" yes yes
529 "DRL" "Management Report - Reviews & Renewals Actioned" 8 150 "Reviews && Renewals" "vwr/mnt/v-mgrvrn.w" yes "" "" "" no no
530 "SEL" "Select an existing Contact record for this Creditor" 29 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = ST   - Suppliers: Trade" "fil_FullName" "" no no
531 "SEL" "Select Existing Contact for Tenant" 118 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes" "fil_FullName" "" no no
532 "DRL" "Report showing Tenant Deposits and their allocations" 8 150 "Daily Deposits" "vwr/mnt/v-depsyd.w" yes "" "" "" no no
533 "SEL" "Select property for debtors report" 220 64 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes, SearchPanel = Yes" "fil_Property" "Int1" no no
535 "DRL" "Add a Scenario" 221 222 "Add" "vwr/mnt/v-scnrio.w" yes "mode = Add" "" "" no no
536 "DRL" "Maintain a Scenario" 221 222 "Maintain" "vwr/mnt/v-scnrio.w" yes "mode = Maintain" "" "" no no
537 "DRL" "Cash Flows of Scenarios" 221 223 "Cash Flows" "vwr/drl/b-cshflw.w" yes "FilterPanel = Yes,Key-Name = ScenarioCode,SearchPanel = Yes" "" "" no no
538 "MSG" "Confirm changes to a Scenario" 222 1 "&OK" "" no "Message = confirm-changes" "" "" no no
539 "MSG" "Cancel changes to a Scenario" 222 1 "&Cancel" "" no "Message = cancel-changes" "" "" no no
540 "DRL" "Browse Forecast Scenarios" 8 221 "Scenarios" "vwr/drl/b-scnrio.w" yes "" "" "" no no
541 "MSG" "Generate Cash Flows for a Scenario" 222 1 "&Generate Cash Flows" "" no "Message = generate-cash-flows" "" "" no no
542 "MSG" "Add a Service Type" 99 1 "Add" "" no "Message = add-record" "" "" no no
543 "MSG" "Delete a Service Type" 99 1 "Delete" "" no "Message = delete-record" "" "" no no
544 "DRL" "Add a cash flow" 223 224 "Add" "vwr/mnt/v-cshflw.w" yes "mode = add" "" "" no no
545 "DRL" "Maintain a Cash Flow" 223 224 "Maintain" "vwr/mnt/v-cshflw.w" yes "mode = Maintain" "" "" no no
546 "SEL" "Select an Entity for a Cash Flow" 225 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Entity,Field = EntityCode" "fil_Entity" "EntityCode" no no
547 "SEL" "Select an Account for a Cash Flow" 225 20 "" "vwr/sel/b-selcoa.w" yes "SortPanel = Yes,FilterPanel = Yes,Fill-In = fil_Account,Field = AccountCode" "fil_Account" "AccountCode" no no
548 "MSG" "Confirm changes to cheque" 179 1 "&OK" "" no "Message = confirm-changes" "" "" no no
549 "MSG" "Delete Cash Flow" 223 1 "Delete" "" no "Message = delete-record" "" "" no no
550 "DRL" "Report or Export on the forecast scenario over time" 8 226 "Forecast Report" "vwr/mnt/v-frecst.w" yes "Key-Name = ScenarioCode" "" "" no no
551 "MSG" "Mark cheque as presented yesterday" 161 1 "&Presented" "Default" no "Message = present-cheque" "" "" no no
552 "MSG" "Mark cheque as unpresented" 161 1 "&Unpresented" "" no "Message = unpresent-cheque" "" "" no no
553 "DRL" "Export Contact information for Creditors" 8 134 "Export Creditors" "vwr/mnt/v-expcrd.w" yes "" "" "" no no
554 "DRL" "Export Contact information for Contractors" 8 134 "Export Contractors" "vwr/mnt/v-expctr.w" yes "" "" "" no no
555 "DRL" "Browse Program Link records, maintaining tooltip information" 8 227 "ToolTips" "b-prglnk2" yes "Key-Name = ,SortPanel = Yes" "" "" yes no
557 "DRL" "Browse the transactions against this Project Budget" 192 7 "Transactions" "vwr/drl/b-actacx.w" yes "FilterPanel = Yes,SortPanel = Yes" "" "" yes yes
558 "SEL" "Select start project for transaction report" 228 190 "" "vwr/sel/b-selprj.w" yes "FilterPanel = Yes" "fil_proj1" "RP.Int1" no yes
559 "SEL" "Select finish project for transaction report" 228 190 "" "vwr/sel/b-selprj.w" yes "SortPanel = Yes" "fil_proj2" "RP.Int2" yes no
560 "DRL" "Report of transactions for Project Accounts" 8 127 "Project trans" "vwr/mnt/v-tr-j.w" yes "" "" "" no no
562 "MSG" "Add a new record" 177 1 "&Add" "Default" no "Message = add-record" "" "" no no
563 "MSG" "Delete the currently selected record" 177 1 "&Delete" "Default" no "Message = delete-record" "" "" no no
564 "MSG" "Save changes to the currently selected record" 177 1 "&Save" "Default" no "Message = save-record" "" "" no no
565 "MSG" "Cancel changes to the currently selected record" 177 1 "&Cancel changes" "" yes "Message = cancel-record" "" "" no no
566 "SEL" "Select first property for a Tenant Listing" 229 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Propertyfrom,Code = Int3" "fil_Propertyfrom" "" no no
567 "SEL" "Select second property for a tenant listing" 229 146 "" "" yes "SortPanel = Yes,FilterPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Propertyto,Field = Int4,Fill-In = fil_Propertyto,Field = Int4" "fil_Propertyto" "Int4" no no
568 "SEL" "Select fist tenant for a tenant listing" 229 64 "" "vwr/sel/b-seltnt.w" yes "AlphabetPanel = Yes,FilterPanel = Yes,SortPanel = Yes,Fill-In = fil_TenantFrom,Field = Int1, SearchPanel = Yes" "fil_TenantFrom" "Int1" no no
569 "SEL" "Select end tenant for this listing" 229 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes,AlphabetPanel = Yes,SortPanel = Yes,Fill-In = fil_TenantTo,Field = Int2, SearchPanel = Yes" "fil_TenantTo" "Int2" no no
570 "DRL" "Report listing all Account Groups" 8 150 "Account Group Listing" "vwr/mnt/v-grplst.w" yes "" "" "" no no
571 "DRL" "Report showing the companies and the properties which they own" 8 150 "Property Ownership Listing" "vwr/mnt/v-grpown.w" yes "" "" "" no no
573 "DRL" "Browse (and Maintain) Standard Invoice Terms" 8 177 "Invoice Terms" "vwr/drl/b-invtrm.w" yes "" "" "" no no
574 "DRL" "Browse (and Maintain) Scenario Status codes" 8 177 "Scenario Status" "vwr/drl/b-scnsts.w" yes "" "" "" no no
575 "SEL" "Select financial year for budget reporting" 230 231 "" "vwr/sel/b-selfyr.w" yes "Fill-In = fil_Year,Field = RP.Int1" "fil_Year" "RP.Int1" no no
576 "DRL" "Select the input entites for this scenario" 222 134 "Entity Filters" "vwr/mnt/v-entsel.w" yes "" "" "" no no
577 "MSG" "Convert transactions closed along with this one back to a partly-closed group" 7 1 "Convert to Part-Clos" "" yes "Message = reopen-group" "" "" no no
578 "DRL" "Approve this Voucher (and maintain allocations)" 15 63 "Approval" "vwr/mnt/v-vouchr.w" yes "Mode = Approve" "" "" no no
579 "SEL" "Select an approver of this voucher" 232 18 "" "vwr/sel/b-selapp.w" yes "Fill-In = fil_Approval,Field = ApproverCode" "fil_Approval" "ApproverCode" no no
580 "SEL" "Select a creditor for this voucher" 232 21 "" "vwr/sel/b-selcrd.w" yes "Fill-In = fil_Creditor,SearchPanel = Yes,Field = CreditorCode,SortPanel = Yes,FilterPanel = Yes,AlphabetPanel = Yes,AlphabetPanel2 = Yes" "fil_Creditor" "CreditorCode" no no
581 "SEL" "Select a batch to hold transactions for approved vouchers" 232 22 "" "vwr/sel/b-selnbc.w" yes "Fill-In = fil_batch-text,Field = fil_batch-no" "fil_batch-text" "fil_batch-no" no no
582 "DRL" "View this Voucher and Allocations" 15 63 "View" "vwr/mnt/v-vouchr.w" yes "Mode = View" "" "" no no
584 "MSG" "Reprint the current invoice" 67 1 "Reprint" "" yes "Message = reprint-invoice" "" "" no no
585 "MSG" "Reprint this voucher" 15 1 "Reprint" "" yes "Message = reprint-voucher" "" "" no no
586 "DRL" "Browse (and Maintain) Types of Cash flows for forecasting" 8 177 "Forecast CF Types" "vwr/drl/b-cfltyp.w" yes "" "" "" no no
587 "DRL" "Browse (and Maintain) Types of Cash Flow Changes" 8 177 "Forecast CF Changes" "vwr/drl/b-cfchgt.w" yes "" "" "" no no
588 "DRL" "Maintain this Account Definition record" 100 54 "Maintain" "vwr/mnt/v-choact.w" yes "Mode = Maintain" "" "" no no
589 "DRL" "Maintain the current rent review record" 162 202 "Maintain" "vwr\mnt\v-rntrvw.w" yes "Mode = Maintain" "" "" no no
591 "DRL" "Browse (and Maintain) Rent Review Status codes" 8 177 "Review Status Codes" "vwr/drl/b-rvusta.w" yes "" "" "" no no
592 "DRL" "Browse (and Maintain) Rent Review Type codes" 8 177 "Review Type Codes" "vwr/drl/b-rvutyp.w" yes "" "" "" no no
593 "SEL" "Select the current Scenario" 233 234 "" "vwr/sel/b-selscn.w" yes "Fill-In = fil_Scenario,Field = Int1" "fil_Scenario" "Int1" no no
594 "DRL" "Select the default scenario" 8 134 "Select Scenario" "vwr/mnt/v-selscn.w" yes "" "" "" no no
595 "DRL" "Browse / Maintain the Rental Assumptions for this space" 13 237 "Rental Assumptions" "vwr/drl/b-rntass.w" yes "" "" "" no no
596 "DRL" "View all transactions in the same closed group as the current one" 7 7 "View Closed Group" "vwr/drl/b-clsgrp.w" yes "Key-Name = ClosingGroup" "" "" no no
597 "SEL" "Select first property for Service Contract listing" 235 146 "" "vwr/sel/b-selpro.w" yes "AlphabetPanel = Yes,SortPanel = Yes,FilterPanel = Yes,Field = Int1,Fill-In = fil_prop1" "fil_prop1" "Int1" no no
598 "SEL" "Select other property for service contract listing" 235 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = yes,SortPanel = yes,AlphabetPanel = yes,Fill-In = fil_prop2,Field = Int2" "fil_prop2" "Int2" no no
599 "DRL" "Menu of Forecasting and Budgetting functions and reports" 8 236 "Forecasting Menu" "Default" no "" "" "" no no
600 "DRL" "Scenario Assumptions Report" 8 150 "Assumptions Report" "vwr/mnt/v-scnass.w" yes "" "" "" no no
601 "MSG" "Regenerate the base rental conditions for the currently selected scenario" 221 1 "Generate Rents" "" yes "Message = regenerate-rentals" "" "" no no
602 "DRL" "View the full details for the currently selected cash flow" 223 224 "&View" "vwr/mnt/v-cshflw.w" yes "Mode = view" "" "" no no
603 "DRL" "Copy a scenario over an existing one, or to a new one" 221 150 "Copy" "vwr/mnt/v-scntol.w" yes "Mode = Copy" "" "" no no
604 "DRL" "Estimated assumptions for the current scenario for this account" 3 237 "Scenario Assumptions" "vwr/drl/b-estass.w" yes "Key-Name = AccountSummary" "" "" no no
605 "MSG" "Add a new assumption record to the currently selected scenario" 237 1 "Add" "" yes "Message = add-record" "" "" no no
606 "MSG" "Delete the currently selected assumption" 237 1 "Delete" "" yes "Message = delete-record" "" "" no no
607 "DRL" "Create a new person with the same company details as the current person" 108 168 "New - Same Company" "vwr/mnt/v-person.w" yes "Mode = Copy" "" "" no no
608 "DRL" "Start a blank e-mail message to this contact" 108 227 "E-Mail" "vwr/mnt/v-email.w" yes "Key-Name = PersonCode" "" "" no no
609 "DRL" "Browse (and Maintain) Types of Variations" 8 177 "Variation Types" "vwr/drl/b-vartyp.w" yes "" "" "" no no
610 "DRL" "Browse (and Maintain) Types of Projects" 8 177 "Project Types" "vwr/drl/b-prjtyp.w" yes "" "" "" no no
611 "DRL" "Browse (and Maintain) Categories for Budgeted Project Expenses" 8 177 "Project Expense Categories" "vwr/drl/b-pexcat.w" yes "" "" "" no no
612 "MNU" "Options for maintaining of system codes tables" 8 238 "System Codes Tables" "" yes "" "" "" no no
613 "DRL" "Browse sub-projects of this project" 183 183 "Sub Projects" "vwr/drl/b-projct.w" yes "Key-Name = ParentProjectCode,FilterPanel = Yes" "" "" no no
614 "DRL" "Browse Projects of this Company" 4 183 "Projects" "vwr/drl/b-projct.w" yes "Key-Name = CompanyCode,FilterPanel = Yes" "" "" no no
615 "DRL" "Browse variations to this project" 183 239 "Variations" "vwr/drl/b-varitn.w" yes "Key-Name = ProjectCode" "" "" no no
616 "DRL" "Browse the cashflow effects of this variation" 239 240 "Variation Cashflows" "vwr/drl/b-varflw.w" yes "Key-Name = Variation" "" "" no no
617 "DRL" "Maintain the current variation" 239 202 "Maintain" "vwr/mnt/v-varitn.w" yes "Mode = Maintain,Key-Name = VariationCode" "" "" no no
618 "DRL" "Add a new variation to this project" 239 202 "Add" "vwr/mnt/v-varitn.w" yes "Key-Name = ProjectCode,Mode = Add" "" "" no no
619 "MSG" "Delete the current variation record" 239 1 "Delete" "" yes "Message = delete-record" "" "" no no
620 "DRL" "Add a new flow to this variation" 240 202 "Add" "vwr/mnt/v-varflw.w" yes "Mode = Add,Key-Name = VariationFlow" "" "" no no
621 "DRL" "Maintain the current variation flow" 240 202 "Maintain" "vwr/mnt/v-varflw.w" yes "Mode = Maintain,Key-Name = VariationFlow" "" "" no no
623 "MSG" "Delete the current variation flow record" 240 1 "Delete" "" yes "Message = delete-record" "" "" no no
624 "DRL" "Browse projects which relate to this property" 11 183 "Projects" "vwr/drl/b-projct.w" yes "Key-Name = PropertyCode,FilterPanel = Yes" "" "" no no
625 "SEL" "Select last project for tracking report" 203 190 "" "vwr/sel/b-selprj.w" yes "Field = Int2,Fill-In = fil_Project2" "fil_Project2" "Int2" no no
626 "DRL" "Browse the tenants (debtors) of this property" 11 14 "Tenants" "vwr/drl/b-tenant.w" yes "Key-Name = PropertyCode,FilterPanel = Yes" "" "" no no
627 "DRL" "Replication Log Browser" 8 242 "Replication Log" "rplctn/b-replog.w" yes "FilterPanel = Yes" "" "" no no
628 "DRL" "Browse (and Maintain) ""Replication In"" Rules" 8 244 "Replication In Rules" "rplctn/b-reprul.w" yes "" "" "" no no
629 "DRL" "Browse (and Maintain) ""Replication Out"" Rules" 8 243 "Replication Out" "rplctn/b-reptrg.w" yes "" "" "" no no
630 "DRL" "Replication Collision Log Browser" 8 241 "Collision Log" "rplctn/b-repcol.w" yes "" "" "" no no
631 "DRL" "Browse the ""dead"" accounts of thiis company" 4 3 "Dead Accounts" "vwr/drl/b-actold.w" yes "Key-Name = Entity" "" "" no no
632 "MSG" "Add a new rule" 243 1 "Add" "" yes "Message = add-record" "" "" no no
633 "MSG" "Delete a rule" 243 1 "Delete" "" yes "Message = delete-record" "" "" no no
634 "MSG" "Regenerate database delta" 243 1 "Regenerate Delta" "" yes "Message = regenerate-df" "" "" no no
635 "MSG" "Add a new rule" 244 1 "Add" "" yes "Message = add-record" "" "" no no
636 "MSG" "Delete a rule" 244 1 "Delete" "" yes "Message = delete-record" "" "" no no
637 "MSG" "Load replication data" 244 1 "Load" "" yes "Message = load-changes" "" "" no no
638 "MSG" "Purge the entire replication log" 242 1 "Purge Log" "" yes "Message = purge-log" "" "" no no
639 "SEL" "Select another approver of this voucher" 232 18 "" "vwr/sel/b-selapp.w" yes "Fill-In = fil_Approval-2,Field = SecondApprover" "fil_Approval-2" "SecondApprover" no no
640 "DRL" "Browse (and Maintain) all possible parameters affecting a scenario" 221 177 "Parameters" "vwr/drl/b-scnpar.w" yes "Key-Name = ScenarioCode" "" "" no no
641 "MSG" "Regenerate OPEX recoveries" 221 1 "Generate Recoveries" "" yes "Message = generate-recoveries" "" "" no no
642 "MSG" "Dump the logged database changes to a file for outward replication" 242 1 "Replicate Out" "" yes "Message = replicate-out" "" "" no no
644 "DRL" "Browse unposted cheques to fix cheque number problems resulting from interrupted printing" 167 161 "Unposted Cheques" "vwr/drl/b-chqfix.w" yes "Key-Name = BankAccountCode" "" "" no no
645 "MSG" "Delete the current project budget record" 192 1 "Delete" "" yes "Message = delete-record" "" "" no no
646 "DRL" "Set the budget and revised budget figures for future periods from the current forecasts" 8 150 "Apply to Budgets" "forecast/v-amtrust-to-budget.w" yes "" "" "" no no
647 "SEL" "Select a scenario to set apply as budget" 245 234 "" "vwr/sel/b-selscn.w" yes "Fill-In = fil_Scenario,Field = Int1" "fil_Scenario" "Int1" no no
648 "MSG" "Purge the log of replication collisions" 241 1 "Purge Log" "" yes "Message = purge-log" "" "" no no
649 "DRL" "History of changes to this Rental Space" 13 227 "History" "vwr/drl/b-rsphst.w" yes "Key-Name = RentalSpace" "" "" no no
650 "DRL" "Wizard to guide the user through the process of setting up a new lease" 8 246 "Lease Wizard" "" yes "Wizard-Name = Adding a new lease,Wizard-Viewers = vwr/wiz/lease/v-alloc-space.w||N@vwr/wiz/lease/v-charge.w||Y@vwr/wiz/lease/v-finish.w||Y@vwr/wiz/lease/v-garntr.w||N@vwr/wiz/lease/v-outgng.w||N@vwr/wiz/lease/v-renew.w||N@vwr/wiz/lease/v-rental.w||N@vwr/wiz/lease/v-review.w||N@vwr/wiz/lease/v-space.w||Y@vwr/wiz/lease/v-tenant.w||Y@vwr/wiz/lease/v-term.w||N@vwr/wiz/lease/v-type.w||N,Viewer-Links = vwr/wiz/lease/v-tenant.w|vwr/wiz/lease/v-space.w@vwr/wiz/lease/v-space.w|vwr/wiz/lease/v-term.w@vwr/wiz/lease/v-term.w|vwr/wiz/lease/v-type.w@vwr/wiz/lease/v-type.w|vwr/wiz/lease/v-renew.w@vwr/wiz/lease/v-renew.w|vwr/wiz/lease/v-rental.w@vwr/wiz/lease/v-rental.w|vwr/wiz/lease/v-charge.w@vwr/wiz/lease/v-charge.w|vwr/wiz/lease/v-alloc-space.w@vwr/wiz/lease/v-alloc-space.w|vwr/wiz/lease/v-garntr.w@vwr/wiz/lease/v-garntr.w|vwr/wiz/lease/v-review.w@vwr/wiz/lease/v-review.w|vwr/wiz/lease/v-outgng.w@vwr/wiz/lease/v-outgng.w|vwr/wiz/lease/v-finish.w,Wizard-Finish = vwr/wiz/lease/v-finish.w,GoTo-Combo = No,Major-Delim = @,Minor-Delim = |" "" "" no no
651 "DRL" "Wizard for defining wizards" 8 247 "Wizard Definition" "" yes "Wizard-Name = Wizard Wizard!,Wizard-Viewers = vwr/wiz/wizard/v-finish.w||N@vwr/wiz/wizard/v-wiz1.w||Y@vwr/wiz/wizard/v-wiz2.w||Y,Viewer-Links = vwr/wiz/wizard/v-wiz1.w|vwr/wiz/wizard/v-wiz2.w@vwr/wiz/wizard/v-wiz2.w|vwr/wiz/wizard/v-finish.w,Wizard-Finish = vwr/wiz/wizard/v-finish.w,Major-Delim = @,Minor-Delim = |" "" "" no no
652 "DRL" "Browse (and Maintain) Rent Charge Status definitions" 8 177 "Rent Charge Statuses" "vwr/drl/b-rchsts.w" yes "" "" "" no no
653 "DRL" "Browse (and Maintain) Rent Charge Type definitions" 8 177 "Rent Charge Types" "vwr/drl/b-rchtyp.w" yes "" "" "" no no
654 "DRL" "Report on Variations and Adjustments for a project" 8 150 "Variation Tracking" "vwr\mnt\v-prjvar.w" yes "" "" "" no no
655 "DRL" "Browse the Rent Reviews for this Lease" 74 162 "Rent Reviews" "vwr/drl/b-rntrvw.w" yes "FilterPanel = Yes,Key-Name = TenancyLeaseCode" "" "" no no
656 "DRL" "Browse the Rental Charges for this Lease" 74 248 "Rent Charges" "vwr\drl\b-rntchr.w" yes "Key-Name = TenancyLeaseCode,FilterPanel = Yes" "" "" no no
657 "MNT" "Maintain the schedule of changes to this rent charge line" 248 249 "Charging Schedule" "vwr\drl\b-rchgln.w" yes "FilterPanel = Yes" "" "" no no
658 "MSG" "Delete the current rent charge record" 248 1 "Delete" "" yes "Message = delete-record" "" "" no no
659 "MNT" "Add a new type of rental charge for this lease" 248 202 "Add" "vwr\mnt\v-rntchr.w" yes "Mode = Add" "" "" no no
660 "MNT" "Maintain the current rental charge for this lease" 248 202 "Maintain" "vwr\mnt\v-rntchr.w" yes "Mode = Maintain" "" "" no no
661 "MNT" "Add a new line to this schedule of rental charges (a change in rental)" 249 202 "Add" "vwr\mnt\v-rchgln.w" yes "Mode = Add" "" "" no no
662 "MNT" "Maintain the selected rental charge line" 249 202 "Maintain" "vwr\mnt\v-rchgln.w" yes "Mode = Maintain" "" "" no no
663 "DRL" "Report on Orders for a project" 8 150 "Order Tracking" "vwr\mnt\v-prjord.w" yes "" "" "" no no
664 "MNU" "Main menu for the selection of printed reports and exporting functions" 8 250 "Reporting" "" yes "" "" "" no no
665 "MNU" "Menu of Project reports" 8 251 "Project Reports" "" yes "" "" "" no no
666 "MNU" "Menu of options related to database replication" 8 252 "Replication" "" yes "" "" "" no no
668 "SEL" "Select a tenant who is party to this lease" 253 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Tenant,Field = tenant-code, SearchPanel = Yes" "fil_Tenant" "tenant-code" no no
669 "SEL" "Select the property in which the space will be leased" 254 64 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property,Field = property-code, SearchPanel = Yes" "fil_Property" "property-code" no no
670 "DRL" "Report on discrepancies between the balances of control accounts and the sum of the balances of the accounts which they control" 8 150 "Reconcile Control Accounts" "vwr/mnt\v-subrec.w" yes "" "" "" no no
671 "DRL" "Wizard to guide the user through the process of setting a Rent Review" 8 246 "Rent Review Wizard" "" yes "Wizard-Name = Actioning a Rent Review,Wizard-Viewers = vwr/wiz/review/v-charge.w||Y@vwr/wiz/review/v-complt.w||Y@vwr/wiz/review/v-finish.w||N@vwr/wiz/review/v-market.w||N@vwr/wiz/review/v-review.w||Y@vwr/wiz/review/v-tenant.w||Y,Viewer-Links = vwr/wiz/review/v-tenant.w|vwr/wiz/review/v-review.w@vwr/wiz/review/v-review.w|vwr/wiz/review/v-market.w@vwr/wiz/review/v-market.w|vwr/wiz/review/v-complt.w@vwr/wiz/review/v-complt.w|vwr/wiz/review/v-charge.w@vwr/wiz/review/v-charge.w|vwr/wiz/review/v-finish.w,Wizard-Finish = vwr/wiz/review/v-finish.w,GoTo-Combo = No,Major-Delim = @,Minor-Delim = |" "" "" no no
672 "MNT" "View details of the currently selected property" 11 57 "View" "vwr/mnt/v-proper.w" yes "mode = View" "" "" no no
673 "DRL" "View details of the current Rental Space record" 13 58 "View" "vwr/mnt/v-rntspc.w" yes "Mode = View" "" "" no no
674 "DRL" "View the details of the current invoice" 67 68 "View" "vwr/mnt/v-ivoice.w" yes "mode = View" "" "" no no
675 "DRL" "View the details of this contract" 5 55 "View" "vwr/mnt/v-contrt.w" yes "mode = View" "" "" no no
676 "DRL" "View details for the current Creditor" 6 153 "View" "vwr/mnt/v-crdtor.w" yes "mode = View" "" "" no no
677 "DRL" "View the details of the current director" 215 216 "View" "vwr/mnt/v-dirctr.w" yes "mode = View" "" "" no no
678 "DRL" "View details of the current Guarantor" 171 202 "View" "vwr/mnt/v-garntr.w" yes "mode = View" "" "" no no
679 "DRL" "View the details of the current Order" 188 189 "View" "vwr/mnt/v-order.w" yes "mode = View" "" "" no no
680 "DRL" "View the details of the current Project Budget record" 192 195 "View" "vwr/mnt/v-prjbdg.w" yes "mode = View" "" "" no no
681 "DRL" "View the details of the current variation" 239 202 "View" "vwr/mnt/v-varitn.w" yes "Mode = View,Key-Name = VariationCode" "" "" no no
682 "DRL" "View the details of the current Project" 183 182 "View" "vwr/mnt/v-projct.w" yes "mode = View" "" "" no no
683 "MNT" "View details of the current rental charge for this lease" 248 202 "View" "vwr\mnt\v-rntchr.w" yes "Mode = View" "" "" no no
684 "DRL" "View the details of this Scenario" 221 222 "View" "vwr/mnt/v-scnrio.w" yes "mode = View" "" "" no no
685 "DRL" "View the details of this Share Holder" 217 218 "View" "vwr/mnt/v-shrhld.w" yes "mode = View" "" "" no no
686 "DRL" "View the details of the current Tenancy Lease" 74 62 "View" "vwr/mnt/v-tntlse.w" yes "Mode = View" "" "" no no
687 "DRL" "View the details for this Tenant record" 14 116 "View" "vwr/mnt/v-tenant.w" yes "mode = View" "" "" no no
688 "DRL" "View this Account Definition record" 100 54 "View" "vwr/mnt/v-choact.w" yes "Mode = View" "" "" no no
689 "SEL" "Select the property account to accumulate these charges" 255 20 "" "vwr/sel/b-selcoa.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = RENT   - Rental Income,Fill-In = fil_account,Field = AccountCode" "fil_account" "AccountCode" no no
690 "MNU" "Infrequently used reports showing various system ssetup and integrity information" 8 256 "System Reports" "" yes "" "" "" no no
691 "DRL" "Check (and fix) the integrity of account balances against the transactions" 8 150 "Integrity Check" "vwr/mnt/v-intchk.w" yes "" "" "" no no
692 "MSG" "Regenerate this table from the existing database triggers" 243 1 "Rebuild Rules" "" yes "Message = match-database" "" "" no no
693 "MSG" "Clear all outward replication actions" 243 1 "Clear all" "" yes "Message = clear-all-rules" "" "" no no
694 "MSG" "Force all outward replication actions on" 243 1 "Force all" "" yes "Message = force-all-rules" "" "" no no
695 "SEL" "Select first property for tenant aged balances" 257 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_PropertyFrom,Field = Int1" "fil_PropertyFrom" "Int1" no no
696 "SEL" "Select last property for tenant aged balances" 257 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_PropertyTo,Field = Int1" "fil_PropertyTo" "Int1" no no
697 "DRL" "Browse (and Maintain) Valuation Types" 8 177 "Valuation Types" "vwr/drl/b-valtyp.w" yes "" "" "" no no
698 "DRL" "Browse (and Maintain) Standard Lease Types" 8 177 "Lease Types" "vwr/drl/b-lsetyp.w" yes "" "" "" no no
699 "DRL" "Browse (and Maintain) standard Lease Status codes" 8 177 "Lease Statuses" "vwr/drl/b-lsesta.w" yes "" "" "" no no
700 "MNU" "Options for maintaining of property-related system codes tables" 8 259 "Property Codes Tables" "" yes "" "" "" no no
701 "MNU" "Options for maintaining of accounting-related system codes tables" 8 258 "Accounts Codes Tables" "" yes "" "" "" no no
702 "SEL" "Select a tenant to whom the rent review applies" 260 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Tenant,Field = tenant-code, SearchPanel = Yes" "fil_Tenant" "tenant-code" no no
703 "SEL" "Select a property to which the rent review applies" 260 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,Field = property-code,Fill-In = fil_Property,SortPanel = Yes,AlphabetPanel = Yes" "fil_Property" "property-code" no no
704 "DRL" "Wizard to guide the user through the process of a re-leasing" 8 261 "Lease Change Wizard" "" yes "Wizard-Name = Lease Change Wizard,Wizard-Viewers = vwr/wiz/release/v-wiz1.w||Y@vwr/wiz/release/v-wiz2.w||Y@vwr/wiz/review/v-space.w||N@vwr/wiz/lease/v-outgng.w||N@vwr/wiz/release/v-charge.w||Y@vwr/wiz/lease/v-alloc-space.w||N@vwr/wiz/release/v-finish.w||Y,Viewer-Links = vwr/wiz/release/v-wiz1.w|vwr/wiz/release/v-wiz2.w@vwr/wiz/release/v-wiz2.w|vwr/wiz/review/v-space.w@vwr/wiz/review/v-space.w|vwr/wiz/lease/v-outgng.w@vwr/wiz/lease/v-outgng.w|vwr/wiz/release/v-charge.w@vwr/wiz/release/v-charge.w|vwr/wiz/lease/v-alloc-space.w@vwr/wiz/lease/v-alloc-space.w|vwr/wiz/release/v-finish.w,Wizard-Finish = vwr/wiz/release/v-finish.w,GoTo-Combo = No,Major-Delim = @,Minor-Delim = |" "" "" no no
705 "SEL" "Select a property to which the rent review applies" 262 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,Field = property-code,Fill-In = fil_Property,SortPanel = Yes,AlphabetPanel = Yes" "fil_Property" "property-code" no no
706 "SEL" "Select a tenant to whom the rent review applies" 262 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Tenant,Field = tenant-code, SearchPanel = Yes" "fil_Tenant" "tenant-code" no no
707 "DRL" "Browse External Files related to this property" 11 263 "External" "vwr\drl\b-image.w" yes "Key-Name = PropertyCode" "" "" no no
708 "DRL" "View the details about the currently highlighted image" 263 202 "Show Details" "vwr/mnt/v-image.w" yes "Mode = View" "" "" no no
709 "MNT" "Add a link to a new image" 263 202 "Add" "vwr/mnt/v-image.w" yes "Mode = Add" "" "" no no
710 "MNT" "Maintain the details of the currently selected image" 263 202 "Maintain Details" "vwr/mnt/v-image.w" yes "Mode = Maintain" "" "" no no
711 "MSG" "View the associated image" 263 1 "View" "" yes "Message = view-image" "" "" no no
712 "MSG" "Change Cancelled Voucher back to Unapproved" 15 1 "Un-Cancel" "" yes "Message = uncancel-voucher" "" "" no no
713 "MSG" "Delete the current scenario and all of it's cash flows" 221 1 "Delete" "" yes "Message = delete-record" "" "" no no
714 "DRL" "Export outgoings percentages to a file, or a set of files" 8 150 "O/G Export" "vwr/mnt/v-ogperc.w" yes "" "" "" no no
715 "MSG" "Edit the description of this voucher" 15 1 "Edit Desc" "" yes "Message = change-description" "" "" no no
716 "SEL" "Select a scenario to be overwritten" 264 234 "" "vwr/sel/b-selscn.w" yes "Fill-In = fil_OtherName,Field = fil_OtherCode" "fil_OtherName" "fil_OtherCode" no no
717 "DRL" "Listing of contact names and addresses for a property" 8 150 "Property Contact Listing" "vwr/mnt/v-pcntct.w" yes "" "" "" no no
718 "SEL" "Select tenant for a Rental Invoice" 265 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Tenant,Field = EntityCode, SearchPanel = Yes" "fil_Tenant" "EntityCode" no no
719 "DRL" "Register Invoices for Rental Charges" 8 227 "Rent Invoices" "vwr/mnt/v-rntinv.w" yes "" "" "" no no
720 "DRL" "Report on Statistics of Debtor Performance" 8 150 "Debtor Statistics" "vwr/mnt/v-dbstat.w" yes "" "" "" no no
721 "SEL" "Select first property for debtor statistics reporting" 266 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_PropertyFrom,Field = Int1" "fil_PropertyFrom" "Int1" no no
722 "SEL" "Select last property for debtor statistics reporting" 266 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_PropertyTo,Field = Int1" "fil_PropertyTo" "Int1" no no
723 "DRL" "Browse (and Maintain) Payment Style definitions" 8 177 "Payment Styles" "vwr/drl/b-paysty.w" yes "" "" "" no no
724 "DRL" "Add a new office record" 267 202 "&Add" "vwr/mnt/v-office.w" yes "Mode = Add" "" "" no no
725 "DRL" "Browse and maintain control accounts for the currently selected office" 267 135 "Control Accounts" "vwr/drl/b-ocaofc.w" yes "" "" "" no no
726 "DRL" "Browse and maintain various configurable settings for the currently selected office" 267 177 "Settings" "vwr/drl/b-ostofc.w" yes "Key-Name = OfficeCode" "" "" no no
727 "MSG" "Delete the current office record" 267 1 "Delete" "Default" no "Message = delete-record" "" "" no no
728 "DRL" "Maintain the current office record, including it's postal address" 267 48 "Maintain" "vwr/mnt/v-office.w" yes "Mode = Maintain" "" "" no no
729 "MSG" "Cancel - Cancel changes" 48 1 "&Cancel" "" yes "Message = cancel-changes" "" "" no no
730 "MSG" "OK - Confirm Changes" 48 1 "&OK" "" yes "Message = confirm-changes" "" "" no no
731 "DRL" "Browse (and Maintain) Types of Postal Addresses" 8 177 "Address Types" "vwr/drl/b-postyp.w" yes "" "" "" no no
732 "DRL" "Browse (and Maintain) Types of Phone Numbers" 8 177 "Phone Types" "vwr/drl/b-phntyp.w" yes "" "" "" no no
733 "DRL" "Browse tasks due for completion" 8 268 "To-Do" "workflow/b-todo-task.w" yes "FilterPanel = Yes,SortPanel = Yes" "" "" no no
734 "DRL" "Add a new task" 268 202 "&Add" "workflow/v-flow-task.w" yes "Mode = Add" "" "" no no
735 "DRL" "Report on Lease Maturity for the portfolio" 8 150 "Lease Maturity" "vwr\mnt\v-maturity.w" yes "" "" "" no no
736 "DRL" "Maintain the currently selected Valuation" 187 202 "Maintain" "vwr/mnt/v-valuation.w" yes "mode = Maintain" "" "" no no
737 "DRL" "View the details of the currently selected Valuation" 187 227 "View" "vwr/mnt/v-valuation.w" yes "mode = View" "" "" no no
738 "DRL" "Maintain descriptive information about the property" 57 269 "Descriptions" "vwr/mnt/v-construction.w" yes "Mode = Maintain,Key-Name = PropertyCode" "" "" no no
739 "DRL" "Export portfolio lease expiry information" 8 150 "Portfolio Expiries" "vwr/mnt/v-portfolio-expiry.w" yes "" "" "" no no
740 "DRL" "Browse Clients" 8 270 "Clients / Owners" "vwr/drl/b-client.w" yes "" "" "" no no
741 "DRL" "Maintain details of the current client" 270 271 "Maintain" "vwr/mnt/v-client.w" yes "Mode = Maintain" "" "" no no
742 "DRL" "Add a new client" 270 271 "Add" "vwr/mnt/v-client.w" yes "Mode = Add" "" "" no no
743 "DRL" "View details of the current client" 270 271 "View" "vwr/mnt/v-client.w" yes "Mode = View" "" "" no no
744 "DRL" "Browse the companies of this client" 270 4 "Companies" "vwr/drl/b-compny.w" yes "SortPanel = Yes,Key-Name = ClientCode,FilterPanel = Yes,AlphabetPanel = Yes" "" "" no no
745 "MSG" "Confirm changes, save record and close the window" 271 1 "&OK" "" yes "Message = confirm-changes" "" "" no no
746 "MSG" "Cancel changes and close the window without saving the record" 271 1 "&Cancel" "" yes "Message = cancel-changes" "" "" no no
747 "DRL" "Browse the properties of this company" 4 11 "Properties" "vwr/drl/b-proper.w" yes "Key-Name = CompanyCode,NotesViewer = Yes,SortPanel = Yes,FilterPanel = Yes,AlphabetPanel = Yes" "" "" no no
748 "DRL" "Export Tenant Charges to a Spreadsheet" 8 150 "Tenant Charges Export" "vwr/mnt/v-tenant-charges.w" yes "" "" "" no no
750 "SEL" "Select a property for exporting tenant charges" 272 146 "" "vwr/sel/b-selpro.w" yes "AlphabetPanel = Yes,FilterPanel = Yes,Fill-In = fil_Property,Field = Int1" "fil_Property" "Int1" no no
751 "DRL" "Report on vouchers approved for payment" 8 150 "Approved for Payment" "vwr\mnt\v-approved-vouchers.w" yes "" "" "" no no
752 "MNU" "Menu of validation codes related to the workflow module" 8 273 "Workflow Codes Tables" "" yes "" "" "" no no
753 "DRL" "Browse (and Maintain) Activity Status codes" 8 177 "Activity Statuses" "workflow/b-activity-status.w" yes "" "" "" no no
754 "DRL" "Browse (and Maintain) Asset Status codes" 8 177 "Asset Statuses" "workflow/b-asset-status.w" yes "" "" "" no no
755 "DRL" "Browse (and Maintain) Asset Type codes" 8 177 "Asset Types" "workflow/b-asset-type.w" yes "" "" "" no no
756 "DRL" "Browse (and Maintain) Workflow Rule Type codes" 8 177 "Rule Types" "workflow/b-flow-rule-type.w" yes "" "" "" no no
757 "DRL" "Browse (and Maintain) Workflow Step Type codes" 8 177 "Step Types" "workflow/b-flow-step-type.w" yes "" "" "" no no
758 "DRL" "Browse (and Maintain) Workflow Task Type codes" 8 275 "Task Types" "workflow/b-flow-task-type.w" yes "" "" "" no no
759 "DRL" "Browse Workflow Rule definitions" 8 274 "Workflow Rules" "workflow/b-flow-rule.w" yes "" "" "" no no
760 "DRL" "Add a new workflow rule" 274 202 "Add" "workflow/v-flow-rule.w" yes "Mode = add" "" "" no no
761 "DRL" "Maintain the current workflow rule" 274 202 "Maintain" "workflow/v-flow-rule.w" yes "Mode = maintain" "" "" no no
762 "MSG" "Delete the current workflow rule" 274 202 "Delete" "" yes "Message = delete-record" "" "" no no
763 "DRL" "Browse the steps involved in this task" 275 276 "Steps" "workflow/b-flow-step-type.w" yes "Key-Name = FlowTaskType" "" "" no no
764 "DRL" "Browse rules applying from this step" 276 274 "Following Rules" "workflow/b-flow-rule.w" yes "Key-Name = FlowStepType" "" "" no no
765 "DRL" "Browse rules which might result in this step" 276 274 "Prior Rules" "workflow/b-flow-rule.w" yes "Key-Name = NextStepType" "" "" no no
766 "DRL" "Add a new workflow task" 275 202 "Add" "workflow/v-flow-task-type.w" yes "Key-Name = FlowTaskType,Mode = Add" "" "" no no
767 "DRL" "Maintain the current workflow task" 275 202 "Maintain" "workflow/v-flow-task-type.w" yes "Key-Name = FlowTaskType,Mode = Maintain" "" "" no no
768 "MSG" "Delete the current workflow task" 275 1 "Delete" "" yes "Message = delete-record" "" "" no no
769 "MSG" "Delete the current workflow step" 276 1 "Delete" "" yes "Message = delete-record" "" "" no no
770 "DRL" "Add a new step to the current task" 276 202 "Add" "workflow/v-flow-step-type.w" yes "Key-Name = FlowTaskType,Mode = Add" "" "" no no
771 "DRL" "Maintain the current step" 276 202 "Maintain" "workflow/v-flow-step-type.w" yes "Mode = Maintain" "" "" no no
772 "DRL" "Browse (and Maintain) Rental Area Status records" 8 177 "Area Statuses" "vwr/drl/b-areastatus.w" yes "" "" "" no no
773 "DRL" "Browse (and Maintain) Types of Contacts to relate to Tenants, Creditors, Properties and so forth" 8 177 "Entity Contact Types" "vwr/drl/b-e-contact-type.w" yes "" "" "" no no
774 "MSG" "Close window without saving changes" 179 1 "&Cancel" "" yes "Message = cancel-changes" "" "" no no
775 "MSG" "Go to the next cheque for the current bank account" 179 1 "&>" "" yes "Message = next-cheque" "" "" no no
776 "MSG" "Go to the previous cheque for this bank account" 179 1 "&<" "" yes "Message = previous-cheque" "" "" no no
777 "DRL" "Browse Contact People for this Tenant" 14 277 "Contacts" "vwr/drl/b-entity-contact.w" yes "Key-Name = TenantCode" "" "" no no
778 "SEL" "Select the Client/Owner for this Company" 28 278 "" "vwr/sel/b-selclient.w" yes "Fill-In = fil_Client,Field = ClientCode" "fil_Client" "ClientCode" no no
779 "DRL" "Browse (and Maintain) records describing types of external data files and the programs which allow viewing/editing of those files" 8 177 "External File Types" "vwr/drl/b-image-type.w" yes "" "" "" no no
780 "DRL" "Browse Building System Inspectors" 8 279 "Inspectors" "compliance/b-inspectors.w" yes "" "" "" no no
781 "DRL" "Browse Building Systems where the owner inspection is done by this inspector" 279 280 "Owner Inspections" "compliance/b-building-systems.w" yes "Key-Name = InspectorOwner" "" "" no no
782 "DRL" "Browse Building Systems where the IQP inspection is done by this inspector" 279 280 "IQP Inspections" "compliance/b-building-systems.w" yes "Key-Name = InspectorIQP" "" "" no no
783 "DRL" "Maintain details for the Creditor for this Inspector" 279 280 "Maintain Creditor" "vwr/mnt/v-crdtor.w" yes "mode = Maintain,Key-Name = CreditorCode" "" "" no no
784 "DRL" "Report on Expense Actual vs Budget Variances for Properties and Accoun" 8 150 "Variance Report" "vwr/mnt/v-expense-variance.w" yes "" "" "" no no
785 "DRL" "Browse Building Systems of this property" 11 280 "Building Systems" "compliance/b-building-systems.w" yes "Key-Name = PropertyCode" "" "" no no
786 "DRL" "Maintain the current building system" 280 281 "Maintain" "compliance/v-building-system.w" yes "Mode = Maintain" "" "" no no
787 "DRL" "Add a new building system" 280 281 "Add" "compliance/v-building-system.w" yes "Mode = Add" "" "" no no
788 "MSG" "Export the records in this browser to an Excel Spreadsheet" 7 1 "Build Spreadsheet" "" yes "Message = dump-to-excel" "" "" no no
789 "DRL" "Maintain the current task" 268 202 "&Maintain" "workflow/v-flow-task.w" yes "Mode = Maintain" "" "" no no
790 "DRL" "Configuration settings controlling various operational aspects of the system" 8 136 "Office Settings" "vwr/drl/b-ostofc.w" yes "Key-Name = ThisOffice" "" "" no no
791 "DRL" "Browse rental parameters and assumptions for scenario flow generation" 221 282 "Assumptions" "forecast/b-scenario-assumptions.w" yes "Key-Name = ScenarioCode" "" "" no no
792 "DRL" "Add a new assumption or parameter to this scenario" 282 202 "Add" "forecast/v-scenario-assumption.w" yes "Mode = Add" "" "" no no
793 "DRL" "Maintain the current scenario assumption" 282 202 "Maintain" "forecast/v-scenario-assumption.w" yes "Mode = Maintain" "" "" no no
794 "DRL" "Entry of supplier invoices which have been approved for payment" 15 63 "Pinks" "vwr/mnt/v-pink.w" yes "mode = Register" "" "" no no
795 "DRL" "Maintain the current voucher record (Pink)" 15 63 "Maintain" "vwr/mnt/v-pink.w" yes "mode = Maintain" "" "" no no
796 "DRL" "Produce invoice/statement printouts suitable for mailing to tenants" 8 150 "Invoice/Statements" "vwr/mnt/v-invoice-statement.w" yes "" "" "" no no
797 "SEL" "Select a Property for a Statement" 283 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property,Field = Int1" "fil_Property" "Int1" no no
798 "SEL" "Select Tenant for a Stement of Account" 283 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,SearchPanel = Yes,Fill-In = fil_Tenant1,Field = Int2" "fil_Tenant1" "Int2" no no
800 "SEL" "Select a creditor for this voucher" 284 21 "" "vwr/sel/b-selcrd.w" yes "Fill-In = fil_Creditor,SearchPanel = Yes,Field = CreditorCode,SortPanel = Yes,FilterPanel = Yes,AlphabetPanel = Yes,AlphabetPanel2 = Yes" "fil_Creditor" "CreditorCode" no no
801 "DRL" "Add or remove members to this user group" 139 227 "Members" "sec\v-usrgrp-members.w" yes "Key-Name = GroupName" "" "" no no
802 "DRL" "View this Voucher and it's Allocations (Pink)" 15 63 "View" "vwr/mnt/v-pink.w" yes "Mode = View" "" "" no no
803 "DRL" "Report on cheques, showing the details of invoices paid and their allocations" 8 150 "Payment Details" "vwr/mnt/v-cheque-whites.w" yes "" "" "" no no
804 "DRL" "Browse Cheques which have not yet been recorded as ""Presented""" 8 161 "Unpresented Cheques" "vwr/drl/b-unpresented-cheques.w" yes "FilterPanel = Yes,SearchPanel = Yes,Key-Name = BankAccountCode" "" "" no yes
805 "DRL" "Report and reconcile tenant outgoings against budgets" 8 150 "Tenant O/G Budgets" "vwr/mnt/v-tenant-outgoing-budgets.w" yes "" "" "" no no
806 "SEL" "Select a Property for the Tenant Outgoing Budgets" 285 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property,Field = Int1" "fil_Property" "Int1" no no
807 "SEL" "Select Tenant for Outgoing Budgets" 285 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Tenant,Field = Int2, SearchPanel = Yes" "fil_Tenant" "Int2" no no
808 "SEL" "Select a Property for the Month End Outgoings Accrual" 286 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property,Field = Int1" "fil_Property" "Int1" no no
809 "DRL" "Report on outgoings at month end report and generate a journal accruing the potential recoverable amounts" 8 150 "Month End OG Accrual" "vwr/mnt/v-month-end-og-accrual.w" yes "" "" "" no no
810 "SEL" "Select a Property for the Actual Recoveries calculation" 287 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property,Field = Int1" "fil_Property" "Int1" no no
811 "DRL" "Report on recoverable amounts for a tenants on actual recoveries" 8 150 "Actual Recoveries" "vwr/mnt/v-actual-recoveries.w" yes "" "" "" no no
812 "MNU" "Menu of programs which generate batches of transactions for update" 8 288 "Automatic Transactions" "" yes "" "" "" no no
813 "DRL" "Browse the complete menu structure" 8 289 "Browse Menus" "sec/b-menuitem.w" yes "Key-Name = LinkCode,Key-Value = 8" "" "" no no
814 "DRL" "Browse menu items off this menu" 289 289 "Menu Items" "sec/b-menuitem.w" yes "Key-Name = LinkCode" "" "" no no
815 "DRL" "Browse (and Maintain) Debt Classification codes" 8 177 "Debt Classes" "vwr/drl/b-debt-class.w" yes "" "" "" no no
816 "DRL" "Browse (and Maintain) Variance Classification codes" 8 177 "Variance Classes" "vwr/drl/b-variance-class.w" yes "" "" "" no no
817 "DRL" "Browse the batch queue to see why your job is taking so long" 8 290 "Queue Viewer" "vwr/drl/b-quevue.w" yes "" "" "" no no
818 "DRL" "Print a building Warrant of Fitness certificate" 8 150 "Building WOF" "vwr/mnt/v-building-wof.w" yes "" "" "" no no
819 "SEL" "Select Property for a Building Warrant of Fitness certificate" 291 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_prop1,Field = Int1" "fil_prop1" "Int1" no no
820 "DRL" "Browse Types of Building Systems" 8 292 "Building System Types" "vwr/drl/b-bldg-system-type.w" yes "" "" "" no no
821 "DRL" "Add a new type of building system" 292 202 "Add" "vwr/mnt/v-bldg-system-type.w" yes "Mode = Add" "" "" no no
822 "DRL" "Maintain the current building system type" 292 202 "Maintain" "vwr/mnt/v-bldg-system-type.w" yes "Mode = Maintain" "" "" no no
823 "MSG" "Delete the currently selected rent charge line" 249 1 "Delete" "" yes "Message = delete-record" "" "" no no
824 "DRL" "Reset the date on which the system believes rent was last charged" 8 150 "Reset Last Rent Charged Date" "vwr/mnt/v-rntchg-date.w" yes "" "" "" no no
825 "DRL" "Browse the balances of all accounts for this tenant" 14 3 "Accounts" "vwr/drl/b-actsum.w" yes "Key-Name = TenantCode" "" "" no no
826 "MSG" "Preview a menu structure report for the currently selected user" 210 1 "Menu Listing" "" yes "Message = print-menus" "" "" no no
827 "DRL" "List property outgoings vs. accruals" 8 150 "O/G Differences" "vwr/mnt/v-og-differences.w" yes "" "" "" no no
828 "DRL" "Maintain the details of the currently selected contact" 277 293 "Maintain" "vwr/mnt/v-entity-contact.w" yes "Mode = Maintain" "" "" no no
829 "MSG" "Confirm changes and close the current screen" 293 1 "&OK" "" yes "Message = confirm-changes" "" "" no no
830 "MSG" "Undo any changes and close the current screen" 293 1 "&Cancel" "" yes "Message = cancel-changes" "" "" no no
831 "DRL" "Add a new contact" 277 293 "Add" "vwr/mnt/v-entity-contact.w" yes "Mode = Add" "" "" no no
833 "DRL" "Duplicate this contact to identify the same person as a different contact type" 277 293 "Same person - new type" "vwr/mnt/v-entity-contact.w" yes "Mode = Duplicate" "" "" no no
834 "MSG" "Delete the current contact" 277 1 "Delete" "" yes "Message = delete-record" "" "" no no
835 "MSG" "Add a new record" 295 1 "&Add" "Default" no "Message = add-record" "" "" no no
837 "MSG" "Delete the currently selected record" 295 1 "&Delete" "Default" no "Message = delete-record" "" "" no no
838 "DRL" "Browse (and Maintain) Outgoings Budgets for this property" 11 295 "O/G Budgets" "vwr/drl/b-property-outgoing.w" yes "Key-Name = PropertyCode,Link-To-1 = panel/p-fill-in.w|Value" "" "" no no
839 "MSG" "Reduce the sequence of this group" 139 1 "Move Up" "" yes "Message = reduce-group-sequence" "" "" no no
840 "MSG" "Increase the sequence of this group" 139 1 "Move Down" "" yes "Message = increase-group-sequence" "" "" no no
841 "DRL" "Generate transactions transferring GST account balances to the GST suspense account" 8 150 "Monthly GST" "vwr/mnt/v-monthly-gst.w" yes "" "" "" no no
842 "DRL" "View the cheque which paid the voucher associated with this transaction" 7 296 "Show Cheque" "vwr/mnt/v-cheque-view.w" yes "Key-Name = ChequeNo,Mode = View" "" "" no no
843 "DRL" "Browse Vouchers of this Cheque" 296 15 "Vouchers" "vwr/drl/b-vouchr.w" yes "Key-Name = ChequeNo" "" "" no no
844 "SEL" "Select other Project for the Report" 204 190 "" "vwr/sel/b-selprj.w" yes "Fill-In = fil_Project2,Field = Int2" "fil_Project2" "Int2" no no
845 "MSG" "Delete the current invoice" 67 1 "Delete" "" yes "Message = delete-current-record" "" "" no no
846 "SEL" "Select a Property for the Tenant Outgoing Budgets" 297 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property,Field = Int1" "fil_Property" "Int1" no no
847 "SEL" "Select Tenant for Outgoing Budgets" 297 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Tenant,Field = Int2, SearchPanel = Yes" "fil_Tenant" "Int2" no no
848 "DRL" "Report on the details of a tenant, or a range of tenants" 8 150 "Tenant Details" "vwr/mnt/v-tenant-details.w" yes "" "" "" no no
849 "DRL" "Browse (and Maintain) Property Forecast Record Types" 8 177 "Forecast Record Types" "forecast/b-prop-fc-type.w" yes "" "" "" no no
850 "DRL" "Browse (and Maintain) Property Forecast Parameters in effect from the selected month" 40 299 "Forecast Parameters" "forecast/b-prop-fc-param.w" yes "Key-Name = MonthCode" "" "" no no
851 "DRL" "Browse (and Maintain) Forecast Parameters for the selected property" 11 299 "Forecast Params" "forecast/b-prop-fc-param.w" yes "Key-Name = PropertyCode" "" "" no no
852 "DRL" "Regenerate the forecast income and expense from property and tenant information" 8 150 "Regenerate" "forecast/v-run-amtrust-rental.w" yes "" "" "" no no
853 "SEL" "Select a Property for regeneration of forecast information" 298 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property,Field = Int1" "fil_Property" "Int1" no no
854 "DRL" "Browse (and Maintain) Property Forecast Parameters in effect during the selected year" 47 299 "Forecast Parameters" "forecast/b-prop-fc-param.w" yes "Key-Name = FinancialYearCode" "" "" no no
855 "MSG" "Add a new record" 299 1 "&Add" "" yes "Message = add-new-line" "" "" no no
856 "MSG" "Cancel changes to the currently selected record" 299 1 "&Cancel changes" "" yes "Message = cancel-record" "" "" no no
857 "MSG" "Delete the currently selected record" 299 1 "&Delete" "Default" no "Message = delete-record" "" "" no no
858 "DRL" "Browse (and Maintain) exceptions to the standard rules for importing bank account transactions" 167 177 "Import Exceptions" "vwr/drl/b-bank-exception.w" yes "Key-Name = BankAccountCode" "" "" no no
859 "DRL" "Report on the forecast rentals and expenses" 8 150 "Forecast Report" "forecast/v-amtrust-rent-report.w" yes "" "" "" no no
860 "SEL" "Select a Property for forecast rent reporting" 300 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property,Field = Int1" "fil_Property" "Int1" no no
861 "DRL" "Report showing the variance from market for current rentals" 8 150 "Market Rentals" "forecast/v-amtrust-market-rents.w" yes "" "" "" no no
862 "SEL" "Select a Property for market rent reporting" 301 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property,Field = Int1" "fil_Property" "Int1" no no
863 "SEL" "Select second Property for forecast rent reporting" 300 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property2,Field = Int2" "fil_Property2" "Int2" no no
864 "SEL" "Select second Property for market rent reporting" 301 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property2,Field = Int2" "fil_Property2" "Int2" no no
865 "SEL" "Select a Property for market rent reporting" 302 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property,Field = Int1" "fil_Property" "Int1" no no
866 "SEL" "Select second Property for market rent reporting" 302 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_Property2,Field = Int2" "fil_Property2" "Int2" no no
867 "DRL" "Report on Yield for the properties in the portfolio" 8 150 "Portfolio Yield" "vwr/mnt/v-portfolio-yield.w" yes "" "" "" no no
868 "MSG" "Regenerate the base expense conditions for the currently selected scenario" 221 1 "Generate Expenses" "" yes "Message = regenerate-expenses" "" "" no no
869 "DRL" "Browse (and Maintain) Meters used for measuring supply, usually of electricity" 11 304 "Meters" "vwr/drl/b-supply-meters.w" yes "Key-Name = PropertyCode" "" "" no no
870 "MSG" "Add a new record" 304 1 "&Add" "Default" no "Message = add-record" "" "" no no
872 "MSG" "Delete the currently selected record" 304 1 "&Delete" "Default" no "Message = delete-record" "" "" no no
873 "DRL" "Browse (and Maintain) forecast Expense Cash Flows" 8 223 "Expense Forecast" "vwr/drl/b-cshflw.w" yes "FilterPanel = Yes,SearchPanel = Yes" "" "" no no
874 "DRL" "Apply scenario information to set the budget and revised budget figures for future periods" 8 150 "Apply to Budgets" "vwr/mnt/v-scnbud.w" yes "" "" "" no no
875 "DRL" "Export account balances as at a particular month" 8 150 "Export Balances" "vwr/mnt/v-export-balances.w" yes "" "" "" no no
876 "DRL" "Browse Contact People for this Creditor" 6 277 "Contacts" "vwr/drl/b-entity-contact.w" yes "Key-Name = CreditorCode" "" "" no no
877 "MSG" "OK - Confirm Changes" 281 1 "&OK" "" yes "Message = confirm-changes" "" "" no no
878 "MSG" "Cancel - Cancel changes" 281 1 "&Cancel" "" yes "Message = cancel-changes" "" "" no no
879 "MSG" "Delete the currently selected record" 280 1 "Delete" "" yes "Message = delete-record" "" "" no no
880 "DRL" "Report on variance from OG budget for selected property accounts" 8 150 "Outgoings Watchlist" "vwr/mnt/v-og-budget-watchlist.w" yes "" "" "" no no
881 "DRL" "Browse vouchers related to this order" 188 15 "Vouchers" "vwr/drl/b-vouchr.w" yes "Key-Name = OrderCode" "" "" no no
882 "DRL" "Report on the details of orders for a range of projects" 8 150 "Order Mgmt Report" "vwr/mnt/v-order-mgmt.w" yes "" "" "" no no
883 "MSG" "Merge all people with the same company and name details into the selected record" 108 1 "Merge duplicates" "" yes "Message = merge-records" "" "" no no
884 "SEL" "Select Property for a range of Rental Advices" 93 64 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,SearchPanel = Yes,Fill-In = fil_Property,Field = Int2" "fil_Property" "Int2" no no
885 "SEL" "Select a Contact Person for this Inspector" 305 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = INSP - Building System Inspectors,Fill-In = fil_ContactPerson" "fil_ContactPerson" "" no no
886 "SEL" "Select a Creditor who is the billing entity for this Inspector" 305 24 "" "vwr/sel/b-selcrd.w" yes "SortPanel = Yes,FilterPanel = Yes,SearchPanel = Yes,Fill-In = fil_Creditor" "fil_Creditor" "" no no
887 "DRL" "Maintain the current inspector details" 279 202 "Maintain" "compliance/v-inspector.w" yes "Mode = Maintain" "" "" no no
888 "DRL" "Add a new inspector" 279 202 "Add" "compliance/v-inspector.w" yes "Mode = Add" "" "" no no
889 "MSG" "Delete the currently selected inspector record" 279 1 "Delete" "" yes "Message = delete-inspector" "" "" no no
890 "DRL" "Maintain details (phone, address and so forth) for the Person who is this Inspector" 279 280 "Maintain Person" "vwr/mnt/v-person.w" yes "mode = Maintain,Key-Name = PersonCode" "" "" no no
891 "DRL" "Browse property tasks due for completion" 11 268 "To-Do" "workflow/b-todo-task.w" yes "Key-Name = PropertyCode,FilterPanel = Yes" "" "" no no
892 "DRL" "Browse tenant tasks due for completion" 14 268 "To-Do" "workflow/b-todo-task.w" yes "Key-Name = TenantCode,FilterPanel = Yes" "" "" no no
893 "DRL" "Browse company tasks due for completion" 4 268 "To-Do" "workflow/b-todo-task.w" yes "Key-Name = CompanyCode,FilterPanel = Yes" "" "" no no
894 "DRL" "Browse project tasks due for completion" 183 268 "To-Do" "workflow/b-todo-task.w" yes "Key-Name = ProjectCode,FilterPanel = Yes" "" "" no no
895 "SEL" "Select a Person this task is allocated to" 306 24 "" "vwr/sel/b-selpsn.w" yes "SortPanel = Yes,FilterPanel = Yes,FilterBy-Case = SF   - Staff member,Fill-In = fil_AllocatedTo" "fil_AllocatedTo" "" no no
896 "MSG" "Mark all steps of the current task as completed" 268 1 "Mark Complete" "" yes "Message = mark-as-completed" "" "" no no
897 "MSG" "Mark all steps of the current task as 'to do'" 268 1 "Mark as ToDo" "" yes "Message = mark-as-todo" "" "" no no
898 "DRL" "Add a new step to the task" 307 202 "&Add" "workflow/v-flow-step.w" yes "Mode = Add" "" "" no no
899 "DRL" "Maintain the current step" 307 202 "&Maintain" "workflow/v-flow-step.w" yes "Mode = Maintain" "" "" no no
900 "MSG" "Mark this step as 'to do'" 307 1 "Mark as ToDo" "" yes "Message = mark-as-todo" "" "" no no
901 "MSG" "Mark this step as completed" 307 1 "Mark Complete" "" yes "Message = mark-as-completed" "" "" no no
902 "DRL" "Browse steps involved in completing this task" 268 307 "Steps" "workflow/b-flow-step.w" yes "" "" "" no no
903 "MSG" "Delete the current task and all of the associated steps" 268 1 "Delete" "" yes "Message = delete-current-task" "" "" no no
904 "DRL" "Browse (and Maintain) Type codes for additional Person details" 8 177 "Extra Person Fields" "vwr/drl/b-person-detail-type.w" yes "" "" "" no no
905 "DRL" "Report on the rentals paid by major tenants" 8 150 "Tenant Rentals" "vwr/run/v-tenant-rentals.w" yes "" "" "" no no
906 "DRL" "Report showing current property income (from rent charges) against budgetted expenditure (from property budgets)" 8 150 "Curr. Inc. and Budget Exp." "vwr/run/v-curr-inc-bud-exp.w" yes "" "" "" no no
907 "SEL" "Select FROM Property for a Property Schedule" 308 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop1" "Int1" no no
908 "SEL" "Select TO Property for a Property Schedule" 308 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop2" "Int2" no no
909 "DRL" "Report showing weighted average lease life in the format used by AGP" 8 150 "Sydney W.A.L.L" "vwr/run/v-sydney-wall.w" yes "" "" "" no no
910 "SEL" "Select FROM Property for a Property Schedule" 309 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop1" "Int1" no no
911 "SEL" "Select TO Property for a Property Schedule" 309 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop2" "Int2" no no
912 "DRL" "Report showing reconciliation of areas with building" 8 150 "Bldg Area Reconciliation" "vwr/run/v-bldg-area-reconciliation.w" yes "" "" "" no no
913 "SEL" "Select FROM Property for a Property Schedule" 310 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop1" "Int1" no no
914 "SEL" "Select TO Property for a Property Schedule" 310 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes" "fil_prop2" "Int2" no no
915 "DRL" "Import transactions from a file of comma-separated values" 8 134 "Import Transactions" "vwr/run/v-import-transactions.w" yes "" "" "" no no
916 "DRL" "Print a To Do List report" 8 150 "To Do List" "vwr/run/v-to-do.w" yes "" "" "" no no
917 "DRL" "Browse all tasks" 8 268 "All Tasks" "workflow/b-flow-task.w" yes "FilterPanel = Yes,SortPanel = Yes" "" "" no no
918 "DRL" "Browse a persons tasks due for completion" 108 268 "To-Do" "workflow/b-todo-task.w" yes "FilterPanel = Yes,Key-Name = PersonCode,SortPanel = Yes" "" "" no no
919 "MSG" "Print the outgoings for the currently selected property" 295 1 "&Print" "" yes "Message = print-outgoings" "" "" no no
920 "DRL" "Report showing Building Systems and Inspectors by building" 8 150 "Building Act" "vwr/run/v-building-act.w" yes "" "" "" no no
921 "SEL" "Select first property for the report" 311 146 "" "vwr/sel/b-selpro.w" yes "AlphabetPanel = Yes,SortPanel = Yes,FilterPanel = Yes,Field = Int1,Fill-In = fil_prop1" "fil_prop1" "Int1" no no
922 "SEL" "Select other property for the report" 311 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = yes,SortPanel = yes,AlphabetPanel = yes,Fill-In = fil_prop2,Field = Int2" "fil_prop2" "Int2" no no
923 "DRL" "Report validating Rental Areas and Charges by building" 8 150 "Property Validation" "vwr/run/v-property-verification.w" yes "" "" "" no no
924 "SEL" "Select first property for Report" 312 146 "" "vwr/sel/b-selpro.w" yes "AlphabetPanel = Yes,SortPanel = Yes,FilterPanel = Yes,Field = Int1,Fill-In = fil_prop1" "fil_prop1" "Int1" no no
925 "SEL" "Select other property for the report" 312 146 "" "" yes "FilterPanel = yes,SortPanel = yes,AlphabetPanel = yes,Fill-In = fil_prop2,Field = Int2" "fil_prop2" "Int2" no no
926 "DRL" "Wizard to guide the user through the process of setting up a new lease (AmTrust)" 8 246 "Lease Wizard" "" yes "Wizard-Name = Adding a new lease (AmTrust),Wizard-Viewers = vwr/wiz/lease/v-tenant.w||Y@vwr/wiz/lease/v-space.w||Y@vwr/wiz/lease/v-term.w||N@vwr/wiz/lease/v-type-amtrust.w||N@vwr/wiz/lease/v-renew-amtrust.w||N@vwr/wiz/lease/v-outgng.w||N@vwr/wiz/lease/v-charge.w||Y@vwr/wiz/lease/v-alloc-space.w||N@vwr/wiz/lease/v-review.w||N@vwr/wiz/lease/v-finish-amtrust.w||Y,Viewer-Links = vwr/wiz/lease/v-tenant.w|vwr/wiz/lease/v-space.w@vwr/wiz/lease/v-space.w|vwr/wiz/lease/v-term.w@vwr/wiz/lease/v-term.w|vwr/wiz/lease/v-type-amtrust.w@vwr/wiz/lease/v-type-amtrust.w|vwr/wiz/lease/v-renew-amtrust.w@vwr/wiz/lease/v-renew-amtrust.w|vwr/wiz/lease/v-outgng.w@vwr/wiz/lease/v-outgng.w|vwr/wiz/lease/v-charge.w@vwr/wiz/lease/v-charge.w|vwr/wiz/lease/v-alloc-space.w@vwr/wiz/lease/v-alloc-space.w|vwr/wiz/lease/v-review.w@vwr/wiz/lease/v-review.w|vwr/wiz/lease/v-finish-amtrust.w,Wizard-Finish = vwr/wiz/lease/v-finish-amtrust.w,GoTo-Combo = No,Major-Delim = @,Minor-Delim = |" "" "" no no
927 "DRL" "Wizard to guide the user through the process of a re-leasing (AmTrust)" 8 261 "Lease Change Wizard" "" yes "Wizard-Name = Lease Change Wizard,Wizard-Viewers = vwr/wiz/release/v-wiz1.w||Y@vwr/wiz/release/v-wiz2.w||Y@vwr/wiz/review/v-space.w||N@vwr/wiz/lease/v-outgng.w||N@vwr/wiz/release/v-charge.w||Y@vwr/wiz/lease/v-alloc-space.w||N@vwr/wiz/release/v-finish-amtrust.w||Y,Viewer-Links = vwr/wiz/release/v-wiz1.w|vwr/wiz/release/v-wiz2.w@vwr/wiz/release/v-wiz2.w|vwr/wiz/review/v-space.w@vwr/wiz/review/v-space.w|vwr/wiz/lease/v-outgng.w@vwr/wiz/lease/v-outgng.w|vwr/wiz/release/v-charge.w@vwr/wiz/release/v-charge.w|vwr/wiz/lease/v-alloc-space.w@vwr/wiz/lease/v-alloc-space.w|vwr/wiz/release/v-finish-amtrust.w,Wizard-Finish = vwr/wiz/release/v-finish-amtrust.w,GoTo-Combo = No,Major-Delim = @,Minor-Delim = |" "" "" no no
928 "DRL" "Wizard to guide the user through the process of setting a Rent Review (AmTrust)" 8 246 "Rent Review Wizard" "" yes "Wizard-Name = Actioning a Rent Review (AmTrust),Wizard-Viewers = vwr/wiz/review/v-tenant.w||Y@vwr/wiz/review/v-review.w||Y@vwr/wiz/review/v-market.w||N@vwr/wiz/review/v-complt.w||Y@vwr/wiz/review/v-charge.w||Y@vwr/wiz/review/v-finish-amtrust.w||N,Viewer-Links = vwr/wiz/review/v-tenant.w|vwr/wiz/review/v-review.w@vwr/wiz/review/v-review.w|vwr/wiz/review/v-market.w@vwr/wiz/review/v-market.w|vwr/wiz/review/v-complt.w@vwr/wiz/review/v-complt.w|vwr/wiz/review/v-charge.w@vwr/wiz/review/v-charge.w|vwr/wiz/review/v-finish-amtrust.w,Wizard-Finish = vwr/wiz/review/v-finish-amtrust.w,GoTo-Combo = No,Major-Delim = @,Minor-Delim = |" "" "" no no
929 "DRL" "Wizard to guide you through the termination of a lease" 8 246 "Lease End Wizard" "" yes "Wizard-Name = Terminating a Lease,Wizard-Viewers = vwr/wiz/review/v-tenant.w||Y@vwr/wiz/ls-end/v-lease-end.w||Y@vwr/wiz/ls-end/v-finish.w||N,Viewer-Links = vwr/wiz/review/v-tenant.w|vwr/wiz/ls-end/v-lease-end.w@vwr/wiz/ls-end/v-lease-end.w|vwr/wiz/ls-end/v-finish.w,Wizard-Finish = vwr/wiz/ls-end/v-finish.w,GoTo-Combo = No,Major-Delim = @,Minor-Delim = |" "" "" no no
930 "MSG" "Print a Tenant Statement of Account" 14 1 "Statement" "" yes "Message = print-statement" "" "" no no
931 "DRL" "Browse Tenant Support Calls" 8 313 "Facilities Mgmt" "vwr/drl/b-tenant-call.w" yes "SortPanel = Yes,FilterPanel = Yes" "" "" yes yes
934 "MSG" "Delete a tenant call" 313 1 "Delete" "" yes "Message = delete-record" "" "" no no
935 "MSG" "Add a tenant call" 313 1 "&Add" "" yes "Message = new-call" "" "" no no
936 "DRL" "Browse (and Maintain) general lookup codes in the system" 8 177 "Lookup Codes" "vwr/drl/b-lookup-code.w" yes "FilterPanel = Yes,SortPanel = Yes" "" "" no no
937 "MNU" "Menu of reports for Facilities Management function" 8 314 "Facilities Mgmt Reports" "" yes "" "" "" no no
938 "DRL" "Browse Contact People for this Property" 11 277 "Contacts" "vwr/drl/b-entity-contact.w" yes "Key-Name = PropertyCode" "" "" no no
939 "DRL" "Wizard to guide the user through the process of setting up a new lease (TTP)" 8 246 "Lease Wizard" "" yes "Wizard-Name = Adding a new lease,Wizard-Viewers = vwr/wiz/lease/v-tenant.w||Y@vwr/wiz/lease/v-space.w||Y@vwr/wiz/lease/v-term.w||N@vwr/wiz/lease/v-type.w||N@vwr/wiz/lease/v-renew.w||N@vwr/wiz/lease/v-rental.w||N@vwr/wiz/lease/v-ttp-space.w||Y@vwr/wiz/lease/v-garntr.w||N@vwr/wiz/lease/v-review.w||N@vwr/wiz/lease/v-outgng.w||N@vwr/wiz/lease/v-finish.w||Y,Viewer-Links = vwr/wiz/lease/v-tenant.w|vwr/wiz/lease/v-space.w@vwr/wiz/lease/v-space.w|vwr/wiz/lease/v-term.w@vwr/wiz/lease/v-term.w|vwr/wiz/lease/v-type.w@vwr/wiz/lease/v-type.w|vwr/wiz/lease/v-renew.w@vwr/wiz/lease/v-renew.w|vwr/wiz/lease/v-rental.w@vwr/wiz/lease/v-rental.w|vwr/wiz/lease/v-ttp-space.w@vwr/wiz/lease/v-ttp-space.w|vwr/wiz/lease/v-garntr.w@vwr/wiz/lease/v-garntr.w|vwr/wiz/lease/v-review.w@vwr/wiz/lease/v-review.w|vwr/wiz/lease/v-outgng.w@vwr/wiz/lease/v-outgng.w|vwr/wiz/lease/v-finish.w,Wizard-Finish = vwr/wiz/lease/v-finish.w,GoTo-Combo = No,Major-Delim = @,Minor-Delim = |" "" "" no no
940 "DRL" "Wizard to guide the user through the process of setting a Rent Review (TTP)" 8 246 "Rent Review Wizard" "" yes "Wizard-Name = Actioning a Rent Review,Wizard-Viewers = vwr/wiz/review/v-tenant.w||Y@vwr/wiz/review/v-review.w||Y@vwr/wiz/review/v-market.w||N@vwr/wiz/review/v-complt.w||Y@vwr/wiz/lease/v-ttp-space.w||Y@vwr/wiz/review/v-finish-ttp.w||N,Viewer-Links = vwr/wiz/review/v-tenant.w|vwr/wiz/review/v-review.w@vwr/wiz/review/v-review.w|vwr/wiz/review/v-market.w@vwr/wiz/review/v-market.w|vwr/wiz/review/v-complt.w@vwr/wiz/review/v-complt.w|vwr/wiz/lease/v-ttp-space.w@vwr/wiz/lease/v-ttp-space.w|vwr/wiz/review/v-finish-ttp.w,Wizard-Finish = vwr/wiz/review/v-finish.w,GoTo-Combo = No,Major-Delim = @,Minor-Delim = |" "" "" no no
941 "DRL" "Wizard to guide the user through the process of a re-leasing (TTP)" 8 261 "Lease Change Wizard" "" yes "Wizard-Name = Lease Change Wizard,Wizard-Viewers = vwr/wiz/release/v-wiz1.w||Y@vwr/wiz/release/v-wiz2.w||Y@vwr/wiz/review/v-space.w||N@vwr/wiz/lease/v-ttp-space.w||Y@vwr/wiz/lease/v-outgng.w||N@vwr/wiz/release/v-finish.w||Y,Viewer-Links = vwr/wiz/release/v-wiz1.w|vwr/wiz/release/v-wiz2.w@vwr/wiz/release/v-wiz2.w|vwr/wiz/review/v-space.w@vwr/wiz/review/v-space.w|vwr/wiz/lease/v-ttp-space.w@vwr/wiz/lease/v-ttp-space.w|vwr/wiz/lease/v-outgng.w@vwr/wiz/lease/v-outgng.w|vwr/wiz/release/v-finish.w,Wizard-Finish = vwr/wiz/release/v-finish.w,GoTo-Combo = No,Major-Delim = @,Minor-Delim = |" "" "" no no
942 "DRL" "Approve Invoices for Issuing to Debtors and create transactions" 8 134 "Invoice Approval" "vwr/mnt/v-invapp.w" yes "" "" "" no no
943 "DRL" "Approve vouchers for payment and create batch" 8 134 "Voucher Approval" "vwr/mnt/v-vchap2.w" yes "" "" "" no no
944 "MSG" "Add a new record" 315 1 "&Add" "Default" no "Message = add-record" "" "" no no
946 "MSG" "Delete the currently selected record" 315 1 "&Delete" "Default" no "Message = delete-record" "" "" no no
947 "DRL" "Browse or Maintain Lists of Ledgers, Companies, Projects, Tenants and so forth" 8 315 "Entity Lists" "vwr/drl/b-entity-list.w" yes "" "" "" no no
948 "DRL" "Browse (and Maintain) the Entities which are members of this list" 315 177 "&Members" "vwr/drl/b-entity-list-member.w" yes "Key-Name = ListCode" "" "" no no
949 "DRL" "Report showing building wardens and floor wardens by building" 8 150 "Warden Summary" "vwr/run/v-warden-summary.w" yes "" "" "" no no
950 "DRL" "Report showing a pictorial representation of the building tenancies" 8 150 "Building Picture" "vwr/run/v-property-picture.w" yes "" "" "" no no
951 "MSG" "Mark cheque as sent today" 161 1 "&Send" "" yes "Message = send-cheque" "" "" no no
952 "MSG" "Mark cheque as not sent" 161 1 "Unsend" "" yes "Message = unsend-cheque" "" "" no no
953 "DRL" "Overrides of this rental space for the Property View Report" 13 177 "Property View Overrides" "vwr/drl/b-property-view.w" yes "Key-Name = RentalSpaceCode" "" "" no no
954 "MSG" "Mark all unsent cheques as sent today" 161 1 "All Sent" "" yes "Message = send-all-cheques" "" "" no no
955 "DRL" "Reports and Exports Property Account balances summarised by account group" 8 150 "Property TB" "vwr/run/v-property-tb.w" yes "" "" "" no no
956 "SEL" "Select property for report" 316 19 "" "vwr/sel/b-selpro.w" yes "SortPanel = Yes,FilterPanel = Yes,Fill-In = fil_prop1,Field = Int1" "fil_prop1" "Int1" no no
957 "SEL" "Select other property in range for report" 316 19 "" "vwr/sel/b-selpro.w" yes "SortPanel = Yes,FilterPanel = Yes,Fill-In = fil_prop2,Field = Int2" "fil_prop2" "Int2" no no
958 "MSG" "Change Queried Voucher back to Unapproved" 15 1 "Un-Query" "" yes "Message = unquery-voucher" "" "" no no
959 "MSG" "Mark voucher as ""Queried""" 15 1 "Query" "" yes "Message = query-voucher" "" "" no no
960 "DRL" "Maintain budgets for the selected account" 3 134 "Budgets" "vwr/mnt/v-budget.w" yes "Key-Name = AccountSummary" "" "" no no
961 "DRL" "Report showing Property Outgoings" 8 150 "Outgoings (AmTrust)" "vwr/run/v-outgoings-amtrust.w" yes "" "" "" no no
962 "SEL" "Select first property for the report" 317 146 "" "vwr/sel/b-selpro.w" yes "AlphabetPanel = Yes,SortPanel = Yes,FilterPanel = Yes,Field = Int1,Fill-In = fil_prop1" "fil_prop1" "Int1" no no
963 "SEL" "Select other property for the report" 317 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = yes,SortPanel = yes,AlphabetPanel = yes,Fill-In = fil_prop2,Field = Int2" "fil_prop2" "Int2" no no
964 "DRL" "Report which prints an Outgoings Budget detail for tenants" 8 150 "Tenant O/G Budget" "vwr/run/v-tog-budget.w" yes "" "" "" no no
965 "SEL" "Select Property for a range of Rental Advices" 318 64 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,SearchPanel = Yes,Fill-In = fil_Property,Field = Int2" "fil_Property" "Int2" no no
966 "SEL" "Select Tenant for a Rental Advice" 318 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes, SortPanel = Yes, AlphabetPanel = Yes, SearchPanel = Yes" "fil_Tenant" "Int1" no no
967 "DRL" "Report showing tenants now washed up" 8 150 "Tenants Not Washed Up" "vwr/run/v-tenants-unwashed.w" yes "" "" "" no no
968 "SEL" "Select first property for Report" 319 146 "" "vwr/sel/b-selpro.w" yes "AlphabetPanel = Yes,SortPanel = Yes,FilterPanel = Yes,Field = Int1,Fill-In = fil_prop1" "fil_prop1" "Int1" no no
969 "SEL" "Select other property for the report" 319 146 "" "" yes "FilterPanel = yes,SortPanel = yes,AlphabetPanel = yes,Fill-In = fil_prop2,Field = Int2" "fil_prop2" "Int2" no no
970 "DRL" "Browse this Project and Subprojects in a 'tree' view" 183 183 "Project Tree" "vwr/drl/b-project-tree.w" yes "Key-Name = ProjectCode,FilterPanel = Yes" "" "" no no
971 "DRL" "Browse Orders for this Property" 11 188 "Orders" "vwr/drl/b-ordprj.w" yes "Key-Name = PropertyCode,FilterPanel = Yes" "" "" no no
973 "SEL" "Select another Approver for this Order" 191 18 "" "vwr/sel/b-selapp.w" yes "Fill-In = fil_Approver2,Field = SecondApprover" "fil_Approver2" "SecondApprover" no no
974 "DRL" "Browse Orders for this Company" 4 188 "Orders" "vwr/drl/b-ordprj.w" yes "Key-Name = CompanyCode,FilterPanel = Yes" "" "" no no
975 "DRL" "Browse Orders for this Creditor" 6 188 "Orders" "vwr/drl/b-ordprj.w" yes "Key-Name = CreditorCode,FilterPanel = Yes" "" "" no no
976 "SEL" "Select Other Tenant for a Stement of Account" 283 64 "" "vwr/sel/b-seltnt.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,SearchPanel = Yes,Fill-In = fil_Tenant2,Field = Int3" "fil_Tenant2" "Int3" no no
977 "DRL" "Browse Contact People for this Creditor/Contractor" 5 277 "Contacts" "vwr/drl/b-entity-contact.w" yes "Key-Name = CreditorCode" "" "" no no
978 "MSG" "Mark call as closed as of now" 313 1 "&Close It" "" yes "Message = mark-as-closed" "" "" no no
979 "MSG" "Edit the current tenant call through a popup dialog" 313 1 "&Edit" "" yes "Message = edit-call" "" "" no no
980 "DRL" "Tenant Calls List report" 8 150 "Tenant Calls Listing" "vwr/run/v-tenant-calls.w" yes "" "" "" no no
981 "MSG" "Delete the currently selected record" 201 1 "&Delete" "" yes "Message = delete-record" "" "" no no
982 "DRL" "A Listing of Creditors" 8 150 "Creditor Listing" "vwr/run/v-creditor-list.w" yes "" "" "" no no
983 "DRL" "Report on Expired Leases" 8 150 "Expired Leases" "vwr/run/v-expired-leases.w" yes "" "" "" no no
984 "DRL" "Amtrust Report on RentalSpace Modifiers for Forecasting" 8 150 "Forecast Rental Multipliers" "vwr/run/v-amtrust-rs-multipliers.w" yes "" "" "" no no
985 "MSG" "Add an account call" 320 1 "&Add" "" yes "Message = new-call" "" "" no no
986 "MSG" "Delete an account call" 320 1 "Delete" "" yes "Message = delete-record" "" "" no no
987 "MSG" "Edit the current accounting call through a popup dialog" 320 1 "&Edit" "" yes "Message = edit-call" "" "" no no
988 "MSG" "Mark call as closed as of now" 320 1 "&Close It" "" yes "Message = mark-as-closed" "" "" no no
989 "DRL" "Browse Tenant Accounts Calls" 8 320 "Accounts Mgmt" "vwr/drl/b-accounts-call.w" yes "SortPanel = Yes,FilterPanel = Yes" "" "" yes yes
990 "DRL" "A one-page-per-tenant report giving all of the forecasting background (AmTrust)" 8 150 "Tenant Forecast Report" "vwr/run/v-amtrust-tenant-fcast.w" yes "" "" "" no no
991 "SEL" "Select a Property for tenant forecast report" 321 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_prop1,Field = Int1" "fil_prop1" "Int1" no no
992 "SEL" "Select second Property for forecast tenant reporting" 321 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_prop2,Field = Int2" "fil_prop2" "Int2" no no
993 "SEL" "Select a Property for Carpark Report" 322 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_prop1,Field = Int1" "fil_prop1" "Int1" no no
995 "SEL" "Select second Property for Carpark Report" 322 146 "" "vwr/sel/b-selpro.w" yes "FilterPanel = Yes,SortPanel = Yes,AlphabetPanel = Yes,Fill-In = fil_prop2,Field = Int2" "fil_prop2" "Int2" no no
997 "DRL" "Carpark Report across a range of Properties" 8 150 "Carpark Report" "vwr/run/v-carpark-report.w" yes "" "" "" no no
998 "DRL" "Report listing all Projects" 8 150 "Project Code List" "vwr/run/v-project-listing.w" yes "" "" "" no no
999 "DRL" "Produce a Tenant Details printout for this tenant" 14 150 "Print" "vwr/mnt/v-tenant-details.w" yes "external-key = TenantCode" "" "" no no
1000 "DRL" "Report showing Building WOF Expiries by building" 8 150 "Building WOF Expiry" "vwr/run/v-wofexpry.w" yes "" "" "" no no
1001 "DRL" "Copy a range of invoices to a new unapproved set" 8 150 "Copy Invoices" "vwr/run/v-copy-invoices.w" yes "" "" "" no no
1002 "MSG" "Reprint the current invoice as a PDF" 67 1 "Reprint PDF" "" yes "Message = reprint-pdfinvoice" "" "" no no
1003 "DRL" "Browse Direct Credit credentials" 8 323 "DC Credentials" "vwr/drl/b-usrdcd.w" yes "" "" "" no no
1004 "DRL" "Add a set of bank credentials" 323 324 "Add" "vwr/mnt/v-usrdcd.w" yes "mode = Add" "" "" no no
1005 "DRL" "Maintain a set of DE credentials" 323 324 "Maintain" "vwr/mnt/v-usrdcd.w" yes "mode = Maintain" "" "" no no
1007 "MSG" "Cancel Changes to Direct Credit credentials" 324 1 "&Cancel" "" yes "Message = cancel-changes" "" "" no no
1008 "MSG" "Confirm changes to Direct Credit credentials" 324 1 "&OK" "" yes "Message = confirm-changes" "" "" no no
1009 "DRL" "Delete the DE credentials" 323 324 "Delete" "vwr/mnt/v-usrdcd.w" yes "mode = Delete" "" "" no no
1010 "DRL" "P&L and Balance Sheet formatted in HTML" 8 150 "HTML Ledgers" "vwr/run/v-html-ledgers.w" yes "" "" "" no no
1011 "SEL" "Select company for report" 326 19 "" "vwr/sel/b-selcmp.w" yes "SortPanel = Yes,FilterPanel = Yes,Fill-In = fil_comp1,Field = Int1" "fil_comp1" "Int1" no no
1012 "MSG" "Copy of Delete this Chart of Account record" 100 1 "&Delete" "" yes "Message = delete-record" "" "" no no
.
PSC
filename=ProgramLink
records=0000000000839
ldbname=ttpl
timestamp=2009/04/06-11:15:34
numformat=44,46
dateformat=dmy-1990
cpstream=UNDEFINED
.
0000122833
