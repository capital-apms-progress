1 "DW" "Account Balances Browser" "win/drl" "w-actbal" "win/drl/w-defdrl.w"
3 "DW" "Accounts Summary Browser" "win/drl" "w-actsum" "win/drl/w-defdrl.w"
4 "DW" "Companies Browser" "win/drl" "w-compny" "win/drl/w-defdrl.w"
5 "DW" "Contracts Browser" "win/drl" "w-contrt" "win/drl/w-defdrl.w"
6 "DW" "Creditors Browser" "win/drl" "w-crdtor" "win/drl/w-defdrl.w"
7 "DW" "Transactions of Account Browser" "win/drl" "w-actacx" "win/drl/w-defdrl.w"
8 "MN" "Main Menu" "win" "win/w-mainmn.w" ""
9 "DW" "Batch (New) Browser" "win/drl" "w-nbcent" "win/drl/w-defdrl.w"
11 "DW" "Properties Browser" "win/drl" "w-proper" "win/drl/w-defdrl.w"
12 "DW" "Property Titles Browser" "win/drl" "w-protit" "win/drl/w-defdrl.w"
13 "DW" "Rental Spaces Browser" "win/drl" "w-rntspc" "win/drl/w-defdrl.w"
14 "DW" "Tenants Browser" "win/drl" "w-tenant" "win/drl/w-defdrl.w"
15 "DW" "Vouchers Browser" "win/drl" "w-vouchr" "win/drl/w-defdrl.w"
17 "DW" "Approval Persons Browser" "win/drl" "w-apprvr" "win/drl/w-defdrl.w"
18 "SW" "Approver Selection" "win" "w-selapp" "win/w-defsel.w"
19 "SW" "Company Selection" "win" "w-selcmp" "win/w-defsel.w"
20 "SW" "Chart of Account Selection" "win" "w-selcoa" "win/w-abcsel.w"
21 "SW" "Creditor Selection" "win" "w-selcrd" "win/w-defsel.w"
22 "SW" "New Batch Selection" "win" "w-selnbc" "win/w-defsel.w"
23 "SW" "Entity Selection" "win" "w-selett" "win/w-defsel.w"
24 "SW" "Persons Selection" "win" "w-selpsn" "win/w-abcsel.w"
28 "MV" "Company Maintenance" "vwr/mnt" "vwr/mnt/v-compny.w" ""
29 "MV" "Creditor Maintenance" "vwr/mnt" "vwr/mnt/v-crdtor.w" ""
32 "MV" "Property Maintenance" "vwr/mnt" "vwr/mnt/v-proper.w" ""
33 "MV" "Rental Space Maintenance" "vwr/mnt" "vwr/mnt/v-rntspc.w" ""
34 "MV" "Tenancy Lease Maintenance" "vwr/mnt" "vwr/mnt/v-tntlse.w" ""
37 "MV" "User Maintenance" "lnk" "sec/v-user.w" ""
40 "DW" "Months Browser" "win/drl" "w-months" "win/drl/w-defdrl.w"
42 "DW" "Transaction Entry" "win/drl" "win/drl/w-trent.w" ""
44 "DW" "Transactions of Document Browser" "win/drl" "w-actdoc" "win/drl/w-defdrl.w"
45 "DW" "Account Groups Browser" "win/drl" "w-actgrp" "win/drl/w-defdrl.w"
47 "DW" "Financial Years Browser" "win/drl" "w-fnclyr" "win/drl/w-defdrl.w"
48 "DW" "Offices Maintenance" "win/drl" "w-office" "win/drl/w-defdrl.w"
52 "DW" "Approver Maintenance" "win/drl" "w-appmnt" "win/drl/w-defdrl.w"
54 "DW" "Chart of Accounts Maintenance" "win/drl" "w-coamnt" "win/drl/w-defdrl.w"
55 "DW" "Contracts Maintenance" "win/drl" "w-crtmnt" "win/drl/w-defdrl.w"
57 "DW" "Property Maintenance" "win/drl" "w-promnt" "win/drl/w-defdrl.w"
58 "DW" "Rental Space Maintenance" "win/drl" "w-rspmnt" "win/drl/w-defdrl.w"
59 "DW" "Company Maintenance" "win/drl" "w-cmpmnt" "win/drl/w-defdrl.w"
62 "DW" "Tenancy Leases Maintenance" "win/drl" "w-tlsmnt" "win/drl/w-defdrl.w"
63 "DW" "Voucher Registration / Maintenance" "win/drl" "w-vchreg" "win/drl/w-defdrl.w"
64 "SW" "Tenant Selection" "win" "w-seltnt" "win/w-defsel.w"
65 "DW" "Property Title Maintenance" "win/drl" "w-ptimnt" "win/drl/w-defdrl.w"
67 "DW" "Invoices Browser" "win/drl" "w-ivoice" "win/drl/w-defdrl.w"
68 "DW" "Invoice Maintenance" "win/drl" "w-invmnt" "win/drl/w-defdrl.w"
69 "MV" "Invoice Maintenance" "vwr/mnt" "vwr/mnt/v-ivoice.w" ""
74 "DW" "Tenancy Leases Browser" "win/drl" "w-tntlse" "win/drl/w-defdrl.w"
80 "DW" "Bank Account Maintenance" "win/drl" "w-bacmnt" "win/drl/w-defdrl.w"
81 "MV" "Bank Account Maintenance" "vwr/mnt" "vwr/mnt/v-bnkact.w" ""
83 "MV" "Contracts Maintenance" "vwr/mnt" "vwr/mnt/v-contrt.w" ""
84 "DW" "Program Link Maintenance / Viewer" "lnk" "lnk/w-lnkmnt.w" ""
88 "DW" "Account Group Resequencing" "win/drl" "win/drl/w-acgres.w" ""
91 "MV" "Directorship Maintenance" "vwr/mnt" "vwr/mnt/v-dirctr.w" ""
92 "MV" "Share Holder Maintenance" "vwr/mnt" "vwr/mnt/v-shrhld.w" ""
93 "MV" "Rental Advice and A/P Form (Manual Print)" "vwr/mnt" "vwr/mnt/v-rntadv.w" ""
96 "MN" "Property Functions Menu" "win" "w-promnu" "win/w-defmnu.w"
97 "MN" "Accounting Functions Menu" "win" "w-accmnu" "win/w-defmnu.w"
98 "MN" "Administration Menu" "win" "w-admmnu" "win/w-defmnu.w"
99 "DW" "Service Type Maintenance" "win/drl" "w-srvtyp" "win/drl/w-defdrl.w"
100 "DW" "Chart of Accounts Browser" "win/DRL" "w-choact" "win/drl/w-defdrl.w"
101 "DW" "Frequency Types Browser" "win/drl" "w-frqtyp" "win/drl/w-defdrl.w"
102 "DW" "Frequency Type Maintenance" "win/drl" "w-frqmnt" "win/drl/w-defdrl.w"
108 "DW" "Contacts Browser" "win/drl" "w-person" "win/drl/w-defdrl.w"
112 "DW" "Total Viewer" "win/DRL" "w-totals" "win/drl/w-defdrl.w"
116 "DW" "Tenant Maintenance" "win/DRL" "w-tntmnt" "win/drl/w-defdrl.w"
118 "MV" "Tenant Maintenance Viewer" "vwr/mnt" "vwr/mnt/v-tenant.w" ""
119 "DW" "Tenant Transactions Report" "win/DRL" "w-trtrpt" "win/drl/w-defdrl.w"
121 "MN" "Accounting Reports Menu" "win" "w-actrpt" "win/w-defmnu.w"
123 "MV" "Tenant Transactions Report Parameters" "vwr/mnt" "vwr/mnt/v-tr-t.w" ""
124 "DW" "Creditor Transactions Report" "win/DRL" "w-tr-c" "win/drl/w-defdrl.w"
125 "MV" "Creditor Transactions Report Parameters" "vwr/mnt" "vwr/mnt/v-tr-c.w" ""
126 "DW" "Receipts and Payments Maintenance" "win/drl" "win/drl/w-rpent.w" ""
127 "DW" "Transaction Report" "win/drl" "w-trnrpt" "win/drl/w-defdrl.w"
128 "MV" "GL Account Transactions Report Parameters" "vwr/mnt" "vwr/mnt/v-tr-l.w" ""
129 "DW" "Transaction Update" "win/DRL" "w-kcktru" "win/drl/w-defdrl.w"
131 "DW" "Transaction Print" "win/drl" "w-kckprt" "win/drl/w-defdrl.w"
134 "DW" "Default Drill Window" "win/drl" "win/drl/w-defdrl.w" ""
135 "DW" "Office Control Accounts" "win/drl" "w-ocaofc" "win/drl/w-defdrl.w"
136 "DW" "Office Settings" "win/drl" "office-settings" "win/drl/w-defdrl.w"
137 "DW" "Printer Selection" "win/drl" "w-selprt" "win/drl/w-defdrl.w"
138 "MN" "Menu Administration" "win" "menu-admin-menu" "win/w-defmnu.w"
139 "DW" "User Groups Browser" "win/drl" "w-usrgrp" "win/drl/w-defdrl.w"
140 "DW" "Contact Listing" "win/drl" "w-ctclst" "win/drl/w-defdrl.w"
141 "DW" "Cheque Printing" "win/drl" "w-chqprt" "win/drl/w-defdrl.w"
142 "DW" "Batch (Posted) Browser" "win/drl" "w-batch" "win/drl/w-defdrl.w"
144 "DW" "Documents Browser" "win/drl" "w-docmnt" "win/drl/w-defdrl.w"
145 "MV" "Property Account Transaction Report Parameters" "vwr/mnt" "vwr/mnt/v-tr-p.w" ""
146 "SW" "Property Selection" "win" "w-selpro" "win/w-defsel.w"
147 "SW" "Meter Number Selection" "win" "w-selmtr" "win/w-defsel.w"
148 "DW" "Company Viewer" "win/drl" "w-cmpvwr" "win/drl/w-defdrl.w"
149 "MV" "Electricity Invoice Registration" "vwr/mnt" "vwr/mnt/v-elcinv.w" ""
150 "DW" "Report Initiation Window" "win/drl" "w-rptini" "win/drl/w-defdrl.w"
151 "MV" "Trial Balance by Group Report" "vwr/mnt" "vwr/mnt/v-tbgrpt.w" ""
153 "DW" "Creditors Maintenance" "win/drl" "w-crdmnt" "win/drl/w-defdrl.w"
155 "DW" "Manual Transaction Closing" "win" "win/w-trncls.w" ""
157 "MV" "statement of Account" "vwr/mnt" "vwr/mnt/v-statmt.w" ""
158 "MV" "Building Income Statement" "vwr/mnt" "vwr/mnt/v-bldinc.w" ""
160 "MV" "Property Schedule Parameters" "vwr/mnt" "vwr/mnt/v-schdul.w" ""
161 "DW" "Cheques Browser" "win/drl" "w-cheque" "win/drl/w-defdrl.w"
162 "DW" "Rent Reviews browser" "win/drl" "w-rntrvw" "win/drl/w-defdrl.w"
164 "DW" "Sub Leases Browser" "win/drl" "w-sublse" "win/drl/w-defdrl.w"
165 "DW" "Street Frontages Browser" "win/drl" "w-stfrnt" "win/drl/w-defdrl.w"
166 "MN" "Property Reports Menu" "win" "w-prprpt" "win/w-defmnu.w"
167 "DW" "Bank Accounts Browser" "win/drl" "w-bnkact" "win/drl/w-defdrl.w"
168 "DW" "Person Maintenance" "win/drl" "w-psnmnt" "win/drl/w-defdrl.w"
171 "DW" "Guarantors Browser" "win/drl" "w-grntor" "win/drl/w-defdrl.w"
172 "DW" "Guarantors Maintenance" "win/drl" "w-gtrmnt" "win/drl/w-defdrl.w"
174 "DW" "Consolidation List Browser" "win/drl" "w-conlst" "win/drl/w-defdrl.w"
175 "DW" "Consolidation List Maintenance" "win/drl" "w-clsmnt" "win/drl/w-defdrl.w"
176 "DW" "Area Types Maintenance" "win/drl" "w-atymnt" "win/drl/w-defdrl.w"
177 "DW" "'Any' Type Browse (Add-Delete)" "win/drl" "w-ltymnt" "win/drl/w-defdrl.w"
178 "DW" "Contact Types Browser / Maintenance" "win/drl" "w-ctctyp" "win/drl/w-defdrl.w"
179 "DW" "Cheque Viewer / Maintenance" "win/drl" "w-chqmnt" "win/drl/w-defdrl.w"
180 "DW" "Outgoings Maintenance" "win/drl" "win/drl/w-tncotg.w" ""
181 "MN" "General Exports" "win" "general-exports" "win/w-defmnu.w"
182 "DW" "Projects Maintenance" "win/drl" "w-prjmnt" "win/drl/w-defdrl.w"
183 "DW" "Projects Browser" "win/drl" "w-projct" "win/drl/w-defdrl.w"
184 "MV" "Project Maintenance" "vwr/mnt" "vwr/mnt/v-projct.w" ""
186 "MN" "Budget Reports" "win" "w-budmnu" "win/w-defmnu.w"
187 "DW" "Valuations Browser" "win/drl" "w-valutn" "win/drl/w-defdrl.w"
188 "DW" "Orders Browser" "win/drl" "w-order" "win/drl/w-defdrl.w"
189 "DW" "Orders Maintenance" "win/drl" "w-ordmnt" "win/drl/w-defdrl.w"
190 "SW" "Project Selection" "win" "w-selprj" "win/w-defsel.w"
191 "MV" "Orders Maintenance" "vwr/mnt" "vwr/mnt/v-order.w" ""
192 "DW" "Project Budgets Browser" "win/drl" "w-prjbdg" "win/drl/w-defdrl.w"
193 "MN" "Management Reports" "win" "w-mgtmnu" "win/w-defmnu.w"
194 "MN" "Code Listings Menu" "win" "w-codmny" "win/w-defmnu.w"
195 "DW" "Project Budgets Maintenance" "win/drl" "w-pbdmnt" "win/drl/w-defdrl.w"
196 "MV" "Project Budgets Maintenance" "vwr/mnt" "vwr/mnt/v-prjbdg.w" ""
197 "DW" "Project Budget Cash Flows" "win/drl" "w-prjcsh" "win/drl/w-defdrl.w"
198 "DW" "Valuations Viewer" "win/drl" "w-proval" "win/drl/w-defdrl.w"
199 "MV" "Ground Lease Maintenance" "vwr/mnt" "vwr/mnt/v-grdlse.w" ""
201 "DW" "Ground Leases Browser" "win/drl" "w-grdlse" "win/drl/w-defdrl.w"
202 "DW" "'Any' type Maintain (OK-Cancel)" "win/drl" "w-anymnt" "win/drl/w-defdrl.w"
203 "MV" "Project Tracking Report" "vwr/mnt" "vwr/mnt/v-prjtrk.w" ""
204 "MV" "Project Cash Flow Report" "vwr/mnt" "vwr/mnt/v-prjcfl.w" ""
205 "MV" "Approver Maintenance" "vwr/mnt" "vwr/mnt/v-apprvr.w" ""
206 "MV" "FID Charges Generation" "vwr/mnt" "vwr/mnt/v-fidchg.w" ""
207 "DW" "Open/Close Months Tool" "win/drl" "w-mthopn" "win/drl/w-defdrl.w"
208 "DW" "Regions Browser" "win/drl" "w-region" "win/drl/w-defdrl.w"
210 "DW" "Users Browser" "win/drl" "w-user" "win/drl/w-defdrl.w"
211 "DW" "User Maintenance" "win/drl" "w-usrmnt" "win/drl/w-defdrl.w"
212 "DW" "User Group Menus" "win/drl" "w-usgmnu" "win/drl/w-defdrl.w"
213 "DW" "User Group Memberships" "win/drl" "w-usgmem" "win/drl/w-defdrl.w"
214 "DW" "User Group Rights Browser" "win/drl" "w-usgrts" "win/drl/w-defdrl.w"
215 "DW" "Directorship Browser" "win/drl" "w-dirctr" "win/drl/w-defdrl.w"
216 "DW" "Directorship Maintenance" "win/drl" "w-dirmnt" "win/drl/w-defdrl.w"
217 "DW" "ShareHolder Browser" "win/drl" "w-shrhld" "win/drl/w-defdrl.w"
218 "DW" "ShareHolder Maintenance" "win/drl" "w-shrmnt" "win/drl/w-defdrl.w"
219 "MV" "Property Valuations Viewer" "vwr/mnt" "vwr/mnt/v-proval.w" ""
220 "MV" "Debtors Report" "vwr/mnt" "vwr/mnt/v-dbtrpt.w" ""
221 "DW" "Scenario Browser" "win/drl" "w-scnrio" "win/drl/w-defdrl.w"
222 "DW" "Scenario Maintenance" "win/drl" "w-scnmnt" "win/drl/w-defdrl.w"
223 "DW" "Cash Flow Browser" "win/drl" "w-cshflw" "win/drl/w-defdrl.w"
224 "DW" "Cash Flow Maintenance" "win/drl" "w-cflmnt" "win/drl/w-defdrl.w"
225 "MV" "Cash Flow Maintenance" "vwr/mnt" "vwr/mnt/v-cshflw.w" ""
226 "DW" "Forecast Generation" "win/drl" "w-frecst" "win/drl/w-defdrl.w"
227 "DW" "'Any' type Browse (no buttons)" "win/drl" "w-anynob" "win/drl/w-defdrl.w"
228 "MV" "Project Transaction Report Parameters" "vwr/mnt" "vwr/mnt/v-tr-j.w" ""
229 "MV" "Tenant Lisiting Parameters" "vwr\mnt" "vwr/mnt/v-tntlst.w" ""
230 "MV" "Entity Budget Report Parameters" "vwr\mnt" "vwr/mnt/v-budrpt.w" ""
231 "SW" "Financial Years Selection" "win" "w-selfyr" "win/w-defsel.w"
232 "MV" "Voucher view/maintain/register/approve" "vwr\mnt" "vwr/mnt/v-vouchr.w" ""
233 "MV" "Scenario Selection (Current)" "vwr/mnt" "vwr/mnt/v-selscn.w" ""
234 "SW" "Scenario Selection" "win" "w-selscn" "win/w-defsel.w"
235 "MV" "Service Contract Listing" "vwr\mnt" "vwr/mnt/v-ctrlst.w" ""
236 "MN" "Forecasting Menu" "win" "forecasting-menu" "win/w-defmnu.w"
237 "DW" "Scenario Assumptions" "win/drl" "w-scnass" "win/drl/w-defdrl.w"
238 "MN" "System Codes Tables" "win" "system-code-menu" "win/w-defmnu.w"
239 "DW" "Project Variations" "win/drl" "w-varitn" "win/drl/w-defdrl.w"
240 "DW" "Project Variation Flows" "win/drl" "w-varflw" "win/drl/w-defdrl.w"
241 "DW" "Replication Collision Log Browser" "win\drl" "w-repcol" "win/drl/w-defdrl.w"
242 "DW" "Replication Log Browser" "win\drl" "w-replog" "win/drl/w-defdrl.w"
243 "DW" "Replication Out Rules" "win\drl" "w-rpltrg" "win/drl/w-defdrl.w"
244 "DW" "Replication In Rules" "win\drl" "w-reprul" "win/drl/w-defdrl.w"
245 "MV" "Scenario to Budget Processing" "vwr/mnt" "vwr/mnt/v-scnbud.w" ""
246 "DW" "New Lease Wizard" "win" "w-wiznls" "win/w-defwiz.w"
247 "DW" "Wizard Definition Wizard" "win" "w-wizwiz" "win/w-defwiz.w"
248 "DW" "Rental Charges Browser" "win/drl" "w-rntchg" "win/drl/w-defdrl.w"
249 "DW" "Rent Charging Schedule" "win/drl" "w-rntchs" "win/drl/w-defdrl.w"
250 "MN" "Reporting Menu" "win" "w-report" "win/w-defmnu.w"
251 "MN" "Project Reports" "win" "w-prjrep" "win/w-defmnu.w"
252 "MN" "Replication Menu" "win" "w-rplmnu" "win/w-defmnu.w"
253 "MV" "Select Tenant for New Lease Wizard" "vwr/mnt" "vwr/wiz/lease/v-tenant.w" ""
254 "MV" "Select Property for New Lease Wizard" "vwr/mnt" "vwr/wiz/lease/v-space.w" ""
255 "MV" "Rental Charges Maintenance" "" "vwr/mnt/v-rntchr.w" ""
256 "MN" "System Reports Menu" "win" "w-sysrpt" "win/w-defmnu.w"
257 "MV" "Tenant Aged Balance" "vwr/mnt" "vwr/mnt/v-tntbal.w" ""
258 "MN" "Accounts Codes Tables" "" "acct-code-menu" "win/w-defmnu.w"
259 "MN" "Property Codes Tables" "" "prop-code-menu" "win/w-defmnu.w"
260 "MV" "Screen one of Rent Review wizard" "vwr/mnt" "vwr/wiz/review/v-wiz1.w" ""
261 "DW" "Re-Leasing Wizard" "win" "w-wizrls" "win/w-defwiz.w"
262 "MV" "Screen one of Re-Leasing wizard" "vwr/mnt" "vwr/wiz/release/v-wiz1.w" ""
263 "DW" "External Files Browser" "" "w-images.w" "win/drl/w-defdrl.w"
264 "MV" "Scenario Tool" "vwr/mnt" "vwr/mnt/v-scntol.w" ""
265 "MV" "Rental Invoice Registration" "" "vwr/mnt/v-rntinv.w" ""
266 "MV" "Debtor Statistics Report" "vwr/mnt" "vwr/mnt/v-dbstat.w" ""
267 "DW" "Offices Browser" "win/drl" "office-browser" "win/drl/w-defdrl.w"
268 "DW" "Tasks Due Browser" "" "tasks-due-browse" "win/drl/w-defdrl.w"
269 "DW" "Property Descriptions" "" "property-descriptions" "win/drl/w-defdrl.w"
270 "DW" "Clients Browser" "" "clients-browser" "win/drl/w-defdrl.w"
271 "DW" "Client Maintenance" "" "client-maintenance" "win/drl/w-defdrl.w"
272 "MV" "Export Tenant Charges" "" "vwr/mnt/v-tenant-charges.w" ""
273 "MN" "Workflow Codes Tables" "" "workflow-codes-tables" "win/w-defmnu.w"
274 "DW" "Workflow Rules Browser" "" "flow-rules-browser" "win/drl/w-defdrl.w"
275 "DW" "Workflow Task Types Browser" "" "flow-task-types" "win/drl/w-defdrl.w"
276 "DW" "Workflow Steps of Task Browser" "" "flow-step-browser" "win/drl/w-defdrl.w"
277 "DW" "Entity Contacts Browser" "" "entity-contacts-browser" "win/drl/w-defdrl.w"
278 "SW" "Client Selection" "" "client-selection" "win/w-defsel.w"
279 "DW" "Inspectors Browser" "" "inspector-browser" "win/drl/w-defdrl.w"
280 "DW" "Building Systems Browser" "" "building-system-browser" "win/drl/w-defdrl.w"
281 "DW" "Building System Maintenance" "" "building-system-maintenance" "win/drl/w-defdrl.w"
282 "DW" "Scenario Rental Assumptions" "" "scenario-rental-assumptions" "win/drl/w-defdrl.w"
283 "MV" "Invoice / Statement" "vwr/mnt" "vwr/mnt/v-invoice-statement.w" ""
284 "MV" """Pink"" view/maintain/register" "vwr\mnt" "vwr/mnt/v-pink.w" ""
285 "MV" "Tenant Outgoings Budgets & Reconciliation" "vwr/mnt" "vwr/mnt/v-tenant-outgoing-budgets.w" ""
286 "MV" "Month End OG Accruals" "vwr/mnt" "vwr/mnt/v-month-end-og-accrual.w" ""
287 "MV" "Actual Recoveries" "vwr/mnt" "vwr/mnt/v-actual-recoveries.w" ""
288 "MN" "Automatic Transactions" "" "automatic-transactions" "win/w-defmnu.w"
289 "DW" "Menu Browser" "" "menu-browser" "win/drl/w-defdrl.w"
290 "DW" "Batch Queue Viewer" "" "batch-queue-viewer" "win/drl/w-defdrl.w"
291 "MV" "Building WOF initiation" "vwr/mnt" "vwr/mnt/v-building-wof.w" ""
292 "DW" "Building System Types Browser" "" "building-system-types" "win/drl/w-defdrl.w"
293 "DW" "Entity Contact Maintenance" "" "entity-contact-maintenance" "win/drl/w-defdrl.w"
295 "DW" "Property Outgoings Browser" "win/drl" "property-outgoings-browser" "win/drl/w-defdrl.w"
296 "DW" "Cheque Viewer (view only)" "" "cheque-view" "win/drl/w-defdrl.w"
297 "MV" "Tenant Details Printout" "vwr/mnt" "vwr/mnt/v-tenant-details.w" ""
298 "MV" "AmTrust Forecast Generation" "vwr/mnt" "forecast/v-run-amtrust-rental.w" ""
299 "DW" "Property Forecast Parameters (Amtrust)" "win/drl" "prop-forecast-param" "win/drl/w-defdrl.w"
300 "MV" "Amtrust Forecast Rent Report" "vwr/mnt" "forecast/v-amtrust-rent-report.w" ""
301 "MV" "Amtrust Market Rentals Report" "vwr/mnt" "forecast/v-amtrust-market-rents.w" ""
302 "MV" "Portfolio Yield Report" "vwr/mnt" "vwr/mnt/v-portfolio-yield.w" ""
304 "DW" "Supply Meters Browser" "win/drl" "supply-meters-browser" "win/drl/w-defdrl.w"
305 "MV" "Inspector Maintenance" "vwr/mnt" "compliance/v-inspector.w" ""
306 "MV" "Task Maintenance" "vwr/mnt" "workflow/v-flow-task.w" ""
307 "DW" "Steps of Task Browser" "" "workflow/b-flow-step.w" "win/drl/w-defdrl.w"
308 "MV" "Current Income and Budgetted Expenditure Parameters" "vwr/mnt" "vwr/run/v-curr-inc-bud-exp.w" ""
309 "MV" "Sydney Weighted Average Lease Life Parameters" "vwr/mnt" "vwr/run/v-sydney-wall.w" ""
310 "MV" "Building Area Reconciliation Parameters" "vwr/mnt" "vwr/run/v-bldg-area-reconciliation.w" ""
311 "MV" "Building Act Report" "vwr\mnt" "vwr/run/v-building-act.w" ""
312 "MV" "Property Verification Report" "vwr\mnt" "vwr/run/v-property-verification.w" ""
313 "DW" "Tenant Calls Browser" "" "tenant-callss-browser" "win/drl/w-defdrl.w"
314 "MN" "Tenant Call Reports" "" "tenant-call-reports" "win/w-defmnu.w"
315 "DW" "Entity List Browser" "win/drl" "entity-list-browser" "win/drl/w-defdrl.w"
316 "MV" "Property Trial Balance Report" "vwr/mnt" "vwr/run/v-property-tb.w" ""
317 "MV" "Amtrust Outgoings" "vwr\mnt" "vwr/run/v-outgoings-amtrust.w" ""
318 "MV" "Tenant Outgoings Budget" "vwr/mnt" "vwr/run/v-tog-budget.w" ""
319 "MV" "Tenants Not Washed Up" "vwr\mnt" "vwr/run/v-tenants-unwashed.w" ""
320 "DW" "Accounting Calls Browser" "" "accounts-calls-browser" "win/drl/w-defdrl.w"
321 "MV" "Amtrust Tenant Forecast Report" "" "vwr/run/v-amtrust-tenant-fcast.w" "win/w-defmnt.w"
322 "MV" "Amtrust Carpark Report" "" "vwr/run/v-carpark-report.w" "win/w-defmnt.w"
323 "DW" "Direct Credit Credential Browser" "" "w-dcacct" "win/drl/w-defdrl.w"
324 "DW" "Direct Credit Credentials Maintenance" "" "b-usrdcd" "win/drl/w-defdrl.w"
325 "MV" "Direct Credit Details Maintenance" "vwr/mnt" "vwr/mnt/v-usrdcd.w" ""
326 "MV" "HTML Ledger Report" "" "vwr/run/v-html-ledgers.w" ""
.
PSC
filename=LinkNode
records=0000000000255
ldbname=ttpl
timestamp=2009/04/06-11:15:34
numformat=44,46
dateformat=dmy-1990
cpstream=UNDEFINED
.
0000019181
