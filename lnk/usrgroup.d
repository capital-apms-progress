"AccountsAdmin" "Accounts Administrator" 71
"AccountsBase" "Basic Accounting Functions" 11
"AccountsClerical" "Accounting Functions for all Accounting Clerks" 15
"AccountsLedger" "Property and General Ledger Accounting" 24
"AccountsPayable" "Accounts Payable Functions" 20
"AccountsPayablePink" "Accounts Payable - Approve then Enter" 19
"AccountsPayableTTP" "Accounts Payable - Enter then Approve" 18
"AccountsProject" "Accounting functions related to Project Management" 26
"AccountsProperty" "Property Accountants" 25
"AccountsReceivable" "Accounts Receivable Functions" 23
"AccountsReceivableTTP" "TTP Accounts Receivable Functions" 23
"AccountsSysAdmin" "Accounts Systems Administrator" 72
"AkldAccounts" "Auckland Accounting Staff" 16
"AmTrust" "AmTrust Pacific Ltd" 50
"ClientsAdmin" "Clients System Administrator" 90
"ClientsBase" "Access the system through Clients browser" 10
"ComplianceAdmin" "Compliance Functions Administration" 83
"ComplianceBase" "Basic Building Compliance Functions" 32
"ComplianceMaint" "Maintenance of Compliance Details" 41
"ContactAdmin" "Administration of Contact Types, esp EntityContact types" 103
"ContactMaint" "Maintenance of Contact Information- esp Entity 1's" -115
"ContractMaint" "Service Contracts Maintenance" 30
"DirectPaymentsAuthority" "Authorisor of Direct Payments" 0
"Everyone" "Absolutely everyone" -135
"Executive" "Executive User" 60
"ForecastAdmin" "Forecasting Module Administration" 100
"ForecastAmTrust" "AmTrust Forecasting System" 64
"Forecasting" "Forecasting module" 70
"ForecastRentals" "Rental Forecasting System" 63
"PeriodReporting" "Generate and Maintain Period Reporting" 53
"Programmer" "Under test or whatever" 120
"Projects" "Project Management Module" 42
"ProjectsAdmin" "Project Management Module Administration" 84
"PropertyAdmin" "Property Administrator" 82
"PropertyBase" "Basic Property Functions" 27
"PropertyMaint" "Maintenance of Property and related details" 33
"PropertyManager" "Functions for Property Managers" 31
"RemoteUser" "Remote Users - no bitmaps on menus" 129
"Replicator" "Administration of database replication functions" 110
"Sales" "Sales Department User" 40
"SdnyAccounts" "Sydney Accounting Users" 17
"System Admin" "System Administrator" 105
"TenantCallsAdmin" "Tenant Calls Database - administrator" 115
"TenantCallsBase" "Tenant Calls Database - most users" 65
"WizPropertyAGP" "Property Wizards at AGP" 28
"WizPropertyAmTrust" "Property Wizards at AmTrust" 29
"WizPropertyTTP" "Property Wizards at TTP" 34
"WorkFlowAdmin" "WorkFlow Administrator" 95
"WorkflowBase" "Basic Workflow User Access" 61
"WorkflowManager" "Workflow Access for Managers" 62
.
PSC
filename=UsrGroup
records=0000000000050
ldbname=ttpl
timestamp=2009/04/06-11:15:35
numformat=44,46
dateformat=dmy-1990
cpstream=UNDEFINED
.
0000002708
