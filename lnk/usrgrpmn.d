"AccountsAdmin" "Companies Browser" 4 no
"AccountsAdmin" "Creditors Browser" 6 no
"AccountsAdmin" "Transactions of Account Browser" 7 no
"AccountsAdmin" "Main Menu" 8 no
"AccountsAdmin" "Batch(New) Browser" 9 no
"AccountsAdmin" "Properties Browser" 11 no
"AccountsAdmin" "Vouchers Browser" 15 no
"AccountsAdmin" "Approval Persons Browser" 17 no
"AccountsAdmin" "Approver Selection" 18 no
"AccountsAdmin" "Company Selection" 19 no
"AccountsAdmin" "Chart of Account Selection" 20 no
"AccountsAdmin" "Creditor Selection" 21 no
"AccountsAdmin" "Entity Selection" 23 no
"AccountsAdmin" "Months Browser" 40 no
"AccountsAdmin" "Account Groups Maintenance" 45 no
"AccountsAdmin" "Financial Years Browser" 47 no
"AccountsAdmin" "Approver Maintenance" 52 no
"AccountsAdmin" "Chart of Accounts Maitenance" 54 no
"AccountsAdmin" "Property Maintenance" 57 no
"AccountsAdmin" "Company Maintenance" 59 no
"AccountsAdmin" "Tenant Selection" 64 no
"AccountsAdmin" "Invoices Browser" 67 no
"AccountsAdmin" "Bank Account Maintenance" 80 no
"AccountsAdmin" "Accounting Functions Menu" 97 no
"AccountsAdmin" "System Administration Menu" 98 no
"AccountsAdmin" "Chart of Accounts Browser" 100 no
"AccountsAdmin" "Tenant Maintenance" 116 no
"AccountsAdmin" "Office Control Accounts" 135 no
"AccountsAdmin" "Batch (Posted) Browser" 142 no
"AccountsAdmin" "Meter Number Selection" 147 no
"AccountsAdmin" "Cheques Browser" 161 no
"AccountsAdmin" "Bank Accounts Browser" 167 no
"AccountsAdmin" "Consolidation List Browser" 174 no
"AccountsAdmin" "Consolidation List Maintenance" 175 no
"AccountsAdmin" "'Any' Type Browse (Add-Delete)" 177 no
"AccountsAdmin" "Projects Browser" 183 no
"AccountsAdmin" "Valuations Browser" 187 no
"AccountsAdmin" "Management Reports" 193 no
"AccountsAdmin" "'Any' type Maintain (OK-Cancel)" 202 no
"AccountsAdmin" "Directorship Browser" 215 no
"AccountsAdmin" "Directorship Maintenance" 216 no
"AccountsAdmin" "ShareHolder Browser" 217 no
"AccountsAdmin" "ShareHolder Maintenance" 218 no
"AccountsAdmin" "System Reports Menu" 256 no
"AccountsAdmin" "Accounts Codes Tables" 258 no
"AccountsAdmin" "Automatic Transactions" 288 no
"AccountsAdmin" "Supply Meters Browser" 304 no
"AccountsAdmin" "Entity List Browser" 315 no
"AccountsBase" "Account Balances Browser" 1 no
"AccountsBase" "Accounts Summary Browser" 3 no
"AccountsBase" "Companies Browser" 4 no
"AccountsBase" "Creditors Browser" 6 no
"AccountsBase" "Transactions of Account Browser" 7 no
"AccountsBase" "Main Menu" 8 no
"AccountsBase" "Properties Browser" 11 no
"AccountsBase" "Tenants Browser" 14 no
"AccountsBase" "Transactions of Document Browser" 44 no
"AccountsBase" "Invoices Browser" 67 no
"AccountsBase" "Accounting Functions Menu" 97 no
"AccountsBase" "Chart of Accounts Browser" 100 no
"AccountsBase" "Accounting Reports Menu" 121 no
"AccountsBase" "Batch (Posted) Browser" 142 no
"AccountsBase" "Documents Browser" 144 no
"AccountsBase" "Cheques Browser" 161 no
"AccountsBase" "Reporting Menu" 250 no
"AccountsClerical" "Batch (New) Browser" 9 no
"AccountsClerical" "Accounting Functions Menu" 97 no
"AccountsClerical" "Batch (Posted) Browser" 142 no
"AccountsClerical" "Automatic Transactions" 288 no
"AccountsLedger" "Companies Browser" 4 no
"AccountsLedger" "Transactions of Account Browser" 7 no
"AccountsLedger" "Main Menu" 8 no
"AccountsLedger" "Chart of Accounts Maintenance" 54 no
"AccountsLedger" "Accounting Functions Menu" 97 no
"AccountsLedger" "Chart of Accounts Browser" 100 no
"AccountsLedger" "Accounting Reports Menu" 121 no
"AccountsLedger" "Cheques Browser" 161 no
"AccountsLedger" "Consolidation List Browser" 174 no
"AccountsLedger" "Consolidation List Maintenance" 175 no
"AccountsLedger" "Cheque Viewer / Maintenance" 179 no
"AccountsLedger" "Directorship Browser" 215 no
"AccountsLedger" "Directorship Maintenance" 216 no
"AccountsLedger" "ShareHolder Browser" 217 no
"AccountsLedger" "ShareHolder Maintenance" 218 no
"AccountsPayable" "Creditors Browser" 6 no
"AccountsPayable" "Transactions of Account Browser" 7 no
"AccountsPayable" "Batch (New) Browser" 9 no
"AccountsPayable" "Vouchers Browser" 15 no
"AccountsPayable" "Creditor Selection" 21 no
"AccountsPayable" "Accounting Functions Menu" 97 no
"AccountsPayable" "Accounting Reports Menu" 121 no
"AccountsPayable" "Creditors Maintenance" 153 no
"AccountsPayable" "Cheques Browser" 161 no
"AccountsPayable" "Cheque Viewer / Maintenance" 179 no
"AccountsPayable" "Automatic Transactions" 288 no
"AccountsPayablePink" "Vouchers Browser" 15 no
"AccountsPayablePink" "Cheques Browser" 161 no
"AccountsPayableTTP" "Vouchers Browser" 15 no
"AccountsPayableTTP" "Accounting Functions Menu" 97 no
"AccountsProject" "Companies Browser" 4 no
"AccountsProject" "Creditors Browser" 6 no
"AccountsProject" "Projects Browser" 183 no
"AccountsProject" "Orders Browser" 188 no
"AccountsProject" "Orders Maintenance" 189 no
"AccountsProject" "Project Budgets Browser" 192 no
"AccountsProject" "Project Variations" 239 no
"AccountsProperty" "Accounts Summary Browser" 3 no
"AccountsProperty" "Rental Spaces Browser" 13 no
"AccountsProperty" "Accounting Reports Menu" 121 no
"AccountsProperty" "Property Reports Menu" 166 no
"AccountsProperty" "General Exports" 181 no
"AccountsProperty" "Rental Charges Browser" 248 no
"AccountsProperty" "Property Outgoings Browser" 295 no
"AccountsReceivable" "Transactions of Account Browser" 7 no
"AccountsReceivable" "Batch (New) Browser" 9 no
"AccountsReceivable" "Tenants Browser" 14 no
"AccountsReceivable" "Tenant Selection" 64 no
"AccountsReceivable" "Invoices Browser" 67 no
"AccountsReceivable" "Accounting Functions Menu" 97 no
"AccountsReceivable" "Tenant Maintenance" 116 no
"AccountsReceivable" "Accounting Reports Menu" 121 no
"AccountsReceivable" "Meter Number Selection" 147 no
"AccountsReceivable" "Automatic Transactions" 288 no
"AccountsReceivable" "Accounting Calls Browser" 320 no
"AccountsReceivableTTP" "Tenants Browser" 14 no
"AmTrust" "Rental Spaces Browser" 13 no
"AmTrust" "Property Maintenance" 57 no
"AmTrust" "Accounting Reports Menu" 121 no
"AmTrust" "Automatic Transactions" 288 no
"ClientsAdmin" "Accounts Codes Tables" 258 no
"ClientsAdmin" "Clients Browser" 270 no
"ClientsAdmin" "Client Maintenance" 271 no
"ClientsBase" "Property Functions Menu" 96 no
"ClientsBase" "Clients Browser" 270 no
"ComplianceAdmin" "Property Reports Menu" 166 no
"ComplianceAdmin" "Property Codes Tables" 259 no
"ComplianceAdmin" "Inspectors Browser" 279 no
"ComplianceAdmin" "Building Systems Browser" 280 no
"ComplianceAdmin" "Building System Types Browser" 292 no
"ComplianceBase" "Properties Browser" 11 no
"ComplianceBase" "Property Functions Menu" 96 no
"ComplianceBase" "Inspectors Browser" 279 no
"ComplianceMaint" "Inspectors Browser" 279 no
"ComplianceMaint" "Building Systems Browser" 280 no
"ComplianceMaint" "Building System Maintenance" 281 no
"ContactAdmin" "Main Menu" 8 no
"ContactAdmin" "Administration Menu" 98 no
"ContactAdmin" "System Codes Tables" 238 no
"ContactMaint" "Properties Browser" 11 no
"ContactMaint" "Contacts Browser" 108 no
"ContactMaint" "Entity Contacts Browser" 277 no
"ContactMaint" "Entity Contact Maintenance" 293 no
"DirectPaymentsAuthority" "Accounts Codes Tables" 258 no
"DirectPaymentsAuthority" "Direct Credit Credential Browser" 323 no
"DirectPaymentsAuthority" "Direct Credit Credentials Maintenance" 324 no
"Everyone" "Companies Browser" 4 no
"Everyone" "Contracts Browser" 5 no
"Everyone" "Main Menu" 8 no
"Everyone" "Properties Browser" 11 no
"Everyone" "Persons Selection" 24 no
"Everyone" "Tenancy Leases Browser" 74 no
"Everyone" "Contacts Browser" 108 no
"Everyone" "Person Maintenance" 168 no
"Everyone" "Phone Detail Maintenance" 170 no
"Everyone" "General Exports" 181 no
"Everyone" "Projects Browser" 183 no
"Everyone" "Code Listings Menu" 194 no
"Everyone" "Reporting Menu" 250 no
"Everyone" "Clients Browser" 270 no
"Executive" "Management Reports" 193 no
"Executive" "Reporting Menu" 250 no
"ForecastAdmin" "Administration Menu" 98 no
"ForecastAdmin" "'Any' Type Browse (Add-Delete)" 177 no
"ForecastAdmin" "'Any' type Maintain (OK-Cancel)" 202 no
"ForecastAdmin" "Scenario Browser" 221 no
"ForecastAdmin" "Codes Tables Maintenance" 238 no
"ForecastAmTrust" "Main Menu" 8 no
"ForecastAmTrust" "Properties Browser" 11 no
"ForecastAmTrust" "Months Browser" 40 no
"ForecastAmTrust" "Financial Years Browser" 47 no
"ForecastAmTrust" "Cash Flow Browser" 223 no
"ForecastAmTrust" "Forecasting and Budgeting" 236 no
"ForecastAmTrust" "Property Codes Tables" 259 no
"ForecastAmTrust" "Property Forecast Parameters (Amtrust)" 299 no
"Forecasting" "Accounts Summary Browser" 3 no
"Forecasting" "Main Menu" 8 no
"Forecasting" "Rental Spaces Browser" 13 no
"Forecasting" "Scenario Browser" 221 no
"Forecasting" "Scenario Maintenance" 222 no
"Forecasting" "Cash Flow Browser" 223 no
"Forecasting" "Forecasting and Budgetting" 236 no
"Forecasting" "Scenario Assumptions" 237 no
"ForecastRentals" "Scenario Browser" 221 no
"ForecastRentals" "Scenario Rental Assumptions" 282 no
"PeriodReporting" "Management Reports" 193 no
"PeriodReporting" "Reporting Menu" 250 no
"Programmer" "External Files Browser" 263 no
"Projects" "Companies Browser" 4 no
"Projects" "Main Menu" 8 no
"Projects" "Properties Browser" 11 no
"Projects" "Projects Maintenance" 182 no
"Projects" "Projects Browser" 183 no
"Projects" "Orders Browser" 188 no
"Projects" "Orders Maintenance" 189 no
"Projects" "Project Budgets Browser" 192 no
"Projects" "Code Listings Menu" 194 no
"Projects" "Project Budgets Maintenance" 195 no
"Projects" "Project Variations" 239 no
"Projects" "Project Variation Flows" 240 no
"Projects" "Reporting Menu" 250 no
"Projects" "Project Reports" 251 no
"ProjectsAdmin" "Administration Menu" 98 no
"ProjectsAdmin" "'Any' Type Browse (Add-Delete)" 177 no
"ProjectsAdmin" "Projects Browser" 183 no
"ProjectsAdmin" "Orders Browser" 188 no
"ProjectsAdmin" "Project Budgets Browser" 192 no
"ProjectsAdmin" "'Any' type Maintain (OK-Cancel)" 202 no
"ProjectsAdmin" "Codes Tables Maintenance" 238 no
"ProjectsAdmin" "Project Variations" 239 no
"ProjectsAdmin" "Project Variation Flows" 240 no
"ProjectsAdmin" "Accounts Codes Tables" 258 no
"ProjectsAdmin" "Entity List Browser" 315 no
"PropertyAdmin" "Contracts Browser" 5 no
"PropertyAdmin" "Main Menu" 8 no
"PropertyAdmin" "Properties Browser" 11 no
"PropertyAdmin" "Property Titles Browser" 12 no
"PropertyAdmin" "Rental Spaces Browser" 13 no
"PropertyAdmin" "Property Maintenance" 57 no
"PropertyAdmin" "Rental Space Maintenance" 58 no
"PropertyAdmin" "Tenancy Leases Maintenance" 62 no
"PropertyAdmin" "Tenant Selection" 64 no
"PropertyAdmin" "Tenancy Leases Browser" 74 no
"PropertyAdmin" "System Administration Menu" 98 no
"PropertyAdmin" "Service Type Maintenance" 99 no
"PropertyAdmin" "Frequency Types Browser" 101 no
"PropertyAdmin" "Frequency Type Maintenance" 102 no
"PropertyAdmin" "Meter Number Selection" 147 no
"PropertyAdmin" "Rent Reviews browser" 162 no
"PropertyAdmin" "Sub Leases Browser" 164 no
"PropertyAdmin" "Street Frontages Browser" 165 no
"PropertyAdmin" "Guarantors Browser" 171 no
"PropertyAdmin" "Area Types Maintenance" 176 no
"PropertyAdmin" "'Any' Type Browse (Add-Delete)" 177 no
"PropertyAdmin" "Valuations Browser" 187 no
"PropertyAdmin" "Ground Leases Browser" 201 no
"PropertyAdmin" "'Any' type Maintain (OK-Cancel" 202 no
"PropertyAdmin" "Regions Browser" 208 no
"PropertyAdmin" "Rental Charges Browser" 248 no
"PropertyAdmin" "Rent Charging Schedule" 249 no
"PropertyAdmin" "Reporting Menu" 250 no
"PropertyAdmin" "System Reports Menu" 256 no
"PropertyAdmin" "Accounts Codes Tables" 258 no
"PropertyAdmin" "Property Codes Tables" 259 no
"PropertyAdmin" "Entity List Browser" 315 no
"PropertyBase" "Account Balances Browser" 1 no
"PropertyBase" "Accounts Summary Browser" 3 no
"PropertyBase" "Contracts Browser" 5 no
"PropertyBase" "Creditors Browser" 6 no
"PropertyBase" "Transactions of Account Browser" 7 no
"PropertyBase" "Main Menu" 8 no
"PropertyBase" "Properties Browser" 11 no
"PropertyBase" "Rental Spaces Browser" 13 no
"PropertyBase" "Tenants Browser" 14 no
"PropertyBase" "Property Maintenance" 57 no
"PropertyBase" "Tenancy Leases Browser" 74 no
"PropertyBase" "Property Functions Menu" 96 no
"PropertyBase" "Property Reports Menu" 166 no
"PropertyBase" "Guarantors Browser" 171 no
"PropertyBase" "Valuations Browser" 187 no
"PropertyBase" "Rental Charges Browser" 248 no
"PropertyBase" "Reporting Menu" 250 no
"PropertyBase" "External Files Browser" 263 no
"PropertyBase" "Property Outgoings Browser" 295 no
"PropertyBase" "Cheque Viewer (view only)" 296 no
"PropertyMaint" "Contracts Browser" 5 no
"PropertyMaint" "Properties Browser" 11 no
"PropertyMaint" "Rental Spaces Browser" 13 no
"PropertyMaint" "Contracts Maintenance" 55 no
"PropertyMaint" "Rent Reviews browser" 162 no
"PropertyManager" "Contracts Browser" 5 no
"PropertyManager" "Accounting Reports Menu" 121 no
"PropertyManager" "Sub Leases Browser" 164 no
"PropertyManager" "Property Reports Menu" 166 no
"PropertyManager" "Reporting Menu" 250 no
"Replicator" "Administration Menu" 98 no
"Replicator" "Replication Collision Log Browser" 241 no
"Replicator" "Replication Log Browser" 242 no
"Replicator" "Replication Out Rules" 243 no
"Replicator" "Replication In Rules" 244 no
"Replicator" "Replication Menu" 252 no
"SdnyAccounts" "Automatic Transactions" 288 no
"System Admin" "Main Menu" 8 no
"System Admin" "Approval Persons Browser" 17 no
"System Admin" "Approver Selection" 18 no
"System Admin" "Offices Maintenance" 48 no
"System Admin" "Administration Menu" 98 no
"System Admin" "Frequency Types Broswer" 101 no
"System Admin" "Frequency Type Maintenance" 102 no
"System Admin" "Office Settings" 136 no
"System Admin" "Menu Administration" 138 no
"System Admin" "User Groups Browser" 139 no
"System Admin" "'Any' Type Browse (Add-Delete)" 177 no
"System Admin" "Contact Types Browser / Mainte" 178 no
"System Admin" "'Any' type Maintain (OK-Cancel)" 202 no
"System Admin" "Users Browser" 210 no
"System Admin" "User Maintenance" 211 no
"System Admin" "User Group Rights Browser" 214 no
"System Admin" "Codes Tables Maintenance" 238 no
"System Admin" "Reporting Menu" 250 no
"System Admin" "System Reports Menu" 256 no
"System Admin" "Offices Browser" 267 no
"System Admin" "Menu Browser" 289 no
"TenantCallsAdmin" "Main Menu" 8 no
"TenantCallsAdmin" "Administration Menu" 98 no
"TenantCallsAdmin" "System Codes Tables" 238 no
"TenantCallsAdmin" "Reporting Menu" 250 no
"TenantCallsAdmin" "Tenant Calls Browser" 313 no
"TenantCallsAdmin" "Tenant Call Reports" 314 no
"TenantCallsBase" "Main Menu" 8 no
"TenantCallsBase" "Property Reports Menu" 166 no
"TenantCallsBase" "Reporting Menu" 250 no
"TenantCallsBase" "Tenant Calls Browser" 313 no
"TenantCallsBase" "Tenant Call Reports" 314 no
"WizPropertyAGP" "Property Functions Menu" 96 no
"WizPropertyAmTrust" "Property Functions Menu" 96 no
"WizPropertyTTP" "Property Functions Menu" 96 no
"WorkFlowAdmin" "Administration Menu" 98 no
"WorkFlowAdmin" "Tasks Due Browser" 268 no
"WorkFlowAdmin" "Workflow Codes Tables" 273 no
"WorkFlowAdmin" "Workflow Rules Browser" 274 no
"WorkFlowAdmin" "Workflow Task Types Browser" 275 no
"WorkFlowAdmin" "Workflow Steps of Task Browser" 276 no
"WorkflowBase" "Main Menu" 8 no
"WorkflowBase" "Contacts Browser" 108 no
"WorkflowBase" "Tasks Due Browser" 268 no
"WorkflowBase" "Steps of Task Browser" 307 no
.
PSC
filename=UsrGroupMenu
records=0000000000336
ldbname=ttpl
timestamp=2009/04/06-11:15:35
numformat=44,46
dateformat=dmy-1990
cpstream=UNDEFINED
.
0000015728
