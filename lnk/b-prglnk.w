&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
  File:  
  Description: 
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR filter-src AS INT NO-UNDO.
DEF VAR filter-trg AS INT NO-UNDO.
DEF VAR filter-by  AS CHAR NO-UNDO.

DEF VAR src-type AS CHAR NO-UNDO.
DEF VAR filter-value AS CHAR NO-UNDO.

DEF VAR last-src AS INT NO-UNDO.
DEF VAR last-trg AS INT NO-UNDO.
DEF VAR last-by  AS CHAR NO-UNDO.
DEF VAR last-value AS CHAR NO-UNDO.

DEF VAR re-open-the-query AS LOGI NO-UNDO INITIAL No.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ProgramLink

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table ProgramLink.LinkType ProgramLink.ButtonLabel ProgramLink.Description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table ProgramLink.LinkType   
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}LinkType ~{&FP2}LinkType ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table ProgramLink
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table ProgramLink
&Scoped-define SELF-NAME br_table
&Scoped-define OPEN-QUERY-br_table CASE filter-by:   WHEN 'from Src' THEN     OPEN QUERY br_table FOR EACH ProgramLink NO-LOCK         WHERE ProgramLink.Source = filter-src           AND ProgramLink.ButtonLabel BEGINS filter-value            BY ProgramLink.ButtonLabel .   WHEN 'to Tgt' THEN     OPEN QUERY br_table FOR EACH ProgramLink NO-LOCK         WHERE ProgramLink.Target = filter-trg         BY ProgramLink.ButtonLabel .   WHEN 'Both' THEN     OPEN QUERY br_table FOR EACH ProgramLink NO-LOCK         WHERE ProgramLink.Source = filter-src AND ProgramLink.Target = filter-trg         BY ProgramLink.ButtonLabel  . END CASE.
&Scoped-define TABLES-IN-QUERY-br_table ProgramLink
&Scoped-define FIRST-TABLE-IN-QUERY-br_table ProgramLink


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-br_table 
       MENU-ITEM m_Delete_Link  LABEL "&Delete Link"  
       MENU-ITEM m_Reset_target LABEL "Reset &Target" 
       MENU-ITEM m_Reset_Source LABEL "Reset &Source" 
       MENU-ITEM m_Make_Copy    LABEL "&Make_Copy"    .


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      ProgramLink SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _FREEFORM
  QUERY br_table NO-LOCK DISPLAY
      ProgramLink.LinkType COLUMN-LABEL "Type" FORMAT "X(5)"
      ProgramLink.ButtonLabel COLUMN-LABEL "Label" FORMAT "X(21)"
      ProgramLink.Description FORMAT "X(39)"
   ENABLE
      ProgramLink.LinkType
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-LABELS NO-ASSIGN NO-ROW-MARKERS NO-COLUMN-SCROLLING SIZE 51.43 BY 12
         BGCOLOR 16 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 14.55
         WIDTH              = 64.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:POPUP-MENU IN FRAME F-Main         = MENU POPUP-MENU-br_table:HANDLE
       br_table:MAX-DATA-GUESS IN FRAME F-Main     = 200.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _START_FREEFORM
CASE filter-by:
  WHEN 'from Src' THEN
    OPEN QUERY br_table FOR EACH ProgramLink NO-LOCK
        WHERE ProgramLink.Source = filter-src
          AND ProgramLink.ButtonLabel BEGINS filter-value
           BY ProgramLink.ButtonLabel .
  WHEN 'to Tgt' THEN
    OPEN QUERY br_table FOR EACH ProgramLink NO-LOCK
        WHERE ProgramLink.Target = filter-trg
        BY ProgramLink.ButtonLabel .
  WHEN 'Both' THEN
    OPEN QUERY br_table FOR EACH ProgramLink NO-LOCK
        WHERE ProgramLink.Source = filter-src AND ProgramLink.Target = filter-trg
        BY ProgramLink.ButtonLabel  .
END CASE.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-DISPLAY OF br_table IN FRAME F-Main
DO:
  DEF VAR fgcolor AS INT NO-UNDO.
  ASSIGN
    fgcolor = (IF ProgramLink.Target = filter-trg THEN 12 ELSE ?)
    ProgramLink.LinkType:FGCOLOR IN BROWSE {&BROWSE-NAME} = fgcolor
    ProgramLink.Description:FGCOLOR = fgcolor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_Link
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_Link B-table-Win
ON CHOOSE OF MENU-ITEM m_Delete_Link /* Delete Link */
DO:
DEF VAR delete-it  AS LOGI NO-UNDO INITIAL No.
  MESSAGE "Are you sure you want to delete this link ?"
    VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE delete-it.

  IF delete-it THEN RUN dispatch( 'delete-record':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Make_Copy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Make_Copy B-table-Win
ON CHOOSE OF MENU-ITEM m_Make_Copy /* Make_Copy */
DO:
  RUN copy-link.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reset_Source
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reset_Source B-table-Win
ON CHOOSE OF MENU-ITEM m_Reset_Source /* Reset Source */
DO:
  RUN reset-source.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reset_target
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reset_target B-table-Win
ON CHOOSE OF MENU-ITEM m_Reset_target /* Reset Target */
DO:
  RUN reset-target.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list( 'FilterBy-Case = from Src,
             FilterBy-Options = from Src|to Tgt|Both|Unlinked':U ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-link B-table-Win 
PROCEDURE add-link :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR src AS INT NO-UNDO.
DEF VAR trg AS INT NO-UNDO.

  RUN get-attribute( 'LinkSource':U ). src = INT( RETURN-VALUE ).
  RUN get-attribute( 'LinkTarget':U ). trg = INT( RETURN-VALUE ).

  IF CAN-FIND( FIRST LinkNode WHERE LinkNode.NodeCode = src ) AND
     CAN-FIND( FIRST LinkNode WHERE LinkNode.NodeCode = trg ) THEN
  DO WITH FRAME {&FRAME-NAME}:
    RUN dispatch( 'add-record':U ).
    ProgramLink.Description:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = "".
    RUN dispatch( 'update-record':U ).
    RUN notify( 'row-available':U ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copy-link B-table-Win 
PROCEDURE copy-link :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE ProgramLink THEN RETURN.
  
  DEF BUFFER CopyLink FOR ProgramLink.
  
  CREATE copyLink.
  BUFFER-COPY ProgramLink EXCEPT LinkCode TO CopyLink
  ASSIGN CopyLink.Description = "Copy of " + ProgramLink.Description.

  re-open-the-query = Yes.
  RUN dispatch( 'open-query':U ).
  REPOSITION {&BROWSE-NAME} TO ROWID ROWID( CopyLink ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-apply-entry B-table-Win 
PROCEDURE local-apply-entry :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF NOT( src-type BEGINS "MN" ) THEN RETURN.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'apply-entry':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement B-table-Win 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

DEF VAR src AS INT NO-UNDO.
DEF VAR trg AS INT NO-UNDO.
DEF VAR link-type AS CHAR INITIAL "DRL" NO-UNDO.
DEF BUFFER ln-t FOR LinkNode.

  RUN get-attribute ( 'LinkSource':U ). src = INT( RETURN-VALUE ).
  RUN get-attribute ( 'LinkTarget':U ). trg = INT( RETURN-VALUE ).

  FIND ln-t WHERE ln-t.NodeCode = trg NO-LOCK NO-ERROR.

  IF src = trg THEN
    link-type = "MSG".
  ELSE IF AVAILABLE(ln-t) AND ln-t.NodeType = "SW" THEN
    link-type = "SEL".
  ELSE IF AVAILABLE(ln-t) AND ln-t.NodeType = "MW" THEN
    link-type = "MNT".
  ELSE IF AVAILABLE(ln-t) AND ln-t.NodeType = "MN" THEN
    link-type = "MNU".
  ELSE
    link-type = "DRL".

  ASSIGN
    ProgramLink.Source = src
    ProgramLink.Target = trg
    ProgramLink.LinkType = link-type
  .
    
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  IF link-type = "MNU" THEN ASSIGN
    ProgramLink.CreateViewer = No
    ProgramLink.Viewer = "Default" .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize B-table-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  ProgramLink.LinkType:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query B-table-Win 
PROCEDURE local-open-query :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF re-open-the-query THEN
    . /* bypass the "don't" logic below! */
  ELSE IF filter-by = "Unlinked" THEN RETURN.
  ELSE IF filter-by = last-by THEN DO:
    IF filter-by = "from Src" THEN DO:
      IF last-src = filter-src AND last-by = filter-by AND last-value = filter-value  THEN RETURN.
    END.
    ELSE IF filter-by = "to Tgt" THEN DO:
      IF last-trg = filter-trg THEN RETURN.
    END.
    ELSE IF last-src = filter-src AND last-by = filter-by 
                AND last-trg = filter-trg AND last-value = filter-value  THEN RETURN.
  END.
  last-src = filter-src.       last-trg = filter-trg.
  last-by = filter-by.         last-value = filter-value.
  re-open-the-query = No.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  {&BROWSE-NAME}:MAX-DATA-GUESS IN FRAME {&FRAME-NAME} = 200.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-source B-table-Win 
PROCEDURE reset-source :
/*------------------------------------------------------------------------------
  Purpose:  Reset the current linknode to point to the current source node.
------------------------------------------------------------------------------*/
  DO TRANSACTION:
    FIND CURRENT ProgramLink EXCLUSIVE-LOCK.
    ProgramLink.Source = filter-src.
    FIND CURRENT ProgramLink NO-LOCK.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-target B-table-Win 
PROCEDURE reset-target :
/*------------------------------------------------------------------------------
  Purpose:  Reset the current linknode to point to the current target.
------------------------------------------------------------------------------*/
  DO TRANSACTION:
    FIND CURRENT ProgramLink EXCLUSIVE-LOCK.
    ProgramLink.Target = filter-trg.
    FIND CURRENT ProgramLink NO-LOCK.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ProgramLink"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Filter-Value B-table-Win 
PROCEDURE use-Filter-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-value AS CHAR NO-UNDO.

  IF src-type BEGINS "MN" THEN
    filter-value = new-value.
  ELSE
    filter-value = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-by AS CHAR NO-UNDO.
  filter-by = new-by.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-LinkSource B-table-Win 
PROCEDURE use-LinkSource :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-src AS CHAR NO-UNDO.
  filter-src = INT(new-src).

DEF BUFFER MyNode FOR LinkNode.
  FIND MyNode WHERE MyNode.NodeCode = filter-src NO-LOCK NO-ERROR.
  src-type = MyNode.NodeType.
  IF NOT(src-type BEGINS "MN") THEN
    filter-value = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-LinkTarget B-table-Win 
PROCEDURE use-LinkTarget :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-trg AS CHAR NO-UNDO.
  filter-trg = INT(new-trg).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


