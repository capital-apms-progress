&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF WORK-TABLE Attribute NO-UNDO
  FIELD Attribute AS CHAR
  FIELD AttribVal AS CHAR.

DEF VAR common-attributes AS CHAR NO-UNDO.
common-attributes =
  "Message,Mode,FilterPanel,SortPanel,OptionPanel,AlphabetPanel,AlphabetPanel2,SearchPanel,Key-Name,Fill-In,Field,NotesViewer,OtherViewer".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_attrib

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES ProgramLink
&Scoped-define FIRST-EXTERNAL-TABLE ProgramLink


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR ProgramLink.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Attribute

/* Definitions for BROWSE br_attrib                                     */
&Scoped-define FIELDS-IN-QUERY-br_attrib Attribute.Attribute Attribute.AttribVal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_attrib Attribute.Attribute  Attribute.AttribVal   
&Scoped-define FIELD-PAIRS-IN-QUERY-br_attrib~
 ~{&FP1}Attribute ~{&FP2}Attribute ~{&FP3}~
 ~{&FP1}AttribVal ~{&FP2}AttribVal ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_attrib Attribute
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_attrib Attribute
&Scoped-define SELF-NAME br_attrib
&Scoped-define OPEN-QUERY-br_attrib OPEN QUERY {&SELF-NAME} FOR EACH Attribute.
&Scoped-define TABLES-IN-QUERY-br_attrib Attribute
&Scoped-define FIRST-TABLE-IN-QUERY-br_attrib Attribute


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br_attrib}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS ProgramLink.LinkType ProgramLink.Viewer ~
ProgramLink.CreateViewer ProgramLink.Description ProgramLink.ButtonLabel 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Viewer ~{&FP2}Viewer ~{&FP3}~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}~
 ~{&FP1}ButtonLabel ~{&FP2}ButtonLabel ~{&FP3}
&Scoped-define ENABLED-TABLES ProgramLink
&Scoped-define FIRST-ENABLED-TABLE ProgramLink
&Scoped-Define ENABLED-OBJECTS RECT-20 cmb_common btn_save br_attrib 
&Scoped-Define DISPLAYED-FIELDS ProgramLink.LinkType ProgramLink.Viewer ~
ProgramLink.CreateViewer ProgramLink.Description ProgramLink.ButtonLabel 
&Scoped-Define DISPLAYED-OBJECTS cmb_common 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-br_attrib 
       MENU-ITEM m_Add_Attribute LABEL "&Add Attribute"
       MENU-ITEM m_Delete_Attribute LABEL "&Delete Attribute".


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_save 
     LABEL "Save" 
     SIZE 9.72 BY 1
     FONT 9.

DEFINE VARIABLE cmb_common AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "","" 
     SIZE 19 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-20
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 51.43 BY 12.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_attrib FOR 
      Attribute SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_attrib
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_attrib V-table-Win _FREEFORM
  QUERY br_attrib DISPLAY
      Attribute.Attribute WIDTH 16 FORMAT "X(1024)"
  Attribute.AttribVal WIDTH 30 FORMAT "X(2048)" COLUMN-LABEL "Value"
ENABLE
  Attribute.Attribute
  Attribute.AttribVal
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 50 BY 6.6
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     ProgramLink.LinkType AT ROW 1.2 COL 5.29 COLON-ALIGNED NO-LABEL
          VIEW-AS COMBO-BOX INNER-LINES 7
          LIST-ITEMS "DRL","MNT","MNU","SEL","MSG" 
          SIZE 7.43 BY 1
          FONT 10
     ProgramLink.Viewer AT ROW 1.2 COL 16.72 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 22.29 BY 1
          FONT 10
     ProgramLink.CreateViewer AT ROW 1.2 COL 41.57
          LABEL "Dynamic?"
          VIEW-AS TOGGLE-BOX
          SIZE 9.72 BY 1
          FONT 10
     ProgramLink.Description AT ROW 2.5 COL 7 NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 44.57 BY 1
          FONT 10
     ProgramLink.ButtonLabel AT ROW 3.5 COL 5 COLON-ALIGNED NO-LABEL FORMAT "X(50)"
          VIEW-AS FILL-IN 
          SIZE 44.57 BY 1
          FONT 10
     cmb_common AT ROW 4.65 COL 1.57 NO-LABEL
     btn_save AT ROW 4.8 COL 42
     br_attrib AT ROW 6.15 COL 1.57
     RECT-20 AT ROW 1 COL 1
     "Button:" VIEW-AS TEXT
          SIZE 5.14 BY 1 AT ROW 3.5 COL 5.71 RIGHT-ALIGNED
          FONT 10
     "Name:" VIEW-AS TEXT
          SIZE 5.14 BY 1 AT ROW 2.5 COL 5.71 RIGHT-ALIGNED
          FONT 10
     "Vwr:" VIEW-AS TEXT
          SIZE 3.43 BY 1 AT ROW 1.2 COL 15.29
          FONT 10
     "Link:" VIEW-AS TEXT
          SIZE 3.43 BY 1 AT ROW 1.2 COL 1.57
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         DEFAULT-BUTTON btn_save.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.ProgramLink
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 12
         WIDTH              = 51.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_attrib btn_save F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_attrib:POPUP-MENU IN FRAME F-Main         = MENU POPUP-MENU-br_attrib:HANDLE.

/* SETTINGS FOR FILL-IN ProgramLink.ButtonLabel IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR COMBO-BOX cmb_common IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR TOGGLE-BOX ProgramLink.CreateViewer IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN ProgramLink.Description IN FRAME F-Main
   ALIGN-L EXP-LABEL EXP-FORMAT                                         */
/* SETTINGS FOR COMBO-BOX ProgramLink.LinkType IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN ProgramLink.Viewer IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TEXT-LITERAL "Name:"
          SIZE 5.14 BY 1 AT ROW 2.5 COL 5.71 RIGHT-ALIGNED              */

/* SETTINGS FOR TEXT-LITERAL "Button:"
          SIZE 5.14 BY 1 AT ROW 3.5 COL 5.71 RIGHT-ALIGNED              */

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_attrib
/* Query rebuild information for BROWSE br_attrib
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH Attribute.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br_attrib */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_attrib
&Scoped-define SELF-NAME br_attrib
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_attrib V-table-Win
ON CTRL-DEL OF br_attrib IN FRAME F-Main
OR "SHIFT-DEL" OF {&SELF-NAME} ANYWHERE DO:
  RUN delete-attribute.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_attrib V-table-Win
ON CTRL-INS OF br_attrib IN FRAME F-Main
OR "SHIFT-INS" OF {&SELF-NAME} ANYWHERE DO:
  RUN add-attribute.
  RETURN NO-APPLY.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_attrib V-table-Win
ON VALUE-CHANGED OF br_attrib IN FRAME F-Main
DO:
  RUN update-common-attribs.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_save
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_save V-table-Win
ON CHOOSE OF btn_save IN FRAME F-Main /* Save */
DO:
  RUN dispatch( 'update-record':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_common
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_common V-table-Win
ON VALUE-CHANGED OF cmb_common IN FRAME F-Main
DO:
  RUN common-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ProgramLink.LinkType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ProgramLink.LinkType V-table-Win
ON VALUE-CHANGED OF ProgramLink.LinkType IN FRAME F-Main /* Link Type */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Add_Attribute
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Add_Attribute V-table-Win
ON CHOOSE OF MENU-ITEM m_Add_Attribute /* Add Attribute */
DO:
  RUN add-attribute.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_Attribute
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_Attribute V-table-Win
ON CHOOSE OF MENU-ITEM m_Delete_Attribute /* Delete Attribute */
DO:
  RUN delete-attribute.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-attribute V-table-Win 
PROCEDURE add-attribute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR reposition-row   AS INT NO-UNDO.
  DEF VAR reposition-rowid AS ROWID NO-UNDO.
  
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN
    RUN open-attribute-query.
    
  IF AVAILABLE Attribute THEN RUN assign-attribute.
  
  CREATE Attribute.
  reposition-rowid = ROWID( Attribute ).
  
  reposition-row = {&BROWSE-NAME}:GET-REPOSITIONED-ROW().
  reposition-row =  INT( {&BROWSE-NAME}:HEIGHT-CHARS ) - 1.
  OPEN QUERY {&BROWSE-NAME} FOR EACH Attribute.
  IF {&BROWSE-NAME}:SET-REPOSITIONED-ROW( reposition-row, "CONDITIONAL" ) THEN.
  REPOSITION {&BROWSE-NAME} TO ROWID reposition-rowid.

  RUN display-attribute.
/*
  APPLY 'ENTRY':U TO Attribute.Attribute IN BROWSE {&BROWSE-NAME}.
  APPLY 'ENTRY':U TO cmb_common.
*/
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "ProgramLink"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "ProgramLink"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-attribute V-table-Win 
PROCEDURE assign-attribute :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE Attribute THEN 
    ASSIGN BROWSE {&BROWSE-NAME} Attribute.Attribute Attribute.AttribVal.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-attributes V-table-Win 
PROCEDURE assign-attributes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR options-list AS CHAR NO-UNDO.

  RUN assign-attribute.
  
  FOR EACH Attribute:
    IF Attribute.Attribute <> "" AND Attribute.Attribute <> "?" THEN DO:
      IF Attribute.Attribute = "Fill-In" THEN
            ProgramLink.FillName = Attribute.AttribVal.
      IF Attribute.Attribute = "Field" THEN
            ProgramLink.CodeName = Attribute.AttribVal.

      options-list = options-list + (IF options-list = "" THEN "" ELSE ",").

      options-list = options-list + Attribute.Attribute + " = "
                   + (IF Attribute.AttribVal = ? THEN "" ELSE Attribute.AttribVal).

    END.
  END.

  ASSIGN ProgramLink.Function = options-list.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-attribute-query V-table-Win 
PROCEDURE close-attribute-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY {&BROWSE-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE common-changed V-table-Win 
PROCEDURE common-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) <> 0 AND NUM-RESULTS( "{&BROWSE-NAME}" ) <> ? THEN
  DO:
/*    APPLY 'ENTRY':U TO Attribute.AttribVal IN BROWSE {&BROWSE-NAME}. */
    Attribute.Attribute:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = INPUT cmb_common.
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-attribute V-table-Win 
PROCEDURE delete-attribute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
    
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN RETURN.

  IF {&BROWSE-NAME}:SELECT-FOCUSED-ROW() AND
     {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN
  DO:
    GET CURRENT {&BROWSE-NAME}.
    DELETE Attribute.
    IF {&BROWSE-NAME}:DELETE-CURRENT-ROW() THEN.
  END.

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = ? OR 
     NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 THEN
  DO:
    RUN close-attribute-query.
    RETURN.
  END.

  IF {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN RUN display-attribute.
    
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-attribute V-table-Win 
PROCEDURE display-attribute :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE Attribute THEN
  DO:
    DISPLAY Attribute WITH BROWSE {&BROWSE-NAME}.
    RUN update-common-attribs.
  END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-attributes V-table-Win 
PROCEDURE display-attributes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER attribute-list AS CHAR NO-UNDO.
  
  RUN close-attribute-query.
  FOR EACH Attribute: DELETE Attribute. END.
  
  DEF VAR i       AS INT  NO-UNDO.
  DEF VAR delim   AS CHAR NO-UNDO.
  DEF VAR setting AS CHAR NO-UNDO.
    
  delim = IF INDEX( attribute-list, "~n" ) <> 0 THEN "~n" ELSE ",".
    
  DO i = 1 TO NUM-ENTRIES( attribute-list, delim ):
    setting = ENTRY( i, attribute-list, delim ).
    IF setting <> "" THEN
    DO:
      CREATE Attribute.
      IF ProgramLink.LinkType = "MSG" AND NUM-ENTRIES( setting, "=" ) = 1 THEN
        ASSIGN
          Attribute.Attribute = "Message"
          Attribute.AttribVal = TRIM( ENTRY( 1, setting, "=" ) ).
      ELSE
        ASSIGN
          Attribute.Attribute = TRIM( ENTRY( 1, setting, "=" ) )
          Attribute.AttribVal = TRIM( ENTRY( 2, setting, "=" ) ).
    END.
  END.

  RUN open-attribute-query.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-apply-entry V-table-Win 
PROCEDURE local-apply-entry :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  RETURN.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'apply-entry':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement V-table-Win 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN assign-attributes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  DO WITH FRAME {&FRAME-NAME}:
    btn_save:SENSITIVE = No.
    {&BROWSE-NAME}:SENSITIVE = No.
    cmb_Common:SENSITIVE = No.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN display-attributes(
    IF NOT AVAILABLE ProgramLink THEN "" ELSE ProgramLink.Function + 
    ( IF ProgramLink.FillName <> "" AND INDEX( ProgramLink.Function, "Fill-In =") = 0 THEN
         ",Fill-In = " + ProgramLink.FillName ELSE "" )  +
    ( IF ProgramLink.CodeName <> "" AND INDEX( ProgramLink.Function, "Field =") = 0 THEN
         ",Field = " + ProgramLink.CodeName ELSE "" )
  ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR is-simple-button AS LOGICAL NO-UNDO.
DEF VAR is-sel-button AS LOGICAL NO-UNDO.

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  DO WITH FRAME {&FRAME-NAME}:
    btn_save:SENSITIVE = Yes.

    is-simple-button = (INPUT ProgramLink.LinkType = 'MSG')
                    OR (INPUT ProgramLink.LinkType = 'MNU').
    ASSIGN
      ProgramLink.CreateViewer:SENSITIVE = NOT is-simple-button
      ProgramLink.Viewer:SENSITIVE       = NOT is-simple-button
    .

    is-sel-button = (INPUT ProgramLink.LinkType = 'SEL').
    ASSIGN
      ProgramLink.ButtonLabel:SENSITIVE  = NOT is-sel-button .

    IF is-simple-button THEN ASSIGN
      ProgramLink.CreateViewer:SCREEN-VALUE = "Y"
      ProgramLink.Viewer:SCREEN-VALUE       = "" .
    
    cmb_common:SENSITIVE = Yes.
    {&BROWSE-NAME}:SENSITIVE = Yes.

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available V-table-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  IF AVAILABLE ProgramLink THEN
    RUN dispatch( 'enable-fields':U ).
  ELSE
    RUN dispatch( 'disable-fields':U ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-attribute-query V-table-Win 
PROCEDURE open-attribute-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  OPEN QUERY {&BROWSE-NAME} FOR EACH Attribute.
  RUN display-attribute.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE really-apply-entry V-table-Win 
PROCEDURE really-apply-entry :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  APPLY 'ENTRY':U TO ProgramLink.Viewer.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ProgramLink"}
  {src/adm/template/snd-list.i "Attribute"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-common-attribs V-table-Win 
PROCEDURE update-common-attribs :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF VAR sv AS CHAR NO-UNDO.
  
  sv = INPUT BROWSE {&BROWSE-NAME} Attribute.Attribute.
  cmb_common:LIST-ITEMS = 
    ( IF LOOKUP( sv, common-attributes ) = 0 AND sv <> "" THEN ( sv + "," ) ELSE "" ) +
     common-attributes.
  cmb_common:SCREEN-VALUE = sv.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


