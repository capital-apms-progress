&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS W-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  History: 
          
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow

&Scoped-define ADM-CONTAINER WINDOW

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btn_addlink 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR W-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_B-DSTNDE AS HANDLE NO-UNDO.
DEFINE VARIABLE h_B-PRGLNK AS HANDLE NO-UNDO.
DEFINE VARIABLE h_B-SRCNDE AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-abc AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-abc-2 AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-option AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-option-2 AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-option-3 AS HANDLE NO-UNDO.
DEFINE VARIABLE h_V-PRGLNK AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_addlink 
     LABEL "&Add" 
     SIZE 11.43 BY 1
     FONT 14.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     btn_addlink AT ROW 14.2 COL 33.57
     SPACE(52.00) SKIP(11.00)
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW W-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Program Links"
         HEIGHT             = 25.25
         WIDTH              = 96.14
         MAX-HEIGHT         = 41.1
         MAX-WIDTH          = 145.86
         VIRTUAL-HEIGHT     = 41.1
         VIRTUAL-WIDTH      = 145.86
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW W-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   Size-to-Fit                                                          */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
THEN W-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB W-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{inc/method/m-drlwin.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON END-ERROR OF W-Win /* Program Links */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON WINDOW-CLOSE OF W-Win /* Program Links */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_addlink
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_addlink W-Win
ON CHOOSE OF btn_addlink IN FRAME F-Main /* Add */
DO:
  RUN add-link IN h_B-PRGLNK. 
  RUN dispatch IN h_V-PRGLNK ( 'really-apply-entry':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK W-Win 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects W-Win _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'adm/objects/p-option.r':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Style = Horizontal Radio-Set,
                     Drawn-in-UIB = yes,
                     Case-Attribute = FilterBy-Case,
                     Case-Changed-Event = Open-Query,
                     Dispatch-Open-Query = yes,
                     Label = ':U + '' + ',
                     Link-Name = FilterBy-Target,
                     Margin-Pixels = 3,
                     Options-Attribute = FilterBy-Options,
                     Edge-Pixels = 0,
                     Font = 10':U ,
             OUTPUT h_p-option-2 ).
       RUN set-position IN h_p-option-2 ( 1.00 , 1.00 ) NO-ERROR.
       RUN set-size IN h_p-option-2 ( 1.20 , 43.43 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'adm/objects/p-option.r':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Style = Horizontal Radio-Set,
                     Drawn-in-UIB = yes,
                     Case-Attribute = FilterBy-Case,
                     Case-Changed-Event = Open-Query,
                     Dispatch-Open-Query = yes,
                     Label = ':U + '' + ',
                     Link-Name = FilterBy-Target,
                     Margin-Pixels = 4,
                     Options-Attribute = FilterBy-Options,
                     Edge-Pixels = 0,
                     Font = 10':U ,
             OUTPUT h_p-option ).
       RUN set-position IN h_p-option ( 1.00 , 52.43 ) NO-ERROR.
       RUN set-size IN h_p-option ( 1.20 , 42.86 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'panel/p-abc.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Button-Font = 10,
                     Dispatch-Open-Query = yes,
                     Edge-Pixels = 0,
                     Margin-Pixels = 0,
                     Drawn-in-UIB = yes':U ,
             OUTPUT h_p-abc ).
       RUN set-position IN h_p-abc ( 2.00 , 1.00 ) NO-ERROR.
       RUN set-size IN h_p-abc ( 1.10 , 44.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'LNK/B-PRGLNK.W':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Layout = ':U ,
             OUTPUT h_B-PRGLNK ).
       RUN set-position IN h_B-PRGLNK ( 2.20 , 45.57 ) NO-ERROR.
       /* Size in UIB:  ( 12.00 , 51.43 ) */

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'LNK/B-SRCNDE.W':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Layout = ':U ,
             OUTPUT h_B-SRCNDE ).
       RUN set-position IN h_B-SRCNDE ( 3.00 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 11.20 , 44.00 ) */

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'adm/objects/p-option.r':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Style = Horizontal Radio-Set,
                     Drawn-in-UIB = yes,
                     Case-Attribute = FilterBy-Case,
                     Case-Changed-Event = Open-Query,
                     Dispatch-Open-Query = yes,
                     Label = ':U + '' + ',
                     Link-Name = FilterBy-Target,
                     Margin-Pixels = 2,
                     Options-Attribute = FilterBy-Options,
                     Edge-Pixels = 0,
                     Font = 10':U ,
             OUTPUT h_p-option-3 ).
       RUN set-position IN h_p-option-3 ( 14.20 , 1.00 ) NO-ERROR.
       RUN set-size IN h_p-option-3 ( 1.20 , 32.57 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'LNK/V-PRGLNK.W':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Initial-Lock = NO-LOCK,
                     Hide-on-Init = no,
                     Disable-on-Init = no,
                     Layout = ,
                     Create-On-Add = ?':U ,
             OUTPUT h_V-PRGLNK ).
       RUN set-position IN h_V-PRGLNK ( 14.20 , 45.57 ) NO-ERROR.
       /* Size in UIB:  ( 12.00 , 51.43 ) */

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'panel/p-abc.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Button-Font = 10,
                     Dispatch-Open-Query = yes,
                     Edge-Pixels = 0,
                     Margin-Pixels = 0,
                     Drawn-in-UIB = yes':U ,
             OUTPUT h_p-abc-2 ).
       RUN set-position IN h_p-abc-2 ( 15.40 , 1.00 ) NO-ERROR.
       RUN set-size IN h_p-abc-2 ( 1.10 , 44.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'LNK/B-TRGNDE.W':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Layout = ':U ,
             OUTPUT h_B-DSTNDE ).
       RUN set-position IN h_B-DSTNDE ( 16.40 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 9.80 , 44.00 ) */

       /* Links to SmartBrowser h_B-PRGLNK. */
       RUN add-link IN adm-broker-hdl ( h_B-DSTNDE , 'PrgLnkTrg':U , h_B-PRGLNK ).
       RUN add-link IN adm-broker-hdl ( h_B-SRCNDE , 'PrgLnkSrc':U , h_B-PRGLNK ).
       RUN add-link IN adm-broker-hdl ( h_p-abc , 'Filter':U , h_B-PRGLNK ).
       RUN add-link IN adm-broker-hdl ( h_p-option , 'FilterBy':U , h_B-PRGLNK ).

       /* Links to SmartBrowser h_B-SRCNDE. */
       RUN add-link IN adm-broker-hdl ( h_p-abc , 'Filter':U , h_B-SRCNDE ).
       RUN add-link IN adm-broker-hdl ( h_p-option-2 , 'FilterBy':U , h_B-SRCNDE ).

       /* Links to SmartViewer h_V-PRGLNK. */
       RUN add-link IN adm-broker-hdl ( h_B-PRGLNK , 'Record':U , h_V-PRGLNK ).

       /* Links to SmartBrowser h_B-DSTNDE. */
       RUN add-link IN adm-broker-hdl ( h_B-SRCNDE , 'Partner':U , h_B-DSTNDE ).
       RUN add-link IN adm-broker-hdl ( h_p-abc-2 , 'Filter':U , h_B-DSTNDE ).
       RUN add-link IN adm-broker-hdl ( h_p-option-3 , 'FilterBy':U , h_B-DSTNDE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_p-option-2 ,
             btn_addlink:HANDLE IN FRAME F-Main , 'BEFORE':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_p-option ,
             h_p-option-2 , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_p-abc ,
             h_p-option , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_B-PRGLNK ,
             h_p-abc , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_B-SRCNDE ,
             h_B-PRGLNK , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_p-option-3 ,
             h_B-SRCNDE , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_V-PRGLNK ,
             btn_addlink:HANDLE IN FRAME F-Main , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_p-abc-2 ,
             h_V-PRGLNK , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_B-DSTNDE ,
             h_p-abc-2 , 'AFTER':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available W-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI W-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
  THEN DELETE WIDGET W-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI W-Win _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE btn_addlink 
      WITH FRAME F-Main IN WINDOW W-Win.
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
  VIEW W-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit W-Win 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
   APPLY "CLOSE":U TO THIS-PROCEDURE.
   
   RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records W-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartWindow, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed W-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE user-layout W-Win 
PROCEDURE user-layout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


