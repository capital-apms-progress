&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR filter-type AS CHAR INIT "All" NO-UNDO.
DEF VAR filter-case AS CHAR INIT "" NO-UNDO.
DEF VAR node-mnt    AS HANDLE       NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES LinkNode

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table LinkNode.Description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table LinkNode.Description   
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table LinkNode
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table LinkNode
&Scoped-define SELF-NAME br_table
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH LinkNode NO-LOCK         WHERE LinkNode.NodeType BEGINS filter-type           AND LinkNode.Description BEGINS filter-case         BY LinkNode.NodeType BY LinkNode.Description.
&Scoped-define TABLES-IN-QUERY-br_table LinkNode
&Scoped-define FIRST-TABLE-IN-QUERY-br_table LinkNode


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-br_table TITLE "?"
       MENU-ITEM m_Create_Node  LABEL "&Create Node"  
       MENU-ITEM m_Modify_Node  LABEL "&Modify Node"  
       MENU-ITEM m_Delete_Node  LABEL "&Delete Node"  
       MENU-ITEM m_Make_Copy    LABEL "Make Co&py"    .


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      LinkNode SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _FREEFORM
  QUERY br_table NO-LOCK DISPLAY
      LinkNode.Description FORMAT "X(57)"
ENABLE
      LinkNode.Description
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-LABELS NO-ASSIGN NO-ROW-MARKERS SIZE 44 BY 11.2
         BGCOLOR 16 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 16.6
         WIDTH              = 57.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:POPUP-MENU IN FRAME F-Main         = MENU POPUP-MENU-br_table:HANDLE
       br_table:MAX-DATA-GUESS IN FRAME F-Main     = 300.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _START_FREEFORM
OPEN QUERY br_table FOR EACH LinkNode NO-LOCK
        WHERE LinkNode.NodeType BEGINS filter-type
          AND LinkNode.Description BEGINS filter-case
        BY LinkNode.NodeType BY LinkNode.Description.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
  RUN source-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Create_Node
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Create_Node B-table-Win
ON CHOOSE OF MENU-ITEM m_Create_Node /* Create Node */
DO:
  RUN add-node.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_Node
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_Node B-table-Win
ON CHOOSE OF MENU-ITEM m_Delete_Node /* Delete Node */
DO:
  MESSAGE "Are you sure you want to delete this node ?"
    VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE delete-it AS LOGI.
  IF delete-it THEN RUN dispatch( 'delete-record':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Make_Copy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Make_Copy B-table-Win
ON CHOOSE OF MENU-ITEM m_Make_Copy /* Make Copy */
DO:
  RUN copy-node.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Modify_Node
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Modify_Node B-table-Win
ON CHOOSE OF MENU-ITEM m_Modify_Node /* Modify Node */
DO:
  RUN modify-node.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list( 'FilterBy-Case = Drl, FilterBy-Options = Drl|Mnu|Sel|Mnt Vwr':U ).
RUN set-attribute-list( 'Filter-Value = ':U ).

LinkNode.Description:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-node B-table-Win 
PROCEDURE add-node :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN start-node-mnt.
  RUN dispatch IN node-mnt ( 'add-node':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copy-node B-table-Win 
PROCEDURE copy-node :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF BUFFER CopyNode FOR LinkNode.
  DEF BUFFER CopyLink FOR ProgramLink.
  DEF VAR file-suffix AS INT INIT 2 NO-UNDO. 
  
  IF NOT AVAILABLE LinkNode THEN RETURN.
  
  DO WHILE CAN-FIND( FIRST CopyNode WHERE
    CopyNode.File = LinkNode.File + STRING( file-suffix ) ):
    file-suffix = file-suffix + 1.
  END.
  
  CREATE CopyNode.
  BUFFER-COPY LinkNode EXCEPT NodeCode File TO CopyNode
  ASSIGN CopyNode.Description = "Copy of " + LinkNode.Description
         CopyNode.File        = LinkNode.File + STRING( file-suffix ).

  FOR EACH ProgramLink NO-LOCK
  WHERE ProgramLink.Source = LinkNode.NodeCode:
    CREATE copyLink.
    BUFFER-COPY ProgramLink EXCEPT LinkCode TO CopyLink
    ASSIGN CopyLink.Source = CopyNode.NodeCode.
  END.
  
  RUN dispatch( 'open-query':U ).
  REPOSITION {&BROWSE-NAME} TO ROWID ROWID( CopyNode ).
  RUN source-changed.
  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-apply-entry B-table-Win 
PROCEDURE local-apply-entry :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF filter-type = "MN" THEN RETURN.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'apply-entry':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize B-table-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  APPLY 'VALUE-CHANGED':U TO BROWSE {&BROWSE-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query B-table-Win 
PROCEDURE local-open-query :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN source-changed.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-changed B-table-Win 
PROCEDURE local-row-changed :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-changed':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN source-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modify-node B-table-Win 
PROCEDURE modify-node :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN start-node-mnt.
  RUN dispatch IN node-mnt ( 'modify-node':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "LinkNode"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE source-changed B-table-Win 
PROCEDURE source-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR c-sctarget AS CHAR   NO-UNDO.
DEF VAR h-sctarget AS HANDLE NO-UNDO.
DEF VAR c-ptarget  AS CHAR   NO-UNDO.
DEF VAR h-ptarget  AS HANDLE NO-UNDO.
  
  IF NOT VALID-HANDLE( adm-broker-hdl ) THEN RETURN.
  
  RUN get-link-handle IN adm-broker-hdl(
    THIS-PROCEDURE, 'PrgLnkSrc-Target':U, OUTPUT c-sctarget ).
    
  RUN get-link-handle IN adm-broker-hdl(
    THIS-PROCEDURE, 'Partner-Target':U, OUTPUT c-ptarget ).
  
  h-sctarget = WIDGET-HANDLE( ENTRY( 1, c-sctarget ) ).
  h-ptarget  = WIDGET-HANDLE( ENTRY( 1, c-ptarget ) ).
  
  IF VALID-HANDLE( h-sctarget ) AND AVAILABLE LinkNode THEN
  DO:
    RUN set-attribute-list IN h-sctarget ( 'LinkSource = ' + STRING( LinkNode.NodeCode ) ).
    RUN dispatch           IN h-sctarget ( 'open-query':U ).
  END.
    
  IF VALID-HANDLE( h-ptarget ) AND AVAILABLE LinkNode THEN
  DO:
    RUN set-attribute-list IN h-ptarget ( 'LinkSource = ' + STRING( LinkNode.NodeCode ) ).
    RUN dispatch           IN h-ptarget ( 'open-query':U ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE start-node-mnt B-table-Win 
PROCEDURE start-node-mnt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN lnk/w-lnknde.w PERSISTENT SET node-mnt.
  RUN add-link IN adm-broker-hdl(THIS-PROCEDURE, 'Record':U, node-mnt ).
  RUN dispatch IN node-mnt ( 'initialize':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Filter-Value B-table-Win 
PROCEDURE use-Filter-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-case AS CHAR NO-UNDO.

  IF filter-type <> "MN" THEN filter-case = new-case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-case AS CHAR NO-UNDO.

  CASE new-case:
    WHEN 'Drl':U      THEN filter-type = 'DW'.
    WHEN 'Sel':U      THEN filter-type = 'SW'.
    WHEN 'Mnu':U      THEN filter-type = 'MN'.
    WHEN 'Mnt Vwr':U  THEN filter-type = 'MV'.
    OTHERWISE              filter-type = ''.
  END CASE.

  IF filter-type = "MN" THEN
    filter-case = "Main".
  ELSE DO:
    RUN get-attribute( "Filter-Value":U ).
    filter-case = RETURN-VALUE.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


