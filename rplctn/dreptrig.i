/*
System:     Property Management and Finance System
Program:    dreptrig.i
Author:     Donald Christie
Date:       26/08/97
Description:    Generic include file to create replication entries in the replication log table on modify
                        Goes on the replication delete trigger.
Usage:  Pass in the table name and action.
        &rtable = <name of the table of this trigger>

Modification Notes
*/



{inc\ofc-this.i}

/* check that replication is valid at this site...*/

FIND FIRST ReplTrigger WHERE ReplTrigger.TableToRepl = "{&rtable}"
NO-LOCK NO-ERROR.

IF AVAILABLE ReplTrigger
AND INDEX (ReplTrigger.Activity, "D") <> 0
THEN DO:

    /* need this as the raw-transfer statement messes up if a 
       table variable name is the same as the table name...
       e.g. AreaType.AreaType! 
    */
    DEFINE TEMP-TABLE del{&rtable} NO-UNDO LIKE {&rtable}.    

    CREATE del{&rtable}.
    BUFFER-COPY {&rtable} TO del{&rtable}.
    
        CREATE ReplLog.

        ASSIGN ReplLog.ReplID = NEXT-VALUE (Replid)
            ReplLog.OfficeCode = Office.OfficeCode
            ReplLog.TableToRepl = '{&rtable}'
            ReplLog.TransactID = DBTASKID (LDBNAME (BUFFER {&rtable}))  
            ReplLog.ReplDate = TODAY
            ReplLog.ReplTime = TIME
            ReplLog.ReplEvent = "D"
            ReplLog.TableRowid = RECID ({&rtable})
            ReplLog.ReplAI = ?.
    
        RAW-TRANSFER del{&rtable} TO ReplLog.ReplBI. 
END. /* if available...*/
