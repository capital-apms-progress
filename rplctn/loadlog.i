/*
System:      Property Management and Finance System
Include:     loadlog.i
Author:      Tyrone McAuley
Date:        28/08/97

Description: Generic Include to detect record replication collisions

The procedure for collsion detection is as follows:

             On Create:
               Try to find the given table record with the given key id
               If it exists and the index is unique then there is a collision
             On Modify or Delete
               Try to find the given table with the given key id
               If it exists then
                 do a buffer compare between the two records
                 if the records are different then there is a collision
               else
                 integrity error - something strange has gone wrong
                 in replication between the two sites
               
PreProcessor Parameters:
            
             1 - Name of the table being replicated
             2 - Indexed find clause for the given table
             3 - yes/no: Whether the index was unique

Parameters:  record-data - RAW description of the after  image of the record
             rep-event   - the type of replication event
             
Returns:     Collision Occurred

Modification Notes
*/

&SCOPED-DEFINE TableName   {1}
&SCOPED-DEFINE WhereClause {2}
&SCOPED-DEFINE UniqueIndex {3}

DEF INPUT PARAMETER record-bi AS RAW  NO-UNDO.
DEF INPUT PARAMETER record-ai AS RAW  NO-UNDO.
DEF INPUT PARAMETER rep-event AS CHAR NO-UNDO.

DEF TEMP-TABLE Repl{&TableName} LIKE {&TableName}.
DEF VAR collision-error    AS LOGI NO-UNDO  INITIAL No.
DEF VAR transfer-error     AS LOGI NO-UNDO  INITIAL No.
DEF VAR create-error       AS LOGI NO-UNDO  INITIAL No.
DEF VAR not-found-error    AS LOGI NO-UNDO  INITIAL No.
DEF VAR records-identical  AS LOGI NO-UNDO.
DEF VAR record-id-found    AS RECID NO-UNDO.
DEF VAR matching-found     AS INT NO-UNDO INITIAL 0.

DISABLE TRIGGERS FOR LOAD OF {&TableName}.

/* Set up the record to be replicated */
CREATE Repl{&TableName} NO-ERROR.
create-error = ERROR-STATUS:ERROR .

ERROR-STATUS:ERROR = No.

IF rep-event = "C" THEN
  RAW-TRANSFER record-ai TO Repl{&TableName} NO-ERROR.
ELSE
  RAW-TRANSFER record-bi TO Repl{&TableName} NO-ERROR.

transfer-error = ERROR-STATUS:ERROR .

IF rep-event = "C" THEN DO: /* Create */
    FIND {&TableName} WHERE {&WhereClause} EXCLUSIVE-LOCK NO-ERROR.
    collision-error = {&UniqueIndex} AND AVAILABLE( {&TableName} ).
    IF NOT collision-error THEN DO:
      CREATE {&TableName} NO-ERROR.
      create-error = create-error OR ERROR-STATUS:ERROR .
      RAW-TRANSFER record-ai TO {&TableName} NO-ERROR.
      transfer-error = transfer-error OR ERROR-STATUS:ERROR.
    END.
END.
ELSE IF {&UniqueIndex} THEN DO:
    FIND {&TableName} WHERE {&WhereClause} EXCLUSIVE-LOCK NO-ERROR.
    IF AVAILABLE( {&TableName} ) THEN DO:
      BUFFER-COMPARE {&TableName} TO Repl{&TableName}
                           SAVE RESULT IN records-identical.
      collision-error = NOT records-identical.
    END.
    ELSE  /* only return not-found error when not deleting */
      not-found-error = (rep-event <> "D") /* Yes */.
END.
ELSE DO:
    not-found-error = Yes.
    for-each-table-name:
    FOR EACH {&TableName} NO-LOCK WHERE {&WhereClause}:
      BUFFER-COMPARE {&TableName} TO Repl{&TableName}
                                     SAVE RESULT IN records-identical.
      IF records-identical THEN ASSIGN
        record-id-found    = RECID( {&TableName} )
        matching-found     = matching-found + 1.
    END.
    IF matching-found = 1 THEN DO:
      FIND {&TableName} WHERE RECID( {&TableName} ) = record-id-found EXCLUSIVE-LOCK NO-ERROR.
      not-found-error = No.
    END.
END.


/* Apply the changes only if a collision */
IF rep-event = "M" THEN
  RAW-TRANSFER record-ai TO {&TableName} NO-ERROR.
ELSE IF rep-event = "D" THEN
  DELETE {&TableName} NO-ERROR.

RETURN TRIM( (IF collision-error THEN "COLLISION," ELSE "")
           + (IF transfer-error THEN "TRANSFER," ELSE "")
           + (IF create-error THEN "CREATE," ELSE "")
           + (IF not-found-error THEN "NOT FOUND," ELSE "")
           , ",").

/* DONE ! */
