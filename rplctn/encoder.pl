#! /perl/bin/perl -w
#
# Encoder: encode one or more files in specified directory (in configuration
# file) into RFC822 messages to be sent by electronic mail.
#
# Written by Ewen McNeill, 18/11/1997.
#
# The specified directory is searched for files, and each file found is processed
# into an individual message in the outbound directory.  The original is then
# moved into the processed directory.
#
# At present the processing for an individual file consists of:
#    compression with the Zip utility (to reduce overall size)
#    MIME (base64) encoding
#    RFC822 (and MIME) headers prepended.
#
#---------------------------------------------------------------------------

use 5;

use DirHandle;                 # Read directories (the OO way)
use MIME::Base64;              # Base64 encoding
use Mail::Header;
use strict;

require "sendconf.ph";         # Configuration information for sender

#---------------------------------------------------------------------------

# Subroutines

# Processfile: expects filename to process (without directory); file is assumed
#              to be in the senddir directory.
#
# At present this: Builds a Zip of the incoming file,
#                  prepends suitable RFC822 (and MIME) headers,
#                  and then appends base64 encoding of the file.

# Returns 1 if the file is processed correctly, 0 otherwise.

sub processfile
{
  my ($filename) = @_;
  die "Filename undefined" if (!defined($filename));

  #print "Filename to process = ", $filename, "\n";

  # Figure out name of zip file to build
  my ($zipfile) = $filename;

  if ($filename =~ /\.(\S+)/)
  {  $zipfile =~ s/\.\S+/.zip/; }
  else
  {  $zipfile = $filename . ".zip"; }

  #print "Zip file = ", $zipfile, "\n";

  # Build Zip file (note: make sure this junks directories, use "-j" with
  # InfoZip; also make sure it accepts "/" slashes :-) )
  #
  # XXX -- it'd be nice if we could hide stdout while doing this.
  my $rc =
     system("zip -j $SendConf::tempdir/$zipfile $SendConf::senddir/$filename");

  if ($rc != 0)
  {
    warn "Zip failed! (RC = $rc)  Skipping $SendConf::senddir/$filename\n";
    return 0;
  }

  # Now we have the zip file, at $SendConf::tempdir/$zipfile, so we can start
  # building the message.

  if (! open ZIP, "$SendConf::tempdir/$zipfile")
  {
    warn "Unable to open zip file at: $SendConf::tempdir/$zipfile -- skipping\n";
    return 0;
  }

  # Zip files have binary data, so we need binary mode for them.
  binmode ZIP;

  if (open MESSAGE, ">$SendConf::outgoing/$filename")
  {
    # Now we can rock and roll.  We've got an input file and an output file.
    # Let's build the header first.

    my $header = new Mail::Header;
    $header->add("From",    $SendConf::myemail);
    $header->add("To",      $SendConf::theiremail);
    $header->add("Subject", "Transfer of \"" . $filename . "\"");

    # MIME headers (these are a hack based on the headers Microsoft mail
    # spits out; they'll do for now)
    $header->add("MIME-Version", "1.0");
    $header->add("Content-Type", "application/x-zip-compressed; name=\"$zipfile\"");
    $header->add("Content-Transfer-Encoding", "base64");

    # And spit it out to the file.
    $header->print(\*MESSAGE);
    print MESSAGE "\n";

#    # This next bit is a waste of time, but we need two parts to satisfy
#    # multipart/mixed.  XXX -- remove this bit when find proper type for above.
#    print MESSAGE "--- cut here ---\n";
#    print MESSAGE "Content-Type: text/plain; charset=\"us-ascii\"\n";
#    print MESSAGE "Content-Transfer-Encoding: 7bit\n";
#    print MESSAGE "\n";
#    print MESSAGE "Attached, one zip file.  One size fits all.\n";
#    print MESSAGE "\n";

    # Introduce our file
#    print MESSAGE "--- cut here ---\n";
#    print MESSAGE "Content-Type: application/x-zip-compressed; name=\"" .
#                  $zipfile . "\"\n";
#    print MESSAGE "Content-Transfer-Encoding: base64\n";
#    print MESSAGE "\n";

    # Now we need to base64 encode the file, and spit that out next.
    # WARNING: <ZIP> style read-it-all often doesn't seem to in Win32 port,
    #          so use read to read things in.  Read for file length, plus a
    #          bit more just to be sure we got it all :-)
    #
    my $thezip;
    my $ziplen = -s "$SendConf::tempdir/$zipfile";
    read ZIP, $thezip, $ziplen + 1024;
    my $base64zip = encode_base64($thezip);
    print MESSAGE $base64zip;

    # Now some MIME trailer stuff.
    print MESSAGE "\n";
#    print MESSAGE "--- cut here ---\n";

    # And we're done -- clean things up.
    close(ZIP);
    close(MESSAGE);
    unlink("$SendConf::tempdir/$zipfile");
    unlink("$SendConf::sendprocessed/$filename") &&
      warn "Removed older copy of \"$filename\" from processed directory\n";
    rename("$SendConf::senddir/$filename","$SendConf::sendprocessed/$filename") ||
      warn "Unable to rename \"$filename\" into processed directory.\n";

    return 1;
  }
  else
  {
    warn "Could not open output file: ", $SendConf::outgoing, "/", $filename, "\n";
    unlink $SendConf::tempdir . "/" . $zipfile;
    return 0;    
  }
}



#---------------------------------------------------------------------------
# Mainline

# Check configuration values are set

die if (! defined($SendConf::senddir));
die if (! defined($SendConf::sendprocessed));
die if (! defined($SendConf::outgoing));
die if (! defined($SendConf::myemail));
die if (! defined($SendConf::theiremail));

# Scan the directory for files to process, and if there are any process them
# one at a time.

my $numprocessed = 0;

my $dir = new DirHandle $SendConf::senddir;
if (defined $dir) 
{
  #print "Scanning directory: ", $SendConf::senddir, "\n";
  my $file = "";
  while (defined($file = $dir->read))
  {
    if (-f "$SendConf::senddir/$file")
    { if (processfile($file))
      { $numprocessed++; }
    }
  }
}
else
{
  warn "Unable to read from directory: ", $SendConf::senddir, "\n";
  print "0 files processed.\n";
  exit 0;
}

if ($numprocessed != 1)
{ print "$numprocessed files processsed.\n"; }
else
{ print "1 file processed.\n"; }

exit $numprocessed;

