&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : delreptg.p
    Purpose     : Create a schema df to delete all replication triggers
                  from the current database

    Syntax      : delreptg.p( <file-name> )

    Description :

    Author(s)   : Donald Christie
    Created     : 
    Notes       :
  ------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEF INPUT PARAMETER file-name AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .3
         WIDTH              = 33.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

IF file-name = ? THEN file-name = "rplctn\delreptg.df".

OUTPUT TO VALUE( file-name ).

FOR EACH _File WHERE NOT _File._File-Name BEGINS "_" NO-LOCK:

  PUT UNFORMATTED    
      "UPDATE TABLE """ _file._file-name """"  SKIP
      "    TABLE-TRIGGER ""REPLICATION-CREATE"" DELETE" SKIP
      "    TABLE-TRIGGER ""REPLICATION-DELETE"" DELETE" SKIP
      "    TABLE-TRIGGER ""REPLICATION-WRITE""  DELETE" SKIP.
      
END.

OUTPUT CLOSE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


