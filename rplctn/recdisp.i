/*
System:      Property Management and Finance System
Include:     recdisp.i
Author:      Tyrone McAuley
Date:        09/10/97

PreProcessor Parameters:
            
             1 - Name of the table being displayed
             2 - Progress statement that converts the
                 record fields into an identifiable/readable string

Parameters:  record-image  - RAW description of the record
             record-string - String description of the record
             
Returns:     Record String Description
Modification Notes
*/

&SCOPED-DEFINE TableName    {1}
&SCOPED-DEFINE RecordString {2}

DEF INPUT  PARAMETER record-image  AS RAW  NO-UNDO.
DEF OUTPUT PARAMETER record-string AS CHAR NO-UNDO.

DEF TEMP-TABLE Temp{&TableNAme} LIKE {&TableName}.

CREATE Temp{&TableName}.
RAW-TRANSFER record-image TO Temp{&TableName} NO-ERROR.

record-string = {&RecordString}.

