#! /perl/bin/perl -w
#
# Decoder: process one or more RFC822 messages containing MIME encoded
#          files (as produced by encoder), into the constituant files.
#
# Written by Ewen McNeill, 19/11/1997.
#
# This program scans the supplied directory for files; each file is
# considered to be a message.  If the message is in a suitable format
# then, the enclosed ZIP file will be extracted into a temporary file,
# and then decompressed into the received directory.  Each file that
# is successfully processed is then moved into the processed directory.
#
#---------------------------------------------------------------------------

use 5;

use Cwd;                           # So we know where we are!
use DirHandle;                     # Scanning directories
use Mail::Header;                  # Internet mail messages
use MIME::Base64;                  # MIME encoding.
use strict;

require "recvconf.ph";             # Configuration of receive tools.

#---------------------------------------------------------------------------

# Subroutines

# DecodeMessage -- expects to receive a filename
#
# The file is assumed to be in the incoming directory.  It is treated as
# a RFC822 message, the headers read in, and scanned for suitable things.
# 
# If it is suitable, then the enclosed file is extracted into the temporary
# directory, and the file is unzipped into the load directory.
#
# Returns 1 if message processed successfully; 0 otherwise.

sub decodemessage
{
  my ($filename) = @_;
  die "No filename" if (! defined($filename));

  #print "Processing: $filename\n";

  if (! open(MSG, "<$RecvConf::downloaded/$filename"))
  {
    warn "Unable to open \"$RecvConf::downloaded/$filename\" -- skipping\n";
    return 0;
  }

  my $message = new Mail::Header(\*MSG);
  if (! defined($message))
  {
    warn "Unable to create new Internet Message Header object\n";
    close(MSG);
    return 0;
  }

  my $subject = $message->get('Subject');
  my $mime    = $message->get('MIME-Version');
  my $content = $message->get('Content-Type');
  my $encoding= $message->get('Content-Transfer-Encoding');

  if (!defined($subject) || !defined($mime) || !defined($content) ||
      !defined($encoding))
  {
    warn "Required header fields missing from file ($filename) -- skipping\n";
    close(MSG);
    return 0;
  }

  if ($subject !~ /^Transfer of ".*"$/)
  { 
    warn "Subject line invalid in file ($filename) -- skipping\n";
    close(MSG);
    return 0;
  }
  if ($mime !~ /^1.0/)
  {
    warn "Unexpected MIME version in file ($filename) -- skipping\n";
    close(MSG);
    return 0;
  }
  if ($content !~ /^application\/x-zip-compressed; name="\S+\.zip"$/)
  {
    warn "Unexpected Content-Type in file ($filename) -- skipping\n";
    warn "Content: $content\n";
    close(MSG);
    return 0;
  }
  if ($encoding !~ /^base64$/)
  { 
    warn "Unexpected encoding in file ($filename) -- skipping\n";
    close(MSG);
    return 0;
  }

  # Okay, looks like our message.  The rest of it should be a base64 
  # encoded file, so we'll pull that into an scalar, and decode it.

  my $base64len  = -s "$RecvConf::downloaded/$filename";
  my $base64text;
  read MSG, $base64text, $base64len;
  my $rawtext    = decode_base64($base64text);

  close(MSG);

  if (!defined($rawtext) || !defined($base64text))
  {
    warn "Error decoding file from message in file ($filename) -- skipping\n";
    return 0;
  }

  # Now we'd better save that somewhere.  We used a fixed filename because
  # (a) it makes us less vulnerable to attacks from outside, and (b) it
  # makes life easier for decoding, and (c) it ensures the old ones go
  # away anyway.

  if (! open(ZIP, ">$RecvConf::tempdir/received.zip"))
  {
    warn "Unable to open temporary file: $RecvConf::tempdir/received.zip\n";
    return 0;
  }

  # Switch it into binary mode -- important since we're about to write
  # binary data out to it!
  binmode ZIP;
  
  if (! print ZIP $rawtext)
  {
    warn "Unable to write data out to temporary file.  Abandoning file.\n";
    close(ZIP);
    unlink("$RecvConf::tempdir/received.zip") ||
      warn "Stray temporary file remains -- unable to remove\n";
    return 0;
  }

  if (! close(ZIP))
  {
    warn "Unable to close temporary zip file.  Abandoning.\n";
    unlink("$RecvConf::tempdir/received.zip") ||
      warn "Stray temporary file remains -- unable to remove\n";
    return 0;
  }

  # We've got it in a temporary file, now we need to extract the file out
  # of the zip, and into the destination directory.  We get the zip program
  # to do most of this hard work, and assume it worked unless the zip program
  # complains to us.
  #
  # WARNING: Do NOT extract directories.  Make certain they are junked, not
  # only to ensure the files end up where we want them, but to reduce the
  # security concerns.
  #
  # NOTE: we use the overwrite flag to avoid things hanging on files existing
  # and to get the (hopefully) latest copy in the directory.
  #
  # WARNING: pkunzip doesn't appear to have a way to fix the output directory
  # somewhere other than "current directory"; and it is not possible to 
  # reliably change directories then give a full path to the ZIP 'cause
  # chances are on a Windows system it'll have spaces in it.
  #
  # Thus: Infozip 5.1 or greater is required.
  #
  # Note: remove -q if you want to see zip at work.
  #
  my $zipfile = "$RecvConf::tempdir/received.zip";

  my $rc = system("unzip -o -j -q $zipfile -d $RecvConf::recvdir");

  unlink("$RecvConf::tempdir/received.zip") || 
    warn "Unable to remove temporary file -- stray remains\n";

  if ($rc != 0)
  {
    warn "Unzip failed.  Abandoning file ($filename)\n";
    return 0;
  }
  else
  { # Unzipped successfully, move file into processed directory
    unlink("$RecvConf::downloadprocessed/$filename") &&
      warn "Removed old copy of \"$RecvConf::downloadprocessed/$filename\"\n";
    rename("$RecvConf::downloaded/$filename", 
           "$RecvConf::downloadprocessed/$filename") ||
      warn "Unable to rename \"$filename\" into processed directory\n";

    # Yippee, it worked.
    return 1;
  }
}

#---------------------------------------------------------------------------

# Mainline

die "No download directory"  if (! defined($RecvConf::downloaded));
die "No processed directory" if (! defined($RecvConf::downloadprocessed));
die "No received directory"  if (! defined($RecvConf::recvdir));
die "No temporary directory" if (! defined($RecvConf::tempdir));

my $numprocessed = 0;
my $dir = new DirHandle $RecvConf::downloaded;
if (defined $dir) 
{
  #print "Scanning directory: ", $RecvConf::downloaded, "\n";
  my $file = "";
  while (defined($file = $dir->read))
  {
    if (-f "$RecvConf::downloaded/$file")
    { if (decodemessage($file))
      { $numprocessed++; }
    }
  }
}
else
{
  warn "Unable to read from directory: ", $RecvConf::downloaded, "\n";
  print "0 files processed.\n";
  exit 0;
}

if ($numprocessed != 1)
{ print "$numprocessed files processsed.\n"; }
else
{ print "1 file processed.\n"; }

exit $numprocessed;
