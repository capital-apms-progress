&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------

  File:  

  Description: from BROWSER.W - Basic SmartBrowser Object Template

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR current-triggers AS CHAR FORMAT "X(3)" LABEL "Currently" NO-UNDO.
DEF VAR df-name AS CHAR NO-UNDO INIT "rplctn\reptrig.df".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ReplTrigger

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table ReplTrigger.TableToRepl ~
ReplTrigger.Activity current-triggers @ current-triggers 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table ReplTrigger.TableToRepl ~
ReplTrigger.Activity 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}TableToRepl ~{&FP2}TableToRepl ~{&FP3}~
 ~{&FP1}Activity ~{&FP2}Activity ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table ReplTrigger
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table ReplTrigger
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH ReplTrigger WHERE ~{&KEY-PHRASE} NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table ReplTrigger
&Scoped-define FIRST-TABLE-IN-QUERY-br_table ReplTrigger


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table RECT-1 cmb_table tgl_create ~
tgl_modify tgl_delete 
&Scoped-Define DISPLAYED-OBJECTS cmb_table tgl_create tgl_modify tgl_delete 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS
><EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_table AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS " "
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 16.57 BY 4.

DEFINE VARIABLE tgl_create AS LOGICAL INITIAL no 
     LABEL "Create" 
     VIEW-AS TOGGLE-BOX
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tgl_delete AS LOGICAL INITIAL no 
     LABEL "Delete" 
     VIEW-AS TOGGLE-BOX
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tgl_modify AS LOGICAL INITIAL no 
     LABEL "Modify" 
     VIEW-AS TOGGLE-BOX
     SIZE 8 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      ReplTrigger SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      ReplTrigger.TableToRepl
      ReplTrigger.Activity COLUMN-LABEL "Rules"
      current-triggers @ current-triggers
  ENABLE
      ReplTrigger.TableToRepl
      ReplTrigger.Activity
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 38.29 BY 15.6
         BGCOLOR 15 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
     cmb_table AT ROW 2.2 COL 38.43 COLON-ALIGNED NO-LABEL
     tgl_create AT ROW 4.6 COL 43.29
     tgl_modify AT ROW 5.6 COL 43.29
     tgl_delete AT ROW 6.6 COL 43.29
     "Table:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 1.2 COL 40.43
     "Replicate Changes on:" VIEW-AS TEXT
          SIZE 15.43 BY 1 AT ROW 3.6 COL 41
     RECT-1 AT ROW 4 COL 40.43
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 16.6
         WIDTH              = 61.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "ttpl.ReplTrigger"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _FldNameList[1]   > ttpl.ReplTrigger.TableToRepl
"ReplTrigger.TableToRepl" ? ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[2]   > ttpl.ReplTrigger.Activity
"ReplTrigger.Activity" "Rules" ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[3]   > "_<CALC>"
"current-triggers @ current-triggers" ? ? ? ? ? ? ? ? ? no ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-DISPLAY OF br_table IN FRAME F-Main
DO:
  current-triggers:FGCOLOR IN BROWSE {&BROWSE-NAME} = 2.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ReplTrigger.TableToRepl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ReplTrigger.TableToRepl br_table _BROWSE-COLUMN B-table-Win
ON LEAVE OF ReplTrigger.TableToRepl IN BROWSE br_table /* Table */
DO:
  IF NOT AVAILABLE ReplTrigger THEN RETURN.

  DEF VAR widg-ent AS WIDGET-HANDLE NO-UNDO.
  widg-ent = LAST-EVENT:WIDGET-ENTER.
  /* Check to see if delete was pressed - Crude */
  IF VALID-HANDLE( widg-ent ) AND 
     INDEX(  REPLACE( widg-ent:LABEL, "&", "" ), "delete" ) <> 0 THEN RETURN.

  IF NOT CAN-FIND( FIRST _File WHERE _File._File-Name = SELF:SCREEN-VALUE ) THEN DO:
    MESSAGE "The table" SELF:SCREEN-VALUE "does not exist!"
      VIEW-AS ALERT-BOX ERROR TITLE "Invalid Table Name".
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_table B-table-Win
ON VALUE-CHANGED OF cmb_table IN FRAME F-Main
DO:
  RUN table-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_create
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_create B-table-Win
ON VALUE-CHANGED OF tgl_create IN FRAME F-Main /* Create */
DO:
  RUN options-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_delete B-table-Win
ON VALUE-CHANGED OF tgl_delete IN FRAME F-Main /* Delete */
DO:
  RUN options-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_modify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_modify B-table-Win
ON VALUE-CHANGED OF tgl_modify IN FRAME F-Main /* Modify */
DO:
  RUN options-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF

ON FIND OF ReplTrigger OVERRIDE DO:
  FIND _File WHERE _File._File-Name = ReplTrigger.TableToRepl
    NO-LOCK NO-ERROR.

  IF AVAILABLE _File THEN DO:
    current-triggers =
      ( IF CAN-FIND( _File-Trig WHERE _File-Trig._File-Recid = RECID( _File )
                                  AND _File-Trig._Event = "REPLICATION-CREATE" )
          THEN "C" ELSE "" ) +   
      ( IF CAN-FIND( _File-Trig WHERE _File-Trig._File-Recid = RECID( _File )
                                  AND _File-Trig._Event = "REPLICATION-WRITE" )
          THEN "M" ELSE "" ) +   
      ( IF CAN-FIND( _File-Trig WHERE _File-Trig._File-Recid = RECID( _File )
                                  AND _File-Trig._Event = "REPLICATION-DELETE" )
          THEN "D" ELSE "" ).   
  END.
  IF current-triggers = "" THEN current-triggers = "---".
END.

ReplTrigger.Activity:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
ReplTrigger.TableToRepl:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-all-rules B-table-Win 
PROCEDURE clear-all-rules :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FOR EACH ReplTrigger EXCLUSIVE-LOCK:
    ReplTrigger.Activity = "".
  END.

  RUN dispatch( 'open-query':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE force-all-rules B-table-Win 
PROCEDURE force-all-rules :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FOR EACH ReplTrigger EXCLUSIVE-LOCK:
    ReplTrigger.Activity = "CMD".
  END.

  RUN dispatch( 'open-query':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-value-changed B-table-Win 
PROCEDURE inst-value-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN trigger-changed.
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, "delete-record", "SENSITIVE = " +
    IF AVAILABLE ReplTrigger THEN "Yes" ELSE "No" ).
      
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record B-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN dispatch( 'add-record':U ).
  RUN table-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy B-table-Win 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF AVAILABLE ReplTrigger THEN RUN dispatch( 'update-record':U ).
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE match-database B-table-Win 
PROCEDURE match-database :
/*------------------------------------------------------------------------------
  Purpose:     Regenerate the dump rules according to the database triggers
------------------------------------------------------------------------------*/

  IF AVAILABLE ReplTrigger THEN RUN dispatch( 'update-record':U ).
  
  MESSAGE
    "Are you sure you want to regenerate this table" SKIP
    "from the existing database triggers?"
    VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "Proceed ?" 
    UPDATE regenerate-it AS LOGI.

  IF NOT regenerate-it THEN RETURN.

  RUN rplctn/db2rptrg.p .  

  RUN dispatch( 'open-query':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE options-changed B-table-Win 
PROCEDURE options-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  IF NOT AVAILABLE ReplTrigger THEN RETURN.
  DEF VAR act AS CHAR NO-UNDO.
  ASSIGN FRAME {&FRAME-NAME} tgl_create tgl_modify tgl_delete.
  act = ( IF tgl_create THEN "C" ELSE "" ) +
        ( IF tgl_modify THEN "M" ELSE "" ) +
        ( IF tgl_delete THEN "D" ELSE "" ).
  ReplTrigger.Activity:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = act.
  /* Ensure that the record gets updated */
  RUN dispatch( 'update-record':U ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize B-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  /* Populate the table name combo */

  FOR EACH _File WHERE NOT _File._File-Name BEGINS "_" NO-LOCK:
    IF cmb_table:ADD-LAST( _File._File-Name ) IN FRAME {&FRAME-NAME} THEN.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE regenerate-df B-table-Win 
PROCEDURE regenerate-df :
/*------------------------------------------------------------------------------
  Purpose:     Regenerate a schema df file containing all
               of the instructions necessary to add replication
               triggers to all of the tables that have dump rules
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE ReplTrigger THEN RUN dispatch( 'update-record':U ).
  
  MESSAGE
    "You should only regenerate the replication schema" SKIP
    "from a pre-production database." SKIP(1)
    "Are you sure you want to continue?"
    VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "Proceed ?" 
    UPDATE regenerate-it AS LOGI.

  IF NOT regenerate-it THEN RETURN.
  
  RUN rplctn\mkreptrg.p( df-name ).
  MESSAGE 
    "Schema regeneration complete!" SKIP(1)
    "You need to load the schema df:" SKIP
    "        " df-name SKIP
    "into all replication databases" SKIP
    "and recompile all/appropriate" SKIP
    "programs"
    VIEW-AS ALERT-BOX INFORMATION.
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE replicate-out B-table-Win 
PROCEDURE replicate-out :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN notify( 'set-busy,container-source':U ).
  RUN rplctn/repldump.p .
  RUN notify( 'set-idle,container-source':U ).
  MESSAGE "Data for outward replication dumped to file".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ReplTrigger"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE table-changed B-table-Win 
PROCEDURE table-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  ReplTrigger.TableToRepl:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = INPUT cmb_table.
  /* Ensure that the record gets updated */
  RUN dispatch( 'update-record':U ).
  IF BROWSE {&BROWSE-NAME}:REFRESH() THEN.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE trigger-changed B-table-Win 
PROCEDURE trigger-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF VAR act AS CHAR NO-UNDO.
  act = INPUT BROWSE {&BROWSE-NAME} ReplTrigger.Activity.
  tgl_create = INDEX( act, "C" ) <> 0.
  tgl_modify = INDEX( act, "M" ) <> 0.
  tgl_delete = INDEX( act, "D" ) <> 0.
  cmb_table:SCREEN-VALUE  = INPUT BROWSE {&BROWSE-NAME} ReplTrigger.TableToRepl.
  DISPLAY
    tgl_create
    tgl_modify
    tgl_delete
  WITH FRAME {&FRAME-NAME}.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


