/*
System:     Property Management and Finance System
Program:    creptrig.i
Author:     Donald Christie
Date:       26/08/97
Description:    Generic include file to create replication entries in the 
                replication log table on modify
                Goes on the Replication-Create trigger which 
                is fires immediately after the create command (ie no data)
                I need this as the NEW function does not work in the 
                replication-write trigger.
Usage:  Pass in the table name and action.
        &rtable = <name of the table of this trigger>

Modification Notes
*/

{inc\ofc-this.i}

/* check that replication is valid at this site...*/


FIND FIRST ReplTrigger WHERE ReplTrigger.TableToRepl = "{&rtable}"
NO-LOCK NO-ERROR.

IF AVAILABLE ReplTrigger
AND INDEX (ReplTrigger.Activity, "C") <> 0
THEN DO:

    /* because the NEW function does not work create a 
       blank repl entry in the replication create trigger
       if this is a new record...*/

    CREATE ReplLog.

    ASSIGN ReplLog.ReplID = NEXT-VALUE (Replid)
           ReplLog.OfficeCode = Office.OfficeCode
           ReplLog.TableToRepl = '{&rtable}'
           ReplLog.TransactID = DBTASKID (LDBNAME (BUFFER {&rtable}))  
           ReplLog.ReplDate = TODAY
           ReplLog.ReplTime = TIME
           ReplLog.TableRowID = RECID ({&rtable})
           ReplLog.ReplEvent = "C".


END. /* if available...*/

