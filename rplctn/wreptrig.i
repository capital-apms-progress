/*
System:     Property Management and Finance System
Program:    wreptrig.i
Author:     Donald Christie
Date:       26/08/97
Description:    Generic include file to create replication entries in the 
                replication log table on modify
                Goes on the Replication-Write trigger which also handles create events
                (the write triggers always fire).
Usage:  Pass in the table name and action.
        &rtable = <name of the table of this trigger>

Modification Notes:
    On      By      Because
6/4/1999  Andrew    The batch processor (process/bq-processor.p) updates an
                    OfficeSettings record every five seconds and since the
                    record is designed to indicate that the local batch queue
                    is not dead yet, the record should not be replicated.
                    Really, the better solution would be to change the batch
                    processor to use an RP record, but there's no guarantee
                    someone wouldn't replicate them either.
                    The perfect solution would be to use a file which cannot
                    be replicated (i.e. is explicitly excluded from replication).
                    Meanwhile, the record is explicitly excluded instead.
*/

{inc\ofc-this.i}

/* check that replication is valid at this site...*/
/* Use a can-find because sometimes progress stuffs up
   and reports that it can't find a repltrigger record
   although there is a no-error clause on the find */

IF CAN-FIND( FIRST ReplTrigger WHERE ReplTrigger.TableToRepl = "{&rtable}" ) THEN
DO:

/*** Following 3 lines added by Andrew on 6 April 1999 ***/
&IF "{&rtable}" = "OfficeSettings" &THEN
IF newOfficeSettings.SetName = "Batch-Queue-Enabled" THEN RETURN.
&ENDIF

    FIND FIRST ReplTrigger WHERE ReplTrigger.TableToRepl = "{&rtable}"
      NO-LOCK NO-ERROR.
    IF INDEX (ReplTrigger.Activity, "M") = 0 OR
       INDEX (ReplTrigger.Activity, "C") = 0 THEN RETURN.
       
    /* because the NEW function does not work we have created a 
       blank repl entry in the replication create trigger
       if this is a new record...*/
    FOR EACH ReplLog WHERE ReplLog.TableToRepl = '{&rtable}'
    AND ReplLog.TransactID = DBTASKID (LDBNAME (BUFFER {&rtable}))
    AND ReplLog.TableRowID = RECID (new{&rtable})
    EXCLUSIVE-LOCK:
        LEAVE.
    END.
        
    IF AVAILABLE ReplLog /* must be a created record */
    AND INDEX (ReplTrigger.Activity, "C") <> 0
        THEN DO:
        RAW-TRANSFER new{&rtable} to ReplLog.ReplAI. 
        ReplLog.ReplBI = ?.
    END.
    
    ELSE IF NOT AVAILABLE ReplLog
    AND INDEX (ReplTrigger.Activity, "M") <> 0
    THEN DO: /* must be an existing record */
            CREATE ReplLog.

        ASSIGN ReplLog.ReplID = NEXT-VALUE (Replid)
               ReplLog.OfficeCode = Office.OfficeCode
               ReplLog.TableToRepl = '{&rtable}'
               ReplLog.TransactID = DBTASKID (LDBNAME (BUFFER {&rtable}))  
               ReplLog.TableRowid = RECID (new{&rtable})
               ReplLog.ReplDate = TODAY
               ReplLog.ReplTime = TIME
               ReplLog.ReplEvent = "M".
    
    END. /* IF NOT AVAILABLE */

    IF AVAILABLE ReplLog 
    AND ReplLog.ReplEvent = "M" THEN DO:
        RAW-TRANSFER new{&rtable} to ReplLog.ReplAI. 
        RAW-TRANSFER old{&rtable} to ReplLog.ReplBI. 
    END.
    
END. /* if available repltrigger */
