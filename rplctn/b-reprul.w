&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------

  File:  

  Description: from BROWSER.W - Basic SmartBrowser Object Template

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ReplLoadRule

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table ReplLoadRule.SourceSystem ~
ReplLoadRule.TableToLoad ReplLoadRule.Activity ReplLoadRule.CollisionDetect 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table ReplLoadRule.SourceSystem ~
ReplLoadRule.TableToLoad ReplLoadRule.Activity ReplLoadRule.CollisionDetect 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}SourceSystem ~{&FP2}SourceSystem ~{&FP3}~
 ~{&FP1}TableToLoad ~{&FP2}TableToLoad ~{&FP3}~
 ~{&FP1}Activity ~{&FP2}Activity ~{&FP3}~
 ~{&FP1}CollisionDetect ~{&FP2}CollisionDetect ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table ReplLoadRule
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table ReplLoadRule
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH ReplLoadRule WHERE ~{&KEY-PHRASE} NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table ReplLoadRule
&Scoped-define FIRST-TABLE-IN-QUERY-br_table ReplLoadRule


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table cmb_office cmb_table RECT-1 ~
tgl_create tgl_modify tgl_delete tgl_rollback 
&Scoped-Define DISPLAYED-OBJECTS cmb_office cmb_table tgl_create tgl_modify ~
tgl_delete tgl_rollback 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
OfficeCode|y|y|ttpl.ReplLog.OfficeCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "OfficeCode",
     Keys-Supplied = "OfficeCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_office AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS " "
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_table AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS " "
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 16.57 BY 4.

DEFINE VARIABLE tgl_create AS LOGICAL INITIAL no 
     LABEL "Create" 
     VIEW-AS TOGGLE-BOX
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tgl_delete AS LOGICAL INITIAL no 
     LABEL "Delete" 
     VIEW-AS TOGGLE-BOX
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tgl_modify AS LOGICAL INITIAL no 
     LABEL "Modify" 
     VIEW-AS TOGGLE-BOX
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tgl_rollback AS LOGICAL INITIAL no 
     LABEL "Rollback on collisions?" 
     VIEW-AS TOGGLE-BOX
     SIZE 18.86 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      ReplLoadRule SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      ReplLoadRule.SourceSystem FORMAT "X(4)"
      ReplLoadRule.TableToLoad
      ReplLoadRule.Activity
      ReplLoadRule.CollisionDetect COLUMN-LABEL "Rollback On?" FORMAT "Yes/No"
  ENABLE
      ReplLoadRule.SourceSystem
      ReplLoadRule.TableToLoad
      ReplLoadRule.Activity
      ReplLoadRule.CollisionDetect
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 48.57 BY 13.6
         BGCOLOR 15 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
     cmb_office AT ROW 2 COL 48.14 COLON-ALIGNED NO-LABEL
     cmb_table AT ROW 4.4 COL 48.14 COLON-ALIGNED HELP
          "Enter the name of the replicated table to load" NO-LABEL
     tgl_create AT ROW 6.8 COL 53
     tgl_modify AT ROW 7.8 COL 53
     tgl_delete AT ROW 8.8 COL 53
     tgl_rollback AT ROW 10.8 COL 50.14
     "Source:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 1 COL 50.14
     "Table:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 3.4 COL 50.14
     "Load Changes on:" VIEW-AS TEXT
          SIZE 15.43 BY 1 AT ROW 5.8 COL 50.72
     RECT-1 AT ROW 6.2 COL 50.14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 14.6
         WIDTH              = 76.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "ttpl.ReplLoadRule"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _FldNameList[1]   > ttpl.ReplLoadRule.SourceSystem
"ReplLoadRule.SourceSystem" ? "X(4)" "character" ? ? ? ? ? ? yes ?
     _FldNameList[2]   > ttpl.ReplLoadRule.TableToLoad
"ReplLoadRule.TableToLoad" ? ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[3]   > ttpl.ReplLoadRule.Activity
"ReplLoadRule.Activity" ? ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[4]   > ttpl.ReplLoadRule.CollisionDetect
"ReplLoadRule.CollisionDetect" "Rollback On?" "Yes/No" "logical" ? ? ? ? ? ? yes ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_office
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_office B-table-Win
ON VALUE-CHANGED OF cmb_office IN FRAME F-Main
DO:
  RUN office-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_table B-table-Win
ON VALUE-CHANGED OF cmb_table IN FRAME F-Main
DO:
  RUN table-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_create
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_create B-table-Win
ON VALUE-CHANGED OF tgl_create IN FRAME F-Main /* Create */
DO:
  RUN options-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_delete B-table-Win
ON VALUE-CHANGED OF tgl_delete IN FRAME F-Main /* Delete */
DO:
  RUN options-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_modify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_modify B-table-Win
ON VALUE-CHANGED OF tgl_modify IN FRAME F-Main /* Modify */
DO:
  RUN options-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_rollback
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_rollback B-table-Win
ON VALUE-CHANGED OF tgl_rollback IN FRAME F-Main /* Rollback on collisions? */
DO:
  RUN rollback-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF

ReplLoadRule.Source:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
ReplLoadRule.Activity:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
ReplLoadRule.CollisionDetect:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
ReplLoadRule.TableToLoad:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'OfficeCode':U THEN DO:
       &Scope KEY-PHRASE ReplLog.OfficeCode eq key-value
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OfficeCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-value-changed B-table-Win 
PROCEDURE inst-value-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, "delete-record",
    "SENSITIVE = " + IF AVAILABLE ReplLoadRule THEN "Yes" ELSE "No" ).
  RUN rule-changed.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE load-changes B-table-Win 
PROCEDURE load-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  MESSAGE
    "Are you sure you want to load all" SKIP
    "outstanding replication changes ?" SKIP
    VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO
    TITLE "Proceed ?" UPDATE reload-it AS LOGI.
    
  IF NOT reload-it THEN RETURN.
  
  RUN rplctn\replload.p.
  
  MESSAGE "Replication Load complete"
    VIEW-AS ALERT-BOX INFORMATION TITLE "Done".
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record B-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN sensitise-fields( Yes ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN rule-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE office-changed B-table-Win 
PROCEDURE office-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  ReplLoadRule.Source:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = INPUT cmb_office.
  /* Ensure that the record gets updated */
  RUN dispatch( 'update-record':U ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE options-changed B-table-Win 
PROCEDURE options-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  IF NOT AVAILABLE ReplLoadRule THEN RETURN.
  DEF VAR act AS CHAR NO-UNDO.
  ASSIGN FRAME {&FRAME-NAME} tgl_create tgl_modify tgl_delete.
  act = ( IF tgl_create THEN "C" ELSE "" ) +
        ( IF tgl_modify THEN "M" ELSE "" ) +
        ( IF tgl_delete THEN "D" ELSE "" ).
  ReplLoadRule.Activity:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = act.
  /* Ensure that the record gets updated */
  RUN dispatch( 'update-record':U ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize B-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Populate the table name combo */

  FOR EACH _File WHERE NOT _File._File-Name BEGINS "_" NO-LOCK:
    IF cmb_table:ADD-LAST( _File._File-Name ) IN FRAME {&FRAME-NAME} THEN.
  END.

  /* Populate the office combo */

  FOR EACH Office NO-LOCK WHERE NOT ThisOffice:
    IF cmb_office:ADD-LAST( Office.OfficeCode ) IN FRAME {&FRAME-NAME} THEN.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rollback-changed B-table-Win 
PROCEDURE rollback-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  ReplLoadRule.CollisionDetect:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = 
    tgl_rollback:SCREEN-VALUE.
  /* Ensure that the record gets updated */
  RUN dispatch( 'update-record':U ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rule-changed B-table-Win 
PROCEDURE rule-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF VAR act AS CHAR NO-UNDO.
  CLEAR FRAME {&FRAME-NAME} ALL.
  act = INPUT BROWSE {&BROWSE-NAME} ReplLoadRule.Activity.
  tgl_create = INDEX( act, "C" ) <> 0.
  tgl_modify = INDEX( act, "M" ) <> 0.
  tgl_delete = INDEX( act, "D" ) <> 0.
  cmb_table:SCREEN-VALUE  = INPUT BROWSE {&BROWSE-NAME} ReplLoadRule.TableToLoad.
  ASSIGN cmb_office:SCREEN-VALUE = INPUT BROWSE {&BROWSE-NAME} ReplLoadRule.Source NO-ERROR.
  tgl_rollback = ReplLoadRule.CollisionDetect:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = "Yes".
  IF tgl_rollback = ? THEN tgl_rollback = No.
  DISPLAY
    tgl_create
    tgl_modify
    tgl_delete
    tgl_rollback
  WITH FRAME {&FRAME-NAME}.
  RUN sensitise-fields( AVAILABLE ReplLoadRule ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "OfficeCode" "ReplLog" "OfficeCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ReplLoadRule"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-fields B-table-Win 
PROCEDURE sensitise-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF INPUT PARAMETER sens AS LOGI NO-UNDO.

  cmb_office:SENSITIVE = sens.
  cmb_table:SENSITIVE  = sens.
  tgl_create:SENSITIVE = sens.
  tgl_modify:SENSITIVE = sens.
  tgl_delete:SENSITIVE = sens.
  tgl_rollback:SENSITIVE = sens.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE table-changed B-table-Win 
PROCEDURE table-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  ReplLoadRule.TableToLoad:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = INPUT cmb_table.
  /* Ensure that the record gets updated */
  RUN dispatch( 'update-record':U ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


