#! /perl/bin/perl
#
# Uploader: upload one or more RFC822-format messages in the specified
# directory to a (local) SMTP server for on-delivery.
#
# Written by Ewen McNeill, 18/11/1997.
#
# This program scans the specified directory for messages to send (all
# files in the directory are considered to be messages).  Any messages
# that are found are instantiated as Mail::Internet objects, and then
# delivered with SMTP to the local SMTP server.
#
#---------------------------------------------------------------------------

use 5;

use DirHandle;                      # Directory handling
use Net::SMTP;                      # Simple Mail Transfer Protocol

require "sendconf.ph";              # Sender configuration information

#---------------------------------------------------------------------------

# Subroutines

# SendMessage: expects filename of message to send (and that the file will
#              exist in the outbound directory).
#
# Sends the message to the SMTP server for on-delivery, and if that is successful
# moves the original file into the sent area.
#
# Returns 1 if the file is delivered to the SMTP server, and 0 otherwise.

sub sendmessage
{
  my ($filename) = @_;
  die "No file to send\n" if (! defined($filename));

  if ( ! open(MSG, "<$SendConf::outgoing/$filename") )
  {  
    warn "Unable to open \"$SendConf::outgoing/$filename\" -- skipping file\n";
    return 0;
  }

  # Note: need to specify who to say on the HELO line, because Windows isn't
  # particularly good at figuring it out.
  my $smtp = Net::SMTP->new($SendConf::SMTPserver, 
                            Hello => $SendConf::myhostname);
  if (!defined($smtp))
  { 
    warn "Cannot contact SMTP server: $SendConf::SMTPserver\n";
    warn "Skipping file: $filename\n";
    return 0;
  }

  my $rc;
  
  # Do the envelope stuff
  $rc = $smtp -> mail($SendConf::myemail);
  if (! $rc)
  { warn "Error encountered sending envelope from\n"; }
  else
  {
    $rc = $smtp -> to($SendConf::theiremail);
    if (! $rc)
    { warn "Error encountered sending envelope to\n"; }
    else
    {
      $rc = $smtp -> to($SendConf::copyemail);
      if (! $rc) 
      { warn "Error encountered sending envelope cc\n"; }
    }
  }

  if (! $rc)
  { 
    warn "Cannot start mail.  Abandoning mail transfer ($filename).\n";
    $smtp->reset();
    $smtp->quit();
    return 0;
  }

  # Send out the body
  $rc = $smtp->data();
  if (! $rc)
  {
    warn "Problems starting message data ($filename).  Abandoning transfer.\n";
    $smtp->reset();
    $smtp->quit();
    return 0;
  }

  while(<MSG>)
  {
    $rc = $smtp->datasend($_);
    if (! $rc)
    { warn "Problems sending data line: $_\n";  
      warn "Attempting to continue.\n";       # XXX -- is this wise?
    }
  }
  $rc = $smtp->dataend();
  if ($rc)
  { # Message was delivered correctly, tidy up.
    close(MSG);
    $smtp->quit();

    unlink("$SendConf::outgoingprocessed/$filename") &&
      warn "Removed older copy of \"$filename\" from processed directory\n";

    rename("$SendConf::outgoing/$filename", 
           "$SendConf::outgoingprocessed/$filename") ||
      warn "Message transfered correctly, unable to move out of way ($filename)\n";
   
    return 1;
  }
  else
  { # Problems sending the message -- leave it alone.
    close(MSG);
    $smtp->reset();
    $smtp->quit();
    warn "Problems sending message ($filename).  Leaving to try again later.\n";

    return 0;
  }
}

#---------------------------------------------------------------------------

# Mainline

# Check configuration values are set

die "No outgoing directory" if (! defined($SendConf::outgoing));
die "No processed directory" if (! defined($SendConf::outgoingprocessed));
die "No SMTP server" if (! defined($SendConf::SMTPserver));
die "No local email address" if (! defined($SendConf::myemail));
die "No remote email address" if (! defined($SendConf::theiremail));
die "Who am I anyway?" if (! defined($SendConf::myhostname));

# Scan the directory for files to process, and if there are any process them
# one at a time.

my $numprocessed = 0;

my $dir = new DirHandle $SendConf::outgoing;
if (defined $dir) 
{
  #print "Scanning directory: ", $SendConf::outgoing, "\n";
  my $file = "";
  while (defined($file = $dir->read))
  {
    if (-f "$SendConf::outgoing/$file")
    { if (sendmessage($file))
      { $numprocessed++; }
    }
  }
}
else
{
  warn "Unable to read from directory: ", $SendConf::senddir, "\n";
  print "0 files processed.\n";
  exit 0;
}

if ($numprocessed != 1)
{ print "$numprocessed files processsed.\n"; }
else
{ print "1 file processed.\n"; }

exit $numprocessed;
