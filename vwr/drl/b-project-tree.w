&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW

/* Temp-Table and Buffer definitions                                    */
DEFINE TEMP-TABLE Hierarchy NO-UNDO LIKE TTPL.Project
       FIELD Level AS INTEGER
       FIELD Committed AS DECIMAL
       FIELD Visible AS LOGICAL
       FIELD MySequence AS INTEGER
       FIELD SubsCount AS INTEGER.


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tpprojecttree.i}

DEF VAR formatted-name AS CHAR LABEL "Name" FORMAT "X(60)" NO-UNDO.
DEF VAR filter-value AS CHAR NO-UNDO        INITIAL "".
DEF VAR show-inactive AS LOGICAL NO-UNDO   INITIAL No.
DEF VAR show-all AS LOGICAL NO-UNDO   INITIAL No.

DEF VAR d-date-1 AS CHAR NO-UNDO.
DEF VAR d-date-2 AS CHAR NO-UNDO.
DEF VAR d-update AS CHAR NO-UNDO.

DEF VAR parent-project AS INT NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Project
&Scoped-define FIRST-EXTERNAL-TABLE Project


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Project.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Hierarchy

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table ( FILL( ' ', Hierarchy.Level ) + Hierarchy.Name ) @ formatted-name Hierarchy.Level Hierarchy.SubsCount Hierarchy.ApprovedAmount Hierarchy.Committed Hierarchy.ProjectCode Hierarchy.StartDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table   
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table
&Scoped-define SELF-NAME br_table
&Scoped-define OPEN-QUERY-br_table OPEN QUERY {&SELF-NAME} FOR EACH Hierarchy       WHERE Visible NO-LOCK       BY MySequence.
&Scoped-define TABLES-IN-QUERY-br_table Hierarchy
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Hierarchy


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
ProjectCode||y|Hierarchy.ProjectCode
Entity||y|Hierarchy.ProjectCode
EntityCode||y|Hierarchy.EntityCode
FirstApprover||y|Hierarchy.FirstApprover
Proposer||y|Hierarchy.Proposer
EntityType||y|Hierarchy.EntityType
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = "ProjectCode,Entity,EntityCode,FirstApprover,Proposer,EntityType"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS></SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ,
     SortBy-Case = ':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD build-sub-projects B-table-Win 
FUNCTION build-sub-projects RETURNS DECIMAL
  ( INPUT project-code AS INT, INPUT this-level AS INT, INPUT-OUTPUT next-seq AS INT)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-link-hdl B-table-Win 
FUNCTION get-link-hdl RETURNS HANDLE
  ( INPUT link-type AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Hierarchy SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _FREEFORM
  QUERY br_table NO-LOCK DISPLAY
      ( FILL( '    ', Hierarchy.Level ) + Hierarchy.Name ) @ formatted-name
      Hierarchy.Level  LABEL 'Lvl' FORMAT '>9'
      Hierarchy.SubsCount LABEL 'Subs' FORMAT '>>>'
      Hierarchy.ApprovedAmount FORMAT '->>>,>>>,>>9.99' LABEL 'Approved'
      Hierarchy.Committed FORMAT '->>>,>>>,>>9.99'
      Hierarchy.ProjectCode LABEL 'Code' FORMAT '>>>>9' WIDTH 5
      Hierarchy.StartDate WIDTH 10
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 94.29 BY 16.95
         BGCOLOR 15 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   External Tables: TTPL.Project
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
   Temp-Tables and Buffers:
      TABLE: Hierarchy T "?" NO-UNDO TTPL Project
      ADDITIONAL-FIELDS:
          FIELD Level AS INTEGER
          FIELD Committed AS DECIMAL
          FIELD Visible AS LOGICAL
          FIELD MySequence AS INTEGER
          FIELD SubsCount AS INTEGER
      END-FIELDS.
   END-TABLES.
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 20.05
         WIDTH              = 98.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main = 1.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH Hierarchy
      WHERE Visible NO-LOCK
      BY MySequence.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Where[1]         = "Visible"
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON CURSOR-LEFT OF br_table IN FRAME F-Main
DO:
  RUN toggle-hierarchy( false ).
  IF RETURN-VALUE = 'CHANGES' THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON CURSOR-RIGHT OF br_table IN FRAME F-Main
DO:
  RUN toggle-hierarchy( true ).
  IF RETURN-VALUE = 'CHANGES' THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON MOUSE-SELECT-DBLCLICK OF br_table IN FRAME F-Main
DO:
  RUN toggle-hierarchy(?).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* Synchronise for sending records to other things */
  IF AVAILABLE(Hierarchy) THEN
    FIND Project WHERE Project.ProjectCode = Hierarchy.ProjectCode NO-LOCK NO-ERROR.

  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */


RUN set-attribute-list( 'SortBy-Options = Code|Name|Entity, SortBy-Case = Code':U ).
RUN set-attribute-list( 'FilterBy-Options = Active|Inactive, FilterBy-Case = Active':U ).
RUN set-attribute-list( 'Filter-Value = ':U ).

DEF VAR select-options AS CHAR NO-UNDO  INITIAL "EXGS - Exclude 'G' and Sub-Projects|All  - All projects".
DEF VAR select-case AS CHAR NO-UNDO.
FOR EACH ProjectType NO-LOCK:
  select-options = select-options + "|" + STRING( ProjectType.ProjectType, "X(4)") + " - " + ProjectType.Description.
END.
RUN set-attribute-list( 'Select-Style = Combo-Box, Select-Options = ':U + select-options + ', Select-Case = ':U + ENTRY(1,select-options,'|')).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

  {&OPEN-QUERY-{&BROWSE-NAME}}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Project"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Project"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-parent-project B-table-Win 
PROCEDURE get-parent-project :
/*------------------------------------------------------------------------------
  Purpose:  Return the parent project to enter "Add" viewers from here
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER parent-project AS INT NO-UNDO.

DEF VAR key-value AS CHAR NO-UNDO.
DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR et AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  key-name = RETURN-VALUE.
  IF key-value = ? THEN DO:
    DEF VAR rs-hdl AS HANDLE NO-UNDO.
    rs-hdl = get-link-hdl( "RECORD-SOURCE":U ).
    IF rs-hdl <> ? THEN
      RUN send-key IN rs-hdl( key-name, OUTPUT key-value ).
  END.
  IF   key-name = "CompanyCode"
    OR key-name = "PropertyCode"
    OR key-name = "ParentProjectCode"
  THEN DO:
    CASE key-name:
      WHEN "CompanyCode" THEN       et = "L".
      WHEN "PropertyCode" THEN      et = "P".
      WHEN "ParentProjectCode" THEN et = "J".
    END CASE.
  END.

  IF et = "J" THEN 
    parent-project = INT(key-value).
  ELSE
    parent-project = ?.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available B-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR no-subs AS INT NO-UNDO INITIAL 0.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  IF RETURN-VALUE <> "ProjectCode" THEN DO:
    MESSAGE "Sorry." RETURN-VALUE "is not a supported key type here!".
    RETURN.
  END.

  FOR EACH Hierarchy: DELETE Hierarchy. END.

  parent-project = INT(key-value).
  FIND Project WHERE Project.ProjectCode = parent-project NO-LOCK.
  CREATE Hierarchy.
  BUFFER-COPY Project TO Hierarchy
                ASSIGN Level = 0
                       Committed = 0
                       Visible = yes
                       MySequence = no-subs.
  Hierarchy.Committed = build-sub-projects( parent-project, 1, no-subs).
  Hierarchy.SubsCount = no-subs.

  FOR EACH Hierarchy WHERE Level < 2:
    Visible = yes.
  END.

  RUN dispatch( 'open-query':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Project) THEN RETURN.

  /* Code placed here will execute PRIOR to standard behavior. */
  IF CAN-FIND( FIRST AcctTran WHERE AcctTran.EntityType = "J"
                     AND AcctTran.EntityCode = Project.ProjectCode) THEN DO:
    MESSAGE "Cannot delete project as posted transactions exist."
        VIEW-AS ALERT-BOX ERROR.
    RETURN.
  END.

  DO TRANSACTION ON ERROR UNDO, LEAVE:
    FOR EACH AccountBalance WHERE AccountBalance.EntityType = "J" AND
                      AccountBalance.EntityCode = Project.ProjectCode:
      DELETE AccountBalance.
    END.
    FOR EACH ProjectBudget OF Project:
      DELETE ProjectBudget.
    END.
    FIND CURRENT Project EXCLUSIVE-LOCK.
    DELETE Project.
  END.
  RUN dispatch( 'open-query':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE normal-send-key B-table-Win adm/support/_key-snd.p
PROCEDURE normal-send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "ProjectCode" "Hierarchy" "ProjectCode"}
  {src/adm/template/sndkycas.i "Entity" "Hierarchy" "ProjectCode"}
  {src/adm/template/sndkycas.i "EntityCode" "Hierarchy" "EntityCode"}
  {src/adm/template/sndkycas.i "FirstApprover" "Hierarchy" "FirstApprover"}
  {src/adm/template/sndkycas.i "Proposer" "Hierarchy" "Proposer"}
  {src/adm/template/sndkycas.i "EntityType" "Hierarchy" "EntityType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-pack B-table-Win 
PROCEDURE pre-pack :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER win-hdl AS HANDLE NO-UNDO.

DEF VAR opt-hdl AS HANDLE NO-UNDO.

  RUN get-attribute IN win-hdl( 'Option-Panel':U ).
  opt-hdl = WIDGET-HANDLE(RETURN-VALUE).

  IF VALID-HANDLE(opt-hdl) THEN RUN set-size IN opt-hdl( ?, 30).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER key-name AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER key-value AS CHAR NO-UNDO INITIAL ?.

  IF AVAILABLE(Hierarchy) THEN
    FIND Project WHERE Project.ProjectCode = Hierarchy.ProjectCode NO-LOCK NO-ERROR.

  IF key-value = ? THEN DO:
    /* Dispatch standard ADM method.                             */
    RUN normal-send-key( key-name, OUTPUT key-value ) NO-ERROR.
  END.

  IF NOT AVAILABLE(Hierarchy) THEN RETURN.

  IF key-name = "Entity" THEN
    key-value = "J/" + STRING(Hierarchy.ProjectCode).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Project"}
  {src/adm/template/snd-list.i "Hierarchy"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-sub-projects B-table-Win 
PROCEDURE set-sub-projects :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER project-code AS INT NO-UNDO.
DEF INPUT PARAMETER target-state AS LOGICAL NO-UNDO.

  IF NOT AVAILABLE(Hierarchy) OR Hierarchy.SubsCount < 1 THEN RETURN.
DEF BUFFER BufHierarchy FOR Hierarchy.

DEF VAR changes AS LOGI NO-UNDO INITIAL No.

  FOR EACH BufHierarchy WHERE BufHierarchy.EntityType = "J" AND BufHierarchy.EntityCode = project-code:
    IF target-state = ? THEN target-state = NOT( BufHierarchy.Visible ).
    IF BufHierarchy.Visible <> target-state THEN changes = Yes.
    BufHierarchy.Visible = target-state.
    IF target-state = false AND BufHierarchy.SubsCount > 0 THEN
      RUN set-sub-projects( BufHierarchy.ProjectCode, false ).
  END.

  IF changes THEN RETURN 'CHANGES'.
             ELSE RETURN ''.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE toggle-hierarchy B-table-Win 
PROCEDURE toggle-hierarchy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER target-state AS LOGICAL NO-UNDO.

DEF VAR current-row AS ROWID NO-UNDO.
  IF AVAILABLE(Hierarchy) AND Hierarchy.SubsCount > 0 THEN DO WITH FRAME {&FRAME-NAME}:
    RUN set-sub-projects( Hierarchy.ProjectCode, target-state ).
    IF RETURN-VALUE = 'CHANGES' THEN DO:
      current-row = ROWID(Hierarchy).
      CLOSE QUERY {&BROWSE-NAME}.
      {&OPEN-QUERY-{&BROWSE-NAME}}
      {&BROWSE-NAME}:SET-REPOSITIONED-ROW( 3, "ALWAYS" ).
      REPOSITION {&BROWSE-NAME} TO ROWID current-row.
      {&BROWSE-NAME}:SELECT-FOCUSED-ROW().
    END.
  END.

  RETURN RETURN-VALUE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Filter-Value B-table-Win 
PROCEDURE use-Filter-Value :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-filter-value AS CHAR NO-UNDO.
  filter-value = new-filter-value.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-filterby-case AS CHAR NO-UNDO.

  show-inactive = (new-filterby-case = "Inactive").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value B-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key AS CHAR NO-UNDO.
  key-value = new-key.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Select-Case B-table-Win 
PROCEDURE use-Select-Case :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-select-case AS CHAR NO-UNDO.

  select-case = ENTRY( 1, new-select-case, " ").
  IF select-case = "All" THEN select-case = "".
  show-all = (select-case <> "EXGS").
  IF NOT show-all THEN select-case = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION build-sub-projects B-table-Win 
FUNCTION build-sub-projects RETURNS DECIMAL
  ( INPUT project-code AS INT, INPUT this-level AS INT, INPUT-OUTPUT next-seq AS INT) :
/*------------------------------------------------------------------------------
  Purpose:  Add all of the sub-projects of this project into the hierarchy
    Notes:  
------------------------------------------------------------------------------*/
DEF BUFFER MyProj FOR Project.
DEF BUFFER BufHierarchy FOR Hierarchy.
DEF VAR total-committed AS DEC NO-UNDO INITIAL 0.0 .
DEF VAR order-committed AS DEC NO-UNDO.
DEF VAR subs-this AS INT NO-UNDO.

  FOR EACH MyProj WHERE MyProj.EntityType ='J' AND MyProj.EntityCode = project-code NO-LOCK:

    next-seq = next-seq + 1.
    subs-this = next-seq.
    CREATE BufHierarchy.
    BUFFER-COPY MyProj TO BufHierarchy
                ASSIGN Level = this-level
                       Committed = 0
                       Visible = no
                       MySequence = next-seq.
    BufHierarchy.Committed = build-sub-projects( MyProj.ProjectCode, this-level + 1, next-seq ).
    FOR EACH Order OF MyProj NO-LOCK:
      order-committed = Order.OverridePaid.
      IF order-committed = ? THEN order-committed = Order.OrderAmount.
      IF order-committed = 0 THEN order-committed = Order.ApprovedAmount .
      BufHierarchy.Committed = BufHierarchy.Committed + order-committed .
    END.
    BufHierarchy.SubsCount = next-seq - subs-this.
    total-committed = total-committed + BufHierarchy.Committed .
  END.

  RETURN total-committed.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-link-hdl B-table-Win 
FUNCTION get-link-hdl RETURNS HANDLE
  ( INPUT link-type AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR record-source-hdl AS HANDLE NO-UNDO.
DEF VAR lnk-hdl AS CHAR NO-UNDO.
DEF VAR parent-project LIKE Project.ProjectCode NO-UNDO INITIAL ?.
  
  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, link-type, OUTPUT lnk-hdl ).
  ASSIGN record-source-hdl = WIDGET-HANDLE( lnk-hdl ) NO-ERROR.
  IF VALID-HANDLE(record-source-hdl) THEN
    RETURN record-source-hdl.
  ELSE
    RETURN ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


