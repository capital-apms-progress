&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR entity-type AS CHAR NO-UNDO.
DEF VAR entity-code AS INT NO-UNDO.
DEF VAR account-code AS DEC NO-UNDO.

DEF VAR code-value AS CHAR NO-UNDO.
DEF VAR code-description AS CHAR NO-UNDO.
DEF VAR calc-balance AS DEC NO-UNDO FORMAT "->,>>>,>>>,>>9.99".

{inc/ofc-this.i}
{inc/ofc-set-l.i "Multi-Ledger-Creditors" "multi-ledger-creditors"}
{inc/topic/tpactsum.i}

DEF VAR fin-year AS INT NO-UNDO.
DEF VAR month-1 AS INT NO-UNDO INITIAL ?.
DEF VAR month-n AS INT NO-UNDO INITIAL ?.
FIND Month WHERE Month.StartDate <= TODAY AND Month.EndDate >= TODAY NO-LOCK NO-ERROR.
fin-year = Month.FinancialYearCode.
FIND FIRST Month WHERE Month.FinancialYearCode = fin-year NO-LOCK NO-ERROR.
month-1 = Month.MonthCode.
FIND LAST Month WHERE Month.FinancialYearCode = fin-year NO-LOCK NO-ERROR.
month-n = Month.MonthCode.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES AccountSummary

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table ~
(IF key-name = 'AccountCode' THEN AccountSummary.EntityType + STRING( AccountSummary.EntityCode, '99999') ELSE STRING( AccountSummary.AccountCode, '9999.99') ) @ code-value ~
get-description(AccountSummary.EntityType, AccountSummary.EntityCode, AccountSummary.AccountCode) @ code-description ~
get-balance( AccountSummary.EntityType, AccountSummary.EntityCode, AccountSummary.AccountCode) @ calc-balance 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table 
&Scoped-define QUERY-STRING-br_table FOR EACH AccountSummary WHERE ~{&KEY-PHRASE} NO-LOCK ~
    ~{&SORTBY-PHRASE}
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH AccountSummary WHERE ~{&KEY-PHRASE} NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table AccountSummary
&Scoped-define FIRST-TABLE-IN-QUERY-br_table AccountSummary


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
NoteCode||y|ttpl.AccountSummary.NoteCode
AccountSummary||y|ttpl.AccountSummary.AccountCode
AccountCode|y|y|ttpl.AccountSummary.AccountCode
TenantCode|y||ttpl.AccountSummary.EntityCode
PropertyCode|y||ttpl.AccountSummary.EntityCode
CompanyCode|y||ttpl.AccountSummary.EntityCode
AssetCode|y||ttpl.AccountSummary.EntityCode
ProjectCode|y||ttpl.AccountSummary.EntityCode
CreditorCode|y||ttpl.AccountSummary.EntityCode
EntityCode|y|y|ttpl.AccountSummary.EntityCode
EntityType||y|ttpl.AccountSummary.EntityType
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "AccountCode,TenantCode,PropertyCode,CompanyCode,AssetCode,ProjectCode,CreditorCode,EntityCode",
     Keys-Supplied = "NoteCode,AccountSummary,AccountCode,EntityCode,EntityType"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD balance-from-balances B-table-Win 
FUNCTION balance-from-balances RETURNS DECIMAL
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC, INPUT m-1 AS INT, INPUT m-n AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD balance-from-summary B-table-Win 
FUNCTION balance-from-summary RETURNS DECIMAL
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC, INPUT mnth AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-balance B-table-Win 
FUNCTION get-balance RETURNS DECIMAL
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-description B-table-Win 
FUNCTION get-description RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      AccountSummary SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      (IF key-name = 'AccountCode' THEN AccountSummary.EntityType + STRING( AccountSummary.EntityCode, '99999') ELSE STRING( AccountSummary.AccountCode, '9999.99') ) @ code-value COLUMN-LABEL "Account" FORMAT "X(8)":U
      get-description(AccountSummary.EntityType, AccountSummary.EntityCode, AccountSummary.AccountCode) @ code-description COLUMN-LABEL "Description" FORMAT "X(58)":U
      get-balance( AccountSummary.EntityType, AccountSummary.EntityCode, AccountSummary.AccountCode) @ calc-balance COLUMN-LABEL "Balance"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 64.57 BY 13.8
         BGCOLOR 16 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 17.4
         WIDTH              = 93.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main     = 1
       br_table:MAX-DATA-GUESS IN FRAME F-Main         = 2000.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.AccountSummary"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _TblOptList       = ", FIRST OUTER USED"
     _FldNameList[1]   > "_<CALC>"
"(IF key-name = 'AccountCode' THEN AccountSummary.EntityType + STRING( AccountSummary.EntityCode, '99999') ELSE STRING( AccountSummary.AccountCode, '9999.99') ) @ code-value" "Account" "X(8)" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > "_<CALC>"
"get-description(AccountSummary.EntityType, AccountSummary.EntityCode, AccountSummary.AccountCode) @ code-description" "Description" "X(58)" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[3]   > "_<CALC>"
"get-balance( AccountSummary.EntityType, AccountSummary.EntityCode, AccountSummary.AccountCode) @ calc-balance" "Balance" ? ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */


RUN set-attribute-list( 'Note-Height = 2.7':U ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'AccountCode':U THEN DO:
       &Scope KEY-PHRASE AccountSummary.AccountCode eq DECIMAL(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* AccountCode */
    WHEN 'TenantCode':U THEN DO:
       &Scope KEY-PHRASE AccountSummary.EntityCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* TenantCode */
    WHEN 'PropertyCode':U THEN DO:
       &Scope KEY-PHRASE AccountSummary.EntityCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* PropertyCode */
    WHEN 'CompanyCode':U THEN DO:
       &Scope KEY-PHRASE AccountSummary.EntityCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* CompanyCode */
    WHEN 'AssetCode':U THEN DO:
       &Scope KEY-PHRASE AccountSummary.EntityCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* AssetCode */
    WHEN 'ProjectCode':U THEN DO:
       &Scope KEY-PHRASE AccountSummary.EntityCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* ProjectCode */
    WHEN 'CreditorCode':U THEN DO:
       &Scope KEY-PHRASE AccountSummary.EntityCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* CreditorCode */
    WHEN 'EntityCode':U THEN DO:
       &Scope KEY-PHRASE AccountSummary.EntityCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* EntityCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query-cases B-table-Win 
PROCEDURE local-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR attribute-value AS CHAR NO-UNDO.
DEF VAR record-source-handle AS HANDLE NO-UNDO.
DEF VAR query-count AS INT NO-UNDO.

  /* Code placed here will execute PRIOR to standard behavior. */
  RUN get-link-handle IN adm-broker-hdl (THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT attribute-value) NO-ERROR.
  IF attribute-value = "":U THEN RETURN. /* There's no active record source */
  ASSIGN record-source-handle = WIDGET-HANDLE(ENTRY(1,attribute-value)).

  RUN get-attribute( 'Key-Value':U ).
  IF key-name = "AccountCode" THEN
    account-code = DECIMAL(RETURN-VALUE).
  ELSE
    entity-code = INT(RETURN-VALUE).

  CASE key-name:
    WHEN "EntityCode" THEN DO:
      RUN send-key IN record-source-handle( "EntityType", OUTPUT entity-type).
    END.
    WHEN "AccountCode"  THEN entity-type = "".
    WHEN "CompanyCode"  THEN entity-type = "L".
    WHEN "PropertyCode" THEN entity-type = "P".
    WHEN "ProjectCode"  THEN entity-type = "J".
    WHEN "AssetCode"    THEN entity-type = "F".
    WHEN "CreditorCode" THEN entity-type = "C".
    WHEN "TenantCode"   THEN entity-type = "T".
  END CASE.

  IF key-name = "AccountCode" THEN DO:
     &Scope KEY-PHRASE AccountSummary.AccountCode eq account-code
     {&OPEN-QUERY-{&BROWSE-NAME}}
  END.
  ELSE DO:
     &Scope KEY-PHRASE AccountSummary.EntityType eq entity-type AND AccountSummary.EntityCode eq entity-code 
     {&OPEN-QUERY-{&BROWSE-NAME}}
  END.

  /* don't Dispatch standard ADM method.                             
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query-cases':U ) . */

  /* Code placed here will execute AFTER standard behavior.    */

  /* Count the rows and set the scroll bar */
  GET FIRST {&BROWSE-NAME}.
  DO WHILE AVAILABLE({&FIRST-TABLE-IN-QUERY-br_table}):
    query-count = query-count + 1.
    GET NEXT {&BROWSE-NAME}.
  END.
  ASSIGN {&BROWSE-NAME}:MAX-DATA-GUESS IN FRAME F-Main = query-count.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE normal-send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE normal-send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "NoteCode" "AccountSummary" "NoteCode"}
  {src/adm/template/sndkycas.i "AccountSummary" "AccountSummary" "AccountCode"}
  {src/adm/template/sndkycas.i "AccountCode" "AccountSummary" "AccountCode"}
  {src/adm/template/sndkycas.i "EntityCode" "AccountSummary" "EntityCode"}
  {src/adm/template/sndkycas.i "EntityType" "AccountSummary" "EntityType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:  Spoof the normal send key routine
------------------------------------------------------------------------------*/
  {inc/sendkey.i}
  IF key-name = "AccountSummary" AND AVAILABLE(AccountSummary) THEN
    key-value = AccountSummary.EntityType + "/"
              + STRING(AccountSummary.EntityCode) + "/"
              + STRING(AccountSummary.AccountCode).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "AccountSummary"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-name AS CHAR NO-UNDO.
  key-name = new-name.

DO WITH FRAME {&FRAME-NAME}:
  IF key-name = "AccountCode" THEN ASSIGN
    code-value:LABEL IN BROWSE {&BROWSE-NAME} = ' Entity'.
  ELSE
    code-value:LABEL IN BROWSE {&BROWSE-NAME} = 'Account'.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION balance-from-balances B-table-Win 
FUNCTION balance-from-balances RETURNS DECIMAL
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC, INPUT m-1 AS INT, INPUT m-n AS INT ) :
/*------------------------------------------------------------------------------
  Purpose: Calculate balance at a particular date by adding a range of months
    Notes: Applies to I&E (P&L) accounts
------------------------------------------------------------------------------*/
DEF VAR bal AS DEC NO-UNDO INITIAL 0.00 .

  FOR EACH AccountBalance WHERE AccountBalance.EntityType = et
                AND AccountBalance.EntityCode = ec
                AND AccountBalance.AccountCode = ac
                AND AccountBalance.MonthCode >= m-1
                AND AccountBalance.MonthCode <= m-n NO-LOCK:
    bal = bal + AccountBalance.Balance.
  END.

  RETURN bal.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION balance-from-summary B-table-Win 
FUNCTION balance-from-summary RETURNS DECIMAL
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC, INPUT mnth AS INT ) :
/*------------------------------------------------------------------------------
  Purpose: Calculate balance at a particular date by subtracting backwards
    Notes: Applies to balance sheet accounts
------------------------------------------------------------------------------*/
DEF VAR bal AS DEC NO-UNDO INITIAL 0.00 .

DEF BUFFER MySummary FOR AccountSummary.
  FIND MySummary WHERE MySummary.EntityType = et
                AND MySummary.EntityCode = ec
                AND MySummary.AccountCode = ac NO-LOCK NO-ERROR.
  IF AVAILABLE(MySummary) THEN bal = MySummary.Balance.

  FOR EACH AccountBalance WHERE AccountBalance.EntityType = et
                AND AccountBalance.EntityCode = ec
                AND AccountBalance.AccountCode = ac
                AND AccountBalance.MonthCode > mnth NO-LOCK:
    bal = bal - AccountBalance.Balance.
  END.

  RETURN bal.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-balance B-table-Win 
FUNCTION get-balance RETURNS DECIMAL
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF BUFFER MyChart FOR ChartOfAccount.

DEF VAR type AS CHAR INITIAL "B" NO-UNDO.

  FIND MyChart WHERE MyChart.AccountCode = ac NO-LOCK NO-ERROR.
  IF AVAILABLE(MyChart) THEN FIND AccountGroup OF MyChart NO-LOCK NO-ERROR.
  IF AVAILABLE(AccountGroup) THEN type = AccountGroup.GroupType.

  IF type = "B" THEN
    RETURN balance-from-summary( et, ec, ac, month-n ).
  ELSE
    RETURN balance-from-balances( et, ec, ac, month-1, month-n ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-description B-table-Win 
FUNCTION get-description RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Return the appropriate code description
    Notes:  
------------------------------------------------------------------------------*/

  /* displayed description for each record depending on key type / entity type */
  IF key-name = 'AccountCode' THEN DO:
    CASE et:
      WHEN 'L' THEN DO:
        FIND Company NO-LOCK WHERE Company.CompanyCode = ec NO-ERROR.
        RETURN IF AVAILABLE(Company) THEN Company.LegalName ELSE 'Company not on file!'.
      END.
      WHEN 'P' THEN DO:
        FIND Property NO-LOCK WHERE Property.PropertyCode = ec NO-ERROR.
        RETURN IF AVAILABLE(Property) THEN Property.Name ELSE 'Property not on file!'.
      END.
      WHEN 'J' THEN DO:
        FIND Project NO-LOCK WHERE Project.ProjectCode = ec NO-ERROR.
        RETURN IF AVAILABLE(Project) THEN Project.Name ELSE 'Project not on file!'.
      END.
      WHEN 'C' THEN DO:
        FIND Creditor NO-LOCK WHERE Creditor.CreditorCode = ec NO-ERROR.
        RETURN IF AVAILABLE(Creditor) THEN Creditor.Name ELSE 'Creditor not on file!'.
      END.
      WHEN 'T' THEN DO:
        FIND Tenant NO-LOCK WHERE Tenant.TenantCode = ec NO-ERROR.
        RETURN IF AVAILABLE(Tenant) THEN Tenant.Name ELSE 'Tenant not on file!'.
      END.
      WHEN 'F' THEN DO:
        FIND FixedAsset NO-LOCK WHERE FixedAsset.AssetCode = ec NO-ERROR.
        RETURN IF AVAILABLE(FixedAsset) THEN FixedAsset.Description ELSE 'Fixed Asset not on file!'.
      END.
    END.
  END.
  ELSE IF multi-ledger-creditors AND key-name = 'CreditorCode' THEN DO:
    FIND FIRST Company WHERE Company.CompanyCode = INT(ac) NO-LOCK NO-ERROR.
    RETURN IF AVAILABLE(Company) THEN Company.LegalName ELSE 'Company not on file!'.
  END.
  ELSE DO:
    FIND FIRST ChartOfAccount WHERE ChartOfAccount.AccountCode = ac NO-LOCK NO-ERROR.
    RETURN IF AVAILABLE(ChartOfAccount) THEN ChartOfAccount.Name ELSE 'Account not in chart of accounts!'.
  END.


END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

