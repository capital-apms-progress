&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR group-list AS CHAR INITIAL "All Account Groups" NO-UNDO.
DEF VAR filter-by AS CHAR NO-UNDO.
DEF VAR sort-by AS CHAR NO-UNDO.
DEF VAR all-records AS LOGICAL NO-UNDO.
DEF VAR search-by AS CHAR NO-UNDO.

{inc/topic/tpchoact.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ChartOfAccount AccountGroup

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table ChartOfAccount.AccountCode ~
ChartOfAccount.Name ChartOfAccount.AccountGroupCode ~
ChartOfAccount.HighVolume ChartOfAccount.UpdateTo ~
ChartOfAccount.ExpenseRecoveryType ChartOfAccount.AlternativeCode ~
ChartOfAccount.ShortName 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table ~
ChartOfAccount.AccountGroupCode 
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table ChartOfAccount
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table ChartOfAccount
&Scoped-define QUERY-STRING-br_table FOR EACH ChartOfAccount WHERE ~{&KEY-PHRASE} ~
      AND ChartOfAccount.Name MATCHES search-by ~
 AND (filter-by = "" OR ChartOfAccount.AccountGroupCode = filter-by) ~
 AND (all-records OR ChartOfAccount.UpdateTo <> "") NO-LOCK, ~
      EACH AccountGroup OF ChartOfAccount NO-LOCK ~
    ~{&SORTBY-PHRASE}
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH ChartOfAccount WHERE ~{&KEY-PHRASE} ~
      AND ChartOfAccount.Name MATCHES search-by ~
 AND (filter-by = "" OR ChartOfAccount.AccountGroupCode = filter-by) ~
 AND (all-records OR ChartOfAccount.UpdateTo <> "") NO-LOCK, ~
      EACH AccountGroup OF ChartOfAccount NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table ChartOfAccount AccountGroup
&Scoped-define FIRST-TABLE-IN-QUERY-br_table ChartOfAccount
&Scoped-define SECOND-TABLE-IN-QUERY-br_table AccountGroup


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
AccountGroupCode|y|y|ttpl.ChartOfAccount.AccountGroupCode
AuditRecordId||y|ttpl.ChartOfAccount.AuditRecordId
AccountCode||y|ttpl.ChartOfAccount.AccountCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "AccountGroupCode",
     Keys-Supplied = "AccountGroupCode,AuditRecordId,AccountCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Group|y||ttpl.AccountGroup.SequenceCode|yes,ttpl.ChartOfAccount.AccountCode|yes
Code|||ttpl.ChartOfAccount.AccountCode|yes
Name|||ttpl.ChartOfAccount.Name|yes
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "':U + 'Group,Code,Name' + '",
     SortBy-Case = ':U + 'Group').

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      ChartOfAccount, 
      AccountGroup SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      ChartOfAccount.AccountCode FORMAT "9999.99":U
      ChartOfAccount.Name FORMAT "X(65)":U WIDTH 49.86
      ChartOfAccount.AccountGroupCode FORMAT "X(9)":U WIDTH 6.43
      ChartOfAccount.HighVolume COLUMN-LABEL "High" FORMAT "yes/no":U
      ChartOfAccount.UpdateTo COLUMN-LABEL "Updt" FORMAT "X(4)":U
      ChartOfAccount.ExpenseRecoveryType COLUMN-LABEL "Exp" FORMAT "X":U
      ChartOfAccount.AlternativeCode COLUMN-LABEL "Description" FORMAT "X(100)":U
      ChartOfAccount.ShortName FORMAT "X(10)":U
  ENABLE
      ChartOfAccount.AccountGroupCode
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 88.57 BY 20.2
         BGCOLOR 16 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 21.1
         WIDTH              = 93.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main     = 2
       br_table:MAX-DATA-GUESS IN FRAME F-Main         = 2000.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "ttpl.ChartOfAccount,ttpl.AccountGroup OF ttpl.ChartOfAccount"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _Where[1]         = "ChartOfAccount.Name MATCHES search-by
 AND (filter-by = """" OR ChartOfAccount.AccountGroupCode = filter-by)
 AND (all-records OR ChartOfAccount.UpdateTo <> """")"
     _FldNameList[1]   = ttpl.ChartOfAccount.AccountCode
     _FldNameList[2]   > ttpl.ChartOfAccount.Name
"ChartOfAccount.Name" ? "X(65)" "character" ? ? ? ? ? ? no ? no no "49.86" yes no no "U" "" ""
     _FldNameList[3]   > ttpl.ChartOfAccount.AccountGroupCode
"ChartOfAccount.AccountGroupCode" ? "X(9)" "character" ? ? ? ? ? ? yes ? no no "6.43" yes no no "U" "" ""
     _FldNameList[4]   > ttpl.ChartOfAccount.HighVolume
"ChartOfAccount.HighVolume" "High" ? "logical" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[5]   > ttpl.ChartOfAccount.UpdateTo
"ChartOfAccount.UpdateTo" "Updt" "X(4)" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[6]   > TTPL.ChartOfAccount.ExpenseRecoveryType
"ChartOfAccount.ExpenseRecoveryType" "Exp" ? "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[7]   > ttpl.ChartOfAccount.AlternativeCode
"ChartOfAccount.AlternativeCode" "Description" "X(100)" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[8]   = ttpl.ChartOfAccount.ShortName
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list( 'SortBy-Options = Group|Name|Code, SortBy-Case = Code':U ).
RUN set-attribute-list( 'Active-Options = Active|All, Active-Case = Active':U ).
RUN set-attribute-list( 'SearchBy-Case = ' ).

FOR EACH AccountGroup BY AccountGroup.SequenceCode:
  group-list = group-list + "|" + 
      STRING( AccountGroup.AccountGroupCode, "X(7)") + "- " + AccountGroup.Name.
END.
RUN set-attribute-list( 'FilterBy-Case = All Account Groups, FilterBy-Style = Combo-Box':U ).
RUN set-attribute-list( 'FilterBy-Options = ':U + group-list ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'AccountGroupCode':U THEN DO:
       &Scope KEY-PHRASE ChartOfAccount.AccountGroupCode eq key-value
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Group':U THEN DO:
           &Scope SORTBY-PHRASE BY AccountGroup.SequenceCode BY ChartOfAccount.AccountCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Code':U THEN DO:
           &Scope SORTBY-PHRASE BY ChartOfAccount.AccountCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Name':U THEN DO:
           &Scope SORTBY-PHRASE BY ChartOfAccount.Name
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* AccountGroupCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Group':U THEN DO:
           &Scope SORTBY-PHRASE BY AccountGroup.SequenceCode BY ChartOfAccount.AccountCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Code':U THEN DO:
           &Scope SORTBY-PHRASE BY ChartOfAccount.AccountCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Name':U THEN DO:
           &Scope SORTBY-PHRASE BY ChartOfAccount.Name
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize B-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  ChartOfAccount.AccountGroupCode:READ-ONLY IN BROWSE {&BROWSE-NAME} = YES.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  DO WITH FRAME {&FRAME-NAME}:
    IF NOT( AVAILABLE(ChartOfAccount)) THEN RETURN.
    FIND FIRST AcctTran OF ChartOfAccount NO-LOCK NO-ERROR.
    IF AVAILABLE(AcctTran) THEN DO:
      MESSAGE 'Deletion is not possible because transactions reference this account.' VIEW-AS ALERT-BOX ERROR.
      RETURN.
    END.
    FIND FIRST NewAcctTran OF ChartOfAccount NO-LOCK NO-ERROR.
    IF AVAILABLE(NewAcctTran) THEN DO:
      MESSAGE 'Deletion is not possible because unposted transactions reference this account.' VIEW-AS ALERT-BOX WARNING.
      RETURN.
    END.
  END.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "AccountGroupCode" "ChartOfAccount" "AccountGroupCode"}
  {src/adm/template/sndkycas.i "AuditRecordId" "ChartOfAccount" "AuditRecordId"}
  {src/adm/template/sndkycas.i "AccountCode" "ChartOfAccount" "AccountCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ChartOfAccount"}
  {src/adm/template/snd-list.i "AccountGroup"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Active-Case B-table-Win 
PROCEDURE use-Active-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-active AS CHAR NO-UNDO.
  all-records = (new-active = "All":U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-filter AS CHAR NO-UNDO.
  filter-by = ENTRY( 1, new-filter, ' ').
  IF filter-by = 'All' THEN filter-by = ''.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SearchBy-Case B-table-Win 
PROCEDURE use-SearchBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-val AS CHAR NO-UNDO.

  search-by = TRIM(new-val) + "*".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

