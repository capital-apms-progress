&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r11 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:         v-totals.w
  Description:  Viewer for totals of account ranges
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Local Variable Definitions ---                                       */

DEF VAR first-time-through AS LOGICAL INITIAL yes NO-UNDO.
DEF VAR group-list AS CHAR NO-UNDO.
DEF VAR source-link-name AS CHAR NO-UNDO.
DEF VAR source-table AS CHAR NO-UNDO.
DEF VAR sundry-debtors LIKE ChartOfAccount.AccountCode NO-UNDO.
DEF VAR sundry-creditors LIKE ChartOfAccount.AccountCode NO-UNDO.

/*
 * We don't want to use the external buffers because we might depend on
 * them remaining the same, so we define substitutes.
 */
DEF BUFFER Account FOR AccountSummary.
DEF BUFFER Chart   FOR ChartOfAccount.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES AccountSummary AcctTran
&Scoped-define FIRST-EXTERNAL-TABLE AccountSummary


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR AccountSummary, AcctTran.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-3 combo-entity-type combo-group-type ~
RECT-1 fill-entity-1 fill-account-1 fill-entity-2 fill-account-2 ~
btn-calculate combo-select-group RECT-2 
&Scoped-Define DISPLAYED-OBJECTS combo-entity-type combo-group-type ~
fill-entity-1 fill-account-1 fill-entity-2 fill-account-2 ~
combo-select-group fill-total-balance fill-total-budget fill-total-revised ~
fill-budget-variance fill-revised-variance 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn-calculate AUTO-GO DEFAULT 
     LABEL "Calculate" 
     SIZE 17.72 BY 2.2
     BGCOLOR 8 FONT 13.

DEFINE VARIABLE combo-entity-type AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "T - Tenant","C - Creditor","L - Ledger","P - Property","J - Project" 
     SIZE 15.57 BY 1
     FONT 8 NO-UNDO.

DEFINE VARIABLE combo-group-type AS CHARACTER FORMAT "X(256)":U 
     LABEL "Group by" 
     VIEW-AS COMBO-BOX INNER-LINES 7
     LIST-ITEMS "EA - Entity + Account range","E  - Entity range","A  - Account range","G  - Account Group range" 
     SIZE 38.86 BY 1 NO-UNDO.

DEFINE VARIABLE combo-select-group AS CHARACTER FORMAT "X(256)":U 
     LABEL "Group" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEMS "PROPEX - Property Expenses" 
     SIZE 54.86 BY 1
     FONT 8 NO-UNDO.

DEFINE VARIABLE fill-account-1 AS DECIMAL FORMAT "9999.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 8.57 BY 1 NO-UNDO.

DEFINE VARIABLE fill-account-2 AS DECIMAL FORMAT "9999.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 8.57 BY 1 NO-UNDO.

DEFINE VARIABLE fill-budget-variance AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Variance" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fill-entity-1 AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "From" 
     VIEW-AS FILL-IN 
     SIZE 6.29 BY 1 NO-UNDO.

DEFINE VARIABLE fill-entity-2 AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 6.29 BY 1 NO-UNDO.

DEFINE VARIABLE fill-revised-variance AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Variance" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fill-total-balance AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Balance" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fill-total-budget AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Budget" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fill-total-revised AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Revised" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 81.72 BY 2.6.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 81.72 BY 2.4.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 81.72 BY 1.4.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     combo-entity-type AT ROW 1.2 COL 13.29 COLON-ALIGNED
     combo-group-type AT ROW 1.2 COL 41.29 COLON-ALIGNED
     fill-entity-1 AT ROW 3 COL 7 COLON-ALIGNED
     fill-account-1 AT ROW 3 COL 16.14 COLON-ALIGNED NO-LABEL
     fill-entity-2 AT ROW 3 COL 44.14 COLON-ALIGNED
     fill-account-2 AT ROW 3 COL 53.29 COLON-ALIGNED NO-LABEL
     btn-calculate AT ROW 3 COL 64.43
     combo-select-group AT ROW 4.2 COL 7 COLON-ALIGNED
     fill-total-balance AT ROW 6 COL 8.72 COLON-ALIGNED
     fill-total-budget AT ROW 6 COL 35.57 COLON-ALIGNED
     fill-total-revised AT ROW 6 COL 63 COLON-ALIGNED
     fill-budget-variance AT ROW 7 COL 27.57
     fill-revised-variance AT ROW 7 COL 55
     RECT-3 AT ROW 1 COL 1
     RECT-1 AT ROW 2.8 COL 1
     RECT-2 AT ROW 5.8 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         DEFAULT-BUTTON btn-calculate.

 

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.AccountSummary,ttpl.AcctTran
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 8
         WIDTH              = 83.43.
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Default                                      */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fill-budget-variance IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN fill-revised-variance IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN fill-total-balance IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fill-total-budget IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fill-total-revised IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn-calculate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-calculate V-table-Win
ON CHOOSE OF btn-calculate IN FRAME F-Main /* Calculate */
DO:
  RUN calculate-totals.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME combo-entity-type
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL combo-entity-type V-table-Win
ON VALUE-CHANGED OF combo-entity-type IN FRAME F-Main /* Entity type */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME combo-group-type
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL combo-group-type V-table-Win
ON VALUE-CHANGED OF combo-group-type IN FRAME F-Main /* Group by */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
/************************ INTERNAL PROCEDURES ********************/

ASSIGN
  combo-group-type = 'EA - Entity + Account range'
  combo-entity-type = 'T - Tenant'
  fill-entity-1 = 0
  fill-account-1 = 0.0
  fill-entity-2 = 0
  fill-account-2 = 0.0
.

FIND Office WHERE Office.ThisOffice = yes NO-LOCK.
FIND OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "CREDITORS" NO-LOCK.
sundry-creditors = OfficeControlAccount.AccountCode.
FIND OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "DEBTORS" NO-LOCK.
sundry-debtors = OfficeControlAccount.AccountCode.

FOR EACH AccountGroup NO-LOCK BY AccountGroup.SequenceCode:
  group-list = group-list + '|'
             + STRING(AccountGroup.AccountGroupCode,"X(7)") + '- '
             + AccountGroup.Name.
  IF AccountGroup.AccountGroupCode = 'PROPEX' THEN
    combo-select-group = STRING(AccountGroup.AccountGroupCode,"X(7)") + '- '
             + AccountGroup.Name.
END.
group-list = SUBSTRING( group-list, 2).
ASSIGN
  combo-select-group:DELIMITER = '|'
  combo-select-group:LIST-ITEMS = group-list
.

DO WITH FRAME {&FRAME-NAME}:
  DISPLAY combo-entity-type combo-group-type combo-select-group
          fill-entity-1 fill-entity-2 fill-account-1 fill-account-2.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-to-totals V-table-Win 
PROCEDURE add-to-totals :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  ASSIGN
    fill-total-balance = fill-total-balance + Account.Balance
    fill-total-budget = fill-total-budget + Account.Budget
    fill-total-revised = fill-total-revised + Account.RevisedBudget
  .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win 
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the record-
               source has a new row available.  This procedure
               tries to get the new row and display it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  /* Note differences in use of " and '                              */
  IF {inc/totals.i "list" "Tenant"}
  ELSE IF {inc/totals.i "list" "Creditor"}
  ELSE IF {inc/totals.i "list" "Property"}
  ELSE IF {inc/totals.i "list" "Company"}
  ELSE IF {inc/totals.i "list" "Project"}
  ELSE IF {inc/totals.i "list" "ChartOfAccount"}
  ELSE IF {inc/totals.i "list" "AccountSummary"}
  ELSE IF {inc/totals.i "list" "AccountBalance"}
  ELSE IF {inc/totals.i "list" "AcctTran"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  /* Note differences in use of " and '                              */
  IF {inc/totals.i "find" "Tenant"}
  ELSE IF {inc/totals.i "find" "Creditor"}
  ELSE IF {inc/totals.i "find" "Property"}
  ELSE IF {inc/totals.i "find" "Company"}
  ELSE IF {inc/totals.i "find" "Project"}
  ELSE IF {inc/totals.i "find" "ChartOfAccount"}
  ELSE IF {inc/totals.i "find" "AccountSummary"}
  ELSE IF {inc/totals.i "find" "AccountBalance"}
  ELSE IF {inc/totals.i "find" "AcctTran"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculate-totals V-table-Win 
PROCEDURE calculate-totals :
/*------------------------------------------------------------------------------
  Purpose:  Re-calculate the totals of Balance, Budget, Revised and variances
------------------------------------------------------------------------------*/
DEF VAR etype AS CHAR NO-UNDO.
DEF VAR gtype AS CHAR NO-UNDO.
DEF VAR ec1 AS INT NO-UNDO.
DEF VAR ec2 AS INT NO-UNDO.
DEF VAR ac1 AS DEC NO-UNDO.
DEF VAR ac2 AS DEC NO-UNDO.

  DO WITH FRAME {&FRAME-NAME}:
    DISABLE btn-calculate.
    RUN notify('set-busy,container-source').
    ASSIGN
      etype = SUBSTRING( INPUT combo-entity-type, 1, 1)
      gtype = SUBSTRING( INPUT combo-group-type, 1, 2)
      ec1 = INPUT fill-entity-1
      ec2 = INPUT fill-entity-2
      ac1 = INPUT fill-account-1
      ac2 = INPUT fill-account-2
      fill-total-balance = 0
      fill-total-budget = 0
      fill-total-revised = 0
    .

    IF ec2 = 0 THEN ec2 = ec1.
    IF etype = 'T' THEN         ac1 = sundry-debtors.
    ELSE IF etype = 'C' THEN    ac1 = sundry-creditors.
  END.

  IF etype = 'T' OR etype = 'C' THEN DO:
    ac2 = ac1.
    FOR EACH Account WHERE Account.EntityType = etype
                       AND Account.EntityCode >= ec1
                       AND Account.EntityCode <= ec2
                       AND Account.AccountCode >= ac1
                       AND Account.AccountCode <= ac2
                       NO-LOCK:
      RUN add-to-totals.
    END.
  END.
  ELSE DO:
    IF ac2 = 0 THEN ac2 = ac1.
    IF gtype = 'G' THEN DO:
      gtype = TRIM( SUBSTRING( INPUT combo-select-group, 1, 6) ).
      FOR EACH Chart WHERE Chart.AccountGroupCode = gtype NO-LOCK:
        FOR EACH Account OF Chart NO-LOCK WHERE Account.EntityType = etype
                       AND Account.EntityCode >= ec1
                       AND Account.EntityCode <= ec2.
          RUN add-to-totals.
        END.
      END.
    END.
    ELSE DO:
      CASE gtype:
        WHEN 'E' THEN ASSIGN
          ac1 = 0
          ac2 = 9999.99
        .
        WHEN 'A' THEN ASSIGN
          ec1 = 0
          ec2 = 99999
        .
      END CASE.
      FOR EACH Account WHERE Account.EntityType = etype
                         AND Account.EntityCode >= ec1
                         AND Account.EntityCode <= ec2
                         AND Account.AccountCode >= ac1
                         AND Account.AccountCode <= ac2
                         NO-LOCK:
        RUN add-to-totals.
      END.
    END.
  END.

  ASSIGN
    fill-budget-variance = fill-total-balance - fill-total-budget
    fill-revised-variance = fill-total-balance - fill-total-revised
  .

  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN
      fill-total-balance:FGCOLOR =    ( IF fill-total-balance < 0    THEN 12 ELSE 1 )
      fill-total-budget:FGCOLOR =     ( IF fill-total-budget < 0     THEN 12 ELSE 1 )
      fill-total-revised:FGCOLOR =    ( IF fill-total-revised < 0    THEN 12 ELSE 1 )
      fill-budget-variance:FGCOLOR =  ( IF fill-budget-variance < 0  THEN 12 ELSE 1 )
      fill-revised-variance:FGCOLOR = ( IF fill-revised-variance < 0 THEN 12 ELSE 1 )
    .
    DISPLAY fill-total-balance fill-total-budget fill-total-revised
            fill-budget-variance fill-revised-variance.

    RUN notify('set-idle,container-source').
    ENABLE btn-calculate.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:  Enable whatever fields are appropriate given the current
            state of combo-entity-type and combo-group-type combo boxes.
------------------------------------------------------------------------------*/
  CASE SUBSTRING( INPUT FRAME {&FRAME-NAME} combo-entity-type, 1, 1):
    WHEN 'T' OR WHEN 'C' THEN DO WITH FRAME {&FRAME-NAME}:
      DISABLE ALL EXCEPT combo-entity-type fill-entity-1 fill-entity-2.
      ENABLE combo-entity-type fill-entity-1 fill-entity-2.
    END.
    WHEN 'L' OR WHEN 'P' OR WHEN 'J' THEN DO WITH FRAME {&FRAME-NAME}:
      DISABLE ALL EXCEPT combo-entity-type combo-group-type.
      ENABLE combo-entity-type combo-group-type.
      CASE TRIM( SUBSTRING( INPUT FRAME {&FRAME-NAME} combo-group-type, 1, 2 ) ):
        WHEN 'EA' THEN ENABLE fill-entity-1 fill-entity-2 fill-account-1 fill-account-2 WITH FRAME {&FRAME-NAME}.
        WHEN 'E' THEN  ENABLE fill-entity-1 fill-entity-2 WITH FRAME {&FRAME-NAME}.
        WHEN 'A' THEN  ENABLE fill-account-1 fill-account-2 WITH FRAME {&FRAME-NAME}.
        WHEN 'G' THEN  ENABLE combo-select-group fill-entity-1 fill-entity-2 WITH FRAME {&FRAME-NAME}.
      END CASE.
    END.
  END CASE.
  ENABLE btn-calculate WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE post-row-available V-table-Win 
PROCEDURE post-row-available :
/*------------------------------------------------------------------------------
  Purpose:  If there is a new row available then we re-total
------------------------------------------------------------------------------*/
DEF VAR et AS CHAR NO-UNDO.
DEF VAR gtype AS CHAR NO-UNDO.
DEF VAR ec AS INT  INITIAL 0 NO-UNDO.
DEF VAR ac AS DEC  INITIAL 0 NO-UNDO.

  IF first-time-through THEN DO:
    IF source-table = "Tenant" AND AVAILABLE(Tenant) THEN DO:
      first-time-through = no.
      ASSIGN    et = "T"         ec = Tenant.TenantCode .
    END.
    ELSE IF source-table = "Creditor" AND AVAILABLE(Creditor) THEN DO:
      first-time-through = no.
      ASSIGN    et = "C"         ec = Creditor.CreditorCode .
    END.
    ELSE IF source-table = "Property" AND AVAILABLE(Property) THEN DO:
      first-time-through = no.
      ASSIGN    et = "P"    gtype = "PROPEX"     ec = Property.PropertyCode .
    END.
    ELSE IF source-table = "Project" AND AVAILABLE(Project) THEN DO:
      first-time-through = no.
      ASSIGN    et = "J"         ec = Project.ProjectCode .
    END.
    ELSE IF source-table = "Company" AND AVAILABLE(Company) THEN DO:
      first-time-through = no.
      ASSIGN    et = "L"         ec = Company.CompanyCode .
    END.
    ELSE IF source-table = "ChartOfAccount" AND AVAILABLE(ChartOfAccount) THEN DO:
      first-time-through = no.
      ASSIGN    et = "L"         ac = ChartOfAccount.AccountCode .
    END.
    ELSE IF source-table = "AcctTran" AND AVAILABLE(AcctTran) THEN DO:
      first-time-through = no.
      ASSIGN
        et = AcctTran.EntityType
        ec = AcctTran.EntityCode
        ac = AcctTran.AccountCode
      .
    END.
    ELSE IF source-table = "AccountSummary" AND AVAILABLE(AccountSummary) THEN DO:
      first-time-through = no.
      ASSIGN
        et = AccountSummary.EntityType
        ec = AccountSummary.EntityCode
        ac = AccountSummary.AccountCode
      .
    END.

    IF NOT first-time-through THEN DO:
      /* i.e. a record was available */
      IF gtype <> "" THEN DO:
        FIND AccountGroup WHERE AccountGroup.AccountGroupCode = gtype NO-LOCK.
        ASSIGN
          combo-group-type = 'G  - Account Group range'
          combo-select-group = STRING(AccountGroup.AccountGroupCode,"X(7)") + '- '
                             + AccountGroup.Name
        .
      END.
      ELSE IF source-table = "AccountSummary" OR source-table = "AcctTran" OR source-table = "AccountBalance" THEN
          combo-group-type = 'EA - Entity + Account range'.
      ELSE IF source-table = "ChartOfAccount" THEN
          combo-group-type = 'A  - Account range'.
      ELSE
          combo-group-type = 'E  - Entity range'.

      CASE et:
        WHEN 'T' THEN combo-entity-type = 'T - Tenant'.
        WHEN 'C' THEN combo-entity-type = 'C - Creditor'.
        WHEN 'P' THEN combo-entity-type = 'P - Property'.
        WHEN 'J' THEN combo-entity-type = 'J - Project'.
        WHEN 'L' THEN combo-entity-type = 'L - Ledger'.
      END CASE.
      ASSIGN
        fill-entity-1 = ec
        fill-account-1 = ac
        fill-entity-2 = 0
        fill-account-2 = 0.0
      .

      DISPLAY combo-entity-type combo-group-type combo-select-group
                    fill-entity-1 fill-entity-2
                    fill-account-1 fill-account-2
                    WITH FRAME {&FRAME-NAME}.
      RUN enable-appropriate-fields.
      RUN calculate-totals.
    END.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Figure out which table we're using a row-available from
------------------------------------------------------------------------------*/

  RUN get-link-handle IN adm-broker-hdl ( INPUT THIS-PROCEDURE, 'record-source':U,
                                           OUTPUT source-link-name ).
  RUN get-attribute IN WIDGET-HANDLE(source-link-name) ('internal-tables':U ).
  source-table = RETURN-VALUE.

  IF INDEX( source-table, "Tenant" ) > 0 THEN          source-table = "Tenant".
  ELSE IF INDEX( source-table, "Creditor" ) > 0 THEN        source-table = "Creditor".
  ELSE IF INDEX( source-table, "Property" ) > 0 THEN        source-table = "Property".
  ELSE IF INDEX( source-table, "Project" ) > 0 THEN         source-table = "Project".
  ELSE IF INDEX( source-table, "Company" ) > 0 THEN         source-table = "Company".
  ELSE IF INDEX( source-table, "ChartOfAccount" ) > 0 THEN  source-table = "ChartOfAccount".
  ELSE IF INDEX( source-table, "AccountSummary" ) > 0 THEN       source-table = "AccountSummary".
  ELSE IF INDEX( source-table, "AccountBalance" ) > 0 THEN  source-table = "AccountBalance".
  ELSE IF INDEX( source-table, "AcctTran" ) > 0 THEN        source-table = "AcctTran".


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "AccountSummary"}
  {src/adm/template/snd-list.i "AcctTran"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


