&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR record-changed AS LOGICAL NO-UNDO  INITIAL No.

DEF VAR fields-disabled AS LOGI INITIAL No NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES TenancyLease
&Scoped-define FIRST-EXTERNAL-TABLE TenancyLease


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR TenancyLease.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES TenancyOutgoing

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table TenancyOutgoing.AccountCode ~
TenancyOutgoing.Percentage TenancyOutgoing.FixedAmount ~
TenancyOutgoing.BaseYear TenancyOutgoing.BaseYearAmount ~
TenancyOutgoing.ReconciliationDue TenancyOutgoing.OutgoingBasis 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table TenancyOutgoing.AccountCode ~
TenancyOutgoing.Percentage TenancyOutgoing.FixedAmount ~
TenancyOutgoing.BaseYear TenancyOutgoing.BaseYearAmount ~
TenancyOutgoing.ReconciliationDue TenancyOutgoing.OutgoingBasis 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}AccountCode ~{&FP2}AccountCode ~{&FP3}~
 ~{&FP1}Percentage ~{&FP2}Percentage ~{&FP3}~
 ~{&FP1}FixedAmount ~{&FP2}FixedAmount ~{&FP3}~
 ~{&FP1}BaseYear ~{&FP2}BaseYear ~{&FP3}~
 ~{&FP1}BaseYearAmount ~{&FP2}BaseYearAmount ~{&FP3}~
 ~{&FP1}ReconciliationDue ~{&FP2}ReconciliationDue ~{&FP3}~
 ~{&FP1}OutgoingBasis ~{&FP2}OutgoingBasis ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table TenancyOutgoing
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table TenancyOutgoing
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH TenancyOutgoing OF TenancyLease NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br_table TenancyOutgoing
&Scoped-define FIRST-TABLE-IN-QUERY-br_table TenancyOutgoing


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table btn_add btn_delete 
&Scoped-Define DISPLAYED-OBJECTS fil_Account fil_Total 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "Add" 
     SIZE 6.86 BY 1.05
     FONT 9.

DEFINE BUTTON btn_delete 
     LABEL "Delete" 
     SIZE 6.29 BY 1.05
     FONT 9.

DEFINE VARIABLE fil_Account AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 40.57 BY 1
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Total AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 10.14 BY 1.05 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      TenancyOutgoing SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      TenancyOutgoing.AccountCode COLUMN-LABEL "Account #"
      TenancyOutgoing.Percentage COLUMN-LABEL "Percent" FORMAT "->>9.99"
      TenancyOutgoing.FixedAmount COLUMN-LABEL "Fixed Amount"
      TenancyOutgoing.BaseYear COLUMN-LABEL "O/G Base Year"
      TenancyOutgoing.BaseYearAmount COLUMN-LABEL "Base Year Amount"
      TenancyOutgoing.ReconciliationDue COLUMN-LABEL "Next Reconl."
      TenancyOutgoing.OutgoingBasis COLUMN-LABEL "Basis"
  ENABLE
      TenancyOutgoing.AccountCode
      TenancyOutgoing.Percentage
      TenancyOutgoing.FixedAmount
      TenancyOutgoing.BaseYear
      TenancyOutgoing.BaseYearAmount
      TenancyOutgoing.ReconciliationDue
      TenancyOutgoing.OutgoingBasis
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 74.86 BY 9.4
         BGCOLOR 15 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
     fil_Account AT ROW 10.4 COL 7.29 NO-LABEL
     fil_Total AT ROW 10.4 COL 50.43 COLON-ALIGNED
     btn_add AT ROW 10.4 COL 62.72
     btn_delete AT ROW 10.4 COL 69.57
     "Account:" VIEW-AS TEXT
          SIZE 6.29 BY 1 AT ROW 10.4 COL 1
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   External Tables: TTPL.TenancyLease
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 11.1
         WIDTH              = 97.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fil_Account IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN fil_Total IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "ttpl.TenancyOutgoing OF ttpl.TenancyLease"
     _Options          = "NO-LOCK"
     _FldNameList[1]   > TTPL.TenancyOutgoing.AccountCode
"TenancyOutgoing.AccountCode" "Account #" ? "decimal" ? ? ? ? ? ? yes ?
     _FldNameList[2]   > TTPL.TenancyOutgoing.Percentage
"TenancyOutgoing.Percentage" "Percent" "->>9.99" "decimal" ? ? ? ? ? ? yes ?
     _FldNameList[3]   > TTPL.TenancyOutgoing.FixedAmount
"TenancyOutgoing.FixedAmount" "Fixed Amount" ? "decimal" ? ? ? ? ? ? yes ?
     _FldNameList[4]   > TTPL.TenancyOutgoing.BaseYear
"TenancyOutgoing.BaseYear" "O/G Base Year" ? "date" ? ? ? ? ? ? yes ?
     _FldNameList[5]   > TTPL.TenancyOutgoing.BaseYearAmount
"TenancyOutgoing.BaseYearAmount" "Base Year Amount" ? "decimal" ? ? ? ? ? ? yes ?
     _FldNameList[6]   > TTPL.TenancyOutgoing.ReconciliationDue
"TenancyOutgoing.ReconciliationDue" "Next Reconl." ? "date" ? ? ? ? ? ? yes ?
     _FldNameList[7]   > TTPL.TenancyOutgoing.OutgoingBasis
"TenancyOutgoing.OutgoingBasis" "Basis" ? "character" ? ? ? ? ? ? yes ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-sysmgr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  record-changed = No.
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
OR 'CURSOR-UP'   OF {&SELF-NAME}
OR 'RETURN'      OF {&SELF-NAME}
OR 'ENTER'       OF {&SELF-NAME}
OR 'CURSOR-DOWN' OF {&SELF-NAME} ANYWHERE
DO:
   /* Do not disable this code or no updates will take place except
      by pressing the Save button on an Update SmartPanel. */
   IF {&BROWSE-NAME}:CURRENT-ROW-MODIFIED OR record-changed THEN DO:
     RUN verify-outgoing.
     IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
     RUN calculate-total.
     RUN dispatch( 'update-record':U ).
   END.
   ELSE DO:
     {src/adm/template/brsleave.i}
   END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
  RUN update-account-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenancyOutgoing.AccountCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyOutgoing.AccountCode br_table _BROWSE-COLUMN B-table-Win
ON LEAVE OF TenancyOutgoing.AccountCode IN BROWSE br_table /* Account # */
DO:
  IF SELF:MODIFIED THEN
  DO:
    SELF:MODIFIED = No.
    RUN verify-account( "Verify" ).
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
    record-changed = yes.
    RUN get-attribute( "ADM-NEW-RECORD":U ).
    IF RETURN-VALUE = "YES" AND AVAILABLE(TenancyLease) THEN
    DO WITH FRAME {&FRAME-NAME}:
      TenancyOutgoing.Percentage:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( TenancyLease.OutgoingsRate, "->>,>>9.99").
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenancyOutgoing.Percentage
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyOutgoing.Percentage br_table _BROWSE-COLUMN B-table-Win
ON LEAVE OF TenancyOutgoing.Percentage IN BROWSE br_table /* Percent */
DO:
  IF SELF:MODIFIED THEN DO:
    SELF:MODIFIED = No.
    IF INPUT BROWSE {&BROWSE-NAME} {&SELF-NAME} > 100.00 THEN DO:
      MESSAGE "You cannot have percentages greater than 100!" VIEW-AS
        ALERT-BOX ERROR TITLE "Percentage Error".
      RETURN NO-APPLY.
    END.  
    record-changed = yes.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenancyOutgoing.FixedAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyOutgoing.FixedAmount br_table _BROWSE-COLUMN B-table-Win
ON LEAVE OF TenancyOutgoing.FixedAmount IN BROWSE br_table /* Fixed Amount */
DO:
  IF SELF:MODIFIED THEN DO:
    SELF:MODIFIED = No.
    record-changed = yes.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenancyOutgoing.BaseYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyOutgoing.BaseYear br_table _BROWSE-COLUMN B-table-Win
ON LEAVE OF TenancyOutgoing.BaseYear IN BROWSE br_table /* O/G Base Year */
DO:
  IF SELF:MODIFIED THEN DO:
    SELF:MODIFIED = No.
    record-changed = yes.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenancyOutgoing.BaseYearAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyOutgoing.BaseYearAmount br_table _BROWSE-COLUMN B-table-Win
ON LEAVE OF TenancyOutgoing.BaseYearAmount IN BROWSE br_table /* Base Year Amount */
DO:
  IF SELF:MODIFIED THEN DO:
    SELF:MODIFIED = No.
    record-changed = yes.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenancyOutgoing.ReconciliationDue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyOutgoing.ReconciliationDue br_table _BROWSE-COLUMN B-table-Win
ON LEAVE OF TenancyOutgoing.ReconciliationDue IN BROWSE br_table /* Next Reconl. */
DO:
  IF SELF:MODIFIED THEN DO:
    SELF:MODIFIED = No.
    record-changed = yes.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add B-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  IF {&BROWSE-NAME}:CURRENT-ROW-MODIFIED OR adm-new-record THEN DO:
    RUN verify-outgoing.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
    RUN dispatch( 'update-record':U ).
  END.
  RUN dispatch( 'add-record':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_delete B-table-Win
ON CHOOSE OF btn_delete IN FRAME F-Main /* Delete */
DO:
  IF adm-new-record THEN RUN dispatch( 'cancel-record':U ).
                    ELSE RUN dispatch( 'delete-record':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "TenancyLease"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "TenancyLease"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculate-total B-table-Win 
PROCEDURE calculate-total :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/
DEF BUFFER TOG FOR TenancyOutgoing.
DEF BUFFER POG FOR PropertyOutgoing.

DEF VAR tog-total AS DEC NO-UNDO INITIAL 0.0 .
  IF NOT AVAILABLE(TenancyLease) THEN RETURN.

  FOR EACH TOG OF TenancyLease NO-LOCK:
    FIND POG WHERE POG.PropertyCode = TenancyLease.PropertyCode
               AND POG.AccountCode = TOG.AccountCode NO-LOCK NO-ERROR.
    IF TOG.FixedAmount > 0 THEN DO:
      tog-total = tog-total + TOG.FixedAmount .
    END.
    ELSE IF AVAILABLE(POG) THEN DO:
      tog-total = tog-total + TOG.Percent * POG.BudgetAmount / 100 .
    END.
  END.

  FOR EACH POG WHERE POG.PropertyCode = TenancyLease.PropertyCode
              AND NOT CAN-FIND( TOG OF TenancyLease WHERE TOG.AccountCode = POG.AccountCode ) NO-LOCK:
    tog-total = tog-total + TenancyLease.OutgoingsRate * POG.BudgetAmount / 100 .
  END.

  fil_Total = tog-total.
  DISPLAY fil_Total WITH FRAME {&FRAME-NAME}.

  IF fields-disabled THEN RETURN.

  IF TenancyLease.RecoveryType = "B" THEN DO:
    FIND CURRENT TenancyLease EXCLUSIVE-LOCK NO-ERROR.
    IF AVAILABLE(TenancyLease) THEN DO:
      TenancyLease.OutgoingsBudget = tog-total.
    END.
    FIND CURRENT TenancyLease NO-LOCK NO-ERROR.
    RUN notify( 'refresh-outgoings,record-source':U ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-outgoing B-table-Win 
PROCEDURE check-outgoing :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-account( "Check" ).
  IF RETURN-VALUE = "FAIL" THEN RETURN( 
    STRING( TenancyOutGoing.AccountCode:HANDLE IN BROWSE {&BROWSE-NAME} ) ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement B-table-Win 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN get-attribute( "ADM-NEW-RECORD":U ).
  IF RETURN-VALUE = "YES" AND AVAILABLE(TenancyLease) THEN
  DO WITH FRAME {&FRAME-NAME}:
    TenancyOutgoing.TenancyLeaseCode = TenancyLease.TenancyLeaseCode.
  END.
  record-changed = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR go-for-it AS LOGICAL NO-UNDO INITIAL No.

  /* Code placed here will execute PRIOR to standard behavior. */
  MESSAGE "Do you want to delete this record?" VIEW-AS ALERT-BOX QUESTION
            BUTTONS OK-CANCEL 
            TITLE "Comfirm Deletion" UPDATE go-for-it.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN calculate-total.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query B-table-Win 
PROCEDURE local-open-query :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN special-enable-fields.
  RUN calculate-total.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available B-table-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN update-account-name.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "TenancyLease"}
  {src/adm/template/snd-list.i "TenancyOutgoing"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE special-enable-fields B-table-Win 
PROCEDURE special-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/
DEF VAR rights AS LOGICAL NO-UNDO.

  RUN get-rights IN sys-mgr( "Tenant-Outgoings", "MODIFY", OUTPUT rights).
  fields-disabled = NOT rights.
  ASSIGN TenancyOutgoing.AccountCode:READ-ONLY IN BROWSE {&BROWSE-NAME} = fields-disabled
         TenancyOutgoing.Percent:READ-ONLY = fields-disabled
         TenancyOutgoing.BaseYear:READ-ONLY = fields-disabled
         TenancyOutgoing.BaseYearAmount:READ-ONLY = fields-disabled 
         TenancyOutgoing.FixedAmount:READ-ONLY = fields-disabled 
         TenancyOutgoing.OutgoingBasis:READ-ONLY = fields-disabled 
         TenancyOutgoing.ReconciliationDue:READ-ONLY = fields-disabled
         btn_add:SENSITIVE IN FRAME {&FRAME-NAME} = rights
         btn_delete:SENSITIVE = rights .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-account-name B-table-Win 
PROCEDURE update-account-name :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  fil_Account = "".
  
  FIND ChartOfAccount WHERE ChartOfAccount.AccountCode =
    INPUT BROWSE {&BROWSE-NAME} TenancyOutgoing.AccountCode NO-LOCK NO-ERROR.
    
  IF AVAILABLE ChartOfAccount THEN fil_Account = ChartOfAccount.Name.
  DISPLAY fil_Account WITH FRAME {&FRAME-NAME}.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-account B-table-Win 
PROCEDURE verify-account :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER v-mode AS CHAR NO-UNDO.

  FIND ChartOfAccount WHERE ChartOfAccount.AccountCode =
    INPUT BROWSE {&BROWSE-NAME} TenancyOutgoing.AccountCode NO-LOCK NO-ERROR.
    
  IF NOT AVAILABLE ChartOfAccount THEN
  DO:
    IF v-mode = "Verify" THEN
    MESSAGE "There is no account with code " + STRING( INPUT BROWSE {&BROWSE-NAME}
      TenancyOutgoing.AccountCode, "9999.99" ) VIEW-AS ALERT-BOX ERROR TITLE
       "Account code Error".
    RETURN "FAIL".
  END.
  
  RUN update-account-name.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-outgoing B-table-Win 
PROCEDURE verify-outgoing :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR err-field AS HANDLE NO-UNDO.

  RUN check-outgoing.
  err-field = WIDGET-HANDLE( RETURN-VALUE ) NO-ERROR.

  IF VALID-HANDLE( err-field ) THEN DO:
    MESSAGE "The current outgoing is in error." SKIP
      "Do you want to abort the changes ? "
      VIEW-AS ALERT-BOX ERROR BUTTONS YES-NO UPDATE abort-it AS LOGI.
    IF abort-it THEN DO:
      IF AVAILABLE TenancyOutGoing THEN
        RUN dispatch( 'display-fields':U ).
      ELSE
        RUN dispatch( 'cancel-record':U ).
    END.
    ELSE DO:
      IF AVAILABLE TenancyOutGoing THEN GET CURRENT {&BROWSE-NAME}.
      APPLY 'ENTRY':U TO err-field.
      RETURN "FAIL".
    END.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


