&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
  File:  
  Description: 
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

{inc/topic/tpnewbch.i}

DEF VAR this-is-a-new-record AS LOGICAL INITIAL no NO-UNDO.
DEF VAR modifying-description AS LOGICAL INITIAL no NO-UNDO.

DEF VAR sort-by AS CHAR NO-UNDO.
DEF VAR filter-list AS CHAR NO-UNDO.
DEF VAR filter-by AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES NewBatch Person

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table NewBatch.BatchCode NewBatch.DocumentCount NewBatch.Total Person.FirstName NewBatch.BatchType NewBatch.Description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table NewBatch.Description   
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table NewBatch
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table NewBatch
&Scoped-define SELF-NAME br_table
&Scoped-define OPEN-QUERY-br_table CASE sort-by:   WHEN "Description" THEN     OPEN QUERY br_table FOR EACH NewBatch NO-LOCK           WHERE NewBatch.BatchType BEGINS filter-by, ~
                 FIRST Person OF NewBatch OUTER-JOIN NO-LOCK           BY NewBatch.Description.   WHEN "Operator" THEN     OPEN QUERY br_table FOR EACH NewBatch NO-LOCK           WHERE NewBatch.BatchType BEGINS filter-by, ~
                 FIRST Person OF NewBatch OUTER-JOIN NO-LOCK           BY Person.FirstName.   OTHERWISE     OPEN QUERY br_table FOR EACH NewBatch NO-LOCK           WHERE NewBatch.BatchType BEGINS filter-by                 OR (filter-by = "NORM" AND NewBatch.BatchType BEGINS "PART"), ~
                 FIRST Person OF NewBatch OUTER-JOIN NO-LOCK           BY NewBatch.BatchCode DESCENDING. END.
&Scoped-define TABLES-IN-QUERY-br_table NewBatch Person
&Scoped-define FIRST-TABLE-IN-QUERY-br_table NewBatch
&Scoped-define SECOND-TABLE-IN-QUERY-br_table Person


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      NewBatch, 
      Person SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _FREEFORM
  QUERY br_table NO-LOCK DISPLAY
      NewBatch.BatchCode
      NewBatch.DocumentCount COLUMN-LABEL 'Docs' FORMAT '>>>>9'
      NewBatch.Total COLUMN-LABEL 'Batch Total' FORMAT '->,>>>,>>>,>>9.99'
      Person.FirstName COLUMN-LABEL 'Created by' FORMAT 'X(15)'
      NewBatch.BatchType FORMAT 'X(8)'
      NewBatch.Description FORMAT 'X(75)'
  ENABLE
      NewBatch.Description
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 100 BY 14.4
         BGCOLOR 15 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 17
         WIDTH              = 108.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:MAX-DATA-GUESS IN FRAME F-Main         = 300.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _START_FREEFORM
CASE sort-by:
  WHEN "Description" THEN
    OPEN QUERY br_table FOR EACH NewBatch NO-LOCK
          WHERE NewBatch.BatchType BEGINS filter-by,
          FIRST Person OF NewBatch OUTER-JOIN NO-LOCK
          BY NewBatch.Description.
  WHEN "Operator" THEN
    OPEN QUERY br_table FOR EACH NewBatch NO-LOCK
          WHERE NewBatch.BatchType BEGINS filter-by,
          FIRST Person OF NewBatch OUTER-JOIN NO-LOCK
          BY Person.FirstName.
  OTHERWISE
    OPEN QUERY br_table FOR EACH NewBatch NO-LOCK
          WHERE NewBatch.BatchType BEGINS filter-by
                OR (filter-by = "NORM" AND NewBatch.BatchType BEGINS "PART"),
          FIRST Person OF NewBatch OUTER-JOIN NO-LOCK
          BY NewBatch.BatchCode DESCENDING.
END.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main B-table-Win
ON RETURN OF FRAME F-Main
ANYWHERE DO:
  IF LOOKUP( FOCUS:TYPE, 'FILL-IN,EDITOR,COMBO-BOX' ) <> 0 OR
     FOCUS:TYPE = 'BUTTON':U AND FOCUS:DYNAMIC THEN
  DO:
    APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN
      NewBatch.Description:READ-ONLY IN BROWSE {&BROWSE-NAME} = NOT this-is-a-new-record
    .
  END.

  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  

  IF this-is-a-new-record THEN DO:
    IF NOT( modifying-description ) THEN
      MESSAGE "New batch" NewBatch.BatchCode "created.".
    APPLY 'ENTRY':U TO NewBatch.Description IN BROWSE {&BROWSE-NAME}.
  END.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
  IF NewBatch.Description:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = "" THEN DO:
    MESSAGE "Each batch must have a description" VIEW-AS ALERT-BOX ERROR
                    TITLE "Batch has no description".
    RETURN NO-APPLY.
  END.
  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN
      NewBatch.Description:READ-ONLY IN BROWSE {&BROWSE-NAME} = yes
      this-is-a-new-record = no.
      modifying-description = no
    .
    RUN dispatch ( 'update-record':U ).
    RETURN NO-APPLY.
  END.

  /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
/*  {src/adm/template/brsleave.i} */

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list( 'SortBy-Options = Code|Operator|Description, SortBy-Case = Code':U).
filter-list = "".
FOR EACH BatchType:
  filter-list = filter-list + "|" + STRING(BatchType.BatchType, "X(4)") + " - " + BatchType.Description.
  IF BatchType.BatchType = "NORM" THEN filter-by = "NORM - " + BatchType.Description.
END.
filter-list = SUBSTRING( filter-list, 2).
RUN set-attribute-list( 'FilterBy-Options = ' + filter-list + ', FilterBy-Style = Combo-Box, FilterBy-Case = ' + filter-by ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record B-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       Standard method can't cope with first record for joined tables!
------------------------------------------------------------------------------*/
{&BROWSE-NAME}:INSERT-ROW("AFTER") IN FRAME {&FRAME-NAME}.

   /* Code placed here will execute PRIOR to standard behavior. */
  NewBatch.Description:READ-ONLY IN BROWSE {&BROWSE-NAME} = No.
  IF NOT AVAILABLE(NewBatch) THEN DO TRANSACTION:
    CREATE NewBatch.
    ASSIGN
      this-is-a-new-record = yes
      NewBatch.Description = ""
      NewBatch.BatchType = ( IF (filter-by = "ACCR") THEN "ACCR" ELSE "NORM" )
      NewBatch.Description:READ-ONLY IN BROWSE {&BROWSE-NAME} = no
    .
    RUN dispatch ( 'open-query':U ).
  END.
  ELSE DO:
  
     /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) . 
    
    /* Code placed here will execute AFTER standard behavior.    */

    ASSIGN
      this-is-a-new-record = yes
      /*
      NewBatch.DESCRIPTION:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = ""
      NewBatch.BatchType:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = ( IF (filter-by = "ACCR") THEN "ACCR" ELSE "NORM" )
      */
      NewBatch.Description:READ-ONLY IN BROWSE {&BROWSE-NAME} = no.
   
    RUN dispatch ('update-record':U). 

    DO TRANSACTION:
      FIND CURRENT NewBatch EXCLUSIVE-LOCK.
      ASSIGN
        NewBatch.Description = ""
        NewBatch.BatchType = ( IF (filter-by = "ACCR") THEN "ACCR" ELSE "NORM" )
      .
      DISPLAY NewBatch.DESCRIPTION 
                           NewBatch.BatchType 
                           WITH BROWSE {&BROWSE-NAME}.

    END.
  END.
  RUN dispatch( 'apply-entry' ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement B-table-Win 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  NewBatch.BatchType = ( IF (filter-by = "ACCR") THEN "ACCR" ELSE "NORM" ).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(NewBatch) THEN RETURN.

  IF NewBatch.BatchType BEGINS "UPDT" OR NewBatch.BatchType BEGINS "PART" THEN DO:
    MESSAGE "Cannot delete partly posted batches," SKIP
            "or batches pending update"
            VIEW-AS ALERT-BOX ERROR
            TITLE "Cannot Delete Batch!".
    RETURN.
  END.

  /* Code placed here will execute PRIOR to standard behavior. */
  MESSAGE "Really?" SKIP(2) "You want to consign this whole batch" SKIP
        "of " STRING(NewBatch.DocumentCount) " documents to oblivion?"
        VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL 
        TITLE "Are you sure?  How sure?"
        SET choice AS LOGICAL.
  IF NOT choice THEN RETURN "FAIL".

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-reset-record B-table-Win 
PROCEDURE local-reset-record :
/*------------------------------------------------------------------------------
  Purpose:     Enable the type and description fields to be maintained.
------------------------------------------------------------------------------*/

  APPLY 'ENTRY':U TO BROWSE {&BROWSE-NAME}.
  DO WITH FRAME {&FRAME-NAME} NO-VALIDATE:
    ASSIGN
      NewBatch.Description:READ-ONLY IN BROWSE {&BROWSE-NAME} = no
      this-is-a-new-record = yes
      modifying-description = Yes
    .
  END.
  APPLY 'ENTRY':U TO NewBatch.Description IN BROWSE {&BROWSE-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-open-query B-table-Win 
PROCEDURE pre-open-query :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF CURRENT-WINDOW:LOAD-MOUSE-POINTER('WAIT':U) THEN .

  RUN retotal-current.

  RUN get-attribute( 'SortBy-Case':U ).
  sort-by = RETURN-VALUE.
  RUN get-attribute( 'FilterBy-Case':U ).
  filter-by = TRIM( SUBSTRING( RETURN-VALUE, 1, 4) ).

  IF CURRENT-WINDOW:LOAD-MOUSE-POINTER('ARROW':U) THEN .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE retotal-current B-table-Win 
PROCEDURE retotal-current :
/*------------------------------------------------------------------------------
  Purpose:  Re-total the current batch.
------------------------------------------------------------------------------*/
DEF VAR new-total LIKE NewBatch.Total NO-UNDO.
DEF VAR new-count LIKE NewBatch.DocumentCount NO-UNDO.

  IF NOT AVAILABLE(NewBatch) THEN RETURN.
  IF NewBatch.BatchType = "AUTO" THEN RETURN.
  IF NewBatch.BatchType BEGINS "PART" THEN RETURN.
  IF NewBatch.BatchType BEGINS "UPDT" THEN RETURN.
  FIND FIRST NewDocument OF NewBatch NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(NewDocument) THEN RETURN.

  IF NewDocument.DocumentType = "RCPT" OR NewDocument.DocumentType = "PYMT" THEN DO:
    FOR EACH NewAcctTrans OF NewDocument NO-LOCK:
      new-total = new-total + NewAcctTrans.Amount.
    END.
  END.
  ELSE DO:
    FOR EACH NewDocument OF NewBatch NO-LOCK:
      FOR EACH NewAcctTrans OF NewDocument NO-LOCK:
        new-total = new-total + NewAcctTrans.Amount.
      END.
      new-count = new-count + 1.
    END.
  END.

  IF new-total <> NewBatch.Total OR new-count <> NewBatch.DocumentCount THEN DO:
    FIND CURRENT NewBatch EXCLUSIVE-LOCK.
    NewBatch.Total = new-total.
    NewBatch.DocumentCount = new-count.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "NewBatch"}
  {src/adm/template/snd-list.i "Person"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

