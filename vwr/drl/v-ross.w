&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description: 
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR last-property-code AS INT NO-UNDO INITIAL ?.
DEF VAR gross-rental AS DEC NO-UNDO.
DEF VAR opex-recovered AS DEC  NO-UNDO.
DEF VAR opex-budget-rec AS DEC NO-UNDO.
DEF VAR opex-budget-nr AS DEC  NO-UNDO.
DEF VAR vacant-at-ear AS DEC  NO-UNDO.
DEF VAR net-rent AS DECIMAL NO-UNDO.
DEF VAR net-rent-fl AS DECIMAL NO-UNDO.
DEF VAR total-area AS DEC NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set.i "AcctGroup-Recovered" "recovered-group-list" "ERROR"}
{inc/ofc-set.i "AcctGroup-Opex" "opex-group-list" "ERROR"}
{inc/ofc-set.i "AcctGroup-Ownex" "ownex-group-list" "ERROR"}
{inc/ofc-set.i "AcctGroup-Value" "valuation-group-list" "ERROR"}

DEF VAR fin-year LIKE FinancialYear.FinancialYearCode NO-UNDO.

FIND FIRST Month WHERE Month.StartDate <= TODAY AND Month.EndDate >= TODAY NO-LOCK.
fin-year = Month.FinancialYear.       /* this financial year */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Property
&Scoped-define FIRST-EXTERNAL-TABLE Property


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Property.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tgl_topmost RECT-4 
&Scoped-Define DISPLAYED-OBJECTS tgl_topmost fil_Details fil_Rental ~
fil_OpexRec fil_TotalIncome fil_OpexBdgRec fil_OpexBdgNR fil_OpexBdg ~
fil_NetRental fil_VacantAtEAR fil_NetRental-FL fil_Area fil_OpexPSM ~
fil_Valuation fil_ValuationDate fil_PassingYield fil_PassingYield-FL ~
fil_OpexPrompt 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD is-floor-area V-table-Win 
FUNCTION is-floor-area RETURNS LOGICAL
  ( INPUT area-type AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD this-year-budget V-table-Win 
FUNCTION this-year-budget RETURNS DECIMAL
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fil_Area AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_Details AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 47.43 BY 1
     BGCOLOR 16 FONT 10 NO-UNDO.

DEFINE VARIABLE fil_NetRental AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_NetRental-FL AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_OpexBdg AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_OpexBdgNR AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_OpexBdgRec AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_OpexPrompt AS CHARACTER FORMAT "X(256)":U INITIAL "Budget Opex per" 
      VIEW-AS TEXT 
     SIZE 20.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_OpexPSM AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_OpexRec AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_PassingYield AS DECIMAL FORMAT "-ZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 11.43 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_PassingYield-FL AS DECIMAL FORMAT "-ZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 11.43 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_Rental AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_TotalIncome AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_VacantAtEAR AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_Valuation AS DECIMAL FORMAT "-ZZZ,ZZZ,ZZ9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE VARIABLE fil_ValuationDate AS DATE FORMAT "99/99/9999":U 
     LABEL "on" 
     VIEW-AS FILL-IN 
     SIZE 11.43 BY 1
     BGCOLOR 16 FONT 8 NO-UNDO.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 49.14 BY 21.1.

DEFINE VARIABLE tgl_topmost AS LOGICAL INITIAL no 
     LABEL "On Top ?" 
     VIEW-AS TOGGLE-BOX
     SIZE 9.14 BY 1
     FONT 10 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     tgl_topmost AT ROW 1 COL 39.86
     fil_Details AT ROW 2 COL 2.14 NO-LABEL
     fil_Rental AT ROW 3.4 COL 31 COLON-ALIGNED NO-LABEL
     fil_OpexRec AT ROW 4.4 COL 31 COLON-ALIGNED NO-LABEL
     fil_TotalIncome AT ROW 5.5 COL 31 COLON-ALIGNED NO-LABEL
     fil_OpexBdgRec AT ROW 7 COL 31 COLON-ALIGNED NO-LABEL
     fil_OpexBdgNR AT ROW 8 COL 31 COLON-ALIGNED NO-LABEL
     fil_OpexBdg AT ROW 9.2 COL 31 COLON-ALIGNED NO-LABEL
     fil_NetRental AT ROW 10.6 COL 31 COLON-ALIGNED NO-LABEL
     fil_VacantAtEAR AT ROW 12 COL 30.86 COLON-ALIGNED NO-LABEL
     fil_NetRental-FL AT ROW 13.15 COL 30.86 COLON-ALIGNED NO-LABEL
     fil_Area AT ROW 14.8 COL 31 COLON-ALIGNED NO-LABEL
     fil_OpexPSM AT ROW 16 COL 31 COLON-ALIGNED NO-LABEL
     fil_Valuation AT ROW 17.2 COL 31 COLON-ALIGNED NO-LABEL
     fil_ValuationDate AT ROW 18.2 COL 36.14 COLON-ALIGNED
     fil_PassingYield AT ROW 19.5 COL 36.14 COLON-ALIGNED NO-LABEL
     fil_PassingYield-FL AT ROW 20.6 COL 36.14 COLON-ALIGNED NO-LABEL
     fil_OpexPrompt AT ROW 16.1 COL 2.72 NO-LABEL
     RECT-4 AT ROW 1.4 COL 1
     "Passing Yield (%):" VIEW-AS TEXT
          SIZE 12 BY 1 AT ROW 19.5 COL 2.72
          FONT 10
     "Total Income:" VIEW-AS TEXT
          SIZE 19.43 BY 1 AT ROW 5.6 COL 2.72
          FONT 10
     "Budget Opex recovered:" VIEW-AS TEXT
          SIZE 18.86 BY 1 AT ROW 4.4 COL 2.72
          FONT 10
     "NPI (fully leased):" VIEW-AS TEXT
          SIZE 20.43 BY 1 AT ROW 13.15 COL 2.57
          FONT 14
     "Vacant Space at EAR:" VIEW-AS TEXT
          SIZE 18.43 BY 1 AT ROW 12 COL 2.57
          FONT 10
     "Passing Yield - fully leased (%):" VIEW-AS TEXT
          SIZE 23.29 BY 1 AT ROW 20.6 COL 2.72
          FONT 10
     "Schedule Rental p.a. (today):" VIEW-AS TEXT
          SIZE 20 BY 1 AT ROW 3.4 COL 2.72
          FONT 10
     "Net Property Income Viewer" VIEW-AS TEXT
          SIZE 29.14 BY 1 AT ROW 1 COL 1.57
          FONT 14
     "Current Capital Valuation:" VIEW-AS TEXT
          SIZE 17.72 BY 1 AT ROW 17.3 COL 2.72
          FONT 10
     "Total Area:" VIEW-AS TEXT
          SIZE 9.72 BY 1 AT ROW 14.8 COL 2.72
          FONT 10
     "NPI (today):" VIEW-AS TEXT
          SIZE 19.43 BY 1 AT ROW 10.7 COL 2.72
          FONT 14
     "Budgeted Opex total:" VIEW-AS TEXT
          SIZE 19.43 BY 1 AT ROW 9.2 COL 2.72
          FONT 10
     "Budget Property Expenses (non-recoverable):" VIEW-AS TEXT
          SIZE 30.29 BY 1 AT ROW 8 COL 2.72
          FONT 10
     "Budget Property Expenses (recoverable):" VIEW-AS TEXT
          SIZE 29.14 BY 1 AT ROW 7 COL 2.72
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Property
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 23.5
         WIDTH              = 60.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fil_Area IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Details IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN fil_NetRental IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_NetRental-FL IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_OpexBdg IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_OpexBdgNR IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_OpexBdgRec IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_OpexPrompt IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN fil_OpexPSM IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_OpexRec IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_PassingYield IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       fil_PassingYield:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN fil_PassingYield-FL IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       fil_PassingYield-FL:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN fil_Rental IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_TotalIncome IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_VacantAtEAR IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Valuation IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       fil_Valuation:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN fil_ValuationDate IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME tgl_topmost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_topmost V-table-Win
ON VALUE-CHANGED OF tgl_topmost IN FRAME F-Main /* On Top ? */
DO:
  RUN top-most-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

{inc/ofc-set.i "Area-Units" "area-units"}
IF NOT AVAILABLE(OfficeSetting) THEN area-units = "Sq.M.".
fil_OpexPrompt = "Budget Opex per " + area-units + ":".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Property"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Property"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-gross-rental V-table-Win 
PROCEDURE get-gross-rental :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  gross-rental = 0.
  total-area = 0.
  vacant-at-ear = 0.
  FOR EACH RentalSpace OF Property NO-LOCK:
    IF RentalSpace.AreaStatus <> "V" AND RentalSpace.AreaStatus <> "C"
        AND CAN-FIND( FIRST TenancyLease OF RentalSpace WHERE
                              TenancyLease.LeaseStatus = "NORM" )
    THEN DO:
      gross-rental = gross-rental + RentalSpace.ContractedRental.   
    END.
    ELSE IF RentalSpace.AreaStatus = "V" THEN
      vacant-at-ear = vacant-at-ear + RentalSpace.MarketRental .

    IF is-floor-area( RentalSpace.AreaType ) THEN
      total-area = total-area + RentalSpace.AreaSize.
  END.

  DISPLAY gross-rental @ fil_rental 
          total-area @ fil_Area
          vacant-at-ear @ fil_VacantAtEAR
          WITH FRAME {&FRAME-NAME}.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-opex-budgets V-table-Win 
PROCEDURE get-opex-budgets :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR valuation AS DEC NO-UNDO INITIAL 0.
  opex-budget-rec = 0.
  opex-budget-nr = 0.
  opex-recovered = 0.


  FOR EACH AccountSummary WHERE AccountSummary.EntityType  = "P"
                            AND AccountSummary.EntityCode  = Property.PropertyCode
                            NO-LOCK,
          FIRST ChartOfAccount OF AccountSummary NO-LOCK:
    IF LOOKUP( ChartOfAccount.AccountGroupCode, recovered-group-list ) > 0 THEN
      opex-recovered = opex-recovered - this-year-budget( AccountSummary.EntityType, AccountSummary.EntityCode, AccountSummary.AccountCode ).
    ELSE IF LOOKUP( ChartOfAccount.AccountGroupCode, opex-group-list ) > 0 THEN
      opex-budget-rec = opex-budget-rec + this-year-budget( AccountSummary.EntityType, AccountSummary.EntityCode, AccountSummary.AccountCode ).
    ELSE IF LOOKUP( ChartOfAccount.AccountGroupCode, ownex-group-list ) > 0 THEN
      opex-budget-nr = opex-budget-nr + this-year-budget( AccountSummary.EntityType, AccountSummary.EntityCode, AccountSummary.AccountCode ).
    ELSE IF LOOKUP( ChartOfAccount.AccountGroupCode, valuation-group-list ) > 0 THEN
      valuation = valuation + AccountSummary.Balance .
  END.

  IF valuation <> 0 THEN fil_Valuation = valuation.

  DISPLAY opex-recovered @ fil_OpexRec
          opex-budget-rec @ fil_OpexBdgRec
          opex-budget-nr @ fil_OpexBdgNR
          (opex-budget-rec + opex-budget-nr) @ fil_OpexBdg
          WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-opex-budgetted V-table-Win 
PROCEDURE get-opex-budgetted :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  opex-budget-rec = 0.
  opex-budget-nr = 0.
  FOR EACH PropertyOutgoing OF Property NO-LOCK,
          FIRST ChartOfAccount OF PropertyOutgoing NO-LOCK:
    IF LOOKUP( ChartOfAccount.AccountGroupCode, opex-group-list ) > 0 THEN
      opex-budget-rec = opex-budget-rec + PropertyOutgoing.BudgetAmount .
    ELSE IF LOOKUP( ChartOfAccount.AccountGroupCode, ownex-group-list ) > 0 THEN
      opex-budget-nr = opex-budget-nr + PropertyOutgoing.BudgetAmount .
  END.

  DISPLAY opex-budget-rec @ fil_OpexBdgRec WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-opex-recovered V-table-Win 
PROCEDURE get-opex-recovered :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  opex-recovered = 0.
  FOR EACH PropertyOutgoing OF Property NO-LOCK,
        FIRST ChartOfAccount OF PropertyOutgoing NO-LOCK:

    IF LOOKUP( ChartOfAccount.AccountGroupCode, opex-group-list ) = 0 THEN NEXT.

    FOR EACH TenancyLease OF Property NO-LOCK WHERE TenancyLease.LeaseStatus <> "PAST"
                                    AND NOT TenancyLease.GrossLease:
      FIND TenancyOutgoing OF TenancyLease WHERE PropertyOutgoing.AccountCode = TenancyOutgoing.AccountCode NO-LOCK NO-ERROR.
      IF AVAILABLE(TenancyOutgoing) AND TenancyOutgoing.FixedAmount <> 0 AND TenancyOutgoing.FixedAmount <> ? THEN
        opex-recovered = opex-recovered + TenancyOutgoing.FixedAmount .
      ELSE IF AVAILABLE(TenancyOutgoing) THEN
        opex-recovered = opex-recovered + (TenancyOutgoing.Percentage * PropertyOutgoing.BudgetAmount) / 100.
      ELSE
        opex-recovered = opex-recovered + (TenancyLease.OutgoingsRate * PropertyOutgoing.BudgetAmount) / 100.
    END.
  END.

  DISPLAY opex-recovered @ fil_OpexRec
           WITH FRAME {&FRAME-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-opex-recovered-2nd V-table-Win 
PROCEDURE get-opex-recovered-2nd :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  opex-recovered = 0.
  FOR EACH TenancyLease OF Property NO-LOCK WHERE TenancyLease.LeaseStatus <> "PAST",
          EACH TenancyOutGoing OF TenancyLease NO-LOCK,
          FIRST ChartOfAccount OF TenancyOutgoing NO-LOCK:

    IF TenancyOutgoing.FixedAmount <> 0 AND TenancyOutgoing.FixedAmount <> ? THEN DO:
      opex-recovered = opex-recovered + TenancyOutgoing.FixedAmount .
      NEXT.
    END.

    FIND FIRST PropertyOutgoing NO-LOCK OF Property
                    WHERE PropertyOutgoing.AccountCode = TenancyOutgoing.AccountCode NO-ERROR.
    IF NOT AVAILABLE(PropertyOutgoing) THEN NEXT.

    IF LOOKUP( ChartOfAccount.AccountGroupCode, opex-group-list ) = 0 THEN NEXT.
    opex-recovered = opex-recovered + (TenancyOutgoing.Percentage * PropertyOutgoing.BudgetAmount) / 100.
  END.

  DISPLAY opex-recovered @ fil_OpexRec
           WITH FRAME {&FRAME-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-opex-recovered-old V-table-Win 
PROCEDURE get-opex-recovered-old :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  opex-recovered = 0.
  FOR EACH TenancyLease OF Property NO-LOCK WHERE
    TenancyLease.LeaseStatus <> "PAST",
      EACH TenancyOutGoing OF TenancyLease NO-LOCK,
        FIRST ChartOfAccount OF TenancyOutGoing WHERE
          LOOKUP( ChartOfAccount.AccountGroupCode, opex-group-list ) <> 0 NO-LOCK:
    FOR EACH Month WHERE Month.FinancialYearCode = fin-year NO-LOCK:
      FIND AccountBalance WHERE AccountBalance.EntityType  = "P"
                        AND AccountBalance.EntityCode  = Property.PropertyCode
                        AND AccountBalance.AccountCode = ChartOfAccount.AccountCode
                        AND AccountBalance.MonthCode = Month.MonthCode
        NO-LOCK NO-ERROR.
      
      IF NOT AVAILABLE AccountBalance THEN NEXT.
      
      opex-recovered = opex-recovered + (TenancyOutgoing.Percentage * AccountBalance.Budget) / 100.
    END.
  END.

  DISPLAY opex-recovered @ fil_OpexRec WITH FRAME {&FRAME-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-valuation-info V-table-Win 
PROCEDURE get-valuation-info :
/*------------------------------------------------------------------------------
  Purpose:  Calculate the fill-ins
------------------------------------------------------------------------------*/

IF NOT AVAILABLE(Property) THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:
  FIND FIRST /*LAST*/ Valuation OF Property WHERE Valuation.ValuationType = "CVAL" NO-LOCK NO-ERROR.
  IF AVAILABLE(Valuation) THEN ASSIGN
    fil_Valuation = Valuation.Amount
    fil_ValuationDate = Valuation.DateDone.

  DISPLAY fil_Valuation fil_ValuationDate .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  CURRENT-WINDOW:TITLE = "Net Property Income Viewer".
  last-property-code = ?.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Property) OR Property.PropertyCode = last-property-code THEN RETURN.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

DO WITH FRAME {&FRAME-NAME}:
  DISPLAY Property.name @ fil_Details.
  fil_Valuation = ?.
  fil_ValuationDate = ?.

  RUN get-gross-rental.
/*  RUN get-opex-recovered. */

  RUN get-opex-budgets.
  DISPLAY gross-rental + opex-recovered @ fil_TotalIncome.
  net-rent = gross-rental + opex-recovered - (opex-budget-rec + opex-budget-nr).
  net-rent-fl = net-rent + vacant-at-ear.
  DISPLAY net-rent @ fil_NetRental
          (IF total-area <> 0 THEN (opex-budget-rec + opex-budget-nr) / total-area ELSE 0) @ fil_OpexPSM
          net-rent-fl @ fil_NetRental-FL .

  RUN get-valuation-info.

  DISPLAY (IF fil_Valuation <> 0 THEN (net-rent / fil_Valuation) * 100 ELSE 0) @ fil_PassingYield.
  DISPLAY (IF fil_Valuation <> 0 THEN (net-rent-fl / fil_Valuation) * 100 ELSE 0) @ fil_PassingYield-FL.
END.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  last-property-code = Property.PropertyCode.
  have-records = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  have-records = No.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Property"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE top-most-changed V-table-Win 
PROCEDURE top-most-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  ASSIGN FRAME {&FRAME-NAME} tgl_topmost.
  IF tgl_topmost THEN
    RUN notify( "set-topmost,container-source" ).
  ELSE
    RUN notify( "reset-topmost,container-source" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION is-floor-area V-table-Win 
FUNCTION is-floor-area RETURNS LOGICAL
  ( INPUT area-type AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  FIND AreaType WHERE AreaType.AreaType = area-type NO-LOCK NO-ERROR.
  IF AVAILABLE(AreaType) THEN
    RETURN AreaType.IsFloorArea.
  ELSE

  RETURN No.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION this-year-budget V-table-Win 
FUNCTION this-year-budget RETURNS DECIMAL
  ( INPUT et AS CHAR, INPUT ec AS INT, INPUT ac AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR budget-amount AS DEC NO-UNDO INITIAL 0.00 .

  FOR EACH Month WHERE Month.FinancialYearCode = fin-year NO-LOCK:
    FIND AccountBalance WHERE AccountBalance.EntityType  = et
                        AND AccountBalance.EntityCode  = ec
                        AND AccountBalance.AccountCode = ac
                        AND AccountBalance.MonthCode = Month.MonthCode
                        NO-LOCK NO-ERROR.
      
    IF NOT AVAILABLE AccountBalance THEN NEXT.
    budget-amount = budget-amount + AccountBalance.Budget.
  END.

  RETURN budget-amount .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

