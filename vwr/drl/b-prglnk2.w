&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ProgramLink LinkNode

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table LinkNode.Description ~
ProgramLink.ButtonLabel ProgramLink.LinkType ProgramLink.LinkCode ~
ProgramLink.Viewer ProgramLink.Description ProgramLink.Function 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table LinkNode.Description ~
ProgramLink.ButtonLabel ProgramLink.LinkType ProgramLink.Viewer ~
ProgramLink.Description ProgramLink.Function 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}~
 ~{&FP1}ButtonLabel ~{&FP2}ButtonLabel ~{&FP3}~
 ~{&FP1}LinkType ~{&FP2}LinkType ~{&FP3}~
 ~{&FP1}Viewer ~{&FP2}Viewer ~{&FP3}~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}~
 ~{&FP1}Function ~{&FP2}Function ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table LinkNode ProgramLink
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table LinkNode
&Scoped-define SECOND-ENABLED-TABLE-IN-QUERY-br_table ProgramLink
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH ProgramLink WHERE ~{&KEY-PHRASE} NO-LOCK, ~
      EACH LinkNode WHERE LinkNode.NodeCode = ProgramLink.Source NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table ProgramLink LinkNode
&Scoped-define FIRST-TABLE-IN-QUERY-br_table ProgramLink


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS>
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Source|y||ttpl.ProgramLink.Source|yes
Viewer|||ttpl.ProgramLink.Viewer|yes
Code|||ttpl.ProgramLink.LinkCode|yes
Button|||ttpl.ProgramLink.ButtonLabel|yes
Description|||ttpl.ProgramLink.Description|yes
Type|||ttpl.ProgramLink.LinkType|yes
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Source,Viewer,Code,Button,Description,Type",
     SortBy-Case = Source':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      ProgramLink, 
      LinkNode SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      LinkNode.Description COLUMN-LABEL "Linked From" FORMAT "X(27)"
      ProgramLink.ButtonLabel COLUMN-LABEL "Button Label" FORMAT "X(15)"
      ProgramLink.LinkType COLUMN-LABEL "Type"
      ProgramLink.LinkCode COLUMN-LABEL "Code"
      ProgramLink.Viewer FORMAT "X(10)"
      ProgramLink.Description COLUMN-LABEL "Tooltip Description" FORMAT "X(60)"
      ProgramLink.Function COLUMN-LABEL "Attributes" FORMAT "X(100)"
  ENABLE
      LinkNode.Description
      ProgramLink.ButtonLabel
      ProgramLink.LinkType
      ProgramLink.Viewer
      ProgramLink.Description
      ProgramLink.Function
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 108.57 BY 23.6
         BGCOLOR 16 FONT 9.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 24.25
         WIDTH              = 115.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main = 2
       br_table:MAX-DATA-GUESS IN FRAME F-Main     = 1000.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "ttpl.ProgramLink,ttpl.LinkNode WHERE ttpl.ProgramLink ..."
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _JoinCode[2]      = "LinkNode.NodeCode = ProgramLink.Source"
     _FldNameList[1]   > ttpl.LinkNode.Description
"LinkNode.Description" "Linked From" "X(27)" "character" ? ? ? ? ? ? yes ?
     _FldNameList[2]   > ttpl.ProgramLink.ButtonLabel
"ProgramLink.ButtonLabel" "Button Label" "X(15)" "character" ? ? ? ? ? ? yes ?
     _FldNameList[3]   > ttpl.ProgramLink.LinkType
"ProgramLink.LinkType" "Type" ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[4]   > ttpl.ProgramLink.LinkCode
"ProgramLink.LinkCode" "Code" ? "integer" ? ? ? ? ? ? no ?
     _FldNameList[5]   > ttpl.ProgramLink.Viewer
"ProgramLink.Viewer" ? "X(10)" "character" ? ? ? ? ? ? yes ?
     _FldNameList[6]   > ttpl.ProgramLink.Description
"ProgramLink.Description" "Tooltip Description" "X(60)" "character" ? ? ? ? ? ? yes ?
     _FldNameList[7]   > ttpl.ProgramLink.Function
"ProgramLink.Function" "Attributes" "X(100)" "character" ? ? ? ? ? ? yes ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
  RUN set-formats.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  RUN set-formats.
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF

RUN set-attribute-list( 'SortBy-Options = Source|Type|Description|Code|Button|Viewer, SortBy-Case = Source':U ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

  RUN get-attribute ('SortBy-Case':U).
  CASE RETURN-VALUE:
    WHEN 'Source':U THEN DO:
      &Scope SORTBY-PHRASE BY ProgramLink.Source
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END.
    WHEN 'Viewer':U THEN DO:
      &Scope SORTBY-PHRASE BY ProgramLink.Viewer
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END.
    WHEN 'Code':U THEN DO:
      &Scope SORTBY-PHRASE BY ProgramLink.LinkCode
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END.
    WHEN 'Button':U THEN DO:
      &Scope SORTBY-PHRASE BY ProgramLink.ButtonLabel
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END.
    WHEN 'Description':U THEN DO:
      &Scope SORTBY-PHRASE BY ProgramLink.Description
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END.
    WHEN 'Type':U THEN DO:
      &Scope SORTBY-PHRASE BY ProgramLink.LinkType
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END.
    OTHERWISE DO:
      &Undefine SORTBY-PHRASE
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ProgramLink"}
  {src/adm/template/snd-list.i "LinkNode"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-formats B-table-Win 
PROCEDURE set-formats :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  LinkNode.Description:FORMAT IN BROWSE {&BROWSE-NAME} = "X(50)".
  ProgramLink.ButtonLabel:FORMAT IN BROWSE {&BROWSE-NAME} = "X(40)".
  ProgramLink.Viewer:FORMAT IN BROWSE {&BROWSE-NAME} = "X(40)".
  ProgramLink.Description:FORMAT IN BROWSE {&BROWSE-NAME} = "X(120)".
  ProgramLink.Function:FORMAT IN BROWSE {&BROWSE-NAME} = "X(2000)".

  LinkNode.Description:MODIFIED IN BROWSE {&BROWSE-NAME} = No.
  ProgramLink.ButtonLabel:MODIFIED IN BROWSE {&BROWSE-NAME} = No.
  ProgramLink.Viewer:MODIFIED IN BROWSE {&BROWSE-NAME} = No.
  ProgramLink.Description:MODIFIED IN BROWSE {&BROWSE-NAME} = No.
  ProgramLink.Function:MODIFIED IN BROWSE {&BROWSE-NAME} = No.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


