&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------

  File:  

  Description: from BROWSER.W - Basic SmartBrowser Object Template

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR filter-case AS CHAR NO-UNDO INITIAL "".
DEF VAR sort-case AS CHAR NO-UNDO INITIAL "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES LookupCode

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table LookupCode._File-Name ~
LookupCode._Field-Name LookupCode.Description LookupCode.LookupCode ~
LookupCode.LookupData 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table LookupCode._File-Name ~
LookupCode._Field-Name LookupCode.Description LookupCode.LookupCode ~
LookupCode.LookupData 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}_File-Name ~{&FP2}_File-Name ~{&FP3}~
 ~{&FP1}_Field-Name ~{&FP2}_Field-Name ~{&FP3}~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}~
 ~{&FP1}LookupCode ~{&FP2}LookupCode ~{&FP3}~
 ~{&FP1}LookupData ~{&FP2}LookupData ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table LookupCode
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table LookupCode
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH LookupCode NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br_table LookupCode
&Scoped-define FIRST-TABLE-IN-QUERY-br_table LookupCode


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br_table}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      LookupCode SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      LookupCode._File-Name
      LookupCode._Field-Name
      LookupCode.Description
      LookupCode.LookupCode
      LookupCode.LookupData
  ENABLE
      LookupCode._File-Name
      LookupCode._Field-Name
      LookupCode.Description
      LookupCode.LookupCode
      LookupCode.LookupData
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 91.43 BY 12.8
         BGCOLOR 15 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 13.3
         WIDTH              = 92.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.LookupCode"
     _Options          = "NO-LOCK"
     _FldNameList[1]   > TTPL.LookupCode._File-Name
"_File-Name" ? ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[2]   > TTPL.LookupCode._Field-Name
"_Field-Name" ? ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[3]   > TTPL.LookupCode.Description
"Description" ? ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[4]   > TTPL.LookupCode.LookupCode
"LookupCode" ? ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[5]   > TTPL.LookupCode.LookupData
"LookupData" ? ? "character" ? ? ? ? ? ? yes ?
     _Query            is OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list( 'FilterBy-Style = COMBO-BOX, FilterBy-Label = File name' ).
RUN set-attribute-list( 'SortBy-Style = COMBO-BOX, SortBy-Label = Field name' ).
RUN set-filter-options.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize B-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN use-FilterBy-Label( "File name" ).
  RUN use-SortBy-Label( "Field name" ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF {&BROWSE-NAME}:NEW-ROW IN FRAME {&FRAME-NAME} THEN DO:
    RUN dispatch( 'cancel-record':U ).
    RETURN.
  END.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "LookupCode"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-filter-options B-table-Win 
PROCEDURE set-filter-options :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR filter-options AS CHAR NO-UNDO INITIAL "".

DEF VAR i AS INT NO-UNDO INITIAL 0.

  FOR EACH _File NO-LOCK WHERE CAN-FIND( FIRST LookupCode WHERE LookupCode._File-Name = _File._File-Name ):
    filter-options = filter-options + _File._File-Name + "|".
    i = i + 1.
    IF i > 120 THEN LEAVE.
  END.

  filter-options = TRIM(filter-options,"|").
  FIND FIRST LookupCode NO-LOCK NO-ERROR.
  IF AVAILABLE( LookupCode ) THEN
    filter-case = LookupCode._File-Name.
  ELSE
    filter-case = ENTRY( 1, filter-options, "|").

  RUN set-attribute-list( 'FilterBy-Options = ' + filter-options ).
  RUN set-attribute-list( 'FilterBy-Case = ' + filter-case ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-sort-options B-table-Win 
PROCEDURE set-sort-options :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR sort-options AS CHAR NO-UNDO INITIAL "".

  FIND _File WHERE _File._File-Name = filter-case NO-LOCK NO-ERROR.
  IF AVAILABLE(_File) THEN DO:
    FOR EACH _Field OF _File NO-LOCK:
      sort-options = sort-options + _Field._Field-Name + "|".
    END.

    sort-options = TRIM(sort-options,"|").
    FIND FIRST LookupCode WHERE LookupCode._File-Name = filter-case NO-LOCK NO-ERROR.
    IF AVAILABLE(LookupCode) THEN
      sort-case = LookupCode._Field-Name.
    ELSE
      sort-case = ENTRY( 1, sort-options, "|").
  END.

  RUN set-attribute-list( 'SortBy-Options = ' + sort-options ).
  RUN set-attribute-list( 'SortBy-Case = ' + sort-case ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-filter AS CHAR NO-UNDO.
  filter-case = new-filter.
  RUN set-sort-options.
  RUN notify( 'initialize,SortBy-Source':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Label B-table-Win 
PROCEDURE use-FilterBy-Label :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-label AS CHAR NO-UNDO.

DEF VAR c-list AS CHAR NO-UNDO.
DEF VAR Filter-panel AS WIDGET-HANDLE NO-UNDO.

  RUN get-link-handle IN adm-broker-hdl (THIS-PROCEDURE, 'FilterBy-Source', OUTPUT c-list).
  Filter-panel = WIDGET-HANDLE( ENTRY(1, c-list ) ).
  IF VALID-HANDLE(Filter-panel) THEN DO:
    RUN set-attribute-list IN Filter-panel ( "Label = " + new-label ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SortBy-Case B-table-Win 
PROCEDURE use-SortBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-sort AS CHAR NO-UNDO.
  sort-case = new-sort.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SortBy-Label B-table-Win 
PROCEDURE use-SortBy-Label :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-label AS CHAR NO-UNDO.

DEF VAR c-list AS CHAR NO-UNDO.
DEF VAR sort-panel AS WIDGET-HANDLE NO-UNDO.

  RUN get-link-handle IN adm-broker-hdl (THIS-PROCEDURE, 'SortBy-Source', OUTPUT c-list).
  sort-panel = WIDGET-HANDLE( ENTRY(1, c-list ) ).
  IF VALID-HANDLE(sort-panel) THEN DO:
    RUN set-attribute-list IN sort-panel ( "Label = " + new-label ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


