&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR scenario-code LIKE Scenario.ScenarioCode NO-UNDO.
DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}

{inc/ofc-this.i}
{inc/ofc-acct.i "RENT" "control-rent-account"}

DEF VAR cf-amount AS DEC NO-UNDO.

DEF TEMP-TABLE RentFlow LIKE CashFlow
  FIELD RentFlowType AS CHAR INITIAL "MRNT"
  FIELD AnnualAmount AS DEC INITIAL 0
  FIELD flow-id AS ROWID
  INDEX start-date IS PRIMARY StartDate
  INDEX flow-id IS UNIQUE flow-id.
  
DEF VAR last-id AS ROWID NO-UNDO.
DEF VAR curr-id-code AS CHAR   NO-UNDO.
DEF VAR this-win     AS HANDLE NO-UNDO.

DEF VAR counter AS INT NO-UNDO INITIAL 0.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RentalSpace
&Scoped-define FIRST-EXTERNAL-TABLE RentalSpace


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RentalSpace.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES RentFlow

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table RentFlow.RentFlowType RentFlow.StartDate RentFlow.CFChangeType RentFlow.AnnualAmount RentFlow.Description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table RentFlow.StartDate  RentFlow.CFChangeType  RentFlow.AnnualAmount  RentFlow.Description   
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}StartDate ~{&FP2}StartDate ~{&FP3}~
 ~{&FP1}CFChangeType ~{&FP2}CFChangeType ~{&FP3}~
 ~{&FP1}AnnualAmount ~{&FP2}AnnualAmount ~{&FP3}~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table RentFlow
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table RentFlow
&Scoped-define SELF-NAME br_table
&Scoped-define OPEN-QUERY-br_table OPEN QUERY {&SELF-NAME} FOR EACH RentFlow ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table RentFlow
&Scoped-define FIRST-TABLE-IN-QUERY-br_table RentFlow


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table tgl_topmost 
&Scoped-Define DISPLAYED-OBJECTS fil_space tgl_topmost 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS>
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD id-code B-table-Win 
FUNCTION id-code RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD is-rent-flow B-table-Win 
FUNCTION is-rent-flow RETURNS LOGICAL
  ( INPUT cf-type AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD next-number B-table-Win 
FUNCTION next-number RETURNS INTEGER
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fil_space AS CHARACTER FORMAT "X(256)":U 
     LABEL "Space" 
     VIEW-AS FILL-IN 
     SIZE 77.57 BY 1
     FGCOLOR 1  NO-UNDO.

DEFINE VARIABLE tgl_topmost AS LOGICAL INITIAL no 
     LABEL "On Top?" 
     VIEW-AS TOGGLE-BOX
     SIZE 9.14 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      RentFlow SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _FREEFORM
  QUERY br_table NO-LOCK DISPLAY
      RentFlow.RentFlowType LABEL " Type " FORMAT "X(4)"
  RentFlow.StartDate LABEL "      Start      "
  RentFlow.CFChangeType COLUMN-LABEL "Type"
  RentFlow.AnnualAmount LABEL "New Rental P.A" FORMAT "->,>>>,>>9.99"
  RentFlow.Description LABEL "Description" FORMAT "X(87)"
ENABLE  
  RentFlow.StartDate
  RentFlow.CFChangeType
  RentFlow.AnnualAmount
  RentFlow.Description
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 97.72 BY 12
         BGCOLOR 15 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_space AT ROW 1 COL 4.29 COLON-ALIGNED
     br_table AT ROW 2 COL 1
     tgl_topmost AT ROW 1 COL 87.86
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FGCOLOR 0 FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   External Tables: TTPL.RentalSpace
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 15.1
         WIDTH              = 98.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
/* BROWSE-TAB br_table fil_space F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fil_space IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH RentFlow ~{&SORTBY-PHRASE}.
     _END_FREEFORM
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main B-table-Win
ON RETURN OF FRAME F-Main
ANYWHERE DO:
  APPLY 'TAB':U TO SELF.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-DISPLAY OF br_table IN FRAME F-Main
DO:
  RUN inst-row-display.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
  RUN assign-if-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  RETURN.
DEF VAR rd-only AS LOGI NO-UNDO INITIAL No.

  IF {&BROWSE-NAME}:NEW-ROW = Yes OR AVAILABLE(RentFlow) THEN DO WITH FRAME {&FRAME-NAME}:
    IF {&BROWSE-NAME}:NEW-ROW = No AND RentFlow.RentFlowType <> "MRNT" THEN rd-only = Yes .
    ASSIGN RentFlow.StartDate:READ-ONLY IN BROWSE {&BROWSE-NAME} = rd-only
          RentFlow.CFChangeType:READ-ONLY = rd-only
          RentFlow.AnnualAmount:READ-ONLY = rd-only
          RentFlow.Description:READ-ONLY = rd-only 
          NO-ERROR.
  END.
  ELSE
    rd-only = Yes.

  IF NOT rd-only THEN
/*    APPLY 'ENTRY':U TO RentFlow.StartDate IN BROWSE {&BROWSE-NAME}. */ .
  ELSE DO:
    /* IF {&BROWSE-NAME}:SELECT-FOCUSED-ROW() THEN . */
    APPLY "TAB":U TO BROWSE {&BROWSE-NAME}.
    APPLY "ENTRY":U TO BROWSE {&BROWSE-NAME}.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_topmost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_topmost B-table-Win
ON VALUE-CHANGED OF tgl_topmost IN FRAME F-Main /* On Top? */
DO:
  RUN top-most-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF

ON WRITE OF CashFlow DO:
DEF BUFFER OtherFlow FOR CashFlow.

  IF NEW(CashFlow) THEN DO:
    FIND LAST OtherFlow WHERE
      OtherFlow.ScenarioCode = CashFlow.ScenarioCode AND
      OtherFlow.EntityType   = CashFlow.EntityType AND
      OtherFlow.EntityCode   = CashFlow.EntityCode AND
      OtherFlow.AccountCode  = CashFlow.AccountCode NO-LOCK NO-ERROR.
    ASSIGN CashFlow.Sequence = IF AVAILABLE OtherFlow THEN
      OtherFlow.Sequence + 1 ELSE 1.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RentalSpace"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RentalSpace"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-flows B-table-Win 
PROCEDURE assign-flows :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Delete all deleted cash flows */
  FOR EACH CashFlow WHERE 
    Cashflow.ScenarioCode = scenario-code AND
    CashFlow.RelatedKey = curr-id-code AND NOT CAN-FIND( FIRST RentFlow
    WHERE ROWID( CashFlow ) = RentFlow.flow-id ) EXCLUSIVE-LOCK:
    DELETE CashFlow.
  END.
  
  DEF VAR last-date AS DATE NO-UNDO INITIAL ?.
  DEF BUFFER RF FOR RentFlow.
  
  FOR EACH RF BY RF.StartDate DESCENDING:

    RF.Amount = - (IF RF.CFChangeType = "A" THEN RF.AnnualAmount ELSE ROUND( RF.AnnualAmount / 12, 2)).
    FIND FIRST Cashflow WHERE ROWID( CashFlow ) = RF.flow-id
      EXCLUSIVE-LOCK NO-ERROR.
    IF NOT AVAILABLE(Cashflow) THEN DO:
      CREATE Cashflow.
      ASSIGN 
        CashFlow.ScenarioCode = scenario-code
        CashFlow.EntityType = "P"
        CashFlow.EntityCode = RentalSpace.PropertyCode
        Cashflow.CashFlowType = "MRNT"
        Cashflow.AccountCode = control-rent-account
        CashFlow.FrequencyCode = "MNTH"
        CashFlow.RelatedKey = curr-id-code.
    END.

    RUN check-cf-sequence.
    IF CashFlow.CFChangeType = "A" THEN ASSIGN
      CashFlow.EndDate = CashFlow.StartDate
      RF.EndDate = CashFlow.StartDate
      CashFlow.FrequencyCode = "YEAR".  /* effectively 'once' */
    ELSE ASSIGN
      RF.EndDate = (IF last-date = ? THEN RF.EndDate ELSE last-date)
      RF.RentFlowType = (IF RF.EndDate > CashFlow.EndDate THEN "MRNT" ELSE RF.RentFlowType)
      CashFlow.EndDate = RF.EndDate
      CashFlow.FrequencyCode = "MNTH"
      last-date = RF.StartDate - 1.

    IF RF.RentFlowType = "MRNT" THEN DO:
      ASSIGN  CashFlow.StartDate = RF.StartDate
              CashFlow.CFChangeType = RF.CFChangeType
              CashFlow.Amount = RF.Amount
              CashFlow.CashFlowType = RF.RentFlowType
              CashFlow.Description = RF.Description.
    END.

  END.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-if-changed B-table-Win 
PROCEDURE assign-if-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR the-same AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  IF NOT {&BROWSE-NAME}:NEW-ROW THEN DO:
    IF NOT AVAILABLE(RentFlow) THEN RETURN.

    FIND FIRST Cashflow NO-LOCK WHERE ROWID( CashFlow ) = RentFlow.flow-id NO-ERROR.
    IF AVAILABLE(Cashflow) THEN DO:
      RentFlow.Amount = - (IF RentFlow.CFChangeType = "A" THEN RentFlow.AnnualAmount ELSE ROUND( RentFlow.AnnualAmount / 12, 2)).
      BUFFER-COMPARE RentFlow TO CashFlow SAVE the-same.
      IF the-same = "" THEN RETURN.
/*      ELSE MESSAGE the-same. */

      /* Override the flow type to indicate they have changed something */
      RentFlow.CashFlowType = "MRNT".
      RentFlow.RentFlowType = "MRNT".
      DISPLAY RentFlow.RentFlowType WITH BROWSE {&BROWSE-NAME}.
    END.
  END.

  RUN assign-flows.
  IF {&BROWSE-NAME}:REFRESH() THEN .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-cf-sequence B-table-Win 
PROCEDURE check-cf-sequence :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER OtherFlow FOR CashFlow.

  FIND OtherFlow WHERE OtherFlow.ScenarioCode = CashFlow.ScenarioCode
                   AND OtherFlow.EntityType   = CashFlow.EntityType
                   AND OtherFlow.EntityCode   = CashFlow.EntityCode
                   AND OtherFlow.AccountCode  = CashFlow.AccountCode
                   AND OtherFlow.Sequence     = CashFlow.Sequence
                   AND ROWID(OtherFlow) <> ROWID(CashFlow)
                   NO-LOCK NO-ERROR.
  IF AVAILABLE(OtherFlow) THEN DO:
    FIND LAST OtherFlow WHERE
      OtherFlow.ScenarioCode = CashFlow.ScenarioCode AND
      OtherFlow.EntityType   = CashFlow.EntityType AND
      OtherFlow.EntityCode   = CashFlow.EntityCode AND
      OtherFlow.AccountCode  = CashFlow.AccountCode
      NO-LOCK NO-ERROR.
    ASSIGN CashFlow.Sequence = IF AVAILABLE OtherFlow THEN
      OtherFlow.Sequence + 1 ELSE 1.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize B-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  this-win = CURRENT-WINDOW.
  tgl_topmost = No.
  DISPLAY tgl_topmost WITH FRAME {&FRAME-NAME}.
  RUN top-most-changed.

  FIND scenario WHERE Scenario.ScenarioCode = scenario-code NO-LOCK NO-ERROR.

  CURRENT-WINDOW:TITLE = "Rental Assumptions, Scenario "
                       + STRING(scenario-code) + " - "
                       + (IF AVAILABLE Scenario THEN Scenario.Name ELSE "unnamed").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available B-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE RentalSpace AND ROWID( RentalSpace ) <> last-id THEN
  DO WITH FRAME {&FRAME-NAME}:
    
    fil_space = "P" + STRING( RentalSpace.PropertyCode, "99999" ) + " "
              + STRING( RentalSpace.Level, "->>,>>9")
              + STRING( RentalSpace.LevelSequence, "->>,>>9") + "     "
              + RentalSpace.Description.
    
    DISPLAY fil_space WITH FRAME {&FRAME-NAME}.
    RUN open-flow-query.
  END.
  last-id = ROWID( RentalSpace ).  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-display B-table-Win 
PROCEDURE inst-row-display :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR fcolor AS INT NO-UNDO INITIAL ?.

IF NOT AVAILABLE(RentFlow) THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:
  IF RentFlow.RentFlowType <> "MRNT" THEN fcolor = 7.
  ASSIGN RentFlow.StartDate:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
        RentFlow.RentFlowType:FGCOLOR = fcolor
        RentFlow.CFChangeType:FGCOLOR = fcolor
        RentFlow.AnnualAmount:FGCOLOR = fcolor
        RentFlow.Description:FGCOLOR = fcolor.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-value-changed B-table-Win 
PROCEDURE inst-value-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION <> "WINDOW-CLOSE" THEN
    RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, "delete-record",
      "SENSITIVE = " + IF AVAILABLE RentFlow THEN "Yes" ELSE "No"
    ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record B-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
/*  APPLY 'ROW-LEAVE':U TO BROWSE {&BROWSE-NAME}. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF {&BROWSE-NAME}:NEW-ROW = Yes THEN DO:
    RUN dispatch( 'cancel-record':U ).
    RETURN.
  END.

  DEF VAR i AS INT NO-UNDO.
  
  DO i = 1 TO {&BROWSE-NAME}:NUM-SELECTED-ROWS:
    IF {&BROWSE-NAME}:FETCH-SELECTED-ROW(i) THEN DO:
      FIND FIRST Cashflow WHERE ROWID( CashFlow ) = RentFlow.flow-id EXCLUSIVE-LOCK NO-ERROR.
      DELETE CashFlow.
      DELETE RentFlow.
      IF {&BROWSE-NAME}:DELETE-SELECTED-ROW(i) THEN.
    END.
  END.
  
  IF AVAILABLE RentFlow THEN APPLY 'ENTRY':U TO RentFlow.StartDate IN BROWSE {&BROWSE-NAME}.
  RUN drl-value-changed.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy B-table-Win 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  APPLY 'ROW-LEAVE':U TO {&BROWSE-NAME} IN FRAME {&FRAME-NAME}.
  RUN check-modified( "CLEAR" ).
  RUN assign-flows.
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-flow-query B-table-Win 
PROCEDURE open-flow-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF last-id <> ? THEN RUN assign-if-changed.
  FOR EACH RentFlow: DELETE RentFlow. END.

  CLOSE QUERY {&BROWSE-NAME}.
  curr-id-code = id-code().
  FOR EACH CashFlow NO-LOCK WHERE CashFlow.ScenarioCode = scenario-code
                    AND CashFlow.RelatedKey = curr-id-code:
    IF NOT is-rent-flow( CashFlow.CashFlowType ) THEN NEXT.
    CREATE RentFlow.
    BUFFER-COPY Cashflow TO RentFlow ASSIGN
      RentFlow.flow-id = ROWID( Cashflow )
      RentFlow.RentFlowType = CashFlow.CashFlowType
      RentFlow.AnnualAmount = - (IF CashFlow.CFChangeType = "A" THEN CashFlow.Amount ELSE CashFlow.Amount * 12).
  END.
    
  OPEN QUERY {&BROWSE-NAME} FOR EACH RentFlow NO-LOCK BY RentFlow.StartDate.
  RUN drl-value-changed.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-enable-fields B-table-Win 
PROCEDURE override-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  /* otherwise we get a stupid message saying you can;t set read-only in the browse */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize B-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FIND FIRST RP WHERE RP.ReportID = "Current Scenario"
                 AND RP.UserName = user-name NO-LOCK NO-ERROR.

  IF AVAILABLE(RP) THEN
    scenario-code = RP.Int1.
  ELSE DO:
    FIND FIRST Scenario NO-LOCK.
    MESSAGE "You should select your current scenario" SKIP(1)
            "Scenario" Scenario.ScenarioCode " - " Scenario.Name SKIP
            "selected by default"
             VIEW-AS ALERT-BOX WARNING TITLE "No current scenario".
    scenario-code = Scenario.ScenarioCode.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available B-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  have-records = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RentalSpace"}
  {src/adm/template/snd-list.i "RentFlow"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE top-most-changed B-table-Win 
PROCEDURE top-most-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF INPUT FRAME {&FRAME-NAME} tgl_topmost THEN
    RUN notify( 'set-topmost,container-source':U ).
  ELSE
    RUN notify( 'reset-topmost,container-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION id-code B-table-Win 
FUNCTION id-code RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  Rental Space must be available 
------------------------------------------------------------------------------*/

  RETURN "RSP," + STRING( RentalSpace.PropertyCode, "99999" )
        + ",L," + STRING( 50000 + RentalSpace.Level, "99999" )
        + ",S," + STRING( 50000 + RentalSpace.LevelSequence, "99999" )
        + ",R," + STRING( RentalSpace.RentalSpaceCode )
        .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION is-rent-flow B-table-Win 
FUNCTION is-rent-flow RETURNS LOGICAL
  ( INPUT cf-type AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  IF cf-type = "RENT" OR cf-type = "MRNT" THEN RETURN TRUE.

  RETURN FALSE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION next-number B-table-Win 
FUNCTION next-number RETURNS INTEGER
  ( ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  counter = counter + 1.
  RETURN counter.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


