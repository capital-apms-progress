&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
  File:  
  Description: 

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO.

DEF VAR bank-account-list AS CHAR NO-UNDO.
DEF VAR filter-by AS CHAR NO-UNDO.
DEF VAR sort-by AS CHAR NO-UNDO.

/* variables for seach panel */
DEF VAR cheque-no AS INT NO-UNDO INITIAL ?.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Cheque

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Cheque.ChequeNo Cheque.CreditorCode ~
Cheque.Date Cheque.Amount Cheque.PayeeName Cheque.DateSent ~
Cheque.DatePresented Cheque.PresentedAmount 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table Cheque.DateSent ~
Cheque.DatePresented Cheque.PresentedAmount 
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table Cheque
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table Cheque
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Cheque WHERE ~{&KEY-PHRASE} ~
      AND Cheque.Date > (TODAY - 190) ~
 AND Cheque.DatePresented = ? ~
 AND Cheque.Cancelled <> TRUE ~
 NO-LOCK ~
    BY Cheque.BankAccountCode DESCENDING ~
       BY Cheque.Date DESCENDING ~
        BY Cheque.ChequeNo DESCENDING.
&Scoped-define TABLES-IN-QUERY-br_table Cheque
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Cheque


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
CreditorCode||y|TTPL.Cheque.CreditorCode
ChequeNo||y|TTPL.Cheque.ChequeNo
BankAccountCode|y|y|TTPL.Cheque.BankAccountCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "BankAccountCode",
     Keys-Supplied = "CreditorCode,ChequeNo,BankAccountCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validate-row B-table-Win 
FUNCTION validate-row RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Cheque SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      Cheque.ChequeNo COLUMN-LABEL "Cheque#" FORMAT "999999":U
      Cheque.CreditorCode FORMAT "99999":U
      Cheque.Date COLUMN-LABEL "Produced on" FORMAT "99/99/9999":U
      Cheque.Amount COLUMN-LABEL "Produced for" FORMAT "->>>,>>>,>>9.99":U
      Cheque.PayeeName COLUMN-LABEL "Payee Name" FORMAT "X(50)":U
      Cheque.DateSent COLUMN-LABEL "Cheque Sent" FORMAT "99/99/9999":U
      Cheque.DatePresented COLUMN-LABEL "Presented On" FORMAT "99/99/9999":U
      Cheque.PresentedAmount COLUMN-LABEL "Presented for" FORMAT ">>>,>>>,>>9.99":U
  ENABLE
      Cheque.DateSent
      Cheque.DatePresented
      Cheque.PresentedAmount
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 105.72 BY 17.8
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 16 FGCOLOR 0 FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 19.5
         WIDTH              = 108.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.Cheque"
     _Options          = "NO-LOCK KEY-PHRASE"
     _OrdList          = "TTPL.Cheque.BankAccountCode|no,TTPL.Cheque.Date|no,TTPL.Cheque.ChequeNo|no"
     _Where[1]         = "Cheque.Date > (TODAY - 190)
 AND Cheque.DatePresented = ?
 AND Cheque.Cancelled <> TRUE
"
     _FldNameList[1]   > TTPL.Cheque.ChequeNo
"Cheque.ChequeNo" "Cheque#" ? "integer" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[2]   = TTPL.Cheque.CreditorCode
     _FldNameList[3]   > TTPL.Cheque.Date
"Cheque.Date" "Produced on" ? "date" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[4]   > TTPL.Cheque.Amount
"Cheque.Amount" "Produced for" ? "decimal" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[5]   > TTPL.Cheque.PayeeName
"Cheque.PayeeName" "Payee Name" ? "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[6]   > TTPL.Cheque.DateSent
"Cheque.DateSent" "Cheque Sent" ? "date" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[7]   > TTPL.Cheque.DatePresented
"Cheque.DatePresented" "Presented On" ? "date" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[8]   > TTPL.Cheque.PresentedAmount
"Cheque.PresentedAmount" "Presented for" ? "decimal" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
      APPLY 'ENTRY':U TO DatePresented IN BROWSE {&BROWSE-NAME}.
      RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
  IF {&BROWSE-NAME}:CURRENT-ROW-MODIFIED THEN DO:
    IF NOT validate-row() THEN RETURN NO-APPLY.
  END.
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Cheque.DatePresented
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Cheque.DatePresented br_table _BROWSE-COLUMN B-table-Win
ON RETURN OF Cheque.DatePresented IN BROWSE br_table /* Presented On */
DO:
  APPLY 'TAB':U TO SELF.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

FIND Office WHERE Office.ThisOffice NO-LOCK.
FIND OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "PYMTBANK" NO-LOCK NO-ERROR.

bank-account-list = "".
FOR EACH BankAccount NO-LOCK
       WHERE BankAccount.BankAccountCode <> ? AND BankAccount.AccountName <> ?
       BY Active DESC:
  sort-by = STRING( BankAccount.BankAccountCode, "X(5)") + "- " + BankAccount.AccountName.
  IF AVAILABLE(OfficeControlAccount) AND BankAccount.BankAccountCode = OfficeControlAccount.Description THEN filter-by = sort-by.
  bank-account-list = bank-account-list + "|" + sort-by.
END.
bank-account-list = SUBSTRING( bank-account-list, 2).

RUN set-attribute-list('FilterBy-Style = Combo-Box, FilterBy-Case = ':U + filter-by + ', FilterBy-Options = ':U + bank-account-list ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'BankAccountCode':U THEN DO:
       &Scope KEY-PHRASE Cheque.BankAccountCode eq key-value
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* BankAccountCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query-cases B-table-Win 
PROCEDURE local-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'BankAccountCode':U THEN DO:
       &Scope KEY-PHRASE Cheque.BankAccountCode eq filter-by
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* filter-by */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy B-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE present-cheque B-table-Win 
PROCEDURE present-cheque :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Cheque) THEN RETURN.
  Cheque.DatePresented:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( TODAY - 1 ).
  Cheque.PresentedAmount:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( Cheque.Amount ).
  IF NOT validate-row() THEN
      RETURN.
  RUN dispatch( 'assign-record' ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-all-cheques B-table-Win 
PROCEDURE send-all-cheques :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR test-date AS DATE NO-UNDO.

  RUN notify( 'set-busy,container-source':U ).
  test-date = TODAY - 50.
/*  MESSAGE "Sending cheques for bank >>" + filter-by + "<< since " + STRING(test-date,"99/99/9999") VIEW-AS ALERT-BOX. */
  FOR EACH Cheque WHERE Cheque.BankAccountCode = filter-by
                    AND Cheque.Date > test-date
                    AND Cheque.DateSent = ? EXCLUSIVE-LOCK:
    Cheque.DateSent = TODAY .
    /* MESSAGE "Processing cheque" Cheque.ChequeNo VIEW-AS ALERT-BOX. */
  END.
  RUN dispatch( 'open-query' ).

  RUN notify( 'set-idle,container-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-cheque B-table-Win 
PROCEDURE send-cheque :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Cheque) THEN RETURN.
  Cheque.DateSent:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( TODAY ).
  IF NOT validate-row() THEN
      RETURN.
  RUN dispatch( 'assign-record' ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "CreditorCode" "Cheque" "CreditorCode"}
  {src/adm/template/sndkycas.i "ChequeNo" "Cheque" "ChequeNo"}
  {src/adm/template/sndkycas.i "BankAccountCode" "Cheque" "BankAccountCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Cheque"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE unpresent-cheque B-table-Win 
PROCEDURE unpresent-cheque :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Cheque) THEN RETURN.
  FIND CURRENT Cheque EXCLUSIVE-LOCK.
  Cheque.DatePresented = ?.
  Cheque.PresentedAmount = 0.
  FIND CURRENT Cheque NO-LOCK.
  DISPLAY Cheque.DatePresented Cheque.PresentedAmount WITH BROWSE {&BROWSE-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE unsend-cheque B-table-Win 
PROCEDURE unsend-cheque :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Cheque) THEN RETURN.
  FIND CURRENT Cheque EXCLUSIVE-LOCK.
  Cheque.DateSent = ?.
  FIND CURRENT Cheque NO-LOCK.
  DISPLAY Cheque.DateSent WITH BROWSE {&BROWSE-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-filter AS CHAR NO-UNDO.

  filter-by = TRIM( SUBSTRING( new-filter, 1, 4) ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  key-name = new-key-name.

  IF key-name = "CreditorCode" THEN
    RUN set-attribute-list( 'SortBy-Case = CreditorCode':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value B-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-value AS CHAR NO-UNDO.
  key-value = new-key-value.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SearchBy-Case B-table-Win 
PROCEDURE use-SearchBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-search AS CHAR NO-UNDO.

DEF VAR ch AS CHAR NO-UNDO.

  new-search = TRIM( new-search ).
  ch = SUBSTRING( new-search, 1, 1).
  IF ch >= "0" AND ch <= "9" THEN DO:
    ASSIGN cheque-no = INTEGER( new-search ) NO-ERROR.
  END.
  ELSE IF INDEX( new-search, "/", 1) > 0 THEN DO:
    ASSIGN cheque-no = INTEGER( ENTRY( 2, new-search, "/") )
           filter-by = TRIM( ENTRY( 1, new-search, "/") )
           NO-ERROR.
  END.
  ELSE DO:
    cheque-no = ?.
    filter-by = TRIM( SUBSTRING( new-search, 1, 4) ).
  END.
  RUN dispatch( 'apply-entry':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validate-row B-table-Win 
FUNCTION validate-row RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR presented-date AS DATE NO-UNDO.
DEF VAR presented-amount AS DEC NO-UNDO.
DEF VAR sent-date AS DATE NO-UNDO.
DEF VAR thats-ok AS LOGI INITIAL No NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  presented-date = INPUT BROWSE {&BROWSE-NAME} Cheque.DatePresented.
  presented-amount = INPUT BROWSE {&BROWSE-NAME} Cheque.PresentedAmount.
  sent-date = INPUT BROWSE {&BROWSE-NAME} Cheque.DateSent.
  IF sent-date < Cheque.Date THEN DO:
    MESSAGE "Was the cheque really sent before it was written?!"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK-CANCEL
            TITLE "Cheque sent before being written"
            UPDATE thats-ok.
    IF NOT thats-ok THEN DO:
      APPLY 'ENTRY':U TO DateSent IN BROWSE {&BROWSE-NAME}.
      RETURN No.
    END.
  END.
  IF presented-date < sent-date THEN DO:
    MESSAGE "Was the cheque really presented before it was sent?!"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK-CANCEL
            TITLE "Cheque presented before being sent"
            UPDATE thats-ok.
    IF NOT thats-ok THEN DO:
      APPLY 'ENTRY':U TO DateSent IN BROWSE {&BROWSE-NAME}.
      RETURN No.
    END.
  END.
  IF presented-amount <> Cheque.Amount AND presented-date <> ? THEN DO:
    MESSAGE "Presented amount differs from cheque amount!"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK-CANCEL
            TITLE "Cheque presented for different amount"
            UPDATE thats-ok.
    IF NOT thats-ok THEN DO:
      APPLY 'ENTRY':U TO PresentedAmount IN BROWSE {&BROWSE-NAME}.
      RETURN No.
    END.
  END.
  IF presented-date < Cheque.Date THEN DO:
    MESSAGE "Was the cheque really presented before it was written?!"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK-CANCEL
            TITLE "Cheque presented before being written"
            UPDATE thats-ok.
    IF NOT thats-ok THEN DO:
      APPLY 'ENTRY':U TO DatePresented IN BROWSE {&BROWSE-NAME}.
      RETURN No.
    END.
  END.
  ELSE IF presented-date > Cheque.Date + 200 THEN DO:
    MESSAGE "Cheque presented more than six months after writing."
            VIEW-AS ALERT-BOX WARNING BUTTONS OK-CANCEL
            TITLE "Cheque presented more than six months after writing"
            UPDATE thats-ok.
    IF NOT thats-ok THEN DO:
      APPLY 'ENTRY':U TO DatePresented IN BROWSE {&BROWSE-NAME}.
      RETURN No.
    END.
  END.

END.
  RETURN Yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

