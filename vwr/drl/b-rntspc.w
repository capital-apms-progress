&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR vacant-only AS LOGICAL NO-UNDO INITIAL No.

{inc/topic/tprntspc.i}

DEF VAR tenant-name-1 AS CHAR NO-UNDO INITIAL ?.
DEF VAR tenant-name AS CHAR NO-UNDO INITIAL ?.
DEF VAR rental-1 AS DEC NO-UNDO INITIAL 0.00 FORMAT "->>,>>>,>>9.99" LABEL "Charged Rent".
DEF VAR rental-2 AS DEC NO-UNDO INITIAL 0.00 FORMAT "->>,>>>,>>9.99" LABEL "Contract Rent".
DEF VAR rental-3 AS DEC NO-UNDO INITIAL 0.00 FORMAT "->>,>>9.99" LABEL "Unit Rate".

{inc/ofc-this.i}
{inc/ofc-set.i "Area-Units" "area-units"}
IF NOT AVAILABLE(OfficeSetting) THEN area-units = "Sq.M.".
{inc/ofc-set-l.i "Property-MarketPerUnit" "market-per-unit"}
{inc/ofc-set-l.i "Use-Rent-Charges" "use-rent-charges"}
{inc/ofc-set-l.i "RentalSpace-Estimates" "rentspace-estimates"}
{inc/ofc-set-l.i "Property-ParksPerMonth" "parks-per-month"}
IF NOT AVAILABLE(OfficeSetting) THEN parks-per-month = No.


DEF BUFFER OtherSpace FOR RentalSpace.
DEF VAR area-like-this AS DEC NO-UNDO.
DEF VAR this-area AS DEC NO-UNDO.
DEF VAR in-find-trigger AS LOGI NO-UNDO INITIAL No.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES RentalSpace

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table RentalSpace.Level ~
RentalSpace.LevelSequence RentalSpace.Description RentalSpace.AreaType ~
RentalSpace.AreaStatus tenant-name @ tenant-name RentalSpace.AreaSize ~
rental-1 @ rental-1 ~
(IF market-per-unit THEN (IF RentalSpace.MarketRental > 0 THEN RentalSpace.MarketRental ELSE 100.0) ELSE RentalSpace.ContractedRental) @ rental-2 ~
RentalSpace.OutgoingsPercentage rental-3 @ rental-3 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table RentalSpace.AreaType 
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table RentalSpace
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table RentalSpace
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH RentalSpace WHERE ~{&KEY-PHRASE} ~
      AND (NOT vacant-only OR RentalSpace.AreaStatus = "V") NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table RentalSpace
&Scoped-define FIRST-TABLE-IN-QUERY-br_table RentalSpace


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
TenancyLeaseCode|y|y|ttpl.RentalSpace.TenancyLeaseCode
RentalSpace||y|ttpl.RentalSpace.RentalSpaceCode
RentalSpaceCode||y|TTPL.RentalSpace.RentalSpaceCode
AreaStatus|y|y|ttpl.RentalSpace.AreaStatus
AreaType|y|y|ttpl.RentalSpace.AreaType
PropertyCode|y|y|ttpl.RentalSpace.PropertyCode
NoteCode||y|ttpl.RentalSpace.NoteCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "TenancyLeaseCode,AreaStatus,AreaType,PropertyCode",
     Keys-Supplied = "TenancyLeaseCode,RentalSpace,RentalSpaceCode,AreaStatus,AreaType,PropertyCode,NoteCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Property|y||ttpl.RentalSpace.PropertyCode|yes,ttpl.RentalSpace.Level|yes,ttpl.RentalSpace.LevelSequence|yes
Status|||ttpl.RentalSpace.AreaStatus|yes,ttpl.RentalSpace.PropertyCode|yes,ttpl.RentalSpace.Level|yes,ttpl.RentalSpace.LevelSequence|yes
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Property,Status",
     SortBy-Case = Property':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD to-annual B-table-Win 
FUNCTION to-annual RETURNS DECIMAL
  ( INPUT freq-code AS CHAR, INPUT period-amount AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      RentalSpace SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      RentalSpace.Level COLUMN-LABEL "Lvl" FORMAT "->>9":U
      RentalSpace.LevelSequence FORMAT ">>9":U
      RentalSpace.Description FORMAT "X(26)":U
      RentalSpace.AreaType COLUMN-LABEL "T" FORMAT "X(2)":U
      RentalSpace.AreaStatus COLUMN-LABEL "S" FORMAT "X(2)":U
      tenant-name @ tenant-name COLUMN-LABEL "Name" FORMAT "X(26)":U
      RentalSpace.AreaSize FORMAT "->>,>>9.99":U
      rental-1 @ rental-1
      (IF market-per-unit THEN (IF RentalSpace.MarketRental > 0 THEN RentalSpace.MarketRental ELSE 100.0) ELSE RentalSpace.ContractedRental) @ rental-2
      RentalSpace.OutgoingsPercentage COLUMN-LABEL "O/G %" FORMAT "->>9.99":U
      rental-3 @ rental-3
  ENABLE
      RentalSpace.AreaType
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 97 BY 14.75
         BGCOLOR 15 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 15.05
         WIDTH              = 112.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}
{inc/method/m-charged-rent.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main     = 4
       br_table:MAX-DATA-GUESS IN FRAME F-Main         = 500.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.RentalSpace"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _TblOptList       = ", FIRST OUTER, FIRST OUTER, FIRST OUTER"
     _Where[1]         = "(NOT vacant-only OR RentalSpace.AreaStatus = ""V"")"
     _FldNameList[1]   > ttpl.RentalSpace.Level
"RentalSpace.Level" "Lvl" ? "integer" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > ttpl.RentalSpace.LevelSequence
"RentalSpace.LevelSequence" ? ">>9" "integer" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[3]   > ttpl.RentalSpace.Description
"RentalSpace.Description" ? "X(26)" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[4]   > ttpl.RentalSpace.AreaType
"RentalSpace.AreaType" "T" "X(2)" "character" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[5]   > ttpl.RentalSpace.AreaStatus
"RentalSpace.AreaStatus" "S" "X(2)" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[6]   > "_<CALC>"
"tenant-name @ tenant-name" "Name" "X(26)" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[7]   = ttpl.RentalSpace.AreaSize
     _FldNameList[8]   > "_<CALC>"
"rental-1 @ rental-1" ? ? ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[9]   > "_<CALC>"
"(IF market-per-unit THEN (IF RentalSpace.MarketRental > 0 THEN RentalSpace.MarketRental ELSE 100.0) ELSE RentalSpace.ContractedRental) @ rental-2" ? ? ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[10]   > ttpl.RentalSpace.OutgoingsPercentage
"RentalSpace.OutgoingsPercentage" "O/G %" ? "decimal" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[11]   > "_<CALC>"
"rental-3 @ rental-3" ? ? ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list ('SortBy-Options = Property|Status, SortBy-Case = Property':U ).
RUN set-attribute-list ('FilterBy-Options = All|Vacant':U ).


RentalSpace.AreaSize:LABEL IN BROWSE {&BROWSE-NAME} = area-units.
IF market-per-unit THEN
  rental-2:LABEL IN BROWSE {&BROWSE-NAME} = "Market %age".

/*
 * This trigger code does most of the calculation for the display. Especially
 * If the OfficeSetting "Use-Rent-Charges" is turned on because there is code
 * here to pro-rate a single charge across a set of rental areas.
 */
ON FIND OF RentalSpace DO:
  IF in-find-trigger THEN RETURN.
  tenant-name = ?.
  rental-1 = 0.
  rental-3 = 0.
  IF RentalSpace.AreaStatus = "V" THEN RETURN.
  FIND TenancyLease OF RentalSpace NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(TenancyLease) THEN RETURN.
  IF TenancyLease.LeaseStatus = "PAST" THEN RETURN.
  FIND Tenant OF TenancyLease NO-LOCK NO-ERROR.
  IF AVAILABLE(Tenant) THEN
    tenant-name = Tenant.Name.
  ELSE
    tenant-name = "T" + STRING( TenancyLease.TenantCode, "99999" ) + " not on file!".

  rental-1 = get-charged-rent( RentalSpace.ChargedRental, RentalSpace.ContractedRental, RentalSpace.AreaType, RentalSpace.TenancyLeaseCode ).
/*
  IF rentspace-estimates THEN DO:
    rental-1 = .
  END.
  ELSE IF use-rent-charges THEN DO:
    rental-1 = 0.0 .
    FOR EACH RentCharge OF TenancyLease WHERE RentCharge.RentChargeType = RentalSpace.AreaType NO-LOCK:
      FOR EACH RentChargeLine OF RentCharge WHERE RentChargeLine.RentChargeLineStatus = "C"
                                AND RentChargeLine.StartDate <= TODAY
                                AND (RentChargeLine.EndDate >= TODAY
                                      OR RentChargeLine.EndDate = ?) NO-LOCK:
        rental-2 = to-annual( RentChargeLine.FrequencyCode, RentChargeLine.Amount ).
/*        MESSAGE rental-1 rental-2 RentChargeLine.FrequencyCode RentChargeLine.Amount . */
        rental-1 = rental-1 + rental-2.
      END.
    END.

    /* pro-rata the rent charges for this type of area to the areas of this type */
    this-area = RentalSpace.AreaSize * ((IF RentalSpace.MarketRental = ? THEN 100.0 ELSE RentalSpace.MarketRental) / 100).
    area-like-this = this-area.
    in-find-trigger = Yes.
    FOR EACH OtherSpace WHERE OtherSpace.TenancyLeaseCode = RentalSpace.TenancyLeaseCode
                          AND OtherSpace.PropertyCode = RentalSpace.PropertyCode
                          AND OtherSpace.AreaType = RentalSpace.AreaType
                          AND OtherSpace.RentalSpaceCode <> RentalSpace.RentalSpaceCode
                          NO-LOCK:
      area-like-this = area-like-this + OtherSpace.AreaSize
                     * ((IF OtherSpace.MarketRental > 0 THEN OtherSpace.MarketRental ELSE 100.0) / 100).
    END.
    in-find-trigger = No.
    IF area-like-this <> 0 AND area-like-this <> ? AND area-like-this <> this-area THEN
      rental-1 = rental-1 * (this-area / area-like-this).
  END.
*/

  /* calculate the rate per area */
  rental-3 = (rental-1 / RentalSpace.AreaSize).
  IF RentalSpace.AreaType = "C" THEN
    rental-3 = rental-3 / (IF parks-per-month THEN 12 ELSE 52).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'TenancyLeaseCode':U THEN DO:
       &Scope KEY-PHRASE RentalSpace.TenancyLeaseCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Status':U THEN DO:
           &Scope SORTBY-PHRASE BY RentalSpace.AreaStatus BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* TenancyLeaseCode */
    WHEN 'AreaStatus':U THEN DO:
       &Scope KEY-PHRASE RentalSpace.AreaStatus eq key-value
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Status':U THEN DO:
           &Scope SORTBY-PHRASE BY RentalSpace.AreaStatus BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* AreaStatus */
    WHEN 'AreaType':U THEN DO:
       &Scope KEY-PHRASE RentalSpace.AreaType eq key-value
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Status':U THEN DO:
           &Scope SORTBY-PHRASE BY RentalSpace.AreaStatus BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* AreaType */
    WHEN 'PropertyCode':U THEN DO:
       &Scope KEY-PHRASE RentalSpace.PropertyCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Status':U THEN DO:
           &Scope SORTBY-PHRASE BY RentalSpace.AreaStatus BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* PropertyCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Status':U THEN DO:
           &Scope SORTBY-PHRASE BY RentalSpace.AreaStatus BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize B-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
/* DO WITH FRAME {&FRAME-NAME}: */
  RentalSpace.AreaType:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
/* END. */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR yes-ok AS LOGICAL NO-UNDO INITIAL No.
  /* Code placed here will execute PRIOR to standard behavior. */
  MESSAGE "Really?" SKIP(1) "Delete rental space?"
          VIEW-AS ALERT-BOX QUESTION
          BUTTONS OK-CANCEL TITLE "Confirm Deletion" UPDATE yes-ok.

  /* Dispatch standard ADM method.                             */
  IF yes-ok THEN RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE normal-send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE normal-send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "TenancyLeaseCode" "RentalSpace" "TenancyLeaseCode"}
  {src/adm/template/sndkycas.i "RentalSpace" "RentalSpace" "RentalSpaceCode"}
  {src/adm/template/sndkycas.i "RentalSpaceCode" "RentalSpace" "RentalSpaceCode"}
  {src/adm/template/sndkycas.i "AreaStatus" "RentalSpace" "AreaStatus"}
  {src/adm/template/sndkycas.i "AreaType" "RentalSpace" "AreaType"}
  {src/adm/template/sndkycas.i "PropertyCode" "RentalSpace" "PropertyCode"}
  {src/adm/template/sndkycas.i "NoteCode" "RentalSpace" "NoteCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:  Spoof the normal send key routine
------------------------------------------------------------------------------*/
  {inc/sendkey.i}

  IF key-name = "RentalSpace" AND AVAILABLE(RentalSpace) THEN
    key-value = STRING(RentalSpace.PropertyCode) + "/"
              + STRING(RentalSpace.RentalSpaceCode) .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RentalSpace"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-filter AS CHAR NO-UNDO.
  vacant-only = (new-filter = "Vacant").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION to-annual B-table-Win 
FUNCTION to-annual RETURNS DECIMAL
  ( INPUT freq-code AS CHAR, INPUT period-amount AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert period amount to annual figure
    Notes:  
------------------------------------------------------------------------------*/
  /* short circuit for most cases */
  IF freq-code = "MNTH" THEN RETURN (period-amount * 12.0).

  FIND FrequencyType WHERE FrequencyType.FrequencyCode = freq-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(FrequencyType) THEN RETURN (period-amount * 12.0). /* assume monthly! */

  IF FrequencyType.RepeatUnits BEGINS "M" THEN
    RETURN ((period-amount / DEC(FrequencyType.UnitCount)) * 12.0 ).

  RETURN ((period-amount / DEC(FrequencyType.UnitCount)) * 365.0 ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

