&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR update-to-acct AS CHAR NO-UNDO.

{inc/topic/tpprjbdg.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ProjectBudget AccountSummary

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table ProjectBudget.AccountCode ~
ProjectBudget.Description ~
ProjectBudget.EntityType + STRING( ProjectBudget.EntityCode, '99999') + '-' + STRING( ProjectBudget.EntityAccount, '9999.99') @ update-to-acct ~
ProjectBudget.OriginalBudget ProjectBudget.CommittedBudget ~
ProjectBudget.AgreedVariation ProjectBudget.Adjustment ~
AccountSummary.Balance 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH ProjectBudget WHERE ~{&KEY-PHRASE} NO-LOCK, ~
      FIRST AccountSummary WHERE AccountSummary.EntityType = "J" ~
  AND AccountSummary.EntityCode = ProjectBudget.ProjectCode ~
  AND AccountSummary.AccountCode = ProjectBudget.AccountCode OUTER-JOIN NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table ProjectBudget AccountSummary
&Scoped-define FIRST-TABLE-IN-QUERY-br_table ProjectBudget


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
ProjectCode|y|y|TTPL.ProjectBudget.ProjectCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "ProjectCode",
     Keys-Supplied = "ProjectCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      ProjectBudget, 
      AccountSummary
    FIELDS(AccountSummary.Balance) SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      ProjectBudget.AccountCode
      ProjectBudget.Description FORMAT "X(30)"
      ProjectBudget.EntityType + STRING( ProjectBudget.EntityCode, '99999') + '-' + STRING( ProjectBudget.EntityAccount, '9999.99') @ update-to-acct COLUMN-LABEL "Update to Acct" FORMAT "X(16)"
      ProjectBudget.OriginalBudget COLUMN-LABEL "Original Budget" FORMAT "->,>>>,>>>,>>9.99"
      ProjectBudget.CommittedBudget COLUMN-LABEL "Committed Budget" FORMAT "->,>>>,>>>,>>9.99"
      ProjectBudget.AgreedVariation COLUMN-LABEL "Agreed Variation" FORMAT "->,>>>,>>>,>>9.99"
      ProjectBudget.Adjustment FORMAT "->,>>>,>>>,>>9.99"
      AccountSummary.Balance COLUMN-LABEL "Account Balance" FORMAT "->,>>>,>>>,>>9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 108.57 BY 14.45
         BGCOLOR 15 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 16.8
         WIDTH              = 111.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "ttpl.ProjectBudget,ttpl.AccountSummary WHERE ttpl.ProjectBudget ..."
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _TblOptList       = ", FIRST OUTER USED"
     _JoinCode[2]      = "AccountSummary.EntityType = ""J""
  AND AccountSummary.EntityCode = ProjectBudget.ProjectCode
  AND AccountSummary.AccountCode = ProjectBudget.AccountCode"
     _FldNameList[1]   = TTPL.ProjectBudget.AccountCode
     _FldNameList[2]   > ttpl.ProjectBudget.Description
"ProjectBudget.Description" ? "X(30)" "character" ? ? ? ? ? ? no ?
     _FldNameList[3]   > "_<CALC>"
"ProjectBudget.EntityType + STRING( ProjectBudget.EntityCode, '99999') + '-' + STRING( ProjectBudget.EntityAccount, '9999.99') @ update-to-acct" "Update to Acct" "X(16)" ? ? ? ? ? ? ? no ?
     _FldNameList[4]   > TTPL.ProjectBudget.OriginalBudget
"ProjectBudget.OriginalBudget" "Original Budget" "->,>>>,>>>,>>9.99" "decimal" ? ? ? ? ? ? no ?
     _FldNameList[5]   > TTPL.ProjectBudget.CommittedBudget
"ProjectBudget.CommittedBudget" "Committed Budget" "->,>>>,>>>,>>9.99" "decimal" ? ? ? ? ? ? no ?
     _FldNameList[6]   > TTPL.ProjectBudget.AgreedVariation
"ProjectBudget.AgreedVariation" "Agreed Variation" "->,>>>,>>>,>>9.99" "decimal" ? ? ? ? ? ? no ?
     _FldNameList[7]   > TTPL.ProjectBudget.Adjustment
"ProjectBudget.Adjustment" ? "->,>>>,>>>,>>9.99" "decimal" ? ? ? ? ? ? no ?
     _FldNameList[8]   > ttpl.AccountSummary.Balance
"AccountSummary.Balance" "Account Balance" "->,>>>,>>>,>>9.99" "decimal" ? ? ? ? ? ? no ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'ProjectCode':U THEN DO:
       &Scope KEY-PHRASE ProjectBudget.ProjectCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* ProjectCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(ProjectBudget) THEN RETURN.
  IF NOT AVAILABLE(Project) THEN FIND Project OF ProjectBudget NO-LOCK.

  IF ProjectBudget.AccountCode = Project.EntityAccount THEN DO:
    MESSAGE "Cannot delete default account for project."
            VIEW-AS ALERT-BOX ERROR.
    RETURN.
  END.

  FIND AccountSummary WHERE AccountSummary.EntityType = "J"
                      AND AccountSummary.EntityCode = ProjectBudget.ProjectCode
                      AND AccountSummary.AccountCode = ProjectBudget.AccountCode NO-LOCK NO-ERROR.
  IF AVAILABLE(AccountSummary) AND AccountSummary.Balance <> 0 THEN DO:
    MESSAGE "Cannot delete project budget as balance is non-zero."
        VIEW-AS ALERT-BOX ERROR.
    RETURN.
  END.

  /* Code placed here will execute PRIOR to standard behavior. */
  IF CAN-FIND( FIRST AcctTran WHERE AcctTran.EntityType = "J"
                     AND AcctTran.EntityCode = ProjectBudget.ProjectCode
                     AND AcctTran.AccountCode = ProjectBudget.AccountCode) THEN DO:
    DEF VAR do-it AS LOGICAL INITIAL No NO-UNDO.
    
    MESSAGE "Transactions have been posted to this account!" SKIP(1)
            "Do you really want to delete it?"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK-CANCEL
            TITLE "Account has Posted Transactions"
            UPDATE do-it.
    IF NOT do-it THEN RETURN.
  END.

  DO TRANSACTION ON ERROR UNDO, LEAVE:
    FOR EACH AccountBalance WHERE AccountBalance.EntityType = "J"
                      AND AccountBalance.EntityCode = ProjectBudget.ProjectCode
                      AND AccountBalance.AccountCode = ProjectBudget.AccountCode:
      IF CAN-FIND( FIRST AcctTran OF AccountBalance ) THEN NEXT.
      DELETE AccountBalance.
    END.
    FIND CURRENT ProjectBudget EXCLUSIVE-LOCK.
    ProjectBudget.OriginalBudget = 0 .
    DELETE ProjectBudget.
  END.

  RUN check-modified( 'clear':U ).
  RUN dispatch( 'open-query':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "ProjectCode" "ProjectBudget" "ProjectCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ProjectBudget"}
  {src/adm/template/snd-list.i "AccountSummary"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


