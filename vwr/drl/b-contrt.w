&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------

  File:  

  Description: from BROWSER.W - Basic SmartBrowser Object Template

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
{inc/topic/tpcontrt.i}

DEF VAR filter-list AS CHAR NO-UNDO.
DEF VAR filter-by AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Contract Creditor

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Contract.PropertyCode ~
Contract.ServiceType Contract.CreditorCode Creditor.Name ~
Contract.FrequencyCode Contract.AnnualEstimate Contract.StartDate ~
Contract.ReviewDate Contract.EndDate Contract.ContractReference 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table Contract.PropertyCode 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}PropertyCode ~{&FP2}PropertyCode ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table Contract
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table Contract
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Contract WHERE ~{&KEY-PHRASE} ~
      AND (filter-by = "All") OR (filter-by = Contract.ServiceType) NO-LOCK, ~
      EACH Creditor WHERE Creditor.CreditorCode = Contract.CreditorCode NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table Contract Creditor
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Contract


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
CreditorCode|y|y|ttpl.Contract.CreditorCode
PropertyCode|y|y|ttpl.Contract.PropertyCode
AuditRecordId||y|ttpl.Contract.AuditRecordId
FrequencyCode||y|ttpl.Contract.FrequencyCode
StartDate||y|ttpl.Contract.StartDate
NoteCode||y|ttpl.Contract.NoteCode
ServiceType|y|y|ttpl.Contract.ServiceType
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "CreditorCode,PropertyCode,ServiceType",
     Keys-Supplied = "CreditorCode,PropertyCode,AuditRecordId,FrequencyCode,StartDate,NoteCode,ServiceType"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Property|y||ttpl.Contract.PropertyCode|yes,ttpl.Contract.ServiceType|yes
Creditor|||ttpl.Contract.CreditorCode|yes,ttpl.Contract.PropertyCode|yes
Review|||ttpl.Contract.ReviewDate|yes
Service|||ttpl.Contract.ServiceType|yes,ttpl.Contract.PropertyCode|yes
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Property,Creditor,Review,Service",
     Sort-Case = Property':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Contract, 
      Creditor SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      Contract.PropertyCode COLUMN-LABEL "Prop." FORMAT ">99999"
      Contract.ServiceType
      Contract.CreditorCode COLUMN-LABEL "Cred." FORMAT ">99999"
      Creditor.Name COLUMN-LABEL "Creditor Name" FORMAT "X(35)"
      Contract.FrequencyCode COLUMN-LABEL "Freqcy"
      Contract.AnnualEstimate
      Contract.StartDate COLUMN-LABEL "  Start  Date"
      Contract.ReviewDate COLUMN-LABEL "Review Date"
      Contract.EndDate COLUMN-LABEL " Finish  Date"
      Contract.ContractReference FORMAT "X(35)"
  ENABLE
      Contract.PropertyCode
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 98.86 BY 15
         BGCOLOR 16 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 17.05
         WIDTH              = 119.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main = 3
       br_table:MAX-DATA-GUESS IN FRAME F-Main     = 2000.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.Contract,TTPL.Creditor WHERE TTPL.Contract ..."
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _Where[1]         = "(filter-by = ""All"") OR (filter-by = Contract.ServiceType)"
     _JoinCode[2]      = "Creditor.CreditorCode = Contract.CreditorCode"
     _FldNameList[1]   > ttpl.Contract.PropertyCode
"Contract.PropertyCode" "Prop." ">99999" "integer" ? ? ? ? ? ? yes ?
     _FldNameList[2]   = ttpl.Contract.ServiceType
     _FldNameList[3]   > ttpl.Contract.CreditorCode
"Contract.CreditorCode" "Cred." ">99999" "integer" ? ? ? ? ? ? no ?
     _FldNameList[4]   > ttpl.Creditor.Name
"Creditor.Name" "Creditor Name" "X(35)" "character" ? ? ? ? ? ? no ?
     _FldNameList[5]   > ttpl.Contract.FrequencyCode
"Contract.FrequencyCode" "Freqcy" ? "character" ? ? ? ? ? ? no ?
     _FldNameList[6]   = ttpl.Contract.AnnualEstimate
     _FldNameList[7]   > ttpl.Contract.StartDate
"Contract.StartDate" "  Start  Date" ? "date" ? ? ? ? ? ? no ?
     _FldNameList[8]   > ttpl.Contract.ReviewDate
"Contract.ReviewDate" "Review Date" ? "date" ? ? ? ? ? ? no ?
     _FldNameList[9]   > ttpl.Contract.EndDate
"Contract.EndDate" " Finish  Date" ? "date" ? ? ? ? ? ? no ?
     _FldNameList[10]   > ttpl.Contract.ContractReference
"Contract.ContractReference" ? "X(35)" "character" ? ? ? ? ? ? no ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list( 'Note-Height = 3.5':U ).
RUN set-attribute-list ('SortBy-Options = Property|Creditor|Service|Review, SortBy-Case = Property':U ).
filter-list = "All".
FOR EACH ServiceType NO-LOCK:
  filter-list = filter-list + "|" + STRING( ServiceType.ServiceType, "X(5)") + "- " + ServiceType.Description.
END.
RUN set-attribute-list ('FilterBy-Options = ':U + filter-list + ', FilterBy-Case = All, FilterBy-Style = Combo-Box':U ).

Contract.PropertyCode:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'CreditorCode':U THEN DO:
       &Scope KEY-PHRASE Contract.CreditorCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.PropertyCode BY Contract.ServiceType
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Creditor':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.CreditorCode BY Contract.PropertyCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Review':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.ReviewDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Service':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.ServiceType BY Contract.PropertyCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* CreditorCode */
    WHEN 'PropertyCode':U THEN DO:
       &Scope KEY-PHRASE Contract.PropertyCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.PropertyCode BY Contract.ServiceType
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Creditor':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.CreditorCode BY Contract.PropertyCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Review':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.ReviewDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Service':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.ServiceType BY Contract.PropertyCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* PropertyCode */
    WHEN 'ServiceType':U THEN DO:
       &Scope KEY-PHRASE Contract.ServiceType eq key-value
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.PropertyCode BY Contract.ServiceType
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Creditor':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.CreditorCode BY Contract.PropertyCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Review':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.ReviewDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Service':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.ServiceType BY Contract.PropertyCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* ServiceType */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.PropertyCode BY Contract.ServiceType
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Creditor':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.CreditorCode BY Contract.PropertyCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Review':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.ReviewDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Service':U THEN DO:
           &Scope SORTBY-PHRASE BY Contract.ServiceType BY Contract.PropertyCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query B-table-Win 
PROCEDURE local-open-query :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR contract-id AS RECID NO-UNDO.

  /* Code placed here will execute PRIOR to standard behavior. */
  IF AVAILABLE(Contract) THEN contract-id = RECID(Contract).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  REPOSITION {&BROWSE-NAME} TO RECID contract-id NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "CreditorCode" "Contract" "CreditorCode"}
  {src/adm/template/sndkycas.i "PropertyCode" "Contract" "PropertyCode"}
  {src/adm/template/sndkycas.i "AuditRecordId" "Contract" "AuditRecordId"}
  {src/adm/template/sndkycas.i "FrequencyCode" "Contract" "FrequencyCode"}
  {src/adm/template/sndkycas.i "StartDate" "Contract" "StartDate"}
  {src/adm/template/sndkycas.i "NoteCode" "Contract" "NoteCode"}
  {src/adm/template/sndkycas.i "ServiceType" "Contract" "ServiceType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Contract"}
  {src/adm/template/snd-list.i "Creditor"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-filterby-case B-table-Win 
PROCEDURE use-filterby-case :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-val AS CHAR NO-UNDO.

  filter-by = TRIM( SUBSTRING( new-val, 1, 4)).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


