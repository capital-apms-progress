&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

{inc/topic/tpacttrn.i}

DEF VAR d-entity AS CHAR FORMAT "X(8)" COLUMN-LABEL " Entity" NO-UNDO.
DEF VAR d-date AS CHAR FORMAT "X(12)" COLUMN-LABEL "   Date" NO-UNDO.
DEF VAR d-reference AS CHAR COLUMN-LABEL "Reference" FORMAT "X(14)" NO-UNDO.
DEF VAR d-scription AS CHAR COLUMN-LABEL "Description" FORMAT "X(70)" NO-UNDO.

DEF VAR doc-code LIKE Document.DocumentCode NO-UNDO.
DEF VAR account-disabled AS LOGICAL NO-UNDO INITIAL Yes.

DEF VAR mod-desc-rights AS LOGICAL NO-UNDO INITIAL No.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Document AcctTran

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table ~
AcctTran.EntityType + STRING( AcctTran.EntityCode, '99999') @ d-entity ~
AcctTran.AccountCode STRING( AcctTran.Date, '99/99/9999') @ d-date ~
(IF AcctTran.Reference = '' THEN Document.Reference ELSE AcctTran.Reference) @ d-reference ~
AcctTran.Amount ~
(IF AcctTran.Description = '' THEN Document.Description ELSE AcctTran.Description) @ d-scription 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table AcctTran.AccountCode 
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table AcctTran
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table AcctTran
&Scoped-define QUERY-STRING-br_table FOR EACH Document WHERE ~{&KEY-PHRASE} ~
      AND Document.DocumentCode = doc-code NO-LOCK, ~
      EACH AcctTran OF Document NO-LOCK ~
    ~{&SORTBY-PHRASE}
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Document WHERE ~{&KEY-PHRASE} ~
      AND Document.DocumentCode = doc-code NO-LOCK, ~
      EACH AcctTran OF Document NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table Document AcctTran
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Document
&Scoped-define SECOND-TABLE-IN-QUERY-br_table AcctTran


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
BatchCode|y|y|ttpl.Document.BatchCode
AccountCode||y|ttpl.AcctTran.AccountCode
EntityType||y|ttpl.AcctTran.EntityType
EntityCode||y|ttpl.AcctTran.EntityCode
ClosingGroup||y|ttpl.AcctTran.ClosingGroup
DocumentCode||y|ttpl.Document.DocumentCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "BatchCode",
     Keys-Supplied = "BatchCode,AccountCode,EntityType,EntityCode,ClosingGroup,DocumentCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Document, 
      AcctTran SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      AcctTran.EntityType + STRING( AcctTran.EntityCode, '99999') @ d-entity COLUMN-LABEL " Entity" FORMAT "X(8)":U
      AcctTran.AccountCode FORMAT "9999.99":U
      STRING( AcctTran.Date, '99/99/9999') @ d-date COLUMN-LABEL "   Date" FORMAT "X(12)":U
      (IF AcctTran.Reference = '' THEN Document.Reference ELSE AcctTran.Reference) @ d-reference COLUMN-LABEL "Reference" FORMAT "X(15)":U
      AcctTran.Amount FORMAT "->>>,>>>,>>9.99":U
      (IF AcctTran.Description = '' THEN Document.Description ELSE AcctTran.Description) @ d-scription COLUMN-LABEL "Description" FORMAT "X(70)":U
  ENABLE
      AcctTran.AccountCode
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 99 BY 12
         BGCOLOR 16 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 12.4
         WIDTH              = 102.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main     = 3.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "ttpl.Document,ttpl.AcctTran OF ttpl.Document"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _Where[1]         = "Document.DocumentCode = doc-code"
     _FldNameList[1]   > "_<CALC>"
"AcctTran.EntityType + STRING( AcctTran.EntityCode, '99999') @ d-entity" " Entity" "X(8)" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > ttpl.AcctTran.AccountCode
"AcctTran.AccountCode" ? ? "decimal" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[3]   > "_<CALC>"
"STRING( AcctTran.Date, '99/99/9999') @ d-date" "   Date" "X(12)" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[4]   > "_<CALC>"
"(IF AcctTran.Reference = '' THEN Document.Reference ELSE AcctTran.Reference) @ d-reference" "Reference" "X(15)" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[5]   = ttpl.AcctTran.Amount
     _FldNameList[6]   > "_<CALC>"
"(IF AcctTran.Description = '' THEN Document.Description ELSE AcctTran.Description) @ d-scription" "Description" "X(70)" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON F2 OF br_table IN FRAME F-Main
DO:
  RUN edit-transaction-description.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON MOUSE-SELECT-DBLCLICK OF br_table IN FRAME F-Main
DO:
  RUN special-enable-fields.
  APPLY 'ENTRY':U TO AcctTran.AccountCode IN BROWSE {&BROWSE-NAME}.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-DISPLAY OF br_table IN FRAME F-Main
DO:
  IF AVAILABLE(AcctTran) THEN DO:
    DEFINE VARIABLE fcolor AS INTEGER INITIAL ? NO-UNDO.
    DEFINE VARIABLE bcolor AS INTEGER INITIAL ? NO-UNDO.
    
    IF EntityType = "L" THEN       ASSIGN fcolor = 0 bcolor = ?.
    ELSE IF EntityType = "P" THEN  ASSIGN fcolor = 1 bcolor = ?.
    ELSE IF EntityType = "T" THEN  ASSIGN fcolor = 2 bcolor = ?.
    ELSE IF EntityType = "J" THEN  ASSIGN fcolor = 6 bcolor = ?.
    ELSE IF EntityType = "C" THEN  ASSIGN fcolor = 3 bcolor = ?.
    ELSE IF EntityType = "F" THEN  ASSIGN fcolor = 5 bcolor = ?.

    ASSIGN
      d-entity:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
      AccountCode:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
      d-date:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
      d-reference:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
      Amount:FGCOLOR IN BROWSE {&BROWSE-NAME} = ( IF Amount < 0 THEN 12 ELSE 0 )
      d-scription:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
    .
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME AcctTran.AccountCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL AcctTran.AccountCode br_table _BROWSE-COLUMN B-table-Win
ON LEAVE OF AcctTran.AccountCode IN BROWSE br_table /* Account */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:
DEF VAR old-code AS DEC NO-UNDO.
DEF VAR new-code AS DEC NO-UNDO.
  new-code = INPUT BROWSE {&BROWSE-NAME} AcctTran.AccountCode.
  old-code = AcctTran.AccountCode.
  IF new-code = old-code THEN RETURN.

DEF BUFFER AltTran FOR AcctTran.
  IF NOT CAN-FIND( FIRST AltTran OF Document
                         WHERE AltTran.AccountCode = old-code
                            AND ROWID(AcctTran) <> ROWID(AltTran)) THEN RETURN.

  /* OK, so there are others in this document coded to the same account,
   * and the user has changed the account code
   */
DEF VAR do-it AS LOGICAL NO-UNDO INITIAL Yes.
  MESSAGE "There are other transactions within this document" SKIP
          "which also code to account" STRING( old-code, "9999.99") SKIP(1)
          "Do you want to recode them to" STRING( new-code, "9999.99")  "as well?"
          VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO-CANCEL
          TITLE "Change Multiple Transactions?"
          UPDATE do-it.

  IF do-it = ? THEN DO:
    AcctTran.AccountCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( old-code, "9999.99").
    RETURN.
  END.
  ELSE IF do-it = Yes THEN DO TRANSACTION:
    FOR EACH AltTran OF Document WHERE AltTran.AccountCode = old-code EXCLUSIVE-LOCK:
      AltTran.AccountCode = new-code.
    END.
    {&BROWSE-NAME}:REFRESH().
    SELF:MODIFIED = No.
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list ( 'Link-To-1 = vwr/mnt/v-actdoc-viewer.w|Record':U ).

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'BatchCode':U THEN DO:
       &Scope KEY-PHRASE Document.BatchCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* BatchCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE edit-transaction-description B-table-Win 
PROCEDURE edit-transaction-description :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT mod-desc-rights THEN RETURN.

DEF VAR new-description AS CHAR NO-UNDO.
DEF VAR old-description AS CHAR NO-UNDO.
  new-description = AcctTran.Description.
  IF new-description = "" OR new-description = ? THEN new-description = Document.Description.
  old-description = new-description.

  RUN vwr/mnt/d-transaction.w( INPUT-OUTPUT new-description ).

  IF new-description <> old-description THEN DO TRANSACTION:
    IF AcctTran.Description = "" OR AcctTran.Description = ? THEN DO:
      FIND CURRENT Document EXCLUSIVE-LOCK.
      Document.Description = new-description.
      FIND CURRENT Document NO-LOCK.
    END.
    ELSE DO:
      FIND CURRENT AcctTran EXCLUSIVE-LOCK.
      AcctTran.Description = new-description.
      FIND CURRENT AcctTran NO-LOCK.
    END.
    RUN dispatch( 'open-query':U ).
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize B-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  ASSIGN AcctTran.Account:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
  RUN get-rights IN sys-mgr( "B-ACTDOC", "MODIFY-DESCRIPTION", OUTPUT mod-desc-rights).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query-cases B-table-Win 
PROCEDURE local-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR attribute-value AS CHAR NO-UNDO.
DEF VAR record-source-handle AS HANDLE NO-UNDO.

  /* Code placed here will execute PRIOR to standard behavior. */
  RUN get-link-handle IN adm-broker-hdl (THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT attribute-value) NO-ERROR.
  IF attribute-value = "":U THEN RETURN. /* There's no active record source */
  ASSIGN record-source-handle = WIDGET-HANDLE(ENTRY(1,attribute-value)).

  RUN send-key IN record-source-handle( "DocumentCode", OUTPUT doc-code).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query-cases':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "BatchCode" "Document" "BatchCode"}
  {src/adm/template/sndkycas.i "AccountCode" "AcctTran" "AccountCode"}
  {src/adm/template/sndkycas.i "EntityType" "AcctTran" "EntityType"}
  {src/adm/template/sndkycas.i "EntityCode" "AcctTran" "EntityCode"}
  {src/adm/template/sndkycas.i "ClosingGroup" "AcctTran" "ClosingGroup"}
  {src/adm/template/sndkycas.i "DocumentCode" "Document" "DocumentCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Document"}
  {src/adm/template/snd-list.i "AcctTran"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE special-enable-fields B-table-Win 
PROCEDURE special-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/
DEF VAR rights AS LOGICAL NO-UNDO.

  RUN get-rights IN sys-mgr( "B-ACTDOC", "MODIFY-ACCOUNT", OUTPUT rights).
  account-disabled = NOT rights.
  ASSIGN AcctTran.Account:READ-ONLY IN BROWSE {&BROWSE-NAME} = account-disabled.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

