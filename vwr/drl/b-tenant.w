&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

{inc/topic/tptenant.i}

DEF VAR related-entity AS CHAR FORMAT "X(8)" COLUMN-LABEL "Entity" NO-UNDO.
DEF VAR tenant-balance AS DEC  FORMAT '->,>>>,>>9.99' COLUMN-LABEL "Balance" NO-UNDO.
DEF VAR active-flag AS LOGI NO-UNDO.
DEF VAR filter-at AS CHAR NO-UNDO.

DEF VAR ext-key-name AS CHAR NO-UNDO.
DEF VAR ext-key-value AS CHAR NO-UNDO.
DEF VAR ext-et AS CHAR NO-UNDO.
DEF VAR ext-ec AS INT  NO-UNDO.

DEF VAR gap-3 AS CHAR FORMAT "X" LABEL "" NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Tenant AccountSummary

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Tenant.TenantCode Tenant.Active ~
Tenant.EntityType + STRING( Tenant.EntityCode, IF Tenant.EntityType = 'L' THEN '999' ELSE '99999' ) @ related-entity ~
gap-3 @ gap-3 Tenant.Name ~
IF AVAILABLE AccountSUmmary THEN AccountSummary.Balance ELSE 0.00 @ tenant-balance 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table Tenant.Active 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}Active ~{&FP2}Active ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table Tenant
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table Tenant
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Tenant WHERE ~{&KEY-PHRASE} ~
      AND ( active-flag = ? OR Tenant.Active = active-flag ) AND ~
Tenant.Name MATCHES filter-at NO-LOCK, ~
      EACH AccountSummary WHERE AccountSummary.EntityType = 'T' ~
  AND AccountSummary.EntityCode = Tenant.TenantCode ~
  AND AccountSummary.AccountCode = sundry-debtors OUTER-JOIN NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table Tenant AccountSummary
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Tenant


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
TenantCode||y|TTPL.Tenant.TenantCode
EntityType||y|TTPL.Tenant.TenantCode
EntityCode||y|TTPL.Tenant.TenantCode
NoteCode||y|TTPL.Tenant.NoteCode
Entity|y|y|ttpl.Tenant.EntityCode
AccountSummary||y|ttpl.Tenant.TenantCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "Entity",
     Keys-Supplied = "TenantCode,EntityType,EntityCode,NoteCode,Entity,AccountSummary"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Name|y||TTPL.Tenant.Name|yes
Code|||TTPL.Tenant.TenantCode|yes
Property|||TTPL.Tenant.EntityType|no,TTPL.Tenant.EntityCode|yes
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Name,Code,Property",
     SortBy-Case = Name':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Tenant, 
      AccountSummary SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      Tenant.TenantCode COLUMN-LABEL "Tenant" FORMAT ">99999"
      Tenant.Active COLUMN-LABEL "A" FORMAT "Y/N"
      Tenant.EntityType + STRING( Tenant.EntityCode, IF Tenant.EntityType = 'L' THEN '999' ELSE '99999' ) @ related-entity
      gap-3 @ gap-3
      Tenant.Name FORMAT "X(70)"
      IF AVAILABLE AccountSUmmary THEN AccountSummary.Balance ELSE 0.00 @ tenant-balance
  ENABLE
      Tenant.Active
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 79.43 BY 13.3
         BGCOLOR 15 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 16
         WIDTH              = 84.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.Tenant,TTPL.AccountSummary WHERE TTPL.Tenant ..."
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _TblOptList       = ", OUTER"
     _Where[1]         = "( active-flag = ? OR Tenant.Active = active-flag ) AND
Tenant.Name MATCHES filter-at"
     _JoinCode[2]      = "AccountSummary.EntityType = 'T'
  AND AccountSummary.EntityCode = Tenant.TenantCode
  AND AccountSummary.AccountCode = sundry-debtors"
     _FldNameList[1]   > TTPL.Tenant.TenantCode
"Tenant.TenantCode" "Tenant" ">99999" "integer" ? ? ? ? ? ? no ?
     _FldNameList[2]   > TTPL.Tenant.Active
"Tenant.Active" "A" "Y/N" "logical" ? ? ? ? ? ? yes ?
     _FldNameList[3]   > "_<CALC>"
"Tenant.EntityType + STRING( Tenant.EntityCode, IF Tenant.EntityType = 'L' THEN '999' ELSE '99999' ) @ related-entity" ? ? ? ? ? ? ? ? ? no ?
     _FldNameList[4]   > "_<CALC>"
"gap-3 @ gap-3" ? ? ? ? ? ? ? ? ? no ?
     _FldNameList[5]   > TTPL.Tenant.Name
"Tenant.Name" ? "X(70)" "character" ? ? ? ? ? ? no ?
     _FldNameList[6]   > "_<CALC>"
"IF AVAILABLE AccountSUmmary THEN AccountSummary.Balance ELSE 0.00 @ tenant-balance" ? ? ? ? ? ? ? ? ? no ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */
Tenant.Active:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.

RUN set-attribute-list( 'Note-Height = 3':U ).
RUN set-attribute-list( 'FilterBy-Options = All|Active|Inactive, FilterBy-Case = Active':U ).
RUN set-attribute-list( 'Filter-Value = ' ).
RUN set-attribute-list( 'SearchBy-Case = ' ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'Entity':U THEN DO:
       &Scope KEY-PHRASE Tenant.EntityCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Name':U THEN DO:
           &Scope SORTBY-PHRASE BY Tenant.Name
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Code':U THEN DO:
           &Scope SORTBY-PHRASE BY Tenant.TenantCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY Tenant.EntityType DESCENDING BY Tenant.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* Entity */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Name':U THEN DO:
           &Scope SORTBY-PHRASE BY Tenant.Name
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Code':U THEN DO:
           &Scope SORTBY-PHRASE BY Tenant.TenantCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY Tenant.EntityType DESCENDING BY Tenant.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF CAN-FIND( FIRST AcctTran WHERE AcctTran.EntityType = "T" AND
                        AcctTran.EntityCode = Tenant.TenantCode ) THEN DO:
    MESSAGE "Tenant with transactions cannot be deleted"
            VIEW-AS ALERT-BOX ERROR TITLE "Tenant has transactions".
    RETURN.
  END.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query-cases B-table-Win 
PROCEDURE local-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:  Handle cases with multi-part keys
------------------------------------------------------------------------------*/

  IF ext-key-name = "Entity" OR ext-key-name = "PropertyCode" OR  ext-key-name = "CompanyCode" THEN DO:
    &Scope KEY-PHRASE Tenant.EntityType eq ext-et AND Tenant.EntityCode eq ext-ec
    RUN get-attribute ('SortBy-Case':U).
    CASE RETURN-VALUE:
      WHEN 'Name':U THEN DO:
        &Scope SORTBY-PHRASE BY Tenant.Name
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      WHEN 'Code':U THEN DO:
        &Scope SORTBY-PHRASE BY Tenant.TenantCode
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      WHEN 'Property':U THEN DO:
        &Scope SORTBY-PHRASE BY Tenant.EntityType DESCENDING BY Tenant.EntityCode
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      OTHERWISE DO:
        &Undefine SORTBY-PHRASE
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END. /* OTHERWISE...*/
    END CASE.
  END.
  ELSE
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query-cases':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE normal-send-key B-table-Win adm/support/_key-snd.p
PROCEDURE normal-send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "TenantCode" "Tenant" "TenantCode"}
  {src/adm/template/sndkycas.i "EntityType" "Tenant" "TenantCode"}
  {src/adm/template/sndkycas.i "EntityCode" "Tenant" "TenantCode"}
  {src/adm/template/sndkycas.i "NoteCode" "Tenant" "NoteCode"}
  {src/adm/template/sndkycas.i "Entity" "Tenant" "EntityCode"}
  {src/adm/template/sndkycas.i "AccountSummary" "Tenant" "TenantCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-statement B-table-Win 
PROCEDURE print-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

  IF NOT AVAILABLE(Tenant) THEN RETURN.

  report-options = "Tenant,"
                 + STRING( Tenant.EntityCode ) + ","      /* property code */
                 + STRING( Tenant.TenantCode ) + ","      /* tenant code */
                 + "All,"
                 + "Yes,"      /* open                      */
                 + "No,"       /* include if inactive       */
                 + ","         /* from month                */
                 + ","         /* to month                  */
                 + "Yes,"      /* summarise part-posted     */
                 + "Yes," .    /* include if zero balance   */

{inc/bq-do.i "process/report/statemnt.p" "report-options" "No"}
MESSAGE "Statement Printed".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  {inc/sendkey.i}

  IF NOT AVAILABLE(Tenant) THEN RETURN.

  IF key-name = "Entity" THEN
    key-value = "T/" + STRING(Tenant.TenantCode).
  ELSE IF key-name = "AccountSummary" THEN
    key-value = "T/" + STRING(Tenant.TenantCode) + "/" + STRING(sundry-debtors).
  ELSE IF key-name = "EntityType" THEN
    key-value = "T".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Tenant"}
  {src/adm/template/snd-list.i "AccountSummary"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Filter-Value B-table-Win 
PROCEDURE use-Filter-Value :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-filter AS CHAR NO-UNDO.
  filter-at = new-filter + "*".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-case AS CHAR NO-UNDO.

  IF new-case = "All" THEN active-flag = ?.
  ELSE active-flag = ( new-case = "Active" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-name AS CHAR NO-UNDO.
  ext-key-name = new-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value B-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-value AS CHAR NO-UNDO.

  ext-key-value = new-value.

  IF ext-key-name = "Entity" AND NUM-ENTRIES(new-value,"/") > 1 THEN ASSIGN
    ext-et = ENTRY(1,new-value,"/")
    ext-ec = INT(ENTRY(2,new-value,"/")).
  ELSE IF ext-key-name = "PropertyCode" THEN ASSIGN
    ext-et = "P"
    ext-ec = INT(new-value).
  ELSE IF ext-key-name = "CompanyCode" THEN ASSIGN
    ext-et = "L"
    ext-ec = INT(new-value).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SearchBy-Case B-table-Win 
PROCEDURE use-SearchBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-filter AS CHAR NO-UNDO.
  filter-at = new-filter + "*".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


