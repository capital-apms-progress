&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&SCOPED-DEFINE ALL-TYPES "All Types"

DEF VAR cash-flow-type AS CHAR NO-UNDO INITIAL "".
DEF VAR flow-type-list AS CHAR NO-UNDO.
DEF VAR gap-1 AS CHAR NO-UNDO FORMAT "X" LABEL "".
DEF VAR gap-2 AS CHAR NO-UNDO FORMAT "X" LABEL "".
DEF VAR gap-3 AS CHAR NO-UNDO FORMAT "X" LABEL "".

DEF VAR start-date AS CHAR NO-UNDO FORMAT "X(12)" LABEL "Start".
DEF VAR end-date   AS CHAR NO-UNDO FORMAT "X(12)" LABEL "Finish".

DEF VAR entity-type AS CHAR NO-UNDO INITIAL "".
DEF VAR entity-code AS INT NO-UNDO INITIAL ?.
DEF VAR account-code AS DEC NO-UNDO INITIAL ?.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES CashFlow

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table CashFlow.EntityType ~
CashFlow.EntityCode CashFlow.AccountCode CashFlow.CashFlowType ~
CashFlow.StartDate CashFlow.EndDate CashFlow.FrequencyCode CashFlow.Amount ~
CashFlow.Description 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table CashFlow.Description 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table CashFlow
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table CashFlow
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH CashFlow WHERE ~{&KEY-PHRASE} ~
      AND CashFlow.CashFlowType BEGINS cash-flow-type NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table CashFlow
&Scoped-define FIRST-TABLE-IN-QUERY-br_table CashFlow


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
ScenarioCode|y|y|TTPL.CashFlow.ScenarioCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "ScenarioCode",
     Keys-Supplied = "ScenarioCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Date|y||TTPL.CashFlow.ScenarioCode|yes,TTPL.CashFlow.EntityType|yes,TTPL.CashFlow.EntityCode|yes,TTPL.CashFlow.AccountCode|yes,TTPL.CashFlow.RelatedKey|yes,TTPL.CashFlow.StartDate|yes
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Date",
     SortBy-Case = Date':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      CashFlow SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      CashFlow.EntityType COLUMN-LABEL "T" FORMAT "X"
      CashFlow.EntityCode FORMAT "Z99999"
      CashFlow.AccountCode
      CashFlow.CashFlowType COLUMN-LABEL "   Type"
      CashFlow.StartDate COLUMN-LABEL "   Start Date"
      CashFlow.EndDate COLUMN-LABEL "  Finish Date"
      CashFlow.FrequencyCode COLUMN-LABEL "Freq'cy"
      CashFlow.Amount FORMAT "->>,>>>,>>9.99"
      CashFlow.Description FORMAT "X(120)"
  ENABLE
      CashFlow.Description
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 93.72 BY 16.4
         BGCOLOR 15 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 16.4
         WIDTH              = 97.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.CashFlow"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _TblOptList       = ","
     _Where[1]         = "CashFlow.CashFlowType BEGINS cash-flow-type"
     _FldNameList[1]   > TTPL.CashFlow.EntityType
"CashFlow.EntityType" "T" "X" "character" ? ? ? ? ? ? no ?
     _FldNameList[2]   > TTPL.CashFlow.EntityCode
"CashFlow.EntityCode" ? "Z99999" "integer" ? ? ? ? ? ? no ?
     _FldNameList[3]   = TTPL.CashFlow.AccountCode
     _FldNameList[4]   > TTPL.CashFlow.CashFlowType
"CashFlow.CashFlowType" "   Type" ? "character" ? ? ? ? ? ? no ?
     _FldNameList[5]   > TTPL.CashFlow.StartDate
"CashFlow.StartDate" "   Start Date" ? "date" ? ? ? ? ? ? no ?
     _FldNameList[6]   > TTPL.CashFlow.EndDate
"CashFlow.EndDate" "  Finish Date" ? "date" ? ? ? ? ? ? no ?
     _FldNameList[7]   > TTPL.CashFlow.FrequencyCode
"CashFlow.FrequencyCode" "Freq'cy" ? "character" ? ? ? ? ? ? no ?
     _FldNameList[8]   > TTPL.CashFlow.Amount
"CashFlow.Amount" ? "->>,>>>,>>9.99" "decimal" ? ? ? ? ? ? no ?
     _FldNameList[9]   > TTPL.CashFlow.Description
"CashFlow.Description" ? "X(120)" "character" ? ? ? ? ? ? yes ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

CashFlow.Description:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.

flow-type-list = {&ALL-TYPES}.
FOR EACH CashflowType NO-LOCK:
  flow-type-list = flow-type-list + "|" +
    STRING( CashFlowType.CashFlowType, "X(4)" ) + " - " + CashFlowType.Description.
END.

RUN set-attribute-list ( 'FilterBy-Options = ' + flow-type-list ).
RUN set-attribute-list ( 'FilterBy-Case = ' + {&ALL-TYPES} + ', FilterBy-Style = COMBO-BOX' ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'ScenarioCode':U THEN DO:
       &Scope KEY-PHRASE CashFlow.ScenarioCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY CashFlow.ScenarioCode BY CashFlow.EntityType BY CashFlow.EntityCode BY CashFlow.AccountCode BY CashFlow.RelatedKey BY CashFlow.StartDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* ScenarioCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY CashFlow.ScenarioCode BY CashFlow.EntityType BY CashFlow.EntityCode BY CashFlow.AccountCode BY CashFlow.RelatedKey BY CashFlow.StartDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-value-changed B-table-Win 
PROCEDURE inst-value-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR attrib-setting AS CHAR NO-UNDO.
  
  FIND CashFlowType OF CashFlow NO-LOCK NO-ERROR.
  attrib-setting = "SENSITIVE = " +
    IF AVAILABLE CashFlowType AND NOT CashFlowType.SystemGenerated THEN "Yes" ELSE "No".
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, "Maintain,delete-record", attrib-setting ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-pack B-table-Win 
PROCEDURE pre-pack :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER win-handle AS HANDLE NO-UNDO.

DEF VAR proc-hdl AS HANDLE NO-UNDO.

  RUN get-link-handle IN adm-broker-hdl (THIS-PROCEDURE, "SearchBy", OUTPUT proc-hdl).
  IF VALID-HANDLE(proc-hdl) THEN RUN set-size IN proc-hdl( 1, 20).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "ScenarioCode" "CashFlow" "ScenarioCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "CashFlow"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-case AS CHAR NO-UNDO.
  cash-flow-type = IF new-case = {&ALL-TYPES} THEN "" ELSE
    TRIM( ENTRY( 1, new-case, "-" ) ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SearchBy-Case B-table-Win 
PROCEDURE use-SearchBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-search AS CHAR NO-UNDO.

  IF INDEX( new-search, "-") > 0 THEN
    account-code = DECIMAL( ENTRY( 2, new-search, "-" )).
  ELSE
    account-code = ?.

  new-search = ENTRY( 1, new-search, "-").
  IF SUBSTRING( new-search, 1, 1) >= "A" AND SUBSTRING( new-search, 1, 1) <= "Z"  THEN ASSIGN
    entity-type = SUBSTRING( new-search, 1, 1)
    new-search = SUBSTRING( new-search, 2).
  ELSE
    entity-type = "".

  IF LEFT-TRIM( TRIM(new-search), " 0-") = "" THEN entity-code = ?.
  ELSE
    ASSIGN entity-code = INT( new-search ) NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


