&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO.

DEF VAR bank-account-list AS CHAR NO-UNDO.
DEF VAR filter-by AS CHAR NO-UNDO.
DEF VAR sort-by AS CHAR NO-UNDO.

/* variables for seach panel */
DEF VAR cheque-no AS INT NO-UNDO INITIAL ?.

{inc/topic/tpcheque.i}

{inc/ofc-this.i}
FIND OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "PYMTBANK" NO-LOCK NO-ERROR.

bank-account-list = "".
FOR EACH BankAccount NO-LOCK:
  sort-by = STRING( BankAccount.BankAccountCode, "X(5)") + "- " + BankAccount.AccountName.
  IF AVAILABLE(OfficeControlAccount) AND BankAccount.BankAccountCode = OfficeControlAccount.Description THEN filter-by = sort-by.
  bank-account-list = bank-account-list + "|" + sort-by.
END.
bank-account-list = SUBSTRING( bank-account-list, 2).

RUN set-attribute-list('FilterBy-Style = Combo-Box, FilterBy-Case = ':U + filter-by).
RUN set-attribute-list('FilterBy-Options = ':U + bank-account-list ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Cheque

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Cheque.ChequeNo Cheque.Amount ~
Cheque.Date Cheque.DatePresented Cheque.CreditorCode Cheque.PayeeName 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table Cheque.ChequeNo 
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table Cheque
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table Cheque
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Cheque WHERE ~{&KEY-PHRASE} ~
      AND Cheque.BankAccountCode = filter-by ~
 AND (cheque-no = ? OR Cheque.ChequeNo <= cheque-no) NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table Cheque
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Cheque


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
CreditorCode|y|y|ttpl.Cheque.CreditorCode
ChequeNo||y|ttpl.Cheque.ChequeNo
BankAccountCode|y|y|ttpl.Cheque.BankAccountCode
BatchCode|y|y|ttpl.Cheque.BatchCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "CreditorCode,BankAccountCode,BatchCode",
     Keys-Supplied = "CreditorCode,ChequeNo,BankAccountCode,BatchCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Date|||ttpl.Cheque.BankAccountCode|yes,ttpl.Cheque.Date|no,ttpl.Cheque.ChequeNo|no
Cheque|y||ttpl.Cheque.BankAccountCode|no,ttpl.Cheque.ChequeNo|no
CreditorCode|||ttpl.Cheque.CreditorCode|yes,ttpl.Cheque.Date|no
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Date,Cheque,CreditorCode",
     SortBy-Case = Cheque':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Cheque SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      Cheque.ChequeNo FORMAT "999999":U
      Cheque.Amount FORMAT ">,>>>,>>>,>>9.99":U
      Cheque.Date FORMAT "99/99/9999":U
      Cheque.DatePresented FORMAT "99/99/9999":U
      Cheque.CreditorCode FORMAT "99999":U
      Cheque.PayeeName FORMAT "X(50)":U
  ENABLE
      Cheque.ChequeNo
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 97.14 BY 12.8
         BGCOLOR 16 FONT 8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 13.55
         WIDTH              = 97.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main     = 2
       br_table:MAX-DATA-GUESS IN FRAME F-Main         = 2000.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "ttpl.Cheque"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _OrdList          = "ttpl.Cheque.BankAccountCode|no,ttpl.Cheque.ChequeNo|no"
     _Where[1]         = "Cheque.BankAccountCode = filter-by
 AND (cheque-no = ? OR Cheque.ChequeNo <= cheque-no)"
     _FldNameList[1]   > ttpl.Cheque.ChequeNo
"Cheque.ChequeNo" ? ? "integer" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > ttpl.Cheque.Amount
"Cheque.Amount" ? ">,>>>,>>>,>>9.99" "decimal" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[3]   = ttpl.Cheque.Date
     _FldNameList[4]   = ttpl.Cheque.DatePresented
     _FldNameList[5]   = ttpl.Cheque.CreditorCode
     _FldNameList[6]   = ttpl.Cheque.PayeeName
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON MOUSE-SELECT-DBLCLICK OF br_table IN FRAME F-Main
DO:
  RUN special-enable-fields.
  APPLY 'ENTRY':U TO Cheque.ChequeNo IN BROWSE {&BROWSE-NAME}.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-DISPLAY OF br_table IN FRAME F-Main
DO:
  IF AVAILABLE Cheque THEN DO WITH FRAME {&FRAME-NAME}:

    DEF VAR fcolor AS INT NO-UNDO.
    fcolor = (IF Cheque.Cancelled THEN 12 ELSE ?).

    ASSIGN
      Cheque.ChequeNo:FGCOLOR IN BROWSE {&BROWSE-NAME} = fcolor
      Cheque.Amount:FGCOLOR        = fcolor
      Cheque.Date:FGCOLOR          = fcolor
      Cheque.DatePresented:FGCOLOR = fcolor
      Cheque.CreditorCode:FGCOLOR  = fcolor
      Cheque.PayeeName:FGCOLOR     = fcolor
    NO-ERROR.

  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */
/* Special routines in this browser are:                                */
/*   present-cheque - marks a cheque as presented yesterday             */
/*   unpresent-cheque - removes presented information from a cheque     */
/* ******************************************************************** */


Cheque.ChequeNo:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'CreditorCode':U THEN DO:
       &Scope KEY-PHRASE Cheque.CreditorCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.BankAccountCode BY Cheque.Date DESCENDING BY Cheque.ChequeNo DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Cheque':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.BankAccountCode DESCENDING BY Cheque.ChequeNo DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'CreditorCode':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.CreditorCode BY Cheque.Date DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* CreditorCode */
    WHEN 'BankAccountCode':U THEN DO:
       &Scope KEY-PHRASE Cheque.BankAccountCode eq key-value
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.BankAccountCode BY Cheque.Date DESCENDING BY Cheque.ChequeNo DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Cheque':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.BankAccountCode DESCENDING BY Cheque.ChequeNo DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'CreditorCode':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.CreditorCode BY Cheque.Date DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* BankAccountCode */
    WHEN 'BatchCode':U THEN DO:
       &Scope KEY-PHRASE Cheque.BatchCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.BankAccountCode BY Cheque.Date DESCENDING BY Cheque.ChequeNo DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Cheque':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.BankAccountCode DESCENDING BY Cheque.ChequeNo DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'CreditorCode':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.CreditorCode BY Cheque.Date DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* BatchCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.BankAccountCode BY Cheque.Date DESCENDING BY Cheque.ChequeNo DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Cheque':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.BankAccountCode DESCENDING BY Cheque.ChequeNo DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'CreditorCode':U THEN DO:
           &Scope SORTBY-PHRASE BY Cheque.CreditorCode BY Cheque.Date DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-value-changed B-table-Win 
PROCEDURE inst-value-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, "vouchers,view,present-cheque,unpresent-cheque",
    "SENSITIVE = " + IF AVAILABLE Cheque THEN "Yes" ELSE "No"
  ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE next-record B-table-Win 
PROCEDURE next-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  APPLY "CURSOR-UP":U TO {&BROWSE-NAME} IN FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE present-cheque B-table-Win 
PROCEDURE present-cheque :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Cheque) THEN RETURN.
  IF Cheque.Cancelled THEN DO:
    MESSAGE "Cancelled cheques may not be" SKIP "marked as presented."
      VIEW-AS ALERT-BOX INFORMATION TITLE "Cheque is Cancelled".
    RETURN.
  END.
  FIND CURRENT Cheque EXCLUSIVE-LOCK.
  Cheque.DatePresented = TODAY - 1.
  Cheque.PresentedAmount = Cheque.Amount.
  FIND CURRENT Cheque NO-LOCK.
  Cheque.DatePresented:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( TODAY - 1, "99/99/9999" ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE prev-record B-table-Win 
PROCEDURE prev-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  APPLY "CURSOR-DOWN":U TO {&BROWSE-NAME} IN FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "CreditorCode" "Cheque" "CreditorCode"}
  {src/adm/template/sndkycas.i "ChequeNo" "Cheque" "ChequeNo"}
  {src/adm/template/sndkycas.i "BankAccountCode" "Cheque" "BankAccountCode"}
  {src/adm/template/sndkycas.i "BatchCode" "Cheque" "BatchCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Cheque"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE special-enable-fields B-table-Win 
PROCEDURE special-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR rights AS LOGICAL NO-UNDO.

  RUN get-rights IN sys-mgr( "B-CHEQUE", "MODIFY-CHEQUE-NO", OUTPUT rights).
  Cheque.ChequeNo:READ-ONLY IN BROWSE {&BROWSE-NAME} = NOT rights.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE unpresent-cheque B-table-Win 
PROCEDURE unpresent-cheque :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Cheque) THEN RETURN.
  FIND CURRENT Cheque EXCLUSIVE-LOCK.
  Cheque.DatePresented = ?.
  Cheque.PresentedAmount = 0.
  FIND CURRENT Cheque NO-LOCK.

  Cheque.DatePresented:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = "".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-filter AS CHAR NO-UNDO.
  filter-by = TRIM( SUBSTRING( new-filter, 1, 4) ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  key-name = new-key-name.

  IF key-name = "CreditorCode" THEN
    RUN set-attribute-list( 'SortBy-Case = CreditorCode':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value B-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-value AS CHAR NO-UNDO.
  key-value = new-key-value.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SearchBy-Case B-table-Win 
PROCEDURE use-SearchBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-search AS CHAR NO-UNDO.

DEF VAR ch AS CHAR NO-UNDO.

  new-search = TRIM( new-search ).
  ch = SUBSTRING( new-search, 1, 1).
  IF ch >= "0" AND ch <= "9" THEN DO:
    ASSIGN cheque-no = INTEGER( new-search ) NO-ERROR.
  END.
  ELSE IF INDEX( new-search, "/", 1) > 0 THEN DO:
    ASSIGN cheque-no = INTEGER( ENTRY( 2, new-search, "/") )
           filter-by = TRIM( ENTRY( 1, new-search, "/") )
           NO-ERROR.
  END.
  ELSE DO:
    cheque-no = ?.
    filter-by = TRIM( SUBSTRING( new-search, 1, 4) ).
  END.
  RUN dispatch( 'apply-entry':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

