&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tpperson.i}
{inc/persndtl.i}

DEF VAR filter-begins AS CHAR NO-UNDO INITIAL "".
DEF VAR filter-to AS LOGICAL NO-UNDO INITIAL No.

DEF VAR phone-num AS CHAR NO-UNDO COLUMN-LABEL "Phone Number" FORMAT "X(50)".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Person

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Person.Company Person.LastName ~
Person.FirstName get-phone-nos(Person.PersonCode) @ phone-num 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table Person.Company 
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table Person
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table Person
&Scoped-define QUERY-STRING-br_table FOR EACH Person WHERE ~{&KEY-PHRASE} ~
      AND (Person.SystemContact = filter-to OR filter-to = ?) ~
AND ((RETURN-VALUE = "Last Name" AND Person.LastName MATCHES filter-begins) ~
OR (RETURN-VALUE = "Company" AND Person.Company MATCHES filter-begins)) NO-LOCK ~
    ~{&SORTBY-PHRASE}
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Person WHERE ~{&KEY-PHRASE} ~
      AND (Person.SystemContact = filter-to OR filter-to = ?) ~
AND ((RETURN-VALUE = "Last Name" AND Person.LastName MATCHES filter-begins) ~
OR (RETURN-VALUE = "Company" AND Person.Company MATCHES filter-begins)) NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table Person
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Person


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
PersonCode||y|TTPL.Person.PersonCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = "PersonCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Last Name|y||TTPL.Person.LastName|yes
Company|||TTPL.Person.Company|yes
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Last Name,Company",
     SortBy-Case = Last Name':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES></FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Person SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      Person.Company FORMAT "X(50)":U
      Person.LastName FORMAT "X(15)":U
      Person.FirstName FORMAT "X(15)":U
      get-phone-nos(Person.PersonCode) @ phone-num
  ENABLE
      Person.Company
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 98.86 BY 16.2
         BGCOLOR 15 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1.1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 20.35
         WIDTH              = 113.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:MAX-DATA-GUESS IN FRAME F-Main         = 200.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.Person"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _Where[1]         = "(Person.SystemContact = filter-to OR filter-to = ?)
AND ((RETURN-VALUE = ""Last Name"" AND Person.LastName MATCHES filter-begins)
OR (RETURN-VALUE = ""Company"" AND Person.Company MATCHES filter-begins))"
     _FldNameList[1]   > TTPL.Person.Company
"Person.Company" ? ? "character" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > TTPL.Person.LastName
"Person.LastName" ? "X(15)" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[3]   > TTPL.Person.FirstName
"Person.FirstName" ? "X(15)" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[4]   > "_<CALC>"
"get-phone-nos(Person.PersonCode) @ phone-num" ? ? ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
  
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */
{inc/DEBUG.i "b-person main begin"}
RUN set-attribute-list ( 'FilterBy-Case = User defined, FilterBy-Options = User defined|System defined|All contacts':U ).
RUN set-attribute-list ( 'SortBy-Case = Last Name, SortBy-Options = Last Name|Company':U ).
RUN set-attribute-list ( 'Filter-Value = A':U ).
RUN set-attribute-list ( 'Link-To-1 = vwr/mnt/v-person-viewer.w|Record':U ).

Person.Company:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.

{inc/DEBUG.i "b-person main end"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

  RUN get-attribute ('SortBy-Case':U).
  CASE RETURN-VALUE:
    WHEN 'Last Name':U THEN DO:
      &Scope SORTBY-PHRASE BY Person.LastName
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END.
    WHEN 'Company':U THEN DO:
      &Scope SORTBY-PHRASE BY Person.Company
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END.
    OTHERWISE DO:
      &Undefine SORTBY-PHRASE
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE columns-by-company B-table-Win 
PROCEDURE columns-by-company :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR wh AS WIDGET-HANDLE NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  wh = {&BROWSE-NAME}:FIRST-COLUMN.
  IF wh:NAME <> "Company" THEN {&BROWSE-NAME}:MOVE-COLUMN(1,3) NO-ERROR.
  wh = {&BROWSE-NAME}:FIRST-COLUMN.
  IF wh:NAME <> "Company" THEN {&BROWSE-NAME}:MOVE-COLUMN(1,3) NO-ERROR.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE columns-by-name B-table-Win 
PROCEDURE columns-by-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR wh AS WIDGET-HANDLE NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  wh = {&BROWSE-NAME}:FIRST-COLUMN.
  IF wh:NAME = "Company" THEN {&BROWSE-NAME}:MOVE-COLUMN(1,3) NO-ERROR.
  wh = {&BROWSE-NAME}:FIRST-COLUMN.
  IF wh:NAME = "Company" THEN {&BROWSE-NAME}:MOVE-COLUMN(1,3) NO-ERROR.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize B-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN columns-by-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR system-contact AS LOGI NO-UNDO.
DEF VAR type-list AS CHAR NO-UNDO.

  IF NOT AVAILABLE(Person) THEN RETURN.

  /* Code placed here will execute PRIOR to standard behavior. */
  FOR EACH Contact OF Person NO-LOCK,
            FIRST ContactType OF Contact NO-LOCK:
    IF ContactType.SystemCode THEN DO:
      type-list = type-list + ContactType.Description + ", ".
      system-contact = Yes.
    END.
  END.
  FIND FIRST Approver WHERE Approver.PersonCode = Person.PersonCode NO-LOCK NO-ERROR.
  IF AVAILABLE(Approver) THEN DO:
    type-list = type-list + "Approver, ".
    system-contact = Yes.
  END.
  FIND FIRST Property WHERE Property.Manager = Person.PersonCode NO-LOCK NO-ERROR.
  IF AVAILABLE(Property) THEN DO:
    type-list = type-list + "Primary Property Manager, ".
    system-contact = Yes.
  END.
  FIND FIRST Property WHERE Property.Administrator = Person.PersonCode NO-LOCK NO-ERROR.
  IF AVAILABLE(Property) THEN DO:
    type-list = type-list + "Backup Property Manager, ".
    system-contact = Yes.
  END.

  IF system-contact THEN DO:
    type-list = TRIM( type-list, " ,").
    MESSAGE "Tenant, Creditor and other system contacts" SKIP
            "cannot be deleted from this screen.  This person is a" SKIP
            type-list
            VIEW-AS ALERT-BOX WARNING
            TITLE "Cannot Delete Person" .
    RETURN.
  END.

  DEF VAR person-id AS CHAR NO-UNDO.
  person-id = TRIM( Person.FirstName + ' ' + Person.LastName ).
  person-id = IF person-id <> "" THEN person-id ELSE Person.Company.
  MESSAGE "Are you sure you want to delete " + person-id + " ?"
            VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO
            TITLE "Confirm Delete" UPDATE delete-it AS LOGI.
  IF NOT delete-it THEN RETURN.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE merge-records B-table-Win 
PROCEDURE merge-records :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR process-options AS CHAR NO-UNDO.

  IF NOT AVAILABLE(Person) THEN RETURN.
  process-options = "PersonCode," + STRING(Person.PersonCode).
  RUN process/merge-duplicate-people.p( process-options ).
  IF RETURN-VALUE <> "FAIL" THEN RUN dispatch( "open-query":U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "PersonCode" "Person" "PersonCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Person"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE start-email B-table-Win 
PROCEDURE start-email :
/********************************************************************
********************************************************************/


DEF VAR e-mail-proc AS HANDLE NO-UNDO.
  RUN win/w-email.w PERSISTENT SET e-mail-proc.
  RUN dispatch IN e-mail-proc ( 'initialize':U ).

  IF AVAILABLE(Person) THEN DO:
    FIND FIRST PhoneDetail OF Person WHERE PhoneDetail.PhoneType = "EMA" NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(PhoneDetail) THEN
      FIND FIRST PhoneDetail OF Person WHERE PhoneDetail.PhoneType = "EMA1" NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(PhoneDetail) THEN
      FIND FIRST PhoneDetail OF Person WHERE PhoneDetail.PhoneType = "EMA2" NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(PhoneDetail) THEN
      FIND FIRST PhoneDetail OF Person WHERE PhoneDetail.PhoneType = "EMA3" NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(PhoneDetail) THEN
      FIND FIRST PhoneDetail OF Person WHERE PhoneDetail.PhoneType = "EMAL" NO-LOCK NO-ERROR.
    IF AVAILABLE(PhoneDetail) THEN
      RUN set-recipient IN e-mail-proc ( PhoneDetail.Number ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Filter-Value B-table-Win 
PROCEDURE use-Filter-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-value AS CHAR NO-UNDO.
  filter-begins = new-value + "*".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-case AS CHAR NO-UNDO.
  CASE ENTRY( 1, new-case, " "):
    WHEN "User"     THEN    filter-to = No.
    WHEN "System"   THEN    filter-to = Yes.
    OTHERWISE               filter-to = ?.
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SearchBy-Case B-table-Win 
PROCEDURE use-SearchBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-search AS CHAR NO-UNDO.
  filter-begins = new-search + "*".
  RUN dispatch( 'apply-entry':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SortBy-Case B-table-Win 
PROCEDURE use-SortBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-case AS CHAR NO-UNDO.
  IF new-case = "Company" THEN RUN columns-by-company.
                          ELSE RUN columns-by-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

