&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------

  File:  

  Description:

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{inc/topic/tporder.i}

DEF VAR paid-to-date NO-UNDO LIKE Order.ApprovedAmount COLUMN-LABEL "Paid to Date".
DEF VAR has-vouchers AS LOGI NO-UNDO INITIAL No.
DEF VAR show-unmatched AS LOGI NO-UNDO INITIAL Yes.
DEF VAR show-any-age AS LOGI NO-UNDO INITIAL No.
DEF VAR show-calculated AS LOGI NO-UNDO INITIAL No.
DEF VAR opening-query AS LOGI NO-UNDO INITIAL NO.

DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR entity-type AS CHAR NO-UNDO INITIAL "J".
DEF VAR entity-code AS INT NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set.i "Order-Hide-Days" "order-hide-days-txt"}
IF NOT AVAILABLE(OfficeSetting) THEN DO:
  order-hide-days-txt = "120".
END.
DEF VAR order-hide-days AS INT NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Order Creditor

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Order.OrderCode Order.OrderDate ~
Order.AccountCode Order.FirstApprover Order.SecondApprover Creditor.Name ~
Order.ApprovedAmount Order.QuotedAmount Order.OrderAmount ~
paid-to-date @ paid-to-date 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table Order.OrderCode 
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table Order
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table Order
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Order WHERE ~{&KEY-PHRASE} NO-LOCK, ~
      FIRST Creditor OF Order OUTER-JOIN NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table Order Creditor
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Order
&Scoped-define SECOND-TABLE-IN-QUERY-br_table Creditor


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
PropertyCode|y|y|TTPL.Order.EntityCode
CreditorCode|y|y|TTPL.Order.CreditorCode
EntityType||y|TTPL.Order.EntityType
OrderEntity||y|TTPL.Order.EntityCode
ProjectCode|y|y|TTPL.Project.ProjectCode
OrderCode||y|TTPL.Order.OrderCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "PropertyCode,CreditorCode,ProjectCode",
     Keys-Supplied = "PropertyCode,CreditorCode,EntityType,OrderEntity,ProjectCode,OrderCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
ProjectCode|y||TTPL.Order.ProjectCode|no,TTPL.Order.OrderCode|no
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "ProjectCode",
     SortBy-Case = ProjectCode':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Order, 
      Creditor SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      Order.OrderCode COLUMN-LABEL "Code" FORMAT ">>>>9":U
      Order.OrderDate COLUMN-LABEL "Order    Date" FORMAT "99/99/9999":U
      Order.AccountCode FORMAT "9999.99":U
      Order.FirstApprover COLUMN-LABEL "App 1" FORMAT "X(4)":U
      Order.SecondApprover COLUMN-LABEL "App 2" FORMAT "X(4)":U
      Creditor.Name COLUMN-LABEL "Creditor" FORMAT "X(40)":U
      Order.ApprovedAmount COLUMN-LABEL "Approved" FORMAT "->>>,>>>,>>9.99":U
      Order.QuotedAmount COLUMN-LABEL "Quoted" FORMAT "->>>,>>>,>>9.99":U
      Order.OrderAmount COLUMN-LABEL "Order Amount" FORMAT "->>>,>>>,>>9.99":U
      paid-to-date @ paid-to-date
  ENABLE
      Order.OrderCode
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 109 BY 15
         BGCOLOR 15 FONT 9.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 9.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 16.95
         WIDTH              = 111.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.Order,TTPL.Creditor OF TTPL.Order"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _TblOptList       = ", FIRST OUTER"
     _FldNameList[1]   > ttpl.Order.OrderCode
"Order.OrderCode" "Code" ? "integer" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > ttpl.Order.OrderDate
"Order.OrderDate" "Order    Date" ? "date" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[3]   = ttpl.Order.AccountCode
     _FldNameList[4]   > ttpl.Order.FirstApprover
"Order.FirstApprover" "App 1" ? "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[5]   > ttpl.Order.SecondApprover
"Order.SecondApprover" "App 2" ? "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[6]   > ttpl.Creditor.Name
"Creditor.Name" "Creditor" "X(40)" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[7]   > ttpl.Order.ApprovedAmount
"Order.ApprovedAmount" "Approved" ? "decimal" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[8]   > ttpl.Order.QuotedAmount
"Order.QuotedAmount" "Quoted" ? "decimal" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[9]   > ttpl.Order.OrderAmount
"Order.OrderAmount" "Order Amount" ? "decimal" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[10]   > "_<CALC>"
"paid-to-date @ paid-to-date" ? ? ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-DISPLAY OF br_table IN FRAME F-Main
DO:
  IF Order.OverridePaid <> ? THEN DO:
    paid-to-date = Order.OverridePaid.
    IF show-unmatched THEN RETURN NO-APPLY.
  END.
  ELSE IF NOT(show-calculated) THEN DO:
    paid-to-date = ?.
    RETURN.
  END.
  ELSE DO:
    has-vouchers = No.
    paid-to-date = 0.0 .
    FOR EACH Voucher WHERE Voucher.EntityType = Order.EntityType
                        AND Voucher.EntityCode = Order.EntityCode
                        AND Voucher.OrderCode = Order.OrderCode NO-LOCK:
      IF Voucher.VoucherStatus = "P" OR Voucher.VoucherStatus = "A" THEN DO:
        has-vouchers = Yes.
        paid-to-date = paid-to-date + Voucher.GoodsValue .
      END.
    END.
  END.

  IF has-vouchers = show-unmatched THEN RETURN NO-APPLY.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
  MESSAGE "rowleave".
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

Order.OrderCode:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.

RUN set-attribute-list( 'FilterBy-Case = Recent, FilterBy-Options = Recent|Any Age|Unmatched|Matched':U ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'PropertyCode':U THEN DO:
       &Scope KEY-PHRASE Order.EntityCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'ProjectCode':U THEN DO:
           &Scope SORTBY-PHRASE BY Order.ProjectCode DESCENDING BY Order.OrderCode DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* PropertyCode */
    WHEN 'CreditorCode':U THEN DO:
       &Scope KEY-PHRASE Order.CreditorCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'ProjectCode':U THEN DO:
           &Scope SORTBY-PHRASE BY Order.ProjectCode DESCENDING BY Order.OrderCode DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* CreditorCode */
    WHEN 'ProjectCode':U THEN DO:
       &Scope KEY-PHRASE Project.ProjectCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'ProjectCode':U THEN DO:
           &Scope SORTBY-PHRASE BY Order.ProjectCode DESCENDING BY Order.OrderCode DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* ProjectCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'ProjectCode':U THEN DO:
           &Scope SORTBY-PHRASE BY Order.ProjectCode DESCENDING BY Order.OrderCode DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  DO TRANSACTION:
    /* this will ensure the committed amounts get changed by the write
       trigger, as there is no delete trigger yet.  */
    FIND CURRENT Order EXCLUSIVE-LOCK NO-ERROR.
    IF NOT AVAILABLE(Order) THEN RETURN.
    Order.ApprovedAmount = 0.
    FIND CURRENT Order NO-LOCK.
  END.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query-cases B-table-Win 
PROCEDURE local-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
  Notes:       The entity-type, entity-code and key-name values are set by the
               use-Key-Name and use-Key-Value functions.
------------------------------------------------------------------------------*/
  IF show-calculated THEN DO:
    ON FIND OF Order DO:
      IF NOT opening-query THEN RETURN.
    
      IF OverridePaid <> ? THEN DO:
        paid-to-date = OverridePaid.
    /*    IF show-unmatched THEN RETURN ERROR.
        RETURN. */
      END.
      ELSE DO:
        paid-to-date = 0.0 .
        has-vouchers = No.
        FOR EACH Voucher WHERE Voucher.EntityType = Order.EntityType
                            AND Voucher.EntityCode = Order.EntityCode
                            AND Voucher.OrderCode = Order.OrderCode NO-LOCK:
          IF Voucher.VoucherStatus = "P" OR Voucher.VoucherStatus = "A" THEN DO:
            has-vouchers = Yes.
            paid-to-date = paid-to-date + Voucher.GoodsValue.
          END.
        END.
      END.
    /*
      OUTPUT TO tmp.txt APPEND.
      MESSAGE Order.OrderCode Order.ProjectCode Order.EntityCode has-vouchers paid-to-date.
      OUTPUT CLOSE.
    */
      IF has-vouchers = show-unmatched THEN RETURN ERROR.
    
    END.
    &SCOPED-DEFINE PREQUALIFIER 
  END.
  ELSE DO:
    IF show-any-age THEN DO:
      order-hide-days = 999999.
    END.
    ELSE DO:
      order-hide-days = INT(order-hide-days-txt).
    END.
    paid-to-date = ?.
    &SCOPED-DEFINE PREQUALIFIER AND Order.OrderDate > (TODAY - order-hide-days)
  END.

  opening-query = Yes.
  &Scope SORTBY-PHRASE BY Order.OrderCode DESCENDING
  IF key-name = "CreditorCode" THEN DO:
    &Scope KEY-PHRASE Order.CreditorCode eq entity-code {&PREQUALIFIER}
/*    MESSAGE entity-type entity-code "~n{&OPEN-QUERY-{&BROWSE-NAME}}". */
    {&OPEN-QUERY-{&BROWSE-NAME}}
  END. /* Projects, for consistency with prior use */
  ELSE IF entity-type = "J" THEN DO:
    &Scope KEY-PHRASE (Order.EntityType eq entity-type ~
                      AND Order.EntityCode eq entity-code) {&PREQUALIFIER}
    {&OPEN-QUERY-{&BROWSE-NAME}}
  END. /* Projects, for consistency with prior use */
  ELSE DO:
    &Scope KEY-PHRASE (Order.EntityType eq entity-type ~
                      AND Order.EntityCode eq entity-code) {&PREQUALIFIER}
    /* MESSAGE entity-type entity-code "~n{&OPEN-QUERY-{&BROWSE-NAME}}". */
    {&OPEN-QUERY-{&BROWSE-NAME}}
  END. /* Others */
  opening-query = NO.

ON FIND OF Order REVERT.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reprint-order B-table-Win 
PROCEDURE reprint-order :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN process\report\orderfrm.p ( Order.ProjectCode, ?, ?, STRING(Order.OrderCode), "Yes,1").
  MESSAGE "The order has been reprinted" VIEW-AS ALERT-BOX INFORMATION
            TITLE "Reprint Complete".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "PropertyCode" "Order" "EntityCode"}
  {src/adm/template/sndkycas.i "CreditorCode" "Order" "CreditorCode"}
  {src/adm/template/sndkycas.i "EntityType" "Order" "EntityType"}
  {src/adm/template/sndkycas.i "OrderEntity" "Order" "EntityCode"}
  {src/adm/template/sndkycas.i "ProjectCode" "Project" "ProjectCode"}
  {src/adm/template/sndkycas.i "OrderCode" "Order" "OrderCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Order"}
  {src/adm/template/snd-list.i "Creditor"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-filter-case AS CHAR NO-UNDO.
  show-unmatched = (new-filter-case = 'Unmatched').
  show-any-age = (new-filter-case = 'Any Age').
  show-calculated = (new-filter-case <> 'Recent') AND NOT show-any-age .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.

  key-name = new-key-name.
  CASE new-key-name:
    WHEN "PropertyCode" THEN entity-type = "P".
    WHEN "ProjectCode" THEN entity-type = "J".
    WHEN "CompanyCode" THEN entity-type = "L".
    OTHERWISE entity-type = "J".
  END CASE.
  opening-query = Yes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value B-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-value AS CHAR NO-UNDO.

  entity-code = INT(new-key-value).
  opening-query = Yes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

