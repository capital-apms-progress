&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tpprojct.i}

DEF VAR filter-value AS CHAR NO-UNDO        INITIAL "".
DEF VAR show-inactive AS LOGICAL NO-UNDO   INITIAL No.
DEF VAR show-all AS LOGICAL NO-UNDO   INITIAL No.

DEF VAR d-date-1 AS CHAR NO-UNDO.
DEF VAR d-date-2 AS CHAR NO-UNDO.
DEF VAR d-update AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Project

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Project.ProjectCode ~
Project.ExpenditureType Project.Name ~
STRING( Project.StartDate, '99/99/9999') @ d-date-1 ~
(IF Project.CompleteDate <> ? THEN STRING( Project.CompleteDate, '99/99/9999') ELSE '') @ d-date-2 ~
Project.EntityType + STRING (Project.EntityCode, '99999') + '-' + STRING( Project.EntityAccount, '9999.99') @ d-update ~
Project.ApprovedAmount 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Project WHERE ~{&KEY-PHRASE} ~
      AND (Project.Active <> show-inactive) ~
AND (IF show-all THEN Yes ELSE (Project.ExpenditureType <> "G" AND Project.EntityType <> "J")) ~
AND Project.Name BEGINS filter-value ~
AND Project.ProjectType BEGINS select-case NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table Project
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Project


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
ProjectCode||y|TTPL.Project.ProjectCode
Entity||y|TTPL.Project.ProjectCode
ParentProjectCode|y|y|ttpl.Project.ProjectCode
EntityCode|y|y|ttpl.Project.EntityCode
FirstApprover|y|y|ttpl.Project.FirstApprover
Proposer|y|y|ttpl.Project.Proposer
EntityType|y|y|TTPL.Project.EntityType
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "ParentProjectCode,EntityCode,FirstApprover,Proposer,EntityType",
     Keys-Supplied = "ProjectCode,Entity,ParentProjectCode,EntityCode,FirstApprover,Proposer,EntityType"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Entity|||ttpl.Project.EntityType|yes,ttpl.Project.EntityCode|yes
Code|y||ttpl.Project.ProjectCode|yes
Name|||ttpl.Project.Name|yes
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Entity,Code,Name",
     Sort-Case = Code':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-link-hdl B-table-Win 
FUNCTION get-link-hdl RETURNS HANDLE
  ( INPUT link-type AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Project SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      Project.ProjectCode COLUMN-LABEL "Project"
      Project.ExpenditureType COLUMN-LABEL "T" FORMAT "X"
      Project.Name FORMAT "X(55)"
      STRING( Project.StartDate, '99/99/9999') @ d-date-1 COLUMN-LABEL "  Start Date" FORMAT "X(12)"
      (IF Project.CompleteDate <> ? THEN STRING( Project.CompleteDate, '99/99/9999') ELSE '') @ d-date-2 COLUMN-LABEL "   End Date" FORMAT "X(12)"
      Project.EntityType + STRING (Project.EntityCode, '99999') + '-' + STRING( Project.EntityAccount, '9999.99') @ d-update COLUMN-LABEL "Update to Acct" FORMAT "X(16)"
      Project.ApprovedAmount FORMAT "->,>>>,>>>,>>9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 94.29 BY 16.95
         BGCOLOR 15 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 20.05
         WIDTH              = 98.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.Project"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _Where[1]         = "(Project.Active <> show-inactive)
AND (IF show-all THEN Yes ELSE (Project.ExpenditureType <> ""G"" AND Project.EntityType <> ""J""))
AND Project.Name BEGINS filter-value
AND Project.ProjectType BEGINS select-case"
     _FldNameList[1]   > TTPL.Project.ProjectCode
"Project.ProjectCode" "Project" ? "integer" ? ? ? ? ? ? no ?
     _FldNameList[2]   > TTPL.Project.ExpenditureType
"Project.ExpenditureType" "T" "X" "character" ? ? ? ? ? ? no ?
     _FldNameList[3]   > TTPL.Project.Name
"Project.Name" ? "X(55)" "character" ? ? ? ? ? ? no ?
     _FldNameList[4]   > "_<CALC>"
"STRING( Project.StartDate, '99/99/9999') @ d-date-1" "  Start Date" "X(12)" ? ? ? ? ? ? ? no ?
     _FldNameList[5]   > "_<CALC>"
"(IF Project.CompleteDate <> ? THEN STRING( Project.CompleteDate, '99/99/9999') ELSE '') @ d-date-2" "   End Date" "X(12)" ? ? ? ? ? ? ? no ?
     _FldNameList[6]   > "_<CALC>"
"Project.EntityType + STRING (Project.EntityCode, '99999') + '-' + STRING( Project.EntityAccount, '9999.99') @ d-update" "Update to Acct" "X(16)" ? ? ? ? ? ? ? no ?
     _FldNameList[7]   > TTPL.Project.ApprovedAmount
"Project.ApprovedAmount" ? "->,>>>,>>>,>>9.99" "decimal" ? ? ? ? ? ? no ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */


RUN set-attribute-list( 'SortBy-Options = Code|Name|Entity, SortBy-Case = Code':U ).
RUN set-attribute-list( 'FilterBy-Options = Active|Inactive, FilterBy-Case = Active':U ).
RUN set-attribute-list( 'Filter-Value = ':U ).

DEF VAR select-options AS CHAR NO-UNDO  INITIAL "EXGS - Exclude 'G' and Sub-Projects|All  - All projects".
DEF VAR select-case AS CHAR NO-UNDO.
FOR EACH ProjectType NO-LOCK:
  select-options = select-options + "|" + STRING( ProjectType.ProjectType, "X(4)") + " - " + ProjectType.Description.
END.
RUN set-attribute-list( 'Select-Style = Combo-Box, Select-Options = ':U + select-options + ', Select-Case = ':U + ENTRY(1,select-options,'|')).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'ParentProjectCode':U THEN DO:
       &Scope KEY-PHRASE Project.ProjectCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.EntityType BY Project.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Code':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.ProjectCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Name':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.Name
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* ParentProjectCode */
    WHEN 'EntityCode':U THEN DO:
       &Scope KEY-PHRASE Project.EntityCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.EntityType BY Project.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Code':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.ProjectCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Name':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.Name
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* EntityCode */
    WHEN 'FirstApprover':U THEN DO:
       &Scope KEY-PHRASE Project.FirstApprover eq key-value
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.EntityType BY Project.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Code':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.ProjectCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Name':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.Name
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* FirstApprover */
    WHEN 'Proposer':U THEN DO:
       &Scope KEY-PHRASE Project.Proposer eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.EntityType BY Project.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Code':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.ProjectCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Name':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.Name
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* Proposer */
    WHEN 'EntityType':U THEN DO:
       &Scope KEY-PHRASE Project.EntityType eq key-value
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.EntityType BY Project.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Code':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.ProjectCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Name':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.Name
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* EntityType */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.EntityType BY Project.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Code':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.ProjectCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Name':U THEN DO:
           &Scope SORTBY-PHRASE BY Project.Name
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-parent-project B-table-Win 
PROCEDURE get-parent-project :
/*------------------------------------------------------------------------------
  Purpose:  Return the parent project to enter "Add" viewers from here
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER parent-project AS INT NO-UNDO.

DEF VAR key-value AS CHAR NO-UNDO.
DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR et AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  key-name = RETURN-VALUE.
  IF key-value = ? THEN DO:
    DEF VAR rs-hdl AS HANDLE NO-UNDO.
    rs-hdl = get-link-hdl( "RECORD-SOURCE":U ).
    IF rs-hdl <> ? THEN
      RUN send-key IN rs-hdl( key-name, OUTPUT key-value ).
  END.
  IF   key-name = "CompanyCode"
    OR key-name = "PropertyCode"
    OR key-name = "ParentProjectCode"
  THEN DO:
    CASE key-name:
      WHEN "CompanyCode" THEN       et = "L".
      WHEN "PropertyCode" THEN      et = "P".
      WHEN "ParentProjectCode" THEN et = "J".
    END CASE.
  END.

  IF et = "J" THEN 
    parent-project = INT(key-value).
  ELSE
    parent-project = ?.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Project) THEN RETURN.

  /* Code placed here will execute PRIOR to standard behavior. */
  IF CAN-FIND( FIRST AcctTran WHERE AcctTran.EntityType = "J"
                     AND AcctTran.EntityCode = Project.ProjectCode) THEN DO:
    MESSAGE "Cannot delete project as posted transactions exist."
        VIEW-AS ALERT-BOX ERROR.
    RETURN.
  END.

  DO TRANSACTION ON ERROR UNDO, LEAVE:
    FOR EACH AccountBalance WHERE AccountBalance.EntityType = "J" AND
                      AccountBalance.EntityCode = Project.ProjectCode:
      DELETE AccountBalance.
    END.
    FOR EACH ProjectBudget OF Project:
      DELETE ProjectBudget.
    END.
    FIND CURRENT Project EXCLUSIVE-LOCK.
    DELETE Project.
  END.
  RUN dispatch( 'open-query':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query-cases B-table-Win 
PROCEDURE local-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  IF RETURN-VALUE = "CompanyCode" OR RETURN-VALUE = "PropertyCode" OR RETURN-VALUE = "ParentProjectCode" THEN DO:
  DEF VAR et AS CHAR NO-UNDO.
    CASE RETURN-VALUE:
      WHEN "CompanyCode" THEN       et = "L".
      WHEN "PropertyCode" THEN      et = "P".
      WHEN "ParentProjectCode" THEN et = "J".
    END CASE.
    /* select-case is not valid for sub-projects */
    select-case = "".
    show-all = Yes.
    &Scope KEY-PHRASE Project.EntityType = et AND Project.EntityCode eq INTEGER(key-value)
    RUN get-attribute ('SortBy-Case':U).
    CASE RETURN-VALUE:
      WHEN 'Entity':U THEN DO:
        &Scope SORTBY-PHRASE BY Project.EntityType BY Project.EntityCode
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      WHEN 'Code':U THEN DO:
        &Scope SORTBY-PHRASE BY Project.ProjectCode
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      WHEN 'Name':U THEN DO:
        &Scope SORTBY-PHRASE BY Project.Name
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      OTHERWISE DO:
        &Undefine SORTBY-PHRASE
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END. /* OTHERWISE...*/
    END CASE.
  END.
  ELSE DO:
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query-cases':U ) .
  END.
  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE normal-send-key B-table-Win adm/support/_key-snd.p
PROCEDURE normal-send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "ProjectCode" "Project" "ProjectCode"}
  {src/adm/template/sndkycas.i "Entity" "Project" "ProjectCode"}
  {src/adm/template/sndkycas.i "ParentProjectCode" "Project" "ProjectCode"}
  {src/adm/template/sndkycas.i "EntityCode" "Project" "EntityCode"}
  {src/adm/template/sndkycas.i "FirstApprover" "Project" "FirstApprover"}
  {src/adm/template/sndkycas.i "Proposer" "Project" "Proposer"}
  {src/adm/template/sndkycas.i "EntityType" "Project" "EntityType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-pack B-table-Win 
PROCEDURE pre-pack :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER win-hdl AS HANDLE NO-UNDO.

DEF VAR opt-hdl AS HANDLE NO-UNDO.

  RUN get-attribute IN win-hdl( 'Option-Panel':U ).
  opt-hdl = WIDGET-HANDLE(RETURN-VALUE).

  IF VALID-HANDLE(opt-hdl) THEN RUN set-size IN opt-hdl( ?, 30).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  {inc/sendkey.i}

  IF NOT AVAILABLE(Project) THEN RETURN.

  IF key-name = "Entity" THEN
    key-value = "J/" + STRING(Project.ProjectCode).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Project"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Filter-Value B-table-Win 
PROCEDURE use-Filter-Value :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-filter-value AS CHAR NO-UNDO.
  filter-value = new-filter-value.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-filterby-case AS CHAR NO-UNDO.

  show-inactive = (new-filterby-case = "Inactive").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Select-Case B-table-Win 
PROCEDURE use-Select-Case :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-select-case AS CHAR NO-UNDO.

  select-case = ENTRY( 1, new-select-case, " ").
  IF select-case = "All" THEN select-case = "".
  show-all = (select-case <> "EXGS").
  IF NOT show-all THEN select-case = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-link-hdl B-table-Win 
FUNCTION get-link-hdl RETURNS HANDLE
  ( INPUT link-type AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR record-source-hdl AS HANDLE NO-UNDO.
DEF VAR lnk-hdl AS CHAR NO-UNDO.
DEF VAR parent-project LIKE Project.ProjectCode NO-UNDO INITIAL ?.
  
  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, link-type, OUTPUT lnk-hdl ).
  ASSIGN record-source-hdl = WIDGET-HANDLE( lnk-hdl ) NO-ERROR.
  IF VALID-HANDLE(record-source-hdl) THEN
    RETURN record-source-hdl.
  ELSE
    RETURN ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


