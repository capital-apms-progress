&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
  File:  
  Description: Browse contacts for a particular entity type
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR entity-type AS CHAR NO-UNDO.
DEF VAR entity-code AS INT NO-UNDO.

DEF VAR filter-value AS CHAR NO-UNDO INITIAL "Active".

DEF VAR calldate-display AS CHAR NO-UNDO INITIAL ''.
DEF VAR closedate-display AS CHAR NO-UNDO INITIAL ''.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES TenantCall

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table TenantCall.CallNumber ~
TenantCall.DateOfCall TenantCall.PropertyCode TenantCall.Level ~
TenantCall.Description TenantCall.OrderNo TenantCall.CreditorCode ~
TenantCall.Action TenantCall.CallStatusCode ~
(STRING( TenantCall.DateComplete, '99/99/99') + ', ' + STRING( TenantCall.TimeComplete, 'HH:MM')) @ closedate-display 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table 
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH TenantCall WHERE ~{&KEY-PHRASE} ~
      AND (TenantCall.CallCategoryCode <> 'ACCT') ~
AND (TenantCall.CallStatusCode BEGINS filter-value) ~
 AND (TenantCall.CallStatusCode <> (IF filter-value = "" THEN "Closed" ELSE "kjhgdkjh" )) NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table TenantCall
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table TenantCall
&Scoped-define TABLES-IN-QUERY-br_table TenantCall
&Scoped-define FIRST-TABLE-IN-QUERY-br_table TenantCall


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
EntityType||y|TTPL.EntityContact.EntityType
EntityCode||y|TTPL.EntityContact.EntityCode
CallNumber||y|TTPL.TenantCall.CallNumber
PersonCode|y|y|TTPL.EntityContact.PersonCode
EntityContactType|y|y|TTPL.EntityContact.EntityContactType
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "PersonCode,EntityContactType",
     Keys-Supplied = "EntityType,EntityCode,CallNumber,PersonCode,EntityContactType"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Call No|y||TTPL.TenantCall.CallNumber|no
Property|||TTPL.TenantCall.PropertyCode|yes
Priority|||TTPL.TenantCall.Priority|yes
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "':U + 'Call No,Property,Priority' + '",
     SortBy-Case = ':U + 'Call No').

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      TenantCall SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      TenantCall.CallNumber FORMAT ">>,>>9":U
      TenantCall.DateOfCall FORMAT "99/99/9999":U
      TenantCall.PropertyCode COLUMN-LABEL "Prop." FORMAT "99999":U
            WIDTH 6
      TenantCall.Level FORMAT "->>9":U
      TenantCall.Description FORMAT "X(60)":U WIDTH 46.57
      TenantCall.OrderNo FORMAT ">>>>>9":U
      TenantCall.CreditorCode COLUMN-LABEL "Cred#" FORMAT "99999":U
      TenantCall.Action FORMAT "X(78)":U
      TenantCall.CallStatusCode FORMAT "X(8)":U
      (STRING( TenantCall.DateComplete, '99/99/99') + ', ' + STRING( TenantCall.TimeComplete, 'HH:MM')) @ closedate-display COLUMN-LABEL "Closed at" FORMAT "X(15)":U
            WIDTH 13
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 106.86 BY 14.4
         BGCOLOR 16 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 15.7
         WIDTH              = 110.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main     = 2.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.TenantCall"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _Where[1]         = "(TenantCall.CallCategoryCode <> 'ACCT')
AND (TenantCall.CallStatusCode BEGINS filter-value)
 AND (TenantCall.CallStatusCode <> (IF filter-value = """" THEN ""Closed"" ELSE ""kjhgdkjh"" ))"
     _FldNameList[1]   > TTPL.TenantCall.CallNumber
"TenantCall.CallNumber" ? ">>,>>9" "integer" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[2]   = TTPL.TenantCall.DateOfCall
     _FldNameList[3]   > TTPL.TenantCall.PropertyCode
"TenantCall.PropertyCode" "Prop." ? "integer" ? ? ? ? ? ? no ? no no "6" yes no no "U" "" ""
     _FldNameList[4]   > TTPL.TenantCall.Level
"TenantCall.Level" ? "->>9" "integer" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[5]   > TTPL.TenantCall.Description
"TenantCall.Description" ? ? "character" ? ? ? ? ? ? no ? no no "46.57" yes no no "U" "" ""
     _FldNameList[6]   = TTPL.TenantCall.OrderNo
     _FldNameList[7]   > TTPL.TenantCall.CreditorCode
"TenantCall.CreditorCode" "Cred#" ? "integer" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[8]   = TTPL.TenantCall.Action
     _FldNameList[9]   = TTPL.TenantCall.CallStatusCode
     _FldNameList[10]   > "_<CALC>"
"(STRING( TenantCall.DateComplete, '99/99/99') + ', ' + STRING( TenantCall.TimeComplete, 'HH:MM')) @ closedate-display" "Closed at" "X(15)" ? ? ? ? ? ? ? no ? no no "13" yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON RETURN OF br_table IN FRAME F-Main
ANYWHERE DO:
/*  MESSAGE KEYLABEL(LASTKEY) LAST-EVENT:FUNCTION LAST-EVENT:EVENT-TYPE .
  IF KEYLABEL(LASTKEY) = 'ENTER':U THEN DO: */
    APPLY 'TAB':U TO SELF.
    RETURN NO-APPLY.
/*  END. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
  IF {&SELF-NAME}:NEW-ROW THEN RUN new-row-defaults.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list( 'FilterBy-Options = Active|Closed, FilterBy-Case = Active':U ).
RUN set-attribute-list( 'SortBy-Options = Call No|Property|Priority, SortBy-Case = Call No':U ).

RUN set-attribute-list ( 'Link-To-1 = vwr/mnt/v-tenantcall-viewer.w|Record':U ).

/* RUN set-attribute-list ( 'Link-To-1 = vwr/mnt/v-person-viewer.w|Record':U ). */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'PersonCode':U THEN DO:
       &Scope KEY-PHRASE EntityContact.PersonCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Call No':U THEN DO:
           &Scope SORTBY-PHRASE BY TenantCall.CallNumber DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY TenantCall.PropertyCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Priority':U THEN DO:
           &Scope SORTBY-PHRASE BY TenantCall.Priority
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* PersonCode */
    WHEN 'EntityContactType':U THEN DO:
       &Scope KEY-PHRASE EntityContact.EntityContactType eq key-value
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Call No':U THEN DO:
           &Scope SORTBY-PHRASE BY TenantCall.CallNumber DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY TenantCall.PropertyCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Priority':U THEN DO:
           &Scope SORTBY-PHRASE BY TenantCall.Priority
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* EntityContactType */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Call No':U THEN DO:
           &Scope SORTBY-PHRASE BY TenantCall.CallNumber DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Property':U THEN DO:
           &Scope SORTBY-PHRASE BY TenantCall.PropertyCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Priority':U THEN DO:
           &Scope SORTBY-PHRASE BY TenantCall.Priority
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE base-send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE base-send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "EntityType" "EntityContact" "EntityType"}
  {src/adm/template/sndkycas.i "EntityCode" "EntityContact" "EntityCode"}
  {src/adm/template/sndkycas.i "CallNumber" "TenantCall" "CallNumber"}
  {src/adm/template/sndkycas.i "PersonCode" "EntityContact" "PersonCode"}
  {src/adm/template/sndkycas.i "EntityContactType" "EntityContact" "EntityContactType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE edit-call B-table-Win 
PROCEDURE edit-call :
/*------------------------------------------------------------------------------
  Purpose: Edit the currently select call in some way
------------------------------------------------------------------------------*/
  RUN vwr/mnt/d-tenant-call.w( TenantCall.CallNumber ).
  RUN dispatch IN THIS-PROCEDURE ('display-fields':U).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE mark-as-closed B-table-Win 
PROCEDURE mark-as-closed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF TenantCall.TimeComplete <> ? THEN RETURN.

  DO TRANSACTION WITH FRAME {&FRAME-NAME}:
    FIND CURRENT TenantCall EXCLUSIVE-LOCK.
    TenantCall.TimeComplete = TIME.
    TenantCall.DateComplete = TODAY.
    TenantCall.CallStatusCode = 'Closed'.
    FIND CURRENT TenantCall NO-LOCK.
  END.
  
  RUN dispatch IN THIS-PROCEDURE ('display-fields':U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE new-call B-table-Win 
PROCEDURE new-call :
/*------------------------------------------------------------------------------
  Purpose: Edit a new call
------------------------------------------------------------------------------*/
  RUN vwr/mnt/d-tenant-call.w( ? ).
  RUN dispatch('open-query').
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE new-row-defaults B-table-Win 
PROCEDURE new-row-defaults :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
DEF BUFFER LastCall FOR TenantCall.

  FIND LAST LastCall NO-LOCK NO-ERROR.
  TenantCall.CallNumber:SCREEN-VALUE IN BROWSE {&BROWSE-NAME}
       = STRING((IF AVAILABLE(LastCall) THEN LastCall.CallNumber ELSE 0) + 1).
  TenantCall.CallStatusCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME}
       = "Active".
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  WHEN "EntityType" THEN    pc_key-value = (IF AVAILABLE(EntityContact) THEN EntityContact.EntityType ELSE entity-type).
  WHEN "EntityCode" THEN    pc_key-value = STRING(entity-code).
  {src/adm/template/sndkycas.i "PersonCode" "EntityContact" "PersonCode"}
  {src/adm/template/sndkycas.i "EntityContactType" "EntityContact" "EntityContactType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "TenantCall"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE tenant-dialog B-table-Win 
PROCEDURE tenant-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER property-code AS INT NO-UNDO.

DEF VAR tenant-code AS INT NO-UNDO.
  RUN win/d-property-tenant.w( INPUT-OUTPUT property-code, OUTPUT tenant-code ).

  IF tenant-code > 0 THEN DO:
    /*
    TenantCall.TenantCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME}
                  = STRING(tenant-code,"99999").
    */

    TenantCall.PropertyCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME}
                  = STRING(property-code,"99999").
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-case AS CHAR NO-UNDO.
  filter-value = new-case.
  IF new-case = 'Active' THEN filter-value = ''.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-key-name B-table-Win 
PROCEDURE use-key-name :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  key-name = new-key-name.

  IF key-name = "EntityCode" THEN
    entity-type = find-parent-key( "EntityType" ).
  ELSE IF key-name = "TenantCode" THEN      entity-type = "T".
  ELSE IF key-name = "CreditorCode" THEN    entity-type = "C".
  ELSE IF key-name = "PropertyCode" THEN    entity-type = "P".
  ELSE IF key-name = "CompanyCode" THEN     entity-type = "L".
  ELSE IF key-name = "ProjectCode" THEN     entity-type = "J".
  ELSE IF key-name = "AssetCode" THEN       entity-type = "F".
  ELSE
    entity-type = find-parent-key( "EntityType" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value B-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-value AS CHAR NO-UNDO.

  entity-code = INT(new-key-value).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

