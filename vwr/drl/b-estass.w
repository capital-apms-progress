&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR current-key-name AS CHAR NO-UNDO.
DEF VAR current-key-value AS CHAR NO-UNDO.

DEF VAR ext-scenario AS INT NO-UNDO.

DEF VAR ext-et AS CHAR NO-UNDO  INITIAL ?.
DEF VAR ext-ec AS INT  NO-UNDO  INITIAL ?.
DEF VAR ext-ac AS DEC  NO-UNDO  INITIAL ?.

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}

ON CREATE OF CashFlow DO:
  CashFlow.CFChangeType = "N".
  CashFlow.Frequency = "MNTH".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES CashFlow

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table CashFlow.StartDate CashFlow.EndDate ~
CashFlow.CFChangeType CashFlow.FrequencyCode CashFlow.Amount ~
CashFlow.Description 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table CashFlow.StartDate ~
CashFlow.EndDate CashFlow.CFChangeType CashFlow.FrequencyCode ~
CashFlow.Amount CashFlow.Description 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}StartDate ~{&FP2}StartDate ~{&FP3}~
 ~{&FP1}EndDate ~{&FP2}EndDate ~{&FP3}~
 ~{&FP1}CFChangeType ~{&FP2}CFChangeType ~{&FP3}~
 ~{&FP1}FrequencyCode ~{&FP2}FrequencyCode ~{&FP3}~
 ~{&FP1}Amount ~{&FP2}Amount ~{&FP3}~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table CashFlow
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table CashFlow
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH CashFlow WHERE ~{&KEY-PHRASE} ~
      AND CashFlow.ScenarioCode = ext-scenario NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table CashFlow
&Scoped-define FIRST-TABLE-IN-QUERY-br_table CashFlow


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
EntityType|y|y|ttpl.CashFlow.EntityType
EntityCode|y|y|ttpl.CashFlow.EntityCode
AccountCode|y|y|ttpl.CashFlow.AccountCode
AccountSummary|y|y|ttpl.AccountSummary.AccountCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "EntityType,EntityCode,AccountCode,AccountSummary",
     Keys-Supplied = "EntityType,EntityCode,AccountCode,AccountSummary"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      CashFlow SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      CashFlow.StartDate COLUMN-LABEL "Start of Flow"
      CashFlow.EndDate COLUMN-LABEL "End  of  Flow"
      CashFlow.CFChangeType
      CashFlow.FrequencyCode COLUMN-LABEL "Freq'cy"
      CashFlow.Amount
      CashFlow.Description FORMAT "X(60)"
  ENABLE
      CashFlow.StartDate
      CashFlow.EndDate
      CashFlow.CFChangeType
      CashFlow.FrequencyCode
      CashFlow.Amount
      CashFlow.Description
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 92.57 BY 12
         BGCOLOR 16 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 12.4
         WIDTH              = 93.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "ttpl.CashFlow"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _Where[1]         = "CashFlow.ScenarioCode = ext-scenario"
     _FldNameList[1]   > ttpl.CashFlow.StartDate
"CashFlow.StartDate" "Start of Flow" ? "date" ? ? ? ? ? ? yes ?
     _FldNameList[2]   > ttpl.CashFlow.EndDate
"CashFlow.EndDate" "End  of  Flow" ? "date" ? ? ? ? ? ? yes ?
     _FldNameList[3]   > ttpl.CashFlow.CFChangeType
"CashFlow.CFChangeType" ? ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[4]   > ttpl.CashFlow.FrequencyCode
"CashFlow.FrequencyCode" "Freq'cy" ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[5]   > ttpl.CashFlow.Amount
"CashFlow.Amount" ? ? "decimal" ? ? ? ? ? ? yes ?
     _FldNameList[6]   > ttpl.CashFlow.Description
"CashFlow.Description" ? "X(60)" "character" ? ? ? ? ? ? yes ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main B-table-Win
ON RETURN OF FRAME F-Main
ANYWHERE DO:
  APPLY 'TAB':U TO SELF.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list( 'create-on-add = Yes':U ).

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'EntityType':U THEN DO:
       &Scope KEY-PHRASE CashFlow.EntityType eq key-value
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* EntityType */
    WHEN 'EntityCode':U THEN DO:
       &Scope KEY-PHRASE CashFlow.EntityCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* EntityCode */
    WHEN 'AccountCode':U THEN DO:
       &Scope KEY-PHRASE CashFlow.AccountCode eq DECIMAL(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* AccountCode */
    WHEN 'AccountSummary':U THEN DO:
       &Scope KEY-PHRASE AccountSummary.AccountCode eq DECIMAL(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* AccountSummary */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record B-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* delete any null flow that might have been created by accident */
  FIND CashFlow WHERE CashFlow.ScenarioCode = 0 AND CashFlow.EntityType = ""
                AND CashFlow.EntityCode = 0 AND CashFlow.AccountCode = 0.0
                AND CashFlow.Sequence = 0 AND CashFlow.CashFlowType = ""
                EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE(CashFlow) THEN DELETE CashFlow.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
/*  CashFlow.CFChangeType:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = "N".
  CashFlow.Frequency:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = "MNTH".
*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement B-table-Win 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DEF BUFFER OtherCF FOR CashFlow.

  IF CAN-FIND( EntityType WHERE EntityType.EntityType = CashFlow.EntityType)
              AND CashFlow.EntityCode = 0 AND CashFlow.AccountCode = 0
              AND CashFlow.ScenarioCode = 0 THEN .
  ELSE DO:
    FIND OtherCF OF CashFlow WHERE RECID(OtherCF) <> RECID(CashFlow) NO-ERROR.
    IF AVAILABLE(OtherCF) THEN DELETE OtherCF.
  END.

  IF adm-new-record OR CashFlow.Sequence < 1 THEN DO:
    FIND LAST OtherCF WHERE OtherCF.EntityType = ext-et
                        AND OtherCF.EntityCode = ext-ec
                        AND OtherCF.AccountCode = ext-ac
                        AND OtherCF.ScenarioCode = ext-scenario
                        AND RECID(OtherCF) <> RECID(CashFlow) NO-LOCK NO-ERROR.
    CashFlow.Sequence = 1 + (IF AVAILABLE(OtherCF) THEN OtherCF.Sequence ELSE 0).
  END.

  /* Code placed here will execute PRIOR to standard behavior. */
  ASSIGN    CashFlow.EntityType = ext-et
            CashFlow.EntityCode = ext-ec
            CashFlow.AccountCode = ext-ac
            CashFlow.CashFlowType = "MEST"
            CashFlow.RelatedKey = current-key-name + "," + current-key-value
            CashFlow.ScenarioCode = ext-scenario .

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  RUN get-attribute( 'create-on-add':U ).
  IF adm-new-record AND RETURN-VALUE <> "Yes" THEN DO:
    RUN dispatch( 'cancel-record':U ).
    RETURN.
  END.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy B-table-Win 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF adm-new-record THEN
    RUN dispatch( 'cancel-record':U ).
  ELSE
    RUN check-modified( 'clear':U ).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query-cases B-table-Win 
PROCEDURE local-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Find the current record using the current Key-Name. */
  IF current-key-name = 'AccountSummary':U 
            AND NUM-ENTRIES(current-key-value, "/") = 3 THEN DO:

    ext-et = ENTRY(1,current-key-value,"/").
    ext-ec = INT(ENTRY(2,current-key-value,"/")).
    ext-ac = DEC(ENTRY(3,current-key-value,"/")).

    &Scope KEY-PHRASE CashFlow.ScenarioCode eq ext-scenario ~
                  AND CashFlow.EntityType eq ext-et ~
                  AND CashFlow.EntityCode eq ext-ec ~
                  AND CashFlow.AccountCode eq ext-ac

    {&OPEN-QUERY-{&BROWSE-NAME}}
  END.
  ELSE
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query-cases':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-update-record B-table-Win 
PROCEDURE local-update-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  RUN verify-cashflow.
  IF RETURN-VALUE = "FAIL" THEN RETURN "ADM-ERROR".

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'update-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE normal-send-key B-table-Win adm/support/_key-snd.p
PROCEDURE normal-send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "EntityType" "CashFlow" "EntityType"}
  {src/adm/template/sndkycas.i "EntityCode" "CashFlow" "EntityCode"}
  {src/adm/template/sndkycas.i "AccountCode" "CashFlow" "AccountCode"}
  {src/adm/template/sndkycas.i "AccountSummary" "AccountSummary" "AccountCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize B-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FIND FIRST RP WHERE RP.ReportID = "Current Scenario"
                 AND RP.UserName = user-name NO-LOCK NO-ERROR.

  IF AVAILABLE(RP) THEN
    ext-scenario = RP.Int1.
  ELSE DO:
    FIND FIRST Scenario NO-LOCK.
    MESSAGE "You should select your current scenario" SKIP(1)
            "Scenario" Scenario.ScenarioCode " - " Scenario.Name SKIP
            "has been selected by default"
             VIEW-AS ALERT-BOX WARNING TITLE "No current scenario".
    ext-scenario = Scenario.ScenarioCode.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:  Spoof the standard sendkey routine
------------------------------------------------------------------------------*/
  {inc/sendkey.i}
  IF AVAILABLE(CashFlow) AND key-name = "AccountSummary" THEN
    key-value = CashFlow.EntityType + "/"
              + STRING(CashFlow.EntityCode) + "/"
              + STRING(CashFlow.AccountCode).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "CashFlow"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-name AS CHAR NO-UNDO.
  current-key-name = new-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value B-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-value AS CHAR NO-UNDO.
  current-key-value = new-value.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-cashflow B-table-Win 
PROCEDURE verify-cashflow :
/*------------------------------------------------------------------------------
  Purpose:  Verify the validity of the current cashflow
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF NOT CAN-FIND( CFChangeType WHERE CFChangeType.CFChangeType EQ INPUT BROWSE {&BROWSE-NAME} CashFlow.CFChangeType ) THEN DO:
    MESSAGE "'" + INPUT BROWSE {&BROWSE-NAME} CashFlow.CFChangeType + "' is not a valid change type" SKIP(1)
            "Your system administrator can add CF change types"
            VIEW-AS ALERT-BOX ERROR TITLE "Invalid CF Change Type".
    RETURN "FAIL".
  END.

  IF NOT CAN-FIND( FrequencyType WHERE FrequencyType.FrequencyCode EQ INPUT BROWSE {&BROWSE-NAME} CashFlow.Frequency ) THEN DO:
    MESSAGE "'" + INPUT BROWSE {&BROWSE-NAME} CashFlow.Frequency + "' is not a valid frequency type" SKIP(1)
            "Your system administrator can add frequency types"
            VIEW-AS ALERT-BOX ERROR TITLE "Invalid Frequency".
    RETURN "FAIL".
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


