&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

{inc/topic/tpvouchr.i}


DEF VAR key-name AS CHAR NO-UNDO INITIAL "".
DEF VAR filter-list AS CHAR NO-UNDO.
DEF VAR filter-case AS CHAR NO-UNDO.
DEF VAR end-voucher AS INT NO-UNDO INITIAL ?.

DEF VAR total-value AS DECIMAL NO-UNDO COLUMN-LABEL "Total Value"  FORMAT "->>,>>>,>>9.99".
DEF VAR cheque-detail AS CHAR NO-UNDO COLUMN-LABEL "Cheque Details" FORMAT "X(45)".
DEF VAR update-detail AS CHAR NO-UNDO COLUMN-LABEL "Update Details" FORMAT "X(30)".
DEF VAR date-due      AS CHAR NO-UNDO COLUMN-LABEL "Date Due" FORMAT "X(15)".

ON FIND OF Voucher DO:
  total-value = Voucher.TaxValue + Voucher.GoodsValue.
  FIND Cheque WHERE Cheque.BankAccountCode = Voucher.BankAccountCode
                AND Cheque.ChequeNo = Voucher.ChequeNo
                NO-LOCK NO-ERROR.
  
  IF AVAILABLE(Cheque) THEN
    cheque-detail = Cheque.BankAccountCode + "/"
                  + STRING( Cheque.ChequeNo, "999999")
                  + " D:" + STRING( Cheque.Date, "99/99/9999")
                  + (IF Cheque.DatePresented > DATE(1,1,1) THEN " P:" + STRING( Cheque.DatePresented, "99/99/9999") ELSE "") .
  ELSE
    cheque-detail = "".

  update-detail = Voucher.EntityType + STRING( Voucher.EntityCode, "99999") + "-"
                + STRING( Voucher.AccountCode, "9999.99") + "  "
                + STRING( Voucher.BatchCode ) + "/" + STRING( Voucher.DocumentCode ).
END.

{inc/ofc-this.i}
{inc/ofc-set.i "CreditorVouchersDefault" "creditor-vouchers-default"}
IF NOT AVAILABLE(OfficeSetting) THEN creditor-vouchers-default = 'All Vouchers'.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Voucher

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Voucher.VoucherSeq ~
Voucher.VoucherStatus Voucher.ApproverCode Voucher.CreditorCode ~
Voucher.Date Voucher.Description Voucher.InvoiceReference ~
total-value @ total-value Voucher.GoodsValue Voucher.TaxValue ~
STRING( Voucher.DateDue, '99/99/9999' ) @ date-due ~
cheque-detail @ cheque-detail update-detail @ update-detail 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table 
&Scoped-define QUERY-STRING-br_table FOR EACH Voucher WHERE ~{&KEY-PHRASE} ~
      AND Voucher.VoucherStatus BEGINS filter-case AND ~
(end-voucher = ? OR Voucher.VoucherSeq <= end-voucher) NO-LOCK ~
    ~{&SORTBY-PHRASE}
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Voucher WHERE ~{&KEY-PHRASE} ~
      AND Voucher.VoucherStatus BEGINS filter-case AND ~
(end-voucher = ? OR Voucher.VoucherSeq <= end-voucher) NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table Voucher
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Voucher


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
ProjectCode|y|y|ttpl.Voucher.ProjectCode
OrderCode|y|y|TTPL.Voucher.OrderCode
BatchCode|y|y|ttpl.Voucher.BatchCode
CreditorCode|y|y|ttpl.Voucher.CreditorCode
VoucherSeq||y|ttpl.Voucher.VoucherSeq
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "ProjectCode,OrderCode,BatchCode,CreditorCode",
     Keys-Supplied = "ProjectCode,OrderCode,BatchCode,CreditorCode,VoucherSeq"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Date|y||ttpl.Voucher.Date|no
Voucher|||ttpl.Voucher.VoucherSeq|no
Account|||ttpl.Voucher.AccountCode|yes
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Date,Voucher,Account",
     Sort-Case = Date':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-br_table 
       MENU-ITEM m_Match        LABEL "Match"          ACCELERATOR "ALT-M".


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Voucher SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      Voucher.VoucherSeq COLUMN-LABEL "Vchr#" FORMAT ">>>>>9":U
      Voucher.VoucherStatus COLUMN-LABEL "St" FORMAT "X":U
      Voucher.ApproverCode COLUMN-LABEL "Apprvr" FORMAT "X(4)":U
      Voucher.CreditorCode COLUMN-LABEL "Creditor" FORMAT "99999":U
      Voucher.Date COLUMN-LABEL "Voucher Date" FORMAT "99/99/9999":U
      Voucher.Description FORMAT "X(65)":U COLUMN-FONT 9
      Voucher.InvoiceReference COLUMN-LABEL "Their Invoice" FORMAT "X(25)":U
            COLUMN-FONT 9
      total-value @ total-value
      Voucher.GoodsValue FORMAT "->,>>>,>>9.99":U
      Voucher.TaxValue FORMAT "->>>,>>9.99":U
      STRING( Voucher.DateDue, '99/99/9999' ) @ date-due
      cheque-detail @ cheque-detail COLUMN-FONT 9
      update-detail @ update-detail COLUMN-FONT 9
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 109.14 BY 16.2
         BGCOLOR 15 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 17.55
         WIDTH              = 110.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:POPUP-MENU IN FRAME F-Main             = MENU POPUP-MENU-br_table:HANDLE
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main     = 3
       br_table:MAX-DATA-GUESS IN FRAME F-Main         = 1000.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "ttpl.Voucher"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _Where[1]         = "Voucher.VoucherStatus BEGINS filter-case AND
(end-voucher = ? OR Voucher.VoucherSeq <= end-voucher)"
     _FldNameList[1]   > ttpl.Voucher.VoucherSeq
"Voucher.VoucherSeq" "Vchr#" ">>>>>9" "integer" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > ttpl.Voucher.VoucherStatus
"Voucher.VoucherStatus" "St" "X" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[3]   > ttpl.Voucher.ApproverCode
"Voucher.ApproverCode" "Apprvr" ? "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[4]   > ttpl.Voucher.CreditorCode
"Voucher.CreditorCode" "Creditor" ? "integer" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[5]   > ttpl.Voucher.Date
"Voucher.Date" "Voucher Date" ? "date" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[6]   > ttpl.Voucher.Description
"Voucher.Description" ? "X(65)" "character" ? ? 9 ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[7]   > ttpl.Voucher.InvoiceReference
"Voucher.InvoiceReference" "Their Invoice" "X(25)" "character" ? ? 9 ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[8]   > "_<CALC>"
"total-value @ total-value" ? ? ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[9]   = ttpl.Voucher.GoodsValue
     _FldNameList[10]   = ttpl.Voucher.TaxValue
     _FldNameList[11]   > "_<CALC>"
"STRING( Voucher.DateDue, '99/99/9999' ) @ date-due" ? ? ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[12]   > "_<CALC>"
"cheque-detail @ cheque-detail" ? ? ? ? ? 9 ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[13]   > "_<CALC>"
"update-detail @ update-detail" ? ? ? ? ? 9 ? ? ? no ? no no ? yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-DISPLAY OF br_table IN FRAME F-Main
DO:
  {inc/rowcol/rcvch1.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Match
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Match B-table-Win
ON CHOOSE OF MENU-ITEM m_Match /* Match */
DO:
  RUN vwr/mnt/d-voucher-match.w( Voucher.VoucherSeq ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

{inc/method/m-vouchr.i}

RUN set-attribute-list( 'SortBy-Options = Date|Voucher|Account|My Vouchers, SortBy-Case = Date':U ).
filter-list = "All vouchers".
FOR EACH VoucherStatus NO-LOCK BY VoucherStatus.SequenceCode:
  filter-list = filter-list + "|" + VoucherStatus.VoucherStatus + " - " + VoucherStatus.Description.
END.

RUN set-attribute-list( 'FilterBy-Options = ':U + filter-list ).

RUN set-attribute-list( 'FilterBy-Case = U - Unapproved, FilterBy-Style = Combo-Box':U ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'ProjectCode':U THEN DO:
       &Scope KEY-PHRASE Voucher.ProjectCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.Date DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Voucher':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.VoucherSeq DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Account':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.AccountCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* ProjectCode */
    WHEN 'OrderCode':U THEN DO:
       &Scope KEY-PHRASE Voucher.OrderCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.Date DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Voucher':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.VoucherSeq DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Account':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.AccountCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OrderCode */
    WHEN 'BatchCode':U THEN DO:
       &Scope KEY-PHRASE Voucher.BatchCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.Date DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Voucher':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.VoucherSeq DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Account':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.AccountCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* BatchCode */
    WHEN 'CreditorCode':U THEN DO:
       &Scope KEY-PHRASE Voucher.CreditorCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.Date DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Voucher':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.VoucherSeq DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Account':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.AccountCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* CreditorCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.Date DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Voucher':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.VoucherSeq DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Account':U THEN DO:
           &Scope SORTBY-PHRASE BY Voucher.AccountCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE change-description B-table-Win 
PROCEDURE change-description :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN process/one-off/vchrdesc.p ( IF AVAILABLE(Voucher) THEN "Voucher," + STRING(Voucher.VoucherSeq) ELSE "" ).

  RUN dispatch( 'open-query' ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-MyVouchers-status B-table-Win 
PROCEDURE get-MyVouchers-status :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE OUTPUT PARAM MyVouchers AS LOG INIT NO NO-UNDO.

     RUN get-attribute ('SortBy-Case':U).
     
     IF RETURN-VALUE =  'My Vouchers':U THEN  MyVouchers = YES.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query-cases B-table-Win 
PROCEDURE local-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).

  IF RETURN-VALUE = "ChequeNo" THEN DO:
    DEF VAR cheque-no AS INT NO-UNDO.
    DEF VAR bank-account AS CHAR NO-UNDO.

    /* Look up the current key-value. */
    RUN get-attribute ('Key-Value':U).
    cheque-no = INTEGER(RETURN-VALUE).

    /* reset the filter-case so that we see all vouchers for the cheque */
    filter-case = "".

    bank-account = find-parent-key( "BankAccountCode" ).
    
    &Scope KEY-PHRASE Voucher.BankAccountCode eq bank-account ~
                                AND Voucher.ChequeNo eq cheque-no

    RUN get-attribute ('SortBy-Case':U).
    CASE RETURN-VALUE:
      WHEN 'Date':U THEN DO:
        &Scope SORTBY-PHRASE BY Voucher.Date DESCENDING
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      WHEN 'Voucher':U THEN DO:
        &Scope SORTBY-PHRASE BY Voucher.VoucherSeq DESCENDING
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      WHEN 'Account':U THEN DO:
        &Scope SORTBY-PHRASE BY Voucher.AccountCode
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      OTHERWISE DO:
        &Undefine SORTBY-PHRASE
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END. /* OTHERWISE...*/
    END CASE.
  END.
  ELSE IF RETURN-VALUE = "OrderCode" THEN DO:
    DEF VAR order-code AS INT NO-UNDO.
    DEF VAR entity-type AS CHAR NO-UNDO.
    DEF VAR entity-code AS INT NO-UNDO.

    /* Look up the current key-value. */
    RUN get-attribute ('Key-Value':U).
    order-code = INTEGER(RETURN-VALUE).

    /* reset the filter-case so that we see all vouchers for the order */
    filter-case = "".
    entity-code = INT( find-parent-key( "OrderEntity" ) ).
    entity-type = find-parent-key( "EntityType" ).

    &Scope KEY-PHRASE Voucher.EntityType eq entity-type ~
                                AND Voucher.EntityCode eq entity-code ~
                                AND Voucher.OrderCode eq order-code

    RUN get-attribute ('SortBy-Case':U).
    CASE RETURN-VALUE:
      WHEN 'Date':U THEN DO:
        &Scope SORTBY-PHRASE BY Voucher.Date DESCENDING
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      WHEN 'Voucher':U THEN DO:
        &Scope SORTBY-PHRASE BY Voucher.VoucherSeq DESCENDING
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      WHEN 'Account':U THEN DO:
        &Scope SORTBY-PHRASE BY Voucher.AccountCode
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
      OTHERWISE DO:
        &Undefine SORTBY-PHRASE
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END. /* OTHERWISE...*/
    END CASE.
  END.
  ELSE DO:
     RUN get-attribute ('SortBy-Case':U).
     
     IF RETURN-VALUE =  'My Vouchers':U THEN DO:
        &Scope SORTBY-PHRASE BY Voucher.Date DESCENDING
        &Scope KEY-PHRASE Voucher.lastModifiedUser = OS-GETENV("PROPUSER":U) 
        {&OPEN-QUERY-{&BROWSE-NAME}}
      END.
     ELSE DO:
         /* Dispatch standard ADM method.                             */
         RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query-cases':U ) .
     END.

  END.

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE normal-send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE normal-send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "ProjectCode" "Voucher" "ProjectCode"}
  {src/adm/template/sndkycas.i "OrderCode" "Voucher" "OrderCode"}
  {src/adm/template/sndkycas.i "BatchCode" "Voucher" "BatchCode"}
  {src/adm/template/sndkycas.i "CreditorCode" "Voucher" "CreditorCode"}
  {src/adm/template/sndkycas.i "VoucherSeq" "Voucher" "VoucherSeq"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Spoof standard ADM method
------------------------------------------------------------------------------*/
  {inc/sendkey.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Voucher"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-filter AS CHAR NO-UNDO.

  IF new-filter = "All vouchers" THEN
    filter-case = "".
  ELSE
    filter-case = SUBSTRING( new-filter, 1, 1).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  key-name = new-key-name.
  
  /* Whenever we receive an external key we will default to All vouchers */
  IF key-name = "CreditorCode" THEN
    RUN set-attribute-list( 'FilterBy-Case = ' + creditor-vouchers-default ).
  ELSE IF key-name <> "" THEN
    RUN set-attribute-list( 'FilterBy-Case = All vouchers':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SearchBy-Case B-table-Win 
PROCEDURE use-SearchBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-search AS CHAR NO-UNDO.

  end-voucher = INT( new-search ).
  RUN dispatch( 'apply-entry':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

