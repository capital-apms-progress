&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

{inc/topic/tpivoice.i}
DEF VAR d-date AS CHAR NO-UNDO.
DEF VAR d-entity AS CHAR NO-UNDO.
DEF VAR d-name AS CHAR NO-UNDO.

DEF VAR filter-by AS CHAR NO-UNDO.
DEF VAR entity-type AS CHAR NO-UNDO.

DEF VAR pdf-support AS LOGICAL INIT YES NO-UNDO.
DEF VAR pdf-printing AS LOGICAL INIT NO NO-UNDO.

&Scop INDEX-PHRASE

{inc/ofc-this.i}
{inc/ofc-set-l.i "Invoice-Advice-Note" "advice-of-charge"}

/* Determine if PDF printing is supported */
{inc/ofc-set.i "PDF-Output-Directory" "pdf-output-directory"}
IF NOT AVAILABLE(OfficeSetting) THEN
    pdf-support = NO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Invoice Tenant

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Invoice.InvoiceStatus ~
Invoice.InvoiceNo STRING (Invoice.InvoiceDate,'99/99/9999') @ d-date ~
Invoice.EntityType +  STRING (Invoice.EntityCode,'99999')  @ d-entity ~
Tenant.Name Invoice.Total Invoice.ToDetail 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table 
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Invoice WHERE ~{&KEY-PHRASE} ~
      AND Invoice.EntityType BEGINS entity-type  ~
 AND Invoice.InvoiceStatus BEGINS filter-by ~{&INDEX-PHRASE} NO-LOCK, ~
      FIRST Tenant WHERE Tenant.TenantCode = Invoice.EntityCode OUTER-JOIN NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table Invoice Tenant
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Invoice
&Scoped-define SECOND-TABLE-IN-QUERY-br_table Tenant


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
InvoiceStatus||y|ttpl.Invoice.InvoiceStatus
TenantCode|y|y|ttpl.Invoice.EntityCode
InvoiceNo||y|ttpl.Invoice.InvoiceNo
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "TenantCode",
     Keys-Supplied = "InvoiceStatus,TenantCode,InvoiceNo"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Invoice No|y||ttpl.Invoice.InvoiceStatus|no,ttpl.Invoice.InvoiceNo|no
Date|||ttpl.Invoice.InvoiceStatus|yes,ttpl.Invoice.InvoiceDate|no
Tenant|||ttpl.Invoice.InvoiceStatus|yes,ttpl.Invoice.EntityType|yes,ttpl.Invoice.EntityCode|yes
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Invoice No,Date,Tenant",
     SortBy-Case = Invoice No':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Invoice, 
      Tenant SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      Invoice.InvoiceStatus COLUMN-LABEL "S" FORMAT "X":U
      Invoice.InvoiceNo FORMAT ">>>>>9":U
      STRING (Invoice.InvoiceDate,'99/99/9999') @ d-date COLUMN-LABEL "     Date" FORMAT "X(12)":U
      Invoice.EntityType +  STRING (Invoice.EntityCode,'99999')  @ d-entity COLUMN-LABEL " Entity" FORMAT "X(8)":U
      Tenant.Name COLUMN-LABEL "Entity Name" FORMAT "X(40)":U
      Invoice.Total FORMAT "->>,>>>,>>>,>>9.99":U
      Invoice.ToDetail FORMAT "X(80)":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 92 BY 15.8
         BGCOLOR 15 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 17.15
         WIDTH              = 93.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:MAX-DATA-GUESS IN FRAME F-Main         = 1000.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.Invoice,TTPL.Tenant WHERE TTPL.Invoice ... ..."
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _TblOptList       = ", FIRST OUTER"
     _Where[1]         = "Invoice.EntityType BEGINS entity-type 
 AND Invoice.InvoiceStatus BEGINS filter-by ~{&INDEX-PHRASE}"
     _JoinCode[2]      = "Tenant.TenantCode = Invoice.EntityCode"
     _FldNameList[1]   > ttpl.Invoice.InvoiceStatus
"Invoice.InvoiceStatus" "S" ? "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[2]   = ttpl.Invoice.InvoiceNo
     _FldNameList[3]   > "_<CALC>"
"STRING (Invoice.InvoiceDate,'99/99/9999') @ d-date" "     Date" "X(12)" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[4]   > "_<CALC>"
"Invoice.EntityType +  STRING (Invoice.EntityCode,'99999')  @ d-entity" " Entity" "X(8)" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[5]   > ttpl.Tenant.Name
"Tenant.Name" "Entity Name" "X(40)" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[6]   = ttpl.Invoice.Total
     _FldNameList[7]   > ttpl.Invoice.ToDetail
"Invoice.ToDetail" ? "X(80)" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-DISPLAY OF br_table IN FRAME F-Main
DO:
/* rcinv1.i */
/* row-display trigger code for Invoice browsers */
/* Row colour is determined by the InvoiceStatus field */

  IF AVAILABLE Invoice THEN DO WITH FRAME {&FRAME-NAME}:

    DEF VAR bcolor AS INT NO-UNDO.
    DEF VAR fcolor AS INT NO-UNDO.
    
    CASE Invoice.InvoiceStatus:
      WHEN "U" THEN ASSIGN bcolor = 15 fcolor = 1.
      WHEN "A" THEN ASSIGN bcolor = 15 fcolor = 2.
      OTHERWISE     ASSIGN bcolor = 8  fcolor = 0.
    END CASE.
    
    ASSIGN 
      Invoice.InvoiceNo:BGCOLOR
             IN BROWSE {&BROWSE-NAME}   = bcolor
      Invoice.InvoiceStatus:BGCOLOR     = bcolor
      d-date:BGCOLOR                    = bcolor
      d-entity:BGCOLOR                  = bcolor
      Tenant.Name:BGCOLOR               = bcolor
      Invoice.ToDetail:BGCOLOR          = bcolor
      Invoice.Total:BGCOLOR             = bcolor

      Invoice.InvoiceNo:FGCOLOR         = fcolor
      Invoice.InvoiceStatus:FGCOLOR     = fcolor
      d-date:FGCOLOR                    = fcolor
      d-entity:FGCOLOR                  = fcolor
      Tenant.Name:FGCOLOR               = fcolor
      Invoice.ToDetail:FGCOLOR          = fcolor
      Invoice.Total:FGCOLOR             = IF Total < 0 THEN 12 ELSE fcolor
    NO-ERROR.
    
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list (
  'FilterBy-Case = Unapproved, FilterBy-Options = Unapproved|Approved|All,
   SortBy-Case = Invoice No, SortBy-Options = Tenant|Invoice No|Date':U ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'TenantCode':U THEN DO:
       &Scope KEY-PHRASE Invoice.EntityCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Invoice No':U THEN DO:
           &Scope SORTBY-PHRASE BY Invoice.InvoiceStatus DESCENDING BY Invoice.InvoiceNo DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY Invoice.InvoiceStatus BY Invoice.InvoiceDate DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Tenant':U THEN DO:
           &Scope SORTBY-PHRASE BY Invoice.InvoiceStatus BY Invoice.EntityType BY Invoice.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* TenantCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Invoice No':U THEN DO:
           &Scope SORTBY-PHRASE BY Invoice.InvoiceStatus DESCENDING BY Invoice.InvoiceNo DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Date':U THEN DO:
           &Scope SORTBY-PHRASE BY Invoice.InvoiceStatus BY Invoice.InvoiceDate DESCENDING
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Tenant':U THEN DO:
           &Scope SORTBY-PHRASE BY Invoice.InvoiceStatus BY Invoice.EntityType BY Invoice.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-current-record B-table-Win 
PROCEDURE delete-current-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Invoice) THEN RETURN.
  IF Invoice.InvoiceStatus <> "U" THEN DO:
    MESSAGE "Only unapproved invoices may be deleted."
            VIEW-AS ALERT-BOX ERROR
            TITLE "Invoice cannot be deleted".
    RETURN.
  END.

DEF VAR do-it AS LOGI NO-UNDO INITIAL No.
  MESSAGE "Delete the current invoice?"
            VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO
            TITLE "Confirm Invoice Deletion"
            UPDATE do-it.
  IF NOT do-it THEN RETURN.

DO TRANSACTION:  
  FIND CURRENT Invoice EXCLUSIVE-LOCK.
  FOR EACH InvoiceLine OF Invoice EXCLUSIVE-LOCK:
    DELETE InvoiceLine.
  END.
  DELETE Invoice.
END.
  RUN dispatch( 'open-query':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-value-changed B-table-Win 
PROCEDURE inst-value-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, "reprint-invoice",
    "SENSITIVE = " + IF AVAILABLE Invoice THEN "Yes" ELSE "No" ).
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, "reprint-pdfinvoice",
    "SENSITIVE = " + IF AVAILABLE Invoice AND pdf-support THEN "Yes" ELSE "No" ).
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, "maintain",
    "SENSITIVE = " + IF AVAILABLE Invoice AND Invoice.InvoiceStatus = "U" 
      THEN "Yes" ELSE "No" ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query-cases B-table-Win 
PROCEDURE local-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR query-count AS INT NO-UNDO.

  /* Code placed here will execute PRIOR to standard behavior. */
  IF filter-by = '' THEN DO:
    DEF VAR key-value AS CHAR NO-UNDO.

    /* Look up the current key-value. */
    RUN get-attribute ('Key-Value':U).
    key-value = RETURN-VALUE.

    /* Find the current record using the current Key-Name. */
    RUN get-attribute ('Key-Name':U).
    CASE RETURN-VALUE:
      WHEN 'TenantCode' THEN DO:
         &Scope KEY-PHRASE Invoice.EntityType eq 'T' AND Invoice.EntityCode eq INTEGER(key-value)
         &Undefine INDEX-PHRASE
         RUN get-attribute ('SortBy-Case':U).
         CASE RETURN-VALUE:
           WHEN 'Invoice No':U THEN DO:
             &Scope SORTBY-PHRASE BY Invoice.InvoiceNo DESCENDING
             {&OPEN-QUERY-{&BROWSE-NAME}}
           END.
           WHEN 'Date':U THEN DO:
             &Scope SORTBY-PHRASE BY Invoice.InvoiceDate DESCENDING
             {&OPEN-QUERY-{&BROWSE-NAME}}
           END.
           WHEN 'Tenant':U THEN DO:
             &Scope SORTBY-PHRASE BY Invoice.EntityType BY Invoice.EntityCode
             {&OPEN-QUERY-{&BROWSE-NAME}}
           END.
           OTHERWISE DO:
             &Undefine SORTBY-PHRASE
             {&OPEN-QUERY-{&BROWSE-NAME}}
           END. /* OTHERWISE...*/
         END CASE.
      END. /* OTHERWISE...*/
      OTHERWISE DO:
         &Scope KEY-PHRASE TRUE
         RUN get-attribute ('SortBy-Case':U).
         CASE RETURN-VALUE:
           WHEN 'Invoice No':U THEN DO:
             &Scope SORTBY-PHRASE BY Invoice.InvoiceNo DESCENDING
             &Scope INDEX-PHRASE USE-INDEX Invoices 
             {&OPEN-QUERY-{&BROWSE-NAME}}
           END.
           WHEN 'Date':U THEN DO:
             &Scope SORTBY-PHRASE BY Invoice.InvoiceDate DESCENDING
             &Scope INDEX-PHRASE USE-INDEX XAK2Date 
             {&OPEN-QUERY-{&BROWSE-NAME}}
           END.
           WHEN 'Tenant':U THEN DO:
             &Scope SORTBY-PHRASE BY Invoice.EntityType BY Invoice.EntityCode
             &Scope INDEX-PHRASE USE-INDEX XAK1Entity 
             {&OPEN-QUERY-{&BROWSE-NAME}}
           END.
           OTHERWISE DO:
             &Undefine SORTBY-PHRASE
             &Undefine INDEX-PHRASE
             {&OPEN-QUERY-{&BROWSE-NAME}}
           END. /* OTHERWISE...*/
         END CASE.
      END. /* OTHERWISE...*/
    END CASE.
  END.
  ELSE
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query-cases':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  /* Count the rows and set the scroll bar */
  GET FIRST {&BROWSE-NAME}.
  DO WHILE AVAILABLE({&FIRST-TABLE-IN-QUERY-br_table}):
    query-count = query-count + 1.
    GET NEXT {&BROWSE-NAME}.
  END.
  ASSIGN {&BROWSE-NAME}:MAX-DATA-GUESS IN FRAME F-Main = query-count.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reprint-invoice B-table-Win 
PROCEDURE reprint-invoice :
/*------------------------------------------------------------------------------
  Purpose: Reprint the invoice to the printer
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE Invoice THEN RETURN.

  IF advice-of-charge = Yes THEN DO:
    RUN process/report/advice-of-charge.p( "InvoiceList," + STRING(Invoice.InvoiceNo) ).
  END.
  ELSE IF Invoice.InvoiceStatus = "U" THEN 
    RUN process/report/invcappr.p( Invoice.InvoiceNo, Invoice.InvoiceNo, ? ).
  ELSE DO:
    RUN process/report/taxinvce.p(
      "InvoiceRange,"
      + STRING(Invoice.InvoiceNo)
      + "," + STRING( Invoice.InvoiceNo )
    ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reprint-pdfinvoice B-table-Win 
PROCEDURE reprint-pdfinvoice :
/*------------------------------------------------------------------------------
  Purpose: Reprint the invoice in PDF format
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE Invoice THEN RETURN.

  IF advice-of-charge = Yes THEN DO:
    RUN process/report/advice-of-charge.p( "InvoiceList," + STRING(Invoice.InvoiceNo) ).
  END.
  ELSE IF Invoice.InvoiceStatus = "U" THEN 
    RUN process/report/invcappr.p( Invoice.InvoiceNo, Invoice.InvoiceNo, ? ).
  ELSE DO:
    RUN process/report/taxinvce.p(
      "InvoiceRange,"
      + STRING(Invoice.InvoiceNo)
      + "," + STRING( Invoice.InvoiceNo )
      + "~nOutputPDF"
    ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "InvoiceStatus" "Invoice" "InvoiceStatus"}
  {src/adm/template/sndkycas.i "TenantCode" "Invoice" "EntityCode"}
  {src/adm/template/sndkycas.i "InvoiceNo" "Invoice" "InvoiceNo"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Invoice"}
  {src/adm/template/snd-list.i "Tenant"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-case AS CHAR NO-UNDO.

  IF new-case = 'All' THEN filter-by = ''.
  ELSE filter-by = SUBSTRING( new-case, 1, 1).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:  Help work out the query
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  CASE new-key-name:
    WHEN 'TenantCode' THEN      entity-type = 'T'.
    WHEN 'CreditorCode' THEN    entity-type = 'C'.
    WHEN 'ProjectCode' THEN     entity-type = 'J'.
    WHEN 'PropertyCode' THEN    entity-type = 'P'.
    WHEN 'CompanyCode' THEN     entity-type = 'L'.
    OTHERWISE DO:
      MESSAGE "Can't cope with '" new-key-name "'".
    END.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

