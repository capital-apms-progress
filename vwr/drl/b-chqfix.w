&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-acct.i "PYMTBANK" "payment-bank-account-code"}
DEF VAR pymt-bank AS CHAR NO-UNDO.
pymt-bank = OfficeControlAccount.Description .

DEF VAR from-cheque AS INT NO-UNDO.
FIND LAST Cheque WHERE Cheque.BankAccountCode = pymt-bank NO-LOCK NO-ERROR.
from-cheque = (IF AVAILABLE(Cheque) THEN Cheque.ChequeNo - 500 ELSE 0).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Cheque NewDocument

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Cheque.ChequeNo Cheque.Amount ~
Cheque.CreditorCode Cheque.Date Cheque.BatchCode Cheque.PayeeName 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table Cheque.ChequeNo 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}ChequeNo ~{&FP2}ChequeNo ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table Cheque
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table Cheque
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Cheque ~
      WHERE Cheque.BankAccountCode = pymt-bank ~
AND Cheque.ChequeNo >= from-cheque NO-LOCK, ~
      EACH NewDocument WHERE NewDocument.BatchCode = Cheque.BatchCode ~
  AND NewDocument.DocumentCode = Cheque.DocumentCode ~
  AND NewDocument.DocumentType = "CHEQ" ~
  AND NewDocument.Reference = STRING( Cheque.ChequeNo ) NO-LOCK ~
    BY Cheque.BankAccountCode DESCENDING ~
       BY Cheque.ChequeNo DESCENDING.
&Scoped-define TABLES-IN-QUERY-br_table Cheque NewDocument
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Cheque


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
FromCheque||y|ttpl.Cheque.ChequeNo
BankAccountCode||y|ttpl.BankAccount.BankAccountCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = "FromCheque,BankAccountCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Cheque, 
      NewDocument SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      Cheque.ChequeNo COLUMN-LABEL "Cheque #"
      Cheque.Amount
      Cheque.CreditorCode
      Cheque.Date COLUMN-LABEL "Cheque Date"
      Cheque.BatchCode COLUMN-LABEL "Batch #"
      Cheque.PayeeName COLUMN-LABEL "Payee Name" FORMAT "X(65)"
  ENABLE
      Cheque.ChequeNo
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 92.57 BY 12
         BGCOLOR 16 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 12.4
         WIDTH              = 95.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "ttpl.Cheque,ttpl.NewDocument WHERE ttpl.Cheque ..."
     _Options          = "NO-LOCK"
     _TblOptList       = ","
     _OrdList          = "ttpl.Cheque.BankAccountCode|no,ttpl.Cheque.ChequeNo|no"
     _Where[1]         = "Cheque.BankAccountCode = pymt-bank
AND Cheque.ChequeNo >= from-cheque"
     _JoinCode[2]      = "NewDocument.BatchCode = Cheque.BatchCode
  AND NewDocument.DocumentCode = Cheque.DocumentCode
  AND NewDocument.DocumentType = ""CHEQ""
  AND NewDocument.Reference = STRING( Cheque.ChequeNo )"
     _FldNameList[1]   > ttpl.Cheque.ChequeNo
"Cheque.ChequeNo" "Cheque #" ? "integer" ? ? ? ? ? ? yes ?
     _FldNameList[2]   = ttpl.Cheque.Amount
     _FldNameList[3]   = ttpl.Cheque.CreditorCode
     _FldNameList[4]   > ttpl.Cheque.Date
"Cheque.Date" "Cheque Date" ? "date" ? ? ? ? ? ? no ?
     _FldNameList[5]   > ttpl.Cheque.BatchCode
"Cheque.BatchCode" "Batch #" ? "integer" ? ? ? ? ? ? no ?
     _FldNameList[6]   > ttpl.Cheque.PayeeName
"Cheque.PayeeName" "Payee Name" "X(65)" "character" ? ? ? ? ? ? no ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Cheque.ChequeNo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Cheque.ChequeNo br_table _BROWSE-COLUMN B-table-Win
ON LEAVE OF Cheque.ChequeNo IN BROWSE br_table /* Cheque # */
DO:
  IF SELF:MODIFIED THEN DO:
    RUN change-cheque-number( Cheque.ChequeNo, INT(SELF:SCREEN-VALUE)).
    IF RETURN-VALUE = "FAIL" THEN SELF:SCREEN-VALUE = STRING( Cheque.ChequeNo, SELF:FORMAT ).
    SELF:MODIFIED = No.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

  {&OPEN-QUERY-{&BROWSE-NAME}}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE change-cheque-number B-table-Win 
PROCEDURE change-cheque-number :
/*------------------------------------------------------------------------------
  Purpose:  Change the cheque number and associated records.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER old-num AS INT NO-UNDO.
DEF INPUT PARAMETER new-num AS INT NO-UNDO.

DEF BUFFER AltChq FOR Cheque.

  IF CAN-FIND( AltChq WHERE AltChq.BankAccountCode = Cheque.BankAccountCode
                        AND AltChq.ChequeNo = new-num ) THEN DO:
    MESSAGE "A cheque already exists with that number" SKIP(1)
            "You must change the number to one that is not in use."
            VIEW-AS ALERT-BOX ERROR TITLE "Cheque Number Used".
    RETURN "FAIL".
  END.

  DO TRANSACTION:
    FIND CURRENT Cheque EXCLUSIVE-LOCK.
    Cheque.ChequeNo = new-num.
    FIND CURRENT Cheque NO-LOCK.
    FIND NewDocument WHERE NewDocument.BatchCode = Cheque.BatchCode
            AND NewDocument.DocumentCode = Cheque.DocumentCode EXCLUSIVE-LOCK.
    NewDocument.Reference = STRING( Cheque.ChequeNo ).
    FIND CURRENT NewDocument NO-LOCK.
    FOR EACH Voucher WHERE Voucher.VoucherStatus = "P"
                         AND Voucher.CreditorCode = Cheque.CreditorCode
                         AND Voucher.BankAccountCode = Cheque.BankAccountCode
                         AND Voucher.ChequeNo = old-num EXCLUSIVE-LOCK:
      Voucher.ChequeNo = new-num .
    END.
  END.
  RUN dispatch( 'display-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "FromCheque" "Cheque" "ChequeNo"}
  {src/adm/template/sndkycas.i "BankAccountCode" "BankAccount" "BankAccountCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Cheque"}
  {src/adm/template/snd-list.i "NewDocument"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  key-name = new-key-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value B-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-value AS CHAR NO-UNDO.
  key-value = new-key-value.

  CASE key-name:
    WHEN "FromCheque" THEN from-cheque = INT(key-value).
    WHEN "BankAccountCode" THEN DO:
      pymt-bank = key-value.
      FIND LAST Cheque WHERE Cheque.BankAccountCode = pymt-bank NO-LOCK NO-ERROR.
/*      from-cheque = (IF AVAILABLE(Cheque) THEN Cheque.ChequeNo - 500 ELSE 0). */
      from-cheque = 0 .
    END.
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


