&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*----------------------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR source-link-name AS CHAR NO-UNDO.
DEF VAR source-table AS CHAR NO-UNDO.
DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO.

DEF VAR status-code AS CHAR NO-UNDO.
DEF VAR not-status AS LOGICAL NO-UNDO.
DEF VAR fltr-by AS CHAR NO-UNDO.
DEF VAR filter-list AS CHAR NO-UNDO.

DEF VAR d-date AS CHAR NO-UNDO.

DEF VAR entity-type  AS CHAR    NO-UNDO.
DEF VAR entity-code  AS INT     NO-UNDO.
DEF VAR account-code AS DECIMAL NO-UNDO.
DEF VAR month-code   AS INT     NO-UNDO.

DEF VAR sundry-debtors   LIKE account-code NO-UNDO.
DEF VAR sundry-creditors LIKE account-code NO-UNDO.

&GLOB ALL-MONTHS "All Months"

{inc/topic/tpacttrn.i}

{inc/ofc-this.i}
{inc/ofc-set-l.i "Multi-Ledger-Creditors" "multi-ledger-creditors"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES AccountSummary AccountBalance
&Scoped-define FIRST-EXTERNAL-TABLE AccountSummary


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR AccountSummary, AccountBalance.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES AcctTran Document Person

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table AcctTran.BatchCode AcctTran.ClosedState STRING( AcctTran.Date, '99/99/9999') @ d-date (IF AcctTran.Reference = '' THEN Document.Reference ELSE AcctTran.Reference) @ AcctTran.Reference AcctTran.Amount (IF AcctTran.Description = '' THEN Document.Description ELSE AcctTran.Description) @ AcctTran.Description AcctTran.FlagAttention (IF AcctTran.FlaggedBy > 0 THEN Person.FirstName ELSE '') @ Person.FirstName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table AcctTran.FlagAttention   
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table AcctTran
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table AcctTran
&Scoped-define SELF-NAME br_table
&Scoped-define OPEN-QUERY-br_table RUN pre-open-query. OPEN QUERY br_table FOR EACH AcctTran NO-LOCK         WHERE AcctTran.EntityType = entity-type         AND AcctTran.EntityCode = entity-code         AND (AcctTran.AccountCode = account-code OR (multi-ledger-creditors AND entity-type = 'C'))         AND ( IF month-code > 0 THEN AcctTran.MonthCode = month-code ELSE                  (IF month-code < 0 THEN AcctTran.MonthCode >= (- month-code) ELSE Yes) )         AND (IF not-status THEN NOT AcctTran.ClosedState BEGINS status-code                            ELSE AcctTran.ClosedState BEGINS status-code ), ~
               FIRST Document OF AcctTran OUTER-JOIN NO-LOCK, ~
               FIRST Person WHERE Person.PersonCode = AcctTran.FlaggedBy OUTER-JOIN NO-LOCK         BY AcctTran.EntityType BY AcctTran.EntityCode BY AcctTran.AccountCode         BY AcctTran.MonthCode DESCENDING BY AcctTran.Date DESCENDING.
&Scoped-define TABLES-IN-QUERY-br_table AcctTran Document Person
&Scoped-define FIRST-TABLE-IN-QUERY-br_table AcctTran
&Scoped-define SECOND-TABLE-IN-QUERY-br_table Document
&Scoped-define THIRD-TABLE-IN-QUERY-br_table Person


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
BatchCode||y|AcctTran.BatchCode
ChequeNo||y|Voucher.ChequeNo
AccountCode||y|AcctTran.AccountCode
EntityType||y|AcctTran.EntityType
ClosingGroup||y|AcctTran.ClosingGroup
BankAccountCode||y|Voucher.BankAccountCode
DocumentCode||y|AcctTran.DocumentCode
EntityCode||y|AcctTran.EntityCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = "BatchCode,ChequeNo,AccountCode,EntityType,ClosingGroup,BankAccountCode,DocumentCode,EntityCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-bank-account-code B-table-Win 
FUNCTION get-bank-account-code RETURNS CHARACTER
  ( INPUT trn-ref AS CHAR  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-cheque-no B-table-Win 
FUNCTION get-cheque-no RETURNS INTEGER
  ( INPUT trn-ref AS CHAR  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      AcctTran, 
      Document, 
      Person SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _FREEFORM
  QUERY br_table NO-LOCK DISPLAY
      AcctTran.BatchCode COLUMN-LABEL 'Batch' FORMAT '99999'
      AcctTran.ClosedState COLUMN-LABEL 'C' FORMAT 'X'
      STRING( AcctTran.Date, '99/99/9999') @ d-date COLUMN-LABEL '    Date' FORMAT 'X(12)'
      (IF AcctTran.Reference = '' THEN Document.Reference ELSE AcctTran.Reference) @ AcctTran.Reference FORMAT 'X(14)' COLUMN-LABEL 'Reference'
      AcctTran.Amount FORMAT '->>>,>>>,>>9.99'
      (IF AcctTran.Description = '' THEN Document.Description ELSE AcctTran.Description) @ AcctTran.Description FORMAT 'X(66)'
      AcctTran.FlagAttention
      (IF AcctTran.FlaggedBy > 0 THEN Person.FirstName ELSE '') @ Person.FirstName FORMAT 'X(14)' COLUMN-LABEL 'Flag by'
  ENABLE
      AcctTran.FlagAttention
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 103.43 BY 15.6
         BGCOLOR 16 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   External Tables: ttpl.AccountSummary,ttpl.AccountBalance
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 15.7
         WIDTH              = 112.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}
{inc/method/m-excel.i}
{inc/excel/doc-trans.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:MAX-DATA-GUESS IN FRAME F-Main         = 500.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _START_FREEFORM
RUN pre-open-query.
OPEN QUERY br_table FOR EACH AcctTran NO-LOCK
        WHERE AcctTran.EntityType = entity-type
        AND AcctTran.EntityCode = entity-code
        AND (AcctTran.AccountCode = account-code OR (multi-ledger-creditors AND entity-type = 'C'))
        AND ( IF month-code > 0 THEN AcctTran.MonthCode = month-code ELSE
                 (IF month-code < 0 THEN AcctTran.MonthCode >= (- month-code) ELSE Yes) )
        AND (IF not-status THEN NOT AcctTran.ClosedState BEGINS status-code
                           ELSE AcctTran.ClosedState BEGINS status-code ),
        FIRST Document OF AcctTran OUTER-JOIN NO-LOCK,
        FIRST Person WHERE Person.PersonCode = AcctTran.FlaggedBy OUTER-JOIN NO-LOCK
        BY AcctTran.EntityType BY AcctTran.EntityCode BY AcctTran.AccountCode
        BY AcctTran.MonthCode DESCENDING BY AcctTran.Date DESCENDING.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON MOUSE-SELECT-DBLCLICK OF br_table IN FRAME F-Main
DO:
  RUN special-enable-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-DISPLAY OF br_table IN FRAME F-Main
DO:
  {inc/rowcol/rcactamt.i}
  {inc/rowcol/rcactacx.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */
{inc/ofc-this.i}

FIND FIRST OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "DEBTORS" NO-LOCK.
sundry-debtors = OfficeControlAccount.AccountCode.
FIND FIRST OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "CREDITORS" NO-LOCK.
sundry-creditors = OfficeControlAccount.AccountCode.

RUN set-attribute-list ( 'SortBy-Case = Open, SortBy-Options = Not closed|Open|Part closed|Closed|All':U ).

RUN set-month-filter.

RUN set-attribute-list ( 'FilterBy-Style = Combo-Box, FilterBy-Options = ':U + filter-list + ', FilterBy-Case = ':U + {&ALL-MONTHS} ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

  {&OPEN-QUERY-{&BROWSE-NAME}}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win 
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN my-row-available.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE dump-to-excel B-table-Win 
PROCEDURE dump-to-excel :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR r AS INT NO-UNDO.
DEF VAR book-mark AS ROWID NO-UNDO.
DEF VAR do-it AS LOGI INITIAL No NO-UNDO.

  r = NUM-RESULTS( "{&BROWSE-NAME}" ).
  IF r > 100 THEN DO:
    MESSAGE "This will dump" r "rows to a new spreadsheet," SKIP
            "possibly more, and is likely to take at least" (r / 200) "minutes." SKIP(1)
            "Continue?"
            VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL
            TITLE "Big Spreadsheet Warning!"
            UPDATE do-it.
    IF NOT( do-it ) THEN RETURN.
  END.
  ELSE DO:
    MESSAGE "This may take a long time.  Approximately 300 records per minute" SKIP
            "will be written to the spreadsheet" SKIP(1)
            "Continue?"
            VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL
            TITLE "Exporting Transactions"
            UPDATE do-it.
    IF NOT( do-it ) THEN RETURN.
  END.

  start-excel().
  IF VALID-HANDLE( chExcelApplication ) THEN DO:
    create-workbook().
    select-sheet(1).

    RUN assign-headings.

    set-excel-visible(Yes).
    set-fast-mode(Yes).

    DO WITH FRAME {&FRAME-NAME}:

      book-mark = ROWID( {&FIRST-TABLE-IN-QUERY-{&BROWSE-NAME}} ).
      GET FIRST {&BROWSE-NAME} NO-LOCK.
      r = 2.
      REPEAT WHILE AVAILABLE( {&FIRST-TABLE-IN-QUERY-{&BROWSE-NAME}} ):
        RUN assign-row(r).
        GET NEXT {&BROWSE-NAME} NO-LOCK.
        r = r + 1.
        IF (r MOD 10) = 0 THEN DO:
          set-fast-mode( No ).
          set-fast-mode( Yes ).
        END.
      END.

      REPOSITION {&BROWSE-NAME} TO ROWID book-mark.
      GET CURRENT {&BROWSE-NAME} NO-LOCK.

    END.
    set-fast-mode(No).
    release-excel().
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-account-summary B-table-Win 
PROCEDURE get-account-summary :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER et AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER ec AS INT  NO-UNDO.
DEF OUTPUT PARAMETER ac AS DEC  NO-UNDO.

DEF VAR key-value AS CHAR NO-UNDO.

  RUN get-link-handle IN adm-broker-hdl ( INPUT THIS-PROCEDURE, 'record-source':U,
                                           OUTPUT source-link-name ).
  RUN send-key IN WIDGET-HANDLE(source-link-name) ( source-table, OUTPUT key-value ).
  IF source-table = "AccountSummary" AND NUM-ENTRIES(key-value, "/") > 2 THEN DO:
    et = ENTRY( 1, key-value, "/").
    ec = INT( ENTRY( 2, key-value, "/")).
    ac = DEC( ENTRY( 3, key-value, "/")).
  END.
  IF NOT AVAILABLE(AccountSummary) THEN FIND FIRST AccountSummary NO-LOCK NO-ERROR.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize B-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  AcctTran.FlagAttention:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE my-row-available B-table-Win 
PROCEDURE my-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Completely override the progress row-available method
------------------------------------------------------------------------------*/
  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  IF {inc/availrow.i "list" "Tenant"}
  ELSE IF {inc/availrow.i "list" "Creditor"}
  ELSE IF {inc/availrow.i "list" "ProjectBudget"}
  ELSE IF {inc/availrow.i "list" "AccountSummary"}
  ELSE IF {inc/availrow.i "list" "AccountBalance"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  IF {inc/availrow.i "find" "Tenant"}
  ELSE IF {inc/availrow.i "find" "Creditor"}
  ELSE IF {inc/availrow.i "find" "ProjectBudget"}
  ELSE IF {inc/availrow.i "find" "AccountSummary"}
  ELSE IF {inc/availrow.i "find" "AccountBalance"}

  /*
   * IF AccountBalance is our source table then we will set the initial
   * filter to only show transactions of that month.
   */
  IF source-table = "AccountBalance" AND AVAILABLE(AccountBalance) THEN DO:
  DEF VAR filter-case AS CHAR NO-UNDO.
  DEF VAR pos AS INT NO-UNDO.

    FIND FIRST Month WHERE Month.MonthCode = AccountBalance.MonthCode NO-LOCK NO-ERROR.
    IF AVAILABLE(Month) THEN DO:
      IF Month.EndDate >= TODAY THEN DO:
        FIND LAST Month WHERE Month.StartDate <= TODAY NO-LOCK NO-ERROR.
        filter-case = STRING( Month.StartDate, "99/99/9999") + " onwards ".
      END.
      ELSE
        filter-case = STRING( Month.StartDate, "99/99/9999") + " to " + STRING( Month.EndDate, "99/99/9999").
      pos = LOOKUP( filter-case, filter-list, "|" ).
      IF pos = 0 THEN DO:
        pos = NUM-ENTRIES(filter-list, "|").
        filter-case = ENTRY(pos, filter-list, "|" ).
      END.
      RUN set-attribute-list ( 'FilterBy-Case = ':U + filter-case ).
      RUN notify( 'apply-style,FilterBy-Source':U ).
    END.
  END.

  RUN dispatch IN THIS-PROCEDURE ('open-query':U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-open-query B-table-Win 
PROCEDURE pre-open-query :
/*------------------------------------------------------------------------------
  Purpose:  Sets the values of entity-type etc for the query.
------------------------------------------------------------------------------*/
  RUN get-attribute( 'FilterBy-Case':U ). /* Month for transactions */
  fltr-by = RETURN-VALUE.
  IF fltr-by = {&ALL-MONTHS} THEN
    month-code = 0.
  ELSE IF SUBSTRING( fltr-by, 1, 7) = 'before ' THEN DO:
    month-code = 0.
  END.
  ELSE DO:
    FIND FIRST Month WHERE Month.StartDate = DATE( SUBSTRING(fltr-by,1,10) ) NO-LOCK.
    IF Month.StartDate <= TODAY AND Month.EndDate >= TODAY THEN
      month-code = - Month.MonthCode.
    ELSE
      month-code = Month.MonthCode.
  END.
  IF source-table = "Tenant" THEN ASSIGN
    entity-type = "T"
    entity-code = Tenant.TenantCode
    account-code = sundry-debtors
    .
  ELSE IF source-table = "Creditor" THEN ASSIGN
    entity-type = "C"
    entity-code = Creditor.CreditorCode
    account-code = sundry-creditors
    .
  ELSE IF source-table = "ProjectBudget" THEN ASSIGN
    entity-type = "J"
    entity-code = ProjectBudget.ProjectCode
    account-code = ProjectBudget.AccountCode
    .
  ELSE IF source-table = "AccountSummary" AND key-name = source-table THEN
    RUN get-account-summary( OUTPUT entity-type, OUTPUT entity-code, OUTPUT account-code ).
  ELSE IF source-table = "AccountBalance" OR AVAILABLE(AccountBalance) THEN ASSIGN
    entity-type = AccountBalance.EntityType
    entity-code = AccountBalance.EntityCode
    account-code = AccountBalance.AccountCode
    .

  IF entity-type <> "T" AND entity-type <> "C" THEN 
    status-code = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available B-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Figure out which table we're using a row-available from
------------------------------------------------------------------------------*/
  IF have-records THEN RETURN.

  RUN get-link-handle IN adm-broker-hdl ( INPUT THIS-PROCEDURE, 'record-source':U,
                                           OUTPUT source-link-name ).
  RUN get-attribute IN WIDGET-HANDLE(source-link-name) ('internal-tables':U ).
  source-table = RETURN-VALUE.

  RUN get-link-handle IN adm-broker-hdl ( INPUT THIS-PROCEDURE, 'SortBy-Source':U,
                                           OUTPUT source-link-name ).
  DEF VAR wh AS HANDLE NO-UNDO.
  wh = WIDGET-HANDLE(source-link-name).
  IF VALID-HANDLE( wh ) THEN
    RUN set-attribute-list IN wh ( 'Label = Types':U ).

  IF INDEX( source-table, "Tenant" ) > 0 THEN               source-table = "Tenant".
  ELSE IF INDEX( source-table, "Creditor" ) > 0 THEN        source-table = "Creditor".
  ELSE IF INDEX( source-table, "ProjectBudget" ) > 0 THEN   source-table = "ProjectBudget".
  ELSE IF INDEX( source-table, "AccountSummary" ) > 0 THEN  source-table = "AccountSummary".
  ELSE IF INDEX( source-table, "AccountBalance" ) > 0 THEN  source-table = "AccountBalance".

  IF source-table <> "AccountSummary" THEN FIND FIRST AccountSummary NO-LOCK NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reopen-group B-table-Win 
PROCEDURE reopen-group :
/*------------------------------------------------------------------------------
  Purpose:  Reopen a closed group of transactions
------------------------------------------------------------------------------*/
DEF BUFFER altTran FOR AcctTran.

  IF NOT AVAILABLE(AcctTran) THEN RETURN.
  IF AcctTran.ClosingGroup = ? THEN DO:
    IF AcctTran.ClosedState <> "O" THEN DO:
      DO TRANSACTION:
        FIND CURRENT AcctTran EXCLUSIVE-LOCK.
        AcctTran.ClosedState = "O".
        FIND CURRENT AcctTran NO-LOCK.
      END.
      RUN dispatch( 'open-query':U ).
    END.
    RETURN.
  END.

  IF AcctTran.ClosedState = "F" THEN DO:
  DEF VAR yes-ok AS LOGICAL NO-UNDO INITIAL No.
    MESSAGE "Re-open all transactions in this group?" VIEW-AS ALERT-BOX QUESTION
            BUTTONS OK-CANCEL TITLE "Confirm Opening Closed Group"
            UPDATE yes-ok.
    IF NOT yes-ok THEN RETURN.
  END.

  FIND ClosingGroup SHARE-LOCK OF AcctTran NO-ERROR.
  IF AVAILABLE(ClosingGroup) THEN DO TRANSACTION:
    FIND CURRENT ClosingGroup EXCLUSIVE-LOCK.
    ASSIGN  ClosingGroup.ClosedStatus = "P".
    IF ClosingGroup.Description = "" THEN ClosingGroup.Description = "Reopened group".
    FIND CURRENT ClosingGroup NO-LOCK.

    FOR EACH altTran EXCLUSIVE-LOCK OF ClosingGroup:
      altTran.ClosedState = "P".
    END.
  END.
  ELSE DO TRANSACTION:
    FIND CURRENT AcctTran EXCLUSIVE-LOCK.
    AcctTran.ClosedState = "O".
    AcctTran.ClosingGroup = ?.
    FIND CURRENT AcctTran NO-LOCK.
  END.

  RUN dispatch( 'open-query':U ).
  MESSAGE "Transaction group has been converted to 'part-closed'"
        VIEW-AS ALERT-BOX INFORMATION.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* short-circuit condition when request matches parent */
  WHEN key-name THEN            pc_key-value = key-value.

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "BatchCode" "AcctTran" "BatchCode"}
  {src/adm/template/sndkycas.i "AccountCode" "AcctTran" "AccountCode"}
  {src/adm/template/sndkycas.i "EntityType" "AcctTran" "EntityType"}
  {src/adm/template/sndkycas.i "ClosingGroup" "AcctTran" "ClosingGroup"}
  {src/adm/template/sndkycas.i "DocumentCode" "AcctTran" "DocumentCode"}
  {src/adm/template/sndkycas.i "EntityCode" "AcctTran" "EntityCode"}

  /* Handle some special cases */
  WHEN "BankAccountCode" THEN   pc_key-value = STRING(get-bank-account-code(AcctTran.Reference)).
  WHEN "ChequeNo" THEN          pc_key-value = STRING(get-cheque-no(AcctTran.Reference)).

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key-original B-table-Win  adm/support/_key-snd.p
PROCEDURE send-key-original :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "BatchCode" "AcctTran" "BatchCode"}
  {src/adm/template/sndkycas.i "ChequeNo" "Voucher" "ChequeNo"}
  {src/adm/template/sndkycas.i "AccountCode" "AcctTran" "AccountCode"}
  {src/adm/template/sndkycas.i "EntityType" "AcctTran" "EntityType"}
  {src/adm/template/sndkycas.i "ClosingGroup" "AcctTran" "ClosingGroup"}
  {src/adm/template/sndkycas.i "BankAccountCode" "Voucher" "BankAccountCode"}
  {src/adm/template/sndkycas.i "DocumentCode" "AcctTran" "DocumentCode"}
  {src/adm/template/sndkycas.i "EntityCode" "AcctTran" "EntityCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "AccountSummary"}
  {src/adm/template/snd-list.i "AccountBalance"}
  {src/adm/template/snd-list.i "AcctTran"}
  {src/adm/template/snd-list.i "Document"}
  {src/adm/template/snd-list.i "Person"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-month-filter B-table-Win 
PROCEDURE set-month-filter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR start-of-month AS DATE NO-UNDO.

  filter-list = {&ALL-MONTHS}.
  FIND Month WHERE Month.StartDate <= TODAY AND Month.EndDate >= TODAY NO-LOCK NO-ERROR.
  IF AVAILABLE(Month) THEN DO:
    filter-list = filter-list + "|" + STRING( Month.StartDate, "99/99/9999")
                    + " onwards ".
    start-of-month = Month.StartDate.
  END.
  FOR EACH Month WHERE Month.StartDate >= (TODAY - 1000) AND Month.StartDate < start-of-month NO-LOCK
              BY Month.StartDate DESCENDING:
    filter-list = filter-list + "|" + STRING( Month.StartDate, "99/99/9999")
                    + " to " + STRING( Month.EndDate, "99/99/9999").
  END.

  FIND LAST Month WHERE Month.StartDate < (TODAY - 1000) NO-LOCK NO-ERROR.
  IF AVAILABLE(Month) THEN
    filter-list = filter-list + "|before " + STRING( Month.StartDate, "99/99/9999").
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE special-enable-fields B-table-Win 
PROCEDURE special-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  AcctTran.FlagAttention:READ-ONLY IN BROWSE {&BROWSE-NAME} = No.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-name AS CHAR NO-UNDO.
  key-name = new-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value B-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-value AS CHAR NO-UNDO.
  key-value = new-value.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SortBy-Case B-table-Win 
PROCEDURE use-SortBy-Case :
/*------------------------------------------------------------------------------
  Purpose:  SortBy hijacked as second filter!
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-status-filter AS CHAR NO-UNDO.
  CASE SUBSTRING( new-status-filter, 1, 1):
    WHEN "N" THEN ASSIGN    status-code = "F"     not-status = Yes .
    WHEN "O" THEN ASSIGN    status-code = "O"     not-status = No .
    WHEN "P" THEN ASSIGN    status-code = "P"     not-status = No .
    WHEN "C" THEN ASSIGN    status-code = "F"     not-status = No .
    WHEN "A" THEN ASSIGN    status-code = ""      not-status = No .
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-bank-account-code B-table-Win 
FUNCTION get-bank-account-code RETURNS CHARACTER
  ( INPUT trn-ref AS CHAR  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  IF trn-ref BEGINS "VCHR" THEN DO:
    FIND Voucher WHERE Voucher.VoucherSeq = INT( SUBSTRING(trn-ref, 5) ) NO-LOCK NO-ERROR.
    IF AVAILABLE(Voucher) THEN RETURN Voucher.BankAccountCode.
  END.

  RETURN "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-cheque-no B-table-Win 
FUNCTION get-cheque-no RETURNS INTEGER
  ( INPUT trn-ref AS CHAR  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  IF trn-ref BEGINS "VCHR" THEN DO:
    FIND Voucher WHERE Voucher.VoucherSeq = INT( SUBSTRING(trn-ref, 5) ) NO-LOCK NO-ERROR.
    IF AVAILABLE(Voucher) THEN RETURN Voucher.ChequeNo.
  END.

  RETURN 0.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

