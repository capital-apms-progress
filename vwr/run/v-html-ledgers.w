&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "Andrew" NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Char3 RP.Int1 RP.Int2 RP.Log3 ~
RP.Log2 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_month1 cmb_month2 Btn_OK RECT-1 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Char3 RP.Int1 RP.Int2 RP.Log3 ~
RP.Log2 
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define DISPLAYED-OBJECTS fil_comp1 fil_comp2 cmb_month1 cmb_month2 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "&OK" 
     SIZE 11.57 BY 1.2
     BGCOLOR 8 FONT 9.

DEFINE VARIABLE cmb_month1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "From" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "01/01/1997" 
     DROP-DOWN-LIST
     SIZE 14.86 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_month2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "31/12/2075" 
     DROP-DOWN-LIST
     SIZE 14.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_comp1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_comp2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 72 BY 10.25.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.2 COL 3.29 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Profit && Loss", "P":U,
"Balance Sheet", "B":U,
"Both", "*":U
          SIZE 16.72 BY 2.7
          FONT 10
     RP.Char3 AT ROW 1.25 COL 33 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Single Company", "1":U,
"Range of Companies", "R":U,
"All Companies", "A":U
          SIZE 17.72 BY 2.7
          FONT 10
     RP.Int1 AT ROW 4.55 COL 9.86 COLON-ALIGNED
          LABEL "Company" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_comp1 AT ROW 4.55 COL 19.57 COLON-ALIGNED NO-LABEL
     RP.Int2 AT ROW 5.75 COL 9.86 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_comp2 AT ROW 5.75 COL 19.57 COLON-ALIGNED NO-LABEL
     RP.Log3 AT ROW 7.65 COL 3.43 HELP
          ""
          LABEL "Exclude closing Year End transaction"
          VIEW-AS TOGGLE-BOX
          SIZE 28.57 BY .8
     RP.Log2 AT ROW 8.65 COL 3.43
          LABEL "Month range"
          VIEW-AS TOGGLE-BOX
          SIZE 13.14 BY .8
          FONT 10
     cmb_month1 AT ROW 8.55 COL 19.14 COLON-ALIGNED
     cmb_month2 AT ROW 8.55 COL 38 COLON-ALIGNED
     Btn_OK AT ROW 9.8 COL 60.86
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.85
         WIDTH              = 79.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fil_comp1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_comp2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN enable-appropriate-fields.
  RUN set-file-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN enable-appropriate-fields.
  RUN set-file-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month1 V-table-Win
ON U1 OF cmb_month1 IN FRAME F-Main /* From */
DO:
  {inc/selcmb/scmths1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month1 V-table-Win
ON U2 OF cmb_month1 IN FRAME F-Main /* From */
DO:
  {inc/selcmb/scmths2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U1 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U2 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_comp1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U1 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U2 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U3 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_comp2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U1 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U2 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U3 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Company */
DO:
  RUN set-file-name.
  {inc/selcde/cdcmp.i "fil_comp1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* To */
DO:
  RUN set-file-name.
  {inc/selcde/cdcmp.i "fil_comp2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Month range */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose: As its name!
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  CASE INPUT RP.Char3:
    WHEN "1" THEN DO:
      VIEW RP.Int1 fil_comp1 .
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int2 fil_comp2 .
    END.
    WHEN "R" THEN DO:
      VIEW RP.Int1 fil_comp1 RP.Int2 fil_comp2 .
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = No" ).
    END.
    WHEN "A" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int1 fil_comp1 RP.Int2 fil_comp2 .
    END.
  END CASE.

  IF INPUT RP.Log2 THEN
    VIEW cmb_month1.
  ELSE
    HIDE cmb_month1.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/
&SCOP REPORT-ID "property-tb"

  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  FIND RP WHERE RP.ReportID = {&REPORT-ID}
                        AND RP.UserName = user-name
                        NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name .
  END.


  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  RUN enable-appropriate-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program.
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.
DEF VAR regen AS LOGI NO-UNDO.
DEF VAR e1 AS INT NO-UNDO.
DEF VAR e2 AS INT NO-UNDO.

  DO TRANSACTION:
    RUN dispatch IN THIS-PROCEDURE ('update-record':U).
  END.

  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN      e1 = RP.Int1            e2 = RP.Int2    .

    CASE RP.Char3:
      WHEN "1" THEN     e2 = e1.
      WHEN "A" THEN     ASSIGN      e1 = 0     e2 = 99999 .
    END CASE.

    report-options = "~nReportType," + RP.Char1
                   + "~nSelectionType," + RP.Char3
                   + "~nEntityRange," + STRING(e1) + "," + STRING(e2)
                   + "~nEndMonth," + STRING(RP.Int4)
                   + (IF RP.Log2 THEN "~nStartMonth," + STRING(RP.Int3) ELSE "")
                   + (IF RP.Log3 THEN "~nExcludeYearEnd" ELSE "")
                    .
  END.

  {inc/bq-do.i "process/report/html-ledgers.p" "report-options" "No"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-file-name V-table-Win 
PROCEDURE set-file-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR directory AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  CASE INPUT RP.Char3:
    WHEN "A" THEN
      file-name = "All-" + INPUT RP.Char1 .
    WHEN "1" THEN
      file-name = STRING( INPUT RP.Int1 ) + "-" + INPUT RP.Char1 .
    
    OTHERWISE
      file-name = STRING( INPUT RP.Int1 ) + "-"
                + STRING( INPUT RP.Int2 ) + "-"
                + INPUT RP.Char1 .
  END CASE.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

