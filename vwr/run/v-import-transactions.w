&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "Andrew" NO-UNDO.

&GLOB REPORT-ID "Import Transactions"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Char2 RP.Log2 RP.Log3 RP.Int2 ~
RP.Int1 RP.Log4 RP.Log5 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Char1 ~{&FP2}Char1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-23 Btn_Browse cmb_BankAccount Btn_OK 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Char2 RP.Log2 RP.Log3 RP.Int2 ~
RP.Int1 RP.Log4 RP.Log5 
&Scoped-Define DISPLAYED-OBJECTS cmb_Style cmb_BankAccount 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Browse 
     LABEL "Browse" 
     SIZE 8.57 BY 1.05.

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 9.72 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE cmb_BankAccount AS CHARACTER FORMAT "X(256)":U 
     LABEL "Bank Account" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     SIZE 36.57 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Style AS CHARACTER FORMAT "X(256)":U 
     LABEL "Style" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "APMS - Capital APMS Standard" 
     SIZE 40 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-23
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 71.43 BY 10.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.4 COL 9.29 COLON-ALIGNED HELP
          ""
          LABEL "File name" FORMAT "X(220)"
          VIEW-AS FILL-IN 
          SIZE 52 BY 1
     Btn_Browse AT ROW 1.4 COL 63.29
     cmb_Style AT ROW 2.4 COL 9.29 COLON-ALIGNED
     RP.Char2 AT ROW 4 COL 11.29 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Receipts", "IRCT":U,
"Payments", "IPYT":U,
"Journal", "JRNL":U
          SIZE 10.29 BY 2.4
     cmb_BankAccount AT ROW 4 COL 33.43 COLON-ALIGNED
     RP.Log2 AT ROW 5.2 COL 35.43
          LABEL "Banking summary"
          VIEW-AS TOGGLE-BOX
          SIZE 15.43 BY .85
     RP.Log3 AT ROW 6.8 COL 11.29
          LABEL "Overwrite unposted batch no."
          VIEW-AS TOGGLE-BOX
          SIZE 24 BY 1
     RP.Int2 AT ROW 6.8 COL 33.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1
     RP.Int1 AT ROW 8.6 COL 9.29 COLON-ALIGNED
          LABEL "Ignore first"
          VIEW-AS FILL-IN 
          SIZE 4 BY 1.05
     RP.Log4 AT ROW 9.8 COL 11.29
          LABEL "More parameters on first line of file"
          VIEW-AS TOGGLE-BOX
          SIZE 26.29 BY .85
     Btn_OK AT ROW 10.4 COL 62.14
     RP.Log5 AT ROW 10.6 COL 11.29
          LABEL "Line of field names line before transaction data"
          VIEW-AS TOGGLE-BOX
          SIZE 34.86 BY .85
     RECT-23 AT ROW 1 COL 1
     "Batch type:" VIEW-AS TEXT
          SIZE 7.72 BY .65 AT ROW 4 COL 3.29
     "lines of file" VIEW-AS TEXT
          SIZE 8 BY .65 AT ROW 8.8 COL 15.29
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 12.4
         WIDTH              = 83.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR RADIO-SET RP.Char2 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR COMBO-BOX cmb_Style IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       cmb_Style:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Browse V-table-Win
ON CHOOSE OF Btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN choose-filename.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.

  RUN dispatch ( 'exit':U ).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON VALUE-CHANGED OF RP.Char2 IN FRAME F-Main /* Char2 */
DO:
  RUN batch-type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_BankAccount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_BankAccount V-table-Win
ON U1 OF cmb_BankAccount IN FRAME F-Main /* Bank Account */
DO:
  {inc/selcmb/scbnk1.i "RP" "Char3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_BankAccount V-table-Win
ON U2 OF cmb_BankAccount IN FRAME F-Main /* Bank Account */
DO:
  {inc/selcmb/scbnk2.i "RP" "Char3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log3 V-table-Win
ON VALUE-CHANGED OF RP.Log3 IN FRAME F-Main /* Overwrite unposted batch no. */
DO:
  RUN batch-option-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE batch-option-changed V-table-Win 
PROCEDURE batch-option-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Log3 THEN
    VIEW TTPL.RP.Int2 .
  ELSE
    HIDE TTPL.RP.Int2 .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE batch-type-changed V-table-Win 
PROCEDURE batch-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Char2 = "JRNL" THEN
    HIDE cmb_BankAccount RP.Log2 .
  ELSE
    VIEW cmb_BankAccount RP.Log2 .

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-filename V-table-Win 
PROCEDURE choose-filename :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR file-chosen AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR temp-filename AS CHAR NO-UNDO FORMAT "X(200)".
DEF VAR directory-string AS CHAR NO-UNDO.

  temp-filename = INPUT FRAME {&FRAME-NAME} RP.Char1 .
  directory-string = SUBSTRING( temp-filename, 1, R-INDEX( temp-filename, "\") ).
  SYSTEM-DIALOG GET-FILE temp-filename
      FILTERS "Comma separated value files" "*.csv", "All files" "*.*"
      INITIAL-FILTER 1
      DEFAULT-EXTENSION "*.csv"
      INITIAL-DIR directory-string MUST-EXIST
      RETURN-TO-START-DIR
      TITLE "Select Transaction Import File" USE-FILENAME
      UPDATE file-chosen.

  IF file-chosen THEN DO:
    IF NOT( temp-filename MATCHES "*~.csv" ) THEN DO:
      temp-filename = temp-filename + ".csv".
    END.
    RP.Char1:SCREEN-VALUE = temp-filename.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN batch-type-changed.
  RUN batch-option-changed.
  RUN style-changed.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/

  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  FIND RP WHERE RP.ReportID = {&REPORT-ID}
                        AND RP.UserName = user-name
                        NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
    .
  END.

  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program.
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

  RUN dispatch IN THIS-PROCEDURE ('update-record':U).

  report-options = "FileName," + RP.Char1
                 + "~nImportType," + RP.Char2
                 + (IF RP.Log2 THEN "~nBankingSummary" ELSE "")
                 + (IF RP.Log3 THEN "~nOverWrite," + STRING(RP.Int2) ELSE "")
                 + (IF RP.Log4 THEN "~nLineOneParameters" ELSE "")
                 + (IF RP.Log5 THEN "~nFieldsLine" ELSE "")
                 + (IF LENGTH(RP.Char3) > 0 THEN "~nBankAccountCode," + RP.Char3 ELSE "")
                 + "~nIgnoreLines," + STRING(RP.Int1)
                  .

  RUN notify( 'set-busy,container-source':U ).
  RUN process/import/transact.p ( report-options ).
  RUN notify( 'set-idle,container-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE style-changed V-table-Win 
PROCEDURE style-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


