&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "Andrew" NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Log5 RP.Char1 RP.Char3 RP.Char4 RP.Int1 ~
RP.Char5 RP.Int2 RP.Log4 RP.Log1 RP.Log3 RP.Log2 RP.Char2 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_month1 cmb_month2 btn-select-file Btn_OK ~
RECT-1 
&Scoped-Define DISPLAYED-FIELDS RP.Log5 RP.Char1 RP.Char3 RP.Char4 RP.Int1 ~
RP.Char5 RP.Int2 RP.Log4 RP.Log1 RP.Log3 RP.Log2 RP.Char2 
&Scoped-Define DISPLAYED-OBJECTS fil_prop1 fil_prop2 cmb_month1 cmb_month2 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn-select-file 
     LABEL "Browse" 
     SIZE 7.43 BY 1
     FONT 9.

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "&OK" 
     SIZE 11.57 BY 1.2
     BGCOLOR 8 FONT 9.

DEFINE VARIABLE cmb_month1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "From" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "01/01/1997" 
     DROP-DOWN-LIST
     SIZE 14.86 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_month2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "31/12/2075" 
     DROP-DOWN-LIST
     SIZE 14.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_prop1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_prop2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 72 BY 14.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Log5 AT ROW 12.6 COL 7.86
          LABEL "Lines for every active account"
          VIEW-AS TOGGLE-BOX
          SIZE 25.72 BY .85
     RP.Char1 AT ROW 1.2 COL 3.29 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Detail then Summary", "DS":U,
"Detail (Accounts)", "D":U,
"Summary (Account Groups)", "S":U,
"Tabulated - by Group", "C":U,
"Tabulated - by Account", "E":U
          SIZE 22.29 BY 4.5
          FONT 10
     RP.Char3 AT ROW 1.2 COL 29.57 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Single Property", "1":U,
"Property list", "E":U,
"Range of Properties", "R":U,
"All Properties", "A":U
          SIZE 17.72 BY 3.6
          FONT 10
     RP.Char4 AT ROW 1.2 COL 50.72 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Preview report", "V":U,
"Print report", "P":U,
"Build Spreadsheet", "S":U
          SIZE 18.86 BY 2.7
          FONT 10
     RP.Int1 AT ROW 6.2 COL 9.86 COLON-ALIGNED
          LABEL "Property" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_prop1 AT ROW 6.2 COL 19.57 COLON-ALIGNED NO-LABEL
     RP.Char5 AT ROW 6.8 COL 10 COLON-ALIGNED HELP
          ""
          LABEL "List" FORMAT "X(60)"
          VIEW-AS COMBO-BOX INNER-LINES 10
          LIST-ITEMS "TTP" 
          DROP-DOWN-LIST
          SIZE 60.57 BY 1
     RP.Int2 AT ROW 7.4 COL 9.86 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_prop2 AT ROW 7.4 COL 19.57 COLON-ALIGNED NO-LABEL
     RP.Log4 AT ROW 8.6 COL 7.86
          LABEL "Consolidate into one 'Property'"
          VIEW-AS TOGGLE-BOX
          SIZE 24.57 BY .8
     RP.Log1 AT ROW 9.6 COL 7.86 HELP
          ""
          LABEL "Monthly Management reporting columns"
          VIEW-AS TOGGLE-BOX
          SIZE 29.72 BY .8
          FONT 10
     RP.Log3 AT ROW 10.6 COL 7.86 HELP
          ""
          LABEL "Exclude closing Year End transaction"
          VIEW-AS TOGGLE-BOX
          SIZE 28.57 BY .8
     RP.Log2 AT ROW 11.6 COL 7.86
          LABEL "Month range"
          VIEW-AS TOGGLE-BOX
          SIZE 13.14 BY .8
          FONT 10
     cmb_month1 AT ROW 11.5 COL 23.57 COLON-ALIGNED
     cmb_month2 AT ROW 11.5 COL 42.43 COLON-ALIGNED
     RP.Char2 AT ROW 14.4 COL 5.86 COLON-ALIGNED
          LABEL "Directory" FORMAT "X(120)"
          VIEW-AS FILL-IN 
          SIZE 44.57 BY 1
     btn-select-file AT ROW 14.4 COL 52.86
     Btn_OK AT ROW 14.2 COL 60.86
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 17.35
         WIDTH              = 87.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR COMBO-BOX RP.Char5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_prop1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_prop2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn-select-file
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-select-file V-table-Win
ON CHOOSE OF btn-select-file IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN enable-appropriate-fields.
  RUN set-file-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN enable-appropriate-fields.
  RUN set-file-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char4 V-table-Win
ON VALUE-CHANGED OF RP.Char4 IN FRAME F-Main /* Char4 */
DO:
  RUN enable-appropriate-fields.
  RUN set-file-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char5 V-table-Win
ON VALUE-CHANGED OF RP.Char5 IN FRAME F-Main /* List */
DO:
  RUN set-file-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month1 V-table-Win
ON U1 OF cmb_month1 IN FRAME F-Main /* From */
DO:
  {inc/selcmb/scmths1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month1 V-table-Win
ON U2 OF cmb_month1 IN FRAME F-Main /* From */
DO:
  {inc/selcmb/scmths2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U1 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U2 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_prop1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U1 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U2 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U3 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_prop2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U1 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U2 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U3 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Property */
DO:
  RUN set-file-name.
  {inc/selcde/cdpro.i "fil_prop1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* To */
DO:
  RUN set-file-name.
  {inc/selcde/cdpro.i "fil_prop2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Month range */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose: As its name!
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF INPUT RP.Char1 = "D" OR INPUT RP.Char1 = "S" OR INPUT RP.Char1 = "DS" THEN DO:
    VIEW RP.Log4.
  END.
  ELSE DO:
    HIDE RP.Log4.
  END.

  CASE INPUT RP.Char3:
    WHEN "1" THEN DO:
      VIEW RP.Int1 fil_prop1 .
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int2 fil_prop2 RP.Char5 .
    END.
    WHEN "R" THEN DO:
      VIEW RP.Int1 fil_prop1 RP.Int2 fil_prop2 .
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = No" ).
      HIDE RP.Char5 .
    END.
    WHEN "E" THEN DO:
      HIDE RP.Int1 fil_prop1 RP.Int2 fil_prop2 .
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
      VIEW RP.Char5 .
    END.
    WHEN "A" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int1 fil_prop1 RP.Int2 fil_prop2 RP.Char5 .
    END.
  END CASE.

  IF INPUT RP.Char4 <> "S" THEN DO:
    HIDE btn-select-file RP.Char2 .
    IF INPUT RP.Char1 = "C" OR INPUT RP.Char1 = "E" THEN
      HIDE RP.Log1 .
    ELSE
      VIEW RP.Log1.
  END.
  ELSE DO:
    VIEW btn-select-file RP.Char2 .
    HIDE RP.Log1.
  END.

  IF INPUT RP.Char4 = "S" THEN
    VIEW RP.Log5.
  ELSE
    HIDE RP.Log5.

  IF INPUT RP.Log2 THEN
    VIEW cmb_month1.
  ELSE
    HIDE cmb_month1.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/
&SCOP REPORT-ID "property-tb"

  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  FIND RP WHERE RP.ReportID = {&REPORT-ID}
                        AND RP.UserName = user-name
                        NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name .
  END.

DEF VAR con-list    AS CHAR INITIAL "" NO-UNDO.
DEF VAR delim AS CHAR INITIAL "|" NO-UNDO.

  /* Initialise the entity list for the combos */
  FOR EACH EntityList NO-LOCK:
    con-list = con-list + delim + STRING( EntityList.ListCode, "X(7)") + "- " + EntityList.Description.
  END.
  con-list = SUBSTRING( con-list, 2).   /* trim initial delimiter */
  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN
      RP.Char5:DELIMITER = delim
      RP.Char5:LIST-ITEMS = con-list
    .
  END.


  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  RUN enable-appropriate-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program.
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.
DEF VAR regen AS LOGI NO-UNDO.
DEF VAR e1 AS INT NO-UNDO.
DEF VAR e2 AS INT NO-UNDO.

  DO TRANSACTION:
    RUN dispatch IN THIS-PROCEDURE ('update-record':U).
  END.

  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN      e1 = RP.Int1            e2 = RP.Int2    .

    CASE RP.Char3:
      WHEN "1" THEN     e2 = e1.
      WHEN "A" THEN     ASSIGN      e1 = 0     e2 = 99999 .
      WHEN "E" THEN     ASSIGN      e1 = 0     e2 = 0 .
    END CASE.

    report-options = "~nReportType," + RP.Char1
                   + "~nSelectionType," + RP.Char3
                   + "~nOutputType," + RP.Char4
                   + (IF RP.Char3 = "E":U THEN "~nEntityList," + TRIM( SUBSTRING( RP.Char5, 1, INDEX(RP.Char5, " -"))) ELSE "")
                   + "~nEntityRange," + STRING(e1) + "," + STRING(e2)
                   + "~nEndMonth," + STRING(RP.Int4)
                   + (IF RP.Char4 = "S" THEN "~nFileName," + RP.Char2 ELSE "")
                   + "~nColumnStyle," + (IF RP.Log1:VISIBLE AND RP.Log1 THEN "Mgmt" ELSE "Std")
                   + (IF RP.Log2 THEN "~nStartMonth," + STRING(RP.Int3) ELSE "")
                   + (IF RP.Log3 THEN "~nExcludeYearEnd" ELSE "")
                   + (IF RP.Char4 = "S" AND RP.Log5 THEN "~nPedantic" ELSE "")
                   + (IF (RP.Char1 = "D":U OR RP.Char1 = "S":U OR RP.Char1 = "DS":U) AND RP.Log4 THEN "~nConsolidated" ELSE "")
                    .
  END.

  {inc/bq-do.i "process/report/property-tb.p" "report-options" "RP.Char4 = 'P'"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char2 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated fields" "*.CSV"
        SAVE-AS ASK-OVERWRITE CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV"
        INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME  UPDATE select-ok.

  IF select-ok THEN DO:
    IF SUBSTRING( file-name, R-INDEX( file-name, ".") ) <> ".CSV" THEN DO:
      IF (LENGTH( file-name ) - R-INDEX( file-name, "." )) < 4 THEN
        file-name = SUBSTRING( file-name, 1, R-INDEX( file-name, ".")) + "CSV" .
    END.
    RP.Char2:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-file-name V-table-Win 
PROCEDURE set-file-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR directory AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  directory = TRIM( SUBSTRING( INPUT RP.Char2, 1, R-INDEX(INPUT RP.Char2, "\" ))).
  IF directory = "" THEN
    directory = RIGHT-TRIM( REPLACE( SESSION:TEMP-DIRECTORY, "/", "\"), "\") + "\".

  CASE INPUT RP.Char3:
    WHEN "L" THEN
      file-name = TRIM( SUBSTRING( INPUT RP.Char5, 1, 4)) + "-"
                + INPUT RP.Char1 .
    WHEN "A" THEN
      file-name = "All-" + INPUT RP.Char1 .
    WHEN "1" THEN
      file-name = STRING( INPUT RP.Int1 ) + "-" + INPUT RP.Char1 .
    WHEN "R" THEN
      file-name = STRING( INPUT RP.Int1 ) + "-"
                + STRING( INPUT RP.Int2 ) + "-"
                + INPUT RP.Char1 .
    OTHERWISE
      file-name = STRING( INPUT RP.Int1 ) + "-"
                + STRING( INPUT RP.Int2 ) + "-"
                + INPUT RP.Char1 .
  END CASE.
  file-name = directory + file-name + ".CSV".

  IF INPUT RP.Char2 <> file-name THEN DO:
    RP.Char2:SCREEN-VALUE = file-name.
    /* DISPLAY RP.Char2 . */
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


