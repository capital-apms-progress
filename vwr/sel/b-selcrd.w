&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR show-inactive AS LOGICAL NO-UNDO.
DEF VAR filter-value AS CHAR NO-UNDO.

DEF VAR gap-1 AS CHAR FORMAT "X" LABEL "" NO-UNDO.
DEF VAR gap-2 AS CHAR FORMAT "X" LABEL "" NO-UNDO.
DEF VAR gap-3 AS CHAR FORMAT "X" LABEL "" NO-UNDO.

DEF VAR u-vouchers AS LOGICAL FORMAT "Yes/---" NO-UNDO.
DEF VAR acct-bal   AS DECIMAL FORMAT "->>,>>>,>>9.99" COLUMN-LABEL "Balance" NO-UNDO.
DEF VAR city       AS CHAR COLUMN-LABEL "City" FORMAT "X(30)" NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-acct.i "CREDITORS" "sundry-creditors"}

ON FIND OF Creditor DO:
  u-vouchers = CAN-FIND( FIRST Voucher WHERE Voucher.CreditorCode = Creditor.CreditorCode AND Voucher.VoucherStatus = 'U').
  FIND AccountSummary WHERE AccountSummary.EntityType = "C"
                AND AccountSummary.EntityCode = Creditor.CreditorCode
                AND AccountSummary.AccountCode = sundry-creditors
                NO-LOCK NO-ERROR.
  IF AVAILABLE(AccountSummary) THEN
    acct-bal = AccountSummary.Balance.
  ELSE
    acct-bal = 0.

  FIND PostalDetail WHERE PostalDetail.PersonCode = Creditor.PaymentContact
                AND PostalDetail.PostalType = 'PYMT'
                AND PostalDetail.City <> ?
                NO-LOCK NO-ERROR.
  IF AVAILABLE(PostalDetail) THEN
    city = PostalDetail.City.
  ELSE
    city = "".

END.

{inc/topic/tpcrdtor.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Creditor

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Creditor.CreditorCode ~
Creditor.Active Creditor.Name city @ city acct-bal @ acct-bal ~
u-vouchers @ u-vouchers 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table Creditor.CreditorCode 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}CreditorCode ~{&FP2}CreditorCode ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table Creditor
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table Creditor
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH Creditor WHERE ~{&KEY-PHRASE} ~
      AND (show-inactive OR Creditor.Active) ~
 AND Creditor.Name MATCHES filter-value NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table Creditor
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Creditor


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
CompanyCode||y|ttpl.Creditor.CompanyCode
PaymentContact||y|TTPL.Creditor.PaymentContact
CreditorCode||y|ttpl.Creditor.CreditorCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = "CompanyCode,PaymentContact,CreditorCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Code|||ttpl.Creditor.CreditorCode|yes
Name|y||ttpl.Creditor.Name|yes
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Code,Name",
     Sort-Case = Name':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Creditor SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      Creditor.CreditorCode
      Creditor.Active COLUMN-LABEL "Act." FORMAT "Yes/---"
      Creditor.Name FORMAT "X(60)"
      city @ city COLUMN-LABEL "City" FORMAT "X(25)"
      acct-bal @ acct-bal COLUMN-LABEL "Balance" FORMAT "->>,>>>,>>9.99"
      u-vouchers @ u-vouchers COLUMN-LABEL "O/s Vch" FORMAT "Yes/---"
  ENABLE
      Creditor.CreditorCode
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 92 BY 15
         BGCOLOR 15 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 17.15
         WIDTH              = 94.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main = 2
       br_table:MAX-DATA-GUESS IN FRAME F-Main     = 500.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.Creditor"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _TblOptList       = ", FIRST OUTER, FIRST OUTER"
     _Where[1]         = "(show-inactive OR Creditor.Active)
 AND Creditor.Name MATCHES filter-value"
     _FldNameList[1]   > ttpl.Creditor.CreditorCode
"CreditorCode" ? ? "integer" ? ? ? ? ? ? yes ?
     _FldNameList[2]   > ttpl.Creditor.Active
"Active" "Act." "Yes/---" "logical" ? ? ? ? ? ? no ?
     _FldNameList[3]   > ttpl.Creditor.Name
"Name" ? "X(60)" "character" ? ? ? ? ? ? no ?
     _FldNameList[4]   > "_<CALC>"
"city @ city" "City" "X(25)" ? ? ? ? ? ? ? no ?
     _FldNameList[5]   > "_<CALC>"
"acct-bal @ acct-bal" "Balance" "->>,>>>,>>9.99" ? ? ? ? ? ? ? no ?
     _FldNameList[6]   > "_<CALC>"
"u-vouchers @ u-vouchers" "O/s Vch" "Yes/---" ? ? ? ? ? ? ? no ?
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-selvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  Creditor.CreditorCode:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list( 'SortBy-Options = Name|Code, SortBy-Case = Name':U ).
RUN set-attribute-list( 'FilterBy-Options = Active|All, FilterBy-Case = Active':U ).
RUN set-attribute-list( 'Filter-Value = ':U ).
RUN set-attribute-list( 'Filter2-Value = ':U ).
RUN set-attribute-list( 'SearchBy-Case = ' ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

  RUN get-attribute ('SortBy-Case':U).
  CASE RETURN-VALUE:
    WHEN 'Code':U THEN DO:
      &Scope SORTBY-PHRASE BY Creditor.CreditorCode
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END.
    WHEN 'Name':U THEN DO:
      &Scope SORTBY-PHRASE BY Creditor.Name
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END.
    OTHERWISE DO:
      &Undefine SORTBY-PHRASE
      {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record B-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR emsg AS CHAR NO-UNDO INITIAL ?.

  IF NOT AVAILABLE(Creditor) THEN RETURN.

  /* Code placed here will execute PRIOR to standard behavior. */
  FIND FIRST AcctTran WHERE AcctTran.EntityType = "C"
                        AND AcctTran.EntityCode = Creditor.CreditorCode
                        NO-LOCK NO-ERROR.
  IF AVAILABLE(AcctTran) THEN emsg = "Posted transactions exist".
  ELSE DO:
    FIND FIRST NewAcctTrans WHERE NewAcctTrans.EntityType = "C"
                          AND NewAcctTrans.EntityCode = Creditor.CreditorCode
                          NO-LOCK NO-ERROR.
    IF AVAILABLE(NewAcctTrans) THEN emsg = "Unposted transactions exist, batch " + STRING( NewAcctTrans.BatchCode ).
    ELSE DO:
      FIND FIRST NewAcctTrans WHERE NewAcctTrans.EntityType = "C"
                            AND NewAcctTrans.EntityCode = Creditor.CreditorCode
                            NO-LOCK NO-ERROR.
      IF AVAILABLE(NewAcctTrans) THEN emsg = "Unposted transactions exist, batch " + STRING( NewAcctTrans.BatchCode ).
    END.
  END.
  IF emsg <> ? THEN DO:
    MESSAGE emsg SKIP(1) VIEW-AS ALERT-BOX ERROR TITLE "Cannot Delete Creditor".
    RETURN.
  END.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "CompanyCode" "Creditor" "CompanyCode"}
  {src/adm/template/sndkycas.i "PaymentContact" "Creditor" "PaymentContact"}
  {src/adm/template/sndkycas.i "CreditorCode" "Creditor" "CreditorCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Creditor"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Filter-Value B-table-Win 
PROCEDURE use-Filter-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-val AS CHAR NO-UNDO.
  RUN get-attribute( 'Filter2-Value':U ).
  filter-value = new-val + RETURN-VALUE + '*'.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Filter2-Value B-table-Win 
PROCEDURE use-Filter2-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-val AS CHAR NO-UNDO.
  RUN get-attribute( 'Filter-Value':U ).
  filter-value = RETURN-VALUE + new-val + '*'.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-FilterBy-Case B-table-Win 
PROCEDURE use-FilterBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-val AS CHAR NO-UNDO.
  show-inactive = (new-val = "All").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-SearchBy-Case B-table-Win 
PROCEDURE use-SearchBy-Case :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-filter AS CHAR NO-UNDO.

  filter-value = new-filter + "*".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


