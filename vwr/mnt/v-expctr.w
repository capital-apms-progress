&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description: 
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

&SCOPED-DEFINE REPORT-ID "Creditor Contact Export"

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}

DEF VAR manager-list AS CHAR NO-UNDO.
DEF VAR manager-pd   AS CHAR NO-UNDO.
DEF VAR region-list  AS CHAR NO-UNDO.
DEF VAR region-pd    AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Log1 RP.Date1 RP.Log2 RP.Date2 RP.Log3 ~
RP.Log5 RP.Log4 RP.Char1 RP.Char6 RP.Log6 RP.Log7 RP.Log8 RP.Char5 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Date1 ~{&FP2}Date1 ~{&FP3}~
 ~{&FP1}Date2 ~{&FP2}Date2 ~{&FP3}~
 ~{&FP1}Char5 ~{&FP2}Char5 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-28 cmb_PostalType-1 cmb_PostalType-2 ~
cmb_PostalType-3 cmb_pselect btn_browse btn_export btn_cancel 
&Scoped-Define DISPLAYED-FIELDS RP.Log1 RP.Date1 RP.Log2 RP.Date2 RP.Log3 ~
RP.Log5 RP.Log4 RP.Char1 RP.Char6 RP.Log6 RP.Log7 RP.Log8 RP.Char5 
&Scoped-Define DISPLAYED-OBJECTS cmb_PostalType-1 cmb_PostalType-2 ~
cmb_PostalType-3 cmb_pselect 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_browse 
     LABEL "&Browse" 
     SIZE 6.57 BY 1.05
     FONT 9.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_export DEFAULT 
     LABEL "&Export" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE VARIABLE cmb_PostalType-1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "1" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "","" 
     SIZE 30.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_PostalType-2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "2" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "","" 
     SIZE 30.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_PostalType-3 AS CHARACTER FORMAT "X(256)":U 
     LABEL "3" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "","" 
     SIZE 30.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_pselect AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "","" 
     SIZE 30 BY 1.05 NO-UNDO.

DEFINE RECTANGLE RECT-28
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 73 BY 12.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Log1 AT ROW 2.5 COL 4 HELP
          ""
          LABEL "Vouchers entered after"
          VIEW-AS TOGGLE-BOX
          SIZE 19.29 BY 1
          FONT 10
     RP.Date1 AT ROW 2.5 COL 21.72 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 14 BY 1
          FONT 10
     cmb_PostalType-1 AT ROW 2.5 COL 41 COLON-ALIGNED
     RP.Log2 AT ROW 3.5 COL 4 HELP
          ""
          LABEL "Cheques entered after"
          VIEW-AS TOGGLE-BOX
          SIZE 19 BY 1
          FONT 10
     RP.Date2 AT ROW 3.5 COL 21.72 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99/99/9999"
          VIEW-AS FILL-IN 
          SIZE 14 BY 1
          FONT 10
     cmb_PostalType-2 AT ROW 3.7 COL 41 COLON-ALIGNED
     cmb_PostalType-3 AT ROW 4.9 COL 41 COLON-ALIGNED
     RP.Log3 AT ROW 5 COL 4
          LABEL "Active Contractors Only"
          VIEW-AS TOGGLE-BOX
          SIZE 20 BY 1
     RP.Log5 AT ROW 6.2 COL 57
          LABEL "All"
          VIEW-AS TOGGLE-BOX
          SIZE 5 BY 1
     RP.Log4 AT ROW 6.5 COL 16
          LABEL "All"
          VIEW-AS TOGGLE-BOX
          SIZE 5 BY 1
     RP.Char1 AT ROW 7.1 COL 42.72 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Manager", "Manager":U,
"Region", "Region":U
          SIZE 10 BY 1.6
     RP.Char6 AT ROW 7.5 COL 2 NO-LABEL
          VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
          SIZE 37 BY 5
     cmb_pselect AT ROW 8.7 COL 40.72 COLON-ALIGNED NO-LABEL
     RP.Log6 AT ROW 10 COL 42.72
          LABEL "Include 'No Mailout' contacts"
          VIEW-AS TOGGLE-BOX
          SIZE 24.29 BY .8
     RP.Log7 AT ROW 10.8 COL 42.72
          LABEL "Only export any individual once"
          VIEW-AS TOGGLE-BOX
          SIZE 30.29 BY .85 TOOLTIP "Only one instance for firstname, lastname and company"
     RP.Log8 AT ROW 11.6 COL 42.72
          LABEL "Clean addresses of company names"
          VIEW-AS TOGGLE-BOX
          SIZE 28 BY .85
     RP.Char5 AT ROW 13.2 COL 7 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 34.72 BY 1
          FONT 10
     btn_browse AT ROW 13.2 COL 45
     btn_export AT ROW 13.2 COL 53
     btn_cancel AT ROW 13.2 COL 63.86
     "Export To:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 13.2 COL 1
          FONT 10
     "Property Selection:" VIEW-AS TEXT
          SIZE 14 BY 1 AT ROW 6.2 COL 41
     "Service Types:" VIEW-AS TEXT
          SIZE 11 BY 1 AT ROW 6.5 COL 2
     "Address Selection Priority" VIEW-AS TEXT
          SIZE 26.29 BY 1 AT ROW 1.5 COL 41
     "Include Contractors with:" VIEW-AS TEXT
          SIZE 17 BY 1 AT ROW 1.5 COL 2
     RECT-28 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_export.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 21.35
         WIDTH              = 75.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Date1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RP.Date2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log6 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log7 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log8 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_browse V-table-Win
ON CHOOSE OF btn_browse IN FRAME F-Main /* Browse */
DO:  
  RUN browse-file.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_export
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_export V-table-Win
ON CHOOSE OF btn_export IN FRAME F-Main /* Export */
DO:
  DEF VAR export-ok AS LOGI NO-UNDO.
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN export-contacts( OUTPUT export-ok ).
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  IF export-ok THEN RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN property-selection-changed.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PostalType-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-1 V-table-Win
ON U1 OF cmb_PostalType-1 IN FRAME F-Main /* 1 */
DO:
  {inc/selcmb/scpdt1.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-1 V-table-Win
ON U2 OF cmb_PostalType-1 IN FRAME F-Main /* 1 */
DO:
  {inc/selcmb/scpdt2.i "RP" "Char2"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PostalType-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-2 V-table-Win
ON U1 OF cmb_PostalType-2 IN FRAME F-Main /* 2 */
DO:
  {inc/selcmb/scpdt1.i "RP" "Char3"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-2 V-table-Win
ON U2 OF cmb_PostalType-2 IN FRAME F-Main /* 2 */
DO:
  {inc/selcmb/scpdt2.i "RP" "Char3"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PostalType-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-3 V-table-Win
ON U1 OF cmb_PostalType-3 IN FRAME F-Main /* 3 */
DO:
  {inc/selcmb/scpdt1.i "RP" "Char4"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-3 V-table-Win
ON U2 OF cmb_PostalType-3 IN FRAME F-Main /* 3 */
DO:
  {inc/selcmb/scpdt2.i "RP" "Char4"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log1 V-table-Win
ON VALUE-CHANGED OF RP.Log1 IN FRAME F-Main /* Vouchers entered after */
DO:
  RUN voucher-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Cheques entered after */
DO:
  RUN cheque-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* All */
DO:
  RUN service-all-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log5 V-table-Win
ON VALUE-CHANGED OF RP.Log5 IN FRAME F-Main /* All */
DO:
  RUN property-all-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE browse-file V-table-Win 
PROCEDURE browse-file :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR save-as     AS CHAR NO-UNDO.
  DEF VAR file-chosen AS LOGI NO-UNDO.

  save-as = INPUT FRAME {&FRAME-NAME} RP.Char5.
  IF LENGTH( save-as ) > 0 THEN
  IF LOOKUP( SUBSTR( save-as, LENGTH( save-as ), 1 ), "\,/" ) <> 0 THEN save-as = "".
    
  SYSTEM-DIALOG GET-FILE save-as
    TITLE   "Export Contacts to file"
    FILTERS "Text Files (*.txt *.csv)" "*.txt,*.csv"
    DEFAULT-EXTENSION ".csv"
    SAVE-AS
    RETURN-TO-START-DIR
    USE-FILENAME
    UPDATE file-chosen.

  IF file-chosen THEN RP.Char5:SCREEN-VALUE IN FRAME {&FRAME-NAME} = 
    save-as.
  IF NOT file-chosen THEN RETURN "FAIL".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cheque-changed V-table-Win 
PROCEDURE cheque-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RP.Date2:HIDDEN    = NOT INPUT RP.Log2.
  RP.Date2:SENSITIVE = INPUT RP.Log2.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE export-contacts V-table-Win 
PROCEDURE export-contacts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF OUTPUT PARAMETER export-status AS LOGI NO-UNDO INIT Yes.
  DEF VAR report-options AS CHAR NO-UNDO.
  DEF VAR address-list   AS CHAR NO-UNDO.

  RUN verify-export.
  IF RETURN-VALUE = "FAIL" THEN
  DO:
    export-status = No.
    RETURN.
  END.
   
  RUN dispatch( 'update-record':U ).

  report-options = "TYPE,CONTRACTOR". /* the name of the file we are using record keys for */
  IF RP.Log1 AND RP.Date1 <> ? THEN report-options =
    report-options + "~n" + "VouchersAfter," + STRING( RP.Date1, "99/99/9999" ).
  IF RP.Log2 AND RP.Date2 <> ? THEN report-options =
    report-options + "~n" + "ChequesAfter," + STRING( RP.Date2, "99/99/9999" ).
  
  address-list = "Address," + RP.Char2 + "," + RP.Char3 + "," + RP.Char4.
  report-options = report-options + "~n" + address-list.
  report-options = report-options + "~n" + "FileName," + RP.Char5.

  IF NOT RP.Log4 THEN
  DO:
    DEF VAR service-types AS CHAR NO-UNDO.
    DEF VAR i AS INT NO-UNDO.
    
    DO i = 1 TO NUM-ENTRIES( RP.Char6 ):
      service-types = service-types + "," + TRIM( SUBSTR( ENTRY( i, RP.Char6 ), 1, 4 ) ).
      service-types = TRIM( service-types, "," ).
    END.
  END.
  
  report-options = report-options + "~n" + "ServiceType," +
    ( IF RP.Log4 THEN "ALL" ELSE service-types ).

  report-options = report-options + "~n" + "Property," +
    ( IF RP.Log5 THEN "ALL" ELSE 
      RP.Char1 + "," +  ENTRY( LOOKUP( INPUT cmb_pselect, cmb_pselect:LIST-ITEMS ), cmb_pselect:PRIVATE-DATA )
    ).
    
  IF RP.Log3 THEN report-options = report-options + "~n" + "ActiveOnly,Yes"
                 + (IF RP.Log6 THEN "~nIncludeNoMailout" ELSE "")
                 + (IF RP.Log7 THEN "~nMakeUnique" ELSE "")
                 + (IF RP.Log8 THEN "~nCleanAddresses" ELSE "").

  RUN process/export/ctractor.p( report-options ).
  
  MESSAGE "Export Complete" VIEW-AS ALERT-BOX INFORMATION TITLE "Done".

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-managers V-table-Win 
PROCEDURE get-managers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF manager-list <> "" THEN RETURN.
  
  FOR EACH Property NO-LOCK
    BREAK BY Property.Administrator:
    IF FIRST-OF( Property.Administrator ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Administrator
        NO-LOCK NO-ERROR.

      manager-list = manager-list + IF manager-list = "" THEN "" ELSE ",".
      manager-list = manager-list +
        ( IF AVAILABLE Person THEN Person.FirstName + ' ' + Person.LastName ELSE "Not Managed" ).

      manager-pd = manager-pd + IF manager-pd = "" THEN "" ELSE ",".
      manager-pd = manager-pd + IF AVAILABLE Person THEN STRING( Person.PersonCode ) ELSE STRING( Property.Administrator ).
    END.
  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-regions V-table-Win 
PROCEDURE get-regions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF region-list <> "" THEN RETURN.
  
  FOR EACH Region NO-LOCK:

    region-list = region-list + IF region-list = "" THEN "" ELSE ",".
    region-list = region-list + Region.Name.

    region-pd = region-pd + IF region-pd = "" THEN "" ELSE ",".
    region-pd = region-pd + Region.Region.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-service-types V-table-Win 
PROCEDURE get-service-types :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF VAR item AS CHAR NO-UNDO.
  FOR EACH ServiceType NO-LOCK:
    item = STRING( ServiceType.ServiceType, "X(4)" ) + " - " + ServiceType.Description.
    IF RP.Char6:ADD-LAST( item ) THEN.
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN voucher-changed.
  RUN cheque-changed.
  RUN property-selection-changed.
  RUN service-all-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN get-service-types.
  
  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.

  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
  
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Char2    = "MAIN"
      RP.Char3    = "POST"
      RP.Char3    = "COUR"
      RP.Log3     = Yes
      .
  END.

  RUN dispatch( 'row-changed':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE property-all-changed V-table-Win 
PROCEDURE property-all-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RP.Char1:HIDDEN = INPUT RP.Log5.
  RP.Char1:SENSITIVE = NOT INPUT RP.Log5.
  cmb_pselect:HIDDEN = INPUT RP.Log5.
  cmb_pselect:SENSITIVE = NOT INPUT RP.Log5.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE property-selection-changed V-table-Win 
PROCEDURE property-selection-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT RP.Char1.
  
  CASE type:
    WHEN "MANAGER" THEN
    DO:
      RUN get-managers.
      cmb_pselect:LIST-ITEMS   = manager-list.
      cmb_pselect:PRIVATE-DATA = manager-pd.
      cmb_pselect:SCREEN-VALUE = ENTRY( 1, manager-list ).
    END.
    WHEN "REGION" THEN
    DO:
      RUN get-regions.
      cmb_pselect:LIST-ITEMS   = region-list.
      cmb_pselect:PRIVATE-DATA = region-pd.
      cmb_pselect:SCREEN-VALUE = ENTRY( 1, region-list ).
    END.
  
  END CASE.

  RUN property-all-changed.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE service-all-changed V-table-Win 
PROCEDURE service-all-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RP.Char6:HIDDEN    = INPUT RP.Log4.
  RP.Char6:SENSITIVE = NOT INPUT RP.Log4.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-export V-table-Win 
PROCEDURE verify-export :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF INPUT RP.Char5 = "" OR
     INPUT RP.Char5 = ? THEN
  DO:
    RUN browse-file.
    RETURN RETURN-VALUE.
  END.

  IF NOT INPUT RP.Log4 AND
    ( INPUT RP.Char6 = "" OR INPUT RP.Char6 = ? ) THEN
  DO:
    MESSAGE "You must select at leats one service type!"
      VIEW-AS ALERT-BOX ERROR TITLE "No service types selected".
    RETURN "FAIL".
  END.
   
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE voucher-changed V-table-Win 
PROCEDURE voucher-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RP.Date1:HIDDEN = NOT INPUT RP.Log1.
  RP.Date1:SENSITIVE = INPUT RP.Log1.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


