&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{inc/topic/tpivoice.i}

DEF VAR add-new-line  AS LOGI INIT No NO-UNDO.

DEF VAR line-modified  AS LOGI NO-UNDO.
DEF VAR last-line      AS LOGI NO-UNDO.
DEF VAR last-entity    LIKE Invoice.EntityCode NO-UNDO.
DEF VAR verifying-line AS LOGI INIT No NO-UNDO.
DEF VAR coa-list       AS HANDLE NO-UNDO.
DEF VAR mode           AS CHAR NO-UNDO.
DEF VAR invoice-list   AS CHAR NO-UNDO.
DEF VAR new-record     AS LOGI NO-UNDO.

DEF WORK-TABLE CurrentLine NO-UNDO LIKE InvoiceLine.

{inc/ofc-this.i}
{inc/ofc-set.i "Invoice-Account-Groups" "confirm-type"}
IF NOT AVAILABLE(OfficeSetting) THEN
  /* backward compatibility for AGP/TTP */
  confirm-type = (IF Office.OfficeCode = "SDNY" THEN "OPINC" ELSE "PROPEX").

{inc/ofc-set.i "Invoice-Approval-Forms" "approval-forms"}
IF SUBSTRING(approval-forms,1,1) <> "N" THEN approval-forms = "Yes".

{inc/ofc-set.i "Invoice-Terms" "default-terms"}
IF default-terms = "" OR default-terms = ? THEN
  default-terms = (IF Office.OfficeCode = "SDNY" THEN "30" ELSE "M").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-invlne

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Invoice
&Scoped-define FIRST-EXTERNAL-TABLE Invoice


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Invoice.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES InvoiceLine

/* Definitions for BROWSE br-invlne                                     */
&Scoped-define FIELDS-IN-QUERY-br-invlne InvoiceLine.EntityType ~
InvoiceLine.EntityCode InvoiceLine.AccountCode InvoiceLine.AccountText ~
InvoiceLine.Amount InvoiceLine.Quantity InvoiceLine.Percent ~
InvoiceLine.YourShare 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-invlne InvoiceLine.EntityType ~
InvoiceLine.EntityCode InvoiceLine.AccountCode InvoiceLine.AccountText ~
InvoiceLine.Amount InvoiceLine.Quantity InvoiceLine.Percent 
&Scoped-define FIELD-PAIRS-IN-QUERY-br-invlne~
 ~{&FP1}EntityType ~{&FP2}EntityType ~{&FP3}~
 ~{&FP1}EntityCode ~{&FP2}EntityCode ~{&FP3}~
 ~{&FP1}AccountCode ~{&FP2}AccountCode ~{&FP3}~
 ~{&FP1}AccountText ~{&FP2}AccountText ~{&FP3}~
 ~{&FP1}Amount ~{&FP2}Amount ~{&FP3}~
 ~{&FP1}Quantity ~{&FP2}Quantity ~{&FP3}~
 ~{&FP1}Percent ~{&FP2}Percent ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br-invlne InvoiceLine
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br-invlne InvoiceLine
&Scoped-define OPEN-QUERY-br-invlne OPEN QUERY br-invlne FOR EACH InvoiceLine OF Invoice NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br-invlne InvoiceLine
&Scoped-define FIRST-TABLE-IN-QUERY-br-invlne InvoiceLine


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Invoice.InvoiceDate Invoice.EntityCode ~
Invoice.ToDetail Invoice.Blurb Invoice.TaxApplies 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}InvoiceDate ~{&FP2}InvoiceDate ~{&FP3}~
 ~{&FP1}EntityCode ~{&FP2}EntityCode ~{&FP3}~
 ~{&FP1}ToDetail ~{&FP2}ToDetail ~{&FP3}
&Scoped-define ENABLED-TABLES Invoice
&Scoped-define FIRST-ENABLED-TABLE Invoice
&Scoped-Define ENABLED-OBJECTS RECT-12 cmb_Terms btn_add-trn br-invlne ~
btn_add btn_ok btn_cancel 
&Scoped-Define DISPLAYED-FIELDS Invoice.InvoiceDate Invoice.InvoiceNo ~
Invoice.EntityCode Invoice.ToDetail Invoice.Blurb Invoice.Total ~
Invoice.TaxApplies Invoice.TaxAmount 
&Scoped-Define DISPLAYED-OBJECTS fil_Tenant cmb_Terms fil_Account ~
fil_Supply fil_total 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-BROWSE-2 
       MENU-ITEM m_Delete       LABEL "Delete Line"   
       MENU-ITEM m_Add          LABEL "Add Line"      .


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_add-trn 
     LABEL "Add transaction" 
     SIZE 15 BY 1.15.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 10 BY 1
     FONT 9.

DEFINE BUTTON btn_ok 
     LABEL "&OK" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE VARIABLE cmb_Terms AS CHARACTER FORMAT "X(30)":U 
     LABEL "Terms" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "","" 
     SIZE 33.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Account AS CHARACTER FORMAT "X(256)":U 
     LABEL "Account Name" 
     VIEW-AS FILL-IN 
     SIZE 43.43 BY 1
     FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE fil_Supply AS CHARACTER FORMAT "X(256)":U 
     LABEL "Supply Name" 
     VIEW-AS FILL-IN 
     SIZE 43.43 BY 1
     FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE fil_Tenant AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 71.57 BY 1
     FONT 11 NO-UNDO.

DEFINE VARIABLE fil_total AS DECIMAL FORMAT "-Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Amt Due" 
     VIEW-AS FILL-IN 
     SIZE 14.86 BY 1
     FGCOLOR 12 FONT 11 NO-UNDO.

DEFINE RECTANGLE RECT-12
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 90.29 BY 21.2.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-invlne FOR 
      InvoiceLine SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-invlne
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-invlne V-table-Win _STRUCTURED
  QUERY br-invlne NO-LOCK DISPLAY
      InvoiceLine.EntityType
      InvoiceLine.EntityCode COLUMN-LABEL "  Code"
      InvoiceLine.AccountCode COLUMN-LABEL " Account"
      InvoiceLine.AccountText COLUMN-LABEL "Description                                                                               ." FORMAT "X(30)"
      InvoiceLine.Amount COLUMN-LABEL "Unit Price"
      InvoiceLine.Quantity
      InvoiceLine.Percent COLUMN-LABEL "Discount"
      InvoiceLine.YourShare COLUMN-LABEL "Amount"
  ENABLE
      InvoiceLine.EntityType
      InvoiceLine.EntityCode
      InvoiceLine.AccountCode
      InvoiceLine.AccountText
      InvoiceLine.Amount
      InvoiceLine.Quantity
      InvoiceLine.Percent
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 89.14 BY 9.5
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Invoice.InvoiceDate AT ROW 1 COL 8.14 COLON-ALIGNED
          LABEL "Date"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
          FONT 11
     Invoice.InvoiceNo AT ROW 1 COL 77.86 COLON-ALIGNED
          LABEL "No" FORMAT "ZZZZZ9"
          VIEW-AS FILL-IN 
          SIZE 7.57 BY 1
          FONT 11
     Invoice.EntityCode AT ROW 2 COL 8.14 COLON-ALIGNED
          LABEL "Debtor"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
          FONT 11
     fil_Tenant AT ROW 2 COL 17.14 COLON-ALIGNED NO-LABEL
     Invoice.ToDetail AT ROW 3 COL 8.14 COLON-ALIGNED
          LABEL "GL Text"
          VIEW-AS FILL-IN 
          SIZE 80.57 BY 1
          FONT 11
     Invoice.Blurb AT Y 64 X 64 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE-PIXELS 564 BY 80
          FONT 11
     cmb_Terms AT ROW 8.4 COL 8.14 COLON-ALIGNED
     btn_add-trn AT ROW 8.4 COL 62.72
     br-invlne AT ROW 9.8 COL 1.57
     fil_Account AT ROW 19.4 COL 11.57 COLON-ALIGNED
     Invoice.Total AT ROW 19.4 COL 73.86 COLON-ALIGNED FORMAT "-Z,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14.86 BY 1
          FGCOLOR 12 FONT 11
     fil_Supply AT ROW 20.4 COL 11.57 COLON-ALIGNED
     Invoice.TaxApplies AT ROW 20.4 COL 58.14
          VIEW-AS TOGGLE-BOX
          SIZE 10.29 BY 1
          FONT 11
     Invoice.TaxAmount AT ROW 20.4 COL 73.86 COLON-ALIGNED FORMAT "-Z,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14.86 BY 1
          FGCOLOR 12 FONT 11
     fil_total AT ROW 21.4 COL 73.86 COLON-ALIGNED
     btn_add AT ROW 22.8 COL 1
     btn_ok AT ROW 22.8 COL 69
     btn_cancel AT ROW 22.8 COL 80.43
     RECT-12 AT ROW 1.4 COL 1
     "Blurb:" VIEW-AS TEXT
          SIZE 3.86 BY 1.1 AT ROW 4.2 COL 6.14
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Invoice
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 25.25
         WIDTH              = 99.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-invlne btn_add-trn F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       Invoice.Blurb:RETURN-INSERTED IN FRAME F-Main  = TRUE.

ASSIGN 
       br-invlne:POPUP-MENU IN FRAME F-Main         = MENU POPUP-MENU-BROWSE-2:HANDLE.

/* SETTINGS FOR FILL-IN Invoice.EntityCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Account IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Supply IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Tenant IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_total IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Invoice.InvoiceDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Invoice.InvoiceNo IN FRAME F-Main
   NO-ENABLE EXP-LABEL EXP-FORMAT                                       */
/* SETTINGS FOR FILL-IN Invoice.TaxAmount IN FRAME F-Main
   NO-ENABLE EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Invoice.ToDetail IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Invoice.Total IN FRAME F-Main
   NO-ENABLE EXP-FORMAT                                                 */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-invlne
/* Query rebuild information for BROWSE br-invlne
     _TblList          = "TTPL.InvoiceLine OF TTPL.Invoice"
     _Options          = "NO-LOCK"
     _FldNameList[1]   > TTPL.InvoiceLine.EntityType
"InvoiceLine.EntityType" ? ? "character" ? ? ? ? ? ? yes ?
     _FldNameList[2]   > TTPL.InvoiceLine.EntityCode
"InvoiceLine.EntityCode" "  Code" ? "integer" ? ? ? ? ? ? yes ?
     _FldNameList[3]   > TTPL.InvoiceLine.AccountCode
"InvoiceLine.AccountCode" " Account" ? "decimal" ? ? ? ? ? ? yes ?
     _FldNameList[4]   > TTPL.InvoiceLine.AccountText
"InvoiceLine.AccountText" "Description                                                                               ." "X(30)" "character" ? ? ? ? ? ? yes ?
     _FldNameList[5]   > TTPL.InvoiceLine.Amount
"InvoiceLine.Amount" "Unit Price" ? "decimal" ? ? ? ? ? ? yes ?
     _FldNameList[6]   > TTPL.InvoiceLine.Quantity
"InvoiceLine.Quantity" ? ? "decimal" ? ? ? ? ? ? yes ?
     _FldNameList[7]   > TTPL.InvoiceLine.Percent
"InvoiceLine.Percent" "Discount" ? "decimal" ? ? ? ? ? ? yes ?
     _FldNameList[8]   > TTPL.InvoiceLine.YourShare
"InvoiceLine.YourShare" "Amount" ? "decimal" ? ? ? ? ? ? no ?
     _Query            is NOT OPENED
*/  /* BROWSE br-invlne */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK KEEP-EMPTY"
     _TblOptList       = "FIRST OUTER"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON RETURN OF FRAME F-Main
ANYWHERE DO:
  IF LOOKUP( FOCUS:TYPE, 'FILL-IN,COMBO-BOX,EDITOR' ) <> 0 OR
     (FOCUS:TYPE = 'BUTTON':U AND FOCUS:DYNAMIC) THEN
  DO:
    APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-invlne
&Scoped-define SELF-NAME br-invlne
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-invlne V-table-Win
ON CTRL-DEL OF br-invlne IN FRAME F-Main
OR 'SHIFT-DEL':U OF {&SELF-NAME} ANYWHERE DO:
  RUN delete-line.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-invlne V-table-Win
ON ENTRY OF br-invlne IN FRAME F-Main
OR 'MOUSE-SELECT-CLICK':U OF {&SELF-NAME} DO:

DEF VAR n-results AS INT NO-UNDO.
DEF VAR s-focused AS LOGI NO-UNDO INIT ?.
DEF VAR n-row AS LOGI NO-UNDO INIT ?.
  IF LOOKUP( LAST-EVENT:LABEL, 'ENTER,TAB,MOUSE-SELECT-UP':U ) = 0 THEN RETURN.

  IF NOT AVAILABLE Invoice THEN RETURN.

  n-row = br-invlne:NEW-ROW IN FRAME {&FRAME-NAME}.

  n-results = NUM-RESULTS("br-invlne":U).
  IF NOT ( n-results = ? OR n-results = 0 ) THEN
    s-focused = br-invlne:SELECT-FOCUSED-ROW().

/*
MESSAGE "ENTRY of br-invlne: AVAILABLE(InvoiceLine) =" AVAILABLE(InvoiceLine) SKIP
        "n-results =" n-results "  s-focused =" s-focused SKIP
        "New row =" n-row.
*/
  IF n-row THEN DO:
    RUN check-last.
    /* Set the default screen-values */
    RUN set-line-defaults.
    line-modified = Yes.
  END.
  ELSE IF NOT AVAILABLE InvoiceLine THEN
    RUN add-new-line.

  APPLY 'ENTRY':U TO InvoiceLine.AccountCode IN BROWSE br-invlne.

  RETURN NO-APPLY.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-invlne V-table-Win
ON ROW-DISPLAY OF br-invlne IN FRAME F-Main
DO:
  RUN set-line-color.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-invlne V-table-Win
ON ROW-ENTRY OF br-invlne IN FRAME F-Main
DO:
  RUN check-last.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-invlne V-table-Win
ON ROW-LEAVE OF br-invlne IN FRAME F-Main
OR 'CURSOR-DOWN':U OF {&SELF-NAME} 
OR 'CURSOR-UP':U   OF {&SELF-NAME}
OR 'ENTER':U       OF InvoiceLine.Percent IN BROWSE {&SELF-NAME} ANYWHERE DO:

  IF line-modified THEN
  DO:
    RUN verify-line.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
  END.

  IF LAST-EVENT:FUNCTION = 'ROW-LEAVE':U AND
     LAST-EVENT:LABEL = 'ENTER':U THEN
  DO:
    IF last-line THEN APPLY 'CHOOSE':U TO btn_add-trn. ELSE APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-invlne V-table-Win
ON VALUE-CHANGED OF br-invlne IN FRAME F-Main
DO:
  RUN update-line-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME InvoiceLine.EntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL InvoiceLine.EntityType br-invlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF InvoiceLine.EntityType IN BROWSE br-invlne /* T */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  line-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME InvoiceLine.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL InvoiceLine.EntityCode br-invlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF InvoiceLine.EntityCode IN BROWSE br-invlne /*   Code */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  line-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME InvoiceLine.AccountCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL InvoiceLine.AccountCode br-invlne _BROWSE-COLUMN V-table-Win
ON GO OF InvoiceLine.AccountCode IN BROWSE br-invlne /*  Account */
DO:
  SELF:MODIFIED = No.
  RUN select-account-code( SELF ).
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL InvoiceLine.AccountCode br-invlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF InvoiceLine.AccountCode IN BROWSE br-invlne /*  Account */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  line-modified = Yes.

  RUN verify-account-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
  DO:
    SELF:MODIFIED = no.
    RETURN NO-APPLY.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME InvoiceLine.AccountText
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL InvoiceLine.AccountText br-invlne _BROWSE-COLUMN V-table-Win
ON GO OF InvoiceLine.AccountText IN BROWSE br-invlne /* Description                                                                               . */
DO:
  RUN update-description.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL InvoiceLine.AccountText br-invlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF InvoiceLine.AccountText IN BROWSE br-invlne /* Description                                                                               . */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  line-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME InvoiceLine.Amount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL InvoiceLine.Amount br-invlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF InvoiceLine.Amount IN BROWSE br-invlne /* Unit Price */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  line-modified = Yes.

  RUN update-line-share.
  RUN set-line-color.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME InvoiceLine.Percent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL InvoiceLine.Percent br-invlne _BROWSE-COLUMN V-table-Win
ON 'F3':U OF InvoiceLine.Percent IN BROWSE br-invlne /* Discount */
DO:
  RUN update-percent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL InvoiceLine.Percent br-invlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF InvoiceLine.Percent IN BROWSE br-invlne /* Discount */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.

  RUN verify-percent( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
  DO:
    SELF:MODIFIED = No.
    RETURN NO-APPLY.
  END.

  line-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN verify-invoice.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  RUN dispatch( 'update-record':U ).
  new-record = Yes.
  RUN dispatch( 'add-record':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add-trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add-trn V-table-Win
ON CHOOSE OF btn_add-trn IN FRAME F-Main /* Add transaction */
DO:
  RUN add-new-line.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Maintain" THEN 
  DO:
    RUN dispatch( 'cancel-record':U ).
  END.
  ELSE IF mode = "Register" AND AVAILABLE Invoice THEN
  DO:
    /* RUN dispatch( 'delete-record':U ). - UNDESIRABLE */
    RUN delete-invoice.
    RUN print-invoices.
    RUN notify( 'open-query, RECORD-SOURCE':U ).
  END.
  RUN dispatch( 'exit':u ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON CHOOSE OF btn_ok IN FRAME F-Main /* OK */
DO:
  RUN verify-invoice.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  RUN dispatch( 'update-record':U ).
  RUN print-invoices.
/*  RUN notify( 'hide, CONTAINER-SOURCE':U ). */
  RUN dispatch( 'exit':U ).
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Terms
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Terms V-table-Win
ON U1 OF cmb_Terms IN FRAME F-Main /* Terms */
DO:
  {inc/selcmb/scivt1.i "Invoice" "TermsCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Terms V-table-Win
ON U2 OF cmb_Terms IN FRAME F-Main /* Terms */
DO:
  {inc/selcmb/scivt2.i "Invoice" "TermsCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Invoice.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Invoice.EntityCode V-table-Win
ON LEAVE OF Invoice.EntityCode IN FRAME F-Main /* Debtor */
DO:
  {inc/selcde/cdtnt.i "fil_Tenant"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Tenant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U1 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt1.i "Invoice" "EntityCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U2 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt2.i "Invoice" "EntityCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U3 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt3.i "Invoice" "EntityCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Invoice.InvoiceDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Invoice.InvoiceDate V-table-Win
ON ENTRY OF Invoice.InvoiceDate IN FRAME F-Main /* Date */
DO:
  SELF:AUTO-ZAP = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Add V-table-Win
ON CHOOSE OF MENU-ITEM m_Add /* Add Line */
DO:
  APPLY 'TAB':U TO Invoiceline.Percent IN BROWSE {&BROWSE-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete V-table-Win
ON CHOOSE OF MENU-ITEM m_Delete /* Delete Line */
DO:
  RUN delete-line.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Invoice.TaxApplies
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Invoice.TaxApplies V-table-Win
ON VALUE-CHANGED OF Invoice.TaxApplies IN FRAME F-Main /* Taxable */
DO:
  RUN update-invoice-total IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Invoice.ToDetail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Invoice.ToDetail V-table-Win
ON LEAVE OF Invoice.ToDetail IN FRAME F-Main /* GL Text */
DO:
DO WITH FRAME {&FRAME-NAME}:
DEF VAR ed-text AS CHAR NO-UNDO.
  ed-text = Invoice.Blurb:SCREEN-VALUE.
  ENTRY( 1, ed-text, "~n") = Invoice.ToDetail:SCREEN-VALUE.
  Invoice.Blurb:SCREEN-VALUE = ed-text.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-new-line V-table-Win 
PROCEDURE add-new-line :
/*------------------------------------------------------------------------------
  Purpose:     Inserts a new invoice line at the end of the browse
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF NUM-RESULTS("br-invlne":U) = ? OR
     NUM-RESULTS("br-invlne":U) = 0 THEN
  DO:
    RUN create-line.
    RUN open-line-query.
    RUN set-line-defaults.
    RUN check-last.
  END.
  ELSE
    IF br-invlne:INSERT-ROW("AFTER") THEN .

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Invoice"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Invoice"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-line V-table-Win 
PROCEDURE assign-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE InvoiceLine THEN RETURN.

DO TRANSACTION:

  IF NOT NEW InvoiceLine THEN FIND CURRENT InvoiceLine EXCLUSIVE-LOCK.

  ASSIGN
    InvoiceLine.InvoiceNo = Invoice.InvoiceNo.

  ASSIGN BROWSE br-invlne
    InvoiceLine.EntityType
    InvoiceLine.EntityCode
    InvoiceLine.AccountCode
    InvoiceLine.AccountText
    InvoiceLine.Amount
    InvoiceLine.Percent
    InvoiceLine.YourShare.

  FIND CURRENT InvoiceLine NO-LOCK.
  
  RUN update-invoice-total.

END.

RUN display-line.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-lines V-table-Win 
PROCEDURE cancel-lines :
/*------------------------------------------------------------------------------
  Purpose:     Cancel the invoice lines
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Invoice THEN RETURN.
  
  FOR EACH InvoiceLine OF Invoice: DELETE InvoiceLine. END.
  
  FOR EACH CurrentLine:
    CREATE InvoiceLine.
    ASSIGN
      InvoiceLine.AccountCode = CurrentLine.AccountCode
      InvoiceLine.AccountText = CurrentLine.AccountText
      InvoiceLine.Amount      = CurrentLine.Amount
      InvoiceLine.InvoiceNo   = CurrentLine.InvoiceNo
      InvoiceLine.Percent     = CurrentLine.Percent
      InvoiceLine.YourShare   = CurrentLine.YourShare.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-last V-table-Win 
PROCEDURE check-last :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  line-modified = No.
  last-line = CURRENT-RESULT-ROW( "br-invlne" ) = NUM-RESULTS( "br-invlne" ).  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-line V-table-Win 
PROCEDURE check-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-account-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( InvoiceLine.AccountCode:HANDLE IN BROWSE br-invlne ).

  RUN verify-percent( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( InvoiceLine.Percent:HANDLE IN BROWSE br-invlne ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-line-query V-table-Win 
PROCEDURE close-line-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY br-invlne.
  RUN update-invoice-total.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-coa-list V-table-Win 
PROCEDURE create-coa-list :
/*------------------------------------------------------------------------------
  Purpose:     Create a dynmic selection list for chart of accounts
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  DEF VAR item AS CHAR NO-UNDO.
    
  CREATE SELECTION-LIST coa-list
  ASSIGN
    FRAME        = FRAME {&FRAME-NAME}:HANDLE
    BGCOLOR      = 0
    FGCOLOR      = 15
    X            = 1
    Y            = 1
    WIDTH-P      = 1
    HEIGHT-P     = 1
    DELIMITER    = '|'
    FONT         = 15
    VISIBLE      = No
    SENSITIVE    = No
    SCROLLBAR-VERTICAL = Yes
    SCROLLBAR-VERTICAL = No
          
  TRIGGERS:
    ON 'RETURN':U, 'DEFAULT-ACTION':U PERSISTENT RUN update-account-code IN THIS-PROCEDURE.
    ON 'LEAVE':U PERSISTENT RUN hide-coa-list IN THIS-PROCEDURE.
  END TRIGGERS.
  
  coa-list:LIST-ITEMS = "".

  DEF VAR i AS INT NO-UNDO.
  DEF VAR grp AS CHAR NO-UNDO.
  DO i = 1 TO NUM-ENTRIES(confirm-type):
    grp = ENTRY(i,confirm-type).
    FOR EACH ChartOfAccount NO-LOCK WHERE ChartOfAccount.AccountGroupCode = grp
                     AND ChartOfAccount.AccountCode - INT( ChartOfAccount.AccountCode ) = 0:
    
      item = STRING( ChartOfAccount.AccountCode, "9999" ) + '  ' + ChartOfAccount.Name.
      IF coa-list:ADD-LAST( item ) THEN.
    END.
  END.

  coa-list:HEIGHT-CHARS = 6.
  coa-list:WIDTH-CHARS  = 40.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-line V-table-Win 
PROCEDURE create-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

DEF BUFFER Other FOR InvoiceLine.
  FIND Tenant WHERE Tenant.TenantCode = INPUT Invoice.EntityCode NO-LOCK.
  FIND LAST Other OF Invoice NO-LOCK NO-ERROR.
  CREATE    InvoiceLine.
  ASSIGN    InvoiceLine.InvoiceNo = Invoice.InvoiceNo
            InvoiceLine.LineSeq   = (IF AVAILABLE(Other) THEN Other.LineSeq ELSE 0) + 1 .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-invoice V-table-Win 
PROCEDURE delete-invoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Invoice THEN RETURN.

  IF NUM-ENTRIES( invoice-list ) = 1 THEN invoice-list = "".
  FIND Current Invoice EXCLUSIVE-LOCK.
  DELETE Invoice.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-line V-table-Win 
PROCEDURE delete-line :
/*------------------------------------------------------------------------------
  Purpose:     Delete the invoice line with focus
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NUM-RESULTS( "br-invlne" ) = 0 OR NUM-RESULTS( "br-invlne" ) = ? THEN RETURN.
  
  IF br-invlne:SELECT-FOCUSED-ROW() IN FRAME {&FRAME-NAME} THEN
  DO:

    IF NOT br-invlne:NEW-ROW THEN
    DO:
      IF br-invlne:FETCH-SELECTED-ROW(1) THEN.
      IF AVAILABLE InvoiceLine THEN
      DO:
        GET CURRENT br-invlne EXCLUSIVE-LOCK.
        DELETE InvoiceLine.
        IF br-invlne:DELETE-CURRENT-ROW() THEN.
      END.
      
    END.
    ELSE IF br-invlne:DELETE-CURRENT-ROW() THEN.

  END.

  IF NUM-RESULTS( "br-invlne" ) = ? OR 
     NUM-RESULTS( "br-invlne" ) = 0 THEN
  DO:
    RUN close-line-query.
    RETURN.
  END.   
  
  IF br-invlne:FETCH-SELECTED-ROW(1) THEN.
  RUN display-line.
  
  RUN check-last.
  RUN update-invoice-total.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-line V-table-Win 
PROCEDURE display-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE InvoiceLine THEN RETURN.

  DISPLAY {&ENABLED-FIELDS-IN-QUERY-{&BROWSE-NAME}}
          WITH BROWSE {&BROWSE-NAME}.

  RUN set-line-color.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hide-coa-list V-table-Win 
PROCEDURE hide-coa-list :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  coa-list:VISIBLE   = No.
  coa-list:SENSITIVE = No.
  APPLY 'ENTRY':U TO InvoiceLine.AccountText IN BROWSE {&BROWSE-NAME}.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  ASSIGN
    Invoice.InvoiceStatus = IF new-record THEN '?' ELSE 'U'
    Invoice.EntityType    = "T"
    Invoice.InvoiceType   = "MANL"
    new-record            = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-delete-record V-table-Win 
PROCEDURE inst-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  Invoice.Blurb:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ?.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-disable-fields V-table-Win 
PROCEDURE inst-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}.
  DISABLE cmb_Terms .
  Invoice.Blurb:READ-ONLY = Yes.
  RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, "Window", 'hidden = Yes' ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     View or hide tax fields depending on the office
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  FIND FIRST Office WHERE ThisOffice NO-LOCK NO-ERROR.
  
  IF AVAILABLE Office AND Office.GST = 0 THEN
    HIDE  Invoice.TaxAmount Invoice.TaxApplies.

  IF AVAILABLE Invoice THEN
    fil_total = Invoice.Total + Invoice.TaxAmount.
  ELSE
    fil_total = 0.

  DISPLAY fil_total.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}.

  ENABLE cmb_Terms .
  Invoice.Blurb:READ-ONLY = No.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "Maintain" THEN DO WITH FRAME {&FRAME-NAME}:
    btn_add:SENSITIVE = No.
    btn_add:VISIBLE   = No.
    RUN dispatch( 'enable-fields':U ).
  END.
  ELSE IF mode = "Register" THEN DO:
    have-records = Yes.
    new-record = Yes.
    ASSIGN last-entity = INT( find-parent-key( 'TenantCode':U ) ) NO-ERROR.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "View" THEN DO:
    have-records = Yes.
    HIDE btn_add btn_ok btn_cancel IN FRAME {&FRAME-NAME}.
    RUN dispatch( 'disable-fields':U ).
  END.

  HIDE btn_Add-Trn IN FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE Invoice THEN DO:
    RUN save-current-lines.
    RUN open-line-query.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-cancel-record V-table-Win 
PROCEDURE local-cancel-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

DO TRANSACTION:

  /* Code placed here will execute PRIOR to standard behavior. */
  RUN cancel-lines.
  RUN update-invoice-total.
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'cancel-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-update-record V-table-Win 
PROCEDURE local-update-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'update-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  IF LOOKUP( STRING( Invoice.InvoiceNo ), invoice-list ) = 0 THEN
  DO:
    invoice-list = invoice-list + IF invoice-list = "" THEN "" ELSE ",".
    invoice-list = invoice-list + STRING( Invoice.InvoiceNo ) .
  END.

  last-entity = Invoice.EntityCode .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-line-query V-table-Win 
PROCEDURE open-line-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE Invoice THEN DO WITH FRAME {&FRAME-NAME}:
    OPEN QUERY br-invlne FOR EACH InvoiceLine OF Invoice NO-LOCK.
    RUN display-line.
    RUN update-line-fields.
    InvoiceLine.AccountCode:READ-ONLY IN BROWSE br-invlne = (mode = "View").
    InvoiceLine.AccountText:READ-ONLY IN BROWSE br-invlne = (mode = "View").
    InvoiceLine.Amount:READ-ONLY IN BROWSE br-invlne      = (mode = "View").
    InvoiceLine.Percent:READ-ONLY IN BROWSE br-invlne     = (mode = "View").
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF new-record THEN DO WITH FRAME {&FRAME-NAME}:
    new-record = No.
    
    CREATE Invoice.
    Invoice.EntityCode = last-entity.
    
    RUN dispatch IN THIS-PROCEDURE( 'display-fields':U ).
    RUN dispatch IN THIS-PROCEDURE( 'enable-fields':U ).

    /* Set default screen values for the invoice */
    ASSIGN
      Invoice.InvoiceDate:SCREEN-VALUE = STRING( TODAY )
      Invoice.TaxApplies:SCREEN-VALUE = IF Office.GST <> ? THEN "Yes" ELSE "No".

    /* Set the default terms code */      
    DEF VAR idx AS INT NO-UNDO.

    cmb_Terms:SCREEN-VALUE = cmb_Terms:ENTRY( 1 ).
    DO idx = 1 TO cmb_Terms:NUM-ITEMS:
      IF TRIM( ENTRY( 1, cmb_Terms:ENTRY(idx), "-" ) ) = default-terms THEN DO:
        cmb_Terms:SCREEN-VALUE = cmb_Terms:ENTRY( idx ).
        idx = cmb_Terms:NUM-ITEMS.
      END.
    END.

    IF AVAILABLE Invoice THEN RUN open-line-query.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = 'WINDOW-CLOSE':U THEN
    APPLY 'CHOOSE':U TO btn_cancel IN FRAME {&FRAME-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "Register" THEN have-records = Yes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-invoices V-table-Win 
PROCEDURE print-invoices :
/*------------------------------------------------------------------------------
  Purpose:     Print the invoice approval forms for all new/modified invoices
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEF VAR n AS INT NO-UNDO.

  n = NUM-ENTRIES( invoice-list ).
  IF  n = 0 THEN RETURN.

  /* Check to see if approval forms is turned off */
  IF SUBSTRING(approval-forms,1,1) = "N" THEN RETURN.

  MESSAGE n "Invoices to be printed" VIEW-AS ALERT-BOX INFORMATION.
  RUN process/report/invcappr.p( ?, ?, invoice-list ).

    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-current-lines V-table-Win 
PROCEDURE save-current-lines :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Invoice THEN RETURN.
  
  FOR EACH CurrentLine: DELETE CurrentLine. END.
  
  FOR EACH InvoiceLine OF Invoice NO-LOCK:
    CREATE CurrentLine.
    ASSIGN
      CurrentLine.AccountCode = InvoiceLine.AccountCode
      CurrentLine.AccountText = InvoiceLine.AccountText
      CurrentLine.Amount      = InvoiceLine.Amount
      CurrentLine.InvoiceNo   = InvoiceLine.InvoiceNo
      CurrentLine.Percent     = InvoiceLine.Percent
      CurrentLine.YourShare   = InvoiceLine.YourShare.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-account-code V-table-Win 
PROCEDURE select-account-code :
/*------------------------------------------------------------------------------
  Purpose:     Select an account code for the InvoiceLine
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER attach-to AS HANDLE NO-UNDO.
DEF VAR item AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  
  IF NOT VALID-HANDLE( coa-list ) THEN RUN create-coa-list.
      
  /* Set the COA List screen value */

  FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = DEC( attach-to:SCREEN-VALUE )
                      NO-LOCK NO-ERROR.
  
  IF AVAILABLE ChartOfAccount AND LOOKUP( ChartOfAccount.AccountGroupCode, confirm-type ) > 0
  THEN DO:
    item = STRING( ChartOfAccount.AccountCode, "9999" ) + '  ' + ChartOfAccount.Name.
    coa-list:SCREEN-VALUE = item.
  END.
  ELSE
    coa-list:SCREEN-VALUE = coa-list:ENTRY(1).
  
  /* Set the position */

  IF br-invlne:y + attach-to:y +
     attach-to:height-pixels + coa-list:height-pixels >
     FRAME {&FRAME-NAME}:HEIGHT-PIXELS THEN
    coa-list:y = br-invlne:y + attach-to:y - coa-list:height-pixels.
  ELSE
    coa-list:y = br-invlne:y + attach-to:y + attach-to:height-pixels.
    
  coa-list:x = br-invlne:x + attach-to:x + 20.
  coa-list:visible   = Yes.
  coa-list:sensitive = Yes.
  APPLY 'ENTRY':U TO coa-list.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records-when-ready V-table-Win 
PROCEDURE send-records-when-ready :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-line-color V-table-Win 
PROCEDURE set-line-color :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR amount AS DEC NO-UNDO.
  
  amount = IF CAN-QUERY( InvoiceLine.Amount:HANDLE IN BROWSE br-invlne, "SCREEN-VALUE" ) THEN
    DEC( InvoiceLine.Amount:SCREEN-VALUE ) ELSE InvoiceLine.Amount.
  
  InvoiceLine.Amount:FGCOLOR    = IF amount < 0 THEN 12 ELSE 0.
  InvoiceLine.YourShare:FGCOLOR = IF amount < 0 THEN 12 ELSE 0.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-line-defaults V-table-Win 
PROCEDURE set-line-defaults :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  ASSIGN
    InvoiceLine.EntityType:SCREEN-VALUE IN BROWSE br-invlne = STRING( Tenant.EntityType,InvoiceLine.EntityType:FORMAT )
    InvoiceLine.EntityCode:SCREEN-VALUE  = STRING( Tenant.EntityCode, InvoiceLine.EntityCode:FORMAT )
    InvoiceLine.AccountCode:SCREEN-VALUE = STRING( 0 )
    InvoiceLine.AccountText:SCREEN-VALUE = ""
    InvoiceLine.Amount:SCREEN-VALUE      = STRING( 0 )
    InvoiceLine.YourShare:SCREEN-VALUE   = STRING( 0 )
    
    InvoiceLine.EntityType:MODIFIED  = No
    InvoiceLine.EntityCode:MODIFIED  = No
    InvoiceLine.AccountCode:MODIFIED = No
    InvoiceLine.AccountText:MODIFIED = No
    InvoiceLine.Amount:MODIFIED      = No
    InvoiceLine.Percent:MODIFIED     = No.

  RUN update-line-fields.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-ac-fields V-table-Win 
PROCEDURE update-ac-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN update-description.
  RUN update-percent.
  RUN update-line-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-account-code V-table-Win 
PROCEDURE update-account-code :
/*------------------------------------------------------------------------------
  Purpose:     reset the code value.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  InvoiceLine.AccountCode:SCREEN-VALUE IN BROWSE br-invlne = 
    ENTRY( 1, coa-list:SCREEN-VALUE, ' ').
  RUN update-ac-fields.
  RUN hide-coa-list.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-account-name V-table-Win 
PROCEDURE update-account-name :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR code AS DEC NO-UNDO.
  code = INPUT BROWSE br-invlne InvoiceLine.AccountCode.

  FIND FIRST ChartOfAccount WHERE ChartOfAccount.AccountCode = code NO-LOCK NO-ERROR.
  
  ASSIGN fil_Account:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
    IF AVAILABLE ChartOfAccount THEN ChartOfAccount.Name ELSE "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-description V-table-Win 
PROCEDURE update-description :
/*------------------------------------------------------------------------------
  Purpose:     Update the account text description
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR code AS INT NO-UNDO.
  code = INPUT BROWSE br-invlne InvoiceLine.AccountCode.

  FIND FIRST ChartOfAccount WHERE ChartOfAccount.AccountCode = code NO-LOCK NO-ERROR.
  
  InvoiceLine.AccountText:SCREEN-VALUE IN BROWSE br-invlne =
                  IF AVAILABLE ChartOfAccount THEN ChartOfAccount.Name ELSE "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-invoice-total V-table-Win 
PROCEDURE update-invoice-total :
/*------------------------------------------------------------------------------
  Purpose:     Update the invoice total from it's invoice lines
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Invoice THEN RETURN.
  FIND CURRENT Invoice EXCLUSIVE-LOCK.

  ASSIGN
    Invoice.TaxAmount = IF Invoice.TaxApplies:SCREEN-VALUE
      IN FRAME {&FRAME-NAME} = "Yes" THEN Invoice.Total * ( Office.GST / 100 ) ELSE 0
    fil_total = Invoice.Total + Invoice.TaxAmount.
      
  DISPLAY
    Invoice.Total
    Invoice.TaxAmount
    fil_total
  WITH FRAME {&FRAME-NAME}.

 FIND CURRENT Invoice NO-LOCK.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-line V-table-Win 
PROCEDURE update-line :
/*------------------------------------------------------------------------------
  Purpose:     Update the current invoice line.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Create a new line if needed and update the details */

  IF br-invlne:NEW-ROW IN FRAME {&FRAME-NAME} THEN
  DO:

    RUN create-line.
    IF br-invlne:CREATE-RESULT-LIST-ENTRY() THEN.
    RUN assign-line.

  END.
  ELSE
    RUN assign-line.

  line-modified = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-line-fields V-table-Win 
PROCEDURE update-line-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN update-account-name.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-line-share V-table-Win 
PROCEDURE update-line-share :
/*------------------------------------------------------------------------------
  Purpose:     update the line share field
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  IF InvoiceLine.Amount:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} <> "" AND
     InvoiceLine.Percent:SCREEN-VALUE <> "" AND
     (InvoiceLine.Amount:MODIFIED OR InvoiceLine.Percent:MODIFIED) THEN
    InvoiceLine.YourShare:SCREEN-VALUE = 
      STRING( DEC( InvoiceLine.Amount:SCREEN-VALUE ) *
              DEC( InvoiceLine.Percent:SCREEN-VALUE ) / 100 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-percent V-table-Win 
PROCEDURE update-percent :
/*------------------------------------------------------------------------------
  Purpose:     reset the invoice line percentage recoverbale based on the
               account code.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR code        AS DEC NO-UNDO.
  DEF VAR tot-percent AS DEC INIT 0 NO-UNDO.
  DEF VAR n-outgoings AS INT INIT 0 NO-UNDO.  
  DEF VAR tenant-code AS INT NO-UNDO.
  
  tenant-code = INT( Invoice.EntityCode:SCREEN-VALUE IN FRAME {&FRAME-NAME} ).
  code = INPUT BROWSE br-invlne InvoiceLine.AccountCode.
  
  FOR EACH TenancyLease
    WHERE TenancyLease.TenantCode = tenant-code NO-LOCK,
    FIRST TenancyOutGoing WHERE
      TenancyOutgoing.TenancyLeaseCode = TenancyLease.TenancyLeaseCode AND
      TenancyOutGoing.AccountCode      = code NO-LOCK:

    n-outgoings = n-outgoings + 1.
    tot-percent = tot-percent + TenancyOutGoing.Percentage.

  END.

  IF n-outgoings = 0 THEN tot-percent = 100.00. ELSE
    IF tot-percent > 100.00 THEN tot-percent = 100.00.
    
  InvoiceLine.Percent:SCREEN-VALUE IN BROWSE br-invlne = STRING( tot-percent ).

  RUN update-line-share.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-account-code V-table-Win 
PROCEDURE verify-account-code :
/*------------------------------------------------------------------------------
  Purpose:     reset the invoice line fields based on the account code.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER v-mode AS CHAR NO-UNDO.

DEF BUFFER coa FOR ChartOfAccount.

  DEF VAR code AS DEC NO-UNDO.
  code = INPUT BROWSE br-invlne InvoiceLine.AccountCode.
  
  /* Check to see if the account exists */
  FIND FIRST coa WHERE coa.AccountCode = code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(coa) THEN DO:
    IF v-mode = "Verify" THEN
      MESSAGE "The account code " + STRING( code ) + " is invalid !"
                VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
    RETURN "FAIL".
  END.

  IF LOOKUP(coa.AccountGroupCode,confirm-type) = 0 THEN DO:
    IF v-mode = "Verify" AND NOT verifying-line THEN
      MESSAGE "Account " + STRING( code ) + " is not a " SKIP
              "Property Income Account."
      VIEW-AS ALERT-BOX WARNING.
  END.

  IF NOT verifying-line THEN RUN update-ac-fields.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-invoice V-table-Win 
PROCEDURE verify-invoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NUM-RESULTS( "br-invlne" ) = 0 OR
     NUM-RESULTS( "br-invlne" ) = ? THEN DO:
     MESSAGE "You must enter at least one line for this invoice !"
       VIEW-AS ALERT-BOX ERROR.
     RETURN "FAIL".
   END.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-line V-table-Win 
PROCEDURE verify-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR err-field AS HANDLE NO-UNDO.

  verifying-line = Yes.
  RUN check-line.
  verifying-line = No.
  
  err-field = WIDGET-HANDLE( RETURN-VALUE ).

  IF VALID-HANDLE( err-field ) THEN
  DO:
    MESSAGE "The current invoice line is in error." SKIP
      "Do you want to abort the changes ? "
      VIEW-AS ALERT-BOX ERROR BUTTONS YES-NO UPDATE abort-it AS LOGI.
    IF abort-it THEN
    DO:
      line-modified = No.
      IF AVAILABLE InvoiceLine AND InvoiceLine.AccountCode <> 0000.00 THEN
        RUN display-line.
      ELSE
        RUN delete-line.
    END.
    ELSE
    DO:
      IF AVAILABLE InvoiceLine THEN GET CURRENT br-invlne.
      APPLY 'ENTRY':U TO err-field.
      RETURN "FAIL".
    END.
  END.
  ELSE
    RUN update-line.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-percent V-table-Win 
PROCEDURE verify-percent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER v-mode AS CHAR NO-UNDO.
  
  IF INPUT BROWSE br-invlne InvoiceLine.Percent > 100 THEN
  DO:
    IF v-mode = "Verify" THEN
      MESSAGE "You cannot have a percentage share " SKIP
              "greater than 100 !"
        VIEW-AS ALERT-BOX ERROR TITLE "Percentage Share Error".

    InvoiceLine.Percent:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = "100.00".
    RETURN "FAIL".
  END.

  RUN update-line-share.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


