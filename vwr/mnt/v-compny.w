&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR mode AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Company
&Scoped-define FIRST-EXTERNAL-TABLE Company


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Company.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Company.CompanyCode Company.Active ~
Company.LegalName Company.ShortName Company.PreviousNames ~
Company.RegisteredAddress Company.IncorporationDate Company.RegisteredNo ~
Company.BusinessNo Company.ParentCode Company.ClientCode ~
Company.AuthorisedCapital Company.OperationalCountry Company.IsssuedCapital ~
Company.Paid Company.TaxRegistered Company.TaxNo Company.NextAnnualReturn ~
Company.ParentPaysBills
&Scoped-define ENABLED-TABLES Company
&Scoped-define FIRST-ENABLED-TABLE Company
&Scoped-Define ENABLED-OBJECTS RECT-4 RECT-5 RECT-7 
&Scoped-Define DISPLAYED-FIELDS Company.CompanyCode Company.Active ~
Company.LegalName Company.ShortName Company.PreviousNames ~
Company.RegisteredAddress Company.IncorporationDate Company.RegisteredNo ~
Company.BusinessNo Company.ParentCode Company.ClientCode ~
Company.AuthorisedCapital Company.OperationalCountry Company.IsssuedCapital ~
Company.Paid Company.TaxRegistered Company.TaxNo Company.NextAnnualReturn ~
Company.ParentPaysBills
&Scoped-define DISPLAYED-TABLES Company
&Scoped-define FIRST-DISPLAYED-TABLE Company
&Scoped-Define DISPLAYED-OBJECTS fil_Secretary fil_Parent fil_Client ~
pr_TaxNo 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
CompanyCode|y|y|TTPL.Company.CompanyCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "CompanyCode",
     Keys-Supplied = "CompanyCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fil_Client AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 38.43 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Parent AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 38.43 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Secretary AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 48 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE pr_TaxNo AS CHARACTER FORMAT "X(256)":U INITIAL "Tax No:" 
      VIEW-AS TEXT 
     SIZE 10.0 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 32 BY .05.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 18 BY .05.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 61.72 BY 21.75.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Company.CompanyCode AT ROW 1 COL 9.29 COLON-ALIGNED
          LABEL "Code"
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1
     Company.Active AT ROW 1 COL 53.57
          VIEW-AS TOGGLE-BOX
          SIZE 8 BY 1
          FONT 10
     Company.LegalName AT ROW 2.2 COL 11.29 NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 51 BY 1
          FONT 10
     Company.ShortName AT ROW 3.2 COL 9.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 15.57 BY 1
          FONT 10
     Company.PreviousNames AT ROW 4.4 COL 11.29 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 51 BY 2.6
          FONT 10
     Company.RegisteredAddress AT ROW 7.2 COL 11.29 NO-LABEL
          VIEW-AS EDITOR NO-WORD-WRAP SCROLLBAR-VERTICAL
          SIZE 51 BY 3
          FONT 10
     Company.IncorporationDate AT ROW 10.2 COL 9.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 10.86 BY 1.05
     Company.RegisteredNo AT ROW 10.2 COL 42.43 COLON-ALIGNED
          LABEL "Registered Company No"
          VIEW-AS FILL-IN 
          SIZE 17.72 BY 1.05
     Company.BusinessNo AT ROW 11.25 COL 42.43 COLON-ALIGNED FORMAT "X(20)"
          VIEW-AS FILL-IN 
          SIZE 17.72 BY 1.05
     fil_Secretary AT ROW 12.8 COL 14.29 NO-LABEL
     Company.ParentCode AT ROW 14.2 COL 9.29 COLON-ALIGNED NO-LABEL FORMAT ">>999"
          VIEW-AS FILL-IN 
          SIZE 9.72 BY 1
     fil_Parent AT ROW 14.2 COL 21.86 COLON-ALIGNED NO-LABEL
     Company.ClientCode AT ROW 15.2 COL 9.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 9.72 BY 1
     fil_Client AT ROW 15.2 COL 21.86 COLON-ALIGNED NO-LABEL
     Company.ParentPaysBills AT ROW 16.2 COL 9.29 COLON-ALIGNED
          LABEL "Use parent company cheque account"
          VIEW-AS TOGGLE-BOX
          SIZE 36 BY 1
     Company.AuthorisedCapital AT ROW 18.4 COL 9.57 COLON-ALIGNED NO-LABEL FORMAT "-Z,ZZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 19 BY 1
          FONT 11
     Company.OperationalCountry AT ROW 18.9 COL 46.57 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "New Zealand", "NZL":U,
"Australian", "AUS":U,
"Other", "OTH":U
          SIZE 13 BY 3
          FONT 10
     Company.IsssuedCapital AT ROW 19.4 COL 9.57 COLON-ALIGNED NO-LABEL FORMAT "-Z,ZZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 19 BY 1
          FONT 11
     Company.Paid AT ROW 19.4 COL 31.29
          VIEW-AS TOGGLE-BOX
          SIZE 6 BY 1
          FONT 10
     Company.TaxRegistered AT ROW 20.8 COL 31.29
          LABEL "Registered"
          VIEW-AS TOGGLE-BOX
          SIZE 10.72 BY 1.05
     Company.TaxNo AT ROW 20.9 COL 9.57 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 19 BY 1
          FONT 10
     Company.NextAnnualReturn AT ROW 21.9 COL 9.57 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 11.14 BY 1
          FONT 10
     pr_TaxNo AT ROW 20.85 COL 1.57 NO-LABEL
     RECT-4 AT ROW 17.9 COL 1
     RECT-5 AT ROW 17.9 COL 44.57
     RECT-7 AT ROW 1.5 COL 1
     "Name:" VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 2.2 COL 1.57
          FONT 10
     "Authorised:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 18.4 COL 1.57
          FONT 10
     "Issued:" VIEW-AS TEXT
          SIZE 6 BY 1 AT ROW 19.4 COL 1.57
          FONT 10
     "Returns Due:" VIEW-AS TEXT
          SIZE 9 BY 1 AT ROW 21.9 COL 1.57
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME F-Main
     "Short Name:" VIEW-AS TEXT
          SIZE 9.14 BY .8 AT ROW 3.2 COL 1.57
     "Prev Names:" VIEW-AS TEXT
          SIZE 9 BY 1 AT ROW 4.2 COL 1.57
          FONT 10
     "Registered" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 7 COL 1.57
          FONT 10
     "Office:" VIEW-AS TEXT
          SIZE 5.14 BY .8 AT ROW 7.8 COL 1.57
          FONT 10
     "Incorporated:" VIEW-AS TEXT
          SIZE 9.14 BY 1 AT ROW 10.2 COL 1.57
     "Secretary:" VIEW-AS TEXT
          SIZE 6.86 BY 1 AT ROW 12.8 COL 1.57
          FONT 10
     "Parent:" VIEW-AS TEXT
          SIZE 5.14 BY 1 AT ROW 14.2 COL 1.57
          FONT 10
     "Client/Owner:" VIEW-AS TEXT
          SIZE 9.72 BY 1 AT ROW 15.2 COL 10.29 RIGHT-ALIGNED
          FONT 10
     "C A P I T A L" VIEW-AS TEXT
          SIZE 9 BY .95 AT ROW 17.4 COL 11.57
          FONT 10
     "Operational Company" VIEW-AS TEXT
          SIZE 15 BY 1 AT ROW 17.4 COL 46.14
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Company
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 25.5
         WIDTH              = 67.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Company.AuthorisedCapital IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Company.BusinessNo IN FRAME F-Main
   EXP-FORMAT                                                           */
/* SETTINGS FOR FILL-IN Company.ClientCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Company.CompanyCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Client IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Parent IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Secretary IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN Company.IncorporationDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Company.IsssuedCapital IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Company.LegalName IN FRAME F-Main
   ALIGN-L EXP-LABEL EXP-FORMAT                                         */
/* SETTINGS FOR FILL-IN Company.NextAnnualReturn IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Company.ParentCode IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
ASSIGN 
       Company.PreviousNames:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR FILL-IN pr_TaxNo IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
ASSIGN 
       pr_TaxNo:READ-ONLY IN FRAME F-Main        = TRUE.

/* SETTINGS FOR EDITOR Company.RegisteredAddress IN FRAME F-Main
   EXP-LABEL                                                            */
ASSIGN 
       Company.RegisteredAddress:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR FILL-IN Company.RegisteredNo IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Company.ShortName IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Company.TaxNo IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX Company.TaxRegistered IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TEXT-LITERAL "Client/Owner:"
          SIZE 9.72 BY 1 AT ROW 15.2 COL 10.29 RIGHT-ALIGNED            */

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Company.ClientCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Company.ClientCode V-table-Win
ON LEAVE OF Company.ClientCode IN FRAME F-Main /* ClientCode */
DO:
  {inc/selcde/cdclient.i "fil_Client"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Client
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Client V-table-Win
ON U1 OF fil_Client IN FRAME F-Main
DO:
  {inc/selfil/sfclient1.i "Company" "ClientCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Client V-table-Win
ON U2 OF fil_Client IN FRAME F-Main
DO:
  {inc/selfil/sfclient2.i "Company" "ClientCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Client V-table-Win
ON U3 OF fil_Client IN FRAME F-Main
DO:
  {inc/selfil/sfclient3.i "Company" "ClientCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Parent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Parent V-table-Win
ON U1 OF fil_Parent IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "Company" "ParentCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Parent V-table-Win
ON U2 OF fil_Parent IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "Company" "ParentCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Parent V-table-Win
ON U3 OF fil_Parent IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "Company" "ParentCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Secretary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Secretary V-table-Win
ON U1 OF fil_Secretary IN FRAME F-Main
DO:
  {inc/selfil/sfpsn1.i "Company" "Secretary"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Secretary V-table-Win
ON U2 OF fil_Secretary IN FRAME F-Main
DO:
  {inc/selfil/sfpsn2.i "Company" "Secretary"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Secretary V-table-Win
ON U3 OF fil_Secretary IN FRAME F-Main
DO:
  {inc/selfil/sfpsn3.i "Company" "Secretary"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Company.OperationalCountry
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Company.OperationalCountry V-table-Win
ON VALUE-CHANGED OF Company.OperationalCountry IN FRAME F-Main /* OperationalCountry */
DO:
  RUN country-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Company.ParentCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Company.ParentCode V-table-Win
ON LEAVE OF Company.ParentCode IN FRAME F-Main /* Parent */
DO:
  {inc/selcde/cdcmp.i "fil_Parent"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

DO WITH FRAME {&FRAME-NAME}:
  IF Company.OperationalCountry:MOVE-AFTER-TAB-ITEM( Company.NextAnnualReturn:HANDLE ) THEN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'CompanyCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Company
           &WHERE = "WHERE Company.CompanyCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Company"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Company"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':u ).
  IF mode = 'Add' THEN RUN delete-company. ELSE RUN check-modified( 'CLEAR':U ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-company.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN notify( 'hide, CONTAINER-SOURCE':u ).
  RUN dispatch( 'update-record':U ).
  IF mode = "Add" THEN RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE country-changed V-table-Win 
PROCEDURE country-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  
  IF INPUT Company.OperationalCountry = "NZL" THEN DO:
    fil_Secretary:HIDDEN = TRUE.
    BusinessNo:HIDDEN = TRUE.
    RegisteredNo:LABEL = "New Zealand Company No".
    pr_TaxNo:SCREEN-VALUE = "IRD No:".
  END.
  ELSE IF INPUT Company.OperationalCountry = "AUS" THEN DO:
    fil_Secretary:HIDDEN = FALSE.
    BusinessNo:HIDDEN = FALSE.
    RegisteredNo:LABEL = "Australian Company No".
    BusinessNo:LABEL = "Australian Business No".
    pr_TaxNo:SCREEN-VALUE = "Tax File No:".
  END.
  ELSE DO:
    fil_Secretary:HIDDEN = FALSE.
    RegisteredNo:LABEL = "Registered Company No".
    pr_TaxNo:SCREEN-VALUE = "Tax No:".
    BusinessNo:HIDDEN = FALSE.
    BusinessNo:LABEL = "Registered Business No".
  END.

  RUN set-link-attributes IN sys-mgr(
    THIS-PROCEDURE, STRING( fil_Secretary:HANDLE ),
    "HIDDEN = " + IF fil_Secretary:HIDDEN THEN "Yes" ELSE "No" ).
      
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-company V-table-Win 
PROCEDURE delete-company :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Company EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Company THEN DELETE Company.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-disable-fields V-table-Win 
PROCEDURE inst-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  Company.PreviousNames:SENSITIVE = No.
  Company.PreviousNames:BGCOLOR = 16.
  Company.RegisteredAddress:SENSITIVE = No.
  Company.RegisteredAddress:BGCOLOR = 16.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN country-changed.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  Company.CompanyCode:SENSITIVE = NOT CAN-FIND( FIRST AcctTran WHERE AcctTran.EntityType = "L"
                                                AND AcctTran.EntityCode = INPUT Company.CompanyCode).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = 'Add' THEN DO:
    have-records = Yes.
    CURRENT-WINDOW:TITLE = "Adding New Company".
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "Maintain" THEN
    RUN dispatch( 'enable-fields':U ).
  ELSE
    RUN dispatch( 'disable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF BUFFER LastCompany FOR Company.

  FIND LAST LastCompany NO-LOCK NO-ERROR.
  
  CREATE Company.

  ASSIGN Company.CompanyCode = (IF AVAILABLE(LastCompany) THEN LastCompany.CompanyCode + 1 ELSE 1)
         Company.Active      = Yes .
  
  CURRENT-WINDOW:TITLE = "Adding a new Company".
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'update-record':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "CompanyCode" "Company" "CompanyCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Company"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-company V-table-Win 
PROCEDURE verify-company :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF INPUT Company.CompanyCode <> Company.CompanyCode
     AND CAN-FIND( Company WHERE Company.CompanyCode = INPUT Company.CompanyCode ) THEN
  DO:
    MESSAGE
      "A company already exists with the code" INPUT Company.CompanyCode "!" SKIP
      "You must choose a unique company code."
      VIEW-AS ALERT-BOX ERROR TITLE "Duplicate Company".
    APPLY 'ENTRY':U TO Company.CompanyCode.
    RETURN "FAIL".
  END.
  
  IF INPUT Company.LegalName = "" OR INPUT company.ShortName = "" THEN
  DO:
    MESSAGE
      "You must supply both a legal and short name" SKIP
      "for the company!"
      VIEW-AS ALERT-BOX ERROR TITLE "Incomplete Name".
    IF INPUT Company.LegalName = "" THEN APPLY 'ENTRY':U TO Company.LegalName. ELSE
    IF INPUT Company.ShortName = "" THEN APPLY 'ENTRY':U TO Company.ShortName.
    RETURN "FAIL".
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

