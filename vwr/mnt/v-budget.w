&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Local Variable Definitions ---                                       */

DEF VAR user-name AS CHAR NO-UNDO.
DEF VAR no-months AS INT NO-UNDO.

DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO.
DEF VAR ext-et AS CHAR NO-UNDO.
DEF VAR ext-ec AS INT NO-UNDO.
DEF VAR ext-ac AS DEC NO-UNDO.

DEF VAR source-link-name AS CHAR NO-UNDO.
DEF VAR source-table AS CHAR NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set.i "Budget-Closed-To" "budget-closed-to" "ERROR"}
DEF VAR budget-closed-date AS DATE NO-UNDO.
budget-closed-date = DATE(budget-closed-to).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Int2 RP.Char2 RP.Int1 RP.Dec1 RP.Char3 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS fil_Month-1 fil_Month-2 fil_Month-3 ~
fil_Month-4 fil_AnnualBudget fil_Month-5 fil_Month-6 fil_Month-7 ~
fil_Month-8 fil_Month-9 fil_Month-10 fil_Month-11 fil_Month-12 Btn_OK ~
Btn_Cancel Btn_Apply RECT-1 
&Scoped-Define DISPLAYED-FIELDS RP.Int2 RP.Char2 RP.Int1 RP.Dec1 RP.Char3 
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define DISPLAYED-OBJECTS fil_YearDescription fil_EntityName ~
fil_AccountName fil_Month-1 fil_Month-2 fil_Month-3 fil_Month-4 ~
fil_AnnualBudget fil_Month-5 fil_Month-6 fil_Month-7 fil_Month-8 ~
fil_Month-9 fil_Month-10 fil_Month-11 fil_Month-12 fil_Budgetclosed 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
EntityType||y|AccountSummary.EntityType
EntityCode||y|AccountSummary.EntityCode
AccountCode||y|AccountSummary.AccountCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = "EntityType,EntityCode,AccountCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Apply DEFAULT 
     LABEL "&Apply" 
     SIZE 12 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn_Cancel AUTO-END-KEY DEFAULT 
     LABEL "&Cancel" 
     SIZE 12 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "&OK" 
     SIZE 12 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE fil_AccountName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45.14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_AnnualBudget AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Full year" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Budgetclosed AS DATE FORMAT "99/99/9999":U 
     LABEL "Budgets are closed up to" 
      VIEW-AS TEXT 
     SIZE 16 BY .7 NO-UNDO.

DEFINE VARIABLE fil_EntityName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45.14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-1 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "January" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-10 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "October" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-11 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "November" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-12 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "December" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-2 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "February" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-3 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "March" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-4 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "April" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-5 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "May" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-6 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "June" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-7 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "July" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-8 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "August" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Month-9 AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "September" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_YearDescription AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45.14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 65.14 BY 17.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Int2 AT ROW 1.2 COL 9.29 COLON-ALIGNED HELP
          ""
          LABEL "Year" FORMAT "9999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_YearDescription AT ROW 1.2 COL 18.43 COLON-ALIGNED NO-LABEL
     RP.Char2 AT ROW 2.2 COL 9.29 COLON-ALIGNED HELP
          ""
          LABEL "Entity" FORMAT "X"
          VIEW-AS FILL-IN 
          SIZE 2.86 BY 1
     RP.Int1 AT ROW 2.2 COL 12.14 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_EntityName AT ROW 2.2 COL 18.43 COLON-ALIGNED NO-LABEL
     RP.Dec1 AT ROW 3.2 COL 9.29 COLON-ALIGNED HELP
          ""
          LABEL "Account" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 8.86 BY 1
     fil_AccountName AT ROW 3.2 COL 18.43 COLON-ALIGNED NO-LABEL
     RP.Char3 AT ROW 4.2 COL 11.29 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Annual", "A":U,
"Monthly", "M":U
          SIZE 11.43 BY 2.4
          FONT 10
     fil_Month-1 AT ROW 4.4 COL 51.29 COLON-ALIGNED
     fil_Month-2 AT ROW 5.4 COL 51.29 COLON-ALIGNED
     fil_Month-3 AT ROW 6.4 COL 51.29 COLON-ALIGNED
     fil_Month-4 AT ROW 7.4 COL 51.29 COLON-ALIGNED
     fil_AnnualBudget AT ROW 8.2 COL 9.29 COLON-ALIGNED
     fil_Month-5 AT ROW 8.4 COL 51.29 COLON-ALIGNED
     fil_Month-6 AT ROW 9.4 COL 51.29 COLON-ALIGNED
     fil_Month-7 AT ROW 10.4 COL 51.29 COLON-ALIGNED
     fil_Month-8 AT ROW 11.4 COL 51.29 COLON-ALIGNED
     fil_Month-9 AT ROW 12.4 COL 51.29 COLON-ALIGNED
     fil_Month-10 AT ROW 13.4 COL 51.29 COLON-ALIGNED
     fil_Month-11 AT ROW 14.4 COL 51.29 COLON-ALIGNED
     fil_Month-12 AT ROW 15.4 COL 51.29 COLON-ALIGNED
     Btn_OK AT ROW 16.8 COL 28.43
     Btn_Cancel AT ROW 16.8 COL 41
     Btn_Apply AT ROW 16.8 COL 53.57
     fil_Budgetclosed AT ROW 10.9 COL 21 COLON-ALIGNED
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 9
         DEFAULT-BUTTON Btn_Apply CANCEL-BUTTON Btn_Cancel.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.6
         WIDTH              = 76.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR RADIO-SET RP.Char3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Dec1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_AccountName IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Budgetclosed IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_EntityName IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_YearDescription IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Btn_Apply
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Apply V-table-Win
ON CHOOSE OF Btn_Apply IN FRAME F-Main /* Apply */
DO:
  RUN assign-budgets.
  APPLY 'ENTRY':U TO RP.Dec1 .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel V-table-Win
ON CHOOSE OF Btn_Cancel IN FRAME F-Main /* Cancel */
DO:
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  RUN assign-budgets.
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON LEAVE OF RP.Char2 IN FRAME F-Main /* Entity */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN dispatch( 'enable-fields':U ).
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN dispatch( 'enable-fields':U ).
      IF INPUT {&SELF-NAME} = "A" THEN
        APPLY "ENTRY":U TO fil_AnnualBudget.
      ELSE
        APPLY "ENTRY":U TO fil_Month-1.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Dec1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Dec1 V-table-Win
ON LEAVE OF RP.Dec1 IN FRAME F-Main /* Account */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN dispatch( 'enable-fields':U ).
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Dec1 V-table-Win
ON RETURN OF RP.Dec1 IN FRAME F-Main /* Account */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    APPLY 'TAB' TO SELF.
    APPLY 'TAB' TO RP.Char3.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_AnnualBudget
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_AnnualBudget V-table-Win
ON LEAVE OF fil_AnnualBudget IN FRAME F-Main /* Full year */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT fil_AnnualBudget <> fil_AnnualBudget THEN DO:
      ASSIGN fil_AnnualBudget.
      RUN set-monthly-amounts.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-1 V-table-Win
ON LEAVE OF fil_Month-1 IN FRAME F-Main /* January */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-10
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-10 V-table-Win
ON LEAVE OF fil_Month-10 IN FRAME F-Main /* October */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-11
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-11 V-table-Win
ON LEAVE OF fil_Month-11 IN FRAME F-Main /* November */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-12
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-12 V-table-Win
ON LEAVE OF fil_Month-12 IN FRAME F-Main /* December */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-2 V-table-Win
ON LEAVE OF fil_Month-2 IN FRAME F-Main /* February */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-3 V-table-Win
ON LEAVE OF fil_Month-3 IN FRAME F-Main /* March */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-4 V-table-Win
ON LEAVE OF fil_Month-4 IN FRAME F-Main /* April */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-5 V-table-Win
ON LEAVE OF fil_Month-5 IN FRAME F-Main /* May */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-6 V-table-Win
ON LEAVE OF fil_Month-6 IN FRAME F-Main /* June */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-7 V-table-Win
ON LEAVE OF fil_Month-7 IN FRAME F-Main /* July */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-8
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-8 V-table-Win
ON LEAVE OF fil_Month-8 IN FRAME F-Main /* August */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Month-9
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Month-9 V-table-Win
ON LEAVE OF fil_Month-9 IN FRAME F-Main /* September */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN set-annual-amount.
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Int1 */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN dispatch( 'enable-fields':U ).
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* Year */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN DO:
      ASSIGN {&SELF-NAME}.
      RUN assign-month-labels.
      RUN dispatch( 'enable-fields':U ).
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

fil_BudgetClosed = budget-closed-date .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-budgets V-table-Win 
PROCEDURE assign-budgets :
/*------------------------------------------------------------------------------
  Purpose:  Assign the budget figures to the AccountBalance
------------------------------------------------------------------------------*/
DEF VAR et AS CHAR NO-UNDO.
DEF VAR ec AS INT NO-UNDO.
DEF VAR ac AS DEC NO-UNDO.
DEF VAR bals AS DEC EXTENT 12 NO-UNDO.
DEF VAR i AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  bals[1] = INPUT fil_Month-1.
  bals[2] = INPUT fil_Month-2.
  bals[3] = INPUT fil_Month-3.
  bals[4] = INPUT fil_Month-4.
  bals[5] = INPUT fil_Month-5.
  bals[6] = INPUT fil_Month-6.
  bals[7] = INPUT fil_Month-7.
  bals[8] = INPUT fil_Month-8.
  bals[9] = INPUT fil_Month-9.
  bals[10] = INPUT fil_Month-10.
  bals[11] = INPUT fil_Month-11.
  bals[12] = INPUT fil_Month-12.
  et = INPUT RP.Char2.
  ec = INPUT RP.Int1.
  ac = INPUT RP.Dec1.
END.

  DO TRANSACTION WITH FRAME {&FRAME-NAME}:
    i = 1.
    FOR EACH Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK:
      IF i > 12 THEN LEAVE.
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode EXCLUSIVE-LOCK NO-ERROR.
      IF NOT AVAILABLE(AccountBalance) THEN DO:
        CREATE AccountBalance.
        ASSIGN AccountBalance.EntityType = et
               AccountBalance.EntityCode = ec
               AccountBalance.AccountCode = ac
               AccountBalance.MonthCode = Month.MonthCode.
      END.
      AccountBalance.RevisedBudget = bals[i] .
      IF Month.StartDate > budget-closed-date THEN
        AccountBalance.Budget = bals[i] .
      i = i + 1.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-month-labels V-table-Win 
PROCEDURE assign-month-labels :
/*------------------------------------------------------------------------------
  Purpose:  Assign the names of the months
------------------------------------------------------------------------------*/
  DO WITH FRAME {&FRAME-NAME}:
    FIND FIRST Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-1:LABEL = Month.MonthName.
      no-months = 1.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-1.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-2:LABEL = Month.MonthName.
      no-months = 2.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-2.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-3:LABEL = Month.MonthName.
      no-months = 3.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-3.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-4:LABEL = Month.MonthName.
      no-months = 4.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-4.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-5:LABEL = Month.MonthName.
      no-months = 5.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-5.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-6:LABEL = Month.MonthName.
      no-months = 6.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-6.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-7:LABEL = Month.MonthName.
      no-months = 7.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-7.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-8:LABEL = Month.MonthName.
      no-months = 8.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-8.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-9:LABEL = Month.MonthName.
      no-months = 9.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-9.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-10:LABEL = Month.MonthName.
      no-months = 10.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-10.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-11:LABEL = Month.MonthName.
      no-months = 11.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-11.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-12:LABEL = Month.MonthName.
      no-months = 12.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END. ELSE HIDE fil_Month-12.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-account-summary V-table-Win 
PROCEDURE get-account-summary :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER et AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER ec AS INT  NO-UNDO.
DEF OUTPUT PARAMETER ac AS DEC  NO-UNDO.

DEF VAR key-value AS CHAR NO-UNDO.
DEF VAR wh AS WIDGET-HANDLE NO-UNDO.

  RUN get-link-handle IN adm-broker-hdl ( INPUT THIS-PROCEDURE, 'record-source':U,
                                           OUTPUT source-link-name ).
  wh = WIDGET-HANDLE(source-link-name).
  IF VALID-HANDLE(wh) THEN DO:
    RUN send-key IN WIDGET-HANDLE(source-link-name) ( source-table, OUTPUT key-value ).
    IF source-table = "AccountSummary" AND NUM-ENTRIES(key-value, "/") > 2 THEN DO:
      et = ENTRY( 1, key-value, "/").
      ec = INT( ENTRY( 2, key-value, "/")).
      ac = DEC( ENTRY( 3, key-value, "/")).
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-current-budgets V-table-Win 
PROCEDURE get-current-budgets :
/*------------------------------------------------------------------------------
  Purpose:  Get the current budget figures
------------------------------------------------------------------------------*/
DEF VAR et AS CHAR NO-UNDO.
DEF VAR ec AS INT NO-UNDO.
DEF VAR ac AS DEC NO-UNDO.

  DO WITH FRAME {&FRAME-NAME}:
    et = INPUT RP.Char2.
    ec = INPUT RP.Int1.
    ac = INPUT RP.Dec1.
    FIND FIRST Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-1 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-1.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-1. ELSE ENABLE fil_Month-1.
    END.
    ELSE HIDE fil_Month-1.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-2 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-2.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-2. ELSE ENABLE fil_Month-2.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    ELSE HIDE fil_Month-2.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-3 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-3.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-3. ELSE ENABLE fil_Month-3.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    ELSE HIDE fil_Month-3.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-4 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-4.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-4. ELSE ENABLE fil_Month-4.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    ELSE HIDE fil_Month-4.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-5 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-5.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-5. ELSE ENABLE fil_Month-5.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    ELSE HIDE fil_Month-5.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-6 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-6.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-6. ELSE ENABLE fil_Month-6.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    ELSE HIDE fil_Month-6.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-7 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-7.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-7. ELSE ENABLE fil_Month-7.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    ELSE HIDE fil_Month-7.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-8 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-8.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-8. ELSE ENABLE fil_Month-8.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    ELSE HIDE fil_Month-8.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-9 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-9.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-9. ELSE ENABLE fil_Month-9.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    ELSE HIDE fil_Month-9.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-10 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-10.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-10. ELSE ENABLE fil_Month-10.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    ELSE HIDE fil_Month-10.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-11 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-11.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-11. ELSE ENABLE fil_Month-11.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    ELSE HIDE fil_Month-11.
    IF AVAILABLE(Month) THEN DO:
      FIND AccountBalance WHERE AccountBalance.EntityType = et
                            AND AccountBalance.EntityCode = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode = Month.MonthCode NO-LOCK NO-ERROR.
      fil_Month-12 = (IF AVAILABLE(AccountBalance) THEN AccountBalance.Budget ELSE 0 ).
      DISPLAY fil_Month-12.
      IF INPUT RP.Char3 = "A" THEN DISABLE fil_Month-12. ELSE ENABLE fil_Month-12.
    END.
    ELSE HIDE fil_Month-12.
  END.
  RUN set-annual-amount.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:  Enable/disable fields appropriately
------------------------------------------------------------------------------*/
DEF VAR ok-enabled AS LOGICAL INITIAL yes NO-UNDO.

  DO WITH FRAME {&FRAME-NAME}:
    FIND FinancialYear WHERE FinancialYear.FinancialYearCode = INPUT RP.Int2 NO-LOCK NO-ERROR.
    IF AVAILABLE( FinancialYear ) THEN DO:
      fil_YearDescription = FinancialYear.Description .
      DISPLAY fil_YearDescription .
    END.
    ELSE
      ok-enabled = no.

    fil_EntityName = "" .
    CASE INPUT RP.Char2:
      WHEN "P" THEN DO:
        FIND Property WHERE Property.PropertyCode = INPUT RP.Int1 NO-LOCK NO-ERROR.
        IF AVAILABLE( Property ) THEN
          fil_EntityName = Property.Name.
        ELSE
           ok-enabled = no.
      END.
      WHEN "J" THEN DO:
        FIND Project WHERE Project.ProjectCode = INPUT RP.Int1 NO-LOCK NO-ERROR.
        IF AVAILABLE( Project ) THEN
          fil_EntityName = Project.Name.
        ELSE
           ok-enabled = no.
      END.
      WHEN "L" THEN DO:
        FIND Company WHERE Company.CompanyCode = INPUT RP.Int1 NO-LOCK NO-ERROR.
        IF AVAILABLE( Company ) THEN
          fil_EntityName = Company.LegalName.
        ELSE
           ok-enabled = no.
      END.
    END CASE.
    DISPLAY fil_EntityName.

    FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = INPUT RP.Dec1 NO-LOCK NO-ERROR.
    IF AVAILABLE(ChartOfAccount) THEN DO:
      fil_AccountName = ChartOfAccount.Name .
      DISPLAY fil_AccountName .
    END.
    ELSE
      ok-enabled = no.

    IF ok-enabled THEN
      ENABLE Btn_OK Btn_Apply.
    ELSE
      DISABLE Btn_OK Btn_Apply.

    IF ok-enabled THEN RUN get-current-budgets.

    IF INPUT RP.Char3 = "A" THEN
      ENABLE fil_AnnualBudget.
    ELSE
      DISABLE fil_AnnualBudget.

    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/

&SCOP RPT-NAME "BudgetMaintenance"
  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  DO TRANSACTION:
    FIND RP WHERE RP.ReportID = {&RPT-NAME}
                        AND RP.UserName = user-name
                        EXCLUSIVE-LOCK NO-ERROR.

    IF NOT AVAILABLE( RP ) THEN DO:
      /* Find the current Financial Year */
      FIND LAST Month WHERE Month.StartDate <= TODAY NO-LOCK NO-ERROR.
      CREATE RP.
      ASSIGN
        RP.ReportID = {&RPT-NAME}
        RP.UserName = user-name
        RP.Int2 = (IF AVAILABLE(Month) THEN Month.FinancialYearCode ELSE 0)
      .
    END.
    IF key-name = "AccountSummary" THEN DO:
      RP.Char2 = ext-et.
      RP.Int1  = ext-ec.
      RP.Dec1  = ext-ac.
    END.
    FIND CURRENT RP SHARE-LOCK.
  END.

  RUN assign-month-labels.
  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF key-name = "AccountSummary" THEN DO WITH FRAME {&FRAME-NAME}:
    RUN get-account-summary( OUTPUT ext-et, OUTPUT ext-ec, OUTPUT ext-ac ).
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Figure out which table we're using a row-available from
------------------------------------------------------------------------------*/
  IF have-records THEN RETURN.

  RUN get-link-handle IN adm-broker-hdl ( INPUT THIS-PROCEDURE, 'record-source':U,
                                           OUTPUT source-link-name ).
  IF NOT VALID-HANDLE(WIDGET-HANDLE(source-link-name)) THEN RETURN.

  RUN get-attribute IN WIDGET-HANDLE(source-link-name) ('internal-tables':U ).
  source-table = RETURN-VALUE.

  IF INDEX( source-table, "AccountSummary" ) > 0 THEN  source-table = "AccountSummary".
  ELSE IF INDEX( source-table, "AccountBalance" ) > 0 THEN  source-table = "AccountBalance".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "EntityType" "AccountSummary" "EntityType"}
  {src/adm/template/sndkycas.i "EntityCode" "AccountSummary" "EntityCode"}
  {src/adm/template/sndkycas.i "AccountCode" "AccountSummary" "AccountCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-annual-amount V-table-Win 
PROCEDURE set-annual-amount :
/*------------------------------------------------------------------------------
  Purpose:  Total the monthly fields to come up with an annual figure
------------------------------------------------------------------------------*/
DEF VAR annual-total AS DECIMAL INITIAL 0 NO-UNDO.

  DO WITH FRAME {&FRAME-NAME}:
    FIND FIRST Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-1.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-2.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-3.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-4.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-5.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-6.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-7.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-8.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-9.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-10.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-11.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    IF AVAILABLE(Month) THEN DO:
      annual-total = annual-total + INPUT fil_Month-12.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK NO-ERROR.
    END.
    fil_AnnualBudget = annual-total.
    DISPLAY fil_AnnualBudget.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-monthly-amounts V-table-Win 
PROCEDURE set-monthly-amounts :
/*------------------------------------------------------------------------------
  Purpose:  Set each month to 1/2th of the annual figure
------------------------------------------------------------------------------*/
DEF VAR one-month AS DECIMAL NO-UNDO.

  DO WITH FRAME {&FRAME-NAME}:
    one-month = ROUND( (INPUT fil_AnnualBudget ) / no-months, 2).

    FIND FIRST Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-1 = one-month.
      DISPLAY fil_Month-1.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    END.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-2 = one-month.
      DISPLAY fil_Month-2.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    END.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-3 = one-month.
      DISPLAY fil_Month-3.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    END.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-4 = one-month.
      DISPLAY fil_Month-4.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    END.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-5 = one-month.
      DISPLAY fil_Month-5.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    END.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-6 = one-month.
      DISPLAY fil_Month-6.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    END.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-7 = one-month.
      DISPLAY fil_Month-7.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    END.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-8 = one-month.
      DISPLAY fil_Month-8.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    END.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-9 = one-month.
      DISPLAY fil_Month-9.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    END.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-10 = one-month.
      DISPLAY fil_Month-10.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    END.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-11 = one-month.
      DISPLAY fil_Month-11.
      FIND NEXT Month WHERE Month.FinancialYearCode = RP.Int2 NO-LOCK.
    END.
    IF AVAILABLE(Month) THEN DO:
      fil_Month-12 = (INPUT fil_AnnualBudget ) - (one-month * 11).
      DISPLAY fil_Month-12.
    END.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name V-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-name AS CHAR NO-UNDO.
  key-name = new-name.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value V-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-value AS CHAR NO-UNDO.
  key-value = new-value.
  IF key-name = "EntityType" THEN DO:
  END.
  ELSE IF key-name = "EntityCode" THEN DO:
  END.
  ELSE IF key-name = "AccountCode" THEN DO:
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

