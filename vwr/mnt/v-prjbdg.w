&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR mode AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES ProjectBudget
&Scoped-define FIRST-EXTERNAL-TABLE ProjectBudget


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR ProjectBudget.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS ProjectBudget.AccountCode ~
ProjectBudget.Description ProjectBudget.OriginalBudget ~
ProjectBudget.EntityType ProjectBudget.EntityCode ~
ProjectBudget.EntityAccount ProjectBudget.AllowPosting ~
ProjectBudget.HighVolume 
&Scoped-define ENABLED-TABLES ProjectBudget
&Scoped-define FIRST-ENABLED-TABLE ProjectBudget
&Scoped-define DISPLAYED-TABLES ProjectBudget
&Scoped-define FIRST-DISPLAYED-TABLE ProjectBudget
&Scoped-Define ENABLED-OBJECTS cmb_Category RECT-2 
&Scoped-Define DISPLAYED-FIELDS ProjectBudget.AccountCode ~
ProjectBudget.Description ProjectBudget.OriginalBudget ~
ProjectBudget.AgreedVariation ProjectBudget.Adjustment ~
ProjectBudget.EntityType ProjectBudget.EntityCode ~
ProjectBudget.EntityAccount ProjectBudget.UncommittedBudget ~
ProjectBudget.CommittedBudget ProjectBudget.AllowPosting ~
ProjectBudget.HighVolume 
&Scoped-Define DISPLAYED-OBJECTS cmb_Category fil_Entity fil_EAccount 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_Category AS CHARACTER FORMAT "X(256)":U 
     LABEL "Expense Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 48.43 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_EAccount AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 48.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Entity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 48.57 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 75 BY 11.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     ProjectBudget.AccountCode AT ROW 1.5 COL 12.14 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9.14 BY 1
     ProjectBudget.Description AT ROW 1.5 COL 24 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 48.43 BY 1
     cmb_Category AT ROW 2.6 COL 24.14 COLON-ALIGNED
     ProjectBudget.OriginalBudget AT ROW 6.5 COL 24.14 COLON-ALIGNED
          LABEL "Original Budget" FORMAT "->>>,>>>,>>9.99"
          VIEW-AS FILL-IN 
          SIZE 16.57 BY 1
     ProjectBudget.AgreedVariation AT ROW 7.5 COL 24.14 COLON-ALIGNED
          LABEL "Agreed Variation" FORMAT "->>>,>>>,>>9.99"
          VIEW-AS FILL-IN 
          SIZE 16.57 BY 1
          BGCOLOR 16 
     ProjectBudget.Adjustment AT ROW 7.5 COL 55.14 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 16.43 BY 1
          BGCOLOR 16 
     ProjectBudget.EntityType AT ROW 4 COL 12.14 COLON-ALIGNED
          LABEL "Entity"
          VIEW-AS FILL-IN 
          SIZE 2.43 BY 1
     ProjectBudget.EntityCode AT ROW 4 COL 14.43 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 6.72 BY 1
     fil_Entity AT ROW 4 COL 24.14 COLON-ALIGNED NO-LABEL
     ProjectBudget.EntityAccount AT ROW 5 COL 12.14 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 9.14 BY 1
     fil_EAccount AT ROW 5 COL 24.14 COLON-ALIGNED NO-LABEL
     ProjectBudget.UncommittedBudget AT ROW 10 COL 24.14 COLON-ALIGNED
          LABEL "Uncommitted Budget"
          VIEW-AS FILL-IN 
          SIZE 16.43 BY .9
          BGCOLOR 16 
     ProjectBudget.CommittedBudget AT ROW 9 COL 24.14 COLON-ALIGNED
          LABEL "Committed Budget" FORMAT "->>>,>>>,>>9.99"
          VIEW-AS FILL-IN 
          SIZE 16.57 BY 1
          BGCOLOR 16 
     ProjectBudget.AllowPosting AT ROW 9 COL 51.14
          LABEL "Allow direct posting"
          VIEW-AS TOGGLE-BOX
          SIZE 15.72 BY 1
     ProjectBudget.HighVolume AT ROW 10 COL 51.14
          VIEW-AS TOGGLE-BOX
          SIZE 11.72 BY 1
     RECT-2 AT ROW 1 COL 1
     "Entity Account:" VIEW-AS TEXT
          SIZE 10.86 BY 1 AT ROW 4 COL 3
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.ProjectBudget
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.25
         WIDTH              = 75.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN ProjectBudget.Adjustment IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN ProjectBudget.AgreedVariation IN FRAME F-Main
   NO-ENABLE EXP-LABEL EXP-FORMAT                                       */
/* SETTINGS FOR TOGGLE-BOX ProjectBudget.AllowPosting IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN ProjectBudget.CommittedBudget IN FRAME F-Main
   NO-ENABLE EXP-LABEL EXP-FORMAT                                       */
/* SETTINGS FOR FILL-IN ProjectBudget.Description IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN ProjectBudget.EntityAccount IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN ProjectBudget.EntityCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN ProjectBudget.EntityType IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_EAccount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Entity IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN ProjectBudget.OriginalBudget IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN ProjectBudget.UncommittedBudget IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME ProjectBudget.AccountCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ProjectBudget.AccountCode V-table-Win
ON LEAVE OF ProjectBudget.AccountCode IN FRAME F-Main /* Account */
DO:
  /* If the given account does not exist then allow the user
     to specify a description */
     
  IF NOT SELF:MODIFIED THEN RETURN.

  IF CAN-FIND( ChartOfAccount WHERE
    ChartOfAccount.AccountCode = INPUT {&SELF-NAME} ) THEN
  DO:
    {inc/selcde/cdcoa.i "ProjectBudget.Description"}
  END.
  ELSE
  DO:
    BELL.
    ProjectBudget.Description:SCREEN-VALUE = "".
    SELF:MODIFIED = No.
    IF LAST-EVENT:LABEL = "TAB" THEN
    DO:
      APPLY 'ENTRY':U TO ProjectBudget.Description.
      RETURN NO-APPLY.
    END.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Category
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Category V-table-Win
ON U1 OF cmb_Category IN FRAME F-Main /* Expense Category */
DO:
  {inc/selcmb/scpxct1.i "ProjectBudget" "ProjectExpenseCategory"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Category V-table-Win
ON U2 OF cmb_Category IN FRAME F-Main /* Expense Category */
DO:
  {inc/selcmb/scpxct2.i "ProjectBudget" "ProjectExpenseCategory"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ProjectBudget.Description
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ProjectBudget.Description V-table-Win
ON U1 OF ProjectBudget.Description IN FRAME F-Main /* Description */
DO:
  {inc/selfil/sfcoa1.i "ProjectBudget" "AccountCode"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ProjectBudget.Description V-table-Win
ON U2 OF ProjectBudget.Description IN FRAME F-Main /* Description */
DO:
  {inc/selfil/sfcoa2.i "ProjectBudget" "AccountCode"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ProjectBudget.Description V-table-Win
ON U3 OF ProjectBudget.Description IN FRAME F-Main /* Description */
DO:
  {inc/selfil/sfcoa3.i "ProjectBudget" "AccountCode"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ProjectBudget.EntityAccount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ProjectBudget.EntityAccount V-table-Win
ON LEAVE OF ProjectBudget.EntityAccount IN FRAME F-Main /* Account */
DO:
IF {&SELF-NAME}:MODIFIED THEN DO:

  DEF VAR old-val  AS CHAR NO-UNDO.
  DEF VAR curr-val AS CHAR NO-UNDO.

  DEF BUFFER tmp_ChartOfAccount FOR ChartOfAccount.

  old-val  = ENTRY( 2, {&SELF-NAME}:PRIVATE-DATA ).
  curr-val = {&SELF-NAME}:SCREEN-VALUE.

  IF old-val <> curr-val THEN DO WITH FRAME {&FRAME-NAME}:

    DEF BUFFER tmp_ProjectBudget FOR ProjectBudget.
    IF (INPUT ProjectBudget.EntityType) = "J" AND CAN-FIND(ProjectBudget WHERE ProjectBudget.ProjectCode = (INPUT ProjectBudget.EntityCode)
                                    AND ProjectBudget.AccountCode = INPUT {&SELF-NAME} )
    THEN DO:
      FIND FIRST tmp_ProjectBudget NO-LOCK WHERE tmp_ProjectBudget.ProjectCode = (INPUT ProjectBudget.EntityCode)
                                    AND tmp_ProjectBudget.AccountCode = INPUT {&SELF-NAME} NO-ERROR.
      IF AVAILABLE(tmp_ProjectBudget) THEN DO:
        fil_EAccount = tmp_ProjectBudget.Description.
        DISPLAY fil_EAccount.
      END.
    END.
    ELSE
      FIND FIRST tmp_ChartOfAccount WHERE tmp_ChartOfAccount.AccountCode = INPUT {&SELF-NAME}
                  NO-LOCK NO-ERROR.

    IF NOT AVAILABLE( tmp_ChartOfAccount )
          AND NOT AVAILABLE( tmp_ProjectBudget )
    THEN DO:
      DEF VAR old-id   AS ROWID NO-UNDO.
      old-id   = TO-ROWID( ENTRY( 1, {&SELF-NAME}:PRIVATE-DATA ) ).

      MESSAGE "There is no ChartOfAccount or ProjectBudget available with code " INPUT {&SELF-NAME}
                VIEW-AS ALERT-BOX ERROR.

      FIND FIRST tmp_ChartOfAccount WHERE ROWID( tmp_ChartOfAccount ) = old-id NO-LOCK NO-ERROR.

      IF AVAILABLE tmp_ChartOfAccount THEN
        DISPLAY tmp_ChartOfAccount.AccountCode @ {&SELF-NAME}.
      ELSE
        DISPLAY {&SELF-NAME}.    

    END.
    ELSE DO:

      DEF VAR spd AS CHAR NO-UNDO.
      spd = {&SELF-NAME}:PRIVATE-DATA.    
      ENTRY( 1, spd ) = STRING( ROWID( tmp_ChartOfAccount ) ).
      ENTRY( 2, spd ) = {&SELF-NAME}:SCREEN-VALUE.
      {&SELF-NAME}:PRIVATE-DATA = spd.
    
      fil_EAccount:PRIVATE-DATA    = STRING( ROWID( tmp_ChartOfAccount ) ).

      DEF VAR enabled AS LOGI NO-UNDO.
      enabled = fil_EAccount:SENSITIVE.

      fil_EAccount:SENSITIVE = TRUE.
      APPLY 'U2':U TO fil_EAccount.
      fil_EAccount:SENSITIVE = enabled.

      /* Tab past the select button for valid codes if tab was pressed*/
      IF LOOKUP( LAST-EVENT:LABEL, 'TAB,ENTER':U ) <> 0 THEN DO:
        DEF VAR next-tab AS HANDLE NO-UNDO.
      
        next-tab = {&SELF-NAME}:NEXT-TAB-ITEM.
        DO WHILE NOT next-tab:SENSITIVE:
          next-tab = next-tab:NEXT-TAB-ITEM.
        END.
      
        IF VALID-HANDLE( next-tab ) AND next-tab:TYPE = 'BUTTON':U
                 AND next-tab:DYNAMIC THEN
        DO: 
          APPLY 'TAB':U TO next-tab.
          RETURN NO-APPLY.
        END.
      END.
    END.
  END.

END. /* Of check if modified */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ProjectBudget.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ProjectBudget.EntityCode V-table-Win
ON LEAVE OF ProjectBudget.EntityCode IN FRAME F-Main /* Code */
DO:
  CASE ProjectBudget.EntityType:SCREEN-VALUE:
    WHEN 'P' THEN DO: {inc/selcde/cdpro.i "fil_Entity"} END.
    WHEN 'L' THEN DO: {inc/selcde/cdcmp.i "fil_Entity"} END.
    WHEN 'J' THEN DO: {inc/selcde/cdprj.i "fil_Entity"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ProjectBudget.EntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ProjectBudget.EntityType V-table-Win
ON ANY-PRINTABLE OF ProjectBudget.EntityType IN FRAME F-Main /* Entity */
DO:
  APPLY CAPS( CHR( LASTKEY ) ) TO SELF.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ProjectBudget.EntityType V-table-Win
ON LEAVE OF ProjectBudget.EntityType IN FRAME F-Main /* Entity */
DO:
  IF SELF:MODIFIED THEN
  DO:
    RUN verify-type.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.  
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_EAccount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_EAccount V-table-Win
ON U1 OF fil_EAccount IN FRAME F-Main
DO:
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT ProjectBudget.EntityType = "J" THEN DO:
    {inc/selfil/sfpbj1.i "ProjectBudget" "EntityAccount"}  
  END.
  ELSE DO:
    {inc/selfil/sfcoa1.i "ProjectBudget" "EntityAccount"}
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_EAccount V-table-Win
ON U2 OF fil_EAccount IN FRAME F-Main
DO:
  IF INPUT ProjectBudget.EntityType = "J" THEN DO:
    {inc/selfil/sfpbj2.i "ProjectBudget" "EntityAccount"}  
  END.
  ELSE DO:
    {inc/selfil/sfcoa2.i "ProjectBudget" "EntityAccount"}  
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_EAccount V-table-Win
ON U3 OF fil_EAccount IN FRAME F-Main
DO:
  IF INPUT ProjectBudget.EntityType = "J" THEN DO:
    {inc/selfil/sfpbj3.i "ProjectBudget" "EntityAccount"}  
  END.
  ELSE DO:
    {inc/selfil/sfcoa3.i "ProjectBudget" "EntityAccount"}
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Entity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Entity V-table-Win
ON U1 OF fil_Entity IN FRAME F-Main
DO:
  CASE ProjectBudget.EntityType:SCREEN-VALUE:
    WHEN 'P' THEN DO: {inc/selfil/sfpro1.i "ProjectBudget" "EntityCode"} END.
    WHEN 'L' THEN DO: {inc/selfil/sfcmp1.i "ProjectBudget" "EntityCode"} END.
    WHEN 'J' THEN DO: {inc/selfil/sfprj1.i "ProjectBudget" "EntityCode"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Entity V-table-Win
ON U2 OF fil_Entity IN FRAME F-Main
DO:
  CASE ProjectBudget.EntityType:SCREEN-VALUE:
    WHEN 'P' THEN DO: {inc/selfil/sfpro2.i "ProjectBudget" "EntityCode"} END.
    WHEN 'L' THEN DO: {inc/selfil/sfcmp2.i "ProjectBudget" "EntityCode"} END.
    WHEN 'J' THEN DO: {inc/selfil/sfprj1.i "ProjectBudget" "EntityCode"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "ProjectBudget"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "ProjectBudget"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-budget. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-budget.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  RUN dispatch( 'update-record':U ).
  IF mode = "Add" THEN RUN notify( 'open-query, RECORD-SORUCE':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-budget V-table-Win 
PROCEDURE delete-budget :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT ProjectBudget EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE ProjectBudget THEN DELETE ProjectBudget.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-project V-table-Win 
PROCEDURE get-project :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR c-recsrc   AS CHAR NO-UNDO.
  DEF VAR rowid-list AS CHAR NO-UNDO.

  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT c-recsrc ).
  RUN send-records IN WIDGET-HANDLE( c-recsrc ) ( "Project", OUTPUT rowid-list ).
  FIND Project WHERE ROWID( Project ) = TO-ROWID( rowid-list ) NO-LOCK NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN update-entity-button.
  RUN update-account-button.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR default-budget AS LOGI NO-UNDO.
  
  /* Check to see if this is the default budget */
  FIND Project WHERE Project.ProjectCode = ProjectBudget.ProjectCode
    NO-LOCK NO-ERROR.
  default-budget = Project.EntityAccount = ProjectBudget.AccountCode.

/*  ProjectBudget.AccountCode:SENSITIVE   = mode = "Add".
  ProjectBudget.EntityType:SENSITIVE    = mode = "Add".
  ProjectBudget.EntityCode:SENSITIVE    = mode = "Add".
  ProjectBudget.EntityAccount:SENSITIVE = mode = "Add". */
  
/*  RUN set-link-attributes IN sys-mgr(
    THIS-PROCEDURE,
    STRING( ProjectBudget.Description:HANDLE ),
    "HIDDEN = " + IF mode = "Add" THEN "No" ELSE "Yes" ). */
  
/*  RUN set-link-attributes IN sys-mgr(
    THIS-PROCEDURE,
    STRING( fil_Entity:HANDLE ),
    "HIDDEN = " + IF mode = "Add" THEN "No" ELSE "Yes" ). */
  
/*  RUN set-link-attributes IN sys-mgr(
    THIS-PROCEDURE,
    STRING( fil_EAccount:HANDLE ),
    "HIDDEN = " + IF mode = "Add" THEN "No" ELSE "Yes" ). */

  ProjectBudget.Adjustment:SENSITIVE = NOT default-budget.
  
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR can-edit AS LOGI NO-UNDO.
  
  can-edit = AVAILABLE Project AND NOT Project.BudgetsFrozen.
  
  IF mode = "Add" THEN
  DO:
    IF can-edit THEN
    DO:
      have-records = Yes.
      RUN dispatch( 'add-record':U ).
    END.
    ELSE
    DO:
      MESSAGE
        "All budget changes have been frozen for this project!" SKIP
        "The add has been cancelled."
        VIEW-AS ALERT-BOX ERROR TITLE "Add Error".
      RUN set-attribute-list ( 'mode = Maintain' ).
      RUN dispatch( 'disable-fields':U ).
    END.
  END.
  ELSE RUN dispatch( IF can-edit THEN 'enable-fields' ELSE 'disable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR account-default AS DEC NO-UNDO INITIAL 1.

  IF NOT AVAILABLE Project THEN
  DO:
    RUN dispatch( 'disable-fields':U ).
    RETURN.
  END.

  FIND LAST ProjectBudget OF Project WHERE ProjectBudget.AccountCode < 100 NO-LOCK NO-ERROR.
  IF AVAILABLE(ProjectBudget) THEN account-default = INT(ProjectBudget.AccountCode) + 1.
  

  CREATE ProjectBudget.
  ASSIGN
    ProjectBudget.ProjectCode   = Project.ProjectCode
    ProjectBudget.AccountCode   = account-default
    ProjectBudget.EntityType    = Project.EntityType
    ProjectBudget.EntityCode    = Project.EntityCode
    ProjectBudget.EntityAccount = Project.EntityAccount.
  
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'display-fields':U ).
  
  CURRENT-WINDOW:TITLE = "Adding a new Budget to Project " + STRING( Project.ProjectCode ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE":U THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize V-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN get-project.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ProjectBudget"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE type-changed V-table-Win 
PROCEDURE type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  /* Reset the entity */
  ProjectBudget.EntityCode:PRIVATE-DATA = ",".
  ProjectBudget.EntityCode:SCREEN-VALUE = STRING( 0 ).
  ProjectBudget.EntityCode:MODIFIED = No.
  fil_Entity:PRIVATE-DATA = "".
  fil_Entity:SCREEN-VALUE = "".
  
  /* Reset the entity account */
  ProjectBudget.EntityAccount:PRIVATE-DATA = ",".
  ProjectBudget.EntityAccount:SCREEN-VALUE = STRING( 0.00 ).
  ProjectBudget.EntityAccount:MODIFIED = No.
  fil_EAccount:PRIVATE-DATA = "".
  fil_EAccount:SCREEN-VALUE = "".
  
  RUN update-entity-button.
  RUN update-account-button.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-account-button V-table-Win 
PROCEDURE update-account-button :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:
  
    DEF VAR account-group AS CHAR NO-UNDO.
    
    CASE INPUT ProjectBudget.EntityType:
      WHEN 'P' THEN account-group = 'PROPEX'.
      WHEN 'L' THEN account-group = 'ADMIN '.
      WHEN 'J' THEN account-group = 'ADMIN '.
    END CASE.

    IF account-group <> "" THEN 
    DO:
      FIND AccountGroup WHERE AccountGroup.AccountGroupCode = account-group
        NO-LOCK NO-ERROR.
      IF NOT AVAILABLE AccountGroup THEN RETURN.
      
      RUN set-link-attributes IN sys-mgr (
        THIS-PROCEDURE,
        STRING( fil_EAccount:HANDLE ),
        "FUNCTION = " +
        "FilterBy-Case = " + account-group + " - " + AccountGroup.Name ).
    END.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-button V-table-Win 
PROCEDURE update-entity-button :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:
  
    DEF VAR node-name     AS CHAR NO-UNDO.
    DEF VAR vwr-name      AS CHAR NO-UNDO.
    DEF VAR filter-panel  AS CHAR NO-UNDO.
    DEF VAR sort-panel    AS CHAR NO-UNDO.
    
    FIND EntityType WHERE EntityType.EntityType = INPUT ProjectBudget.EntityType
      NO-LOCK NO-ERROR.
    ProjectBudget.EntityType:LABEL = IF AVAILABLE EntityType THEN
      EntityType.Description ELSE "Entity".
    
    CASE INPUT ProjectBudget.EntityType:
      WHEN 'P' THEN ASSIGN
        node-name = 'w-selpro' vwr-name = 'b-selpro'
        filter-panel = "Yes" sort-panel = "Yes".
      WHEN 'L' THEN ASSIGN
        node-name = 'w-selcmp' vwr-name = 'b-selcmp'
        filter-panel = "No" sort-panel = "Yes".
      WHEN 'J' THEN ASSIGN
        node-name = 'w-selprj' vwr-name = 'b-selprj'
        filter-panel = "No" sort-panel = "Yes".
    END CASE.

    RUN set-link-attributes IN sys-mgr(
      THIS-PROCEDURE,
      STRING( fil_Entity:HANDLE ),
      "TARGET = " + node-name + "," +
      "VIEWER = " + vwr-name  + "," +
      "FILTER-PANEL = " + filter-panel + "," +
      "SORT-PANEL = "   + sort-panel ).
          
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-budget V-table-Win 
PROCEDURE verify-budget :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF BUFFER OtherBudget FOR ProjectBudget.

  IF NOT ((INPUT ProjectBudget.EntityType = "J") AND CAN-FIND( OtherBudget WHERE OtherBudget.ProjectCode = INPUT ProjectBudget.EntityCode
                                                AND OtherBudget.AccountCode = INPUT ProjectBudget.EntityAccount))
     AND NOT ((INPUT ProjectBudget.EntityType <> "J") AND CAN-FIND(ChartOfAccount WHERE ChartOfAccount.AccountCode = INPUT ProjectBudget.EntityAccount))
  THEN DO:
    MESSAGE "You must enter an account for this project"
      VIEW-AS ALERT-BOX ERROR TITLE "No Account Selected".
    APPLY 'ENTRY':U TO ProjectBudget.EntityAccount.
    RETURN "FAIL".
  END.

  IF CAN-FIND( OtherBudget WHERE
    OtherBudget.ProjectCode = Project.ProjectCode AND
    OtherBudget.AccountCode = INPUT ProjectBudget.AccountCode AND
    ROWID( OtherBudget ) <> ROWID( ProjectBudget ) ) THEN
  DO:
    MESSAGE
      "Another budget already exists for project " + STRING( Project.ProjectCode ) SKIP
      "and account" ProjectBudget.AccountCode:SCREEN-VALUE + "." SKIP(1)
      "Please change the account code."
      VIEW-AS ALERT-BOX ERROR TITLE "Duplicate budget".
    APPLY 'ENTRY':U TO ProjectBudget.AccountCode.
    RETURN "FAIL".
  END.
  
  IF fil_Entity:SCREEN-VALUE = "" THEN
  DO:
    MESSAGE
      "You must select a" ProjectBudget.EntityType:LABEL "for this" SKIP
      "Project Budget"
      VIEW-AS ALERT-BOX ERROR TITLE "No " + ProjectBudget.EntityType:LABEL + " selected".
    APPLY 'ENTRY' TO ProjectBudget.EntityType.
    RETURN "FAIL".
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-type V-table-Win 
PROCEDURE verify-type :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF LOOKUP( INPUT ProjectBudget.EntityType, "P,L,J" ) = 0 THEN
    RETURN "FAIL".
  RUN type-changed.
  ProjectBudget.EntityType:MODIFIED = No.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

