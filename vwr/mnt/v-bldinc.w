&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

&SCOPED-DEFINE REPORT-ID "Building Income Statement"

DEF VAR manager-list        AS CHAR NO-UNDO.
DEF VAR manager-pd          AS CHAR NO-UNDO.
DEF VAR administrator-list  AS CHAR NO-UNDO.
DEF VAR administrator-pd    AS CHAR NO-UNDO.
DEF VAR region-list         AS CHAR NO-UNDO.
DEF VAR region-pd           AS CHAR NO-UNDO.

DEF VAR consolidation-list  AS CHAR NO-UNDO.
DEF VAR consolidation-pd    AS CHAR NO-UNDO.
DEF VAR entlist-list        AS CHAR NO-UNDO.
DEF VAR entlist-pd          AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Char2 RP.Int1 RP.Int2 RP.Char3 ~
RP.Log4 RP.Char4 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_EntityList cmb_Select cmb_month ~
cmb_month2 btn_Browse btn_print RECT-30 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Char2 RP.Int1 RP.Int2 RP.Char3 ~
RP.Log4 RP.Char4 
&Scoped-Define DISPLAYED-OBJECTS cmb_EntityList fil_PropertyFrom cmb_Select ~
fil_PropertyTo cmb_month cmb_month2 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Browse 
     LABEL "Browse" 
     SIZE 7 BY 1.

DEFINE BUTTON btn_print 
     LABEL "&OK" 
     SIZE 9 BY 1
     FONT 9.

DEFINE VARIABLE cmb_EntityList AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 45 BY 1.05 NO-UNDO.

DEFINE VARIABLE cmb_month AS CHARACTER FORMAT "X(256)":U 
     LABEL "Month Ending" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 14.86 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_month2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 14.86 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Select AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 45.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_PropertyFrom AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_PropertyTo AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45.14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-30
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 60 BY 15.2.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.9 COL 3.29 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Property Range", "RANGE":U,
"Building Manager", "MANAGER":U,
"Consolidation List", "LIST":U,
"Region", "REGION":U,
"Entity List", "ELIST":U
          SIZE 15 BY 4.3
     RP.Char2 AT ROW 2.6 COL 24.86 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Detailed - each Property", "Detailed":U,
"Consolidated - By Account", "Account":U,
"Consolidated - By Property", "Property":U,
"each Property with Account Groups", "Groups":U
          SIZE 27.86 BY 3.1
     cmb_EntityList AT ROW 6.25 COL 14 NO-LABEL
     RP.Int1 AT ROW 6.5 COL 4.43 COLON-ALIGNED
          LABEL "From" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_PropertyFrom AT ROW 6.5 COL 13.57 COLON-ALIGNED NO-LABEL
     cmb_Select AT ROW 6.8 COL 11.86 COLON-ALIGNED NO-LABEL
     RP.Int2 AT ROW 7.5 COL 4.43 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_PropertyTo AT ROW 7.5 COL 13.57 COLON-ALIGNED NO-LABEL
     cmb_month AT ROW 10.2 COL 25
     RP.Char3 AT ROW 10.6 COL 2.72 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Normal", "Normal":U,
"For a range of months", "RANGE":U
          SIZE 18.29 BY 1.6
     cmb_month2 AT ROW 11.2 COL 32.57
     RP.Log4 AT ROW 13.2 COL 2.72 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "File", ?,
"Printer", no,
"Preview", yes
          SIZE 8.57 BY 2.4
     RP.Char4 AT ROW 13.2 COL 9.29 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 41.14 BY 1.05
     btn_Browse AT ROW 13.2 COL 53
     btn_print AT ROW 15 COL 51.29
     RECT-30 AT ROW 1 COL 1
     "Reporting Type:" VIEW-AS TEXT
          SIZE 11.43 BY 1 AT ROW 9.6 COL 2.14
     "Report Style:" VIEW-AS TEXT
          SIZE 9 BY 1 AT ROW 1.6 COL 23.86
     "Report on a:" VIEW-AS TEXT
          SIZE 11 BY .8 AT ROW 1.1 COL 2.14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.9
         WIDTH              = 67.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char4 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR COMBO-BOX cmb_EntityList IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_month IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_month2 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fil_PropertyFrom IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_PropertyTo IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR RADIO-SET RP.Log4 IN FRAME F-Main
   EXP-HELP                                                             */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Browse V-table-Win
ON CHOOSE OF btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN run-report.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN property-selection-changed.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON VALUE-CHANGED OF RP.Char2 IN FRAME F-Main /* Char2 */
DO:
  RUN report-style-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN report-type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_EntityList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_EntityList V-table-Win
ON U1 OF cmb_EntityList IN FRAME F-Main
DO:
  {inc/selcmb/scels1.i "RP" "Char5"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_EntityList V-table-Win
ON U2 OF cmb_EntityList IN FRAME F-Main
DO:
  {inc/selcmb/scels2.i "RP" "Char5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month V-table-Win
ON U1 OF cmb_month IN FRAME F-Main /* Month Ending */
DO:
  CASE INPUT RP.Char3:
    WHEN "NORMAL" THEN {inc/selcmb/scmthe1.i "RP" "Int3"}    
    WHEN "RANGE"  THEN {inc/selcmb/scmths1.i "RP" "Int3"}    
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month V-table-Win
ON U2 OF cmb_month IN FRAME F-Main /* Month Ending */
DO:
  CASE INPUT RP.Char3:
    WHEN "NORMAL" THEN {inc/selcmb/scmthe2.i "RP" "Int3"}    
    WHEN "RANGE"  THEN {inc/selcmb/scmths2.i "RP" "Int3"}    
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U1 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int4"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U2 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int4"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_PropertyFrom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyFrom V-table-Win
ON U1 OF fil_PropertyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int1"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyFrom V-table-Win
ON U2 OF fil_PropertyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int1"}
  ASSIGN RP.int2:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING( RP.Int1 ).
  APPLY 'LEAVE':U TO RP.Int2 IN FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyFrom V-table-Win
ON U3 OF fil_PropertyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int1"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_PropertyTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyTo V-table-Win
ON U1 OF fil_PropertyTo IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int2"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyTo V-table-Win
ON U2 OF fil_PropertyTo IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int2"}      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyTo V-table-Win
ON U3 OF fil_PropertyTo IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int2"}      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* From */
DO:
  {inc/selcde/cdpro.i "fil_PropertyFrom"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* To */
DO:
  {inc/selcde/cdpro.i "fil_PropertyTo"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Log4 */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-administrators V-table-Win 
PROCEDURE get-administrators :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF administrator-list <> "" THEN RETURN.
  
  FOR EACH Property WHERE Property.Active NO-LOCK
    BREAK BY Property.Administrator:
    IF FIRST-OF( Property.Administrator ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Administrator
        NO-LOCK NO-ERROR.

      administrator-list = administrator-list + IF administrator-list = "" THEN "" ELSE ",".
      administrator-list = administrator-list +
        ( IF AVAILABLE Person THEN Person.FirstName + ' ' + Person.LastName ELSE "Not Managed" ).

      administrator-pd = administrator-pd + IF administrator-pd = "" THEN "" ELSE ",".
      administrator-pd = administrator-pd + IF AVAILABLE Person THEN STRING( Person.PersonCode ) ELSE STRING( Property.Administrator ).
    END.
  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-consolidation V-table-Win 
PROCEDURE get-consolidation :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF consolidation-list <> "" THEN RETURN.
  
  FOR EACH ConsolidationList NO-LOCK:

    consolidation-list = consolidation-list + IF consolidation-list = "" THEN "" ELSE ",".
    consolidation-list = consolidation-list + ConsolidationList.Description.

    consolidation-pd = consolidation-pd + IF consolidation-pd = "" THEN "" ELSE ",".
    consolidation-pd = consolidation-pd + ConsolidationList.Name.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-entlist V-table-Win 
PROCEDURE get-entlist :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF entlist-list <> "" THEN RETURN.
  
  FOR EACH EntityList WHERE EntityList.ListType='P' NO-LOCK:

    entlist-list = entlist-list + IF entlist-list = "" THEN "" ELSE ",".
    entlist-list = entlist-list + EntityList.Description.

    entlist-pd = entlist-pd + IF entlist-pd = "" THEN "" ELSE ",".
    entlist-pd = entlist-pd + EntityList.ListCode.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-managers V-table-Win 
PROCEDURE get-managers :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF manager-list <> "" THEN RETURN.
  
  FOR EACH Property WHERE Property.Active NO-LOCK
    BREAK BY Property.Manager:
    IF FIRST-OF( Property.Manager ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Manager
        NO-LOCK NO-ERROR.

      manager-list = manager-list + IF manager-list = "" THEN "" ELSE ",".
      manager-list = manager-list +
        (IF AVAILABLE Person THEN
            Person.FirstName + " " + Person.LastName + " - " + Person.JobTitle
         ELSE
            (IF Property.Manager > 0 THEN
               "Deleted Person " + STRING(Property.Manager)
             ELSE
               "No Building Manager Assigned") ).

      manager-pd = manager-pd + IF manager-pd = "" THEN "" ELSE ",".
      manager-pd = manager-pd + IF AVAILABLE Person THEN STRING( Person.PersonCode ) ELSE STRING( Property.Manager ).
    END.
  
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-regions V-table-Win 
PROCEDURE get-regions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF region-list <> "" THEN RETURN.
  
  FOR EACH Region NO-LOCK:

    region-list = region-list + IF region-list = "" THEN "" ELSE ",".
    region-list = region-list + Region.Name.

    region-pd = region-pd + IF region-pd = "" THEN "" ELSE ",".
    region-pd = region-pd + Region.Region.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN property-selection-changed.
  RUN report-type-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:  Enable/disable filename for spreadsheet output
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Log4 = ? THEN
    VIEW RP.Char4 btn_Browse .
  ELSE
    HIDE RP.Char4 btn_Browse .
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR user-name AS CHAR NO-UNDO.

  RUN get-username IN sys-mgr( OUTPUT user-name ).
  
  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.
    
  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
  
    DEF VAR first-property LIKE Property.PropertyCode NO-UNDO.
    DEF VAR last-property  LIKE Property.PropertyCode NO-UNDO.
    
    FIND FIRST Property NO-LOCK. first-property = Property.PropertyCode.
    FIND LAST  Property NO-LOCK. last-property  = Property.PropertyCode.

    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Int1     = first-property
      RP.Int2     = last-property.
  END.

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE property-selection-changed V-table-Win 
PROCEDURE property-selection-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT RP.Char1.
  
  CASE type:
    WHEN "RANGE" THEN.
    WHEN "ELIST" THEN.
    WHEN "MANAGER" THEN
    DO:
      RUN get-managers.
      cmb_Select:LIST-ITEMS   = manager-list.
      cmb_Select:PRIVATE-DATA = manager-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, manager-list ).
    END.
    WHEN "ADMINISTRATOR" THEN
    DO:
      RUN get-administrators.
      cmb_Select:LIST-ITEMS   = administrator-list.
      cmb_Select:PRIVATE-DATA = administrator-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, administrator-list ).
    END.
    WHEN "LIST" THEN DO:
      RUN get-consolidation.
      cmb_Select:LIST-ITEMS   = consolidation-list.
      cmb_Select:PRIVATE-DATA = consolidation-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, consolidation-list ).
    END.
    WHEN "REGION" THEN
    DO:
      RUN get-regions.
      cmb_Select:LIST-ITEMS   = region-list.
      cmb_Select:PRIVATE-DATA = region-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, region-list ).
    END.
  
  END CASE.

  /* Enable fields as appropriate */
  DEF VAR sens-attrib AS CHAR NO-UNDO.

  RP.Int1:HIDDEN = type <> "RANGE".
  RP.Int2:HIDDEN = type <> "RANGE".
  RP.Int1:SENSITIVE = type = "RANGE".
  RP.Int2:SENSITIVE = type = "RANGE".
  cmb_EntityList:HIDDEN = type <> "ELIST".
  fil_PropertyFrom:HIDDEN = type <> "RANGE".
  fil_PropertyTo:HIDDEN   = type <> "RANGE".
  sens-attrib = "HIDDEN = " + IF type <> "RANGE" THEN "Yes" ELSE "No".
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_PropertyFrom:HANDLE ), sens-attrib ).
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_PropertyTo:HANDLE ), sens-attrib ).
  cmb_Select:HIDDEN = (type = "RANGE" OR type = "ELIST").

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-style-changed V-table-Win 
PROCEDURE report-style-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-type-changed V-table-Win 
PROCEDURE report-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  cmb_month2:HIDDEN = INPUT RP.Char3 = "NORMAL".
  cmb_month:LABEL = IF INPUT RP.Char3 = "NORMAL" THEN "Month Ending" ELSE "From".
  /* Refresh the display list */
  APPLY 'U1':U TO cmb_month.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR report-options AS CHAR NO-UNDO.
  DEF VAR selection-id   AS CHAR NO-UNDO.
  
  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).

  selection-id = IF RP.Char1 = "RANGE"
    THEN ( STRING( RP.Int1 ) + "-" + STRING( RP.Int2 ) )
    ELSE IF RP.Char1 = "ELIST" THEN ( RP.Char5 )
    ELSE
      ENTRY( cmb_Select:LOOKUP( cmb_Select:SCREEN-VALUE ), cmb_Select:PRIVATE-DATA ).

  report-options = "Property," + RP.Char1 + "," + selection-id
                 + "~nStyle," + RP.Char2
                 + (IF RP.Log4 THEN "~nPreview" ELSE "")
                 + "~nMonth," + RP.Char3 + "," + STRING( RP.Int3 ) + "," + STRING( RP.Int4 )
                 + (IF RP.Log4 = ? THEN "~nFile," + RP.Char4 ELSE "") .

  {inc/bq-do.i "process/report/bdincome.p" "report-options" "(RP.Log4 = No)"}

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char4 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated fields" "*.CSV" ASK-OVERWRITE
        CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV" INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME  UPDATE select-ok.

  IF select-ok THEN
    RP.Char4:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF INPUT FRAME {&FRAME-NAME} RP.Int1 >
     INPUT FRAME {&FRAME-NAME} RP.Int2 THEN
  DO:
    MESSAGE "The TO property must be greater than the from property !"
      VIEW-AS ALERT-BOX ERROR.
    APPLY 'ENTRY':U TO RP.Int1 IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END. 
   
  IF cmb_month:SCREEN-VALUE = "" OR cmb_month:SCREEN-VALUE = ? THEN
  DO:
    MESSAGE "You must select a month" VIEW-AS ALERT-BOX ERROR.
    APPLY 'ENTRY':U TO cmb_month IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

