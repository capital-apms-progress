&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r2 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
{inc/topic/tpprotit.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define FIRST-EXTERNAL-TABLE PropertyTitle
&Scoped-define EXTERNAL-TABLES PropertyTitle
/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR PropertyTitle.

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS PropertyTitle.TitleCode ~
PropertyTitle.LegalDescription PropertyTitle.Easements ~
PropertyTitle.RightsOfWay PropertyTitle.CTReference PropertyTitle.WhereHeld 
&Scoped-define FIELD-PAIRS ~
      ~{&FP1}TitleCode      ~{&FP2}TitleCode       ~{&FP3} ~
      ~{&FP1}LegalDescription ~{&FP2}LegalDescription  ~{&FP3} ~
      ~{&FP1}Easements      ~{&FP2}Easements       ~{&FP3} ~
      ~{&FP1}RightsOfWay    ~{&FP2}RightsOfWay     ~{&FP3} ~
      ~{&FP1}CTReference    ~{&FP2}CTReference     ~{&FP3} ~
      ~{&FP1}WhereHeld      ~{&FP2}WhereHeld       ~{&FP3}
&Scoped-Define ENABLED-TABLES PropertyTitle
&Scoped-Define ENABLED-OBJECTS RECT-19 fil_Property 
&Scoped-Define DISPLAYED-FIELDS PropertyTitle.TitleCode ~
PropertyTitle.LegalDescription PropertyTitle.Easements ~
PropertyTitle.RightsOfWay PropertyTitle.CTReference PropertyTitle.WhereHeld 
&Scoped-Define DISPLAYED-OBJECTS fil_Property 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fil_Property AS CHARACTER FORMAT "X(256)":U 
     LABEL "Property" 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     FONT 11 NO-UNDO.

DEFINE RECTANGLE RECT-19
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 65.72 BY 6.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     PropertyTitle.TitleCode AT ROW 1 COL 56.14 COLON-ALIGNED
          LABEL "Title Code" FORMAT "ZZZZZ9"
          VIEW-AS FILL-IN 
          SIZE 7 BY 1
          FONT 11
     fil_Property AT ROW 2.5 COL 12.14 COLON-ALIGNED
     PropertyTitle.LegalDescription AT ROW 4 COL 20.72 COLON-ALIGNED
          LABEL "Legal Description"
          VIEW-AS FILL-IN 
          SIZE 9 BY 1
          FONT 11
     PropertyTitle.Easements AT ROW 4 COL 53.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9 BY 1
          FONT 11
     PropertyTitle.RightsOfWay AT ROW 4.9 COL 20.72 COLON-ALIGNED
          LABEL "Rights Of way"
          VIEW-AS FILL-IN 
          SIZE 9 BY 1
          FONT 11
     PropertyTitle.CTReference AT ROW 5 COL 53.86 COLON-ALIGNED
          LABEL "CT Reference"
          VIEW-AS FILL-IN 
          SIZE 9 BY 1
          FONT 11
     PropertyTitle.WhereHeld AT ROW 6.25 COL 12.57 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 51 BY 1
          FONT 11
     RECT-19 AT ROW 1.5 COL 1.57
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .

 

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.PropertyTitle
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 7.25
         WIDTH              = 67.43.
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN PropertyTitle.CTReference IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN PropertyTitle.LegalDescription IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN PropertyTitle.RightsOfWay IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN PropertyTitle.TitleCode IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fil_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U1 OF fil_Property IN FRAME F-Main /* Property */
DO:
  {inc/selfil/sfpro1.i "PropertyTitle" "PropertyCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U2 OF fil_Property IN FRAME F-Main /* Property */
DO:
  {inc/selfil/sfpro2.i "PropertyTitle" "PropertyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U3 OF fil_Property IN FRAME F-Main /* Property */
DO:
  {inc/selfil/sfpro1.i "PropertyTitle" "PropertyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the record-
               source has a new row available.  This procedure
               tries to get the new row and display it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "PropertyTitle"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "PropertyTitle"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "PropertyTitle"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


