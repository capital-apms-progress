&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Property
&Scoped-define FIRST-EXTERNAL-TABLE Property


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Property.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS ConstructionDetails.ConstructionDate ~
ConstructionDetails.IntendedLife ConstructionDetails.Classification ~
ConstructionDetails.TerritorialAuthority ConstructionDetails.UsageText ~
ConstructionDetails.DescriptionText ConstructionDetails.LegalText ~
ConstructionDetails.LocationText ConstructionDetails.ZoningText 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}ConstructionDate ~{&FP2}ConstructionDate ~{&FP3}~
 ~{&FP1}IntendedLife ~{&FP2}IntendedLife ~{&FP3}~
 ~{&FP1}Classification ~{&FP2}Classification ~{&FP3}~
 ~{&FP1}TerritorialAuthority ~{&FP2}TerritorialAuthority ~{&FP3}~
 ~{&FP1}UsageText ~{&FP2}UsageText ~{&FP3}
&Scoped-define ENABLED-TABLES ConstructionDetails
&Scoped-define FIRST-ENABLED-TABLE ConstructionDetails
&Scoped-Define DISPLAYED-FIELDS Property.PropertyCode Property.Name ~
ConstructionDetails.ConstructionDate ConstructionDetails.IntendedLife ~
ConstructionDetails.Classification ConstructionDetails.TerritorialAuthority ~
ConstructionDetails.UsageText ConstructionDetails.DescriptionText ~
ConstructionDetails.LegalText ConstructionDetails.LocationText ~
ConstructionDetails.ZoningText 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */
&Scoped-define ADM-ASSIGN-FIELDS ConstructionDetails.ConstructionDate ~
ConstructionDetails.IntendedLife ConstructionDetails.Classification ~
ConstructionDetails.DescriptionText ConstructionDetails.LegalText ~
ConstructionDetails.LocationText ConstructionDetails.ZoningText 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
PropertyCode|y|y|TTPL.Property.PropertyCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "PropertyCode",
     Keys-Supplied = "PropertyCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Property.PropertyCode AT ROW 1.2 COL 5 NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
     Property.Name AT ROW 1.2 COL 9.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 56.57 BY 1
     ConstructionDetails.ConstructionDate AT ROW 2.2 COL 9.29 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     ConstructionDetails.IntendedLife AT ROW 2.2 COL 35.57 COLON-ALIGNED
          LABEL "Intended Life (Yrs)"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     ConstructionDetails.Classification AT ROW 3.2 COL 9.29 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     ConstructionDetails.TerritorialAuthority AT ROW 3.2 COL 35.57 COLON-ALIGNED
          LABEL "Territorial Authority" FORMAT "X(40)"
          VIEW-AS FILL-IN 
          SIZE 30.29 BY 1
     ConstructionDetails.UsageText AT ROW 4.2 COL 9.29 COLON-ALIGNED
          LABEL "Building Use"
          VIEW-AS FILL-IN 
          SIZE 56.57 BY 1.05
     ConstructionDetails.DescriptionText AT ROW 5.4 COL 11.29 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
          SIZE 56.57 BY 6.2
     ConstructionDetails.LegalText AT ROW 11.6 COL 11.29 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
          SIZE 56.57 BY 2.6
     ConstructionDetails.LocationText AT ROW 14.2 COL 11.29 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
          SIZE 56.57 BY 3
     ConstructionDetails.ZoningText AT ROW 17.2 COL 11.29 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
          SIZE 56.57 BY 2.4
     "Description:" VIEW-AS TEXT
          SIZE 9.72 BY 1 AT ROW 5.4 COL 1.57
     "Legal" VIEW-AS TEXT
          SIZE 9.72 BY .7 AT ROW 11.6 COL 1.57
     "description:" VIEW-AS TEXT
          SIZE 9.72 BY .7 AT ROW 12.2 COL 1.57
     "Location:" VIEW-AS TEXT
          SIZE 9.72 BY 1 AT ROW 14.2 COL 1.57
     "Zoning:" VIEW-AS TEXT
          SIZE 9.72 BY 1 AT ROW 17.2 COL 1.57
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Property
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 19.85
         WIDTH              = 87.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN ConstructionDetails.Classification IN FRAME F-Main
   2                                                                    */
/* SETTINGS FOR FILL-IN ConstructionDetails.ConstructionDate IN FRAME F-Main
   2                                                                    */
/* SETTINGS FOR EDITOR ConstructionDetails.DescriptionText IN FRAME F-Main
   2                                                                    */
ASSIGN 
       ConstructionDetails.DescriptionText:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR FILL-IN ConstructionDetails.IntendedLife IN FRAME F-Main
   2 EXP-LABEL                                                          */
/* SETTINGS FOR EDITOR ConstructionDetails.LegalText IN FRAME F-Main
   2                                                                    */
ASSIGN 
       ConstructionDetails.LegalText:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR EDITOR ConstructionDetails.LocationText IN FRAME F-Main
   2                                                                    */
ASSIGN 
       ConstructionDetails.LocationText:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR FILL-IN Property.Name IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN Property.PropertyCode IN FRAME F-Main
   NO-ENABLE ALIGN-L EXP-LABEL                                          */
/* SETTINGS FOR FILL-IN ConstructionDetails.TerritorialAuthority IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN ConstructionDetails.UsageText IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR EDITOR ConstructionDetails.ZoningText IN FRAME F-Main
   2                                                                    */
ASSIGN 
       ConstructionDetails.ZoningText:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'PropertyCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Property
           &WHERE = "WHERE Property.PropertyCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Property"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Property"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
  RUN dispatch( 'exit':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN dispatch( 'update-record':U ).
  RUN dispatch( 'exit':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  ASSIGN ConstructionDetails.Classification:SENSITIVE   = Yes
         ConstructionDetails.ConstructionDate:SENSITIVE = Yes
         ConstructionDetails.DescriptionText:SENSITIVE  = Yes
         ConstructionDetails.IntendedLife:SENSITIVE     = Yes
         ConstructionDetails.LegalText:SENSITIVE        = Yes
         ConstructionDetails.LocationText:SENSITIVE     = Yes
         ConstructionDetails.TerritorialAuthority:SENSITIVE = Yes
         ConstructionDetails.ZoningText:SENSITIVE       = Yes.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-find-using-key V-table-Win 
PROCEDURE local-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'find-using-key':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  IF AVAILABLE(Property) THEN DO:
    FIND ConstructionDetails OF Property NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(ConstructionDetails) THEN DO TRANSACTION:
      CREATE ConstructionDetails.
      ConstructionDetails.PropertyCode = Property.PropertyCode.
    END.
  END.
  IF AVAILABLE(ConstructionDetails) THEN RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "PropertyCode" "Property" "PropertyCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Property"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


