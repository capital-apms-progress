&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File: vwr\mnt\v-approved-vouchers.w
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

&SCOP REPORT-ID "approved-vouchers"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char3 RP.Dec1 RP.Dec2 RP.Char4 RP.Char1 ~
RP.Log2 RP.Log3 RP.Log1 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Dec1 ~{&FP2}Dec1 ~{&FP3}~
 ~{&FP1}Dec2 ~{&FP2}Dec2 ~{&FP3}~
 ~{&FP1}Char4 ~{&FP2}Char4 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-22 fil_from fil_to fil_due cmb_Client ~
btn_print 
&Scoped-Define DISPLAYED-FIELDS RP.Char3 RP.Dec1 RP.Dec2 RP.Char4 RP.Char1 ~
RP.Log2 RP.Log3 RP.Log1 
&Scoped-Define DISPLAYED-OBJECTS fil_from fil_to fil_due cmb_Client 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_print DEFAULT 
     LABEL "&OK" 
     SIZE 8.57 BY 1
     FONT 9.

DEFINE VARIABLE cmb_Client AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     SIZE 45.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_due AS INTEGER FORMAT ">>9":U INITIAL 10 
     LABEL "Payments due in" 
     VIEW-AS FILL-IN 
     SIZE 4 BY 1 NO-UNDO.

DEFINE VARIABLE fil_from AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "Creditor from" 
     VIEW-AS FILL-IN 
     SIZE 5.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_to AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "to" 
     VIEW-AS FILL-IN 
     SIZE 5.72 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 65.72 BY 15.8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char3 AT ROW 2.2 COL 4.43 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "A range of creditor codes", "CreditorRange":U,
"Vouchers charged to a range of projects", "ProjectRange":U,
"Vouchers charged to a range of accounts", "AccountRange":U,
"A list of creditor codes", "CreditorList":U
          SIZE 33.14 BY 3.2
     fil_from AT ROW 2.4 COL 49.86 COLON-ALIGNED
     fil_to AT ROW 2.4 COL 58.43 COLON-ALIGNED
     RP.Dec1 AT ROW 3.6 COL 47.57 COLON-ALIGNED HELP
          ""
          LABEL "Account from" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1.05
     RP.Dec2 AT ROW 3.6 COL 57.29 COLON-ALIGNED HELP
          ""
          LABEL "to" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1.05
     RP.Char4 AT ROW 5.4 COL 11.57 COLON-ALIGNED HELP
          ""
          LABEL "Creditor List" FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 52.57 BY 1.05 TOOLTIP "Enter creditor codes separated by commas"
     fil_due AT ROW 7.2 COL 11.57 COLON-ALIGNED
     RP.Char1 AT ROW 9 COL 4.43 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "All Clients/Owners", "All":U,
"One Client/Owner", "OneClient":U
          SIZE 16.57 BY 1.6
     cmb_Client AT ROW 9.8 COL 19 COLON-ALIGNED NO-LABEL
     RP.Log2 AT ROW 12.6 COL 4.43
          LABEL "Enforce creditor cheque limits"
          VIEW-AS TOGGLE-BOX
          SIZE 23.43 BY 1
     RP.Log3 AT ROW 14 COL 4.43
          LABEL "Print approval page"
          VIEW-AS TOGGLE-BOX
          SIZE 16.57 BY 1
     RP.Log1 AT ROW 15.6 COL 4.43 HELP
          ""
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 9.14 BY .8
     btn_print AT ROW 15.6 COL 57.57
     RECT-22 AT ROW 1 COL 1
     "Select creditors by:" VIEW-AS TEXT
          SIZE 13.14 BY .8 AT ROW 1.4 COL 2.72
     "days or less should be printed." VIEW-AS TEXT
          SIZE 20.57 BY 1 AT ROW 7.2 COL 18.14
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_print.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.6
         WIDTH              = 75.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR RADIO-SET RP.Char1 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR RADIO-SET RP.Char3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Char4 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Dec1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Dec2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR Toggle-Box RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  RUN do-report.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Client
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Client V-table-Win
ON U1 OF cmb_Client IN FRAME F-Main
DO:
  {inc/selcmb/scclient1.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Client V-table-Win
ON U2 OF cmb_Client IN FRAME F-Main
DO:
  {inc/selcmb/scclient2.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_from
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_from V-table-Win
ON LEAVE OF fil_from IN FRAME F-Main /* Creditor from */
DO:
  IF SELF:MODIFIED THEN
  DO:
  
    ASSIGN
      {&SELF-NAME}
      fil_to = {&SELF-NAME}.
      
    DISPLAY fil_to WITH FRAME {&FRAME-NAME}.
  
    SELF:MODIFIED = No.
        
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE do-report V-table-Win 
PROCEDURE do-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

  RUN dispatch( 'update-record':U ).

DO WITH FRAME {&FRAME-NAME}:
  report-options = "DueBefore," + STRING( TODAY + INPUT fil_due, "99/99/9999" )
                  + "~nBankAccount," + RP.Char2
                  + (IF RP.Log1 THEN "~nPreview" ELSE "")
                  + (IF RP.Log2 THEN "~nEnforceLimit" ELSE "")
                  + (IF RP.Log3 THEN "~nApprovalPage" ELSE "")
                  + (IF RP.Char1 = "OneClient" THEN "~n" + RP.Char1 + "," + RP.Char2 ELSE "").

  CASE RP.Char3:
    WHEN "CreditorRange":U THEN
      report-options = report-options + "~nCreditorRange," + STRING( INPUT fil_from ) + "," + STRING( INPUT fil_to ).
    WHEN "ProjectRange":U THEN
      report-options = report-options + "~nProjectRange," + STRING( INPUT fil_from ) + "," + STRING( INPUT fil_to ).
    WHEN "CreditorList":U THEN
      report-options = report-options + "~nCreditorList," + INPUT RP.Char4 .
    WHEN "AccountRange":U THEN
      report-options = report-options + "~nAccountRange," + STRING( INPUT RP.Dec1 ) + "," + STRING( INPUT RP.Dec2 ).
  END CASE.
END.

  RUN process/report/approved-vouchers.p ( report-options ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:  Enable the apprpriate fields based on the chosen SelectionType
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  CASE INPUT RP.Char3:
    WHEN "CreditorRange" THEN DO:
      HIDE RP.Char4 RP.Dec1 RP.Dec2.
      fil_from:LABEL = "Creditor from".
      VIEW fil_from fil_to.
    END.
    WHEN "ProjectRange" THEN DO:
      HIDE RP.Char4 RP.Dec1 RP.Dec2.
      fil_from:LABEL = "Project from".
      VIEW fil_from fil_to.
    END.
    WHEN "AccountRange" THEN DO:
      HIDE RP.Char4 fil_from fil_to.
      VIEW RP.Dec1 RP.Dec2.
    END.
    WHEN "CreditorList" THEN DO:
      HIDE fil_from fil_to RP.Dec1 RP.Dec2.
      VIEW RP.Char4.
    END.
  END.

  CASE INPUT RP.Char1:
    WHEN "All":U THEN       HIDE cmb_Client.
    OTHERWISE               VIEW cmb_Client.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
  
  FIND RP WHERE RP.ReportID = {&REPORT-ID} AND RP.UserName = user-name NO-ERROR.

  IF NOT AVAILABLE RP THEN DO:
    CREATE RP.
    ASSIGN RP.ReportID = {&REPORT-ID}
           RP.UserName = user-name .
  END.

  fil_due  = 10.
  fil_to   = 99999.
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN enable-appropriate-fields.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


