&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description: 
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR loc_Account LIKE BankAccount.BankAccountCode NO-UNDO.
DEF VAR loc_Lease   LIKE TenancyLease.TenancyLeaseCode NO-UNDO.

&SCOPED-DEFINE REPORT-ID "Invoice/Statement"

DEF VAR tenant-clause AS CHAR NO-UNDO.
DEF VAR month-clause  AS CHAR NO-UNDO.
DEF VAR item-clause   AS CHAR NO-UNDO.

{inc/ofc-this.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Date1 RP.Int5 RP.Int2 RP.Int3 ~
RP.Int1 RP.Log3 RP.Log2 RP.Log1 RP.Log4 RP.Log5 RP.Log6 RP.Log7 RP.Char3 ~
RP.Char2 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_startmonth cmb_endmonth btn_print ~
RECT-23 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Date1 RP.Int5 RP.Int2 RP.Int3 ~
RP.Int1 RP.Log3 RP.Log2 RP.Log1 RP.Log4 RP.Log5 RP.Log6 RP.Log7 RP.Char3 ~
RP.Char2 
&Scoped-Define DISPLAYED-OBJECTS fil_Tenant1 fil_Tenant2 fil_Property ~
cmb_startmonth cmb_endmonth 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_print 
     LABEL "&OK" 
     SIZE 10.29 BY 1
     FONT 9.

DEFINE VARIABLE cmb_endmonth AS CHARACTER FORMAT "X(30)" INITIAL "0" 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 18
     DROP-DOWN-LIST
     SIZE 18.57 BY 1
     FONT 10.

DEFINE VARIABLE cmb_startmonth AS CHARACTER FORMAT "X(30)" INITIAL "0" 
     LABEL "From" 
     VIEW-AS COMBO-BOX INNER-LINES 18
     DROP-DOWN-LIST
     SIZE 18.57 BY 1
     FONT 10.

DEFINE VARIABLE fil_Property AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Tenant1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Tenant2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-23
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 68 BY 15.25.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.2 COL 2 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Include All Tenants", "All":U,
"Tenant Range", "Range":U,
"Tenants of a particular property", "Property":U
          SIZE 24 BY 2.4
          FONT 10
     RP.Date1 AT ROW 1.4 COL 46.43 COLON-ALIGNED HELP
          ""
          LABEL "Statement Date" FORMAT "99/99/9999"
          VIEW-AS FILL-IN 
          SIZE 13.29 BY 1.05
     RP.Int5 AT ROW 2.4 COL 46.43 COLON-ALIGNED HELP
          ""
          LABEL "After Batch" FORMAT "->>>,>>9"
          VIEW-AS FILL-IN 
          SIZE 8.57 BY 1.05 TOOLTIP "The last batch to be summarised in the Balance brought forward line"
     RP.Int2 AT ROW 3.8 COL 11 COLON-ALIGNED
          LABEL "Tenant" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
          FONT 10
     fil_Tenant1 AT ROW 3.75 COL 21 COLON-ALIGNED NO-LABEL
     RP.Int3 AT ROW 5 COL 11 COLON-ALIGNED
          LABEL "to" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1.05
     fil_Tenant2 AT ROW 5 COL 21 COLON-ALIGNED NO-LABEL
     RP.Int1 AT ROW 3.8 COL 11 COLON-ALIGNED
          LABEL "Property" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
          FONT 10
     fil_Property AT ROW 3.8 COL 21 COLON-ALIGNED NO-LABEL
     RP.Log3 AT ROW 6.75 COL 2 HELP
          ""
          LABEL "Summarise Part-Closed Groups"
          VIEW-AS TOGGLE-BOX
          SIZE 24.57 BY .8
     RP.Log2 AT ROW 7.55 COL 2
          LABEL "Active Tenants only"
          VIEW-AS TOGGLE-BOX
          SIZE 16.57 BY .8
          FONT 10
     RP.Log1 AT ROW 8.35 COL 2
          LABEL "Open items only"
          VIEW-AS TOGGLE-BOX
          SIZE 14.29 BY .8
          FONT 10
     RP.Log4 AT ROW 9.15 COL 2
          LABEL "Include zero-value debtors"
          VIEW-AS TOGGLE-BOX
          SIZE 22.29 BY .8
     RP.Log5 AT ROW 6.75 COL 34
          LABEL "Hide aging of balances"
          VIEW-AS TOGGLE-BOX
          SIZE 18.86 BY .85
     RP.Log6 AT ROW 7.55 COL 34
          LABEL "Exception Report"
          VIEW-AS TOGGLE-BOX
          SIZE 18.29 BY .85
     RP.Log7 AT ROW 8.35 COL 34
          LABEL "Don't actually print the invoices"
          VIEW-AS TOGGLE-BOX
          SIZE 24.57 BY .85
     RP.Char3 AT ROW 11.15 COL 12.86 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "Original only", "Original":U,
"Copy only", "Copy":U,
"Original + Copy", "Original+Copy":U
          SIZE 44 BY 1.2
     RP.Char2 AT ROW 13.15 COL 2 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Include all months", "All":U,
"For a range of months", "Range":U
          SIZE 17.72 BY 1.6
          FONT 10
     cmb_startmonth AT ROW 13.85 COL 21.29
     cmb_endmonth AT ROW 13.85 COL 45.57
     btn_print AT ROW 15.15 COL 58
     RECT-23 AT ROW 1 COL 1
     "Print:" VIEW-AS TEXT
          SIZE 4 BY .65 AT ROW 11.35 COL 11.86 RIGHT-ALIGNED
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_print.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 16.2
         WIDTH              = 76.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR RADIO-SET RP.Char3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR COMBO-BOX cmb_endmonth IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_startmonth IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN RP.Date1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_Property IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Tenant1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Tenant2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log6 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log7 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TEXT-LITERAL "Print:"
          SIZE 4 BY .65 AT ROW 11.35 COL 11.86 RIGHT-ALIGNED            */

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN tenant-options-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON VALUE-CHANGED OF RP.Char2 IN FRAME F-Main /* Char2 */
DO:
  RUN month-options-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_endmonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_endmonth V-table-Win
ON U1 OF cmb_endmonth IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int4"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_endmonth V-table-Win
ON U2 OF cmb_endmonth IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int4"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_startmonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_startmonth V-table-Win
ON U1 OF cmb_startmonth IN FRAME F-Main /* From */
DO:
  {inc/selcmb/scmths1.i "RP" "Int3"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_startmonth V-table-Win
ON U2 OF cmb_startmonth IN FRAME F-Main /* From */
DO:
  {inc/selcmb/scmths2.i "RP" "Int3"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U1 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U2 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U3 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Tenant1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant1 V-table-Win
ON U1 OF fil_Tenant1 IN FRAME F-Main
DO:
  {inc/selfil/sftnt1.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant1 V-table-Win
ON U2 OF fil_Tenant1 IN FRAME F-Main
DO:
  {inc/selfil/sftnt2.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant1 V-table-Win
ON U3 OF fil_Tenant1 IN FRAME F-Main
DO:
  {inc/selfil/sftnt3.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Tenant2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant2 V-table-Win
ON U1 OF fil_Tenant2 IN FRAME F-Main
DO:
  {inc/selfil/sftnt1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant2 V-table-Win
ON U2 OF fil_Tenant2 IN FRAME F-Main
DO:
  {inc/selfil/sftnt2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant2 V-table-Win
ON U3 OF fil_Tenant2 IN FRAME F-Main
DO:
  {inc/selfil/sftnt3.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Property */
DO:
  {inc/selcde/cdpro.i "fil_Property"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* Tenant */
DO:
  {inc/selcde/cdtnt.i "fil_Tenant1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int3 V-table-Win
ON LEAVE OF RP.Int3 IN FRAME F-Main /* to */
DO:
  {inc/selcde/cdtnt.i "fil_Tenant2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log1 V-table-Win
ON VALUE-CHANGED OF RP.Log1 IN FRAME F-Main /* Open items only */
DO:
  RUN open-items-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log3 V-table-Win
ON VALUE-CHANGED OF RP.Log3 IN FRAME F-Main /* Summarise Part-Closed Groups */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win 
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override (thoroughly!) the Progress adm-row-available
------------------------------------------------------------------------------*/

 /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Creditor"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Creditor"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN tenant-options-changed.
  RUN month-options-changed.
  RUN open-items-changed.
  RUN summarise-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.
  
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  
  FIND RP WHERE RP.UserName = user-name AND RP.ReportID = {&REPORT-ID} NO-ERROR.

  IF NOT AVAILABLE RP THEN DO WITH FRAME {&FRAME-NAME}:
    CREATE RP.
    ASSIGN  RP.ReportID = {&REPORT-ID}
            RP.UserName = user-name
            RP.Char1 = "All"
            RP.Char2 = "All"
            RP.Log1 = Yes
            RP.Log2 = Yes.
  END.

  IF RP.Date1 = ? THEN RP.Date1 = TODAY .
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE month-options-changed V-table-Win 
PROCEDURE month-options-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR options AS CHAR NO-UNDO.
  options = INPUT FRAME {&FRAME-NAME} RP.CHar2.
  
  cmb_startmonth:SENSITIVE = options <> "All" AND RP.Char2:SENSITIVE.
  cmb_endmonth:SENSITIVE   = options <> "All" AND RP.Char2:SENSITIVE.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-items-changed V-table-Win 
PROCEDURE open-items-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR open-items AS LOGI NO-UNDO.
  open-items = INPUT FRAME {&FRAME-NAME} RP.Log1.
  
  RP.Char2:SENSITIVE       = NOT open-items.
  RUN month-options-changed.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE post-initialize V-table-Win 
PROCEDURE post-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN tenant-options-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     Actually run the report through RB engine.
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.
DEF VAR open-only AS LOGI NO-UNDO INITIAL No.

  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).

  open-only = RP.Log1.
  IF RP.Log3 THEN open-only = Yes.

  report-options = "Selection," + RP.Char1 + ","
                     + (IF RP.Char1 = "Property" THEN 
                          STRING( RP.Int1 )      /* property code */
                       ELSE (IF RP.Char1 = "Tenant" THEN
                         STRING( RP.Int2 )       /* tenant codes */
                        ELSE (IF RP.Char1 = "Range" THEN
                          STRING( RP.Int2 ) + "," + STRING( RP.Int3 ) /* tenant codes */
                        ELSE "")))
                 + "~nPrint," + REPLACE( RP.Char3, "+", ",")
                 + (IF RP.Log3 THEN "~nSummarisePartClosed" ELSE "")
                 + (IF open-only THEN "~nOpenOnly" ELSE
                      "~nMonths," + RP.Char2 + "," + STRING( RP.Int3 ) + "," + STRING( RP.Int4 )
                    )
                 + (IF RP.Log2 THEN "~nActiveOnly" ELSE "")
                 + (IF RP.Log4 THEN "~nShowZeroes" ELSE "")
                 + (IF RP.Log5 THEN "~nHideAging" ELSE "")
                 + (IF RP.Log6 THEN "~nShowDifferences" ELSE "")
                 + (IF RP.Log6 AND RP.Log7 THEN "~nDontPrint" ELSE "")
                 + (IF RP.Int5 = ? THEN "" ELSE "~nAfterBatch," + STRING(RP.Int5))
                 + (IF RP.Date1 <> ? THEN "~nStatementDate," + STRING( RP.Date1, "99/99/9999") ELSE "")
                 .
  {inc/bq-do.i "process/report/invoice-statement.p" "report-options" "RP.Char1 <> 'Tenant'"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win 
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Creditor"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE summarise-changed V-table-Win 
PROCEDURE summarise-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR summarise AS LOGI NO-UNDO.
  summarise = INPUT FRAME {&FRAME-NAME} RP.Log3.
  
  RP.Log1:SENSITIVE       = NOT summarise.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE tenant-options-changed V-table-Win 
PROCEDURE tenant-options-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR options AS CHAR NO-UNDO.
  options = INPUT FRAME {&FRAME-NAME} RP.Char1.

  CASE options:
    WHEN "ALL" THEN
    DO WITH FRAME {&FRAME-NAME}:
      HIDE RP.Int1 Rp.Int2 fil_Property fil_Tenant1 RP.Int3 fil_Tenant2.
      RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = Yes":U ).  
      RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Tenant1:HANDLE ),   "HIDDEN = Yes":U ).  
      RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Tenant2:HANDLE ),   "HIDDEN = Yes":U ).  
    END.

    WHEN "Range" THEN
    DO WITH FRAME {&FRAME-NAME}:
      HIDE RP.Int1 fil_Property.
      VIEW RP.Int2 fil_Tenant1 RP.Int3 fil_Tenant2.
      RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = Yes":U ).
      RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Tenant1:HANDLE ),   "HIDDEN = No":U ).  
      RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Tenant2:HANDLE ),   "HIDDEN = No":U ).  
    END.
    
    WHEN "Property" THEN
    DO WITH FRAME {&FRAME-NAME}:
      HIDE RP.Int2 fil_Tenant1 RP.Int3 fil_Tenant2.
      VIEW RP.Int1 fil_Property.
      RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Tenant1:HANDLE ),   "HIDDEN = Yes":U ).  
      RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Tenant2:HANDLE ),   "HIDDEN = Yes":U ).  
      RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = No":U ).
    END.

  END.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  CASE INPUT FRAME {&FRAME-NAME} RP.Char1:

    WHEN 'Single' THEN
      IF NOT CAN-FIND( FIRST Tenant WHERE Tenant.TenantCode = 
        INPUT FRAME {&FRAME-NAME} RP.Int2 ) THEN
      DO:
        MESSAGE "You must select a Tenant" VIEW-AS ALERT-BOX ERROR.
        APPLY 'ENTRY':U TO RP.Int2 IN FRAME {&FRAME-NAME}.
        RETURN "FAIL".
      END.
      
    WHEN 'Property' THEN
      IF NOT CAN-FIND( FIRST Property WHERE Property.PropertyCode =
        INPUT FRAME {&FRAME-NAME} RP.Int1 ) THEN
      DO:
        MESSAGE "You must select a property" VIEW-AS ALERT-BOX ERROR.
        APPLY 'ENTRY':U TO RP.Int1 IN FRAME {&FRAME-NAME}.
        RETURN "FAIL".
      END.
    
  END.

  IF NOT INPUT FRAME {&FRAME-NAME} RP.Log1 AND
         INPUT FRAME {&FRAME-NAME} Rp.Char2 = "Range" AND
    ( cmb_startmonth:SCREEN-VALUE = "" OR cmb_startmonth:SCREEN-VALUE = ? OR
      cmb_endmonth:SCREEN-VALUE   = "" OR cmb_endmonth:SCREEN-VALUE   = ? ) THEN
  DO:
    MESSAGE "You must select a month range" VIEW-AS ALERT-BOX ERROR.
    APPLY 'ENTRY':U TO cmb_startmonth IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

