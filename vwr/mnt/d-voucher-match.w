&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: Dialog to assign project/order for matching vouchers to orders.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Andrew McMillan

  Created: 3 Feb 2000
------------------------------------------------------------------------*/

DEF INPUT PARAMETER vchrseq AS INT NO-UNDO.

RUN initialize-values.
IF RETURN-VALUE= "FAIL" THEN RETURN.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE DIALOG-BOX
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fil_EntityType fil_EntityCode fil_OrderCode ~
fil_OrderDescription Btn_OK Btn_Cancel RECT-30 
&Scoped-Define DISPLAYED-OBJECTS fil_EntityType fil_TypeName fil_EntityCode ~
fil_EntityName fil_OrderCode fil_OrderDescription 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 9.43 BY 1.15
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 9.14 BY 1.15
     BGCOLOR 8 .

DEFINE VARIABLE fil_OrderDescription AS CHARACTER 
     VIEW-AS EDITOR NO-WORD-WRAP
     SIZE 49.72 BY 3.9
     BGCOLOR 16 FGCOLOR 0  NO-UNDO.

DEFINE VARIABLE fil_EntityCode AS INTEGER FORMAT ">>>>9" INITIAL 0 
     LABEL "Entity" 
     VIEW-AS FILL-IN 
     SIZE 5.72 BY 1.

DEFINE VARIABLE fil_EntityName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_EntityType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN 
     SIZE 3 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_OrderCode AS INTEGER FORMAT ">>9" INITIAL 0 
     LABEL "Order" 
     VIEW-AS FILL-IN 
     SIZE 5.72 BY 1.

DEFINE VARIABLE fil_TypeName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1.05 NO-UNDO.

DEFINE RECTANGLE RECT-30
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 63.43 BY 6.9.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fil_EntityType AT ROW 1.25 COL 7 COLON-ALIGNED
     fil_TypeName AT ROW 1.25 COL 13 COLON-ALIGNED NO-LABEL
     fil_EntityCode AT ROW 2.5 COL 7 COLON-ALIGNED
     fil_EntityName AT ROW 2.5 COL 12.72 COLON-ALIGNED NO-LABEL
     fil_OrderCode AT ROW 3.8 COL 7 COLON-ALIGNED
     fil_OrderDescription AT ROW 3.8 COL 14.72 NO-LABEL
     Btn_OK AT ROW 8.25 COL 1
     Btn_Cancel AT ROW 8.25 COL 10.72
     RECT-30 AT ROW 1.1 COL 1.29
     SPACE(2.70) SKIP(1.99)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 9
         TITLE "Select Project / Order for matching"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: DIALOG-BOX
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Dialog-Frame 
/* ************************* Included-Libraries *********************** */

{inc/entity.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
                                                                        */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fil_EntityName IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fil_OrderDescription:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fil_TypeName IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Select Project / Order for matching */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  RUN save-current-values.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_EntityCode Dialog-Frame
ON LEAVE OF fil_EntityCode IN FRAME Dialog-Frame /* Entity */
DO:
  RUN entity-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_EntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_EntityType Dialog-Frame
ON LEAVE OF fil_EntityType IN FRAME Dialog-Frame /* Type */
DO:
  RUN type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_OrderCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_OrderCode Dialog-Frame
ON LEAVE OF fil_OrderCode IN FRAME Dialog-Frame /* Order */
DO:
  RUN order-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fil_EntityType fil_TypeName fil_EntityCode fil_EntityName 
          fil_OrderCode fil_OrderDescription 
      WITH FRAME Dialog-Frame.
  ENABLE fil_EntityType fil_EntityCode fil_OrderCode fil_OrderDescription 
         Btn_OK Btn_Cancel RECT-30 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-changed Dialog-Frame 
PROCEDURE entity-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  fil_EntityName = get-entity-name( INPUT fil_EntityType, INPUT fil_EntityCode ).
  fil_EntityName:SCREEN-VALUE = fil_EntityName .
END.
  RUN order-changed.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialize-values Dialog-Frame 
PROCEDURE initialize-values :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FIND Voucher NO-LOCK WHERE VoucherSeq = vchrseq NO-ERROR.
  IF NOT AVAILABLE(Voucher) THEN RETURN "FAIL".

  DO WITH FRAME {&FRAME-NAME}:
    fil_EntityType = Voucher.EntityType.
    fil_EntityCode = Voucher.EntityCode.
    fil_OrderCode = Voucher.OrderCode.
    DISPLAY fil_OrderCode fil_EntityCode fil_EntityType.
    RUN type-changed.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE order-changed Dialog-Frame 
PROCEDURE order-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  FIND FIRST Order WHERE Order.EntityType = INPUT fil_EntityType
                AND Order.EntityCode = INPUT fil_EntityCode
                AND Order.OrderCode = INPUT fil_OrderCode
                NO-LOCK NO-ERROR.
  IF AVAILABLE(Order) THEN
    fil_OrderDescription = Order.Description .
  ELSE
    fil_OrderDescription = "".
  fil_OrderDescription:SCREEN-VALUE = fil_OrderDescription .
  fil_OrderDescription:FGCOLOR = ?.
  fil_OrderDescription:BGCOLOR = ?.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-current-values Dialog-Frame 
PROCEDURE save-current-values :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  FIND Voucher NO-LOCK WHERE VoucherSeq = vchrseq NO-ERROR.
  FIND FIRST Order WHERE Order.EntityType = INPUT fil_EntityType
                AND Order.EntityCode = INPUT fil_EntityCode
                AND Order.OrderCode = INPUT fil_OrderCode
                NO-LOCK NO-ERROR.
  IF AVAILABLE(Order) AND AVAILABLE(Voucher) THEN DO:
    FIND CURRENT Voucher EXCLUSIVE-LOCK NO-ERROR.
    IF INPUT fil_EntityType = "J" THEN Voucher.ProjectCode = INPUT fil_EntityCode.
    Voucher.EntityType = INPUT fil_EntityType.
    Voucher.EntityCode = INPUT fil_EntityCode.
    Voucher.OrderCode = INPUT fil_OrderCode.
  END.
  ELSE DO:
    MESSAGE "Not assigned - no such order" INPUT fil_EntityType
     + STRING(INPUT fil_EntityCode) + "/" + (INPUT fil_OrderCode)
    VIEW-AS ALERT-BOX WARNING.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE type-changed Dialog-Frame 
PROCEDURE type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  fil_TypeName = get-entity-type-name( INPUT fil_EntityType ) .
  fil_TypeName:SCREEN-VALUE = fil_TypeName .
END.
  RUN entity-changed.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

