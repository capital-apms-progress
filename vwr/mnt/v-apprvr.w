&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:
  Description: 

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Local Variable Definitions ---                                       */
{inc/topic/tpapprvr.i}

DEF VAR mode AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Approver
&Scoped-define FIRST-EXTERNAL-TABLE Approver


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Approver.
/* Definitions for FRAME F-Main                                         */
&Scoped-define FIELDS-IN-QUERY-F-Main Approver.ApproverCode Approver.Active ~
Approver.ApprovalLimit Approver.OverBudgetLimit Approver.SignatoryLimit 
&Scoped-define ENABLED-FIELDS-IN-QUERY-F-Main Approver.ApproverCode ~
Approver.Active Approver.ApprovalLimit Approver.OverBudgetLimit ~
Approver.SignatoryLimit 
&Scoped-define ENABLED-TABLES-IN-QUERY-F-Main Approver
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-F-Main Approver

&Scoped-define FIELD-PAIRS-IN-QUERY-F-Main~
 ~{&FP1}ApproverCode ~{&FP2}ApproverCode ~{&FP3}~
 ~{&FP1}ApprovalLimit ~{&FP2}ApprovalLimit ~{&FP3}~
 ~{&FP1}OverBudgetLimit ~{&FP2}OverBudgetLimit ~{&FP3}~
 ~{&FP1}SignatoryLimit ~{&FP2}SignatoryLimit ~{&FP3}
&Scoped-define OPEN-QUERY-F-Main OPEN QUERY F-Main FOR EACH Approver NO-LOCK.
&Scoped-define TABLES-IN-QUERY-F-Main Approver
&Scoped-define FIRST-TABLE-IN-QUERY-F-Main Approver


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Approver.ApproverCode Approver.Active ~
Approver.ApprovalLimit Approver.OverBudgetLimit Approver.SignatoryLimit 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}ApproverCode ~{&FP2}ApproverCode ~{&FP3}~
 ~{&FP1}ApprovalLimit ~{&FP2}ApprovalLimit ~{&FP3}~
 ~{&FP1}OverBudgetLimit ~{&FP2}OverBudgetLimit ~{&FP3}~
 ~{&FP1}SignatoryLimit ~{&FP2}SignatoryLimit ~{&FP3}
&Scoped-define ENABLED-TABLES Approver
&Scoped-define FIRST-ENABLED-TABLE Approver
&Scoped-Define ENABLED-OBJECTS RECT-1 
&Scoped-Define DISPLAYED-FIELDS Approver.ApproverCode Approver.Active ~
Approver.ApprovalLimit Approver.OverBudgetLimit Approver.SignatoryLimit 
&Scoped-Define DISPLAYED-OBJECTS fil_Person 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fil_Person AS CHARACTER FORMAT "X(50)":U 
     LABEL "Person" 
     VIEW-AS FILL-IN 
     SIZE 41.72 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 48.57 BY 6.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY F-Main FOR 
      Approver SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Approver.ApproverCode AT ROW 1.4 COL 5.29 COLON-ALIGNED
          LABEL "Code"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     Approver.Active AT ROW 1.4 COL 41
          VIEW-AS TOGGLE-BOX
          SIZE 7.43 BY 1
     fil_Person AT ROW 2.4 COL 5.29 COLON-ALIGNED
     Approver.ApprovalLimit AT ROW 3.6 COL 34.43 COLON-ALIGNED
          LABEL "Normal Approval Limit"
          VIEW-AS FILL-IN 
          SIZE 12.57 BY 1
     Approver.OverBudgetLimit AT ROW 4.6 COL 34.43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 12.57 BY 1
     Approver.SignatoryLimit AT ROW 5.8 COL 34.43 COLON-ALIGNED
          LABEL "Cheque Signatory Limit"
          VIEW-AS FILL-IN 
          SIZE 12.57 BY 1
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Approver
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 7.7
         WIDTH              = 51.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Approver.ApprovalLimit IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Approver.ApproverCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Person IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Approver.SignatoryLimit IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _TblList          = "TTPL.Approver"
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fil_Person
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Person V-table-Win
ON U1 OF fil_Person IN FRAME F-Main /* Person */
DO:
  {inc/selfil/sfpsn1.i "Approver" "PersonCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Person V-table-Win
ON U2 OF fil_Person IN FRAME F-Main /* Person */
DO:
  {inc/selfil/sfpsn2.i "Approver" "PersonCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Person V-table-Win
ON U3 OF fil_Person IN FRAME F-Main /* Person */
DO:
  {inc/selfil/sfpsn3.i "Approver" "PersonCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Approver"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Approver"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN RUN delete-approver. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN verify-approver.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).
  IF mode = "Add" THEN RUN notify( 'open-query,record-source':U ).
  RUN dispatch( 'exit':u ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-approver V-table-Win 
PROCEDURE delete-approver :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Approver EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Approver THEN DELETE Approver.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF mode = "Add" THEN
  DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE RUN dispatch( 'enable-fields':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FIND FIRST Approver WHERE Approver.ApproverCode = ""
                        AND Approver.PersonCode = 0 NO-ERROR.
  IF NOT AVAILABLE(Approver) THEN CREATE Approver NO-ERROR.

  ASSIGN Approver.Active = Yes.

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

  CURRENT-WINDOW = THIS-PROCEDURE:CURRENT-WINDOW.
  CURRENT-WINDOW:TITLE = "Adding new Approver".
  RUN update-node IN sys-mgr ( THIS-PROCEDURE ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = 'WINDOW-CLOSE':U THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Approver"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-approver V-table-Win 
PROCEDURE verify-approver :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT CAN-FIND( Contact WHERE ROWID( Contact ) = TO-ROWID( 
    fil_Person:PRIVATE-DATA IN FRAME {&FRAME-NAME} ) ) THEN
  DO WITH FRAME {&FRAME-NAME}:
    MESSAGE "Youy must select a person!" VIEW-AS ALERT-BOX ERROR
      TITLE "No person selected".
    APPLY 'ENTRY':U TO Approver.ApproverCode.
    APPLY 'TAB':U   TO Approver.ApproverCode.
    RETURN "FAIL".
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


