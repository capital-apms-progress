&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "" NO-UNDO.
{inc/username.i "user-name"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Int1 RP.Int2 RP.Char4 RP.Dec1 ~
RP.Dec2 RP.Log4 RP.Date1 RP.Log1 RP.Log3 RP.Log9 RP.Log6 RP.Log8 RP.Log5 ~
RP.Char3 RP.Log2 RP.Char2 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_ListName cmb_AccountGroup ~
fil_AccountLike cmb_month1 cmb_month2 tgl_MonthTotals btn_Browse Btn_OK ~
RECT-1 RECT-4 RECT-5 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Int1 RP.Int2 RP.Char4 RP.Dec1 ~
RP.Dec2 RP.Log4 RP.Date1 RP.Log1 RP.Log3 RP.Log9 RP.Log6 RP.Log8 RP.Log5 ~
RP.Char3 RP.Log2 RP.Char2 
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define DISPLAYED-OBJECTS fil_prop1 cmb_ListName fil_prop2 ~
fil_account1 cmb_AccountGroup fil_account2 fil_AccountLike cmb_month1 ~
cmb_month2 tgl_MonthTotals 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Browse 
     LABEL "Browse" 
     SIZE 6.29 BY 1
     FONT 10.

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 9.72 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE cmb_AccountGroup AS CHARACTER FORMAT "X(100)" 
     LABEL "Group" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "PROPEX - Property Expenses" 
     DROP-DOWN-LIST
     SIZE 56.57 BY 1
     FONT 8.

DEFINE VARIABLE cmb_ListName AS CHARACTER FORMAT "X(100)" 
     LABEL "List" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "TTP" 
     DROP-DOWN-LIST
     SIZE 56.57 BY 1.

DEFINE VARIABLE cmb_month1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "10/07/1886" 
     DROP-DOWN-LIST
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_month2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_account1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_account2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_AccountLike AS CHARACTER FORMAT "XXXX.XX":U INITIAL "******" 
     LABEL "Pattern" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "Enter a pattern to match the accounts to be reported" NO-UNDO.

DEFINE VARIABLE fil_prop1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_prop2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 80.57 BY 20.25.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 79.43 BY 3.8.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 79.43 BY 2.8.

DEFINE VARIABLE tgl_MonthTotals AS LOGICAL INITIAL no 
     LABEL "running totals at end of each period" 
     VIEW-AS TOGGLE-BOX
     SIZE 30.29 BY .8 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.4 COL 2.14 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Single property", "1":U,
"Property range", "R":U,
"Property list", "L":U
          SIZE 13.72 BY 2.4
          FONT 10
     RP.Int1 AT ROW 1.4 COL 21.86 COLON-ALIGNED
          LABEL "From" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_prop1 AT ROW 1.4 COL 33.43 COLON-ALIGNED NO-LABEL
     cmb_ListName AT ROW 1.8 COL 21.86 COLON-ALIGNED
     RP.Int2 AT ROW 2.4 COL 21.86 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_prop2 AT ROW 2.4 COL 33.43 COLON-ALIGNED NO-LABEL
     RP.Char4 AT ROW 4.4 COL 2.14 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Single account", "1":U,
"Account range", "R":U,
"Account group", "G":U,
"Accounts like", "L":U
          SIZE 14.86 BY 3.35
          FONT 10
     RP.Dec1 AT ROW 4.6 COL 21.86 COLON-ALIGNED HELP
          ""
          LABEL "From" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 8 BY 1
     fil_account1 AT ROW 4.6 COL 33.43 COLON-ALIGNED NO-LABEL
     cmb_AccountGroup AT ROW 5 COL 21.86 COLON-ALIGNED
     RP.Dec2 AT ROW 5.6 COL 21.86 COLON-ALIGNED HELP
          ""
          LABEL "to" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 8 BY 1
     fil_account2 AT ROW 5.6 COL 33.43 COLON-ALIGNED NO-LABEL
     fil_AccountLike AT ROW 6.6 COL 21.86 COLON-ALIGNED
     cmb_month1 AT ROW 8.15 COL 8.72 COLON-ALIGNED
     cmb_month2 AT ROW 8.15 COL 30.43 COLON-ALIGNED
     RP.Log4 AT ROW 9.65 COL 10.43 HELP
          ""
          LABEL "Only when transaction date greater on or after"
          VIEW-AS TOGGLE-BOX
          SIZE 34.29 BY .8
     RP.Date1 AT ROW 9.65 COL 42.72 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99/99/9999"
          VIEW-AS FILL-IN 
          SIZE 12 BY 1
     RP.Log1 AT ROW 10.45 COL 10.43 HELP
          ""
          LABEL "Show all, including for high-volume accounts"
          VIEW-AS TOGGLE-BOX
          SIZE 33.72 BY .8
          FONT 10
     RP.Log3 AT ROW 11.25 COL 10.43
          LABEL "Put each property on a new page"
          VIEW-AS TOGGLE-BOX
          SIZE 26.29 BY .8
          FONT 10
     RP.Log9 AT ROW 12.05 COL 10.43 HELP
          ""
          LABEL "Hide related records"
          VIEW-AS TOGGLE-BOX
          SIZE 17.14 BY .8
     RP.Log6 AT ROW 12.85 COL 10.43
          LABEL "Show only when account balance is non-zero at end of period"
          VIEW-AS TOGGLE-BOX
          SIZE 46 BY .8
          FONT 10
     RP.Log8 AT ROW 13.65 COL 10.43
          LABEL "Show opening balance at start of period"
          VIEW-AS TOGGLE-BOX
          SIZE 30.29 BY .8
     tgl_MonthTotals AT ROW 14.45 COL 10.43
     RP.Log5 AT ROW 15.15 COL 10.43
          LABEL "Show balances only"
          VIEW-AS TOGGLE-BOX
          SIZE 17.14 BY .85
     RP.Char3 AT ROW 16.45 COL 9.86 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "by Property then by Account", "P":U,
"by Account then by Property", "A":U
          SIZE 23.43 BY 1.6
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME F-Main
     RP.Log2 AT ROW 18.55 COL 9.86 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Export", ?,
"Print", no,
"Preview", yes
          SIZE 8.57 BY 2.4
     RP.Char2 AT ROW 18.55 COL 17 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 55.43 BY 1
     btn_Browse AT ROW 18.55 COL 74.43
     Btn_OK AT ROW 19.95 COL 71
     RECT-1 AT ROW 1 COL 1
     RECT-4 AT ROW 4.2 COL 1.57
     RECT-5 AT ROW 1.2 COL 1.57
     "Sequence" VIEW-AS TEXT
          SIZE 7.43 BY .8 AT ROW 15.5 COL 1.86
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 21.8
         WIDTH              = 91.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR RADIO-SET RP.Char3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR RADIO-SET RP.Char4 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Date1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Dec1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Dec2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_account1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_account2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_prop1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_prop2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR RADIO-SET RP.Log2 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log6 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log8 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log9 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Browse V-table-Win
ON CHOOSE OF btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
DO WITH FRAME {&FRAME-NAME}:
  DISABLE {&SELF-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME}.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char4 V-table-Win
ON VALUE-CHANGED OF RP.Char4 IN FRAME F-Main /* Char4 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_AccountGroup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AccountGroup V-table-Win
ON U1 OF cmb_AccountGroup IN FRAME F-Main /* Group */
DO:
  {inc/selcmb/scacg1.i "RP" "Char5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AccountGroup V-table-Win
ON U2 OF cmb_AccountGroup IN FRAME F-Main /* Group */
DO:
  {inc/selcmb/scacg2.i "RP" "Char5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_ListName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ListName V-table-Win
ON U1 OF cmb_ListName IN FRAME F-Main /* List */
DO:
  {inc/selcmb/sccls1.i "RP" "Char6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ListName V-table-Win
ON U2 OF cmb_ListName IN FRAME F-Main /* List */
DO:
  {inc/selcmb/sccls2.i "RP" "Char6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month1 V-table-Win
ON U1 OF cmb_month1 IN FRAME F-Main /* Period */
DO:
  {inc/selcmb/scmths1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month1 V-table-Win
ON U2 OF cmb_month1 IN FRAME F-Main /* Period */
DO:
  {inc/selcmb/scmths2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U1 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U2 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Dec1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Dec1 V-table-Win
ON LEAVE OF RP.Dec1 IN FRAME F-Main /* From */
DO:
  {inc/selcde/cdcoa.i "fil_account1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Dec2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Dec2 V-table-Win
ON LEAVE OF RP.Dec2 IN FRAME F-Main /* to */
DO:
  {inc/selcde/cdcoa.i "fil_account2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_account1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U1 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa1.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U2 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa2.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U3 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa3.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_account2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U1 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa1.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U2 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa2.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U3 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa3.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_prop1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U1 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U2 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U3 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_prop2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U1 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U2 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U3 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* From */
DO:
  {inc/selcde/cdpro.i "fil_prop1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* To */
DO:
  {inc/selcde/cdpro.i "fil_prop2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Log2 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Only when transaction date greater on or after */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose: As its name!
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  /* Type of property range selection */
  CASE INPUT RP.Char1:
    WHEN "1" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int2 fil_prop2 cmb_ListName.
      VIEW RP.Int1 fil_prop1 .
      DISABLE RP.Log3 .
    END.
    WHEN "R" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = No" ).
      HIDE cmb_ListName .
      VIEW RP.Int1 fil_prop1 RP.Int2 fil_prop2 .
      ENABLE RP.Log3.
    END.
    OTHERWISE DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int2 fil_prop2 RP.Int1 fil_prop1 .
      VIEW cmb_ListName .
      ENABLE RP.Log3 .
    END.
  END CASE.

  /* Type of account range selection */
  CASE INPUT RP.Char4:
    WHEN "1" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Dec2 fil_account2 cmb_AccountGroup  fil_AccountLike.
      VIEW RP.Dec1 fil_account1 .
    END.
    WHEN "R" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account2:HANDLE ), "HIDDEN = No" ).
      HIDE cmb_AccountGroup fil_AccountLike.
      VIEW RP.Dec1 fil_account1 RP.Dec2 fil_account2 .
    END.
      WHEN "L" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account1:HANDLE ), "HIDDEN = No" ).
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account2:HANDLE ), "HIDDEN = No" ).
        HIDE cmb_AccountGroup .
        VIEW RP.Dec1 fil_account1 RP.Dec2 fil_account2 fil_AccountLike.
      END.
    OTHERWISE DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Dec1 RP.Dec2 fil_account1 fil_account2  fil_AccountLike.
      VIEW cmb_AccountGroup .
    END.
  END CASE.

  /* Sequence only applies if either property or account range is non-single */
  RP.Char3:SENSITIVE = Yes .
  IF INPUT RP.Char1 = "1" OR INPUT RP.Char4 = "1" THEN DO:
    RP.Char3:SCREEN-VALUE = ENTRY( 2, RP.Char3:RADIO-BUTTONS) .
    RP.Char3:SENSITIVE = No.
  END.

  /* Exporting changes a few things */
  IF INPUT RP.Log2 = ? THEN DO:
    VIEW RP.Char2 btn_Browse .
  END.
  ELSE DO:
    HIDE RP.Char2 btn_Browse .
  END.

  /* comparison with a specific transaction date */
  IF INPUT RP.Log4 THEN
    VIEW RP.Date1 .
  ELSE
    HIDE RP.Date1 .

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/
DEF VAR month1-list AS CHAR INITIAL "" NO-UNDO.
DEF VAR month2-list AS CHAR INITIAL "" NO-UNDO.
DEF VAR delim AS CHAR INITIAL "|" NO-UNDO.

DO TRANSACTION:
  FIND RP WHERE RP.ReportID = "tr-p" AND RP.UserName = user-name NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:
    CREATE RP.
    ASSIGN RP.ReportID = "tr-p"
           RP.UserName = user-name .
  END.
  FIND CURRENT RP NO-LOCK.
END.
  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  RUN enable-appropriate-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program.
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

  RUN verify-record.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  DO TRANSACTION:
    RUN dispatch IN THIS-PROCEDURE ('update-record':U).
    ASSIGN FRAME {&FRAME-NAME} fil_AccountLike .
  END.

DEF VAR e1 AS INTEGER NO-UNDO.
DEF VAR e2 AS INTEGER NO-UNDO.
DEF VAR a1 AS DECIMAL NO-UNDO.
DEF VAR a2 AS DECIMAL NO-UNDO.

  ASSIGN    e1 = RP.Int1        e2 = RP.Int2
            a1 = RP.Dec1        a2 = RP.Dec2 .
  IF RP.Char1 = "1" THEN e2 = e1 .
  IF RP.Char4 = "1" THEN a2 = a1 .

  report-options = "PropertyRange," + STRING(e1) + "," + STRING(e2)
               + "~nAccountRange," + STRING(a1) + "," + STRING(a2)
               + "~nMonthRange," + STRING(RP.Int3) + "," + STRING(RP.Int4)
               + "~nSequence," + RP.Char3
               + (IF RP.Char1 = "L" THEN "~nConsolidationList," + RP.Char6
                                    ELSE "")
               + (IF RP.Char4 = "G" THEN "~nAccountGroup," + RP.Char5
                                    ELSE "")
               + (IF RP.Char4 = "L":U THEN "~nAccountsLike," + fil_AccountLike ELSE "")
               + (IF RP.Log1 THEN "~nShowHighVolume" ELSE "")
               + (IF RP.Log2 = Yes THEN "~nPreview" ELSE "")
               + (IF RP.Log2 = ?   THEN "~nExport," + RP.Char2 ELSE "")
               + (IF RP.Log3 THEN "~nPageBreaks" ELSE "")
               + (IF RP.Log8 THEN "~nShowOpening" ELSE "")
               + (IF RP.Log5 THEN "~nBalancesOnly" ELSE "")
               + (IF RP.Log6 THEN "~nNonZeroAtEnd" ELSE "")
               + (IF RP.Log4 THEN "~nEarliestDate," + STRING(RP.Date1) ELSE "")
               + (IF RP.Log9 THEN "~nNoRelations" ELSE "")
               + (IF INPUT FRAME {&FRAME-NAME} tgl_MonthTotals THEN "~nMonthTotals" ELSE "") .

{inc/bq-do.i "process/report/tr-p.p" "report-options" "RP.Log2 = No"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char2 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated values" "*.CSV"
        SAVE-AS ASK-OVERWRITE CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV"
        INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME UPDATE select-ok.

  IF select-ok THEN DO:
    IF SUBSTRING( file-name, R-INDEX( file-name, ".") ) <> ".CSV" THEN DO:
      IF (LENGTH( file-name ) - R-INDEX( file-name, "." )) < 4 THEN
        file-name = SUBSTRING( file-name, 1, R-INDEX( file-name, ".")) + "CSV" .
    END.
    RP.Char2:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-record V-table-Win 
PROCEDURE verify-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Log4 AND INPUT RP.Date1 = ? THEN DO:
    MESSAGE "You must enter a valid date (or turn the option off)"
             VIEW-AS ALERT-BOX ERROR
             TITLE "Date greater than ...?".
    APPLY 'ENTRY':U TO RP.Date1.
    RETURN "FAIL".
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

