&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tprntspc.i}

DEF VAR mode AS CHAR NO-UNDO.
DEF VAR enable-contracted AS LOGI NO-UNDO INITIAL Yes.

DEF VAR record-changed AS LOGICAL INITIAL No NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set-l.i "Use-Rent-Charges" "use-rent-charges"}
{inc/ofc-set.i "Area-Units" "area-units"}
IF NOT AVAILABLE(OfficeSetting) THEN area-units = "Sq.M.".
{inc/ofc-set-l.i "Property-MarketPerUnit" "market-per-unit"}
IF NOT AVAILABLE(OfficeSetting) THEN market-per-unit = No.
{inc/ofc-set-l.i "Property-ParksPerMonth" "parks-per-month"}
IF NOT AVAILABLE(OfficeSetting) THEN parks-per-month = No.
{inc/ofc-set-l.i "RentalSpace-Estimates" "rentspace-estimates"}
IF NOT AVAILABLE(OfficeSetting) THEN rentspace-estimates = No.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RentalSpace
&Scoped-define FIRST-EXTERNAL-TABLE RentalSpace


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RentalSpace.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RentalSpace.PropertyCode ~
RentalSpace.Description RentalSpace.AreaSize RentalSpace.Level ~
RentalSpace.LevelSequence RentalSpace.OutgoingsPercentage ~
RentalSpace.VacationDate RentalSpace.ContractedRental ~
RentalSpace.VacantCosts RentalSpace.ChargedRental RentalSpace.MarketRental ~
RentalSpace.MarketRentalDate 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}PropertyCode ~{&FP2}PropertyCode ~{&FP3}~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}~
 ~{&FP1}AreaSize ~{&FP2}AreaSize ~{&FP3}~
 ~{&FP1}Level ~{&FP2}Level ~{&FP3}~
 ~{&FP1}LevelSequence ~{&FP2}LevelSequence ~{&FP3}~
 ~{&FP1}OutgoingsPercentage ~{&FP2}OutgoingsPercentage ~{&FP3}~
 ~{&FP1}VacationDate ~{&FP2}VacationDate ~{&FP3}~
 ~{&FP1}ContractedRental ~{&FP2}ContractedRental ~{&FP3}~
 ~{&FP1}VacantCosts ~{&FP2}VacantCosts ~{&FP3}~
 ~{&FP1}ChargedRental ~{&FP2}ChargedRental ~{&FP3}~
 ~{&FP1}MarketRental ~{&FP2}MarketRental ~{&FP3}~
 ~{&FP1}MarketRentalDate ~{&FP2}MarketRentalDate ~{&FP3}
&Scoped-define ENABLED-TABLES RentalSpace
&Scoped-define FIRST-ENABLED-TABLE RentalSpace
&Scoped-Define ENABLED-OBJECTS RECT-31 cmb_AreaType cmb_AreaStatus 
&Scoped-Define DISPLAYED-FIELDS RentalSpace.RentalSpaceCode ~
RentalSpace.PropertyCode RentalSpace.Description RentalSpace.AreaSize ~
RentalSpace.Level RentalSpace.LevelSequence RentalSpace.OutgoingsPercentage ~
RentalSpace.VacationDate RentalSpace.ContractedRental ~
RentalSpace.VacantCosts RentalSpace.ChargedRental RentalSpace.MarketRental ~
RentalSpace.MarketRentalDate 
&Scoped-Define DISPLAYED-OBJECTS fil_Property fil_LeaseCode ~
fil_LeaseDescription fil_TenantCode fil_TenantName cmb_AreaType ~
cmb_AreaStatus fil_RatePerUnit fil_AreaMarket 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD to-annual V-table-Win 
FUNCTION to-annual RETURNS DECIMAL
  ( INPUT freq-code AS CHAR, INPUT period-amount AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_AreaStatus AS CHARACTER FORMAT "X(60)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "","" 
     SIZE 21.14 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_AreaType AS CHARACTER FORMAT "X(60)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "","" 
     SIZE 23.43 BY 1 NO-UNDO.

DEFINE VARIABLE fil_AreaMarket AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Market P.A." 
     VIEW-AS FILL-IN 
     SIZE 10.14 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_LeaseCode AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "Lease" 
     VIEW-AS FILL-IN 
     SIZE 6.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_LeaseDescription AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 50.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Property AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     FONT 11 NO-UNDO.

DEFINE VARIABLE fil_RatePerUnit AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "per Unit" 
     VIEW-AS FILL-IN 
     SIZE 8.57 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_TenantCode AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "Tenant" 
     VIEW-AS FILL-IN 
     SIZE 6.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_TenantName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 50.86 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 70.86 BY 13.2.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RentalSpace.RentalSpaceCode AT ROW 1 COL 59 COLON-ALIGNED
          LABEL "Code"
          VIEW-AS FILL-IN 
          SIZE 9 BY 1
          FONT 11
     RentalSpace.PropertyCode AT ROW 2.2 COL 9.29 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_Property AT ROW 2.2 COL 18.43 COLON-ALIGNED NO-LABEL
     fil_LeaseCode AT ROW 3.2 COL 9.29 COLON-ALIGNED
     fil_LeaseDescription AT ROW 3.2 COL 18.43 COLON-ALIGNED NO-LABEL
     fil_TenantCode AT ROW 4.2 COL 9.29 COLON-ALIGNED
     fil_TenantName AT ROW 4.2 COL 18.43 COLON-ALIGNED NO-LABEL
     RentalSpace.Description AT ROW 5.6 COL 2.86
          VIEW-AS FILL-IN 
          SIZE 60 BY 1
     RentalSpace.AreaSize AT ROW 6.7 COL 57.86 COLON-ALIGNED FORMAT "->>,>>>,>>9.99"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     RentalSpace.Level AT ROW 6.8 COL 9.29 COLON-ALIGNED FORMAT "->>9"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     RentalSpace.LevelSequence AT ROW 6.8 COL 26.43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
     RentalSpace.OutgoingsPercentage AT ROW 7.7 COL 57.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 8.43 BY 1
     cmb_AreaType AT ROW 8.9 COL 9.29 COLON-ALIGNED
     cmb_AreaStatus AT ROW 8.9 COL 48.14 COLON-ALIGNED
     RentalSpace.VacationDate AT ROW 10 COL 48.14 COLON-ALIGNED
          LABEL "Date Vacated"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     RentalSpace.ContractedRental AT ROW 11 COL 15.57 COLON-ALIGNED
          LABEL "Contracted" FORMAT "-Z,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14 BY 1
          FONT 11
     RentalSpace.VacantCosts AT ROW 11 COL 48.14 COLON-ALIGNED HELP
          ""
          LABEL "Vacant Costs" FORMAT "-Z,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14.43 BY 1
     RentalSpace.ChargedRental AT ROW 12 COL 15.57 COLON-ALIGNED
          LABEL "Charged" FORMAT "-Z,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14 BY 1
          FONT 11
     RentalSpace.MarketRental AT ROW 13 COL 15.57 COLON-ALIGNED
          LABEL "Budget EAR" FORMAT "-Z,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14 BY 1
          FONT 11
     fil_RatePerUnit AT ROW 13 COL 42.43 COLON-ALIGNED
     RentalSpace.MarketRentalDate AT ROW 13 COL 44.14 COLON-ALIGNED
          LABEL "Market Date"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
          FONT 11
     fil_AreaMarket AT ROW 13 COL 59.57 COLON-ALIGNED
     RECT-31 AT ROW 1.4 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RentalSpace
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 14.45
         WIDTH              = 86.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RentalSpace.AreaSize IN FRAME F-Main
   EXP-FORMAT                                                           */
/* SETTINGS FOR FILL-IN RentalSpace.ChargedRental IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RentalSpace.ContractedRental IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RentalSpace.Description IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fil_AreaMarket IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_LeaseCode IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_LeaseDescription IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Property IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_RatePerUnit IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_TenantCode IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_TenantName IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RentalSpace.Level IN FRAME F-Main
   EXP-FORMAT                                                           */
/* SETTINGS FOR FILL-IN RentalSpace.MarketRental IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RentalSpace.MarketRentalDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RentalSpace.RentalSpaceCode IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN RentalSpace.VacantCosts IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RentalSpace.VacationDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/method/m-charged-rent.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON MOUSE-SELECT-DBLCLICK OF FRAME F-Main
ANYWHERE DO:
DEF VAR focus-hdl AS HANDLE NO-UNDO.
  IF enable-contracted THEN RETURN NO-APPLY.
  enable-contracted = Yes.
  focus-hdl = FOCUS:HANDLE.
  RUN dispatch( 'enable-fields':U ).
  APPLY 'ENTRY':U TO focus-hdl.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_AreaStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AreaStatus V-table-Win
ON U1 OF cmb_AreaStatus IN FRAME F-Main /* Status */
DO:
  {inc/selcmb/scast1.i "RentalSpace" "AreaStatus"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AreaStatus V-table-Win
ON U2 OF cmb_AreaStatus IN FRAME F-Main /* Status */
DO:
  {inc/selcmb/scast2.i "RentalSpace" "AreaStatus"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AreaStatus V-table-Win
ON VALUE-CHANGED OF cmb_AreaStatus IN FRAME F-Main /* Status */
DO:
  record-changed = Yes.
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_AreaType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AreaType V-table-Win
ON U1 OF cmb_AreaType IN FRAME F-Main /* Type */
DO:
  {inc/selcmb/scaty1.i "RentalSpace" "AreaType"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AreaType V-table-Win
ON U2 OF cmb_AreaType IN FRAME F-Main /* Type */
DO:
  {inc/selcmb/scaty2.i "RentalSpace" "AreaType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AreaType V-table-Win
ON VALUE-CHANGED OF cmb_AreaType IN FRAME F-Main /* Type */
DO:
  record-changed = Yes.
  RUN set-prompts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U1 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RentalSpace" "PropertyCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U2 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RentalSpace" "PropertyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U3 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RentalSpace" "PropertyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RentalSpace.MarketRental
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RentalSpace.MarketRental V-table-Win
ON LEAVE OF RentalSpace.MarketRental IN FRAME F-Main /* Budget EAR */
DO:
  IF use-rent-charges AND rentspace-estimates THEN RUN calculate-charged-rent.
  IF market-per-unit THEN RUN calculate-market-rates.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RentalSpace.PropertyCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RentalSpace.PropertyCode V-table-Win
ON LEAVE OF RentalSpace.PropertyCode IN FRAME F-Main /* Property */
DO:
  {inc/selcde/cdpro.i "fil_Property"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-prompts.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RentalSpace"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RentalSpace"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculate-charged-rent V-table-Win 
PROCEDURE calculate-charged-rent :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER OtherSpace FOR RentalSpace.
DEF VAR rent-chg AS DEC NO-UNDO.
DEF VAR rent-ann AS DEC NO-UNDO.
DEF VAR this-area AS DEC NO-UNDO.
DEF VAR area-like-this AS DEC NO-UNDO.

  IF NOT use-rent-charges THEN RETURN.
  IF NOT AVAILABLE(RentalSpace) THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:
  RentalSpace.ChargedRental:SCREEN-VALUE = STRING( get-charged-rent( RentalSpace.ChargedRental, RentalSpace.ContractedRental, RentalSpace.AreaType, RentalSpace.TenancyLeaseCode ),
                                                   RentalSpace.ChargedRental:FORMAT ).
END.
  RETURN.


  rent-chg = ?.
  IF RentalSpace.AreaStatus = "V" THEN RETURN.
  FIND TenancyLease OF RentalSpace NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(TenancyLease) THEN RETURN.
  IF TenancyLease.LeaseStatus = "PAST" THEN RETURN.

  rent-chg = 0.0 .
  FOR EACH RentCharge OF TenancyLease WHERE RentCharge.RentChargeType = RentalSpace.AreaType NO-LOCK:
    FOR EACH RentChargeLine OF RentCharge WHERE RentChargeLine.RentChargeLineStatus = "C"
                              AND RentChargeLine.StartDate <= TODAY
                              AND (RentChargeLine.EndDate >= TODAY
                                    OR RentChargeLine.EndDate = ?) NO-LOCK:
      rent-ann = to-annual( RentChargeLine.FrequencyCode, RentChargeLine.Amount ).
/*      MESSAGE rent-chg rent-ann RentChargeLine.FrequencyCode RentChargeLine.Amount . */
      rent-chg = rent-chg + rent-ann.
    END.
  END.

DO WITH FRAME {&FRAME-NAME}:
  /* pro-rata the rent charges for this type of area to the areas of this type */
  this-area = RentalSpace.AreaSize * ((IF INPUT RentalSpace.MarketRental = ? THEN 100.0 ELSE INPUT RentalSpace.MarketRental) / 100).
  area-like-this = this-area.
  FOR EACH OtherSpace WHERE OtherSpace.TenancyLeaseCode = RentalSpace.TenancyLeaseCode
                        AND OtherSpace.PropertyCode = RentalSpace.PropertyCode
                        AND OtherSpace.AreaType = RentalSpace.AreaType
                        AND OtherSpace.RentalSpaceCode <> RentalSpace.RentalSpaceCode
                        NO-LOCK:
    area-like-this = area-like-this + OtherSpace.AreaSize
                   * ((IF OtherSpace.MarketRental > 0 THEN OtherSpace.MarketRental ELSE 100.0) / 100).
  END.
  IF area-like-this <> 0 AND area-like-this <> ? AND area-like-this <> this-area THEN
    rent-chg = rent-chg * (this-area / area-like-this).

  RentalSpace.ChargedRental:SCREEN-VALUE = STRING( rent-chg, RentalSpace.ChargedRental:FORMAT ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculate-market-rates V-table-Win 
PROCEDURE calculate-market-rates :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER ThisProp FOR Property.
DEF VAR prop-market AS DEC NO-UNDO.

  IF NOT AVAILABLE(RentalSpace) THEN RETURN.
  FIND ThisProp OF RentalSpace No-LOCK.
  FIND AreaType OF RentalSpace NO-LOCK.

DO WITH FRAME {&FRAME-NAME}:
  IF AreaType.IsCarPark THEN
    prop-market = ThisProp.MarketCarpark.
  ELSE IF AreaType.IsFloorArea THEN
    prop-market = ThisProp.MarketRental.
  ELSE
    prop-market = get-charged-rent( RentalSpace.ChargedRental, RentalSpace.ContractedRental, RentalSpace.AreaType, RentalSpace.TenancyLeaseCode ).

  fil_RatePerUnit = (RentalSpace.MarketRental / 100.0) * prop-market.
  IF AreaType.IsCarPark THEN
    fil_AreaMarket = fil_RatePerUnit * RentalSpace.AreaSize * (IF parks-per-month THEN 12 ELSE 52).
  ELSE IF AreaType.IsFloorArea THEN
    fil_AreaMarket = fil_RatePerUnit * RentalSpace.AreaSize.

  DISPLAY fil_RatePerUnit fil_AreaMarket.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN check-modified( 'clear':U ).
  IF mode = "Add" THEN RUN delete-rental-space.
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT record-changed THEN RUN test-modified( OUTPUT record-changed ).

  IF record-changed THEN DO:
    RUN verify-rental-space.
    IF RETURN-VALUE = "FAIL" THEN RETURN.
  
    RUN check-rights IN sys-mgr( "V-RNTSPC", "MODIFY" ).
    IF RETURN-VALUE = "FAIL" THEN RETURN.

    RUN notify( 'hide, CONTAINER-SOURCE':U ).
    RUN dispatch( 'update-record':U ).
    IF mode = "Add" THEN RUN notify( 'open-query,record-source':U ).
  END.
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-rental-space V-table-Win 
PROCEDURE delete-rental-space :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(RentalSpace) THEN RETURN.
  FIND CURRENT RentalSpace EXCLUSIVE-LOCK.
  DELETE RentalSpace.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  ASSIGN FRAME {&FRAME-NAME} RentalSpace.VacationDate.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-disable-fields V-table-Win 
PROCEDURE inst-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  cmb_AreaType:SENSITIVE = No.
  cmb_AreaStatus:SENSITIVE = No.
  IF market-per-unit THEN DO:
    HIDE RentalSpace.MarketRentalDate.
    VIEW fil_AreaMarket fil_RatePerUnit.
  END.
  ELSE DO:
    VIEW RentalSpace.MarketRentalDate.
    HIDE fil_AreaMarket fil_RatePerUnit.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  FIND TenancyLease WHERE TenancyLease.TenancyLeaseCode = RentalSpace.TenancyLeaseCode 
                    AND TenancyLease.LeaseStatus <> "PAST" NO-LOCK NO-ERROR.
  IF AVAILABLE(TenancyLease) THEN DO:
    FIND Tenant WHERE Tenant.TenantCode = TenancyLease.TenantCode NO-LOCK NO-ERROR.
    fil_LeaseCode           = TenancyLease.TenancyLeaseCode.
    fil_LeaseDescription    = TenancyLease.AreaDescription.
    fil_TenantCode          = TenancyLease.TenantCode.
    fil_TenantName          = (IF AVAILABLE(Tenant) THEN Tenant.Name ELSE "!  !  !  Tenant not on file  !  !  !").
  END.
  ELSE ASSIGN
    fil_LeaseCode           = ?
    fil_LeaseDescription    = ""
    fil_TenantCode          = ?
    fil_TenantName          = "".

  DISPLAY fil_LeaseCode fil_LeaseDescription fil_TenantCode fil_TenantName.

  IF use-rent-charges THEN RUN calculate-charged-rent.
  IF market-per-unit THEN RUN calculate-market-rates.
END.

  RUN status-changed.
  RUN set-prompts.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  IF mode <> "Add" THEN DO:
    DISABLE RentalSpace.PropertyCode WITH FRAME {&FRAME-NAME}.
    RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = Yes" ).
  END.

DEF VAR area-status AS CHAR NO-UNDO.
  area-status = TRIM( ENTRY( 1, INPUT cmb_AreaStatus, "-" ) ).
  IF area-status = "V" THEN DO:
    DISPLAY RentalSpace.VacationDate  RentalSpace.VacantCosts  .
    VIEW RentalSpace.VacationDate  RentalSpace.VacantCosts .
  END.
  ELSE DO:
    HIDE RentalSpace.VacationDate  RentalSpace.VacantCosts .
    RentalSpace.VacationDate:SCREEN-VALUE = "".
    RentalSpace.VacantCosts:SCREEN-VALUE = "".
  END.

  RentalSpace.Vacationdate:SENSITIVE = (area-status = "V").
  RentalSpace.VacantCosts:SENSITIVE = (area-status = "V").

  IF use-rent-charges THEN DO:
    RentalSpace.ChargedRental:SENSITIVE    = No.
    RentalSpace.ContractedRental:SENSITIVE = enable-contracted.
  END.

  IF market-per-unit THEN DO:
    HIDE RentalSpace.MarketRentalDate.
    VIEW fil_AreaMarket fil_RatePerUnit.
  END.
  ELSE DO:
    VIEW RentalSpace.MarketRentalDate.
    HIDE fil_AreaMarket fil_RatePerUnit.
  END.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.

  IF mode = "View" THEN
    RUN dispatch( 'disable-fields':U ).
  ELSE
    RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR c-recsrc   AS CHAR NO-UNDO.
DEF VAR ext-tables AS CHAR NO-UNDO.

  RUN request-attribute IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE':U, 'EXTERNAL-TABLES':U ).
  ext-tables = RETURN-VALUE.
 
  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT c-recsrc ).

  DEF VAR rowid-list AS CHAR NO-UNDO.
  IF LOOKUP( "Property", ext-tables ) <> 0 THEN
  DO:
    RUN send-records IN WIDGET-HANDLE( c-recsrc ) ( "Property", OUTPUT rowid-list ).
    FIND Property WHERE ROWID( Property ) = TO-ROWID( rowid-list ) NO-LOCK NO-ERROR.
  END.
  ELSE DO:
    DEF VAR property-code AS CHAR NO-UNDO.
    RUN send-key IN WIDGET-HANDLE( c-recsrc ) ( "PropertyCode", OUTPUT property-code ).
    FIND Property WHERE Property.PropertyCode = INT( property-code ) NO-LOCK NO-ERROR.
  END.

  IF AVAILABLE(Property) THEN
    FIND RentalSpace WHERE RentalSpace.PropertyCode = Property.PropertyCode
                       AND RentalSpace.RentalSpaceCode = 0 EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE( RentalSpace ) THEN CREATE RentalSpace.

  IF AVAILABLE(Property) THEN DO:
    ASSIGN RentalSpace.PropertyCode = Property.PropertyCode.
    APPLY 'U1':U TO fil_Property IN FRAME {&FRAME-NAME}.
  END.

  record-changed = Yes.

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

  CURRENT-WINDOW:TITLE = "Adding new Rental Space " + STRING( RentalSpace.RentalSpaceCode, "99999").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = 'WINDOW-CLOSE':U THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RentalSpace"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-prompts V-table-Win 
PROCEDURE set-prompts :
/*------------------------------------------------------------------------------
  Purpose:  Set the prompts to appropriate values based on office settings
------------------------------------------------------------------------------*/
DEF VAR this-area-units AS CHAR NO-UNDO.
DEF VAR this-per-units  AS CHAR NO-UNDO.
DEF VAR this-area-fmt   AS CHAR NO-UNDO.
DEF VAR area-type AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  this-area-units = area-units.
  this-per-units  = "per " + area-units.
  this-area-fmt   = "->>,>>>,>>9.99".
  IF AVAILABLE(RentalSpace) THEN DO:
    ASSIGN area-type = ENTRY( cmb_AreaType:LOOKUP( cmb_AreaType:SCREEN-VALUE ), cmb_AreaType:PRIVATE-DATA ) NO-ERROR.
    FIND AreaType WHERE ROWID(AreaType) = TO-ROWID(area-type) NO-LOCK NO-ERROR.
    IF AVAILABLE(AreaType) THEN DO:
      IF AreaType.IsCarPark THEN ASSIGN
        this-area-units = "Parks"
        this-per-units  = "per Park/" + (IF parks-per-month THEN "Mth" ELSE "Wk")
        this-area-fmt   = "->>,>>>,>>9".
      ELSE IF AreaType.IsFloorArea THEN
        /* the default is correct, so do nothing */ .
      ELSE ASSIGN
        this-area-units = "No of units"
        this-per-units  = "per Unit"
        this-area-fmt   = "->>,>>>,>>9.99".
    END.
  END.

  IF market-per-unit THEN DO:
    /* market rentals are per unit area for building,
     * so this is a percentage of the market rate for the building */
    RentalSpace.MarketRental:LABEL = "Market %age".
    fil_RatePerUnit:LABEL = "Mkt. " + this-per-units.
  END.
  RentalSpace.AreaSize:LABEL  = this-area-units.
  RentalSpace.AreaSize:FORMAT = this-area-fmt.
  IF use-rent-charges AND rentspace-estimates THEN ASSIGN
    RentalSpace.ContractedRental:LABEL = "Estimated Portion P.A"
    RentalSpace.ChargedRental:LABEL = "Pro-rated Charge P.A".

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE status-changed V-table-Win 
PROCEDURE status-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-rental-space V-table-Win 
PROCEDURE verify-rental-space :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR area-status AS CHAR NO-UNDO.
  area-status = TRIM( ENTRY( 1, INPUT cmb_AreaStatus, "-" ) ).

  IF area-status = "V" AND INPUT RentalSpace.VacationDate = ? THEN
  DO:
    MESSAGE "You have not entered a vacation date." VIEW-AS ALERT-BOX
      WARNING TITLE "No vacation date.".
/*    APPLY 'ENTRY':U TO RentalSpace.VacationDate IN FRAME {&FRAME-NAME}.   */
/*    RETURN "FAIL". */
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION to-annual V-table-Win 
FUNCTION to-annual RETURNS DECIMAL
  ( INPUT freq-code AS CHAR, INPUT period-amount AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert period amount to annual figure
    Notes:  
------------------------------------------------------------------------------*/
  /* short circuit for most cases */
  IF freq-code = "MNTH" THEN RETURN (period-amount * 12.0).

  FIND FrequencyType WHERE FrequencyType.FrequencyCode = freq-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(FrequencyType) THEN RETURN (period-amount * 12.0). /* assume monthly! */

  IF FrequencyType.RepeatUnits BEGINS "M" THEN
    RETURN ((period-amount / DEC(FrequencyType.UnitCount)) * 12.0 ).

  RETURN ((period-amount / DEC(FrequencyType.UnitCount)) * 365.0 ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


