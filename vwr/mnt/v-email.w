&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR this-win AS HANDLE NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES PhoneDetail
&Scoped-define FIRST-EXTERNAL-TABLE PhoneDetail


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR PhoneDetail.
/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for FRAME F-Main                                         */
&Scoped-define FIELDS-IN-QUERY-F-Main PhoneDetail.Number 
&Scoped-define OPEN-QUERY-F-Main OPEN QUERY F-Main FOR EACH PhoneDetail WHERE ~{&KEY-PHRASE} ~
      AND PhoneDetail.PhoneType = "EMA" NO-LOCK.
&Scoped-define TABLES-IN-QUERY-F-Main PhoneDetail
&Scoped-define FIRST-TABLE-IN-QUERY-F-Main PhoneDetail


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fil_To fil_Subject ed-mailtext btn_Send 
&Scoped-Define DISPLAYED-FIELDS PhoneDetail.Number 
&Scoped-Define DISPLAYED-OBJECTS fil_To fil_Subject ed-mailtext 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
PersonCode|y|y|ttpl.PhoneDetail.PersonCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "PersonCode",
     Keys-Supplied = "PersonCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Send 
     LABEL "&Send" 
     SIZE 5.57 BY 1.7.

DEFINE VARIABLE ed-mailtext AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 65.14 BY 11.4 NO-UNDO.

DEFINE VARIABLE fil_Subject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subject" 
     VIEW-AS FILL-IN 
     SIZE 52 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_To AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 52 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY F-Main FOR 
      PhoneDetail SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_To AT ROW 1 COL 12.14 COLON-ALIGNED
     fil_Subject AT ROW 2 COL 12.14 COLON-ALIGNED
     PhoneDetail.Number AT ROW 3 COL 30.14 NO-LABEL BLANK  FORMAT "X(80)"
           VIEW-AS TEXT 
          SIZE 18.86 BY .6
     ed-mailtext AT ROW 3.2 COL 1 NO-LABEL
     btn_Send AT ROW 1.1 COL 1.57
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.PhoneDetail
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.65
         WIDTH              = 92.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       ed-mailtext:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR FILL-IN PhoneDetail.Number IN FRAME F-Main
   NO-ENABLE ALIGN-L EXP-LABEL EXP-FORMAT                               */
ASSIGN 
       PhoneDetail.Number:HIDDEN IN FRAME F-Main           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _TblList          = "ttpl.PhoneDetail"
     _Options          = "NO-LOCK KEY-PHRASE"
     _Where[1]         = "PhoneDetail.PhoneType = ""EMA"""
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Send
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Send V-table-Win
ON CHOOSE OF btn_Send IN FRAME F-Main /* Send */
DO:
  RUN send-email.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'PersonCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = PhoneDetail
           &WHERE = "WHERE PhoneDetail.PersonCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "PhoneDetail"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "PhoneDetail"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-apply-entry V-table-Win 
PROCEDURE local-apply-entry :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'apply-entry':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
DO WITH FRAME {&FRAME-NAME}:
  DISPLAY fil_To.
  IF TRIM(fil_To) <> "" AND fil_To <> "Unknown" THEN APPLY 'ENTRY':U TO fil_Subject.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-find-using-key V-table-Win 
PROCEDURE local-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  RETURN.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'find-using-key':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destory V-table-Win 
PROCEDURE pre-destory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  RUN check-modified('clear':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize V-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose: Before standard ADM Method
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.

  /* Code placed here will execute PRIOR to standard behavior. */
  {inc/username.i "user-name"}
  FIND RP WHERE RP.UserName = user-name
          AND RP.ReportID = "{&RP-ID}" NO-LOCK NO-ERROR.
  IF AVAILABLE(RP) THEN DO:
    ASSIGN  CURRENT-WINDOW:VIRTUAL-WIDTH-PIXELS = SESSION:HEIGHT-PIXELS
            CURRENT-WINDOW:VIRTUAL-HEIGHT-PIXELS = SESSION:WIDTH-PIXELS
            CURRENT-WINDOW:WIDTH-PIXELS = RP.Int1
            CURRENT-WINDOW:HEIGHT-PIXELS = RP.Int2 .
  END.
  RUN resize-editor.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resize-editor V-table-Win 
PROCEDURE resize-editor :
/*------------------------------------------------------------------------------
  Purpose:     Resize the editor widget to match the window size
------------------------------------------------------------------------------*/
DEF VAR space-above AS INT NO-UNDO INITIAL 0.

  RETURN.

  DO WITH FRAME {&FRAME-NAME}:

    space-above = fil_Subject:Y + fil_Subject:HEIGHT-PIXELS + 2.

    /*
     * The error-free order of assignment depends on whether the window
     * size is increasing or diminishing in each direction.
     */

    IF CURRENT-WINDOW:WIDTH-PIXELS > ed-mailtext:WIDTH-PIXELS THEN ASSIGN
      FRAME {&FRAME-NAME}:WIDTH-PIXELS = CURRENT-WINDOW:WIDTH-PIXELS + 8
      ed-mailtext:WIDTH-PIXELS = CURRENT-WINDOW:WIDTH-PIXELS
      fil_Subject:WIDTH-PIXELS = CURRENT-WINDOW:WIDTH-PIXELS - fil_Subject:X
      fil_To:WIDTH-PIXELS = CURRENT-WINDOW:WIDTH-PIXELS - fil_To:X.
    ELSE ASSIGN
      ed-mailtext:WIDTH-PIXELS = CURRENT-WINDOW:WIDTH-PIXELS
      fil_Subject:WIDTH-PIXELS = CURRENT-WINDOW:WIDTH-PIXELS - fil_Subject:X
      fil_To:WIDTH-PIXELS = CURRENT-WINDOW:WIDTH-PIXELS - fil_To:X
      FRAME {&FRAME-NAME}:WIDTH-PIXELS = CURRENT-WINDOW:WIDTH-PIXELS + 8.

    IF CURRENT-WINDOW:HEIGHT-PIXELS > ed-mailtext:HEIGHT-PIXELS THEN ASSIGN
      FRAME {&FRAME-NAME}:HEIGHT-PIXELS = CURRENT-WINDOW:HEIGHT-PIXELS + 8
      ed-mailtext:HEIGHT-PIXELS = CURRENT-WINDOW:HEIGHT-PIXELS - space-above.
    ELSE ASSIGN
      ed-mailtext:HEIGHT-PIXELS = CURRENT-WINDOW:HEIGHT-PIXELS - space-above
      FRAME {&FRAME-NAME}:HEIGHT-PIXELS = CURRENT-WINDOW:HEIGHT-PIXELS + 8.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-settings V-table-Win 
PROCEDURE save-settings :
/*------------------------------------------------------------------------------
  Purpose:  Save current settings
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.

  {inc/username.i "user-name"}
  DO TRANSACTION:
    FIND RP WHERE RP.UserName = user-name 
            AND RP.ReportID = "{&RP-ID}" EXCLUSIVE-LOCK NO-ERROR.
    IF NOT AVAILABLE(RP) THEN CREATE RP.
    ASSIGN  RP.UserName = user-name
            RP.ReportID = "{&RP-ID}" .
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-email V-table-Win 
PROCEDURE send-email :
/*------------------------------------------------------------------------------
  Purpose:  Start an e-mail message to the current person
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

DEFINE VAR objSession AS COM-HANDLE.
DEFINE VAR objMessage AS COM-HANDLE.
DEFINE VAR objRecip AS COM-HANDLE.
DEFINE VAR one AS LOGICAL INIT YES.

CREATE "MAPI.SESSION" objSession.

/* MESSAGE objSession  objSession:Name. */
objSession:Logon( "", "", FALSE, FALSE, 0, FALSE, ?).
/* MESSAGE objSession  objSession:Name. */
/* objSession:Logon(). */
/* MESSAGE objSession objSession:Name . */

objMessage = objSession:OutBox:Messages:Add().
objMessage:Subject = fil_Subject:SCREEN-VALUE IN FRAME {&FRAME-NAME}.
objMessage:Text = ed-mailtext:SCREEN-VALUE IN FRAME {&FRAME-NAME}.
objRecip = objMessage:Recipients:Add().

objRecip:Name = fil_To:SCREEN-VALUE IN FRAME {&FRAME-NAME}.

objRecip:Type = 1.
objRecip:Resolve.
objMessage:Update(TRUE, TRUE).
objMessage:Send(TRUE, FALSE).
objSession:Logoff.
        
RELEASE OBJECT objRecip.
RELEASE OBJECT objMessage.
RELEASE OBJECT objSession.

MESSAGE "Message Sent" VIEW-AS ALERT-BOX.
RUN dispatch( 'exit':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "PersonCode" "PhoneDetail" "PersonCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "PhoneDetail"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-recipient V-table-Win 
PROCEDURE set-recipient :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-to AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  fil_To = TRIM(new-to).
  fil_To:SCREEN-VALUE = fil_To.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-title V-table-Win 
PROCEDURE set-title :
/*------------------------------------------------------------------------------
  Purpose:  Set the subsidiary title of the viewer
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER title-text AS CHAR NO-UNDO.

  {&WINDOW-NAME}:TITLE = "E-Mail " + title-text + " - " + STRING( TIME, "HH:MM:SS").
  APPLY 'ENTRY':U TO ed-mailtext IN FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value V-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key AS CHAR NO-UNDO.

  FIND FIRST PhoneDetail WHERE PhoneDetail.PersonCode = INT(new-key)
                    AND PhoneDetail.PhoneType = "EMA" NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(PhoneDetail) THEN
    FIND FIRST PhoneDetail WHERE PhoneDetail.PersonCode = INT(new-key)
                    AND PhoneDetail.PhoneType = "EMAL" NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(PhoneDetail) THEN
    FIND FIRST PhoneDetail WHERE PhoneDetail.PersonCode = INT(new-key)
                    AND PhoneDetail.PhoneType = "EMA1" NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(PhoneDetail) THEN
    FIND FIRST PhoneDetail WHERE PhoneDetail.PersonCode = INT(new-key)
                    AND PhoneDetail.PhoneType = "EMA2" NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(PhoneDetail) THEN
    FIND FIRST PhoneDetail WHERE PhoneDetail.PersonCode = INT(new-key)
                    AND PhoneDetail.PhoneType = "EMA3" NO-LOCK NO-ERROR.

  IF AVAILABLE(PhoneDetail) THEN
    new-key = TRIM( PhoneDetail.Number ).
  ELSE
    new-key = "Unknown".

  RUN set-recipient( new-key ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


