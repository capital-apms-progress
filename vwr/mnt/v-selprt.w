&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&SCOPED-DEFINE REPORT-ID "Current Printer"

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-4 btn-select-local btn-select-batch ~
Btn_OK 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Char2 RP.Char3 RP.Char4 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn-select-batch 
     LABEL "Select" 
     SIZE 8.57 BY 1
     FONT 9.

DEFINE BUTTON btn-select-local 
     LABEL "Select" 
     SIZE 8.57 BY 1
     FONT 9.

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 12 BY 1
     BGCOLOR 8 .

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 57.14 BY 9.2.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 2.2 COL 2.15
          LABEL "Printer" FORMAT "X(50)"
          VIEW-AS FILL-IN 
          SIZE 50.29 BY 1
          BGCOLOR 16 FONT 10
     RP.Char2 AT ROW 3.4 COL 5.29 COLON-ALIGNED HELP
          ""
          LABEL "On" FORMAT "X(100)"
          VIEW-AS FILL-IN 
          SIZE 41.72 BY 1
          BGCOLOR 16 FONT 10
     btn-select-local AT ROW 3.4 COL 49
     RP.Char3 AT ROW 5.8 COL 2.15
          LABEL "Printer" FORMAT "X(50)"
          VIEW-AS FILL-IN 
          SIZE 50.29 BY 1
          BGCOLOR 16 FONT 10
     RP.Char4 AT ROW 7 COL 5.29 COLON-ALIGNED HELP
          ""
          LABEL "On" FORMAT "X(100)"
          VIEW-AS FILL-IN 
          SIZE 41.72 BY 1
          BGCOLOR 16 FONT 10
     btn-select-batch AT ROW 7 COL 49
     Btn_OK AT ROW 9 COL 45.57
     RECT-4 AT ROW 1 COL 1
     "Local Printout" VIEW-AS TEXT
          SIZE 20 BY 1 AT ROW 1.2 COL 2.14
          FONT 14
     "Batch Printout" VIEW-AS TEXT
          SIZE 20 BY 1 AT ROW 4.8 COL 2.14
          FONT 14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 10.7
         WIDTH              = 66.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char1 IN FRAME F-Main
   NO-ENABLE ALIGN-L EXP-LABEL EXP-FORMAT                               */
/* SETTINGS FOR FILL-IN RP.Char2 IN FRAME F-Main
   NO-ENABLE EXP-LABEL EXP-FORMAT EXP-HELP                              */
/* SETTINGS FOR FILL-IN RP.Char3 IN FRAME F-Main
   NO-ENABLE ALIGN-L EXP-LABEL EXP-FORMAT                               */
/* SETTINGS FOR FILL-IN RP.Char4 IN FRAME F-Main
   NO-ENABLE EXP-LABEL EXP-FORMAT EXP-HELP                              */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn-select-batch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-select-batch V-table-Win
ON CHOOSE OF btn-select-batch IN FRAME F-Main /* Select */
DO:
  RUN configure-printer("Batch").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-select-local
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-select-local V-table-Win
ON CHOOSE OF btn-select-local IN FRAME F-Main /* Select */
DO:
  RUN configure-printer("Local").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  RUN assign-printer.
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-printer V-table-Win 
PROCEDURE assign-printer :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO TRANSACTION:
  FIND RP WHERE RP.UserName = user-name AND RP.ReportID = {&REPORT-ID} EXCLUSIVE-LOCK NO-ERROR.
    
  IF NOT AVAILABLE(RP) THEN DO:
    CREATE RP.
    ASSIGN  RP.ReportID = {&REPORT-ID}
            RP.UserName = user-name .
  END.
  DO WITH FRAME {&FRAME-NAME}:
    RP.Char1 = RP.Char1:SCREEN-VALUE.
    RP.Char2 = RP.Char2:SCREEN-VALUE.
    RP.Char3 = RP.Char3:SCREEN-VALUE.
    RP.Char4 = RP.Char4:SCREEN-VALUE.
  END.
END.
FIND RP WHERE RP.UserName = user-name AND RP.ReportID = {&REPORT-ID} NO-LOCK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE configure-printer V-table-Win 
PROCEDURE configure-printer :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER printer-use AS CHAR NO-UNDO.

DEF VAR dlg-ok AS LOGICAL NO-UNDO INITIAL Yes.

DO WITH FRAME {&FRAME-NAME}:
  SYSTEM-DIALOG PRINTER-SETUP UPDATE dlg-ok.
  IF printer-use = "Local" THEN DO:
    IF dlg-ok THEN ASSIGN
      RP.Char1:SCREEN-VALUE = SESSION:PRINTER-NAME
      RP.Char2:SCREEN-VALUE = SESSION:PRINTER-PORT
      RP.Char2:SENSITIVE = Yes.
  END.
  ELSE DO:
    IF dlg-ok THEN ASSIGN
      RP.Char3:SCREEN-VALUE = SESSION:PRINTER-NAME
      RP.Char4:SCREEN-VALUE = SESSION:PRINTER-PORT.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FIND RP WHERE RP.UserName = user-name AND RP.ReportID = {&REPORT-ID} NO-LOCK NO-ERROR.
    
  IF NOT AVAILABLE(RP) THEN DO TRANSACTION:
    CREATE RP.
    ASSIGN  RP.ReportID = {&REPORT-ID}
            RP.UserName = user-name
            RP.Char1 = SESSION:PRINTER-NAME
            RP.Char2 = SESSION:PRINTER-PORT
            RP.Char3 = SESSION:PRINTER-NAME
            RP.Char4 = SESSION:PRINTER-PORT .
  END.
  FIND RP WHERE RP.UserName = user-name AND RP.ReportID = {&REPORT-ID} NO-LOCK NO-ERROR.

  RUN dispatch( 'display-fields':U ).

  RP.Char2:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  RP.Char3:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  RP.Char4:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  RP.Char4:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


