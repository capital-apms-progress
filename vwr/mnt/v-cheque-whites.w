&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File: vwr\mnt\v-cheque-whites.w
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

&SCOP REPORT-ID "cheque-whites"

DEF VAR pdf-support AS LOGICAL INIT YES NO-UNDO.
DEF VAR pdf-output AS LOGICAL INIT NO NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set.i "PDF-Output-Directory" "pdf-output-directory"}
IF NOT AVAILABLE(OfficeSetting) THEN
    pdf-support = NO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Int1 RP.Int2 RP.Log2 RP.Log1 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-22 cmb_bnkact btn_print cbx_pdfoutput
&Scoped-Define DISPLAYED-FIELDS RP.Int1 RP.Int2 RP.Log2 RP.Log1 
&Scoped-Define DISPLAYED-OBJECTS cmb_bnkact 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_print DEFAULT 
     LABEL "&OK" 
     SIZE 8.57 BY 1
     FONT 9.

DEFINE VARIABLE cmb_bnkact AS CHARACTER FORMAT "X(256)":U 
     LABEL "Bank Account" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "","" 
     SIZE 52.57 BY 1 NO-UNDO.

DEFINE VARIABLE cbx_pdfoutput AS LOGICAL INITIAL no
     LABEL "PDF output"
     VIEW-AS TOGGLE-BOX
     SIZE 14 BY .8 TOOLTIP "Generate the payment details for this cheque sequence as a batch of PDF files" NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 65.72 BY 7.4.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     cmb_bnkact AT ROW 1.2 COL 11.57 COLON-ALIGNED
     RP.Int1 AT ROW 2.4 COL 11.57 COLON-ALIGNED HELP
          ""
          LABEL "Cheques from" FORMAT ">999999"
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
     RP.Int2 AT ROW 3.4 COL 11.57 COLON-ALIGNED HELP
          ""
          LABEL "to" FORMAT ">999999"
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
     RP.Log2 AT ROW 5.2 COL 4.43
          LABEL "One cheque per page"
          VIEW-AS TOGGLE-BOX
          SIZE 20.57 BY .8
     RP.Log1 AT ROW 7.2 COL 4.43 HELP
          ""
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 9.14 BY .8
     cbx_pdfoutput AT ROW 6.2 COL 4.43
     btn_print AT ROW 6.2 COL 57.57
     RECT-22 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_print.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 8.9
         WIDTH              = 71.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR Toggle-Box RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.

/*   RUN dispatch( 'exit':U ). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&Scoped-define SELF-NAME cbx_pdfoutput
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbx_pdfoutput V-table-Win
ON VALUE-CHANGED OF cbx_pdfoutput IN FRAME F-Main /* OK */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    pdf-output = cbx_pdfoutput:CHECKED IN FRAME {&FRAME-NAME}.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&Scoped-define SELF-NAME cmb_bnkact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact V-table-Win
ON U1 OF cmb_bnkact IN FRAME F-Main /* Bank Account */
DO:
  {inc/selcmb/scbnk1.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact V-table-Win
ON U2 OF cmb_bnkact IN FRAME F-Main /* Bank Account */
DO:
  {inc/selcmb/scbnk2.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  IF NOT pdf-support THEN
    HIDE cbx_pdfoutput.

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
  
  FIND RP WHERE RP.ReportID = {&REPORT-ID} AND RP.UserName = user-name NO-ERROR.

  IF NOT AVAILABLE RP THEN DO:
    CREATE RP.
    ASSIGN RP.ReportID = {&REPORT-ID}
           RP.UserName = user-name .
  END.

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN enable-appropriate-fields.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

  RUN dispatch( 'update-record':U ).

  DO WITH FRAME {&FRAME-NAME}:
    report-options =  "~nChequeRange," + RP.Char2 + "," + STRING(RP.Int1) + "," + STRING(RP.Int2)
                    + (IF RP.Log1 THEN "~nPreview" ELSE "")
                    + (IF RP.Log2 THEN "~nOnePerPage" ELSE "") .

    IF pdf-output THEN
      report-options = report-options + "~nOutputPDF".
  END.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN process/report/cheque-whites.p ( report-options ).
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


