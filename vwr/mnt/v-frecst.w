&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR portfolio-list AS CHAR NO-UNDO.
DEF VAR portfolio-pd   AS CHAR NO-UNDO.
DEF VAR manager-list   AS CHAR NO-UNDO.
DEF VAR manager-pd     AS CHAR NO-UNDO.
DEF VAR region-list    AS CHAR NO-UNDO.
DEF VAR region-pd      AS CHAR NO-UNDO.

DEF VAR user-name AS CHAR NO-UNDO.  
{inc/username.i "user-name"}

{inc/ofc-this.i}
{inc/ofc-set.i "AcctGroup-Rent" "rental-groups"}
{inc/ofc-set.i "AcctGroup-Opex" "opex-groups"}
{inc/ofc-set.i "AcctGroup-Recoveries" "recovery-groups"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Int3 RP.Log4 RP.Char6 RP.Log1 RP.Char5 ~
RP.Char1 RP.Char2 RP.Date1 RP.Date2 RP.Log2 RP.Int1 RP.Int2 RP.Char4 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Int3 ~{&FP2}Int3 ~{&FP3}~
 ~{&FP1}Char6 ~{&FP2}Char6 ~{&FP3}~
 ~{&FP1}Date1 ~{&FP2}Date1 ~{&FP3}~
 ~{&FP1}Date2 ~{&FP2}Date2 ~{&FP3}~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}~
 ~{&FP1}Char4 ~{&FP2}Char4 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-1 cmb_freq btn_browse btn_ok 
&Scoped-Define DISPLAYED-FIELDS RP.Int3 RP.Log4 RP.Char6 RP.Log1 RP.Char5 ~
RP.Char1 RP.Char2 RP.Date1 RP.Date2 RP.Log2 RP.Int1 RP.Int2 RP.Char4 
&Scoped-Define DISPLAYED-OBJECTS fil_Scenario cmb_freq 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS></FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = ':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_browse 
     LABEL "&Browse" 
     SIZE 6.29 BY 1
     FONT 9.

DEFINE BUTTON btn_ok 
     LABEL "&OK" 
     SIZE 9.72 BY 1
     FONT 9.

DEFINE VARIABLE cmb_freq AS CHARACTER FORMAT "X(50)" 
     LABEL "By" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "","" 
     SIZE 20 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Scenario AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 39.43 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 67.43 BY 16.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Int3 AT ROW 1.4 COL 16.72 COLON-ALIGNED NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_Scenario AT ROW 1.4 COL 26.43 COLON-ALIGNED NO-LABEL
     RP.Log4 AT ROW 1.45 COL 1.57 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Single Scenario", yes,
"Scenario Set", no
          SIZE 13.72 BY 2
     RP.Char6 AT ROW 2.4 COL 16.72 COLON-ALIGNED HELP
          ""
          LABEL "Set" FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 49.14 BY 1
     RP.Log1 AT ROW 4 COL 1.57 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "Export", yes,
"Preview", ?,
"Print", no
          SIZE 30.29 BY 1
     RP.Char5 AT ROW 5.2 COL 1.57 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "General", "GENERAL":U,
"Rentals", "RENT":U,
"Property Expenses", "PROPEX":U,
"Recoveries", "RECOVER":U
          SIZE 50.29 BY 1
     RP.Char1 AT ROW 8 COL 4.43 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Property or Ledger", "ENTITY":U,
"Account group", "GROUP":U,
"Account code", "ACCOUNT":U
          SIZE 17.72 BY 2.4
     RP.Char2 AT ROW 8 COL 38.14 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Property or Ledger", "ENTITY":U,
"Account group", "GROUP":U,
"Account code", "ACCOUNT":U,
"Related record", "RECORD":U
          SIZE 16.57 BY 3.2
          FONT 10
     cmb_freq AT ROW 11.2 COL 4.72 COLON-ALIGNED
     RP.Date1 AT ROW 12.4 COL 4.72 COLON-ALIGNED HELP
          ""
          LABEL "From" FORMAT "99/99/9999"
          VIEW-AS FILL-IN 
          SIZE 14 BY 1
          FONT 10
     RP.Date2 AT ROW 12.4 COL 36.14 COLON-ALIGNED HELP
          ""
          LABEL "To" FORMAT "99/99/9999"
          VIEW-AS FILL-IN 
          SIZE 14 BY 1
          FONT 10
     RP.Log2 AT ROW 13.8 COL 6.72
          LABEL "Property range"
          VIEW-AS TOGGLE-BOX
          SIZE 13.14 BY 1.05
     RP.Int1 AT ROW 13.8 COL 36.14 COLON-ALIGNED HELP
          ""
          LABEL "From" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1.05
     RP.Int2 AT ROW 13.8 COL 47.57 COLON-ALIGNED HELP
          ""
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1.05
     RP.Char4 AT ROW 15 COL 3.58 HELP
          ""
          LABEL "File" FORMAT "X(100)"
          VIEW-AS FILL-IN 
          SIZE 54.29 BY 1
          FONT 10
     btn_browse AT ROW 15 COL 61.57
     btn_ok AT ROW 16.4 COL 58.14
     RECT-1 AT ROW 1 COL 1
     "Least detailed level is:" VIEW-AS TEXT
          SIZE 16 BY .7 AT ROW 7.4 COL 1.57
          FONT 10
     "Most detailed level is:" VIEW-AS TEXT
          SIZE 14.29 BY .6 AT ROW 7.4 COL 34.14
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 20.8
         WIDTH              = 85.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR RADIO-SET RP.Char1 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR RADIO-SET RP.Char2 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Char4 IN FRAME F-Main
   ALIGN-L EXP-LABEL EXP-FORMAT EXP-HELP                                */
/* SETTINGS FOR RADIO-SET RP.Char5 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Char6 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Date1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Date2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_Scenario IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR RADIO-SET RP.Log1 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR RADIO-SET RP.Log4 IN FRAME F-Main
   EXP-HELP                                                             */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/date.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_browse V-table-Win
ON CHOOSE OF btn_browse IN FRAME F-Main /* Browse */
DO:
  RUN browse-file.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON CHOOSE OF btn_ok IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_freq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON U1 OF cmb_freq IN FRAME F-Main /* By */
DO:
  {inc/selcmb/scfty1.i "RP" "Char3"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON U2 OF cmb_freq IN FRAME F-Main /* By */
DO:
  {inc/selcmb/scfty2.i "RP" "Char3"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON VALUE-CHANGED OF cmb_freq IN FRAME F-Main /* By */
DO:
  RUN columns-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Date1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Date1 V-table-Win
ON LEAVE OF RP.Date1 IN FRAME F-Main /* From */
DO:
  RUN columns-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Date2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Date2 V-table-Win
ON LEAVE OF RP.Date2 IN FRAME F-Main /* To */
DO:
  RUN columns-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Scenario
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Scenario V-table-Win
ON U1 OF fil_Scenario IN FRAME F-Main
DO:
  {inc/selfil/sfscn1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Scenario V-table-Win
ON U2 OF fil_Scenario IN FRAME F-Main
DO:
  {inc/selfil/sfscn2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Scenario V-table-Win
ON U3 OF fil_Scenario IN FRAME F-Main
DO:
  {inc/selfil/sfscn3.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int3 V-table-Win
ON LEAVE OF RP.Int3 IN FRAME F-Main /* Int3 */
DO:
  {inc/selcde/cdscn.i "fil_Scenario"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log1 V-table-Win
ON VALUE-CHANGED OF RP.Log1 IN FRAME F-Main /* Log1 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Property range */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Log4 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE browse-file V-table-Win 
PROCEDURE browse-file :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR save-as     AS CHAR NO-UNDO.
  DEF VAR file-chosen AS LOGI NO-UNDO.

  save-as = INPUT FRAME {&FRAME-NAME} RP.Char4.
  IF LENGTH( save-as ) > 0 THEN
  IF LOOKUP( SUBSTR( save-as, LENGTH( save-as ), 1 ), "\,/" ) <> 0 THEN save-as = "".
    
  SYSTEM-DIALOG GET-FILE save-as
    TITLE   "Export Contacts to file"
    FILTERS "CSV Files (*.csv)" "*.csv"
    DEFAULT-EXTENSION ".csv"
    SAVE-AS
    RETURN-TO-START-DIR
    USE-FILENAME
    UPDATE file-chosen.

  IF file-chosen THEN RP.Char4:SCREEN-VALUE IN FRAME {&FRAME-NAME} = save-as.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE columns-changed V-table-Win 
PROCEDURE columns-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       For monthly frequency types - the period from should be the first
               of the month, and the period to should be the last of the month
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR idx AS INT NO-UNDO.
  
  idx = LOOKUP( INPUT cmb_freq, cmb_freq:LIST-ITEMS, cmb_freq:DELIMITER ).
  FIND FrequencyType WHERE
    ROWID( FrequencyType ) = TO-ROWID( ENTRY( idx, cmb_freq:PRIVATE-DATA ) )
    NO-LOCK NO-ERROR.
    
  IF AVAILABLE FrequencyType AND FrequencyType.RepeatUnits = "MONTHS" THEN DO:
    DEF VAR date-from AS DATE NO-UNDO.
    DEF VAR date-to   AS DATE NO-UNDO.
    
    date-from = INPUT RP.Date1.
    date-to   = INPUT RP.Date2.
    date-from = first-of-month( date-from ).
    date-to   = last-of-month( date-to ).
    RP.Date1:SCREEN-VALUE = STRING( date-from ).
    RP.Date2:SCREEN-VALUE = STRING( date-to ).
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF INPUT RP.Log2 THEN
    VIEW RP.Int1 RP.Int2 .
  ELSE
    HIDE RP.Int1 RP.Int2 .

  IF INPUT RP.Log1 THEN
    VIEW RP.Char4 btn_Browse RP.Date2.
  ELSE
    HIDE RP.Char4 btn_Browse RP.Date2.

  IF INPUT RP.Log4 THEN DO:
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Scenario:HANDLE ), "HIDDEN = No" ).
    HIDE RP.Char6 .
    VIEW RP.Int3 fil_Scenario .
  END.
  ELSE DO:
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Scenario:HANDLE ), "HIDDEN = Yes" ).
    VIEW RP.Char6 .
    HIDE RP.Int3 fil_Scenario .
  END.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-managers V-table-Win 
PROCEDURE get-managers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF manager-list <> "" THEN RETURN.
  
  FOR EACH Property NO-LOCK
    BREAK BY Property.Administrator:
    IF FIRST-OF( Property.Administrator ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Administrator
        NO-LOCK NO-ERROR.

      manager-list = manager-list + IF manager-list = "" THEN "" ELSE ",".
      manager-list = manager-list +
        ( IF AVAILABLE Person THEN Person.FirstName + ' ' + Person.LastName ELSE "Not Managed" ).

      manager-pd = manager-pd + IF manager-pd = "" THEN "" ELSE ",".
      manager-pd = manager-pd + IF AVAILABLE Person THEN STRING( Person.PersonCode ) ELSE STRING( Property.Administrator ).
    END.
  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-portfolios V-table-Win 
PROCEDURE get-portfolios :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF portfolio-list <> "" THEN RETURN.
  
  FOR EACH Property NO-LOCK
    BREAK BY Property.Manager:
    IF FIRST-OF( Property.Manager ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Manager
        NO-LOCK NO-ERROR.

      portfolio-list = portfolio-list + IF portfolio-list = "" THEN "" ELSE ",".
      portfolio-list = portfolio-list +
        ( IF AVAILABLE Person THEN Person.JobTitle ELSE "Not in a Portfolio" ).

      portfolio-pd = portfolio-pd + IF portfolio-pd = "" THEN "" ELSE ",".
      portfolio-pd = portfolio-pd + IF AVAILABLE Person THEN STRING( Person.PersonCode ) ELSE STRING( Property.Manager ).
    END.
  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-regions V-table-Win 
PROCEDURE get-regions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF region-list <> "" THEN RETURN.
  
  FOR EACH Region NO-LOCK:

    region-list = region-list + IF region-list = "" THEN "" ELSE ",".
    region-list = region-list + Region.Name.

    region-pd = region-pd + IF region-pd = "" THEN "" ELSE ",".
    region-pd = region-pd + Region.Region.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN columns-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

&SCOP RPT-NAME "Forecasting"

  FIND RP WHERE
    RP.ReportID = {&RPT-NAME} AND
    RP.UserName = user-name NO-ERROR.

  IF NOT AVAILABLE RP THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&RPT-NAME}
      RP.UserName = user-name
      RP.Char2    = "DETAILED"
      RP.Char3    = "MNTH"
      RP.Date1    = TODAY
      RP.Date2    = TODAY + 365
      RP.Log1     = Yes
      RP.Log2     = Yes
      RP.Log4     = Yes
      RP.Char4    = "cashflow.csv"
    .
  END.

  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  RUN enable-appropriate-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN "FAIL".
  
  RUN columns-changed.
  RUN dispatch( 'update-record':U ).
  
  DEF VAR report-options     AS CHAR NO-UNDO.
  DEF VAR account-groups     AS CHAR NO-UNDO INITIAL "".
  DEF VAR entity-range       AS CHAR NO-UNDO.
  DEF VAR period-selection   AS CHAR NO-UNDO.

  period-selection = RP.Char3 + "," + STRING( RP.Date1, "99/99/9999" ) + "-"
     + STRING( IF RP.Log1 THEN RP.Date2 ELSE (add-freq( RP.Date1, RP.Char3, 12) - 1), "99/99/9999" ).

  CASE RP.Char5:
    WHEN "RENT" THEN    account-groups = "~nAccountGroups," + rental-groups + "~nExcludeGroup".
    WHEN "PROPEX" THEN  account-groups = "~nAccountGroups," + opex-groups + "~nExcludeGroup".
    WHEN "RECOVER" THEN account-groups = "~nAccountGroups," + recovery-groups + "~nExcludeGroup".
  END.

  IF RP.Log2 THEN entity-range = "~nEntityType,P~nEntityRange,"
                               + STRING( RP.Int1 ) + "," + STRING( RP.Int2 ).

  report-options = "Consolidate,"  + RP.Char1
                 + "~nDetailTo,"   + RP.Char2
                 + "~nPeriod,"     + period-selection
                 + "~nFile,"       + (IF RP.Log1 THEN RP.Char4 ELSE "Printer")
                 + (IF RP.Log1 = ? THEN "~nPreview" ELSE "")
                 + (IF RP.Log4 THEN "~nScenario,"       + STRING(RP.Int3)
                               ELSE "~nScenarioSet,"    + STRING(RP.Char6) )
                 + account-groups
                 + entity-range.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN process/export/forecast.p ( report-options ).
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* There are no foreign keys supplied by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


