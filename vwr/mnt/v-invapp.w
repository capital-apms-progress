&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR Tenant  AS CHAR FORMAT "X(27)" LABEL "Tenant" NO-UNDO.
DEF VAR break-1 AS CHAR FORMAT "X" LABEL "" INIT "" NO-UNDO.
DEF VAR break-2 AS CHAR FORMAT "X" LABEL "" INIT "" NO-UNDO.
DEF VAR break-3 AS CHAR FORMAT "X" LABEL "" INIT "" NO-UNDO.

DEF VAR h-msg          AS HANDLE NO-UNDO.
DEF VAR GST-No         LIKE Office.GSTNo NO-UNDO.

DEF VAR gst-applies    AS LOGI NO-UNDO.
DEF VAR gst-out        LIKE ChartOfAccount.AccountCode NO-UNDO.
DEF VAR gst-type       LIKE NewAcctTran.EntityType     NO-UNDO.
DEF VAR gst-code       LIKE NewAcctTran.EntityCode     NO-UNDO.
DEF VAR pdf-support AS LOGICAL INIT YES NO-UNDO.
DEF VAR pdf-printing AS LOGICAL INIT NO NO-UNDO.


DEF NEW SHARED TEMP-TABLE company-gst 
         FIELD company_id LIKE Company.CompanyCode
         FIELD tax_amount  LIKE NewAcctTrans.Amount 
         INDEX company_id IS PRIMARY company_id ASCENDING.

{inc/ofc-this.i}
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}
gst-applies = (Office.GST <> ?).
IF gst-applies THEN DO:
  {inc/ofc-acct.i "GST-OUT" "xxxxx"}
  ASSIGN  gst-type = OfficeControlAccount.EntityType
          gst-code = OfficeControlAccount.EntityCode
          gst-out  = OfficeControlAccount.AccountCode.
END.
{inc/ofc-set-l.i "Invoice-Tax-Separately" "invoice-tax-separately"}
{inc/ofc-set.i "Invoice-Offset-Groups" "invoice-offset-groups"}
{inc/ofc-set.i "Invoice-Offset" "offset-by-text"}
{inc/ofc-set-l.i "Invoice-Printing" "invoice-printing"}
{inc/ofc-set-l.i "Invoice-Advice-Note" "advice-of-charge"}
{inc/ofc-set.i "Voucher-Non-Propex" "old-style-voucher-propex-offset"}
{inc/ofc-set-l.i "GST-Multi-Company"  "GST-Multi-Company" }
{inc/ofc-set.i "GST-Rate"  "gst-rate" }
DEF VAR offset-by AS DEC NO-UNDO.
ASSIGN offset-by = DEC(offset-by-text) NO-ERROR.
IF offset-by = ? THEN DO:
  ASSIGN offset-by = DEC(old-style-voucher-propex-offset) NO-ERROR.
  IF offset-by = ? THEN offset-by = 0.
END.

{inc/ofc-set.i "Invoice-Line-Text" "invoice-line-text-raw"}
DEF VAR invoice-line-text AS LOGI NO-UNDO INITIAL No.
IF SUBSTRING(TRIM(invoice-line-text-raw), 1, 1) = "Y" THEN invoice-line-text = Yes.

{inc/ofc-set.i "Tenant-Accounts" "tenant-accounts-txt"}
DEF VAR tenant-accounts AS LOGI NO-UNDO INIT No.
IF SUBSTRING( TRIM(tenant-accounts-txt), 1, 1) = "Y" THEN tenant-accounts = Yes.

{inc/ofc-set-l.i "Invoice-Tenant-Detailed" "tenant-detailed"}
IF tenant-detailed THEN invoice-tax-separately = Yes.

/* Determine if PDF printing is supported */
{inc/ofc-set.i "PDF-Output-Directory" "pdf-output-directory"}
IF NOT AVAILABLE(OfficeSetting) THEN
    pdf-support = NO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME BROWSE-3

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Invoice Tenant

/* Definitions for BROWSE BROWSE-3                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-3 Invoice.InvoiceNo break-1 @ break-1 ~
Invoice.InvoiceDate break-2 @ break-2 ~
STRING( Tenant.TenantCode, '>>>>9' ) + ' ' + Tenant.Name @ Tenant ~
break-3 @ break-3 Invoice.ToDetail Invoice.Total 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-3 
&Scoped-define OPEN-QUERY-BROWSE-3 OPEN QUERY BROWSE-3 FOR EACH Invoice ~
      WHERE Invoice.InvoiceStatus = "U" ~
 AND Invoice.InvoiceDate < (TODAY + 28) NO-LOCK, ~
      EACH Tenant WHERE Invoice.EntityType = 'T' ~
  AND ~
Invoice.EntityCode = Tenant.TenantCode NO-LOCK ~
    BY Invoice.InvoiceNo DESCENDING.
&Scoped-define TABLES-IN-QUERY-BROWSE-3 Invoice Tenant
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-3 Invoice
&Scoped-define SECOND-TABLE-IN-QUERY-BROWSE-3 Tenant


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-BROWSE-3}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS BROWSE-3 btn_SelectAll btn_cancel RECT-21 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_approve 
     LABEL "&Approve" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_SelectAll 
     LABEL "&Select All" 
     SIZE 11.43 BY 1.

DEFINE RECTANGLE RECT-21
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 90.86 BY 16.8.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-3 FOR 
      Invoice, 
      Tenant SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-3 V-table-Win _STRUCTURED
  QUERY BROWSE-3 NO-LOCK DISPLAY
      Invoice.InvoiceNo FORMAT ">>>>>9":U
      break-1 @ break-1 FORMAT "X":U
      Invoice.InvoiceDate COLUMN-LABEL "Invoice Date" FORMAT "99/99/9999":U
      break-2 @ break-2 FORMAT "X":U
      STRING( Tenant.TenantCode, '>>>>9' ) + ' ' + Tenant.Name @ Tenant COLUMN-LABEL "Tenant" FORMAT "X(30)":U
      break-3 @ break-3 FORMAT "X":U
      Invoice.ToDetail COLUMN-LABEL "Narrative" FORMAT "X(45)":U
      Invoice.Total COLUMN-LABEL "Invoice Total" FORMAT "->>>,>>>,>>9.99":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE SIZE 89.72 BY 15.2
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     BROWSE-3 AT ROW 1.2 COL 1.57
     btn_SelectAll AT ROW 16.6 COL 1.57
     btn_approve AT ROW 16.6 COL 70.14
     btn_cancel AT ROW 16.6 COL 81
     RECT-21 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 19.05
         WIDTH              = 97.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB BROWSE-3 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON btn_approve IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-3
/* Query rebuild information for BROWSE BROWSE-3
     _TblList          = "TTPL.Invoice,TTPL.Tenant WHERE TTPL.Invoice ..."
     _Options          = "NO-LOCK"
     _OrdList          = "TTPL.Invoice.InvoiceNo|no"
     _Where[1]         = "Invoice.InvoiceStatus = ""U""
 AND Invoice.InvoiceDate < (TODAY + 28)"
     _JoinCode[2]      = "Invoice.EntityType = 'T'
  AND
Invoice.EntityCode = Tenant.TenantCode"
     _FldNameList[1]   = TTPL.Invoice.InvoiceNo
     _FldNameList[2]   > "_<CALC>"
"break-1 @ break-1" ? "X" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[3]   > TTPL.Invoice.InvoiceDate
"Invoice.InvoiceDate" "Invoice Date" ? "date" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[4]   > "_<CALC>"
"break-2 @ break-2" ? "X" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[5]   > "_<CALC>"
"STRING( Tenant.TenantCode, '>>>>9' ) + ' ' + Tenant.Name @ Tenant" "Tenant" "X(30)" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[6]   > "_<CALC>"
"break-3 @ break-3" ? "X" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[7]   > TTPL.Invoice.ToDetail
"Invoice.ToDetail" "Narrative" "X(45)" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[8]   > TTPL.Invoice.Total
"Invoice.Total" "Invoice Total" "->>>,>>>,>>9.99" "decimal" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _Query            is OPENED
*/  /* BROWSE BROWSE-3 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME BROWSE-3
&Scoped-define SELF-NAME BROWSE-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-3 V-table-Win
ON ROW-DISPLAY OF BROWSE-3 IN FRAME F-Main
DO:
  {inc/rowcol/rcinv2.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-3 V-table-Win
ON VALUE-CHANGED OF BROWSE-3 IN FRAME F-Main
DO:
  ASSIGN btn_approve:SENSITIVE = SELF:NUM-SELECTED-ROWS <> 0.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_approve
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_approve V-table-Win
ON CHOOSE OF btn_approve IN FRAME F-Main /* Approve */
DO:
  DEF VAR approve-it AS LOGI INIT Yes NO-UNDO.

  RUN notify( 'set-busy, CONTAINER-SOURCE':u ).
  REPEAT WHILE approve-it:
    RUN win/w-prgmsg.w PERSISTENT SET h-msg.
    RUN approve-invoices.
    IF RETURN-VALUE = "FAIL" THEN
      MESSAGE
        "An error has occured approving invoices!" SKIP
        "No invoices have been approved" SKIP(2)
        "Do you want to retry ?"
        VIEW-AS ALERT-BOX ERROR BUTTONS RETRY-CANCEL TITLE "Invoice Approval Error"
        UPDATE approve-it.
     ELSE
       approve-it = No.
       
     IF VALID-HANDLE( h-msg ) THEN RUN dispatch IN h-msg ( 'destroy':U ).
  END.
  RUN notify( 'set-idle, CONTAINER-SOURCE':u ).
  RUN dispatch( 'exit':U ).
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN dispatch( 'exit':u ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_SelectAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_SelectAll V-table-Win
ON CHOOSE OF btn_SelectAll IN FRAME F-Main /* Select All */
DO:
  RUN select-all-invoices.
  ASSIGN btn_approve:SENSITIVE = BROWSE-3:NUM-SELECTED-ROWS <> 0.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i} 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE approve-invoices V-table-Win 
PROCEDURE approve-invoices :
/*------------------------------------------------------------------------------
  Purpose:     Approve this invoice
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR inv-list AS CHAR NO-UNDO.

        
DO TRANSACTION ON ERROR UNDO, RETURN "FAIL":

  /* Create the batch in which to approve the invoices */
  CREATE NewBatch.
  ASSIGN
    NewBatch.BatchType   = 'NORM'
    NewBatch.Description = "Invoice Batch - " + STRING( TODAY, "99/99/9999" ).
       
  DO i = 1 TO {&BROWSE-NAME}:NUM-SELECTED-ROWS IN FRAME {&FRAME-NAME}:
    IF {&BROWSE-NAME}:FETCH-SELECTED-ROW(i) THEN.

    RUN update-details IN h-msg ( 'Approving Invoices', ( i / {&BROWSE-NAME}:NUM-SELECTED-ROWS ) * 100 ).

    FIND CURRENT Invoice EXCLUSIVE-LOCK.
    FIND Tenant WHERE Tenant.TenantCode = Invoice.EntityCode NO-LOCK.
    
    /* Create the invoice document */
    CREATE NewDocument.
    ASSIGN
      NewDocument.BatchCode     = NewBatch.BatchCode
      NewDocument.DocumentType  = "INVC"
      NewDocument.Description   = Invoice.ToDetail
      NewDocument.Reference     = 'INV ' + STRING( Invoice.InvoiceNo, "99999" ).

    inv-list = inv-list + IF inv-list = "" THEN "" ELSE ",".
    inv-list = inv-list + STRING( Invoice.InvoiceNo ).
        
     /* Initialise temp-table */
     FOR EACH company-gst EXCLUSIVE-LOCK:
          DELETE company-gst.
     END.
       
     
    FOR EACH InvoiceLine OF Invoice NO-LOCK:
      RUN credit-account.
      IF RETURN-VALUE = "FAIL" THEN RETURN "FAIL".
      IF tenant-detailed THEN RUN debit-tenant( "Line" ).
      IF RETURN-VALUE = "FAIL" THEN RETURN "FAIL".
      
      /* Store Companies proportional GST */
      IF GST-Multi-Company = YES AND 
          gst-applies AND 
          Invoice.TaxApplies  AND
          InvoiceLine.EntityType <> 'T'  AND
          InvoiceLine.EntityType <> 'C' 
         THEN RUN update-gst-table.
    END.

    IF tenant-detailed THEN RUN debit-tenant( "GST" ).
                       ELSE RUN debit-tenant( "Both" ).
    IF RETURN-VALUE = "FAIL" THEN RETURN "FAIL".

    /* Create the GST Transactions if necessary */
    IF gst-applies AND Invoice.TaxApplies THEN RUN credit-account-gst.
    IF RETURN-VALUE = "FAIL" THEN RETURN "FAIL".

    IF Invoice.InvoiceType = "RENT" THEN RUN update-rent-charge-dates.
    IF RETURN-VALUE = "FAIL" THEN RETURN "FAIL".
    ASSIGN Invoice.InvoiceStatus = "A".
    
  END.
  
END.

RUN dispatch IN h-msg( 'destroy':U ).
IF invoice-printing <> No THEN DO:
  IF pdf-support THEN
    MESSAGE "Print to PDF?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO
    TITLE "PDF Printing ?" UPDATE pdf-printing.

  RUN process/report/taxinvce.p(
    "InvoiceList,"
    + inv-list
    + (IF pdf-printing THEN "~nOutputPDF" ELSE "")
  ).
END.
ELSE IF advice-of-charge = Yes THEN DO:
  RUN process/report/advice-of-charge.p( "InvoiceList," + inv-list ).
END.


MESSAGE "Invoices successfully Approved" VIEW-AS ALERT-BOX INFORMATION.
RUN open-inv-query.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE credit-account V-table-Win 
PROCEDURE credit-account :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR account-code LIKE ChartOfAccount.AccountCode NO-UNDO.
  
  account-code = IF CAN-FIND( ChartOfAccount WHERE ChartOfAccount.AccountCode = InvoiceLine.AccountCode
                                 AND LOOKUP( ChartOfAccount.AccountGroupCode, invoice-offset-groups) > 0 )
                 THEN InvoiceLine.AccountCode + offset-by
                 ELSE InvoiceLine.AccountCode.
    
  CREATE NewAcctTrans.
  ASSIGN
    NewAcctTrans.BatchCode       = NewBatch.BatchCode
    NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
    NewAcctTrans.EntityType      = (IF InvoiceLine.EntityType >= "A" THEN InvoiceLine.EntityType ELSE Tenant.EntityType)
    NewAcctTrans.EntityCode      = (IF InvoiceLine.EntityCode > 0 THEN InvoiceLine.EntityCode ELSE Tenant.EntityCode)
    NewAcctTrans.AccountCode     = account-code
    NewAcctTrans.Amount          = InvoiceLine.YourShare * -1
    NewAcctTrans.Date            = Invoice.InvoiceDate
    NewAcctTrans.Description     = (IF tenant-detailed THEN InvoiceLine.AccountText ELSE (IF invoice-line-text THEN InvoiceLine.AccountText ELSE Invoice.ToDetail))
    NewAcctTrans.Reference       = NewDocument.Reference
    NO-ERROR.

  IF ERROR-STATUS:ERROR THEN DO:
    MESSAGE "Error approving Invoice" Invoice.InvoiceNo SKIP(1)
            ERROR-STATUS:GET-MESSAGE(1) VIEW-AS ALERT-BOX ERROR
            TITLE "Invoice Approval Error".
    RETURN "FAIL".
  END.

  IF tenant-accounts THEN ASSIGN
    NewAcctTrans.EntityType      = "T"
    NewAcctTrans.EntityCode      = Tenant.TenantCode .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE credit-account-gst V-table-Win 
PROCEDURE credit-account-gst :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 DEF VAR tax-paid LIKE InvoiceLine.Amount INIT 0 NO-UNDO.

  IF GST-multi-company = YES  THEN RUN write-gst-transactions (OUTPUT tax-paid).
   
  /* if Not GST-multi-company OR there is residual tax (sometimes because of  rounding) */
  IF (invoice.TaxAmount - tax-paid) <> 0 THEN DO:
    CREATE NewAcctTrans.
    ASSIGN
      NewAcctTrans.BatchCode       = NewBatch.BatchCode
      NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
      NewAcctTrans.EntityType      = gst-type
      NewAcctTrans.EntityCode      = gst-code
      NewAcctTrans.AccountCode     = gst-out
      NewAcctTrans.Amount          = (Invoice.TaxAmount - tax-paid) * -1
      NewAcctTrans.Date            = Invoice.InvoiceDate
      NewAcctTrans.Description     = 'GST, ' + Invoice.ToDetail
      NewAcctTrans.Reference       = NewDocument.Reference.
 END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE debit-tenant V-table-Win 
PROCEDURE debit-tenant :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER debit-type AS CHAR NO-UNDO.

  IF debit-type = "Both" OR debit-type = "Line" THEN DO:
    CREATE NewAcctTrans.
    ASSIGN
      NewAcctTrans.BatchCode       = NewBatch.BatchCode
      NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
      NewAcctTrans.EntityType      = "T"
      NewAcctTrans.EntityCode      = Tenant.TenantCode
      NewAcctTrans.AccountCode     = sundry-debtors
      NewAcctTrans.Date            = Invoice.InvoiceDate
      NewAcctTrans.Reference       = NewDocument.Reference
      NO-ERROR.

    IF ERROR-STATUS:ERROR THEN RETURN "FAIL".

    IF debit-type = "Both" THEN ASSIGN
      NewAcctTrans.Description     = Invoice.ToDetail
      NewAcctTrans.Amount          = Invoice.Total + Invoice.TaxAmount.
    ELSE ASSIGN
      NewAcctTrans.Description     = InvoiceLine.AccountText
      NewAcctTrans.Amount          = InvoiceLine.YourShare.
  END.

  IF (debit-type = "Both" OR debit-type = "GST") 
            AND gst-applies 
            AND Invoice.TaxApplies
            AND Invoice.TaxAmount <> 0 
            AND invoice-tax-separately THEN DO:

    IF debit-type = "Both" THEN
      /* change the amount to exclude tax on the created transaction */
      NewAcctTrans.Amount          = Invoice.Total.

    /* create a tax debit to the tenant */
    CREATE NewAcctTrans.
    ASSIGN
      NewAcctTrans.BatchCode       = NewBatch.BatchCode
      NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
      NewAcctTrans.EntityType      = "T"
      NewAcctTrans.EntityCode      = Tenant.TenantCode
      NewAcctTrans.AccountCode     = sundry-debtors
      NewAcctTrans.Amount          = Invoice.TaxAmount
      NewAcctTrans.Date            = Invoice.InvoiceDate
      NewAcctTrans.Description     = "GST, " + Invoice.ToDetail
      NewAcctTrans.Reference       = NewDocument.Reference
      NO-ERROR.
  END.

  IF ERROR-STATUS:ERROR THEN RETURN "FAIL".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN open-inv-query.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-inv-query V-table-Win 
PROCEDURE open-inv-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  {&OPEN-QUERY-{&BROWSE-NAME}}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-all-invoices V-table-Win 
PROCEDURE select-all-invoices :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO INITIAL 1.
DEF VAR test-1 AS LOGI NO-UNDO.
DEF VAR test-2 AS LOGI NO-UNDO.

  IF NUM-RESULTS("{&BROWSE-NAME}") = 0 OR
     NUM-RESULTS("{&BROWSE-NAME}") = ? THEN RETURN.
     
  GET FIRST {&BROWSE-NAME}.
  REPOSITION {&BROWSE-NAME} TO ROWID ROWID( Invoice ).
  
  DO WHILE AVAILABLE Invoice:
   ASSIGN test-1 = BROWSE {&BROWSE-NAME}:IS-ROW-SELECTED(i) NO-ERROR.
   IF NOT test-1 THEN DO:
     ASSIGN test-2 = {&BROWSE-NAME}:SELECT-ROW(i) IN FRAME {&FRAME-NAME} NO-ERROR.
     IF NOT test-2 THEN DO:
       REPOSITION {&BROWSE-NAME} TO ROWID ROWID( Invoice ).
       i = 1.
       ASSIGN test-1 = BROWSE {&BROWSE-NAME}:IS-ROW-SELECTED(i) NO-ERROR.
       ASSIGN test-2 = {&BROWSE-NAME}:SELECT-ROW(i) IN FRAME {&FRAME-NAME} NO-ERROR.
     END.
     {&BROWSE-NAME}:SELECT-FOCUSED-ROW() IN FRAME {&FRAME-NAME}.
   END.
   GET NEXT {&BROWSE-NAME}.
   i = i + 1.
  END.  
  
  GET FIRST {&BROWSE-NAME}.
  REPOSITION {&BROWSE-NAME} TO ROWID ROWID( Invoice ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Invoice"}
  {src/adm/template/snd-list.i "Tenant"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-rent-charge-dates V-table-Win 
PROCEDURE update-rent-charge-dates :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.
DEF VAR charge-info AS CHAR NO-UNDO.
DEF VAR lease-code AS INT NO-UNDO.
DEF VAR sequence AS INT NO-UNDO.
DEF VAR start-date AS DATE NO-UNDO.
DEF VAR paid-up-to AS DATE NO-UNDO.

  n = NUM-ENTRIES( Invoice.ToPay ).
  DO i = 1 TO n:
    charge-info = ENTRY( i, Invoice.ToPay).
    lease-code = INT( ENTRY( 1, charge-info, "|")).
    sequence   = INT( ENTRY( 2, charge-info, "|")).
    start-date = DATE(ENTRY( 3, charge-info, "|")).
    paid-up-to = DATE(ENTRY( 4, charge-info, "|")).
    FIND RentChargeLine WHERE RentChargeLine.TenancyLeaseCode = lease-code
                        AND RentChargeLine.Sequence = sequence
                        AND RentChargeLine.StartDate = start-date
                        EXCLUSIVE-LOCK.
    IF paid-up-to > RentChargeLine.EndDate THEN ASSIGN
      RentChargeLine.LastChargedDate = RentChargeLine.EndDate
      RentChargeLine.RentChargeLineStatus = "P".
    ELSE
      RentChargeLine.LastChargedDate = paid-up-to.

    IF RentChargeLine.RentChargeLineStatus = "C" AND paid-up-to = RentChargeLine.EndDate THEN
      RentChargeLine.RentChargeLineStatus = "P".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-gst-table V-table-Win 
PROCEDURE update-gst-table :
/*------------------------------------------------------------------------------
  Purpose:  Updates the GST table that stores the proportion of GST to be allocated for each company 
                    for all the transactions in the current  document.   
                    2003/03/17 GLF  
------------------------------------------------------------------------------*/
DEF VAR v_eType  LIKE EntityType.EntityType  NO-UNDO.
DEF VAR v_eCode LIKE company.CompanyCode  NO-UNDO.

ASSIGN v_EType =InvoiceLine.EntityType
               v_ECode= InvoiceLine.EntityCode.

/* Drill up to the first company/ledger hierarchy */
IF v_eType <> 'L'  THEN DO:
    REPEAT WHILE v_eType <> 'L':
         IF v_eType = 'J'  THEN DO:
             FIND Project WHERE Project.ProjectCode = v_eCode NO-LOCK.
              ASSIGN   v_eType =Project.EntityType
                               v_eCode =Project.EntityCode.
          END.
          ELSE DO:
              IF v_eType = 'P' THEN DO:
                FIND Property WHERE Property.PropertyCode = v_eCode NO-LOCK.
                ASSIGN v_eType = 'L'
                                v_eCode = Property.CompanyCode.
              END.
         END.
    END. /* Repeat */
END.

/* Update amounts to company gst temporary table */
FIND company-gst WHERE company-gst.company_id  = v_eCode EXCLUSIVE-LOCK NO-ERROR.
IF AVAILABLE company-gst  THEN 
    company-gst.tax_amount  = company-gst.tax_amount + (InvoiceLine.Amount * (DECIMAL(gst-rate) / 100 )). 
ELSE DO:
    CREATE company-gst.
    ASSIGN company-gst.company_id = v_eCode
                    company-gst.tax_amount = InvoiceLine.Amount * (DECIMAL(gst-rate) / 100 ).
END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE write-gst-transactions V-table-Win 
PROCEDURE write-gst-transactions :
/*------------------------------------------------------------------------------
  Purpose:      Writes multiple gst transactions, one each for each company posted to.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEF OUTPUT PARAM tax-paid LIKE InvoiceLine.Amount.

FOR EACH company-gst:
    CREATE NewAcctTrans.
    ASSIGN
      NewAcctTrans.BatchCode       = NewBatch.BatchCode
      NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
      NewAcctTrans.EntityType      = "L"
      NewAcctTrans.EntityCode      = company-gst.company_id
      NewAcctTrans.AccountCode     = gst-out
      NewAcctTrans.Amount          = company-gst.Tax_Amount * -1
      NewAcctTrans.Date            = Invoice.InvoiceDate
      NewAcctTrans.Description     = 'GST, ' + Invoice.ToDetail
      NewAcctTrans.Reference       = NewDocument.Reference.
    tax-paid = tax-paid + company-gst.Tax_Amount.
 END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

        






