&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description: 
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

&SCOPED-DEFINE REPORT-ID "Property Schedule"

{inc/ofc-this.i}
{inc/ofc-set.i "Standard-Schedules" "standard-schedules"}
IF NOT AVAILABLE(OfficeSetting) THEN standard-schedules = "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Char2 RP.Int1 RP.Int2 RP.Int3 ~
RP.Date1 RP.Int4 RP.Char6 RP.Log2 RP.Log3 RP.Log4 RP.Log5 RP.Log6 RP.Log7 ~
RP.Char3 RP.Log1 RP.Log8 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_CompanyList cmb_Clients btn_Browse ~
btn_print RECT-29 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Char2 RP.Int1 RP.Int2 RP.Int3 ~
RP.Date1 RP.Int4 RP.Char6 RP.Log2 RP.Log3 RP.Log4 RP.Log5 RP.Log6 RP.Log7 ~
RP.Char3 RP.Log1 RP.Log8 
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define DISPLAYED-OBJECTS cmb_CompanyList cmb_Clients fil_prop1 ~
fil_prop2 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Browse 
     LABEL "Browse" 
     SIZE 5.72 BY 1.05
     FONT 10.

DEFINE BUTTON btn_print DEFAULT 
     LABEL "&OK" 
     SIZE 10.29 BY 1
     FONT 9.

DEFINE VARIABLE cmb_Clients AS CHARACTER FORMAT "X(256)":U 
     LABEL "Client" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 47.43 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_CompanyList AS CHARACTER FORMAT "X(256)":U 
     LABEL "List" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 47.43 BY 1 NO-UNDO.

DEFINE VARIABLE fil_prop1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 41.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_prop2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 41.29 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-29
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 71 BY 15.25.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.2 COL 2.14 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "All properties", "AP":U,
"Company List", "CompanyList":U,
"One Client", "OneClient":U,
"Single property", "1P":U,
"Range of properties", "RP":U
          SIZE 16.57 BY 4
          FONT 10
     RP.Char2 AT ROW 1.2 COL 26.58 HELP
          ""
          LABEL "Style" FORMAT "X(256)"
          VIEW-AS COMBO-BOX INNER-LINES 15
          LIST-ITEM-PAIRS "Trans Tasman Properties","AKLD",
                     "AmTrust Pacific Limited","APL",
                     "Prime Property Group"," PPG",
                     "Australian Growth Properties","SDNY",
                     "The George Group","TGG",
                     "TGG Management Report","TGG2",
                     "Old TGG Management Report","TGG3",
                     "MS Excel Export","XLS1",
                     "Excel Export for AGP","XLS2"
          DROP-DOWN-LIST
          SIZE 40.57 BY 1
     cmb_CompanyList AT ROW 2.5 COL 22 COLON-ALIGNED
     cmb_Clients AT ROW 3.5 COL 22 COLON-ALIGNED
     RP.Int1 AT ROW 5.2 COL 19 COLON-ALIGNED
          LABEL "Property" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_prop1 AT ROW 5.2 COL 28.14 COLON-ALIGNED NO-LABEL
     RP.Int2 AT ROW 6.2 COL 19 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_prop2 AT ROW 6.2 COL 28.14 COLON-ALIGNED NO-LABEL
     RP.Int3 AT ROW 8.2 COL 19 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 5.72 BY .9
     RP.Date1 AT ROW 8.2 COL 19 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 10 BY .9
     RP.Int4 AT ROW 8.2 COL 51.57 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 6.29 BY .9
     RP.Char6 AT ROW 9.5 COL 21 NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "Sort by Tenant No", "TenantNo":U,
"Sort by Floor", "Floor":U
          SIZE 37 BY 1
     RP.Log2 AT ROW 11.25 COL 9
          VIEW-AS TOGGLE-BOX
          SIZE 23.14 BY 1
          FONT 10
     RP.Log3 AT ROW 11.25 COL 32.43
          VIEW-AS TOGGLE-BOX
          SIZE 20.57 BY 1
          FONT 10
     RP.Log4 AT ROW 11.25 COL 53
          VIEW-AS TOGGLE-BOX
          SIZE 18 BY 1
          FONT 10
     RP.Log5 AT ROW 12.45 COL 9
          VIEW-AS TOGGLE-BOX
          SIZE 23.14 BY 1
          FONT 10
     RP.Log6 AT ROW 12.45 COL 32.43
          VIEW-AS TOGGLE-BOX
          SIZE 20.57 BY 1
          FONT 10
     RP.Log7 AT ROW 12.45 COL 53
          VIEW-AS TOGGLE-BOX
          SIZE 18 BY 1
     RP.Char3 AT ROW 13.85 COL 7 COLON-ALIGNED AUTO-RETURN 
          LABEL "File name"
          VIEW-AS FILL-IN 
          SIZE 55.72 BY 1.05
     btn_Browse AT ROW 13.85 COL 65
     RP.Log1 AT ROW 14.95 COL 9
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 10.29 BY .9
     RP.Log8 AT ROW 15.05 COL 32.43
          LABEL "Agents version"
          VIEW-AS TOGGLE-BOX
          SIZE 13.14 BY .9
     btn_print AT ROW 15.05 COL 60.43
     RECT-29 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_print.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 16.9
         WIDTH              = 82.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR RADIO-SET RP.Char1 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR COMBO-BOX RP.Char2 IN FRAME F-Main
   ALIGN-L EXP-LABEL EXP-FORMAT EXP-HELP                                */
/* SETTINGS FOR FILL-IN RP.Char3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_prop1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_prop2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log8 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON GO OF FRAME F-Main
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Browse V-table-Win
ON CHOOSE OF btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN run-report.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:

  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON VALUE-CHANGED OF RP.Char2 IN FRAME F-Main /* Style */
DO:
  RUN find-style-rp.
  RUN style-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Clients
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Clients V-table-Win
ON U1 OF cmb_Clients IN FRAME F-Main /* Client */
DO:
  {inc/selcmb/scclient1.i "RP" "Char5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Clients V-table-Win
ON U2 OF cmb_Clients IN FRAME F-Main /* Client */
DO:
  {inc/selcmb/scclient2.i "RP" "Char5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_CompanyList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_CompanyList V-table-Win
ON U1 OF cmb_CompanyList IN FRAME F-Main /* List */
DO:
  {inc/selcmb/sccls1.i "RP" "Char4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_CompanyList V-table-Win
ON U2 OF cmb_CompanyList IN FRAME F-Main /* List */
DO:
  {inc/selcmb/sccls2.i "RP" "Char4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_prop1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U1 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int1"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U2 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U3 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int1"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_prop2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U1 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int2"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U2 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int2"}      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U3 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int2"}      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Property */
DO:
  {inc/selcde/cdpro.i "fil_prop1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* To */
DO:
  {inc/selcde/cdpro.i "fil_prop2"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Log2 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log3 V-table-Win
ON VALUE-CHANGED OF RP.Log3 IN FRAME F-Main /* Log3 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log8
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log8 V-table-Win
ON VALUE-CHANGED OF RP.Log8 IN FRAME F-Main /* Agents version */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR style AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
/*   MESSAGE RP.Char2:SCREEN-VALUE VIEW-AS ALERT-BOX . */
  /* style = ENTRY( 1, INPUT RP.Char2, " "). */
  style = RP.Char2:SCREEN-VALUE.
  CASE style:
    WHEN "AGP" THEN DO:
      ASSIGN 
        RP.Int3:LABEL       = "x"
        RP.Int4:LABEL       = "x"
        RP.Log2:LABEL       = "Export"
        RP.Log3:LABEL       = "Warnings"
        RP.Log4:LABEL       = "Level Summaries"
        RP.Log5:LABEL       = "I&&E Summary"
        RP.Log6:LABEL       = "Pro-rate O/G"
        RP.Log7:LABEL       = "Show notes"
      .
      VIEW   /* RP.Int3 RP.Int4 */
             RP.Log2 RP.Log3 RP.Log4 RP.Log5 RP.Log6 RP.Log7 RP.Log8 .
      HIDE   RP.Date1 RP.Int3 RP.Int4 RP.Char3 btn_Browse
             /* RP.Log7 */.
      IF INPUT RP.Log2 THEN DO:
        HIDE RP.Log1.
        VIEW RP.Char3 btn_Browse.
      END.
      ELSE DO:
        HIDE RP.Char3 btn_Browse.
        VIEW RP.Log1.
      END.
      IF INPUT RP.Log8 THEN
        HIDE RP.Log4 RP.Log7 .
      ELSE
        VIEW RP.Log4 RP.Log7 .
    END.
    WHEN "SDNY" THEN DO:
      ASSIGN 
        RP.Log2:LABEL       = "Total Every Level"
        RP.Log3:LABEL       = "Show Future Rentals".
      VIEW   RP.Log1 RP.Log2 RP.Log3.
      HIDE   RP.Int3 RP.Int4 RP.Log7 RP.Char3 btn_Browse 
             RP.Date1 RP.Log8 RP.Log4 RP.Log5 RP.Log6 .
    END.
    WHEN "GLEN" THEN DO:
      ASSIGN 
        RP.Int3:LABEL       = "Max Sequence"
        RP.Int4:LABEL       = "Max Remaining Life"
        RP.Log2:LABEL       = "Areas"
        RP.Log3:LABEL       = "W.A.L.L."
        RP.Log4:LABEL       = "I&&E Summary"
        RP.Log5:LABEL       = "Level Reconciliation"
        RP.Log6:LABEL       = "Include 'X' spaces"
      .
      VIEW   RP.Log1 RP.Int3 RP.Int4
             RP.Log2 RP.Log3 RP.Log4 RP.Log5 RP.Log6 .
      HIDE   RP.Date1 RP.Log7 RP.Char3 btn_Browse RP.Log8 .
    END.
    WHEN "AKLD" THEN DO:
      ASSIGN 
        RP.Log2:LABEL       = "Main Schedule"
        RP.Log3:LABEL       = "Outgoings Page"
        RP.Log4:LABEL       = "O/G as Amounts"
        RP.Log5:LABEL       = "No Charged Rents"
        RP.Log6:LABEL       = "No Notes"
      .
      VIEW   RP.Log1 RP.Log2 RP.Log3 RP.Log5 RP.Log6.
      HIDE   RP.Date1 RP.Int3 RP.Int4 RP.Char3 btn_Browse
             RP.Log7 RP.Log8 .
      IF INPUT RP.Log3 THEN VIEW RP.Log4. ELSE HIDE RP.Log4.
    END.
    WHEN "TGG" THEN DO:
      ASSIGN 
        RP.Log2:LABEL       = "Main Schedule"
        RP.Log3:LABEL       = "Outgoings Page"
        RP.Log4:LABEL       = "O/G as Amounts"
        RP.Log5:LABEL       = "No Charged Rents"
        RP.Log6:LABEL       = "No Notes"
      .
      VIEW   RP.Log1 RP.Log2 RP.Log3 RP.Log5 RP.Log6.
      HIDE   RP.Int3 RP.Int4 RP.Char3 btn_Browse
             RP.Date1 RP.Log7 RP.Log8 .
      IF INPUT RP.Log3 THEN VIEW RP.Log4. ELSE HIDE RP.Log4.
    END.
    WHEN "TGG2" THEN DO:
      ASSIGN 
        RP.Log2:LABEL       = "Financials"
        RP.Log3:LABEL       = "Arrears"
        RP.Log4:LABEL       = "Vacancies"
        RP.Log5:LABEL       = "Building Income"
        RP.Log6:LABEL       = "Issues"
        RP.Log7:LABEL       = "Capital Works"
        RP.Date1:LABEL      = "As At Date"
      .
      VIEW   RP.Date1 RP.Log2 RP.Log3 RP.Log4 RP.Log5 RP.Log6 RP.Log7.
      HIDE   RP.Log1 RP.Int3 RP.Int4 RP.Char3 btn_Browse
             RP.Log8 RP.Char6 .
    END.
    WHEN "TGG3" THEN DO:
      ASSIGN 
        RP.Log2:LABEL       = "Financials"
        RP.Log3:LABEL       = "Arrears"
        RP.Log4:LABEL       = "Vacancies"
        RP.Log5:LABEL       = "Issues"
        RP.Log6:LABEL       = "Facilities Management"
        RP.Log7:LABEL       = "Building Income"
        RP.Date1:LABEL      = "As At Date"
      .
      VIEW   RP.Date1 RP.Log2 RP.Log3 RP.Log4 RP.Log5 RP.Log6 RP.Log7.
      HIDE   RP.Log1 RP.Int3 RP.Int4 RP.Char3 btn_Browse
             RP.Log8 .
    END.
    WHEN "AGNT" THEN DO:
      ASSIGN RP.Log2:LABEL       = "Main Schedule" .
      VIEW   RP.Log1 RP.Log2 .
      HIDE   RP.Log3 RP.Log4 RP.Int3 RP.Int4 RP.Char3 RP.Char6 btn_Browse
             RP.Date1 RP.Log4 RP.Log5 RP.Log6 RP.Log7 RP.Log8 .
    END.
    WHEN "APL" THEN DO:
      ASSIGN RP.Log2:LABEL = "Warnings"
             RP.Log3:LABEL = "Show Future Charges".
      VIEW   RP.Log1 RP.Log2 RP.Log3 RP.Log8 .
      HIDE   RP.Int3 RP.Int4 RP.Char3 RP.Char6 btn_Browse
             RP.Date1 RP.Log4 RP.Log5 RP.Log6 RP.Log7 .
    END.
     WHEN "PPG" THEN DO:
       ASSIGN RP.Log2:LABEL = "Warnings"
              RP.Log3:LABEL = "Show Future Charges".
       VIEW   RP.Char6 RP.Log1 RP.Log2 RP.Log3 RP.Log8 .
       HIDE   RP.Int3 RP.Int4 RP.Char3 btn_Browse
              RP.Date1 RP.Log4 RP.Log5 RP.Log6 RP.Log7 .
     END.
    WHEN "XLS1" THEN DO:
      ASSIGN RP.Log2:LABEL = "Main Schedule"
             RP.Log3:LABEL = "Outgoings"
             RP.Log4:LABEL = "Warnings".
      VIEW   RP.Log2 RP.Log3 RP.Log4 .
      HIDE   RP.Log1 RP.Int3 RP.Int4 RP.Char3 RP.Char6 btn_Browse
             RP.Date1 RP.Log5 RP.Log6 RP.Log7 RP.Log8 .
    END.
    WHEN "XLS2" THEN DO:
      ASSIGN RP.Log2:LABEL = "Warnings"
             RP.Log3:LABEL = "Show Future Rentals".
      VIEW   RP.Log2 RP.Log3.
      HIDE   RP.Log1 RP.Int3 RP.Int4 RP.Char3 RP.Char6 btn_Browse
             RP.Date1 RP.Log4 RP.Log5 RP.Log6 RP.Log7 RP.Log8 .
    END.
    OTHERWISE DO:
      HIDE   RP.Int3 RP.Int4 RP.Char3 RP.Char6 btn_Browse
             RP.Date1 RP.Log2 RP.Log3 RP.Log4 RP.Log5 RP.Log6 RP.Log7 RP.Log8 .
    END.
  END CASE.

  CASE INPUT RP.Char1:
    WHEN "1P" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
      VIEW RP.Int1 fil_prop1.
      HIDE RP.Int2 fil_prop2 cmb_CompanyList cmb_Clients.
    END.
    WHEN "RP" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = No" ).
      VIEW RP.Int1 RP.Int2 fil_prop1 fil_prop2.
      HIDE cmb_CompanyList cmb_Clients.
    END.
    WHEN "AP" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int1 RP.Int2 fil_prop1 fil_prop2 cmb_CompanyList cmb_Clients.
    END.
    WHEN "OneClient" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int1 RP.Int2 fil_prop1 fil_prop2 cmb_CompanyList.
      VIEW cmb_Clients.
    END.
    WHEN "CompanyList" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int1 RP.Int2 fil_prop1 fil_prop2 cmb_Clients .
      VIEW cmb_CompanyList.
    END.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE find-style-rp V-table-Win 
PROCEDURE find-style-rp :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR report-type AS CHAR NO-UNDO.
DEF VAR user-name AS CHAR NO-UNDO.

DEF BUFFER NRP FOR RP.

DO WITH FRAME {&FRAME-NAME}:
  report-type = INPUT RP.Char2.
  user-name = RP.UserName.
  FIND CURRENT RP NO-LOCK.
  IF CAN-FIND( RP WHERE RP.UserName = user-name AND RP.ReportID = {&REPORT-ID}  + report-type ) THEN
    FIND NRP WHERE NRP.UserName = user-name AND NRP.ReportID = {&REPORT-ID}  + report-type NO-LOCK NO-ERROR.
  ELSE DO:
    CREATE NRP.
    BUFFER-COPY RP TO NRP ASSIGN NRP.ReportID = {&REPORT-ID}  + report-type.
    FIND CURRENT NRP NO-LOCK.
  END.
  FIND RP WHERE RECID(RP) = RECID(NRP).
  DISPLAY   RP.Char3 RP.Int3 RP.Int4 RP.Log1 RP.Log2 RP.Log3 
            RP.Log4 RP.Log5 RP.Log6 RP.Log7 RP.Log8 .
  RUN dispatch( 'enable-fields':U ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.
DEF VAR report-type AS CHAR NO-UNDO.
  
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  FIND RP WHERE RP.UserName = user-name AND RP.ReportID = {&REPORT-ID} NO-ERROR.

DO WITH FRAME {&FRAME-NAME}:
  IF NOT AVAILABLE RP THEN DO:
  
    DEF VAR first-property LIKE Property.PropertyCode NO-UNDO.
    DEF VAR last-property  LIKE Property.PropertyCode NO-UNDO.
    
    FIND FIRST Property NO-LOCK. first-property = Property.PropertyCode.
    FIND LAST  Property NO-LOCK. last-property  = Property.PropertyCode.

    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Int1     = first-property
      RP.Int2     = last-property
      RP.Char2    = ""
      RP.Int3     = 999
      RP.Int4     = 10
      RP.Date1    = TODAY
      RP.Log1     = Yes
      RP.Log2     = Yes
      RP.Log3     = Yes
      RP.Log4     = Yes
      RP.Log5     = Yes
      .
  END.
  ELSE DO:
    report-type = RP.Char2.
    IF CAN-FIND( RP WHERE RP.UserName = user-name AND RP.ReportID = {&REPORT-ID}  + report-type ) THEN
      FIND RP WHERE RP.UserName = user-name AND RP.ReportID = {&REPORT-ID}  + report-type NO-ERROR.
    RP.ReportID = {&REPORT-ID}  + report-type.
  END.

  IF RP.Char2 = "" THEN RP.Char2    = RP.Char2:ENTRY(1) .

  RUN get-attribute( 'external-key':U ).
  IF RETURN-VALUE <> "" THEN RUN use-external-key( RETURN-VALUE ).

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN style-changed.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-update-record V-table-Win 
PROCEDURE local-update-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'update-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
DEF BUFFER Other_RP FOR RP.

  FIND Other_RP WHERE Other_RP.UserName = RP.UserName 
                AND Other_RP.ReportID = {&REPORT-ID} NO-ERROR.
  IF NOT AVAILABLE(Other_RP) THEN CREATE Other_RP.
  BUFFER-COPY RP TO Other_RP ASSIGN Other_RP.ReportID = {&REPORT-ID}.
  FIND CURRENT Other_RP NO-LOCK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR e1 AS INTEGER NO-UNDO.
DEF VAR e2 AS INTEGER NO-UNDO.
DEF VAR progname AS CHAR NO-UNDO.
DEF VAR report-options AS CHAR NO-UNDO.

  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).

DO WITH FRAME {&FRAME-NAME}:
  e1 = RP.Int1.
  e2 = RP.Int2.
  CASE RP.Char1:
    WHEN "1P" THEN e2 = e1.
    WHEN "RP" THEN .
    WHEN "AP" THEN ASSIGN e1 = 0    e2 = 99999.
  END.
  report-options = RP.Char1 + "|"
                 + STRING( RP.Int3 ) + "|"
                 + STRING( RP.Int4 ) + "|"
                 + STRING( RP.Log2 ) + "|"
                 + STRING( RP.Log3 ) + "|"
                 + STRING( RP.Log4 ) + "|"
                 + STRING( RP.Log5 ) + "|"
                 + STRING( RP.Log6 ) + "|"
                 + STRING( RP.Log7 ) + "|"
                 + "~nProperties," + STRING( e1 ) + "," + STRING( e2 )
                 + "~n" + RP.Int3:LABEL + "," + STRING( RP.Int3 )
                 + "~n" + RP.Int4:LABEL + "," + STRING( RP.Int4 )
                 + "~n" + RP.Log1:LABEL + "," + STRING( RP.Log1 )
                 + "~n" + RP.Date1:LABEL + "," + STRING( RP.Date1 )
                 + "~n" + RP.Log2:LABEL + "," + STRING( RP.Log2 )
                 + "~n" + RP.Log3:LABEL + "," + STRING( RP.Log3 )
                 + "~n" + RP.Log4:LABEL + "," + STRING( RP.Log4 )
                 + "~n" + RP.Log5:LABEL + "," + STRING( RP.Log5 )
                 + "~n" + RP.Log6:LABEL + "," + STRING( RP.Log6 )
                 + "~n" + RP.Log7:LABEL + "," + STRING( RP.Log7 )
                 + "~n" + RP.Log8:LABEL + "," + STRING( RP.Log8 )
                 + "~nSelection," + STRING( RP.Char1 )
                 + (IF RP.Char1 = "CompanyList" THEN "~nCompanyList," + STRING( RP.Char4 ) ELSE "")
                 + (IF RP.Char1 = "OneClient" THEN "~nClient," + STRING( RP.Char5 ) ELSE "")
                 + "~n" + RP.Char2:LABEL + "," + STRING( RP.Char2 )
                 + "~n" + RP.Char3:LABEL + "," + STRING( RP.Char3 )
                 + "~n" +  "Sortby," + STRING( RP.Char6 )
                 .
END.


  progname = "process/report/schd" + RP.Char2 + ".p".
  IF SEARCH(progname) <> ? THEN DO:
    IF LOOKUP( ENTRY( 1, RP.Char2, " "), "AGP,AKLD,APL,PPG,SDNY,TGG,TGG2,TGG3,XLS1,XLS2") > 0 THEN
      RUN VALUE(progname) (  report-options ) NO-ERROR.
    ELSE
      RUN VALUE(progname) (  report-options,
                             RP.Log1,    /* Preview */
                             e1, e2      /* Entity range */
                          ) NO-ERROR.
  END.
  ELSE DO:
    MESSAGE "The schedule format for " + ENTRY( 1, RP.Char2, " ") + " is not available."
             VIEW-AS ALERT-BOX ERROR.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char3 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated values" "*.CSV"
        SAVE-AS ASK-OVERWRITE CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV"
        INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME UPDATE select-ok.

  IF select-ok THEN DO:
    IF SUBSTRING( file-name, R-INDEX( file-name, ".") ) <> ".CSV" THEN DO:
      IF (LENGTH( file-name ) - R-INDEX( file-name, "." )) < 4 THEN
        file-name = SUBSTRING( file-name, 1, R-INDEX( file-name, ".")) + "CSV" .
    END.
    RP.Char3:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win 
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE style-changed V-table-Win 
PROCEDURE style-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR style AS CHAR NO-UNDO.
DEF VAR current-value AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  style = ENTRY( 1, INPUT RP.Char2, " ").
  IF LOOKUP( style, "AKLD,XLS1") > 0 THEN DO:
    IF RP.Char1:NUM-BUTTONS <> 5 THEN DO:
      RP.Char1:RADIO-BUTTONS = "All properties,AP,Company List,CompanyList,One Client,OneClient,Single property,1P,Range of properties,RP".
    END.
  END.
  ELSE DO:
    IF RP.Char1:NUM-BUTTONS <> 3 THEN DO:
      current-value = INPUT RP.Char1.
      RP.Char1:RADIO-BUTTONS = "All properties,AP,Single property,1P,Range of properties,RP".
      IF LOOKUP( current-value, "AP,1P,RP" ) = 0 THEN
        RP.Char1:SCREEN-VALUE = "1P".
      ELSE
        RP.Char1:SCREEN-VALUE = current-value.

    END.
  END.
  IF NOT (standard-schedules = "" OR CAN-DO( standard-schedules, ENTRY(1,INPUT RP.Char2," "))) THEN DO:
    MESSAGE "The" ENTRY(1,INPUT RP.Char2," ") "schedule is not one of the office" SKIP
            "standard schedule formats." SKIP(1)
            "You should check the output for correctness against the" SKIP
            "office standards (" + standard-schedules + ") before using it."
            VIEW-AS ALERT-BOX WARNING
            TITLE "Warning: non-standard schedule format".
  END.
END.

  RUN enable-appropriate-fields.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-external-key V-table-Win 
PROCEDURE use-external-key :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-name AS CHAR NO-UNDO.

DEF VAR link-handle AS CHAR NO-UNDO.
DEF VAR source-browse AS WIDGET-HANDLE NO-UNDO.
DEF VAR foreign-key AS CHAR NO-UNDO.
DEF VAR user-name AS CHAR NO-UNDO.

  RUN get-link-handle IN adm-broker-hdl (INPUT THIS-PROCEDURE,
                INPUT "RECORD-SOURCE":U, OUTPUT link-handle) NO-ERROR.
  IF link-handle NE "":U THEN DO:
    source-browse = WIDGET-HANDLE(ENTRY(1,link-handle)).
    RUN send-key IN source-browse ( INPUT new-name, OUTPUT foreign-key ).

    RP.Char1 = "1P":U.
    RP.Int1 = INTEGER( foreign-key ).
    FIND Property WHERE Property.PropertyCode = RP.Int1 NO-LOCK NO-ERROR.
    fil_Prop1 = IF AVAILABLE(Property) THEN Property.Name ELSE "* * * Unknown Property * * *".

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF INPUT FRAME {&FRAME-NAME} RP.Char1 = "RP"
     AND (INPUT FRAME {&FRAME-NAME} RP.Int1 > INPUT FRAME {&FRAME-NAME} RP.Int2 )
  THEN DO:
    MESSAGE "The TO property must be greater than the from property !"
      VIEW-AS ALERT-BOX ERROR.
    APPLY 'ENTRY':U TO RP.Int1 IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END. 
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

