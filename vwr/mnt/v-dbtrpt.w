&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "Andrew" NO-UNDO.

DEF VAR portfolio-list AS CHAR NO-UNDO.
DEF VAR portfolio-pd   AS CHAR NO-UNDO.
DEF VAR manager-list   AS CHAR NO-UNDO.
DEF VAR manager-pd     AS CHAR NO-UNDO.
DEF VAR region-list    AS CHAR NO-UNDO.
DEF VAR region-pd      AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char2 RP.Int1 RP.Int2 RP.Char1 RP.Dec1 ~
RP.Log5 RP.Dec2 RP.Log4 RP.Int4 RP.Log3 RP.Log2 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}~
 ~{&FP1}Dec1 ~{&FP2}Dec1 ~{&FP3}~
 ~{&FP1}Dec2 ~{&FP2}Dec2 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-24 cmb_Select1 cmb_Select2 ~
cmb_ConsolidationList cmb_month Btn_OK 
&Scoped-Define DISPLAYED-FIELDS RP.Char2 RP.Int1 RP.Int2 RP.Char1 RP.Dec1 ~
RP.Log5 RP.Dec2 RP.Log4 RP.Int4 RP.Log3 RP.Log2 
&Scoped-Define DISPLAYED-OBJECTS fil_Company fil_Property cmb_Select1 ~
cmb_Select2 cmb_ConsolidationList cmb_month 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 11.72 BY 1.1
     BGCOLOR 8 FONT 9.

DEFINE VARIABLE cmb_ConsolidationList AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     SIZE 38.29 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_month AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "","" 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Select1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Select 1" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "","" 
     SIZE 40.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_Select2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Select 2" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "","" 
     SIZE 40.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Company AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 37.43 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Property AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 37.43 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-24
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 69.43 BY 14.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char2 AT ROW 1.1 COL 3.86 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "One Property", "1P":U,
"One Company", "1G":U,
"One Region", "1R":U,
"One Building Manager", "1M":U,
"Range of Regions", "RR":U,
"Portfolio - by Region", "AR":U,
"Portfolio - by Company", "AG":U,
"Portfolio - by Manager", "AM":U,
"Consolidation List", "List":U
          SIZE 18.29 BY 6.4
          FONT 10
     RP.Int1 AT ROW 1.3 COL 20.72 COLON-ALIGNED NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1
     RP.Int2 AT ROW 1.3 COL 20.72 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1
     fil_Company AT ROW 1.3 COL 30.14 COLON-ALIGNED NO-LABEL
     fil_Property AT ROW 1.3 COL 30.14 COLON-ALIGNED NO-LABEL
     cmb_Select1 AT ROW 2.6 COL 27.29 COLON-ALIGNED
     cmb_Select2 AT ROW 3.7 COL 27.29 COLON-ALIGNED
     cmb_ConsolidationList AT ROW 6 COL 23 COLON-ALIGNED NO-LABEL
     RP.Char1 AT ROW 8.2 COL 3.86 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "As at Today", "TODAY":U,
"At a particular month ending", "MONTH":U
          SIZE 24 BY 1.6
          FONT 10
     cmb_month AT ROW 8.8 COL 27.86 NO-LABEL
     RP.Dec1 AT ROW 10.5 COL 14.14 COLON-ALIGNED
          LABEL "Minimum current" FORMAT "->,>>>,>>9.99"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
          FONT 10
     RP.Log5 AT ROW 10.6 COL 29.57
          LABEL "Only include current debit balances"
          VIEW-AS TOGGLE-BOX
          SIZE 28.57 BY .85
     RP.Dec2 AT ROW 12.1 COL 25.57 COLON-ALIGNED
          LABEL "Minimum" FORMAT "->,>>>,>>9.99"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1.05
     RP.Log4 AT ROW 12.2 COL 3.57
          LABEL "Overdue tests:"
          VIEW-AS TOGGLE-BOX
          SIZE 12.86 BY .85
     RP.Int4 AT ROW 12.2 COL 42.14 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "30", 1,
"60", 2,
"90 days", 3
          SIZE 22 BY .9
     RP.Log3 AT ROW 13.2 COL 3.57
          LABEL "Summarise part-closed transaction groups"
          VIEW-AS TOGGLE-BOX
          SIZE 30.86 BY 1
          FONT 10
     Btn_OK AT ROW 14.2 COL 57.86
     RP.Log2 AT ROW 14.3 COL 3.57 HELP
          ""
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 8.57 BY 1
          FONT 10
     RECT-24 AT ROW 1 COL 1
     "by" VIEW-AS TEXT
          SIZE 2 BY .9 AT ROW 12.2 COL 39.86
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 20.1
         WIDTH              = 89.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR RADIO-SET RP.Char2 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR COMBO-BOX cmb_month IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN RP.Dec1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Dec2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN fil_Company IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Property IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR RADIO-SET RP.Int4 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN month-ending-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON VALUE-CHANGED OF RP.Char2 IN FRAME F-Main /* Char2 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_ConsolidationList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ConsolidationList V-table-Win
ON U1 OF cmb_ConsolidationList IN FRAME F-Main
DO:
  {inc/selcmb/sccls1.i "RP" "Char4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ConsolidationList V-table-Win
ON U2 OF cmb_ConsolidationList IN FRAME F-Main
DO:
  {inc/selcmb/sccls2.i "RP" "Char4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month V-table-Win
ON U1 OF cmb_month IN FRAME F-Main
DO:
  {inc/selcmb/scmthe1.i "RP" "Int3"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month V-table-Win
ON U2 OF cmb_month IN FRAME F-Main
DO:
  {inc/selcmb/scmthe2.i "RP" "Int3"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Select2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Select2 V-table-Win
ON U1 OF cmb_Select2 IN FRAME F-Main /* Select 2 */
DO:
  {inc/selcmb/scrgn1.i "RP" "Char3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Select2 V-table-Win
ON U2 OF cmb_Select2 IN FRAME F-Main /* Select 2 */
DO:
  {inc/selcmb/scrgn2.i "RP" "Char3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Company
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U1 OF fil_Company IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U2 OF fil_Company IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U3 OF fil_Company IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U1 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U2 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U3 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Int1 */
DO:
  {inc/selcde/cdpro.i "fil_Property"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main
DO:
  {inc/selcde/cdcmp.i "fil_Company"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Overdue tests: */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose: As its name!
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  CASE INPUT RP.Char2:
      WHEN "1P" THEN DO:
        VIEW RP.Int1 fil_Property .
        HIDE cmb_Select1 cmb_Select2 cmb_ConsolidationList RP.Int2 fil_Company .
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = No" ).
      END.
      WHEN "1G" THEN DO:
        VIEW RP.Int2 fil_Company .
        HIDE cmb_Select1 cmb_Select2 cmb_ConsolidationList RP.Int1 fil_Property.
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Company:HANDLE ), "HIDDEN = No" ).
      END.
      WHEN "1R" THEN DO:
        RUN get-regions.

        cmb_Select1:LIST-ITEMS   = region-list.
        cmb_Select1:PRIVATE-DATA = region-pd.
        cmb_Select1:SCREEN-VALUE = ENTRY( 1, region-list ).

        VIEW cmb_Select1 . cmb_Select1:LABEL = "Region".
        HIDE RP.Int1 cmb_Select2 fil_Property cmb_ConsolidationList RP.Int2 fil_Company .
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = Yes" ).
      END.
      WHEN "1M" THEN DO:
        RUN get-managers.
        cmb_Select1:LIST-ITEMS   = manager-list.
        cmb_Select1:PRIVATE-DATA = manager-pd.
        cmb_Select1:SCREEN-VALUE = ENTRY( 1, manager-list ).

        VIEW cmb_Select1. cmb_Select1:LABEL = "Manager".
        HIDE RP.Int1 fil_Property cmb_Select2 cmb_ConsolidationList RP.Int2 fil_Company .
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = Yes" ).
      END.
      WHEN "RR" THEN DO:
        RUN get-regions.
        
        cmb_Select1:LIST-ITEMS   = region-list.
        cmb_Select1:PRIVATE-DATA = region-pd.
        cmb_Select1:SCREEN-VALUE = ENTRY( 1, region-list ).
        
        cmb_Select2:LIST-ITEMS   = region-list.
        cmb_Select2:PRIVATE-DATA = region-pd.
        cmb_Select2:SCREEN-VALUE = ENTRY( 1, region-list ).

        VIEW cmb_Select1 cmb_Select2.
        cmb_Select1:LABEL = "Region". cmb_Select2:LABEL = "To".
        HIDE RP.Int1 fil_Property cmb_ConsolidationList RP.Int2 fil_Company .
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = Yes" ).
      END.
      WHEN "List" THEN DO:
        HIDE cmb_Select1 cmb_Select2 RP.Int1 fil_Property RP.Int2 fil_Company .
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = Yes" ).
        VIEW cmb_ConsolidationList .
      END.
      OTHERWISE DO:
        HIDE cmb_ConsolidationList cmb_Select1 cmb_Select2 RP.Int1 fil_Property RP.Int2 fil_Company .
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = Yes" ).
      END.
  END CASE.

  IF INPUT RP.Log4 THEN DO:
    ENABLE RP.Int4 RP.Dec2 .
  END.
  ELSE DO:
    DISABLE RP.Int4 RP.Dec2 .
  END.

  RUN month-ending-changed.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-managers V-table-Win 
PROCEDURE get-managers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF manager-list <> "" THEN RETURN.
  
  FOR EACH Property NO-LOCK
    BREAK BY Property.Administrator:
    IF FIRST-OF( Property.Administrator ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Administrator
        NO-LOCK NO-ERROR.

      manager-list = manager-list + IF manager-list = "" THEN "" ELSE ",".
      manager-list = manager-list +
        ( IF AVAILABLE Person THEN Person.FirstName + ' ' + Person.LastName ELSE "Not Managed" ).

      manager-pd = manager-pd + IF manager-pd = "" THEN "" ELSE ",".
      manager-pd = manager-pd + IF AVAILABLE Person THEN STRING( Person.PersonCode ) ELSE STRING( Property.Administrator ).
    END.
  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-portfolios V-table-Win 
PROCEDURE get-portfolios :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF portfolio-list <> "" THEN RETURN.
  
  FOR EACH Property NO-LOCK
    BREAK BY Property.Manager:
    IF FIRST-OF( Property.Manager ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Manager
        NO-LOCK NO-ERROR.

      portfolio-list = portfolio-list + IF portfolio-list = "" THEN "" ELSE ",".
      portfolio-list = portfolio-list +
        ( IF AVAILABLE Person THEN Person.JobTitle ELSE "Not in a Portfolio" ).

      portfolio-pd = portfolio-pd + IF portfolio-pd = "" THEN "" ELSE ",".
      portfolio-pd = portfolio-pd + IF AVAILABLE Person THEN STRING( Person.PersonCode ) ELSE STRING( Property.Manager ).
    END.
  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-regions V-table-Win 
PROCEDURE get-regions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF region-list <> "" THEN RETURN.
  
  FOR EACH Region NO-LOCK:

    region-list = region-list + IF region-list = "" THEN "" ELSE ",".
    region-list = region-list + STRING( Region.Region, "X(5)") + "- " + Region.Name.

    region-pd = region-pd + IF region-pd = "" THEN "" ELSE ",".
    region-pd = region-pd + Region.Region.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/

&SCOP RPT-NAME "debtors"
  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  FIND RP WHERE RP.ReportID = {&RPT-NAME}
                        AND RP.UserName = user-name
                        NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&RPT-NAME}
      RP.UserName = user-name
    .
  END.

  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  RUN enable-appropriate-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE month-ending-changed V-table-Win 
PROCEDURE month-ending-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:
    cmb_month:SENSITIVE = INPUT FRAME {&FRAME-NAME} RP.Char1 = 'MONTH':U.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program.
------------------------------------------------------------------------------*/
DEF VAR asat-date AS DATE NO-UNDO.
DEF VAR report-options AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  RUN dispatch IN THIS-PROCEDURE ('update-record':U).
  IF RP.Char1 = "TODAY" THEN asat-date = TODAY. ELSE DO:
    FIND Month WHERE Month.MonthCode = RP.Int3 NO-LOCK.
    asat-date = Month.EndDate .
  END.

  report-options = (IF RP.Log3 THEN "SummarisePart" ELSE "")
                 + "~nSortBy,".
  DEF VAR select-1 AS CHAR NO-UNDO.
  DEF VAR select-2 AS CHAR NO-UNDO.
  
  select-1 = ENTRY( cmb_Select1:LOOKUP( cmb_Select1:SCREEN-VALUE ),
    cmb_Select1:PRIVATE-DATA ).
  select-2 = ENTRY( cmb_Select2:LOOKUP( cmb_Select2:SCREEN-VALUE ),
    cmb_Select1:PRIVATE-DATA ).

  CASE RP.Char2:
    WHEN "1P" THEN ASSIGN
      report-options = report-options + "Property~nRecordRange," + STRING(RP.Int1) + "," + STRING(RP.Int1).
    WHEN "1G" THEN ASSIGN
      report-options = report-options + "Company~nRecordRange," + STRING(RP.Int2) + "," + STRING(RP.Int2).
    WHEN "1R" THEN ASSIGN
      report-options = report-options + "Region~nRecordRange," + select-1 + "," + select-1.
    WHEN "1M" THEN ASSIGN
      report-options = report-options + "Manager~nRecordRange," + select-1 + "," + select-1.
    WHEN "RR" THEN ASSIGN
      report-options = report-options + "Region~nRecordRange," + select-1 +  "," + select-2.
    WHEN "AR" THEN ASSIGN
      report-options = report-options + "Region~nAllRecords".
    WHEN "AG" THEN ASSIGN
      report-options = report-options + "Company~nAllRecords".
    WHEN "AM" THEN ASSIGN
      report-options = report-options + "Manager~nAllRecords".
    WHEN "List" THEN ASSIGN
      report-options = report-options + "Company~nConsolidationList," + RP.Char4.
  END CASE.

  report-options = report-options
                 + (IF RP.Log5 THEN "~nMinDebitBalance," ELSE "~nMinAbsBalance,") + STRING( RP.Dec1 )
                 + (IF RP.Log4 THEN "~nOverduePeriods,"  + STRING(RP.Int4) + "~nOverdueBalance," + STRING(RP.Dec2) ELSE "")
                 + "~nAsAtDate," + STRING( asat-date, "99/99/9999")
                 + (IF RP.Log2 THEN "~nPreview" ELSE "").

{inc/bq-do.i "process/report/debtors.p" "report-options" "NOT RP.Log2"}

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


