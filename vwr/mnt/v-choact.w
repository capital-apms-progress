&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
{inc/topic/tpchoact.i}

DEF VAR mode AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES ChartOfAccount
&Scoped-define FIRST-EXTERNAL-TABLE ChartOfAccount


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR ChartOfAccount.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS ChartOfAccount.AccountCode ~
ChartOfAccount.HighVolume ChartOfAccount.Name ChartOfAccount.ShortName ~
ChartOfAccount.Security ChartOfAccount.AlternativeCode ~
ChartOfAccount.ExpenseRecoveryType 
&Scoped-define ENABLED-TABLES ChartOfAccount
&Scoped-define FIRST-ENABLED-TABLE ChartOfAccount
&Scoped-define DISPLAYED-TABLES ChartOfAccount
&Scoped-define FIRST-DISPLAYED-TABLE ChartOfAccount
&Scoped-Define ENABLED-OBJECTS cmb_AccountGroup tgl_property tgl_tenant ~
tgl_ledger tgl_creditor tgl_Project RECT-1 RECT-24 
&Scoped-Define DISPLAYED-FIELDS ChartOfAccount.AccountCode ~
ChartOfAccount.HighVolume ChartOfAccount.Name ChartOfAccount.ShortName ~
ChartOfAccount.Security ChartOfAccount.AlternativeCode ~
ChartOfAccount.ExpenseRecoveryType 
&Scoped-Define DISPLAYED-OBJECTS cmb_AccountGroup tgl_property tgl_tenant ~
tgl_ledger tgl_creditor tgl_Project 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_AccountGroup AS CHARACTER FORMAT "X(80)" 
     LABEL "Group" 
     VIEW-AS COMBO-BOX INNER-LINES 25
     DROP-DOWN-LIST
     SIZE 50.86 BY 1.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 61.14 BY 12.5.

DEFINE RECTANGLE RECT-24
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 29.14 BY 3.4.

DEFINE VARIABLE tgl_creditor AS LOGICAL INITIAL no 
     LABEL "Creditors" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.72 BY .8
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_ledger AS LOGICAL INITIAL no 
     LABEL "Ledgers" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.72 BY .8
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_Project AS LOGICAL INITIAL no 
     LABEL "Projects" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.72 BY .8
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_property AS LOGICAL INITIAL no 
     LABEL "Properties" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.72 BY .8
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_tenant AS LOGICAL INITIAL no 
     LABEL "Tenants" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.72 BY .8
     FONT 10 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     ChartOfAccount.AccountCode AT ROW 1.2 COL 8.72 COLON-ALIGNED
          LABEL "Code"
          VIEW-AS FILL-IN 
          SIZE 9.14 BY 1
     ChartOfAccount.HighVolume AT ROW 1.2 COL 48.43
          VIEW-AS TOGGLE-BOX
          SIZE 12 BY 1
          FONT 10
     cmb_AccountGroup AT ROW 2.2 COL 5.72
     ChartOfAccount.Name AT ROW 3.2 COL 8.72 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 50.86 BY 1
     ChartOfAccount.ShortName AT ROW 4.2 COL 8.72 COLON-ALIGNED
          LABEL "Short"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     ChartOfAccount.Security AT ROW 4.2 COL 48.14 COLON-ALIGNED
          LABEL "Account Security"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     ChartOfAccount.AlternativeCode AT ROW 5.4 COL 10.72 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 50.86 BY 3.85
     ChartOfAccount.ExpenseRecoveryType AT ROW 9.75 COL 38
          LABEL "Expense Recovery Type"
          VIEW-AS COMBO-BOX INNER-LINES 15
          LIST-ITEMS "P","O","X","N","I","E" 
          DROP-DOWN-LIST
          SIZE 6.14 BY 1
     tgl_property AT ROW 10.35 COL 5.29
     tgl_tenant AT ROW 10.35 COL 19.57
     tgl_ledger AT ROW 11.15 COL 5.29
     tgl_creditor AT ROW 11.15 COL 19.57
     tgl_Project AT ROW 11.95 COL 5.29
     RECT-1 AT ROW 1 COL 1
     RECT-24 AT ROW 9.75 COL 3
     "Can Update Through:" VIEW-AS TEXT
          SIZE 21.72 BY .6 AT ROW 9.75 COL 5.29
          FONT 14
     "Description:" VIEW-AS TEXT
          SIZE 8 BY .65 AT ROW 5.5 COL 2.4
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.ChartOfAccount
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 16.25
         WIDTH              = 67.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN ChartOfAccount.AccountCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR Editor ChartOfAccount.AlternativeCode IN FRAME F-Main
   EXP-LABEL                                                            */
ASSIGN 
       ChartOfAccount.AlternativeCode:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR COMBO-BOX cmb_AccountGroup IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX ChartOfAccount.ExpenseRecoveryType IN FRAME F-Main
   ALIGN-L EXP-LABEL                                                    */
/* SETTINGS FOR FILL-IN ChartOfAccount.Security IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN ChartOfAccount.ShortName IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME cmb_AccountGroup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AccountGroup V-table-Win
ON U1 OF cmb_AccountGroup IN FRAME F-Main /* Group */
DO:
  {inc/selcmb/scacg1.i "ChartOfAccount" "AccountGroupCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AccountGroup V-table-Win
ON U2 OF cmb_AccountGroup IN FRAME F-Main /* Group */
DO:
  {inc/selcmb/scacg2.i "ChartOfAccount" "AccountGroupCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "ChartOfAccount"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "ChartOfAccount"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-coa. ELSE RUN check-modified( 'CLEAR':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-account.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  RUN dispatch( 'update-record':U ).
  IF mode = "Add" THEN RUN notify( 'open-query, record-source':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-coa V-table-Win 
PROCEDURE delete-coa :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT ChartOfAccount EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE ChartOfAccount THEN DELETE ChartOfAccount.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-updateto-string V-table-Win 
PROCEDURE get-updateto-string :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF OUTPUT PARAMETER update-to AS CHAR NO-UNDO.
  
  ASSIGN FRAME {&FRAME-NAME}
    tgl_property
    tgl_ledger
    tgl_project
    tgl_tenant
    tgl_creditor.

  IF tgl_property THEN update-to = update-to + "P".
  IF tgl_ledger   THEN update-to = update-to + "L".
  IF tgl_project  THEN update-to = update-to + "J".
  IF tgl_tenant   THEN update-to = update-to + "T".
  IF tgl_creditor THEN update-to = update-to + "C".

  DEF VAR i AS INT NO-UNDO.
  DEF VAR comma-list AS CHAR NO-UNDO.  
  DO i = 1 TO LENGTH( update-to ):
    comma-list = comma-list + IF i = 1 THEN "" ELSE ",".
    comma-list = comma-list + SUBSTR( update-to, i, 1 ).
  END.

  update-to = comma-list.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN get-updateto-string( OUTPUT ChartOfAccount.UpdateTo ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  tgl_property = INDEX( ChartOfAccount.UpdateTo, "P" ) <> 0.
  tgl_ledger   = INDEX( ChartOfAccount.UpdateTo, "L" ) <> 0. 
  tgl_project  = INDEX( ChartOfAccount.UpdateTo, "J" ) <> 0.
  tgl_tenant   = INDEX( ChartOfAccount.UpdateTo, "T" ) <> 0.
  tgl_creditor = INDEX( ChartOfAccount.UpdateTo, "C" ) <> 0.
  
  DISPLAY
    tgl_property
    tgl_ledger
    tgl_project
    tgl_tenant
    tgl_creditor
  WITH FRAME {&FRAME-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN
    ENABLE ChartOfAccount.AccountCode WITH FRAME {&FRAME-NAME}.
  ELSE
    DISABLE ChartOfAccount.AccountCode WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN get-attribute( 'mode' ).
  mode = RETURN-VALUE.
  
  IF mode = "Add" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  
  RUN dispatch( 'enable-fields':U ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = 0 EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE(ChartOfAccount) THEN DO:
    CREATE ChartOfAccount.
    ASSIGN
      ChartOfAccount.AccountCode = 0000.00.
  END.

  RUN dispatch( 'display-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = 'WINDOW-CLOSE':U THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ChartOfAccount"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-account V-table-Win 
PROCEDURE verify-account :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR code LIKE ChartOfAccount.AccountCode NO-UNDO.
  
  code = INPUT FRAME {&FRAME-NAME} ChartOfAccount.AccountCode.

  
  IF code = 0 THEN DO:
    MESSAGE "Account code 0000.00 is invalid!"
      VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
    APPLY 'ENTRY':U TO ChartOfAccount.AccountCode IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.
  
  IF ChartOfAccount.AccountCode <> code AND
    CAN-FIND( ChartOfAccount WHERE ChartOfAccount.AccountCode = code ) THEN
  DO:
    MESSAGE "An account already exists with account code" STRING( code, "9999.99" ) "."
      VIEW-AS ALERT-BOX ERROR TITLE "Account already exists".
    APPLY 'ENTRY':U TO ChartOfAccount.AccountCode IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.

  IF INPUT FRAME {&FRAME-NAME} cmb_accountGroup = ? OR 
     INPUT FRAME {&FRAME-NAME} cmb_accountGroup = "" THEN DO:
    MESSAGE "You must select an account group!" VIEW-AS ALERT-BOX ERROR
      TITLE "No Account Group selected".
    APPLY 'ENTRY':U TO cmb_Accountgroup IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.
  
  IF INPUT FRAME {&FRAME-NAME} ChartOfAccount.Name = "" THEN DO:
    MESSAGE "You need to enter a name for this account!" VIEW-AS ALERT-BOX ERROR
      TITLE "No Account Name entered".
    APPLY 'ENTRY':U TO ChartOfAccount.Name IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

