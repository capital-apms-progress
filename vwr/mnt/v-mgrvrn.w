&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&SCOPED-DEFINE REPORT-ID "Mgmt - RVRN"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Log2 RP.Log3 RP.Log1 RP.Char2 RP.Char3 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Char3 ~{&FP2}Char3 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-32 fil_Date1 fil_Date2 fil_AsAtDate ~
btn_browse btn_OK 
&Scoped-Define DISPLAYED-FIELDS RP.Log2 RP.Log3 RP.Log1 RP.Char2 RP.Char3 
&Scoped-Define DISPLAYED-OBJECTS fil_Date1 fil_Date2 fil_AsAtDate 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_browse 
     LABEL "&Browse" 
     SIZE 7.43 BY 1.05
     FONT 9.

DEFINE BUTTON btn_OK DEFAULT 
     LABEL "&OK" 
     SIZE 9.72 BY 1.05
     FONT 9.

DEFINE VARIABLE fil_AsAtDate AS DATE FORMAT "99/99/9999":U 
     LABEL "Print As At Date" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Date1 AS DATE FORMAT "99/99/9999":U 
     LABEL "Report Start" 
     VIEW-AS FILL-IN 
     SIZE 11.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Date2 AS DATE FORMAT "99/99/9999":U 
     LABEL "Report End" 
     VIEW-AS FILL-IN 
     SIZE 11.57 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-32
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 60 BY 13.4.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_Date1 AT ROW 1.6 COL 19 COLON-ALIGNED
     fil_Date2 AT ROW 2.6 COL 19 COLON-ALIGNED
     RP.Log2 AT ROW 4.2 COL 21
          LABEL "Rent Reviews"
          VIEW-AS TOGGLE-BOX
          SIZE 12.57 BY 1
          FONT 10
     RP.Log3 AT ROW 5.2 COL 21
          LABEL "Lease Renewals"
          VIEW-AS TOGGLE-BOX
          SIZE 14.29 BY 1
          FONT 10
     RP.Log1 AT ROW 9.6 COL 3.29
          LABEL "Regenerate Statistics"
          VIEW-AS TOGGLE-BOX
          SIZE 17.72 BY 1
          FONT 10
     fil_AsAtDate AT ROW 9.6 COL 39.57 COLON-ALIGNED
     RP.Char2 AT ROW 11.2 COL 3.29 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Printer", "Printer":U,
"File", "File":U,
"Preview", "Preview":U
          SIZE 8 BY 2.7
          FONT 10
     RP.Char3 AT ROW 12 COL 9.29 COLON-ALIGNED NO-LABEL FORMAT "X(100)"
          VIEW-AS FILL-IN 
          SIZE 41.72 BY 1
          FONT 10
     btn_browse AT ROW 12 COL 53
     btn_OK AT ROW 13.2 COL 50.72
     RECT-32 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         DEFAULT-BUTTON btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 17.55
         WIDTH              = 75.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_browse V-table-Win
ON CHOOSE OF btn_browse IN FRAME F-Main /* Browse */
DO:
  RUN browse-file.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_OK V-table-Win
ON CHOOSE OF btn_OK IN FRAME F-Main /* OK */
DO:
  DISABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON VALUE-CHANGED OF RP.Char2 IN FRAME F-Main /* Char2 */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log1 V-table-Win
ON VALUE-CHANGED OF RP.Log1 IN FRAME F-Main /* Regenerate Statistics */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE browse-file V-table-Win 
PROCEDURE browse-file :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR save-as     AS CHAR NO-UNDO.
  DEF VAR file-chosen AS LOGI NO-UNDO.

  save-as = INPUT FRAME {&FRAME-NAME} RP.Char3.
  IF LENGTH( save-as ) > 0 THEN
  IF LOOKUP( SUBSTR( save-as, LENGTH( save-as ), 1 ), "\,/" ) <> 0 THEN save-as = "".
    
  SYSTEM-DIALOG GET-FILE save-as
    TITLE   "Export Report to file"
    FILTERS "Text Files (*.txt *.csv)" "*.txt,*.csv"
    DEFAULT-EXTENSION ".csv"
    SAVE-AS
    RETURN-TO-START-DIR
    USE-FILENAME
    UPDATE file-chosen.

  IF file-chosen THEN RP.Char3:SCREEN-VALUE IN FRAME {&FRAME-NAME} = save-as.
                 ELSE RETURN "No".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT RP.Log1 THEN
      HIDE fil_AsAtDate.
    ELSE
      VIEW fil_AsAtDate.

    IF INPUT RP.Char2 = "File" THEN
      VIEW RP.Char3 btn_Browse.
    ELSE
      HIDE RP.Char3 btn_Browse.

    ENABLE UNLESS-HIDDEN {&DISPLAYED-FIELDS}.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.
DEF VAR mm AS INT NO-UNDO.
DEF VAR yy AS INT NO-UNDO.
  
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  
  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.
    
  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Char1 = ""
      RP.Char2 = "Printer"
      RP.Log1 = Yes
      RP.Log2 = Yes.
  END.

  fil_AsAtDate = TODAY.
  mm = MONTH(TODAY).
  yy = YEAR(TODAY).
  IF DAY(TODAY) >= 15 THEN mm = mm + 1.
  IF mm > 12 THEN ASSIGN     mm = mm - 12    yy = yy + 1 .
  fil_Date2 = DATE( mm, 1, yy) - 1.
  mm = mm - 1.
  IF mm < 1 THEN ASSIGN     mm = mm + 12    yy = yy - 1 .
  fil_Date1 = DATE( mm, 1, yy).

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program
------------------------------------------------------------------------------*/
DEF VAR options     AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  RUN verify-fields.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  ASSIGN {&ENABLED-FIELDS}.  
  RUN dispatch( 'update-record':U ).

  options = STRING( RP.Log1 ) + ","
          + (IF RP.Char2 = "File" THEN RP.Char3 ELSE RP.Char2) + ","
          + fil_AsAtDate:SCREEN-VALUE + ","
          + fil_Date1:SCREEN-VALUE + ","
          + fil_Date2:SCREEN-VALUE + ","
          + STRING( RP.Log2 ) + ","
          + STRING( RP.Log3 )
          .

  RUN notify( 'set-busy, CONTAINER-SOURCE':u ).
  RUN process/period/RR-RENEW.P( options ).
  RUN notify( 'set-idle, CONTAINER-SOURCE':u ).
  
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-fields V-table-Win 
PROCEDURE verify-fields :
/*------------------------------------------------------------------------------
  Purpose:  Verify that parameters are reasonable.
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF INPUT RP.Char2 = "File" AND ( INPUT RP.Char3 = ? OR INPUT RP.Char3 = "" ) THEN
  DO:
    MESSAGE
      "You must enter a file to export the report  to!"
      VIEW-AS ALERT-BOX ERROR TITLE "No export file entered".
    RUN browse-file.
    IF RETURN-VALUE = "No" THEN RETURN "FAIL".
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


