&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&SCOPED-DEFINE REPORT-ID "Mgmt - Lexp"

DEF VAR entlist-list        AS CHAR NO-UNDO.
DEF VAR entlist-pd          AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Dec1 RP.Log2 RP.Log3 RP.Log4 RP.Char1 ~
RP.Log5 RP.Log1 RP.Char2 RP.Char3 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define ENABLED-OBJECTS fil_Date1 fil_Date2 fil_Date3 cmb_Manager ~
cmb_select fil_AsAtDate btn_browse btn_OK RECT-32 
&Scoped-Define DISPLAYED-FIELDS RP.Dec1 RP.Log2 RP.Log3 RP.Log4 RP.Char1 ~
RP.Log5 RP.Log1 RP.Char2 RP.Char3 
&Scoped-Define DISPLAYED-OBJECTS fil_Date1 fil_Date2 fil_Date3 cmb_Manager ~
cmb_select fil_AsAtDate 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_browse 
     LABEL "&Browse" 
     SIZE 7.43 BY 1.05
     FONT 9.

DEFINE BUTTON btn_OK DEFAULT 
     LABEL "&OK" 
     SIZE 9.72 BY 1.05
     FONT 9.

DEFINE VARIABLE cmb_Manager AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 33.72 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_select AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 33.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_AsAtDate AS DATE FORMAT "99/99/9999":U 
     LABEL "Print As At Date" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Date1 AS DATE FORMAT "99/99/9999" 
     LABEL "Start Report" 
     VIEW-AS FILL-IN 
     SIZE 11.43 BY .9.

DEFINE VARIABLE fil_Date2 AS DATE FORMAT "99/99/9999" 
     LABEL "Break Report" 
     VIEW-AS FILL-IN 
     SIZE 11.43 BY .9.

DEFINE VARIABLE fil_Date3 AS DATE FORMAT "99/99/9999" 
     LABEL "End Report" 
     VIEW-AS FILL-IN 
     SIZE 11.43 BY .9.

DEFINE RECTANGLE RECT-32
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 60 BY 17.1.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_Date1 AT ROW 1.4 COL 13.86 COLON-ALIGNED
     RP.Dec1 AT ROW 1.4 COL 47 COLON-ALIGNED
          LABEL "Min. Lease Value" FORMAT "->,>>>,>>9.99"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY .9
     fil_Date2 AT ROW 2.4 COL 13.86 COLON-ALIGNED
     fil_Date3 AT ROW 3.4 COL 13.86 COLON-ALIGNED
     RP.Log2 AT ROW 4.8 COL 3.29 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Exclude carparking", no,
"Carparking only", yes,
"All Areas", ?
          SIZE 16.57 BY 2.7
          FONT 10
     RP.Log3 AT ROW 4.8 COL 23.86 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Exclude monthlies", no,
"Monthlies only", yes,
"All leases", ?
          SIZE 15.43 BY 2.7
          FONT 10
     RP.Log4 AT ROW 5.6 COL 44.43
          LABEL "Summary Page"
          VIEW-AS TOGGLE-BOX
          SIZE 13.72 BY 1
          FONT 10
     RP.Char1 AT ROW 8 COL 3.29 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "by Expiry Date", "Expiry Date":U,
"by Region", "Region":U,
"by Building Manager", "Manager":U,
"Single Building Manager", "Single":U
          SIZE 20.57 BY 3.2
          FONT 10
     cmb_Manager AT ROW 10.3 COL 24.72 COLON-ALIGNED NO-LABEL
     RP.Log5 AT ROW 11.8 COL 3.29
          LABEL "Exclude entity list"
          VIEW-AS TOGGLE-BOX
          SIZE 20.57 BY 1
     cmb_select AT ROW 11.8 COL 24.72 COLON-ALIGNED NO-LABEL
     RP.Log1 AT ROW 13 COL 3.29
          LABEL "Regenerate Statistics"
          VIEW-AS TOGGLE-BOX
          SIZE 17.72 BY 1
          FONT 10
     fil_AsAtDate AT ROW 13 COL 39.57 COLON-ALIGNED
     RP.Char2 AT ROW 14.4 COL 3.29 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Printer", "Printer":U,
"File", "File":U,
"Preview", "Preview":U
          SIZE 8 BY 2.7
          FONT 10
     RP.Char3 AT ROW 15.2 COL 9.29 COLON-ALIGNED NO-LABEL FORMAT "X(100)"
          VIEW-AS FILL-IN 
          SIZE 41.72 BY 1
          FONT 10
     btn_browse AT ROW 15.2 COL 53
     btn_OK AT ROW 16.6 COL 50.72
     RECT-32 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 17.25
         WIDTH              = 67.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR RADIO-SET RP.Char1 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Char3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Dec1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR RADIO-SET RP.Log2 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR RADIO-SET RP.Log3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_browse V-table-Win
ON CHOOSE OF btn_browse IN FRAME F-Main /* Browse */
DO:
  RUN browse-file.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_OK V-table-Win
ON CHOOSE OF btn_OK IN FRAME F-Main /* OK */
DO:
  DISABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON VALUE-CHANGED OF RP.Char2 IN FRAME F-Main /* Char2 */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Manager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Manager V-table-Win
ON U1 OF cmb_Manager IN FRAME F-Main
DO:
  {inc/selcmb/scpmgr1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Manager V-table-Win
ON U2 OF cmb_Manager IN FRAME F-Main
DO:
  {inc/selcmb/scpmgr2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log1 V-table-Win
ON VALUE-CHANGED OF RP.Log1 IN FRAME F-Main /* Regenerate Statistics */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log5 V-table-Win
ON VALUE-CHANGED OF RP.Log5 IN FRAME F-Main /* Exclude entity list */
DO:
    IF INPUT log5 THEN DO:
      RUN get-entlist.
      cmb_Select:LIST-ITEMS   = entlist-list.
      cmb_Select:PRIVATE-DATA = entlist-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, entlist-list ).
    END.
    RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE browse-file V-table-Win 
PROCEDURE browse-file :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR save-as     AS CHAR NO-UNDO.
  DEF VAR file-chosen AS LOGI NO-UNDO.

  save-as = INPUT FRAME {&FRAME-NAME} RP.Char3.
  IF LENGTH( save-as ) > 0 THEN
  IF LOOKUP( SUBSTR( save-as, LENGTH( save-as ), 1 ), "\,/" ) <> 0 THEN save-as = "".
    
  SYSTEM-DIALOG GET-FILE save-as
    TITLE   "Export Report to file"
    FILTERS "Text Files (*.txt *.csv)" "*.txt,*.csv"
    DEFAULT-EXTENSION ".csv"
    SAVE-AS
    RETURN-TO-START-DIR
    USE-FILENAME
    UPDATE file-chosen.

  IF file-chosen THEN RP.Char3:SCREEN-VALUE IN FRAME {&FRAME-NAME} = save-as.
                 ELSE RETURN "No".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT RP.Log1 THEN
      HIDE fil_AsAtDate.
    ELSE
      VIEW fil_AsAtDate.

    IF INPUT RP.Char2 = "File" THEN
      VIEW RP.Char3 btn_Browse.
    ELSE
      HIDE RP.Char3 btn_Browse.

    IF INPUT RP.Char1 = "Single":U THEN
      VIEW cmb_Manager.
    ELSE
      HIDE cmb_Manager.

    IF INPUT RP.Log5 THEN
      VIEW cmb_Select.
    ELSE
      HIDE cmb_Select.

    ENABLE UNLESS-HIDDEN {&DISPLAYED-FIELDS}.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-entlist V-table-Win 
PROCEDURE get-entlist :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF entlist-list <> "" THEN RETURN.
  
  FOR EACH EntityList WHERE EntityList.ListType='P' NO-LOCK:

    entlist-list = entlist-list + IF entlist-list = "" THEN "" ELSE ",".
    entlist-list = entlist-list + EntityList.Description.

    entlist-pd = entlist-pd + IF entlist-pd = "" THEN "" ELSE ",".
    entlist-pd = entlist-pd + EntityList.ListCode.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN enable-appropriate-fields.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.
DEF VAR mm AS INT NO-UNDO.
DEF VAR yy AS INT NO-UNDO.
  
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  
  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.
    
  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Char1 = ""
      RP.Char2 = "Printer"
      RP.Log1 = Yes
      RP.Log2 = ?
      RP.Log3 = Yes
      RP.Log4 = Yes
      RP.Dec1 = 0 .
  END.

  mm = MONTH(TODAY).
  yy = YEAR(TODAY).
  IF DAY(TODAY ) < 15 THEN mm = mm - 1.
  IF mm < 1 THEN ASSIGN     mm = mm + 12    yy = yy - 1.
  fil_Date1 = DATE( mm, 1, yy).
  mm = mm + 3.
  IF mm > 12 THEN ASSIGN mm = mm - 12   yy = yy + 1.
  fil_Date2 = DATE( mm, 1, yy) - 1.
  mm = mm + 9.
  IF mm > 12 THEN ASSIGN mm = mm - 12   yy = yy + 1.
  fil_Date3 = DATE( mm, 1, yy) - 1.
  fil_AsAtDate = TODAY.


  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program
------------------------------------------------------------------------------*/
DEF VAR options     AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  RUN verify-fields.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  ASSIGN {&ENABLED-FIELDS}.  
  RUN dispatch( 'update-record':U ).

  options = "FileName," + (IF RP.Char2 = "File" THEN RP.Char3 ELSE RP.Char2)
          + (IF RP.Log1 THEN "~nRegenerate" ELSE "")
          + "~nParks," + (IF RP.Log2 = ? THEN "?" ELSE STRING(RP.Log2, "Yes/No"))
          + "~nMonthly," + (IF RP.Log3 = ? THEN "?" ELSE STRING(RP.Log3, "Yes/No") )
          + (IF RP.Log4 THEN "~nShowSummary" ELSE "")
          + "~nAsAtDate," + fil_AsAtDate:SCREEN-VALUE
          + "~nStart," + (IF INPUT fil_Date1 = ? THEN "?" ELSE STRING( INPUT fil_Date1 ))
          + "~nBreak," + (IF INPUT fil_Date2 = ? THEN "?" ELSE STRING( INPUT fil_Date2 ))
          + "~nEnd," + (IF INPUT fil_Date3 = ? THEN "?" ELSE STRING( INPUT fil_Date3 ))
          + "~nMinRental," + (IF RP.Dec1 = ? THEN "?" ELSE STRING( RP.Dec1 ))
          + "~nSequence," + RP.Char1
          + (IF RP.Char1 = "Single":U  THEN "~nOneManager," + STRING(RP.Int1) ELSE "")
          + (IF RP.Log5 THEN "~nEntityList," + 
                ENTRY( cmb_Select:LOOKUP( cmb_Select:SCREEN-VALUE ), cmb_Select:PRIVATE-DATA ) 
             ELSE "")
          .

  RUN notify( 'set-busy, CONTAINER-SOURCE':u ).
  RUN process/period/lsexpire.p( options ).
  RUN notify( 'set-idle, CONTAINER-SOURCE':u ).

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-fields V-table-Win 
PROCEDURE verify-fields :
/*------------------------------------------------------------------------------
  Purpose:  Verify that parameters are reasonable.
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF INPUT RP.Char2 = "File" AND ( INPUT RP.Char3 = ? OR INPUT RP.Char3 = "" ) THEN
  DO:
    MESSAGE
      "You must enter a file to export the report  to!"
      VIEW-AS ALERT-BOX ERROR TITLE "No export file entered".
    RUN browse-file.
    IF RETURN-VALUE = "No" THEN RETURN "FAIL".
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

