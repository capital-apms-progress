&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR mode AS CHAR NO-UNDO.
DEF VAR scenario-code LIKE Scenario.ScenarioCode NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}
{inc/ofc-acct.i "CREDITORS" "sundry-creditors"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES CashFlow
&Scoped-define FIRST-EXTERNAL-TABLE CashFlow


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR CashFlow.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS CashFlow.EntityType CashFlow.EntityCode ~
CashFlow.AccountCode CashFlow.StartDate CashFlow.EndDate CashFlow.Amount ~
CashFlow.Description 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}EntityType ~{&FP2}EntityType ~{&FP3}~
 ~{&FP1}EntityCode ~{&FP2}EntityCode ~{&FP3}~
 ~{&FP1}AccountCode ~{&FP2}AccountCode ~{&FP3}~
 ~{&FP1}StartDate ~{&FP2}StartDate ~{&FP3}~
 ~{&FP1}EndDate ~{&FP2}EndDate ~{&FP3}~
 ~{&FP1}Amount ~{&FP2}Amount ~{&FP3}
&Scoped-define ENABLED-TABLES CashFlow
&Scoped-define FIRST-ENABLED-TABLE CashFlow
&Scoped-Define ENABLED-OBJECTS RECT-24 cmb_Frequency cmb_cftype cmb_cfctype ~
btn_add btn_ok btn_cancel 
&Scoped-Define DISPLAYED-FIELDS CashFlow.ScenarioCode CashFlow.EntityType ~
CashFlow.EntityCode CashFlow.AccountCode CashFlow.StartDate ~
CashFlow.EndDate CashFlow.Amount CashFlow.Description 
&Scoped-Define DISPLAYED-OBJECTS fil_Entity fil_Account cmb_Frequency ~
cmb_cftype cmb_cfctype 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
CashFlowType||y|TTPL.CashFlow.CashFlowType
AccountCode||y|TTPL.CashFlow.AccountCode
EntityType||y|TTPL.CashFlow.EntityType
FrequencyCode||y|TTPL.CashFlow.FrequencyCode
StartDate||y|TTPL.CashFlow.StartDate
ScenarioCode||y|TTPL.CashFlow.ScenarioCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = "CashFlowType,AccountCode,EntityType,FrequencyCode,StartDate,ScenarioCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add Another" 
     SIZE 11 BY 1.05
     FONT 9.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 7 BY 1.05
     FONT 9.

DEFINE BUTTON btn_ok 
     LABEL "&OK" 
     SIZE 4 BY 1.05
     FONT 9.

DEFINE VARIABLE cmb_cfctype AS CHARACTER FORMAT "X(100)" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS " "
     SIZE 26.29 BY 1.

DEFINE VARIABLE cmb_cftype AS CHARACTER FORMAT "X(100)" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS " "
     SIZE 26 BY 1.

DEFINE VARIABLE cmb_Frequency AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "","" 
     SIZE 26.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Account AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 42 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Entity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 42 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-24
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 64 BY 11.7.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     CashFlow.ScenarioCode AT ROW 1 COL 54.43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
          FONT 10
     CashFlow.EntityType AT ROW 2.2 COL 7.14 COLON-ALIGNED
          LABEL "Entity" FORMAT "X"
          VIEW-AS FILL-IN 
          SIZE 3 BY 1
          FONT 10
     CashFlow.EntityCode AT ROW 2.2 COL 10 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 7 BY 1
          FONT 10
     fil_Entity AT ROW 2.2 COL 20 COLON-ALIGNED NO-LABEL
     CashFlow.AccountCode AT ROW 3.2 COL 7 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 10 BY 1
          FONT 10
     fil_Account AT ROW 3.2 COL 20 COLON-ALIGNED NO-LABEL
     CashFlow.StartDate AT ROW 4.4 COL 7 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
          FONT 10
     CashFlow.EndDate AT ROW 5.4 COL 7 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
          FONT 10
     cmb_Frequency AT ROW 4.35 COL 37.57 NO-LABEL
     CashFlow.Amount AT ROW 5.4 COL 35.57 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 13.57 BY 1
          FONT 10
     cmb_cftype AT ROW 7.7 COL 9 NO-LABEL
     cmb_cfctype AT ROW 7.7 COL 37.57 NO-LABEL
     CashFlow.Description AT ROW 10 COL 2.14 NO-LABEL
          VIEW-AS EDITOR
          SIZE 61.72 BY 2.8
          FONT 10
     btn_add AT ROW 13.4 COL 1
     btn_ok AT ROW 13.4 COL 53
     btn_cancel AT ROW 13.4 COL 58
     RECT-24 AT ROW 1.4 COL 1
     "Account:" VIEW-AS TEXT
          SIZE 6.86 BY 1 AT ROW 3.2 COL 2
          FONT 10
     "Start:" VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 4.4 COL 2
          FONT 10
     "Frequency:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 4.4 COL 27.29
          FONT 10
     "Finish:" VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 5.4 COL 2
          FONT 10
     "Amount:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 5.4 COL 27.29
          FONT 10
     "Cash Flow Type" VIEW-AS TEXT
          SIZE 12 BY 1 AT ROW 6.7 COL 9
     "Change Type" VIEW-AS TEXT
          SIZE 9.72 BY 1 AT ROW 6.7 COL 37.57
     "Description:" VIEW-AS TEXT
          SIZE 9 BY 1 AT ROW 9 COL 2.14
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.CashFlow
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.6
         WIDTH              = 65.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN CashFlow.Amount IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR COMBO-BOX cmb_cfctype IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_cftype IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Frequency IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN CashFlow.EndDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN CashFlow.EntityCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN CashFlow.EntityType IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN fil_Account IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Entity IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN CashFlow.ScenarioCode IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN CashFlow.StartDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON RETURN OF FRAME F-Main
ANYWHERE DO:
  IF SELF:TYPE <> "EDITOR" THEN
  DO:
    APPLY 'TAB':U TO SELF.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME CashFlow.AccountCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL CashFlow.AccountCode V-table-Win
ON LEAVE OF CashFlow.AccountCode IN FRAME F-Main /* Account */
DO:
  {inc/selcde/cdcoa.i "fil_Account"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add Another */
DO:
  RUN add-another.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN cancel-changes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON CHOOSE OF btn_ok IN FRAME F-Main /* OK */
DO:
  RUN confirm-changes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_cfctype
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_cfctype V-table-Win
ON U1 OF cmb_cfctype IN FRAME F-Main
DO:
  {inc/selcmb/sccfct1.i "CashFlow" "CFChangeType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_cfctype V-table-Win
ON U2 OF cmb_cfctype IN FRAME F-Main
DO:
  {inc/selcmb/sccfct2.i "CashFlow" "CFChangeType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_cftype
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_cftype V-table-Win
ON U1 OF cmb_cftype IN FRAME F-Main
DO:
  {inc/selcmb/sccft1.i "CashFlow" "CashFlowType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_cftype V-table-Win
ON U2 OF cmb_cftype IN FRAME F-Main
DO:
  {inc/selcmb/sccft2.i "CashFlow" "CashFlowType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Frequency
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Frequency V-table-Win
ON U1 OF cmb_Frequency IN FRAME F-Main
DO:
  {inc/selcmb/scfty1.i "CashFlow" "FrequencyCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Frequency V-table-Win
ON U2 OF cmb_Frequency IN FRAME F-Main
DO:
  {inc/selcmb/scfty2.i "CashFlow" "FrequencyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME CashFlow.Description
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL CashFlow.Description V-table-Win
ON RETURN OF CashFlow.Description IN FRAME F-Main /* Description */
DO:
  apply lastkey to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME CashFlow.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL CashFlow.EntityCode V-table-Win
ON LEAVE OF CashFlow.EntityCode IN FRAME F-Main /* Entity */
DO:
  CASE INPUT CashFlow.EntityType:
    WHEN "P" THEN {inc/selcde/cdpro.i "Fil_Entity"}
    WHEN "L" THEN {inc/selcde/cdcmp.i "Fil_Entity"}
    WHEN "J" THEN {inc/selcde/cdprj.i "Fil_Entity"}
    WHEN "C" THEN {inc/selcde/cdcrd.i "Fil_Entity"}
    WHEN "T" THEN {inc/selcde/cdtnt.i "Fil_Entity"}
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Account
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Account V-table-Win
ON U1 OF fil_Account IN FRAME F-Main
DO:
  {inc/selfil/sfcoa1.i "CashFlow" "AccountCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Account V-table-Win
ON U2 OF fil_Account IN FRAME F-Main
DO:
  {inc/selfil/sfcoa2.i "CashFlow" "AccountCode"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Account V-table-Win
ON U3 OF fil_Account IN FRAME F-Main
DO:
  {inc/selfil/sfcoa3.i "CashFlow" "AccountCode"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Entity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Entity V-table-Win
ON U1 OF fil_Entity IN FRAME F-Main
DO:
  CASE INPUT CashFlow.EntityType:
    WHEN "P" THEN DO: {inc/selfil/sfpro1.i "CashFlow" "EntityCode"} END.
    WHEN "L" THEN DO: {inc/selfil/sfcmp1.i "CashFlow" "EntityCode"} END.
    WHEN "J" THEN DO: {inc/selfil/sfprj1.i "CashFlow" "EntityCode"} END.
    WHEN "C" THEN DO: {inc/selfil/sfcrd1.i "CashFlow" "EntityCode"} END.
    WHEN "T" THEN DO: {inc/selfil/sftnt1.i "CashFlow" "EntityCode"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Entity V-table-Win
ON U2 OF fil_Entity IN FRAME F-Main
DO:
  CASE INPUT CashFlow.EntityType:
    WHEN "P" THEN DO: {inc/selfil/sfpro2.i "CashFlow" "EntityCode"} END.
    WHEN "L" THEN DO: {inc/selfil/sfcmp2.i "CashFlow" "EntityCode"} END.
    WHEN "J" THEN DO: {inc/selfil/sfprj2.i "CashFlow" "EntityCode"} END.
    WHEN "C" THEN DO: {inc/selfil/sfcrd2.i "CashFlow" "EntityCode"} END.
    WHEN "T" THEN DO: {inc/selfil/sftnt2.i "CashFlow" "EntityCode"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Entity V-table-Win
ON U3 OF fil_Entity IN FRAME F-Main
DO:
  CASE INPUT CashFlow.EntityType:
    WHEN "P" THEN DO: {inc/selfil/sfpro3.i "CashFlow" "EntityCode"} END.
    WHEN "L" THEN DO: {inc/selfil/sfcmp3.i "CashFlow" "EntityCode"} END.
    WHEN "J" THEN DO: {inc/selfil/sfprj3.i "CashFlow" "EntityCode"} END.
    WHEN "C" THEN DO: {inc/selfil/sfcrd3.i "CashFlow" "EntityCode"} END.
    WHEN "T" THEN DO: {inc/selfil/sftnt3.i "CashFlow" "EntityCode"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

{inc/ent-chg.i "CashFlow.EntityType" "ALL" "entity-type-changed"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-another V-table-Win 
PROCEDURE add-another :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-cash-flow.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).
  RUN dispatch( 'add-record':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "CashFlow"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "CashFlow"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-cash-flow. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-cash-flow.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  RUN dispatch( 'update-record':U ).
  IF mode = "Add" THEN RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-cash-flow V-table-Win 
PROCEDURE delete-cash-flow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT CashFlow EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE CashFlow THEN DELETE CashFlow.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-type-changed V-table-Win 
PROCEDURE entity-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  
  /* Reset the entity code / description fields */
  
  CashFlow.EntityCode:PRIVATE-DATA = ",".
  CashFlow.EntityCode:SCREEN-VALUE = STRING( 0 ).
  fil_Entity:PRIVATE-DATA = "".
  fil_Entity:SCREEN-VALUE = "".
  CashFlow.AccountCode:PRIVATE-DATA = ",".
  CashFlow.AccountCode:SCREEN-VALUE = STRING( 0 ).
  fil_Account:PRIVATE-DATA = "".
  fil_Account:SCREEN-VALUE = "".

  RUN update-entity-button.
  RUN update-account-button.  
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-key V-table-Win 
PROCEDURE get-key :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF INPUT  PARAMETER k-name AS CHAR NO-UNDO.
  DEF OUTPUT PARAMETER k-code AS CHAR NO-UNDO.
  
  DEF VAR recsrc-hdl AS HANDLE NO-UNDO.
  DEF VAR c-recsrc   AS CHAR NO-UNDO.
  DEF VAR c-code     AS CHAR NO-UNDO.
  
  RUN get-link-handle IN adm-broker-hdl(
    THIS-PROCEDURE, 'RECORD-SOURCE', OUTPUT c-recsrc ).
  recsrc-hdl = WIDGET-HANDLE( c-recsrc ).
  IF NOT VALID-HANDLE( recsrc-hdl ) THEN RETURN.
  
  RUN send-key IN recsrc-hdl( k-name, OUTPUT k-code ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  
  /* Assign the sequence for this record */
  DEF BUFFER LastCashFlow FOR CashFlow.
  
  FIND LAST LastCashFlow WHERE
    LastCashFlow.ScenarioCode = scenario-code AND
    LastCashFlow.EntityType   = INPUT CashFlow.EntityType AND
    LastCashFlow.EntityCode   = INPUT CashFlow.EntityCode AND
    LastCashFLow.AccountCode  = INPUT CashFlow.AccountCode 
    NO-LOCK NO-ERROR.
    
  ASSIGN CashFlow.Sequence = IF AVAILABLE LastCashFlow THEN
    LastCashFlow.Sequence + 1 ELSE 1.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN update-entity-button.
  RUN update-account-button.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR c-code AS CHAR NO-UNDO.
  
  c-code = find-parent-key( "ScenarioCode" ).
  ASSIGN scenario-code = INT( c-code ) NO-ERROR.
  IF scenario-code = ? OR scenario-code < 1 THEN
    scenario-code = 1. /* default to 1 if invalid */

  IF mode = "Add" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE RUN dispatch( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR d-et AS CHAR NO-UNDO INITIAL "P".
DEF VAR d-ec AS INT NO-UNDO INITIAL 0.
DEF VAR d-ac AS DEC NO-UNDO INITIAL 0.
DEF VAR d-cf AS CHAR NO-UNDO INITIAL "A".
DEF VAR d-frq AS CHAR NO-UNDO INITIAL "MNTH".

  IF AVAILABLE(CashFlow) THEN DO:
    d-et = CashFlow.EntityType.
    d-ec = CashFlow.EntityCode.
    d-ac = CashFlow.AccountCode.
    d-cf = CashFlow.CashFlowType.
    d-frq = CashFlow.FrequencyCode.
  END.

  CREATE CashFlow.
  ASSIGN
    CashFlow.ScenarioCode = scenario-code
    CashFlow.EntityType   = d-et
    CashFlow.EntityCode   = d-ec
    CashFlow.AccountCode  = d-ac
    CashFlow.CashFlowType = d-cf
    CashFlow.FrequencyCode = d-frq.
    
  CURRENT-WINDOW:TITLE = "Adding a new Cash Flow".
  IF mode <> "view" THEN RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'display-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-enable-fields V-table-Win 
PROCEDURE override-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF mode = 'View' THEN DO WITH FRAME {&FRAME-NAME}:
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, "ALL", "HIDDEN = Yes" ).
    DISABLE cmb_Frequency cmb_cfctype cmb_cftype.
    ASSIGN cmb_Frequency:BGCOLOR = 16
           cmb_cfctype:BGCOLOR = 16
           cmb_cftype:BGCOLOR = 16.
    HIDE btn_Add  btn_ok.
    RETURN.
  END.

  RUN pre-enable-fields IN THIS-PROCEDURE NO-ERROR.

  /* Dispatch standard ADM method.                             */
  RUN adm-enable-fields IN THIS-PROCEDURE NO-ERROR .
  RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, "ALL", "SENSITIVE = Yes" ).
  
  /* Code placed here will execute AFTER standard behavior.    */
  DO WITH FRAME {&FRAME-NAME}:
    btn_add:HIDDEN = mode <> "Add".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE post-initialize V-table-Win 
PROCEDURE post-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN inst-display-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "CashFlowType" "CashFlow" "CashFlowType"}
  {src/adm/template/sndkycas.i "AccountCode" "CashFlow" "AccountCode"}
  {src/adm/template/sndkycas.i "EntityType" "CashFlow" "EntityType"}
  {src/adm/template/sndkycas.i "FrequencyCode" "CashFlow" "FrequencyCode"}
  {src/adm/template/sndkycas.i "StartDate" "CashFlow" "StartDate"}
  {src/adm/template/sndkycas.i "ScenarioCode" "CashFlow" "ScenarioCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "CashFlow"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-account-button V-table-Win 
PROCEDURE update-account-button :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "View" THEN RETURN.

  DO WITH FRAME {&FRAME-NAME}:
  
    DEF VAR account-group AS CHAR NO-UNDO.    
    CASE INPUT CashFlow.EntityType:
      WHEN 'P' THEN account-group = 'PROPEX'.
      WHEN 'L' THEN account-group = 'ADMIN '.
    END CASE.

    FIND ChartOfAccount WHERE ChartOfAccount.AccountCode =
      IF INPUT CashFlow.EntityType = "T" THEN sundry-debtors ELSE
      IF INPUT CashFlow.EntityType = "C" THEN sundry-creditors ELSE ?
      NO-LOCK NO-ERROR.
    IF AVAILABLE ChartOfAccount THEN
    DO:
      fil_Account:SENSITIVE = Yes.
      fil_Account:PRIVATE-DATA = STRING( ROWID( ChartOfAccount ) ).
      APPLY 'U2' TO fil_Account.
      fil_Account:SENSITIVE = No.
    END.
    
    CashFlow.AccountCode:SENSITIVE = LOOKUP( INPUT CashFlow.EntityType, "T,C" ) = 0.
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Account:HANDLE ), "SENSITIVE = "
      + IF LOOKUP( INPUT CashFlow.EntityType, "T,C" ) = 0 THEN "Yes" ELSE "No" ).

    IF account-group <> "" THEN 
    DO:
      FIND AccountGroup WHERE AccountGroup.AccountGroupCode = account-group
        NO-LOCK NO-ERROR.
      IF NOT AVAILABLE AccountGroup THEN RETURN.
      
      RUN set-link-attributes IN sys-mgr (
        THIS-PROCEDURE,
        STRING( fil_Account:HANDLE ),
        "FUNCTION = " + 
        "FilterBy-Case = " + account-group + " - " + AccountGroup.Name + "~n" +
        "FilterPanel = Yes~n SortPanel = Yes" ).
    END.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-button V-table-Win 
PROCEDURE update-entity-button :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR side-label AS HANDLE NO-UNDO.
  
  FIND EntityType WHERE EntityType.EntityType = INPUT CashFlow.EntityType
    NO-LOCK NO-ERROR.
  IF NOT AVAILABLE EntityType THEN RETURN.
  
  side-label = CashFlow.EntityType:SIDE-LABEL-HANDLE.
  side-label:HIDDEN = Yes.
  side-label:SCREEN-VALUE = EntityType.Description.
  side-label:COL = 2.
  side-label:HIDDEN = No.

  IF mode = "View" THEN RETURN.

  DEF VAR node-name    AS CHAR NO-UNDO.
  DEF VAR vwr-name     AS CHAR NO-UNDO.
  DEF VAR options      AS CHAR NO-UNDO.
    
  CASE INPUT CashFlow.EntityType:
    WHEN 'P' THEN ASSIGN
      node-name = 'w-selpro' vwr-name = 'b-selpro'
      options = "FilterPanel = Yes~n SortPanel = Yes~n AlphabetPanel = Yes".
    WHEN 'L' THEN ASSIGN
      node-name = 'w-selcmp' vwr-name = 'b-selcmp'
      options = "FilterPanel = Yes~n SortPanel = Yes~n AlphabetPanel = Yes".
    WHEN 'J' THEN ASSIGN
      node-name = 'w-selprj' vwr-name = 'b-selprj'
      options = "FilterPanel = Yes~n SortPanel = Yes~n AlphabetPanel = Yes".
    WHEN 'T' THEN ASSIGN
      node-name = 'w-seltnt' vwr-name = 'b-seltnt'
      options = "FilterPanel = Yes~n SortPanel = Yes~n AlphabetPanel = Yes".
    WHEN 'C' THEN ASSIGN
      node-name = 'w-selcrd' vwr-name = 'b-selcrd'
      options = "FilterPanel =Yes~n SortPanel = Yes~n AlphabetPanel = Yes~n AlphabetPanel2 = Yes".
  END CASE.

  RUN set-link-attributes IN sys-mgr (
    THIS-PROCEDURE, 
    STRING( fil_Entity:HANDLE ),
    "Target = " + node-name + "," +
    "Viewer = " + vwr-name + "," +
    "Function = " + options
  ).
          
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-cash-flow V-table-Win 
PROCEDURE verify-cash-flow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  /* Verify the entity and account code */
  DEF VAR entity-ok  AS LOGI NO-UNDO.
  DEF VAR ent-id     AS ROWID NO-UNDO.
  DEF VAR account-ok AS LOGI NO-UNDO.
  
  ent-id = TO-ROWID( fil_Entity:PRIVATE-DATA ).
  CASE INPUT CashFlow.EntityType:
    WHEN "P" THEN entity-ok = CAN-FIND( Property WHERE ROWID( Property ) = ent-id ).
    WHEN "L" THEN entity-ok = CAN-FIND( Company WHERE ROWID( Company ) = ent-id ).
    WHEN "C" THEN entity-ok = CAN-FIND( Creditor WHERE ROWID( Creditor ) = ent-id ).
    WHEN "J" THEN entity-ok = CAN-FIND( Project WHERE ROWID( Project ) = ent-id ).
    WHEN "T" THEN entity-ok = CAN-FIND( Tenant WHERE ROWID( Tenant ) = ent-id ).
  END CASE.

  account-ok = CAN-FIND( FIRST ChartOfAccount WHERE ChartOfAccount.AccountCode =
    INPUT CashFlow.AccountCode ).

  IF NOT entity-ok OR NOT account-ok THEN
  DO:
    MESSAGE "You must enter a valid entity / account code!"
      VIEW-AS ALERT-BOX ERROR TITLE "Invalid Entity / Account".
    IF entity-ok
      THEN APPLY 'ENTRY' TO CashFlow.AccountCode.
      ELSE APPLY 'ENTRY' TO Cashflow.EntityType.
    RETURN "FAIL".
  END.
  
  /* Verify the cash flow details */
  IF INPUT CashFlow.StartDate <> ? AND INPUT CashFlow.EndDate <> ? AND
     INPUT CashFlow.StartDate > INPUT CashFlow.EndDate THEN
  DO:
    MESSAGE 
      "The cash flow end date must be greater than" SKIP
      "the start date!"
       VIEW-AS ALERT-BOX ERROR TITLE "Invalid cash flow period".
    APPLY 'ENTRY':U TO Cashflow.EndDate.
    RETURN "FAIL".
  END.
  
  IF INPUT cmb_Frequency = "" OR INPUT cmb_Frequency = ? THEN
  DO:
    MESSAGE "You must select a frequency for the cash flow"
      VIEW-AS ALERT-BOX ERROR TITLE "No frequency selected".
    APPLY 'ENTRY':U TO cmb_Frequency.
    RETURN "FAIL".
  END.
  
  IF INPUT cmb_cftype = ? OR INPUT cmb_cftype = "" THEN
  DO:
    MESSAGE "You must select the type of this cash flow"
      VIEW-AS ALERT-BOX ERROR TITLE "No type selected".
    APPLY 'ENTRY':U TO cmb_cftype.
    RETURN "FAIL".
  END.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


