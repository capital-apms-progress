&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description:

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.

DEF VAR lease-code AS INT NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RentReview
&Scoped-define FIRST-EXTERNAL-TABLE RentReview


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RentReview.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RentReview.DateDue RentReview.Earliest ~
RentReview.DateComplete RentReview.Latest RentReview.EstimateBasis ~
RentReview.NewRental 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}DateDue ~{&FP2}DateDue ~{&FP3}~
 ~{&FP1}Earliest ~{&FP2}Earliest ~{&FP3}~
 ~{&FP1}DateComplete ~{&FP2}DateComplete ~{&FP3}~
 ~{&FP1}Latest ~{&FP2}Latest ~{&FP3}~
 ~{&FP1}EstimateBasis ~{&FP2}EstimateBasis ~{&FP3}~
 ~{&FP1}NewRental ~{&FP2}NewRental ~{&FP3}
&Scoped-define ENABLED-TABLES RentReview
&Scoped-define FIRST-ENABLED-TABLE RentReview
&Scoped-Define ENABLED-OBJECTS RECT-31 cmb_ReviewType cmb_ReviewStatus 
&Scoped-Define DISPLAYED-FIELDS RentReview.DateDue RentReview.Earliest ~
RentReview.DateComplete RentReview.Latest RentReview.EstimateBasis ~
RentReview.NewRental 
&Scoped-Define DISPLAYED-OBJECTS cmb_ReviewType cmb_ReviewStatus 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS></FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = ':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_ReviewStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS " "
     SIZE 26.86 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_ReviewType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS " "
     SIZE 27 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 48.57 BY 9.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RentReview.DateDue AT ROW 1.4 COL 10.43 COLON-ALIGNED
          LABEL "Due for"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     RentReview.Earliest AT ROW 1.4 COL 35 COLON-ALIGNED
          LABEL "Earliest"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     RentReview.DateComplete AT ROW 2.4 COL 10.43 COLON-ALIGNED
          LABEL "Completed on"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     RentReview.Latest AT ROW 2.4 COL 35 COLON-ALIGNED
          LABEL "Latest"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     RentReview.EstimateBasis AT ROW 3.6 COL 1.86
          LABEL "Estimate Basis"
          VIEW-AS FILL-IN 
          SIZE 36 BY 1
     cmb_ReviewType AT ROW 5.2 COL 10.43 COLON-ALIGNED
     cmb_ReviewStatus AT ROW 6.2 COL 10.43 COLON-ALIGNED
     RentReview.NewRental AT ROW 8.6 COL 10.43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 16.43 BY 1
     RECT-31 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RentReview
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 14.6
         WIDTH              = 55.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RentReview.DateComplete IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RentReview.DateDue IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RentReview.Earliest IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RentReview.EstimateBasis IN FRAME F-Main
   ALIGN-L EXP-LABEL                                                    */
/* SETTINGS FOR FILL-IN RentReview.Latest IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME cmb_ReviewStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ReviewStatus V-table-Win
ON U1 OF cmb_ReviewStatus IN FRAME F-Main /* Status */
DO:
  {inc/selcmb/scrrst1.i "RentReview" "ReviewStatus"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ReviewStatus V-table-Win
ON U2 OF cmb_ReviewStatus IN FRAME F-Main /* Status */
DO:
  {inc/selcmb/scrrst2.i "RentReview" "ReviewStatus"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_ReviewType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ReviewType V-table-Win
ON U1 OF cmb_ReviewType IN FRAME F-Main /* Type */
DO:
  {inc/selcmb/scrrt1.i "RentReview" "ReviewType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ReviewType V-table-Win
ON U2 OF cmb_ReviewType IN FRAME F-Main /* Type */
DO:
  {inc/selcmb/scrrt2.i "RentReview" "ReviewType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RentReview.DateComplete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RentReview.DateComplete V-table-Win
ON ANY-KEY OF RentReview.DateComplete IN FRAME F-Main /* Completed on */
DO:
  IF LAST-EVENT:LABEL = "DEL" THEN DO:
    SELF:SCREEN-VALUE = "".
    {&SELF-NAME} = ?.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RentReview.Earliest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RentReview.Earliest V-table-Win
ON ANY-KEY OF RentReview.Earliest IN FRAME F-Main /* Earliest */
DO:
  IF LAST-EVENT:LABEL = "DEL" THEN DO:
    SELF:SCREEN-VALUE = "".
    {&SELF-NAME} = ?.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RentReview.Latest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RentReview.Latest V-table-Win
ON ANY-KEY OF RentReview.Latest IN FRAME F-Main /* Latest */
DO:
  IF LAST-EVENT:LABEL = "DEL" THEN DO:
    SELF:SCREEN-VALUE = "".
    {&SELF-NAME} = ?.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RentReview"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RentReview"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONATINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-rent-review.
                  ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit') .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-rent-review.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).
  IF mode = "Add" THEN RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-rent-review V-table-Win 
PROCEDURE delete-rent-review :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FIND CURRENT RentReview EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE RentReview THEN DELETE RentReview NO-ERROR.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-lease-code V-table-Win 
PROCEDURE get-lease-code :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  lease-code = INT( find-parent-key( 'TenancyLeaseCode' ) ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN get-lease-code.

  IF mode = "Add" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE
    RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  FIND FIRST RentReview WHERE RentReview.TenancyLeaseCode = 0 EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE(RentReview) THEN CREATE RentReview.

  ASSIGN
    RentReview.TenancyLeaseCode = lease-code
    RentReview.ReviewStatus     = "TODO"
    RentReview.ReviewType       = "MKT".
  
  CURRENT-WINDOW:TITLE = "Adding a Rent Review".
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RentReview"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.

  mode = new-mode.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-rent-review V-table-Win 
PROCEDURE verify-rent-review :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


