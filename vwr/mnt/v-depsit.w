&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:         v-kcktru.w
  Description:  Kick of transaction update
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR report-options AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES NewBatch
&Scoped-define FIRST-EXTERNAL-TABLE NewBatch


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR NewBatch.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS NewBatch.BatchCode 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}BatchCode ~{&FP2}BatchCode ~{&FP3}
&Scoped-define ENABLED-TABLES NewBatch
&Scoped-define FIRST-ENABLED-TABLE NewBatch
&Scoped-Define ENABLED-OBJECTS RECT-13 btn_print 
&Scoped-Define DISPLAYED-FIELDS NewBatch.BatchCode NewBatch.Description ~
NewBatch.BatchType NewBatch.DocumentCount NewBatch.Total 
&Scoped-Define DISPLAYED-OBJECTS fil_Operator fil_BankAct 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_print DEFAULT 
     LABEL "&Print" 
     SIZE 16 BY 1.45
     FONT 9.

DEFINE VARIABLE fil_BankAct AS CHARACTER FORMAT "X(256)":U 
     LABEL "Bank" 
     VIEW-AS FILL-IN 
     SIZE 57.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Operator AS CHARACTER FORMAT "X(256)":U 
     LABEL "Operator" 
     VIEW-AS FILL-IN 
     SIZE 48.57 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-13
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 66.86 BY 7.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     NewBatch.BatchCode AT ROW 1.4 COL 7.57 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 8.57 BY 1
          BGCOLOR 16 FONT 11
     NewBatch.Description AT ROW 1.4 COL 16.72 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 48.57 BY 1
          BGCOLOR 16 FONT 11
     fil_Operator AT ROW 2.8 COL 16.72 COLON-ALIGNED
     NewBatch.BatchType AT ROW 4.2 COL 7.57 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 5.43 BY 1
          BGCOLOR 16 
     NewBatch.DocumentCount AT ROW 4.2 COL 27 COLON-ALIGNED
          LABEL "Documents"
          VIEW-AS FILL-IN 
          SIZE 8.43 BY .9
          BGCOLOR 16 
     NewBatch.Total AT ROW 4.2 COL 48.72 COLON-ALIGNED FORMAT "-ZZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 16.43 BY .9
          BGCOLOR 16 
     fil_BankAct AT ROW 5.4 COL 5.14
     btn_print AT ROW 6.8 COL 51.29
     RECT-13 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_print.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.NewBatch
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 9.4
         WIDTH              = 71.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN NewBatch.BatchType IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN NewBatch.Description IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN NewBatch.DocumentCount IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN fil_BankAct IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN fil_Operator IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN NewBatch.Total IN FRAME F-Main
   NO-ENABLE EXP-FORMAT                                                 */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* Print */
DO:
  RUN print-deposit-report.
  RUN dispatch ( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "NewBatch"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "NewBatch"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE post-row-available V-table-Win 
PROCEDURE post-row-available :
/*------------------------------------------------------------------------------
  Purpose:  Set the local batch code to the current row available
------------------------------------------------------------------------------*/
  IF AVAILABLE(NewBatch) THEN DO:
    FIND Person WHERE Person.PersonCode = NewBatch.PersonCode NO-LOCK NO-ERROR.
    IF AVAILABLE(Person) AND Person.PersonCode > 0 THEN DO:
      ASSIGN
        fil_Operator = Person.FirstName + " " + Person.LastName
      .
      DISPLAY fil_Operator
                 WITH FRAME {&FRAME-NAME} IN WINDOW {&WINDOW-NAME}.
    END.
    FIND FIRST NewAcctTrans WHERE NewAccttrans.BatchCode = NewBatch.BatchCode
                            AND NewAcctTrans.DocumentCode = 2
                            NO-LOCK NO-ERROR.
    IF AVAILABLE(NewAcctTrans) THEN DO:
      FIND FIRST BankAccount WHERE BankAccount.CompanyCode = NewAcctTrans.EntityCode
                       AND BankAccount.AccountCode = NewAcctTrans.AccountCode
                       NO-LOCK.
      FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = NewAcctTrans.AccountCode NO-LOCK.
      ASSIGN
        fil_BankAct = TRIM( STRING( NewAcctTrans.EntityCode, ">>999")) + "/"
                    + STRING( NewAcctTrans.AccountCode, "9999.99")
                    + " "
                    + BankAccount.AccountName .
      .
      DISPLAY fil_BankAct
                 WITH FRAME {&FRAME-NAME} IN WINDOW {&WINDOW-NAME}.
      report-options = BankAccount.BankAccountCode + "," + STRING( NewBatch.BatchCode, "99999").
    END.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-deposit-report V-table-Win 
PROCEDURE print-deposit-report :
/*------------------------------------------------------------------------------
  Purpose:  Run the deposit report printing program
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE(NewBatch) THEN
  DO:
    MESSAGE "There is no batch selected for printing!"      SKIP
            "Please choose a valid batch"
      VIEW-AS ALERT-BOX ERROR TITLE "Error Running Print".
    RETURN.
  END.

  DISABLE btn_print WITH FRAME {&FRAME-NAME}.
  IF CURRENT-WINDOW:LOAD-MOUSE-POINTER('WAIT':U) THEN .

  RUN process/report/deposit.p( report-options, no ).
  IF CURRENT-WINDOW:LOAD-MOUSE-POINTER('ARROW':U) THEN .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "NewBatch"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


