&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR debug-mode AS LOGI NO-UNDO INITIAL No.
DEF VAR mode AS CHAR NO-UNDO.
DEF VAR committing-charge AS LOGI NO-UNDO.

DEF VAR maintain-enabled AS LOGI INITIAL No NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RentChargeLine
&Scoped-define FIRST-EXTERNAL-TABLE RentChargeLine


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RentChargeLine.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RentChargeLine.StartDate ~
RentChargeLine.EndDate RentChargeLine.Amount ~
RentChargeLine.EstimatedNetRentAmount RentChargeLine.LastChargedDate 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}StartDate ~{&FP2}StartDate ~{&FP3}~
 ~{&FP1}EndDate ~{&FP2}EndDate ~{&FP3}~
 ~{&FP1}Amount ~{&FP2}Amount ~{&FP3}~
 ~{&FP1}EstimatedNetRentAmount ~{&FP2}EstimatedNetRentAmount ~{&FP3}~
 ~{&FP1}LastChargedDate ~{&FP2}LastChargedDate ~{&FP3}
&Scoped-define ENABLED-TABLES RentChargeLine
&Scoped-define FIRST-ENABLED-TABLE RentChargeLine
&Scoped-Define ENABLED-OBJECTS RECT-34 cmb_status cmb_freq fil_AnnualAmount 
&Scoped-Define DISPLAYED-FIELDS RentChargeLine.StartDate ~
RentChargeLine.EndDate RentChargeLine.Amount ~
RentChargeLine.EstimatedNetRentAmount RentChargeLine.LastChargedDate 
&Scoped-Define DISPLAYED-OBJECTS cmb_status cmb_freq fil_AnnualAmount 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD include-lease V-table-Win 
FUNCTION include-lease RETURNS LOGICAL
  ( INPUT lease-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_freq AS CHARACTER FORMAT "X(256)":U 
     LABEL "Frequency" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS " "
     SIZE 20.57 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_status AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS " "
     SIZE 20.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_AnnualAmount AS DECIMAL FORMAT "->>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Annual Amount" 
     VIEW-AS FILL-IN 
     SIZE 13.72 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 46.86 BY 11.4.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     cmb_status AT ROW 1.2 COL 11 COLON-ALIGNED
     RentChargeLine.StartDate AT ROW 2.6 COL 11 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     RentChargeLine.EndDate AT ROW 2.6 COL 33.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     cmb_freq AT ROW 5 COL 11 COLON-ALIGNED
     RentChargeLine.Amount AT ROW 6 COL 21.86 COLON-ALIGNED
          LABEL "Period Amount"
          VIEW-AS FILL-IN 
          SIZE 13.72 BY 1
     fil_AnnualAmount AT ROW 7 COL 21.86 COLON-ALIGNED
     RentChargeLine.EstimatedNetRentAmount AT ROW 8.2 COL 21.86 COLON-ALIGNED
          LABEL "Estimated Net Annual Rental"
          VIEW-AS FILL-IN 
          SIZE 13.72 BY 1.05
     RentChargeLine.LastChargedDate AT ROW 9.4 COL 11 COLON-ALIGNED
          LABEL "Charged Up To"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     RECT-34 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RentChargeLine
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 14.95
         WIDTH              = 58.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RentChargeLine.Amount IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RentChargeLine.EstimatedNetRentAmount IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RentChargeLine.LastChargedDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/rentchrg.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME RentChargeLine.Amount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RentChargeLine.Amount V-table-Win
ON LEAVE OF RentChargeLine.Amount IN FRAME F-Main /* Period Amount */
DO:
  IF SELF:MODIFIED THEN RUN calculate-annual-from-period.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_freq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON U1 OF cmb_freq IN FRAME F-Main /* Frequency */
DO:
  {inc/selcmb/scfty1.i "RentChargeLine" "FrequencyCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON U2 OF cmb_freq IN FRAME F-Main /* Frequency */
DO:
  {inc/selcmb/scfty2.i "RentChargeLine" "FrequencyCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON VALUE-CHANGED OF cmb_freq IN FRAME F-Main /* Frequency */
DO:
  RUN calculate-annual-from-period.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_status
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_status V-table-Win
ON U1 OF cmb_status IN FRAME F-Main /* Status */
DO:
  {inc/selcmb/scrclst1.i "RentChargeLine" "RentChargeLineStatus"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_status V-table-Win
ON U2 OF cmb_status IN FRAME F-Main /* Status */
DO:
  IF committing-charge THEN RETURN.
  {inc/selcmb/scrclst2.i "RentChargeLine" "RentChargeLineStatus"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RentChargeLine.EndDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RentChargeLine.EndDate V-table-Win
ON ANY-KEY OF RentChargeLine.EndDate IN FRAME F-Main /* EndDate */
DO:
  IF LAST-EVENT:LABEL = "DEL" THEN DO:
    SELF:SCREEN-VALUE = "".
    {&SELF-NAME} = ?.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_AnnualAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_AnnualAmount V-table-Win
ON LEAVE OF fil_AnnualAmount IN FRAME F-Main /* Annual Amount */
DO:
  IF SELF:MODIFIED THEN RUN calculate-period-from-annual.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RentChargeLine.LastChargedDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RentChargeLine.LastChargedDate V-table-Win
ON ANY-KEY OF RentChargeLine.LastChargedDate IN FRAME F-Main /* Charged Up To */
DO:
  IF LAST-EVENT:LABEL = "DEL" THEN DO:
    SELF:SCREEN-VALUE = "".
    {&SELF-NAME} = ?.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RECT-34
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RECT-34 V-table-Win
ON MOUSE-SELECT-DBLCLICK OF RECT-34 IN FRAME F-Main
DO:
  RUN special-enable-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RentChargeLine"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RentChargeLine"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculate-annual-from-period V-table-Win 
PROCEDURE calculate-annual-from-period :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR period-amnt AS DEC NO-UNDO.
DEF VAR annual-amnt AS DEC NO-UNDO.
DEF VAR freq-type AS CHAR NO-UNDO.
DEF VAR part-of-year AS DEC NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  freq-type = ENTRY( 1, cmb_freq:SCREEN-VALUE, " ").
  period-amnt = INPUT RentChargeLine.Amount.
  RUN process/calcfreq.p ( INPUT freq-type, OUTPUT part-of-year ).
  annual-amnt = ROUND( period-amnt / part-of-year, 2).

  fil_AnnualAmount = annual-amnt.
  DISPLAY fil_AnnualAmount.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculate-period-from-annual V-table-Win 
PROCEDURE calculate-period-from-annual :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR period-amnt AS DEC NO-UNDO.
DEF VAR annual-amnt AS DEC NO-UNDO.
DEF VAR freq-type AS CHAR NO-UNDO.
DEF VAR part-of-year AS DEC NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  freq-type = ENTRY( 1, cmb_freq:SCREEN-VALUE, " ").
  annual-amnt = INPUT fil_AnnualAmount.
  RUN process/calcfreq.p ( INPUT freq-type, OUTPUT part-of-year ).
  period-amnt = ROUND( annual-amnt * part-of-year, 2).

  RentChargeLine.Amount:SCREEN-VALUE = STRING( period-amnt, RentChargeLine.Amount:FORMAT).
  RentChargeLine.Amount:MODIFIED = Yes.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-rent-charge-line.
  RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  RUN verify-rent-charge-line.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  /* Set a flag if the charge is being committed as chargeable for the first time
     so we don't just set the chargestatus and screw up the commit charge
     validation */
  committing-charge = SUBSTR( INPUT cmb_status, 1, 1 ) = "C" AND
    RentChargeLine.RentchargeLineStatus <> SUBSTR( INPUT cmb_status, 1, 1 ).
    
  RUN dispatch( 'update-record':U ).

  IF committing-charge THEN DO:
    RUN commit-charge-entry( RentChargeLine.TenancyLeaseCode,
                       RentChargeLine.SequenceCode, RentChargeLine.StartDate ).
    IF RETURN-VALUE = "FAIL" THEN
      MESSAGE "Rent Charge changes not committed!" VIEW-AS ALERT-BOX WARNING
                TITLE "Changes not applied!".
  END.
  
  IF mode = "Add" OR committing-charge THEN RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit':U ).

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-rent-charge-line V-table-Win 
PROCEDURE delete-rent-charge-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT RentChargeLine EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE RentChargeLine THEN DELETE RentChargeLine NO-ERROR.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF VAR maintainable AS LOGI NO-UNDO.
  maintainable = (RentChargeLine.RentchargeLineStatus <> "C")
               OR maintain-enabled.
  
  RentChargeLine.StartDate:SENSITIVE = maintainable.
  RentChargeLine.EndDate:SENSITIVE   = maintainable.
  RentChargeLine.Amount:SENSITIVE    = maintainable.
  cmb_Freq:SENSITIVE                 = maintainable.
  cmb_Status:SENSITIVE               = maintainable.
  fil_AnnualAmount:SENSITIVE         = maintainable.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN enable-appropriate-fields.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR rights AS LOGICAL NO-UNDO.
  RUN get-rights IN sys-mgr( "V-RCHGLN", "MODIFY", OUTPUT rights).
  maintain-enabled = rights.

  IF mode = "Add" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record' ).
  END.
  ELSE DO:
    RUN calculate-annual-from-period.
    RUN dispatch( 'enable-fields':U ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  CURRENT-WINDOW:TITLE = "Scheduling a charge".
  
  RUN create-charge-entry( 
    INT( find-parent-key( 'TenancyLeaseCode' ) ),
    INT( find-parent-key( 'SequenceCode' ) ),
    ?,
    ?,
    "MNTH",
    0.00
  ).

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  ASSIGN
    cmb_status:SCREEN-VALUE = cmb_status:ENTRY( 1 )
    cmb_status:MODIFIED     = Yes
    RentChargeLine.StartDate:SCREEN-VALUE = STRING( TODAY ).

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RentChargeLine"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE special-enable-fields V-table-Win 
PROCEDURE special-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/
DEF VAR rights AS LOGICAL NO-UNDO.

  RUN get-rights IN sys-mgr( "V-RCHGLN", "MODIFY", OUTPUT rights).
  maintain-enabled = rights.

  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-rent-charge-line V-table-Win 
PROCEDURE verify-rent-charge-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  IF SUBSTR( INPUT cmb_status, 1, 1 ) = "C" THEN DO:
    
    /* Check that all required variables are entered and valid */
    IF INPUT RentChargeLine.StartDate = ? THEN DO:
      MESSAGE "You must enter a start date!" VIEW-AS ALERT-BOX ERROR
        TITLE "Invalid Start Date".
      APPLY 'ENTRY':U TO RentChargeLine.StartDate.
      RETURN "FAIL".
    END.

    DEF BUFFER OtherChargeEntry FOR RentChargeLine.
    IF ( mode = "Add" OR
         mode = "Maintain" AND INPUT RentChargeLine.StartDate <> RentChargeLine.StartDate )
      AND CAN-FIND( OtherChargeEntry
      WHERE OtherChargeEntry.TenancyLeaseCode = RentChargeLine.TenancyLeaseCode
        AND OtherChargeEntry.SequenceCode     = RentChargeLine.SequenceCode
        AND OtherChargeEntry.StartDate        = INPUT RentChargeLine.StartDate )
    THEN DO:
      MESSAGE "An scheduled charge already exists with" SKIP
              "the start date" INPUT RentChargeLine.StartDate 
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid Start Date".
      APPLY 'ENTRY':U TO RentChargeLine.StartDate.
      RETURN "FAIL".
    END.

    IF INPUT RentChargeLine.LastChargedDate <> ? AND
       INPUT RentChargeLine.LastChargedDate < (INPUT RentChargeLine.StartDate - 1)THEN DO:
      MESSAGE "The charged up to date should be" SKIP
              "greater than the start date" VIEW-AS ALERT-BOX WARNING
        TITLE "'Charged To' earlier than start of charge".
      APPLY 'ENTRY':U TO RentChargeLine.LastChargedDate.
    END.
    
    IF INPUT cmb_freq = "" THEN DO:
      MESSAGE "You must enter a frequency!" VIEW-AS ALERT-BOX ERROR
        TITLE "Invalid Frequency".
      APPLY 'ENTRY':U TO cmb_freq.
      RETURN "FAIL".
    END.
    
    IF INPUT RentchargeLine.Amount = 0.00 THEN DO:
      MESSAGE "You must enter an amount!" VIEW-AS ALERT-BOX ERROR
        TITLE "Zero Amount Entered".
      APPLY 'ENTRY':U TO RentchargeLine.Amount.
      RETURN "FAIL".
    END.
    
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION include-lease V-table-Win 
FUNCTION include-lease RETURNS LOGICAL
  ( INPUT lease-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  Dummy function.
------------------------------------------------------------------------------*/

  RETURN Yes.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


