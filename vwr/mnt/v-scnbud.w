&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&GLOB RPT-NAME "ApplyToBudgets"

/* Local Variable Definitions ---                                       */

DEF VAR user-name AS CHAR NO-UNDO.  
{inc/username.i "user-name"}

{inc/ofc-this.i}
{inc/ofc-set.i "Budget-Closed-To" "budget-closed-to"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Log4 RP.Int3 RP.Char6 RP.Log2 RP.Char2 ~
RP.Int1 RP.Int2 RP.Char1 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Int3 ~{&FP2}Int3 ~{&FP3}~
 ~{&FP1}Char6 ~{&FP2}Char6 ~{&FP3}~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-1 cmb_mth-from cmb_mth-to btn_ok 
&Scoped-Define DISPLAYED-FIELDS RP.Log4 RP.Int3 RP.Char6 RP.Log2 RP.Char2 ~
RP.Int1 RP.Int2 RP.Char1 
&Scoped-Define DISPLAYED-OBJECTS fil_Scenario cmb_mth-from cmb_mth-to 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS></FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = ':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_ok 
     LABEL "&OK" 
     SIZE 9.72 BY 1
     FONT 9.

DEFINE VARIABLE cmb_mth-from AS CHARACTER FORMAT "X(20)" INITIAL ? 
     LABEL "From" 
     VIEW-AS COMBO-BOX INNER-LINES 18
     LIST-ITEMS "Start of first month" 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_mth-to AS CHARACTER FORMAT "X(18)" INITIAL ? 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 18
     LIST-ITEMS "End of last month" 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Scenario AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 39.43 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 67.43 BY 11.4.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Log4 AT ROW 1.4 COL 1.57 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Single Scenario", yes,
"Scenario Set", no
          SIZE 13.72 BY 2
     RP.Int3 AT ROW 1.4 COL 16.72 COLON-ALIGNED NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_Scenario AT ROW 1.4 COL 26.43 COLON-ALIGNED NO-LABEL
     RP.Char6 AT ROW 2.4 COL 16.72 COLON-ALIGNED HELP
          ""
          LABEL "Set" FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 49.14 BY 1
     RP.Log2 AT ROW 3.6 COL 6.72
          LABEL "Entity range restriction"
          VIEW-AS TOGGLE-BOX
          SIZE 18.29 BY 1.05
     RP.Char2 AT ROW 3.6 COL 26.43 HELP
          ""
          LABEL "Type" FORMAT "X(255)"
          VIEW-AS COMBO-BOX INNER-LINES 10
          LIST-ITEMS "P - Properties","L - General Ledger","J - Project","F - Fixed Asset","C - Creditor","T - Tenant" 
          SIZE 16 BY 1.05
     RP.Int1 AT ROW 3.6 COL 50.43 COLON-ALIGNED HELP
          ""
          LABEL "From" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 5.72 BY 1.05
     RP.Int2 AT ROW 3.6 COL 60.14 COLON-ALIGNED HELP
          ""
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 5.72 BY 1.05
     cmb_mth-from AT ROW 5 COL 4.72 COLON-ALIGNED
     cmb_mth-to AT ROW 5 COL 28.72 COLON-ALIGNED
     RP.Char1 AT ROW 7.2 COL 6.72 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Clear affected accounts", "accounts":U,
"Clear affected property accounts only", "prop-accounts":U,
"Clear all accounts of affected properties ", "properties":U,
"Clear all accounts of all affected ledgers ", "entities":U,
"Clear all ledgers and accounts", "all":U,
"Do not clear any forecasts", "none":U
          SIZE 30.86 BY 4.8
     btn_ok AT ROW 11.2 COL 58.14
     RECT-1 AT ROW 1 COL 1
     "Before writing forecasts:" VIEW-AS TEXT
          SIZE 18.29 BY .8 AT ROW 6.4 COL 2.72
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.2
         WIDTH              = 70.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR RADIO-SET RP.Char1 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR COMBO-BOX RP.Char2 IN FRAME F-Main
   ALIGN-L EXP-LABEL EXP-FORMAT EXP-HELP                                */
/* SETTINGS FOR FILL-IN RP.Char6 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_Scenario IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR RADIO-SET RP.Log4 IN FRAME F-Main
   EXP-HELP                                                             */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/date.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON CHOOSE OF btn_ok IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_mth-from
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_mth-from V-table-Win
ON U1 OF cmb_mth-from IN FRAME F-Main /* From */
DO:
  {inc/selcmb/scmths1.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_mth-from V-table-Win
ON U2 OF cmb_mth-from IN FRAME F-Main /* From */
DO:
  {inc/selcmb/scmths2.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_mth-to
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_mth-to V-table-Win
ON U1 OF cmb_mth-to IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_mth-to V-table-Win
ON U2 OF cmb_mth-to IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Scenario
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Scenario V-table-Win
ON U1 OF fil_Scenario IN FRAME F-Main
DO:
  {inc/selfil/sfscn1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Scenario V-table-Win
ON U2 OF fil_Scenario IN FRAME F-Main
DO:
  {inc/selfil/sfscn2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Scenario V-table-Win
ON U3 OF fil_Scenario IN FRAME F-Main
DO:
  {inc/selfil/sfscn3.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int3 V-table-Win
ON LEAVE OF RP.Int3 IN FRAME F-Main /* Int3 */
DO:
  {inc/selcde/cdscn.i "fil_Scenario"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Entity range restriction */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Log4 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF INPUT RP.Log2 THEN
    VIEW RP.Char2 RP.Int1 RP.Int2 .
  ELSE
    HIDE RP.Char2 RP.Int1 RP.Int2 .

  IF INPUT RP.Log4 THEN DO:
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Scenario:HANDLE ), "HIDDEN = No" ).
    HIDE RP.Char6 .
    VIEW RP.Int3 fil_Scenario .
  END.
  ELSE DO:
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Scenario:HANDLE ), "HIDDEN = Yes" ).
    VIEW RP.Char6 .
    HIDE RP.Int3 fil_Scenario .
  END.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/


  FIND RP WHERE
    RP.ReportID = {&RPT-NAME} AND
    RP.UserName = user-name NO-ERROR.

  IF NOT AVAILABLE RP THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&RPT-NAME}
      RP.UserName = user-name
      RP.Log2     = Yes
      RP.Log4     = Yes .
  END.

/*
DO WITH FRAME {&FRAME-NAME}:

  RP.Char2:LIST-ITEMS = "".

  FOR EACH EntityType NO-LOCK:
    IF RP.Char2:INSERT( EntityType.EntityType + " - " + EntityType.Description, -1 ) THEN .
  END.

END.
*/
  RUN dispatch ( 'display-fields':U ).

  RUN dispatch ( 'enable-fields':U ).
  RUN enable-appropriate-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN "FAIL".
  
  RUN dispatch( 'update-record':U ).
  
  DEF VAR report-options     AS CHAR NO-UNDO.
  DEF VAR entity-range       AS CHAR NO-UNDO.

  IF RP.Log2 THEN entity-range = "~nEntityType," + RP.Char2
                               + "~nEntityRange," + STRING( RP.Int1 ) + "," + STRING( RP.Int2 ).

  report-options = (IF RP.Log4 THEN "Scenario,"       + STRING(RP.Int3)
                               ELSE "ScenarioSet,"    + RP.Char6 )
                 + entity-range
                 + "~nClear," + RP.Char1
                 + "~nMonthRange," + STRING(RP.Int4) + "," + STRING(RP.Int5) .

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN process/scn2bud.p( report-options ).
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* There are no foreign keys supplied by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


