&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tpvouchr.i}

{inc/ofc-this.i}
DEF VAR gst-applies    AS LOGI NO-UNDO.
gst-applies = Office.GST <> ?.
DEF VAR gst-in-ecode           AS INT NO-UNDO.
DEF VAR sundry-creditors-ecode AS INT NO-UNDO.
{inc/ofc-acct.i "CREDITORS" "sundry-creditors"}
sundry-creditors-ecode = OfficeControlAccount.EntityCode.
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}
{inc/ofc-acct.i "GST-IN" "gst-in"}
gst-in-ecode = OfficeControlAccount.EntityCode.

{inc/ofc-set.i "Restrict-Project-Posting" "restrict-project-posting"}
IF NOT AVAILABLE(OfficeSetting) THEN restrict-project-posting = "".
{inc/ofc-set.i "Voucher-Style" "voucher-style" "WARNING"}
{inc/ofc-set.i "Voucher-NoDupCheck" "voucher-nodupcheck"}
IF NOT AVAILABLE(OfficeSetting) THEN voucher-nodupcheck = "".

{inc/ofc-set.i "Voucher-Orders" "voucher-orders" "WARNING"}
{inc/ofc-set.i "Voucher-Printing" "print-vouchers"}
{inc/ofc-set-l.i "Tenant-Accounts" "tenant-accounts"}

/* DEF VAR coa-list AS HANDLE NO-UNDO. */
/* DEF VAR enabling-fields AS LOGICAL NO-UNDO INITIAL No. */
/* DEF VAR account-code AS DEC NO-UNDO. */

DEF VAR insert-new-line AS LOGI NO-UNDO.
/* DEF VAR new-result-entry AS LOGICAL NO-UNDO. */
/* DEF VAR my-new-line     AS LOGI NO-UNDO INITIAL No. */
DEF VAR trn-modified    AS LOGI NO-UNDO.
DEF VAR last-line       AS LOGI NO-UNDO.
DEF VAR verifying-line  AS LOGI NO-UNDO.
DEF VAR selecting-code  AS LOGI NO-UNDO.


DEF VAR mode            AS CHAR NO-UNDO.
DEF VAR last-creditor   LIKE Voucher.CreditorCode NO-UNDO.
DEF VAR last_Period     AS CHAR NO-UNDO INITIAL "".
DEF VAR last_PeriodFrom AS DATE NO-UNDO INITIAL ?.
DEF VAR last_PeriodTo   AS DATE NO-UNDO INITIAL ?.

DEF WORK-TABLE CurrentLine NO-UNDO LIKE VoucherLine.
DEF WORK-TABLE DefTrans    NO-UNDO LIKE VoucherLine.

DEF VAR last-order-code   AS INT NO-UNDO.
DEF VAR last-project-code AS INT NO-UNDO.

DEF WORK-TABLE AccountComboList NO-UNDO
        FIELD EntityType    AS CHAR
        FIELD AccountList   AS CHAR
        FIELD CurrentEntry  AS CHAR.

DEF WORK-TABLE EntityComboList NO-UNDO
        FIELD EntityType    AS CHAR
        FIELD EntityList    AS CHAR
        FIELD CurrentEntry  AS CHAR.

DEF WORK-TABLE LastVoucher NO-UNDO LIKE Voucher.
CREATE LastVoucher.
LastVoucher.VoucherSeq = ?.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-vchlne

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Voucher
&Scoped-define FIRST-EXTERNAL-TABLE Voucher


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Voucher.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES VoucherLine

/* Definitions for BROWSE br-vchlne                                     */
&Scoped-define FIELDS-IN-QUERY-br-vchlne VoucherLine.EntityType ~
VoucherLine.EntityCode VoucherLine.AccountCode VoucherLine.Description ~
VoucherLine.Amount VoucherLine.TaxAmount 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-vchlne VoucherLine.EntityType ~
VoucherLine.EntityCode VoucherLine.AccountCode VoucherLine.Description ~
VoucherLine.Amount VoucherLine.TaxAmount 
&Scoped-define ENABLED-TABLES-IN-QUERY-br-vchlne VoucherLine
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br-vchlne VoucherLine
&Scoped-define QUERY-STRING-br-vchlne FOR EACH VoucherLine OF Voucher NO-LOCK
&Scoped-define OPEN-QUERY-br-vchlne OPEN QUERY br-vchlne FOR EACH VoucherLine OF Voucher NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br-vchlne VoucherLine
&Scoped-define FIRST-TABLE-IN-QUERY-br-vchlne VoucherLine


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Voucher.VoucherSeq Voucher.ProjectCode ~
Voucher.OrderCode Voucher.CreditorCode Voucher.InvoiceReference ~
Voucher.Date Voucher.DateDue Voucher.Description 
&Scoped-define ENABLED-TABLES Voucher
&Scoped-define FIRST-ENABLED-TABLE Voucher
&Scoped-Define ENABLED-OBJECTS fil_Period fil_PeriodFrom fil_PeriodTo ~
cmb_PaymentStyle br-vchlne Btn_Add-Trn btn_ok btn_cancel btn_UnlockFields ~
RECT-1 RECT-2 
&Scoped-Define DISPLAYED-FIELDS Voucher.VoucherSeq Voucher.ProjectCode ~
Voucher.OrderCode Voucher.CreditorCode Voucher.InvoiceReference ~
Voucher.Date Voucher.DateDue Voucher.Description Voucher.GoodsValue 
&Scoped-define DISPLAYED-TABLES Voucher
&Scoped-define FIRST-DISPLAYED-TABLE Voucher
&Scoped-Define DISPLAYED-OBJECTS fil_Creditor fil_Period fil_PeriodFrom ~
fil_PeriodTo cmb_PaymentStyle fil_total fil_Entity fil_Account 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD calculate-period V-table-Win 
FUNCTION calculate-period RETURNS LOGICAL
  ( INPUT period-char AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-client V-table-Win 
FUNCTION get-client RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-parent-entity V-table-Win 
FUNCTION get-parent-entity RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-remaining-gross V-table-Win 
FUNCTION get-remaining-gross RETURNS DECIMAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-remaining-tax V-table-Win 
FUNCTION get-remaining-tax RETURNS DECIMAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD string-to-date V-table-Win 
FUNCTION string-to-date RETURNS DATE
  ( INPUT date-string AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-br-vchlne 
       MENU-ITEM m_Delete       LABEL "Delete Line"   
       MENU-ITEM m_Add          LABEL "Add Line"      .


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "Register &Another" 
     SIZE 16 BY 1.05 TOOLTIP "Finalise the registration of this voucher and start registering another."
     FONT 9.

DEFINE BUTTON Btn_Add-Trn 
     LABEL "Add Tr" 
     SIZE 15 BY 1.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 9.72 BY 1.05
     FONT 9.

DEFINE BUTTON btn_ok 
     LABEL "&Done" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_UnlockFields 
     LABEL "Unlock Fields" 
     SIZE 15 BY 1.

DEFINE VARIABLE cmb_PaymentStyle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Pay by" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 24.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Account AS CHARACTER FORMAT "X(256)":U 
     LABEL "Account" 
     VIEW-AS FILL-IN 
     SIZE 75.43 BY .9
     FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE fil_Creditor AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 46.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Entity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS FILL-IN 
     SIZE 45.72 BY .9
     FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE fil_Period AS CHARACTER FORMAT "X(256)":U 
     LABEL "Period" 
     VIEW-AS FILL-IN 
     SIZE 23.43 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_PeriodFrom AS DATE FORMAT "99/99/9999":U 
     LABEL "From" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_PeriodTo AS DATE FORMAT "99/99/9999":U 
     LABEL "to" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_total AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Gross expense" 
     VIEW-AS FILL-IN 
     SIZE 15.86 BY 1
     FONT 11 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 101.14 BY 16.8.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 100 BY 5.4.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-vchlne FOR 
      VoucherLine SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-vchlne
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-vchlne V-table-Win _STRUCTURED
  QUERY br-vchlne NO-LOCK DISPLAY
      VoucherLine.EntityType FORMAT "X":U WIDTH 1.86 COLUMN-FONT 10
      VoucherLine.EntityCode FORMAT "99999":U
      VoucherLine.AccountCode FORMAT "9999.99":U WIDTH 6.29
      VoucherLine.Description FORMAT "X(50)":U WIDTH 51.29
      VoucherLine.Amount COLUMN-LABEL "Gross Amount" FORMAT "->>>,>>>,>>9.99":U
            WIDTH 14.29
      VoucherLine.TaxAmount COLUMN-LABEL "Tax Amount" FORMAT "->>,>>>,>>9.99":U
            WIDTH 14.43
  ENABLE
      VoucherLine.EntityType
      VoucherLine.EntityCode
      VoucherLine.AccountCode
      VoucherLine.Description
      VoucherLine.Amount
      VoucherLine.TaxAmount
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 100 BY 6.5
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Voucher.VoucherSeq AT ROW 1.2 COL 8.72 COLON-ALIGNED
          LABEL "Voucher No"
          VIEW-AS FILL-IN 
          SIZE 9.72 BY 1
     Voucher.ProjectCode AT ROW 1.2 COL 26.43 COLON-ALIGNED
          LABEL "Prj/Order" FORMAT ">>>>9"
          VIEW-AS FILL-IN 
          SIZE 5.72 BY 1
     Voucher.OrderCode AT ROW 1.2 COL 32.14 COLON-ALIGNED NO-LABEL FORMAT ">>9"
          VIEW-AS FILL-IN 
          SIZE 4 BY 1
     Voucher.CreditorCode AT ROW 1.2 COL 43.57 COLON-ALIGNED
          LABEL "Creditor"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_Creditor AT ROW 1.2 COL 53.29 COLON-ALIGNED NO-LABEL
     Voucher.InvoiceReference AT ROW 3 COL 8.72 COLON-ALIGNED
          LABEL "Supplier Ref" FORMAT "X(30)"
          VIEW-AS FILL-IN 
          SIZE 23.43 BY 1
     Voucher.Date AT ROW 3 COL 43.57 COLON-ALIGNED
          LABEL "Invc. Date"
          VIEW-AS FILL-IN 
          SIZE 10.86 BY 1
     fil_Period AT ROW 4.4 COL 8.72 COLON-ALIGNED
     fil_PeriodFrom AT ROW 4.4 COL 43.57 COLON-ALIGNED
     fil_PeriodTo AT ROW 4.4 COL 57.29 COLON-ALIGNED
     Voucher.DateDue AT ROW 5.4 COL 8.72 COLON-ALIGNED
          LABEL "Due Date"
          VIEW-AS FILL-IN 
          SIZE 11.57 BY 1
     cmb_PaymentStyle AT ROW 5.4 COL 43.57 COLON-ALIGNED
     fil_total AT ROW 4.4 COL 83.14 COLON-ALIGNED
     Voucher.TaxValue AT ROW 5.4 COL 83.14 COLON-ALIGNED
          LABEL "GST" FORMAT "-ZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 15.86 BY 1
          FONT 11
     Voucher.Description AT ROW 6.8 COL 8.72 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 75.43 BY 1
     Voucher.GoodsValue AT ROW 8 COL 84.14 COLON-ALIGNED
          LABEL "Goods value" FORMAT "-Z,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14.86 BY 1
          FONT 11
     fil_Entity AT ROW 8.2 COL 8.72 COLON-ALIGNED
     fil_Account AT ROW 9 COL 8.72 COLON-ALIGNED
     br-vchlne AT ROW 10 COL 1.57
     btn_add AT ROW 16.6 COL 1.57
     Btn_Add-Trn AT ROW 16.6 COL 26.14
     btn_ok AT ROW 16.6 COL 81
     btn_cancel AT ROW 16.6 COL 91.86
     btn_UnlockFields AT ROW 16.6 COL 47.86
     RECT-1 AT ROW 1 COL 1
     RECT-2 AT ROW 2.6 COL 1.57
     "Invoice Details" VIEW-AS TEXT
          SIZE 14.86 BY .8 AT ROW 2.2 COL 2.14
          FONT 14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Voucher
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 22.1
         WIDTH              = 110.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/string.i}
{inc/date.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
/* BROWSE-TAB br-vchlne fil_Account F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br-vchlne:POPUP-MENU IN FRAME F-Main             = MENU POPUP-MENU-br-vchlne:HANDLE.

/* SETTINGS FOR BUTTON btn_add IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Voucher.CreditorCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Voucher.Date IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Voucher.DateDue IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Account IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Creditor IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Entity IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_total IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       fil_total:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN Voucher.GoodsValue IN FRAME F-Main
   NO-ENABLE EXP-LABEL EXP-FORMAT                                       */
/* SETTINGS FOR FILL-IN Voucher.InvoiceReference IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Voucher.OrderCode IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Voucher.ProjectCode IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Voucher.TaxValue IN FRAME F-Main
   NO-DISPLAY NO-ENABLE EXP-LABEL EXP-FORMAT                            */
ASSIGN 
       Voucher.TaxValue:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN Voucher.VoucherSeq IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-vchlne
/* Query rebuild information for BROWSE br-vchlne
     _TblList          = "ttpl.VoucherLine OF ttpl.Voucher"
     _Options          = "NO-LOCK"
     _FldNameList[1]   > TTPL.VoucherLine.EntityType
"VoucherLine.EntityType" ? ? "character" ? ? 10 ? ? ? yes ? no no "1.86" yes no no "U" "" ""
     _FldNameList[2]   > TTPL.VoucherLine.EntityCode
"VoucherLine.EntityCode" ? ? "integer" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[3]   > TTPL.VoucherLine.AccountCode
"VoucherLine.AccountCode" ? ? "decimal" ? ? ? ? ? ? yes ? no no "6.29" yes no no "U" "" ""
     _FldNameList[4]   > TTPL.VoucherLine.Description
"VoucherLine.Description" ? ? "character" ? ? ? ? ? ? yes ? no no "51.29" yes no no "U" "" ""
     _FldNameList[5]   > TTPL.VoucherLine.Amount
"VoucherLine.Amount" "Gross Amount" ? "decimal" ? ? ? ? ? ? yes ? no no "14.29" yes no no "U" "" ""
     _FldNameList[6]   > TTPL.VoucherLine.TaxAmount
"VoucherLine.TaxAmount" "Tax Amount" ? "decimal" ? ? ? ? ? ? yes ? no no "14.43" yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br-vchlne */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON RETURN OF FRAME F-Main
ANYWHERE DO:
  IF LOOKUP( FOCUS:TYPE, 'FILL-IN,COMBO-BOX,EDITOR') > 0
                 OR (FOCUS:TYPE = 'BUTTON':U AND FOCUS:DYNAMIC) THEN DO:
    APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-vchlne
&Scoped-define SELF-NAME br-vchlne
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON CTRL-DEL OF br-vchlne IN FRAME F-Main
OR 'SHIFT-DEL':U OF {&SELF-NAME} ANYWHERE DO:
  RUN delete-line.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON ENTRY OF br-vchlne IN FRAME F-Main
OR 'MOUSE-SELECT-CLICK':U OF {&SELF-NAME} DO:

DEF VAR n-results AS INT NO-UNDO.
DEF VAR s-focused AS LOGI NO-UNDO INITIAL ?.
DEF VAR n-row AS LOGI NO-UNDO INIT ?.

  IF LOOKUP( LAST-EVENT:LABEL, 'ENTER,TAB,MOUSE-SELECT-UP':U ) = 0 THEN RETURN.
  IF NOT AVAILABLE Voucher THEN RETURN.

  n-row = {&BROWSE-NAME}:NEW-ROW IN FRAME {&FRAME-NAME}.
  n-results = NUM-RESULTS("{&BROWSE-NAME}":U).
  IF n-results <> ? AND n-results <> 0 THEN
    s-focused = {&BROWSE-NAME}:SELECT-FOCUSED-ROW().

  IF n-row THEN DO:
    RUN check-last.
    RUN set-trn-defaults.
    trn-modified = Yes.
  END.
  ELSE IF NOT AVAILABLE(VoucherLine) THEN
    RUN add-new-line.

  APPLY 'ENTRY':U TO VoucherLine.EntityType IN BROWSE {&BROWSE-NAME}.
  RETURN NO-APPLY.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON ROW-DISPLAY OF br-vchlne IN FRAME F-Main
DO:
  RUN set-trn-color.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON ROW-ENTRY OF br-vchlne IN FRAME F-Main
DO:
  RUN update-line-editable.
  RUN check-last.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON ROW-LEAVE OF br-vchlne IN FRAME F-Main
OR 'CURSOR-DOWN':U OF {&SELF-NAME} 
OR 'CURSOR-UP':U   OF {&SELF-NAME}
OR 'ENTER':U       OF VoucherLine.TaxAmount IN BROWSE {&SELF-NAME} ANYWHERE DO:

  IF trn-modified THEN DO:
    RUN verify-line.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
  END.

  IF LAST-EVENT:FUNCTION = 'ROW-LEAVE':U AND
     LAST-EVENT:LABEL = 'ENTER':U THEN
  DO:
    IF mode <> "View" AND last-line THEN DO:
      RUN check-line-total.
      IF RETURN-VALUE = "FAIL" THEN
        APPLY 'CHOOSE':U TO btn_Add-Trn IN FRAME {&FRAME-NAME}.
      ELSE IF mode = "Maintain" THEN
        APPLY 'ENTRY':U TO btn_OK IN FRAME {&FRAME-NAME}.
      ELSE
        APPLY 'ENTRY':U TO btn_Add IN FRAME {&FRAME-NAME}.
    END.
    ELSE
      APPLY 'TAB':U TO FOCUS.

    RETURN NO-APPLY.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON VALUE-CHANGED OF br-vchlne IN FRAME F-Main
DO:
  RUN update-trn-fields.  
  IF LAST-EVENT:LABEL = "ENTER" THEN DO:
    APPLY 'ENTRY':U TO VoucherLine.EntityType IN BROWSE br-vchlne.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME VoucherLine.EntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.EntityType br-vchlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF VoucherLine.EntityType IN BROWSE br-vchlne /* T */
DO:

  IF NOT SELF:MODIFIED THEN RETURN.

  RUN verify-entity-type( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN DO:
    SELF:MODIFIED = No.
    RETURN NO-APPLY.
  END.
  trn-modified = Yes.
  RUN set-trn-color.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME VoucherLine.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.EntityCode br-vchlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF VoucherLine.EntityCode IN BROWSE br-vchlne /* Code */
DO:

  IF NOT SELF:MODIFIED THEN RETURN.
  
  RUN verify-entity-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN DO:
    SELF:MODIFIED = no.
    RETURN NO-APPLY.
  END.
  trn-modified = Yes.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME VoucherLine.AccountCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.AccountCode br-vchlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF VoucherLine.AccountCode IN BROWSE br-vchlne /* Account */
DO:

  IF NOT SELF:MODIFIED THEN RETURN.
  
  RUN verify-account-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN DO:
    SELF:MODIFIED = no.
    RETURN NO-APPLY.
  END.
  trn-modified = Yes.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME VoucherLine.Description
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.Description br-vchlne _BROWSE-COLUMN V-table-Win
ON F3 OF VoucherLine.Description IN BROWSE br-vchlne /* Description */
DO:
  RUN update-description.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.Description br-vchlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF VoucherLine.Description IN BROWSE br-vchlne /* Description */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME VoucherLine.Amount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.Amount br-vchlne _BROWSE-COLUMN V-table-Win
ON F3 OF VoucherLine.Amount IN BROWSE br-vchlne /* Gross Amount */
DO:
  SELF:SCREEN-VALUE = STRING( get-remaining-gross(), SELF:FORMAT).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.Amount br-vchlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF VoucherLine.Amount IN BROWSE br-vchlne /* Gross Amount */
DO:
DEF VAR tax-excluded AS DEC NO-UNDO.

  IF SELF:MODIFIED THEN DO WITH FRAME {&FRAME-NAME}:
    trn-modified = Yes.
    RUN set-trn-color.
    tax-excluded = ROUND( INPUT BROWSE {&BROWSE-NAME} VoucherLine.Amount / ( 1 + (Office.GST / 100)), 2 ).
    VoucherLine.TaxAmount:SCREEN-VALUE IN BROWSE {&BROWSE-NAME}
            = STRING( INPUT BROWSE {&BROWSE-NAME} VoucherLine.Amount - tax-excluded, 
                        VoucherLine.TaxAmount:FORMAT ).
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME VoucherLine.TaxAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.TaxAmount br-vchlne _BROWSE-COLUMN V-table-Win
ON F3 OF VoucherLine.TaxAmount IN BROWSE br-vchlne /* Tax Amount */
DO:
  SELF:SCREEN-VALUE = STRING( get-remaining-tax(), SELF:FORMAT).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.TaxAmount br-vchlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF VoucherLine.TaxAmount IN BROWSE br-vchlne /* Tax Amount */
DO:
  IF SELF:MODIFIED THEN trn-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Register Another */
DO:
  RUN verify-voucher.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  RUN dispatch( 'update-record':U ).
  
  RUN dispatch( 'add-record':U ).
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON RETURN OF btn_add IN FRAME F-Main /* Register Another */
DO:
  MESSAGE 'Press the space-bar to activate the button'
                 VIEW-AS ALERT-BOX INFORMATION TITLE "Useless Helpful Hint!".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Add-Trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Add-Trn V-table-Win
ON CHOOSE OF Btn_Add-Trn IN FRAME F-Main /* Add Tr */
DO:
  RUN add-new-line.
/*  RETURN NO-APPLY. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN cancel-changes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON CHOOSE OF btn_ok IN FRAME F-Main /* Done */
DO:
  RUN confirm-changes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON RETURN OF btn_ok IN FRAME F-Main /* Done */
DO:
  MESSAGE 'Press the space-bar to activate the button'
                 VIEW-AS ALERT-BOX INFORMATION TITLE "Useless Helpful Hint!".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_UnlockFields
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_UnlockFields V-table-Win
ON CHOOSE OF btn_UnlockFields IN FRAME F-Main /* Unlock Fields */
DO:
DO WITH FRAME {&FRAME-NAME}:
  Voucher.InvoiceReference:SENSITIVE    = Yes.
  Voucher.Date:SENSITIVE                = Yes.
  Voucher.DateDue:SENSITIVE             = Yes.
  Voucher.Description:SENSITIVE         = Yes.
  fil_Period:SENSITIVE                  = Yes.
  fil_PeriodFrom:SENSITIVE              = Yes.
  fil_PeriodTo:SENSITIVE                = Yes.
  APPLY 'ENTRY':U TO Voucher.InvoiceReference.
  RETURN NO-APPLY.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PaymentStyle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON ENTER OF cmb_PaymentStyle IN FRAME F-Main /* Pay by */
DO:
  APPLY 'TAB':U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON RETURN OF cmb_PaymentStyle IN FRAME F-Main /* Pay by */
DO:
  APPLY 'TAB':U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON U1 OF cmb_PaymentStyle IN FRAME F-Main /* Pay by */
DO:
  {inc/selcmb/scpsty1.i "Voucher" "PaymentStyle"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON U2 OF cmb_PaymentStyle IN FRAME F-Main /* Pay by */
DO:
  {inc/selcmb/scpsty2.i "Voucher" "PaymentStyle"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.CreditorCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.CreditorCode V-table-Win
ON ENTRY OF Voucher.CreditorCode IN FRAME F-Main /* Creditor */
DO:
  SELF:AUTO-ZAP = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.CreditorCode V-table-Win
ON LEAVE OF Voucher.CreditorCode IN FRAME F-Main /* Creditor */
DO:
  IF Voucher.CreditorCode <> INPUT Voucher.CreditorCode THEN
    RUN set-creditor-defaults.

  {inc/selcde/cdcrd.i "fil_Creditor"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.Date
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.Date V-table-Win
ON LEAVE OF Voucher.Date IN FRAME F-Main /* Invc. Date */
DO:
  IF SELF:MODIFIED THEN RUN get-default-due-date.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.Date V-table-Win
ON MOUSE-SELECT-DBLCLICK OF Voucher.Date IN FRAME F-Main /* Invc. Date */
DO:
  SELF:SENSITIVE = NOT( SELF:SENSITIVE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.DateDue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.DateDue V-table-Win
ON MOUSE-SELECT-DBLCLICK OF Voucher.DateDue IN FRAME F-Main /* Due Date */
DO:
  SELF:SENSITIVE = NOT( SELF:SENSITIVE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.Description
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.Description V-table-Win
ON LEAVE OF Voucher.Description IN FRAME F-Main /* Description */
DO:
DO WITH FRAME {&FRAME-NAME}:
DEF VAR lines-changed AS LOGICAL INITIAL No NO-UNDO.
  IF Voucher.Description <> Voucher.Description:SCREEN-VALUE THEN DO TRANSACTION:
    FOR EACH VoucherLine OF Voucher WHERE VoucherLine.Description = Voucher.Description EXCLUSIVE-LOCK:
      VoucherLine.Description = Voucher.Description:SCREEN-VALUE .
      lines-changed = Yes.
    END.
  END.
  IF lines-changed AND br-vchlne:REFRESH() THEN .
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.Description V-table-Win
ON MOUSE-SELECT-DBLCLICK OF Voucher.Description IN FRAME F-Main /* Description */
DO:
  SELF:SENSITIVE = NOT( SELF:SENSITIVE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Creditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U1 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd1.i "Voucher" "CreditorCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U2 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd2.i "Voucher" "CreditorCode"}
  ASSIGN Voucher.Description :SCREEN-VALUE = {&SELF-NAME}:SCREEN-VALUE.
  RUN set-creditor-defaults.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U3 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd3.i "Voucher" "CreditorCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Period
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Period V-table-Win
ON LEAVE OF fil_Period IN FRAME F-Main /* Period */
DO:
  IF TRIM( SELF:SCREEN-VALUE ) = "" THEN
    /* do nothing */.
  ELSE IF calculate-period( SELF:SCREEN-VALUE ) THEN DO:
    /* successfully calculated period */
    APPLY "ENTRY":U TO Voucher.DateDue.
    RETURN NO-APPLY.
  END.
  ELSE DO:
    /* successfully calculated period */
    MESSAGE "period calculation failed " SKIP SELF:SCREEN-VALUE.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Period V-table-Win
ON MOUSE-SELECT-DBLCLICK OF fil_Period IN FRAME F-Main /* Period */
DO:
DO WITH FRAME {&FRAME-NAME}:
  SELF:SENSITIVE = NOT( SELF:SENSITIVE ).
  fil_PeriodFrom:SENSITIVE = SELF:SENSITIVE.
  fil_PeriodTo:SENSITIVE   = SELF:SENSITIVE.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_total
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_total V-table-Win
ON LEAVE OF fil_total IN FRAME F-Main /* Gross expense */
DO:
  IF gst-applies AND SELF:MODIFIED THEN DO WITH FRAME {&FRAME-NAME}:
    Voucher.GoodsValue:SCREEN-VALUE = STRING( INPUT fil_Total / ( 1 + ( Office.GST / 100 ) ), Voucher.GoodsValue:FORMAT ).
    Voucher.TaxValue:SCREEN-VALUE = STRING( INPUT fil_Total - INPUT Voucher.GoodsValue ).

    RUN clear-modifieds.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.InvoiceReference
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.InvoiceReference V-table-Win
ON MOUSE-SELECT-DBLCLICK OF Voucher.InvoiceReference IN FRAME F-Main /* Supplier Ref */
DO:
  SELF:SENSITIVE = NOT( SELF:SENSITIVE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Add V-table-Win
ON CHOOSE OF MENU-ITEM m_Add /* Add Line */
DO:
  APPLY 'TAB':U TO VoucherLine.TaxAmount IN BROWSE {&BROWSE-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete V-table-Win
ON CHOOSE OF MENU-ITEM m_Delete /* Delete Line */
DO:
  RUN delete-line.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.OrderCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.OrderCode V-table-Win
ON LEAVE OF Voucher.OrderCode IN FRAME F-Main /* OrderCode */
DO:
  RUN order-number-changed.
  IF RETURN-VALUE = "NO-APPLY" THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.ProjectCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.ProjectCode V-table-Win
ON LEAVE OF Voucher.ProjectCode IN FRAME F-Main /* Prj/Order */
DO:
  RUN order-number-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.TaxValue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.TaxValue V-table-Win
ON LEAVE OF Voucher.TaxValue IN FRAME F-Main /* GST */
DO:

  IF gst-applies AND SELF:MODIFIED THEN DO WITH FRAME {&FRAME-NAME}:
    Voucher.GoodsValue:SCREEN-VALUE = STRING( INPUT fil_Total - INPUT {&SELF-NAME}, Voucher.GoodsValue:FORMAT).
    RUN clear-modifieds.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.VoucherSeq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.VoucherSeq V-table-Win
ON LEAVE OF Voucher.VoucherSeq IN FRAME F-Main /* Voucher No */
DO:
DO WITH FRAME {&FRAME-NAME}:
DEF VAR curr-vchr AS INT NO-UNDO.

  IF INPUT Voucher.VoucherSeq = Voucher.VoucherSeq THEN RETURN.

  IF AVAILABLE(Voucher) THEN curr-vchr = Voucher.VoucherSeq.
  FIND Voucher WHERE Voucher.VoucherSeq = INPUT Voucher.VoucherSeq NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Voucher) THEN DO:
    MESSAGE "Voucher" INPUT VoucherSeq "not on file!" VIEW-AS ALERT-BOX ERROR TITLE "No Such Voucher".
    FIND Voucher WHERE Voucher.VoucherSeq = curr-vchr NO-LOCK NO-ERROR.
    IF AVAILABLE(Voucher) THEN DISPLAY Voucher.VoucherSeq.
    RETURN NO-APPLY.
  END.

  RUN save-lines.

  IF LAST-EVENT:WIDGET-ENTER = btn_Cancel:HANDLE
     OR LAST-EVENT:WIDGET-ENTER = btn_Add:HANDLE
     OR LAST-EVENT:WIDGET-ENTER = btn_OK:HANDLE THEN
    /* They pressed a button - we'll just run with that one */
    RETURN.

  IF mode = "View" THEN
    APPLY 'ENTRY':U TO btn_Cancel .
  ELSE
    APPLY 'ENTRY':U TO br-vchlne.

  RUN dispatch( 'display-fields':U ).
  IF mode <> "View" THEN RUN dispatch( 'enable-fields':U ).

  RETURN NO-APPLY.

END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-new-line V-table-Win 
PROCEDURE add-new-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF NUM-RESULTS("{&BROWSE-NAME}":U) = ? OR
     NUM-RESULTS("{&BROWSE-NAME}":U) = 0 THEN
  DO:
    RUN create-line.
    RUN open-trn-query.
    RUN check-last.
  END.
  ELSE DO:
    IF {&BROWSE-NAME}:INSERT-ROW("AFTER") THEN .
  END.

  RUN set-trn-defaults.
  trn-modified = Yes.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Voucher"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Voucher"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-line V-table-Win 
PROCEDURE assign-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE VoucherLine THEN RETURN.

  DO TRANSACTION:
    IF NOT NEW VoucherLine THEN FIND CURRENT VoucherLine EXCLUSIVE-LOCK.

    ASSIGN VoucherLine.VoucherSeq = Voucher.VoucherSeq .

    ASSIGN BROWSE {&BROWSE-NAME}
        VoucherLine.EntityType
        VoucherLine.EntityCode
        VoucherLine.AccountCode
        VoucherLine.Amount
        VoucherLine.TaxAmount
        VoucherLine.Description.

    FIND CURRENT VoucherLine NO-LOCK.
  END.

  RUN display-line.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = "Register" THEN DO:
    RUN delete-voucher.
    RUN notify( 'open-query,record-source':U ).
  END.
  ELSE DO:
    RUN check-modified( "CLEAR" ).
    RUN cancel-lines.
  END.

  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-lines V-table-Win 
PROCEDURE cancel-lines :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Voucher THEN RETURN.

  FOR EACH VoucherLine OF Voucher: DELETE VoucherLine. END.
  
  FOR EACH CurrentLine:
    CREATE VoucherLine.
    BUFFER-COPY CurrentLine TO VoucherLine.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-last V-table-Win 
PROCEDURE check-last :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  last-line = CURRENT-RESULT-ROW( "br-vchlne" ) = NUM-RESULTS( "br-vchlne" ) OR
    br-vchlne:NEW-ROW  IN FRAME {&FRAME-NAME} .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-line V-table-Win 
PROCEDURE check-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-entity-type( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( VoucherLine.EntityType:HANDLE IN BROWSE br-vchlne ).

  RUN verify-entity-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( VoucherLine.EntityCode:HANDLE IN BROWSE br-vchlne ).

  RUN verify-account-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( VoucherLine.AccountCode:HANDLE ).

  RUN verify-one-client.
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( VoucherLine.EntityCode:HANDLE IN BROWSE br-vchlne ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-line-total V-table-Win 
PROCEDURE check-line-total :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER VL FOR VoucherLine.

DEF VAR vl-total AS DECIMAL NO-UNDO INITIAL 0.
DEF VAR vl-rowid AS ROWID NO-UNDO.

  IF NOT AVAILABLE(Voucher) THEN RETURN "".

  IF AVAILABLE(VoucherLine) THEN vl-rowid = ROWID(VoucherLine) .

  vl-total = INPUT BROWSE {&BROWSE-NAME} VoucherLine.Amount .
  FOR EACH VL NO-LOCK OF Voucher WHERE ROWID(VL) <> vl-rowid:
    vl-total = vl-total + VL.Amount .
  END.

  IF vl-total = INPUT FRAME {&FRAME-NAME} fil_Total THEN
    RETURN "PASS".
  ELSE
    RETURN "FAIL".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-modifieds V-table-Win 
PROCEDURE clear-modifieds :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:
    Voucher.GoodsValue:MODIFIED = No.
    Voucher.TaxValue:MODIFIED = No.
    fil_Total:MODIFIED = No.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-trn-query V-table-Win 
PROCEDURE close-trn-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY br-vchlne.
  RUN update-trn-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-voucher.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  RUN dispatch( 'update-record':U ).
  RUN check-modified( 'clear':U ).

  RUN notify( 'open-query, record-source':U ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-line V-table-Win 
PROCEDURE create-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR line-seq AS INT NO-UNDO.
DEF BUFFER Other FOR VoucherLine.
  FIND LAST Other OF Voucher NO-LOCK NO-ERROR.
  line-seq = ((IF AVAILABLE(Other) THEN Other.LineSeq ELSE 0) + 1 ) .

  RUN get-default-trn.
  CREATE VoucherLine.
  BUFFER-COPY DefTrans TO VoucherLine ASSIGN VoucherLine.LineSeq = line-seq.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-line V-table-Win 
PROCEDURE delete-line :
/*------------------------------------------------------------------------------
  Purpose:     Delete the transaction line with focus
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR current-row-number AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  IF NUM-RESULTS( "br-vchlne" ) = 0 OR NUM-RESULTS( "br-vchlne" ) = ? THEN RETURN.
  IF br-vchlne:NEW-ROW THEN DO:
    IF br-vchlne:DELETE-CURRENT-ROW() THEN.
  END.
  ELSE DO:
    IF br-vchlne:SELECT-FOCUSED-ROW() THEN DO:
      IF br-vchlne:FETCH-SELECTED-ROW(1) THEN.
      IF AVAILABLE VoucherLine THEN DO:
        GET CURRENT br-vchlne EXCLUSIVE-LOCK.
        DELETE VoucherLine.
        IF br-vchlne:DELETE-CURRENT-ROW() THEN.
      END.
    END.
  END.

  /* once we've deleted the line there may be no lines left! */
  IF NUM-RESULTS( "br-vchlne" ) = ? OR 
     NUM-RESULTS( "br-vchlne" ) = 0 THEN DO:
    RUN close-trn-query.
    RETURN.
  END.   

  IF br-vchlne:FETCH-SELECTED-ROW(1) THEN .
  RUN display-line.
  
  RUN check-last.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-voucher V-table-Win 
PROCEDURE delete-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Voucher EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Voucher THEN
  DO:
    FOR EACH VoucherLine OF Voucher: DELETE VoucherLine. END.
    DELETE Voucher.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-line V-table-Win 
PROCEDURE display-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE VoucherLine THEN RETURN.

  DISPLAY   VoucherLine.EntityType
            VoucherLine.EntityCode
            VoucherLine.AccountCode
            VoucherLine.Description
            VoucherLine.Amount
            VoucherLine.TaxAmount
            WITH BROWSE br-vchlne.
  
  RUN set-trn-color.
  RUN update-trn-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-default-due-date V-table-Win 
PROCEDURE get-default-due-date :
/*------------------------------------------------------------------------------
  Purpose:  Default the due date to the 20th of the month following
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
DEF VAR mm AS INT NO-UNDO.
DEF VAR yy AS INT NO-UNDO.

  mm = MONTH( INPUT Voucher.Date ) + 1.
  yy = YEAR( INPUT Voucher.Date ).
  DO WHILE mm > 12:
    yy = yy + 1.
    mm = mm - 12.
  END.
  Voucher.DateDue:SCREEN-VALUE = STRING( DATE( mm, 20, yy), Voucher.DateDue:FORMAT).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-default-trn V-table-Win 
PROCEDURE get-default-trn :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FIND FIRST DefTrans NO-ERROR.
  IF NOT AVAILABLE(Voucher) THEN RETURN.
  IF NOT AVAILABLE DefTrans THEN CREATE DefTrans.

DEF BUFFER PrevLine FOR VoucherLine.
  FIND LAST PrevLine OF Voucher NO-LOCK NO-ERROR.
  IF AVAILABLE( PrevLine ) THEN DO:
    BUFFER-COPY PrevLine TO DefTrans ASSIGN
                DefTrans.Amount    = 0.00
                DefTrans.TaxAmount = 0.00.
  END.
  ELSE DO WITH FRAME {&FRAME-NAME}:
    ASSIGN  DefTrans.VoucherSeq  = Voucher.VoucherSeq
            DefTrans.Amount      = INPUT fil_Total
            DefTrans.TaxAmount   = INPUT Voucher.TaxValue
    DefTrans.Description = INPUT Voucher.Description + (IF INPUT fil_Period = ? THEN "" ELSE " " + INPUT fil_Period ).

    FIND Creditor WHERE Creditor.CreditorCode = INPUT Voucher.CreditorCode NO-LOCK NO-ERROR.
    IF AVAILABLE(Creditor) THEN ASSIGN
      DefTrans.EntityType  = Creditor.VchrEntityType
      DefTrans.EntityCode  = Creditor.VchrEntityCode
      DefTrans.AccountCode = Creditor.VchrAccount .

    IF DefTrans.EntityType = "" OR DefTrans.EntityType = ? THEN
      DefTrans.EntityType = Voucher.EntityType.
    IF DefTrans.EntityType = "" OR DefTrans.EntityType = ? THEN
      DefTrans.EntityType = "P".

    IF NOT( DefTrans.EntityCode > 0 ) THEN DefTrans.EntityCode = Voucher.EntityCode.
    IF NOT( DefTrans.EntityCode > 0 ) THEN DefTrans.EntityCode = 0.

    IF NOT( DefTrans.AccountCode > 0 ) THEN DefTrans.AccountCode = Voucher.AccountCode.
    IF NOT( DefTrans.AccountCode > 0 ) THEN DefTrans.AccountCode = 0.0 .

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  last-creditor = Voucher.CreditorCode.

DEF BUFFER FirstLine FOR VoucherLine.
  FIND FIRST FirstLine NO-LOCK OF Voucher NO-ERROR.
  IF AVAILABLE(FirstLine) THEN ASSIGN
        Voucher.EntityType  = FirstLine.EntityType
        Voucher.EntityCode  = FirstLine.EntityCode
        Voucher.AccountCode = FirstLine.AccountCode.

  ASSIGN Voucher.TaxValue .
  IF INPUT fil_PeriodTo <> ? AND INPUT fil_PeriodFrom <> ? THEN
    Voucher.InvoicePeriod = STRING( INPUT fil_PeriodFrom, "99/99/9999")
                  + " - " + STRING( INPUT fil_PeriodTo, "99/99/9999").
  ELSE
    Voucher.InvoicePeriod = INPUT fil_Period.

  Voucher.GoodsValue = INPUT fil_Total - Voucher.TaxValue.

  BUFFER-COPY Voucher TO LastVoucher.
  last_Period = INPUT fil_Period.
  last_PeriodFrom = INPUT fil_PeriodFrom.
  last_PeriodTo   = INPUT fil_PeriodTo.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-disable-fields V-table-Win 
PROCEDURE inst-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  {&BROWSE-NAME}:SENSITIVE = Yes.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Voucher) THEN RETURN.
DO WITH FRAME {&FRAME-NAME}:
  IF fil_Period:SENSITIVE THEN DO:
    fil_Period = Voucher.InvoicePeriod .
    calculate-period( fil_Period ).
    DISPLAY fil_Period.
  END.
  IF gst-applies THEN DO:
    fil_Total = Voucher.GoodsValue + Voucher.TaxValue.
    DISPLAY fil_Total Voucher.TaxValue .
  END.
END.
  RUN open-trn-query.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF mode = "Register" THEN DO WITH FRAME {&FRAME-NAME}:
    have-records = Yes.
    ASSIGN last-creditor = INT( find-parent-key( "CreditorCode":U ) ) NO-ERROR.
    Voucher.Description:SENSITIVE = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  RUN dispatch( 'enable-fields':U ).

  IF mode = "View" THEN DO:
    btn_cancel:LABEL = "&Close".
    btn_cancel:TOOLTIP = "Close the window".
  END.
  ELSE IF mode = "Register" THEN DO:
    btn_OK:LABEL = "&Done".
    btn_OK:TOOLTIP = "Finalise the registration of this voucher, print all registered vouchers and close the window.".
  END.
  ELSE IF mode = "Maintain" THEN DO:
    btn_OK:LABEL = "&Apply".
    btn_OK:TOOLTIP = "Apply the changes to this voucher, and go to the next one".
    btn_cancel:LABEL = "&Close".
    btn_cancel:TOOLTIP = "Close the window - your changes will be cancelled".
    DISABLE Voucher.VoucherSeq .
  END.

  IF gst-applies THEN
    VIEW Voucher.TaxValue Voucher.GoodsValue.
  ELSE
    HIDE Voucher.TaxValue Voucher.GoodsValue.

  btn_Add-Trn:HIDDEN IN FRAME {&FRAME-NAME} = Yes.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN save-lines.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-trn-query V-table-Win 
PROCEDURE open-trn-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF AVAILABLE Voucher THEN DO WITH FRAME {&FRAME-NAME}:
    OPEN QUERY {&BROWSE-NAME} FOR EACH VoucherLine OF Voucher NO-LOCK.
    RUN display-line.
    RUN update-trn-fields.
    RUN update-line-editable.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE order-number-changed V-table-Win 
PROCEDURE order-number-changed :
/*------------------------------------------------------------------------------
  Purpose:     Update the coding details for the given project/order number
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  
  DEF VAR project-code LIKE Project.ProjectCode NO-UNDO.
  DEF VAR order-code   LIKE Order.OrderCode NO-UNDO.
  
  project-code = INPUT Voucher.ProjectCode.
  order-code   = INPUT Voucher.OrderCode.  

  IF last-project-code = project-code AND last-order-code = order-code THEN RETURN.
  last-project-code = project-code.
  last-order-code   = order-code.
  
  /* Try and find the order record */
  RUN update-from-order( project-code, order-code ).
  RETURN RETURN-VALUE.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF AVAILABLE(DefTrans) THEN DELETE DefTrans.

  DO TRANSACTION WITH FRAME {&FRAME-NAME}:
    CREATE Voucher.
    ASSIGN  Voucher.VoucherStatus = "U"
            Voucher.CreditorCode  = last-creditor.

DEF BUFFER MyCred FOR Creditor.
    FIND MyCred NO-LOCK WHERE MyCred.CreditorCode = INPUT Voucher.CreditorCode NO-ERROR.
    IF AVAILABLE(MyCred) THEN DO:
      Voucher.EntityType = (IF MyCred.VchrEntityType <> "" THEN MyCred.VchrEntityType ELSE "P"). 
      Voucher.EntityCode = MyCred.VchrEntityCode. 
      Voucher.AccountCode = MyCred.VchrAccountCode. 
      Voucher.PaymentStyle = MyCred.PaymentStyle . 
      fil_Creditor = MyCred.Name .
    END.

    IF NOT Voucher.InvoiceReference:SENSITIVE THEN
      Voucher.InvoiceReference = LastVoucher.InvoiceReference.
    IF NOT Voucher.Date:SENSITIVE THEN
      Voucher.Date = LastVoucher.Date.
    IF fil_Period:SENSITIVE THEN ASSIGN fil_Period = "".
                            ELSE ASSIGN fil_Period = last_Period.
    Voucher.InvoicePeriod = fil_Period.
    IF fil_PeriodFrom:SENSITIVE THEN fil_PeriodFrom = ?.
                                ELSE fil_PeriodFrom = last_PeriodFrom.
    IF fil_PeriodTo:SENSITIVE THEN fil_PeriodTo = ?.
                              ELSE fil_PeriodTo = last_PeriodTo.
    DISPLAY fil_Period fil_PeriodFrom fil_PeriodTo fil_Creditor.

    IF NOT Voucher.DateDue:SENSITIVE THEN
      Voucher.DateDue = LastVoucher.DateDue.

    Voucher.Description = fil_Creditor.
    IF NOT Voucher.Description:SENSITIVE THEN
      Voucher.Description = LastVoucher.Description.

  END.
  RUN dispatch ( 'display-fields':U ) .

  CURRENT-WINDOW = THIS-PROCEDURE:CURRENT-WINDOW.
  CURRENT-WINDOW:TITLE = "Registering voucher " + STRING( Voucher.VoucherSeq, "999999").
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-enable-fields V-table-Win 
PROCEDURE override-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF voucher-orders = "Project" THEN DO:
    Voucher.projectCode:LABEL = "Project".
    HIDE Voucher.OrderCode .
  END.
  ELSE IF voucher-orders = "None" THEN DO:
    HIDE Voucher.ProjectCode Voucher.OrderCode .
  END.

  IF CAN-FIND( FIRST PaymentStyle WHERE PaymentStyle.Payments ) THEN
    VIEW cmb_PaymentStyle.
  ELSE
    HIDE cmb_PaymentStyle.

  IF Office.GST = 0 OR Office.GST = ? THEN
    HIDE Voucher.TaxValue Voucher.GoodsValue.
  ELSE
    VIEW Voucher.TaxValue Voucher.GoodsValue.

  /* Code placed here will execute AFTER standard behavior.    */
  IF Voucher.VoucherStatus <> "U" OR mode = "View" THEN DO:
    HIDE btn_OK btn_Add btn_UnlockFields .
    DISABLE btn_Add btn_OK btn_UnlockFields br-vchlne 
            Voucher.TaxValue fil_Total cmb_PaymentStyle
            fil_Period  fil_PeriodFrom  fil_PeriodTo .
    RUN dispatch ( 'disable-fields':U ).
    ENABLE Voucher.VoucherSeq.
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, "ALL", "HIDDEN = Yes" ).
  END.
  ELSE DO:
    /* standard toolkit method */
    RUN pre-enable-fields IN THIS-PROCEDURE NO-ERROR.

    /* Dispatch standard ADM method.                             */
    RUN adm-enable-fields IN THIS-PROCEDURE NO-ERROR .

    ENABLE UNLESS-HIDDEN btn_OK cmb_PaymentStyle Voucher.TaxValue
                         Voucher.ProjectCode Voucher.OrderCode .
    ENABLE br-vchlne Voucher.Date Voucher.InvoiceReference Voucher.DateDue
           Voucher.Description fil_Total fil_Period  fil_PeriodFrom  fil_PeriodTo 
           Voucher.CreditorCode .

    VIEW btn_OK .
    IF mode = "Register" THEN DO:
      VIEW btn_Add  btn_UnlockFields.
      ENABLE btn_Add  btn_UnlockFields.
      DISABLE Voucher.VoucherSeq .
    END.
    ELSE DO:
      ENABLE Voucher.VoucherSeq.
    END.

    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, "ALL", "SENSITIVE = Yes" ).
  END.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE":U THEN RUN cancel-changes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-lines V-table-Win 
PROCEDURE save-lines :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF mode = "Register" THEN RETURN.
  IF NOT AVAILABLE Voucher THEN RETURN.
  
  FOR EACH CurrentLine: DELETE CurrentLine. END.

  FOR EACH VoucherLine OF Voucher NO-LOCK:
    CREATE CurrentLine.
    BUFFER-COPY VoucherLine TO CurrentLine.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Voucher"}
  {src/adm/template/snd-list.i "VoucherLine"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-creditor-defaults V-table-Win 
PROCEDURE set-creditor-defaults :
/*------------------------------------------------------------------------------
  Purpose:  Set any defaults from the creditor record
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME} TRANSACTION:
  FIND CURRENT Voucher EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE(Voucher) THEN RETURN.
  IF Voucher.CreditorCode = INPUT Voucher.CreditorCode THEN RETURN.

DEF BUFFER MyCred FOR Creditor.
  FIND MyCred NO-LOCK WHERE MyCred.CreditorCode = INPUT Voucher.CreditorCode NO-ERROR.
  IF NOT AVAILABLE(MyCred) THEN RETURN.

  Voucher.EntityType = (IF MyCred.VchrEntityType <> "" THEN MyCred.VchrEntityType ELSE "P"). 
  Voucher.EntityCode = MyCred.VchrEntityCode. 
  Voucher.AccountCode = MyCred.VchrAccountCode. 
  Voucher.PaymentStyle = MyCred.PaymentStyle . 

  FIND CURRENT Voucher NO-LOCK.

  APPLY 'U1':U TO cmb_PaymentStyle .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-trn-color V-table-Win 
PROCEDURE set-trn-color :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR fcolor AS INT INIT ? NO-UNDO.
  DEF VAR bcolor AS INT INIT ? NO-UNDO.
  
  DEF VAR type   AS CHAR NO-UNDO.
  DEF VAR amount AS DEC  NO-UNDO.
  type = IF CAN-QUERY( VoucherLine.EntityType:HANDLE IN BROWSE br-vchlne, "SCREEN-VALUE" ) THEN
    VoucherLine.EntityType:SCREEN-VALUE ELSE VoucherLine.EntityType.
  amount = IF CAN-QUERY( VoucherLine.Amount:HANDLE IN BROWSE br-vchlne, "SCREEN-VALUE" ) THEN
    DEC( VoucherLine.Amount:SCREEN-VALUE ) ELSE VoucherLine.Amount.

  CASE type:
    WHEN "L" THEN ASSIGN fcolor = 1  bcolor = 15.
    WHEN "P" THEN ASSIGN fcolor = 2  bcolor = 15.
    WHEN "T" THEN ASSIGN fcolor = 4  bcolor = 15.
    WHEN "J" THEN ASSIGN fcolor = 6 bcolor = 15.
    WHEN "C" THEN ASSIGN fcolor = 5 bcolor = 15.
  END CASE.

  ASSIGN
    VoucherLine.EntityType:FGCOLOR  = fcolor
    VoucherLine.EntityCode:FGCOLOR  = fcolor
    VoucherLine.AccountCode:FGCOLOR = fcolor
    VoucherLine.Amount:FGCOLOR      = IF amount < 0 THEN 12 ELSE 0
    VoucherLine.TaxAmount:FGCOLOR   = IF amount < 0 THEN 12 ELSE 0
    VoucherLine.Description:FGCOLOR = fcolor.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-trn-defaults V-table-Win 
PROCEDURE set-trn-defaults :
/*------------------------------------------------------------------------------
  Purpose:  set defaults for a new line
------------------------------------------------------------------------------*/
  RUN get-default-trn.
  IF NOT AVAILABLE DefTrans THEN RETURN.  

DO WITH FRAME {&FRAME-NAME}:
  IF NOT {&BROWSE-NAME}:NEW-ROW THEN RETURN.

  ASSIGN
    VoucherLine.EntityType:SCREEN-VALUE IN BROWSE br-vchlne = DefTrans.EntityType
    VoucherLine.EntityCode:SCREEN-VALUE  = STRING( DefTrans.EntityCode, VoucherLine.EntityCode:FORMAT )
    VoucherLine.AccountCode:SCREEN-VALUE = STRING( DefTrans.AccountCode, VoucherLine.AccountCode:FORMAT )
    VoucherLine.Description:SCREEN-VALUE = DefTrans.Description
    VoucherLine.Amount:SCREEN-VALUE      = STRING( get-remaining-gross(), VoucherLine.Amount:FORMAT)
    VoucherLine.TaxAmount:SCREEN-VALUE   = STRING( get-remaining-tax(), VoucherLine.TaxAmount:FORMAT) .

  ASSIGN    VoucherLine.EntityType:MODIFIED IN BROWSE br-vchlne = No 
            VoucherLine.EntityCode:MODIFIED  = No 
            VoucherLine.AccountCode:MODIFIED = No
            VoucherLine.Amount:MODIFIED      = No     
            VoucherLine.Description:MODIFIED = No.

  RUN update-trn-fields.
  RUN update-entity-fields.

END.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-account-name V-table-Win 
PROCEDURE update-account-name :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR type AS CHAR NO-UNDO.
DEF VAR code AS INT  NO-UNDO.
DEF VAR acct AS DEC  NO-UNDO.

  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.
  acct = INPUT BROWSE br-vchlne VoucherLine.AccountCode.

  /* Check to see if the account exists */
  IF type = "J" THEN DO:
    code = INPUT BROWSE br-vchlne VoucherLine.EntityCode.
    FIND Project WHERE Project.ProjectCode = code NO-LOCK NO-ERROR.
    IF AVAILABLE(Project) AND Project.ExpenditureType <> "G" THEN DO:
      FIND ProjectBudget WHERE ProjectBudget.ProjectCode = code
                                AND ProjectBudget.AccountCode = acct NO-LOCK NO-ERROR.
      IF AVAILABLE(ProjectBudget) THEN DO WITH FRAME {&FRAME-NAME}:
        fil_Account:SCREEN-VALUE = ProjectBudget.Description.
        RETURN.
      END.
    END.
  END.

  FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = acct NO-LOCK NO-ERROR.
  ASSIGN fil_Account:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
            IF AVAILABLE ChartOfAccount THEN ChartOfAccount.Name ELSE "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-description V-table-Win 
PROCEDURE update-description :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  VoucherLine.Description:SCREEN-VALUE IN BROWSE br-vchlne = Voucher.Description.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-fields V-table-Win 
PROCEDURE update-entity-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.

  ASSIGN
    VoucherLine.AccountCode:SCREEN-VALUE =
      IF type = 'T' THEN STRING( sundry-debtors )   ELSE
      IF type = 'C' THEN STRING( sundry-creditors ) ELSE
      VoucherLine.AccountCode:SCREEN-VALUE
    VoucherLine.AccountCode:MODIFIED = No
    VoucherLine.AccountCode:READ-ONLY = ((type = "C") OR ((type = "T") AND NOT tenant-accounts))
    VoucherLine.AccountCode:MODIFIED = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-name V-table-Win 
PROCEDURE update-entity-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR type AS CHAR NO-UNDO.
  DEF VAR code AS INT  NO-UNDO.
  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.
  code = INPUT BROWSE br-vchlne VoucherLine.EntityCode.
  
  CASE type:
  
    WHEN 'T' THEN DO:
      FIND FIRST Tenant WHERE Tenant.TenantCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
                IF AVAILABLE Tenant THEN Tenant.Name ELSE "".
    END.

    WHEN 'C' THEN DO:
      FIND FIRST Creditor WHERE Creditor.CreditorCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
                IF AVAILABLE Creditor THEN Creditor.Name ELSE "".
    END.
  
    WHEN 'P' THEN DO:
      FIND FIRST Property WHERE Property.PropertyCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
                IF AVAILABLE Property THEN Property.Name ELSE "".
    END.
  
    WHEN 'L' THEN DO:
      FIND FIRST Company WHERE Company.CompanyCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
                IF AVAILABLE Company THEN Company.LegalName ELSE "".
    END.
  
    WHEN 'J' THEN DO:
      FIND FIRST Project WHERE Project.ProjectCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
                IF AVAILABLE Project THEN Project.Name ELSE "".
    END.

    OTHERWISE fil_Entity:SCREEN-VALUE = "".
    
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-from-order V-table-Win 
PROCEDURE update-from-order :
/*------------------------------------------------------------------------------
  Purpose:     Set the coding details for the current voucher
               from the current order
  Parameters:  <none>
  Notes:       Order and project must be available
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER project-code LIKE Project.ProjectCode NO-UNDO.
  DEF INPUT PARAMETER order-code   LIKE Order.OrderCode NO-UNDO.
  
  DEF VAR et AS CHAR NO-UNDO.
  DEF VAR ec AS INT NO-UNDO.
  DEF VAR ac AS DEC NO-UNDO.
  DEF VAR return-me AS CHAR NO-UNDO INITIAL "NOTHING".
  
  FIND Project WHERE Project.ProjectCode = project-code NO-LOCK NO-ERROR.
  FIND FIRST Order OF Project WHERE Order.OrderCode = order-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE Order THEN RETURN.

  /* Creditor */

  FIND Creditor OF Order NO-LOCK NO-ERROR.
  IF AVAILABLE Creditor THEN DO:
    fil_Creditor:PRIVATE-DATA = STRING( ROWID( Creditor ) ).
    APPLY 'U2':U TO fil_Creditor.
    APPLY 'ENTRY':U TO Voucher.Date IN FRAME {&FRAME-NAME}.
    return-me = "NO-APPLY".     /* tell the LEAVE trigger to RETURN NO-APPLY */
  END.
  ELSE DO:
    fil_Creditor:SCREEN-VALUE = "".
    fil_Creditor:PRIVATE-DATA = "".
    Voucher.CreditorCode:SCREEN-VALUE = STRING( 0 ).
    Voucher.CreditorCode:PRIVATE-DATA = "".
  END.


  /* Entity Type/Code &  Account Code */
  ec = Project.ProjectCode.
  et = (IF Project.ExpenditureType = "G" THEN "P" ELSE "J").
  ac = Order.AccountCode.

  IF et <> "J" THEN
    FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = ac NO-LOCK NO-ERROR.
  IF et = "J" OR NOT(AVAILABLE(ChartOfAccount))THEN DO:
    FIND ProjectBudget OF Project WHERE ProjectBudget.AccountCode = ac NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(ProjectBudget) THEN ac = 0.
  END.

  DEF BUFFER DefaultLine FOR VoucherLine.
  FIND FIRST DefaultLine OF Voucher NO-ERROR.
  IF NOT AVAILABLE DefaultLine THEN DO:
    CREATE  DefaultLine.
    ASSIGN  DefaultLine.VoucherSeq = Voucher.VoucherSeq
            DefaultLine.Description = Voucher.Description
            DefaultLine.Amount = Voucher.GoodsValue.
  END.

  ASSIGN  
    DefaultLine.EntityType = et
    DefaultLine.EntityCode = ec
    DefaultLine.AccountCode = ac.
  RELEASE DefaultLine.

  IF NUM-RESULTS( "br-vchlne" ) = 0 OR NUM-RESULTS( "br-vchlne" ) = ? THEN
    RUN open-trn-query.
  ELSE IF BROWSE {&BROWSE-NAME}:REFRESH() THEN.
  
END.

  RETURN return-me.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-line V-table-Win 
PROCEDURE update-line :
/*------------------------------------------------------------------------------
  Purpose:     update the current invoice line.
------------------------------------------------------------------------------*/

  /* Create a new line if needed and reset the details */
  IF {&BROWSE-NAME}:NEW-ROW IN FRAME {&FRAME-NAME} THEN DO:
    RUN create-line.
    {&BROWSE-NAME}:CREATE-RESULT-LIST-ENTRY().
    RUN assign-line.
  END.
  ELSE
    RUN assign-line.

  trn-modified = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-line-editable V-table-Win 
PROCEDURE update-line-editable :
/*------------------------------------------------------------------------------
  Purpose:  Decide whether the user can edit the current voucherline.
------------------------------------------------------------------------------*/
DEF VAR ineditable AS LOGI NO-UNDO INITIAL Yes.
  ineditable = (Voucher.VoucherStatus <> "U" OR mode = "View").

  VoucherLine.EntityType:READ-ONLY IN BROWSE {&BROWSE-NAME}  = ineditable.
  VoucherLine.EntityCode:READ-ONLY IN BROWSE {&BROWSE-NAME}  = ineditable.
  VoucherLine.AccountCode:READ-ONLY IN BROWSE {&BROWSE-NAME} = ineditable.
  VoucherLine.Description:READ-ONLY IN BROWSE {&BROWSE-NAME} = ineditable.
  VoucherLine.Amount:READ-ONLY IN BROWSE {&BROWSE-NAME}      = ineditable.
  VoucherLine.TaxAmount:READ-ONLY IN BROWSE {&BROWSE-NAME}   = ineditable.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-trn-fields V-table-Win 
PROCEDURE update-trn-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF NUM-RESULTS( "br-vchlne" ) = 0 OR NUM-RESULTS( "br-vchlne" ) = ? THEN DO:
    fil_Account:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
    fil_entity:SCREEN-VALUE = "".
    RETURN.
  END.
  
DEF VAR type AS CHAR NO-UNDO.
  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.
  VoucherLine.AccountCode:READ-ONLY IN BROWSE br-vchlne = 
                (type = "C" OR (type = "T" AND NOT tenant-accounts)).

  RUN update-entity-name.
  RUN update-account-name.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-account-code V-table-Win 
PROCEDURE verify-account-code :
/*------------------------------------------------------------------------------
  Purpose:     Verify that the transaction's account is a valid
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER mode AS CHAR NO-UNDO.
  
DEF VAR type AS CHAR NO-UNDO.
DEF VAR code AS INT  NO-UNDO.
DEF VAR acct AS DEC  NO-UNDO.
DEF VAR success AS LOGI INITIAL Yes NO-UNDO.

  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.
  code = INPUT BROWSE br-vchlne VoucherLine.EntityCode.
  acct = INPUT BROWSE br-vchlne VoucherLine.AccountCode.

  /* Check to see if the account exists */
  IF type = "J" THEN
    success = CAN-FIND( ProjectBudget WHERE ProjectBudget.ProjectCode = code
                                AND ProjectBudget.AccountCode = acct ).
  ELSE
    success = CAN-FIND( ChartOfAccount WHERE ChartOfAccount.AccountCode = acct ).

  IF NOT success THEN DO:
    IF mode = "Verify" THEN
      MESSAGE "The account code " + STRING( acct ) + " is invalid !"
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
    RETURN "FAIL".
  END.

  IF type = "J" AND restrict-project-posting BEGINS "Y" THEN DO:  
    FIND ProjectBudget WHERE ProjectBudget.ProjectCode = code
                         AND ProjectBudget.AccountCode = acct NO-LOCK.
    IF NOT ProjectBudget.AllowPosting THEN DO:
      IF mode = "Verify" THEN DO:
        FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
        MESSAGE "Project account " + STRING( acct ) + " cannot be updated to."
                 VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
      END.
      RETURN "FAIL".
    END.
  END.
  ELSE IF type <> "J" THEN DO:  
    FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = acct NO-LOCK.

    /* Check to see if we can update to this account for the given entity type */

    IF INDEX( ChartOfAccount.UpdateTo, type ) = 0 THEN DO:
      IF mode = "Verify" THEN DO:
        FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
        MESSAGE "Account " + STRING( acct ) + " cannot be updated to a " + 
                 EntityType.Description
                 VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
      END.
      RETURN "FAIL".
    END.
  END.

  IF NOT verifying-line THEN RUN update-trn-fields.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-entity-code V-table-Win 
PROCEDURE verify-entity-code :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER mode AS CHAR NO-UNDO.

  DEF VAR entity-ok AS LOGI INIT No NO-UNDO.
  DEF VAR entity-warn AS LOGI INIT No NO-UNDO.
  DEF VAR type AS CHAR NO-UNDO.
  DEF VAR code AS INT  NO-UNDO.
  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.
  code = INPUT BROWSE br-vchlne VoucherLine.EntityCode.
  
  entity-ok = 
    IF type = 'T' THEN CAN-FIND( FIRST Tenant   WHERE Tenant.TenantCode = code )     ELSE
    IF type = 'C' THEN CAN-FIND( FIRST Creditor WHERE Creditor.CreditorCode = code ) ELSE
    IF type = 'P' THEN CAN-FIND( FIRST Property WHERE Property.PropertyCode = code ) ELSE
    IF type = 'L' THEN CAN-FIND( FIRST Company  WHERE Company.CompanyCode = code )   ELSE
    IF type = 'J' THEN CAN-FIND( FIRST Project  WHERE Project.ProjectCode = code )   ELSE
      No.
      
  entity-warn = 
    IF type = 'T' THEN CAN-FIND( FIRST Tenant   WHERE Tenant.TenantCode = code AND Active )     ELSE
    IF type = 'C' THEN CAN-FIND( FIRST Creditor WHERE Creditor.CreditorCode = code AND Active ) ELSE
    IF type = 'P' THEN CAN-FIND( FIRST Property WHERE Property.PropertyCode = code AND Active ) ELSE
    IF type = 'L' THEN CAN-FIND( FIRST Company  WHERE Company.CompanyCode = code AND Active )   ELSE
    IF type = 'J' THEN CAN-FIND( FIRST Project  WHERE Project.ProjectCode = code AND Active )   ELSE
      No.

  IF NOT entity-ok THEN DO:
    IF mode = "Verify" THEN DO:
      FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
      MESSAGE "There is no " + EntityType.Description + " with code " + STRING( code )
        VIEW-AS ALERT-BOX ERROR TITLE "Entity Code Error - " + EntityType.Description.
    END.
    RETURN "FAIL".
  END.
  ELSE IF NOT entity-warn AND mode = "Verify" THEN DO:
    FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
    MESSAGE EntityType.Description + " " + STRING( code ) + " is not active"
      VIEW-AS ALERT-BOX WARNING TITLE "Inactive " + EntityType.Description + " Warning".
  END.

  RUN update-entity-name.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-entity-type V-table-Win 
PROCEDURE verify-entity-type :
/*------------------------------------------------------------------------------
  Purpose:     Verify the entity type
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER mode AS CHAR NO-UNDO.

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.

  IF NOT CAN-FIND( FIRST Entitytype WHERE EntityType.EntityType = type ) THEN DO:
    IF mode = "Verify" THEN
      MESSAGE "The entity type " + type + " is invalid !"
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid Entity Type".
    RETURN "FAIL".
  END.

  IF verifying-line THEN RETURN.

  VoucherLine.EntityType:SCREEN-VALUE IN BROWSE br-vchlne = CAPS( type ).  
  RUN update-entity-fields.
  RUN update-account-name.
  RUN update-entity-name.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-line V-table-Win 
PROCEDURE verify-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR err-field AS HANDLE NO-UNDO.

  verifying-line = Yes.
  RUN check-line.
  verifying-line = No.
  
  err-field = WIDGET-HANDLE( RETURN-VALUE ).

  IF VALID-HANDLE( err-field ) THEN DO:
    MESSAGE "The current transaction line is in error." SKIP
      "Do you want to abort the changes ? "
      VIEW-AS ALERT-BOX ERROR BUTTONS YES-NO UPDATE abort-it AS LOGI.
    IF abort-it THEN DO:
      trn-modified = No.
      IF AVAILABLE VoucherLine AND VoucherLine.AccountCode <> 0000.00 THEN
        RUN display-line.
      ELSE
        RUN delete-line.
    END.
    ELSE DO:
      IF AVAILABLE VoucherLine THEN GET CURRENT br-vchlne.
      APPLY 'ENTRY':U TO err-field.
      RETURN "FAIL".
    END.
  END.
  ELSE
    RUN update-line.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-one-client V-table-Win 
PROCEDURE verify-one-client :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR test-client AS CHAR NO-UNDO.

DEF BUFFER v-line FOR VoucherLine.

  IF NOT AVAILABLE(Voucher) THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:
  test-client = get-client( INPUT BROWSE {&BROWSE-NAME} VoucherLine.EntityType,
                            INPUT BROWSE {&BROWSE-NAME} VoucherLine.EntityCode ).

  FOR EACH v-line OF Voucher NO-LOCK:
    IF NOT( {&BROWSE-NAME}:NEW-ROW ) AND AVAILABLE(VoucherLine) THEN DO:
      IF RECID(v-line) = RECID(VoucherLine) THEN NEXT.
    END.
    IF test-client <> get-client( v-line.EntityType, v-line.EntityCode ) THEN DO:
      MESSAGE "The allocations of a voucher may not be split" SKIP
              "across the properties or companies of more than" SKIP
              "one client/owner."
              VIEW-AS ALERT-BOX ERROR
              TITLE "Voucher Allocated across multiple clients/owners".
      RETURN "FAIL".
    END.
  END.
END.

  RETURN "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-voucher V-table-Win 
PROCEDURE verify-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR thats-fine AS LOGICAL NO-UNDO INITIAL No.

DO WITH FRAME {&FRAME-NAME}:

  IF INPUT Voucher.CreditorCode = 0
        OR NOT CAN-FIND( Creditor WHERE Creditor.CreditorCode = INPUT Voucher.CreditorCode) THEN
  DO:
    MESSAGE "You must select a creditor!" VIEW-AS ALERT-BOX ERROR
      TITLE "No creditor selected".
    APPLY 'ENTRY':U TO Voucher.CreditorCode.
    RETURN "FAIL".
  END.

  IF INPUT Voucher.Date = ? OR INPUT Voucher.Date < ( TODAY - 1100 ) THEN DO:
    MESSAGE "The date of this voucher is too old" SKIP(1)
            VIEW-AS ALERT-BOX ERROR
            TITLE "Voucher more than 3 years old".
    APPLY "ENTRY":U TO Voucher.Date.
    RETURN "FAIL".
  END.
  ELSE IF INPUT Voucher.Date < ( TODAY - 200 ) THEN DO:
    MESSAGE "The date of this voucher is some time past" SKIP(1)
            "Is that OK?"
            VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL
            TITLE "Voucher more than 7 months old" UPDATE thats-fine .
    IF NOT thats-fine THEN DO:
      APPLY "ENTRY":U TO Voucher.Date.
      RETURN "FAIL".
    END.
  END.
  ELSE IF INPUT Voucher.Date > (TODAY + 60) THEN DO:
    MESSAGE "The date of this voucher is in the future" SKIP(1)
            "Is that OK?"
            VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL
            TITLE "Voucher more than 2 months in the future" UPDATE thats-fine .
    IF NOT thats-fine THEN DO:
      APPLY "ENTRY":U TO Voucher.Date.
      RETURN "FAIL".
    END.
  END.

&SCOP t_format VoucherLine.Amount:FORMAT IN BROWSE br-vchlne
DEF VAR total AS DEC NO-UNDO.
  FOR EACH VoucherLine OF Voucher NO-LOCK: total = total + VoucherLine.Amount. END.
  IF total <> INPUT fil_Total AND total <> 0 THEN DO:
    MESSAGE "The sum of all the transactions must" SKIP 
            "equal the gross value of the voucher" SKIP(2)
            "You can press F3 on any transaction" SKIP
            "amount to make it balance"
      VIEW-AS ALERT-BOX ERROR TITLE "Incorrect Total, " +
        TRIM( STRING( total, {&t_format} ) ) + " vs " + 
        TRIM( STRING( INPUT fil_Total, {&t_format} ) ).
    APPLY 'ENTRY':U TO VoucherLine.Amount IN BROWSE br-vchlne.
    RETURN "FAIL".
  END.

DEF VAR tax-total AS DEC NO-UNDO.
  FOR EACH VoucherLine OF Voucher NO-LOCK: tax-total = tax-total + VoucherLine.TaxAmount. END.
  IF tax-total <> INPUT Voucher.TaxValue AND (tax-total <> 0 OR total <> 0) THEN DO:
    MESSAGE "The sum of the transactions tax must" SKIP 
            "equal the tax value of the voucher" SKIP(2)
            "You can press F3 on any transaction" SKIP
            "tax amount to make it balance"
      VIEW-AS ALERT-BOX ERROR TITLE "Incorrect Total, " +
        TRIM( STRING( tax-total, {&t_format} ) ) + " vs " + 
        TRIM( STRING( INPUT Voucher.TaxValue, {&t_format} ) ).
    APPLY 'ENTRY':U TO VoucherLine.TaxAmount IN BROWSE br-vchlne.
    RETURN "FAIL".
  END.

  /* We don't check duplicate-ness for some creditors */
  IF CAN-DO( voucher-nodupcheck, STRING( INPUT Voucher.CreditorCode ) ) THEN RETURN.

DEF BUFFER Other FOR Voucher.
  FIND FIRST Other WHERE Other.CreditorCode = INPUT Voucher.CreditorCode
               AND Other.InvoiceReference = INPUT Voucher.InvoiceReference
/*               AND Other.Date = INPUT Voucher.Date  */
               AND Other.VoucherSeq <> INPUT Voucher.VoucherSeq
               NO-LOCK NO-ERROR.
  IF AVAILABLE(Other) THEN DO:
    thats-fine = No.
    MESSAGE "IS THIS A DUPLICATE INVOICE?" SKIP(1)
            "An earlier voucher," Other.VoucherSeq "for" fil_Creditor:SCREEN-VALUE + "," SKIP
            "has the same Invoice No of '" + Other.InvoiceReference + "'" SKIP(1)
            "Is that OK?"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK-CANCEL
            TITLE "Duplicate Invoice?" UPDATE thats-fine .
    IF NOT thats-fine THEN DO:
      APPLY "ENTRY":U TO Voucher.InvoiceReference.
      RETURN "FAIL".
    END.
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION calculate-period V-table-Win 
FUNCTION calculate-period RETURNS LOGICAL
  ( INPUT period-char AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert the character value of the period into a start and end date
            combination.
    Notes:  
------------------------------------------------------------------------------*/

DEF VAR in-period-char AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  in-period-char = period-char.
  IF period-char = "1" THEN DO:
    /* a single payment */
    fil_PeriodFrom = INPUT Voucher.Date .
    fil_PeriodTo = INPUT Voucher.Date .
  END.
  ELSE IF NUM-ENTRIES( period-char, "-" ) = 2 THEN DO:
    fil_PeriodFrom = string-to-date( ENTRY(1,period-char,"-") ).
    fil_PeriodTo   = string-to-date( ENTRY(2,period-char,"-") ).
    IF fil_PeriodFrom = first-of-month(fil_PeriodFrom)
                AND fil_PeriodTo = first-of-month(fil_PeriodTo) THEN
      fil_PeriodTo = last-of-month(fil_PeriodTo).

    IF fil_PeriodFrom = ? OR fil_PeriodTo = ? THEN RETURN No.
  END.
  ELSE IF NUM-ENTRIES( period-char, "/") = 2 THEN DO:
    fil_PeriodFrom = first-of-month( string-to-date( ENTRY(1,period-char,"-") ) ).
    fil_PeriodTo   = last-of-month( string-to-date( ENTRY(1,period-char,"-") ) ).
    IF fil_PeriodFrom = ? OR fil_PeriodTo = ? THEN RETURN No.
  END.
  ELSE IF NUM-ENTRIES( period-char, "/") = 3 THEN DO:
    fil_PeriodFrom = string-to-date( ENTRY(1,period-char,"-") ).
    fil_PeriodTo   = string-to-date( ENTRY(1,period-char,"-") ).
    IF fil_PeriodFrom = ? OR fil_PeriodTo = ? THEN RETURN No.
  END.

  DISPLAY fil_PeriodFrom fil_PeriodTo.
END.

  RETURN Yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-client V-table-Win 
FUNCTION get-client RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Find the client for this entity type / entity code.
------------------------------------------------------------------------------*/
DEF VAR entity-code AS CHAR NO-UNDO.

  DO WHILE et <> "L":
    entity-code = get-parent-entity( et, ec ).
    IF entity-code = ? THEN RETURN ?.
    et = SUBSTRING( entity-code, 1, 1).
    ec = INT( SUBSTRING( entity-code, 2) ).
  END.

  FIND Company WHERE Company.CompanyCode = ec NO-LOCK NO-ERROR.
  RETURN Company.ClientCode.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-parent-entity V-table-Win 
FUNCTION get-parent-entity RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR parent-entity AS CHAR NO-UNDO.

  CASE et:
    WHEN "J" THEN DO:
      FIND Project WHERE Project.ProjectCode = ec NO-LOCK NO-ERROR.
      parent-entity = Project.EntityType + STRING( Project.EntityCode, "99999").
      IF NOT AVAILABLE(Project) THEN RETURN ?.
    END.
    WHEN "P" THEN DO:
      FIND Property WHERE Property.PropertyCode = ec NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(Property) THEN RETURN ?.
      parent-entity = "L" + STRING( Property.CompanyCode, "99999").
    END.
    WHEN "T" THEN DO:
      FIND Tenant WHERE Tenant.TenantCode = ec NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(Tenant) THEN RETURN ?.
      parent-entity = Tenant.EntityType + STRING( Tenant.EntityCode, "99999").
    END.
    WHEN "C" THEN DO:
      FIND Creditor WHERE Creditor.CreditorCode = ec NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(Creditor) THEN RETURN ?.
      parent-entity = "L" + STRING( Creditor.CompanyCode, "99999").
    END.
    WHEN "L" THEN
      parent-entity = "L" + STRING( ec, "99999").
  END CASE.

  RETURN parent-entity.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-remaining-gross V-table-Win 
FUNCTION get-remaining-gross RETURNS DECIMAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose: Returns the Gross remaining to make the current transaction
           balance the voucher.
------------------------------------------------------------------------------*/
  DEF BUFFER OtherTrans FOR VoucherLine.
  
  DEF VAR sub-total  AS DEC   NO-UNDO.
  DEF VAR this-rowid AS ROWID NO-UNDO.
  
  IF br-vchlne:NEW-ROW IN FRAME {&FRAME-NAME} THEN DO:
    FOR EACH OtherTrans OF Voucher:
      sub-total = sub-total + OtherTrans.Amount.
    END.
  END.
  ELSE DO:
    IF br-vchlne:SELECT-FOCUSED-ROW() THEN.
    this-rowid = ROWID( VoucherLine ).
    FOR EACH OtherTrans OF Voucher WHERE ROWID( OtherTrans ) <> this-rowid:
      sub-total = sub-total + OtherTrans.Amount.
    END.
  END.
  
  RETURN ( INPUT FRAME {&FRAME-NAME} fil_Total - sub-total ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-remaining-tax V-table-Win 
FUNCTION get-remaining-tax RETURNS DECIMAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose: Returns the Tax remaining to make the current transaction
           balance the voucher.
------------------------------------------------------------------------------*/
  DEF BUFFER OtherTrans FOR VoucherLine.
  
  DEF VAR sub-total  AS DEC   NO-UNDO.
  DEF VAR this-rowid AS ROWID NO-UNDO.
  
  IF br-vchlne:NEW-ROW IN FRAME {&FRAME-NAME} THEN DO:
    FOR EACH OtherTrans OF Voucher:
      sub-total = sub-total + OtherTrans.TaxAmount.
    END.
  END.
  ELSE DO:
    IF br-vchlne:SELECT-FOCUSED-ROW() THEN.
    this-rowid = ROWID( VoucherLine ).
    FOR EACH OtherTrans OF Voucher WHERE ROWID( OtherTrans ) <> this-rowid:
      sub-total = sub-total + OtherTrans.TaxAmount.
    END.
  END.
  
  RETURN ( INPUT FRAME {&FRAME-NAME} Voucher.TaxValue - sub-total ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION string-to-date V-table-Win 
FUNCTION string-to-date RETURNS DATE
  ( INPUT date-string AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR n1 AS INT NO-UNDO.
DEF VAR n2 AS INT NO-UNDO.

DEF VAR result AS DATE NO-UNDO.
DEF VAR euro-date AS LOGI NO-UNDO INITIAL No.
  IF SESSION:DATE-FORMAT = "dmy" THEN euro-date = Yes.

  IF NUM-ENTRIES( date-string, "/" ) = 3 THEN
    result = DATE( date-string ).
  ELSE IF NUM-ENTRIES( date-string, "/") = 2 THEN DO:
    n1 = INT( TRIM(ENTRY( 1, date-string, "/"))).
    n2 = INT( TRIM(ENTRY( 2, date-string, "/"))).
    IF n2 > 31 OR (euro-date AND n2 > 12) THEN                  /* mm/yy */
      result = DATE( n1, 1, n2 + 1900 ).
    ELSE IF (euro-date AND n1 > 12) OR (NOT euro-date AND n2 > 12) THEN
      result = (IF euro-date THEN DATE( n2, n1, YEAR(TODAY) ) ELSE DATE( n1, n2, YEAR(TODAY) )).
    ELSE IF TRIM(ENTRY( 2, date-string, "/")) BEGINS "0" THEN   /* mm/0y */
      result = DATE( n1, 1, n2 + 2000 ).
    ELSE IF n1 > 31 OR (NOT euro-date AND n1 > 12) THEN         /* yy/mm */
      result = DATE( n2, 1, n1 + 1900 ).
    ELSE IF TRIM(ENTRY( 1, date-string, "/")) BEGINS "0" THEN   /* 0y/mm */
      result = DATE( n2, 1, n1 + 2000 ).
  END.

  IF result = ? THEN DO:
    MESSAGE "Can't understand '" + date-string + "' as a date or period." SKIP(1)
            euro-date n1 n2
            VIEW-AS ALERT-BOX ERROR
            TITLE "Invalid Period".
  END.
  RETURN result.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

