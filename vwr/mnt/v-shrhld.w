&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r11 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR mode AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES ShareHolder Company
&Scoped-define FIRST-EXTERNAL-TABLE ShareHolder


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR ShareHolder, Company.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-8 edt_Notes 
&Scoped-Define DISPLAYED-OBJECTS fil_Company fil_ShareHolder edt_Notes 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE edt_Notes AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 49 BY 9
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Company AS CHARACTER FORMAT "X(256)":U 
     LABEL "Company" 
     VIEW-AS FILL-IN 
     SIZE 39 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_ShareHolder AS CHARACTER FORMAT "X(256)":U 
     LABEL "Holder" 
     VIEW-AS FILL-IN 
     SIZE 39 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 51 BY 13.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_Company AT ROW 1.5 COL 10 COLON-ALIGNED
     fil_ShareHolder AT ROW 2.5 COL 4
     edt_Notes AT ROW 5 COL 2 NO-LABEL
     RECT-8 AT ROW 1 COL 1
     "Company:" VIEW-AS TEXT
          SIZE 10 BY 1 AT ROW 1.5 COL 2
          FONT 10
     "ShareHolder:" VIEW-AS TEXT
          SIZE 10 BY 1 AT ROW 2.5 COL 2
          FONT 10
     "Notes" VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 4 COL 2
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .

 

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.ShareHolder,TTPL.Company
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.6
         WIDTH              = 52.14.
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Default                                      */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fil_Company IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_ShareHolder IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fil_Company
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U1 OF fil_Company IN FRAME F-Main /* Company */
DO:
  {inc/selfil/sfcmp1.i "ShareHolder" "CompanyCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U2 OF fil_Company IN FRAME F-Main /* Company */
DO:
  {inc/selfil/sfcmp2.i "ShareHolder" "CompanyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U3 OF fil_Company IN FRAME F-Main /* Company */
DO:
  {inc/selfil/sfcmp3.i "ShareHolder" "CompanyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_ShareHolder
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_ShareHolder V-table-Win
ON U1 OF fil_ShareHolder IN FRAME F-Main /* Holder */
DO:
  {inc/selfil/sfpsn1.i "ShareHolder" "PersonCode"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_ShareHolder V-table-Win
ON U2 OF fil_ShareHolder IN FRAME F-Main /* Holder */
DO:
  {inc/selfil/sfpsn2.i "ShareHolder" "PersonCode"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_ShareHolder V-table-Win
ON U3 OF fil_ShareHolder IN FRAME F-Main /* Holder */
DO:
  {inc/selfil/sfpsn3.i "ShareHolder" "PersonCode"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "ShareHolder"}
  {src/adm/template/row-list.i "Company"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "ShareHolder"}
  {src/adm/template/row-find.i "Company"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-notes V-table-Win 
PROCEDURE assign-notes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  ASSIGN FRAME {&FRAME-NAME} edt_Notes.
  
  IF edt_Notes = "" THEN
  DO:
    IF AVAILABLE Note THEN DELETE Note.
    ShareHolder.NoteCode = ?.
  END.
  ELSE
  DO:
    IF NOT AVAILABLE Note THEN
    DO:
      DEF BUFFER LastNote FOR Note.
      FIND LAST LastNote NO-LOCK NO-ERROR.
      CREATE Note.
      ASSIGN
        Note.NoteCode = IF AVAILABLE LastNote THEN LastNote.NoteCode + 1 ELSE 1
        ShareHolder.NoteCode  = Note.NoteCode.
    END.
    
    ASSIGN Note.Details = edt_notes.
  END.
      
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONATINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-shareholder. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-shareholder.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  RUN dispatch( 'update-record':U ).
  RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit' ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-shareholder V-table-Win 
PROCEDURE delete-shareholder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT ShareHolder EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE ShareHolder THEN DELETE ShareHolder.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-notes V-table-Win 
PROCEDURE display-notes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NOT AVAILABLE ShareHolder THEN RETURN.
  
  FIND Note OF ShareHolder NO-ERROR.
  edt_Notes = IF AVAILABLE Note THEN Note.Details ELSE "".
  DISPLAY edt_Notes WITH FRAME {&FRAME-NAME}.
    
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN assign-notes.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN display-notes.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  edt_Notes:READ-ONLY = No.
  edt_Notes:SENSITIVE = Yes.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN
  DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE RUN dispatch( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-update-record V-table-Win 
PROCEDURE local-update-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/


/*  RUN dispatch IN THIS-PROCEDURE ( INPUT 'update-record':U ) . */
/* The default will not run because there are no enabled tables */

DO TRANSACTION:
  FIND CURRENT ShareHolder EXCLUSIVE-LOCK.
  RUN dispatch( 'assign-statement':U ).
  FIND CURRENT ShareHolder NO-LOCK.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CREATE ShareHolder.
  
  IF AVAILABLE Company THEN
    ASSIGN ShareHolder.CompanyCode = Company.CompanyCode.
    
  CURRENT-WINDOW:TITLE = "Adding a new Share Holder".
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'display-fields':U ).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ShareHolder"}
  {src/adm/template/snd-list.i "Company"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-shareholder V-table-Win 
PROCEDURE verify-shareholder :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR company-selected      AS LOGI NO-UNDO.
  DEF VAR person-selected       AS LOGI NO-UNDO.
  DEF VAR duplicate-shareholder AS LOGI NO-UNDO.
  
  company-selected =
    CAN-FIND( Company WHERE ROWID( Company ) = TO-ROWID( fil_Company:PRIVATE-DATA ) ).
  person-selected =
    CAN-FIND( Contact WHERE ROWID( Contact ) = TO-ROWID( fil_shareholder:PRIVATE-DATA ) ).
    
  IF NOT ( company-selected AND person-selected ) THEN
  DO:
    MESSAGE
      "You must select both a company and a" SKIP
      "person for a Share Holder."
      VIEW-AS ALERT-BOX ERROR TITLE "Incomplete Details".
    RETURN "FAIL".
  END.
  
  DEF BUFFER OtherShareHolder FOR ShareHolder.
  DEF VAR person-code  LIKE Person.PersonCode NO-UNDO.
  DEF VAR company-code LIKE Company.CompanyCode NO-UNDO.
  
  FIND Contact WHERE ROWID( Contact ) = TO-ROWID( fil_shareholder:PRIVATE-DATA )
    NO-LOCK NO-ERROR.
  person-code = IF AVAILABLE Contact THEN Contact.PersonCode ELSE 0.
  
  FIND Company WHERE ROWID( Company ) = TO-ROWID( fil_Company:PRIVATE-DATA )
    NO-LOCK NO-ERROR.
  company-code = IF AVAILABLE Company THEN Company.CompanyCode ELSE 0.

  IF person-code  <> ShareHolder.PersonCode OR
     company-code <> ShareHolder.CompanyCode THEN
  DO:
    FIND OtherShareHolder WHERE
      OtherShareHolder.CompanyCode = company-code AND
      OtherShareHolder.PersonCode  = person-code  AND
      ROWID( OtherShareHolder ) <> ROWID( ShareHolder )
      NO-LOCK NO-ERROR.
    duplicate-shareholder = AVAILABLE OtherShareHolder.
  END.

  IF duplicate-shareholder THEN
  DO:
    MESSAGE
      "The Share Holder" INPUT fil_shareholder " already exists for" SKIP
      "company" INPUT fil_Company "!"
      VIEW-AS ALERT-BOX ERROR TITLE "Duplicate Share Holder".
    RETURN "FAIL".
  END.
    

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


