&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.

{inc/topic/tpperson.i}

/* parts of the person's name */
DEF VAR p-title AS CHAR NO-UNDO.
DEF VAR p-first AS CHAR NO-UNDO.
DEF VAR p-middle AS CHAR NO-UNDO.
DEF VAR p-last AS CHAR NO-UNDO.
DEF VAR p-suffix AS CHAR NO-UNDO.

DEF VAR p-full AS CHAR NO-UNDO.

{inc/persndtl.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Person
&Scoped-define FIRST-EXTERNAL-TABLE Person


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Person.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Person.PersonTitle Person.Preferred ~
Person.JobTitle Person.Department Person.Company Person.Spouse ~
Person.DateOfBirth Person.GolfHandicap Person.MailOut 
&Scoped-define ENABLED-TABLES Person
&Scoped-define FIRST-ENABLED-TABLE Person
&Scoped-define DISPLAYED-TABLES Person
&Scoped-define FIRST-DISPLAYED-TABLE Person
&Scoped-Define ENABLED-OBJECTS fill_FullName cmb_Address edt_Address ~
fil_City fil_State fil_Country fil_Zip btn_Maintain btn_Clear cmb_Phone1 ~
fil_Phone1 cmb_Phone2 fil_Phone2 cmb_Phone3 fil_Phone3 cmb_Phone4 ~
fil_Phone4 edt_notes btn_alt-job btn_alt-company RECT-1 RECT-2 RECT-23 
&Scoped-Define DISPLAYED-FIELDS Person.PersonTitle Person.Preferred ~
Person.JobTitle Person.Department Person.Company Person.Spouse ~
Person.DateOfBirth Person.GolfHandicap Person.MailOut 
&Scoped-Define DISPLAYED-OBJECTS fill_FullName cmb_Address edt_Address ~
fil_City fil_State fil_Country fil_Zip cmb_Phone1 fil_Phone1 cmb_Phone2 ~
fil_Phone2 cmb_Phone3 fil_Phone3 cmb_Phone4 fil_Phone4 edt_notes ~
fil_ContactTypes 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
PersonCode|y|y|TTPL.Person.PersonCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "PersonCode",
     Keys-Supplied = "PersonCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD copy-person V-table-Win 
FUNCTION copy-person RETURNS LOGICAL
  (  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_alt-company 
     LABEL "Alternate" 
     SIZE 6.86 BY 1.05
     BGCOLOR 8 .

DEFINE BUTTON btn_alt-job 
     LABEL "Alternate" 
     SIZE 6.86 BY 1.05
     BGCOLOR 8 .

DEFINE BUTTON btn_Clear 
     LABEL "&Clear" 
     SIZE 9.72 BY 1.05
     FONT 10.

DEFINE BUTTON btn_Maintain 
     LABEL "Maintain" 
     SIZE 9.72 BY 1.

DEFINE VARIABLE cmb_Address AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_Phone1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_Phone2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_Phone3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_Phone4 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE edt_Address AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 49.57 BY 5
     FONT 10 NO-UNDO.

DEFINE VARIABLE edt_notes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 40.57 BY 5.5
     FONT 10 NO-UNDO.

DEFINE VARIABLE fill_FullName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 31.72 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_City AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_ContactTypes AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 81.14 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Country AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Phone1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 34 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Phone2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 34 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Phone3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 34 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Phone4 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 34 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_State AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Zip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 89.43 BY .05.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 90 BY .05.

DEFINE RECTANGLE RECT-23
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 93 BY 17.3.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Person.PersonTitle AT ROW 1.2 COL 7.29 COLON-ALIGNED NO-LABEL
          VIEW-AS COMBO-BOX INNER-LINES 10
          LIST-ITEMS "Mr","Mrs","Ms","Miss","Dr","Sir","Dame","Lord","Hon","(None)" 
          DROP-DOWN-LIST
          SIZE 8.57 BY 1
          FONT 10
     fill_FullName AT ROW 1.2 COL 15.86 COLON-ALIGNED NO-LABEL
     Person.Preferred AT ROW 1.2 COL 56.43 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 34.86 BY 1
          FONT 10
     Person.JobTitle AT ROW 2.6 COL 7.29 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 32 BY 1
          FONT 10
     Person.Department AT ROW 2.6 COL 56.43 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 34.86 BY 1
          FONT 10
     Person.Company AT ROW 3.8 COL 7.29 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 32 BY 1
          FONT 10
     Person.SortOn AT ROW 3.8 COL 56.43 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 34.86 BY 1
     Person.Spouse AT ROW 5 COL 7.29 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 40.29 BY 1
          FONT 10
     Person.DateOfBirth AT ROW 5 COL 56.43 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 12 BY 1
          FONT 10
     Person.GolfHandicap AT ROW 5 COL 86.14 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 5.14 BY 1
          FONT 10
     cmb_Address AT ROW 7.3 COL 1.57 NO-LABEL
     edt_Address AT ROW 6.4 COL 17.86 NO-LABEL
     fil_City AT ROW 6.4 COL 75 COLON-ALIGNED NO-LABEL
     fil_State AT ROW 7.4 COL 75 COLON-ALIGNED NO-LABEL
     fil_Country AT ROW 8.4 COL 75 COLON-ALIGNED NO-LABEL
     fil_Zip AT ROW 9.4 COL 75 COLON-ALIGNED NO-LABEL
     btn_Maintain AT ROW 8.4 COL 1.57
     btn_Clear AT ROW 9.4 COL 1.57
     Person.MailOut AT ROW 10.6 COL 1.86
          LABEL "Mailout is OK"
          VIEW-AS TOGGLE-BOX
          SIZE 12.86 BY .8
     cmb_Phone1 AT ROW 12.6 COL 1.86 NO-LABEL
     fil_Phone1 AT ROW 12.6 COL 15.86 COLON-ALIGNED NO-LABEL
     cmb_Phone2 AT ROW 14.1 COL 1.86 NO-LABEL
     fil_Phone2 AT ROW 14.1 COL 15.86 COLON-ALIGNED NO-LABEL
     cmb_Phone3 AT ROW 15.6 COL 1.86 NO-LABEL
     fil_Phone3 AT ROW 15.6 COL 15.86 COLON-ALIGNED NO-LABEL
     cmb_Phone4 AT ROW 17 COL 1.86 NO-LABEL
     fil_Phone4 AT ROW 17 COL 15.86 COLON-ALIGNED NO-LABEL
     edt_notes AT ROW 12.6 COL 52.72 NO-LABEL
     fil_ContactTypes AT ROW 18.5 COL 12.43 NO-LABEL
     btn_alt-job AT ROW 2.6 COL 41.29
     btn_alt-company AT ROW 3.8 COL 41.29
     RECT-1 AT ROW 11.7 COL 2.86
     RECT-2 AT ROW 6.2 COL 2.72
     RECT-23 AT ROW 1 COL 1
     "Address:" VIEW-AS TEXT
          SIZE 7 BY .8 AT ROW 6.5 COL 1.86
          FONT 10
     "Phone:" VIEW-AS TEXT
          SIZE 8 BY .6 AT ROW 12 COL 1.86
          FONT 10
     "Zip:" VIEW-AS TEXT
          SIZE 3.43 BY 1 AT ROW 9.4 COL 70.72
          FONT 10
     "Country:" VIEW-AS TEXT
          SIZE 5.72 BY 1 AT ROW 8.4 COL 70.72
          FONT 10
     "State:" VIEW-AS TEXT
          SIZE 4.57 BY 1 AT ROW 7.4 COL 70.72
          FONT 10
     "Company:" VIEW-AS TEXT
          SIZE 6.86 BY 1 AT ROW 3.8 COL 1.86
          FONT 10
.
/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME F-Main
     "Department:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 2.6 COL 50.43
          FONT 10
     "Job Titile:" VIEW-AS TEXT
          SIZE 6.86 BY 1 AT ROW 2.6 COL 1.86
          FONT 10
     "Preferred:" VIEW-AS TEXT
          SIZE 7 BY 1 AT ROW 1.2 COL 50.43
          FONT 10
     "Notes:" VIEW-AS TEXT
          SIZE 5 BY .6 AT ROW 12 COL 52.72
          FONT 10
     "Contact Types:" VIEW-AS TEXT
          SIZE 10.86 BY 1 AT ROW 18.5 COL 1
          FONT 10
     "File as" VIEW-AS TEXT
          SIZE 6.29 BY .65 AT ROW 4 COL 50.43
     "Spouse:" VIEW-AS TEXT
          SIZE 5.72 BY 1 AT ROW 5 COL 1.86
          FONT 10
     "City:" VIEW-AS TEXT
          SIZE 3.43 BY 1 AT ROW 6.4 COL 70.72
          FONT 10
     "Golf Handicap:" VIEW-AS TEXT
          SIZE 10.29 BY 1 AT ROW 5 COL 77.86
          FONT 10
     "Full Name:" VIEW-AS TEXT
          SIZE 7.43 BY 1 AT ROW 1.2 COL 1.86
          FONT 10
     "Birth Date:" VIEW-AS TEXT
          SIZE 7.43 BY 1 AT ROW 5 COL 50.43
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Person
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 23.35
         WIDTH              = 108.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cmb_Address IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Phone1 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Phone2 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Phone3 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Phone4 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Person.Company IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Person.DateOfBirth IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Person.Department IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
ASSIGN 
       edt_Address:RETURN-INSERTED IN FRAME F-Main  = TRUE.

ASSIGN 
       edt_notes:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR FILL-IN fil_ContactTypes IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN Person.GolfHandicap IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Person.JobTitle IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX Person.MailOut IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR COMBO-BOX Person.PersonTitle IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Person.Preferred IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Person.SortOn IN FRAME F-Main
   NO-DISPLAY NO-ENABLE EXP-LABEL                                       */
ASSIGN 
       Person.SortOn:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN Person.Spouse IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_alt-company
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_alt-company V-table-Win
ON CHOOSE OF btn_alt-company IN FRAME F-Main /* Alternate */
DO:
DO WITH FRAME {&FRAME-NAME}:
  RUN vwr/mnt/d-alternate.w ( Person.PersonCode, INPUT Person.Company, "Company" ).
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_alt-job
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_alt-job V-table-Win
ON CHOOSE OF btn_alt-job IN FRAME F-Main /* Alternate */
DO:
DO WITH FRAME {&FRAME-NAME}:
  RUN vwr/mnt/d-alternate.w ( Person.PersonCode, INPUT Person.JobTitle, "JobTitle" ).
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_Clear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Clear V-table-Win
ON CHOOSE OF btn_Clear IN FRAME F-Main /* Clear */
DO:
  RUN clear-address.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_Maintain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Maintain V-table-Win
ON CHOOSE OF btn_Maintain IN FRAME F-Main /* Maintain */
DO:
  IF NOT AVAILABLE( Person ) THEN RETURN.
  IF NOT AVAILABLE( PostalDetail ) THEN RETURN.
  RUN vwr/mnt/d-postal.w( Person.PersonCode, PostalDetail.PostalType ) .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Address
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Address V-table-Win
ON VALUE-CHANGED OF cmb_Address IN FRAME F-Main
DO:
  RUN address-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone1 V-table-Win
ON VALUE-CHANGED OF cmb_Phone1 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone2 V-table-Win
ON VALUE-CHANGED OF cmb_Phone2 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone3 V-table-Win
ON VALUE-CHANGED OF cmb_Phone3 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone4 V-table-Win
ON VALUE-CHANGED OF cmb_Phone4 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edt_Address
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edt_Address V-table-Win
ON LEAVE OF edt_Address IN FRAME F-Main
DO:
  IF NUM-ENTRIES( SELF:SCREEN-VALUE, "~n" ) > 4 THEN DO:
    MESSAGE "Some window envelopes and/or reports may not correctly~n"
            "display addresses with more than four lines"
            VIEW-AS ALERT-BOX WARNING
            TITLE "Warning: possibly too many address lines".
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fill_FullName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fill_FullName V-table-Win
ON LEAVE OF fill_FullName IN FRAME F-Main
DO:
  RUN check-sort-on.
  IF mode = "Add" THEN RUN check-duplicate.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fill_FullName V-table-Win
ON MOUSE-SELECT-DBLCLICK OF fill_FullName IN FRAME F-Main
DO:
  RUN specify-full-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone1 V-table-Win
ON LEAVE OF fil_Phone1 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone2 V-table-Win
ON LEAVE OF fil_Phone2 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone3 V-table-Win
ON LEAVE OF fil_Phone3 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone4 V-table-Win
ON LEAVE OF fil_Phone4 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE address-changed V-table-Win 
PROCEDURE address-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN update-address.
  RUN display-address.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'PersonCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Person
           &WHERE = "WHERE Person.PersonCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Person"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Person"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE alternate-company-dialog V-table-Win 
PROCEDURE alternate-company-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  RUN vwr/mnt/d-alt-company.w ( Person.PersonCode, Person.Company, "Company" ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE alternate-title-dialog V-table-Win 
PROCEDURE alternate-title-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  RUN vwr/mnt/d-alt-job.w ( Person.PersonCode, Person.JobTitle ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-person. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-duplicate V-table-Win 
PROCEDURE check-duplicate :
/*------------------------------------------------------------------------------
  Purpose:  Look for a duplicate of the contact.
------------------------------------------------------------------------------*/
DEF VAR my-full AS CHAR NO-UNDO.
DEF VAR my-title AS CHAR NO-UNDO.
DEF VAR my-first AS CHAR NO-UNDO.
DEF VAR my-middle AS CHAR NO-UNDO.
DEF VAR my-last AS CHAR NO-UNDO.
DEF VAR my-suffix AS CHAR NO-UNDO.
DEF VAR tmp_edit AS LOGI NO-UNDO INITIAL Yes.

  my-full = INPUT FRAME {&FRAME-NAME} fill_FullName .
  RUN split-name( my-full, OUTPUT my-title, OUTPUT my-first, OUTPUT my-middle,
                                            OUTPUT my-last, OUTPUT my-suffix).

  DEF BUFFER tmp_Person FOR Person.
  FIND FIRST tmp_Person WHERE tmp_Person.FirstName = my-first
                          AND tmp_Person.LastName = my-last
                          NO-LOCK NO-ERROR.
  IF AVAILABLE(tmp_Person) THEN DO:
    MESSAGE "Person" tmp_Person.PersonCode "already exists with that name.~n"
            "~nDo you want to edit that record?"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK-CANCEL
            TITLE "Person already on file"
            UPDATE tmp_edit.
    IF tmp_edit THEN DO:
      IF mode = "Add" THEN RUN delete-person.
      mode = "Maintain".
      RUN check-modified( "CLEAR" ).
      have-records = Yes.
      RUN set-attribute-list( 'PersonCode = ' + STRING(tmp_Person.PersonCode) ).
      RUN dispatch( 'find-using-key':U ).
      FIND Person WHERE Person.PersonCode = tmp_Person.PersonCode
                          NO-LOCK NO-ERROR.
      RUN inst-row-available.
      RUN dispatch( 'row-changed':U ).
      APPLY 'ENTRY':U TO FRAME {&FRAME-NAME}.
      RETURN NO-APPLY.
    END.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-sort-on V-table-Win 
PROCEDURE check-sort-on :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-address V-table-Win 
PROCEDURE clear-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  edt_Address:SCREEN-VALUE = "".
  fil_City:SCREEN-VALUE = "".
  fil_State:SCREEN-VALUE = "".
  fil_Country:SCREEN-VALUE = "".
  fil_Zip:SCREEN-VALUE = "".
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN verify-person.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  RUN dispatch( 'update-record':U ).
  IF mode = "Add" THEN RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-person V-table-Win 
PROCEDURE delete-person :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Person EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Person THEN DELETE Person.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-address V-table-Win 
PROCEDURE display-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR idx         AS INT  NO-UNDO.
  DEF VAR postal-type AS CHAR NO-UNDO.
  
  idx = LOOKUP( INPUT cmb_Address, cmb_Address:LIST-ITEMS ).
  postal-type = ENTRY( idx, cmb_Address:PRIVATE-DATA ).

  FIND PostalDetail WHERE
    PostalDetail.PersonCode = Person.PersonCode AND
    PostalDetail.PostalType = postal-type
    NO-LOCK NO-ERROR.

  ASSIGN
    edt_address = IF AVAILABLE Postaldetail THEN PostalDetail.Address ELSE ""
    fil_City    = IF AVAILABLE Postaldetail THEN PostalDetail.City    ELSE ""
    fil_State   = IF AVAILABLE Postaldetail THEN PostalDetail.State   ELSE ""
    fil_Zip     = IF AVAILABLE Postaldetail THEN PostalDetail.Zip     ELSE ""
    fil_Country = IF AVAILABLE Postaldetail THEN PostalDetail.Country ELSE "".
    
  DISPLAY edt_address fil_City fil_State fil_Zip fil_Country WITH FRAME {&FRAME-NAME}.
  edt_Address:PRIVATE-DATA = postal-type.
   
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-contact-types V-table-Win 
PROCEDURE display-contact-types :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  IF NOT AVAILABLE Person THEN RETURN.
  
  fil_ContactTypes = "".
  FOR EACH Contact OF Person NO-LOCK:
    fil_ContactTypes = fil_ContactTypes + IF fil_ContactTypes = "" THEN "" ELSE ", ".
    fil_ContactTypes = fil_ContactTypes + Contact.ContactType.
  END.
  
  DISPLAY fil_ContactTypes WITH FRAME {&FRAME-NAME}.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-notes V-table-Win 
PROCEDURE display-notes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  
  FIND Note WHERE Note.NoteCode = Person.Notes NO-ERROR.
  
  ASSIGN  edt_notes = IF AVAILABLE Note THEN Note.Details ELSE "".
  DISPLAY edt_notes WITH FRAME {&FRAME-NAME}.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-phone V-table-Win 
PROCEDURE display-phone :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.

  DEF VAR idx          AS INT  NO-UNDO.
  DEF VAR phone-type   AS CHAR NO-UNDO.
  DEF VAR sv           AS CHAR NO-UNDO INITIAL "".

  idx = LOOKUP( h-cmb:SCREEN-VALUE, h-cmb:LIST-ITEMS ).
  phone-type = ENTRY( idx, h-cmb:PRIVATE-DATA ).
  
  FIND PhoneDetail WHERE
    PhoneDetail.PersonCode = Person.PersonCode AND
    PhoneDetail.PhoneType  = phone-type
    NO-LOCK NO-ERROR.

  IF AVAILABLE PhoneDetail THEN
    RUN combine-phone( PhoneDetail.cCountryCode, PhoneDetail.cSTDCode, PhoneDetail.Number, OUTPUT sv ).
  
  h-fil:SCREEN-VALUE = sv.
  h-fil:PRIVATE-DATA = phone-type.  

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF p-full <> INPUT fill_FullName THEN DO:
    p-full = INPUT fill_FullName .

    IF INPUT Person.Company = p-full THEN ASSIGN
      Person.PersonTitle = ""
      p-first   = ""
      p-middle  = ""
      p-last    = p-full
      p-suffix  = "".
    ELSE
      RUN split-name( p-full, OUTPUT p-title, OUTPUT p-first, OUTPUT p-middle,
                                            OUTPUT p-last, OUTPUT p-suffix).
  END.

  Person.FirstName   = p-first.
  Person.MiddleName  = p-middle.
  Person.LastName    = p-last.
  Person.NameSuffix  = p-suffix.

  RUN update-address.
  RUN update-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN update-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN update-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN update-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
  RUN update-notes.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  p-full = combine-name( "", Person.FirstName, Person.MiddleName, Person.LastName, Person.NameSuffix).
  fill_FullName:SCREEN-VALUE = p-full.
  RUN display-address.
  RUN refresh-phone-types.
  RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
  RUN display-notes.
  RUN display-contact-types.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "Add" OR mode = "Copy" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE RUN dispatch( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN set-default-address-type.

  IF AVAILABLE(Person) THEN DO:
    p-title     = Person.PersonTitle.
    p-first     = Person.FirstName.
    p-middle    = Person.MiddleName.
    p-last      = Person.LastName.
    p-suffix    = Person.NameSuffix.
  END.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
DEF BUFFER CurrentPerson FOR Person.

  IF mode = "Copy" AND copy-person() THEN DO:
    RUN set-attribute-list( 'Mode = Add':U ).
  END.
  ELSE DO:
    DEF VAR contact-type AS CHAR NO-UNDO.

    CREATE Person.

    RUN request-attribute IN adm-broker-hdl ( THIS-PROCEDURE,
                                     'RECORD-SOURCE':U, 'FilterBy-Case':U ).
    contact-type = TRIM( ENTRY ( 1, RETURN-VALUE, "-" ) ).

    IF CAN-FIND( ContactType WHERE ContactType.ContactType = contact-type ) THEN DO:
      CREATE Contact.
      ASSIGN
        Contact.ContactType = contact-type.
        Contact.PersonCode  = Person.PersonCode.
    END.
  END.

  CURRENT-WINDOW:TITLE = "Adding a new Person".
  RUN set-default-address-type.
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'display-fields':U ).
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE phone-changed V-table-Win 
PROCEDURE phone-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.
  
  RUN update-phone( h-cmb, h-fil ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize V-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN refresh-address-types.
  RUN refresh-phone-types.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN have-records = Yes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-address-types V-table-Win 
PROCEDURE refresh-address-types :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item         AS CHAR NO-UNDO.
  DEF VAR pd           AS CHAR NO-UNDO.

  cmb_Address:LIST-ITEMS = "".

  FOR EACH PostalType NO-LOCK /* WHERE
    PostalType.PostalType <> "BILL" */ :
    item = PostalType.Description.
    IF cmb_Address:ADD-LAST( item ) THEN.
    pd = pd + IF pd = "" THEN "" ELSE ",".
    pd = pd + PostalType.PostalType.
  END.
  
  cmb_address:SCREEN-VALUE = cmb_address:ENTRY( 1 ).
  edt_address:PRIVATE-DATA = ENTRY( 1, cmb_address:PRIVATE-DATA ).
  cmb_address:PRIVATE-DATA = pd.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-phone-types V-table-Win 
PROCEDURE refresh-phone-types :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR pd   AS CHAR NO-UNDO.
  DEF VAR li   AS CHAR NO-UNDO.
  DEF VAR sv-list   AS CHAR NO-UNDO INITIAL "".
  DEF VAR pd-list   AS CHAR NO-UNDO INITIAL "".
  DEF VAR sv-list2  AS CHAR NO-UNDO INITIAL "".
  DEF VAR pd-list2  AS CHAR NO-UNDO INITIAL "".
  DEF VAR i         AS INT  NO-UNDO.
    
  DO i = 1 TO NUM-ENTRIES( phone-types ):
    pd = ENTRY( i, phone-types ).
    FIND PhoneType WHERE PhoneType = pd NO-LOCK NO-ERROR.
    li = (IF AVAILABLE(PhoneType) THEN PhoneType.Description ELSE pd).
    IF AVAILABLE(Person) AND CAN-FIND( PhoneDetail OF Person WHERE PhoneDetail.PhoneType = pd)
    THEN DO:
      sv-list = sv-list + "," + li.
      pd-list = pd-list + "," + pd.
    END.
    ELSE DO:
      sv-list2 = sv-list2 + "," + li.
      pd-list2 = pd-list2 + "," + pd.
    END.
  END.
  IF NUM-ENTRIES(sv-list) < 5 THEN DO:
    sv-list = sv-list + sv-list2.
    pd-list = pd-list + pd-list2.
  END.
  sv-list = TRIM(sv-list, ",").
  pd-list = TRIM(pd-list, ",").

  li = "".
  pd = "".
  FOR EACH PhoneType NO-LOCK:
    li = li + (IF li = "" THEN "" ELSE ",") + PhoneType.Description.
    pd = pd + (IF pd = "" THEN "" ELSE ",") + PhoneType.PhoneType.
  END.

  cmb_Phone1:LIST-ITEMS = li.  cmb_Phone2:LIST-ITEMS = li.
  cmb_Phone3:LIST-ITEMS = li.  cmb_Phone4:LIST-ITEMS = li.

  cmb_Phone1:SCREEN-VALUE = ENTRY( 1, sv-list ).
  cmb_Phone2:SCREEN-VALUE = ENTRY( 2, sv-list ).
  cmb_Phone3:SCREEN-VALUE = ENTRY( 3, sv-list ).
  cmb_Phone4:SCREEN-VALUE = ENTRY( 4, sv-list ).
  
  fil_Phone1:PRIVATE-DATA = ENTRY( 1, phone-types ).
  fil_Phone2:PRIVATE-DATA = ENTRY( 2, phone-types ).
  fil_Phone3:PRIVATE-DATA = ENTRY( 3, phone-types ).
  fil_Phone4:PRIVATE-DATA = ENTRY( 4, phone-types ).

  cmb_Phone1:PRIVATE-DATA = pd.  cmb_Phone2:PRIVATE-DATA = pd.
  cmb_Phone3:PRIVATE-DATA = pd.  cmb_Phone4:PRIVATE-DATA = pd.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "PersonCode" "Person" "PersonCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Person"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-default-address-type V-table-Win 
PROCEDURE set-default-address-type :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR got-address  AS LOGI NO-UNDO.
  DEF VAR priori-list  AS CHAR NO-UNDO.
  DEF VAR default-type AS CHAR NO-UNDO.
  DEF VAR i            AS INT  NO-UNDO.
  DEF VAR default-item AS CHAR NO-UNDO.
  
  priori-list = "POST,MAIN,COUR,BILL".
  
  DO WHILE NOT got-address AND i < NUM-ENTRIES( priori-list ):
    
    i = i + 1.
    FIND PostalDetail OF Person WHERE PostalDetail.PostalType = ENTRY( i, priori-list )
      NO-LOCK NO-ERROR.
    got-address = AVAILABLE PostalDetail.
    
  END.
  
  IF NOT AVAILABLE PostalDetail THEN
    FIND FIRST PostalDetail OF Person /* WHERE PostalDetail.PostalType <> "BILL" */
      NO-LOCK NO-ERROR.
      
  default-type = IF AVAILABLE PostalDetail THEN
    PostalDetail.PostalType ELSE ENTRY( 1, priori-list ).
  
  IF default-type = "" THEN
  DO:
    FIND FIRST PostalType WHERE PostalType.PostalType <> "BILL"
      NO-LOCK NO-ERROR.
    IF AVAILABLE Postaltype THEN default-type = Postaltype.PostalType.
  END.
  
  ASSIGN default-item = ENTRY( LOOKUP( default-type, cmb_address:PRIVATE-DATA ), cmb_address:LIST-ITEMS )
    NO-ERROR.
  
  cmb_address:SCREEN-VALUE = IF ERROR-STATUS:ERROR THEN "" ELSE default-item.

  RUN display-address.
  
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE specify-full-name V-table-Win 
PROCEDURE specify-full-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  p-title = INPUT Person.PersonTitle .
  RUN vwr/mnt/d-name.w ( INPUT-OUTPUT p-title, INPUT-OUTPUT p-first,
                    INPUT-OUTPUT p-middle, INPUT-OUTPUT p-last, INPUT-OUTPUT p-suffix ).

  p-full = combine-name( "", p-first, p-middle, p-last, p-suffix).
  fill_FullName:SCREEN-VALUE = p-full.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-address V-table-Win 
PROCEDURE update-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR del-record AS LOGI NO-UNDO.

  IF NOT AVAILABLE Person THEN RETURN.

  FIND PostalDetail WHERE
    PostalDetail.PersonCode = Person.PersonCode AND
    PostalDetail.PostalType = edt_Address:PRIVATE-DATA NO-ERROR.
  
  ASSIGN FRAME {&FRAME-NAME} edt_address fil_state fil_zip fil_country fil_city.
  del-record = edt_address = "" AND fil_zip = "" AND fil_state = "" AND fil_country = "" AND fil_City = "".

  IF del-record THEN
    IF AVAILABLE PostalDetail THEN
      DELETE PostalDetail.
    ELSE DO: END.
  ELSE
  DO:
    IF NOT AVAILABLE PostalDetail THEN
    DO:
      CREATE PostalDetail.
      ASSIGN
        PostalDetail.PersonCode = Person.PersonCode
        PostalDetail.PostalType = edt_Address:PRIVATE-DATA.
    END.

    ASSIGN
      PostalDetail.Address = TRIM( edt_Address )
      PostalDetail.City    = fil_city
      PostalDetail.State   = fil_State
      PostalDetail.Zip     = fil_Zip
      PostalDetail.Country = fil_Country.      
  
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-notes V-table-Win 
PROCEDURE update-notes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  ASSIGN FRAME {&FRAME-NAME} edt_Notes.
  
  IF edt_Notes = "" THEN
  DO:
    IF AVAILABLE Note THEN DELETE Note.
    Person.Notes = ?.
  END.
  ELSE
  DO:
    IF NOT AVAILABLE Note THEN
    DO:
      DEF BUFFER LastNote FOR Note.
      FIND LAST LastNote NO-LOCK NO-ERROR.
      CREATE Note.
      ASSIGN
        Note.NoteCode = IF AVAILABLE LastNote THEN LastNote.NoteCode + 1 ELSE 1
        Person.Notes  = Note.NoteCode.
    END.
    
    ASSIGN Note.Details = edt_notes.
  END.
      
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-phone V-table-Win 
PROCEDURE update-phone :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.
  
  DEF VAR del-record AS LOGI NO-UNDO.

  IF NOT AVAILABLE Person THEN RETURN.

  FIND PhoneDetail WHERE
    PhoneDetail.PersonCode = Person.PersonCode AND
    PhoneDetail.PhoneType = h-fil:PRIVATE-DATA   NO-ERROR.

  del-record = TRIM( h-fil:SCREEN-VALUE ) = "".

  IF del-record THEN DO:
    IF AVAILABLE PhoneDetail THEN DELETE PhoneDetail.
  END.
  ELSE DO:
    IF NOT AVAILABLE PhoneDetail THEN
    DO:
      CREATE PhoneDetail.
      ASSIGN
        PhoneDetail.PersonCode = Person.PersonCode
        PhoneDetail.PhoneType  = h-fil:PRIVATE-DATA.
    END.

    /* Parse the phone number ! */
    RUN split-phone( h-fil:SCREEN-VALUE, OUTPUT PhoneDetail.cCountryCode,
                        OUTPUT PhoneDetail.cSTDCode, OUTPUT PhoneDetail.Number ).
  END.
    
  /* Reflect changes to other linked numbers on screen */
  DEF VAR update-type AS CHAR NO-UNDO. /* Do not delete this variable!! */
  update-type = h-fil:PRIVATE-DATA.

  IF fil_Phone1:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
        
  IF fil_Phone2:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
        
  IF fil_Phone3:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
        
  IF fil_Phone4:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-person V-table-Win 
PROCEDURE verify-person :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION copy-person V-table-Win 
FUNCTION copy-person RETURNS LOGICAL
  (  ) :
/*------------------------------------------------------------------------------
  Purpose:  Copy the current row-available to this new person
    Notes:  
------------------------------------------------------------------------------*/
DEF BUFFER CurrentPerson FOR Person.
DEF BUFFER CurrentPhone FOR PhoneDetail.
DEF BUFFER CurrentPost  FOR PostalDetail.
DEF BUFFER CurrentContact FOR Contact.

  RUN notify( 'send-records,record-source':U ).
  IF NOT AVAILABLE(Person) THEN RETURN FALSE.

  FIND CurrentPerson WHERE ROWID(CurrentPerson) = ROWID(Person) NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Person) THEN RETURN FALSE.

  CREATE Person.
  ASSIGN Person.Company = CurrentPerson.Company
         Person.Department = CurrentPerson.Department
         Person.JobTitle = CurrentPerson.JobTitle
         Person.Office = CurrentPerson.Office.

  FOR EACH CurrentPhone NO-LOCK OF CurrentPerson:
    CREATE PhoneDetail.
    BUFFER-COPY CurrentPhone EXCEPT CurrentPhone.PersonCode TO PhoneDetail
                ASSIGN PhoneDetail.PersonCode = Person.PersonCode.
  END.

  FOR EACH CurrentPost NO-LOCK OF CurrentPerson:
    CREATE PostalDetail.
    BUFFER-COPY CurrentPost EXCEPT CurrentPost.PersonCode TO PostalDetail
                ASSIGN PostalDetail.PersonCode = Person.PersonCode.
  END.

  FOR EACH CurrentContact NO-LOCK OF CurrentPerson:
    CREATE Contact.
    BUFFER-COPY CurrentContact EXCEPT CurrentContact.PersonCode TO Contact
                ASSIGN Contact.PersonCode = Person.PersonCode.
  END.

  RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


