&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.

{inc/ofc-this.i}
{inc/topic/tpprojct.i}

{inc/ofc-set.i "Project-Groups-Property" "property-groups"}
IF NOT AVAILABLE(OfficeSetting) THEN property-groups = "PROPEX,TENEX,OWNEX".
{inc/ofc-set.i "Project-Groups-Company" "company-groups"}
IF NOT AVAILABLE(OfficeSetting) THEN company-groups = "ADMIN".
{inc/ofc-set.i "AcctGroup-Investment" "investment-groups"}
IF AVAILABLE(OfficeSetting) THEN DO:
  property-groups = investment-groups + "," + property-groups.
  company-groups  = investment-groups + "," + company-groups.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Project
&Scoped-define FIRST-EXTERNAL-TABLE Project


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Project.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Project.ProjectCode Project.ExpenditureType ~
Project.Active Project.Name Project.Description Project.EntityType ~
Project.EntityCode Project.StartDate Project.CompleteDate Project.AreaSize ~
Project.Approved Project.ApproveDate Project.ApprovedAmount ~
Project.BudgetsFrozen Project.FirstApprover Project.SecondApprover 
&Scoped-define ENABLED-TABLES Project
&Scoped-define FIRST-ENABLED-TABLE Project
&Scoped-define DISPLAYED-TABLES Project
&Scoped-define FIRST-DISPLAYED-TABLE Project
&Scoped-Define ENABLED-OBJECTS cmb_ProjectType fil_ProposerAbbr cmb_Account ~
RECT-1 RECT-2 RECT-3 
&Scoped-Define DISPLAYED-FIELDS Project.ProjectCode Project.ExpenditureType ~
Project.Active Project.Name Project.Description Project.EntityType ~
Project.EntityCode Project.StartDate Project.CompleteDate Project.AreaSize ~
Project.Approved Project.ApproveDate Project.ApprovedAmount ~
Project.BudgetsFrozen Project.FirstApprover Project.SecondApprover 
&Scoped-Define DISPLAYED-OBJECTS cmb_ProjectType fil_ProposerAbbr ~
fil_Proposer fil_Entity cmb_Account fil_Approver1 fil_Approver2 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
ProjectCode|y|y|ttpl.Project.ProjectCode
EntityType||y|ttpl.Project.EntityType
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "ProjectCode",
     Keys-Supplied = "ProjectCode,EntityType"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-parent-project V-table-Win 
FUNCTION get-parent-project RETURNS INTEGER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD invalid-hierarchy V-table-Win 
FUNCTION invalid-hierarchy RETURNS LOGICAL
  ( INPUT try-project AS INTEGER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD set-proposer-from-abbr V-table-Win 
FUNCTION set-proposer-from-abbr RETURNS LOGICAL
  ( INPUT new-proposer-abbr AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_Account AS CHARACTER FORMAT "X(256)":U 
     LABEL "Account" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     DROP-DOWN-LIST
     SIZE 60.57 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_ProjectType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Classification" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 61.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Approver1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51.43 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Approver2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51.43 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Entity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 48.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Proposer AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 50.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_ProposerAbbr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Proposer" 
     VIEW-AS FILL-IN 
     SIZE 5.43 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 73.72 BY 18.2.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 72.57 BY 2.6.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 72.57 BY 3.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Project.ProjectCode AT ROW 1 COL 65.29 COLON-ALIGNED
          LABEL "Project Code"
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
     Project.ExpenditureType AT ROW 1.6 COL 1.86
          LABEL "Expense Type" FORMAT "X(50)"
          VIEW-AS COMBO-BOX INNER-LINES 5
          LIST-ITEMS "C - Property CapEx","G - General Expenses","P - Project CapEx","S - Special Project","M - Major Project" 
          DROP-DOWN-LIST
          SIZE 35.43 BY 1
          FONT 10
     Project.Active AT ROW 2 COL 67.29
          VIEW-AS TOGGLE-BOX
          SIZE 7.14 BY .8
     cmb_ProjectType AT ROW 2.8 COL 10.43 COLON-ALIGNED
     Project.Name AT ROW 3.8 COL 10.43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 61.72 BY 1
          FONT 10
     Project.Description AT ROW 4.8 COL 12.43 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 61.72 BY 5.2
          FONT 10
     fil_ProposerAbbr AT ROW 10 COL 10.43 COLON-ALIGNED
     fil_Proposer AT ROW 10 COL 19.86 COLON-ALIGNED NO-LABEL
     Project.EntityType AT ROW 12 COL 10.43 COLON-ALIGNED
          LABEL "Entity"
          VIEW-AS FILL-IN 
          SIZE 2.43 BY 1
     Project.EntityCode AT ROW 12 COL 12.86 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 6.72 BY 1
     fil_Entity AT ROW 12 COL 22.43 COLON-ALIGNED NO-LABEL
     cmb_Account AT ROW 13 COL 10.43 COLON-ALIGNED
     Project.StartDate AT ROW 14.4 COL 10.43 COLON-ALIGNED
          LABEL "Start Date"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     Project.CompleteDate AT ROW 14.4 COL 34.43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     Project.AreaSize AT ROW 14.4 COL 58.43 COLON-ALIGNED
          LABEL "Project Area"
          VIEW-AS FILL-IN 
          SIZE 12.57 BY 1.05
     Project.Approved AT ROW 16 COL 2
          VIEW-AS TOGGLE-BOX
          SIZE 9.57 BY 1.05
     Project.ApproveDate AT ROW 16 COL 16 COLON-ALIGNED
          LABEL "Date"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1.05
     Project.ApprovedAmount AT ROW 16 COL 40 COLON-ALIGNED
          LABEL "Approved CapEx" FORMAT "->>>,>>>,>>9.99"
          VIEW-AS FILL-IN 
          SIZE 12.57 BY 1
     Project.BudgetsFrozen AT ROW 16 COL 59
          VIEW-AS TOGGLE-BOX
          SIZE 14 BY 1.05
     Project.FirstApprover AT ROW 17.2 COL 10.43 COLON-ALIGNED
          LABEL "Approver 1"
          VIEW-AS FILL-IN 
          SIZE 5.43 BY 1
     fil_Approver1 AT ROW 17.2 COL 19.57 COLON-ALIGNED NO-LABEL
     Project.SecondApprover AT ROW 18.2 COL 10.43 COLON-ALIGNED
          LABEL "Approver 2"
          VIEW-AS FILL-IN 
          SIZE 5.43 BY 1
     fil_Approver2 AT ROW 18.2 COL 19.57 COLON-ALIGNED NO-LABEL
     RECT-1 AT ROW 1.4 COL 1
     RECT-2 AT ROW 11.6 COL 1.57
     RECT-3 AT ROW 15.8 COL 1.57
     "Parent Account Details" VIEW-AS TEXT
          SIZE 18.29 BY .65 AT ROW 11.2 COL 2.72
     "Description:" VIEW-AS TEXT
          SIZE 8 BY .8 AT ROW 4.8 COL 3.86
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.Project
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 21.25
         WIDTH              = 85.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Project.ApprovedAmount IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Project.ApproveDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Project.AreaSize IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Project.EntityCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Project.EntityType IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR COMBO-BOX Project.ExpenditureType IN FRAME F-Main
   ALIGN-L EXP-LABEL EXP-FORMAT                                         */
/* SETTINGS FOR FILL-IN fil_Approver1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Approver2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Entity IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Proposer IN FRAME F-Main
   NO-ENABLE DEF-LABEL                                                  */
/* SETTINGS FOR FILL-IN Project.FirstApprover IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Project.ProjectCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Project.SecondApprover IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Project.StartDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME cmb_ProjectType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ProjectType V-table-Win
ON U1 OF cmb_ProjectType IN FRAME F-Main /* Classification */
DO:
  {inc/selcmb/scpjt1.i "Project" "ProjectType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ProjectType V-table-Win
ON U2 OF cmb_ProjectType IN FRAME F-Main /* Classification */
DO:
  {inc/selcmb/scpjt2.i "Project" "ProjectType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Project.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Project.EntityCode V-table-Win
ON LEAVE OF Project.EntityCode IN FRAME F-Main /* Code */
DO:
DO WITH FRAME {&FRAME-NAME}:
  CASE INPUT Project.EntityType:
    WHEN 'P' THEN DO: {inc/selcde/cdpro.i "fil_Entity"} END.
    WHEN 'L' THEN DO: {inc/selcde/cdcmp.i "fil_Entity"} END.
    WHEN 'J' THEN DO:
      RUN build-account-combo.
      {inc/selcde/cdprj.i "fil_Entity"}
    END.
  END CASE.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Project.EntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Project.EntityType V-table-Win
ON ANY-PRINTABLE OF Project.EntityType IN FRAME F-Main /* Entity */
DO:
  APPLY CAPS( CHR( LASTKEY ) ) TO SELF.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Project.EntityType V-table-Win
ON LEAVE OF Project.EntityType IN FRAME F-Main /* Entity */
DO:
  IF SELF:MODIFIED THEN DO:
    RUN verify-entity-type.
    SELF:MODIFIED = No.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
    RUN entity-type-changed.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Project.ExpenditureType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Project.ExpenditureType V-table-Win
ON VALUE-CHANGED OF Project.ExpenditureType IN FRAME F-Main /* Expense Type */
DO:
  RUN expenditure-type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Approver1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver1 V-table-Win
ON U1 OF fil_Approver1 IN FRAME F-Main
DO:
  {inc/selfil/sfapp1.i "Project" "FirstApprover"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver1 V-table-Win
ON U2 OF fil_Approver1 IN FRAME F-Main
DO:
  {inc/selfil/sfapp2.i "Project" "FirstApprover"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver1 V-table-Win
ON U3 OF fil_Approver1 IN FRAME F-Main
DO:
  {inc/selfil/sfapp3.i "Project" "FirstApprover"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Approver2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver2 V-table-Win
ON U1 OF fil_Approver2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp1.i "Project" "SecondApprover"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver2 V-table-Win
ON U2 OF fil_Approver2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp2.i "Project" "SecondApprover"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver2 V-table-Win
ON U3 OF fil_Approver2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp3.i "Project" "SecondApprover"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Entity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Entity V-table-Win
ON U1 OF fil_Entity IN FRAME F-Main
DO:
  CASE Project.EntityType:SCREEN-VALUE:
    WHEN 'P' THEN DO: {inc/selfil/sfpro1.i "Project" "EntityCode"} END.
    WHEN 'L' THEN DO: {inc/selfil/sfcmp1.i "Project" "Entitycode"} END.
    WHEN 'J' THEN DO: {inc/selfil/sfprj1.i "Project" "Entitycode"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Entity V-table-Win
ON U2 OF fil_Entity IN FRAME F-Main
DO:
  CASE Project.EntityType:SCREEN-VALUE:
    WHEN 'P' THEN DO: {inc/selfil/sfpro2.i "Project" "EntityCode"} END.
    WHEN 'L' THEN DO: {inc/selfil/sfcmp2.i "Project" "Entitycode"} END.
    WHEN 'J' THEN DO: {inc/selfil/sfprj2.i "Project" "Entitycode"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Entity V-table-Win
ON U3 OF fil_Entity IN FRAME F-Main
DO:
  CASE Project.EntityType:SCREEN-VALUE:
    WHEN 'P' THEN DO: {inc/selfil/sfpro3.i "Project" "EntityCode"} END.
    WHEN 'L' THEN DO: {inc/selfil/sfcmp3.i "Project" "Entitycode"} END.
    WHEN 'J' THEN DO: {inc/selfil/sfprj3.i "Project" "Entitycode"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Proposer
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Proposer V-table-Win
ON U1 OF fil_Proposer IN FRAME F-Main
DO:
  {inc/selfil/sfpsn1.i "Project" "Proposer"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Proposer V-table-Win
ON U2 OF fil_Proposer IN FRAME F-Main
DO:
  {inc/selfil/sfpsn2.i "Project" "Proposer"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Proposer V-table-Win
ON U3 OF fil_Proposer IN FRAME F-Main
DO:
  {inc/selfil/sfpsn3.i "Project" "Proposer"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_ProposerAbbr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_ProposerAbbr V-table-Win
ON LEAVE OF fil_ProposerAbbr IN FRAME F-Main /* Proposer */
DO:
DEF VAR abbrev AS CHAR NO-UNDO.
DO WITH FRAME {&FRAME-NAME}:
  abbrev = INPUT fil_ProposerAbbr .
  IF abbrev <> "" THEN DO:
    IF NOT set-proposer-from-abbr( abbrev ) THEN RETURN NO-APPLY.
    APPLY 'ENTRY':U TO Project.EntityType .
    RETURN NO-APPLY.
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Project.FirstApprover
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Project.FirstApprover V-table-Win
ON LEAVE OF Project.FirstApprover IN FRAME F-Main /* Approver 1 */
DO:
  {inc/selcde/cdapp.i "fil_Approver1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Project.SecondApprover
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Project.SecondApprover V-table-Win
ON LEAVE OF Project.SecondApprover IN FRAME F-Main /* Approver 2 */
DO:
  {inc/selcde/cdapp.i "fil_Approver2"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

RUN get-attribute( 'Key-Name':U ).
IF RETURN-VALUE = "" THEN
    RUN set-attribute-list( 'Key-Name = ProjectCode':U ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'ProjectCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Project
           &WHERE = "WHERE Project.ProjectCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Project"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Project"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-account-combo V-table-Win 
PROCEDURE build-account-combo :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR item AS CHAR NO-UNDO.
DEF VAR account-codes AS CHAR NO-UNDO.
DEF VAR account-code AS CHAR NO-UNDO.
DEF VAR account-descriptions AS CHAR NO-UNDO.
DEF VAR group-list AS CHAR NO-UNDO INITIAL "".
DEF VAR i AS INTEGER NO-UNDO.
DEF VAR selected AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  cmb_Account:LIST-ITEMS = "".
  IF NOT AVAILABLE Project THEN RETURN.

  IF INPUT Project.EntityType = "J" THEN DO:
    FOR EACH ProjectBudget NO-LOCK WHERE ProjectBudget.ProjectCode = INPUT Project.EntityCode:
      item = TRIM(ProjectBudget.Description).
      IF item = "" OR item = ? THEN DO:
        FIND ChartOfAccount OF ProjectBudget NO-LOCK NO-ERROR.
        IF AVAILABLE(ChartOfAccount) THEN item = ChartOfAccount.Name.
      END.
      account-descriptions = account-descriptions
                           + (IF account-codes = "" THEN "" ELSE "~n")
                           + " - " + item.
      account-codes = account-codes + (IF account-codes = "" THEN "" ELSE ",")
                    + STRING( ProjectBudget.AccountCode, "9999.99" ).
    END.
  END.
  ELSE DO:
    IF INPUT Project.EntityType = "P" THEN
      group-list = property-groups.
    ELSE IF INPUT Project.EntityType = "L" THEN
      group-list = company-groups.

    DO i = 1 TO NUM-ENTRIES(group-list):
      FOR EACH ChartOfAccount WHERE ChartOfAccount.AccountGroupCode = ENTRY( i, group-list) NO-LOCK:
        IF LOOKUP( STRING(ChartOfAccount.AccountCode, "9999.99"), account-codes) = 0 THEN DO:
          account-descriptions = account-descriptions + (IF account-codes = "" THEN "" ELSE "~n")
                               + " - " + ChartOfAccount.Name.
          account-codes = account-codes + (IF account-codes = "" THEN "" ELSE ",")
                        + STRING( ChartOfAccount.AccountCode, "9999.99" ).
        END.
      END.
    END.
  END.

  /* add them into the combo box. */
  cmb_account:DELIMITER = "~n".
  DO i = 1 TO NUM-ENTRIES(account-codes):
    account-code = ENTRY( i, account-codes).
    item = account-code + ENTRY(i, account-descriptions,"~n").
    IF i=1 THEN
      selected = item.
    ELSE IF account-code = STRING( Project.EntityAccount, "9999.99") THEN
      selected = item.
    IF cmb_account:ADD-LAST( item ) THEN .
  END.
  cmb_Account:SCREEN-VALUE = selected.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-project. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-project.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  RUN dispatch( 'update-record':U ).
/*  RUN notify( 'hide, CONTAINER-SOURCE':u ). */
  IF mode = "Add" THEN RUN notify( 'open-query, RECORD-SOURCE':U ).
  
  RUN dispatch( 'exit':U ).  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-project V-table-Win 
PROCEDURE delete-project :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Project EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Project THEN DELETE Project.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-type-changed V-table-Win 
PROCEDURE entity-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN reset-entity.
  RUN update-entity-button.
  RUN build-account-combo.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE expenditure-type-changed V-table-Win 
PROCEDURE expenditure-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF mode <> "Add" THEN RETURN.
  
  DEF BUFFER LastProject FOR Project.
  DEF VAR project-code AS INT NO-UNDO.
  
  CASE TRIM( ENTRY( 1, INPUT Project.ExpenditureType, "-" ) ):
  
    WHEN "G" THEN
    DO:
      FIND LAST LastProject WHERE LastProject.ProjectCode < 10000
        NO-LOCK NO-ERROR.
      project-code = IF AVAILABLE LastProject THEN LastProject.ProjectCode + 1 ELSE 1.
    END.      
      
    OTHERWISE
    DO:
      DEF VAR beg-range LIKE Project.ProjectCode NO-UNDO.
      DEF VAR end-range LIKE Project.ProjectCode NO-UNDO.
      DEF VAR no-blocks AS LOGI INIT Yes.
      project-code = ?.

      scan-for-capex-block:      
      FOR EACH OfficeSettings OF Office WHERE
        OfficeSettings.SetName BEGINS "CAPEX-RANGE" NO-LOCK:

        beg-range = INT( TRIM( ENTRY( 1, OfficeSettings.SetValue, "-" ) ) ).
        end-range = INT( TRIM( ENTRY( 2, OfficeSettings.SetValue, "-" ) ) ).

        FIND LAST LastProject WHERE
          LastProject.ProjectCode >= beg-range AND
          LastProject.ProjectCode <= end-range NO-LOCK NO-ERROR.
        
        project-code =
          IF AVAILABLE LastProject AND LastProject.ProjectCode <> end-range
            THEN LastProject.ProjectCode + 1
          ELSE IF AVAILABLE LastProject AND LastProject.ProjectCode = end-range
            THEN ? ELSE beg-range.
        IF project-code <> ? THEN LEAVE scan-for-capex-block.
        
      END.

      IF project-code = ? THEN
      DO:
/*
        MESSAGE
          "A project number could not be assigned" SKIP
          "for this office." SKIP(1)
          "Either all number ranges defined for this" SKIP
          "office are full or there area no ranges" SKIP
          "defined." SKIP(1)
          "In either case you will need to create a new" SKIP
          "Office settings record of type CAPEX-RANGE"
          VIEW-AS ALERT-BOX ERROR TITLE "Couldn't allocate a project code".
        Project.ExpenditureType:SCREEN-VALUE = Project.ExpenditureType:ENTRY(1).
*/
        RETURN.
      END.
      
    END.
    
  END CASE.

  Project.ProjectCode:SCREEN-VALUE = STRING( project-code ).
    
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER OtherBudget FOR ProjectBudget.

DO WITH FRAME {&FRAME-NAME}:

  ASSIGN Project.ExpenditureType =
    TRIM( ENTRY( 1, INPUT Project.ExpenditureType, "-" ) ).

  Project.EntityAccount = DEC( SUBSTRING( cmb_Account:SCREEN-VALUE, 1, 7) ).

  IF Project.EntityType <> "J" THEN
    FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = Project.EntityAccount
               NO-LOCK NO-ERROR.
  ELSE
    FIND OtherBudget WHERE OtherBudget.ProjectCode = Project.EntityCode
                       AND OtherBudget.AccountCode = Project.EntityAccount
                       NO-LOCK NO-ERROR.

  DEF VAR diff AS DEC NO-UNDO INITIAL 0.0.
  FOR EACH ProjectBudget OF Project NO-LOCK:
    diff = diff + ProjectBudget.OriginalBudget.
  END.
  diff = Project.ApprovedAmount - diff.

  FIND ProjectBudget WHERE ProjectBudget.ProjectCode = Project.ProjectCode
                       AND ProjectBudget.AccountCode = Project.EntityAccount
                       EXCLUSIVE-LOCK NO-ERROR.
          
  IF NOT AVAILABLE(ProjectBudget) THEN DO:
    CREATE ProjectBudget.
    ASSIGN  ProjectBudget.ProjectCode    = Project.ProjectCode
            ProjectBudget.AccountCode    = Project.EntityAccount
            ProjectBudget.EntityType     = Project.EntityType
            ProjectBudget.EntityCode     = Project.EntityCode
            ProjectBudget.EntityAccount  = Project.EntityAccount
            ProjectBudget.Description    = IF AVAILABLE ChartOfAccount THEN
                    ChartOfAccount.Name ELSE (IF AVAILABLE(OtherBudget) THEN
                    OtherBudget.Description ELSE "").
  END.
  ProjectBudget.OriginalBudget = ProjectBudget.OriginalBudget + diff .
  FIND CURRENT ProjectBudget NO-LOCK.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

IF NOT AVAILABLE Project THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR i AS INT NO-UNDO.
  DEF VAR item AS CHAR NO-UNDO.
  
  DO i = 1 TO NUM-ENTRIES( Project.ExpenditureType:LIST-ITEMS ):
    item = ENTRY( i, Project.ExpenditureType:LIST-ITEMS ).
    IF TRIM( ENTRY( 1, item, "-" ) ) = Project.ExpenditureType THEN
    DO:
      ASSIGN Project.ExpenditureType:SCREEN-VALUE = item.
      LEAVE.
    END.
  END.

  FIND FIRST Approver WHERE Approver.PersonCode = Project.Proposer NO-LOCK NO-ERROR.
  IF AVAILABLE(Approver) THEN DO:
    fil_ProposerAbbr = Approver.ApproverCode .
    DISPLAY fil_ProposerAbbr.
  END.

  RUN expenditure-type-changed.
  RUN update-entity-button.
  RUN build-account-combo.
  
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  Project.ProjectCode:SENSITIVE     = mode = "Add".
  Project.ExpenditureType:SENSITIVE = Yes.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN DO:
    have-records = yes.
    RUN dispatch( 'add-record' ).
  END.
  ELSE IF mode = "View" THEN
    RUN dispatch( 'disable-fields':u ).
  ELSE
    RUN dispatch( 'enable-fields':u ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND Project WHERE Project.ProjectCode = 0 EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE(Project) THEN CREATE Project.

  ASSIGN
    Project.ExpenditureType = "C"
    Project.EntityType      = "P"
    Project.StartDate       = TODAY.

DEF VAR parent-project LIKE Project.ProjectCode NO-UNDO.
DEF BUFFER ParentProject FOR Project.
  parent-project = get-parent-project().
  IF parent-project > 0 THEN DO:
    FIND ParentProject WHERE ParentProject.ProjectCode = parent-project NO-LOCK NO-ERROR.
    IF AVAILABLE(ParentProject) THEN ASSIGN
      Project.ExpenditureType = "S"
      Project.EntityType      = "J"
      Project.EntityCode      = parent-project
      Project.EntityAccount   = ParentProject.EntityAccount
      Project.StartDate       = ParentProject.StartDate
      Project.CompleteDate    = ParentProject.CompleteDate
      Project.FirstApprover   = ParentProject.FirstApprover
      Project.Proposer        = ParentProject.Proposer
      Project.SecondApprover  = ParentProject.SecondApprover
      Project.ProjectType     = ParentProject.ProjectType .
  END.

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

  CURRENT-WINDOW:TITLE = "Adding a new Project".
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-entity V-table-Win 
PROCEDURE reset-entity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  fil_Entity:PRIVATE-DATA = "".
  fil_Entity:SCREEN-VALUE = "".
  Project.EntityCode:SCREEN-VALUE = STRING( 0 ).
  Project.EntityCode:PRIVATE-DATA = ",00000".
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "ProjectCode" "Project" "ProjectCode"}
  {src/adm/template/sndkycas.i "EntityType" "Project" "EntityType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Project"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-button V-table-Win 
PROCEDURE update-entity-button :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  
  FIND EntityType WHERE EntityType.EntityType = INPUT Project.EntityType
    NO-LOCK NO-ERROR.
  IF AVAILABLE EntityType THEN Project.EntityType:LABEL = EntityType.Description.
    
  DEF VAR node-name    AS CHAR NO-UNDO.
  DEF VAR vwr-name     AS CHAR NO-UNDO.
  DEF VAR sort-panel   AS CHAR NO-UNDO.
  DEF VAR filter-panel AS CHAR NO-UNDO.
  
    
  CASE INPUT Project.EntityType:
    WHEN 'P' THEN ASSIGN
      node-name = 'w-selpro' vwr-name = 'b-selpro'
      filter-panel = "Yes" sort-panel = "Yes".
    WHEN 'L' THEN ASSIGN
      node-name = 'w-selcmp' vwr-name = 'b-selcmp'
      filter-panel = "Yes" sort-panel = "Yes".
    WHEN 'J' THEN ASSIGN
      node-name = 'w-selprj' vwr-name = 'b-selprj'
      filter-panel = "No" sort-panel = "No".
  END CASE.

  RUN set-link-attributes IN sys-mgr (
    THIS-PROCEDURE, 
    STRING( fil_Entity:HANDLE ),
    "Target = "       + node-name    + "," +
    "Viewer = "       + vwr-name     + "," +
    "Filter-Panel = " + filter-panel + "," +
    "Sort-Panel = "   + sort-panel
  ).
          
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-entity-code V-table-Win 
PROCEDURE verify-entity-code :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR ec AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  CASE (INPUT Project.EntityType):
    WHEN "L" THEN DO:
      FIND Company WHERE Company.CompanyCode = INPUT Project.EntityCode NO-LOCK NO-ERROR.
      IF AVAILABLE(Company) THEN RETURN "".
    END.
    WHEN "P" THEN DO:
      FIND Property WHERE Property.PropertyCode = INPUT Project.EntityCode NO-LOCK NO-ERROR.
      IF AVAILABLE(Property) THEN RETURN "".
    END.
    WHEN "J" THEN DO:
    DEF BUFFER MyProject FOR Project.
      FIND MyProject WHERE MyProject.ProjectCode = INPUT Project.EntityCode NO-LOCK NO-ERROR.
      IF AVAILABLE(MyProject) THEN RETURN "".
    END.
  END.

  MESSAGE INPUT Project.EntityType INPUT Project.EntityCode "not on file."
          VIEW-AS ALERT-BOX ERROR TITLE "Invalid Entity Code".

  APPLY 'ENTRY':U TO Project.EntityType.
  RETURN "FAIL".

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-entity-type V-table-Win 
PROCEDURE verify-entity-type :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF LOOKUP( INPUT Project.EntityType, "P,L,J" ) = 0 THEN
  DO:
    MESSAGE "The Entity Type must be 'P' or 'L' or 'J'"
      VIEW-AS ALERT-BOX ERROR TITLE "Invalid Entity Type".
    Project.EntityType:SCREEN-VALUE = IF Project.EntityType:PRIVATE-DATA <> ?
      THEN Project.EntityType:PRIVATE-DATA
      ELSE Project.EntityType.
    RETURN "FAIL".
  END.
  ELSE Project.EntityType:PRIVATE-DATA = INPUT Project.EntityType.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-project V-table-Win 
PROCEDURE verify-project :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN verify-project-code.
  IF RETURN-VALUE = "FAIL" THEN RETURN RETURN-VALUE.

  IF INPUT Project.Name = "" THEN DO:
    MESSAGE "You must enter the Project Name."
            VIEW-AS ALERT-BOX ERROR TITLE "No Name Entered".
    APPLY 'ENTRY':U TO Project.Name.
    RETURN "FAIL".
  END.
  
  IF NOT CAN-FIND( FIRST Contact WHERE
    ROWID( Contact ) = TO-ROWID( fil_Proposer:PRIVATE-DATA ) ) THEN
  DO:
    MESSAGE "You must choose a proposer for this project!"
            VIEW-AS ALERT-BOX ERROR TITLE "No Proposer selected".
    APPLY 'ENTRY':U TO Project.Description.
    APPLY 'TAB':U TO Project.Description.
    RETURN "FAIL".
  END.

  RUN verify-entity-type.
  IF RETURN-VALUE = "FAIL" THEN RETURN RETURN-VALUE.

  RUN verify-entity-code.
  IF RETURN-VALUE = "FAIL" THEN RETURN RETURN-VALUE.

  IF INPUT Project.EntityType = "J" AND
     INPUT Project.EntityCode = INPUT Project.ProjectCode THEN
  DO:
    MESSAGE "A project cannot be associated with itself!" SKIP
            "You must change the entity code."
            VIEW-AS ALERT-BOX ERROR TITLE "Invalid Entity".
    APPLY 'ENTRY':U TO Project.EntityCode.
    RETURN "FAIL".
  END.

  IF INPUT Project.EntityType = "J" AND
    CAN-FIND( Project WHERE
      Project.ProjectCode = INPUT Project.EntityCode AND
      Project.ExpenditureType = "C" ) THEN DO:
    MESSAGE "The parent project may not be a property capex!"
            VIEW-AS ALERT-BOX ERROR TITLE "Property CapEx as Parent".
    APPLY 'ENTRY':U TO Project.EntityCode.
    RETURN "FAIL".
  END.

  IF INPUT Project.EntityType = "J" AND invalid-hierarchy( Project.ProjectCode ) THEN DO:
    MESSAGE "There is a circular link in the project hierarchy!"
            VIEW-AS ALERT-BOX ERROR TITLE "Attempt to set Child as Parent".
    APPLY 'ENTRY':U TO Project.EntityCode.
    RETURN "FAIL".
  END.

  IF INPUT Project.StartDate = ? THEN DO:
    MESSAGE "You must enter a Start Date for this Project"
            VIEW-AS ALERT-BOX ERROR TITLE "No start date entered".
    APPLY 'ENTRY':U TO Project.StartDate.
    RETURN "FAIL".
  END.

  IF INPUT Project.FirstApprover = "" AND
     INPUT Project.SecondApprover = "" THEN DO:
    MESSAGE "You need to enter at least one approver" SKIP
            "for this Project."
            VIEW-AS ALERT-BOX ERROR TITLE "No Approver Entered.".
    APPLY 'ENTRY':U TO Project.FirstApprover.
    RETURN "FAIL".
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-project-code V-table-Win 
PROCEDURE verify-project-code :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF mode = "Maintain" THEN RETURN. /* Because you cannot maintain this field */
  
  /* Verify that the given number is in a valid block for this Office */
  IF TRIM( ENTRY( 1, INPUT Project.ExpenditureType, "-" ) ) = "G" THEN
  DO: /* Type = "G" */
    IF INPUT Project.ProjectCode >= 10000 THEN
    DO:
      MESSAGE
        "General Expenditure Projects must have" SKIP
        "project codes less than 10000"
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid Project Code".
      APPLY 'ENTRY':U TO Project.ProjectCode.
      RETURN "FAIL".
    END.
  END.
  ELSE IF CAN-FIND( FIRST OfficeSetting OF Office WHERE OfficeSetting.SetName BEGINS "CAPEX-RANGE") THEN DO:
    DEF VAR beg-range LIKE Project.ProjectCode NO-UNDO.
    DEF VAR end-range LIKE Project.ProjectCode NO-UNDO.
    DEF VAR in-a-range AS LOGI INIT No.
    
    FOR EACH Officesettings OF Office WHERE
      OfficeSettings.SetName BEGINS "CAPEX-RANGE" NO-LOCK:

      beg-range = INT( TRIM( ENTRY( 1, OfficeSettings.SetValue, "-" ) ) ).
      end-range = INT( TRIM( ENTRY( 2, OfficeSettings.SetValue, "-" ) ) ).
      in-a-range = in-a-range OR
        ( INPUT Project.ProjectCode >= beg-range AND
          INPUT Project.ProjectCode <= end-range ).
      
    END.

    IF NOT in-a-range THEN
    DO:
      MESSAGE
        "The given project number has not been allocated to" SKIP
        "this office." SKIP
        VIEW-AS ALERT-BOX WARNING TITLE "Project Number for Different Office".
    END.  
  
  END.
    
  IF CAN-FIND( FIRST Project WHERE Project.ProjectCode = INPUT Project.ProjectCode ) THEN
  DO:
    MESSAGE
      "A project already exists with code " + Project.ProjectCode:SCREEN-VALUE + "." SKIP
      "If you have not changed the code from the default" SKIP
      "then another user has created a project with the"  SKIP
      "same code."
/*       SKIP(1)
      "Do you want to the system to automatically assign" SKIP
      "a unique code?" */
      VIEW-AS ALERT-BOX QUESTION TITLE "Duplicate Project"
      UPDATE assign-it AS LOGI.
    IF assign-it
      THEN RUN expenditure-type-changed.
      ELSE APPLY 'ENTRY':U TO Project.ProjectCode.
    RETURN "FAIL".
  END.    

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-parent-project V-table-Win 
FUNCTION get-parent-project RETURNS INTEGER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR record-source-hdl AS HANDLE NO-UNDO.
DEF VAR lnk-hdl AS CHAR NO-UNDO.
DEF VAR parent-project LIKE Project.ProjectCode NO-UNDO INITIAL ?.
  
  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT lnk-hdl ).
  ASSIGN record-source-hdl = WIDGET-HANDLE( lnk-hdl ) NO-ERROR.
  IF VALID-HANDLE(record-source-hdl) THEN DO:
    RUN get-parent-project IN record-source-hdl ( OUTPUT parent-project ).
  END.

  RETURN parent-project.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION invalid-hierarchy V-table-Win 
FUNCTION invalid-hierarchy RETURNS LOGICAL
  ( INPUT try-project AS INTEGER ) :
/*------------------------------------------------------------------------------
  Purpose:  Check that the project hierarchy is not circular
    Notes:  If the hierarchy is totally screwed (referred projects not present)
            then the function also returns TRUE, i.e.invalid.
------------------------------------------------------------------------------*/
DEF BUFFER ThisProject FOR Project.
DEF VAR parent-code LIKE Project.ProjectCode NO-UNDO.

  FIND ThisProject WHERE ThisProject.ProjectCode = try-project NO-LOCK NO-ERROR.

  DO WHILE AVAILABLE(ThisProject) AND ThisProject.EntityType = "J" AND ThisProject.EntityCode <> try-project:
    parent-code = ThisProject.EntityCode.
    FIND ThisProject WHERE ThisProject.ProjectCode = parent-code NO-LOCK NO-ERROR.
  END.

  RETURN (NOT AVAILABLE(ThisProject))
         OR ((ThisProject.EntityType = "J") AND (ThisProject.EntityCode = try-project)).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION set-proposer-from-abbr V-table-Win 
FUNCTION set-proposer-from-abbr RETURNS LOGICAL
  ( INPUT new-proposer-abbr AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  FIND Approver WHERE Approver.ApproverCode = new-proposer-abbr NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Approver) THEN DO:
    MESSAGE "Approver '" + new-proposer-abbr + "' not found"
            VIEW-AS ALERT-BOX ERROR
            TITLE "Invalid Approver Code".
    RETURN FALSE.
  END.
  FIND FIRST Contact WHERE Contact.PersonCode = Approver.PersonCode NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Contact) THEN DO:
    MESSAGE "Contact '" + STRING(Approver.PersonCode) + "' not found"
            VIEW-AS ALERT-BOX ERROR
            TITLE "Invalid Approver Record".
    RETURN FALSE.
  END.

DO WITH FRAME {&FRAME-NAME}:
  fil_Proposer:PRIVATE-DATA = STRING(ROWID(Contact)).
  APPLY 'U2':U TO fil_Proposer.
END.

  RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

