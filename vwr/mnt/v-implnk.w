&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "Andrew" NO-UNDO.

&GLOB REPORT-ID "implnk"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char5 RP.Char6 RP.Char1 RP.Char2 RP.Log2 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define ENABLED-OBJECTS Btn_Browse Btn_OK RECT-1 
&Scoped-Define DISPLAYED-FIELDS RP.Char5 RP.Char6 RP.Char1 RP.Char2 RP.Log2 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Browse 
     LABEL "Browse" 
     SIZE 6.29 BY 1.05.

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 12 BY 1.2
     BGCOLOR 8 .

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 70.29 BY 13.8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char5 AT ROW 1.2 COL 1.57 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Apply changes from update file", "zip":U,
"Restart incomplete update", "restart":U,
"Select system components to update", ""
          SIZE 27.43 BY 2.4
          FONT 10
     RP.Char6 AT ROW 1.2 COL 27 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 35.43 BY 1
     Btn_Browse AT ROW 1.2 COL 64.43
     RP.Char1 AT ROW 4.4 COL 1.57 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Don't Compile Anything", "",
"Compile All Programs", "compile-all":U,
"Compile All Uncompiled Programs", "compile-rest":U,
"Compile Important Programs", "compile-important":U,
"Compile Specified Programs", "compile-list":U
          SIZE 25.72 BY 4
          FONT 10
     RP.Char2 AT ROW 4.6 COL 28.43 NO-LABEL
          VIEW-AS EDITOR NO-WORD-WRAP SCROLLBAR-HORIZONTAL SCROLLBAR-VERTICAL
          SIZE 42.29 BY 8.6
          FONT 10
     RP.Log2 AT ROW 11.6 COL 2.14
          LABEL "Rebuild Menus"
          VIEW-AS TOGGLE-BOX
          SIZE 13.72 BY 1
          FONT 10
     Btn_OK AT ROW 13.4 COL 58.72
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 19.9
         WIDTH              = 77.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR EDITOR RP.Char2 IN FRAME F-Main
   EXP-LABEL                                                            */
ASSIGN 
       RP.Char2:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR FILL-IN RP.Char6 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Browse V-table-Win
ON CHOOSE OF Btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN choose-filename.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  DISABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char5 V-table-Win
ON VALUE-CHANGED OF RP.Char5 IN FRAME F-Main /* Char5 */
DO:
  RUN base-select-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE base-select-changed V-table-Win 
PROCEDURE base-select-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Char5 = 'zip' THEN DO:
    ENABLE RP.Char6 btn_Browse.
    DISABLE ALL EXCEPT RP.Char5 RP.Char6 Btn_Browse Btn_OK.
  END.
  ELSE IF INPUT RP.Char5 = "restart" THEN DO:
    DISABLE ALL EXCEPT RP.Char5 Btn_OK.
  END.
  ELSE DO:
    ENABLE RP.Log2 .
    IF INPUT RP.Char1 = "compile-list" THEN DO:
      ENABLE RP.Char1 RP.Char2 .
      RP.Char2:READ-ONLY = No.
    END.
    ELSE DO:
      ENABLE RP.Char1 .
      DISABLE RP.Char2 .
      RP.Char2:READ-ONLY = Yes.
    END.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-filename V-table-Win 
PROCEDURE choose-filename :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR file-chosen AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR temp-filename AS CHAR NO-UNDO FORMAT "X(200)".
DEF VAR directory-string AS CHAR NO-UNDO.

  temp-filename = INPUT FRAME {&FRAME-NAME} RP.Char1 .
  directory-string = SUBSTRING( temp-filename, 1, R-INDEX( temp-filename, "\") ).
  SYSTEM-DIALOG GET-FILE temp-filename
      FILTERS "Update Zip files" "UPDATE*.ZIP", "All files" "*.*"
      INITIAL-FILTER 1
      DEFAULT-EXTENSION "*.TXT"
      INITIAL-DIR directory-string MUST-EXIST
      RETURN-TO-START-DIR
      TITLE "Select System Update File" USE-FILENAME
      UPDATE file-chosen.

  IF file-chosen THEN DO:
    RP.Char6:SCREEN-VALUE = temp-filename.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose: Any special mucking around enabling fields.
------------------------------------------------------------------------------*/
  RUN base-select-changed.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/

  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  FIND RP WHERE RP.ReportID = {&REPORT-ID}
                        AND RP.UserName = user-name
                        NO-LOCK NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Char1 = ""
      RP.Char2 = ""
      RP.Log1 = No
      RP.Log2 = No .
  END.

  RUN dispatch ( 'display-fields':U ).

  FIND UsrGroupMember WHERE UsrGroupMember.UserName = user-name
                        AND UsrGroupMember.GroupName = "Programmer" NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(UsrGroupMember) THEN
    RP.Char5:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "zip".

  RUN dispatch ( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program.
------------------------------------------------------------------------------*/
DEF VAR process-options AS CHAR NO-UNDO.
DEF VAR cmpl AS CHAR NO-UNDO INITIAL "".
DEF VAR plst AS CHAR NO-UNDO INITIAL "".

  RUN dispatch IN THIS-PROCEDURE ('update-record':U).
  IF RP.Char5 = "zip" THEN DO:
    process-options = "UpdateFile," + RP.Char6.
  END.
  ELSE IF RP.Char5 = "restart" THEN DO:
    process-options = "Restart".
  END.
  ELSE DO:
    IF RP.Char1 BEGINS "compile" THEN cmpl = ENTRY(2, RP.Char1, "-").
    IF cmpl = "important" THEN ASSIGN
      cmpl = "List"
      plst =  "vwr/sel/*.w~n"            + "win/*.w~n"
            + "trigger/*.p~n"            + "rplctn/*~n"
            + "lnk/*~n"                  + "sec/*~n"
            + "panel/*~n"
            + "vwr/mnt/v-ivoice.w~n"     + "vwr/mnt/v-vouchr.w~n"
            + "vwr/mnt/v-proper.w~n"     + "vwr/mnt/v-crdtor.w~n"
            + "vwr/mnt/v-tenant.w~n"
            + "vwr/drl/b-act*.w~n"       + "vwr/drl/b-choact.w~n"
            + "vwr/drl/b-crdtor.w~n"     + "vwr/drl/b-proper.w~n"
            + "vwr/drl/b-vouchr.w~n"     + "vwr/drl/b-projct.w~n"
            + "vwr/drl/b-tenant.w".
    ELSE IF cmpl = "list" THEN
      plst = RP.Char2.

    process-options = (IF RP.Log2 THEN "UpdateMenus~n" ELSE "")
                    + (IF cmpl <> "" THEN "Compile," + cmpl + "~n" ELSE "")
                    + (IF plst <> "" THEN "ProgramList~n" + plst ELSE "").


    IF TRIM( ENTRY( 1, process-options, "~n") ) = "" THEN DO:
      MESSAGE "Select something to do or close the window!"
                VIEW-AS ALERT-BOX INFORMATION TITLE "Nothing to do!".
      RETURN.
    END.
  END.

  /* The update process controls it's own busy/non-busy status
   * so there is no need for this here, except to ensure it ain't
   * after we return.
   *    RUN notify( 'set-busy,container-source':U ).
   */
  RUN process/linkimpt.p ( process-options ).
  RUN notify( 'set-idle,container-source':U ).

  FIND UsrGroupMember WHERE UsrGroupMember.UserName = user-name
                        AND UsrGroupMember.GroupName = "Programmer" NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(UsrGroupMember) THEN
    RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

