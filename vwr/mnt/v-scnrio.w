&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tpscnrio.i}

DEF VAR user-code LIKE Person.PersonCode NO-UNDO.
DEF VAR mode        AS CHAR NO-UNDO.
DEF VAR user-name   AS CHAR NO-UNDO.
DEF VAR param-delim AS CHAR NO-UNDO INIT "~~".
DEF VAR param-defs  AS CHAR NO-UNDO.


RUN add-param-def( "Member-Of", "<set1>[,<set2>...]", "A list of names of sets that this scenario participates in." ).
RUN add-param-def( "Forecast-Start", "<date>", "The start of the period being forecast." ).
RUN add-param-def( "Forecast-End", "<date>", "The end of the forecasting period." ).
RUN add-param-def( "Growth", "<percentage>", "The base percentage of annual growth." ).
RUN add-param-def( "Re-Leasing", "Yes/No", "Space released at new rental after lease end." ).
RUN add-param-def( "Re-Lease-Delay" , "<n>", "Number of months before re-leasing." ).
RUN add-param-def( "Re-Lease-Length" , "<n>", "The length in months of each re-leasing." ).
RUN add-param-def( "Monthly-Length", "<n>", "The default length for a monthly lease." ).
RUN add-param-def( "Vacant-Space", "Yes/No", "Include all vacant rental space." ).
RUN add-param-def( "Rent-Reviews", "Yes/No", "Whether or not the forecast should calculate rent reviews." ).
RUN add-param-def( "Review-Months", "<n>", "The number of months between rent reviews." ).
RUN add-param-def( "Exclude-Areas", "<areatype>[,<areatype>...]", "A list of area type codes to be excluded from generation" ).
RUN add-param-def( "Exclude-Contracts", "<servicetype>[,<servicetype>...]", "A list of service type codes to be excluded from generation." ).
RUN add-param-def( "Default-RentAccount", "<account code>", "The account code to accumulate rental when no other is found." ).
RUN add-param-def( "Default-ContractAccount", "<account code>", "The account code for service contracts when no other is found." ).


/* Get the person code of the user who added this record */
{inc/username.i "user-name"}

FIND Usr WHERE Usr.UserName = user-name NO-LOCK NO-ERROR.
user-code = IF AVAILABLE Usr THEN Usr.PersonCode ELSE 0.

DEF WORK-TABLE Params NO-UNDO
  LIKE ScenarioParameter
  
  FIELD Type        AS CHAR
  FIELD Usage       AS CHAR
  FIELD Vals        AS CHAR.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME BROWSE-1

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Scenario
&Scoped-define FIRST-EXTERNAL-TABLE Scenario


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Scenario.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Params

/* Definitions for BROWSE BROWSE-1                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-1 Params.ParameterID Params.Data   
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-1 Params.Data   
&Scoped-define FIELD-PAIRS-IN-QUERY-BROWSE-1~
 ~{&FP1}Data ~{&FP2}Data ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-BROWSE-1 Params
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-BROWSE-1 Params
&Scoped-define SELF-NAME BROWSE-1
&Scoped-define OPEN-QUERY-BROWSE-1 OPEN QUERY {&SELF-NAME} FOR EACH Params.
&Scoped-define TABLES-IN-QUERY-BROWSE-1 Params
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-1 Params


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-BROWSE-1}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Scenario.Name Scenario.Description 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Name ~{&FP2}Name ~{&FP3}
&Scoped-define ENABLED-TABLES Scenario
&Scoped-define FIRST-ENABLED-TABLE Scenario
&Scoped-Define ENABLED-OBJECTS RECT-1 cmb_ScenarioStatus BROWSE-1 ~
fil_parameffect 
&Scoped-Define DISPLAYED-FIELDS Scenario.ScenarioCode Scenario.Name ~
Scenario.Description 
&Scoped-Define DISPLAYED-OBJECTS fil_Person cmb_ScenarioStatus ~
fil_parameffect fil_paramval 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
ScenarioCode|y|y|TTPL.Scenario.ScenarioCode
PersonCode||y|TTPL.Scenario.PersonCode
ScenarioStatus||y|TTPL.Scenario.ScenarioStatus
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "ScenarioCode",
     Keys-Supplied = "ScenarioCode,PersonCode,ScenarioStatus"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_ScenarioStatus AS CHARACTER FORMAT "X(50)" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS " "
     SIZE 29 BY 1
     FONT 10.

DEFINE VARIABLE fil_parameffect AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 47.57 BY 2.6
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_paramval AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 48 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Person AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 57 BY 19.5.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-1 FOR 
      Params SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-1 V-table-Win _FREEFORM
  QUERY BROWSE-1 DISPLAY
      Params.ParameterID COLUMN-LABEL "Parameter"
      Params.Data        COLUMN-LABEL "Value"

ENABLE
      Params.Data
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 55.43 BY 10
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_Person AT ROW 1 COL 9 COLON-ALIGNED NO-LABEL
     Scenario.ScenarioCode AT ROW 1 COL 47 COLON-ALIGNED
          LABEL "Code"
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
          FONT 10
     Scenario.Name AT ROW 2.2 COL 9.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 46 BY 1
          FONT 10
     Scenario.Description AT ROW 3.2 COL 11.29 NO-LABEL
          VIEW-AS EDITOR NO-WORD-WRAP
          SIZE 46 BY 2
          FONT 10
     cmb_ScenarioStatus AT ROW 5.4 COL 11.14 NO-LABEL
     BROWSE-1 AT ROW 6.6 COL 2
     fil_parameffect AT ROW 16.8 COL 9 NO-LABEL
     fil_paramval AT ROW 19.5 COL 7 COLON-ALIGNED NO-LABEL
     "Created By:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 1 COL 2
          FONT 10
     RECT-1 AT ROW 1.5 COL 1
     "Name:" VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 2.2 COL 2.14
          FONT 10
     "Description:" VIEW-AS TEXT
          SIZE 9 BY 1 AT ROW 3.2 COL 2.14
          FONT 10
     "Status:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 5.4 COL 2.14
          FONT 10
     "Effect:" VIEW-AS TEXT
          SIZE 5.29 BY 1 AT ROW 16.8 COL 2
          FONT 10
     "Values:" VIEW-AS TEXT
          SIZE 5.86 BY 1 AT ROW 19.5 COL 2
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Scenario
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 20.05
         WIDTH              = 57.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB BROWSE-1 cmb_ScenarioStatus F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cmb_ScenarioStatus IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR EDITOR Scenario.Description IN FRAME F-Main
   EXP-LABEL                                                            */
ASSIGN 
       fil_parameffect:READ-ONLY IN FRAME F-Main        = TRUE.

/* SETTINGS FOR FILL-IN fil_paramval IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Person IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Scenario.Name IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Scenario.ScenarioCode IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-1
/* Query rebuild information for BROWSE BROWSE-1
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH Params.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE BROWSE-1 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/string.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME BROWSE-1
&Scoped-define SELF-NAME BROWSE-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-1 V-table-Win
ON ROW-DISPLAY OF BROWSE-1 IN FRAME F-Main
DO:
  ASSIGN Params.Data:FORMAT IN BROWSE {&BROWSE-NAME} = "X(1024)" NO-ERROR.
  ASSIGN Params.Data:SCREEN-VALUE = RIGHT-TRIM( Params.Data:SCREEN-VALUE ) NO-ERROR.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-1 V-table-Win
ON VALUE-CHANGED OF BROWSE-1 IN FRAME F-Main
DO:
  RUN parameter-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_ScenarioStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ScenarioStatus V-table-Win
ON U1 OF cmb_ScenarioStatus IN FRAME F-Main
DO:
  {inc/selcmb/scsst1.i "Scenario" "ScenarioStatus"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ScenarioStatus V-table-Win
ON U2 OF cmb_ScenarioStatus IN FRAME F-Main
DO:
  {inc/selcmb/scsst2.i "Scenario" "ScenarioStatus"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Person
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Person V-table-Win
ON U1 OF fil_Person IN FRAME F-Main
DO:
  {inc/selfil/sfpsn1.i "Scenario" "PersonCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-param-def V-table-Win 
PROCEDURE add-param-def :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER param-id    AS CHAR NO-UNDO.
  DEF INPUT PARAMETER param-vals  AS CHAR NO-UNDO.
  DEF INPUT PARAMETER param-usage AS CHAR NO-UNDO.
  
  add-to-list-delim( param-defs, 
    param-id + param-delim + param-vals + param-delim + param-usage ,
    param-delim
  ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'ScenarioCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Scenario
           &WHERE = "WHERE Scenario.ScenarioCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Scenario"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Scenario"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-parameters V-table-Win 
PROCEDURE assign-parameters :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Scenario THEN RETURN.
  
  ASSIGN BROWSE {&BROWSE-NAME} Params.ParameterID Params.Data.
  
  FOR EACH Params:
    FIND ScenarioParameter OF Scenario WHERE
      ScenarioParameter.ParameterID = Params.ParameterID
      EXCLUSIVE-LOCK NO-ERROR.
    
    /* Delete Parameter if necessary */
    IF Params.Data = "" THEN DO:
      IF AVAILABLE(ScenarioParameter) THEN DELETE ScenarioParameter.
    END.
    ELSE DO:
      IF NOT AVAILABLE(ScenarioParameter) THEN DO:
        CREATE ScenarioParameter.
        ASSIGN
          ScenarioParameter.ScenarioCode = Scenario.ScenarioCode
          ScenarioParameter.ParameterID  = Params.ParameterID.
      END.
      ASSIGN ScenarioParameter.Data = Params.Data.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide,CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-scenario. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-scenario.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  RUN dispatch( 'update-record':U  ).
  IF mode = "Add" THEN RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit' ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-scenario V-table-Win 
PROCEDURE delete-scenario :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Scenario EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Scenario THEN DELETE Scenario.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE generate-cash-flows V-table-Win 
PROCEDURE generate-cash-flows :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-scenario.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).
  RUN process/gencshfl.p ( STRING(Scenario.ScenarioCode) + "~nDebug" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN assign-parameters.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN open-parameter-query.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN 
  DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE RUN dispatch( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-parameter-query V-table-Win 
PROCEDURE open-parameter-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Scenario THEN RETURN.
  
  DEF VAR i AS INT NO-UNDO.
  DEF VAR param-id    AS CHAR NO-UNDO.
  DEF VAR param-data  AS CHAR NO-UNDO.
  DEF VAR param-vals  AS CHAR NO-UNDO.
  DEF VAR param-usage AS CHAR NO-UNDO.
  
  FOR EACH Params: DELETE Params. END.
  
  DO i = 1 TO NUM-ENTRIES( param-defs, param-delim ) BY 3:

    param-id    = ENTRY( i, param-defs, param-delim ).
    param-vals  = ENTRY( i + 1, param-defs, param-delim ).
    param-usage = ENTRY( i + 2, param-defs, param-delim ).

    FIND ScenarioParameter NO-LOCK OF Scenario WHERE ScenarioParameter.ParameterID = param-id NO-ERROR.
    param-data = IF AVAILABLE(ScenarioParameter) THEN ScenarioParameter.Data ELSE "".
    
    CREATE Params.
    ASSIGN
      Params.ScenarioCode = Scenario.ScenarioCode
      Params.ParameterID  = param-id
      Params.Data         = param-data
      Params.Vals         = param-vals
      Params.Usage        = param-usage.
    
  END.

  OPEN QUERY {&BROWSE-NAME} FOR EACH Params.
  RUN parameter-changed.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CREATE Scenario.
  
  DEF BUFFER LastScenario FOR Scenario.

  FIND LAST LastScenario NO-LOCK NO-ERROR.
  ASSIGN
    Scenario.ScenarioCode =
      IF AVAILABLE LastScenario THEN LastScenario.ScenarioCode + 1 ELSE 1
    Scenario.PersonCode = user-code.
    
  CURRENT-WINDOW:TITLE = "Adding a new Scenario".
  
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parameter-changed V-table-Win 
PROCEDURE parameter-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  fil_parameffect = IF AVAILABLE Params THEN Params.Usage ELSE "".
  fil_paramval    = IF AVAILABLE Params THEN Params.Vals ELSE "".

  DISPLAY
    fil_parameffect
    fil_paramval
  WITH FRAME {&FRAME-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-entity-selection V-table-Win 
PROCEDURE send-entity-selection :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER entity-selection AS CHAR NO-UNDO.
  
  IF NOT AVAILABLE Scenario THEN RETURN.
  FIND ScenarioParameter OF Scenario WHERE ScenarioParameter.ParameterID = "In-Entities" NO-ERROR.
  IF NOT AVAILABLE(ScenarioParameter) THEN RETURN.

  entity-selection = ScenarioParameter.Data.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "ScenarioCode" "Scenario" "ScenarioCode"}
  {src/adm/template/sndkycas.i "PersonCode" "Scenario" "PersonCode"}
  {src/adm/template/sndkycas.i "ScenarioStatus" "Scenario" "ScenarioStatus"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Scenario"}
  {src/adm/template/snd-list.i "Params"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-selection V-table-Win 
PROCEDURE update-entity-selection :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER entity-selection AS CHAR NO-UNDO.

  IF NOT AVAILABLE Scenario THEN RETURN.
  FIND ScenarioParameter OF Scenario WHERE ScenarioParameter.ParameterID = "In-Entities" NO-ERROR.
  IF NOT AVAILABLE(ScenarioParameter) THEN DO:
    CREATE ScenarioParameter.
    ScenarioParameter.ScenarioCode = Scenario.ScenarioCode.
    ScenarioParameter.ParameterID = "In-Entities".
  END.

  ScenarioParameter.Data = entity-selection.

  IF BROWSE {&BROWSE-NAME}:REFRESH() THEN.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-scenario V-table-Win 
PROCEDURE verify-scenario :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


