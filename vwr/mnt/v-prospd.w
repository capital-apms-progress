&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r11 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tpproper.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Property
&Scoped-define FIRST-EXTERNAL-TABLE Property


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Property.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Property.PurchasedFrom ~
Property.PurchaseAgreement Property.PurchaseSettlement ~
Property.PurchasePrice Property.PurchaseCosts Property.SoldTo ~
Property.SaleAgreement Property.SaleSettlement Property.SalePrice 
&Scoped-define ENABLED-TABLES Property
&Scoped-define FIRST-ENABLED-TABLE Property
&Scoped-Define ENABLED-OBJECTS RECT-15 RECT-16 
&Scoped-Define DISPLAYED-FIELDS Property.PurchasedFrom ~
Property.PurchaseAgreement Property.PurchaseSettlement ~
Property.PurchasePrice Property.PurchaseCosts Property.SoldTo ~
Property.SaleAgreement Property.SaleSettlement Property.SalePrice 
&Scoped-define DISPLAYED-TABLES Property
&Scoped-define FIRST-DISPLAYED-TABLE Property


/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
BuildingType|y|y|TTPL.Property.BuildingType
PropertyCode|y|y|TTPL.Property.PropertyCode
CompanyCode|y|y|TTPL.Property.CompanyCode
NoteCode||y|TTPL.Property.NoteCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "BuildingType,PropertyCode,CompanyCode",
     Keys-Supplied = "BuildingType,PropertyCode,CompanyCode,NoteCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE RECTANGLE RECT-15
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 96 BY 2.8.

DEFINE RECTANGLE RECT-16
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 96 BY 2.8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Property.PurchasedFrom AT ROW 2 COL 5.86
          LABEL "Bought From"
          VIEW-AS FILL-IN 
          SIZE 81.14 BY 1
     Property.PurchaseAgreement AT ROW 3 COL 13.29 COLON-ALIGNED
          LABEL "Date"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     Property.PurchaseSettlement AT ROW 3 COL 37.29 COLON-ALIGNED
          LABEL "Settlement"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     Property.PurchasePrice AT ROW 3 COL 56.72 COLON-ALIGNED
          LABEL "Price" FORMAT "ZZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 15 BY 1
          FONT 11
     Property.PurchaseCosts AT ROW 3 COL 79.57 COLON-ALIGNED
          LABEL "Costs" FORMAT "ZZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14.86 BY 1
          FONT 11
     Property.SoldTo AT ROW 6 COL 13.29 COLON-ALIGNED
          LABEL "Sold To"
          VIEW-AS FILL-IN 
          SIZE 81 BY 1
     Property.SaleAgreement AT ROW 7 COL 13.29 COLON-ALIGNED
          LABEL "Date"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     Property.SaleSettlement AT ROW 7 COL 37.29 COLON-ALIGNED
          LABEL "Settlement"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     Property.SalePrice AT ROW 7 COL 56.72 COLON-ALIGNED
          LABEL "Price" FORMAT "ZZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 15 BY 1
          FONT 11
     RECT-15 AT ROW 1.4 COL 1
     RECT-16 AT ROW 5.4 COL 1
     "Purchase Details" VIEW-AS TEXT
          SIZE 17 BY .6 AT ROW 1 COL 1.86
          FONT 14
     "Sale Details" VIEW-AS TEXT
          SIZE 12 BY .6 AT ROW 5 COL 2.14
          FONT 14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Property
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 11.6
         WIDTH              = 113.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Property.PurchaseAgreement IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Property.PurchaseCosts IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Property.PurchasedFrom IN FRAME F-Main
   ALIGN-L EXP-LABEL                                                    */
/* SETTINGS FOR FILL-IN Property.PurchasePrice IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Property.PurchaseSettlement IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Property.SaleAgreement IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Property.SalePrice IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Property.SaleSettlement IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Property.SoldTo IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'BuildingType':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Property
           &WHERE = "WHERE Property.BuildingType eq key-value"
       }
    WHEN 'PropertyCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Property
           &WHERE = "WHERE Property.PropertyCode eq INTEGER(key-value)"
       }
    WHEN 'CompanyCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Property
           &WHERE = "WHERE Property.CompanyCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Property"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Property"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':u ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':u ).
  RUN dispatch( 'update-record':U ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-update-record V-table-Win 
PROCEDURE local-update-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'update-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  IF Property.PurchasePrice NE ? AND Property.PurchasePrice GT 0.0 THEN DO:
    FIND FIRST Valuation OF Property WHERE ValuationType = 'HCST' NO-ERROR.
    IF NOT AVAILABLE(Valuation) THEN DO:
      CREATE Valuation.
      ASSIGN 
        Valuation.PropertyCode = Property.PropertyCode
        Valuation.ValuationType = 'HCST'.
    END.
    ASSIGN
      Valuation.Amount   = Property.PurchasePrice
      Valuation.DateDone = Property.PurchaseAgreement
      Valuation.Valuer   = "Purchase Price".
  END.

  IF Property.SalePrice NE ? AND Property.SalePrice GT 0.0 THEN DO:
    FIND FIRST Valuation OF Property WHERE ValuationType = 'SALE' NO-ERROR.
    IF NOT AVAILABLE(Valuation) THEN DO:
      CREATE Valuation.
      ASSIGN 
        Valuation.PropertyCode = Property.PropertyCode
        Valuation.ValuationType = 'SALE'.
    END.
    ASSIGN
      Valuation.Amount   = Property.PurchasePrice
      Valuation.DateDone = Property.PurchaseAgreement
      Valuation.Valuer   = "Sale Price".
  END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "BuildingType" "Property" "BuildingType"}
  {src/adm/template/sndkycas.i "PropertyCode" "Property" "PropertyCode"}
  {src/adm/template/sndkycas.i "CompanyCode" "Property" "CompanyCode"}
  {src/adm/template/sndkycas.i "NoteCode" "Property" "NoteCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Property"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

