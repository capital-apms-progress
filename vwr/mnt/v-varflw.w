&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.
DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO.

DEF VAR project-code AS INT NO-UNDO.
DEF VAR variation-code AS INT NO-UNDO.
DEF VAR account-code AS DEC NO-UNDO.
DEF VAR month-code AS INT NO-UNDO.

/* record scoping */
FIND FIRST VariationFlow NO-LOCK NO-ERROR.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Variation Project VariationFlow
&Scoped-define FIRST-EXTERNAL-TABLE Variation


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Variation, Project, VariationFlow.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS VariationFlow.Amount 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Amount ~{&FP2}Amount ~{&FP3}
&Scoped-define ENABLED-TABLES VariationFlow
&Scoped-define FIRST-ENABLED-TABLE VariationFlow
&Scoped-Define ENABLED-OBJECTS RECT-1 cmb_Account cmb_Month 
&Scoped-Define DISPLAYED-FIELDS VariationFlow.ProjectCode Project.Name ~
VariationFlow.VariationCode Variation.Reason VariationFlow.Amount 
&Scoped-Define DISPLAYED-OBJECTS cmb_Account cmb_Month 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
ProjectCode|y|y|ttpl.VariationFlow.ProjectCode
VariationCode|y|y|ttpl.VariationFlow.VariationCode
MonthCode||y|ttpl.VariationFlow.MonthCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "ProjectCode,VariationCode",
     Keys-Supplied = "ProjectCode,VariationCode,MonthCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_Account AS CHARACTER FORMAT "X(256)":U 
     LABEL "Account" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     SIZE 56 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Month AS CHARACTER FORMAT "X(256)":U 
     LABEL "Month" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "Item 1" 
     SIZE 26.86 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 64.57 BY 5.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     VariationFlow.ProjectCode AT ROW 1.2 COL 7 COLON-ALIGNED
          LABEL "Project"
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
     Project.Name AT ROW 1.2 COL 17.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 45.72 BY 1.05
     VariationFlow.VariationCode AT ROW 2.2 COL 7 COLON-ALIGNED
          LABEL "Variation"
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
     Variation.Reason AT ROW 2.2 COL 17.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 45.72 BY 1.05
     cmb_Account AT ROW 4.2 COL 7 COLON-ALIGNED
     cmb_Month AT ROW 5.4 COL 7 COLON-ALIGNED
     VariationFlow.Amount AT ROW 5.4 COL 51 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 12 BY 1.05
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.Variation,ttpl.Project,ttpl.VariationFlow
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 14.95
         WIDTH              = 68.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Project.Name IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN VariationFlow.ProjectCode IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN Variation.Reason IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN VariationFlow.VariationCode IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME cmb_Account
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Account V-table-Win
ON U1 OF cmb_Account IN FRAME F-Main /* Account */
DO:
  {inc/selcmb/scpbdg1.i "VariationFlow" "AccountCode" "INPUT VariationFlow.ProjectCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Account V-table-Win
ON U2 OF cmb_Account IN FRAME F-Main /* Account */
DO:
  {inc/selcmb/scpbdg2.i "VariationFlow" "AccountCode" "INPUT VariationFlow.ProjectCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Month
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month V-table-Win
ON U1 OF cmb_Month IN FRAME F-Main /* Month */
DO:
  {inc/selcmb/scmths1.i "VariationFlow" "MonthCode"}

  /* plus we try and find a near-current month when adding */
  IF mode = "Add" AND month-code <> 0 THEN DO WITH FRAME {&FRAME-NAME}:
    FIND Month WHERE Month.MonthCode = month-code NO-LOCK NO-ERROR.
    IF AVAILABLE(Month) THEN
      {&SELF-NAME}:SCREEN-VALUE = STRING( Month.StartDate, "99/99/9999").
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month V-table-Win
ON U2 OF cmb_Month IN FRAME F-Main /* Month */
DO:
  {inc/selcmb/scmths2.i "VariationFlow" "MonthCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'ProjectCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = VariationFlow
           &WHERE = "WHERE VariationFlow.ProjectCode eq INTEGER(key-value)"
       }
    WHEN 'VariationCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = VariationFlow
           &WHERE = "WHERE VariationFlow.VariationCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Variation"}
  {src/adm/template/row-list.i "Project"}
  {src/adm/template/row-list.i "VariationFlow"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Variation"}
  {src/adm/template/row-find.i "Project"}
  {src/adm/template/row-find.i "VariationFlow"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF mode = "Add" THEN RUN delete-flow.
  RUN dispatch( 'exit':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN verify-flow.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  RUN dispatch( 'update-record':U ).
  IF mode = "Add" THEN RUN notify( 'open-query,record-source':U ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-flow V-table-Win 
PROCEDURE delete-flow :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(VariationFlow) THEN RETURN.
  FIND CURRENT VariationFlow EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE(VariationFlow) THEN RETURN.
  DELETE VariationFlow NO-ERROR.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF mode = "Add" THEN DO:
    ENABLE cmb_Account cmb_Month.
  END.
  ELSE DO:
    DISABLE cmb_Account cmb_Month.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF mode = "Add" THEN RUN dispatch( 'add-record':U ).

  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE normal-send-key V-table-Win adm/support/_key-snd.p
PROCEDURE normal-send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "ProjectCode" "VariationFlow" "ProjectCode"}
  {src/adm/template/sndkycas.i "VariationCode" "VariationFlow" "VariationCode"}
  {src/adm/template/sndkycas.i "MonthCode" "VariationFlow" "MonthCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR rs-hdl AS HANDLE NO-UNDO.
DEF VAR new-key AS CHAR NO-UNDO.

  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT new-key ).
  rs-hdl = WIDGET-HANDLE(new-key).
  IF VALID-HANDLE( rs-hdl ) THEN DO:
    RUN send-key IN rs-hdl( "VariationFlow", OUTPUT new-key ).
    IF new-key <> ? AND TRIM(new-key) <> "" THEN DO:
      RUN set-attribute-list( 'Key-Name = VariationFlow, Key-Value = ':U + new-key).
    END.
    IF project-code = 0 OR variation-code = 0 THEN DO:
      RUN send-key IN rs-hdl( "ProjectCode", OUTPUT new-key ).
      project-code = INT(new-key).
      FIND Project WHERE Project.ProjectCode = project-code NO-LOCK NO-ERROR.
      RUN send-key IN rs-hdl( "VariationCode", OUTPUT new-key ).
      variation-code = INT(new-key).
    END.
  END.

  have-records = Yes.

  FIND FIRST VariationFlow EXCLUSIVE-LOCK WHERE VariationFlow.ProjectCode = project-code
                        AND VariationFlow.VariationCode = variation-code
                        AND VariationFlow.AccountCode = 0
                        AND VariationFlow.MonthCode = 0  NO-ERROR.
  IF NOT AVAILABLE(VariationFlow) THEN CREATE VariationFlow.

  ASSIGN VariationFlow.ProjectCode = project-code
         VariationFlow.VariationCode = variation-code
         VariationFlow.AccountCode = account-code
         VariationFlow.MonthCode = 0
         VariationFlow.Amount = 0.

  FIND CURRENT VariationFlow NO-LOCK.

  FIND Variation OF VariationFlow NO-LOCK.
  DEF BUFFER VFL FOR VariationFlow.
  FOR EACH VFL OF Variation WHERE RECID(VFL) <> RECID(VariationFlow) EXCLUSIVE-LOCK:
    IF VFL.MonthCode = 0 OR VFL.AccountCode = 0 THEN DELETE VFL.
  END.
  RUN dispatch IN THIS-PROCEDURE ('display-fields':U).


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {inc/sendkey.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Variation"}
  {src/adm/template/snd-list.i "Project"}
  {src/adm/template/snd-list.i "VariationFlow"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name V-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-name AS CHAR NO-UNDO.
  key-name = new-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value V-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key AS CHAR NO-UNDO.

  IF key-name = "VariationFlow" AND TRIM(new-key) <> "" THEN DO:
    project-code = INT( ENTRY( 1, new-key, "/" ) ).
    variation-code = INT( ENTRY( 2, new-key, "/" ) ).
    account-code = DEC( ENTRY( 3, new-key, "/" ) ).
    month-code = INT( ENTRY( 4, new-key, "/" ) ).
    FIND VariationFlow NO-LOCK WHERE VariationFlow.ProjectCode = project-code
                    AND VariationFlow.VariationCode = variation-code
                    AND VariationFlow.AccountCode = account-code
                    AND VariationFlow.MonthCode = month-code NO-ERROR.
  END.
  ELSE IF key-name = "Variation" AND TRIM(new-key) <> "" THEN DO:
    project-code = INT( ENTRY( 1, new-key ) ).
    variation-code = INT( ENTRY( 2, new-key ) ).
  END.
  FIND Variation NO-LOCK WHERE Variation.ProjectCode = project-code
                    AND Variation.VariationCode = variation-code NO-ERROR.
  IF AVAILABLE(Variation) THEN
    FIND Project OF Variation NO-LOCK NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-flow V-table-Win 
PROCEDURE verify-flow :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER VFL FOR VariationFlow.

DEF VAR in-month AS INT NO-UNDO.
DEF VAR in-account AS DEC NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  FIND Month WHERE Month.StartDate = DATE( cmb_Month:SCREEN-VALUE ) NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Month) THEN RETURN "FAIL".
  in-month = Month.MonthCode.

  in-account = DEC( ENTRY( 1, cmb_Account:SCREEN-VALUE, " ") ).
  FIND VFL NO-LOCK WHERE VFL.ProjectCode = VariationFlow.ProjectCode
                    AND VFL.VariationCode = VariationFlow.VariationCode
                    AND VFL.AccountCode = in-account
                    AND VFL.MonthCode = in-month
                    NO-ERROR.
  IF mode = "Add" AND AVAILABLE(VFL) THEN DO:
    MESSAGE "Flow already exists for account " + STRING( in-account, "9999.99") "for that month."
            VIEW-AS ALERT-BOX ERROR TITLE "Flow already exists - use Maintain".
    RETURN "FAIL".
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


