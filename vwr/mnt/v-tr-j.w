&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "Andrew" NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Int1 RP.Int2 RP.Char4 RP.Dec1 ~
RP.Dec2 RP.Log9 RP.Char3 RP.Log5 RP.Char2 RP.Log2 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}~
 ~{&FP1}Dec1 ~{&FP2}Dec1 ~{&FP3}~
 ~{&FP1}Dec2 ~{&FP2}Dec2 ~{&FP3}~
 ~{&FP1}Char2 ~{&FP2}Char2 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-5 RECT-4 RECT-6 cb-month1 ~
cb-month2 btn_Browse Btn_OK 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Int1 RP.Int2 RP.Char4 RP.Dec1 ~
RP.Dec2 RP.Log9 RP.Char3 RP.Log5 RP.Char2 RP.Log2 
&Scoped-Define DISPLAYED-OBJECTS fil_proj1 fil_proj2 fil_account1 ~
fil_account2 cb-month1 cb-month2 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Browse 
     LABEL "Browse" 
     SIZE 6.29 BY 1.05
     FONT 10.

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "&OK" 
     SIZE 12 BY 1.2
     BGCOLOR 8 .

DEFINE VARIABLE cb-month1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "10/07/1886" 
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE cb-month2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_account1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51.29 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_account2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51.29 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_proj1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51.29 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_proj2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51.29 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 74.86 BY 15.6.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 73.72 BY 3.45.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 73.72 BY 3.55.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 73.72 BY 1.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.45 COL 2.14 NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL EXPAND 
          RADIO-BUTTONS 
                    "Single project", "1":U,
"Project range", "R":U,
"Project hierarchy", "H":U
          SIZE 48 BY 1
          FONT 10
     RP.Int1 AT ROW 2.55 COL 9.29 COLON-ALIGNED
          LABEL "Project" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_proj1 AT ROW 2.55 COL 21.43 COLON-ALIGNED NO-LABEL
     RP.Int2 AT ROW 3.55 COL 9.29 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_proj2 AT ROW 3.55 COL 21.43 COLON-ALIGNED NO-LABEL
     RP.Char4 AT ROW 5.45 COL 2.14 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL EXPAND 
          RADIO-BUTTONS 
                    "All accounts", "*":U,
"Single account", "1":U,
"Account range", "R":U
          SIZE 48 BY 1
          FONT 10
     RP.Dec1 AT ROW 6.55 COL 9.29 COLON-ALIGNED HELP
          ""
          LABEL "Account" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 8 BY 1
     fil_account1 AT ROW 6.55 COL 21.43 COLON-ALIGNED NO-LABEL
     RP.Dec2 AT ROW 7.55 COL 9.29 COLON-ALIGNED HELP
          ""
          LABEL "to" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 8 BY 1
     fil_account2 AT ROW 7.55 COL 21.43 COLON-ALIGNED NO-LABEL
     cb-month1 AT ROW 9.35 COL 9.29 COLON-ALIGNED
     cb-month2 AT ROW 9.35 COL 36.14 COLON-ALIGNED
     RP.Log9 AT ROW 11 COL 11.29 HELP
          ""
          LABEL "Hide related records"
          VIEW-AS TOGGLE-BOX
          SIZE 17.14 BY .8
     RP.Char3 AT ROW 12.2 COL 11.29 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "by Project then by Account", "J":U,
"by Account then by Project", "A":U
          SIZE 21.72 BY 1.6
          FONT 10
     RP.Log5 AT ROW 13.95 COL 11.29
          LABEL "Export to"
          VIEW-AS TOGGLE-BOX
          SIZE 9.14 BY 1
          FONT 10
     RP.Char2 AT ROW 13.95 COL 18.43 COLON-ALIGNED NO-LABEL FORMAT "X(120)"
          VIEW-AS FILL-IN 
          SIZE 48.57 BY 1
          FONT 10
     btn_Browse AT ROW 13.95 COL 69
     RP.Log2 AT ROW 15.2 COL 11.29 HELP
          ""
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 11.72 BY .8
          FONT 10
     Btn_OK AT ROW 15.2 COL 63.29
     RECT-1 AT ROW 1 COL 1
     RECT-5 AT ROW 1.2 COL 1.57
     RECT-4 AT ROW 5.2 COL 1.57
     RECT-6 AT ROW 9 COL 1.57
     "Sequence" VIEW-AS TEXT
          SIZE 7.43 BY .8 AT ROW 12.2 COL 3.29
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 17.45
         WIDTH              = 78.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR RADIO-SET RP.Char3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR RADIO-SET RP.Char4 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Dec1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Dec2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_account1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_account2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_proj1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_proj2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log9 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Browse V-table-Win
ON CHOOSE OF btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  DISABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char4 V-table-Win
ON VALUE-CHANGED OF RP.Char4 IN FRAME F-Main /* Char4 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Dec1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Dec1 V-table-Win
ON LEAVE OF RP.Dec1 IN FRAME F-Main /* Account */
DO:
DO WITH FRAME {&FRAME-NAME}:
  FIND ProjectBudget WHERE ProjectBudget.ProjectCode = INPUT RP.Int1
                        AND ProjectBudget.AccountCode = INPUT RP.Dec1
                        NO-LOCK NO-ERROR.
  IF AVAILABLE(ProjectBudget) THEN
    fil_account1:SCREEN-VALUE = ProjectBudget.Description.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Dec2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Dec2 V-table-Win
ON LEAVE OF RP.Dec2 IN FRAME F-Main /* to */
DO:
DO WITH FRAME {&FRAME-NAME}:
  FIND ProjectBudget WHERE ProjectBudget.ProjectCode = INPUT RP.Int1
                        AND ProjectBudget.AccountCode = INPUT RP.Dec2
                        NO-LOCK NO-ERROR.
  IF AVAILABLE(ProjectBudget) THEN
    fil_account2:SCREEN-VALUE = ProjectBudget.Description.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_proj1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_proj1 V-table-Win
ON U1 OF fil_proj1 IN FRAME F-Main
DO:
  {inc/selfil/sfprj1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_proj1 V-table-Win
ON U2 OF fil_proj1 IN FRAME F-Main
DO:
  {inc/selfil/sfprj2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_proj1 V-table-Win
ON U3 OF fil_proj1 IN FRAME F-Main
DO:
  {inc/selfil/sfprj3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_proj2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_proj2 V-table-Win
ON U1 OF fil_proj2 IN FRAME F-Main
DO:
  {inc/selfil/sfprj1.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_proj2 V-table-Win
ON U2 OF fil_proj2 IN FRAME F-Main
DO:
  {inc/selfil/sfprj2.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_proj2 V-table-Win
ON U3 OF fil_proj2 IN FRAME F-Main
DO:
  {inc/selfil/sfprj3.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Project */
DO:
  {inc/selcde/cdprj.i "fil_proj1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* To */
DO:
  {inc/selcde/cdprj.i "fil_proj2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log5 V-table-Win
ON VALUE-CHANGED OF RP.Log5 IN FRAME F-Main /* Export to */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
  RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF         

/************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose: As its name!
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:
    CASE INPUT RP.Char1:   /* Project: single, hierarchy or range */
      WHEN "1" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_proj2:HANDLE), 'hidden = yes' ).
        HIDE RP.Int2 fil_proj2 .
      END.
      WHEN "H" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_proj2:HANDLE), 'hidden = yes' ).
        HIDE RP.Int2 fil_proj2 .
      END.
      WHEN "R" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_proj2:HANDLE), 'hidden = no' ).
        VIEW RP.Int2 fil_proj2 .
      END.
    END CASE.

    CASE INPUT RP.Char4:    /* Account: All, Single, range*/
      WHEN "*" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account1:HANDLE), 'hidden = yes' ).
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account2:HANDLE), 'hidden = yes' ).
        HIDE RP.Dec1 fil_account1 RP.Dec2 fil_account2 .
      END.
      WHEN "1" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account1:HANDLE), 'hidden = no' ).
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account2:HANDLE), 'hidden = yes' ).
        VIEW RP.Dec1 fil_account1 .
        HIDE RP.Dec2 fil_account2 .
      END.
      WHEN "R" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account1:HANDLE), 'hidden = no' ).
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account2:HANDLE), 'hidden = no' ).
        VIEW RP.Dec1 fil_account1 RP.Dec2 fil_account2 .
      END.
    END CASE.

    /* Export to... */
    IF INPUT RP.Log5 THEN
      VIEW RP.Char2 btn_Browse .
    ELSE
      HIDE RP.Char2 btn_Browse .

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/
DEF VAR month1-list AS CHAR INITIAL "" NO-UNDO.
DEF VAR month2-list AS CHAR INITIAL "" NO-UNDO.
DEF VAR delim AS CHAR INITIAL "|" NO-UNDO.

  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  FIND RP WHERE RP.ReportID = "tr-j"
                        AND RP.UserName = user-name
                        NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = "tr-j"
      RP.UserName = user-name
    .
  END.

  /* Initialise the month list for the combos */
  FOR EACH Month NO-LOCK:
    month1-list = month1-list + delim + STRING( Month.StartDate, "99/99/9999" ).
    month2-list = month2-list + delim + STRING( Month.EndDate, "99/99/9999" ).
  END.
  month1-list = SUBSTRING( month1-list, 2).   /* trim initial delimiter */
  month2-list = SUBSTRING( month2-list, 2).   /* trim initial delimiter */
  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN
      cb-month1:DELIMITER = delim
      cb-month2:DELIMITER = delim
      cb-month1:LIST-ITEMS = month1-list
      cb-month2:LIST-ITEMS = month2-list
    .
  END.

  /* Set to initially saved values */
  FIND Month WHERE Month.MonthCode = RP.Int3 NO-LOCK NO-ERROR.
  IF AVAILABLE(Month) THEN cb-month1 = STRING( Month.StartDate, "99/99/9999" ).
  FIND Month WHERE Month.MonthCode = RP.Int4 NO-LOCK NO-ERROR.
  IF AVAILABLE(Month) THEN cb-month2 = STRING( Month.EndDate, "99/99/9999" ).

  DO WITH FRAME {&FRAME-NAME}:
  FIND ProjectBudget WHERE ProjectBudget.ProjectCode = INPUT RP.Int1
                        AND ProjectBudget.AccountCode = INPUT RP.Dec1 NO-LOCK NO-ERROR.
  IF AVAILABLE(ProjectBudget) THEN fil_account1 = ProjectBudget.Description.
  FIND ProjectBudget WHERE ProjectBudget.ProjectCode = INPUT RP.Int1
                        AND ProjectBudget.AccountCode = INPUT RP.Dec2 NO-LOCK NO-ERROR.
  IF AVAILABLE(ProjectBudget) THEN fil_account2 = ProjectBudget.Description.
  END.

  DISPLAY cb-month1 cb-month2 fil_account1 fil_account2 WITH FRAME {&FRAME-NAME} IN WINDOW {&WINDOW-NAME}.

  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  RUN enable-appropriate-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program.
------------------------------------------------------------------------------*/

  DO TRANSACTION:
    RUN dispatch IN THIS-PROCEDURE ('update-record':U).

    FIND CURRENT RP EXCLUSIVE-LOCK.
    FIND Month WHERE Month.StartDate = DATE( cb-month1:SCREEN-VALUE IN FRAME {&FRAME-NAME}) NO-LOCK NO-ERROR.
    IF AVAILABLE(Month) THEN RP.Int3 = Month.MonthCode.

    FIND Month WHERE Month.EndDate = DATE( cb-month2:SCREEN-VALUE IN FRAME {&FRAME-NAME}) NO-LOCK NO-ERROR.
    IF AVAILABLE(Month) THEN RP.Int4 = Month.MonthCode.
    FIND CURRENT RP NO-LOCK.
  END.

DEF VAR report-options AS CHAR NO-UNDO.
DEF VAR e1 AS INTEGER NO-UNDO.
DEF VAR e2 AS INTEGER NO-UNDO.
DEF VAR show-all AS LOGICAL NO-UNDO.
DEF VAR a1 AS DECIMAL NO-UNDO.
DEF VAR a2 AS DECIMAL NO-UNDO.
DEF VAR export-fname AS CHAR INITIAL ? NO-UNDO.


  ASSIGN
    e1 = RP.Int1        e2 = (IF RP.Char1 = "1" OR RP.Char1 = "H" THEN RP.Int1 ELSE RP.Int2 )
    a1 = RP.Dec1        a2 = (IF RP.Char4 = "1" THEN RP.Dec1 ELSE RP.Dec2 )
    export-fname =  (IF RP.Log5 THEN RP.Char2 ELSE "" )
  .
  IF RP.Char4 = "*" THEN ASSIGN     a1 = 0      a2 = 9999.99    .

  report-options = RP.Char1 + "," + RP.Char4 + "," + RP.Char3 + "," 
                 + STRING( e1 ) + "," + STRING( e2 ) + ","
                 + STRING( RP.Int3 ) + "," + STRING( RP.Int4 ) + ","
                 + STRING( a1 ) + "," + STRING( a2 ) + ","
                 + STRING( RP.Log2, "Yes/No" ) + ","
                 + export-fname
                 + (IF RP.Log9 THEN "~nNoRelations" ELSE "").

  RUN notify( 'set-busy, container-source':U ).
  RUN process/report/tr-j.p( report-options ).
  RUN notify( 'set-idle, container-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char2 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated values" "*.CSV"
        SAVE-AS ASK-OVERWRITE CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV"
        INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME UPDATE select-ok.

  IF select-ok THEN DO:
    IF SUBSTRING( file-name, R-INDEX( file-name, ".") ) <> ".CSV" THEN DO:
      IF (LENGTH( file-name ) - R-INDEX( file-name, "." )) < 4 THEN
        file-name = SUBSTRING( file-name, 1, R-INDEX( file-name, ".")) + "CSV" .
    END.
    RP.Char2:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


