&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&SCOPED-DEFINE REPORT-ID "Debtor Statistics"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char3 RP.Int1 RP.Int2 RP.Log1 RP.Log2 ~
RP.Char2 RP.Log3 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}~
 ~{&FP1}Char2 ~{&FP2}Char2 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-29 cmb_ConsolidationList cmb_month ~
btn_Browse btn_print 
&Scoped-Define DISPLAYED-FIELDS RP.Char3 RP.Int1 RP.Int2 RP.Log1 RP.Log2 ~
RP.Char2 RP.Log3 
&Scoped-Define DISPLAYED-OBJECTS cmb_ConsolidationList fil_PropertyFrom ~
fil_PropertyTo cmb_month 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Browse 
     LABEL "Browse" 
     SIZE 9.72 BY 1.

DEFINE BUTTON btn_print DEFAULT 
     LABEL "&OK" 
     SIZE 9.72 BY 1
     FONT 9.

DEFINE VARIABLE cmb_ConsolidationList AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     SIZE 43.43 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_month AS CHARACTER FORMAT "X(256)":U 
     LABEL "Statistics since" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "","" 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fil_PropertyFrom AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_PropertyTo AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49.72 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-29
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 66.29 BY 13.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char3 AT ROW 1.2 COL 5 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "All properties", "AllProperties":U,
"Range of Properties", "PropertyRange":U,
"Company List", "CompanyList":U,
"All Companies", "AllCompanies":U
          SIZE 17.72 BY 3.6
     cmb_ConsolidationList AT ROW 2.8 COL 21.29 COLON-ALIGNED NO-LABEL
     RP.Int1 AT ROW 5 COL 5.86 COLON-ALIGNED
          LABEL "Property" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_PropertyFrom AT ROW 5 COL 15 COLON-ALIGNED NO-LABEL
     RP.Int2 AT ROW 6 COL 5.86 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_PropertyTo AT ROW 6 COL 15 COLON-ALIGNED NO-LABEL
     cmb_month AT ROW 7.8 COL 6.14
     RP.Log1 AT ROW 10.2 COL 4.43
          LABEL "Summary Only"
          VIEW-AS TOGGLE-BOX
          SIZE 13.14 BY .8
          FONT 10
     RP.Log2 AT ROW 11.2 COL 4.43 HELP
          ""
          LABEL "Export"
          VIEW-AS TOGGLE-BOX
          SIZE 7.43 BY 1
     RP.Char2 AT ROW 11.2 COL 9.86 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 44.57 BY 1
     btn_Browse AT ROW 11.2 COL 57
     RP.Log3 AT ROW 12.6 COL 4.43
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 12.29 BY 1.05
     btn_print AT ROW 12.8 COL 57
     RECT-29 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_print.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 15.4
         WIDTH              = 73.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR RADIO-SET RP.Char3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR COMBO-BOX cmb_month IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fil_PropertyFrom IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_PropertyTo IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Browse V-table-Win
ON CHOOSE OF btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_ConsolidationList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ConsolidationList V-table-Win
ON U1 OF cmb_ConsolidationList IN FRAME F-Main
DO:
  {inc/selcmb/sccls1.i "RP" "Char4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ConsolidationList V-table-Win
ON U2 OF cmb_ConsolidationList IN FRAME F-Main
DO:
  {inc/selcmb/sccls2.i "RP" "Char4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month V-table-Win
ON U1 OF cmb_month IN FRAME F-Main /* Statistics since */
DO:
  {inc/selcmb/scmths1.i "RP" "Int3"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month V-table-Win
ON U2 OF cmb_month IN FRAME F-Main /* Statistics since */
DO:
  {inc/selcmb/scmths2.i "RP" "Int3"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_PropertyFrom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyFrom V-table-Win
ON U1 OF fil_PropertyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int1"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyFrom V-table-Win
ON U2 OF fil_PropertyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int1"}
  ASSIGN RP.int2:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING( RP.Int1 ).
  APPLY 'LEAVE':U TO RP.Int2 IN FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyFrom V-table-Win
ON U3 OF fil_PropertyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int1"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_PropertyTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyTo V-table-Win
ON U1 OF fil_PropertyTo IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int2"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyTo V-table-Win
ON U2 OF fil_PropertyTo IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int2"}      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyTo V-table-Win
ON U3 OF fil_PropertyTo IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int2"}      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Property */
DO:
  {inc/selcde/cdpro.i "fil_PropertyFrom"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* To */
DO:
  {inc/selcde/cdpro.i "fil_PropertyTo"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Export */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Log2 THEN
    VIEW RP.Char2 btn_Browse .
  ELSE
    HIDE RP.Char2 btn_Browse .

  IF INPUT RP.Char3 = "PropertyRange" THEN DO:
    HIDE cmb_ConsolidationList .
    VIEW RP.Int1 RP.Int2 fil_PropertyFrom fil_PropertyTo .
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_PropertyFrom:HANDLE), "hidden = No" ).
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_PropertyTo:HANDLE), "hidden = No" ).
  END.
  ELSE IF INPUT RP.Char3 = "CompanyList" THEN DO:
    HIDE RP.Int1 RP.Int2 fil_PropertyFrom fil_PropertyTo .
    VIEW cmb_ConsolidationList .
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_PropertyFrom:HANDLE), "hidden = Yes" ).
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_PropertyTo:HANDLE), "hidden = Yes" ).
  END.
  ELSE DO:
    HIDE RP.Int1 RP.Int2 fil_PropertyFrom fil_PropertyTo cmb_ConsolidationList .
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_PropertyFrom:HANDLE), "hidden = Yes" ).
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_PropertyTo:HANDLE), "hidden = Yes" ).
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN enable-appropriate-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR user-name AS CHAR NO-UNDO.
  
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  
  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.
    
  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
  
    DEF VAR first-property LIKE Property.PropertyCode NO-UNDO INITIAL 0.
    DEF VAR last-property  LIKE Property.PropertyCode NO-UNDO INITIAL 99999.
    
    FIND FIRST Property NO-LOCK NO-ERROR.
    IF AVAILABLE(Property) THEN DO:
      first-property = Property.PropertyCode.
      FIND LAST  Property NO-LOCK.
      last-property  = Property.PropertyCode.
    END.

    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Int1     = first-property
      RP.Int2     = last-property.
  END.

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  RUN dispatch( 'update-record':U ).

  CASE RP.Char3:
    WHEN "PropertyRange" THEN
      report-options = "EntityRange,P," + STRING(RP.Int1) + "," + STRING(RP.Int2).
    WHEN "AllProperties" THEN
      report-options = "EntityRange,P,0,99999".
    WHEN "CompanyList" THEN
      report-options = "CompanyList," + RP.Char4 .
    WHEN "AllCompanies" THEN
      report-options = "EntityRange,L,0,99999".
  END CASE.

  report-options = report-options
                 + "~nDebtSince," + STRING(RP.Int3)
                 + (IF RP.Log1 THEN "~nSummary" ELSE "")
                 + (IF RP.Log2 THEN "~nExport," + RP.Char2 ELSE "")
                 + (IF RP.Log3 THEN "~nPreview" ELSE "")
                 .

  RUN notify( 'set-busy, container-source':U ).
  RUN process/report/debtstat.p ( report-options ).
  RUN notify( 'set-idle, container-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char2 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated values" "*.CSV"
        SAVE-AS ASK-OVERWRITE CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV"
        INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME UPDATE select-ok.

  IF select-ok THEN DO:
    IF SUBSTRING( file-name, R-INDEX( file-name, ".") ) <> ".CSV" THEN DO:
      IF (LENGTH( file-name ) - R-INDEX( file-name, "." )) < 4 THEN
        file-name = SUBSTRING( file-name, 1, R-INDEX( file-name, ".")) + "CSV" .
    END.
    RP.Char2:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF INPUT FRAME {&FRAME-NAME} RP.Int1 >
     INPUT FRAME {&FRAME-NAME} RP.Int2 THEN
  DO:
    MESSAGE "The TO property must be greater than or equal to the FROM property!"
      VIEW-AS ALERT-BOX ERROR.
    APPLY 'ENTRY':U TO RP.Int1 IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END. 
   
  IF ( cmb_month:SCREEN-VALUE = "" OR cmb_month:SCREEN-VALUE = ? ) THEN
  DO:
    MESSAGE "You must select a month" VIEW-AS ALERT-BOX ERROR.
    APPLY 'ENTRY':U TO cmb_month IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


