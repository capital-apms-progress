&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.
{inc/topic/tpbnkact.i}

{inc/ofc-this.i}
{inc/ofc-set.i "BankAccount-Format" "bank-format"}
IF NOT AVAILABLE(OfficeSetting) THEN DO:
  CASE Office.OfficeCode:
    WHEN "AKLD" THEN      bank-format = "99-9999-9999999-99X".
    WHEN "SDNY" THEN      bank-format = "999-999-9999999XXX".
    OTHERWISE             bank-format = "99-9999-9999999-99X".
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES BankAccount
&Scoped-define FIRST-EXTERNAL-TABLE BankAccount


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR BankAccount.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS BankAccount.BankAccountCode ~
BankAccount.Active BankAccount.CompanyCode BankAccount.AccountCode ~
BankAccount.BankName BankAccount.BankBranchName BankAccount.AccountName ~
BankAccount.BankAccount BankAccount.ChequeAccount BankAccount.DEUserId
&Scoped-define FIELD-PAIRS~
 ~{&FP1}BankAccountCode ~{&FP2}BankAccountCode ~{&FP3}~
 ~{&FP1}CompanyCode ~{&FP2}CompanyCode ~{&FP3}~
 ~{&FP1}AccountCode ~{&FP2}AccountCode ~{&FP3}~
 ~{&FP1}BankName ~{&FP2}BankName ~{&FP3}~
 ~{&FP1}BankBranchName ~{&FP2}BankBranchName ~{&FP3}~
 ~{&FP1}BankAccount ~{&FP2}BankAccount ~{&FP3}
&Scoped-define ENABLED-TABLES BankAccount
&Scoped-define FIRST-ENABLED-TABLE BankAccount
&Scoped-Define ENABLED-OBJECTS RECT-2 
&Scoped-Define DISPLAYED-FIELDS BankAccount.BankAccountCode ~
BankAccount.Active BankAccount.CompanyCode BankAccount.AccountCode ~
BankAccount.BankName BankAccount.BankBranchName BankAccount.AccountName ~
BankAccount.BankAccount BankAccount.ChequeAccount BankAccount.DEUserId
&Scoped-Define DISPLAYED-OBJECTS fil_Company fil_Account 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fil_Account AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 48.43 BY 1
     FONT 11 NO-UNDO.

DEFINE VARIABLE fil_Company AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 48.43 BY 1
     FONT 11 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE NO-FILL 
     SIZE 72.29 BY 11.5.

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     BankAccount.BankAccountCode AT ROW 1.2 COL 10.86 COLON-ALIGNED
          LABEL "Bank A/C"
          VIEW-AS FILL-IN 
          SIZE 5 BY 1
          FONT 11
     BankAccount.Active AT ROW 1.2 COL 63.72
          VIEW-AS TOGGLE-BOX
          SIZE 8.72 BY 1.05
     BankAccount.CompanyCode AT ROW 2.5 COL 10.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 8.14 BY 1
          FONT 11
     fil_Company AT ROW 2.5 COL 22.29 COLON-ALIGNED NO-LABEL
     BankAccount.AccountCode AT ROW 3.5 COL 10.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 8.14 BY 1
          FONT 11
     fil_Account AT ROW 3.5 COL 22.29 COLON-ALIGNED NO-LABEL
     BankAccount.BankName AT ROW 5 COL 10.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 60 BY 1
          FONT 11
     BankAccount.BankBranchName AT ROW 6 COL 10.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 60 BY 1
          FONT 11
     BankAccount.AccountName AT ROW 7.5 COL 12.86 NO-LABEL
          VIEW-AS EDITOR NO-WORD-WRAP
          SIZE 60 BY 2.5
          FONT 11
     BankAccount.BankAccount AT ROW 10 COL 10.86 COLON-ALIGNED
          LABEL "Account No" FORMAT "99-9999-9999999-999"
          VIEW-AS FILL-IN 
          SIZE 20 BY 1
     BankAccount.ChequeAccount AT ROW 10.05 COL 51.72
          VIEW-AS TOGGLE-BOX
          SIZE 21 BY .8
     BankAccount.DEUserId AT ROW 11.2 COL 10.86 COLON-ALIGNED
          LABEL "DE User"
          VIEW-AS FILL-IN 
          SIZE 7 BY 1
          FONT 11
     RECT-2 AT ROW 1 COL 1
     "Account Name:" VIEW-AS TEXT
          SIZE 10.86 BY 1 AT ROW 7.5 COL 1.86
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.BankAccount
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 11.1
         WIDTH              = 85.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR EDITOR BankAccount.AccountName IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN BankAccount.BankAccount IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN BankAccount.BankAccountCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Account IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Company IN FRAME F-Main
   NO-ENABLE DEF-LABEL                                                  */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME BankAccount.AccountCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BankAccount.AccountCode V-table-Win
ON LEAVE OF BankAccount.AccountCode IN FRAME F-Main /* GL Account */
DO:
  {inc/selcde/cdcoa.i "fil_Account"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BankAccount.CompanyCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BankAccount.CompanyCode V-table-Win
ON LEAVE OF BankAccount.CompanyCode IN FRAME F-Main /* Company */
DO:
  {inc/selcde/cdcmp.i "fil_Company"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Account
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Account V-table-Win
ON U1 OF fil_Account IN FRAME F-Main
DO:
  {inc/selfil/sfcoa1.i "BankAccount" "AccountCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Account V-table-Win
ON U2 OF fil_Account IN FRAME F-Main
DO:
  {inc/selfil/sfcoa2.i "BankAccount" "AccountCode"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Account V-table-Win
ON U3 OF fil_Account IN FRAME F-Main
DO:
  {inc/selfil/sfcoa3.i "BankAccount" "AccountCode"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Company
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U1 OF fil_Company IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "BankAccount" "CompanyCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U2 OF fil_Company IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "BankAccount" "CompanyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U3 OF fil_Company IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "BankAccount" "CompanyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */


BankAccount.BankAccount:FORMAT = bank-format.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "BankAccount"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "BankAccount"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-bank-account.
  RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN notify( 'open-query,record-source':U ).
  RUN dispatch( 'update-record':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-bank-account V-table-Win 
PROCEDURE delete-bank-account :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT BankAccount EXCLUSIVE-LOCK.
  DELETE BankAccount.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN get-attribute( 'mode':U ).
  mode = RETURN-VALUE.
  
  IF mode = "Add" THEN
  DO:
    have-records = yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE RUN dispatch( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CREATE BankAccount.
  ASSIGN BankAccount.BankAccount = FILL( "0", 16 ).
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'update-record':U ).
  
  CURRENT-WINDOW:TITLE = "Adding a new Bank Account".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = 'WINDOW-CLOSE':U THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "BankAccount"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


