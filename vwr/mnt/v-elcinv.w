&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description:

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tpivoice.i}

DEF VAR invoice-list AS CHAR NO-UNDO.
DEF VAR default-rate AS DEC  NO-UNDO.
DEF VAR unit-rate    AS DEC  NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-acct.i "Electricity" "electricity-recovered"}
{inc/ofc-set.i "Electricity-Rate" "c-default-rate" "WARNING"}
{inc/ofc-set.i "Invoice-Terms" "default-terms" "WARNING"}
ASSIGN default-rate = DEC( c-default-rate ) NO-ERROR.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Invoice
&Scoped-define FIRST-EXTERNAL-TABLE Invoice


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Invoice.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Invoice.InvoiceDate Invoice.EntityType ~
Invoice.EntityCode Invoice.TaxAmount Invoice.TaxApplies 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}InvoiceDate ~{&FP2}InvoiceDate ~{&FP3}~
 ~{&FP1}EntityType ~{&FP2}EntityType ~{&FP3}~
 ~{&FP1}EntityCode ~{&FP2}EntityCode ~{&FP3}~
 ~{&FP1}TaxAmount ~{&FP2}TaxAmount ~{&FP3}
&Scoped-define ENABLED-TABLES Invoice
&Scoped-define FIRST-ENABLED-TABLE Invoice
&Scoped-Define ENABLED-OBJECTS RECT-13 loc_Property loc_Meter loc_Account ~
fil_prevdate fil_prevunits fil_currdate fil_currunits cmb_Terms btn_add ~
btn_ok btn_cancel 
&Scoped-Define DISPLAYED-FIELDS Invoice.InvoiceNo Invoice.InvoiceDate ~
Invoice.EntityType Invoice.EntityCode Invoice.TaxAmount Invoice.TaxApplies 
&Scoped-Define DISPLAYED-OBJECTS loc_Property fil_PropertyName loc_Meter ~
fil_meter fil_EntityName loc_Account fil_AccountName fil_prevdate ~
fil_prevunits fil_currdate fil_currunits cmb_Terms fil_total fil_amtdue ~
fil_calc 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD current-unit-rate V-table-Win 
FUNCTION current-unit-rate RETURNS DECIMAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD total-units V-table-Win 
FUNCTION total-units RETURNS INTEGER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 6.86 BY 1
     FONT 9.

DEFINE BUTTON btn_ok 
     LABEL "&OK" 
     SIZE 5.72 BY 1.05
     FONT 9.

DEFINE VARIABLE cmb_Terms AS CHARACTER FORMAT "X(30)":U 
     LABEL "Terms" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "","" 
     SIZE 44.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_AccountName AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE fil_amtdue AS DECIMAL FORMAT "-Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Amt Due" 
     VIEW-AS FILL-IN 
     SIZE 14.57 BY 1
     FGCOLOR 12 FONT 11 NO-UNDO.

DEFINE VARIABLE fil_calc AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 28 BY 1
     FGCOLOR 1  NO-UNDO.

DEFINE VARIABLE fil_currdate AS DATE FORMAT "99/99/9999":U 
     LABEL "Current Reading on" 
     VIEW-AS FILL-IN 
     SIZE 9.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_currunits AS INTEGER FORMAT "ZZ,ZZZ,ZZ9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 12.14 BY 1
     FONT 11 NO-UNDO.

DEFINE VARIABLE fil_EntityName AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE fil_meter AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE fil_prevdate AS DATE FORMAT "99/99/9999":U 
     LABEL "Previous reading on" 
     VIEW-AS FILL-IN 
     SIZE 9.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_prevunits AS INTEGER FORMAT "ZZ,ZZZ,ZZ9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 12.14 BY 1
     FONT 11 NO-UNDO.

DEFINE VARIABLE fil_PropertyName AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE fil_total AS DECIMAL FORMAT "-Z,ZZZ,ZZ9.99" INITIAL 0 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 14.57 BY 1
     FGCOLOR 12 FONT 11.

DEFINE VARIABLE loc_Account AS DECIMAL FORMAT "9999.99":U INITIAL 0 
     LABEL "Account" 
     VIEW-AS FILL-IN 
     SIZE 7.43 BY 1 NO-UNDO.

DEFINE VARIABLE loc_Meter AS CHARACTER FORMAT "X(8)":U INITIAL "0" 
     LABEL "Meter" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE loc_Property AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "Property" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-13
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 53.72 BY 13.7.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Invoice.InvoiceNo AT ROW 1 COL 43.57 COLON-ALIGNED
          LABEL "No" FORMAT "ZZZZZ9"
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
          FONT 11
     Invoice.InvoiceDate AT ROW 2.1 COL 8.14 COLON-ALIGNED
          LABEL "Date"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     loc_Property AT ROW 3.4 COL 8.14 COLON-ALIGNED
     fil_PropertyName AT ROW 3.4 COL 17 COLON-ALIGNED NO-LABEL
     loc_Meter AT ROW 4.4 COL 8.14 COLON-ALIGNED
     fil_meter AT ROW 4.4 COL 17 COLON-ALIGNED NO-LABEL
     Invoice.EntityType AT ROW 5.9 COL 8.14 COLON-ALIGNED
          LABEL "Code to"
          VIEW-AS FILL-IN 
          SIZE 2.43 BY 1
     Invoice.EntityCode AT ROW 5.9 COL 10.43 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_EntityName AT ROW 5.9 COL 17 COLON-ALIGNED NO-LABEL
     loc_Account AT ROW 6.9 COL 8.14 COLON-ALIGNED
     fil_AccountName AT ROW 6.9 COL 17 COLON-ALIGNED NO-LABEL
     fil_prevdate AT ROW 8.2 COL 17 COLON-ALIGNED
     fil_prevunits AT ROW 8.2 COL 30.43 COLON-ALIGNED NO-LABEL
     fil_currdate AT ROW 9.2 COL 17 COLON-ALIGNED
     fil_currunits AT ROW 9.2 COL 30.43 COLON-ALIGNED NO-LABEL
     cmb_Terms AT ROW 10.4 COL 7.57 COLON-ALIGNED
     fil_total AT ROW 11.5 COL 7.57 COLON-ALIGNED
     Invoice.TaxAmount AT ROW 12.5 COL 7.57 COLON-ALIGNED FORMAT "-Z,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14.57 BY 1
          FGCOLOR 12 FONT 11
     Invoice.TaxApplies AT ROW 12.5 COL 26.14
          VIEW-AS TOGGLE-BOX
          SIZE 9.14 BY 1
     fil_amtdue AT ROW 13.5 COL 7.57 COLON-ALIGNED
     btn_add AT ROW 14.8 COL 1
     btn_ok AT ROW 14.8 COL 41
     btn_cancel AT ROW 14.8 COL 47.86
     fil_calc AT ROW 11.5 COL 26.14 NO-LABEL
     "Electricity Invoice Registration" VIEW-AS TEXT
          SIZE 34.86 BY 1 AT ROW 1.1 COL 1.57
          FONT 13
     "was" VIEW-AS TEXT
          SIZE 2.86 BY 1 AT ROW 8.2 COL 29
     "units." VIEW-AS TEXT
          SIZE 4.57 BY 1 AT ROW 8.2 COL 45
     RECT-13 AT ROW 1 COL 1
     "units." VIEW-AS TEXT
          SIZE 4.57 BY 1 AT ROW 9.2 COL 45
     "is" VIEW-AS TEXT
          SIZE 1.14 BY 1 AT ROW 9.2 COL 30.14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Invoice
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.75
         WIDTH              = 62.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Invoice.EntityCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Invoice.EntityType IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_AccountName IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_amtdue IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_calc IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN fil_EntityName IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_meter IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_PropertyName IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_total IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Invoice.InvoiceDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Invoice.InvoiceNo IN FRAME F-Main
   NO-ENABLE EXP-LABEL EXP-FORMAT                                       */
/* SETTINGS FOR FILL-IN Invoice.TaxAmount IN FRAME F-Main
   EXP-FORMAT                                                           */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK KEEP-EMPTY"
     _TblOptList       = "FIRST OUTER"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/entity.i}
{inc/string.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON RETURN OF FRAME F-Main
ANYWHERE DO:
  IF LOOKUP( FOCUS:TYPE, 'FILL-IN,EDITOR,COMBO-BOX' ) <> 0 OR
     FOCUS:TYPE = 'BUTTON':U AND FOCUS:DYNAMIC THEN
  DO:
    APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN dispatch( 'add-record':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN cancel-changes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON CHOOSE OF btn_ok IN FRAME F-Main /* OK */
DO:
  RUN confirm-changes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Terms
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Terms V-table-Win
ON U1 OF cmb_Terms IN FRAME F-Main /* Terms */
DO:
  {inc/selcmb/scivt1.i "Invoice" "TermsCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Terms V-table-Win
ON U2 OF cmb_Terms IN FRAME F-Main /* Terms */
DO:
  {inc/selcmb/scivt2.i "Invoice" "TermsCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Invoice.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Invoice.EntityCode V-table-Win
ON LEAVE OF Invoice.EntityCode IN FRAME F-Main /* Code */
DO:
  RUN show-entity-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_currunits
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_currunits V-table-Win
ON LEAVE OF fil_currunits IN FRAME F-Main
DO:
  IF SELF:MODIFIED THEN RUN update-details.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_meter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_meter V-table-Win
ON U1 OF fil_meter IN FRAME F-Main
DO:
  {inc/selfil/sfmtr1.i "?" "loc_Meter"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_meter V-table-Win
ON U2 OF fil_meter IN FRAME F-Main
DO:
  {inc/selfil/sfmtr2.i "?" "loc_Meter"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_meter V-table-Win
ON U3 OF fil_meter IN FRAME F-Main
DO:
  {inc/selfil/sfmtr3.i "?" "loc_Meter"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_prevunits
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prevunits V-table-Win
ON LEAVE OF fil_prevunits IN FRAME F-Main
DO:
  IF SELF:MODIFIED THEN RUN update-details.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME loc_Account
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL loc_Account V-table-Win
ON LEAVE OF loc_Account IN FRAME F-Main /* Account */
DO:
  RUN show-account-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME loc_Meter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL loc_Meter V-table-Win
ON LEAVE OF loc_Meter IN FRAME F-Main /* Meter */
DO:
/*  {inc/selcde/cdmtr.i "fil_Meter"} */
  IF LAST-EVENT:LABEL <> "SHIFT-TAB":U THEN DO:
    RUN apply-meter-choice.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME loc_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL loc_Property V-table-Win
ON LEAVE OF loc_Property IN FRAME F-Main /* Property */
DO:
  RUN show-property-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Invoice.TaxApplies
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Invoice.TaxApplies V-table-Win
ON VALUE-CHANGED OF Invoice.TaxApplies IN FRAME F-Main /* Taxable */
DO:
  RUN update-details.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = "PropertyCode,EntityType,EntityCode,AccountCode"':U).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Invoice"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Invoice"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE apply-meter-choice V-table-Win 
PROCEDURE apply-meter-choice :
/*------------------------------------------------------------------------------
  Purpose:  Apply entry to the appropriate field after the meter id is entered
------------------------------------------------------------------------------*/
DEF VAR apply-to-account AS LOGI NO-UNDO    INITIAL No.

DO WITH FRAME {&FRAME-NAME}:
  FIND SupplyMeter WHERE SupplyMeter.PropertyCode = INPUT loc_Property
                     AND SupplyMeter.MeterCode = INPUT loc_Meter NO-LOCK NO-ERROR.
  IF AVAILABLE(SupplyMeter) THEN DO:
    fil_Meter:SCREEN-VALUE = SupplyMeter.Description.
    Invoice.EntityType:SCREEN-VALUE = SupplyMeter.EntityType.
    Invoice.EntityCode:SCREEN-VALUE = STRING(SupplyMeter.EntityCode, Invoice.EntityCode:FORMAT ).
    loc_Account:SCREEN-VALUE = STRING( (IF SupplyMeter.AccountCode > 0.001 THEN SupplyMeter.AccountCode ELSE electricity-recovered), loc_Account:FORMAT ).
    RUN show-entity-name.
    RUN show-account-name.
    IF RETURN-VALUE = "FAIL" THEN apply-to-account = Yes.

    FIND LAST SupplyMeterReading WHERE SupplyMeterReading.PropertyCode = SupplyMeter.PropertyCode
                                 AND SupplyMeterReading.MeterCode = SupplyMeter.MeterCode
                                 NO-LOCK NO-ERROR.
    IF AVAILABLE(SupplyMeterReading) THEN DO:
      fil_prevdate:SCREEN-VALUE  = STRING( SupplyMeterReading.ReadingDate, fil_prevdate:FORMAT ).
      fil_prevunits:SCREEN-VALUE = STRING( SupplyMeterReading.Reading, fil_prevunits:FORMAT ).
      IF NOT apply-to-account THEN APPLY "ENTRY":U TO fil_currdate.
    END.
    ELSE
      IF NOT apply-to-account THEN APPLY "ENTRY":U TO fil_prevdate.
  END.
  ELSE DO:
    APPLY 'ENTRY':U TO Invoice.EntityType.
/*    MESSAGE "Meter not found with code " + loc_Meter:SCREEN-VALUE
                              + " for P" + loc_Property:SCREEN-VALUE
            VIEW-AS ALERT-BOX ERROR TITLE "Invalid Property/Meter Code".
    RETURN "FAIL". */
  END.

  IF apply-to-account THEN APPLY "ENTRY":U TO loc_Account.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-invoice-line V-table-Win 
PROCEDURE assign-invoice-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR ivl-et AS CHAR NO-UNDO.
DEF VAR ivl-ec AS INT NO-UNDO.
DEF VAR units AS INT NO-UNDO.
DEF VAR amount AS DEC NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  IF NOT AVAILABLE Invoice THEN RETURN.

  IF Invoice.EntityType = "T" THEN DO:
    FIND Tenant WHERE Tenant.TenantCode = Invoice.EntityCode NO-LOCK.
    ivl-et = Tenant.EntityType.
    ivl-ec = Tenant.EntityCode.
  END.
  ELSE DO:
    ivl-et = Invoice.EntityType.
    ivl-ec = Invoice.EntityCode.
  END.

  units = total-units().
  amount = units * current-unit-rate().

  FIND FIRST InvoiceLine OF Invoice NO-ERROR.
  IF NOT AVAILABLE InvoiceLine THEN CREATE InvoiceLine.

  ASSIGN
    InvoiceLine.InvoiceNo   = Invoice.InvoiceNo
    InvoiceLine.EntityType  = ivl-et
    InvoiceLine.EntityCode  = ivl-ec
    InvoiceLine.AccountCode = INPUT loc_Account
    InvoiceLine.AccountText = fil_AccountName:SCREEN-VALUE
    InvoiceLine.Amount      = amount
    InvoiceLine.Percent     = 100.00
    InvoiceLine.YourShare   = InvoiceLine.Amount
    InvoiceLine.Quantity    = units.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-meter V-table-Win 
PROCEDURE assign-meter :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER pcode AS INT NO-UNDO.
DEF INPUT PARAMETER mcode AS CHAR NO-UNDO.
DEF INPUT PARAMETER read-date AS DATE NO-UNDO.
DEF INPUT PARAMETER reading AS DEC NO-UNDO.

  FIND SupplyMeter WHERE SupplyMeter.PropertyCode = pcode
                     AND SupplyMeter.MeterCode = mcode NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(SupplyMeter) THEN RETURN "FAIL".

DO TRANSACTION:
  FIND SupplyMeterReading WHERE SupplyMeterReading.PropertyCode = SupplyMeter.PropertyCode
                          AND SupplyMeterReading.MeterCode = SupplyMeter.MeterCode
                          AND SupplyMeterReading.ReadingDate = read-date EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE(SupplyMeterReading) THEN CREATE SupplyMeterReading.

  BUFFER-COPY SupplyMeter TO SupplyMeterReading
            ASSIGN  SupplyMeterReading.ReadingDate = read-date
                    SupplyMeterReading.Reading     = reading.
  FIND CURRENT SupplyMeterReading NO-LOCK.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-meter-readings V-table-Win 
PROCEDURE assign-meter-readings :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  ASSIGN FRAME {&FRAME-NAME} loc_Property loc_Meter fil_PrevDate fil_PrevUnits
                              fil_CurrDate fil_CurrUnits.
  RUN assign-meter( loc_Property, loc_Meter, fil_PrevDate, fil_PrevUnits ).
  RUN assign-meter( loc_Property, loc_Meter, fil_CurrDate, fil_CurrUnits ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN delete-invoice.
  RUN print-invoices.
  IF NUM-ENTRIES( invoice-list ) <> 0 THEN
    RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN check-modified( "CLEAR":U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN verify-invoice.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).
  RUN print-invoices.
  RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-invoice V-table-Win 
PROCEDURE delete-invoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Invoice THEN RETURN.
  
  FIND Current Invoice EXCLUSIVE-LOCK.
  DELETE Invoice.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR to-detail AS CHAR NO-UNDO.
DEF VAR inv-blurb AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  to-detail = "Electricity recovery from " + STRING( INPUT fil_prevdate, "99/99/9999" ) +
                                    " to " + STRING( INPUT fil_currdate, "99/99/9999" ).
  inv-blurb = to-detail + "~n~n" +
      "Meter Number " + TRIM( INPUT loc_Meter ) + ", " + INPUT fil_meter + "~n~n" +
      "Previous reading on " + STRING( INPUT fil_prevdate, "99/99/9999" ) + FILL( " ", 8 ) + STRING( INPUT fil_prevunits, "ZZ,ZZZ,ZZ9" ) + "~n" +
      "Current reading on  " + STRING( INPUT fil_currdate, "99/99/9999" ) + FILL( " ", 8 ) + STRING( INPUT fil_currunits, "ZZ,ZZZ,ZZ9" ) + "~n" +
      SPC( 36 ) + "------------" + "~n" +
      SPC( 37 ) + STRING( total-units(), "-ZZ,ZZZ,ZZ9" ) + "~n" +
      SPC( 36 ) + "============" + "~n~n" +
      "Unit rate:" + SPC( 10 ) + STRING( current-unit-rate(), "$>9.9999" ).

  ASSIGN
    Invoice.InvoiceStatus = "U"
    Invoice.InvoiceType   = "ELEC"
    Invoice.ToDetail      = to-detail
    Invoice.Blurb         = inv-blurb.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     View or hide tax fields depending on the office
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF AVAILABLE Invoice THEN
  DO:
    fil_total = Invoice.Total + Invoice.TaxAmount.
    DISPLAY fil_total WITH FRAME {&FRAME-NAME}.
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  Invoice.TaxAmount:HIDDEN    = AVAILABLE Office AND Office.GST = 0.
  Invoice.TaxAmount:SENSITIVE = No.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN dispatch( 'add-record':U ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-update-record V-table-Win 
PROCEDURE local-update-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'update-record':U ) .
  RUN assign-invoice-line.
  RUN assign-meter-readings.
  
  /* Code placed here will execute AFTER standard behavior.    */
  IF LOOKUP( STRING( Invoice.InvoiceNo ), invoice-list ) = 0 THEN DO:
    invoice-list = invoice-list + IF invoice-list = "" THEN "" ELSE ",".
    invoice-list = invoice-list + STRING( Invoice.InvoiceNo ) .
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  
  IF AVAILABLE Invoice THEN DO:
    RUN verify-invoice.
    IF RETURN-VALUE = "FAIL" THEN DO:
      RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
      RETURN.
    END.
    RUN dispatch( 'update-record':U ).
  END.

  CREATE Invoice.
  ASSIGN
    Invoice.InvoiceDate = TODAY
    Invoice.TaxApplies  = Office.GST <> ?.

  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'display-fields':U ).

  RUN reset-local-fields.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  APPLY 'ENTRY':U TO Invoice.InvoiceDate IN FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  have-records = Yes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-invoices V-table-Win 
PROCEDURE print-invoices :
/*------------------------------------------------------------------------------
  Purpose:     Print the invoice approval forms for all new/modified invoices
------------------------------------------------------------------------------*/

DEF VAR n AS INT NO-UNDO.

  n = NUM-ENTRIES( invoice-list ).
  IF  n = 0 THEN RETURN.

  MESSAGE n "Invoices to be printed" VIEW-AS ALERT-BOX INFORMATION.
  RUN process/report/invcappr.p( ?, ?, invoice-list ).

  /* work around obscure Progress wierdness so we don't print twice! */
  invoice-list = "".
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-local-fields V-table-Win 
PROCEDURE reset-local-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  loc_meter:SCREEN-VALUE = STRING( 0 ).
  loc_Meter:PRIVATE-DATA = ",".
  loc_meter:MODIFIED = No.

  fil_meter:SCREEN-VALUE = "".
  fil_meter:PRIVATE-DATA = "".

  fil_prevdate:SCREEN-VALUE  = "".    fil_prevdate:MODIFIED = No.
  fil_currdate:SCREEN-VALUE  = "".    fil_currdate:MODIFIED = No.

  fil_prevunits:SCREEN-VALUE = STRING( 0 ). fil_prevunits:MODIFIED = No.
  fil_currunits:SCREEN-VALUE = STRING( 0 ). fil_currunits:MODIFIED = No.


  /* Set the default invoice terms */
  DEF VAR terms-index AS INT NO-UNDO.
  
  FIND InvoiceTerms WHERE InvoiceTerms.TermsCode = default-terms
    NO-LOCK NO-ERROR.

  IF AVAILABLE InvoiceTerms THEN terms-index =
    LOOKUP( STRING( ROWID( InvoiceTerms ) ), cmb_Terms:PRIVATE-DATA ).
  terms-index = MAX( terms-index, 1 ).

  cmb_Terms:SCREEN-VALUE = cmb_Terms:ENTRY( terms-index ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER key-name AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER key-value AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  CASE key-name:
    WHEN "PropertyCode" THEN    key-value = loc_Property:SCREEN-VALUE .
    WHEN "EntityType" THEN      key-value = Invoice.EntityType:SCREEN-VALUE .
    WHEN "EntityCode" THEN      key-value = Invoice.EntityCode:SCREEN-VALUE .
    WHEN "AccountCode" THEN     key-value = loc_Account:SCREEN-VALUE .
  END CASE.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Invoice"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE show-account-name V-table-Win 
PROCEDURE show-account-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = INPUT loc_Account NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(ChartOfAccount) THEN RETURN "FAIL".
  fil_AccountName = ChartOfAccount.Name.
  DISPLAY fil_AccountName.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE show-entity-name V-table-Win 
PROCEDURE show-entity-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  fil_EntityName = get-entity-name( INPUT Invoice.EntityType, INPUT Invoice.EntityCode ).
  DISPLAY fil_PropertyName.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE show-property-name V-table-Win 
PROCEDURE show-property-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  FIND Property WHERE Property.PropertyCode = INPUT loc_Property NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Property) THEN RETURN "FAIL".
  fil_PropertyName = Property.Name.
  DISPLAY fil_PropertyName.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-details V-table-Win 
PROCEDURE update-details :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF ( INPUT fil_prevunits = ? OR INPUT fil_prevunits = 0 OR
       INPUT fil_currunits = ? OR INPUT fil_currunits = 0 )
    THEN RETURN.
  
  DEF VAR invoice-total  AS DEC NO-UNDO.
  DEF VAR invoice-tax    AS DEC NO-UNDO.
  DEF VAR invoice-amount AS DEC NO-UNDO.

  fil_calc:SCREEN-VALUE = '(' + STRING( total-units() ) + ' units x ' + STRING( current-unit-rate(), "$>9.9999" ) + ' x ' +
        STRING( 100, ">>9.99%" ) + ')'.

  invoice-total  = total-units() * current-unit-rate().
  invoice-tax    = IF INPUT Invoice.TaxApplies THEN invoice-total * ( Office.GST / 100 ) ELSE 0.
  invoice-amount = invoice-total + invoice-tax.
  
  fil_total:SCREEN-VALUE         = STRING( invoice-total ).
  Invoice.TaxAmount:SCREEN-VALUE = STRING( invoice-tax ).
  fil_amtdue:SCREEN-VALUE        = STRING( invoice-amount ).
  
  fil_prevunits:MODIFIED = No.
  fil_currunits:MODIFIED = No.
      
END.   
                
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-invoice V-table-Win 
PROCEDURE verify-invoice :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  RUN show-entity-name.
  IF fil_EntityName MATCHES "~* ~* ~* Unknown*" THEN DO:
    MESSAGE "You must select a valid entity for updating"
            VIEW-AS ALERT-BOX ERROR TITLE "Invalid Entity Code".
    APPLY 'ENTRY':U TO Invoice.EntityType.
    RETURN "FAIL".
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION current-unit-rate V-table-Win 
FUNCTION current-unit-rate RETURNS DECIMAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Returns the electricity unit rate for the current tenant
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  FIND FIRST TenancyLease WHERE TenancyLease.TenantCode = INPUT Invoice.EntityCode NO-LOCK NO-ERROR.
  IF AVAILABLE( TenancyLease ) AND TenancyLease.ElectricityUnitRate <> 0
                               AND TenancyLease.ElectricityUnitRate <> ?
  THEN RETURN TenancyLease.ElectricityUnitRate.

  FIND SupplyMeter WHERE SupplyMeter.PropertyCode = INPUT loc_Property
                     AND SupplyMeter.MeterCode = INPUT loc_Meter NO-LOCK NO-ERROR.
  IF AVAILABLE(SupplyMeter) AND SupplyMeter.ElectricityUnitRate <> 0
                            AND SupplyMeter.ElectricityUnitRate <> ?
  THEN RETURN SupplyMeter.ElectricityUnitRate.

/* otherwise */
  RETURN default-rate.
  
END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION total-units V-table-Win 
FUNCTION total-units RETURNS INTEGER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RETURN( INPUT fil_currunits - INPUT fil_prevunits ).
END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


