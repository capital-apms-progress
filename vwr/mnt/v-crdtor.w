&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}

{inc/persndtl.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Creditor
&Scoped-define FIRST-EXTERNAL-TABLE Creditor


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Creditor.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Creditor.NonAccounting ~
Creditor.AcctCreditorCode Creditor.Name Creditor.PayeeName ~
Creditor.VchrEntityType Creditor.VchrEntityCode Creditor.VchrAccountCode ~
Creditor.VchrApprover Creditor.ChequesPerMonth Creditor.Active 
&Scoped-define ENABLED-TABLES Creditor
&Scoped-define FIRST-ENABLED-TABLE Creditor
&Scoped-Define ENABLED-OBJECTS cmb_PaymentStyle fil_FullName fil_Company ~
cmb_Address btn_Clear fil_City fil_state fil_Country fil_Zip cmb_Phone1 ~
fil_Phone1 cmb_Phone2 fil_Phone2 cmb_Phone3 fil_Phone3 cmb_Phone4 ~
fil_Phone4 RECT-26 RECT-27 
&Scoped-Define DISPLAYED-FIELDS Creditor.NonAccounting ~
Creditor.AcctCreditorCode Creditor.CreditorCode Creditor.Name ~
Creditor.PayeeName Creditor.VchrEntityType Creditor.VchrEntityCode ~
Creditor.VchrAccountCode Creditor.VchrApprover Creditor.ChequesPerMonth ~
Creditor.BankDetails Creditor.EnableDirectPayment Creditor.Active ~
Creditor.DcStatementText Creditor.DcRemittanceEmail 
&Scoped-define DISPLAYED-TABLES Creditor
&Scoped-define FIRST-DISPLAYED-TABLE Creditor
&Scoped-Define DISPLAYED-OBJECTS fil_PaymentEnabledBy fil_AcctChangedBy ~
cmb_PaymentStyle fil_FullName fil_Company cmb_Address edt_address fil_City ~
fil_state fil_Country fil_Zip cmb_Phone1 fil_Phone1 cmb_Phone2 fil_Phone2 ~
cmb_Phone3 fil_Phone3 cmb_Phone4 fil_Phone4 fil_OtherContact 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */
&Scoped-define List-1 F-Main 
&Scoped-define List-2 F-Main Creditor.EnableDirectPayment 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
CreditorCode|y|y|TTPL.Creditor.CreditorCode
CompanyCode||y|TTPL.Creditor.CompanyCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "CreditorCode",
     Keys-Supplied = "CreditorCode,CompanyCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Clear 
     LABEL "&Clear" 
     SIZE 12 BY 1.05
     FONT 10.

DEFINE VARIABLE cmb_Address AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 22.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_PaymentStyle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Default payment method" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 23.43 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Phone1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 12.57 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_Phone2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 12.57 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_Phone3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 12.57 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_Phone4 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 12.57 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE edt_address AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 46.86 BY 4
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_AcctChangedBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Set by" 
     VIEW-AS FILL-IN 
     SIZE 22.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_City AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1
     FONT 10.

DEFINE VARIABLE fil_Company AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 60.57 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Country AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1
     FONT 10.

DEFINE VARIABLE fil_FullName AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 60.57 BY 1
     FONT 10.

DEFINE VARIABLE fil_OtherContact AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 57.14 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_PaymentEnabledBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Enabled by" 
     VIEW-AS FILL-IN 
     SIZE 22.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Phone1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Phone2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Phone3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 22.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Phone4 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 22.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_state AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1
     FONT 10.

DEFINE VARIABLE fil_Zip AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1
     FONT 10.

DEFINE RECTANGLE RECT-26
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 70 BY .05.

DEFINE RECTANGLE RECT-27
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 72 BY 20.85.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Creditor.NonAccounting AT ROW 1 COL 33.57
          LABEL "Non Accounting"
          VIEW-AS TOGGLE-BOX
          SIZE 14.86 BY 1
     fil_PaymentEnabledBy AT ROW 8.95 COL 47.57 COLON-ALIGNED
     Creditor.AcctCreditorCode AT ROW 1 COL 61.86 COLON-ALIGNED
          LABEL "Accounting Creditor"
          VIEW-AS FILL-IN 
          SIZE 5.72 BY 1
     fil_AcctChangedBy AT ROW 6.6 COL 47.57 COLON-ALIGNED
     Creditor.CreditorCode AT ROW 1 COL 4.14 COLON-ALIGNED
          LABEL "Code"
          VIEW-AS FILL-IN 
          SIZE 7 BY 1
          FONT 10
     Creditor.Name AT ROW 2.2 COL 9.86 COLON-ALIGNED NO-LABEL FORMAT "X(100)"
          VIEW-AS FILL-IN 
          SIZE 60.57 BY 1
          FONT 10
     Creditor.PayeeName AT ROW 3.2 COL 11.86 NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 60.57 BY 1
          FONT 10
     Creditor.VchrEntityType AT ROW 4.4 COL 17.86 COLON-ALIGNED
          LABEL "Default Voucher Coding"
          VIEW-AS FILL-IN 
          SIZE 2.43 BY 1
     Creditor.VchrEntityCode AT ROW 4.4 COL 22.43 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
     Creditor.VchrAccountCode AT ROW 4.4 COL 33.86 COLON-ALIGNED
          LABEL "Acct"
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
     Creditor.VchrApprover AT ROW 4.4 COL 64.72 COLON-ALIGNED
          LABEL "Default Approver"
          VIEW-AS FILL-IN 
          SIZE 5.72 BY 1
     cmb_PaymentStyle AT ROW 5.4 COL 17.86 COLON-ALIGNED
     Creditor.ChequesPerMonth AT ROW 5.4 COL 64.72 COLON-ALIGNED
          LABEL "Max paymts per month"
          VIEW-AS FILL-IN 
          SIZE 5.72 BY 1
     Creditor.BankDetails AT ROW 6.6 COL 9.86 COLON-ALIGNED
          LABEL "A/c details" FORMAT "X(70)"
          VIEW-AS FILL-IN 
          SIZE 31.43 BY 1 TOOLTIP "Enter the bank details as Bank/Branch/Account #"
     Creditor.EnableDirectPayment AT ROW 8.95 COL 11.86
          LABEL "Direct Payment Enabled"
          VIEW-AS TOGGLE-BOX
          SIZE 22.29 BY 1.05
     fil_FullName AT ROW 10.4 COL 9.86 COLON-ALIGNED NO-LABEL
     fil_Company AT ROW 11.5 COL 11.86 NO-LABEL
     cmb_Address AT ROW 12.65 COL 9.86 COLON-ALIGNED NO-LABEL
     btn_Clear AT ROW 12.65 COL 36.43
     edt_address AT ROW 13.85 COL 1.57 NO-LABEL
     fil_City AT ROW 13.95 COL 55 COLON-ALIGNED NO-LABEL
     fil_state AT ROW 14.95 COL 55 COLON-ALIGNED NO-LABEL
     fil_Country AT ROW 15.95 COL 55 COLON-ALIGNED NO-LABEL
     fil_Zip AT ROW 16.95 COL 55 COLON-ALIGNED NO-LABEL
     cmb_Phone1 AT ROW 18.55 COL 1.57 NO-LABEL
     fil_Phone1 AT ROW 18.55 COL 12.14 COLON-ALIGNED NO-LABEL
     cmb_Phone2 AT ROW 19.75 COL 1.57 NO-LABEL
     fil_Phone2 AT ROW 19.75 COL 12.14 COLON-ALIGNED NO-LABEL
     cmb_Phone3 AT ROW 18.55 COL 35.57 COLON-ALIGNED NO-LABEL
     fil_Phone3 AT ROW 18.55 COL 48.14 COLON-ALIGNED NO-LABEL
     cmb_Phone4 AT ROW 19.75 COL 35.57 COLON-ALIGNED NO-LABEL
     fil_Phone4 AT ROW 19.75 COL 48.14 COLON-ALIGNED NO-LABEL
     fil_OtherContact AT ROW 21.05 COL 13.29 COLON-ALIGNED NO-LABEL
     Creditor.Active AT ROW 1 COL 18.14
          VIEW-AS TOGGLE-BOX
          SIZE 8 BY 1
          FONT 10
     Creditor.DcStatementText AT ROW 7.75 COL 10 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 18.57 BY 1.05
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME F-Main
     Creditor.DcRemittanceEmail AT ROW 7.75 COL 47.57 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 22.86 BY 1.05
     RECT-26 AT ROW 10.2 COL 1.57
     RECT-27 AT ROW 1.4 COL 1
     "Name:" VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 2.2 COL 1.57
          FONT 10
     "Payee:" VIEW-AS TEXT
          SIZE 6 BY 1 AT ROW 3.2 COL 1.57
          FONT 10
     "Person:" VIEW-AS TEXT
          SIZE 5.72 BY 1 AT ROW 10.5 COL 1.57
          FONT 10
     "Company:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 11.5 COL 1.57
          FONT 10
     "Address:" VIEW-AS TEXT
          SIZE 6.29 BY 1 AT ROW 12.75 COL 1.57
          FONT 10
     "City:" VIEW-AS TEXT
          SIZE 4 BY 1 AT ROW 13.95 COL 50.72
          FONT 10
     "State:" VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 14.95 COL 50.72
          FONT 10
     "Country:" VIEW-AS TEXT
          SIZE 5.72 BY 1 AT ROW 15.95 COL 50.72
          FONT 10
     "Zip:" VIEW-AS TEXT
          SIZE 4 BY 1 AT ROW 16.95 COL 50.72
          FONT 10
     "Telephones:" VIEW-AS TEXT
          SIZE 8.57 BY .8 AT ROW 17.85 COL 1.57
          FONT 10
     "Other Contact:" VIEW-AS TEXT
          SIZE 10 BY 1 AT ROW 21.05 COL 1.57
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Creditor
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 23.15
         WIDTH              = 86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit 1 2 Custom                                   */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Creditor.AcctCreditorCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Creditor.BankDetails IN FRAME F-Main
   NO-ENABLE EXP-LABEL EXP-FORMAT                                       */
/* SETTINGS FOR FILL-IN Creditor.ChequesPerMonth IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR COMBO-BOX cmb_Phone1 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Phone2 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Creditor.CreditorCode IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN Creditor.DcRemittanceEmail IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Creditor.DcStatementText IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR edt_address IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       edt_address:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR TOGGLE-BOX Creditor.EnableDirectPayment IN FRAME F-Main
   NO-ENABLE 2 EXP-LABEL                                                */
/* SETTINGS FOR FILL-IN fil_AcctChangedBy IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Company IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fil_OtherContact IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_PaymentEnabledBy IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Creditor.Name IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX Creditor.NonAccounting IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Creditor.PayeeName IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Creditor.VchrAccountCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Creditor.VchrApprover IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Creditor.VchrEntityCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Creditor.VchrEntityType IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON MOUSE-SELECT-DBLCLICK OF FRAME F-Main
ANYWHERE DO:
  RUN special-enable-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Creditor.BankDetails
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Creditor.BankDetails V-table-Win
ON LEAVE OF Creditor.BankDetails IN FRAME F-Main /* A/c details */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_Clear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Clear V-table-Win
ON CHOOSE OF btn_Clear IN FRAME F-Main /* Clear */
DO:
  RUN clear-address.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Address
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Address V-table-Win
ON VALUE-CHANGED OF cmb_Address IN FRAME F-Main
DO:
  RUN address-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PaymentStyle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON U1 OF cmb_PaymentStyle IN FRAME F-Main /* Default payment method */
DO:
  {inc/selcmb/scpsty1.i "Creditor" "PaymentStyle"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON U2 OF cmb_PaymentStyle IN FRAME F-Main /* Default payment method */
DO:
  {inc/selcmb/scpsty2.i "Creditor" "PaymentStyle"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone1 V-table-Win
ON VALUE-CHANGED OF cmb_Phone1 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone2 V-table-Win
ON VALUE-CHANGED OF cmb_Phone2 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone3 V-table-Win
ON VALUE-CHANGED OF cmb_Phone3 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone4 V-table-Win
ON VALUE-CHANGED OF cmb_Phone4 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edt_address
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edt_address V-table-Win
ON LEAVE OF edt_address IN FRAME F-Main
DO:
  IF NUM-ENTRIES( SELF:SCREEN-VALUE, "~n" ) > 4 THEN DO:
    MESSAGE "Some window envelopes and/or reports may not correctly~n"
            "display addresses with more than four lines"
            VIEW-AS ALERT-BOX WARNING
            TITLE "Warning: possibly too many address lines".
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_FullName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_FullName V-table-Win
ON U1 OF fil_FullName IN FRAME F-Main
DO:
  {inc/selfil/sfpsn1.i "Creditor" "PaymentContact"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_FullName V-table-Win
ON U2 OF fil_FullName IN FRAME F-Main
DO:
  {inc/selfil/sfpsn2.i "Creditor" "PaymentContact"}
  FIND Contact WHERE ROWID( Contact ) = TO-ROWID( SELF:PRIVATE-DATA )
    NO-LOCK NO-ERROR.
  IF AVAILABLE Contact THEN RUN new-contact-selected( Contact.PersonCode ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_FullName V-table-Win
ON U3 OF fil_FullName IN FRAME F-Main
DO:
  /* do not put a trigger in here - the assignment of the person code has
     been overriden ! */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_OtherContact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_OtherContact V-table-Win
ON U1 OF fil_OtherContact IN FRAME F-Main
DO:
  {inc/selfil/sfpsn1.i "Creditor" "OtherContact"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_OtherContact V-table-Win
ON U2 OF fil_OtherContact IN FRAME F-Main
DO:
  {inc/selfil/sfpsn2.i "Creditor" "OtherContact"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_OtherContact V-table-Win
ON U3 OF fil_OtherContact IN FRAME F-Main
DO:
  {inc/selfil/sfpsn3.i "Creditor" "OtherContact"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone1 V-table-Win
ON LEAVE OF fil_Phone1 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone2 V-table-Win
ON LEAVE OF fil_Phone2 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone3 V-table-Win
ON LEAVE OF fil_Phone3 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone4 V-table-Win
ON LEAVE OF fil_Phone4 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Zip
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Zip V-table-Win
ON F3 OF fil_Zip IN FRAME F-Main
DO:
DEF VAR city AS CHAR NO-UNDO.
DEF VAR state AS CHAR NO-UNDO.
DEF VAR postcode AS CHAR NO-UNDO.
  DO WITH FRAME {&FRAME-NAME}:
    city = INPUT fil_City.
    state = INPUT fil_State.
    RUN process\postcode.p( city, state, OUTPUT postcode ).
/*
    IF postcode <> ? THEN DO:
      fil_Zip = postcode.
      DISPLAY fil_Zip.
    END.
*/
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Creditor.Name
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Creditor.Name V-table-Win
ON LEAVE OF Creditor.Name IN FRAME F-Main /* Name */
DO:
  IF SELF:MODIFIED THEN
  DO:
    Creditor.PayeeName:SCREEN-VALUE = INPUT {&SELF-NAME}.
    fil_FullName:SCREEN-VALUE = INPUT {&SELF-NAME}.
    fil_Company:SCREEN-VALUE  = INPUT {&SELF-NAME}.
    RUN person-details-changed.
    SELF:MODIFIED = No.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Creditor.NonAccounting
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Creditor.NonAccounting V-table-Win
ON VALUE-CHANGED OF Creditor.NonAccounting IN FRAME F-Main /* Non Accounting */
DO:
  RUN changed-non-accounting.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

ON "ANY-PRINTABLE", "BACKSPACE", "DELETE-CHARACTER" OF fil_FullName, fil_Company DO:
  APPLY LASTKEY TO SELF.
  RUN person-details-changed.
  RETURN NO-APPLY.
END.

DO WITH FRAME {&FRAME-NAME}:
  edt_Address:READ-ONLY = Yes.
  edt_Address:SENSITIVE = No.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE address-changed V-table-Win 
PROCEDURE address-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN update-address.
  RUN display-address.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'CreditorCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Creditor
           &WHERE = "WHERE Creditor.CreditorCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Creditor"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Creditor"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-creditor. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changed-non-accounting V-table-Win 
PROCEDURE changed-non-accounting :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT Creditor.NonAccounting THEN
    VIEW Creditor.AcctCreditorCode.
  ELSE
    HIDE Creditor.AcctCreditorCode.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clean-previous-contact V-table-Win 
PROCEDURE clean-previous-contact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER prev-person LIKE Person.PersonCode NO-UNDO.

DEF BUFFER AltContact FOR Contact.
DEF BUFFER AltPerson FOR Person.

  FIND AltPerson WHERE AltPerson.PersonCode = prev-person NO-ERROR.
  IF NOT AVAILABLE(AltPerson) THEN RETURN.

  FIND AltContact OF AltPerson WHERE AltContact.ContactType = "CRED"
            EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE(AltContact) THEN DELETE AltContact.

  /* don't delete if they have other contact types */
  FIND FIRST AltContact OF AltPerson NO-LOCK NO-ERROR.
  IF AVAILABLE(AltContact) THEN RETURN.

  DELETE AltPerson.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-address V-table-Win 
PROCEDURE clear-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  edt_Address:SCREEN-VALUE = "".
  fil_City:SCREEN-VALUE = "".
  fil_State:SCREEN-VALUE = "".
  fil_Country:SCREEN-VALUE = "".
  fil_Zip:SCREEN-VALUE = "".
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-creditor.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  RUN dispatch( 'update-record':U ).
  IF mode = "Add" THEN RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-creditor V-table-Win 
PROCEDURE delete-creditor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Creditor EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Creditor THEN DELETE Creditor.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-address V-table-Win 
PROCEDURE display-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR idx         AS INT  NO-UNDO.
  DEF VAR postal-type AS CHAR NO-UNDO.
  
  idx = LOOKUP( INPUT cmb_Address, cmb_Address:LIST-ITEMS ).
  postal-type = ENTRY( idx, cmb_Address:PRIVATE-DATA ).

  FIND PostalDetail WHERE
    PostalDetail.PersonCode = Person.PersonCode AND
    PostalDetail.PostalType = postal-type
    NO-LOCK NO-ERROR.

  ASSIGN
    edt_address = IF AVAILABLE Postaldetail THEN PostalDetail.Address ELSE ""
    fil_City    = IF AVAILABLE Postaldetail THEN PostalDetail.City    ELSE ""
    fil_State   = IF AVAILABLE Postaldetail THEN PostalDetail.State   ELSE ""
    fil_Zip     = IF AVAILABLE Postaldetail THEN PostalDetail.Zip     ELSE ""
    fil_Country = IF AVAILABLE Postaldetail THEN PostalDetail.Country ELSE "".
    
  DISPLAY edt_address fil_City fil_State fil_Zip fil_Country WITH FRAME {&FRAME-NAME}.
  edt_Address:PRIVATE-DATA = postal-type.
   
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-person V-table-Win 
PROCEDURE display-person :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER person-code LIKE Person.PersonCode NO-UNDO.
  FIND Person WHERE Person.PersonCode = person-code NO-LOCK NO-ERROR.

  IF AVAILABLE Person THEN ASSIGN
    fil_FullName = combine-name( Person.PersonTitle, Person.FirstName, Person.MiddleName,
                                     Person.LastName, Person.NameSuffix )
    fil_Company  = Person.Company.
  ELSE ASSIGN
    fil_FullName = ""
    fil_Company  = "".
      
  DISPLAY fil_FullName fil_Company WITH FRAME {&FRAME-NAME}.
  ASSIGN fil_Company:PRIVATE-DATA = STRING( person-code ).
     
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-phone V-table-Win 
PROCEDURE display-phone :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.

  DEF VAR idx        AS INT  NO-UNDO.
  DEF VAR phone-type AS CHAR NO-UNDO.
  DEF VAR sv         AS CHAR NO-UNDO INITIAL "".

  idx = LOOKUP( h-cmb:SCREEN-VALUE, h-cmb:LIST-ITEMS ).
  phone-type = ENTRY( idx, h-cmb:PRIVATE-DATA ).
  
  FIND PhoneDetail WHERE
    PhoneDetail.PersonCode = Person.PersonCode AND
    PhoneDetail.PhoneType  = phone-type
    NO-LOCK NO-ERROR.

  IF AVAILABLE PhoneDetail THEN
    RUN combine-phone( PhoneDetail.cCountryCode, PhoneDetail.cSTDCode, PhoneDetail.Number,
      OUTPUT sv ).
  
  h-fil:SCREEN-VALUE = sv.
  h-fil:PRIVATE-DATA = phone-type.  

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR rights AS LOGI NO-UNDO INITIAL No.

DO WITH FRAME {&FRAME-NAME}:

  RUN changed-non-accounting.

  /* Quite complex logic for handling direct transfer enablement
   * Basically we want to insist on two people being involved, and 
   * the enabling one of them must be from a controlled set of users.
   */

  IF INPUT Creditor.BankDetails <> Creditor.BankDetails
      OR LENGTH(REPLACE(INPUT Creditor.BankDetails, " ","")) < 13 THEN DO:
  ASSIGN
    Creditor.EnableDirectPayment:SCREEN-VALUE = "No"
    Creditor.EnableDirectPayment:CHECKED = No
    Creditor.EnableDirectPayment:SENSITIVE = No
    NO-ERROR.
    IF LENGTH(TRIM(INPUT Creditor.BankDetails)) < 13 THEN DO:
        Creditor.BankDetails:FGCOLOR = 12.
    END.
  END.
  ELSE IF Creditor.EnableDirectPayment THEN DO:
    Creditor.EnableDirectPayment:SENSITIVE = Yes.
  END.
  ELSE DO:
    RUN get-rights IN sys-mgr ("Enable-Direct-Pymts", "ENABLE", OUTPUT rights).
    rights = rights AND (user-name <> Creditor.BankDetailsChangedBy).
    Creditor.EnableDirectPayment:SENSITIVE = rights.
  END.

  /* But with the right office setting ... the weight falls on one set of shoulders */
  IF LENGTH(TRIM(INPUT Creditor.BankDetails)) >= 15 THEN DO:
    {inc/ofc-set.i "Creditor-BankAcc-DPEnable-SinglePerson" "enable-dp-same-user"}
    IF TRIM(enable-dp-same-user) = "Yes" THEN
      Creditor.EnableDirectPayment:SENSITIVE = Yes.
  END.
  
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF Creditor.BankDetails:SENSITIVE THEN DO:
      ASSIGN  Creditor.BankDetails
              Creditor.DcStatementText
              Creditor.DcRemittanceEmail.
  END.
  
  IF Creditor.EnableDirectPayment <> INPUT Creditor.EnableDirectPayment
  THEN DO:
      ASSIGN Creditor.EnableDirectPayment.
  END.

  RUN update-person.

  IF Creditor.PaymentContact <> INT( fil_Company:PRIVATE-DATA ) THEN DO:
    RUN clean-previous-contact( Creditor.PaymentContact ).
    ASSIGN Creditor.PaymentContact = INT( fil_Company:PRIVATE-DATA ).
  END.

  RUN update-address.
  RUN update-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN update-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN update-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN update-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-disable-fields V-table-Win 
PROCEDURE inst-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  DISABLE btn_Clear fil_Company fil_FullName cmb_PaymentStyle .
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, 'window', 'hidden=yes').
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN display-person( IF fil_Company:PRIVATE-DATA = ? AND mode <> "Add" THEN
    Creditor.PaymentContact ELSE INT( fil_Company:PRIVATE-DATA ) ).
  RUN set-default-address-type.
  RUN display-address.
  RUN refresh-phone-types.
  RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
  RUN person-details-changed.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE(Creditor) THEN
    RUN sensitise-address-details(
             CAN-FIND( FIRST Person WHERE Person.PersonCode = Creditor.PaymentContact ) ).

  IF CAN-FIND(FIRST PaymentStyle) THEN DO WITH FRAME {&FRAME-NAME}:
    VIEW cmb_PaymentStyle Creditor.BankDetails Creditor.DcStatementText Creditor.DcRemittanceEmail .
  END.
  ELSE DO:
    HIDE cmb_PaymentStyle Creditor.BankDetails Creditor.DcStatementText Creditor.DcRemittanceEmail .
  END.

  RUN enable-appropriate-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "View" THEN DO:
    RUN dispatch( 'disable-fields':U ).
  END.
  ELSE
    RUN dispatch( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF AVAILABLE(Creditor) THEN DO WITH FRAME {&FRAME-NAME}:
    IF Creditor.BankDetailsChangedBy <> ? THEN DO:
      FIND Usr WHERE Usr.UserName = Creditor.BankDetailsChangedBy NO-LOCK NO-ERROR.
      IF AVAILABLE(Usr) THEN DO:
        FIND Person WHERE Person.PersonCode = Usr.PersonCode NO-LOCK NO-ERROR.
        fil_AcctChangedBy = TRIM(Usr.UserName) + ": " + Person.FirstName + " " + Person.LastName .
      END.
    END.
    IF Creditor.DirectPaymentEnabledBy <> ? THEN DO:
      FIND Usr WHERE Usr.UserName = Creditor.DirectPaymentEnabledBy NO-LOCK NO-ERROR.
      IF AVAILABLE(Usr) THEN DO:
        FIND Person WHERE Person.PersonCode = Usr.PersonCode NO-LOCK NO-ERROR.
        fil_PaymentEnabledBy = TRIM(Usr.UserName) + ": " + Person.FirstName + " " + Person.LastName .
      END.
    END.
    DISPLAY fil_AcctChangedBy fil_PaymentEnabledBy .
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE new-contact-selected V-table-Win 
PROCEDURE new-contact-selected :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER person-code LIKE Person.PersonCode NO-UNDO.

  FIND Person NO-LOCK WHERE Person.PersonCode = person-code NO-ERROR.
  IF NOT AVAILABLE(Person) THEN RETURN.

  DEF VAR priori-list  AS CHAR NO-UNDO.
  DEF VAR i AS INT NO-UNDO.
  priori-list = "POST,MAIN,COUR".
  FIND PostalDetail NO-LOCK OF Person WHERE PostalDetail.PostalType = "PYMT" NO-ERROR.
  IF NOT AVAILABLE(PostalDetail) THEN DO:
    DEF BUFFER PYMTAddress FOR PostalDetail.

     DO i = 1 TO 3 WHILE NOT AVAILABLE(PostalDetail):
       FIND PostalDetail NO-LOCK OF Person WHERE PostalDetail.PostalType = ENTRY( i, priori-list) NO-ERROR.
     END.

     IF AVAILABLE(PostalDetail) THEN DO:
       CREATE PYMTAddress.
       BUFFER-COPY PostalDetail TO PYMTAddress ASSIGN PYMTAddress.PostalType = "PYMT".
     END.
  END.

  RUN person-changed( person-code ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CREATE Creditor.
  ASSIGN Creditor.Active = Yes .
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'display-fields':U ).
  
  CURRENT-WINDOW:TITLE = "Adding a new Creditor".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE person-changed V-table-Win 
PROCEDURE person-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF INPUT PARAMETER person-code LIKE Person.PersonCode NO-UNDO.

  fil_fullName:SCREEN-VALUE = "". fil_company:SCREEN-VALUE = "".
  RUN update-person.
  RUN display-person( person-code ).
  RUN set-default-address-type.
  RUN display-address.
  RUN refresh-phone-types.
  RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
  RUN person-details-changed.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE person-details-changed V-table-Win 
PROCEDURE person-details-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN sensitise-address-details(
    NOT ( INPUT fil_fullname = "" AND INPUT fil_Company = "" ) ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE phone-changed V-table-Win 
PROCEDURE phone-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.
  RUN update-phone( h-cmb, h-fil ).
  RUN display-phone( h-cmb, h-fil ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-assign-statement V-table-Win 
PROCEDURE pre-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:  Save the user name for the user who changed the bank fields.
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF Creditor.EnableDirectPayment 
        AND Creditor.EnableDirectPayment <> INPUT Creditor.EnableDirectPayment
  THEN DO:
    Creditor.DirectPaymentEnabledBy = user-name.
  END.
  ELSE IF Creditor.EnableDirectPayment <> INPUT Creditor.EnableDirectPayment
  THEN DO:
    Creditor.DirectPaymentEnabledBy = user-name.
  END.

  IF Creditor.BankDetails <> INPUT Creditor.BankDetails THEN DO:
    Creditor.BankDetailsChangedBy = user-name.
    Creditor.EnableDirectPayment = No.
    Creditor.DirectPaymentEnabledBy = "".
  END.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize V-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN refresh-address-types.
  RUN refresh-phone-types.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-address-types V-table-Win 
PROCEDURE refresh-address-types :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item         AS CHAR NO-UNDO.
  DEF VAR pd           AS CHAR NO-UNDO.

  cmb_Address:LIST-ITEMS = "".

  FOR EACH PostalType NO-LOCK WHERE
    PostalType.PostalType <> "BILL":
    item = PostalType.Description.
    IF cmb_Address:ADD-LAST( item ) THEN.
    pd = pd + IF pd = "" THEN "" ELSE ",".
    pd = pd + PostalType.PostalType.
  END.
  
  cmb_address:SCREEN-VALUE = cmb_address:ENTRY( 1 ).
  edt_address:PRIVATE-DATA = ENTRY( 1, cmb_address:PRIVATE-DATA ).
  cmb_address:PRIVATE-DATA = pd.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-phone-types V-table-Win 
PROCEDURE refresh-phone-types :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR pd   AS CHAR NO-UNDO.
  DEF VAR li   AS CHAR NO-UNDO.
  DEF VAR sv-list   AS CHAR NO-UNDO INITIAL "".
  DEF VAR pd-list   AS CHAR NO-UNDO INITIAL "".
  DEF VAR sv-list2  AS CHAR NO-UNDO INITIAL "".
  DEF VAR pd-list2  AS CHAR NO-UNDO INITIAL "".
  DEF VAR i         AS INT  NO-UNDO.
    
  DO i = 1 TO NUM-ENTRIES( phone-types ):
    pd = ENTRY( i, phone-types ).
    FIND PhoneType WHERE PhoneType = pd NO-LOCK NO-ERROR.
    li = (IF AVAILABLE(PhoneType) THEN PhoneType.Description ELSE pd).
    IF AVAILABLE(Person) AND CAN-FIND( PhoneDetail OF Person WHERE PhoneDetail.PhoneType = pd)
    THEN DO:
      sv-list = sv-list + "," + li.
      pd-list = pd-list + "," + pd.
    END.
    ELSE DO:
      sv-list2 = sv-list2 + "," + li.
      pd-list2 = pd-list2 + "," + pd.
    END.
  END.
  IF NUM-ENTRIES(sv-list) < 5 THEN DO:
    sv-list = sv-list + sv-list2.
    pd-list = pd-list + pd-list2.
  END.
  sv-list = TRIM(sv-list, ",").
  pd-list = TRIM(pd-list, ",").

  li = "".
  pd = "".
  FOR EACH PhoneType NO-LOCK:
    li = li + (IF li = "" THEN "" ELSE ",") + PhoneType.Description.
    pd = pd + (IF pd = "" THEN "" ELSE ",") + PhoneType.PhoneType.
  END.

  cmb_Phone1:LIST-ITEMS = li.  cmb_Phone2:LIST-ITEMS = li.
  cmb_Phone3:LIST-ITEMS = li.  cmb_Phone4:LIST-ITEMS = li.

  cmb_Phone1:SCREEN-VALUE = ENTRY( 1, sv-list ).
  cmb_Phone2:SCREEN-VALUE = ENTRY( 2, sv-list ).
  cmb_Phone3:SCREEN-VALUE = ENTRY( 3, sv-list ).
  cmb_Phone4:SCREEN-VALUE = ENTRY( 4, sv-list ).
  
  fil_Phone1:PRIVATE-DATA = ENTRY( 1, phone-types ).
  fil_Phone2:PRIVATE-DATA = ENTRY( 2, phone-types ).
  fil_Phone3:PRIVATE-DATA = ENTRY( 3, phone-types ).
  fil_Phone4:PRIVATE-DATA = ENTRY( 4, phone-types ).

  cmb_Phone1:PRIVATE-DATA = pd.  cmb_Phone2:PRIVATE-DATA = pd.
  cmb_Phone3:PRIVATE-DATA = pd.  cmb_Phone4:PRIVATE-DATA = pd.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "CreditorCode" "Creditor" "CreditorCode"}
  {src/adm/template/sndkycas.i "CompanyCode" "Creditor" "CompanyCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Creditor"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-address-details V-table-Win 
PROCEDURE sensitise-address-details :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER enable-it AS LOGI NO-UNDO.
  IF mode = "View" THEN enable-it = No.

  /* Use the editor as the test subject because the default smart object code
     enables widgets when they shouldn't be */

  /* IF edt_address:SENSITIVE <> enable-it THEN */ DO:
    cmb_address:SENSITIVE   = enable-it.
    edt_address:READ-ONLY   = NOT enable-it.
    edt_address:SENSITIVE   = enable-it.
    fil_city:SENSITIVE      = enable-it.
    fil_state:SENSITIVE     = enable-it.
    fil_zip:SENSITIVE       = enable-it.
    fil_Country:SENSITIVE   = enable-it.
    cmb_Phone1:SENSITIVE    = enable-it.
    fil_Phone1:SENSITIVE    = enable-it.
    cmb_Phone2:SENSITIVE    = enable-it.
    fil_Phone2:SENSITIVE    = enable-it.
    cmb_Phone3:SENSITIVE    = enable-it.
    fil_Phone3:SENSITIVE    = enable-it.
    cmb_Phone4:SENSITIVE    = enable-it.
    fil_Phone4:SENSITIVE    = enable-it.
    btn_clear:SENSITIVE     = enable-it.
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-default-address-type V-table-Win 
PROCEDURE set-default-address-type :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR got-address  AS LOGI NO-UNDO.
  DEF VAR priori-list  AS CHAR NO-UNDO.
  DEF VAR default-type AS CHAR NO-UNDO.
  DEF VAR i            AS INT  NO-UNDO.
  DEF VAR default-item AS CHAR NO-UNDO.
  
  priori-list = "PYMT,POST,MAIN,COUR".
  
  DO WHILE NOT got-address AND i < NUM-ENTRIES( priori-list ):
    
    i = i + 1.
    FIND PostalDetail OF Person WHERE PostalDetail.PostalType = ENTRY( i, priori-list )
      NO-LOCK NO-ERROR.
    got-address = AVAILABLE PostalDetail.
    
  END.
  
  IF NOT AVAILABLE PostalDetail THEN
    FIND FIRST PostalDetail OF Person WHERE PostalDetail.PostalType <> "BILL"
      NO-LOCK NO-ERROR.
      
  default-type = IF AVAILABLE PostalDetail THEN
    PostalDetail.PostalType ELSE ENTRY( 1, priori-list ).
  
  IF default-type = "" THEN
  DO:
    FIND FIRST PostalType WHERE PostalType.PostalType <> "BILL"
      NO-LOCK NO-ERROR.
    IF AVAILABLE Postaltype THEN default-type = Postaltype.PostalType.
  END.
  
  ASSIGN default-item = ENTRY( LOOKUP( default-type, cmb_address:PRIVATE-DATA ), cmb_address:LIST-ITEMS )
    NO-ERROR.
  
  cmb_address:SCREEN-VALUE = IF ERROR-STATUS:ERROR THEN "" ELSE default-item.

  RUN display-address.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE special-enable-fields V-table-Win 
PROCEDURE special-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  Creditor.BankDetails:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  Creditor.DcStatementText:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  Creditor.DcRemittanceEmail:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  APPLY 'ENTRY':U TO Creditor.BankDetails IN FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-address V-table-Win 
PROCEDURE update-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR del-record AS LOGI NO-UNDO.

  IF NOT AVAILABLE Person THEN RUN update-person.
  IF NOT AVAILABLE Person THEN RETURN.

  FIND PostalDetail WHERE
    PostalDetail.PersonCode = Person.PersonCode AND
    PostalDetail.PostalType = edt_Address:PRIVATE-DATA NO-ERROR.
  
  ASSIGN FRAME {&FRAME-NAME} edt_address fil_state fil_zip fil_country fil_city.
  del-record = edt_address = "" AND fil_zip = "" AND fil_state = "" AND fil_country = "" AND fil_City = "".

  IF del-record THEN
    IF AVAILABLE PostalDetail THEN
      DELETE PostalDetail.
    ELSE DO: END.
  ELSE
  DO:
    IF NOT AVAILABLE PostalDetail THEN
    DO:
      CREATE PostalDetail.
      ASSIGN
        PostalDetail.PersonCode = Person.PersonCode
        PostalDetail.PostalType = edt_Address:PRIVATE-DATA.
    END.

    ASSIGN
      PostalDetail.Address = TRIM( edt_Address )
      PostalDetail.City    = fil_city
      PostalDetail.State   = fil_State
      PostalDetail.Zip     = fil_Zip
      PostalDetail.Country = fil_Country.      
  
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-person V-table-Win 
PROCEDURE update-person :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR del-record AS LOGI NO-UNDO.

  IF NOT AVAILABLE Creditor THEN RETURN.

  FIND Person WHERE Person.PersonCode = INT( fil_Company:PRIVATE-DATA ) NO-ERROR.
  
  ASSIGN FRAME {&FRAME-NAME} fil_fullName fil_company edt_address fil_City.
  del-record = fil_FullName = "" AND fil_Company = ""
                   AND edt_address = "" AND fil_City = "".

  IF del-record THEN
    IF AVAILABLE Person THEN
    DO:
      IF NOT CAN-FIND( FIRST Contact OF Person WHERE Contact.ContactType <> "CRED" )
        THEN DELETE Person.
      fil_Company:PRIVATE-DATA = STRING( 0 ).
    END.
    ELSE DO: END.
  ELSE
  DO:
    IF NOT AVAILABLE Person THEN CREATE Person.
    FIND Contact NO-LOCK OF Person WHERE Contact.ContactType = "CRED" NO-ERROR.
    IF NOT AVAILABLE(Contact) THEN DO:
      CREATE Contact.
      ASSIGN
        Contact.PersonCode  = Person.PersonCode
        Contact.ContactType = "CRED".
    END.

    FIND FIRST Contact NO-LOCK OF Person WHERE Contact.ContactType <> "CRED"
        AND CAN-FIND( ContactType OF Contact WHERE NOT ContactType.SystemCode) NO-ERROR.
    Person.SystemContact = NOT AVAILABLE(Contact).

    RUN split-name( INPUT fil_FullName, OUTPUT Person.PersonTitle, OUTPUT Person.FirstName,
            OUTPUT Person.MiddleName, OUTPUT Person.LastName, OUTPUT Person.NameSuffix ).

    ASSIGN
      Person.Company = fil_Company
      fil_Company:PRIVATE-DATA = STRING( Person.PersonCode ).
      
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-phone V-table-Win 
PROCEDURE update-phone :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.
  
  DEF VAR del-record AS LOGI NO-UNDO.

  IF NOT AVAILABLE Person THEN RUN update-person.
  IF NOT AVAILABLE Person THEN RETURN.

  FIND PhoneDetail WHERE
    PhoneDetail.PersonCode = Person.PersonCode AND
    PhoneDetail.PhoneType = h-fil:PRIVATE-DATA   NO-ERROR.

  del-record = TRIM( h-fil:SCREEN-VALUE ) = "".

  IF del-record THEN DO:
    IF AVAILABLE PhoneDetail THEN DELETE PhoneDetail.
  END.
  ELSE DO:
    IF NOT AVAILABLE PhoneDetail THEN
    DO:
      CREATE PhoneDetail.
      ASSIGN
        PhoneDetail.PersonCode = Person.PersonCode
        PhoneDetail.PhoneType  = h-fil:PRIVATE-DATA.
    END.
    
    RUN split-phone( h-fil:SCREEN-VALUE, OUTPUT PhoneDetail.cCountryCode,
                      OUTPUT PhoneDetail.cSTDCode, OUTPUT PhoneDetail.Number ).
      
  END.

  /* Reflect changes to other linked numbers on screen */
  
  DEF VAR update-type AS CHAR NO-UNDO. /* Do not delete this variable!! */
  update-type = h-fil:PRIVATE-DATA.

  IF fil_Phone1:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
        
  IF fil_Phone2:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
        
  IF fil_Phone3:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
        
  IF fil_Phone4:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
  have-records = mode = "Add".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-creditor V-table-Win 
PROCEDURE verify-creditor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

