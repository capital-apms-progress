&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description: 
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Log3 RP.Char1 RP.Int1 RP.Int2 RP.Log4 ~
RP.Log5 RP.Log9 RP.Int5 RP.Int6 RP.Log1 RP.Log2 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}~
 ~{&FP1}Int5 ~{&FP2}Int5 ~{&FP3}~
 ~{&FP1}Int6 ~{&FP2}Int6 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-30 RECT-28 RECT-29 RECT-27 cmb_Month-1 ~
cmb_Month-n tgl_TestSubBal tgl_TestNoGL tgl_TestNoSub tgl_TestGroups btn_ok 
&Scoped-Define DISPLAYED-FIELDS RP.Log3 RP.Char1 RP.Int1 RP.Int2 RP.Log4 ~
RP.Log5 RP.Log9 RP.Int5 RP.Int6 RP.Log1 RP.Log2 
&Scoped-Define DISPLAYED-OBJECTS cmb_Month-1 cmb_Month-n tgl_TestSubBal ~
tgl_TestNoGL tgl_TestNoSub tgl_TestGroups 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_ok DEFAULT 
     LABEL "&OK" 
     SIZE 9.72 BY 1.05.

DEFINE VARIABLE cmb_Month-1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Months from" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "Item 1" 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Month-n AS CHARACTER FORMAT "X(256)":U 
     LABEL "to" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "Item 1" 
     SIZE 16 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-27
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 54.86 BY 19.2.

DEFINE RECTANGLE RECT-28
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 53.14 BY 6.6.

DEFINE RECTANGLE RECT-29
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 53.14 BY 5.

DEFINE RECTANGLE RECT-30
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 53.14 BY 1.9.

DEFINE VARIABLE tgl_TestGroups AS LOGICAL INITIAL no 
     LABEL "Test for closed groups which do not balance" 
     VIEW-AS TOGGLE-BOX
     SIZE 42.86 BY .8.

DEFINE VARIABLE tgl_TestNoGL AS LOGICAL INITIAL no 
     LABEL "Test for subs transactions where there is no GL posting" 
     VIEW-AS TOGGLE-BOX
     SIZE 41.14 BY .8.

DEFINE VARIABLE tgl_TestNoSub AS LOGICAL INITIAL no 
     LABEL "Test for GL transactions where there is no posting to subs" 
     VIEW-AS TOGGLE-BOX
     SIZE 42.86 BY .8.

DEFINE VARIABLE tgl_TestSubBal AS LOGICAL INITIAL no 
     LABEL "Test for control a/c balance not equal subs" 
     VIEW-AS TOGGLE-BOX
     SIZE 32.57 BY .8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Log3 AT ROW 1.2 COL 3.86 HELP
          ""
          LABEL "Check Transaction, Balance and Summary records"
          VIEW-AS TOGGLE-BOX
          SIZE 39.43 BY .8
     RP.Char1 AT ROW 2.2 COL 7.86 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "All Entity Types", "",
"Ledger Accounts", "L":U,
"Property Accounts", "P":U,
"Project Accounts", "J":U,
"Tenant Accounts", "T":U,
"Creditor Accounts", "C":U
          SIZE 16.57 BY 5.4
     RP.Int1 AT ROW 2.2 COL 36.14 COLON-ALIGNED HELP
          ""
          LABEL "Entity range from" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1.05
     RP.Int2 AT ROW 3.2 COL 36.14 COLON-ALIGNED HELP
          ""
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1.05
     cmb_Month-1 AT ROW 4.6 COL 36.14 COLON-ALIGNED
     cmb_Month-n AT ROW 5.6 COL 36.14 COLON-ALIGNED
     RP.Log4 AT ROW 8.4 COL 3.86 HELP
          ""
          LABEL "Debtors Control A/c"
          VIEW-AS TOGGLE-BOX
          SIZE 17.14 BY .8
     RP.Log5 AT ROW 9.2 COL 3.86 HELP
          ""
          LABEL "Creditors Control A/c"
          VIEW-AS TOGGLE-BOX
          SIZE 17.14 BY .8
     tgl_TestSubBal AT ROW 10.4 COL 7.86
     tgl_TestNoGL AT ROW 11.2 COL 7.86
     tgl_TestNoSub AT ROW 12 COL 7.86
     tgl_TestGroups AT ROW 12.8 COL 7.86
     RP.Log9 AT ROW 14.3 COL 3.57
          LABEL "Check batches balance"
          VIEW-AS TOGGLE-BOX
          SIZE 18.72 BY 1.05
     RP.Int5 AT ROW 15.5 COL 25.86 COLON-ALIGNED HELP
          ""
          LABEL "From batch" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 7 BY 1
     RP.Int6 AT ROW 15.5 COL 36.14 COLON-ALIGNED HELP
          ""
          LABEL "to" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 7 BY 1
     RP.Log1 AT ROW 18.9 COL 3.86
          LABEL "Print Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 12 BY .9
     RP.Log2 AT ROW 18.9 COL 24.72 HELP
          ""
          LABEL "Fix problems"
          VIEW-AS TOGGLE-BOX
          SIZE 11.57 BY .9
     btn_ok AT ROW 18.9 COL 45.57
     RECT-30 AT ROW 14.8 COL 2.14
     RECT-28 AT ROW 1.4 COL 2.14
     RECT-29 AT ROW 9 COL 2.14
     RECT-27 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_ok.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 21.4
         WIDTH              = 61.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int6 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log9 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON CHOOSE OF btn_ok IN FRAME F-Main /* OK */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Month-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month-1 V-table-Win
ON U1 OF cmb_Month-1 IN FRAME F-Main /* Months from */
DO:
  {inc/selcmb/scmths1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month-1 V-table-Win
ON U2 OF cmb_Month-1 IN FRAME F-Main /* Months from */
DO:
  {inc/selcmb/scmths2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Month-n
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month-n V-table-Win
ON U1 OF cmb_Month-n IN FRAME F-Main /* to */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month-n V-table-Win
ON U2 OF cmb_Month-n IN FRAME F-Main /* to */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log3 V-table-Win
ON VALUE-CHANGED OF RP.Log3 IN FRAME F-Main /* Check Transaction, Balance and Summary records */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Debtors Control A/c */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log5 V-table-Win
ON VALUE-CHANGED OF RP.Log5 IN FRAME F-Main /* Creditors Control A/c */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log9
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log9 V-table-Win
ON VALUE-CHANGED OF RP.Log9 IN FRAME F-Main /* Check batches balance */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_TestGroups
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_TestGroups V-table-Win
ON VALUE-CHANGED OF tgl_TestGroups IN FRAME F-Main /* Test for closed groups which do not balance */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Log3 THEN
    ENABLE RP.Char1 RP.Int1 RP.Int2 cmb_Month-1 cmb_Month-n .
  ELSE
    DISABLE RP.Char1 RP.Int1 RP.Int2 cmb_Month-1 cmb_Month-n .

  IF INPUT RP.Log4 OR INPUT RP.Log5 THEN
    ENABLE tgl_TestGroups tgl_TestNoGL tgl_TestNoSub tgl_TestSubBal.
  ELSE
    DISABLE tgl_TestGroups tgl_TestNoGL tgl_TestNoSub tgl_TestSubBal.

  IF INPUT RP.Log9 THEN
    ENABLE RP.Int5 RP.Int6 .
  ELSE
    DISABLE RP.Int5 RP.Int6 .

  IF INPUT RP.Log9 OR INPUT RP.Log3 OR INPUT tgl_TestGroups THEN
    ENABLE RP.Log2 .
  ELSE
    DISABLE RP.Log2 .

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RP.Char2 = STRING( tgl_TestSubBal,"Yes/No") + ","
           + STRING( tgl_TestNoGL,"Yes/No") + ","
           + STRING( tgl_TestNoSub, "Yes/No") + ","
           + STRING( tgl_TestGroups, "Yes/No").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

&SCOP REPORT-ID "System Integrity Check"

  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.

  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
  
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Log1     = Yes
      RP.Int1     = 0
      RP.Int2     = 99999.
  END.

DEF VAR rp-char2 AS CHAR NO-UNDO.
  rp-char2 = RP.Char2 + ",,,".
  tgl_TestSubBal = (IF ENTRY(1,rp-char2) = "Yes" THEN Yes ELSE No).
  tgl_TestNoGl   = (IF ENTRY(2,rp-char2) = "Yes" THEN Yes ELSE No).
  tgl_TestNoSub  = (IF ENTRY(3,rp-char2) = "Yes" THEN Yes ELSE No).
  tgl_TestGroups = (IF ENTRY(4,rp-char2) = "Yes" THEN Yes ELSE No).

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN enable-appropriate-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR report-options AS CHAR NO-UNDO.
  
  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  ASSIGN FRAME {&FRAME-NAME} tgl_TestGroups tgl_TestNoGL tgl_TestNoSub tgl_TestSubBal .

  RUN dispatch( 'update-record':U ).

  report-options = "Entity-Type," + RP.Char1
                 + "~nEntity-Range," + STRING(RP.Int1) + "," + STRING(RP.Int2)
                 + "~nMonth-Range," + STRING(RP.Int3) + "," + STRING(RP.Int4)
                 + "~nAction," + (IF RP.Log2 THEN "Fix" ELSE "Check")
                 + (IF RP.Log1 THEN "~nPreview" ELSE "")
                 + (IF RP.Log3 THEN "~nCheck-TBS" ELSE "")
                 + (IF RP.Log4 THEN "~nCheck-Db" ELSE "")
                 + (IF RP.Log5 THEN "~nCheck-Cr" ELSE "")
                 + (IF tgl_TestSubBal THEN  "~nTest-Sub-Bal" ELSE "")
                 + (IF tgl_TestNoGL THEN    "~nTest-No-GL" ELSE "")
                 + (IF tgl_TestNoSub THEN   "~nTest-No-Sub" ELSE "")
                 + (IF tgl_TestGroups THEN  "~nTest-Groups" ELSE "")
                 + (IF RP.Log9 THEN "~nCheck-Batches," + STRING(RP.Int5) + "," + STRING(RP.Int6) ELSE "")
                 .
  RUN process/one-off/integchk.p ( report-options ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


