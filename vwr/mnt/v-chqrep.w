&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description:

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&SCOPED-DEFINE REPORT-ID "Cheque Reprint"

DEF BUFFER ToBank FOR BankAccount.
DEF VAR pdf-support AS LOGICAL INIT YES NO-UNDO.
DEF VAR pdf-output AS LOGICAL INIT NO NO-UNDO.

{inc/ofc-this.i}

{inc/method/m-bankimportfile-gen.i}

{inc/ofc-set.i "PDF-Output-Directory" "pdf-output-directory"}
IF NOT AVAILABLE(OfficeSetting) THEN
    pdf-support = NO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Int1 RP.Int2 RP.Log4 RP.Log1 RP.Int3 ~
RP.Log2 RP.Date1 RP.Char1 RP.Log3 RP.Log7 RP.Char3 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_bnkact cmb_PaymentStyle cmb_bnkact-2 ~
btn-select-file btn_print cbx_pdfoutput RECT-27 
&Scoped-Define DISPLAYED-FIELDS RP.Int1 RP.Int2 RP.Log4 RP.Log1 RP.Int3 ~
RP.Log2 RP.Date1 RP.Char1 RP.Log3 RP.Log7 RP.Char3 
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define DISPLAYED-OBJECTS cmb_bnkact cmb_PaymentStyle cmb_bnkact-2 ~
cbx_pdfoutput 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn-select-file 
     LABEL "Browse" 
     SIZE 7.43 BY 1
     FONT 9.

DEFINE BUTTON btn_print DEFAULT 
     LABEL "&OK" 
     SIZE 9.43 BY .9
     FONT 9.

DEFINE VARIABLE cbx_pdfoutput AS LOGICAL INITIAL no
     LABEL "PDF remittance"
     VIEW-AS TOGGLE-BOX
     SIZE 14 BY .8 TOOLTIP "Generate the remittance advice from this cheque batch in PDF format" NO-UNDO.

DEFINE VARIABLE cmb_bnkact AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 51 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_bnkact-2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 51 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_PaymentStyle AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 44.57 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-27
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 65.72 BY 18.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     cmb_bnkact AT ROW 1.1 COL 15 NO-LABEL
     RP.Int1 AT ROW 2.4 COL 13.29 COLON-ALIGNED NO-LABEL FORMAT "999999"
          VIEW-AS FILL-IN 
          SIZE 7 BY 1
     RP.Int2 AT ROW 2.4 COL 22.72 COLON-ALIGNED NO-LABEL FORMAT "999999"
          VIEW-AS FILL-IN 
          SIZE 7 BY 1
     RP.Log4 AT ROW 3.7 COL 4.29 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "One Payment Style", no,
"Force Payment by", yes
          SIZE 16.57 BY 1.6
     cmb_PaymentStyle AT ROW 4.05 COL 19.43 COLON-ALIGNED NO-LABEL
     RP.Log1 AT ROW 5.55 COL 2.14 HELP
          ""
          LABEL "Renumber cheques into new range starting at"
          VIEW-AS TOGGLE-BOX
          SIZE 33.86 BY .8
          FONT 10
     RP.Int3 AT ROW 5.55 COL 33.86 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "999999"
          VIEW-AS FILL-IN 
          SIZE 7.57 BY 1
     cmb_bnkact-2 AT ROW 6.65 COL 15 NO-LABEL
     RP.Log2 AT ROW 8.75 COL 2.14 HELP
          ""
          LABEL "Change cheque dates to"
          VIEW-AS TOGGLE-BOX
          SIZE 20 BY 1
          FONT 10
     RP.Date1 AT ROW 8.75 COL 20.72 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99/99/9999"
          VIEW-AS FILL-IN 
          SIZE 11.57 BY 1
     RP.Char1 AT ROW 10.15 COL 15 NO-LABEL
          VIEW-AS EDITOR NO-WORD-WRAP SCROLLBAR-VERTICAL
          SIZE 51.14 BY 4.05
     RP.Log3 AT ROW 14.25 COL 2.43 HELP
          ""
          LABEL "Re-print the cheques while renumbering them"
          VIEW-AS TOGGLE-BOX
          SIZE 38.29 BY .8
     RP.Log7 AT ROW 15.5 COL 4.57 HELP
          ""
          LABEL "Generate Bank Import File"
          VIEW-AS TOGGLE-BOX
          SIZE 21.43 BY .8
     RP.Char3 AT ROW 16.4 COL 10 COLON-ALIGNED HELP
          ""
          LABEL "Directory" FORMAT "X(120)"
          VIEW-AS FILL-IN 
          SIZE 46.29 BY 1
     btn-select-file AT ROW 16.4 COL 58.72
     btn_print AT ROW 17.75 COL 56.86
     cbx_pdfoutput AT ROW 17.95 COL 42.86
     RECT-27 AT ROW 1 COL 1
     "To Bank A/C:" VIEW-AS TEXT
          SIZE 9.72 BY .8 AT ROW 6.75 COL 4.43
          FONT 10
     "to" VIEW-AS TEXT
          SIZE 1.72 BY 1 AT ROW 2.4 COL 22.43
          FONT 10
     "Message Text:" VIEW-AS TEXT
          SIZE 10.57 BY 1 AT ROW 10 COL 4.14
          FONT 10
     "From Bank A/C:" VIEW-AS TEXT
          SIZE 11.57 BY .9 AT ROW 1.2 COL 3.29
          FONT 10
     "Reprint Cheques" VIEW-AS TEXT
          SIZE 12 BY 1 AT ROW 2.5 COL 2.57
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_print.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 23.75
         WIDTH              = 77.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR COMBO-BOX cmb_bnkact IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_bnkact-2 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN RP.Date1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR RADIO-SET RP.Log4 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR TOGGLE-BOX RP.Log7 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn-select-file
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-select-file V-table-Win
ON CHOOSE OF btn-select-file IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN renumber-cheque-range.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbx_pdfoutput
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbx_pdfoutput V-table-Win
ON VALUE-CHANGED OF cbx_pdfoutput IN FRAME F-Main /* OK */
DO:
DO WITH FRAME {&FRAME-NAME}:
  pdf-output = cbx_pdfoutput:CHECKED IN FRAME {&FRAME-NAME}.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON LEAVE OF RP.Char3 IN FRAME F-Main /* Directory */
DO:
      RP.Char3 = SUBSTRING( INPUT RP.Char3, 1, R-INDEX(INPUT RP.Char3, "\" ) ).
    
      DISPLAY RP.Char3.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_bnkact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact V-table-Win
ON U1 OF cmb_bnkact IN FRAME F-Main
DO:
  {inc/selcmb/scbnk1.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact V-table-Win
ON U2 OF cmb_bnkact IN FRAME F-Main
DO:
  {inc/selcmb/scbnk2.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact V-table-Win
ON VALUE-CHANGED OF cmb_bnkact IN FRAME F-Main
DO:
  /* Display Import File Generation option? */
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_bnkact-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact-2 V-table-Win
ON U1 OF cmb_bnkact-2 IN FRAME F-Main
DO:
  {inc/selcmb/scchq1.i "RP" "Char3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact-2 V-table-Win
ON U2 OF cmb_bnkact-2 IN FRAME F-Main
DO:
  {inc/selcmb/scchq2.i "RP" "Char3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact-2 V-table-Win
ON VALUE-CHANGED OF cmb_bnkact-2 IN FRAME F-Main
DO:
  /* Display Import File Generation option? */
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PaymentStyle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON U1 OF cmb_PaymentStyle IN FRAME F-Main
DO:
  {inc/selcmb/scpsty1.i "RP" "Char6"}
  
  /* Display Import File Generation option? */
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON U2 OF cmb_PaymentStyle IN FRAME F-Main
DO:
  {inc/selcmb/scpsty2.i "RP" "Char6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON VALUE-CHANGED OF cmb_PaymentStyle IN FRAME F-Main
DO:
  /* Display Import File Generation option? */
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Int1 */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT RP.Int2 < INPUT RP.Int1 AND INPUT RP.Int1 <> RP.Int1 THEN DO:
      RP.Int2:SCREEN-VALUE = STRING( INPUT RP.Int1, "999999").
    END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log1 V-table-Win
ON VALUE-CHANGED OF RP.Log1 IN FRAME F-Main /* Renumber cheques into new range starting at */
DO:
  RUN renumber-changed.

  /* Display Import File Generation option? */
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Change cheque dates to */
DO:
  RUN redate-changed.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log3 V-table-Win
ON VALUE-CHANGED OF RP.Log3 IN FRAME F-Main /* Re-print the cheques while renumbering them */
DO:
  /* Display Import File Generation option? */
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Log4 */
DO:
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log7 V-table-Win
ON VALUE-CHANGED OF RP.Log7 IN FRAME F-Main /* Generate Bank Import File */
DO:
  /* Display Import File Generation option? */
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

IF NOT pdf-support THEN DO:
  cbx_pdfoutput:VISIBLE IN FRAME {&FRAME-NAME} = NO.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-import-file-gen V-table-Win 
PROCEDURE enable-import-file-gen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Display Import File Generation option? */
/* Payer Bank Account */
DEFINE VARIABLE item-idx AS INTEGER INITIAL 0 NO-UNDO.
DEFINE VARIABLE v-bankaccount LIKE BankAccount.BankAccount INITIAL 0 NO-UNDO.
DEFINE VARIABLE v-bank AS INTEGER INITIAL 0 NO-UNDO.
DEFINE VARIABLE v-branch AS INTEGER INITIAL 0 NO-UNDO.
DEFINE VARIABLE v-account AS INTEGER INITIAL 0 NO-UNDO.
DEFINE VARIABLE v-suffix AS INTEGER INITIAL 0 NO-UNDO.
/* Payment Style */
DEFINE VARIABLE v-allorone-payment-style AS CHARACTER INITIAL '' NO-UNDO.
DEFINE VARIABLE v-payment-style AS CHARACTER INITIAL '' NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  FIND CURRENT RP SHARE-LOCK NO-ERROR.

  IF INPUT RP.Log3 THEN DO:
      /* Check Payer Bank Account */
      IF INPUT RP.Log1 THEN DO:
          item-idx = cmb_bnkact-2:LOOKUP( INPUT cmb_bnkact-2 ).
          IF item-idx > 0 THEN
            FIND BankAccount WHERE ROWID( BankAccount ) =
              TO-ROWID( ENTRY( item-idx, cmb_bnkact-2:PRIVATE-DATA ) ) NO-LOCK NO-ERROR.    
      END.
      IF NOT INPUT RP.Log1 THEN DO:
          item-idx = cmb_bnkact:LOOKUP( INPUT cmb_bnkact ).
          IF item-idx > 0 THEN
            FIND BankAccount WHERE ROWID( BankAccount ) =
              TO-ROWID( ENTRY( item-idx, cmb_bnkact:PRIVATE-DATA ) ) NO-LOCK NO-ERROR.    
      END.
    
      IF AVAILABLE BankAccount THEN DO:
          RUN get-bankbranchaccountsuffix (
              BankAccount.BankAccount,
              OUTPUT v-bank,
              OUTPUT v-branch,
              OUTPUT v-account,
              OUTPUT v-suffix
          ).
          /* IF RETURN-VALUE = "FAIL"  THEN RETURN "FAIL". */
      END.
    
      IF v-bank = 3 THEN DO:
            item-idx = cmb_PaymentStyle:LOOKUP( INPUT cmb_PaymentStyle ).
            IF item-idx > 0 THEN
              FIND PaymentStyle WHERE ROWID( PaymentStyle ) =
                TO-ROWID( ENTRY( item-idx, cmb_PaymentStyle:PRIVATE-DATA ) ) NO-LOCK NO-ERROR.

          IF NOT AVAILABLE PaymentStyle OR PaymentStyle.PaymentStyle <> 'DD  ' THEN DO:
            RP.Log7 = No. DISPLAY RP.Log7.
            HIDE RP.Log7 RP.Char3 btn-select-file.
          END.
          ELSE DO:
            VIEW RP.Log7. ENABLE RP.LOG7.
            IF INPUT RP.Log7 THEN DO:
                VIEW RP.Char3 btn-select-file. ENABLE RP.Char3 btn-select-file.
            END.
            ELSE HIDE RP.Char3 btn-select-file.
          END.
      END.
      ELSE DO:
          RP.Log7 = NO. DISPLAY RP.Log7.
          HIDE RP.Log7 RP.Char3 btn-select-file.
      END.
  END.
  ELSE DO:
    RP.Log7 = NO. DISPLAY RP.Log7.
    HIDE RP.Log7 RP.Char3 btn-select-file.
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-from-bank-account V-table-Win 
PROCEDURE get-from-bank-account :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item-idx AS INT NO-UNDO.

  item-idx = cmb_bnkact:LOOKUP( INPUT cmb_bnkact ).
  IF item-idx = 0 THEN RETURN.
    
  FIND BankAccount WHERE ROWID( BankAccount ) =
    TO-ROWID( ENTRY( item-idx, cmb_bnkact:PRIVATE-DATA ) ) NO-LOCK.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-to-bank-account V-table-Win 
PROCEDURE get-to-bank-account :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item-idx AS INT NO-UNDO.

  item-idx = cmb_bnkact-2:LOOKUP( INPUT cmb_bnkact-2 ).
  IF item-idx = 0 THEN RETURN.
    
  FIND ToBank WHERE ROWID( ToBank ) =
    TO-ROWID( ENTRY( item-idx, cmb_bnkact-2:PRIVATE-DATA ) ) NO-LOCK.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN renumber-changed.
  RUN redate-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR user-name AS CHAR NO-UNDO.
  
  RUN get-username IN sys-mgr( OUTPUT user-name ).

  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.

  IF NOT AVAILABLE RP THEN DO:
  
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Date1    = TODAY.

    {inc/ofc-this.i}
    RP.Char1 = "Please forward all invoices to:~n" + Office.StreetAddress .
      
  END.

/*  RUN update-bank-accounts. */
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN enable-import-file-gen.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE redate-changed V-table-Win 
PROCEDURE redate-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RP.Date1:SENSITIVE IN FRAME {&FRAME-NAME} = 
    INPUT FRAME {&FRAME-NAME} RP.Log2 = "Yes".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE renumber-changed V-table-Win 
PROCEDURE renumber-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME} :
  RP.Int3:SENSITIVE = INPUT RP.Log1 = "Yes".

  IF INPUT RP.Log1 THEN 
    VIEW cmb_bnkact-2.
  ELSE
    HIDE cmb_bnkact-2.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE renumber-cheque-range V-table-Win 
PROCEDURE renumber-cheque-range :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.
DEF VAR i              AS INT  NO-UNDO.
DEF VAR n-cheques      AS INT  NO-UNDO INITIAL 0.
DEF VAR change-no      AS LOGI NO-UNDO.
DEF VAR change-acct    AS LOGI NO-UNDO.
DEF VAR msg            AS CHAR NO-UNDO.
DEF VAR start-printing AS LOGI NO-UNDO INIT No.

DEFINE VARIABLE item-idx AS INTEGER INITIAL 0 NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:            
    item-idx = cmb_PaymentStyle:LOOKUP( INPUT cmb_PaymentStyle ).
    IF item-idx > 0 THEN
        FIND PaymentStyle WHERE ROWID( PaymentStyle ) =
            TO-ROWID( ENTRY( item-idx, cmb_PaymentStyle:PRIVATE-DATA ) ) NO-LOCK NO-ERROR.    

        IF AVAILABLE PaymentStyle THEN DO:
            FIND CURRENT RP SHARE-LOCK.
            RP.Char6 = PaymentStyle.PaymentStyle.
    END.
END.

RUN verify-run.
IF RETURN-VALUE = "FAIL" THEN RETURN.
RUN dispatch( 'update-record':U ).

DEF VAR f1 AS INT NO-UNDO.
DEF VAR f2 AS INT NO-UNDO.
DEF VAR t1 AS INT NO-UNDO.
DEF VAR t2 AS INT NO-UNDO.
DEF VAR upwards AS LOGI NO-UNDO INITIAL Yes.
DEF VAR offset  AS INT INIT 0 NO-UNDO.
  f1 = RP.Int1.
  f2 = RP.Int2.
  t1 = RP.Int3.
  t2 = t1 + f2 - f1.
  offset = t1 - f1.

  IF t1 >= f1 AND t1 <= f2 THEN upwards = No.

  change-no   = RP.Log1 AND f1 <> t1.
  change-acct = RP.Log1 AND (BankAccount.BankAccountCode <> ToBank.BankAccountCode).
  IF RP.Log3 THEN DO:
    /* Count the cheques */
    /* Check Voucher.PaymentStyle of cheque against Forced Payment Style
         IF Forced Payment Style = 'DD' AND Creditor.EnableDirectPayment
         OR Voucher.PaymentStyle = PaymentStyle selected
         then cheque will be printed */
    IF INPUT RP.Log4 THEN DO: /* If Force PaymentStyle */
        IF TRIM(RP.Char6) = 'DD' THEN DO:
            FOR EACH Cheque WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
                AND Cheque.ChequeNo >= f1 AND Cheque.ChequeNo <= f2
                AND NOT Cheque.Cancelled NO-LOCK:
    
                IF CAN-FIND(FIRST Creditor
                            WHERE Creditor.CreditorCode = Cheque.CreditorCode
                              AND Creditor.EnableDirectPayment) THEN n-cheques = n-cheques + 1.
            END.
        END.
      ELSE DO:
          FOR EACH Cheque WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
              AND Cheque.ChequeNo >= f1 AND Cheque.ChequeNo <= f2
              AND NOT Cheque.Cancelled NO-LOCK:

               n-cheques = n-cheques + 1.
          END.
      END.
    END.
    ELSE DO: /* Else check Voucher.PaymentStyle and BankImport */
        FOR EACH Cheque WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
            AND Cheque.ChequeNo >= f1 AND Cheque.ChequeNo <= f2
            AND NOT Cheque.Cancelled NO-LOCK:

            FIND FIRST Voucher WHERE Voucher.BankAccountCode = Cheque.BankAccountCode
                AND Voucher.ChequeNo = Cheque.ChequeNo NO-LOCK NO-ERROR.

            IF AVAILABLE Voucher THEN DO:
                IF Voucher.PaymentStyle = RP.Char6 THEN n-cheques = n-cheques + 1.
            END.
        END.
    END.
    msg = "There are " + STRING( n-cheques ) + " cheques to printed~n" +
          "Start printing Now ?~n~n~n" +
          "The printer will wait for you to insert cheque paper.".
  END.

  IF change-no OR change-acct OR RP.Log2 OR RP.Log4 THEN
modify-cheques:
  DO /*TRANSACTION */ ON ERROR UNDO modify-cheques, RETURN "FAIL":

    DEF VAR cheque-no AS INT NO-UNDO.

    IF upwards THEN
      FIND FIRST Cheque EXCLUSIVE-LOCK WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
                       AND Cheque.ChequeNo >= f1 AND Cheque.ChequeNo <= f2 NO-ERROR.
    ELSE
      FIND LAST Cheque EXCLUSIVE-LOCK WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
                       AND Cheque.ChequeNo >= f1 AND Cheque.ChequeNo <= f2 NO-ERROR.

    DO WHILE AVAILABLE(Cheque):
      IF RP.Log1 THEN DO:
        cheque-no = Cheque.ChequeNo + offset.
        FOR EACH Voucher WHERE Voucher.BankAccountCode = Cheque.BankAccountCode
                           AND Voucher.ChequeNo = Cheque.ChequeNo EXCLUSIVE-LOCK:
          IF change-no THEN Voucher.ChequeNo = cheque-no.
          IF change-acct THEN Voucher.BankAccountCode = ToBank.BankAccountCode.
        END.
        IF change-no THEN Cheque.ChequeNo = cheque-no.
        IF change-acct THEN Cheque.BankAccountCode = ToBank.BankAccountCode.
      END.
      IF RP.Log2 THEN Cheque.Date = RP.Date1.
      IF upwards THEN
        FIND NEXT Cheque EXCLUSIVE-LOCK WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
                       AND Cheque.ChequeNo >= f1 AND Cheque.ChequeNo <= f2 NO-ERROR.
      ELSE
        FIND PREV Cheque EXCLUSIVE-LOCK WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
                       AND Cheque.ChequeNo >= f1 AND Cheque.ChequeNo <= f2 NO-ERROR.
    END.

    /* IF Forced Payment Style option set then go through vouchers for each cheque
       in the range specified and update paymentstyle */
    IF RP.Log4 AND LENGTH(RP.Char6) > 0 THEN DO:
        FIND FIRST Cheque EXCLUSIVE-LOCK WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
                     AND Cheque.ChequeNo >= f1 AND Cheque.ChequeNo <= f2 NO-ERROR.

        DO WHILE AVAILABLE(Cheque):
        /* IF ForcedPaymentStyle */
        IF RP.Log4 THEN DO:
            /* IF forced-style 'DD'
               THEN only alter Voucher.PaymentStyle if Creditor.EnableDirectPayment */
            IF TRIM(RP.Char6) = 'DD' THEN DO:
                IF CAN-FIND(FIRST Creditor
                            WHERE Creditor.CreditorCode = Cheque.CreditorCode
                              AND Creditor.EnableDirectPayment) THEN DO:
                    FOR EACH Voucher WHERE Voucher.BankAccountCode = Cheque.BankAccountCode
                                       AND Voucher.ChequeNo = Cheque.ChequeNo EXCLUSIVE-LOCK:
                        Voucher.PaymentStyle = RP.Char6.
                    END.
                END.
            END.
            ELSE DO:
                FOR EACH Voucher WHERE Voucher.BankAccountCode = Cheque.BankAccountCode
                                   AND Voucher.ChequeNo = Cheque.ChequeNo EXCLUSIVE-LOCK:
                    Voucher.PaymentStyle = RP.Char6.
                END.
            END.            
        END.
        FIND NEXT Cheque EXCLUSIVE-LOCK WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
                       AND Cheque.ChequeNo >= f1 AND Cheque.ChequeNo <= f2 NO-ERROR.
      END.
    END.

    IF RP.Log3 THEN DO:
      MESSAGE msg VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO 
          TITLE "Start Printing ?" UPDATE start-printing.

      IF NOT start-printing THEN UNDO modify-cheques, RETURN "FAIL".
    END.

  END.
  ELSE IF RP.Log3 THEN DO:
    MESSAGE msg VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO 
        TITLE "Start Printing ?" UPDATE start-printing.
    IF NOT start-printing THEN RETURN.
  END.

  IF (change-acct OR change-no OR RP.Log2) AND NOT RP.Log3 THEN DO:
    msg = "".
    IF change-acct THEN
      msg = "moved from Bank '" + BankAccount.BankAccountCode + "' to '" + ToBank.BankAccountCode + "'".
    IF change-no THEN
      msg = msg + (IF msg <> "" THEN ",~n" ELSE "") + "renumbered by " + STRING(offset) + " (" + STRING(f1) + " becomes " + STRING(t1) + " and so on)".
    IF RP.Log2 THEN
      msg = msg + (IF msg <> "" THEN "~nand " ELSE "") + "dated " + STRING(RP.Date1,"99/99/9999").
    MESSAGE "Cheques" msg VIEW-AS ALERT-BOX INFORMATION TITLE "Cheques Modified".
  END.
  ELSE DO:
      /* paymentstyle option -> both forced-paymentstyle and payment style should be reduced to
        PaymentStyle and passed to chqprt which should then honour payment style
        so cheques where voucher.paymentstyle = 'DD' won't get printed
        and if exporting BankImportFile
           cheques where voucher.paymentstyle <> 'DD'  won't get printed */
    report-options = "BankAccount," + (IF AVAILABLE(ToBank) THEN ToBank.BankAccountCode ELSE BankAccount.BankAccountCode)
                   + "~nChequeRange," + STRING(IF RP.Log1 THEN RP.Int3 ELSE RP.Int1)
                                + "," + STRING(IF RP.Log1 THEN RP.Int3 + ( RP.Int2 - RP.Int1 ) ELSE RP.Int2)
                   + "~nMessage," + REPLACE( RP.Char1, "~n", CHR(7))    
                   + "~nPaymentStyle," + RP.Char6.

    IF RP.Log7 AND LENGTH(RP.Char3) > 0 THEN
        report-options = report-options + "~nWriteBankImportFileTo," + RP.Char3.

    IF pdf-output THEN
      report-options = report-options + "~nOutputPDF".

    RUN process/report/chqprt.p( report-options ).

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR v-directory AS CHAR NO-UNDO.
DEF VAR v-start-dir AS CHAR NO-UNDO.

  v-directory = INPUT FRAME {&FRAME-NAME} RP.Char3 .
  v-start-dir = SUBSTRING( v-directory, 1, R-INDEX(v-directory, "\" ) ).

  SYSTEM-DIALOG GET-FILE v-directory FILTERS "Text" "*.txt"
        SAVE-AS CREATE-TEST-FILE DEFAULT-EXTENSION ".txt"
        INITIAL-DIR v-start-dir RETURN-TO-START-DIR
        TITLE "" UPDATE select-ok.

  IF select-ok THEN DO:
      /* Trim off filename to leave directory */
      v-directory = SUBSTRING( v-directory, 1, R-INDEX(v-directory, "\" ) ).
    
      RP.Char3:SCREEN-VALUE IN FRAME {&FRAME-NAME} = v-directory.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-bank-accounts V-table-Win 
PROCEDURE update-bank-accounts :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR item    AS CHAR NO-UNDO.
DEF VAR id-list AS CHAR NO-UNDO.
DEF VAR default-account AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  cmb_bnkact:LIST-ITEMS = "".

  FIND OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "PYMTBANK" NO-LOCK NO-ERROR.

  FOR EACH BankAccount NO-LOCK:
    item = STRING( BankAccount.CompanyCode, ">>>>9" )   + ' ' + 
           STRING( BankAccount.AccountCode, "9999.99" ) + ' ' +
                   BankAccount.AccountName.
    IF cmb_bnkact:ADD-LAST( item ) THEN.
    id-list = id-list + IF id-list = "" THEN "" ELSE ",".
    id-list = id-list + STRING( ROWID( BankAccount ) ).
    IF AVAILABLE(OfficeControlAccount) AND BankAccount.BankAccountCode = OfficeControlAccount.Description THEN default-account = item.
  END.
  
  cmb_bnkact:PRIVATE-DATA = id-list.
  cmb_bnkact:SCREEN-VALUE = IF default-account <> "" THEN default-account ELSE
    ENTRY( 1, cmb_bnkact:LIST-ITEMS ).

  cmb_bnkact-2:LIST-ITEMS = cmb_bnkact:LIST-ITEMS.
  cmb_bnkact-2:PRIVATE-DATA = id-list.
  cmb_bnkact-2:SCREEN-VALUE = IF default-account <> "" THEN default-account ELSE
    ENTRY( 1, cmb_bnkact-2:LIST-ITEMS ).

  RUN get-from-bank-account.
  RUN get-to-bank-account.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-run V-table-Win 
PROCEDURE verify-run :
/*------------------------------------------------------------------------------
  Purpose:     
  Notes:    Verifying the cheque range has three stages:
     1 - From cheque number is greater than the to cheque number
     2 - If the existing cheque records are to be renumbered then verify that
         the target range is empty of existing cheques.
------------------------------------------------------------------------------*/
DEF VAR range-ok   AS LOGI INIT No NO-UNDO.
DEF VAR batch-code LIKE NewBatch.BatchCode NO-UNDO.
DEF VAR i          AS INT NO-UNDO.
DEF VAR n-cheques      AS INT  NO-UNDO INITIAL 0.

DO WITH FRAME {&FRAME-NAME}:

  RUN get-from-bank-account.
  IF NOT AVAILABLE BankAccount THEN DO:
    MESSAGE "You must select a BankAccount." VIEW-AS ALERT-BOX ERROR
      TITLE "No bank Account selected".
    APPLY 'ENTRY':U TO cmb_bnkact.
    RETURN "FAIL".
  END.

  IF INPUT RP.Log1 THEN DO:
    IF INPUT RP.Int3 = 0 THEN DO:
      MESSAGE "You must enter a starting number to renumber" SKIP
              "cheques from" VIEW-AS ALERT-BOX ERROR
              TITLE "No starting number entered".
      APPLY 'ENTRY':U TO RP.Int3.
      RETURN "FAIL".
    END.

    RUN get-to-bank-account.
    IF NOT AVAILABLE ToBank THEN DO:
      MESSAGE "You must select a 'To' Bank." VIEW-AS ALERT-BOX ERROR
              TITLE "No 'To' bank Account selected".
      APPLY 'ENTRY':U TO cmb_bnkact-2.
      RETURN "FAIL".
    END.

    IF INPUT RP.Log7 AND LENGTH(RP.Char3) = 0 THEN DO:
      MESSAGE "You must select a Directory to output Bank Import File to."
          VIEW-AS ALERT-BOX ERROR TITLE "No Output Directory selected".
      APPLY 'ENTRY':U TO RP.Char3.
      RETURN "FAIL".
    END.

DEF VAR f1 AS INT NO-UNDO.
DEF VAR f2 AS INT NO-UNDO.
DEF VAR t1 AS INT NO-UNDO.
DEF VAR t2 AS INT NO-UNDO.
    f1 = INPUT RP.Int1.
    f2 = INPUT RP.Int2.
    t1 = INPUT RP.Int3.
    t2 = t1 + f2 - f1.
    IF CAN-FIND( FIRST Cheque WHERE Cheque.BankAccountCode = ToBank.BankAccountCode 
                              AND Cheque.ChequeNo >= t1 AND Cheque.ChequeNo <= t2
                              AND NOT(Cheque.BankAccountCode = BankAccount.BankAccountCode
                                     AND Cheque.ChequeNo >= f1 AND Cheque.ChequeNo <= f2 ))
    THEN DO:
      MESSAGE "Cheques cannot be renumbered into the range starting" INPUT RP.Int3 SKIP
              "because one or more cheques within the target range" SKIP
              "already exist." VIEW-AS ALERT-BOX ERROR
              TITLE "Cheque Number Assignment Error".
      APPLY 'ENTRY':U TO RP.Date1.
      RETURN "FAIL".
    END.
  END.

  IF INPUT RP.Log2 = Yes AND INPUT RP.Date1 = ? THEN DO:
    MESSAGE "You must enter a cheque date to re-date cheques with." VIEW-AS ALERT-BOX ERROR
      TITLE "No cheque date entered.".
    APPLY 'ENTRY':U TO RP.Date1.
    RETURN "FAIL".
  END.
  
  IF INPUT RP.Int1 > INPUT RP.Int2 THEN DO:
    MESSAGE "The cheque range is invalid" VIEW-AS ALERT-BOX ERROR
      TITLE "Invalid Cheque Number range".
    APPLY 'ENTRY':U TO RP.Int1.
    RETURN "FAIL".
  END.

  /* Count the cheques */
  /* Check Voucher.PaymentStyle of cheque against Forced Payment Style
       IF Forced Payment Style = 'DD' AND Creditor.EnableDirectPayment
       OR Voucher.PaymentStyle = PaymentStyle selected
       THEN cheque will be printed */
  IF INPUT RP.Log4 THEN DO: /* IF Force PaymentStyle */
      IF TRIM(RP.Char6) = 'DD' THEN DO:
          FOR EACH Cheque WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
              AND Cheque.ChequeNo >= INPUT RP.Int1 AND Cheque.ChequeNo <= INPUT RP.Int2
              AND NOT Cheque.Cancelled NO-LOCK:
  
              IF CAN-FIND(FIRST Creditor
                          WHERE Creditor.CreditorCode = Cheque.CreditorCode
                            AND Creditor.EnableDirectPayment) THEN n-cheques = n-cheques + 1.
          END.
      END.
      ELSE DO:
          FOR EACH Cheque WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
              AND Cheque.ChequeNo >= INPUT RP.Int1 AND Cheque.ChequeNo <= INPUT RP.Int2
              AND NOT Cheque.Cancelled NO-LOCK:

               n-cheques = n-cheques + 1.
          END.
      END.
  END.
  ELSE DO: /* Else check Voucher.PaymentStyle */
      FOR EACH Cheque WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
          AND Cheque.ChequeNo >= INPUT RP.Int1 AND Cheque.ChequeNo <= INPUT RP.Int2
          AND NOT Cheque.Cancelled NO-LOCK:

          FIND FIRST Voucher WHERE Voucher.BankAccountCode = Cheque.BankAccountCode
              AND Voucher.ChequeNo = Cheque.ChequeNo NO-LOCK NO-ERROR.

          IF AVAILABLE Voucher THEN DO:
              IF TRIM(Voucher.PaymentStyle) = TRIM(RP.Char6) THEN n-cheques = n-cheques + 1.              
          END.
      END.
  END.

  IF n-cheques = 0 THEN
  DO:
    MESSAGE
      "There are no cheques available in the given range" SKIP
      "of PaymentStyle " + RP.Char6 + " for bank account:" SKIP(1)
      INPUT cmb_bnkact VIEW-AS ALERT-BOX ERROR TITLE "No cheques available".
    APPLY 'ENTRY':U TO cmb_bnkact.
    RETURN "FAIL".
  END.
END.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

