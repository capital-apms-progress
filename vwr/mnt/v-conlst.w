&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR mode AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES ConsolidationList
&Scoped-define FIRST-EXTERNAL-TABLE ConsolidationList


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR ConsolidationList.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS ConsolidationList.Name ~
ConsolidationList.Description 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Name ~{&FP2}Name ~{&FP3}~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}
&Scoped-define ENABLED-TABLES ConsolidationList
&Scoped-define FIRST-ENABLED-TABLE ConsolidationList
&Scoped-Define ENABLED-OBJECTS RECT-33 sel_Curr btn_add sel_avail ~
btn_remove 
&Scoped-Define DISPLAYED-FIELDS ConsolidationList.Name ~
ConsolidationList.Description 
&Scoped-Define DISPLAYED-OBJECTS sel_Curr sel_avail 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_remove 
     LABEL "&Remove" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE RECTANGLE RECT-33
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 95.43 BY 18.

DEFINE VARIABLE sel_avail AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SORT SCROLLBAR-VERTICAL 
     SIZE 40 BY 14.5
     FONT 9 NO-UNDO.

DEFINE VARIABLE sel_Curr AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SORT SCROLLBAR-VERTICAL 
     SIZE 40 BY 14.5
     FONT 9 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     ConsolidationList.Name AT ROW 2 COL 5.86 COLON-ALIGNED
          LABEL "Name"
          VIEW-AS FILL-IN 
          SIZE 5.43 BY 1
     ConsolidationList.Description AT ROW 2 COL 24.72 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 51.43 BY 1
     sel_Curr AT ROW 4.5 COL 2.72 NO-LABEL
     btn_add AT ROW 4.5 COL 43.86
     sel_avail AT ROW 4.5 COL 55.86 NO-LABEL
     btn_remove AT ROW 6 COL 43.86
     RECT-33 AT ROW 1.5 COL 1.57
     "Current Companies" VIEW-AS TEXT
          SIZE 22 BY 1 AT ROW 3.5 COL 2.72
          FONT 14
     "Available Companies" VIEW-AS TEXT
          SIZE 22 BY 1 AT ROW 3.5 COL 56.43
          FONT 14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.ConsolidationList
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.75
         WIDTH              = 96.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN ConsolidationList.Name IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN move-companies( sel_avail:HANDLE, sel_curr:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_remove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_remove V-table-Win
ON CHOOSE OF btn_remove IN FRAME F-Main /* Remove */
DO:
  RUN move-companies( sel_curr:HANDLE, sel_avail:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_avail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_avail V-table-Win
ON MOUSE-SELECT-DBLCLICK OF sel_avail IN FRAME F-Main
DO:
  APPLY 'CHOOSE':U TO btn_add IN FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_avail V-table-Win
ON VALUE-CHANGED OF sel_avail IN FRAME F-Main
DO:
  RUN sensitise-buttons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_Curr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_Curr V-table-Win
ON MOUSE-SELECT-DBLCLICK OF sel_Curr IN FRAME F-Main
DO:
  APPLY 'CHOOSE':U TO btn_remove IN FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_Curr V-table-Win
ON VALUE-CHANGED OF sel_Curr IN FRAME F-Main
DO:
  RUN sensitise-buttons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "ConsolidationList"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "ConsolidationList"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SORUCE':U ).
  IF mode = "Add" THEN RUN delete-consolidation. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-consolidation.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  RUN notify( 'hide, CONTAINER-SORUCE':U ).
  RUN dispatch( 'update-record':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-consolidation V-table-Win 
PROCEDURE delete-consolidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT ConsolidationList EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE ConsolidationList THEN DELETE ConsolidationList.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR i      AS INT  NO-UNDO.
  DEF VAR delim  AS CHAR NO-UNDO.
  DEF VAR c      AS INT  NO-UNDO.
  DEF VAR c-list AS CHAR NO-UNDO.
  DEF VAR item   AS CHAR NO-UNDO.
  
  delim = sel_curr:DELIMITER IN FRAME {&FRAME-NAME}.
  
  DO i = 1 TO NUM-ENTRIES( sel_curr:LIST-ITEMS, delim ):
    item = ENTRY( i, sel_curr:LIST-ITEMS, delim ).
    ASSIGN c = INT( TRIM ( ENTRY( 1, item, "-" ) ) ).
    IF CAN-FIND( Company WHERE Company.CompanyCode = c ) THEN
    DO:  
      c-list = c-list + IF c-list = "" THEN "" ELSE ",".
      c-list = c-list + STRING( c ).
    END.
  END.
  
  ASSIGN ConsolidationList.CompanyList = c-list.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN update-lists.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN get-attribute( 'mode' ).
  mode = RETURN-VALUE.
  
  IF mode = "Add" THEN
  DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE RUN dispatch( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE move-companies V-table-Win 
PROCEDURE move-companies :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER h_src AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER h_trg AS HANDLE NO-UNDO.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  
  DEF VAR i     AS INT  NO-UNDO.
  DEF VAR sv    AS CHAR NO-UNDO.
  DEF VAR item  AS CHAR NO-UNDO.
  DEF VAR delim AS CHAR NO-UNDO.
  DEF VAR c     LIKE Company.CompanyCode NO-UNDO.
  
  sv = h_src:SCREEN-VALUE.
  delim = h_src:DELIMITER.
  
  DO i = 1 TO NUM-ENTRIES( sv, delim ):
    item = ENTRY( i, sv, delim ).
    IF h_src:DELETE( item ) THEN.
    ASSIGN c = INT( TRIM( ENTRY( 1, item , "-" ) ) ).
    IF NOT ERROR-STATUS:ERROR AND CAN-FIND( Company WHERE Company.CompanyCode = c ) THEN
      IF h_trg:ADD-LAST( item ) THEN.
  END.
  
  RUN sensitise-buttons.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CREATE ConsolidationList.
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  
  CURRENT-WINDOW:TITLE = "Adding a new Consolidation List".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE":U THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ConsolidationList"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-buttons V-table-Win 
PROCEDURE sensitise-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  btn_add:SENSITIVE    = sel_avail:SCREEN-VALUE <> ? AND
                         sel_avail:SCREEN-VALUE <> "".
  btn_remove:SENSITIVE = sel_curr:SCREEN-VALUE <> ? AND
                         sel_curr:SCREEN-VALUE <> "".

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-lists V-table-Win 
PROCEDURE update-lists :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR i    AS INT NO-UNDO.
  DEF VAR c    LIKE Company.CompanyCode NO-UNDO.
  DEF VAR item AS CHAR NO-UNDO.
  
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  
  /* Current List */
  
  sel_curr:LIST-ITEMS IN FRAME {&FRAME-NAME} = "".
  DO i = 1 TO NUM-ENTRIES( ConsolidationList.Companylist ):
    
    ASSIGN c = INT( ENTRY( i, ConsolidationList.CompanyList ) ) NO-ERROR.
    IF ERROR-STATUS:ERROR THEN NEXT.
    
    FIND Company WHERE Company.CompanyCode = c NO-LOCK NO-ERROR.
    item = STRING( c, ">>999" ) + " - " + IF AVAILABLE Company
      THEN Company.LegalName
      ELSE "***** Company Not Available !!! *****".
    IF sel_curr:ADD-LAST( item ) THEN.
  END.

  sel_avail:LIST-ITEMS = "".
  FOR EACH Company NO-LOCK WHERE
    LOOKUP( STRING( Company.CompanyCode ), ConsolidationList.CompanyList ) = 0:
    item = STRING( Company.CompanyCode, ">>999" ) + " - " + Company.LegalName.
    IF sel_avail:ADD-LAST( item ) THEN.
  END.
  
  RUN sensitise-buttons.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-consolidation V-table-Win 
PROCEDURE verify-consolidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF INPUT ConsolidationList.Name = "" OR
     INPUT ConsolidationList.Description = "" OR
     sel_curr:LIST-ITEMS = ? OR sel_curr:LIST-ITEMS = "" THEN
  DO:
    MESSAGE "You must enter a name, description and" SKIP
            "select at least one company." VIEW-AS ALERT-BOX ERROR
      TITLE "Invalid Consolidation List".
    RETURN "FAIL".
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


