&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r11 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.

&GLOB EXCLUDE-local-enable-fields 1

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Property
&Scoped-define FIRST-EXTERNAL-TABLE Property


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Property.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Property.BusIntIndemnityPeriod 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}BusIntIndemnityPeriod ~{&FP2}BusIntIndemnityPeriod ~{&FP3}
&Scoped-define ENABLED-TABLES Property
&Scoped-define FIRST-ENABLED-TABLE Property
&Scoped-Define ENABLED-OBJECTS RECT-3 RECT-1 RECT-2 RECT-5 fil_Broker ~
RECT-4 
&Scoped-Define DISPLAYED-FIELDS Property.Name ~
Property.BusIntIndemnityPeriod 
&Scoped-Define DISPLAYED-OBJECTS fil_CVAL_amount fil_IIDV_amount ~
fil_CVAL_person fil_IIDI_amount fil_CVAL_date fil_IRPL_amount ~
fil_IDEM_amount fil_BVAM_amount fil_IRPI_amount fil_HCST_amount ~
fil_TWDV_date fil_TWDV_amount fil_GVIM_date fil_IBZI_amount fil_IBZI_date ~
fil_GVLV_amount fil_IBZI_person fil_GVIM_amount fil_Broker ~
fil_Capital_Value 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */
&Scoped-define ADM-CREATE-FIELDS Property.BusIntIndemnityPeriod 
&Scoped-define ADM-ASSIGN-FIELDS Property.BusIntIndemnityPeriod 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
PropertyCode|y|y|ttpl.Property.PropertyCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "PropertyCode",
     Keys-Supplied = "PropertyCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fil_Broker AS CHARACTER FORMAT "X(256)":U 
     LABEL "Broker" 
     VIEW-AS FILL-IN 
     SIZE 27.43 BY 1 NO-UNDO.

DEFINE VARIABLE fil_BVAM_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Book value at merger" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Capital_Value AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Capital Value" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_CVAL_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Current valuation" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_CVAL_date AS DATE FORMAT "99/99/9999":U INITIAL ? 
     LABEL "Dated" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_CVAL_person AS CHARACTER FORMAT "X(256)":U 
     LABEL "Valuer" 
     VIEW-AS FILL-IN 
     SIZE 29.14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_GVIM_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Improvements" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_GVIM_date AS DATE FORMAT "99/99/9999":U INITIAL ? 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_GVLV_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Land Value" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_HCST_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Historical cost" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_IBZI_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Sum insured" 
     VIEW-AS FILL-IN 
     SIZE 13.72 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_IBZI_date AS DATE FORMAT "99/99/9999":U INITIAL ? 
     LABEL "Dated" 
     VIEW-AS FILL-IN 
     SIZE 13.72 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_IBZI_person AS CHARACTER FORMAT "X(256)":U 
     LABEL "Valuer" 
     VIEW-AS FILL-IN 
     SIZE 27.43 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_IDEM_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Demolition" 
     VIEW-AS FILL-IN 
     SIZE 13.72 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_IIDI_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Indeminty Inflation" 
     VIEW-AS FILL-IN 
     SIZE 13.72 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_IIDV_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Indemnity Value" 
     VIEW-AS FILL-IN 
     SIZE 13.72 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_IRPI_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Replacement Inflation" 
     VIEW-AS FILL-IN 
     SIZE 13.72 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_IRPL_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Replacement Value" 
     VIEW-AS FILL-IN 
     SIZE 13.72 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_TWDV_amount AS INTEGER FORMAT "-ZZZ,ZZZ,ZZ9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_TWDV_date AS DATE FORMAT "99/99/9999":U INITIAL ? 
     LABEL "TWDV" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 35.43 BY 13.4.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 34.29 BY 6.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 76 BY 14.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 17.14 BY .2.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 37.72 BY 4.2.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Property.Name AT ROW 1.2 COL 1.57 NO-LABEL
           VIEW-AS TEXT 
          SIZE 40.57 BY 1.2
          FONT 14
     fil_CVAL_amount AT ROW 2.4 COL 18.43 COLON-ALIGNED
     fil_IIDV_amount AT ROW 2.4 COL 59.57 COLON-ALIGNED
     fil_CVAL_person AT ROW 3.4 COL 7.57 COLON-ALIGNED
     fil_IIDI_amount AT ROW 3.4 COL 59.57 COLON-ALIGNED
     fil_CVAL_date AT ROW 4.4 COL 7.57 COLON-ALIGNED
     fil_IRPL_amount AT ROW 4.4 COL 59.57 COLON-ALIGNED
     fil_IDEM_amount AT ROW 5.4 COL 59.57 COLON-ALIGNED
     fil_BVAM_amount AT ROW 6.2 COL 22.43 COLON-ALIGNED
     fil_IRPI_amount AT ROW 6.4 COL 59.57 COLON-ALIGNED
     fil_HCST_amount AT ROW 7.2 COL 22.43 COLON-ALIGNED
     fil_TWDV_date AT ROW 8.2 COL 11 COLON-ALIGNED
     fil_TWDV_amount AT ROW 8.2 COL 22.43 COLON-ALIGNED NO-LABEL
     Property.BusIntIndemnityPeriod AT ROW 9.2 COL 59.57 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 5.14 BY 1
     fil_GVIM_date AT ROW 10 COL 25.29 COLON-ALIGNED NO-LABEL
     fil_IBZI_amount AT ROW 10.2 COL 59.57 COLON-ALIGNED
     fil_IBZI_date AT ROW 11.2 COL 59.57 COLON-ALIGNED
     fil_GVLV_amount AT ROW 11.4 COL 22.43 COLON-ALIGNED
     fil_IBZI_person AT ROW 12.2 COL 45.86 COLON-ALIGNED
     fil_GVIM_amount AT ROW 12.4 COL 22.43 COLON-ALIGNED
     fil_Broker AT ROW 13.2 COL 45.86 COLON-ALIGNED
     fil_Capital_Value AT ROW 13.6 COL 22.43 COLON-ALIGNED
     RECT-3 AT ROW 1 COL 1
     "Insurance Valuations" VIEW-AS TEXT
          SIZE 21.14 BY 1 AT ROW 1 COL 45.57
          FONT 14
     RECT-1 AT ROW 1.4 COL 41
     "Business Interruption" VIEW-AS TEXT
          SIZE 21.14 BY 1 AT ROW 8 COL 45.57
          FONT 14
     RECT-2 AT ROW 8.6 COL 41.57
     "months" VIEW-AS TEXT
          SIZE 5.72 BY 1 AT ROW 9.2 COL 67.29
          FONT 9
     "Government Valuation" VIEW-AS TEXT
          SIZE 22.29 BY 1 AT ROW 10 COL 2.72
          FONT 14
     RECT-5 AT ROW 10.6 COL 1.57
     RECT-4 AT ROW 13.4 COL 21.57
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .

 

/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.Property
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 17.85
         WIDTH              = 89.29.
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Default                                      */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Property.BusIntIndemnityPeriod IN FRAME F-Main
   1 2                                                                  */
/* SETTINGS FOR FILL-IN fil_BVAM_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Capital_Value IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_CVAL_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_CVAL_date IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_CVAL_person IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_GVIM_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_GVIM_date IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_GVLV_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_HCST_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_IBZI_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_IBZI_date IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_IBZI_person IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_IDEM_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_IIDI_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_IIDV_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_IRPI_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_IRPL_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_TWDV_amount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_TWDV_date IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Property.Name IN FRAME F-Main
   NO-ENABLE ALIGN-L EXP-LABEL                                          */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fil_Broker
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Broker V-table-Win
ON U1 OF fil_Broker IN FRAME F-Main /* Broker */
DO:
  {inc/selfil/sfpsn1.i "Property" "InsuranceBroker"} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Broker V-table-Win
ON U2 OF fil_Broker IN FRAME F-Main /* Broker */
DO:
  {inc/selfil/sfpsn2.i "Property" "InsuranceBroker"} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Broker V-table-Win
ON U3 OF fil_Broker IN FRAME F-Main /* Broker */
DO:
  {inc/selfil/sfpsn3.i "Property" "InsuranceBroker"} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'PropertyCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Property
           &WHERE = "WHERE Property.PropertyCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Property"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Property"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:  Cancel changes and exit
------------------------------------------------------------------------------*/

  RUN dispatch ( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:  Confirm any changes
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF    INPUT fil_CVAL_date <> fil_CVAL_date
     OR INPUT fil_CVAL_amount <> fil_CVAL_amount
     OR INPUT fil_CVAL_person <> fil_CVAL_person
  THEN DO:
  END.

  IF    INPUT fil_IBZI_date <> fil_IBZI_date
     OR INPUT fil_IBZI_amount <> fil_IBZI_amount
     OR INPUT fil_IBZI_person <> fil_IBZI_person
  THEN DO:
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  ENABLE Property.BusIntIndemnityPeriod WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Our bit of initialisation in a non-standard world
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Property) THEN RETURN.

&SCOP V-TYPE CVAL
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  IF AVAILABLE(Valuation) THEN DO:
    ASSIGN fil_{&V-TYPE}_amount = Valuation.Amount
           fil_{&V-TYPE}_date   = Valuation.DateDone
           fil_{&V-TYPE}_person = Valuation.Valuer.
  END.

&SCOP V-TYPE BVAM
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  IF AVAILABLE(Valuation) THEN ASSIGN fil_{&V-TYPE}_amount = Valuation.Amount.

&SCOP V-TYPE HCST
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  IF AVAILABLE(Valuation) THEN ASSIGN fil_{&V-TYPE}_amount = Valuation.Amount.

&SCOP V-TYPE TWDV
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  IF AVAILABLE(Valuation) THEN ASSIGN fil_{&V-TYPE}_amount = Valuation.Amount
                                      fil_{&V-TYPE}_date   = Valuation.DateDone.

&SCOP V-TYPE GVIM
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  IF AVAILABLE(Valuation) THEN ASSIGN fil_{&V-TYPE}_amount = Valuation.Amount
                                      fil_{&V-TYPE}_date   = Valuation.DateDone.

&SCOP V-TYPE GVLV
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  fil_{&V-TYPE}_amount = IF AVAILABLE(Valuation) THEN Valuation.Amount ELSE 0.

&SCOP V-TYPE IIDV
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  fil_{&V-TYPE}_amount = IF AVAILABLE(Valuation) THEN Valuation.Amount ELSE 0.

&SCOP V-TYPE IIDI
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  fil_{&V-TYPE}_amount = IF AVAILABLE(Valuation) THEN Valuation.Amount ELSE 0.

&SCOP V-TYPE IRPL
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  fil_{&V-TYPE}_amount = IF AVAILABLE(Valuation) THEN Valuation.Amount ELSE 0.

&SCOP V-TYPE IDEM
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  fil_{&V-TYPE}_amount = IF AVAILABLE(Valuation) THEN Valuation.Amount ELSE 0.

&SCOP V-TYPE IRPI
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  fil_{&V-TYPE}_amount = IF AVAILABLE(Valuation) THEN Valuation.Amount ELSE 0.

&SCOP V-TYPE IBZI
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  IF AVAILABLE(Valuation) THEN DO:
    ASSIGN fil_{&V-TYPE}_amount = Valuation.Amount
           fil_{&V-TYPE}_date   = Valuation.DateDone
           fil_{&V-TYPE}_person = Valuation.Valuer.
  END.


&SCOP V-TYPE TWDV
  FIND LAST Valuation WHERE Valuation.PropertyCode = Property.PropertyCode
                        AND Valuation.ValuationType = "{&V-TYPE}"
                        NO-LOCK NO-ERROR.
  IF AVAILABLE(Valuation) THEN ASSIGN fil_{&V-TYPE}_amount = Valuation.Amount
                                      fil_{&V-TYPE}_date   = Valuation.DateDone.

  fil_Capital_Value = fil_GVIM_amount + fil_GVLV_amount .

  RUN dispatch ( 'display-fields':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "PropertyCode" "Property" "PropertyCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Property"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


