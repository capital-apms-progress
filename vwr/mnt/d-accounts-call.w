&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:         vwr/mnt/d-tenant-call.w
  Description:  Dialog to handle entry of tenant call details
  Author:       Andrew McMillan
  Created:      21 May 2001
------------------------------------------------------------------------*/

/* Parameters Definitions ---                                           */
DEF INPUT PARAMETER in-call-number LIKE TenantCall.CallNumber NO-UNDO.

/* Local Variable Definitions ---                                       */
DEF VAR new-call-number LIKE TenantCall.CallNumber NO-UNDO.
DEF VAR hh AS INTEGER NO-UNDO.
DEF VAR mm AS INTEGER NO-UNDO.
DEF VAR ss AS INTEGER NO-UNDO.

DEF VAR original-property AS INT NO-UNDO INITIAL ?.
DEF VAR current-property AS INT NO-UNDO INITIAL ?.
DEF VAR current-tenant   AS INT NO-UNDO INITIAL ?.

{inc/ofc-this.i}
{inc/ofc-set.i "TenantCall-Creditors" "standard-creditors"}

IF NOT AVAILABLE(OfficeSetting) THEN standard-creditors = "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE DIALOG-BOX
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME Dialog-Frame

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES TenantCall

/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define FIELDS-IN-QUERY-Dialog-Frame TenantCall.CallNumber ~
TenantCall.DateOfCall TenantCall.TenantCode TenantCall.Description ~
TenantCall.Problem TenantCall.ContactName TenantCall.ContactPhone ~
TenantCall.Action 
&Scoped-define ENABLED-FIELDS-IN-QUERY-Dialog-Frame TenantCall.CallNumber ~
TenantCall.DateOfCall TenantCall.TenantCode TenantCall.Description ~
TenantCall.Problem TenantCall.ContactName TenantCall.ContactPhone ~
TenantCall.Action 
&Scoped-define ENABLED-TABLES-IN-QUERY-Dialog-Frame TenantCall
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-Dialog-Frame TenantCall
&Scoped-define OPEN-QUERY-Dialog-Frame OPEN QUERY Dialog-Frame FOR EACH TenantCall SHARE-LOCK.
&Scoped-define TABLES-IN-QUERY-Dialog-Frame TenantCall
&Scoped-define FIRST-TABLE-IN-QUERY-Dialog-Frame TenantCall


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS TenantCall.CallNumber TenantCall.DateOfCall ~
TenantCall.TenantCode TenantCall.Description TenantCall.Problem ~
TenantCall.ContactName TenantCall.ContactPhone TenantCall.Action 
&Scoped-define ENABLED-TABLES TenantCall
&Scoped-define FIRST-ENABLED-TABLE TenantCall
&Scoped-define DISPLAYED-TABLES TenantCall
&Scoped-define FIRST-DISPLAYED-TABLE TenantCall
&Scoped-Define ENABLED-OBJECTS disp-call-time cmb_Status Btn_OK Btn_Cancel ~
Btn_CallClosed 
&Scoped-Define DISPLAYED-FIELDS TenantCall.CallNumber TenantCall.DateOfCall ~
TenantCall.TenantCode TenantCall.Description TenantCall.Problem ~
TenantCall.ContactName TenantCall.ContactPhone TenantCall.Action 
&Scoped-Define DISPLAYED-OBJECTS disp-call-time fil_OrderNo fil_Tenant ~
cmb_Status fil_Closed 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD time2int Dialog-Frame 
FUNCTION time2int RETURNS INTEGER
  ( INPUT tm AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_CallClosed AUTO-END-KEY 
     LABEL "Mark Call Closed" 
     SIZE 15 BY 1.15.

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel Changes" 
     SIZE 15 BY 1.15.

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.15.

DEFINE VARIABLE cmb_Status AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE disp-call-time AS CHARACTER FORMAT "X(5)" INITIAL "0" 
     LABEL "Call Time" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Closed AS CHARACTER FORMAT "X(256)":U 
     LABEL "Closed" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_OrderNo AS CHARACTER FORMAT "X(256)" INITIAL "0" 
     LABEL "Order No" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_Tenant AS CHARACTER FORMAT "X(256)":U 
     LABEL "" 
     VIEW-AS FILL-IN 
     SIZE 57 BY 1.05 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY Dialog-Frame FOR 
      TenantCall SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     TenantCall.CallNumber AT ROW 1.25 COL 8 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9 BY 1
     TenantCall.DateOfCall AT ROW 1.25 COL 27 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     disp-call-time AT ROW 1.25 COL 48 COLON-ALIGNED
     fil_OrderNo AT ROW 1.25 COL 63 COLON-ALIGNED
     TenantCall.TenantCode AT ROW 3 COL 8 COLON-ALIGNED
          LABEL "Tenant"
          VIEW-AS FILL-IN 
          SIZE 7 BY 1.05
     fil_Tenant AT ROW 3 COL 16 COLON-ALIGNED
     TenantCall.Description AT ROW 4.25 COL 8 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 65 BY 1.05
     TenantCall.Problem AT ROW 5.5 COL 10 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 65 BY 3.5
     TenantCall.ContactName AT ROW 9 COL 8 COLON-ALIGNED
          LABEL "Contact"
          VIEW-AS FILL-IN 
          SIZE 19 BY 1.05
     TenantCall.ContactPhone AT ROW 9 COL 43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 30 BY 1.05
     TenantCall.Action AT ROW 10.25 COL 10 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 65 BY 6.75
     cmb_Status AT ROW 17.25 COL 8 COLON-ALIGNED
     fil_Closed AT ROW 17.25 COL 44 COLON-ALIGNED
     Btn_OK AT ROW 18.75 COL 2
     Btn_Cancel AT ROW 18.75 COL 18
     Btn_CallClosed AT ROW 18.75 COL 38
     "Problem:" VIEW-AS TEXT
          SIZE 6 BY 1.25 AT ROW 5.35 COL 9 RIGHT-ALIGNED
     "Action:" VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 10.2 COL 9 RIGHT-ALIGNED
     SPACE(65.71) SKIP(9.04)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 10
         TITLE "Enter Name Details".


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: DIALOG-BOX
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   L-To-R                                                               */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR EDITOR TenantCall.Action IN FRAME Dialog-Frame
   EXP-LABEL                                                            */
ASSIGN 
       TenantCall.Action:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN TenantCall.ContactName IN FRAME Dialog-Frame
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Closed IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_OrderNo IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Tenant IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fil_Tenant:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR EDITOR TenantCall.Problem IN FRAME Dialog-Frame
   EXP-LABEL                                                            */
ASSIGN 
       TenantCall.Problem:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN TenantCall.TenantCode IN FRAME Dialog-Frame
   EXP-LABEL                                                            */
/* SETTINGS FOR TEXT-LITERAL "Problem:"
          SIZE 6 BY 1.25 AT ROW 5.35 COL 9 RIGHT-ALIGNED                */

/* SETTINGS FOR TEXT-LITERAL "Action:"
          SIZE 5 BY 1 AT ROW 10.2 COL 9 RIGHT-ALIGNED                   */

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX Dialog-Frame
/* Query rebuild information for DIALOG-BOX Dialog-Frame
     _TblList          = "TTPL.TenantCall"
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX Dialog-Frame */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON END-ERROR OF FRAME Dialog-Frame /* Enter Name Details */
DO:
  RUN cancel-and-close.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Enter Name Details */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_CallClosed
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_CallClosed Dialog-Frame
ON CHOOSE OF Btn_CallClosed IN FRAME Dialog-Frame /* Mark Call Closed */
DO:
  RUN mark-as-closed.
  RUN save-and-close.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel Changes */
DO:
  RUN cancel-and-close.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  RUN save-and-close.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenantCall.TenantCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenantCall.TenantCode Dialog-Frame
ON LEAVE OF TenantCall.TenantCode IN FRAME Dialog-Frame /* Tenant */
DO:
  RUN update-tenant-name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


IF in-call-number > 0 THEN DO:
  FIND TenantCall WHERE CallNumber = in-call-number NO-LOCK.
  original-property = TenantCall.PropertyCode .
  current-property = TenantCall.PropertyCode .
END.
ELSE DO:
  FIND LAST TenantCall NO-LOCK.
  new-call-number = TenantCall.CallNumber + 1.
  DO TRANSACTION:
    CREATE TenantCall.
    TenantCall.CallNumber = new-call-number.
     TenantCall.CallCategoryCode = 'ACCT'.
    TenantCall.DateOfCall = TODAY.
    TenantCall.TimeOfCall = TIME.
    TenantCall.CallStatusCode = "Active".
    FIND CURRENT TenantCall SHARE-LOCK.
  END.
END.

disp-call-time = STRING( TenantCall.TimeOfCall, "HH:MM" ).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE adm-enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY disp-call-time fil_OrderNo fil_Tenant cmb_Status fil_Closed 
      WITH FRAME Dialog-Frame.
  IF AVAILABLE TenantCall THEN 
    DISPLAY TenantCall.CallNumber TenantCall.DateOfCall TenantCall.TenantCode 
          TenantCall.Description TenantCall.Problem TenantCall.ContactName 
          TenantCall.ContactPhone TenantCall.Action 
      WITH FRAME Dialog-Frame.
  ENABLE TenantCall.CallNumber TenantCall.DateOfCall disp-call-time 
         TenantCall.TenantCode TenantCall.Description TenantCall.Problem 
         TenantCall.ContactName TenantCall.ContactPhone TenantCall.Action 
         cmb_Status Btn_OK Btn_Cancel Btn_CallClosed 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-status-list Dialog-Frame 
PROCEDURE build-status-list :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR public-list AS CHAR NO-UNDO INITIAL ''.
DEF VAR currval AS INT NO-UNDO INITIAL 0.

DO WITH FRAME {&FRAME-NAME}:
  public-list = ''.
  FOR EACH LookupCode WHERE LookupCode._File-Name = 'TenantCall'
                      AND LookupCode._Field-Name = 'CallStatusCode' NO-LOCK:
    public-list = public-list + (IF public-list <> '' THEN '|' ELSE '')
                + LookupCode.LookupCode .
  END.
  cmb_Status:DELIMITER = '|'.
  cmb_Status:LIST-ITEMS = public-list.
  cmb_Status:PRIVATE-DATA = public-list.
  currval = LOOKUP( TenantCall.CallStatusCode, public-list, '|' ).
  IF NOT(currval > 0) THEN currval = 1. 
  cmb_Status = ENTRY( currval, public-list, '|' ).
  
  DISPLAY cmb_Status WITH FRAME {&FRAME-NAME}.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-and-close Dialog-Frame 
PROCEDURE cancel-and-close :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF in-call-number > 0 THEN RETURN.

  DO TRANSACTION:
    FIND CURRENT TenantCall EXCLUSIVE-LOCK.
    DELETE TenantCall.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame 
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN adm-enable_UI.
  RUN build-status-list.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE mark-as-closed Dialog-Frame 
PROCEDURE mark-as-closed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
IF TenantCall.TimeComplete <> ? THEN RETURN.

DO TRANSACTION WITH FRAME {&FRAME-NAME}:
  FIND CURRENT TenantCall EXCLUSIVE-LOCK.
  TenantCall.TimeComplete = TIME.
  TenantCall.DateComplete = TODAY.
  TenantCall.CallStatusCode = 'Closed'.
  FIND CURRENT TenantCall NO-LOCK.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-and-close Dialog-Frame 
PROCEDURE save-and-close :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO TRANSACTION WITH FRAME {&FRAME-NAME}:
  FIND CURRENT TenantCall EXCLUSIVE-LOCK.
  ASSIGN FRAME {&FRAME-NAME} {&ENABLED-FIELDS}.
  TenantCall.CallCategoryCode = 'ACCT'.
  TenantCall.PropertyCode = current-property .
  TenantCall.TimeOfCall = time2int( INPUT FRAME {&FRAME-NAME} disp-call-time ).
  TenantCall.CallStatusCode = ENTRY( cmb_Status:LOOKUP(cmb_Status:SCREEN-VALUE), cmb_Status:PRIVATE-DATA, cmb_Status:DELIMITER ).
  FIND CURRENT TenantCall NO-LOCK.
END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-tenant-name Dialog-Frame 
PROCEDURE update-tenant-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  FIND Tenant WHERE Tenant.TenantCode = INPUT TenantCall.TenantCode NO-LOCK NO-ERROR.
  IF AVAILABLE(Tenant) THEN DO:
    fil_Tenant = Tenant.Name .
    DISPLAY fil_Tenant.
    current-property = Tenant.EntityCode .
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION time2int Dialog-Frame 
FUNCTION time2int RETURNS INTEGER
  ( INPUT tm AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR hh AS INT NO-UNDO.
DEF VAR mm AS INT NO-UNDO.
DEF VAR ss AS INT NO-UNDO.

  ASSIGN hh = INT(ENTRY( 1, tm, ":" )) NO-ERROR.
  ASSIGN mm = INT(ENTRY( 2, tm, ":" )) NO-ERROR.
  ASSIGN ss = INT(ENTRY( 3, tm, ":" )) NO-ERROR.
  IF ( hh = ? ) THEN hh = 0.
  IF ( mm = ? ) THEN mm = 0.
  IF ( ss = ? ) THEN ss = 0.
  
  ss = 3600 * hh + 60 * mm + ss.

  IF INDEX( tm, "p" ) > 0 THEN ss = ss + 43200 .
/*  MESSAGE "Converted" tm "to" ss. */

  RETURN ss.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

