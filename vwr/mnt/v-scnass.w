&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "Andrew" NO-UNDO.

&GLOB REPORT-ID {&FILE-NAME}

{inc/ofc-this.i}
{inc/ofc-set.i "AcctGroup-Rent" "rent-group"}
{inc/ofc-set.i "AcctGroup-Opex" "opex-group"}
{inc/ofc-set.i "AcctGroup-Recoveries" "recover-group"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Log4 RP.Int3 RP.Char4 RP.Char1 RP.Char2 ~
RP.Int1 RP.Int2 RP.Char3 RP.Date1 RP.Log3 RP.Log1 RP.Log2 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Int3 ~{&FP2}Int3 ~{&FP3}~
 ~{&FP1}Char4 ~{&FP2}Char4 ~{&FP3}~
 ~{&FP1}Char2 ~{&FP2}Char2 ~{&FP3}~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}~
 ~{&FP1}Date1 ~{&FP2}Date1 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-1 cmb_start cmb_Finish Btn_OK 
&Scoped-Define DISPLAYED-FIELDS RP.Log4 RP.Int3 RP.Char4 RP.Char1 RP.Char2 ~
RP.Int1 RP.Int2 RP.Char3 RP.Date1 RP.Log3 RP.Log1 RP.Log2 
&Scoped-Define DISPLAYED-OBJECTS fil_Scenario fil_prop1 fil_prop2 cmb_start ~
cmb_Finish 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 12 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE cmb_Finish AS CHARACTER FORMAT "X(256)":U 
     LABEL "to" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "Item 1" 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_start AS CHARACTER FORMAT "X(256)":U 
     LABEL "Flows in effect from" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "Item 1" 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fil_prop1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 39.43 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_prop2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 39.43 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Scenario AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 39.43 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 67.43 BY 15.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Log4 AT ROW 1.4 COL 1.57 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Single Scenario", yes,
"Scenario Set", no
          SIZE 13.72 BY 2
     RP.Int3 AT ROW 1.4 COL 16.72 COLON-ALIGNED NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_Scenario AT ROW 1.4 COL 26.43 COLON-ALIGNED NO-LABEL
     RP.Char4 AT ROW 2.4 COL 16.72 COLON-ALIGNED HELP
          ""
          LABEL "Set" FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 49.14 BY 1
     RP.Char1 AT ROW 4.2 COL 1.57 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "All assumptions", "A":U,
"One entity", "1":U,
"Range of entities", "R":U
          SIZE 14.29 BY 2.7
          FONT 10
     RP.Char2 AT ROW 5 COL 14.43 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "X"
          VIEW-AS FILL-IN 
          SIZE 2.43 BY 1
     RP.Int1 AT ROW 5 COL 16.72 COLON-ALIGNED NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_prop1 AT ROW 5 COL 26.43 COLON-ALIGNED NO-LABEL
     RP.Int2 AT ROW 6 COL 16.72 COLON-ALIGNED NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_prop2 AT ROW 6 COL 26.43 COLON-ALIGNED NO-LABEL
     RP.Char3 AT ROW 8.4 COL 16.14
          LABEL "Type" FORMAT "X(256)"
          VIEW-AS COMBO-BOX INNER-LINES 12
          LIST-ITEMS "All types","All Rental types","All Service types","All Recoveries types" 
          SIZE 47.43 BY 1
     RP.Date1 AT ROW 9.6 COL 18.43 COLON-ALIGNED
          LABEL "Annual Total as at"
          VIEW-AS FILL-IN 
          SIZE 13.14 BY 1
     cmb_start AT ROW 11.4 COL 18.43 COLON-ALIGNED
     cmb_Finish AT ROW 11.4 COL 38.43 COLON-ALIGNED
     RP.Log3 AT ROW 13.2 COL 11.29
          LABEL "Show zero flows"
          VIEW-AS TOGGLE-BOX
          SIZE 14.29 BY .8
     RP.Log1 AT ROW 14 COL 11.29 HELP
          ""
          LABEL "Generation Assumptions Page"
          VIEW-AS TOGGLE-BOX
          SIZE 23.43 BY .8
          FONT 10
     RP.Log2 AT ROW 14.8 COL 11.29 HELP
          ""
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 8.57 BY .8
          FONT 10
     Btn_OK AT ROW 14.8 COL 55.86
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18
         WIDTH              = 74.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR COMBO-BOX RP.Char3 IN FRAME F-Main
   ALIGN-L EXP-LABEL EXP-FORMAT                                         */
/* SETTINGS FOR FILL-IN RP.Char4 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Date1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_prop1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_prop2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Scenario IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR RADIO-SET RP.Log4 IN FRAME F-Main
   EXP-HELP                                                             */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/entity.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  DISABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN view-hide-fields.
  RUN display-fill-ins.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON LEAVE OF RP.Char2 IN FRAME F-Main /* Char2 */
DO:
  IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN
    RUN display-fill-ins.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Finish
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Finish V-table-Win
ON U1 OF cmb_Finish IN FRAME F-Main /* to */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Finish V-table-Win
ON U2 OF cmb_Finish IN FRAME F-Main /* to */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_start
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_start V-table-Win
ON U1 OF cmb_start IN FRAME F-Main /* Flows in effect from */
DO:
  {inc/selcmb/scmths1.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_start V-table-Win
ON U2 OF cmb_start IN FRAME F-Main /* Flows in effect from */
DO:
  {inc/selcmb/scmths2.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Scenario
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Scenario V-table-Win
ON U1 OF fil_Scenario IN FRAME F-Main
DO:
  {inc/selfil/sfscn1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Scenario V-table-Win
ON U2 OF fil_Scenario IN FRAME F-Main
DO:
  {inc/selfil/sfscn2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Scenario V-table-Win
ON U3 OF fil_Scenario IN FRAME F-Main
DO:
  {inc/selfil/sfscn3.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Int1 */
DO:
  IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN
    RUN display-fill-ins.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* Int2 */
DO:
  IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN
    RUN display-fill-ins.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int3 V-table-Win
ON LEAVE OF RP.Int3 IN FRAME F-Main /* Int3 */
DO:
  {inc/selcde/cdscn.i "fil_Scenario"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Log4 */
DO:
  RUN view-hide-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-fill-ins V-table-Win 
PROCEDURE display-fill-ins :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  fil_prop1:SCREEN-VALUE = get-entity-name( RP.Char2:SCREEN-VALUE, INT(RP.Int1:SCREEN-VALUE) ).
  fil_prop2:SCREEN-VALUE = get-entity-name( RP.Char2:SCREEN-VALUE, INT(RP.Int2:SCREEN-VALUE) ).

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/
DEF VAR type-list AS CHAR INITIAL "" NO-UNDO.
DEF VAR delim AS CHAR INITIAL "|" NO-UNDO.

  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  FIND RP WHERE RP.ReportID = "{&REPORT-ID}"
                        AND RP.UserName = user-name
                        NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = "{&REPORT-ID}"
      RP.UserName = user-name
    .
  END.

DO WITH FRAME {&FRAME-NAME}:
  type-list = "All types|All rental types|All service types|All recoveries types".
  FOR EACH CashFlowType NO-LOCK:
    type-list = type-list + delim + STRING( CashFlowType.CashFlowType, "X(4)")
              + " - " + CashFlowType.Description.
  END.
  RP.Char3:DELIMITER = delim.
  RP.Char3:LIST-ITEMS = type-list.
  IF RP.Char3 = "" THEN RP.Char3 = RP.Char3:ENTRY(1).
END.

  fil_prop1 = get-entity-name( RP.Char2, RP.Int1 ).
  fil_prop2 = get-entity-name( RP.Char2, RP.Int2 ).

  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  RUN view-hide-fields.
  RUN display-fill-ins.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program.
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.
DEF VAR flow-types AS CHAR NO-UNDO.

  DO TRANSACTION:
    RUN dispatch IN THIS-PROCEDURE ('update-record':U).
  END.

  flow-types = RP.Char3.
  IF ENTRY( 1, flow-types, " ") = "All" THEN DO:
    flow-types = SUBSTRING( ENTRY( 2, flow-types, " "), 1, 4).
    CASE flow-types:
      WHEN "Serv" THEN flow-types = "SCL,MSCL~nStyle,Contract".
      WHEN "Rent" THEN flow-types = "RENT,MRNT~nStyle,Rental".
      WHEN "Reco" THEN flow-types = "RECV,MREC~nStyle,Recoveries".
      OTHERWISE     flow-types = "".
    END.
  END.
  ELSE
    flow-types = TRIM( ENTRY( 1, flow-types, " ")).

  report-options = "RangeStyle,"     + RP.Char1
                 + "~nEntity1,"        + STRING( RP.Int1 )
                 + "~nEntity2,"        + STRING( RP.Int2 )
                 + "~nTotalDate,"      + STRING( RP.Date1 )
                 + "~nEntityType,"     + RP.Char2
                 + "~nMonthRange,"     + STRING(RP.Int4) + "," + STRING(RP.Int5)
                 + "~nFlowTypes,"      + flow-types
                 + (IF RP.Log1 THEN "~nGenAssumptions," ELSE "")
                 + (IF RP.Log2 THEN "~nPreview" ELSE "")
                 + (IF RP.Log3 THEN "~nShowZero" ELSE "")
                 + (IF RP.Log4 THEN "~nScenario,"       + STRING( RP.Int3 )
                               ELSE "~nScenarioSet,"    + STRING( RP.Char4 ) )
                  .

  RUN notify( 'set-busy,container-source':U ).
  RUN process/report/scnass.p( report-options ).
  RUN notify( 'set-idle,container-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE view-hide-fields V-table-Win 
PROCEDURE view-hide-fields :
/*------------------------------------------------------------------------------
  Purpose: As its name!
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF INPUT RP.Log4 THEN DO:
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Scenario:HANDLE ), "HIDDEN = No" ).
    HIDE RP.Char4 .
    VIEW RP.Int3 fil_Scenario .
  END.
  ELSE DO:
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Scenario:HANDLE ), "HIDDEN = Yes" ).
    VIEW RP.Char4 .
    HIDE RP.Int3 fil_Scenario .
  END.

  CASE INPUT RP.Char1:
    WHEN "A" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Char2 RP.Int1 fil_prop1 RP.Int2 fil_prop2 .
    END.
    WHEN "1" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int2 fil_prop2 .
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = No" ).
      VIEW RP.Char2 RP.Int1 fil_prop1 .
    END.
    OTHERWISE DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = No" ).
      VIEW RP.Char2 RP.Int1 fil_prop1 RP.Int2 fil_prop2 .
    END.
  END CASE.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


