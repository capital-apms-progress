&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.

DEF VAR entity-type AS CHAR NO-UNDO INITIAL "?".
DEF VAR entity-code AS INT NO-UNDO INITIAL 0.
DEF VAR entity-type-name AS CHAR NO-UNDO INITIAL "".

{inc/persndtl.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES EntityContact Person
&Scoped-define FIRST-EXTERNAL-TABLE EntityContact


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR EntityContact, Person.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS EntityContact.EntityCode 
&Scoped-define ENABLED-TABLES EntityContact
&Scoped-define FIRST-ENABLED-TABLE EntityContact
&Scoped-define DISPLAYED-TABLES EntityContact
&Scoped-define FIRST-DISPLAYED-TABLE EntityContact
&Scoped-Define ENABLED-OBJECTS cmb_ContactType fil_FullName fil_JobTitle ~
fil_Company cmb_Address btn_Clear fil_City fil_state fil_Country fil_Zip ~
cmb_Phone1 fil_Phone1 cmb_Phone3 fil_Phone3 cmb_Phone2 fil_Phone2 ~
cmb_Phone4 fil_Phone4 RECT-27 
&Scoped-Define DISPLAYED-FIELDS EntityContact.EntityCode 
&Scoped-Define DISPLAYED-OBJECTS fil_EntityName cmb_ContactType ~
fil_FullName fil_JobTitle fil_Company cmb_Address edt_address fil_City ~
fil_state fil_Country fil_Zip cmb_Phone1 fil_Phone1 cmb_Phone3 fil_Phone3 ~
cmb_Phone2 fil_Phone2 cmb_Phone4 fil_Phone4 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS></FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = ':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-parent-entitycode V-table-Win 
FUNCTION get-parent-entitycode RETURNS INT
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Clear 
     LABEL "&Clear" 
     SIZE 12 BY 1.05
     FONT 10.

DEFINE VARIABLE cmb_Address AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 22.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_ContactType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contact Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 34.29 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Phone1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 12.57 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_Phone2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 12.57 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_Phone3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 12.57 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_Phone4 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 12.57 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE edt_address AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 46.86 BY 4
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_City AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1
     FONT 10.

DEFINE VARIABLE fil_Company AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 60.57 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Country AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1
     FONT 10.

DEFINE VARIABLE fil_EntityName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 56.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_FullName AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 60.57 BY 1
     FONT 10.

DEFINE VARIABLE fil_JobTitle AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 60.57 BY 1
     FONT 10.

DEFINE VARIABLE fil_Phone1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Phone2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Phone3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 22.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Phone4 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 22.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_state AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1
     FONT 10.

DEFINE VARIABLE fil_Zip AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1
     FONT 10.

DEFINE RECTANGLE RECT-27
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 72 BY 14.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     EntityContact.EntityCode AT ROW 1.2 COL 4.72 COLON-ALIGNED
          LABEL "Entity"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1.05
          BGCOLOR 16 
     fil_EntityName AT ROW 1.2 COL 13.86 COLON-ALIGNED NO-LABEL
     cmb_ContactType AT ROW 2.4 COL 9.86 COLON-ALIGNED
     fil_FullName AT ROW 3.4 COL 9.86 COLON-ALIGNED NO-LABEL
     fil_JobTitle AT ROW 4.4 COL 9.86 COLON-ALIGNED NO-LABEL
     fil_Company AT ROW 5.4 COL 11.86 NO-LABEL
     cmb_Address AT ROW 6.6 COL 9.86 COLON-ALIGNED NO-LABEL
     btn_Clear AT ROW 6.6 COL 36.43
     edt_address AT ROW 7.8 COL 1.57 NO-LABEL
     fil_City AT ROW 7.9 COL 55 COLON-ALIGNED NO-LABEL
     fil_state AT ROW 8.9 COL 55 COLON-ALIGNED NO-LABEL
     fil_Country AT ROW 9.9 COL 55 COLON-ALIGNED NO-LABEL
     fil_Zip AT ROW 10.9 COL 55 COLON-ALIGNED NO-LABEL
     cmb_Phone1 AT ROW 12.5 COL 1.57 NO-LABEL
     fil_Phone1 AT ROW 12.5 COL 12.14 COLON-ALIGNED NO-LABEL
     cmb_Phone3 AT ROW 12.5 COL 35.57 COLON-ALIGNED NO-LABEL
     fil_Phone3 AT ROW 12.5 COL 48.14 COLON-ALIGNED NO-LABEL
     cmb_Phone2 AT ROW 13.7 COL 1.57 NO-LABEL
     fil_Phone2 AT ROW 13.7 COL 12.14 COLON-ALIGNED NO-LABEL
     cmb_Phone4 AT ROW 13.7 COL 35.57 COLON-ALIGNED NO-LABEL
     fil_Phone4 AT ROW 13.8 COL 48.14 COLON-ALIGNED NO-LABEL
     RECT-27 AT ROW 1 COL 1
     "Telephones:" VIEW-AS TEXT
          SIZE 8.57 BY .8 AT ROW 11.8 COL 1.57
          FONT 10
     "Person:" VIEW-AS TEXT
          SIZE 5.72 BY 1 AT ROW 3.4 COL 1.57
          FONT 10
     "Zip:" VIEW-AS TEXT
          SIZE 4 BY 1 AT ROW 10.9 COL 50.72
          FONT 10
     "Country:" VIEW-AS TEXT
          SIZE 5.72 BY 1 AT ROW 9.9 COL 50.72
          FONT 10
     "State:" VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 8.9 COL 50.72
          FONT 10
     "Job Titile:" VIEW-AS TEXT
          SIZE 6.86 BY 1 AT ROW 4.4 COL 1.57
          FONT 10
     "Address:" VIEW-AS TEXT
          SIZE 6.29 BY 1 AT ROW 6.7 COL 1.57
          FONT 10
     "City:" VIEW-AS TEXT
          SIZE 4 BY 1 AT ROW 7.9 COL 50.72
          FONT 10
     "Company:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 5.4 COL 1.57
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.EntityContact,TTPL.Person
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.5
         WIDTH              = 77.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/entity.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cmb_Phone1 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Phone2 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR EDITOR edt_address IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       edt_address:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR FILL-IN EntityContact.EntityCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Company IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fil_EntityName IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Clear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Clear V-table-Win
ON CHOOSE OF btn_Clear IN FRAME F-Main /* Clear */
DO:
  RUN clear-address.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Address
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Address V-table-Win
ON VALUE-CHANGED OF cmb_Address IN FRAME F-Main
DO:
  RUN address-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_ContactType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ContactType V-table-Win
ON U1 OF cmb_ContactType IN FRAME F-Main /* Contact Type */
DO:
  {inc/selcmb/scectyp1.i "EntityContact" "EntityContactType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ContactType V-table-Win
ON U2 OF cmb_ContactType IN FRAME F-Main /* Contact Type */
DO:
  {inc/selcmb/scectyp2.i "EntityContact" "EntityContactType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone1 V-table-Win
ON VALUE-CHANGED OF cmb_Phone1 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone2 V-table-Win
ON VALUE-CHANGED OF cmb_Phone2 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone3 V-table-Win
ON VALUE-CHANGED OF cmb_Phone3 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone4 V-table-Win
ON VALUE-CHANGED OF cmb_Phone4 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edt_address
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edt_address V-table-Win
ON LEAVE OF edt_address IN FRAME F-Main
DO:
  IF SELF:MODIFIED THEN RUN address-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone1 V-table-Win
ON LEAVE OF fil_Phone1 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone2 V-table-Win
ON LEAVE OF fil_Phone2 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone3 V-table-Win
ON LEAVE OF fil_Phone3 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone4 V-table-Win
ON LEAVE OF fil_Phone4 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Zip
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Zip V-table-Win
ON F3 OF fil_Zip IN FRAME F-Main
DO:
DEF VAR city AS CHAR NO-UNDO.
DEF VAR state AS CHAR NO-UNDO.
DEF VAR postcode AS CHAR NO-UNDO.
  DO WITH FRAME {&FRAME-NAME}:
    city = INPUT fil_City.
    state = INPUT fil_State.
    RUN process\postcode.p( city, state, OUTPUT postcode ).
/*
    IF postcode <> ? THEN DO:
      fil_Zip = postcode.
      DISPLAY fil_Zip.
    END.
*/
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

ON "ANY-PRINTABLE", "BACKSPACE", "DELETE-CHARACTER" OF fil_FullName, fil_Company DO:
  APPLY LASTKEY TO SELF.
  RUN person-details-changed.
  RETURN NO-APPLY.
END.

DO WITH FRAME {&FRAME-NAME}:
  edt_Address:READ-ONLY = Yes.
  edt_Address:SENSITIVE = No.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE address-changed V-table-Win 
PROCEDURE address-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN update-address.
  RUN display-address.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "EntityContact"}
  {src/adm/template/row-list.i "Person"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "EntityContact"}
  {src/adm/template/row-find.i "Person"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-contact. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clean-previous-contact V-table-Win 
PROCEDURE clean-previous-contact :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER prev-person LIKE Person.PersonCode NO-UNDO.

DEF BUFFER AltContact FOR Contact.
DEF BUFFER AltPerson FOR Person.

  FIND AltPerson WHERE AltPerson.PersonCode = prev-person NO-ERROR.
  IF NOT AVAILABLE(AltPerson) THEN RETURN.

  FIND AltContact OF AltPerson WHERE AltContact.ContactType = "ENT" + EntityContact.EntityType EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE(AltContact) THEN DELETE AltContact.

  /* don't delete if they have other contact types */
  FIND FIRST AltContact OF AltPerson NO-LOCK NO-ERROR.
  IF AVAILABLE(AltContact) THEN RETURN.

  DELETE AltPerson.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-address V-table-Win 
PROCEDURE clear-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  edt_Address:SCREEN-VALUE = "".
  fil_City:SCREEN-VALUE = "".
  fil_State:SCREEN-VALUE = "".
  fil_Country:SCREEN-VALUE = "".
  fil_Zip:SCREEN-VALUE = "".
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN verify-contact.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).
  RUN update-entity.
  IF mode = "Add" THEN RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-contact V-table-Win 
PROCEDURE delete-contact :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FIND CURRENT EntityContact EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE EntityContact THEN DELETE EntityContact.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-address V-table-Win 
PROCEDURE display-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR idx         AS INT  NO-UNDO.
  DEF VAR postal-type AS CHAR NO-UNDO.
  
  idx = LOOKUP( INPUT cmb_Address, cmb_Address:LIST-ITEMS ).
  postal-type = ENTRY( idx, cmb_Address:PRIVATE-DATA ).

  FIND PostalDetail WHERE
    PostalDetail.PersonCode = Person.PersonCode AND
    PostalDetail.PostalType = postal-type
    NO-LOCK NO-ERROR.

  ASSIGN
    edt_address = IF AVAILABLE Postaldetail THEN PostalDetail.Address ELSE ""
    fil_City    = IF AVAILABLE Postaldetail THEN PostalDetail.City    ELSE ""
    fil_State   = IF AVAILABLE Postaldetail THEN PostalDetail.State   ELSE ""
    fil_Zip     = IF AVAILABLE Postaldetail THEN PostalDetail.Zip     ELSE ""
    fil_Country = IF AVAILABLE Postaldetail THEN PostalDetail.Country ELSE "".
    
  DISPLAY edt_address fil_City fil_State fil_Zip fil_Country WITH FRAME {&FRAME-NAME}.
  edt_Address:PRIVATE-DATA = postal-type.
   
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-person V-table-Win 
PROCEDURE display-person :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER person-code LIKE Person.PersonCode NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  FIND Person WHERE Person.PersonCode = person-code NO-LOCK NO-ERROR.

  IF AVAILABLE Person THEN ASSIGN
    fil_FullName = combine-name( Person.PersonTitle, Person.FirstName, Person.MiddleName,
                                     Person.LastName, Person.NameSuffix )
    fil_JobTitle = Person.JobTitle
    fil_Company  = Person.Company.
  ELSE ASSIGN
    fil_FullName = ""
    fil_JobTitle = ""
    fil_Company  = "".

  IF (entity-type = "T" OR entity-type = "C") AND (fil_Company = ? OR fil_Company = "") THEN DO:
    fil_Company = fil_EntityName .
  END.
      
  DISPLAY fil_FullName fil_JobTitle fil_Company WITH FRAME {&FRAME-NAME}.
  ASSIGN fil_Company:PRIVATE-DATA = STRING( person-code ).
     
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-phone V-table-Win 
PROCEDURE display-phone :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:


  DEF VAR idx        AS INT  NO-UNDO.
  DEF VAR phone-type AS CHAR NO-UNDO.
  DEF VAR sv         AS CHAR NO-UNDO INITIAL "".

  idx = LOOKUP( h-cmb:SCREEN-VALUE, h-cmb:LIST-ITEMS ).
  phone-type = ENTRY( idx, h-cmb:PRIVATE-DATA ).
  
  FIND PhoneDetail WHERE
    PhoneDetail.PersonCode = Person.PersonCode AND
    PhoneDetail.PhoneType  = phone-type
    NO-LOCK NO-ERROR.

  IF AVAILABLE PhoneDetail THEN
    RUN combine-phone( PhoneDetail.cCountryCode, PhoneDetail.cSTDCode, PhoneDetail.Number,
      OUTPUT sv ).
  
  h-fil:SCREEN-VALUE = sv.
  h-fil:PRIVATE-DATA = phone-type.  

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  EntityContact.EntityType = entity-type.
  EntityContact.EntityCode = entity-code.

  RUN update-person.

  IF EntityContact.PersonCode <> INT( fil_Company:PRIVATE-DATA ) THEN DO:
    RUN clean-previous-contact( EntityContact.PersonCode ).
    ASSIGN EntityContact.PersonCode = INT( fil_Company:PRIVATE-DATA ).
  END.

  RUN update-address.
  RUN update-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN update-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN update-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN update-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-disable-fields V-table-Win 
PROCEDURE inst-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  DISABLE btn_Clear fil_Company fil_FullName EntityContact.EntityCode .
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, 'window', 'hidden=yes').
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN display-person( IF fil_Company:PRIVATE-DATA = ? AND mode <> "Add" THEN
    EntityContact.PersonCode ELSE INT( fil_Company:PRIVATE-DATA ) ).
  RUN set-default-address-type.
  RUN display-address.
  RUN refresh-phone-types.
  RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
  RUN person-details-changed.
END.

  DISABLE EntityContact.EntityCode WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE(EntityContact) THEN
    RUN sensitise-address-details( CAN-FIND( Person OF EntityContact ) ).

  DISABLE EntityContact.EntityCode WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  CURRENT-WINDOW = THIS-PROCEDURE:CURRENT-WINDOW.

  CURRENT-WINDOW:TITLE = (IF mode = "Add" THEN "Adding a new " + entity-type-name + " record"
                          ELSE mode + "ing " + entity-type + STRING(entity-code) + " - " + fil_EntityName).
  RUN update-node IN sys-mgr ( THIS-PROCEDURE ).

  IF mode = "Add" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "Duplicate" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "View" THEN DO:
    RUN dispatch( 'disable-fields':U ).
  END.
  ELSE
    RUN dispatch( 'enable-fields':U ).

  DISABLE EntityContact.EntityCode WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE new-contact-selected V-table-Win 
PROCEDURE new-contact-selected :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER person-code LIKE Person.PersonCode NO-UNDO.

  FIND Person NO-LOCK WHERE Person.PersonCode = person-code NO-ERROR.
  IF NOT AVAILABLE(Person) THEN RETURN.

  DEF VAR priori-list  AS CHAR NO-UNDO.
  DEF VAR i AS INT NO-UNDO.
  priori-list = "POST,MAIN,COUR".
  FIND PostalDetail NO-LOCK OF Person WHERE PostalDetail.PostalType = "POST" NO-ERROR.
  IF NOT AVAILABLE(PostalDetail) THEN DO:
    DEF BUFFER POSTAddress FOR PostalDetail.

     DO i = 1 TO 3 WHILE NOT AVAILABLE(PostalDetail):
       FIND PostalDetail NO-LOCK OF Person WHERE PostalDetail.PostalType = ENTRY( i, priori-list) NO-ERROR.
     END.

     IF AVAILABLE(PostalDetail) THEN DO:
       CREATE POSTAddress.
       BUFFER-COPY PostalDetail TO POSTAddress ASSIGN POSTAddress.PostalType = "POST".
     END.
  END.

  RUN person-changed( person-code ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR person-code AS INT NO-UNDO.

  CREATE EntityContact.
  EntityContact.EntityType = entity-type.
  EntityContact.EntityCode = entity-code.

  IF mode = "Duplicate" THEN DO:
    EntityContact.PersonCode = INT( find-parent-key( "PersonCode":U ) ).
  END.

  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'display-fields':U ).
  
  CURRENT-WINDOW:TITLE = "Adding a new " + entity-type-name + " Contact".

  mode = "Add".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE person-changed V-table-Win 
PROCEDURE person-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER person-code LIKE Person.PersonCode NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  fil_fullName:SCREEN-VALUE = "". fil_company:SCREEN-VALUE = "".
  RUN update-person.
  RUN display-person( person-code ).
  RUN set-default-address-type.
  RUN display-address.
  RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
  RUN person-details-changed.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE person-details-changed V-table-Win 
PROCEDURE person-details-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN sensitise-address-details(
    NOT ( INPUT fil_fullname = "" AND INPUT fil_Company = "" ) ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE phone-changed V-table-Win 
PROCEDURE phone-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.
  RUN update-phone( h-cmb, h-fil ).
  RUN display-phone( h-cmb, h-fil ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize V-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN refresh-address-types.
  RUN refresh-phone-types.

  entity-type = find-parent-key( "EntityType" ).
  entity-code = get-parent-entitycode().
  RUN set-attribute-list( "EntityType = " + entity-type ).
  fil_EntityName = get-entity-name( entity-type, entity-code ).
  DISPLAY fil_EntityName WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-address-types V-table-Win 
PROCEDURE refresh-address-types :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item         AS CHAR NO-UNDO.
  DEF VAR pd           AS CHAR NO-UNDO.

  cmb_Address:LIST-ITEMS = "".

  FOR EACH PostalType NO-LOCK :
    item = PostalType.Description.
    IF cmb_Address:ADD-LAST( item ) THEN.
    pd = pd + IF pd = "" THEN "" ELSE ",".
    pd = pd + PostalType.PostalType.
  END.
  
  cmb_address:SCREEN-VALUE = cmb_address:ENTRY( 1 ).
  edt_address:PRIVATE-DATA = ENTRY( 1, cmb_address:PRIVATE-DATA ).
  cmb_address:PRIVATE-DATA = pd.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-phone-types V-table-Win 
PROCEDURE refresh-phone-types :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR pd   AS CHAR NO-UNDO.
  DEF VAR li   AS CHAR NO-UNDO.
  DEF VAR sv-list   AS CHAR NO-UNDO INITIAL "".
  DEF VAR pd-list   AS CHAR NO-UNDO INITIAL "".
  DEF VAR sv-list2  AS CHAR NO-UNDO INITIAL "".
  DEF VAR pd-list2  AS CHAR NO-UNDO INITIAL "".
  DEF VAR i         AS INT  NO-UNDO.
    
  DO i = 1 TO NUM-ENTRIES( phone-types ):
    pd = ENTRY( i, phone-types ).
    FIND PhoneType WHERE PhoneType = pd NO-LOCK NO-ERROR.
    li = (IF AVAILABLE(PhoneType) THEN PhoneType.Description ELSE pd).
    IF AVAILABLE(Person) AND CAN-FIND( PhoneDetail OF Person WHERE PhoneDetail.PhoneType = pd)
    THEN DO:
      sv-list = sv-list + "," + li.
      pd-list = pd-list + "," + pd.
    END.
    ELSE DO:
      sv-list2 = sv-list2 + "," + li.
      pd-list2 = pd-list2 + "," + pd.
    END.
  END.
  IF NUM-ENTRIES(sv-list) < 5 THEN DO:
    sv-list = sv-list + sv-list2.
    pd-list = pd-list + pd-list2.
  END.
  sv-list = TRIM(sv-list, ",").
  pd-list = TRIM(pd-list, ",").

  li = "".
  pd = "".
  FOR EACH PhoneType NO-LOCK:
    li = li + (IF li = "" THEN "" ELSE ",") + PhoneType.Description.
    pd = pd + (IF pd = "" THEN "" ELSE ",") + PhoneType.PhoneType.
  END.

  cmb_Phone1:LIST-ITEMS = li.  cmb_Phone2:LIST-ITEMS = li.
  cmb_Phone3:LIST-ITEMS = li.  cmb_Phone4:LIST-ITEMS = li.

  cmb_Phone1:SCREEN-VALUE = ENTRY( 1, sv-list ).
  cmb_Phone2:SCREEN-VALUE = ENTRY( 2, sv-list ).
  cmb_Phone3:SCREEN-VALUE = ENTRY( 3, sv-list ).
  cmb_Phone4:SCREEN-VALUE = ENTRY( 4, sv-list ).
  
  fil_Phone1:PRIVATE-DATA = ENTRY( 1, phone-types ).
  fil_Phone2:PRIVATE-DATA = ENTRY( 2, phone-types ).
  fil_Phone3:PRIVATE-DATA = ENTRY( 3, phone-types ).
  fil_Phone4:PRIVATE-DATA = ENTRY( 4, phone-types ).

  cmb_Phone1:PRIVATE-DATA = pd.  cmb_Phone2:PRIVATE-DATA = pd.
  cmb_Phone3:PRIVATE-DATA = pd.  cmb_Phone4:PRIVATE-DATA = pd.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* There are no foreign keys supplied by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "EntityContact"}
  {src/adm/template/snd-list.i "Person"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-address-details V-table-Win 
PROCEDURE sensitise-address-details :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER enable-it AS LOGI NO-UNDO.
  IF mode = "View" THEN enable-it = No.

  /* Use the editor as the test subject because the default smart object code
     enables widgets when they shouldn't be */

  /* IF edt_address:SENSITIVE <> enable-it THEN */ DO:
    cmb_address:SENSITIVE   = enable-it.
    edt_address:READ-ONLY   = NOT enable-it.
    edt_address:SENSITIVE   = enable-it.
    fil_city:SENSITIVE      = enable-it.
    fil_state:SENSITIVE     = enable-it.
    fil_zip:SENSITIVE       = enable-it.
    fil_Country:SENSITIVE   = enable-it.
    cmb_Phone1:SENSITIVE    = enable-it.
    fil_Phone1:SENSITIVE    = enable-it.
    cmb_Phone2:SENSITIVE    = enable-it.
    fil_Phone2:SENSITIVE    = enable-it.
    cmb_Phone3:SENSITIVE    = enable-it.
    fil_Phone3:SENSITIVE    = enable-it.
    cmb_Phone4:SENSITIVE    = enable-it.
    fil_Phone4:SENSITIVE    = enable-it.
    btn_clear:SENSITIVE     = enable-it.
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-default-address-type V-table-Win 
PROCEDURE set-default-address-type :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR got-address  AS LOGI NO-UNDO.
  DEF VAR priori-list  AS CHAR NO-UNDO.
  DEF VAR default-type AS CHAR NO-UNDO.
  DEF VAR i            AS INT  NO-UNDO.
  DEF VAR default-item AS CHAR NO-UNDO.
  
  priori-list = "BILL,PYMT,POST,MAIN,COUR".
  
  DO WHILE NOT got-address AND i < NUM-ENTRIES( priori-list ):
    
    i = i + 1.
    FIND PostalDetail OF Person WHERE PostalDetail.PostalType = ENTRY( i, priori-list )
      NO-LOCK NO-ERROR.
    got-address = AVAILABLE PostalDetail.
    
  END.
  
  IF NOT AVAILABLE PostalDetail THEN
    FIND FIRST PostalDetail OF Person /* WHERE PostalDetail.PostalType <> "BILL" */
      NO-LOCK NO-ERROR.
      
  default-type = IF AVAILABLE PostalDetail THEN
    PostalDetail.PostalType ELSE ENTRY( 1, priori-list ).
  
  IF default-type = "" THEN
  DO:
    FIND FIRST PostalType /* WHERE PostalType.PostalType <> "BILL" */
      NO-LOCK NO-ERROR.
    IF AVAILABLE Postaltype THEN default-type = Postaltype.PostalType.
  END.
  
  ASSIGN default-item = ENTRY( LOOKUP( default-type, cmb_address:PRIVATE-DATA ), cmb_address:LIST-ITEMS )
    NO-ERROR.
  
  cmb_address:SCREEN-VALUE = IF ERROR-STATUS:ERROR THEN "" ELSE default-item.

  RUN display-address.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-address V-table-Win 
PROCEDURE update-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR del-record AS LOGI NO-UNDO.

  IF NOT AVAILABLE Person THEN RUN update-person.
  IF NOT AVAILABLE Person THEN RETURN.

  FIND PostalDetail WHERE
    PostalDetail.PersonCode = Person.PersonCode AND
    PostalDetail.PostalType = edt_Address:PRIVATE-DATA NO-ERROR.
  
  ASSIGN FRAME {&FRAME-NAME} edt_address fil_state fil_zip fil_country fil_city.
  del-record = edt_address = "" AND fil_zip = "" AND fil_state = "" AND fil_country = "" AND fil_City = "".

  IF del-record THEN
    IF AVAILABLE PostalDetail THEN
      DELETE PostalDetail.
    ELSE DO: END.
  ELSE
  DO:
    IF NOT AVAILABLE PostalDetail THEN
    DO:
      CREATE PostalDetail.
      ASSIGN
        PostalDetail.PersonCode = Person.PersonCode
        PostalDetail.PostalType = edt_Address:PRIVATE-DATA.
    END.

    ASSIGN
      PostalDetail.Address = TRIM( edt_Address )
      PostalDetail.City    = fil_city
      PostalDetail.State   = fil_State
      PostalDetail.Zip     = fil_Zip
      PostalDetail.Country = fil_Country.      
  
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity V-table-Win 
PROCEDURE update-entity :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF EntityContact.EntityType = "T" THEN DO:
    IF NOT CAN-DO( "ACCT,PROP,AH1,AH2", EntityContact.EntityContactType ) THEN RETURN.
    FIND Tenant WHERE Tenant.TenantCode = EntityContact.EntityCode EXCLUSIVE-LOCK NO-ERROR.
    CASE EntityContact.EntityContactType:
      WHEN "ACCT" THEN DO:
        /* If we can't find a billing address for the new contact, assume
           that the existing billing address will continue in use. If
           there isn't one of those we will let the error filter up
        */
        IF NOT CAN-FIND( PostalDetail WHERE PostalDetail.PostalType = 'BILL'
                     AND PostalDetail.PersonCode = EntityContact.PersonCode ) THEN DO:
          FIND PostalDetail WHERE PostalDetail.PostalType = 'BILL'
                       AND PostalDetail.PersonCode = Tenant.BillingContact 
                       EXCLUSIVE-LOCK NO-ERROR.
          IF AVAILABLE(PostalDetail) THEN
            PostalDetail.PersonCode = EntityContact.PersonCode .
          ELSE
            MESSAGE 'Accounting contact has no billing address for' SKIP
                    'tenant!  This might be a problem for invoicing.'
                    VIEW-AS ALERT-BOX WARNING TITLE
                    Tenant.Name + " (T" + TRIM(STRING(Tenant.TenantCode,">>>>>9")) + ") has no billing address".
        END.
        Tenant.BillingContact = EntityContact.PersonCode.
      END.
      WHEN "PROP" THEN Tenant.PropertyContact = EntityContact.PersonCode.
      WHEN "AH1"  THEN Tenant.AH1Contact = EntityContact.PersonCode.
      WHEN "AH2"  THEN Tenant.AH2Contact = EntityContact.PersonCode.
    END CASE.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-person V-table-Win 
PROCEDURE update-person :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR entity-contact-type AS CHAR NO-UNDO INITIAL "".
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR del-record AS LOGI NO-UNDO.

  IF NOT AVAILABLE EntityContact THEN RETURN.
  entity-contact-type = "ENT" + entity-type.

  FIND Person WHERE Person.PersonCode = INT( fil_Company:PRIVATE-DATA ) NO-ERROR.
  
  ASSIGN FRAME {&FRAME-NAME} fil_fullName fil_company fil_JobTitle edt_address fil_City.
  del-record = fil_FullName = "" AND fil_Company = ""
                   AND edt_address = "" AND fil_City = "".

  IF del-record THEN
    IF AVAILABLE Person THEN
    DO:
      IF NOT CAN-FIND( FIRST Contact OF Person WHERE Contact.ContactType <> entity-contact-type )
        THEN DELETE Person.
      fil_Company:PRIVATE-DATA = STRING( 0 ).
    END.
    ELSE DO: END.
  ELSE
  DO:
    IF NOT AVAILABLE Person THEN CREATE Person.
    FIND Contact NO-LOCK OF Person WHERE Contact.ContactType = entity-contact-type NO-ERROR.
    IF NOT AVAILABLE(Contact) THEN DO:
      CREATE Contact.
      ASSIGN
        Contact.PersonCode  = Person.PersonCode
        Contact.ContactType = entity-contact-type.
    END.

    FIND FIRST Contact NO-LOCK OF Person WHERE Contact.ContactType <> entity-contact-type
        AND CAN-FIND( ContactType OF Contact WHERE NOT ContactType.SystemCode) NO-ERROR.
    Person.SystemContact = NOT AVAILABLE(Contact).

    RUN split-name( INPUT fil_FullName, OUTPUT Person.PersonTitle, OUTPUT Person.FirstName,
            OUTPUT Person.MiddleName, OUTPUT Person.LastName, OUTPUT Person.NameSuffix ).

    ASSIGN
      Person.Company = fil_Company
      Person.JobTitle = fil_JobTitle
      fil_Company:PRIVATE-DATA = STRING( Person.PersonCode ).

  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-phone V-table-Win 
PROCEDURE update-phone :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.
  
  DEF VAR del-record AS LOGI NO-UNDO.

  IF NOT AVAILABLE Person THEN RUN update-person.
  IF NOT AVAILABLE Person THEN RETURN.

  FIND PhoneDetail WHERE
    PhoneDetail.PersonCode = Person.PersonCode AND
    PhoneDetail.PhoneType = h-fil:PRIVATE-DATA   NO-ERROR.

  del-record = TRIM( h-fil:SCREEN-VALUE ) = "".

  IF del-record THEN DO:
    IF AVAILABLE PhoneDetail THEN DELETE PhoneDetail.
  END.
  ELSE DO:
    IF NOT AVAILABLE PhoneDetail THEN
    DO:
      CREATE PhoneDetail.
      ASSIGN
        PhoneDetail.PersonCode = Person.PersonCode
        PhoneDetail.PhoneType  = h-fil:PRIVATE-DATA.
    END.
    
    RUN split-phone( h-fil:SCREEN-VALUE, OUTPUT PhoneDetail.cCountryCode,
                      OUTPUT PhoneDetail.cSTDCode, OUTPUT PhoneDetail.Number ).
      
  END.

  /* Reflect changes to other linked numbers on screen */
  
  DEF VAR update-type AS CHAR NO-UNDO. /* Do not delete this variable!! */
  update-type = h-fil:PRIVATE-DATA.

  IF fil_Phone1:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
        
  IF fil_Phone2:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
        
  IF fil_Phone3:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
        
  IF fil_Phone4:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-entitytype V-table-Win 
PROCEDURE use-entitytype :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-entity-type AS CHAR NO-UNDO.

  IF new-entity-type = entity-type THEN RETURN.

  FIND EntityType WHERE EntityType.EntityType = new-entity-type NO-LOCK NO-ERROR.
  IF AVAILABLE(EntityType) THEN DO:
    entity-type = new-entity-type.
    entity-type-name = EntityType.Description.
    EntityContact.EntityCode:LABEL IN FRAME {&FRAME-NAME} = entity-type-name.
  END.
  ELSE DO:
    MESSAGE "Entity type  of '" + new-entity-type + "' is not supported by this program!"
            VIEW-AS ALERT-BOX ERROR.
    mode = 'View'.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
  have-records = mode = "Add".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-contact V-table-Win 
PROCEDURE verify-contact :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-parent-entitycode V-table-Win 
FUNCTION get-parent-entitycode RETURNS INT
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR parent-key AS CHAR NO-UNDO.
DEF VAR entity-key AS CHAR NO-UNDO.
DEF VAR loc-entity-code AS INT NO-UNDO.

  /* try using 'EntityCode' key name */
  entity-type = find-parent-key( "EntityType" ).
  entity-key = find-parent-key( "EntityCode" ).
  ASSIGN loc-entity-code = INT(entity-key) NO-ERROR.
  IF loc-entity-code <> ? THEN RETURN loc-entity-code.

  /* try using 'entity' key-name */
  parent-key = find-parent-key( "Entity" ).
  entity-key = ENTRY( 2, parent-key, "/" ).
  ASSIGN loc-entity-code = INT(entity-key) NO-ERROR.
  IF loc-entity-code <> ? THEN RETURN loc-entity-code.

  MESSAGE "Could not retrieve parent key code!" VIEW-AS ALERT-BOX ERROR.

  RETURN ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

