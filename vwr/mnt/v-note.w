&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode           AS CHAR NO-UNDO.
DEF VAR rec-src        AS HANDLE NO-UNDO.
DEF VAR ext-rowid      AS CHAR NO-UNDO. /* The rowid of the external entity */
DEF VAR external-table AS CHAR NO-UNDO INITIAL "".
DEF VAR key-name       AS CHAR NO-UNDO.

/* ensure Note its scoped to entire procedure */
FIND Note NO-LOCK NO-ERROR.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Note
&Scoped-define FIRST-EXTERNAL-TABLE Note


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Note.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Note.Details 
&Scoped-define FIELD-PAIRS
&Scoped-define ENABLED-TABLES Note
&Scoped-define FIRST-ENABLED-TABLE Note
&Scoped-Define DISPLAYED-FIELDS Note.Details 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
NoteCode|y||TTPL.Note.NoteCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "NoteCode",
     Keys-Supplied = ':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD make-note V-table-Win 
FUNCTION make-note RETURNS INTEGER
  ( INPUT note-text AS CHAR, INPUT note-key AS CHAR, INPUT note-file AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Note.Details AT ROW 1 COL 1 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 33.72 BY 2.6
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Note
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 3
         WIDTH              = 35.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Note.Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Note.Details V-table-Win
ON LEAVE OF Note.Details IN FRAME F-Main /* Details */
DO:
  IF Note.Details:MODIFIED THEN DO:
    RUN assign-note( IF AVAILABLE Note THEN Note.NoteCode ELSE ? ).
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list( 'Key-Name = NoteCode':U ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'NoteCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Note
           &WHERE = "WHERE Note.NoteCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Note"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Note"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-note V-table-Win 
PROCEDURE assign-note :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  /* For some reason the Note record's availablity is lost between the
     call in the leave trigger and this bit of code - so we pass the code
     as a parameter! */
DEF INPUT PARAMETER note-code LIKE Note.NoteCode NO-UNDO.

DEF VAR is-null        AS LOGI NO-UNDO.
DEF VAR key-value      AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  is-null = TRIM( INPUT Note.Details ) = "" OR INPUT Note.Details = ?.
  FIND Note WHERE Note.NoteCode = note-code NO-ERROR.

  IF ext-rowid = ? THEN RETURN.

  IF is-null THEN DO:
    IF AVAILABLE Note THEN DELETE Note. ELSE RETURN.
    note-code = 0.
  END.
  ELSE DO:
    IF NOT AVAILABLE Note THEN DO:
      CASE external-table:
        WHEN "Tenant" THEN DO:
          FIND Tenant WHERE ROWID( Tenant ) = TO-ROWID( ext-rowid ) NO-ERROR.
          key-value = STRING(Tenant.TenantCode).
        END.
        WHEN "Property" THEN DO:
          FIND Property WHERE ROWID( Property ) = TO-ROWID( ext-rowid ) NO-ERROR.
          key-value = STRING(Property.PropertyCode).
        END.
        WHEN "RentalSpace" THEN DO:
          FIND RentalSpace WHERE ROWID( RentalSpace ) = TO-ROWID( ext-rowid ) NO-ERROR.
          key-value = STRING(RentalSpace.PropertyCode) + "/" + STRING(RentalSpace.RentalSpaceCode).
        END.
        WHEN "TenancyLease" THEN DO:
          FIND TenancyLease WHERE ROWID( TenancyLease ) = TO-ROWID( ext-rowid ) NO-ERROR.
          key-value = STRING(TenancyLease.TenancyLeaseCode).
        END.
        WHEN "Contract" THEN DO:
          FIND Contract WHERE ROWID( Contract ) = TO-ROWID( ext-rowid ) NO-ERROR.
          key-value = STRING(Contract.PropertyCode) + "/"
                    + Contract.ServiceType + "/"
                    + STRING(Contract.CreditorCode) + "/"
                    + STRING(Contract.ContractSeq).
        END.
        WHEN "AccountSummary" THEN DO:
          FIND AccountSummary WHERE ROWID( AccountSummary ) = TO-ROWID( ext-rowid ) NO-ERROR.
          key-value = AccountSummary.EntityType + "/" 
                    + STRING( AccountSummary.EntityCode ) + "/"
                    + STRING( AccountSummary.AccountCode ).
        END.
        WHEN "AccountBalance" THEN DO:
          FIND AccountBalance WHERE ROWID( AccountBalance ) = TO-ROWID( ext-rowid ) NO-ERROR.
          key-value = AccountBalance.EntityType + "/" 
                    + STRING( AccountBalance.EntityCode ) + "/"
                    + STRING( AccountBalance.AccountCode ) + "/"
                    + STRING( AccountBalance.MonthCode).
        END.
        WHEN "SubLease" THEN DO:
          FIND SubLease WHERE ROWID( SubLease ) = TO-ROWID( ext-rowid ) NO-ERROR.
          key-value = STRING( SubLease.TenancyLeaseCode ) + "/"
                    + STRING( SubLease.SubLeaseCode ).
        END.
        OTHERWISE DO:
          MESSAGE "Unknown external table for NoteViewer: '" + external-table + "'"
                VIEW-AS ALERT-BOX ERROR
                TITLE "Cannot Attach Note to " + external-table + " Record".
        END.
      END CASE.
      note-code = make-note( INPUT INPUT FRAME {&FRAME-NAME} Note.Details , external-table, key-value ).
    END.
    ELSE DO:
      IF INPUT Note.Details <> Note.Details THEN
        ASSIGN Note.Details = INPUT Note.Details.
      note-code = Note.NoteCode.
    END.
  END.
  
  Note.Details:MODIFIED = No.
  CASE external-table:
    WHEN "Tenant" THEN DO:
      FIND Tenant WHERE ROWID( Tenant ) = TO-ROWID( ext-rowid ) NO-ERROR.
      IF AVAILABLE Tenant THEN ASSIGN Tenant.NoteCode = note-code.
    END.
    WHEN "Property" THEN DO:
      FIND Property WHERE ROWID( Property ) = TO-ROWID( ext-rowid ) NO-ERROR.
      IF AVAILABLE Property THEN ASSIGN Property.NoteCode = note-code.
    END.
    WHEN "RentalSpace" THEN DO:
      FIND RentalSpace WHERE ROWID( RentalSpace ) = TO-ROWID( ext-rowid ) NO-ERROR.
      IF AVAILABLE RentalSpace THEN ASSIGN RentalSpace.NoteCode = note-code.
    END.
    WHEN "TenancyLease" THEN DO:
      FIND TenancyLease WHERE ROWID( TenancyLease ) = TO-ROWID( ext-rowid ) NO-ERROR.
      IF AVAILABLE TenancyLease THEN ASSIGN TenancyLease.NoteCode = note-code.
    END.
    WHEN "Contract" THEN DO:
      FIND Contract WHERE ROWID( Contract ) = TO-ROWID( ext-rowid ) NO-ERROR.
      IF AVAILABLE Contract THEN ASSIGN Contract.NoteCode = note-code.
    END.
    WHEN "AccountSummary" THEN DO:
      FIND AccountSummary WHERE ROWID( AccountSummary ) = TO-ROWID( ext-rowid ) NO-ERROR.
      IF AVAILABLE AccountSummary THEN ASSIGN AccountSummary.NoteCode = note-code.
    END.
    WHEN "AccountBalance" THEN DO:
      FIND AccountBalance WHERE ROWID( AccountBalance ) = TO-ROWID( ext-rowid ) NO-ERROR.
      IF AVAILABLE AccountBalance THEN ASSIGN AccountBalance.NoteCode = note-code.
    END.
    WHEN "SubLease" THEN DO:
      FIND SubLease WHERE ROWID( SubLease ) = TO-ROWID( ext-rowid ) NO-ERROR.
      IF AVAILABLE SubLease THEN ASSIGN SubLease.NoteCode = note-code.
    END.
    OTHERWISE DO:
      MESSAGE "Unknown external table for NoteViewer: '" + external-table + "'"
            VIEW-AS ALERT-BOX ERROR
            TITLE "Cannot Attach Note to " + external-table + " Record".
      RETURN.
    END.
  END CASE.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable-edits V-table-Win 
PROCEDURE disable-edits :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN set-attribute IN THIS-PROCEDURE ( 'Mode = View' ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-edits V-table-Win 
PROCEDURE enable-edits :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN set-attribute IN THIS-PROCEDURE ( 'Mode = Maintain' ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-external-id V-table-Win 
PROCEDURE get-external-id :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF external-table = "" THEN DO:
    RUN request-attribute IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE', "INTERNAL-TABLES" ).
    external-table = ENTRY( 1, RETURN-VALUE, " " ).
  END.
  IF NOT VALID-HANDLE( rec-src ) THEN RETURN.
  RUN send-records IN rec-src( external-table, OUTPUT ext-rowid ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-apply-entry V-table-Win 
PROCEDURE local-apply-entry :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  RETURN.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'apply-entry':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF mode = "View" THEN RETURN.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available V-table-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose: Override the standard behaviour because it is undesirable.
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF Note.Details:MODIFIED THEN
    RUN assign-note( IF AVAILABLE Note THEN Note.NoteCode ELSE ? ).

  Note.Details:SCREEN-VALUE = "".
  Note.Details:MODIFIED = No.

  /* Get the handle of the record source */
  IF NOT VALID-HANDLE( rec-src ) THEN DO:
    DEF VAR c-rec-src AS CHAR   NO-UNDO.
    RUN get-link-handle IN adm-broker-hdl ( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT c-rec-src ).
    rec-src = WIDGET-HANDLE( c-rec-src ).
  END.

  IF VALID-HANDLE( rec-src ) THEN DO:
    RUN get-external-id.

    DEF VAR key-value AS CHAR   NO-UNDO.

    RUN send-key IN rec-src ( key-name, OUTPUT key-value ).
    FIND FIRST Note WHERE Note.NoteCode = INT( key-value ) NO-LOCK NO-ERROR.
    IF AVAILABLE Note THEN DISPLAY Note.Details WITH FRAME {&FRAME-NAME}.
  END.
  RUN dispatch( 'enable-fields':U ).
END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF Note.Details:MODIFIED THEN DO:
    RUN assign-note( IF AVAILABLE Note THEN Note.NoteCode ELSE ? ).
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* There are no foreign keys supplied by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Note"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-size V-table-Win 
PROCEDURE set-size :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:    

  DEF INPUT PARAMETER height-chars AS DEC NO-UNDO.
  DEF INPUT PARAMETER width-chars  AS DEC NO-UNDO.

  DEF VAR frm AS HANDLE NO-UNDO.
  frm = FRAME {&FRAME-NAME}:HANDLE.

  frm:HIDDEN = Yes.
  frm:SCROLLABLE = Yes.

  IF width-chars = ?  THEN width-chars  = Note.Details:WIDTH-C.
  IF height-chars = ? THEN height-chars = Note.Details:HEIGHT-C.

   ASSIGN
    frm:WIDTH-C  = width-chars
    frm:HEIGHT-C = height-chars
    Note.Details:WIDTH-C  = width-chars
    Note.Details:HEIGHT-C = height-chars.

  frm:SCROLLABLE = Yes.
  frm:SCROLLABLE = No.
  frm:HIDDEN = No.

END.

  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-font V-table-Win 
PROCEDURE use-font :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER font-num AS CHAR NO-UNDO.

  FRAME {&FRAME-NAME}:FONT = INTEGER( font-num ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name V-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  Key-Name = new-key-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.

  IF mode = "View" THEN
    RUN dispatch( 'disable-fields' ).
  ELSE
    RUN dispatch( 'enable-fields' ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-ToolTip V-table-Win 
PROCEDURE use-ToolTip :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-tip AS CHAR NO-UNDO.

  Note.Details:TOOLTIP IN FRAME {&FRAME-NAME} = new-tip.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION make-note V-table-Win 
FUNCTION make-note RETURNS INTEGER
  ( INPUT note-text AS CHAR, INPUT note-key AS CHAR, INPUT note-file AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR note-code AS INT NO-UNDO.

DEF BUFFER LastNote FOR Note.
  FIND LAST LastNote NO-LOCK NO-ERROR.
  note-code = (IF AVAILABLE(LastNote) THEN LastNote.NoteCode ELSE 0) + 1.
  CURRENT-VALUE(NoteCode) = note-code NO-ERROR.

  CREATE Note.
  IF Note.NoteCode = ? OR Note.NoteCode = 0 THEN Note.NoteCode = note-code.
  Note.Detail       = note-text.
  Note.NoteTableKey = note-key.
  Note.NoteTable    = note-file.

  RETURN Note.NoteCode.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


