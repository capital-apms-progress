&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR mode AS CHAR NO-UNDO.
DEF BUFFER FaxDetail FOR PhoneDetail.
DEF BUFFER MobDetail FOR PhoneDetail.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Guarantor
&Scoped-define FIRST-EXTERNAL-TABLE Guarantor


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Guarantor.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Guarantor.Limit 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Limit ~{&FP2}Limit ~{&FP3}
&Scoped-define ENABLED-TABLES Guarantor
&Scoped-define FIRST-ENABLED-TABLE Guarantor
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 cmb_Type loc_FirstName ~
loc_LastName loc_Company edt_address loc_City loc_state loc_Country loc_Zip ~
loc_Mobile loc_Expires 
&Scoped-Define DISPLAYED-FIELDS Guarantor.Limit 
&Scoped-Define DISPLAYED-OBJECTS cmb_Type loc_FirstName loc_LastName ~
loc_Company edt_address loc_City loc_state loc_Country loc_Zip loc_Phone ~
loc_Fax loc_Mobile loc_Expires 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_Type AS CHARACTER FORMAT "X(20)" 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "B - Bank Guarantee","S - Security Deposit","I - Individual","C - Company" 
     SIZE 25.14 BY 1.

DEFINE VARIABLE edt_address AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 33.14 BY 4.2 NO-UNDO.

DEFINE VARIABLE loc_City AS CHARACTER FORMAT "X(30)" 
     LABEL "City" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1
     FONT 10.

DEFINE VARIABLE loc_Company AS CHARACTER FORMAT "X(256)":U 
     LABEL "Company" 
     VIEW-AS FILL-IN 
     SIZE 63.43 BY 1 NO-UNDO.

DEFINE VARIABLE loc_Country AS CHARACTER FORMAT "X(30)" 
     LABEL "Country" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1
     FONT 10.

DEFINE VARIABLE loc_Expires AS DATE FORMAT "99/99/9999":U 
     LABEL "Expires" 
     VIEW-AS FILL-IN 
     SIZE 10.29 BY 1 NO-UNDO.

DEFINE VARIABLE loc_Fax AS CHARACTER FORMAT "X(256)":U 
     LABEL "Fax" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE loc_FirstName AS CHARACTER FORMAT "X(256)":U 
     LABEL "First Name" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE loc_LastName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Last Name" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE loc_Mobile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Mobile" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE loc_Phone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Ph" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE loc_state AS CHARACTER FORMAT "X(15)" 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1
     FONT 10.

DEFINE VARIABLE loc_Zip AS CHARACTER FORMAT "X(20)" 
     LABEL "Zip" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1
     FONT 10.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 77.72 BY 13.5.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 75.43 BY 11.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Guarantor.Limit AT ROW 1.4 COL 11 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 14.43 BY .9
     cmb_Type AT ROW 1.4 COL 34.43
     loc_FirstName AT ROW 4 COL 11 COLON-ALIGNED
     loc_LastName AT ROW 4 COL 49.29 COLON-ALIGNED
     loc_Company AT ROW 5.4 COL 11 COLON-ALIGNED
     edt_address AT ROW 6.8 COL 13 NO-LABEL
     loc_City AT ROW 6.8 COL 54.43 COLON-ALIGNED
     loc_state AT ROW 7.8 COL 54.43 COLON-ALIGNED
     loc_Country AT ROW 8.8 COL 54.43 COLON-ALIGNED
     loc_Zip AT ROW 9.8 COL 54.43 COLON-ALIGNED
     loc_Phone AT ROW 11.5 COL 11 COLON-ALIGNED
     loc_Fax AT ROW 11.5 COL 54.43 COLON-ALIGNED
     loc_Mobile AT ROW 12.5 COL 11 COLON-ALIGNED
     loc_Expires AT ROW 12.6 COL 54.43 COLON-ALIGNED
     RECT-1 AT ROW 1 COL 1
     "Contact Details" VIEW-AS TEXT
          SIZE 15.43 BY 1 AT ROW 2.6 COL 3.86
          FONT 14
     RECT-2 AT ROW 3 COL 2.14
     "Address:" VIEW-AS TEXT
          SIZE 6.29 BY 1 AT ROW 6.4 COL 5.57
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 9.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.Guarantor
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.5
         WIDTH              = 79.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cmb_Type IN FRAME F-Main
   ALIGN-L                                                              */
ASSIGN 
       edt_address:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR FILL-IN loc_Fax IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN loc_Phone IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Guarantor"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Guarantor"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-guarantor. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-guarantor.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN notify( 'open-query,record-source':U ).
  RUN dispatch( 'update-record':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-guarantor V-table-Win 
PROCEDURE delete-guarantor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Guarantor EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Guarantor THEN DELETE Guarantor.
  
  FIND CURRENT Person EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Person THEN DELETE Person.
  
  FIND CURRENT PostalDetail EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE PostalDetail THEN DELETE PostalDetail.
  
  FIND CURRENT PhoneDetail EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE PhoneDetail THEN DELETE PhoneDetail.

  FIND CURRENT FaxDetail EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE FaxDetail THEN DELETE FaxDetail.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-contact-details V-table-Win 
PROCEDURE get-contact-details :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Guarantor THEN RETURN.
  
  FIND FIRST Person WHERE Person.PersonCode = Guarantor.PersonCode NO-ERROR.
  
  IF NOT AVAILABLE Person THEN
  DO:
    CREATE Person.
    CREATE Contact.
    ASSIGN
      Contact.PersonCode   = Person.PersonCode
      Contact.ContactType  = 'GTOR'
      Guarantor.PersonCode = Person.PersonCode.
  END.

  FIND FIRST PostalDetail WHERE PostalDetail.PersonCode = Person.PersonCode
    AND PostalDetail.PostalType = 'MAIN' NO-ERROR.
  
  IF NOT AVAILABLE PostalDetail THEN
  DO:
    CREATE PostalDetail.
    ASSIGN 
      PostalDetail.PersonCode = Person.PersonCode
      PostalDetail.PostalType = 'MAIN'.
  END.

  FIND FIRST PhoneDetail WHERE PhoneDetail.PersonCode = Guarantor.PersonCode
    AND PhoneDetail.PhoneType = "BUS" NO-ERROR.

  IF NOT AVAILABLE PhoneDetail THEN
  DO:
    CREATE PhoneDetail.
    ASSIGN
      PhoneDetail.PhoneType = 'BUS'
      PhoneDetail.PersonCode = Person.PersonCode.
  END.

  FIND FIRST FaxDetail WHERE FaxDetail.PersonCode = Guarantor.PersonCode
    AND FaxDetail.PhoneType = "FAX" NO-ERROR.

  IF NOT AVAILABLE FaxDetail THEN
  DO:
    CREATE FaxDetail.
    ASSIGN
      FaxDetail.PhoneType = 'FAX'
      FaxDetail.PersonCode = Person.PersonCode.
  END.

  FIND FIRST MobDetail WHERE MobDetail.PersonCode = Guarantor.PersonCode
    AND MobDetail.PhoneType = "Mob" NO-ERROR.

  IF NOT AVAILABLE MobDetail THEN
  DO:
    CREATE MobDetail.
    ASSIGN
      MobDetail.PhoneType = 'MOB'
      MobDetail.PersonCode = Person.PersonCode.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  ASSIGN FRAME {&FRAME-NAME}
    cmb_Type
    edt_Address
    loc_City loc_State loc_Country loc_Zip
    loc_Phone loc_Fax loc_Mobile
    loc_FirstName loc_LastName loc_Company
    loc_Expires .
    
  ASSIGN
    Guarantor.Type = SUBSTR( cmb_Type, 1, 1 )
    PostalDetail.Address = edt_Address 
    PostalDetail.City    = loc_City 
    PostalDetail.State   = loc_State
    PostalDetail.Country = loc_Country 
    PostalDetail.Zip     = loc_Zip
    PhoneDetail.Number   = loc_Phone
    FaxDetail.Number     = loc_Fax
    MobDetail.Number     = loc_Mobile
    Person.FirstName     = loc_FirstName
    Person.LastName      = loc_LastName
    Person.Company       = loc_Company
    Guarantor.AnyData    = "Expiry," + STRING(loc_Expires, "99/99/9999").
.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Guarantor) THEN RETURN.

  IF mode = "Maintain" THEN RUN get-contact-details.
  
  cmb_Type =
    IF Guarantor.Type = "I" THEN "I - Individual"       ELSE
    IF Guarantor.Type = "B" THEN "B - Bank Guarantee"   ELSE
    IF Guarantor.Type = "S" THEN "S - Security Deposit" ELSE
    IF Guarantor.Type = "C" THEN "C - Company"          ELSE "".

  ASSIGN edt_Address = PostalDetail.Address.

  IF NUM-ENTRIES( Guarantor.AnyData ) > 1 THEN
    loc_Expires = DATE( ENTRY( 2, Guarantor.AnyData ) ).

  DISPLAY
    cmb_Type
    edt_Address
    loc_Expires
    PostalDetail.City    @ loc_City
    PostalDetail.State   @ loc_State
    PostalDetail.Country @ loc_Country
    PostalDetail.Zip     @ loc_Zip
    PhoneDetail.Number   @ loc_Phone
    FaxDetail.Number     @ loc_Fax
    MobDetail.Number     @ loc_Mobile
    Person.FirstName     @ loc_FirstName
    Person.LastName      @ loc_LastName
    Person.Company       @ loc_Company
   WITH FRAME {&FRAME-NAME}.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  ENABLE
    edt_Address
    loc_City loc_State loc_Country loc_Zip
    loc_Phone loc_Fax loc_Mobile
    loc_FirstName loc_LastName loc_Company
  WITH FRAME {&FRAME-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN RUN dispatch( 'add-record':U ).
  ELSE RUN dispatch( 'enable-fields' ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR c-recsrc   AS CHAR NO-UNDO.
  DEF VAR rowid-list AS CHAR NO-UNDO.
 
  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT c-recsrc ).
  RUN send-records IN WIDGET-HANDLE( c-recsrc ) ( "TenancyLease", OUTPUT rowid-list ).
  FIND TenancyLease WHERE ROWID( TenancyLease ) = TO-ROWID( rowid-list ) NO-LOCK.

  CREATE Guarantor.
  RUN get-contact-details.
  ASSIGN
    Guarantor.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
    Guarantor.Type = "I".

  CURRENT-WINDOW:TITLE = "Adding a Guarantor to Lease " +
    STRING( TenancyLease.TenancyLeaseCode ).
  
  RUN dispatch( 'display-fields' ).
  RUN dispatch( 'enable-fields' ).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       

------------------------------------------------------------------------------*/

  IF mode = "Add" THEN have-records = Yes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Guarantor"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:  Called when the mode attribute is set.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.

  mode = new-mode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-guarantor V-table-Win 
PROCEDURE verify-guarantor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR g-type AS CHAR NO-UNDO.
  g-type = SUBSTR( INPUT cmb_Type, 1, 1 ).

    
  IF g-type = "I" AND
    ( INPUT loc_FirstName = "" OR
      INPUT loc_LastName  = "" ) THEN
  DO:
    MESSAGE "Individuals must have a first and a last name."
      VIEW-AS ALERT-BOX ERROR.
    IF INPUT loc_FirstName = "" THEN APPLY 'ENTRY':U TO loc_FirstName. 
    IF INPUT loc_LastName  = "" THEN APPLY 'ENTRY':U TO loc_LastName. 
    RETURN "FAIL".
  END.
  ELSE IF g-type = "C" AND INPUT loc_Company = "" THEN
  DO:
    MESSAGE "Companies must have a company name."
      VIEW-AS ALERT-BOX ERROR.
    APPLY 'ENTRY':U TO loc_Company. 
    RETURN "FAIL".
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


