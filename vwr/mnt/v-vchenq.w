&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description: 
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR loc_Account LIKE BankAccount.BankAccountCode NO-UNDO.
DEF VAR loc_Lease   LIKE TenancyLease.TenancyLeaseCode NO-UNDO.

&SCOPED-DEFINE REPORT-ID "Voucher Enquiry"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP Creditor
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP, Creditor.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char3 RP.Char2 RP.Int1 RP.Int2 RP.Char1 ~
RP.Date1 RP.Date2 RP.Log1 RP.Log8 RP.Log2 RP.Log3 RP.Log4 RP.Log5 RP.Date3 ~
RP.Int3 RP.Log7 RP.Log9 RP.Log6 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS btn_OK RECT-1 RECT-2 RECT-3 
&Scoped-Define DISPLAYED-FIELDS RP.Char3 RP.Char2 RP.Int1 RP.Int2 RP.Char1 ~
RP.Date1 RP.Date2 RP.Log1 RP.Log8 RP.Log2 RP.Log3 RP.Log4 RP.Log5 RP.Date3 ~
RP.Int3 RP.Log7 RP.Log9 RP.Log6 
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP


/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_OK DEFAULT 
     LABEL "&OK" 
     SIZE 10.29 BY 1
     FONT 9.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 44.57 BY 23.75.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 43.43 BY 2.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 43.43 BY 2.1.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char3 AT ROW 1.2 COL 2.72 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Creditor Code", "C":U,
"Creditor Name", "Name":U,
"Approver Order", "A":U,
"By Company", "E":U,
"Exportable Voucher Details", "ExportDetails":U
          SIZE 24.29 BY 5
          FONT 10
     RP.Char2 AT ROW 7.1 COL 2.72 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Include Vouchers for all Creditors", "All":U,
"Include Vouchers within Creditor number range", "Range":U
          SIZE 34.29 BY 1.6
          FONT 10
     RP.Int1 AT ROW 8.7 COL 15 COLON-ALIGNED
          LABEL "From" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.72 BY 1
          FONT 11
     RP.Int2 AT ROW 8.7 COL 31 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.14 BY 1
          FONT 11
     RP.Char1 AT ROW 10.3 COL 2.72 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Include vouchers with any date", "All":U,
"Include vouchers within a date range", "Range":U
          SIZE 28.57 BY 1.6
          FONT 10
     RP.Date1 AT ROW 11.9 COL 15 COLON-ALIGNED
          LABEL "From"
          VIEW-AS FILL-IN 
          SIZE 11.29 BY 1
          FONT 11
     RP.Date2 AT ROW 11.9 COL 31 COLON-ALIGNED
          LABEL "To"
          VIEW-AS FILL-IN 
          SIZE 11.29 BY 1
          FONT 11
     RP.Log1 AT ROW 13.35 COL 17.86
          LABEL "Unapproved"
          VIEW-AS TOGGLE-BOX
          SIZE 12 BY .8
          FONT 10
     RP.Log8 AT ROW 14.15 COL 17.86
          LABEL "Queried"
          VIEW-AS TOGGLE-BOX
          SIZE 12 BY .8
          FONT 10
     RP.Log2 AT ROW 14.95 COL 17.86
          LABEL "Approved"
          VIEW-AS TOGGLE-BOX
          SIZE 12 BY .8
          FONT 10
     RP.Log3 AT ROW 15.75 COL 17.86
          LABEL "Paid"
          VIEW-AS TOGGLE-BOX
          SIZE 12 BY .8
          FONT 10
     RP.Log4 AT ROW 16.55 COL 17.86
          LABEL "Held"
          VIEW-AS TOGGLE-BOX
          SIZE 12 BY .8
          FONT 10
     RP.Log5 AT ROW 17.35 COL 17.86
          LABEL "Cancelled"
          VIEW-AS TOGGLE-BOX
          SIZE 12 BY .8
          FONT 10
     RP.Date3 AT ROW 18.5 COL 22.72 COLON-ALIGNED
          LABEL "Include vouchers entered before"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     RP.Int3 AT ROW 19.7 COL 22.72 COLON-ALIGNED
          LABEL "Include vouchers due within"
          VIEW-AS FILL-IN 
          SIZE 4.57 BY 1.05
     RP.Log7 AT ROW 21.05 COL 4.72
          LABEL "Show detail of allocations"
          VIEW-AS TOGGLE-BOX
          SIZE 30.14 BY .8
     RP.Log9 AT ROW 21.95 COL 4.72
          LABEL "Show 6 months creditor activity"
          VIEW-AS TOGGLE-BOX
          SIZE 27.29 BY .85 TOOLTIP "Do you want to see the last six months activity for the creditor?"
     RP.Log6 AT ROW 23.25 COL 4.72
          LABEL "Print Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 12.29 BY .8
          FONT 10
     btn_OK AT ROW 23.25 COL 34.14
     RECT-1 AT ROW 1 COL 1
     RECT-2 AT ROW 11 COL 1.57
     RECT-3 AT ROW 7.8 COL 1.57
     "Include Statuses of:" VIEW-AS TEXT
          SIZE 13.72 BY .6 AT ROW 13.35 COL 2.14
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_OK.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME F-Main
     "days" VIEW-AS TEXT
          SIZE 8 BY .65 AT ROW 19.9 COL 29.86
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP,ttpl.Creditor
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 27.85
         WIDTH              = 67.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR RADIO-SET RP.Char3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Date1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RP.Date2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RP.Date3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log6 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log7 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log8 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log9 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_OK V-table-Win
ON CHOOSE OF btn_OK IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN sensitise-dates.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON VALUE-CHANGED OF RP.Char2 IN FRAME F-Main /* Char2 */
DO:
  RUN sensitise-creditors.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log1 V-table-Win
ON VALUE-CHANGED OF RP.Log1 IN FRAME F-Main /* Unapproved */
DO:
  RUN sensitise-include.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Approved */
DO:
  RUN sensitise-include.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log3 V-table-Win
ON VALUE-CHANGED OF RP.Log3 IN FRAME F-Main /* Paid */
DO:
  RUN sensitise-include.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Held */
DO:
  RUN sensitise-include.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log5 V-table-Win
ON VALUE-CHANGED OF RP.Log5 IN FRAME F-Main /* Cancelled */
DO:
  RUN sensitise-include.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log8
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log8 V-table-Win
ON VALUE-CHANGED OF RP.Log8 IN FRAME F-Main /* Queried */
DO:
  RUN sensitise-include.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win 
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override (thoroughly!) the Progress adm-row-available
------------------------------------------------------------------------------*/

 /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Creditor"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Creditor"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN sensitise-dates.
  RUN sensitise-creditors.
  RUN sensitise-include.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RP.Date3:SCREEN-VALUE = STRING( TODAY + 1, "99/99/9999" ).
  RUN dispatch( 'enable-fields':U ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE post-row-available V-table-Win 
PROCEDURE post-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE Creditor AND AVAILABLE RP THEN DO:
  
    FIND CURRENT RP EXCLUSIVE-LOCK NO-ERROR.
    
    ASSIGN RP.Char2 = "Range"
           RP.Int1 = Creditor.CreditorCode
           RP.Int2 = Creditor.CreditorCode .

    RUN dispatch( 'display-fields':U ).
    RUN dispatch( 'enable-fields':U ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize V-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR user-name AS CHAR NO-UNDO.
  DEF VAR c-parent  AS CHAR NO-UNDO.
  DEF VAR parent    AS HANDLE NO-UNDO.
  
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  
  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.
    
  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Char1 = "All"
      RP.Date1 = TODAY - 360
      RP.Date2 = TODAY + 1000
      RP.Char2 = "Range"
      RP.Log1 = Yes
      RP.Log2 = No
      RP.Log3 = No
      RP.Log4 = No
      RP.Log5 = No
      RP.Log6 = Yes
      RP.Char3 = "C".
  END.

  RUN dispatch( 'display-fields':U ).
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     Actually run the report through RB engine.
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  RUN dispatch( 'update-record':U ).
  
  DEF VAR date-from     AS DATE NO-UNDO.
  DEF VAR date-to       AS DATE NO-UNDO.
  DEF VAR creditor-from LIKE Creditor.CreditorCode NO-UNDO.  
  DEF VAR creditor-to   LIKE Creditor.CreditorCode NO-UNDO.  
  DEF VAR status-list   AS CHAR NO-UNDO.

  IF RP.Char1 = "All" THEN ASSIGN date-from = DATE(1,1,1)   date-to = DATE(12,31,9999).
                      ELSE ASSIGN date-from = RP.Date1      date-to = RP.Date2.
                      
  IF RP.Char2 = "All" THEN ASSIGN creditor-from = 0         creditor-to = 99999.
                      ELSE ASSIGN creditor-from = RP.Int1   creditor-to = RP.Int2.
  
  status-list = status-list + IF RP.Log1 THEN "U" ELSE "".
  status-list = status-list + IF RP.Log2 THEN "A" ELSE "".
  status-list = status-list + IF RP.Log3 THEN "P" ELSE "".
  status-list = status-list + IF RP.Log4 THEN "H" ELSE "".
  status-list = status-list + IF RP.Log5 THEN "C" ELSE "".
  status-list = status-list + IF RP.Log8 THEN "Q" ELSE "".

  report-options = "Sort," + RP.Char3
                 + "~nCreditorRange," + STRING(creditor-from) + "," + STRING(creditor-to)
                 + "~nDateRange," + STRING( date-from, "99/99/9999") + "," + STRING(date-to, "99/99/9999")
                 + "~nStatuses," + status-list
                 + "~nEnteredBefore," + STRING( RP.Date3, "99/99/9999")
                 + (IF RP.Int3:SENSITIVE THEN "~nDueBefore," + STRING( TODAY + RP.Int3, "99/99/9999") ELSE "")
                 + (IF RP.Log6 THEN "~nPreview" ELSE "")
                 + (IF RP.Log9 THEN "~nCreditorActivity" ELSE "")
                 + (IF RP.Log7 THEN "~nDetail" ELSE "").

  RUN notify( 'set-busy, container-source':U ).
  RUN process/report/vchenqry.p ( report-options ).
  RUN notify( 'set-idle, container-source':U ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win 
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Creditor"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-creditors V-table-Win 
PROCEDURE sensitise-creditors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RP.Int1:SENSITIVE = INPUT FRAME {&FRAME-NAME} RP.Char2 = "Range".
  RP.Int2:SENSITIVE = INPUT FRAME {&FRAME-NAME} RP.Char2 = "Range".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-dates V-table-Win 
PROCEDURE sensitise-dates :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RP.Date1:SENSITIVE = INPUT FRAME {&FRAME-NAME} RP.Char1 = "Range".
  RP.Date2:SENSITIVE = INPUT FRAME {&FRAME-NAME} RP.Char1 = "Range".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-include V-table-Win 
PROCEDURE sensitise-include :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  RP.Int3:SENSITIVE = INPUT RP.Log2 AND
          NOT( INPUT RP.Log1 OR INPUT RP.Log3 OR INPUT RP.Log4 OR INPUT RP.Log5 ) .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:

    IF NOT ( INPUT RP.Log1 OR INPUT RP.Log2 OR INPUT RP.Log3 OR
             INPUT RP.Log4 OR INPUT RP.Log5 ) THEN
    DO:
      MESSAGE "You must select at least one status to include"
          VIEW-AS ALERT-BOX ERROR TITLE "No statuses selected".
      RETURN "FAIL".
    END.

    IF INPUT RP.Log3 AND INPUT RP.Char2 = "All" THEN DO:
      MESSAGE "You should not ask for vouchers with Paid" SKIP 
              "status for all creditors!"
              VIEW-AS ALERT-BOX ERROR TITLE "Toooooo many vouchers!".
      RETURN "FAIL".
    END.

  END.
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

