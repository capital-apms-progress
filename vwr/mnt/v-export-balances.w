&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

DEF VAR mth-desc AS CHAR NO-UNDO.

DEFINE TEMP-TABLE MthFigs NO-UNDO
    FIELD MonthCode AS INT
    FIELD StartDate AS CHAR   FORMAT 'X(13)'  COLUMN-LABEL '    From'
    FIELD EndDate AS CHAR     FORMAT 'X(13)'  COLUMN-LABEL '      To'
    FIELD FigsFrom AS CHAR    FORMAT '   X' COLUMN-LABEL 'Type'
    FIELD Description AS CHAR FORMAT 'X(32)'  COLUMN-LABEL 'Description'
INDEX IX-A IS UNIQUE PRIMARY MonthCode ASCENDING.

DEF VAR set-list AS CHAR NO-UNDO INITIAL "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Int2 RP.Int3 RP.Char2 RP.Dec1 ~
RP.Dec2 RP.Log1 RP.Log2 RP.Char5 RP.Log3 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Char1 ~{&FP2}Char1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}~
 ~{&FP1}Int3 ~{&FP2}Int3 ~{&FP3}~
 ~{&FP1}Dec1 ~{&FP2}Dec1 ~{&FP3}~
 ~{&FP1}Dec2 ~{&FP2}Dec2 ~{&FP3}~
 ~{&FP1}Char5 ~{&FP2}Char5 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-2 cmb_AccountGroups cmb_To btn_Browse ~
btn_OK 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Int2 RP.Int3 RP.Char2 RP.Dec1 ~
RP.Dec2 RP.Log1 RP.Log2 RP.Char5 RP.Log3 
&Scoped-Define DISPLAYED-OBJECTS fil_Entity-1 fil_Entity-2 ~
cmb_AccountGroups fil_account1 fil_account2 cmb_To 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Browse 
     LABEL "&Browse" 
     SIZE 8.57 BY 1.

DEFINE BUTTON btn_OK DEFAULT 
     LABEL "&OK" 
     SIZE 10.86 BY 1.2
     FONT 9.

DEFINE VARIABLE cmb_To AS DATE FORMAT "99/99/9999":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 18
     LIST-ITEMS "31/12/97" 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE fil_account1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 50.72 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_account2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 50.72 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Entity-1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 52.57 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Entity-2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 52.57 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 70.86 BY 17.2.

DEFINE VARIABLE cmb_AccountGroups AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
     SIZE 62.29 BY 7 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.4 COL 7 COLON-ALIGNED
          LABEL "Entity" FORMAT "X"
          VIEW-AS FILL-IN 
          SIZE 2.86 BY 1
     RP.Int2 AT ROW 1.4 COL 9.86 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_Entity-1 AT ROW 1.4 COL 16.72 COLON-ALIGNED NO-LABEL
     RP.Int3 AT ROW 2.55 COL 9.86 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_Entity-2 AT ROW 2.55 COL 16.72 COLON-ALIGNED NO-LABEL
     RP.Char2 AT ROW 4.2 COL 1.57 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "Account Range", "Range":U,
"Account Groups", "Groups":U
          SIZE 36.57 BY 1
     cmb_AccountGroups AT ROW 5.2 COL 9 NO-LABEL
     RP.Dec1 AT ROW 6 COL 7 COLON-ALIGNED HELP
          ""
          LABEL "From" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 8 BY 1
     fil_account1 AT ROW 6 COL 18.57 COLON-ALIGNED NO-LABEL
     RP.Dec2 AT ROW 7 COL 7 COLON-ALIGNED HELP
          ""
          LABEL "to" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 8 BY 1
     fil_account2 AT ROW 7 COL 18.57 COLON-ALIGNED NO-LABEL
     RP.Log1 AT ROW 12.4 COL 9 HELP
          ""
          LABEL "Detailed"
          VIEW-AS TOGGLE-BOX
          SIZE 9.14 BY 1
     RP.Log2 AT ROW 12.4 COL 28.43 HELP
          ""
          LABEL "Merge sub-accounts"
          VIEW-AS TOGGLE-BOX
          SIZE 17.14 BY 1
     cmb_To AT ROW 13.6 COL 7 COLON-ALIGNED
     RP.Char5 AT ROW 15 COL 7 COLON-ALIGNED
          LABEL "File" FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 53.72 BY 1
     btn_Browse AT ROW 15 COL 62.72
     RP.Log3 AT ROW 16.2 COL 9 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Entity Rows - Account Columns", TRUE,
"Account Rows - Entity Columns", FALSE
          SIZE 26.29 BY 1.6
     btn_OK AT ROW 16.8 COL 60.43
     RECT-2 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 20.15
         WIDTH              = 83.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR RADIO-SET RP.Char2 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Char5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Dec1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Dec2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_account1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_account2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Entity-1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Entity-2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR RADIO-SET RP.Log3 IN FRAME F-Main
   EXP-HELP                                                             */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Browse V-table-Win
ON CHOOSE OF btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_OK V-table-Win
ON CHOOSE OF btn_OK IN FRAME F-Main /* OK */
DO:
  DISABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON LEAVE OF RP.Char1 IN FRAME F-Main /* Entity */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  DO WITH FRAME {&FRAME-NAME}:
    SELF:SCREEN-VALUE = CAPS(SELF:SCREEN-VALUE).
    RUN get-entity-name( SELF:SCREEN-VALUE, INT(RP.Int2:SCREEN-VALUE), OUTPUT fil_Entity-1 ).
    RUN get-entity-name( SELF:SCREEN-VALUE, INT(RP.Int2:SCREEN-VALUE), OUTPUT fil_Entity-2 ).
    DISPLAY fil_Entity-1 fil_Entity-2 .
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON VALUE-CHANGED OF RP.Char2 IN FRAME F-Main /* Char2 */
DO:
  RUN account-selection-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_AccountGroups
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AccountGroups V-table-Win
ON U1 OF cmb_AccountGroups IN FRAME F-Main
DO:
  {inc/selcmb/sel-acg1.i "RP" "Char6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AccountGroups V-table-Win
ON U2 OF cmb_AccountGroups IN FRAME F-Main
DO:
  {inc/selcmb/sel-acg2.i "RP" "Char6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_To
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_To V-table-Win
ON U1 OF cmb_To IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_To V-table-Win
ON U2 OF cmb_To IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Dec1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Dec1 V-table-Win
ON LEAVE OF RP.Dec1 IN FRAME F-Main /* From */
DO:
  {inc/selcde/cdcoa.i "fil_account1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Dec2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Dec2 V-table-Win
ON LEAVE OF RP.Dec2 IN FRAME F-Main /* to */
DO:
  {inc/selcde/cdcoa.i "fil_account2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_account1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U1 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa1.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U2 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa2.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U3 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa3.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_account2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U1 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa1.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U2 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa2.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U3 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa3.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* Int2 */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    RUN get-entity-name( RP.Char1:SCREEN-VALUE, INT(SELF:SCREEN-VALUE), OUTPUT fil_Entity-1 ).
    DISPLAY fil_Entity-1.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int3 V-table-Win
ON LEAVE OF RP.Int3 IN FRAME F-Main /* Int3 */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    RUN get-entity-name( RP.Char1:SCREEN-VALUE, INT(SELF:SCREEN-VALUE), OUTPUT fil_Entity-2 ).
    DISPLAY fil_Entity-2 .
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log1 V-table-Win
ON VALUE-CHANGED OF RP.Log1 IN FRAME F-Main /* Detailed */
DO:
  RUN detail-flag-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE account-selection-changed V-table-Win 
PROCEDURE account-selection-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Char2 = "Range" THEN DO:
    VIEW RP.Dec1 RP.Dec2 fil_account1 fil_account2.
    HIDE cmb_AccountGroups RP.Log1.
  END.
  ELSE DO:
    HIDE RP.Dec1 RP.Dec2 fil_account1 fil_account2.
    VIEW cmb_AccountGroups RP.Log1.
  END.
  RUN detail-flag-changed.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE detail-flag-changed V-table-Win 
PROCEDURE detail-flag-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Log1 AND INPUT RP.Char2 = "Groups" THEN DO:
    VIEW RP.Log2.
  END.
  ELSE DO:
    HIDE RP.Log2.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-entity-name V-table-Win 
PROCEDURE get-entity-name :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER et AS CHAR NO-UNDO.
DEF INPUT PARAMETER ec AS INT NO-UNDO.
DEF OUTPUT PARAMETER o-entityname AS CHAR NO-UNDO.

  CASE et :
    WHEN "L" THEN DO:
      FIND Company WHERE Company.CompanyCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Company) THEN o-EntityName = Company.LegalName.
                            ELSE o-EntityName = "* * * Company not on file * * *".
    END.
    WHEN "P" THEN DO:
      FIND Property WHERE Property.PropertyCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Property) THEN o-EntityName = Property.Name.
                             ELSE o-EntityName = "* * * Property not on file * * *".
    END.
    WHEN "J" THEN DO:
      FIND Project WHERE Project.ProjectCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Project) THEN o-EntityName = Project.Name.
                            ELSE o-EntityName = "* * * Project not on file * * *".
    END.
    WHEN "F" THEN DO:
      FIND FixedAsset WHERE FixedAsset.AssetCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(FixedAsset) THEN o-EntityName = FixedAsset.Description.
                            ELSE o-EntityName = "* * * Fixed Asset not on file * * *".
    END.
    /* T and C should be fairly unlikely but we'll do them anyway */
    WHEN "C" THEN DO:
      FIND Creditor WHERE Creditor.CreditorCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Creditor) THEN o-EntityName = Creditor.Name.
                             ELSE o-EntityName = "* * * Creditor not on file * * *".
    END.
    WHEN "T" THEN DO:
      FIND Tenant WHERE Tenant.TenantCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Tenant) THEN o-EntityName = Tenant.Name.
                           ELSE o-EntityName = "* * * Tenant not on file * * *".
    END.
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  RUN get-entity-name( INPUT RP.Char1, INPUT RP.Int2, OUTPUT fil_Entity-1 ).
  RUN get-entity-name( INPUT RP.Char1, INPUT RP.Int3, OUTPUT fil_Entity-2 ).
  DISPLAY UNLESS-HIDDEN fil_Entity-1 fil_Entity-2.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN account-selection-changed.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.  {inc/username.i "user-name"}
&SCOP REPORT-ID "Account Balance Export"

  FIND RP WHERE RP.ReportID = {&REPORT-ID} AND RP.UserName = user-name EXCLUSIVE-LOCK NO-ERROR .
  IF NOT AVAILABLE(RP) THEN DO:
    FIND LAST Month WHERE Month.StartDate <= TODAY NO-LOCK NO-ERROR.
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
    .
  END.


  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'disable-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  RUN dispatch( 'update-record':U ).

  report-options = "EntityType," + RP.Char1
                 + "~nEntityRange," + STRING(RP.Int2) + "," + STRING( RP.Int3 )
                 + (IF RP.Char2 = "Range" THEN
                      "~nAccountRange," + STRING(RP.Dec1) + "," + STRING(RP.Dec2)
                    ELSE
                      "~nAccountGroups," + RP.Char6
                         + (IF RP.Log1 THEN "~nDetailed" ELSE "")
                         + (IF RP.Log1 AND RP.Log2 THEN "~nMergeAccounts" ELSE "")
                    )
                 + "~nToMonth," + STRING(RP.Int6)
                 + "~nColumns," + (IF RP.Log3 THEN "Accounts" ELSE "Entities")
                 + "~nFileName," + RP.Char5 .

  RUN notify( 'set-busy,container-source':U ).
  RUN process/export/export-balances.p( report-options ).
  RUN notify( 'set-idle,container-source':U ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char5 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated values" "*.CSV"
        SAVE-AS ASK-OVERWRITE CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV"
        INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME UPDATE select-ok.

  IF select-ok THEN DO:
    IF SUBSTRING( file-name, R-INDEX( file-name, ".") ) <> ".CSV" THEN DO:
      IF (LENGTH( file-name ) - R-INDEX( file-name, "." )) < 4 THEN
        file-name = SUBSTRING( file-name, 1, R-INDEX( file-name, ".")) + "CSV" .
    END.
    RP.Char5:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


