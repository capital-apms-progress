&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tpivoice.i}

DEF VAR invoice-list AS CHAR NO-UNDO.
DEF VAR debug-mode AS LOGI NO-UNDO      INITIAL Yes.

DEF VAR invoice-total  AS DEC NO-UNDO     INITIAL 0.
DEF VAR invoice-tax    AS DEC NO-UNDO     INITIAL 0.
DEF VAR invoice-amount AS DEC NO-UNDO.
DEF VAR invoice-blurb  AS CHAR NO-UNDO    INITIAL "".

{inc/ofc-this.i}
{inc/ofc-set.i "Invoice-Terms" "default-terms" "WARNING"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Invoice
&Scoped-define FIRST-EXTERNAL-TABLE Invoice


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Invoice.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Invoice.InvoiceDate Invoice.EntityCode ~
Invoice.TaxAmount 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}InvoiceDate ~{&FP2}InvoiceDate ~{&FP3}~
 ~{&FP1}EntityCode ~{&FP2}EntityCode ~{&FP3}~
 ~{&FP1}TaxAmount ~{&FP2}TaxAmount ~{&FP3}
&Scoped-define ENABLED-TABLES Invoice
&Scoped-define FIRST-ENABLED-TABLE Invoice
&Scoped-Define ENABLED-OBJECTS RECT-13 rs_charge-type fil_From fil_To ~
fil_Blurb cmb_Terms btn_add btn_ok btn_cancel 
&Scoped-Define DISPLAYED-FIELDS Invoice.InvoiceNo Invoice.InvoiceDate ~
Invoice.EntityCode Invoice.TaxAmount 
&Scoped-Define DISPLAYED-OBJECTS fil_Tenant rs_charge-type fil_From fil_To ~
fil_Blurb cmb_Terms fil_total fil_amtdue 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD include-lease V-table-Win 
FUNCTION include-lease RETURNS LOGICAL
  ( INPUT lease-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 6.86 BY 1
     FONT 9.

DEFINE BUTTON btn_ok 
     LABEL "&OK" 
     SIZE 5.72 BY 1.05
     FONT 9.

DEFINE VARIABLE cmb_Terms AS CHARACTER FORMAT "X(30)":U 
     LABEL "Terms" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "","" 
     SIZE 36.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Blurb AS CHARACTER 
     VIEW-AS EDITOR NO-WORD-WRAP SCROLLBAR-HORIZONTAL SCROLLBAR-VERTICAL
     SIZE 83.43 BY 7.6
     FONT 8 NO-UNDO.

DEFINE VARIABLE fil_amtdue AS DECIMAL FORMAT "-Z,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Amt Due" 
     VIEW-AS FILL-IN 
     SIZE 14.57 BY 1
     FGCOLOR 12 FONT 11 NO-UNDO.

DEFINE VARIABLE fil_From AS DATE FORMAT "99/99/9999":U 
     LABEL "From" 
     VIEW-AS FILL-IN 
     SIZE 10.14 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_Tenant AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 66.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_To AS DATE FORMAT "99/99/9999":U 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 10.14 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_total AS DECIMAL FORMAT "-Z,ZZZ,ZZ9.99" INITIAL 0 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 14.57 BY 1
     FGCOLOR 12 FONT 11.

DEFINE VARIABLE rs_charge-type AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Current charges", "charges",
"Charges in arrears", "arrears"
     SIZE 33.72 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-13
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 84.57 BY 16.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Invoice.InvoiceNo AT ROW 1 COL 75 COLON-ALIGNED
          LABEL "No" FORMAT "ZZZZZ9"
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
          FONT 11
     Invoice.InvoiceDate AT ROW 2.4 COL 7.57 COLON-ALIGNED
          LABEL "Date"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     Invoice.EntityCode AT ROW 4 COL 7.57 COLON-ALIGNED
          LABEL "Tenant"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_Tenant AT ROW 4 COL 16.72 COLON-ALIGNED NO-LABEL
     rs_charge-type AT ROW 5.2 COL 9.57 NO-LABEL
     fil_From AT ROW 5.2 COL 54.43 COLON-ALIGNED
     fil_To AT ROW 5.2 COL 72.72 COLON-ALIGNED
     fil_Blurb AT ROW 6.4 COL 1.57 NO-LABEL
     cmb_Terms AT ROW 14.2 COL 7.57 COLON-ALIGNED
     fil_total AT ROW 14.2 COL 68.14 COLON-ALIGNED
     Invoice.TaxAmount AT ROW 15.2 COL 68.14 COLON-ALIGNED FORMAT "-Z,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14.57 BY 1
          FGCOLOR 12 FONT 11
     fil_amtdue AT ROW 16.2 COL 68.14 COLON-ALIGNED
     btn_add AT ROW 17.8 COL 1
     btn_ok AT ROW 17.8 COL 71.86
     btn_cancel AT ROW 17.8 COL 78.72
     RECT-13 AT ROW 1.4 COL 1
     "Rent Invoice Registration" VIEW-AS TEXT
          SIZE 30.29 BY 1 AT ROW 1 COL 1.57
          FONT 13
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Invoice
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.8
         WIDTH              = 91.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Invoice.EntityCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_amtdue IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       fil_Blurb:READ-ONLY IN FRAME F-Main        = TRUE.

/* SETTINGS FOR FILL-IN fil_Tenant IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_total IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Invoice.InvoiceDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Invoice.InvoiceNo IN FRAME F-Main
   NO-ENABLE EXP-LABEL EXP-FORMAT                                       */
/* SETTINGS FOR FILL-IN Invoice.TaxAmount IN FRAME F-Main
   EXP-FORMAT                                                           */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK KEEP-EMPTY"
     _TblOptList       = "FIRST OUTER"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/string.i}
{inc/rentchrg.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON RETURN OF FRAME F-Main
ANYWHERE DO:
  IF LOOKUP( FOCUS:TYPE, 'FILL-IN,EDITOR,COMBO-BOX' ) <> 0 OR
     FOCUS:TYPE = 'BUTTON':U AND FOCUS:DYNAMIC THEN
  DO:
    APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN dispatch( 'add-record':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN cancel-changes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON CHOOSE OF btn_ok IN FRAME F-Main /* OK */
DO:
  RUN confirm-changes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Terms
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Terms V-table-Win
ON U1 OF cmb_Terms IN FRAME F-Main /* Terms */
DO:
  {inc/selcmb/scivt1.i "Invoice" "TermsCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Terms V-table-Win
ON U2 OF cmb_Terms IN FRAME F-Main /* Terms */
DO:
  {inc/selcmb/scivt2.i "Invoice" "TermsCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Invoice.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Invoice.EntityCode V-table-Win
ON LEAVE OF Invoice.EntityCode IN FRAME F-Main /* Tenant */
DO:
  IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN RUN update-details.
  {inc/selcde/cdtnt.i "fil_Tenant"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_From
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_From V-table-Win
ON LEAVE OF fil_From IN FRAME F-Main /* From */
DO:
  IF INPUT fil_From <> fil_From THEN RUN update-details.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Tenant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U1 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt1.i "Invoice" "EntityCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U2 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt2.i "Invoice" "EntityCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U3 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt3.i "Invoice" "EntityCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_To
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_To V-table-Win
ON LEAVE OF fil_To IN FRAME F-Main /* To */
DO:
  IF INPUT fil_to <> fil_to THEN RUN update-details.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs_charge-type
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs_charge-type V-table-Win
ON VALUE-CHANGED OF rs_charge-type IN FRAME F-Main
DO:
  RUN update-details.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Invoice"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Invoice"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-invoice-line V-table-Win 
PROCEDURE assign-invoice-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR tenant-code AS INT NO-UNDO.
DEF VAR period-d1 AS DATE NO-UNDO.
DEF VAR period-dn AS DATE NO-UNDO.

DEF VAR last-type AS CHAR NO-UNDO INITIAL "not a possible type".
DEF VAR last-account AS DEC NO-UNDO INITIAL -999999.999 .
DEF VAR charge-from AS DATE NO-UNDO .

DO WITH FRAME {&FRAME-NAME}:

  IF NOT AVAILABLE Invoice THEN RETURN.
  RUN update-details.
  IF ERROR-STATUS:ERROR THEN RETURN ERROR.

  FIND Tenant WHERE Tenant.TenantCode = Invoice.EntityCode NO-LOCK.
  FOR EACH InvoiceLine OF Invoice:      DELETE InvoiceLine.     END.

ON ASSIGN OF InvoiceLine.YourShare OVERRIDE DO: END.
  FOR EACH ChargeDetail:
    IF last-type <> ChargeDetail.ChargeType OR last-account <> ChargeDetail.AccountCode THEN DO:
      CREATE  InvoiceLine.
      ASSIGN  InvoiceLine.InvoiceNo   = Invoice.InvoiceNo
              InvoiceLine.EntityType  = Tenant.EntityType
              InvoiceLine.EntityCode  = Tenant.EntityCode
              InvoiceLine.AccountCode = ChargeDetail.AccountCode
              InvoiceLine.Percent     = 100.00
              charge-from             = ChargeDetail.ChargedFrom
              last-type               = ChargeDetail.ChargeType
              last-account            = ChargeDetail.AccountCode .
    END.

    ASSIGN  InvoiceLine.AccountText = ChargeDetail.Description
                                    + " from " + STRING( charge-from, "99/99/9999") + " to " + STRING( ChargeDetail.ChargedUpTo, "99/99/9999")
            InvoiceLine.Amount      = InvoiceLine.Amount + ChargeDetail.ChargeAmount
            InvoiceLine.YourShare   = InvoiceLine.Amount.
  END.
ON ASSIGN OF InvoiceLine.YourShare REVERT.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).

  RUN delete-invoice.
  RUN print-invoices.
  IF NUM-ENTRIES( invoice-list ) <> 0 THEN
    RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN check-modified( "CLEAR":U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-invoice.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN notify( 'hide, CONTAINER-SOURCE':u ).
  RUN dispatch( 'update-record':U ).
  RUN print-invoices.
  RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-invoice V-table-Win 
PROCEDURE delete-invoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Invoice THEN RETURN.
  
  FIND Current Invoice EXCLUSIVE-LOCK.
  DELETE Invoice.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR paid-to-list AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  ASSIGN
    Invoice.InvoiceStatus = "U"
    Invoice.InvoiceType   = "RENT"
    Invoice.EntityType    = "T"
    Invoice.ToDetail =
      "Rent charges from " + STRING( INPUT fil_from, "99/99/9999" ) +
                            " to " + STRING( INPUT fil_to, "99/99/9999" )
    Invoice.Blurb = fil_blurb:SCREEN-VALUE
    Invoice.TaxAmount = invoice-tax
    Invoice.Total     = invoice-total.

  paid-to-list = "".
  FOR EACH ChargeDetail:
    paid-to-list = paid-to-list
                 + STRING( ChargeDetail.TenancyLeaseCode ) + "|"
                 + STRING( ChargeDetail.ChargeSeq) + "|"
                 + STRING( ChargeDetail.ChargeStart, "99/99/9999") + "|"
                 + STRING( ChargeDetail.ChargedUpTo, "99/99/9999")
                 + ",".
  END.
  Invoice.ToPay = RIGHT-TRIM( paid-to-list, ",").
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     View or hide tax fields depending on the office
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF AVAILABLE Invoice THEN
  DO:
    fil_total = Invoice.Total + Invoice.TaxAmount.
    DISPLAY fil_total WITH FRAME {&FRAME-NAME}.
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  Invoice.TaxAmount:HIDDEN    = AVAILABLE Office AND Office.GST = 0.
  Invoice.TaxAmount:SENSITIVE = No.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN dispatch( 'add-record':U ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-update-record V-table-Win 
PROCEDURE local-update-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'update-record':U ) .
  RUN assign-invoice-line.
  
  /* Code placed here will execute AFTER standard behavior.    */
  IF LOOKUP( STRING( Invoice.InvoiceNo ), invoice-list ) = 0 THEN
  DO:
    invoice-list = invoice-list + IF invoice-list = "" THEN "" ELSE ",".
    invoice-list = invoice-list + STRING( Invoice.InvoiceNo ) .
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  
  IF AVAILABLE Invoice THEN
  DO:
    RUN verify-invoice.
    IF RETURN-VALUE = "FAIL" THEN DO:
      RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
      RETURN.
    END.
    RUN dispatch( 'update-record':U ).
  END.

  CREATE Invoice.
  ASSIGN
    Invoice.InvoiceDate = TODAY
    Invoice.TaxApplies  = Office.GST <> ?.

  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'display-fields':U ).

  RUN reset-local-fields.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  APPLY 'ENTRY':U TO Invoice.InvoiceDate IN FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  have-records = Yes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-invoices V-table-Win 
PROCEDURE print-invoices :
/*------------------------------------------------------------------------------
  Purpose:     Print the invoice approval forms for all new/modified invoices
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEF VAR n AS INT NO-UNDO.

  n = NUM-ENTRIES( invoice-list ).
  IF  n = 0 THEN RETURN.

  MESSAGE n "Invoices to be printed" VIEW-AS ALERT-BOX INFORMATION.
  RUN process/report/invcappr.p( ?, ?, invoice-list ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-local-fields V-table-Win 
PROCEDURE reset-local-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  fil_from:SCREEN-VALUE  = "".  fil_from:MODIFIED = No.
  fil_to:SCREEN-VALUE  = "".    fil_to:MODIFIED = No.


  /* Set the default invoice terms */
  DEF VAR terms-index AS INT NO-UNDO.
  
  FIND InvoiceTerms WHERE InvoiceTerms.TermsCode = default-terms NO-LOCK NO-ERROR.
  IF AVAILABLE InvoiceTerms THEN
    terms-index = LOOKUP( STRING( ROWID( InvoiceTerms ) ), cmb_Terms:PRIVATE-DATA ).
  terms-index = MAX( terms-index, 1 ).

  cmb_Terms:SCREEN-VALUE = cmb_Terms:ENTRY( terms-index ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Invoice"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-details V-table-Win 
PROCEDURE update-details :
/*------------------------------------------------------------------------------
  Purpose:  Calculate all of the details of the tenants rental for the
            period indicated.
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF ( INPUT fil_From = ? OR INPUT fil_To = ? ) THEN RETURN.
  
  DEF VAR descriptive-word AS CHAR NO-UNDO  INITIAL "charges".
  DEF VAR tenant-code AS INT NO-UNDO.
  DEF VAR period-d1 AS DATE NO-UNDO.
  DEF VAR period-dn AS DATE NO-UNDO.

  tenant-code = (INPUT Invoice.EntityCode).
  period-d1   = (INPUT fil_from).
  period-dn   = (INPUT fil_to).
  descriptive-word = (INPUT rs_charge-type).

  RUN build-tenant-charges( tenant-code, period-d1, period-dn ).
  RUN make-blurb( descriptive-word, period-d1, period-dn, OUTPUT invoice-blurb,
                OUTPUT invoice-amount, OUTPUT invoice-tax, OUTPUT invoice-total ).

  fil_Blurb = invoice-blurb.
  DISPLAY fil_Blurb .
  fil_total:SCREEN-VALUE         = STRING( invoice-total ).
  Invoice.TaxAmount:SCREEN-VALUE = STRING( invoice-tax ).
  fil_amtdue:SCREEN-VALUE        = STRING( invoice-amount ).

END.
                
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-invoice V-table-Win 
PROCEDURE verify-invoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION include-lease V-table-Win 
FUNCTION include-lease RETURNS LOGICAL
  ( INPUT lease-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  Dummy function.
------------------------------------------------------------------------------*/

  RETURN Yes.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


