&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR sub-li  AS CHAR NO-UNDO.
DEF VAR sub-sv  AS CHAR NO-UNDO.
DEF VAR sub-pd  AS CHAR NO-UNDO.
DEF VAR sub-all AS LOGI NO-UNDO.
DEF VAR list-delim  AS CHAR NO-UNDO INIT "~~".
DEF VAR entity-selection AS CHAR NO-UNDO.

DEF VAR src-et AS CHAR NO-UNDO INITIAL "".
DEF VAR src-esub AS CHAR NO-UNDO INITIAL "".


DEF WORK-TABLE EntSel NO-UNDO
  FIELD EntityType  AS CHAR
  FIELD SelectionID AS CHAR
  FIELD LI AS CHAR
  FIELD SV AS CHAR
  FIELD PD AS CHAR
  FIELD SelAll AS LOGI.

DEF VAR rec-src AS HANDLE NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rad_etype cmb_esub tgl_Inactive tgl_all ~
sel_selection btn_ok btn_cancel 
&Scoped-Define DISPLAYED-OBJECTS rad_etype cmb_esub tgl_Inactive tgl_all ~
sel_selection 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD retrieve-attribute V-table-Win 
FUNCTION retrieve-attribute RETURNS CHARACTER
  ( INPUT selection-id AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 6.29 BY 1.15.

DEFINE BUTTON btn_ok 
     LABEL "&OK" 
     SIZE 4 BY 1.15.

DEFINE VARIABLE cmb_esub AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX SORT INNER-LINES 5
     LIST-ITEMS " "
     SIZE 36 BY 1 NO-UNDO.

DEFINE VARIABLE rad_etype AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Properties", "P",
"Ledgers", "L",
"Projects", "J"
     SIZE 37.72 BY 1 NO-UNDO.

DEFINE VARIABLE sel_selection AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
     SIZE 54.29 BY 10.6 NO-UNDO.

DEFINE VARIABLE tgl_all AS LOGICAL INITIAL no 
     LABEL "All" 
     VIEW-AS TOGGLE-BOX
     SIZE 5.14 BY 1 NO-UNDO.

DEFINE VARIABLE tgl_Inactive AS LOGICAL INITIAL no 
     LABEL "Inactives" 
     VIEW-AS TOGGLE-BOX
     SIZE 9.14 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     rad_etype AT ROW 1 COL 11.29 NO-LABEL
     cmb_esub AT ROW 2 COL 1 NO-LABEL
     tgl_Inactive AT ROW 2 COL 38.14
     tgl_all AT ROW 2 COL 49.57
     sel_selection AT ROW 3 COL 1 NO-LABEL
     btn_ok AT ROW 13.8 COL 1.57
     btn_cancel AT ROW 13.8 COL 6.14
     "Selection for:" VIEW-AS TEXT
          SIZE 9.14 BY 1 AT ROW 1 COL 1.57
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.95
         WIDTH              = 54.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cmb_esub IN FRAME F-Main
   ALIGN-L                                                              */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/string.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN cancel-changes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON CHOOSE OF btn_ok IN FRAME F-Main /* OK */
DO:
  RUN confirm-changes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_esub
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_esub V-table-Win
ON VALUE-CHANGED OF cmb_esub IN FRAME F-Main
DO:
  RUN get-selection( ?, ? ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rad_etype
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rad_etype V-table-Win
ON VALUE-CHANGED OF rad_etype IN FRAME F-Main
DO:
  RUN entity-type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_selection
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_selection V-table-Win
ON VALUE-CHANGED OF sel_selection IN FRAME F-Main
DO:
  IF AVAILABLE EntSel THEN EntSel.SV = SELF:SCREEN-VALUE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_all
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_all V-table-Win
ON VALUE-CHANGED OF tgl_all IN FRAME F-Main /* All */
DO:
  RUN all-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_Inactive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_Inactive V-table-Win
ON VALUE-CHANGED OF tgl_Inactive IN FRAME F-Main /* Inactives */
DO:
  ASSIGN FRAME {&FRAME-NAME} tgl_Inactive .
  DELETE EntSel.
  RUN get-selection( ?, ? ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE all-changed V-table-Win 
PROCEDURE all-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  IF AVAILABLE EntSel THEN EntSel.SelAll = INPUT tgl_all.
  sel_selection:SENSITIVE = NOT INPUT tgl_all.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN dispatch( 'exit':u ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR i AS INT NO-UNDO.
  
  FIND FIRST EntSel WHERE EntSel.EntityType = INPUT rad_etype
                AND EntSel.SelectionID = REPLACE(INPUT cmb_esub, " ", "-")
                NO-ERROR.
  IF NOT AVAILABLE(EntSel) THEN DO:
    MESSAGE INPUT rad_etype SKIP INPUT cmb_esub .
    FOR EACH EntSel:
      MESSAGE EntSel.EntityType EntSel.SelectionID .
    END.
  END.

  entity-selection = EntSel.EntityType + "," + INPUT cmb_esub.
  IF EntSel.SelALL THEN
    entity-selection = entity-selection + ",ALL".
  ELSE DO i = 1 TO NUM-ENTRIES( EntSel.SV, list-delim ):
    entity-selection = entity-selection + ","
                     + ENTRY( LOOKUP( ENTRY( i, EntSel.SV, list-delim ), EntSel.LI, list-delim ), EntSel.PD ).
  END.

  RUN get-rec-src.
  RUN update-entity-selection IN rec-src ( entity-selection ) NO-ERROR.
  RUN dispatch( 'exit':U ).
  
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-type-changed V-table-Win 
PROCEDURE entity-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR entity-type AS CHAR NO-UNDO.
  DEF VAR i AS INT NO-UNDO.
  DEF VAR id-list AS CHAR NO-UNDO.
  DEF VAR proc-id AS CHAR NO-UNDO.
  DEF VAR ie AS CHAR NO-UNDO.
  DEF VAR proc-entry AS CHAR NO-UNDO.
  
  ie = THIS-PROCEDURE:INTERNAL-ENTRIES.
  entity-type = INPUT FRAME {&FRAME-NAME} rad_etype.

  DO i = 1 TO NUM-ENTRIES( ie ):
    proc-entry = ENTRY( i, ie ).
    IF ENTRY( 1, proc-entry, '-' ) = 'get' AND
       ENTRY( NUM-ENTRIES( proc-entry, '-' ), proc-entry, '-' ) = entity-type THEN
    DO:
      proc-id = REPLACE( SUBSTR( proc-entry, 5 ), "-" + entity-type, "" ).
      proc-id = REPLACE( proc-id, '-', ' ' ).
      add-to-list( id-list, proc-id ).
    END.
  END.
  
  cmb_esub:LIST-ITEMS IN FRAME {&FRAME-NAME} = id-list.

  IF src-et = entity-type AND cmb_esub:LOOKUP(src-esub) > 0 THEN 
    cmb_esub:SCREEN-VALUE = src-esub.
  ELSE
    cmb_esub:SCREEN-VALUE = cmb_esub:ENTRY(1).

  RUN get-selection( entity-type, REPLACE( cmb_esub:SCREEN-VALUE, " ", "-" ) ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-Companies-L V-table-Win 
PROCEDURE get-Companies-L :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH Company WHERE Company.Active OR tgl_Inactive 
                    NO-LOCK:
    add-to-list-delim( sub-li, STRING( Company.CompanyCode, "9999" ) + " - " + Company.LegalName, list-delim ).
    add-to-list( sub-pd, STRING( Company.CompanyCode ) ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-Portfolios-P V-table-Win 
PROCEDURE get-Portfolios-P :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH Property WHERE Property.Active OR tgl_Inactive 
                    NO-LOCK BREAK BY Property.Manager:
  
    IF FIRST-OF( Property.Manager ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Manager NO-LOCK NO-ERROR.
      add-to-list-delim( sub-li, IF AVAILABLE Person
        THEN TRIM( Person.FirstName + ' ' + Person.LastName )
        ELSE 'Not in a Portfolio', list-delim ).
      add-to-list( sub-pd, STRING( Property.Administrator ) ).
    END.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-Projects-J V-table-Win 
PROCEDURE get-Projects-J :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH Project NO-LOCK:
    add-to-list-delim( sub-li, STRING( Project.ProjectCode, "99999" ) + " - " + Project.Name, list-delim ).
    add-to-list( sub-pd, STRING( Project.ProjectCode ) ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-Properties-P V-table-Win 
PROCEDURE get-Properties-P :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH Property WHERE Property.Active OR tgl_Inactive
                    NO-LOCK:
    add-to-list-delim( sub-li, STRING( Property.PropertyCode, "99999" ) + " - " + ( IF Property.Name = ? THEN "?" ELSE Property.Name ), list-delim ).
    add-to-list( sub-pd, STRING( Property.PropertyCode ) ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-Property-Managers-P V-table-Win 
PROCEDURE get-Property-Managers-P :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH Property WHERE Property.Active OR tgl_Inactive 
                      NO-LOCK BREAK BY Property.Administrator:
  
    IF FIRST-OF( Property.Administrator ) THEN DO:
      FIND Person WHERE Person.PersonCode = Property.Administrator NO-LOCK NO-ERROR.
      add-to-list-delim( sub-li, IF AVAILABLE Person
        THEN TRIM( Person.FirstName + ' ' + Person.LastName )
        ELSE 'Not managed', list-delim ).
      add-to-list( sub-pd, STRING( Property.Administrator ) ).
    END.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-rec-src V-table-Win 
PROCEDURE get-rec-src :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR c-rec-src AS CHAR NO-UNDO.
  
  IF VALID-HANDLE( rec-src ) THEN RETURN.
  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE', OUTPUT c-rec-src ).
  ASSIGN rec-src = WIDGET-HANDLE( c-rec-src ) NO-ERROR.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-Regions-P V-table-Win 
PROCEDURE get-Regions-P :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH Region NO-LOCK:
    add-to-list-delim( sub-li, Region.Region + " - " + Region.Name, list-delim ).
    add-to-list( sub-pd, Region.Region ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-selection V-table-Win 
PROCEDURE get-selection :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER entity-type  AS CHAR NO-UNDO.
DEF INPUT PARAMETER selection-id AS CHAR NO-UNDO.  

DO WITH FRAME {&FRAME-NAME}:

  IF entity-type  = ? THEN entity-type  = INPUT rad_etype.
  IF selection-id = ? THEN selection-id = REPLACE( cmb_esub:SCREEN-VALUE, " ", "-" ).

  FIND FIRST EntSel WHERE EntSel.EntityType  = entity-type
                    AND EntSel.SelectionID = selection-id NO-ERROR.
  
  IF NOT AVAILABLE EntSel THEN DO:
    sub-sv = "". sub-pd = "". sub-li = "". sub-all = No.
    RUN VALUE( "get-" + selection-id + "-" + entity-type ) IN THIS-PROCEDURE NO-ERROR.
    IF NOT ERROR-STATUS:ERROR THEN DO:
      RUN get-sv( selection-id ).
      CREATE EntSel.
      ASSIGN
        EntSel.EntityType  = entity-type
        EntSel.SelectionID = selection-id
        EntSel.LI = sub-li
        EntSel.SV = sub-sv
        EntSel.PD = sub-pd
        EntSel.SelAll = sub-all.
    END.
  END.
  
  sel_selection:DELIMITER    = list-delim.
  sel_selection:LIST-ITEMS   = EntSel.LI.
  sel_selection:PRIVATE-DATA = EntSel.PD.
  sel_selection:SCREEN-VALUE = EntSel.SV.
  tgl_all:SCREEN-VALUE = STRING( EntSel.SelAll ).
  
  RUN all-changed.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-sv V-table-Win 
PROCEDURE get-sv :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER selection-id AS CHAR NO-UNDO.
  
  DEF VAR options    AS CHAR NO-UNDO.
  DEF VAR option-id  AS CHAR NO-UNDO.
  DEF VAR option-et  AS CHAR NO-UNDO.
  DEF VAR option-val AS CHAR NO-UNDO.
  DEF VAR option-idx AS INT  NO-UNDO.
  DEF VAR i AS INT NO-UNDO.
  DEF VAR found AS LOGI NO-UNDO INIT No.

  sub-sv = "".

  option-et = ENTRY( 1, entity-selection).
  options = SUBSTRING( entity-selection, LENGTH(option-et) + 2 ).

  option-id = ENTRY( 1, options).
  found = (REPLACE(option-id, " ", "-") = selection-id).
  IF NOT found THEN RETURN.
  
  options = SUBSTRING( options, LENGTH(option-id) + 2 ).
  sub-all = options = "ALL".
  IF sub-all THEN RETURN.
  
  DO i = 1 TO NUM-ENTRIES( options ):
    option-val = ENTRY( i, options ).
    option-idx = LOOKUP( option-val, sub-pd ).
    IF option-idx > 0 THEN add-to-list-delim( sub-sv, ENTRY( option-idx, sub-li, list-delim ), list-delim ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN entity-type-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize V-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN retrieve-entities.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE retrieve-entities V-table-Win 
PROCEDURE retrieve-entities :
/*------------------------------------------------------------------------------
  Purpose:     Get the entity desctiption string from the record source.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  RUN get-rec-src.
  RUN send-entity-selection IN rec-src( OUTPUT entity-selection ) NO-ERROR.

  src-et = ENTRY( 1, entity-selection ).
  IF NUM-ENTRIES( entity-selection ) > 1 THEN
    src-esub = ENTRY( 2, entity-selection ).

  IF TRIM(src-et) <> "" THEN DO:
    rad_etype = src-et.
    DISPLAY rad_etype WITH FRAME {&FRAME-NAME}.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartViewer, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION retrieve-attribute V-table-Win 
FUNCTION retrieve-attribute RETURNS CHARACTER
  ( INPUT selection-id AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:
------------------------------------------------------------------------------*/

  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


