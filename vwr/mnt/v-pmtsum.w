&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r11 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* ***************************  History ******************************* */
/* $History: v-pmtsum.w $
 * 
 * *****************  Version 4  *****************
 * User: Andrew       Date: 22/12/97   Time: 14:50
 * Updated in $/VWR/MNT
 * Should only check in a couple of items - let's see!
 * 
 * *****************  Version 3  *****************
 * User: Andrew       Date: 22/12/97   Time: 9:45
 * Updated in $/VWR/MNT
 * Testing new SS version
 * 
 * *****************  Version 2  *****************
 * User: Andrew       Date: 19/12/97   Time: 10:47
 * Updated in $/VWR/MNT
 * Sending an update to OZ and to Auckland
 * 
 * *****************  Version 1  *****************
 * User: Andrew       Date: 14/04/97   Time: 10:51
 * Created in $/VWR/MNT
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Date1 RP.Int1 RP.Int2 RP.Date2 ~
RP.Log4 RP.Log1 RP.Log2 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define ENABLED-OBJECTS rs_SelectBy btn_OK RECT-2 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Date1 RP.Int1 RP.Int2 RP.Date2 ~
RP.Log4 RP.Log1 RP.Log2 
&Scoped-Define DISPLAYED-OBJECTS rs_SelectBy 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_OK DEFAULT 
     LABEL "&OK" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE VARIABLE rs_SelectBy AS LOGICAL 
     VIEW-AS RADIO-SET HORIZONTAL EXPAND 
     RADIO-BUTTONS 
          "by Cheque Number", FALSE,
"by Date Produced", TRUE
     SIZE 37 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 80.57 BY 9.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.25 COL 13 COLON-ALIGNED HELP
          ""
          LABEL "Bank Account" FORMAT "X(256)"
          VIEW-AS COMBO-BOX INNER-LINES 5
          DROP-DOWN-LIST
          SIZE 65.14 BY 1
     rs_SelectBy AT ROW 2.5 COL 7 NO-LABEL
     RP.Date1 AT ROW 4 COL 29 COLON-ALIGNED
          LABEL "From"
          VIEW-AS FILL-IN 
          SIZE 13 BY 1.05
     RP.Int1 AT ROW 4.1 COL 13 COLON-ALIGNED HELP
          ""
          LABEL "Start" FORMAT "999999"
          VIEW-AS FILL-IN 
          SIZE 7.72 BY .9
     RP.Int2 AT ROW 5 COL 13 COLON-ALIGNED HELP
          ""
          LABEL "Finish" FORMAT "999999"
          VIEW-AS FILL-IN 
          SIZE 7.72 BY .9
     RP.Date2 AT ROW 5 COL 29 COLON-ALIGNED
          LABEL "To"
          VIEW-AS FILL-IN 
          SIZE 13 BY 1.05
     RP.Log4 AT ROW 6.75 COL 15
          LABEL "Exclude property operating expenditure"
          VIEW-AS TOGGLE-BOX
          SIZE 30 BY .85
     RP.Log1 AT ROW 7.75 COL 15
          LABEL "Print Authorisation Page"
          VIEW-AS TOGGLE-BOX
          SIZE 21 BY 1
          FONT 10
     RP.Log2 AT ROW 8.75 COL 15
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 11 BY 1
          FONT 10
     btn_OK AT ROW 9.15 COL 70.72
     RECT-2 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 9
         DEFAULT-BUTTON btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 11.05
         WIDTH              = 100.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX RP.Char1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Date1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RP.Date2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_OK V-table-Win
ON CHOOSE OF btn_OK IN FRAME F-Main /* OK */
DO:
  RUN run-report.
  IF NOT (INPUT RP.Log2 OR RETURN-VALUE = "FAIL") THEN RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs_SelectBy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs_SelectBy V-table-Win
ON VALUE-CHANGED OF rs_SelectBy IN FRAME F-Main
DO:
  RUN selectby-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-bank-account V-table-Win 
PROCEDURE get-bank-account :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item-idx AS INT NO-UNDO.

  item-idx = RP.Char1:LOOKUP( INPUT RP.Char1 ).
  IF item-idx = 0 THEN RETURN.
    
  FIND BankAccount WHERE ROWID( BankAccount ) =
    TO-ROWID( ENTRY( item-idx, RP.Char1:PRIVATE-DATA ) ) NO-LOCK.

  RETURN BankAccount.BankAccountCode .
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN selectby-changed.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.  {inc/username.i "user-name"}
&SCOP REPORT-ID "Payment Summary"

  FIND RP WHERE RP.ReportID = {&REPORT-ID} AND RP.UserName = user-name NO-ERROR.
  IF NOT AVAILABLE(RP) THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Log1 = Yes
      RP.Log2 = Yes
    .
    RUN setup-bank-accounts( Yes ).
  END.
  ELSE
    RUN setup-bank-accounts( No ).

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.
DEF VAR item-idx AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  ASSIGN {&ENABLED-FIELDS}.
  RUN dispatch( 'update-fields':U ).

  item-idx = RP.Char1:LOOKUP( INPUT RP.Char1 ).
  FIND BankAccount WHERE ROWID( BankAccount ) =
    TO-ROWID( ENTRY( item-idx, RP.Char1:PRIVATE-DATA ) ) NO-LOCK.
  
  report-options = "BankAccount," + BankAccount.BankAccountCode
                 + (IF INPUT rs_SelectBy THEN
                    "~nDateRange," + STRING(RP.Date1) + "," + STRING(RP.Date2)
                   ELSE
                    "~nChequeRange," + STRING(RP.Int1) + "," + STRING(RP.Int2) )
                 + (IF RP.Log2 THEN "~nPreview" ELSE "")
                 + (IF RP.Log1 THEN "~nAuthorisationPage" ELSE "")
                 + (IF RP.Log4 THEN "~nNoOpex" ELSE "") .

  RUN notify( 'set-busy,container-source':U ).
  RUN process/report/pymtsum.p( report-options ).
  RUN notify( 'set-idle,container-source':U ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectby-changed V-table-Win 
PROCEDURE selectby-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF INPUT rs_SelectBy THEN DO:
    ENABLE RP.Date1 RP.Date2.
    DISABLE RP.Int1 RP.Int2.
  END.
  ELSE DO:
    DISABLE RP.Date1 RP.Date2.
    ENABLE RP.Int1 RP.Int2.
  END.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setup-bank-accounts V-table-Win 
PROCEDURE setup-bank-accounts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER set-default AS LOGICAL NO-UNDO.

  DEF VAR item    AS CHAR NO-UNDO.
  DEF VAR id-list AS CHAR NO-UNDO.
  DEF VAR default-account AS CHAR NO-UNDO.
  RP.Char1:LIST-ITEMS IN FRAME {&FRAME-NAME} = "".

  IF set-default THEN DO:
    FIND Office WHERE Office.ThisOffice NO-LOCK.
    FIND OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "PYMTBANK" NO-LOCK NO-ERROR.
  END.

  FOR EACH BankAccount NO-LOCK:
    item = STRING( BankAccount.CompanyCode, ">>9" )   + ' ' + 
           STRING( BankAccount.AccountCode, "9999.99" ) + ' ' +
                   BankAccount.AccountName.
    IF RP.Char1:ADD-LAST( item ) THEN.
    id-list = id-list + IF id-list = "" THEN "" ELSE ",".
    id-list = id-list + STRING( ROWID( BankAccount ) ).
    IF set-default AND AVAILABLE(OfficeControlAccount) AND BankAccount.BankAccountCode = OfficeControlAccount.Description THEN default-account = item.
  END.
  
  RP.Char1:PRIVATE-DATA = id-list.
  IF set-default THEN
    RP.Char1:SCREEN-VALUE = IF default-account <> "" THEN default-account
                                                     ELSE ENTRY( 1, RP.Char1:LIST-ITEMS ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

