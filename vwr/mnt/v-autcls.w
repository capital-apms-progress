&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: 

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&GLOB REPORT-ID "{&FILE-NAME}"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Log3 RP.Dec1 RP.Log1 RP.Log2 RP.Int4 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS rad_type fil_from fil_to fil_MaxTop ~
fil_MaxMed fil_MedLow fil_MaxLow btn_close RECT-24 
&Scoped-Define DISPLAYED-FIELDS RP.Log3 RP.Dec1 RP.Log1 RP.Log2 RP.Int4 
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define DISPLAYED-OBJECTS rad_type fil_from fil_to fil_MaxTop ~
fil_MaxMed fil_MedLow fil_MaxLow 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_close DEFAULT 
     LABEL "&OK" 
     SIZE 11.14 BY 1.05
     FONT 9.

DEFINE VARIABLE fil_from AS INTEGER FORMAT "999999":U INITIAL 0 
     LABEL "From" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE fil_MaxLow AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 15 
     LABEL "Lower level tests" 
     VIEW-AS FILL-IN 
     SIZE 5.14 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_MaxMed AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 30 
     LABEL "Medium level tests" 
     VIEW-AS FILL-IN 
     SIZE 5.14 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_MaxTop AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 40 
     LABEL "Top level tests" 
     VIEW-AS FILL-IN 
     SIZE 5.14 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_MedLow AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 3 
     LABEL "Med->Low at level" 
     VIEW-AS FILL-IN 
     SIZE 4.57 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_to AS INTEGER FORMAT "999999":U INITIAL 0 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE rad_type AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Tenants", "T",
"Creditors", "C"
     SIZE 9.72 BY 2
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-24
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 51.72 BY 7.7.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     rad_type AT ROW 1.3 COL 2.14 NO-LABEL
     fil_from AT ROW 1.7 COL 21.57 COLON-ALIGNED
     fil_to AT ROW 2.8 COL 21.57 COLON-ALIGNED
     RP.Log3 AT ROW 4 COL 2.43
          LABEL "Advanced options"
          VIEW-AS TOGGLE-BOX
          SIZE 17.72 BY .85
     RP.Dec1 AT ROW 5.1 COL 23.86 COLON-ALIGNED
          LABEL "Fuzziness"
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1.05
     RP.Log1 AT ROW 5.2 COL 5
          LABEL "Exact match"
          VIEW-AS TOGGLE-BOX
          SIZE 11.72 BY .85
     RP.Log2 AT ROW 6.2 COL 5
          LABEL "Generate batch"
          VIEW-AS TOGGLE-BOX
          SIZE 14.29 BY .85
     RP.Int4 AT ROW 7.5 COL 23.29 COLON-ALIGNED HELP
          ""
          LABEL "Maximum open transactions" FORMAT ">>9"
          VIEW-AS FILL-IN 
          SIZE 5.14 BY 1.05
     fil_MaxTop AT ROW 8.6 COL 23.29 COLON-ALIGNED
     fil_MaxMed AT ROW 9.7 COL 23.29 COLON-ALIGNED
     fil_MedLow AT ROW 10.2 COL 43.57 COLON-ALIGNED
     fil_MaxLow AT ROW 10.8 COL 23.29 COLON-ALIGNED
     btn_close AT ROW 12.2 COL 42.14
     RECT-24 AT ROW 4.3 COL 1.57
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_close.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 15.85
         WIDTH              = 62.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Dec1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RP.Int4 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_close
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_close V-table-Win
ON CHOOSE OF btn_close IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log1 V-table-Win
ON VALUE-CHANGED OF RP.Log1 IN FRAME F-Main /* Exact match */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log3 V-table-Win
ON VALUE-CHANGED OF RP.Log3 IN FRAME F-Main /* Advanced options */
DO:
  RUN enable-advanced-options.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rad_type
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rad_type V-table-Win
ON VALUE-CHANGED OF rad_type IN FRAME F-Main
DO:
  RUN entity-type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-advanced-options V-table-Win 
PROCEDURE enable-advanced-options :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
IF NOT AVAILABLE(RP) THEN RETURN.
DO WITH FRAME {&FRAME-NAME}:
  /* IF INPUT rad_type = "T" THEN */
    ENABLE RP.Log3.
  /* ELSE 
    DISABLE RP.Log3. */

  IF INPUT RP.Log3 /* AND (INPUT rad_type = "T") */ THEN DO:
    ENABLE RP.Int4 fil_MaxLow fil_MaxMed fil_MaxTop fil_MedLow RP.Dec1 RP.Log1 RP.Log2.
  END.
  ELSE DO:
    DISABLE RP.Int4 fil_MaxLow fil_MaxMed fil_MaxTop fil_MedLow RP.Dec1 RP.Log1 RP.Log2.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
IF NOT AVAILABLE(RP) THEN RETURN.
DO WITH FRAME {&FRAME-NAME}:
  RUN enable-advanced-options.
  IF INPUT RP.Log1 THEN DO:
    RP.Dec1:SCREEN-VALUE = "0.00".
    DISABLE RP.Dec1.
  END.
  ELSE DO:
    ENABLE RP.Dec1.
    DISPLAY RP.Dec1.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-type-changed V-table-Win 
PROCEDURE entity-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR entity-type AS CHAR NO-UNDO.
  entity-type = INPUT FRAME {&FRAME-NAME} rad_type.
  
  IF entity-type = 'T' THEN
  DO:
    FIND FIRST Tenant NO-LOCK. fil_from = Tenant.TenantCode.
    FIND LAST  Tenant NO-LOCK. fil_to   = Tenant.TenantCode.
  END.
  ELSE IF entity-type = 'C' THEN
  DO:
    FIND FIRST Creditor NO-LOCK. fil_from = Creditor.CreditorCode.
    FIND LAST  Creditor NO-LOCK. fil_to   = Creditor.CreditorCode.
  END.
  
  DISPLAY
    fil_from
    fil_to
  WITH FRAME {&FRAME-NAME}.

  RUN enable-appropriate-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN enable-appropriate-fields.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.
  
  RUN get-username IN sys-mgr( OUTPUT user-name ).

DO TRANSACTION:
  FIND RP WHERE RP.UserName = user-name AND RP.ReportID = {&REPORT-ID} NO-ERROR.

  IF NOT AVAILABLE RP THEN DO WITH FRAME {&FRAME-NAME}:
    CREATE RP.
    ASSIGN  RP.ReportID = {&REPORT-ID}
            RP.UserName = user-name
            RP.Log1 = Yes
            RP.Int4 = 40.
  END.
END.

  RUN dispatch( 'display-fields':U ).
  RUN entity-type-changed.
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  ASSIGN FRAME {&FRAME-NAME} rad_type fil_from fil_to
            fil_MaxLow fil_MaxMed fil_MaxTop fil_MedLow.
  RUN dispatch( 'update-record':U ).

  RUN notify( 'set-busy, CONTAINER-SOURCE':u ).  

  report-options = "EntityType," + rad_type
                 + "~nEntityRange," + STRING(fil_from) + "," + STRING(fil_to)
                 + (IF RP.Log3 AND (rad_type = "T") THEN
                      (IF RP.Log1 THEN "~nFuzziness," + TRIM(STRING(RP.Dec1,">9.99")) ELSE "")
                    + (IF RP.Log2 THEN "~nGenerateBatch" ELSE "")
                    + "~nMaxOpen," + STRING(RP.Int4)
                    + "~nMaxTop," + STRING(fil_MaxTop)
                    + "~nMaxMed," + STRING(fil_MaxMed)
                    + "~nMaxLow," + STRING(fil_MaxLow)
                    + "~nMedLevel," + STRING(fil_MedLow) ELSE "~nSimpleOnly").

  /* run it on the batch queue for more 'grunt' :-) */
  {inc/bq-do.i "process/autoclos.p" "report-options" "Yes"}

  RUN notify( 'set-idle, CONTAINER-SOURCE':u ).  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  ASSIGN FRAME {&FRAME-NAME} rad_type.
  
  IF INPUT FRAME {&FRAME-NAME} fil_to < INPUT FRAME {&FRAME-NAME} fil_from THEN
  DO:
    DEF VAR type-name AS CHAR NO-UNDO.
    type-name = IF rad_type = 'T' THEN 'Tenant' ELSE 'Creditor'.
    MESSAGE
      "The TO " + type-name + " must be greater than the FROM " + type-name
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid " + type-name + " Range".
    RETURN "FAIL".
  END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

