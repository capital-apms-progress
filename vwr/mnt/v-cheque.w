&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO.
DEF VAR bank-account AS CHAR NO-UNDO.
DEF VAR mode AS CHAR NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-acct.i "CREDITORS" "sundry-creditors"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Cheque
&Scoped-define FIRST-EXTERNAL-TABLE Cheque


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Cheque.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Cheque.DatePresented 
&Scoped-define ENABLED-TABLES Cheque
&Scoped-define FIRST-ENABLED-TABLE Cheque
&Scoped-define DISPLAYED-TABLES Cheque
&Scoped-define FIRST-DISPLAYED-TABLE Cheque
&Scoped-Define ENABLED-OBJECTS RECT-1 
&Scoped-Define DISPLAYED-FIELDS Cheque.CreditorCode Cheque.PayeeName ~
Cheque.BankAccountCode Cheque.ChequeNo Cheque.Date Cheque.BatchCode ~
Cheque.DatePresented Cheque.DocumentCode Cheque.Cancelled Cheque.Amount 
&Scoped-Define DISPLAYED-OBJECTS fil_CreditorName fil_BankDescription 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fil_BankDescription AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 61.14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_CreditorName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 61.14 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 80.57 BY 6.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Cheque.CreditorCode AT ROW 1.2 COL 11 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
          BGCOLOR 16 
     fil_CreditorName AT ROW 1.2 COL 17.86 COLON-ALIGNED NO-LABEL
     Cheque.PayeeName AT ROW 2.2 COL 11 COLON-ALIGNED
          LABEL "Payee"
          VIEW-AS FILL-IN 
          SIZE 68 BY 1
          BGCOLOR 16 
     Cheque.BankAccountCode AT ROW 3.2 COL 11 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
          BGCOLOR 16 
     fil_BankDescription AT ROW 3.2 COL 17.86 COLON-ALIGNED NO-LABEL
     Cheque.ChequeNo AT ROW 4.2 COL 11 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
          BGCOLOR 16 
     Cheque.Date AT ROW 4.2 COL 36.72 COLON-ALIGNED
          LABEL "Cheque Date"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
          BGCOLOR 16 
     Cheque.BatchCode AT ROW 4.2 COL 80.01 RIGHT-ALIGNED
          LABEL "Batch" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY .9
          BGCOLOR 16 
     Cheque.DatePresented AT ROW 5.2 COL 36.72 COLON-ALIGNED
          LABEL "Presented On"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
          BGCOLOR 16 
     Cheque.DocumentCode AT ROW 5.2 COL 72.72 COLON-ALIGNED
          LABEL "Document"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY .9
          BGCOLOR 16 
     Cheque.Cancelled AT ROW 6.4 COL 11 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 4.43 BY 1
          BGCOLOR 16 
     Cheque.Amount AT ROW 6.4 COL 61.86 COLON-ALIGNED FORMAT "ZZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 17.14 BY 1
          BGCOLOR 16 
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.Cheque
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 11.65
         WIDTH              = 98.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Cheque.Amount IN FRAME F-Main
   NO-ENABLE EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Cheque.BankAccountCode IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Cheque.BatchCode IN FRAME F-Main
   NO-ENABLE ALIGN-R EXP-LABEL EXP-FORMAT                               */
/* SETTINGS FOR FILL-IN Cheque.Cancelled IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Cheque.ChequeNo IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Cheque.CreditorCode IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Cheque.Date IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN Cheque.DatePresented IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Cheque.DocumentCode IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN fil_BankDescription IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_CreditorName IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Cheque.PayeeName IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Cheque.DatePresented
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Cheque.DatePresented V-table-Win
ON ANY-KEY OF Cheque.DatePresented IN FRAME F-Main /* Presented On */
DO:
  IF LAST-EVENT:LABEL = "DEL" THEN DO:
    SELF:SCREEN-VALUE = "".
    {&SELF-NAME} = ?.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Cheque"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Cheque"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
  RUN dispatch( 'exit':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-cheque V-table-Win 
PROCEDURE cancel-cheque :
/*------------------------------------------------------------------------------
  Purpose:  Cancel the cheque.
------------------------------------------------------------------------------*/
DEF VAR yes-do-it AS LOGICAL INITIAL no NO-UNDO.
DEF VAR cheque-month LIKE Month.MonthCode INITIAL ? NO-UNDO.
DEF VAR closing-group LIKE AcctTran.ClosingGroup INITIAL ? NO-UNDO.
DEF VAR docref-code AS INT NO-UNDO.

  IF Cheque.Cancelled = Yes THEN DO:
    MESSAGE "The cheque is already cancelled!"
        VIEW-AS ALERT-BOX ERROR TITLE "Cheque Already Cancelled".
    RETURN.
  END.

  IF Cheque.DatePresented <> ? THEN DO:
    MESSAGE "The cheque cannot be cancelled as it has already been presented!"
        VIEW-AS ALERT-BOX ERROR TITLE "Cheque Already Presented".
    RETURN.
  END.

  MESSAGE "Are you sure you want to cancel this cheque?"
        VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL TITLE "Cancel Cheque"
        UPDATE yes-do-it.

  IF NOT yes-do-it THEN RETURN.

  DEF VAR cheque-string AS CHAR NO-UNDO.
  cheque-string = TRIM( STRING( Cheque.ChequeNo, ">>>>>>>9")).

  FIND Document WHERE Document.BatchCode = Cheque.BatchCode
                  AND Document.DocumentCode = Cheque.DocumentCode
                  NO-LOCK NO-ERROR.

  IF AVAILABLE(Document) THEN ASSIGN docref-code = INTEGER(Document.Reference) NO-ERROR.
  IF AVAILABLE(Document) AND docref-code = Cheque.ChequeNo THEN DO:
    FIND FIRST AcctTran OF Document NO-LOCK NO-ERROR.
    cheque-month = AcctTran.MonthCode.
  END.
  ELSE DO:
    FOR EACH AcctTran NO-LOCK WHERE AcctTran.EntityType = "C"
             AND AcctTran.EntityCode = Cheque.CreditorCode
             AND AcctTran.AccountCode = sundry-creditors,
        FIRST Document OF AcctTran NO-LOCK WHERE Document.Reference = cheque-string:
      cheque-month = AcctTran.MonthCode.
      LEAVE.
    END.
  END.

  FIND FIRST BankAccount WHERE BankAccount.BankAccountCode = Cheque.BankAccountCode NO-LOCK.

  cancel-block:
  DO TRANSACTION:

    FIND CURRENT Cheque EXCLUSIVE-LOCK.
    Cheque.Cancelled = yes.

    CREATE NewBatch.
    ASSIGN NewBatch.BatchType = "NORM"
           NewBatch.Description = "Cancel cheque " + cheque-string.

    CREATE NewDocument.
    ASSIGN NewDocument.BatchCode = NewBatch.BatchCode
           NewDocument.DocumentCode = 1
           NewDocument.Reference = cheque-string
           NewDocument.Description = "CANCEL CHEQUE " + cheque-string.

    /* Credit the creditor to reverse payment */
    RUN create-trans( NewBatch.BatchCode, "C", Cheque.CreditorCode, sundry-creditors,
                        Cheque.Date, - Cheque.Amount, "" ).

    /* debit the bank account to reverse payment */
    RUN create-trans( NewBatch.BatchCode, "L", BankAccount.CompanyCode, BankAccount.AccountCode,
                        Cheque.Date, Cheque.Amount, "" ).

    /* unapprove and unallocate all the vouchers */
    FIND FIRST Voucher WHERE Voucher.ChequeNo = Cheque.ChequeNo NO-LOCK NO-ERROR.
    IF AVAILABLE(Voucher) AND Voucher.BatchCode > 0 AND Voucher.DocumentCode > 0 THEN
      FIND FIRST AcctTran WHERE AcctTran.BatchCode = Voucher.BatchCode
                            AND AcctTran.DocumentCode = Voucher.DocumentCode
                            AND AcctTran.ConsequenceOf = 0 NO-LOCK NO-ERROR.
    IF NOT (AVAILABLE(Voucher) AND Voucher.BatchCode > 0 AND Voucher.DocumentCode > 0 AND AVAILABLE(AcctTran)) THEN DO:
      yes-do-it = No.
      MESSAGE "No transactions for vouchers related to the cheque can be found!" SKIP(2)
              "The voucher batch may not yet have been updated yet.  If you " SKIP
              "continue, you will need to reverse the voucher allocation manually." SKIP(2)
              "Are you sure you still want to cancel this cheque?"
            VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL TITLE "Allocations for Vouchers of Cheque could not be found!"
            UPDATE yes-do-it.

      IF NOT yes-do-it THEN
        UNDO cancel-block, LEAVE cancel-block.
    END.
    FOR EACH Voucher WHERE Voucher.ChequeNo = Cheque.ChequeNo:
      IF Voucher.BatchCode > 0 AND Voucher.DocumentCode > 0 THEN DO:
        FOR EACH AcctTran WHERE AcctTran.BatchCode = Voucher.BatchCode
                            AND AcctTran.DocumentCode = Voucher.DocumentCode
                            AND AcctTran.ConsequenceOf = 0
                            NO-LOCK:
          RUN create-trans( NewBatch.BatchCode, AcctTran.EntityType, AcctTran.EntityCode, AcctTran.AccountCode, 
                            AcctTran.Date, - AcctTran.Amount, "CANCEL CHEQUE - " + Cheque.PayeeName ).
        END.
      END.
      ASSIGN Voucher.VoucherStatus = "C"
             Voucher.ChequeNo = ?
             Voucher.BatchCode = ?
             Voucher.DocumentCode = ?
             .
    END.
  END.
  RUN dispatch( 'row-available':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN dispatch( 'update-record':U ).
  RUN dispatch( 'exit':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-trans V-table-Win 
PROCEDURE create-trans :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER bcode AS INTEGER NO-UNDO.
DEF INPUT PARAMETER et AS CHAR NO-UNDO.
DEF INPUT PARAMETER ec AS INT NO-UNDO.
DEF INPUT PARAMETER ac AS DEC NO-UNDO.
DEF INPUT PARAMETER dt AS DATE NO-UNDO.
DEF INPUT PARAMETER amt AS DEC NO-UNDO.
DEF INPUT PARAMETER dsc AS CHAR NO-UNDO.

    CREATE NewAcctTrans.
    ASSIGN NewAcctTrans.BatchCode = bcode
           NewAcctTrans.DocumentCode = 1
           NewAcctTrans.Reference = (IF AVAILABLE(Voucher) THEN "VCHR" + STRING(Voucher.VoucherSeq) ELSE "")
           NewAcctTrans.EntityType = et
           NewAcctTrans.EntityCode = ec
           NewAcctTrans.AccountCode = ac
           NewAcctTrans.Date = dt
           NewAcctTrans.Amount = amt
           NewAcctTrans.Description = dsc.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF mode = "View" OR Cheque.Cancelled THEN Cheque.DatePresented:SENSITIVE = No.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE next-cheque V-table-Win 
PROCEDURE next-cheque :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN dispatch( 'update-record':U ).
  RUN notify( 'next-record,record-source':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE post-row-available V-table-Win 
PROCEDURE post-row-available :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Cheque) THEN RETURN.

  FIND Creditor WHERE Creditor.CreditorCode = Cheque.CreditorCode NO-LOCK NO-ERROR.
  IF AVAILABLE(Creditor) THEN fil_CreditorName = Creditor.Name.

  RUN refresh-window-title IN sys-mgr ( THIS-PROCEDURE ).

  FIND BankAccount WHERE BankAccount.BankAccountCode = Cheque.BankAccountCode NO-LOCK NO-ERROR.
  IF AVAILABLE(BankAccount) THEN
    fil_BankDescription = STRING( BankAccount.CompanyCode, "999") + "-" + STRING( BankAccount.AccountCode, "9999.99") + " - "
                        + BankAccount.BankName + ", " + BankAccount.BankBranchName .

  DISPLAY fil_CreditorName fil_BankDescription WITH FRAME {&FRAME-NAME}.
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  have-records = no.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE present-cheque V-table-Win 
PROCEDURE present-cheque :
/*------------------------------------------------------------------------------
  Purpose:  Mark the cheque as presented
------------------------------------------------------------------------------*/
  ENABLE Cheque.DatePresented WITH FRAME {&FRAME-NAME}.
  APPLY "ENTRY":U TO Cheque.DatePresented.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE previous-cheque V-table-Win 
PROCEDURE previous-cheque :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN dispatch( 'update-record':U ).
  RUN notify( 'prev-record,record-source':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Cheque"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name V-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-name AS CHAR NO-UNDO.
  key-name = new-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value V-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-value AS CHAR NO-UNDO.

  key-value = new-value.

  IF key-name = "ChequeNo" THEN DO:
    bank-account = find-parent-key( "BankAccountCode" ).
    FIND Cheque WHERE Cheque.BankAccountCode = bank-account
                  AND Cheque.ChequeNo = INT(key-value)
                  NO-LOCK NO-ERROR.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

