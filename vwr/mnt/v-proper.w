&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tpproper.i}

DEF VAR mode AS CHAR NO-UNDO.

DEF VAR record-changed AS LOGICAL NO-UNDO INITIAL No.

{inc/ofc-this.i}
{inc/ofc-set.i "Reconciliation-Date" "reconciliation-date-text"}
IF NOT AVAILABLE(OfficeSetting) THEN reconciliation-date-text = "".

{inc/ofc-set-l.i "Property-MarketPerUnit" "market-per-unit"}
{inc/ofc-set.i "Area-Units" "area-units"}
IF NOT AVAILABLE(OfficeSetting) THEN area-units = "Sq.M.".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Property
&Scoped-define FIRST-EXTERNAL-TABLE Property


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Property.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Property.PropertyCode Property.Active ~
Property.ExternallyManaged Property.ShortName Property.Name ~
Property.CompanyCode Property.City Property.StreetAddress ~
Property.WofExpiryDate Property.ReconciliationDue Property.MarketCarpark ~
Property.TotalArea Property.TotalParks Property.MarketRental ~
Property.OpexEstimate 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}PropertyCode ~{&FP2}PropertyCode ~{&FP3}~
 ~{&FP1}ShortName ~{&FP2}ShortName ~{&FP3}~
 ~{&FP1}Name ~{&FP2}Name ~{&FP3}~
 ~{&FP1}CompanyCode ~{&FP2}CompanyCode ~{&FP3}~
 ~{&FP1}City ~{&FP2}City ~{&FP3}~
 ~{&FP1}StreetAddress ~{&FP2}StreetAddress ~{&FP3}~
 ~{&FP1}WofExpiryDate ~{&FP2}WofExpiryDate ~{&FP3}~
 ~{&FP1}ReconciliationDue ~{&FP2}ReconciliationDue ~{&FP3}~
 ~{&FP1}MarketCarpark ~{&FP2}MarketCarpark ~{&FP3}~
 ~{&FP1}TotalArea ~{&FP2}TotalArea ~{&FP3}~
 ~{&FP1}TotalParks ~{&FP2}TotalParks ~{&FP3}~
 ~{&FP1}MarketRental ~{&FP2}MarketRental ~{&FP3}~
 ~{&FP1}OpexEstimate ~{&FP2}OpexEstimate ~{&FP3}
&Scoped-define ENABLED-TABLES Property
&Scoped-define FIRST-ENABLED-TABLE Property
&Scoped-Define ENABLED-OBJECTS RECT-17 RECT-18 cmb_BuildingType cmb_Region 
&Scoped-Define DISPLAYED-FIELDS Property.PropertyCode Property.Active ~
Property.ExternallyManaged Property.ShortName Property.Name ~
Property.CompanyCode Property.City Property.StreetAddress ~
Property.WofExpiryDate Property.ReconciliationDue Property.MarketCarpark ~
Property.TotalArea Property.TotalParks Property.MarketRental ~
Property.OpexEstimate 
&Scoped-Define DISPLAYED-OBJECTS fil_Company cmb_BuildingType cmb_Region ~
fil_TotalArea fil_TotalParks fil_MarketRental fil_OpexEstimate fil_Manager ~
fil_Administrator 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
BuildingType|y|y|TTPL.Property.BuildingType
PropertyCode|y|y|TTPL.Property.PropertyCode
CompanyCode|y|y|TTPL.Property.CompanyCode
NoteCode||y|TTPL.Property.NoteCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "BuildingType,PropertyCode,CompanyCode",
     Keys-Supplied = "BuildingType,PropertyCode,CompanyCode,NoteCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_BuildingType AS CHARACTER FORMAT "X(50)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "","" 
     SIZE 19.43 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Region AS CHARACTER FORMAT "X(50)":U 
     LABEL "Region" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "","" 
     SIZE 23.43 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Administrator AS CHARACTER FORMAT "X(25)":U 
     LABEL "Backup Mgr" 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Company AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 44 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Manager AS CHARACTER FORMAT "X(25)":U 
     LABEL "Building Mgr" 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE fil_MarketRental AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99" INITIAL 0 
     LABEL "Budget EAR" 
     VIEW-AS FILL-IN 
     SIZE 14.29 BY 1
     BGCOLOR 16 .

DEFINE VARIABLE fil_OpexEstimate AS DECIMAL FORMAT "-Z,ZZZ,ZZ9.99" INITIAL 0 
     LABEL "OPEX" 
     VIEW-AS FILL-IN 
     SIZE 13.14 BY .9
     BGCOLOR 16 .

DEFINE VARIABLE fil_TotalArea AS DECIMAL FORMAT "Z,ZZZ,ZZ9.999" INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 14.86 BY .9
     BGCOLOR 16 .

DEFINE VARIABLE fil_TotalParks AS INTEGER FORMAT "ZZ,ZZ9" INITIAL 0 
     LABEL "Parks" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1
     BGCOLOR 16 .

DEFINE RECTANGLE RECT-17
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 92 BY 1.8.

DEFINE RECTANGLE RECT-18
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 93.14 BY 11.8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Property.PropertyCode AT ROW 1.2 COL 10.43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
          FONT 11
     Property.Active AT ROW 1.2 COL 41.57
          VIEW-AS TOGGLE-BOX
          SIZE 10.72 BY 1
     Property.ExternallyManaged AT ROW 1.2 COL 69
          LABEL "Externally Managed"
          VIEW-AS TOGGLE-BOX
          SIZE 24.57 BY 1
     Property.ShortName AT ROW 2.4 COL 10.43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     Property.Name AT ROW 2.4 COL 39.57 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 52 BY 1
     Property.CompanyCode AT ROW 3.45 COL 39.57 COLON-ALIGNED FORMAT ">>999"
          VIEW-AS FILL-IN 
          SIZE 4.57 BY 1
     fil_Company AT ROW 3.4 COL 92.57 RIGHT-ALIGNED NO-LABEL
     cmb_BuildingType AT ROW 4.6 COL 10.43 COLON-ALIGNED
     cmb_Region AT ROW 4.6 COL 35.85
     Property.City AT ROW 4.6 COL 67 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 24.57 BY 1
     Property.StreetAddress AT ROW 5.6 COL 10.43 COLON-ALIGNED
          LABEL "Street Address" FORMAT "X(80)"
          VIEW-AS FILL-IN 
          SIZE 81.14 BY 1
     Property.WofExpiryDate AT ROW 7 COL 10.43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11.43 BY .9
     Property.ReconciliationDue AT ROW 7 COL 35 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11.57 BY .9
     Property.MarketCarpark AT ROW 7 COL 78.43 COLON-ALIGNED
          LABEL "Budget - per Park, per Week" FORMAT "-ZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 13.14 BY .9
     Property.TotalArea AT ROW 8.4 COL 10.43 COLON-ALIGNED NO-LABEL FORMAT "Z,ZZZ,ZZ9.999"
          VIEW-AS FILL-IN 
          SIZE 14.86 BY .9
     Property.TotalParks AT ROW 8.4 COL 35 COLON-ALIGNED
          LABEL "Parks" FORMAT "ZZ,ZZ9"
          VIEW-AS FILL-IN 
          SIZE 8 BY .9
     Property.MarketRental AT ROW 8.4 COL 56.72 COLON-ALIGNED
          LABEL "Budget EAR" FORMAT "-ZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14.29 BY .9
     Property.OpexEstimate AT ROW 8.4 COL 78.43 COLON-ALIGNED
          LABEL "OPEX" FORMAT "-Z,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 13.14 BY .9
     fil_TotalArea AT ROW 9.4 COL 10.43 COLON-ALIGNED NO-LABEL
     fil_TotalParks AT ROW 9.4 COL 35 COLON-ALIGNED
     fil_MarketRental AT ROW 9.4 COL 56.72 COLON-ALIGNED
     fil_OpexEstimate AT ROW 9.4 COL 78.43 COLON-ALIGNED
     fil_Manager AT ROW 11.4 COL 10.43 COLON-ALIGNED
     fil_Administrator AT ROW 11.4 COL 60.14 COLON-ALIGNED
     RECT-17 AT ROW 10.85 COL 1.57
     RECT-18 AT ROW 1 COL 1
     "Entered - Area:" VIEW-AS TEXT
          SIZE 10.86 BY .9 AT ROW 8.4 COL 1.57
          FONT 10
     "Sums of - Area:" VIEW-AS TEXT
          SIZE 10.86 BY .9 AT ROW 9.4 COL 1.57
          FONT 10
     "Officials" VIEW-AS TEXT
          SIZE 9 BY .6 AT ROW 10.6 COL 2.14
          FONT 14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Property
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.8
         WIDTH              = 105.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cmb_Region IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Property.CompanyCode IN FRAME F-Main
   EXP-FORMAT                                                           */
/* SETTINGS FOR TOGGLE-BOX Property.ExternallyManaged IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Administrator IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Company IN FRAME F-Main
   NO-ENABLE ALIGN-R                                                    */
/* SETTINGS FOR FILL-IN fil_Manager IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_MarketRental IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_OpexEstimate IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_TotalArea IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_TotalParks IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Property.MarketCarpark IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Property.MarketRental IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Property.OpexEstimate IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Property.StreetAddress IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Property.TotalArea IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Property.TotalParks IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME cmb_BuildingType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_BuildingType V-table-Win
ON U1 OF cmb_BuildingType IN FRAME F-Main /* Type */
DO:
  {inc/selcmb/scbty1.i "Property" "BuildingType"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_BuildingType V-table-Win
ON U2 OF cmb_BuildingType IN FRAME F-Main /* Type */
DO:
  {inc/selcmb/scbty2.i "Property" "BuildingType"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Region
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Region V-table-Win
ON U1 OF cmb_Region IN FRAME F-Main /* Region */
DO:
  {inc/selcmb/scrgn1.i "Property" "Region"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Region V-table-Win
ON U2 OF cmb_Region IN FRAME F-Main /* Region */
DO:
  {inc/selcmb/scrgn2.i "Property" "Region"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Property.CompanyCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Property.CompanyCode V-table-Win
ON LEAVE OF Property.CompanyCode IN FRAME F-Main /* Company */
DO:
  {inc/selcde/cdcmp.i "fil_Company"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Administrator
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Administrator V-table-Win
ON U1 OF fil_Administrator IN FRAME F-Main /* Backup Mgr */
DO:
  {inc/selfil/sfpsn1.i "Property" "Administrator"} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Administrator V-table-Win
ON U2 OF fil_Administrator IN FRAME F-Main /* Backup Mgr */
DO:
  {inc/selfil/sfpsn2.i "Property" "Administrator"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Administrator V-table-Win
ON U3 OF fil_Administrator IN FRAME F-Main /* Backup Mgr */
DO:
  {inc/selfil/sfpsn3.i "Property" "Administrator"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Company
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U1 OF fil_Company IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "Property" "CompanyCode}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U2 OF fil_Company IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "Property" "CompanyCode}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Company V-table-Win
ON U3 OF fil_Company IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "Property" "CompanyCode}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Manager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Manager V-table-Win
ON U1 OF fil_Manager IN FRAME F-Main /* Building Mgr */
DO:
  {inc/selfil/sfpsn1.i "Property" "Manager"} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Manager V-table-Win
ON U2 OF fil_Manager IN FRAME F-Main /* Building Mgr */
DO:
  {inc/selfil/sfpsn2.i "Property" "Manager"} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Manager V-table-Win
ON U3 OF fil_Manager IN FRAME F-Main /* Building Mgr */
DO:
  {inc/selfil/sfpsn3.i "Property" "Manager"}   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

IF market-per-unit THEN DO WITH FRAME {&FRAME-NAME}:
  Property.MarketRental:LABEL = "Budget, per " + area-units.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'BuildingType':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Property
           &WHERE = "WHERE Property.BuildingType eq key-value"
       }
    WHEN 'PropertyCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Property
           &WHERE = "WHERE Property.PropertyCode eq INTEGER(key-value)"
       }
    WHEN 'CompanyCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Property
           &WHERE = "WHERE Property.CompanyCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Property"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Property"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':u ).
  IF mode = "Add" THEN RUN delete-property. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT record-changed THEN RUN test-modified( OUTPUT record-changed ).

  IF record-changed THEN DO:
    RUN verify-property.
    IF RETURN-VALUE = "FAIL" THEN RETURN.

    RUN check-rights IN sys-mgr( "V-PROPER", "MODIFY" ).
    IF RETURN-VALUE = "FAIL" THEN RETURN.

    RUN notify( 'hide, CONTAINER-SOURCE':u ).
    RUN dispatch( 'update-record':U ).
    RUN dispatch( 'open-query,record-source':U ).
  END.
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-property V-table-Win 
PROCEDURE delete-property :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Property EXCLUSIVE-LOCK.
  DELETE Property.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-disable-fields V-table-Win 
PROCEDURE inst-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  cmb_BuildingType:SENSITIVE = No.
  cmb_Region:SENSITIVE       = No.
  cmb_BuildingType:FGCOLOR   = 0.
  cmb_Region:FGCOLOR         = 0.
  cmb_BuildingType:BGCOLOR   = 16.
  cmb_Region:BGCOLOR         = 16.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  Property.PropertyCode:SENSITIVE IN FRAME {&FRAME-NAME} = mode = "Add".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN DO:
    have-records = yes.
    CURRENT-WINDOW:TITLE = "Adding New Property".
    RUN dispatch( 'add-record':U ).
  END.

  IF reconciliation-date-text <> "" THEN
    Property.ReconciliationDue:LABEL IN FRAME {&FRAME-NAME} = "Annual Rec " + reconciliation-date-text.

  IF mode <> "View" THEN
    RUN dispatch( 'enable-fields':U ).
  ELSE
    RUN dispatch( 'disable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Property) OR mode = "Add" THEN RETURN.
  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN
      fil_TotalArea = 0
      fil_TotalParks = 0
      fil_MarketRental = 0
      fil_OpexEstimate = 0
    .

      FOR EACH RentalSpace OF Property NO-LOCK:
        IF RentalSpace.AreaType = "C" THEN
          fil_TotalParks = fil_TotalParks + RentalSpace.AreaSize.
        ELSE IF (RentalSpace.AreaSize > 20)
                OR (INTEGER(RentalSpace.AreaSize) <> RentalSpace.AreaSize ) THEN
          fil_TotalArea = fil_TotalArea + RentalSpace.AreaSize.

        fil_MarketRental = fil_MarketRental + IF RentalSpace.MarketRental <> ? THEN RentalSpace.MarketRental ELSE 0.

        IF RentalSpace.AreaStatus = "V" THEN
          fil_OpexEstimate = fil_OpexEstimate + RentalSpace.VacantCosts.
      END.
      FOR EACH TenancyLease OF Property WHERE TenancyLease.LeaseStatus <> "PAST" NO-LOCK:
        fil_OpexEstimate = fil_OpexEstimate + TenancyLease.OutgoingsBudget.
      END.

    DISPLAY fil_TotalArea   fil_TotalParks   fil_MarketRental  fil_OpexEstimate .
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND Property WHERE Property.PropertyCode = 0 EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE(Property) THEN DO:
    CREATE Property.
  END.
  ASSIGN Property.PropertyCode = 0
         Property.Active = Yes
         record-changed = Yes.

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'update-record':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "BuildingType" "Property" "BuildingType"}
  {src/adm/template/sndkycas.i "PropertyCode" "Property" "PropertyCode"}
  {src/adm/template/sndkycas.i "CompanyCode" "Property" "CompanyCode"}
  {src/adm/template/sndkycas.i "NoteCode" "Property" "NoteCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Property"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
 mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-property V-table-Win 
PROCEDURE verify-property :
/*------------------------------------------------------------------------------
  Purpose:  Check property values are OK before writing
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF NOT AVAILABLE(Property) THEN RETURN.

  IF INPUT Property.PropertyCode = 0 THEN DO:
    MESSAGE "You must enter a property code !" VIEW-AS ALERT-BOX
      ERROR TITLE "Invalid Property Code".
    APPLY 'ENTRY':U TO Property.PropertyCode IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.

  FIND Company WHERE Company.CompanyCode = INPUT Property.CompanyCode NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Company) THEN DO:
    MESSAGE "You must enter a company code !" VIEW-AS ALERT-BOX
      ERROR TITLE "Invalid Company Code".
    APPLY 'ENTRY':U TO fil_Company IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.

  IF mode = "Add" AND INPUT Property.PropertyCode <> Property.PropertyCode THEN DO:
    DEF BUFFER AltProp FOR Property.
    FIND AltProp WHERE AltProp.PropertyCode = INPUT Property.PropertyCode NO-LOCK NO-ERROR.
    IF AVAILABLE(AltProp) THEN DO:
      MESSAGE "Property already exists with code " + STRING( INPUT Property.PropertyCode , "99999")
              VIEW-AS ALERT-BOX ERROR
              TITLE "Property Already Exists".
      APPLY 'ENTRY':U TO Property.PropertyCode.
      RETURN "FAIL".
    END.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


