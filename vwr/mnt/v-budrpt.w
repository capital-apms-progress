&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

DEF VAR mth-desc AS CHAR NO-UNDO.

DEFINE TEMP-TABLE MthFigs NO-UNDO
    FIELD MonthCode AS INT
    FIELD StartDate AS CHAR   FORMAT 'X(13)'  COLUMN-LABEL '    From'
    FIELD EndDate AS CHAR     FORMAT 'X(13)'  COLUMN-LABEL '      To'
    FIELD FigsFrom AS CHAR    FORMAT '   X' COLUMN-LABEL 'Type'
    FIELD Description AS CHAR FORMAT 'X(32)'  COLUMN-LABEL 'Description'
INDEX IX-A IS UNIQUE PRIMARY MonthCode ASCENDING.

DEF VAR set-list AS CHAR NO-UNDO INITIAL "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-mths

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES MthFigs

/* Definitions for BROWSE br-mths                                       */
&Scoped-define FIELDS-IN-QUERY-br-mths StartDate EndDate FigsFrom Description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-mths FigsFrom   
&Scoped-define FIELD-PAIRS-IN-QUERY-br-mths~
 ~{&FP1}FigsFrom ~{&FP2}FigsFrom ~{&FP3}
&Scoped-define SELF-NAME br-mths
&Scoped-define OPEN-QUERY-br-mths OPEN QUERY {&SELF-NAME} FOR EACH MthFigs.
&Scoped-define TABLES-IN-QUERY-br-mths MthFigs
&Scoped-define FIRST-TABLE-IN-QUERY-br-mths MthFigs


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-mths}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Log3 RP.Log1 RP.Char4 RP.Char1 RP.Int2 ~
RP.Int3 RP.Int1 RP.Char5 RP.Char3 RP.Log2 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Char1 ~{&FP2}Char1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}~
 ~{&FP1}Int3 ~{&FP2}Int3 ~{&FP3}~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}~
 ~{&FP1}Char5 ~{&FP2}Char5 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_EntityList RECT-2 cmb_From cmb_To ~
br-mths btn_OK btn_Browse 
&Scoped-Define DISPLAYED-FIELDS RP.Log3 RP.Log1 RP.Char4 RP.Char1 RP.Int2 ~
RP.Int3 RP.Int1 RP.Char5 RP.Char3 RP.Log2 
&Scoped-Define DISPLAYED-OBJECTS cmb_EntityList fil_Entity-1 fil_Entity-2 ~
cmb_From cmb_To fil_Year 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Browse 
     LABEL "&Browse" 
     SIZE 8.57 BY 1.1.

DEFINE BUTTON btn_OK DEFAULT 
     LABEL "&OK" 
     SIZE 10.86 BY 1.2
     FONT 9.

DEFINE VARIABLE cmb_EntityList AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity List" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     SIZE 54.86 BY 1.05 NO-UNDO.

DEFINE VARIABLE cmb_From AS DATE FORMAT "99/99/9999":U 
     LABEL "Period from" 
     VIEW-AS COMBO-BOX INNER-LINES 18
     LIST-ITEMS "1/1/97" 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_To AS DATE FORMAT "99/99/9999":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 18
     LIST-ITEMS "31/12/97" 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Entity-1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 52.57 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Entity-2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 52.57 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_Year AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 52.57 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 70.86 BY 20.9.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-mths FOR 
      MthFigs SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-mths
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-mths V-table-Win _FREEFORM
  QUERY br-mths DISPLAY
      StartDate
    EndDate
    FigsFrom
    Description
ENABLE
    FigsFrom
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SIZE 52.57 BY 10.9
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     cmb_EntityList AT ROW 2.8 COL 14.43 COLON-ALIGNED
     RP.Log3 AT ROW 1.2 COL 3.57 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "Range", TRUE,
"List", FALSE
          SIZE 21.72 BY .8
     RP.Log1 AT ROW 20.4 COL 4.43
          LABEL "Summary"
          VIEW-AS TOGGLE-BOX
          SIZE 13.43 BY .85
     RP.Char4 AT ROW 5.2 COL 3.86 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL EXPAND 
          RADIO-BUTTONS 
                    "Printer", "P":U,
"Spreadsheet", "S":U
          SIZE 23.43 BY .8
          FONT 10
     RP.Char1 AT ROW 2.3 COL 7 COLON-ALIGNED
          LABEL "Entity" FORMAT "X"
          VIEW-AS FILL-IN 
          SIZE 2.86 BY 1
     RP.Int2 AT ROW 2.3 COL 9.86 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_Entity-1 AT ROW 2.3 COL 16.72 COLON-ALIGNED NO-LABEL
     RP.Int3 AT ROW 3.45 COL 9.86 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_Entity-2 AT ROW 3.45 COL 16.72 COLON-ALIGNED NO-LABEL
     cmb_From AT ROW 6.15 COL 9.86 COLON-ALIGNED
     cmb_To AT ROW 6.15 COL 35 COLON-ALIGNED
     RP.Int1 AT ROW 6.85 COL 6.43 COLON-ALIGNED HELP
          ""
          LABEL "Year" FORMAT "9999"
          VIEW-AS FILL-IN 
          SIZE 5.14 BY 1
     fil_Year AT ROW 6.85 COL 16.72 COLON-ALIGNED NO-LABEL
     RP.Char5 AT ROW 7.5 COL 9.86 COLON-ALIGNED
          LABEL "File" FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 47.43 BY 1
     RP.Char3 AT ROW 9.3 COL 3.86 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Individual", "Individual":U,
"Budget", "Budget":U,
"Revised", "Revised Budget":U,
"Actual", "Actual":U,
"Variance (B)", "Variance (Budget)":U,
"Variance (R)", "Variance (Revised)":U
          SIZE 12 BY 5.4
          FONT 10
     br-mths AT ROW 8.85 COL 18.14
     RP.Log2 AT ROW 20.4 COL 35.29
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 8.57 BY 1
          FONT 10
     btn_OK AT ROW 20.4 COL 59.29
     btn_Browse AT ROW 7.5 COL 61.57
     RECT-2 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 22.7
         WIDTH              = 87.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
/* BROWSE-TAB br-mths Char3 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR RADIO-SET RP.Char3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR RADIO-SET RP.Char4 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Char5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN fil_Entity-1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Entity-2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Year IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR RADIO-SET RP.Log3 IN FRAME F-Main
   EXP-HELP                                                             */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-mths
/* Query rebuild information for BROWSE br-mths
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH MthFigs.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-mths */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br-mths
&Scoped-define SELF-NAME br-mths
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-mths V-table-Win
ON ROW-DISPLAY OF br-mths IN FRAME F-Main
DO:
  RUN set-row-description.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Browse V-table-Win
ON CHOOSE OF btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_OK V-table-Win
ON CHOOSE OF btn_OK IN FRAME F-Main /* OK */
DO:
  DISABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON LEAVE OF RP.Char1 IN FRAME F-Main /* Entity */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  DO WITH FRAME {&FRAME-NAME}:
    SELF:SCREEN-VALUE = CAPS(SELF:SCREEN-VALUE).
    RUN get-entity-name( SELF:SCREEN-VALUE, INT(RP.Int2:SCREEN-VALUE), OUTPUT fil_Entity-1 ).
    RUN get-entity-name( SELF:SCREEN-VALUE, INT(RP.Int2:SCREEN-VALUE), OUTPUT fil_Entity-2 ).
    DISPLAY fil_Entity-1 fil_Entity-2 .
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN data-type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char4 V-table-Win
ON VALUE-CHANGED OF RP.Char4 IN FRAME F-Main /* Char4 */
DO:
  RUN output-type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_EntityList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_EntityList V-table-Win
ON U1 OF cmb_EntityList IN FRAME F-Main /* Entity List */
DO:
  {inc/selcmb/scels1.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_EntityList V-table-Win
ON U2 OF cmb_EntityList IN FRAME F-Main /* Entity List */
DO:
  {inc/selcmb/scels2.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_From
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_From V-table-Win
ON VALUE-CHANGED OF cmb_From IN FRAME F-Main /* Period from */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_To
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_To V-table-Win
ON VALUE-CHANGED OF cmb_To IN FRAME F-Main /* To */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Year
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Year V-table-Win
ON U1 OF fil_Year IN FRAME F-Main
DO:
  {inc/selfil/sffyr1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Year V-table-Win
ON U2 OF fil_Year IN FRAME F-Main
DO:
  {inc/selfil/sffyr2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Year V-table-Win
ON U3 OF fil_Year IN FRAME F-Main
DO:
  {inc/selfil/sffyr3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Year */
DO:
  IF INPUT {&SELF-NAME} <> {&SELF-NAME} THEN
  RUN dispatch( 'enable-fields':U ).
  {inc/selcde/cdfyr.i "fil_Year"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* Int2 */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    RUN get-entity-name( RP.Char1:SCREEN-VALUE, INT(SELF:SCREEN-VALUE), OUTPUT fil_Entity-1 ).
    DISPLAY fil_Entity-1.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int3 V-table-Win
ON LEAVE OF RP.Int3 IN FRAME F-Main /* Int3 */
DO:
  DO WITH FRAME {&FRAME-NAME}:
    RUN get-entity-name( RP.Char1:SCREEN-VALUE, INT(SELF:SCREEN-VALUE), OUTPUT fil_Entity-2 ).
    DISPLAY fil_Entity-2 .
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log3 V-table-Win
ON VALUE-CHANGED OF RP.Log3 IN FRAME F-Main /* Log3 */
DO:
  RUN selection-type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

ON LEAVE OF MthFigs.FigsFrom DO:
  IF LOOKUP( INPUT BROWSE {&BROWSE-NAME} MthFigs.FigsFrom, 'A,B,R,V,X') = 0 THEN DO:
    MESSAGE 'Value must be one of' SKIP '(A)ctual, (B)udget, (R)evised, (V)ariance from B or (X) variance from R'
            VIEW-AS ALERT-BOX ERROR TITLE "Invalid Value".
    RETURN NO-APPLY.
  END.
  MthFigs.FigsFrom:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = CAPS(MthFigs.FigsFrom:SCREEN-VALUE).
  APPLY 'ROW-DISPLAY' TO BROWSE br-mths.
END.

ON ENTRY OF MthFigs.FigsFrom DO:
  MthFigs.FigsFrom:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = CAPS(MthFigs.FigsFrom:SCREEN-VALUE).
  APPLY 'ROW-DISPLAY' TO BROWSE br-mths.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-mths-query V-table-Win 
PROCEDURE close-mths-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR m1 AS INT NO-UNDO.
DEF VAR m2 AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  IF INPUT RP.Char4 = "P" THEN DO:
    FIND FIRST Month WHERE Month.FinancialYearCode = INPUT RP.Int1 NO-LOCK.
    m1 = Month.MonthCode.
    FIND LAST Month WHERE Month.FinancialYearCode = INPUT RP.Int1 NO-LOCK.
    m2 = Month.MonthCode.
  END.
  ELSE DO:
    FIND FIRST Month WHERE Month.StartDate >= INPUT cmb_From NO-LOCK.
    m1 = Month.MonthCode.
    FIND LAST Month WHERE Month.EndDate <= INPUT cmb_To NO-LOCK.
    m2 = Month.MonthCode.
  END.

  set-list = "".
  FOR EACH Month WHERE Month.MonthCode >= m1 AND Month.MonthCode <= m2 NO-LOCK:
    set-list = set-list + (IF set-list = "" THEN "" ELSE ",").
    CASE INPUT RP.Char3:
      WHEN "Budget" THEN                set-list = set-list + "B".
      WHEN "Revised Budget" THEN        set-list = set-list + "R".
      WHEN "Actual" THEN                set-list = set-list + "A".
      WHEN "Variance (Budget)" THEN     set-list = set-list + "V".
      WHEN "Variance (Revised)" THEN    set-list = set-list + "X".
    END CASE.
  END.

  IF CAN-FIND(FIRST MthFigs) THEN DO:
    FOR EACH MthFigs:   DELETE MthFigs. END.
  END.

  OPEN QUERY br-mths FOR EACH MthFigs.

  br-mths:BGCOLOR = 8.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE data-type-changed V-table-Win 
PROCEDURE data-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Char3  <> "Individual" THEN
    RUN close-mths-query.
  ELSE DO:
    RUN open-mths-query.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-entity-name V-table-Win 
PROCEDURE get-entity-name :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER et AS CHAR NO-UNDO.
DEF INPUT PARAMETER ec AS INT NO-UNDO.
DEF OUTPUT PARAMETER o-entityname AS CHAR NO-UNDO.

  CASE et :
    WHEN "L" THEN DO:
      FIND Company WHERE Company.CompanyCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Company) THEN o-EntityName = Company.LegalName.
                            ELSE o-EntityName = "* * * Company not on file * * *".
    END.
    WHEN "P" THEN DO:
      FIND Property WHERE Property.PropertyCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Property) THEN o-EntityName = Property.Name.
                             ELSE o-EntityName = "* * * Property not on file * * *".
    END.
    WHEN "J" THEN DO:
      FIND Project WHERE Project.ProjectCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Project) THEN o-EntityName = Project.Name.
                            ELSE o-EntityName = "* * * Project not on file * * *".
    END.
    WHEN "F" THEN DO:
      FIND FixedAsset WHERE FixedAsset.AssetCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(FixedAsset) THEN o-EntityName = FixedAsset.Description.
                            ELSE o-EntityName = "* * * Fixed Asset not on file * * *".
    END.
    /* T and C should be fairly unlikely but we'll do them anyway */
    WHEN "C" THEN DO:
      FIND Creditor WHERE Creditor.CreditorCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Creditor) THEN o-EntityName = Creditor.Name.
                             ELSE o-EntityName = "* * * Creditor not on file * * *".
    END.
    WHEN "T" THEN DO:
      FIND Tenant WHERE Tenant.TenantCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Tenant) THEN o-EntityName = Tenant.Name.
                           ELSE o-EntityName = "* * * Tenant not on file * * *".
    END.
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-month-settings V-table-Win 
PROCEDURE get-month-settings :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.

  set-list = "".
  FOR EACH MthFigs:
    i = i + 1.
    set-list = set-list + (IF i > 1 THEN "," ELSE "") + MthFigs.FigsFrom.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  FIND Month WHERE Month.StartDate = INPUT cmb_From NO-LOCK NO-ERROR.
  RP.Int4 = Month.MonthCode.
  FIND Month WHERE Month.EndDate   = INPUT cmb_To NO-LOCK NO-ERROR.
  RP.Int5 = Month.MonthCode.

  IF CAN-FIND(FIRST MthFigs) THEN RUN get-month-settings.
  RP.Char6 = set-list.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  RUN get-entity-name( INPUT RP.Char1, INPUT RP.Int2, OUTPUT fil_Entity-1 ).
  RUN get-entity-name( INPUT RP.Char1, INPUT RP.Int3, OUTPUT fil_Entity-2 ).
  DISPLAY UNLESS-HIDDEN fil_Entity-1 fil_Entity-2.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN output-type-changed.
  RUN data-type-changed.
  RUN selection-type-changed.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR month-start AS CHAR NO-UNDO.
DEF VAR month-end AS CHAR NO-UNDO.

DEF VAR user-name AS CHAR NO-UNDO.  {inc/username.i "user-name"}
&SCOP REPORT-ID "Entity Budget"

  FIND RP WHERE RP.ReportID = {&REPORT-ID} AND RP.UserName = user-name EXCLUSIVE-LOCK NO-ERROR .
  IF NOT AVAILABLE(RP) THEN DO:
    FIND LAST Month WHERE Month.StartDate <= TODAY NO-LOCK NO-ERROR.
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Char1 = "P"
      RP.Char2 = "D"
      RP.Int1 = (IF AVAILABLE(Month) THEN Month.FinancialYearCode ELSE 1998)
      RP.Int4 = (IF AVAILABLE(Month) THEN Month.MonthCode ELSE 0).
      RP.Int5 = RP.Int4 + 11.
    .
  END.

DO WITH FRAME {&FRAME-NAME}:
  FOR EACH Month NO-LOCK:
    month-start = month-start + "," + STRING( Month.StartDate, "99/99/9999").
    month-end   = month-end   + "," + STRING( Month.EndDate, "99/99/9999").
  END.
  cmb_From:LIST-ITEMS = SUBSTRING( month-start, 2).
  cmb_To:LIST-ITEMS   = SUBSTRING( month-end, 2).

  FIND Month WHERE Month.MonthCode = RP.Int4 NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Month) THEN FIND LAST Month WHERE Month.StartDate <= TODAY NO-LOCK NO-ERROR.
  cmb_From = (IF AVAILABLE(Month) THEN Month.StartDate ELSE DATE(cmb_From:ENTRY(1)) ).

  FIND Month WHERE Month.MonthCode = RP.Int5 NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Month) THEN FIND FIRST Month WHERE Month.EndDate >= (TODAY + 337) NO-LOCK NO-ERROR.
  cmb_To = (IF AVAILABLE(Month) THEN Month.EndDate ELSE DATE( cmb_To:ENTRY( cmb_To:NUM-ENTRIES ) ) ).

  set-list = RP.Char6.
END.

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'disable-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-mths-query V-table-Win 
PROCEDURE open-mths-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR no-set AS INT NO-UNDO.

DEF VAR m1 AS INT NO-UNDO.
DEF VAR m2 AS INT NO-UNDO.

  IF CAN-FIND( FIRST MthFigs ) THEN DO:
    set-list = "".
    FOR EACH MthFigs:
      i = i + 1.
      set-list = set-list + (IF i > 1 THEN "," ELSE "") + MthFigs.FigsFrom.
      DELETE MthFigs.
    END.
    no-set = i.
  END.
  ELSE
    no-set = NUM-ENTRIES( set-list ).

DO WITH FRAME {&FRAME-NAME}:
  ENABLE br-mths.
  br-mths:BGCOLOR = ?.
  IF INPUT RP.Char4 = "P" THEN DO:
    FIND FIRST Month WHERE Month.FinancialYearCode = INPUT RP.Int1 NO-LOCK.
    m1 = Month.MonthCode.
    FIND LAST Month WHERE Month.FinancialYearCode = INPUT RP.Int1 NO-LOCK.
    m2 = Month.MonthCode.
  END.
  ELSE DO:
    FIND FIRST Month WHERE Month.StartDate >= INPUT cmb_From NO-LOCK.
    m1 = Month.MonthCode.
    FIND LAST Month WHERE Month.EndDate <= INPUT cmb_To NO-LOCK.
    m2 = Month.MonthCode.
  END.
END.

  i = 0.
  FOR EACH Month WHERE Month.MonthCode >= m1 AND Month.MonthCode <= m2 NO-LOCK:
    i = i + 1.
    CREATE MthFigs.
    ASSIGN  MthFigs.MonthCode = Month.MonthCode
            MthFigs.StartDate = STRING( Month.StartDate, "99/99/9999")
            MthFigs.EndDate = STRING( Month.EndDate, "99/99/9999")
            MthFigs.FigsFrom = (IF i <= no-set THEN ENTRY( i, set-list ) ELSE 'R').
    IF   MthFigs.FigsFrom = 'R'    THEN MthFigs.Description = 'Revised budget'.
    ELSE IF MthFigs.FigsFrom = 'B' THEN MthFigs.Description = 'Budget'.
    ELSE IF MthFigs.FigsFrom = 'A' THEN MthFigs.Description = 'Actual'.
    ELSE IF MthFigs.FigsFrom = 'V' THEN MthFigs.Description = 'Variance from Budget'.
    ELSE IF MthFigs.FigsFrom = 'X' THEN MthFigs.Description = 'Variance from Revised Budget'.
  END.
  OPEN QUERY br-mths FOR EACH MthFigs.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE output-type-changed V-table-Win 
PROCEDURE output-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Char4 = "P" THEN DO:
    HIDE RP.Char5 btn_Browse cmb_From cmb_To .
    VIEW RP.Int1 fil_Year RP.Log2 .
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Year:HANDLE ), "HIDDEN = No" ).
  END.
  ELSE DO:
    HIDE RP.Int1 fil_Year RP.Log2 .
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Year:HANDLE ), "HIDDEN = Yes" ).
    VIEW RP.Char5 btn_Browse cmb_From cmb_To .
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  IF TRIM( fil_Year:SCREEN-VALUE ) = "" THEN DO:
    APPLY "ENTRY":U TO RP.Int1.
    RETURN "FAIL".
  END.
  RUN dispatch( 'update-record':U ).

  report-options = "FinancialYear," + STRING( RP.Int1 )
                 + (IF RP.Log3 THEN
                        "~nEntityRange," + RP.Char1 + "," + STRING( RP.Int2 ) + "," + STRING( RP.Int3 )
                    ELSE
                        "~nEntityList," + RP.Char2 )
                 + (IF RP.Log1 THEN "~nSummary" ELSE "")
                 + "~nReportOn," + RP.Char3 + "," + set-list
                 + (IF RP.Log2 THEN "~nPreview" ELSE "")
                 + (IF RP.Char4 = "F" THEN "~nExport," + RP.Char5 ELSE "")
                 + "~nMonthRange," + STRING( RP.Int4 ) + "," + STRING( RP.Int5 ).

  {inc/bq-do.i "process/report/budgets.p" "report-options" "NOT RP.Log2" }
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char5 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated values" "*.CSV"
        SAVE-AS ASK-OVERWRITE CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV"
        INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME UPDATE select-ok.

  IF select-ok THEN DO:
    IF SUBSTRING( file-name, R-INDEX( file-name, ".") ) <> ".CSV" THEN DO:
      IF (LENGTH( file-name ) - R-INDEX( file-name, "." )) < 4 THEN
        file-name = SUBSTRING( file-name, 1, R-INDEX( file-name, ".")) + "CSV" .
    END.
    RP.Char5:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selection-type-changed V-table-Win 
PROCEDURE selection-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Log3 THEN DO:
    HIDE cmb_EntityList .
    VIEW RP.Char1 RP.Int2 RP.Int3 fil_Entity-1 fil_Entity-2.
  END.
  ELSE DO:
    HIDE RP.Char1 RP.Int2 RP.Int3 fil_Entity-1 fil_Entity-2.
    VIEW cmb_EntityList .
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}
  {src/adm/template/snd-list.i "MthFigs"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-row-description V-table-Win 
PROCEDURE set-row-description :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT BROWSE {&BROWSE-NAME} MthFigs.FigsFrom = 'R'    THEN
    ASSIGN MthFigs.Description:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = 'Revised budget' NO-ERROR.
  ELSE IF INPUT BROWSE {&BROWSE-NAME} MthFigs.FigsFrom = 'B' THEN
    ASSIGN MthFigs.Description:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = 'Budget' NO-ERROR.
  ELSE IF INPUT BROWSE {&BROWSE-NAME} MthFigs.FigsFrom = 'A' THEN
    ASSIGN MthFigs.Description:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = 'Actual' NO-ERROR.
  ELSE IF INPUT BROWSE {&BROWSE-NAME} MthFigs.FigsFrom = 'V' THEN
    ASSIGN MthFigs.Description:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = 'Variance from Budget' NO-ERROR.
  ELSE IF INPUT BROWSE {&BROWSE-NAME} MthFigs.FigsFrom = 'X' THEN
    ASSIGN MthFigs.Description:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = 'Variance from Revised Budget' NO-ERROR.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


