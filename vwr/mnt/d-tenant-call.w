&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:         vwr/mnt/d-tenant-call.w
  Description:  Dialog to handle entry of tenant call details
  Author:       Andrew McMillan
  Created:      21 May 2001
------------------------------------------------------------------------*/

/* Parameters Definitions ---                                           */
DEF INPUT PARAMETER in-call-number LIKE TenantCall.CallNumber NO-UNDO.

/* Local Variable Definitions ---                                       */
DEF VAR new-call-number LIKE TenantCall.CallNumber NO-UNDO.
DEF VAR hh AS INTEGER NO-UNDO.
DEF VAR mm AS INTEGER NO-UNDO.
DEF VAR ss AS INTEGER NO-UNDO.

DEF VAR original-property AS INT NO-UNDO INITIAL ?.
DEF VAR current-property AS INT NO-UNDO INITIAL ?.
DEF VAR current-tenant   AS INT NO-UNDO INITIAL ?.
DEF VAR set-pdf-output AS LOG NO-UNDO INITIAL NO.

/* These allow different procedures outside "build-<X>-list" to modify dropdown lists */
DEF VAR public-list-creditor AS CHAR NO-UNDO INITIAL ''.
DEF VAR private-list-creditor AS CHAR NO-UNDO INITIAL ''.
DEF VAR public-list-tenant AS CHAR NO-UNDO INITIAL ''.
DEF VAR private-list-tenant AS CHAR NO-UNDO INITIAL ''.

DEFINE BUFFER b-TenantCall FOR TenantCall.

{inc/ofc-this.i}
{inc/ofc-set.i "TenantCall-Creditors" "standard-creditors"}
IF NOT AVAILABLE(OfficeSetting) THEN standard-creditors = "".

{inc/ofc-set.i "TenantCall-Tenants" "standard-tenants"}
IF NOT AVAILABLE(OfficeSetting) THEN standard-tenants = "".

DEFINE VARIABLE v-username AS CHARACTER INITIAL '' NO-UNDO.
{inc/username.i "v-username"}

DEF VAR logged-by AS CHARACTER NO-UNDO.
DEF VAR creditor-email-address AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE DIALOG-BOX
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME Dialog-Frame

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES TenantCall

/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define FIELDS-IN-QUERY-Dialog-Frame TenantCall.ContactName ~
TenantCall.ContactPhone TenantCall.Description TenantCall.OrderAmount ~
TenantCall.CallNumber TenantCall.DateOfCall TenantCall.Level ~
TenantCall.Problem TenantCall.Action 
&Scoped-define ENABLED-FIELDS-IN-QUERY-Dialog-Frame TenantCall.ContactName ~
TenantCall.ContactPhone TenantCall.Description TenantCall.OrderAmount ~
TenantCall.CallNumber TenantCall.DateOfCall TenantCall.Level ~
TenantCall.Problem TenantCall.Action 
&Scoped-define ENABLED-TABLES-IN-QUERY-Dialog-Frame TenantCall
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-Dialog-Frame TenantCall
&Scoped-define QUERY-STRING-Dialog-Frame FOR EACH TenantCall SHARE-LOCK
&Scoped-define OPEN-QUERY-Dialog-Frame OPEN QUERY Dialog-Frame FOR EACH TenantCall SHARE-LOCK.
&Scoped-define TABLES-IN-QUERY-Dialog-Frame TenantCall
&Scoped-define FIRST-TABLE-IN-QUERY-Dialog-Frame TenantCall


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS TenantCall.ContactName ~
TenantCall.ContactPhone TenantCall.Description TenantCall.OrderAmount ~
TenantCall.CallNumber TenantCall.DateOfCall TenantCall.Level ~
TenantCall.Problem TenantCall.Action 
&Scoped-define ENABLED-TABLES TenantCall
&Scoped-define FIRST-ENABLED-TABLE TenantCall
&Scoped-Define ENABLED-OBJECTS cbx_EmailAddr fil_LoggedBy disp-call-time ~
inp_Property cmb_Tenant cmb_Creditor Btn_OK Btn_Cancel Btn_CallClosed ~
cmb_Status cmb_Category cmb_Priority Btn_PrintOrder ui-email-address 
&Scoped-Define DISPLAYED-FIELDS TenantCall.ContactName ~
TenantCall.ContactPhone TenantCall.Description TenantCall.OrderAmount ~
TenantCall.CallNumber TenantCall.DateOfCall TenantCall.Level ~
TenantCall.Problem TenantCall.Action 
&Scoped-define DISPLAYED-TABLES TenantCall
&Scoped-define FIRST-DISPLAYED-TABLE TenantCall
&Scoped-Define DISPLAYED-OBJECTS cbx_EmailAddr fil_LoggedBy fil_OrderNo ~
disp-call-time inp_Property cmb_Tenant cmb_Creditor fil_Closed cmb_Status ~
cmb_Category cmb_Priority ui-email-address 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-next-order Dialog-Frame 
FUNCTION get-next-order RETURNS INTEGER
  ( INPUT et AS CHAR, INPUT ec AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD time2int Dialog-Frame 
FUNCTION time2int RETURNS INTEGER
  ( INPUT tm AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_CallClosed AUTO-END-KEY 
     LABEL "Mark Call Closed" 
     SIZE 13 BY 1.15.

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel Changes" 
     SIZE 13 BY 1.15.

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 13 BY 1.15.

DEFINE BUTTON Btn_PrintOrder AUTO-END-KEY 
     LABEL "Print Order" 
     SIZE 13 BY 1.15.

DEFINE VARIABLE cmb_Category AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 26 BY 1.05 NO-UNDO.

DEFINE VARIABLE cmb_Creditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Creditor" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 32 BY 1.05 NO-UNDO.

DEFINE VARIABLE cmb_Priority AS CHARACTER FORMAT "X(256)":U 
     LABEL "Job Priority" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Status AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Tenant AS CHARACTER FORMAT "X(256)":U 
     LABEL "Tenant" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 39 BY 1 NO-UNDO.

DEFINE VARIABLE disp-call-time AS CHARACTER FORMAT "X(5)" INITIAL "0" 
     LABEL "Call Time" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Closed AS CHARACTER FORMAT "X(256)":U 
     LABEL "Closed" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_LoggedBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Logged By" 
      VIEW-AS TEXT 
     SIZE 35 BY .8 NO-UNDO.

DEFINE VARIABLE fil_OrderNo AS CHARACTER FORMAT "X(256)" INITIAL "0" 
     LABEL "Order No" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1.05 NO-UNDO.

DEFINE VARIABLE inp_Property AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "Property" 
     VIEW-AS FILL-IN 
     SIZE 6.43 BY 1 NO-UNDO.

DEFINE VARIABLE ui-email-address AS CHARACTER FORMAT "X(50)" 
     LABEL "Creditor email" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE cbx_EmailAddr AS LOGICAL INITIAL no 
     LABEL "Email the order" 
     VIEW-AS TOGGLE-BOX
     SIZE 22 BY .8 TOOLTIP "Email the Building Manager of this Facility outlining Location and Problem." NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY Dialog-Frame FOR 
      TenantCall SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbx_EmailAddr AT ROW 21.7 COL 48
     fil_LoggedBy AT ROW 11.85 COL 8.14 COLON-ALIGNED NO-TAB-STOP 
     TenantCall.ContactName AT ROW 9.2 COL 8 COLON-ALIGNED
          LABEL "Contact"
          VIEW-AS FILL-IN 
          SIZE 26 BY 1.05
     TenantCall.ContactPhone AT ROW 9.2 COL 41 COLON-ALIGNED
          LABEL "Phone"
          VIEW-AS FILL-IN 
          SIZE 32 BY 1.05
     TenantCall.Description AT ROW 4.25 COL 8 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 65 BY 1.05
     fil_OrderNo AT ROW 1.25 COL 63 COLON-ALIGNED
     TenantCall.OrderAmount AT ROW 17.45 COL 54 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 19 BY 1.05
     TenantCall.CallNumber AT ROW 1.25 COL 8 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9 BY 1
     TenantCall.DateOfCall AT ROW 1.25 COL 27 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     disp-call-time AT ROW 1.25 COL 48 COLON-ALIGNED
     inp_Property AT ROW 3 COL 8 COLON-ALIGNED
     TenantCall.Level AT ROW 3 COL 21 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 4 BY 1
     cmb_Tenant AT ROW 3 COL 34 COLON-ALIGNED
     TenantCall.Problem AT ROW 5.5 COL 10 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 65 BY 3.5
     cmb_Creditor AT ROW 10.45 COL 41 COLON-ALIGNED
     TenantCall.Action AT ROW 13 COL 10 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 65 BY 4.25
     fil_Closed AT ROW 18.7 COL 8 COLON-ALIGNED
     Btn_OK AT ROW 21.5 COL 2
     Btn_Cancel AT ROW 21.5 COL 16
     Btn_CallClosed AT ROW 21.5 COL 30
     cmb_Status AT ROW 17.45 COL 8 COLON-ALIGNED
     cmb_Category AT ROW 10.45 COL 8 COLON-ALIGNED
     cmb_Priority AT ROW 18.7 COL 54 COLON-ALIGNED
     Btn_PrintOrder AT ROW 21.5 COL 62
     ui-email-address AT ROW 20 COL 38 COLON-ALIGNED
     "Problem:" VIEW-AS TEXT
          SIZE 6 BY 1.25 AT ROW 5.5 COL 9 RIGHT-ALIGNED
     "Action:" VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 13 COL 5
     SPACE(65.71) SKIP(8.65)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 10
         TITLE "Tenant Call Details".


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: DIALOG-BOX
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   Custom                                                               */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR EDITOR TenantCall.Action IN FRAME Dialog-Frame
   EXP-LABEL                                                            */
ASSIGN 
       TenantCall.Action:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN TenantCall.ContactName IN FRAME Dialog-Frame
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN TenantCall.ContactPhone IN FRAME Dialog-Frame
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Closed IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_OrderNo IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR TenantCall.Problem IN FRAME Dialog-Frame
   EXP-LABEL                                                            */
ASSIGN 
       TenantCall.Problem:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR TEXT-LITERAL "Problem:"
          SIZE 6 BY 1.25 AT ROW 5.5 COL 9 RIGHT-ALIGNED                 */

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX Dialog-Frame
/* Query rebuild information for DIALOG-BOX Dialog-Frame
     _TblList          = "TTPL.TenantCall"
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX Dialog-Frame */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON END-ERROR OF FRAME Dialog-Frame /* Tenant Call Details */
DO:
  RUN cancel-and-close.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON ENTRY OF FRAME Dialog-Frame /* Tenant Call Details */
DO:
  FIND CURRENT TenantCall.

  /* If TenantCall.Action has been updated in the backgroud redisplay */
  IF TenantCall.Action <> b-TenantCall.Action THEN
    FIND CURRENT TenantCall.
    DISPLAY TenantCall.Action WITH FRAME Dialog-Frame.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Tenant Call Details */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_CallClosed
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_CallClosed Dialog-Frame
ON CHOOSE OF Btn_CallClosed IN FRAME Dialog-Frame /* Mark Call Closed */
DO:
  RUN mark-as-closed.
  RUN save-and-close.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel Changes */
DO:
  RUN cancel-and-close.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  RUN save-and-close.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_PrintOrder
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_PrintOrder Dialog-Frame
ON CHOOSE OF Btn_PrintOrder IN FRAME Dialog-Frame /* Print Order */
DO:
/*  IF INPUT cbx_PDFEmail THEN
    RUN get-email-address. */
  RUN order-and-close.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbx_EmailAddr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbx_EmailAddr Dialog-Frame
ON VALUE-CHANGED OF cbx_EmailAddr IN FRAME Dialog-Frame /* Email the order */
DO:
DO WITH FRAME {&FRAME-NAME}:
  IF SELF:MODIFIED THEN DO:
    RUN toggle-email-field.
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Category
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Category Dialog-Frame
ON VALUE-CHANGED OF cmb_Category IN FRAME Dialog-Frame /* Category */
DO:
DO WITH FRAME {&FRAME-NAME}:
  IF SELF:MODIFIED THEN DO:
    RUN find-last-creditor.
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Creditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Creditor Dialog-Frame
ON VALUE-CHANGED OF cmb_Creditor IN FRAME Dialog-Frame /* Creditor */
DO:
DO WITH FRAME {&FRAME-NAME}:
  IF SELF:MODIFIED THEN DO:
    RUN find-creditor-email.
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Tenant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Tenant Dialog-Frame
ON VALUE-CHANGED OF cmb_Tenant IN FRAME Dialog-Frame /* Tenant */
DO:
DO WITH FRAME {&FRAME-NAME}:
  IF SELF:MODIFIED THEN DO:
    RUN find-property-for-tenant.
    RUN build-creditor-list.
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME inp_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL inp_Property Dialog-Frame
ON VALUE-CHANGED OF inp_Property IN FRAME Dialog-Frame /* Property */
DO:
DO WITH FRAME {&FRAME-NAME}:
  IF SELF:MODIFIED THEN DO:
    RUN build-tenant-list.
    RUN build-creditor-list.
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

IF in-call-number > 0 THEN DO:
  FIND TenantCall WHERE TenantCall.CallNumber = in-call-number NO-LOCK.
  original-property = TenantCall.PropertyCode.
  
  FIND b-TenantCall WHERE b-TenantCall.CallNumber = in-call-number NO-LOCK.

  FIND FIRST Person WHERE Person.PersonCode = TenantCall.LoggedBy NO-LOCK NO-ERROR.
  IF AVAILABLE Person THEN
      logged-by = Person.FirstName + ' ' + Person.LastName.
END.
ELSE DO:
  FIND LAST TenantCall NO-LOCK NO-ERROR.
  IF AVAILABLE TenantCall THEN
    new-call-number = TenantCall.CallNumber + 1.
  ELSE
    new-call-number = 1.
  DO TRANSACTION:
    CREATE TenantCall.
    TenantCall.CallNumber = new-call-number.
    TenantCall.DateOfCall = TODAY.
    TenantCall.TimeOfCall = TIME.
    TenantCall.CallStatusCode = "Active".
    FIND FIRST Usr WHERE Usr.UserName = v-username NO-LOCK NO-ERROR.
    IF AVAILABLE Usr THEN
        TenantCall.LoggedBy = Usr.PersonCode.
    FIND CURRENT TenantCall SHARE-LOCK.
  END.
  FIND b-TenantCall WHERE b-TenantCall.CallNumber = new-call-number NO-LOCK.

  /* Populate the tenant list with all tenants initially. Then set the property value to match the first one */
  RUN build-tenant-list.
END.

disp-call-time = STRING( TenantCall.TimeOfCall, "HH:MM" ).
inp_Property = TenantCall.PropertyCode.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE adm-enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbx_EmailAddr fil_LoggedBy fil_OrderNo disp-call-time inp_Property 
          cmb_Tenant cmb_Creditor fil_Closed cmb_Status cmb_Category 
          cmb_Priority ui-email-address 
      WITH FRAME Dialog-Frame.
  IF AVAILABLE TenantCall THEN 
    DISPLAY TenantCall.ContactName TenantCall.ContactPhone TenantCall.Description 
          TenantCall.OrderAmount TenantCall.CallNumber TenantCall.DateOfCall 
          TenantCall.Level TenantCall.Problem TenantCall.Action 
      WITH FRAME Dialog-Frame.
  ENABLE cbx_EmailAddr fil_LoggedBy TenantCall.ContactName 
         TenantCall.ContactPhone TenantCall.Description TenantCall.OrderAmount 
         TenantCall.CallNumber TenantCall.DateOfCall disp-call-time 
         inp_Property TenantCall.Level cmb_Tenant TenantCall.Problem 
         cmb_Creditor TenantCall.Action Btn_OK Btn_Cancel Btn_CallClosed 
         cmb_Status cmb_Category cmb_Priority Btn_PrintOrder ui-email-address 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-category-list Dialog-Frame 
PROCEDURE build-category-list :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR public-list AS CHAR NO-UNDO INITIAL ''.
DEF VAR private-list AS CHAR NO-UNDO INITIAL ''.
DEF VAR currval AS INT NO-UNDO INITIAL 0.

DO WITH FRAME {&FRAME-NAME}:
  FOR EACH ServiceType NO-LOCK:
    public-list = public-list + (IF public-list <> '' THEN '|' ELSE '')
                + ServiceType.Description .
    private-list = private-list + (IF private-list <> '' THEN '|' ELSE '')
                + ServiceType.ServiceType .
  END.
  public-list = public-list + "|Accounting Query".
  private-list = private-list + "|ACCT".

  cmb_Category:DELIMITER = '|'.
  cmb_Category:LIST-ITEMS = public-list.
  cmb_Category:PRIVATE-DATA = private-list.
  currval = LOOKUP( TenantCall.CallCategoryCode, private-list, '|' ).
  IF NOT(currval > 0) THEN currval = 1. 
  cmb_Category = ENTRY( currval, public-list, '|' ).
  
  DISPLAY cmb_Category WITH FRAME {&FRAME-NAME}.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-creditor-list Dialog-Frame 
PROCEDURE build-creditor-list :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
/*DEF VAR public-list AS CHAR NO-UNDO INITIAL ''.
DEF VAR private-list AS CHAR NO-UNDO INITIAL ''.*/
DEF VAR currval AS INT NO-UNDO INITIAL 0.

DO WITH FRAME {&FRAME-NAME}:
  current-property = INPUT FRAME {&FRAME-NAME} inp_Property .

  public-list-creditor = ''.
  private-list-creditor = standard-creditors.
  
  /* Build up names of standard creditors first */
  DO currval = 1 TO NUM-ENTRIES(private-list-creditor):
    FIND Creditor WHERE Creditor.CreditorCode = INT(ENTRY(currval,private-list-creditor)) NO-LOCK NO-ERROR.
    IF AVAILABLE(Creditor) THEN
      public-list-creditor = public-list-creditor + (IF public-list-creditor <> '' THEN '|' ELSE '')
                  + (IF AVAILABLE(Creditor) THEN Creditor.Name ELSE '<missing>')
                  + ' (C' + TRIM(STRING(Creditor.CreditorCode, '>>>>>9')) + ')'.
  END.
  
  /* Now add in any service contractors for this building who aren't there already */
  FOR EACH Contract WHERE Contract.PropertyCode = current-property
              AND Contract.Active NO-LOCK,
      FIRST Creditor WHERE Creditor.CreditorCode = Contract.CreditorCode NO-LOCK
      BY Creditor.Name:
    IF NOT CAN-DO( private-list-creditor, TRIM(STRING(Contract.CreditorCode, '>>>>>9')) ) THEN DO:
      public-list-creditor = public-list-creditor + (IF public-list-creditor <> '' THEN '|' ELSE '')
                  + Creditor.Name + ' (C' + TRIM(STRING(Contract.CreditorCode, '>>>>>9')) + ')'.
      private-list-creditor = private-list-creditor + (IF private-list-creditor <> '' THEN ',' ELSE '')
                  + TRIM(STRING(Contract.CreditorCode,'>>>>>9')).
    END.
  END.
  cmb_Creditor:DELIMITER = '|'.
  cmb_Creditor:LIST-ITEMS = public-list-creditor.
  cmb_Creditor:PRIVATE-DATA = private-list-creditor.
  currval = LOOKUP( TRIM(STRING(TenantCall.CreditorCode, '>>>>>9')), private-list-creditor ).
  IF NOT(currval > 0) THEN currval = 1.

  cmb_Creditor = ENTRY( currval, public-list-creditor, '|' ).
  DISPLAY cmb_Creditor WITH FRAME {&FRAME-NAME}.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-priority-list Dialog-Frame 
PROCEDURE build-priority-list :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR public-list AS CHAR NO-UNDO INITIAL ''.
DEF VAR private-list AS CHAR NO-UNDO INITIAL ''.
DEF VAR currval AS INT NO-UNDO INITIAL 0.

DO WITH FRAME {&FRAME-NAME}:
  FOR EACH LookupCode WHERE LookupCode._File-Name = 'TenantCall'
                      AND LookupCode._Field-Name = 'Priority' NO-LOCK:
    public-list = public-list + (IF public-list <> '' THEN '|' ELSE '')
                + LookupCode.Description .
    private-list = private-list + (IF private-list <> '' THEN '|' ELSE '')
                + LookupCode.LookupCode .
  END.
  cmb_Priority:DELIMITER = '|'.
  cmb_Priority:LIST-ITEMS = public-list.
  cmb_Priority:PRIVATE-DATA = private-list.
  currval = LOOKUP( TenantCall.Priority, private-list, '|' ).
  IF NOT(currval > 0) THEN currval = 1. 
  cmb_Priority = ENTRY( currval, public-list, '|' ).
  
  DISPLAY cmb_Priority WITH FRAME {&FRAME-NAME}.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-status-list Dialog-Frame 
PROCEDURE build-status-list :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR public-list AS CHAR NO-UNDO INITIAL ''.
DEF VAR currval AS INT NO-UNDO INITIAL 0.

DO WITH FRAME {&FRAME-NAME}:
  public-list = ''.
  FOR EACH LookupCode WHERE LookupCode._File-Name = 'TenantCall'
                      AND LookupCode._Field-Name = 'CallStatusCode' NO-LOCK:
    public-list = public-list + (IF public-list <> '' THEN '|' ELSE '')
                + LookupCode.LookupCode .
  END.
  cmb_Status:DELIMITER = '|'.
  cmb_Status:LIST-ITEMS = public-list.
  cmb_Status:PRIVATE-DATA = public-list.
  currval = LOOKUP( TenantCall.CallStatusCode, public-list, '|' ).
  IF NOT(currval > 0) THEN currval = 1. 
  cmb_Status = ENTRY( currval, public-list, '|' ).
  
  DISPLAY cmb_Status WITH FRAME {&FRAME-NAME}.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-tenant-list Dialog-Frame 
PROCEDURE build-tenant-list :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
/*DEF VAR public-list AS CHAR NO-UNDO INITIAL ''.
DEF VAR private-list AS CHAR NO-UNDO INITIAL ''.*/
DEF VAR last-entry AS CHAR NO-UNDO INITIAL ''.
DEF VAR this-entry AS CHAR NO-UNDO INITIAL ''.
DEF VAR currval AS INT NO-UNDO INITIAL 0.

DO WITH FRAME {&FRAME-NAME}:
  private-list-tenant = ''.
  public-list-tenant = ''.

  current-property = INPUT FRAME {&FRAME-NAME} inp_Property .
  IF current-property = ? OR current-property = 0 THEN DO:
    FOR EACH Tenant WHERE Tenant.EntityType = 'P' AND Tenant.Active NO-LOCK BY Tenant.Name:
      public-list-tenant = public-list-tenant + (IF public-list-tenant <> '' THEN '|' ELSE '')
                  + Tenant.Name + ' (T' + TRIM(STRING(Tenant.TenantCode, '>>>>9')) + ')' .
      private-list-tenant = private-list-tenant + (IF private-list-tenant <> '' THEN '|' ELSE '')
                  + TRIM(STRING(Tenant.TenantCode,'>>>>>9')).
    END.
  END.
  ELSE DO:
    /* First a list of tenants that always appear regardless of the property selected */
    IF standard-tenants <> "" THEN DO:
      DO currval = 1 to NUM-ENTRIES( standard-tenants ):
        FIND FIRST Tenant WHERE Tenant.TenantCode = INTEGER( ENTRY( currval, standard-tenants ) ) NO-ERROR.
        IF AVAILABLE( Tenant ) THEN DO:
          public-list-tenant = public-list-tenant + (IF public-list-tenant <> '' THEN '|' ELSE '')
                      + Tenant.Name + ' (T' + TRIM(STRING(Tenant.TenantCode, '>>>>9')) + ')'.
          private-list-tenant = private-list-tenant + (IF private-list-tenant <> '' THEN '|' ELSE '')
                      + TRIM( STRING( Tenant.TenantCode, '>>>>>9' ) ).
        END.
      END.
    END.
    /* Then the property tenants */
    FOR EACH Tenant WHERE Tenant.EntityType = 'P' AND Tenant.EntityCode = current-property
                AND Tenant.Active NO-LOCK
                BY Tenant.Name:
      public-list-tenant = public-list-tenant + (IF public-list-tenant <> '' THEN '|' ELSE '')
                  + Tenant.Name + ' (T' + TRIM(STRING(Tenant.TenantCode, '>>>>9')) + ')' .
      private-list-tenant = private-list-tenant + (IF private-list-tenant <> '' THEN '|' ELSE '')
                  + TRIM(STRING(Tenant.TenantCode,'>>>>>9')).
    END.
  END.
  cmb_Tenant:DELIMITER = '|'.
  cmb_Tenant:LIST-ITEMS = public-list-tenant.
  cmb_Tenant:PRIVATE-DATA = private-list-tenant.
  currval = LOOKUP( TRIM(STRING(TenantCall.TenantCode, '>>>>>9')), private-list-tenant, '|' ).
  IF NOT(currval > 0) THEN currval = 1.

  IF current-property = ? OR current-property = 0 THEN
    cmb_Tenant = "".
  ELSE
    cmb_Tenant = ENTRY( currval, public-list-tenant, '|' ).

  DISPLAY cmb_Tenant WITH FRAME {&FRAME-NAME}.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-and-close Dialog-Frame 
PROCEDURE cancel-and-close :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF in-call-number > 0 THEN RETURN.

  DO TRANSACTION:
    FIND CURRENT TenantCall EXCLUSIVE-LOCK.
    DELETE TenantCall.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-appropriate-fields Dialog-Frame 
PROCEDURE display-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND FIRST Person WHERE Person.PersonCode = TenantCall.LoggedBy NO-LOCK NO-ERROR.
  IF AVAILABLE Person THEN
    logged-by = Person.FirstName + ' ' + Person.LastName.
  
  fil_LoggedBy:SCREEN-VALUE IN FRAME {&FRAME-NAME} = logged-by.
  cbx_EmailAddr:CHECKED IN FRAME {&FRAME-NAME} = TenantCall.SendEmail.
  /*cbx_PDFEmail:CHECKED IN FRAME {&FRAME-NAME} = set-pdf-output.*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame 
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN adm-enable_UI.
  fil_OrderNo:SCREEN-VALUE IN FRAME {&FRAME-NAME} = (IF TenantCall.OrderNo > 0 THEN STRING(TenantCall.OrderNo) ELSE '<new>').
  RUN build-status-list.
  RUN build-category-list.
  RUN build-priority-list.
  RUN build-creditor-list.
  RUN build-tenant-list.
  RUN find-creditor-email.
  RUN display-appropriate-fields.
  RUN toggle-email-field.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE find-creditor-email Dialog-Frame 
PROCEDURE find-creditor-email :
/*------------------------------------------------------------------------------
  Purpose: When changes to the creditor field are made, find and update the email
           address from the creditor record. Called from onchange of cmb_Creditor
           and when the creditor list is rebuilt.
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  FIND FIRST Creditor WHERE
    Creditor.CreditorCode = INT( ENTRY( cmb_Creditor:LOOKUP(cmb_Creditor:SCREEN-VALUE), cmb_Creditor:PRIVATE-DATA ) )
    NO-LOCK NO-ERROR.
  IF AVAILABLE Creditor AND Creditor.DcRemittanceEmail <> "" THEN
    ui-email-address = Creditor.DcRemittanceEmail.
  ELSE
    ui-email-address = "".

  DISPLAY ui-email-address WITH FRAME {&FRAME-NAME}.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE find-last-creditor Dialog-Frame 
PROCEDURE find-last-creditor :
/*------------------------------------------------------------------------------
  Purpose: Provide a history of creditor/category selections so that previously
  selected creditors for a given tenant will be automatically selected if the
  user selects that category again.
------------------------------------------------------------------------------*/

DEF VAR category-cmb-value AS CHARACTER NO-UNDO.
DEF VAR property-int-value AS INTEGER NO-UNDO.
DEF VAR currval AS INTEGER NO-UNDO.
DEF BUFFER TCFind FOR TenantCall.

DO WITH FRAME {&FRAME-NAME}:
  category-cmb-value = ENTRY(
    cmb_Category:LOOKUP( cmb_Category:SCREEN-VALUE),
    cmb_Category:PRIVATE-DATA,
    cmb_Category:DELIMITER
  ).
  property-int-value = INPUT FRAME {&FRAME-NAME} inp_Property.

  /* Find the last record for this tenant that used this category code */
  FIND LAST TCFind WHERE TCFind.CallCategoryCode = category-cmb-value
    AND TCFind.PropertyCode = property-int-value NO-LOCK NO-ERROR.

  IF AVAILABLE TCFind THEN DO:
    IF TCFind.CreditorCode <> 0 AND TCFind.CreditorCode <> ? THEN DO:
      /* Set the dropdown box to equal this value, but dont change the DB yet */
      currval = LOOKUP( TRIM(STRING(TCFind.CreditorCode, '>>>>>9')), private-list-creditor ).
      IF NOT(currval > 0) THEN currval = 1. 
      cmb_Creditor = ENTRY( currval, public-list-creditor, '|' ).
      DISPLAY cmb_Creditor WITH FRAME {&FRAME-NAME}.

      RUN find-creditor-email.
    END.
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE find-property-for-tenant Dialog-Frame 
PROCEDURE find-property-for-tenant :
/*------------------------------------------------------------------------------
  Purpose: Find the property code from the tenant if the user changes the tenant field    
------------------------------------------------------------------------------*/

DEF VAR tenant-cmb-value AS INTEGER NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  tenant-cmb-value = INT( ENTRY( cmb_Tenant:LOOKUP(cmb_Tenant:SCREEN-VALUE), cmb_Tenant:PRIVATE-DATA, cmb_Tenant:DELIMITER )).
  FIND FIRST Tenant WHERE Tenant.EntityType = 'P' AND Tenant.TenantCode = tenant-cmb-value NO-ERROR.
  IF AVAILABLE Tenant THEN DO:
    FIND FIRST Property WHERE Property.PropertyCode = Tenant.EntityCode NO-ERROR.
    IF AVAILABLE Property THEN DO:
      /* Update the Property field on screen */
      FIND CURRENT TenantCall EXCLUSIVE-LOCK.
      inp_Property = Property.PropertyCode.
      DISPLAY inp_Property WITH FRAME {&FRAME-NAME}.

      /* Rebuild the creditor list now we have a property code */
      RUN build-creditor-list.
    END.
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE mark-as-closed Dialog-Frame 
PROCEDURE mark-as-closed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
IF TenantCall.TimeComplete <> ? THEN RETURN.

DO TRANSACTION WITH FRAME {&FRAME-NAME}:
  FIND CURRENT TenantCall EXCLUSIVE-LOCK.
  TenantCall.TimeComplete = TIME.
  TenantCall.DateComplete = TODAY.
  TenantCall.CallStatusCode = 'Closed'.
  FIND CURRENT TenantCall NO-LOCK.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE order-and-close Dialog-Frame 
PROCEDURE order-and-close :
/*------------------------------------------------------------------------------
  Purpose:     Create an order and close the dialog
------------------------------------------------------------------------------*/
DEF VAR next-order AS INT NO-UNDO INITIAL 517.
DEF VAR ac AS DEC NO-UNDO INITIAL 0.0 .

DO TRANSACTION WITH FRAME {&FRAME-NAME}:
  RUN save-and-close.
  FIND Order WHERE Order.EntityType = 'P' 
               AND Order.EntityCode = original-property
               AND Order.OrderCode = TenantCall.OrderNo
               AND Order.OrderCode > 0
               NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Order) THEN DO:
    next-order = get-next-order( 'P', TenantCall.PropertyCode ).
    FIND ServiceType WHERE ServiceType.ServiceType = TenantCall.CallCategoryCode No-LOCK NO-ERROR.
    IF AVAILABLE(ServiceType) THEN ac = ServiceType.AccountCode.
    CREATE Order.
    ASSIGN Order.EntityType = 'P'
           Order.EntityCode = TenantCall.PropertyCode
           Order.Account    = ac
           Order.ProjectCode = TenantCall.PropertyCode
           Order.OrderCode  = next-order.
    FIND CURRENT TenantCall EXCLUSIVE-LOCK.
    TenantCall.OrderNo = Order.OrderCode.
  END.
  ELSE DO:
    FIND CURRENT Order EXCLUSIVE-LOCK.
    IF TenantCall.PropertyCode <> original-property THEN DO:
      ASSIGN Order.OrderCode     = get-next-order('P', TenantCall.PropertyCode )
             Order.EntityCode    = TenantCall.PropertyCode.
      FIND CURRENT TenantCall EXCLUSIVE-LOCK.
      TenantCall.OrderNo = Order.OrderCode.
    END.
  END.
  Order.OrderAmount   = TenantCall.OrderAmount.
  Order.CreditorCode  = TenantCall.CreditorCode.
  Order.Description   = TenantCall.Problem
     + "~n~nContact: " + TenantCall.ContactName
     +   "~n  Phone: " + TenantCall.ContactPhone .
  /* Order.FirstApprover = Property.FirstApprover. */
  Order.OrderDate     = TODAY.
  FIND CURRENT Order NO-LOCK.
  FIND CURRENT TenantCall NO-LOCK.
END.

IF TenantCall.OrderNo > 0 THEN DO:
  DEF VAR report-options AS CHAR NO-UNDO INITIAL "".
  report-options = 'SupplierCopy'
                 + '~nInternalCopies,1'
                 + '~nEntity,P,' + STRING(TenantCall.PropertyCode)
                 + '~nOrders,' + STRING(TenantCall.OrderNo).

/* If the email box is checked, add the PDF and email options to the end */
IF cbx_EmailAddr:CHECKED THEN DO:
  creditor-email-address = ui-email-address:SCREEN-VALUE.
  report-options = report-options
                 + '~nOutputPDF,'
                 + '~nEmailAddress,' + creditor-email-address.
END.

  RUN process/report/orderfrm.p( report-options ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-and-close Dialog-Frame 
PROCEDURE save-and-close :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO TRANSACTION WITH FRAME {&FRAME-NAME}:
  FIND CURRENT TenantCall EXCLUSIVE-LOCK.
  ASSIGN FRAME {&FRAME-NAME} {&ENABLED-FIELDS}.
  TenantCall.TimeOfCall = time2int( INPUT FRAME {&FRAME-NAME} disp-call-time ).
  TenantCall.TenantCode = INT( ENTRY( cmb_Tenant:LOOKUP(cmb_Tenant:SCREEN-VALUE), cmb_Tenant:PRIVATE-DATA, cmb_Tenant:DELIMITER )).
  TenantCall.CreditorCode = INT( ENTRY( cmb_Creditor:LOOKUP(cmb_Creditor:SCREEN-VALUE), cmb_Creditor:PRIVATE-DATA ) ).
  TenantCall.CallStatusCode = ENTRY( cmb_Status:LOOKUP(cmb_Status:SCREEN-VALUE), cmb_Status:PRIVATE-DATA, cmb_Status:DELIMITER ).
  TenantCall.CallCategoryCode = ENTRY( cmb_Category:LOOKUP(cmb_Category:SCREEN-VALUE), cmb_Category:PRIVATE-DATA, cmb_Category:DELIMITER ).
  TenantCall.Priority = ENTRY( cmb_Priority:LOOKUP(cmb_Priority:SCREEN-VALUE), cmb_Priority:PRIVATE-DATA, cmb_Priority:DELIMITER ).
  TenantCall.PropertyCode = INPUT FRAME {&FRAME-NAME} inp_Property.
  TenantCall.SendEmail = INPUT cbx_EmailAddr.

  /* Save the email address in the creditor record */
  creditor-email-address = ui-email-address:SCREEN-VALUE.
  FIND FIRST Creditor WHERE Creditor.CreditorCode = TenantCall.CreditorCode EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Creditor AND Creditor.DcRemittanceEmail <> creditor-email-address THEN
    ASSIGN Creditor.DcRemittanceEmail = creditor-email-address.

  FIND CURRENT TenantCall NO-LOCK.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE toggle-email-field Dialog-Frame 
PROCEDURE toggle-email-field :
/*------------------------------------------------------------------------------
  Purpose: Toggle the email field to active/non-active depending on the tick in
  the email as PDF field. Just makes it clearer that the order will not be emailed
  unless the box is ticked.
------------------------------------------------------------------------------*/
/* creditor-email-address = ui-email-address:SCREEN-VALUE. */

DO WITH FRAME {&FRAME-NAME}:
  IF cbx_EmailAddr:CHECKED THEN DO:
    ui-email-address:READ-ONLY = NO.
    Btn_PrintOrder:LABEL = "Send Order".
  END.
  ELSE DO:
    ui-email-address:READ-ONLY = YES.
    Btn_PrintOrder:LABEL = "Print Order".
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-next-order Dialog-Frame 
FUNCTION get-next-order RETURNS INTEGER
  ( INPUT et AS CHAR, INPUT ec AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR next-num AS INT NO-UNDO INITIAL 0.  

  FIND LAST Order WHERE Order.EntityType = et AND Order.EntityCode = ec 
                    AND Order.OrderCode > 0 NO-LOCK NO-ERROR.
  IF AVAILABLE(Order) AND Order.OrderCode <> ? THEN next-num = Order.OrderCode.
  next-num = next-num + 1.

  RETURN next-num.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION time2int Dialog-Frame 
FUNCTION time2int RETURNS INTEGER
  ( INPUT tm AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR hh AS INT NO-UNDO.
DEF VAR mm AS INT NO-UNDO.
DEF VAR ss AS INT NO-UNDO.

  ASSIGN hh = INT(ENTRY( 1, tm, ":" )) NO-ERROR.
  ASSIGN mm = INT(ENTRY( 2, tm, ":" )) NO-ERROR.
  ASSIGN ss = INT(ENTRY( 3, tm, ":" )) NO-ERROR.
  IF ( hh = ? ) THEN hh = 0.
  IF ( mm = ? ) THEN mm = 0.
  IF ( ss = ? ) THEN ss = 0.
  
  ss = 3600 * hh + 60 * mm + ss.

  IF INDEX( tm, "p" ) > 0 THEN ss = ss + 43200 .
/*  MESSAGE "Converted" tm "to" ss. */

  RETURN ss.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

