&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tptntlse.i}

DEF VAR mode AS CHAR NO-UNDO.
DEF WORK-TABLE RSP NO-UNDO LIKE RentalSpace
  FIELD rsp-id AS ROWID.
  
DEF VAR break-1 AS CHAR FORMAT "X" INIT " " LABEL " " NO-UNDO.

DEF VAR primary-space     AS INT  NO-UNDO INIT ?.
DEF VAR record-changed    AS LOGI NO-UNDO INIT No.
DEF VAR default-unit-rate AS DEC  NO-UNDO INIT ?.
DEF VAR attached-to       AS CHAR NO-UNDO FORMAT "X(10)" LABEL "Assignment".

{inc/ofc-this.i}
{inc/ofc-set.i "Electricity-Rate" "c-default-rate"}
ASSIGN default-unit-rate = DEC( c-default-rate ) NO-ERROR.
{inc/ofc-set-l.i "Use-Rent-Charges" "use-rent-charges"}

{inc/ofc-set.i "Ratchet-Label" "ratchet-label"}
IF NOT AVAILABLE(OfficeSetting) THEN ratchet-label = "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-rntspc

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES TenancyLease
&Scoped-define FIRST-EXTERNAL-TABLE TenancyLease


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR TenancyLease.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES RSP

/* Definitions for BROWSE br-rntspc                                     */
&Scoped-define FIELDS-IN-QUERY-br-rntspc attached-to @ attached-to RSP.Level RSP.LevelSequence RSP.AreaSize RSP.ContractedRental RSP.Description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-rntspc   
&Scoped-define FIELD-PAIRS-IN-QUERY-br-rntspc
&Scoped-define SELF-NAME br-rntspc
&Scoped-define OPEN-QUERY-br-rntspc OPEN QUERY br-rntspc FOR EACH RSP NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br-rntspc RSP
&Scoped-define FIRST-TABLE-IN-QUERY-br-rntspc RSP


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-rntspc}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS TenancyLease.TenantCode ~
TenancyLease.PropertyCode TenancyLease.AreaDescription ~
TenancyLease.GrossLease TenancyLease.TaxApplies TenancyLease.HasRatchet ~
TenancyLease.RatchetClause TenancyLease.LeaseStartDate ~
TenancyLease.LeaseEndDate TenancyLease.RentStartDate ~
TenancyLease.RentEndDate TenancyLease.FirstLeaseStart ~
TenancyLease.ElectricityUnitRate TenancyLease.ReviewNoticePeriod ~
TenancyLease.RightsOfRenewal TenancyLease.RORNoticePeriod 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}TenantCode ~{&FP2}TenantCode ~{&FP3}~
 ~{&FP1}PropertyCode ~{&FP2}PropertyCode ~{&FP3}~
 ~{&FP1}AreaDescription ~{&FP2}AreaDescription ~{&FP3}~
 ~{&FP1}RatchetClause ~{&FP2}RatchetClause ~{&FP3}~
 ~{&FP1}LeaseStartDate ~{&FP2}LeaseStartDate ~{&FP3}~
 ~{&FP1}LeaseEndDate ~{&FP2}LeaseEndDate ~{&FP3}~
 ~{&FP1}RentStartDate ~{&FP2}RentStartDate ~{&FP3}~
 ~{&FP1}RentEndDate ~{&FP2}RentEndDate ~{&FP3}~
 ~{&FP1}FirstLeaseStart ~{&FP2}FirstLeaseStart ~{&FP3}~
 ~{&FP1}ElectricityUnitRate ~{&FP2}ElectricityUnitRate ~{&FP3}~
 ~{&FP1}ReviewNoticePeriod ~{&FP2}ReviewNoticePeriod ~{&FP3}~
 ~{&FP1}RightsOfRenewal ~{&FP2}RightsOfRenewal ~{&FP3}~
 ~{&FP1}RORNoticePeriod ~{&FP2}RORNoticePeriod ~{&FP3}
&Scoped-define ENABLED-TABLES TenancyLease
&Scoped-define FIRST-ENABLED-TABLE TenancyLease
&Scoped-Define ENABLED-OBJECTS RECT-30 br-rntspc btn_primary btn_remove ~
btn_add cmb_Status cmb_Type cmb_pymt 
&Scoped-Define DISPLAYED-FIELDS TenancyLease.TenancyLeaseCode ~
TenancyLease.TenantCode TenancyLease.PropertyCode ~
TenancyLease.AreaDescription TenancyLease.GrossLease ~
TenancyLease.TaxApplies TenancyLease.HasRatchet TenancyLease.RatchetClause ~
TenancyLease.LeaseStartDate TenancyLease.LeaseEndDate ~
TenancyLease.RentStartDate TenancyLease.RentEndDate ~
TenancyLease.FirstLeaseStart TenancyLease.ElectricityUnitRate ~
TenancyLease.ReviewNoticePeriod TenancyLease.RightsOfRenewal ~
TenancyLease.RORNoticePeriod 
&Scoped-Define DISPLAYED-OBJECTS fil_Tenant fil_Property cmb_Status ~
cmb_Type fil_Term cmb_pymt 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 9.72 BY 1.05
     FONT 9.

DEFINE BUTTON btn_primary 
     LABEL "Set As &Primary" 
     SIZE 11.43 BY 1.05
     FONT 9.

DEFINE BUTTON btn_remove 
     LABEL "&Remove" 
     SIZE 9.72 BY 1.05
     FONT 9.

DEFINE VARIABLE cmb_pymt AS CHARACTER FORMAT "X(256)":U 
     LABEL "Freq" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS " "
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Status AS CHARACTER FORMAT "X(50)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "","" 
     SIZE 37.14 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Type AS CHARACTER FORMAT "X(50)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "","" 
     SIZE 37.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Property AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Tenant AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 38.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Term AS CHARACTER FORMAT "X(9)":U 
     LABEL "Term" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-30
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 72 BY 20.6.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-rntspc FOR 
      RSP SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-rntspc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-rntspc V-table-Win _FREEFORM
  QUERY br-rntspc NO-LOCK DISPLAY
      attached-to @ attached-to
      RSP.Level COLUMN-LABEL 'Lvl ' FORMAT '->>>9'
      RSP.LevelSequence COLUMN-LABEL 'Seq ' FORMAT '->>9'
      RSP.AreaSize FORMAT '->,>>>,>>9.99' COLUMN-LABEL '  Area '
      RSP.ContractedRental FORMAT '->>>,>>>,>>9.99' COLUMN-LABEL 'Contracted'
      RSP.Description FORMAT 'X(40)' COLUMN-LABEL 'Description'
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 70.86 BY 7.8
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     TenancyLease.TenancyLeaseCode AT ROW 1 COL 63.57 COLON-ALIGNED
          LABEL "Code"
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1
          FONT 11
     TenancyLease.TenantCode AT ROW 1.4 COL 6.43 COLON-ALIGNED
          LABEL "Tenant" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
     fil_Tenant AT ROW 1.4 COL 17.29 COLON-ALIGNED NO-LABEL
     TenancyLease.PropertyCode AT ROW 2.4 COL 6.43 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
     fil_Property AT ROW 2.4 COL 17.29 COLON-ALIGNED NO-LABEL
     TenancyLease.AreaDescription AT ROW 3.4 COL 13.29 COLON-ALIGNED
          LABEL "Description" FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 57.14 BY 1
     br-rntspc AT ROW 5.4 COL 1.57
     btn_primary AT ROW 4.4 COL 37
     btn_remove AT ROW 4.4 COL 51.29
     btn_add AT ROW 4.4 COL 62.72
     TenancyLease.GrossLease AT ROW 13.2 COL 7.29
          VIEW-AS TOGGLE-BOX
          SIZE 12 BY 1
     TenancyLease.TaxApplies AT ROW 14.2 COL 7.29
          LABEL "GST Chargeable"
          VIEW-AS TOGGLE-BOX
          SIZE 14.29 BY 1
     cmb_Status AT ROW 13.2 COL 33.29 COLON-ALIGNED
     cmb_Type AT ROW 14.2 COL 33.29 COLON-ALIGNED
     TenancyLease.HasRatchet AT ROW 15.2 COL 7.29
          LABEL "Ratchet applies, put any notes here"
          VIEW-AS TOGGLE-BOX
          SIZE 28 BY 1
     TenancyLease.RatchetClause AT ROW 15.2 COL 34
          LABEL "." FORMAT "X(50)"
          VIEW-AS FILL-IN 
          SIZE 37.14 BY 1
     TenancyLease.LeaseStartDate AT ROW 16.6 COL 13.29 COLON-ALIGNED
          LABEL "Lease Start"
          VIEW-AS FILL-IN 
          SIZE 10.86 BY 1
     TenancyLease.LeaseEndDate AT ROW 16.6 COL 33.29 COLON-ALIGNED
          LABEL "End"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     fil_Term AT ROW 16.6 COL 50.43 COLON-ALIGNED
     TenancyLease.RentStartDate AT ROW 17.6 COL 13.29 COLON-ALIGNED
          LABEL "Rent Start"
          VIEW-AS FILL-IN 
          SIZE 10.86 BY 1
     TenancyLease.RentEndDate AT ROW 17.6 COL 33.29 COLON-ALIGNED HELP
          ""
          LABEL "End"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     cmb_pymt AT ROW 17.6 COL 50.43 COLON-ALIGNED
     TenancyLease.FirstLeaseStart AT ROW 18.6 COL 13.29 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 10.86 BY 1
     TenancyLease.ElectricityUnitRate AT ROW 19.6 COL 13.29 COLON-ALIGNED
          LABEL "Electricity Rate" FORMAT ">>9.9999"
          VIEW-AS FILL-IN 
          SIZE 10.86 BY 1
     TenancyLease.ReviewNoticePeriod AT ROW 19.6 COL 60.72 COLON-ALIGNED
          LABEL "Rent Review Notice" FORMAT ">9"
          VIEW-AS FILL-IN 
          SIZE 4 BY 1
          FONT 11
     TenancyLease.RightsOfRenewal AT ROW 20.6 COL 13.29 COLON-ALIGNED NO-LABEL FORMAT "X(10)"
          VIEW-AS FILL-IN 
          SIZE 13.14 BY 1
     TenancyLease.RORNoticePeriod AT ROW 20.6 COL 60.72 COLON-ALIGNED
          LABEL "Right of Renewal Notice"
          VIEW-AS FILL-IN 
          SIZE 4 BY 1
          FONT 11
     RECT-30 AT ROW 1.2 COL 1
     "Rental Spaces" VIEW-AS TEXT
          SIZE 14.86 BY 1 AT ROW 4.4 COL 1.57
          FONT 14
     "months" VIEW-AS TEXT
          SIZE 5.14 BY 1 AT ROW 19.6 COL 67.29
          FONT 10
.
/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME F-Main
     "Right of Renewal:" VIEW-AS TEXT
          SIZE 12.57 BY 1 AT ROW 20.6 COL 2.14
          FONT 10
     "months" VIEW-AS TEXT
          SIZE 5.14 BY 1 AT ROW 20.6 COL 67.29
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.TenancyLease
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 24.9
         WIDTH              = 75.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
/* BROWSE-TAB br-rntspc AreaDescription F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN TenancyLease.AreaDescription IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN TenancyLease.ElectricityUnitRate IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN fil_Property IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Tenant IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Term IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX TenancyLease.GrossLease IN FRAME F-Main
   EXP-FORMAT                                                           */
/* SETTINGS FOR TOGGLE-BOX TenancyLease.HasRatchet IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN TenancyLease.LeaseEndDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN TenancyLease.LeaseStartDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN TenancyLease.RatchetClause IN FRAME F-Main
   ALIGN-L EXP-LABEL EXP-FORMAT                                         */
/* SETTINGS FOR FILL-IN TenancyLease.RentEndDate IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR FILL-IN TenancyLease.RentStartDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN TenancyLease.ReviewNoticePeriod IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN TenancyLease.RightsOfRenewal IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN TenancyLease.RORNoticePeriod IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX TenancyLease.TaxApplies IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN TenancyLease.TenancyLeaseCode IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN TenancyLease.TenantCode IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-rntspc
/* Query rebuild information for BROWSE br-rntspc
     _START_FREEFORM
OPEN QUERY br-rntspc FOR EACH RSP NO-LOCK.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is OPENED
*/  /* BROWSE br-rntspc */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/date.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME TenancyLease.AreaDescription
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyLease.AreaDescription V-table-Win
ON ENTRY OF TenancyLease.AreaDescription IN FRAME F-Main /* Description */
DO:
  IF SELF:SCREEN-VALUE = "" THEN DO:
    FIND RentalSpace WHERE RentalSpace.PropertyCode = TenancyLease.PropertyCode
                    AND RentalSpace.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
                    AND RentalSpace.RentalSpaceCode = TenancyLease.PrimarySpace
                    AND RentalSpace.AreaStatus <> "V"
                    NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(RentalSpace) THEN
      FIND FIRST RentalSpace WHERE RentalSpace.PropertyCode = TenancyLease.PropertyCode
                    AND RentalSpace.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
                    AND RentalSpace.AreaStatus <> "V"
                    NO-LOCK NO-ERROR.

    AreaDescription:SCREEN-VALUE IN FRAME {&FRAME-NAME} = 
              (IF AVAILABLE(RentalSpace) THEN RentalSpace.Description ELSE "").
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-rntspc
&Scoped-define SELF-NAME br-rntspc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-rntspc V-table-Win
ON MOUSE-SELECT-DBLCLICK OF br-rntspc IN FRAME F-Main
DO:
DO WITH FRAME {&FRAME-NAME}:
  APPLY 'CHOOSE':U TO btn_add.
  RETURN NO-APPLY.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-rntspc V-table-Win
ON ROW-DISPLAY OF br-rntspc IN FRAME F-Main
DO:
  RUN set-line-color.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-rntspc V-table-Win
ON VALUE-CHANGED OF br-rntspc IN FRAME F-Main
DO:
  RUN sensitise-buttons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN add-spaces.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_primary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_primary V-table-Win
ON CHOOSE OF btn_primary IN FRAME F-Main /* Set As Primary */
DO:
  RUN set-primary.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_remove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_remove V-table-Win
ON CHOOSE OF btn_remove IN FRAME F-Main /* Remove */
DO:
  RUN remove-spaces.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_pymt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_pymt V-table-Win
ON U1 OF cmb_pymt IN FRAME F-Main /* Freq */
DO:
  {inc/selcmb/scfty1.i "TenancyLease" "PaymentFrequency"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_pymt V-table-Win
ON U2 OF cmb_pymt IN FRAME F-Main /* Freq */
DO:
  {inc/selcmb/scfty2.i "TenancyLease" "PaymentFrequency"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Status
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Status V-table-Win
ON U1 OF cmb_Status IN FRAME F-Main /* Status */
DO:
  {inc/selcmb/sclst1.i "TenancyLease" "LeaseStatus"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Status V-table-Win
ON U2 OF cmb_Status IN FRAME F-Main /* Status */
DO:
  {inc/selcmb/sclst2.i "TenancyLease" "LeaseStatus"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Type
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Type V-table-Win
ON U1 OF cmb_Type IN FRAME F-Main /* Type */
DO:
  {inc/selcmb/sclty1.i "TenancyLease" "LeaseType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Type V-table-Win
ON U2 OF cmb_Type IN FRAME F-Main /* Type */
DO:
  {inc/selcmb/sclty2.i "TenancyLease" "LeaseType"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U1 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "TenancyLease" "PropertyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U2 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "TenancyLease" "PropertyCode"}  
  RUN open-rntspc-query.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U3 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "TenancyLease" "PropertyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Tenant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U1 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt1.i "TenancyLease" "TenantCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U2 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt2.i "TenancyLease" "TenantCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U3 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt3.i "TenancyLease" "TenantCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenancyLease.LeaseEndDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyLease.LeaseEndDate V-table-Win
ON ANY-KEY OF TenancyLease.LeaseEndDate IN FRAME F-Main /* End */
DO:
  IF LAST-EVENT:LABEL = "DEL" THEN DO:
    SELF:SCREEN-VALUE = "".
    {&SELF-NAME} = ?.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyLease.LeaseEndDate V-table-Win
ON LEAVE OF TenancyLease.LeaseEndDate IN FRAME F-Main /* End */
DO:
  RUN term-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenancyLease.LeaseStartDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyLease.LeaseStartDate V-table-Win
ON LEAVE OF TenancyLease.LeaseStartDate IN FRAME F-Main /* Lease Start */
DO:
  RUN term-changed.
DO WITH FRAME {&FRAME-NAME}:
  IF TenancyLease.LeaseStartDate = ? AND INPUT TenancyLease.LeaseStartDate <> ? THEN DO:
    IF INPUT TenancyLease.FirstLeaseStart = ? THEN
      TenancyLease.FirstLeaseStart:SCREEN-VALUE = TenancyLease.LeaseStartDate:SCREEN-VALUE.
    IF INPUT TenancyLease.RentStartDate = ? THEN
      TenancyLease.RentStartDate:SCREEN-VALUE = TenancyLease.LeaseStartDate:SCREEN-VALUE.
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenancyLease.PropertyCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyLease.PropertyCode V-table-Win
ON LEAVE OF TenancyLease.PropertyCode IN FRAME F-Main /* Property */
DO:
  {inc/selcde/cdpro.i "fil_Property"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenancyLease.RentEndDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyLease.RentEndDate V-table-Win
ON ANY-KEY OF TenancyLease.RentEndDate IN FRAME F-Main /* End */
DO:
  IF LAST-EVENT:LABEL = "DEL" THEN DO:
    SELF:SCREEN-VALUE = "".
    {&SELF-NAME} = ?.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME TenancyLease.TenantCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TenancyLease.TenantCode V-table-Win
ON LEAVE OF TenancyLease.TenantCode IN FRAME F-Main /* Tenant */
DO:
  {inc/selcde/cdtnt.i "fil_Tenant"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-spaces V-table-Win 
PROCEDURE add-spaces :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
  
  DO i = 1 TO br-rntspc:NUM-SELECTED-ROWS IN FRAME {&FRAME-NAME}:
    IF br-rntspc:FETCH-SELECTED-ROW(i) THEN.
    IF CAN-FIND( FIRST TenancyLease WHERE TenancyLease.TenancyLeaseCode = RSP.TenancyLeaseCode 
                                    AND TenancyLease.LeaseStatus <> "PAST" ) THEN
    DO:
      MESSAGE   "This rental space is already assigned to" SKIP
                "lease" RSP.TenancyLeaseCode SKIP(2)
                "Do you want to override the previous assignment ?"
                VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "Confirm Rental Space"
                UPDATE confirm-it AS LOGI.
      IF NOT confirm-it THEN RETURN.
    END.

    IF TenancyLease.AreaDescription:SCREEN-VALUE = "" 
                AND NOT( CAN-FIND(FIRST RSP OF TenancyLease) ) THEN
      TenancyLease.AreaDescription:SCREEN-VALUE = RSP.Description.
    RSP.TenancyLeaseCode = TenancyLease.TenancyLeaseCode.

    RUN set-line-color.
    IF {&BROWSE-NAME}:DESELECT-FOCUSED-ROW() THEN.
    RUN sensitise-buttons.
    record-changed = Yes.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "TenancyLease"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "TenancyLease"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-rental-spaces V-table-Win 
PROCEDURE assign-rental-spaces :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  /* Delete any that no longer exist */
  FOR EACH RentalSpace OF TenancyLease:
    FIND FIRST RSP WHERE RSP.rsp-id = ROWID( RentalSpace ).
    IF RSP.TenancyLeaseCode = 0 THEN
      ASSIGN RentalSpace.TenancyLeaseCode = 0.
  END.

  /* Assign all given rental spaces to this lease */
  FOR EACH RSP WHERE RSP.TenancyLeaseCode = TenancyLease.TenancyLeaseCode:
    FIND RentalSpace WHERE ROWID( RentalSpace ) = RSP.rsp-id EXCLUSIVE-LOCK.
    RentalSpace.TenancyLeaseCode = TenancyLease.TenancyLeaseCode.
    IF TenancyLease.LeaseStatus <> "PAST" THEN
      RentalSpace.AreaStatus = "L".
    RELEASE RentalSpace.
  END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN check-modified( 'clear':U ).
  IF mode = "Add" THEN RUN delete-lease.
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT record-changed THEN RUN test-modified( OUTPUT record-changed ).

  IF record-changed THEN DO:
    RUN verify-lease.
    IF RETURN-VALUE = "FAIL" THEN RETURN.

    RUN check-rights IN sys-mgr( "V-TNTLSE", "MODIFY" ).
    IF RETURN-VALUE = "FAIL" THEN RETURN.

    RUN dispatch( 'update-record':U ).

    IF mode = "Add" THEN RUN notify( 'open-query,record-source':U ).
  END.
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-rsp V-table-Win 
PROCEDURE create-rsp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-lease V-table-Win 
PROCEDURE delete-lease :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT TenancyLease EXCLUSIVE-LOCK.
  DELETE TenancyLease.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  date-diff( TenancyLease.LeaseStartDate, TenancyLease.LeaseEndDate,
            OUTPUT TenancyLease.TermYears, OUTPUT TenancyLease.TermMonths,
            OUTPUT TenancyLease.TermDays ).

  IF TenancyLease.PrimarySpace = ? OR TenancyLease.PrimarySpace = 0
     OR primary-space <> TenancyLease.PrimarySpace THEN DO:

    FIND FIRST RentalSpace OF TenancyLease NO-LOCK NO-ERROR.
    IF primary-space = ? AND AVAILABLE(RentalSpace) THEN
      TenancyLease.PrimarySpace = RentalSpace.RentalSpaceCode.
    ELSE
      TenancyLease.PrimarySpace = primary-space.

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  primary-space = (IF AVAILABLE(TenancyLease) THEN TenancyLease.PrimarySpace ELSE ?).
  RUN term-changed.
  IF mode <> "Add" THEN RUN open-rntspc-query.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN sensitise-buttons.

  IF ratchet-label <> "" THEN DO WITH FRAME {&FRAME-NAME}:
    TenancyLease.HasRatchet:LABEL = ratchet-label.
    TenancyLease.RatchetClause:LABEL = "Ratchet".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN
  DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE
    RUN dispatch( 'enable-fields':U ).

DO WITH FRAME {&FRAME-NAME}:
  IF Office.GST = ? THEN            HIDE TenancyLease.TaxApplies.
  IF default-unit-rate = ? THEN     HIDE TenancyLease.ElectricityUnitRate .
  IF use-rent-charges THEN          HIDE TenancyLease.RentEndDate TenancyLease.RentStartDate cmb_pymt.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-update-record V-table-Win 
PROCEDURE local-update-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'update-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN assign-rental-spaces.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-rntspc-query V-table-Win 
PROCEDURE open-rntspc-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FOR EACH RSP: DELETE RSP. END.
  
  FIND Property WHERE ROWID( Property ) =
    TO-ROWID( ENTRY( 1, fil_Property:PRIVATE-DATA IN FRAME {&FRAME-NAME} ) ) NO-LOCK NO-ERROR.

  IF NOT AVAILABLE Property THEN
  DO:
    CLOSE QUERY br-rntspc.
    RETURN.
  END.
  
  FOR EACH RentalSpace OF Property NO-LOCK
        BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence:
    CREATE RSP.
    ASSIGN
      RSP.RentalSpaceCode  = RentalSpace.RentalSpaceCode
      RSP.Level            = RentalSpace.Level
      RSP.LevelSequence    = RentalSpace.LevelSequence
      RSP.AreaSize         = RentalSpace.AreaSize
      RSP.ContractedRental = RentalSpace.ContractedRental
      RSP.Description      = RentalSpace.Description
      RSP.TenancyLeaseCode = RentalSpace.TenancyLeaseCode
      RSP.rsp-id           = ROWID( RentalSpace ).
  END.

  OPEN QUERY br-rntspc FOR EACH RSP.
  RUN sensitise-buttons.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  CREATE TenancyLease.
  IF default-unit-rate <> ? THEN
    ASSIGN TenancyLease.ElectricityUnitRate = default-unit-rate.

  TenancyLease.TenantCode = INT( find-parent-key( "TenantCode":U ) ) NO-ERROR.
  TenancyLease.PropertyCode = INT( find-parent-key( "PropertyCode":U )) NO-ERROR.
  TenancyLease.LeaseStatus = "NORM".
  TenancyLease.LeaseType = "NORM".
  TenancyLease.PaymentFrequency = "MNTH".

  record-changed = true.
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'update-record':U ).
  RUN open-rntspc-query.
  record-changed = true.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = 'WINDOW-CLOSE':U THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR c-recsrc   AS CHAR NO-UNDO.
  DEF VAR rowid-list AS CHAR NO-UNDO.
  DEF VAR ext-tables AS CHAR NO-UNDO.

  RUN request-attribute IN adm-broker-hdl(
    THIS-PROCEDURE, 'RECORD-SOURCE':U, 'EXTERNAL-TABLES':U ).
  ext-tables = RETURN-VALUE.
 
  IF mode = "Add" THEN DO:

    RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT c-recsrc ).

    IF LOOKUP( "Tenant", ext-tables ) <> 0 THEN DO:
      RUN send-records IN WIDGET-HANDLE( c-recsrc ) ( "Tenant", OUTPUT rowid-list ).
      FIND Tenant WHERE ROWID( Tenant ) = TO-ROWID( rowid-list ) NO-LOCK NO-ERROR.
      FIND CURRENT TenancyLease EXCLUSIVE-LOCK NO-ERROR.
      IF AVAILABLE(TenancyLease) THEN DO:
        ASSIGN TenancyLease.TenantCode = Tenant.TenantCode.
        FIND CURRENT TenancyLease NO-LOCK.
        APPLY 'U1':U TO fil_Tenant IN FRAME {&FRAME-NAME}.
      END.
    END.
    ELSE IF LOOKUP( "Property", ext-tables ) <> 0 THEN
    DO:
      RUN send-records IN WIDGET-HANDLE( c-recsrc ) ( "Property", OUTPUT rowid-list ).
      FIND Property WHERE ROWID( Property ) = TO-ROWID( rowid-list ) NO-LOCK NO-ERROR.
      FIND CURRENT TenancyLease EXCLUSIVE-LOCK NO-ERROR.
      IF AVAILABLE(TenancyLease) THEN DO:
        ASSIGN TenancyLease.PropertyCode = Property.PropertyCode.
        FIND CURRENT TenancyLease NO-LOCK.
        APPLY 'U1':U TO fil_Property IN FRAME {&FRAME-NAME}.
      END.
    END.
    
    RUN dispatch( 'display-fields' ).

  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-spaces V-table-Win 
PROCEDURE remove-spaces :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR i AS INT NO-UNDO.
  
  DO i = 1 TO br-rntspc:NUM-SELECTED-ROWS IN FRAME {&FRAME-NAME}:
    IF br-rntspc:FETCH-SELECTED-ROW(i) THEN.
    IF primary-space = RSP.RentalSpaceCode THEN primary-space = ?.
    FIND RentalSpace WHERE ROWID( RentalSpace ) = RSP.rsp-id NO-LOCK.
    RSP.TenancyLeaseCode =     IF RentalSpace.TenancyLeaseCode <> TenancyLease.TenancyLeaseCode THEN
      RentalSpace.TenancyLeaseCode ELSE 0.
    RUN set-line-color.
    IF {&BROWSE-NAME}:DESELECT-FOCUSED-ROW() THEN.
    RUN sensitise-buttons.
    record-changed = Yes.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "TenancyLease"}
  {src/adm/template/snd-list.i "RSP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-buttons V-table-Win 
PROCEDURE sensitise-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  DO WITH FRAME {&FRAME-NAME}:
    btn_add:SENSITIVE     = AVAILABLE RSP AND RSP.TenancyLeaseCode <> TenancyLease.TenancyLeaseCode.
    btn_remove:SENSITIVE  = AVAILABLE RSP AND RSP.TenancyLeaseCode =  TenancyLease.TenancyLeaseCode.
    btn_primary:SENSITIVE = AVAILABLE RSP AND RSP.TenancyLeaseCode =  TenancyLease.TenancyLeaseCode
                                          AND RSP.RentalSpaceCode  <>  primary-space.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-line-color V-table-Win 
PROCEDURE set-line-color :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE RSP THEN RETURN.
  
  DEF VAR fgcolor AS INT NO-UNDO.
  fgcolor =
    IF RSP.TenancyLeaseCode = TenancyLease.TenancyLeaseCode THEN
       (IF RSP.RentalSpaceCode = primary-space THEN 2 ELSE 12) ELSE
    IF CAN-FIND( FIRST TenancyLease WHERE TenancyLease.TenancyLeaseCode = RSP.TenancyLeaseCode
        AND TenancyLease.LeaseStatus <> "PAST" ) THEN 8 ELSE ?.
        
  attached-to = 
    IF RSP.RentalSpaceCode = primary-space THEN "Primary" ELSE
    IF RSP.TenancyLeaseCode = TenancyLease.TenancyLeaseCode THEN "This lease" ELSE
    IF CAN-FIND( FIRST TenancyLease WHERE TenancyLease.TenancyLeaseCode = RSP.TenancyLeaseCode
        AND TenancyLease.LeaseStatus <> "PAST" ) THEN "to " + STRING( RSP.TenancyleaseCode, "9999" )
    ELSE "".
    
  ASSIGN
    RSP.Level:FGCOLOR IN BROWSE br-rntspc = fgcolor
    RSP.Description:FGCOLOR = fgcolor
    attached-to:FGCOLOR = fgcolor.
  DISPLAY attached-to WITH BROWSE {&BROWSE-NAME} NO-ERROR.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-primary V-table-Win 
PROCEDURE set-primary :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/

  IF br-rntspc:NUM-SELECTED-ROWS IN FRAME {&FRAME-NAME} > 0 THEN DO:
    IF br-rntspc:FETCH-SELECTED-ROW(1) THEN.  
    primary-space = RSP.RentalSpaceCode.
    OPEN QUERY br-rntspc FOR EACH RSP.
    RUN sensitise-buttons.
    record-changed = Yes.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE term-changed V-table-Win 
PROCEDURE term-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR yy LIKE TenancyLease.TermYears NO-UNDO.
  DEF VAR mm LIKE TenancyLease.TermMonths NO-UNDO.
  DEF VAR dd LIKE TenancyLease.TermDays NO-UNDO.
  DEF VAR start-date LIKE TenancyLease.LeaseStartDate NO-UNDO.
  DEF VAR end-date  LIKE TenancyLease.LeaseEndDate NO-UNDO.

  start-date = INPUT FRAME {&FRAME-NAME} TenancyLease.LeaseStartDate.
  end-date   = INPUT FRAME {&FRAME-NAME} TenancyLease.LeaseEndDate.

  IF start-date = ? OR end-date = ? THEN RETURN.
  
  date-diff( start-date, end-date, OUTPUT yy, OUTPUT mm, OUTPUT dd ).
  
  DO WITH FRAME {&FRAME-NAME}:
  fil_Term = (IF yy > 0 THEN TRIM( STRING( yy, ">>>9y")) ELSE "")
            + (IF mm > 0 THEN TRIM( STRING( mm, ">9m")) ELSE "")
            + (IF dd > 0 THEN TRIM( STRING( dd, ">9d")) ELSE "").
  END.
  DISPLAY fil_Term WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.

  mode = new-mode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-lease V-table-Win 
PROCEDURE verify-lease :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF NOT CAN-FIND(Tenant WHERE Tenant.TenantCode = INPUT TenancyLease.TenantCode) THEN DO:
    MESSAGE "Invalid Tenant Code" VIEW-AS ALERT-BOX ERROR TITLE "Invalid Tenant Code".
    APPLY "ENTRY":U TO TenancyLease.TenantCode.
    RETURN "FAIL".
  END.

  IF NOT CAN-FIND(Property WHERE Property.PropertyCode = INPUT TenancyLease.PropertyCode) THEN DO:
    MESSAGE "Invalid Property Code" VIEW-AS ALERT-BOX ERROR TITLE "Invalid Property Code".
    APPLY "ENTRY":U TO TenancyLease.PropertyCode.
    RETURN "FAIL".
  END.

  IF INPUT TenancyLease.LeaseStartDate = ? THEN DO:
    MESSAGE "The Lease Start Date has not been set" 
    VIEW-AS ALERT-BOX ERROR TITLE "Invalid Start Date".
    APPLY "ENTRY":U TO TenancyLease.LeaseStartDate.
    RETURN "FAIL".
  END.

  IF NOT( CAN-FIND( FIRST RSP OF TenancyLease ) ) THEN DO:
    MESSAGE "No Rental Areas have been assigned!" 
    VIEW-AS ALERT-BOX ERROR TITLE "No Rental Areas".
    APPLY "ENTRY":U TO br-rntspc.
    RETURN "FAIL".
  END.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


