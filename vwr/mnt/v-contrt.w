&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{inc/topic/tpcontrt.i}

DEF VAR mode AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Contract
&Scoped-define FIRST-EXTERNAL-TABLE Contract


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Contract.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Contract.CreditorCode Contract.PropertyCode ~
Contract.ContractReference Contract.StartDate Contract.EndDate ~
Contract.ReviewDate Contract.TerminationNotice Contract.Renewing ~
Contract.PaymentDate Contract.Recoverable Contract.AnnualEstimate 
&Scoped-define ENABLED-TABLES Contract
&Scoped-define FIRST-ENABLED-TABLE Contract
&Scoped-define DISPLAYED-TABLES Contract
&Scoped-define FIRST-DISPLAYED-TABLE Contract
&Scoped-Define ENABLED-OBJECTS cmb_ServiceType cmb_frequency ~
fil_PeriodAmount RECT-25 RECT-26 RECT-27 
&Scoped-Define DISPLAYED-FIELDS Contract.CreditorCode Contract.PropertyCode ~
Contract.ContractReference Contract.StartDate Contract.EndDate ~
Contract.ReviewDate Contract.TerminationNotice Contract.Renewing ~
Contract.PaymentDate Contract.Recoverable Contract.AnnualEstimate 
&Scoped-Define DISPLAYED-OBJECTS fil_Creditor fil_Property fil_Contact ~
cmb_ServiceType cmb_frequency fil_PeriodAmount 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
PropertyCode|y|y|TTPL.Contract.PropertyCode
CreditorCode|y|y|TTPL.Contract.CreditorCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "PropertyCode,CreditorCode",
     Keys-Supplied = "PropertyCode,CreditorCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_frequency AS CHARACTER FORMAT "X(256)":U 
     LABEL "Frequency" 
     VIEW-AS COMBO-BOX INNER-LINES 8
     DROP-DOWN-LIST
     SIZE 22.86 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_ServiceType AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS COMBO-BOX INNER-LINES 8
     DROP-DOWN-LIST
     SIZE 51 BY 1
     FONT 11 NO-UNDO.

DEFINE VARIABLE fil_Contact AS CHARACTER FORMAT "X(50)":U 
     LABEL "Contact" 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Creditor AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 42 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_PeriodAmount AS DECIMAL FORMAT "-Z,ZZZ,ZZ9.99" INITIAL 0 
     LABEL "Period Amt" 
     VIEW-AS FILL-IN 
     SIZE 15.14 BY 1
     FONT 11.

DEFINE VARIABLE fil_Property AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 42 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-25
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 63 BY 16.5.

DEFINE RECTANGLE RECT-26
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 62 BY .05.

DEFINE RECTANGLE RECT-27
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 62 BY .05.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Contract.CreditorCode AT ROW 1.2 COL 10.14 COLON-ALIGNED
          LABEL "Creditor"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
          FONT 11
     fil_Creditor AT ROW 1.2 COL 19.14 COLON-ALIGNED NO-LABEL
     Contract.PropertyCode AT ROW 2.2 COL 10.14 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
          FONT 11
     fil_Property AT ROW 2.2 COL 19.14 COLON-ALIGNED NO-LABEL
     Contract.ContractReference AT ROW 3.4 COL 12.14 NO-LABEL
          VIEW-AS EDITOR NO-WORD-WRAP
          SIZE 51 BY 2.5
          FONT 10
     fil_Contact AT ROW 6.4 COL 10.43 COLON-ALIGNED
     cmb_ServiceType AT ROW 7.4 COL 10.43 COLON-ALIGNED NO-LABEL
     Contract.StartDate AT ROW 10 COL 9.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
          FONT 10
     Contract.EndDate AT ROW 10 COL 28.14 COLON-ALIGNED
          LABEL "End"
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
          FONT 10
     Contract.ReviewDate AT ROW 10 COL 48.14 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 10.86 BY 1
          FONT 10
     Contract.TerminationNotice AT ROW 11.4 COL 9.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 4.43 BY 1
          FONT 10
     Contract.Renewing AT ROW 11.4 COL 30.14
          VIEW-AS TOGGLE-BOX
          SIZE 10.86 BY 1
     cmb_frequency AT ROW 13.6 COL 9.86 COLON-ALIGNED
     Contract.PaymentDate AT ROW 13.6 COL 48.14 COLON-ALIGNED
          LABEL "Pymt Cycle Date"
          VIEW-AS FILL-IN 
          SIZE 10.86 BY 1.05
     Contract.Recoverable AT ROW 14.6 COL 11.86
          VIEW-AS TOGGLE-BOX
          SIZE 12 BY 1
     Contract.AnnualEstimate AT ROW 15.7 COL 9.86 COLON-ALIGNED
          LABEL "Annual Est" FORMAT "-Z,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 14.72 BY 1
          FONT 11
     fil_PeriodAmount AT ROW 15.8 COL 43.57 COLON-ALIGNED
     RECT-25 AT ROW 1 COL 1
     RECT-26 AT ROW 8.6 COL 1.57
     RECT-27 AT ROW 12.8 COL 1
     "Creditor:" VIEW-AS TEXT
          SIZE 10 BY 1 AT ROW 1.2 COL 2.14
          FONT 10
     "Property:" VIEW-AS TEXT
          SIZE 10 BY 1 AT ROW 2.2 COL 2.14
          FONT 10
     "Description:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 3.4 COL 2.14
          FONT 10
     "Contact:" VIEW-AS TEXT
          SIZE 10 BY 1 AT ROW 6.4 COL 2.43
          FONT 10
     "Service Type:" VIEW-AS TEXT
          SIZE 10 BY 1 AT ROW 7.4 COL 2.43
          FONT 10
     "Duration" VIEW-AS TEXT
          SIZE 8.57 BY 1 AT ROW 9 COL 2.14
          FONT 14
     "months." VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 11.4 COL 16.43
          FONT 10
     "Cost Details" VIEW-AS TEXT
          SIZE 12.14 BY 1 AT ROW 12.6 COL 2.14
          FONT 14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Contract
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.1
         WIDTH              = 73.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Contract.AnnualEstimate IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR EDITOR Contract.ContractReference IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Contract.CreditorCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Contract.EndDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Contact IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Creditor IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Property IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Contract.PaymentDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Contract.AnnualEstimate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Contract.AnnualEstimate V-table-Win
ON LEAVE OF Contract.AnnualEstimate IN FRAME F-Main /* Annual Est */
DO:
  IF SELF:MODIFIED THEN RUN annual-cost-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_frequency
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_frequency V-table-Win
ON U1 OF cmb_frequency IN FRAME F-Main /* Frequency */
DO:
  {inc/selcmb/scfty1.i "Contract" "FrequencyCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_frequency V-table-Win
ON U2 OF cmb_frequency IN FRAME F-Main /* Frequency */
DO:
  {inc/selcmb/scfty2.i "Contract" "FrequencyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_frequency V-table-Win
ON VALUE-CHANGED OF cmb_frequency IN FRAME F-Main /* Frequency */
DO:
  RUN annual-cost-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_ServiceType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ServiceType V-table-Win
ON U1 OF cmb_ServiceType IN FRAME F-Main
DO:
  {inc/selcmb/scsty1.i "Contract" "ServiceType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ServiceType V-table-Win
ON U2 OF cmb_ServiceType IN FRAME F-Main
DO:
  {inc/selcmb/scsty2.i "Contract" "ServiceType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Contract.CreditorCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Contract.CreditorCode V-table-Win
ON LEAVE OF Contract.CreditorCode IN FRAME F-Main /* Creditor */
DO:
  {inc/selcde/cdcrd.i "fil_Creditor"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Contact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Contact V-table-Win
ON U1 OF fil_Contact IN FRAME F-Main /* Contact */
DO:
  {inc/selfil/sfpsn1.i "Contract" "Contact"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Contact V-table-Win
ON U2 OF fil_Contact IN FRAME F-Main /* Contact */
DO:
  {inc/selfil/sfpsn2.i "Contract" "Contact"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Contact V-table-Win
ON U3 OF fil_Contact IN FRAME F-Main /* Contact */
DO:
  {inc/selfil/sfpsn3.i "Contract" "Contact"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Creditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U1 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd1.i "Contract" "CreditorCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U2 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd2.i "Contract" "CreditorCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U3 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd3.i "Contract" "CreditorCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_PeriodAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PeriodAmount V-table-Win
ON LEAVE OF fil_PeriodAmount IN FRAME F-Main /* Period Amt */
DO:
  IF SELF:MODIFIED THEN RUN period-cost-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U1 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "Contract" "PropertyCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U2 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "Contract" "PropertyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U3 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "Contract" "PropertyCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Contract.PropertyCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Contract.PropertyCode V-table-Win
ON LEAVE OF Contract.PropertyCode IN FRAME F-Main /* Property */
DO:
  {inc/selcde/cdpro.i "fil_Property"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Contract.StartDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Contract.StartDate V-table-Win
ON LEAVE OF Contract.StartDate IN FRAME F-Main /* Start */
DO:
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT Contract.PaymentDate = ? THEN ASSIGN
    Contract.PaymentDate:SCREEN-VALUE = SELF:SCREEN-VALUE
    Contract.PaymentDate:MODIFIED = Yes .
END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'PropertyCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Contract
           &WHERE = "WHERE Contract.PropertyCode eq INTEGER(key-value)"
       }
    WHEN 'CreditorCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Contract
           &WHERE = "WHERE Contract.CreditorCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Contract"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Contract"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE annual-cost-changed V-table-Win 
PROCEDURE annual-cost-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR fraction  AS DEC NO-UNDO.
  DEF VAR freq-code LIKE FrequencyType.FrequencyCode NO-UNDO.

  DO WITH FRAME {&FRAME-NAME}:  
  
    freq-code = TRIM( ENTRY( 1, INPUT cmb_Frequency, "-" ) ).
    RUN process/calcfreq.p( freq-code, OUTPUT fraction ).
      fil_PeriodAmount:SCREEN-VALUE IN FRAME {&FRAME-NAME} = 
        STRING( INPUT Contract.AnnualEstimate * fraction ).
    fil_PeriodAmount:MODIFIED = No.
    Contract.AnnualEstimate:MODIFIED = No.
  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE' ).
  IF mode = "Add" THEN RUN delete-contract. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/


  RUN verify-contract.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN notify( 'hide, CONTAINER-SOURCE' ).
  RUN dispatch( 'update-record':U ).
  IF mode = "Add":U THEN RUN notify ( 'open-query,record-source':U ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-contract V-table-Win 
PROCEDURE delete-contract :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Contract EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Contract THEN DELETE Contract.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-creditor-code V-table-Win 
PROCEDURE get-creditor-code :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF OUTPUT PARAMETER creditor-code LIKE Creditor.CreditorCode NO-UNDO.
  
  DEF VAR c-recsrc AS CHAR   NO-UNDO.
  DEF VAR h-recsrc AS HANDLE NO-UNDO.
  DEF VAR c-code   AS CHAR   NO-UNDO.
  
  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE', OUTPUT c-recsrc ).
  h-recsrc = WIDGET-HANDLE( c-recsrc ).
  
  IF VALID-HANDLE( h-recsrc ) THEN
  DO:
    RUN send-key IN h-recsrc (INPUT "CreditorCode", OUTPUT c-code ).
    creditor-code = IF c-code <> ? THEN INT( c-code ) ELSE 0.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-property-code V-table-Win 
PROCEDURE get-property-code :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF OUTPUT PARAMETER property-code LIKE Property.PropertyCode NO-UNDO.
  
  DEF VAR c-recsrc AS CHAR   NO-UNDO.
  DEF VAR h-recsrc AS HANDLE NO-UNDO.
  DEF VAR c-code   AS CHAR   NO-UNDO.
  
  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE', OUTPUT c-recsrc ).
  h-recsrc = WIDGET-HANDLE( c-recsrc ).
  
  IF VALID-HANDLE( h-recsrc ) THEN
  DO:
    RUN send-key IN h-recsrc (INPUT "PropertyCode", OUTPUT c-code ).
    property-code = IF c-code <> ? THEN INT( c-code ) ELSE 0.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-disable-fields V-table-Win 
PROCEDURE inst-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  DISABLE cmb_frequency cmb_ServiceType fil_PeriodAmount.
  RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, 'Window', 'hidden = yes' ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN annual-cost-changed.    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "View" THEN DO:
    RUN dispatch( 'disable-fields':U ).
  END.
  ELSE
    RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CREATE Contract.
  RUN get-property-code( OUTPUT Contract.PropertyCode ).
  RUN get-creditor-code( OUTPUT Contract.CreditorCode ).

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  
  CURRENT-WINDOW:TITLE = "Adding a new contract".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE period-cost-changed V-table-Win 
PROCEDURE period-cost-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR fraction  AS DEC NO-UNDO.
  DEF VAR freq-code LIKE FrequencyType.FrequencyCode NO-UNDO.

  DO WITH FRAME {&FRAME-NAME}:  
  
    freq-code = TRIM( ENTRY( 1, INPUT cmb_Frequency, "-" ) ).
    RUN process/calcfreq.p( freq-code, OUTPUT fraction ).
      Contract.AnnualEstimate:SCREEN-VALUE IN FRAME {&FRAME-NAME} = 
        STRING( INPUT fil_PeriodAmount / fraction ).
    fil_PeriodAmount:MODIFIED = No.
    Contract.AnnualEstimate:MODIFIED = No.
  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-assign-statement V-table-Win 
PROCEDURE pre-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER OC FOR Contract.

DEF VAR idx-seq AS INT NO-UNDO.
DEF VAR item-id AS CHAR NO-UNDO.

IF NOT AVAILABLE Contract THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:

  IF CAN-FIND( FIRST OC WHERE OC.PropertyCode = INPUT Contract.PropertyCode
                        AND OC.ContractSeq = Contract.ContractSeq
                        AND RECID(OC) <> RECID(Contract) )
  THEN DO:
    DEF VAR max-seq AS INT NO-UNDO INITIAL 0.
    FOR EACH OC WHERE OC.PropertyCode = INPUT Contract.PropertyCode
                           AND ROWID(OC) <> ROWID(Contract) NO-LOCK:
      IF OC.ContractSeq > max-seq THEN max-seq = OC.ContractSeq.
    END.
    Contract.ContractSeq = max-seq + 1.
  END.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "PropertyCode" "Contract" "PropertyCode"}
  {src/adm/template/sndkycas.i "CreditorCode" "Contract" "CreditorCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Contract"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-contract V-table-Win 
PROCEDURE verify-contract :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NOT CAN-FIND( Creditor WHERE ROWID( Creditor ) = 
    TO-ROWID( fil_Creditor:PRIVATE-DATA ) ) THEN DO:
    MESSAGE "You must select a Creditor" VIEW-AS ALERT-BOX ERROR
      TITLE "No Creditor selected".
    APPLY 'ENTRY' TO Contract.CreditorCode.
    RETURN "FAIL".
  END.
  
  IF NOT CAN-FIND( Property WHERE ROWID( Property ) = 
    TO-ROWID( fil_Property:PRIVATE-DATA ) ) THEN DO:
    MESSAGE "You must select a Property" VIEW-AS ALERT-BOX ERROR
      TITLE "No Property selected".
    APPLY 'ENTRY' TO Contract.PropertyCode.
    RETURN "FAIL".
  END.

  IF INPUT cmb_ServiceType = ? OR INPUT cmb_serviceType = "" THEN DO:
    MESSAGE "You must select a Service Type" VIEW-AS ALERT-BOX ERROR
      TITLE "No Service type selected".
    APPLY 'ENTRY' TO cmb_ServiceType.
    RETURN "FAIL".
  END.  

/*
  IF INPUT cmb_Frequency = "" OR INPUT cmb_Frequency = ? 
      /* OR INPUT Contract.AnnualEstimate = 0 */
  THEN DO:
    MESSAGE
      "You enter both a frequency and an" SKIP
      "annual estimate for the contract cost."
      VIEW-AS ALERT-BOX ERROR TITLE "Invalid cost details.".
    IF INPUT cmb_Frequency = "" OR INPUT cmb_Frequency = ? THEN
      APPLY 'ENTRY' TO cmb_Frequency.
/*    ELSE IF INPUT Contract.AnnualEstimate = 0 THEN
      APPLY 'ENTRY' TO Contract.AnnualEstimate. */
    RETURN "FAIL".
  END.
*/

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

