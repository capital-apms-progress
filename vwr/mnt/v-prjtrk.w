&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

&SCOPED-DEFINE REPORT-ID "Project Tracking"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char3 RP.Int1 RP.Char5 RP.Int2 RP.Int3 ~
RP.Int4 RP.Log6 RP.Char1 RP.Log5 RP.Log4 RP.Log3 RP.Log1 RP.Log7 RP.Log2 ~
RP.Char2 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_AsAt btn_browse btn_print RECT-32 
&Scoped-Define DISPLAYED-FIELDS RP.Char3 RP.Int1 RP.Char5 RP.Int2 RP.Int3 ~
RP.Int4 RP.Log6 RP.Char1 RP.Log5 RP.Log4 RP.Log3 RP.Log1 RP.Log7 RP.Log2 ~
RP.Char2 
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define DISPLAYED-OBJECTS fil_Project1 fil_Project2 cmb_AsAt 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_browse 
     LABEL "&Browse" 
     SIZE 7.14 BY 1.05
     FONT 9.

DEFINE BUTTON btn_print DEFAULT 
     LABEL "&OK" 
     SIZE 10 BY 1
     FONT 9.

DEFINE VARIABLE cmb_AsAt AS CHARACTER FORMAT "X(256)":U 
     LABEL "date" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 18.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Project1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 50.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Project2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 50.86 BY 1.05 NO-UNDO.

DEFINE RECTANGLE RECT-32
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 69 BY 16.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char3 AT ROW 1.2 COL 1.86 NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "Project range", "J":U,
"Entity list", "E":U
          SIZE 30.57 BY .8
          FONT 10
     RP.Int1 AT ROW 2.1 COL 5.86 COLON-ALIGNED
          LABEL "From" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_Project1 AT ROW 2.1 COL 15.57 COLON-ALIGNED NO-LABEL
     RP.Char5 AT ROW 2.8 COL 11.86 COLON-ALIGNED
          LABEL "List" FORMAT "X(60)"
          VIEW-AS COMBO-BOX INNER-LINES 10
          LIST-ITEMS "TTP" 
          DROP-DOWN-LIST
          SIZE 54.72 BY 1
     RP.Int2 AT ROW 3.3 COL 5.86 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1.05
     fil_Project2 AT ROW 3.3 COL 15.57 COLON-ALIGNED NO-LABEL
     RP.Int3 AT ROW 4.8 COL 15.57 COLON-ALIGNED
          LABEL "Hierarchy levels"
          VIEW-AS FILL-IN 
          SIZE 3.43 BY 1.05
     RP.Int4 AT ROW 4.8 COL 25.29 COLON-ALIGNED
          LABEL "through"
          VIEW-AS FILL-IN 
          SIZE 3.43 BY 1.05
     RP.Log6 AT ROW 4.9 COL 38.14
          LABEL "Show bottom level only"
          VIEW-AS TOGGLE-BOX
          SIZE 19.14 BY .85
     RP.Char1 AT ROW 6.2 COL 5.86 COLON-ALIGNED HELP
          ""
          LABEL "Style" FORMAT "X(256)"
          VIEW-AS COMBO-BOX INNER-LINES 15
          LIST-ITEMS "ACCT - Project Accounting","COFA - Project Chart of Accounts","PMGR - Project Management","AMTR - AmTrust Pacific" 
          DROP-DOWN-LIST
          SIZE 34.86 BY 1
     cmb_AsAt AT ROW 7.75 COL 22.43 COLON-ALIGNED
     RP.Log5 AT ROW 7.8 COL 9
          LABEL "Report as at"
          VIEW-AS TOGGLE-BOX
          SIZE 11.43 BY 1
     RP.Log4 AT ROW 8.75 COL 9
          LABEL "Show totals for the budgets of each project"
          VIEW-AS TOGGLE-BOX
          SIZE 37.72 BY 1.05
     RP.Log3 AT ROW 9.75 COL 9
          LABEL "Show project budget detail lines"
          VIEW-AS TOGGLE-BOX
          SIZE 24.14 BY 1.05
     RP.Log1 AT ROW 10.75 COL 9
          LABEL "Show Order Details"
          VIEW-AS TOGGLE-BOX
          SIZE 16 BY 1
          FONT 10
     RP.Log7 AT ROW 11.75 COL 9
          LABEL "Show only Active projects"
          VIEW-AS TOGGLE-BOX
          SIZE 22 BY .8
     RP.Log2 AT ROW 13 COL 8 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Export", no,
"Preview", yes,
"Print", ?
          SIZE 8.57 BY 3
     RP.Char2 AT ROW 13 COL 14.57 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 44 BY 1
          FONT 10
     btn_browse AT ROW 13 COL 60.86
     btn_print AT ROW 15.1 COL 58
     RECT-32 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_print.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 17.3
         WIDTH              = 81.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX RP.Char1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Char2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR COMBO-BOX RP.Char5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN fil_Project1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Project2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RP.Int4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR RADIO-SET RP.Log2 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log6 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log7 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_browse V-table-Win
ON CHOOSE OF btn_browse IN FRAME F-Main /* Browse */
DO:  
  RUN browse-file.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
  DISABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_AsAt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AsAt V-table-Win
ON U1 OF cmb_AsAt IN FRAME F-Main /* date */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AsAt V-table-Win
ON U2 OF cmb_AsAt IN FRAME F-Main /* date */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Project1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Project1 V-table-Win
ON U1 OF fil_Project1 IN FRAME F-Main
DO:
  {inc/selfil/sfprj1.i "RP" "Int1"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Project1 V-table-Win
ON U2 OF fil_Project1 IN FRAME F-Main
DO:
  {inc/selfil/sfprj2.i "RP" "Int1"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Project1 V-table-Win
ON U3 OF fil_Project1 IN FRAME F-Main
DO:
  {inc/selfil/sfprj3.i "RP" "Int1"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Project2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Project2 V-table-Win
ON U1 OF fil_Project2 IN FRAME F-Main
DO:
  {inc/selfil/sfprj1.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Project2 V-table-Win
ON U2 OF fil_Project2 IN FRAME F-Main
DO:
  {inc/selfil/sfprj2.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Project2 V-table-Win
ON U3 OF fil_Project2 IN FRAME F-Main
DO:
  {inc/selfil/sfprj3.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* From */
DO:
  {inc/selcde/cdprj.i "fil_Project1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* To */
DO:
  {inc/selcde/cdprj.i "fil_Project2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Log2 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log5 V-table-Win
ON VALUE-CHANGED OF RP.Log5 IN FRAME F-Main /* Report as at */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log6 V-table-Win
ON VALUE-CHANGED OF RP.Log6 IN FRAME F-Main /* Show bottom level only */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE browse-file V-table-Win 
PROCEDURE browse-file :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR save-as     AS CHAR NO-UNDO.
  DEF VAR file-chosen AS LOGI NO-UNDO.

  save-as = INPUT FRAME {&FRAME-NAME} RP.Char2.
  IF LENGTH( save-as ) > 0 THEN
  IF LOOKUP( SUBSTR( save-as, LENGTH( save-as ), 1 ), "\,/" ) <> 0 THEN save-as = "".
    
  SYSTEM-DIALOG GET-FILE save-as
    TITLE   "Export Report to file"
    FILTERS "CSV Files (*.csv)" "*.CSV"
    DEFAULT-EXTENSION ".CSV"
    SAVE-AS
    RETURN-TO-START-DIR
    USE-FILENAME
    UPDATE file-chosen.

  IF file-chosen THEN RP.Char2:SCREEN-VALUE IN FRAME {&FRAME-NAME} = 
    save-as.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF INPUT RP.Log5 THEN
    VIEW cmb_AsAt.
  ELSE
    HIDE cmb_AsAt.

  /* RP.Log2 is three-state logic so " = FALSE" is the right check */
  IF INPUT RP.Log2 = FALSE THEN DO:
    VIEW RP.Char2 btn_Browse.
  END.
  ELSE DO:
    HIDE RP.Char2 btn_Browse.
  END.

  IF INPUT RP.Log6 THEN
    HIDE RP.Int3 RP.Int4.
  ELSE
    VIEW RP.Int3 RP.Int4.

  IF INPUT RP.Char3 = "E" THEN DO:
    HIDE fil_Project1 fil_Project2 RP.Int1 RP.Int2 .
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Project1:HANDLE ), "HIDDEN = Yes" ).
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Project2:HANDLE ), "HIDDEN = Yes" ).
    VIEW RP.Char5.
  END.
  ELSE DO:
    HIDE RP.Char5.
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Project1:HANDLE ), "HIDDEN = No" ).
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Project2:HANDLE ), "HIDDEN = No" ).
    VIEW fil_Project1 fil_Project2 RP.Int1 RP.Int2 .
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN enable-appropriate-fields.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR user-name AS CHAR NO-UNDO.
  
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  
  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.
    
  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name.
  END.

DEF VAR con-list    AS CHAR INITIAL "" NO-UNDO.
DEF VAR delim AS CHAR INITIAL "|" NO-UNDO.

  /* Initialise the entity list for the combos */
  FOR EACH EntityList NO-LOCK:
    con-list = con-list + delim + STRING( EntityList.ListCode, "X(7)") + "- " + EntityList.Description.
  END.
  con-list = SUBSTRING( con-list, 2).   /* trim initial delimiter */
  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN
      RP.Char5:DELIMITER = delim
      RP.Char5:LIST-ITEMS = con-list
    .
  END.

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR options AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).

  options = "Style," + ENTRY( 1, RP.Char1, " ")
          + (IF RP.Log6 THEN "" ELSE "~nLevel-Range," + STRING( RP.Int3 ) + "," + STRING( RP.Int4 ))
          + (IF RP.Char3 = "E" THEN
                 "~nList," + TRIM( SUBSTRING( RP.Char5, 1, 7) )
             ELSE
                 "~nRange,J," + STRING( RP.Int1 ) + "," + STRING( RP.Int2 )
             )
          + (IF RP.Log1 THEN "~nShow-Orders" ELSE "")
          + (IF RP.Log2 THEN "~nPreview" ELSE "")
          + (IF RP.Log2 = FALSE THEN "~nExport," + RP.Char2 ELSE "")
          + (IF RP.Log3 THEN "~nShow-Accounts" ELSE "")
          + (IF RP.Log4 THEN "~nShow-Totals" ELSE "")
          + (IF RP.Log5 THEN "~nAsAt," + STRING(RP.Int5) ELSE "")
          + (IF RP.Log6 THEN "~nBottomLevel" ELSE "")
          + (IF RP.Log7 THEN "~nActiveOnly" ELSE "")
          .

  RUN notify( 'set-busy, CONTAINER-SOURCE':u ).
  RUN process/report/project.p ( options ).
  RUN notify( 'set-idle, CONTAINER-SOURCE':u ).
        
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

