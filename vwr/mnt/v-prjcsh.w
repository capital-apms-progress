&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF TEMP-TABLE PrjCashFlow NO-UNDO
  FIELD YearCode  LIKE FinancialYear.FinancialYearCode
  FIELD MonthCode LIKE Month.MonthCode
  FIELD MonthEnd  LIKE Month.EndDate
  FIELD MonthName LIKE Month.MonthName
  FIELD Balance   LIKE AccountBalance.Balance
  FIELD Budget    LIKE AccountBalance.Budget
  FIELD Revised   LIKE AccountBalance.Revised
  
  INDEX Month IS UNIQUE PRIMARY
    MonthCode
    
  INDEX MonthYear
    YearCode
    MonthEnd ASCENDING.

DEF VAR this-win AS HANDLE NO-UNDO.
DEF VAR very-first-month LIKE Month.MonthCode NO-UNDO.
DEF VAR very-first-year  LIKE FinancialYear.FinancialYearCode NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME BROWSE-1

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES ProjectBudget
&Scoped-define FIRST-EXTERNAL-TABLE ProjectBudget


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR ProjectBudget.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES PrjCashFlow

/* Definitions for BROWSE BROWSE-1                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-1 PrjCashFlow.MonthName PrjCashFlow.Balance PrjCashFlow.Budget PrjCashFlow.Revised   
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-1 PrjCashFlow.Revised   
&Scoped-define ENABLED-TABLES-IN-QUERY-BROWSE-1 PrjCashFlow
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-BROWSE-1 PrjCashFlow
&Scoped-define SELF-NAME BROWSE-1
&Scoped-define OPEN-QUERY-BROWSE-1 OPEN QUERY {&SELF-NAME} FOR EACH PrjCashFlow NO-LOCK.
&Scoped-define TABLES-IN-QUERY-BROWSE-1 PrjCashFlow
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-1 PrjCashFlow


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cmb_Years tgl_topmost BROWSE-1 RECT-1 
&Scoped-Define DISPLAYED-OBJECTS cmb_Years tgl_topmost 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
AccountCode|y|y|TTPL.ProjectBudget.AccountCode
EntityCode|y|y|TTPL.ProjectBudget.EntityCode
EntityType|y|y|TTPL.ProjectBudget.EntityType
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "AccountCode,EntityCode,EntityType",
     Keys-Supplied = "AccountCode,EntityCode,EntityType"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_Years AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 48.29 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 66 BY 11.9.

DEFINE VARIABLE tgl_topmost AS LOGICAL INITIAL no 
     LABEL "On Top?" 
     VIEW-AS TOGGLE-BOX
     SIZE 9.29 BY 1
     FONT 10 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-1 FOR 
      PrjCashFlow SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-1 V-table-Win _FREEFORM
  QUERY BROWSE-1 NO-LOCK DISPLAY
      PrjCashFlow.MonthName FORMAT "X(3)" LABEL "Mth"
      PrjCashFlow.Balance
      PrjCashFlow.Budget
      PrjCashFlow.Revised
ENABLE
    PrjCashFlow.Revised
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 64.14 BY 10
         BGCOLOR 15 FGCOLOR 1 FONT 8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     cmb_Years AT ROW 1.35 COL 4.72 COLON-ALIGNED NO-LABEL
     tgl_topmost AT ROW 1.5 COL 56
     BROWSE-1 AT ROW 2.65 COL 1.86
     RECT-1 AT ROW 1 COL 1
     "Year:" VIEW-AS TEXT
          SIZE 4 BY 1 AT ROW 1.35 COL 2.14
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.ProjectBudget
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 12.15
         WIDTH              = 66.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB BROWSE-1 tgl_topmost F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-1
/* Query rebuild information for BROWSE BROWSE-1
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH PrjCashFlow NO-LOCK.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* BROWSE BROWSE-1 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME cmb_Years
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Years V-table-Win
ON VALUE-CHANGED OF cmb_Years IN FRAME F-Main
DO:
  RUN assign-cash-flows.
  RUN year-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_topmost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_topmost V-table-Win
ON VALUE-CHANGED OF tgl_topmost IN FRAME F-Main /* On Top? */
DO:
  RUN top-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-1
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */


/* Triggers for cash flows */

ON 'LEAVE':U OF PrjCashFlow.Revised
DO:
  RUN budget-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'AccountCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = ProjectBudget
           &WHERE = "WHERE ProjectBudget.AccountCode eq DECIMAL(key-value)"
       }
    WHEN 'EntityCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = ProjectBudget
           &WHERE = "WHERE ProjectBudget.EntityCode eq INTEGER(key-value)"
       }
    WHEN 'EntityType':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = ProjectBudget
           &WHERE = "WHERE ProjectBudget.EntityType eq key-value"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "ProjectBudget"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "ProjectBudget"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-cash-flows V-table-Win 
PROCEDURE assign-cash-flows :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE FinancialYear 
        OR NOT AVAILABLE ProjectBudget
        OR NOT AVAILABLE PrjCashFlow
     THEN RETURN.
  
  ASSIGN BROWSE {&BROWSE-NAME}
/*    PrjCashFlow.MonthName
    PrjCashFlow.Budget
*/    PrjCashFlow.Revised.
    
  FOR EACH PrjCashFlow:

    FIND AccountBalance WHERE
      AccountBalance.EntityType  = "J"  AND
      AccountBalance.EntityCode  = Project.ProjectCode  AND
      AccountBalance.AccountCode = ProjectBudget.AccountCode AND
      AccountBalance.MonthCode   = PrjCashFlow.MonthCode
      EXCLUSIVE-LOCK NO-ERROR.

    IF NOT AVAILABLE AccountBalance AND
      ( PrjCashFlow.Budget <> 0.00 OR PrjCashFlow.Revised <> 0.00 ) THEN
    DO:
      CREATE AccountBalance.
      ASSIGN
        AccountBalance.EntityType  = "J"
        AccountBalance.EntityCode  = Project.ProjectCode
        AccountBalance.AccountCode = ProjectBudget.AccountCode
        AccountBalance.MonthCode   = PrjCashFlow.MonthCode.
    END.
          
    IF AVAILABLE AccountBalance THEN
    ASSIGN
      AccountBalance.Budget  = PrjCashFlow.Budget
      AccountBalance.Revised = PrjCashFlow.Revised.
  
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE budget-changed V-table-Win 
PROCEDURE budget-changed :
/*------------------------------------------------------------------------------
  Purpose:  1. Reflect changes in the revised budget
            2. Apply the difference between new and old to:
               If the previous cash flow is the first for the budget then
                  a newly created one for that month
               else
                  the first cash flow for this budget
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR change AS DEC NO-UNDO.

  RUN verify-budget.  

  change = DEC( PrjCashFlow.Revised:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} )
         - PrjCashFlow.Revised.

  IF change = 0 THEN RETURN.
  PrjCashFlow.Revised = INPUT PrjCashFlow.Revised.

  IF NOT Project.BudgetsFrozen THEN DO:
    PrjCashFlow.Budget  = PrjCashFlow.Budget + change .
  END.

DEF BUFFER FirstBalance FOR AccountBalance.
DEF BUFFER FirstFlow  FOR PrjCashFlow.
DEF BUFFER ChangeMonth FOR Month.
DEF VAR first-month   LIKE Month.MonthCode NO-UNDO.
DEF VAR change-month LIKE Month.MonthCode NO-UNDO.

   FIND FIRST FirstBalance WHERE
      FirstBalance.EntityType  =  "J" AND
      FirstBalance.EntityCode  =  ProjectBudget.ProjectCode AND
      FirstBalance.AccountCode =  ProjectBudget.AccountCode AND
      FirstBalance.MonthCode   <= PrjCashFlow.MonthCode AND
      FirstBalance.Revised      <> 0
      NO-LOCK NO-ERROR.

  FIND FIRST FirstFlow WHERE FirstFlow.MonthCode < PrjCashFlow.MonthCode
                       AND FirstFlow.Revised <> 0 NO-LOCK NO-ERROR.
    
  IF AVAILABLE FirstBalance AND AVAILABLE FirstFlow THEN
    first-month = MIN( FirstBalance.MonthCode, FirstFlow.MonthCode ).
  ELSE IF AVAILABLE FirstBalance THEN
    first-month = FirstBalance.MonthCode.
  ELSE IF AVAILABLE FirstFlow THEN
    first-month = FirstFlow.MonthCode.
  ELSE
    first-month = PrjCashFlow.MonthCode.
      
  /* Apply changes to the appropriate month */
  IF first-month = PrjCashFlow.MonthCode THEN DO:
      IF DEC( PrjCashFlow.Revised:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} ) = 0.00
                     AND first-month > very-first-month THEN
        FIND FIRST ChangeMonth WHERE ChangeMonth.MonthCode > first-month NO-LOCK NO-ERROR.
      ELSE
        FIND FIRST ChangeMonth WHERE ChangeMonth.MonthCode > first-month NO-LOCK NO-ERROR.
      change-month = IF AVAILABLE ChangeMonth THEN ChangeMonth.MonthCode ELSE ?.
  END.
  ELSE
    change-month = first-month.

  IF change-month = ? THEN DO:
    MESSAGE "Error adjusting cash flows" SKIP "they may need to be reset".
    RETURN.
  END.

  FIND ChangeMonth WHERE ChangeMonth.MonthCode = change-month NO-LOCK.

  FIND AccountBalance WHERE
        AccountBalance.EntityType  = "J" AND
        AccountBalance.EntityCode  = ProjectBudget.ProjectCode AND
        AccountBalance.AccountCode = ProjectBudget.AccountCode AND
        AccountBalance.MonthCode   = ChangeMonth.MonthCode
        NO-ERROR.
        
  IF NOT AVAILABLE AccountBalance THEN DO:
    CREATE AccountBalance.
    ASSIGN  AccountBalance.EntityType  = "J"
            AccountBalance.EntityCode  = ProjectBudget.ProjectCode
            AccountBalance.AccountCode = ProjectBudget.AccountCode
            AccountBalance.MonthCode   = ChangeMonth.MonthCode.
  END.

  ASSIGN    AccountBalance.Revised = AccountBalance.Revised - change.
  IF NOT Project.BudgetsFrozen THEN DO:
    AccountBalance.Budget  = AccountBalance.Budget - change .
  END.
    
  IF ChangeMonth.FinancialYearCode = FinancialYear.FinancialYearCode THEN DO:
    DEF BUFFER TheFlow FOR PrjCashFlow.
    FIND TheFlow WHERE TheFlow.MonthCode = change-month.
    TheFlow.Revised = TheFlow.Revised - change.
    IF NOT Project.BudgetsFrozen THEN DO:
      TheFlow.Budget = TheFlow.Budget - change.
    END.
    IF {&BROWSE-NAME}:REFRESH() IN FRAME {&FRAME-NAME} THEN.
  END.

  PrjCashFlow.Budget:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} =
      STRING( PrjCashFlow.Budget, PrjCashFlow.Budget:FORMAT IN BROWSE {&BROWSE-NAME}).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE budget-source-changed V-table-Win 
PROCEDURE budget-source-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Called when the source record for the cash flow information
     has been changed */
  
  IF NOT AVAILABLE ProjectBudget THEN RETURN.
  FIND Project OF ProjectBudget NO-LOCK NO-ERROR.
  IF NOT AVAILABLE Project THEN RETURN.
  
  RUN sensitise-budgets.
  RUN refresh-years.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-cash-flow-query V-table-Win 
PROCEDURE close-cash-flow-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY {&BROWSE-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-current-cash-flows V-table-Win 
PROCEDURE get-current-cash-flows :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH PrjCashFlow: DELETE PrjCashFlow. END.

  IF NOT AVAILABLE FinancialYear OR
     NOT AVAILABLE ProjectBudget OR
     NOT AVAILABLE Project THEN RETURN.
     
  very-first-month = ?.

  FOR EACH Month OF FinancialYear NO-LOCK:

    IF very-first-month = ? AND FinancialYear.FinancialYearCode = very-first-year THEN
      very-first-month = Month.MonthCode.
        
    CREATE PrjCashFlow.
    ASSIGN
      PrjCashFlow.YearCode  = FinancialYear.FinancialYearCode
      PrjCashFlow.MonthCode = Month.MonthCode
      PrjCashFlow.MonthName = Month.MonthName.
      
    FIND AccountBalance WHERE
      AccountBalance.EntityType  = "J"  AND
      AccountBalance.EntityCode  = Project.ProjectCode       AND
      AccountBalance.AccountCode = ProjectBudget.AccountCode AND
      AccountBalance.MonthCode   = Month.MonthCode
      NO-LOCK NO-ERROR.
    
    IF AVAILABLE AccountBalance THEN
    ASSIGN
      PrjCashFlow.Balance = AccountBalance.Balance
      PrjCashFlow.Budget  = AccountBalance.Budget
      PrjCashFlow.Revised = AccountBalance.Revised.

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  this-win = CURRENT-WINDOW.
  RUN top-changed.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN budget-source-changed.
  RUN refresh-window-title IN sys-mgr ( THIS-PROCEDURE ).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-cash-flow-query V-table-Win 
PROCEDURE open-cash-flow-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN close-cash-flow-query.
  RUN get-current-cash-flows.
  
  OPEN QUERY {&BROWSE-NAME} FOR EACH PrjCashFlow.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN assign-cash-flows.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  have-records = No.
  /* If the row has changed and there is already a
     project budget available then svae the current ones */
  IF AVAILABLE ProjectBudget THEN RUN assign-cash-flows.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-years V-table-Win 
PROCEDURE refresh-years :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  ASSIGN cmb_Years:LIST-ITEMS = "".
  
  DEF VAR years-from AS INT  NO-UNDO.
  DEF VAR item       AS CHAR NO-UNDO.
  
  years-from = IF Project.StartDate <> ?
    THEN YEAR( Project.StartDate )
    ELSE YEAR( TODAY ).
  very-first-year = years-from.
    
  FOR EACH FinancialYear WHERE
    FinancialYear.FinancialYearCode >= years-from NO-LOCK:
    item = STRING( FinancialYear.FinancialYearCode ) + " - " + FinancialYear.Description.
    IF cmb_Years:ADD-LAST( item ) THEN.
  END.
  
  cmb_Years:SCREEN-VALUE = cmb_Years:ENTRY(1).
  RUN year-changed.
  
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "AccountCode" "ProjectBudget" "AccountCode"}
  {src/adm/template/sndkycas.i "EntityCode" "ProjectBudget" "EntityCode"}
  {src/adm/template/sndkycas.i "EntityType" "ProjectBudget" "EntityType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ProjectBudget"}
  {src/adm/template/snd-list.i "PrjCashFlow"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-budgets V-table-Win 
PROCEDURE sensitise-budgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*
  PrjCashFlow.Budget:READ-ONLY  IN BROWSE {&BROWSE-NAME} = Project.BudgetsFrozen.    
  PrjCashFlow.Revised:READ-ONLY IN BROWSE {&BROWSE-NAME} = NOT Project.BudgetsFrozen.    
*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE top-changed V-table-Win 
PROCEDURE top-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF INPUT FRAME {&FRAME-NAME} tgl_topmost THEN
    RUN notify( 'set-topmost,container-source':U ).
  ELSE
    RUN notify( 'reset-topmost,container-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-budget V-table-Win 
PROCEDURE verify-budget :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  /* Test to see if we need to delete the budget */
  
  IF DEC( PrjCashFlow.Budget:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} ) = 0.00 THEN
  DO:
    FIND AccountBalance WHERE
      AccountBalance.EntityType  = "J" AND
      AccountBalance.EntityCode  = ProjectBudget.ProjectCode AND
      AccountBalance.AccountCode = ProjectBudget.AccountCode AND
      AccountBalance.MonthCode   = PrjCashFlow.MonthCode
      NO-ERROR.

    IF AVAILABLE AccountBalance AND
       NOT CAN-FIND( FIRST AcctTran OF AccountBalance ) THEN
      DELETE AccountBalance.

  END.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE year-changed V-table-Win 
PROCEDURE year-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  FIND FinancialYear WHERE
    FinancialYear.FinancialYearCode = INT( TRIM( ENTRY( 1, INPUT cmb_Years, "-" ) ) )
    NO-LOCK NO-ERROR.
  RUN open-cash-flow-query.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

