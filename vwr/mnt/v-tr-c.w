&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "Andrew" NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Int1 RP.Int5 RP.Int2 RP.Int6 ~
RP.Log1 RP.Log5 RP.Char3 RP.Log2 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_month1 cmb_month2 btn_Browse Btn_OK ~
RECT-1 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Int1 RP.Int5 RP.Int2 RP.Int6 ~
RP.Log1 RP.Log5 RP.Char3 RP.Log2 
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define DISPLAYED-OBJECTS fil_cred1 fil_comp1 fil_cred2 fil_comp2 ~
cmb_month1 cmb_month2 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */
&Scoped-define ADM-CREATE-FIELDS RP.Log2 
&Scoped-define ADM-ASSIGN-FIELDS RP.Log2 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Browse 
     LABEL "Browse" 
     SIZE 5.72 BY 1.05
     FONT 10.

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 12 BY 1
     BGCOLOR 8 FONT 10.

DEFINE VARIABLE cmb_month1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_month2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_comp1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_comp2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_cred1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_cred2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 72 BY 13.75.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.5 COL 3 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "All creditors", "All":U,
"for a single Creditor", "C1":U,
"for a range of Creditor Codes", "CR":U,
"Creditors of a single company", "L1":U,
"Creditors of a range of companies", "LR":U
          SIZE 31 BY 5
          FONT 10
     RP.Int1 AT ROW 7.2 COL 10 COLON-ALIGNED HELP
          ""
          LABEL "Creditor" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     RP.Int5 AT ROW 7.2 COL 10 COLON-ALIGNED
          LABEL "Company" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_cred1 AT ROW 7.2 COL 19.14 COLON-ALIGNED NO-LABEL
     fil_comp1 AT ROW 7.2 COL 19.14 COLON-ALIGNED NO-LABEL
     RP.Int2 AT ROW 8.2 COL 10 COLON-ALIGNED HELP
          ""
          LABEL "to" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     RP.Int6 AT ROW 8.2 COL 10 COLON-ALIGNED
          LABEL "to" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_cred2 AT ROW 8.2 COL 19.14 COLON-ALIGNED NO-LABEL
     fil_comp2 AT ROW 8.2 COL 19.14 COLON-ALIGNED NO-LABEL
     cmb_month1 AT ROW 9.65 COL 10 COLON-ALIGNED
     cmb_month2 AT ROW 9.65 COL 36.29 COLON-ALIGNED
     RP.Log1 AT ROW 11.25 COL 12 HELP
          ""
          LABEL "Include closed transactions"
          VIEW-AS TOGGLE-BOX
          SIZE 23.43 BY .8
          FONT 10
     RP.Log5 AT ROW 12.25 COL 12
          LABEL "Export to"
          VIEW-AS TOGGLE-BOX
          SIZE 9.14 BY .8
          FONT 10
     RP.Char3 AT ROW 12.25 COL 20.29 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 44 BY 1.05
     btn_Browse AT ROW 12.25 COL 66.29
     RP.Log2 AT ROW 13.25 COL 12 HELP
          ""
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 11.72 BY .8
          FONT 10
     Btn_OK AT ROW 13.45 COL 60
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.6
         WIDTH              = 76.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_comp1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_comp2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_cred1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_cred2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int6 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   1 2 EXP-LABEL EXP-HELP                                               */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Browse V-table-Win
ON CHOOSE OF btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  DISABLE Btn_OK WITH FRAME {&FRAME-NAME}.
  RUN run-report.
  ENABLE Btn_OK WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month1 V-table-Win
ON U1 OF cmb_month1 IN FRAME F-Main /* Period */
DO:
  {inc/selcmb/scmths1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month1 V-table-Win
ON U2 OF cmb_month1 IN FRAME F-Main /* Period */
DO:
  {inc/selcmb/scmths2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U1 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U2 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_comp1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U1 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U2 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U3 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_comp2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U1 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "RP" "Int6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U2 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "RP" "Int6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U3 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "RP" "Int6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_cred1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_cred1 V-table-Win
ON U1 OF fil_cred1 IN FRAME F-Main
DO:
  {inc/selfil/sfcrd1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_cred1 V-table-Win
ON U2 OF fil_cred1 IN FRAME F-Main
DO:
  {inc/selfil/sfcrd2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_cred1 V-table-Win
ON U3 OF fil_cred1 IN FRAME F-Main
DO:
  {inc/selfil/sfcrd3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_cred2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_cred2 V-table-Win
ON U1 OF fil_cred2 IN FRAME F-Main
DO:
  {inc/selfil/sfcrd1.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_cred2 V-table-Win
ON U2 OF fil_cred2 IN FRAME F-Main
DO:
  {inc/selfil/sfcrd2.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_cred2 V-table-Win
ON U3 OF fil_cred2 IN FRAME F-Main
DO:
  {inc/selfil/sfcrd3.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Creditor */
DO:
  {inc/selcde/cdcrd.i "fil_cred1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* to */
DO:
  {inc/selcde/cdcrd.i "fil_cred2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int5 V-table-Win
ON LEAVE OF RP.Int5 IN FRAME F-Main /* Company */
DO:
DO WITH FRAME {&FRAME-NAME}:
  {inc/selcde/cdcmp.i "fil_comp1"}
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int6 V-table-Win
ON LEAVE OF RP.Int6 IN FRAME F-Main /* to */
DO:
  {inc/selcde/cdcmp.i "fil_comp2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log5 V-table-Win
ON VALUE-CHANGED OF RP.Log5 IN FRAME F-Main /* Export to */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose: As its name!
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:
    IF INPUT RP.Char1 = "All" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_cred1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_cred2:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int1 fil_cred1 RP.Int2 fil_cred2 RP.Int5 RP.Int6 fil_comp1 fil_comp2.
    END.
    ELSE IF INPUT RP.Char1 = "CR" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_cred1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_cred2:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = Yes" ).
      VIEW RP.Int1 fil_cred1 RP.Int2 fil_cred2.
      HIDE RP.Int5 RP.Int6 fil_comp1 fil_comp2.
    END.
    ELSE IF INPUT RP.Char1 = "C1" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_cred1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_cred2:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int2 fil_cred2 RP.Int5 RP.Int6 fil_comp1 fil_comp2.
      VIEW RP.Int1 fil_cred1.
    END.
    ELSE IF INPUT RP.Char1 = "LR" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_cred1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_cred2:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = No" ).
      HIDE RP.Int1 fil_cred1 RP.Int2 fil_cred2.
      VIEW RP.Int5 fil_comp1 RP.Int6 fil_comp2.
    END.
    ELSE IF INPUT RP.Char1 = "L1" THEN DO:
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_cred1:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_cred2:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = No" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = Yes" ).
      HIDE RP.Int1 fil_cred1 RP.Int2 fil_cred2 RP.Int6 fil_comp2.
      VIEW RP.Int5 fil_comp1.
    END.
    IF INPUT RP.Log5 THEN DO:
      VIEW RP.Char3 btn_Browse .
      HIDE RP.Log2 .
    END.
    ELSE DO:
      HIDE RP.Char3 btn_Browse .
      VIEW RP.Log2 .
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/
  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  FIND RP WHERE RP.ReportID = "tr-c"
                        AND RP.UserName = user-name
                        NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = "tr-c"
      RP.UserName = user-name
    .
  END.

  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  RUN enable-appropriate-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program.
------------------------------------------------------------------------------*/
DEF VAR c1 AS INT INITIAL 0 NO-UNDO.
DEF VAR c2 AS INT INITIAL 99999 NO-UNDO.
DEF VAR e1 AS INT INITIAL 0 NO-UNDO.
DEF VAR e2 AS INT INITIAL 99999 NO-UNDO.
DEF VAR et AS CHAR INITIAL "C" NO-UNDO.
DEF VAR report-options AS CHAR NO-UNDO.

  RUN dispatch IN THIS-PROCEDURE ('update-record':U).

  DO WITH FRAME {&FRAME-NAME}:
    IF RP.Char1 = "CR" THEN DO:
      ASSIGN      c1 = RP.Int1    c2 = RP.Int2 .
    END.
    ELSE IF RP.Char1 = "C1" THEN DO:
      ASSIGN      c1 = RP.Int1    c2 = RP.Int1 .
    END.
    ELSE IF RP.Char1 = "LR" THEN DO:
      et = "L".
      ASSIGN      e1 = RP.Int5    e2 = RP.Int6 .
    END.
    ELSE IF RP.Char1 = "L1" THEN DO:
      et = "L".
      ASSIGN      e1 = RP.Int5    e2 = RP.Int5 .
    END.
    ELSE IF RP.Char1 = "All" THEN DO:
      FIND FIRST Creditor NO-LOCK.
      c1 = Creditor.CreditorCode.
      FIND LAST Creditor NO-LOCK.
      c2 = Creditor.CreditorCode.
    END.
  END.
  report-options = "EntityType," + et
                 + "~nEntityRange," + STRING(e1) + "," + STRING(e2) 
                 + "~nCreditorRange," + STRING(c1) + "," + STRING(c2) 
                 + "~nMonthRange," + STRING(RP.Int3) + "," + STRING(RP.Int4)
                 + (IF RP.Log1 THEN "~nShow-All" ELSE "")
                 + (IF RP.Log5 THEN "~nExport," + RP.Char3 ELSE "")
                 + (IF RP.Log2 THEN "~nPreview" ELSE "") .

  RUN notify( 'set-busy,container-source':U ).
  RUN process/report/tr-c.p ( report-options ).
  RUN notify( 'set-idle,container-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char3 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated values" "*.CSV"
        SAVE-AS ASK-OVERWRITE CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV"
        INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME UPDATE select-ok.

  IF select-ok THEN DO:
    IF SUBSTRING( file-name, R-INDEX( file-name, ".") ) <> ".CSV" THEN DO:
      IF (LENGTH( file-name ) - R-INDEX( file-name, "." )) < 4 THEN
        file-name = SUBSTRING( file-name, 1, R-INDEX( file-name, ".")) + "CSV" .
    END.
    RP.Char3:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

