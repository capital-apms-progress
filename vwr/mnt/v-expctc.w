&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

&SCOPED-DEFINE REPORT-ID "Contact Export"

DEF VAR cty-list AS CHAR NO-UNDO.
DEF VAR pty-list AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Char6 RP.Log5 RP.Log4 RP.Char5 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Char5 ~{&FP2}Char5 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-28 cmb_PostalType-1 cmb_PostalType-2 ~
cmb_PostalType-3 btn_browse btn_export btn_cancel 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Char6 RP.Log5 RP.Log4 RP.Char5 
&Scoped-Define DISPLAYED-OBJECTS cmb_PostalType-1 cmb_PostalType-2 ~
cmb_PostalType-3 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_browse 
     LABEL "&Browse" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_export 
     LABEL "&Export" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE VARIABLE cmb_PostalType-1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "First" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     SIZE 30.29 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_PostalType-2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Second" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     SIZE 30.29 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_PostalType-3 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Third" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     SIZE 30.29 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-28
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 76 BY 19.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 2.5 COL 3.29 HELP
          "" NO-LABEL
          VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
          SIZE 36 BY 17.5
     RP.Char6 AT ROW 2.9 COL 45.29 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Original Style", "Standard":U,
"For sending to another system", "Transfer":U,
"For loading into Outlook 97", "Outlook97":U
          SIZE 30 BY 2.4
     cmb_PostalType-1 AT ROW 9.9 COL 43.86 COLON-ALIGNED
     cmb_PostalType-2 AT ROW 11.3 COL 43.86 COLON-ALIGNED
     cmb_PostalType-3 AT ROW 12.7 COL 43.86 COLON-ALIGNED
     RP.Log5 AT ROW 16.1 COL 45.86
          LABEL "Split address into separate lines"
          VIEW-AS TOGGLE-BOX
          SIZE 25.72 BY 1
     RP.Log4 AT ROW 17.6 COL 45.86
          LABEL "Include 'No Mailout' contacts"
          VIEW-AS TOGGLE-BOX
          SIZE 24.29 BY .85
     RP.Char5 AT ROW 21 COL 7.57 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 30.86 BY 1
     btn_browse AT ROW 21 COL 41
     btn_export AT ROW 21 COL 56.43
     btn_cancel AT ROW 21 COL 67.29
     RECT-28 AT ROW 1 COL 1.57
     "Contact Types" VIEW-AS TEXT
          SIZE 14.86 BY 1 AT ROW 1.5 COL 3.29
          FONT 14
     "Postal Types" VIEW-AS TEXT
          SIZE 13.72 BY 1 AT ROW 8.5 COL 41.29
          FONT 14
     "Export To:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 21 COL 1.57
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_export.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 22.1
         WIDTH              = 81.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR SELECTION-LIST RP.Char1 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Char5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR RADIO-SET RP.Char6 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_browse V-table-Win
ON CHOOSE OF btn_browse IN FRAME F-Main /* Browse */
DO:  
  RUN browse-file.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_export
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_export V-table-Win
ON CHOOSE OF btn_export IN FRAME F-Main /* Export */
DO:
  SELF:SENSITIVE = No.
  RUN export-contacts.
  SELF:SENSITIVE = Yes.
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char6 V-table-Win
ON VALUE-CHANGED OF RP.Char6 IN FRAME F-Main /* Char6 */
DO:
  RUN export-style-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PostalType-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-1 V-table-Win
ON U1 OF cmb_PostalType-1 IN FRAME F-Main /* First */
DO:
  {inc/selcmb/scpdt1.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-1 V-table-Win
ON U2 OF cmb_PostalType-1 IN FRAME F-Main /* First */
DO:
  {inc/selcmb/scpdt2.i "RP" "Char2"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PostalType-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-2 V-table-Win
ON U1 OF cmb_PostalType-2 IN FRAME F-Main /* Second */
DO:
  {inc/selcmb/scpdt1.i "RP" "Char3"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-2 V-table-Win
ON U2 OF cmb_PostalType-2 IN FRAME F-Main /* Second */
DO:
  {inc/selcmb/scpdt2.i "RP" "Char3"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PostalType-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-3 V-table-Win
ON U1 OF cmb_PostalType-3 IN FRAME F-Main /* Third */
DO:
  {inc/selcmb/scpdt1.i "RP" "Char4"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-3 V-table-Win
ON U2 OF cmb_PostalType-3 IN FRAME F-Main /* Third */
DO:
  {inc/selcmb/scpdt2.i "RP" "Char4"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE browse-file V-table-Win 
PROCEDURE browse-file :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR save-as     AS CHAR NO-UNDO.
  DEF VAR file-chosen AS LOGI NO-UNDO.

  save-as = INPUT FRAME {&FRAME-NAME} RP.Char5.
  IF LENGTH( save-as ) > 0 THEN
  IF LOOKUP( SUBSTR( save-as, LENGTH( save-as ), 1 ), "\,/" ) <> 0 THEN save-as = "".
    
  SYSTEM-DIALOG GET-FILE save-as
    TITLE   "Export Contacts to file"
    FILTERS "Text Files (*.txt *.csv)" "*.txt,*.csv"
    DEFAULT-EXTENSION ".csv"
    SAVE-AS
    RETURN-TO-START-DIR
    USE-FILENAME
    UPDATE file-chosen.

  IF file-chosen THEN RP.Char5:SCREEN-VALUE IN FRAME {&FRAME-NAME} = 
    save-as.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE export-contacts V-table-Win 
PROCEDURE export-contacts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

  IF INPUT FRAME {&FRAME-NAME} RP.Char5 = "" OR
     INPUT FRAME {&FRAME-NAME} RP.Char5 = "" THEN
  DO:
    MESSAGE "You must choose a file to export to!" VIEW-AS ALERT-BOX ERROR
      TITLE "No file name chosen".
    RUN browse-file.
    RETURN.
  END.
  
  RUN dispatch( 'update-record':U ).
  RUN get-cty-list.
  RUN get-pty-list.

  report-options = "ExportFor,CONTACT"
                 + "~nContactTypes," + cty-list
                 + "~nFileName," + RP.Char5
                 + "~nAddressTypes," + pty-list
                 + (IF RP.Log5 THEN "~nSplitAddress" ELSE "")
                 + (IF RP.Log4 THEN "~nIncludeNoMailout" ELSE "")
                 + "~nStyle," + RP.Char6 .

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  IF RP.Char6 = "Transfer" THEN
    RUN process/export/dump-contacts.p ( report-options ).
  ELSE
    RUN process/export/contacts.p ( report-options ).
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

  MESSAGE "Export Complete" VIEW-AS ALERT-BOX INFORMATION TITLE "Done".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE export-style-changed V-table-Win 
PROCEDURE export-style-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  CASE INPUT RP.Char6:
    WHEN "Transfer" THEN
      DISABLE cmb_PostalType-1 cmb_PostalType-2 cmb_PostalType-3 RP.Log5 .
    WHEN "Outlook97" THEN
      DISABLE cmb_PostalType-1 cmb_PostalType-2 cmb_PostalType-3 RP.Log5 .
    OTHERWISE
      ENABLE cmb_PostalType-1 cmb_PostalType-2 cmb_PostalType-3 RP.Log5 .
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-cty-list V-table-Win 
PROCEDURE get-cty-list :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR delim AS CHAR NO-UNDO.
  DEF VAR sv    AS CHAR NO-UNDO.
  DEF VAR i     AS INT  NO-UNDO.
  
  cty-list = "".
  delim    = RP.Char1:DELIMITER IN FRAME {&FRAME-NAME}.
  sv       = RP.Char1:SCREEN-VALUE.
  
  DO i = 1 TO NUM-ENTRIES( sv, delim ):
    cty-list = cty-list + IF cty-list = "" THEN "" ELSE ",".
    cty-list = cty-list + TRIM( ENTRY( 1, ENTRY( i, sv, delim ), "-" ) ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-pty-list V-table-Win 
PROCEDURE get-pty-list :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  pty-list = "".
  IF RP.Char2 <> "" THEN pty-list = TRIM( pty-list + "," + RP.Char2, "," ).
  IF RP.Char3 <> "" THEN pty-list = TRIM( pty-list + "," + RP.Char3, "," ).
  IF RP.Char4 <> "" THEN pty-list = TRIM( pty-list + "," + RP.Char4, "," ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN export-style-changed.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR user-name AS CHAR NO-UNDO.
  
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  RUN update-contact-types.

  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.

  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
  
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Char2    = "MAIN"
      RP.Char3    = "POST".
  END.

  RUN dispatch( 'row-changed':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-contact-types V-table-Win 
PROCEDURE update-contact-types :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR item AS CHAR NO-UNDO.
  
  FOR EACH ContactType NO-LOCK:
    item = STRING( ContactType.ContactType, "X(6)" ) + " - " + ContactType.Description.
    IF RP.Char1:ADD-LAST( item ) IN FRAME {&FRAME-NAME} THEN.
  END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


