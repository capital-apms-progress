&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "Andrew" NO-UNDO.

&GLOB REPORT-ID "rntchg"

DEF VAR manager-list        AS CHAR NO-UNDO.
DEF VAR manager-pd          AS CHAR NO-UNDO.
DEF VAR administrator-list  AS CHAR NO-UNDO.
DEF VAR administrator-pd    AS CHAR NO-UNDO.
DEF VAR region-list         AS CHAR NO-UNDO.
DEF VAR region-pd           AS CHAR NO-UNDO.

DEF VAR consolidation-list  AS CHAR NO-UNDO.
DEF VAR consolidation-pd    AS CHAR NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set-l.i "Use-Rent-Charges" "use-rent-charges"}
IF NOT AVAILABLE(OfficeSetting) THEN use-rent-charges = Yes.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Char5 RP.Int1 RP.Int2 RP.Log1 ~
RP.Log2 RP.Log4 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_Select cmb_month1 Btn_OK RECT-1 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Char5 RP.Int1 RP.Int2 RP.Log1 ~
RP.Log2 RP.Log4 
&Scoped-Define DISPLAYED-OBJECTS cmb_Select cmb_month1 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 12 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE cmb_month1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "10/07/1886" 
     DROP-DOWN-LIST
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Select AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 40.57 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 60 BY 10.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.2 COL 2.72 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "All tenants", "ALL":U,
"Property Range", "RANGE":U,
"Tenant Range", "TENANTS":U,
"Building Manager", "MANAGER":U,
"Consolidation List", "LIST":U,
"Region", "REGION":U
          SIZE 15 BY 4.55
     RP.Char5 AT ROW 1.25 COL 7 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Rent charges", "R":U,
"Outgoings contributions", "O":U,
"Rent and Outgoings", "RO":U
          SIZE 19 BY 2.4
          FONT 10
     RP.Int1 AT ROW 2.25 COL 28 COLON-ALIGNED
          LABEL "From" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     RP.Int2 AT ROW 2.25 COL 40 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     cmb_Select AT ROW 4.25 COL 18 COLON-ALIGNED NO-LABEL
     cmb_month1 AT ROW 6 COL 18 COLON-ALIGNED
     RP.Log1 AT ROW 7.2 COL 3.29 HELP
          ""
          LABEL "Preview report"
          VIEW-AS TOGGLE-BOX
          SIZE 13.72 BY .8
          FONT 10
     RP.Log2 AT ROW 8 COL 3.29 HELP
          ""
          LABEL "Tenant subtotals"
          VIEW-AS TOGGLE-BOX
          SIZE 15.43 BY .8
     RP.Log4 AT ROW 8.8 COL 3.29
          LABEL "Exception report"
          VIEW-AS TOGGLE-BOX
          SIZE 16.57 BY .85
     RP.Log3 AT ROW 10.2 COL 3.29 HELP
          ""
          LABEL "Use old rent charge process"
          VIEW-AS TOGGLE-BOX
          SIZE 27.14 BY .8
     Btn_OK AT ROW 10.2 COL 48.43
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.75
         WIDTH              = 63.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{inc/null.i}
{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   NO-DISPLAY NO-ENABLE EXP-LABEL EXP-HELP                              */
ASSIGN 
       RP.Log3:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  DISABLE {&SELF-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN property-selection-changed.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char5 V-table-Win
ON VALUE-CHANGED OF RP.Char5 IN FRAME F-Main /* Char5 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month1 V-table-Win
ON U1 OF cmb_month1 IN FRAME F-Main /* Period */
DO:
  {inc/selcmb/scmths1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month1 V-table-Win
ON U2 OF cmb_month1 IN FRAME F-Main /* Period */
DO:
  {inc/selcmb/scmths2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log1 V-table-Win
ON VALUE-CHANGED OF RP.Log1 IN FRAME F-Main /* Preview report */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log3 V-table-Win
ON VALUE-CHANGED OF RP.Log3 IN FRAME F-Main /* Use old rent charge process */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose: As its name!
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NOT(use-rent-charges) THEN DO:
    HIDE RP.Char1 RP.Int1 RP.Int2 cmb_Select .
    VIEW RP.Char5.
    ENABLE RP.Char5.
  END.
  ELSE IF INPUT RP.Char1 = "RANGE" THEN DO:
    HIDE RP.Char5 cmb_Select.
    VIEW RP.Char1 RP.Int1 RP.Int2 .
    ENABLE RP.Char1 RP.Int1 RP.Int2 .
  END.
  ELSE IF INPUT RP.Char1 = "TENANTS" THEN DO:
    HIDE RP.Char5 cmb_Select.
    VIEW RP.Char1 RP.Int1 RP.Int2 .
    ENABLE RP.Char1 RP.Int1 RP.Int2 .
  END.
  ELSE IF INPUT RP.Char1 = "ALL" THEN DO:
    HIDE RP.Char5 cmb_Select RP.Int1 RP.Int2 .
    VIEW RP.Char1 .
    ENABLE RP.Char1 .
  END.
  ELSE DO:
    HIDE RP.Char5 RP.Int1 RP.Int2 .
    VIEW RP.Char1 cmb_Select .
    ENABLE RP.Char1 cmb_Select .
  END.

  IF INPUT RP.Log1 AND use-rent-charges THEN DO:
    VIEW RP.Log2 .
    ENABLE RP.Log2 .
  END.
  ELSE
    HIDE RP.Log2 .

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-consolidation V-table-Win 
PROCEDURE get-consolidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF consolidation-list <> "" THEN RETURN.
  
  FOR EACH ConsolidationList NO-LOCK:

    consolidation-list = consolidation-list + IF consolidation-list = "" THEN "" ELSE ",".
    consolidation-list = consolidation-list + ConsolidationList.Description.

    consolidation-pd = consolidation-pd + IF consolidation-pd = "" THEN "" ELSE ",".
    consolidation-pd = consolidation-pd + ConsolidationList.Name.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-managers V-table-Win 
PROCEDURE get-managers :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF manager-list <> "" THEN RETURN.
  
  FOR EACH Property WHERE Property.Active NO-LOCK
    BREAK BY Property.Manager:
    IF FIRST-OF( Property.Manager ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Manager
        NO-LOCK NO-ERROR.

      manager-list = manager-list + IF manager-list = "" THEN "" ELSE ",".
      manager-list = manager-list +
        (IF AVAILABLE Person THEN
            Person.FirstName + " " + Person.LastName + " - " + Person.JobTitle
         ELSE
            (IF Property.Manager > 0 THEN
               "Deleted Person " + STRING(Property.Manager)
             ELSE
               "No Building Manager Assigned") ).

      manager-pd = manager-pd + IF manager-pd = "" THEN "" ELSE ",".
      manager-pd = manager-pd + IF AVAILABLE Person THEN STRING( Person.PersonCode ) ELSE STRING( Property.Manager ).
    END.
  
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-regions V-table-Win 
PROCEDURE get-regions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF region-list <> "" THEN RETURN.
  
  FOR EACH Region NO-LOCK:

    region-list = region-list + IF region-list = "" THEN "" ELSE ",".
    region-list = region-list + Region.Name.

    region-pd = region-pd + IF region-pd = "" THEN "" ELSE ",".
    region-pd = region-pd + Region.Region.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN enable-appropriate-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/
DEF VAR month1-list AS CHAR INITIAL "" NO-UNDO.
DEF VAR delim AS CHAR INITIAL "|" NO-UNDO.

  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  DO TRANSACTION:
    FIND RP WHERE RP.ReportID = {&REPORT-ID} AND RP.UserName = user-name NO-ERROR.
    IF NOT AVAILABLE( RP ) THEN DO:
      CREATE RP.
      ASSIGN RP.ReportID = {&REPORT-ID}
             RP.UserName = user-name .
    END.
    RP.Log3 = NOT(use-rent-charges) .
    FIND CURRENT RP NO-LOCK.
  END.
  
  RUN dispatch ( 'display-fields':U ).
  RUN property-selection-changed.
  RUN dispatch ( 'enable-fields':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE property-selection-changed V-table-Win 
PROCEDURE property-selection-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT RP.Char1.
  
  CASE type:
    WHEN "ALL" THEN.
    WHEN "RANGE" THEN.
    WHEN "MANAGER" THEN
    DO:
      RUN get-managers.
      cmb_Select:LIST-ITEMS   = manager-list.
      cmb_Select:PRIVATE-DATA = manager-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, manager-list ).
    END.
    WHEN "LIST" THEN
    DO:
      RUN get-consolidation.
      cmb_Select:LIST-ITEMS   = consolidation-list.
      cmb_Select:PRIVATE-DATA = consolidation-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, consolidation-list ).
    END.
    WHEN "REGION" THEN
    DO:
      RUN get-regions.
      cmb_Select:LIST-ITEMS   = region-list.
      cmb_Select:PRIVATE-DATA = region-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, region-list ).
    END.
  
  END CASE.
  RUN enable-appropriate-fields.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the rent charge program.
------------------------------------------------------------------------------*/
DEF VAR process-options AS CHAR NO-UNDO.
DEF VAR selection-id   AS CHAR NO-UNDO      INITIAL "".

DO WITH FRAME {&FRAME-NAME}:
  RUN dispatch IN THIS-PROCEDURE ('update-record':U).

  IF RP.Log3 OR RP.Char1 = "ALL" THEN
    selection-id = "".
  ELSE IF RP.Char1 = "RANGE" THEN
    selection-id = ( STRING( RP.Int1 ) + "-" + STRING( RP.Int2 ) ).
  ELSE IF RP.Char1 = "TENANTS" THEN
    selection-id = ( STRING( RP.Int1 ) + "-" + STRING( RP.Int2 ) ).
  ELSE
    selection-id = ENTRY( cmb_Select:LOOKUP( cmb_Select:SCREEN-VALUE ), cmb_Select:PRIVATE-DATA ).

  process-options = RP.Char5 + "~n"                 /* charge type (if applicable) */
                  + null-str(STRING(RP.Int3),"?") + "~n"          /* month */
                  + null-str(STRING(RP.Log1),"?") + "~n"          /* preview */
                  + "All"                           /* frequencies */
                  + "~nProperty," + RP.Char1 + "," + selection-id
                  + (IF RP.Log1 AND RP.Log2 AND NOT RP.Log3 THEN "~nTenantTotals" ELSE "")
                  + (IF RP.Log1 AND RP.Log4 AND NOT RP.Log3 THEN "~nShowDifferences" ELSE "")
                  .

  RUN notify( 'set-busy,container-source':U ).

  IF RP.Log3 THEN
    RUN process/rentchg.p ( process-options ). /* The old process */
  ELSE
    RUN process/rentchrg.p ( process-options ).

  RUN notify( 'set-idle,container-source':U ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

