&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File: vwr\mnt\v-chqrun.w
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

&SCOP REPORT-ID "Cheque Run"

{inc/ofc-this.i}
{inc/ofc-acct.i "CREDITORS" "sundry-creditors"}

DEF VAR creditor-list AS CHAR NO-UNDO.
DEF VAR pdf-support AS LOGICAL INIT YES NO-UNDO.
DEF VAR pdf-output AS LOGICAL INIT NO NO-UNDO.

/* ensure bank account is scoped to entire program */
FIND FIRST BankAccount NO-LOCK NO-ERROR.

{inc/method/m-bankimportfile-gen.i}

{inc/ofc-set.i "PDF-Output-Directory" "pdf-output-directory"}
IF NOT AVAILABLE(OfficeSetting) THEN
    pdf-support = NO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Log5 RP.Int1 RP.Char4 RP.Log2 RP.Log6 ~
RP.Log4 RP.Char1 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_Client cmb_bnkact fil_from fil_to ~
fil_acct-from fil_acct-to fil_due cmb_PaymentStyle fil_startchq fil_Date ~
btn_print cbx_pdfoutput RECT-22 
&Scoped-Define DISPLAYED-FIELDS RP.Log5 RP.Int1 RP.Char4 RP.Log2 RP.Log6 ~
RP.Log4 RP.Char1 
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define DISPLAYED-OBJECTS cmb_Client cmb_bnkact fil_from fil_to ~
fil_acct-from fil_acct-to fil_due cmb_PaymentStyle fil_startchq ~
fil_LastCheque fil_Date cbx_pdfoutput 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_print DEFAULT 
     LABEL "&OK" 
     SIZE 8.57 BY 1
     FONT 9.

DEFINE VARIABLE cbx_pdfoutput AS LOGICAL INITIAL no
     LABEL "PDF remittance"
     VIEW-AS TOGGLE-BOX
     SIZE 14 BY .8 TOOLTIP "Generate the remittance advice from this cheque run a batch of PDF files" NO-UNDO.

DEFINE VARIABLE cmb_bnkact AS CHARACTER FORMAT "X(256)":U 
     LABEL "Bank Account" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 52.57 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Client AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 45.14 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_PaymentStyle AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 44.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_acct-from AS DECIMAL FORMAT "9999.99":U INITIAL 0 
     LABEL "Account from" 
     VIEW-AS FILL-IN 
     SIZE 6.86 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_acct-to AS DECIMAL FORMAT "9999.99":U INITIAL 0 
     LABEL "to" 
     VIEW-AS FILL-IN 
     SIZE 6.86 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_Date AS DATE FORMAT "99/99/9999":U 
     LABEL "Cheque Date" 
     VIEW-AS FILL-IN 
     SIZE 11.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_due AS INTEGER FORMAT ">>9":U INITIAL 10 
     LABEL "Cheques due in" 
     VIEW-AS FILL-IN 
     SIZE 4 BY 1 NO-UNDO.

DEFINE VARIABLE fil_from AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "Creditor from" 
     VIEW-AS FILL-IN 
     SIZE 5.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_LastCheque AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 21.14 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_startchq AS INTEGER FORMAT "999999":U INITIAL 0 
     LABEL "Next Cheque No" 
     VIEW-AS FILL-IN 
     SIZE 7.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_to AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "to" 
     VIEW-AS FILL-IN 
     SIZE 5.72 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 65.72 BY 19.75.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Log5 AT ROW 1.4 COL 4.43 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "All Clients/Owners", no,
"One Client/Owner", yes
          SIZE 16.57 BY 1.6
     cmb_Client AT ROW 2 COL 19 COLON-ALIGNED NO-LABEL
     cmb_bnkact AT ROW 3.4 COL 11.57 COLON-ALIGNED
     RP.Int1 AT ROW 5.4 COL 4.43 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "A range of creditor codes", 1,
"Vouchers charged to a range of projects", 2,
"Vouchers charged to a range of accounts", 3,
"A list of creditor codes", 4
          SIZE 33.14 BY 3.2
     fil_from AT ROW 5.6 COL 49.86 COLON-ALIGNED
     fil_to AT ROW 5.6 COL 58.43 COLON-ALIGNED
     fil_acct-from AT ROW 6.8 COL 47.57 COLON-ALIGNED
     fil_acct-to AT ROW 6.8 COL 57.29 COLON-ALIGNED
     RP.Char4 AT ROW 7.8 COL 20.72 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 43.43 BY 1.05 TOOLTIP "Enter creditor codes separated by commas"
     fil_due AT ROW 9.2 COL 11.57 COLON-ALIGNED
     RP.Log2 AT ROW 9.25 COL 43
          LABEL "Enforce creditor cheque limits"
          VIEW-AS TOGGLE-BOX
          SIZE 23.43 BY 1
     RP.Log6 AT ROW 10.25 COL 43 HELP
          ""
          LABEL "Select my vouchers only"
          VIEW-AS TOGGLE-BOX
          SIZE 21 BY .8 TOOLTIP "Only select Approved vouchers last modified by current user."
     RP.Log4 AT ROW 11 COL 4.43 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "One Payment Style", no,
"Force Payment by", yes
          SIZE 16.57 BY 1.6
     cmb_PaymentStyle AT ROW 11.4 COL 19.57 COLON-ALIGNED NO-LABEL
     fil_startchq AT ROW 13.05 COL 11.57 COLON-ALIGNED
     fil_LastCheque AT ROW 13.05 COL 19 COLON-ALIGNED NO-LABEL
     fil_Date AT ROW 13.05 COL 52.72 COLON-ALIGNED
     RP.Char1 AT ROW 14.25 COL 13.57 NO-LABEL
          VIEW-AS EDITOR NO-WORD-WRAP SCROLLBAR-VERTICAL
          SIZE 52.57 BY 4.6
          FONT 8
     btn_print AT ROW 19.25 COL 57
     cbx_pdfoutput AT ROW 19.45 COL 43
     RECT-22 AT ROW 1 COL 1
     "Select creditors by:" VIEW-AS TEXT
          SIZE 13.14 BY .8 AT ROW 4.6 COL 2.72
     "Message Text:" VIEW-AS TEXT
          SIZE 10.29 BY 1 AT ROW 14.25 COL 3.29
          FONT 10
     "days or less" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 9.2 COL 18.14
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_print.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 22.15
         WIDTH              = 72.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char4 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_LastCheque IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR RADIO-SET RP.Int1 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR RADIO-SET RP.Log4 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR RADIO-SET RP.Log5 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR TOGGLE-BOX RP.Log6 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
DO WITH FRAME {&FRAME-NAME}:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  RUN cheque-run.

  IF RETURN-VALUE = "FAIL" THEN
    RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  ELSE
    RUN dispatch( 'exit':U ).
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbx_pdfoutput
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbx_pdfoutput V-table-Win
ON VALUE-CHANGED OF cbx_pdfoutput IN FRAME F-Main /* OK */
DO:
DO WITH FRAME {&FRAME-NAME}:
  pdf-output = cbx_pdfoutput:CHECKED IN FRAME {&FRAME-NAME}.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_bnkact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact V-table-Win
ON U1 OF cmb_bnkact IN FRAME F-Main /* Bank Account */
DO:
  {inc/selcmb/scchq1.i "RP" "Char5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact V-table-Win
ON U2 OF cmb_bnkact IN FRAME F-Main /* Bank Account */
DO:
  {inc/selcmb/scchq2.i "RP" "Char5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact V-table-Win
ON VALUE-CHANGED OF cmb_bnkact IN FRAME F-Main /* Bank Account */
DO:
  RUN display-last-cheque-no.
  
  /* Display Import File Generation option? */
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Client
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Client V-table-Win
ON U1 OF cmb_Client IN FRAME F-Main
DO:
  {inc/selcmb/scclient1.i "RP" "Char2"}
  RUN display-last-cheque-no.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Client V-table-Win
ON U2 OF cmb_Client IN FRAME F-Main
DO:
  {inc/selcmb/scclient2.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PaymentStyle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON U1 OF cmb_PaymentStyle IN FRAME F-Main
DO:
  {inc/selcmb/scpsty1.i "RP" "Char6"}
  
  /* Display Import File Generation option? */
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON U2 OF cmb_PaymentStyle IN FRAME F-Main
DO:
  {inc/selcmb/scpsty2.i "RP" "Char6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON VALUE-CHANGED OF cmb_PaymentStyle IN FRAME F-Main
DO:
  RUN enable-appropriate-fields.

  /* Display Import File Generation option? */
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Date
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Date V-table-Win
ON LEAVE OF fil_Date IN FRAME F-Main /* Cheque Date */
DO:
  IF INPUT {&SELF-NAME} < TODAY THEN
  DO:
    MESSAGE "The cheque should only be dated in the past" SKIP
            "if you are creating a record of manually produced cheques."
      VIEW-AS ALERT-BOX WARNING TITLE "Cheque date in the past".
/*    DISPLAY {&SELF-NAME} WITH FRAME {&FRAME-NAME}. */
    ASSIGN {&SELF-NAME}.
  END.
  ELSE
    ASSIGN {&SELF-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_from
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_from V-table-Win
ON LEAVE OF fil_from IN FRAME F-Main /* Creditor from */
DO:
  IF SELF:MODIFIED THEN
  DO:
  
    ASSIGN
      {&SELF-NAME}
      fil_to = {&SELF-NAME}.
      
    DISPLAY fil_to WITH FRAME {&FRAME-NAME}.
  
    SELF:MODIFIED = No.
        
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON VALUE-CHANGED OF RP.Int1 IN FRAME F-Main /* Int1 */
DO:
  RUN enable-appropriate-fields.

  /* Display Import File Generation option? */
  RUN enable-import-file-gen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Log4 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log5 V-table-Win
ON VALUE-CHANGED OF RP.Log5 IN FRAME F-Main /* Log5 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

IF NOT pdf-support THEN DO:
  cbx_pdfoutput:VISIBLE IN FRAME {&FRAME-NAME} = NO.
END.
    
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-account-range V-table-Win 
PROCEDURE build-account-range :
/*------------------------------------------------------------------------------
  Purpose:  Build a list of creditors based on vouchers which have been updated
            against a particular range of projects.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER print-date AS DATE NO-UNDO.
DEF OUTPUT PARAMETER creditor-list AS CHAR NO-UNDO.

DEF VAR start-account AS DEC NO-UNDO.
DEF VAR end-account AS DEC NO-UNDO.
DEF VAR cheque-total AS DEC INIT 0.00 NO-UNDO.
DEF VAR c-creditor AS CHAR NO-UNDO.
DEF BUFFER CrdVoucher FOR Voucher.

DO WITH FRAME {&FRAME-NAME}:
  start-account = INPUT fil_acct-from.
  end-account   = INPUT fil_acct-to.
END.

  /* Search by voucher rather than creditor for efficiency */
  creditor-list = "".
  FOR EACH Voucher WHERE Voucher.VoucherStatus = "A"
                AND (Voucher.DateDue <= print-date OR Voucher.DateDue = ?) NO-LOCK:

    c-creditor = STRING( Voucher.CreditorCode ).

    IF LOOKUP( c-creditor, creditor-list ) = 0 THEN DO:

      cheque-total = 0.    
      FOR EACH CrdVoucher WHERE CrdVoucher.CreditorCode = Voucher.CreditorCode
                    AND CrdVoucher.VoucherStatus = "A"
                    AND CrdVoucher.AccountCode >= start-account
                    AND CrdVoucher.AccountCode <= end-account
                    AND (CrdVoucher.DateDue <= print-date
                         OR CrdVoucher.DateDue = ?) NO-LOCK:

        cheque-total = cheque-total + CrdVoucher.GoodsValue + CrdVoucher.TaxValue.    
      END.
    
      IF cheque-total > 0 THEN DO:
        creditor-list = creditor-list + IF creditor-list = "" THEN "" ELSE ",".
        creditor-list = creditor-list + c-creditor.
      END.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-creditor-list V-table-Win 
PROCEDURE build-creditor-list :
/*------------------------------------------------------------------------------
  Purpose:  Build a list of creditors based on an entered list.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER print-date AS DATE NO-UNDO.
DEF OUTPUT PARAMETER creditor-list AS CHAR NO-UNDO.

DEF VAR in-creditor-list AS CHAR NO-UNDO.
DEF VAR cheque-total AS DEC INIT 0.00 NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR creditor-code AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  in-creditor-list = INPUT RP.Char4.
END.

creditor-list = "".
DO i = 1 TO NUM-ENTRIES(in-creditor-list):
  creditor-code = INT( TRIM( ENTRY( i, in-creditor-list ) ) ).
  cheque-total = 0.
  FOR EACH Voucher WHERE Voucher.CreditorCode = creditor-code
                   AND Voucher.VoucherStatus = "A"
                   AND (Voucher.DateDue <= print-date
                        OR Voucher.DateDue = ?) NO-LOCK:
    cheque-total = cheque-total + Voucher.GoodsValue + Voucher.TaxValue.    
  END.
  IF cheque-total > 0 THEN DO:
    creditor-list = creditor-list + IF creditor-list = "" THEN "" ELSE ",".
    creditor-list = creditor-list + STRING(creditor-code).
  END.
  ELSE DO:
    MESSAGE "Creditor " + STRING(creditor-code) ":" SKIP(1)
            "No cheque will be produced as the total would be " + STRING(cheque-total)
            VIEW-AS ALERT-BOX INFORMATION
            TITLE "Cheque Not Required".
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-creditor-range V-table-Win 
PROCEDURE build-creditor-range :
/*------------------------------------------------------------------------------
  Purpose:  Find the creditors with cheques due in a simple range of creditors
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER print-date AS DATE NO-UNDO.
DEF OUTPUT PARAMETER creditor-list AS CHAR NO-UNDO.

DEF VAR start-crdtor AS INT NO-UNDO.
DEF VAR end-crdtor   AS INT NO-UNDO.
DEF VAR c-creditor   AS CHAR NO-UNDO.
DEF VAR cheque-total AS DEC INIT 0.00 NO-UNDO.
DEF BUFFER CrdVoucher FOR Voucher.

  start-crdtor = INPUT FRAME {&FRAME-NAME} fil_from.
  end-crdtor   = INPUT FRAME {&FRAME-NAME} fil_to.
  IF end-crdtor < start-crdtor THEN end-crdtor = start-crdtor.

  /* Search by voucher rather than creditor for efficiency */
  creditor-list = "".
  FOR EACH Voucher WHERE 
    Voucher.VoucherStatus = "A" AND
    ( Voucher.DateDue <= print-date OR Voucher.DateDue = ? ) NO-LOCK:

    c-creditor = STRING( Voucher.CreditorCode ).

    IF Voucher.CreditorCode >= start-crdtor AND
       Voucher.CreditorCode <= end-crdtor   AND
       LOOKUP( c-creditor, creditor-list ) = 0 THEN
    DO:

      cheque-total = 0.    
      FOR EACH CrdVoucher WHERE
        CrdVoucher.CreditorCode = Voucher.CreditorCode AND
        CrdVoucher.VoucherStatus = "A" AND
        ( CrdVoucher.DateDue <= print-date OR CrdVoucher.DateDue = ? ) NO-LOCK:
        cheque-total = cheque-total + CrdVoucher.GoodsValue + CrdVoucher.TaxValue.    
      END.
    
      IF cheque-total > 0 THEN
      DO:
        creditor-list = creditor-list + IF creditor-list = "" THEN "" ELSE ",".
        creditor-list = creditor-list + c-creditor.
      END.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-project-range V-table-Win 
PROCEDURE build-project-range :
/*------------------------------------------------------------------------------
  Purpose:  Build a list of creditors based on vouchers which have been updated
            against a particular range of projects.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER print-date AS DATE NO-UNDO.
DEF OUTPUT PARAMETER creditor-list AS CHAR NO-UNDO.

DEF VAR start-project AS INT NO-UNDO.
DEF VAR end-project AS INT NO-UNDO.
DEF VAR cheque-total AS DEC INIT 0.00 NO-UNDO.
DEF VAR c-creditor AS CHAR NO-UNDO.
DEF BUFFER CrdVoucher FOR Voucher.

DO WITH FRAME {&FRAME-NAME}:
  start-project = INPUT fil_from.
  end-project   = INPUT fil_to.
END.

  /* Search by voucher rather than creditor for efficiency */
  creditor-list = "".
  FOR EACH Voucher WHERE Voucher.VoucherStatus = "A"
                AND (Voucher.DateDue <= print-date OR Voucher.DateDue = ?) NO-LOCK:

    c-creditor = STRING( Voucher.CreditorCode ).

    IF LOOKUP( c-creditor, creditor-list ) = 0 THEN DO:

      cheque-total = 0.    
      FOR EACH CrdVoucher WHERE CrdVoucher.CreditorCode = Voucher.CreditorCode
                    AND CrdVoucher.VoucherStatus = "A"
                    AND CrdVoucher.EntityType = "J"
                    AND CrdVoucher.EntityCode >= start-project
                    AND CrdVoucher.EntityCode <= end-project
                    AND (CrdVoucher.DateDue <= print-date
                         OR CrdVoucher.DateDue = ?) NO-LOCK:

        cheque-total = cheque-total + CrdVoucher.GoodsValue + CrdVoucher.TaxValue.    
      END.
    
      IF cheque-total > 0 THEN DO:
        creditor-list = creditor-list + IF creditor-list = "" THEN "" ELSE ",".
        creditor-list = creditor-list + c-creditor.
      END.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cheque-run V-table-Win 
PROCEDURE cheque-run :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.
DEFINE VARIABLE item-idx AS INTEGER INITIAL 0 NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:            
    item-idx = cmb_PaymentStyle:LOOKUP( INPUT cmb_PaymentStyle ).
    IF item-idx > 0 THEN
        FIND PaymentStyle WHERE ROWID( PaymentStyle ) =
            TO-ROWID( ENTRY( item-idx, cmb_PaymentStyle:PRIVATE-DATA ) ) NO-LOCK NO-ERROR.    

        IF AVAILABLE PaymentStyle THEN DO:
            FIND CURRENT RP SHARE-LOCK.
            RP.Char6 = PaymentStyle.PaymentStyle.
    END.
END.

RUN verify-run.
IF RETURN-VALUE = "FAIL" THEN RETURN "FAIL".

RUN dispatch( 'update-record':U ).

DO WITH FRAME {&FRAME-NAME}:
  report-options = "ChequeDate," + STRING( INPUT fil_Date, "99/99/9999" )
                  + "~nFirstCheque," + STRING( INPUT fil_StartChq )
                  + "~nDueBefore," + STRING( TODAY + INPUT fil_due, "99/99/9999" )
                  + (IF RP.Log2 THEN "~nEnforceLimit" ELSE "")
                  + (IF RP.Log5 THEN "~nOneClient," + RP.Char2 ELSE "")
                  + (IF RP.Log6 THEN "~nMyVouchers," ELSE "")
                  + "~nBankAccount," + RP.Char5
                  + "~nMessage," + REPLACE( RP.Char1, "~n", CHR(7) ) .

  report-options = report-options
                  + (IF RP.Log4 = No THEN "~nPaymentStyle," ELSE "~nForcePaymentBy,")
                  + RP.Char6 .

  CASE RP.Int1:
    WHEN 1 THEN /* "CreditorRange":U */
      report-options = report-options + "~nCreditorRange," + STRING( INPUT fil_from ) + "," + STRING( INPUT fil_to ).
    WHEN 2 THEN /* "ProjectRange":U */
      report-options = report-options + "~nProjectRange," + STRING( INPUT fil_from ) + "," + STRING( INPUT fil_to ).
    WHEN 3 THEN /* "AccountRange":U */
      report-options = report-options + "~nAccountRange," + STRING( INPUT fil_acct-from ) + "," + STRING( INPUT fil_acct-to ).
    WHEN 4 THEN /* "CreditorList":U */
      report-options = report-options + "~nCreditorList," + INPUT RP.Char4 .
  END CASE.

END.

IF pdf-output THEN
  report-options = report-options + "~nOutputPDF".

  RUN process/cheq-run.p ( report-options ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-last-cheque-no V-table-Win 
PROCEDURE display-last-cheque-no :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR bank-id AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  fil_LastCheque = "".

  i = cmb_bnkact:LOOKUP( cmb_bnkact:SCREEN-VALUE ).
  bank-id = ENTRY( i, cmb_bnkact:PRIVATE-DATA ).
  FIND BankAccount WHERE ROWID(BankAccount) = TO-ROWID(bank-id) NO-LOCK NO-ERROR.

  IF AVAILABLE(BankAccount) THEN DO:
    /* the Cheque.Date clause encourages Progress to choose a good index */
    FIND FIRST Cheque WHERE Cheque.BankAccountCode = BankAccount.BankAccountCode
                     AND Cheque.Date <= TODAY + 999 NO-LOCK NO-ERROR.
    IF AVAILABLE(Cheque) THEN
      fil_LastCheque = "(last: " + STRING(Cheque.ChequeNo) + " " + STRING(Cheque.Date, "99/99/9999") + ")".
  END.

  DISPLAY fil_LastCheque.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:  Enable the apprpriate fields based on the chosen SelectionType
------------------------------------------------------------------------------*/
DEFINE VARIABLE payment-styles AS LOGICAL NO-UNDO INITIAL No.

FIND FIRST PaymentStyle NO-LOCK NO-ERROR.
  IF AVAILABLE(PaymentStyle) THEN DO:
    FIND NEXT PaymentStyle NO-LOCK NO-ERROR.
    payment-styles = AVAILABLE(PaymentStyle).
  END.

DO WITH FRAME {&FRAME-NAME}:
  CASE INPUT RP.Int1:
    WHEN 1 THEN DO: /* "CreditorRange" */
      HIDE RP.Char4 fil_acct-from fil_acct-to.
      fil_from:LABEL = "Creditor from".
      VIEW fil_from fil_to.
    END.
    WHEN 2 THEN DO: /* "ProjectRange" */
      HIDE RP.Char4 fil_acct-from fil_acct-to.
      fil_from:LABEL = "Project from".
      VIEW fil_from fil_to.
    END.
    WHEN 3 THEN DO: /* "AccountRange" */
      HIDE RP.Char4 fil_from fil_to.
      VIEW fil_acct-from fil_acct-to.
    END.
    WHEN 4 THEN DO: /* "CreditorList" */
      HIDE fil_from fil_to fil_acct-from fil_acct-to.
      VIEW RP.Char4.
    END.
  END.

  VIEW RP.Log5 RP.Log2.

  IF payment-styles THEN
    VIEW RP.Log4 cmb_PaymentStyle.
  ELSE
    HIDE RP.Log4 cmb_PaymentStyle .

  IF INPUT RP.Log5 THEN 
    VIEW cmb_Client.
  ELSE
    HIDE cmb_Client.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-import-file-gen V-table-Win 
PROCEDURE enable-import-file-gen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Display Import File Generation option? */
/* Payer Bank Account */
DEFINE VARIABLE item-idx AS INTEGER NO-UNDO.
DEFINE VARIABLE v-bankaccount LIKE BankAccount.BankAccount INITIAL 0 NO-UNDO.
DEFINE VARIABLE v-bank AS INTEGER INITIAL 0 NO-UNDO.
DEFINE VARIABLE v-branch AS INTEGER INITIAL 0 NO-UNDO.
DEFINE VARIABLE v-account AS INTEGER INITIAL 0 NO-UNDO.
DEFINE VARIABLE v-suffix AS INTEGER INITIAL 0 NO-UNDO.
/* Payment Style */
DEFINE VARIABLE v-allorone-payment-style AS CHARACTER INITIAL '' NO-UNDO.
DEFINE VARIABLE v-payment-style AS CHARACTER INITIAL '' NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  FIND CURRENT RP SHARE-LOCK NO-ERROR.

  /* Check Payer Bank Account */
  item-idx = cmb_bnkact:LOOKUP( INPUT cmb_bnkact ).
  IF item-idx > 0 THEN DO:
      FIND BankAccount WHERE ROWID( BankAccount ) =
        TO-ROWID( ENTRY( item-idx, cmb_bnkact:PRIVATE-DATA ) ) NO-LOCK NO-ERROR.
    
      IF AVAILABLE BankAccount THEN DO:
          RUN get-bankbranchaccountsuffix (
              BankAccount.BankAccount,
              OUTPUT v-bank,
              OUTPUT v-branch,
              OUTPUT v-account,
              OUTPUT v-suffix
          ).
          /* IF RETURN-VALUE = "FAIL"  THEN RETURN "FAIL". */
      END.
    
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-bank-account V-table-Win 
PROCEDURE get-bank-account :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item-idx AS INT NO-UNDO.

  item-idx = cmb_bnkact:LOOKUP( INPUT cmb_bnkact ).
  IF item-idx = 0 THEN RETURN.
    
  FIND BankAccount WHERE ROWID( BankAccount ) =
    TO-ROWID( ENTRY( item-idx, cmb_bnkact:PRIVATE-DATA ) ) NO-LOCK.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  
  DEF VAR user-name AS CHAR NO-UNDO.
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  
  FIND RP WHERE
    RP.ReportID = {&REPORT-ID} AND
    RP.UserName = user-name
    NO-ERROR.

  {inc/ofc-this.i}
  IF NOT AVAILABLE RP THEN DO:
    CREATE RP.
    ASSIGN RP.ReportID = {&REPORT-ID}
           RP.UserName = user-name .

    RP.Char1 = "Please forward all invoices to:~n" + Office.StreetAddress .

  END.
  
  fil_from = 0.
  fil_to = 99999.

  fil_due  = 10.
  fil_date = TODAY.
  
  DISPLAY
    fil_from
    fil_to
    fil_due
    fil_date
  WITH FRAME {&FRAME-NAME}.

  fil_from:MODIFIED IN FRAME {&FRAME-NAME} = No.
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN enable-appropriate-fields.
  RUN enable-import-file-gen.
  RUN display-last-cheque-no.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE old-cheque-run V-table-Win 
PROCEDURE old-cheque-run :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.
DEF VAR batch-no AS INT NO-UNDO.
DEF VAR start-printing AS LOGICAL NO-UNDO INITIAL Yes.

  RUN get-bank-account.
  RUN verify-run.
  IF RETURN-VALUE = "FAIL" THEN RETURN RETURN-VALUE.
  
  RUN dispatch( 'update-record':U ).
  
  DEF VAR print-date   AS DATE NO-UNDO.
  DEF VAR tran-date    AS DATE NO-UNDO.
  DEF VAR cheque-no    AS INT NO-UNDO.
  DEF VAR start-no     AS INT NO-UNDO.
  DEF VAR end-no       AS INT NO-UNDO.
  DEF VAR n-days       AS INT NO-UNDO.
  DEF VAR n-cheques    AS INT INIT 0 NO-UNDO.
  DEF VAR printed-ok   AS LOGI NO-UNDO.
  DEF VAR cheque-total AS DEC NO-UNDO.
  DEF VAR i            AS INT NO-UNDO.

  n-days     = INPUT FRAME {&FRAME-NAME} fil_due.
  start-no   = INPUT FRAME {&FRAME-NAME} fil_startchq.
  print-date = TODAY + n-days.
  tran-date  = INPUT FRAME {&FRAME-NAME} fil_Date .

  cheque-no = start-no.
  
cheque-run:
DO TRANSACTION ON ERROR UNDO cheque-run, RETURN:

  /* Create the batch */
  CREATE NewBatch.
  ASSIGN
    NewBatch.BatchType   = 'NORM'
    NewBatch.Description = "Cheque Batch - " + STRING( TODAY, "99/99/9999" ).

  batch-no = NewBatch.BatchCode .
  n-cheques = NUM-ENTRIES( creditor-list ).
  
  DO i = 1 TO n-cheques:  

    FIND FIRST Creditor WHERE Creditor.CreditorCode = INT( ENTRY( i, creditor-list ) )
      NO-LOCK.

    CREATE NewDocument.
    ASSIGN
      NewDocument.BatchCode     = batch-no
      NewDocument.DocumentType  = "CHEQ"
      NewDocument.Description   = Creditor.PayeeName
      NewDocument.Reference     = STRING( cheque-no ).

    CREATE Cheque.
    ASSIGN
      Cheque.BatchCode          = batch-no
      Cheque.DocumentCode       = NewDocument.DocumentCode
      Cheque.BankAccountCode    = BankAccount.BankAccountCode
      Cheque.ChequeNo           = cheque-no
      Cheque.CreditorCode       = Creditor.CreditorCode
      Cheque.Date               = INPUT FRAME {&FRAME-NAME} fil_Date
      Cheque.PayeeName          = Creditor.PayeeName.

    cheque-total = 0.
    FOR EACH Voucher OF Creditor WHERE
      Voucher.VoucherStatus = "A" AND
      ( Voucher.DateDue <= print-date OR Voucher.DateDue = ? ) EXCLUSIVE-LOCK:

      ASSIGN
        Voucher.VoucherStatus        = "P"
        Voucher.BankAccountCode      = Cheque.BankAccountCode
        Voucher.ChequeNo             = Cheque.ChequeNo

      cheque-total = cheque-total + Voucher.GoodsValue + Voucher.TaxValue.
    END.

    /* Debit the creditor */
    CREATE NewAcctTrans.
    ASSIGN
      NewAcctTrans.BatchCode       = batch-no
      NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
      NewAcctTrans.EntityType      = "C"
      NewAcctTrans.EntityCode      = Creditor.CreditorCode
      NewAcctTrans.AccountCode     = sundry-creditors
      NewAcctTrans.Amount          = cheque-total
      NewAcctTrans.Date            = tran-date
      NewAcctTrans.Description     = ""
      NewAcctTrans.Reference       = "".

    ASSIGN
      Cheque.Amount             = cheque-total.

    end-no    = cheque-no.
    cheque-no = cheque-no + 1.
    
    /* Credit the bank account */
    CREATE NewAcctTrans.
    ASSIGN
      NewAcctTrans.BatchCode       = batch-no
      NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
      NewAcctTrans.EntityType      = "L"
      NewAcctTrans.EntityCode      = BankAccount.CompanyCode
      NewAcctTrans.AccountCode     = BankAccount.AccountCode
      NewAcctTrans.Amount          = cheque-total * -1
      NewAcctTrans.Date            = tran-date
      NewAcctTrans.Description     = ""
      NewAcctTrans.Reference       = "".
        
  END.

  MESSAGE "There are" n-cheques "cheques to printed." SKIP
          "Start printing Now ?" SKIP(2)
          "The printer will wait for you to insert cheque paper."
          VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO 
          TITLE "Start Printing ?" UPDATE start-printing.
    
  IF NOT start-printing THEN UNDO cheque-run, RETURN.
END.

  report-options = "BankAccount," + BankAccount.BankAccountCode
                 + "~nChequeRange," + STRING(start-no) + "," + STRING(end-no)
                 + "~nMessage," + REPLACE( RP.Char1, "~n", CHR(7)).
  IF pdf-output THEN
    report-options = report-options + "~nOutputPDF".

  RUN process/report/chqprt.p( report-options ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sort-by-creditor-name V-table-Win 
PROCEDURE sort-by-creditor-name :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT  PARAMETER in-list  AS CHAR NO-UNDO.
  DEF OUTPUT PARAMETER out-list AS CHAR NO-UNDO.

  DEF VAR sel_sort AS HANDLE NO-UNDO.
  DEF VAR i        AS INT  NO-UNDO.
  DEF VAR delim    AS CHAR NO-UNDO INIT "~~".
  /* HACK - Create a selection list to do the sorting */
  
  CREATE SELECTION-LIST sel_sort
  ASSIGN
    FRAME = FRAME {&FRAME-NAME}:HANDLE
    HIDDEN = Yes
    DELIMITER = delim
    SORT   = Yes.
  
  sel_sort:LIST-ITEMS = "".
  
  DO i = 1 TO NUM-ENTRIES( in-list ):
    FIND Creditor WHERE Creditor.CreditorCode = INT( ENTRY( i, in-list ) )
      NO-LOCK NO-ERROR.
    IF AVAILABLE Creditor THEN
      IF sel_sort:ADD-LAST( Creditor.Name + "|" + ENTRY( i, in-list ) ) THEN.
  END.
  
  DO i = 1 TO NUM-ENTRIES( sel_sort:LIST-ITEMS, delim ):
    out-list = TRIM( out-list + "," + ENTRY( 2, sel_sort:ENTRY( i ), "|" ), "," ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-bank-accounts V-table-Win 
PROCEDURE update-bank-accounts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR item    AS CHAR NO-UNDO.
  DEF VAR id-list AS CHAR NO-UNDO.
  DEF VAR default-account AS CHAR NO-UNDO.
  cmb_bnkact:LIST-ITEMS IN FRAME {&FRAME-NAME} = "".

  FIND Office WHERE Office.ThisOffice NO-LOCK.
  FIND OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "PYMTBANK" NO-LOCK NO-ERROR.

  FOR EACH BankAccount NO-LOCK:
    item = STRING( BankAccount.CompanyCode, ">>>>9" )   + ' ' + 
           STRING( BankAccount.AccountCode, "9999.99" ) + ' ' +
                   BankAccount.AccountName.
    IF cmb_bnkact:ADD-LAST( item ) THEN.
    id-list = id-list + IF id-list = "" THEN "" ELSE ",".
    id-list = id-list + STRING( ROWID( BankAccount ) ).
    IF AVAILABLE(OfficeControlAccount) AND BankAccount.BankAccountCode = OfficeControlAccount.Description THEN default-account = item.
  END.
  
  cmb_bnkact:PRIVATE-DATA = id-list.
  cmb_bnkact:SCREEN-VALUE = IF default-account <> "" THEN default-account ELSE
    ENTRY( 1, cmb_bnkact:LIST-ITEMS ).
  RUN get-bank-account.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-run V-table-Win 
PROCEDURE verify-run :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR print-date   AS DATE NO-UNDO.
DEF VAR n-days       AS INT  NO-UNDO.
DEF VAR i            AS INT  NO-UNDO.
DEF VAR start-cheque AS INT NO-UNDO.
DEF VAR end-cheque   AS INT NO-UNDO.
  
DO WITH FRAME {&FRAME-NAME}:
  start-cheque = INPUT fil_startchq.
  n-days       = INPUT fil_due.
END.
  print-date   = TODAY + n-days.

  IF start-cheque = 0 THEN DO:
    MESSAGE "You must enter a starting cheque number."
      VIEW-AS ALERT-BOX ERROR TITLE "No starting cheque entered".
    APPLY 'ENTRY':U TO fil_startchq.
    RETURN "FAIL".
  END.

  CASE INPUT RP.Int1:
    WHEN 1 THEN RUN build-creditor-range( print-date, OUTPUT creditor-list ). /* "CreditorRange" */
    WHEN 2 THEN RUN build-project-range( print-date, OUTPUT creditor-list ). /* "ProjectRange" */
    WHEN 3 THEN RUN build-account-range( print-date, OUTPUT creditor-list ). /* "AccountRange" */
    WHEN 4 THEN RUN build-creditor-list( print-date, OUTPUT creditor-list ). /* "CreditorList" */
    OTHERWISE DO:
      MESSAGE "Some form of selection must be chosen!"
                VIEW-AS ALERT-BOX ERROR TITLE "Select Range Type".
      /*APPLY 'ENTRY':U TO RP.Char3.*/
      RETURN "FAIL".
    END.
  END.

  RUN sort-by-creditor-name( INPUT creditor-list, OUTPUT creditor-list ).
  
  end-cheque = start-cheque + NUM-ENTRIES( creditor-list ) - 1.
  IF CAN-FIND( FIRST Cheque WHERE
    Cheque.BankAccountCode = BankAccount.BankAccountCode AND
    Cheque.ChequeNo >= start-cheque AND Cheque.ChequeNo <= end-cheque ) THEN
  DO:
    MESSAGE
      "Cheque numbers could not be assigned because" SKIP
      "one or more of the required numbers has already" SKIP
      "been used." SKIP(2)
      "You need to change the starting cheque number."
    VIEW-AS ALERT-BOX ERROR TITLE "Cheque numbers already allocated".
    APPLY 'ENTRY':U TO fil_startchq IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.


  IF NUM-ENTRIES( creditor-list ) = 0 THEN DO:
    MESSAGE "There are no cheques to be printed !" VIEW-AS ALERT-BOX INFORMATION.
    RETURN "FAIL".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

