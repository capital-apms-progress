&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&SCOPED-DEFINE REPORT-ID "Periodic Commentary"

DEF VAR entity-description AS CHAR NO-UNDO.
DEF VAR report-id          AS CHAR NO-UNDO.
DEF VAR can-edit-comment   AS LOGI NO-UNDO INIT No.
DEF VAR pd AS CHAR NO-UNDO.

DEF BUFFER CommentDetail FOR PeriodicDetail.

DEF VAR last-detail-rowid AS ROWID NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME BROWSE-1

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP PeriodicDetail
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP, PeriodicDetail.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES PeriodicDetail

/* Definitions for BROWSE BROWSE-1                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-1 entity-description @ entity-description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-1   
&Scoped-define FIELD-PAIRS-IN-QUERY-BROWSE-1
&Scoped-define SELF-NAME BROWSE-1
&Scoped-define OPEN-QUERY-BROWSE-1 OPEN QUERY BROWSE-1 FOR EACH PeriodicDetail NO-LOCK.
&Scoped-define TABLES-IN-QUERY-BROWSE-1 PeriodicDetail
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-1 PeriodicDetail


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-BROWSE-1}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 
&Scoped-define FIELD-PAIRS
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-31 BROWSE-1 btn_delete edt-comment 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 
&Scoped-Define DISPLAYED-OBJECTS fil_CurrentDate fil_B1 fil_B2 fil_B3 ~
fil_Statistic fil_CommentDate edt-comment 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_delete 
     LABEL "&Delete Comment" 
     SIZE 14.57 BY 1.05
     FONT 9.

DEFINE VARIABLE edt-comment AS CHARACTER 
     VIEW-AS EDITOR NO-WORD-WRAP SCROLLBAR-HORIZONTAL SCROLLBAR-VERTICAL
     SIZE 84 BY 6.4
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Statistic AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 84 BY 2.4
     BGCOLOR 16 FGCOLOR 1 FONT 10 NO-UNDO.

DEFINE VARIABLE fil_B1 AS CHARACTER FORMAT "X(50)" 
     LABEL "B1" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1
     FONT 10.

DEFINE VARIABLE fil_B2 AS CHARACTER FORMAT "X(50)" 
     LABEL "B2" 
     VIEW-AS FILL-IN 
     SIZE 17.72 BY 1
     FONT 10.

DEFINE VARIABLE fil_B3 AS CHARACTER FORMAT "X(50)" 
     LABEL "B3" 
     VIEW-AS FILL-IN 
     SIZE 18.57 BY 1
     FONT 10.

DEFINE VARIABLE fil_CommentDate AS DATE FORMAT "99/99/9999":U 
     LABEL "Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fil_CurrentDate AS DATE FORMAT "99/99/9999":U 
     LABEL "Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 85.14 BY 23.6.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-1 FOR 
      PeriodicDetail SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-1 V-table-Win _FREEFORM
  QUERY BROWSE-1 NO-LOCK DISPLAY
      entity-description @ entity-description FORMAT "X(80)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 84 BY 10.5
         FONT 8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_CurrentDate AT ROW 1.2 COL 69.29 COLON-ALIGNED
     RP.Char1 AT ROW 1.4 COL 7 COLON-ALIGNED
          LABEL "Report" FORMAT "X(256)"
          VIEW-AS COMBO-BOX SORT INNER-LINES 12
          LIST-ITEMS "CAPX - Approved CapEx Programmes","LEXP - Lease Expiries","NLSE - New Leases and Renewals","RVWO - Rent Reviews Outstanding","RVRN - Rent Reviews and Renewals","VCNT - Rental Space Vacancies" 
          SIZE 51 BY 1
     BROWSE-1 AT ROW 3.2 COL 1.57
     fil_B1 AT ROW 13.6 COL 13.29 COLON-ALIGNED
     fil_B2 AT ROW 13.6 COL 43.29 COLON-ALIGNED
     fil_B3 AT ROW 13.6 COL 64.72 COLON-ALIGNED
     fil_Statistic AT ROW 14.6 COL 1.57 NO-LABEL
     fil_CommentDate AT ROW 17 COL 16.86 COLON-ALIGNED
     btn_delete AT ROW 17 COL 70.72
     edt-comment AT ROW 18 COL 1.57 NO-LABEL
     "Comment" VIEW-AS TEXT
          SIZE 6.86 BY 1 AT ROW 17 COL 1.57
          FGCOLOR 1 FONT 10
     "Statistic" VIEW-AS TEXT
          SIZE 5.72 BY .8 AT ROW 13.8 COL 1.57
          FGCOLOR 1 FONT 10
     "Current Details" VIEW-AS TEXT
          SIZE 10.43 BY .8 AT ROW 2.4 COL 1.57
          FGCOLOR 1 FONT 10
     RECT-31 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP,TTPL.PeriodicDetail
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 26.7
         WIDTH              = 87.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB BROWSE-1 Char1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       BROWSE-1:MAX-DATA-GUESS IN FRAME F-Main     = 300.

/* SETTINGS FOR COMBO-BOX RP.Char1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN fil_B1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_B2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_B3 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_CommentDate IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_CurrentDate IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR fil_Statistic IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       fil_Statistic:READ-ONLY IN FRAME F-Main        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-1
/* Query rebuild information for BROWSE BROWSE-1
     _START_FREEFORM
OPEN QUERY BROWSE-1 FOR EACH PeriodicDetail NO-LOCK.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is OPENED
*/  /* BROWSE BROWSE-1 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME BROWSE-1
&Scoped-define SELF-NAME BROWSE-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-1 V-table-Win
ON ROW-DISPLAY OF BROWSE-1 IN FRAME F-Main
DO:
  RUN set-description.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-1 V-table-Win
ON VALUE-CHANGED OF BROWSE-1 IN FRAME F-Main
DO:
  RUN detail-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_delete V-table-Win
ON CHOOSE OF btn_delete IN FRAME F-Main /* Delete Comment */
DO:
  RUN delete-comment.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Report */
DO:
  RUN report-changed.
  APPLY 'ENTRY':U TO BROWSE BROWSE-1.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edt-comment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edt-comment V-table-Win
ON LEAVE OF edt-comment IN FRAME F-Main
DO:
  RUN update-comment.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}
  {src/adm/template/row-list.i "PeriodicDetail"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}
  {src/adm/template/row-find.i "PeriodicDetail"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-detail-query V-table-Win 
PROCEDURE close-detail-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY {&BROWSE-NAME}.
  RUN detail-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-comment V-table-Win 
PROCEDURE delete-comment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT CommentDetail EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE CommentDetail THEN DELETE CommentDetail.
  RUN detail-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE detail-changed V-table-Win 
PROCEDURE detail-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  can-edit-comment = No.
  ASSIGN fil_Statistic = "" edt-comment = "" fil_CommentDate = ?
         fil_B1 = "" fil_B2 = "" fil_B3 = "" .
  
  last-detail-rowid = ?.
  IF AVAILABLE PeriodicDetail THEN DO:
  
    fil_Statistic = REPLACE( PeriodicDetail.Data, "|", "~~" ).
    fil_B1 = PeriodicDetail.BreakValue1.
    fil_B2 = PeriodicDetail.BreakValue2.
    fil_B3 = PeriodicDetail.BreakValue3.
  
    FIND LAST CommentDetail WHERE
      CommentDetail.ReportID   = PeriodicDetail.ReportID AND
      CommentDetail.EntityID   = PeriodicDetail.EntityID AND
      CommentDetail.DetailType = "C"
      NO-LOCK NO-ERROR.

    can-edit-comment = AVAILABLE CommentDetail AND CommentDetail.Date = TODAY.
    IF AVAILABLE CommentDetail THEN
      ASSIGN
        edt-comment = CommentDetail.Data
        fil_CommentDate = CommentDetail.Date.
    ELSE
      edt-comment = "".

    last-detail-rowid = ROWID(PeriodicDetail).
  END.

  RUN sensitise-comment.
  DISPLAY fil_Statistic edt-comment fil_CommentDate
          fil_B1 fil_B2 fil_B3
          WITH FRAME {&FRAME-NAME}.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN report-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  
  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.
    
  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Char1 = RP.Char1:ENTRY(1).
  END.

  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'display-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-detail-query V-table-Win 
PROCEDURE open-detail-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'set-busy, CONTAINER-SOURCE':u ).
  RUN close-detail-query.

  fil_CurrentDate = ?.
  report-id = TRIM( ENTRY( 1, INPUT FRAME {&FRAME-NAME} RP.Char1, "-" ) ).

  entity-description:LABEL IN BROWSE {&BROWSE-NAME} = 
    IF LOOKUP( report-id, "VCNT" )      <> 0 THEN "Rental Space"  ELSE
    IF LOOKUP( report-id, "LEXP,NLSE" ) <> 0 THEN "Tenancy Lease" ELSE
    IF LOOKUP( report-id, "CAPX" )      <> 0 THEN "Capex"         ELSE
      "Description".
    
    
  /* Get the date of the most recent r-u-n */
  DEF BUFFER CurrentDetail FOR PeriodicDetail.
  
  FIND LAST CurrentDetail WHERE
    CurrentDetail.ReportID   = report-id AND
    CurrentDetail.DetailType = "D"
  NO-LOCK NO-ERROR.
    
  IF AVAILABLE CurrentDetail THEN
  DO:
    fil_CurrentDate = CurrentDetail.Date.
    
    OPEN QUERY {&BROWSE-NAME} FOR EACH PeriodicDetail NO-LOCK WHERE
      PeriodicDetail.ReportId   = report-id AND
      PeriodicDetail.Date       = CurrentDetail.Date AND
      PeriodicDetail.DetailType = "D".
    
    RUN detail-changed.    
  END.

  DISPLAY fil_CurrentDate WITH FRAME {&FRAME-NAME}.
  RUN notify( 'set-idle, CONTAINER-SOURCE':u ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN update-comment.
  RUN dispatch( 'update-record':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-changed V-table-Win 
PROCEDURE report-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN open-detail-query.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}
  {src/adm/template/snd-list.i "PeriodicDetail"}
  {src/adm/template/snd-list.i "PeriodicDetail"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-comment V-table-Win 
PROCEDURE sensitise-comment :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  edt-comment:SENSITIVE = Yes.
  btn_delete:SENSITIVE  = can-edit-comment.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-description V-table-Win 
PROCEDURE set-description :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF PeriodicDetail.DetailType = "C" THEN RETURN.

  entity-description = "".
  CASE report-id:
    
    WHEN "VCNT" THEN DO:

      FIND RentalSpace WHERE
        RentalSpace.PropertyCode    = INT( ENTRY( 1, PeriodicDetail.EntityID ) ) AND
        RentalSpace.RentalSpaceCode = INT( ENTRY( 2, PeriodicDetail.EntityID ) )
      NO-LOCK NO-ERROR.

      IF AVAILABLE RentalSpace THEN DO:
        FIND Property OF RentalSpace NO-LOCK.
        entity-description = STRING( Property.Name, "X(25)" ) + " - " + RentalSpace.Description.
      END.

    END.

    WHEN "CAPX" THEN DO:
      FIND Project WHERE Project.ProjectCode = INT( PeriodicDetail.EntityID )
        NO-LOCK NO-ERROR.
      entity-description = IF AVAILABLE Project THEN
        STRING( Project.ProjectCode, "99999" ) + " " + Project.Name ELSE "".
    END.
    WHEN 'LEXP' THEN DO:       /* lease expiry */
      ASSIGN entity-description = "Expiry"
                    + ": " + ENTRY( 15, PeriodicDetail.Data, "|")
                    + ",  " + ENTRY( 1, PeriodicDetail.Data, "|")
                    + ",  " + ENTRY( 2, PeriodicDetail.Data, "|")
                    + ",  " + ENTRY( 3, PeriodicDetail.Data, "|") NO-ERROR.
    END.
    
    OTHERWISE DO:       /* Other lease-based information */
      ASSIGN entity-description = ENTRY( 24, PeriodicDetail.Data, "|")
                    + ": " + ENTRY( 23, PeriodicDetail.Data, "|")
                    + ",  " + ENTRY( 1, PeriodicDetail.Data, "|")
                    + ",  " + ENTRY( 2, PeriodicDetail.Data, "|")
                    + ",  " + ENTRY( 3, PeriodicDetail.Data, "|") NO-ERROR.
    END.

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-comment V-table-Win 
PROCEDURE update-comment :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR new-comment AS LOGI NO-UNDO INITIAL No.

DEF BUFFER LastDetail FOR PeriodicDetail.
  IF last-detail-rowid = ? THEN RETURN.
  FIND LastDetail WHERE ROWID(LastDetail) = last-detail-rowid NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(LastDetail) THEN RETURN.

  FIND CURRENT CommentDetail NO-ERROR.

  ASSIGN FRAME {&FRAME-NAME} edt-comment.

  IF NOT AVAILABLE(CommentDetail) AND (edt-comment = "") THEN RETURN.
  IF AVAILABLE(CommentDetail)
        AND CommentDetail.Date <> TODAY 
        AND edt-comment = CommentDetail.Data
  THEN
    RETURN.     /* not changed */


  IF NOT AVAILABLE(CommentDetail) OR CommentDetail.Date <> TODAY THEN DO:
    CREATE CommentDetail.
    ASSIGN  CommentDetail.ReportID   = LastDetail.ReportID
            CommentDetail.Date       = TODAY
            CommentDetail.EntityID   = LastDetail.EntityID
            CommentDetail.DetailType = "C"
            CommentDetail.Data       = edt-comment.
    RUN detail-changed.
  END.
  ELSE IF AVAILABLE(CommentDetail) THEN
    CommentDetail.Data = edt-comment.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


