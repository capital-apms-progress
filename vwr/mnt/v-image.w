&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO INITIAL "view".

{inc/ofc-this.i}
{inc/ofc-set.i "Base-Image-Directory" "image-root" "ERROR"}

ON ASSIGN OF Image.PropertyCode DO:
DEF BUFFER LastImage FOR Image.
  FIND LAST LastImage WHERE LastImage.PropertyCode = Image.PropertyCode NO-LOCK NO-ERROR.
  Image.ImageCode = 1 + (IF AVAILABLE(LastImage) THEN LastImage.ImageCode ELSE 0).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Image
&Scoped-define FIRST-EXTERNAL-TABLE Image


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Image.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Image.ImageCode Image.PropertyCode ~
Image.RentalSpaceCode Image.ImageData 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}ImageCode ~{&FP2}ImageCode ~{&FP3}~
 ~{&FP1}PropertyCode ~{&FP2}PropertyCode ~{&FP3}~
 ~{&FP1}RentalSpaceCode ~{&FP2}RentalSpaceCode ~{&FP3}~
 ~{&FP1}ImageData ~{&FP2}ImageData ~{&FP3}
&Scoped-define ENABLED-TABLES Image
&Scoped-define FIRST-ENABLED-TABLE Image
&Scoped-Define ENABLED-OBJECTS RECT-1 IMAGE-1 cmb_ImageType bnt_Browse 
&Scoped-Define DISPLAYED-FIELDS Image.PropertyCode Image.RentalSpaceCode ~
Image.ImageData 
&Scoped-Define DISPLAYED-OBJECTS cmb_ImageType edt_Description 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON bnt_Browse 
     LABEL "Browse" 
     SIZE 9.57 BY 1.

DEFINE VARIABLE cmb_ImageType AS CHARACTER FORMAT "X(256)":U 
     LABEL "File type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     SIZE 52 BY 1 NO-UNDO.

DEFINE VARIABLE edt_Description AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 53.72 BY 7.4 NO-UNDO.

DEFINE IMAGE IMAGE-1
     FILENAME "adeicon/blank":U
     SIZE 21.14 BY 7.4.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 76 BY 12.4.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Image.ImageCode AT ROW 1.2 COL 65.29 COLON-ALIGNED
          LABEL "Image code"
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
     Image.PropertyCode AT ROW 1.4 COL 12.14 COLON-ALIGNED
          LABEL "Property"
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
     Image.RentalSpaceCode AT ROW 1.4 COL 36.14 COLON-ALIGNED
          LABEL "Rental Space"
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
     cmb_ImageType AT ROW 3.2 COL 12.14 COLON-ALIGNED
     Image.ImageData AT ROW 4.4 COL 1.57
          LABEL "Image Reference"
          VIEW-AS FILL-IN 
          SIZE 52 BY 1
     bnt_Browse AT ROW 4.4 COL 66.72
     edt_Description AT ROW 5.8 COL 1.57 NO-LABEL
     RECT-1 AT ROW 1 COL 1
     IMAGE-1 AT ROW 5.8 COL 55.29
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Image
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 15.75
         WIDTH              = 93.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR EDITOR edt_Description IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       edt_Description:RETURN-INSERTED IN FRAME F-Main  = TRUE
       edt_Description:READ-ONLY IN FRAME F-Main        = TRUE.

/* SETTINGS FOR FILL-IN Image.ImageCode IN FRAME F-Main
   NO-DISPLAY EXP-LABEL                                                 */
ASSIGN 
       Image.ImageCode:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN Image.ImageData IN FRAME F-Main
   ALIGN-L EXP-LABEL                                                    */
/* SETTINGS FOR FILL-IN Image.PropertyCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Image.RentalSpaceCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME bnt_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bnt_Browse V-table-Win
ON CHOOSE OF bnt_Browse IN FRAME F-Main /* Browse */
DO:
  RUN browse-for-file.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_ImageType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ImageType V-table-Win
ON U1 OF cmb_ImageType IN FRAME F-Main /* File type */
DO:
  {inc/selcmb/scimgtyp1.i "Image" "ImageType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ImageType V-table-Win
ON U2 OF cmb_ImageType IN FRAME F-Main /* File type */
DO:
  {inc/selcmb/scimgtyp2.i "Image" "ImageType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Image"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Image"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE browse-for-file V-table-Win 
PROCEDURE browse-for-file :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} Image.ImageData .
  start-dir = image-root.

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Image Files" "*.JPG,*.GIF,*.TIF,*.BMP,*.WMF" MUST-EXIST
        INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "Select an image file" USE-FILENAME  UPDATE select-ok.

  IF select-ok THEN DO:
    Image.ImageData:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = 'Add' THEN RUN delete-image. ELSE RUN check-modified( 'CLEAR':U ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN verify-image.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).
  IF mode = "Add" THEN
    RUN notify( 'open-query, RECORD-SOURCE':U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-image V-table-Win 
PROCEDURE delete-image :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Image EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Image THEN DELETE Image.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-thumbnail V-table-Win 
PROCEDURE display-thumbnail :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR image-path AS CHAR NO-UNDO.
DEF VAR pos AS INT NO-UNDO.
DEF VAR success AS LOGI NO-UNDO.

  IF NOT AVAILABLE(Image) THEN RETURN.
DO WITH FRAME {&FRAME-NAME}:
  image-path = REPLACE( Image.ImageData, "/", "\").
  pos = R-INDEX( image-path, "\" ).
  image-path = SUBSTRING(image-path, 1, pos) + "thumb\" + SUBSTRING(image-path, pos + 1).
/*  MESSAGE "Trying thumbnail:" SKIP image-path. */
  success = IMAGE-1:LOAD-IMAGE( image-path ) NO-ERROR.
  IF NOT(success) THEN DO:
    image-path = REPLACE( Image.ImageData, "/", "\").
    pos = R-INDEX( image-path, "." ).
    IF SUBSTRING( image-path, pos + 1) = "bmp" THEN DO:
/*      MESSAGE "Trying bitmap:" SKIP image-path. */
      success = IMAGE-1:LOAD-IMAGE( image-path ) NO-ERROR.
    END.
  END.

/*  MESSAGE "Success:" success. */
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF TRIM( INPUT edt_Description ) = "" THEN DO:
    IF Image.Description <> 0 AND Image.Description <> ? THEN DO:
      FIND Note WHERE Note.NoteCode = Image.Description EXCLUSIVE-LOCK NO-ERROR.
      IF AVAILABLE(Note) THEN DELETE Note.
      Image.Description = 0.
    END.
  END.
  ELSE DO:
    IF Image.Description = 0 OR Image.Description = ? THEN DO:
      FIND LAST Note NO-LOCK NO-ERROR.
      Image.Description = 1 + (IF AVAILABLE(Note) THEN Note.NoteCode ELSE 0).
      CREATE Note.
      Note.NoteCode = Image.Description.
    END.
    ELSE DO:
      FIND Note WHERE Note.NoteCode = Image.Description EXCLUSIVE-LOCK.
    END.
    ASSIGN  Note.Detail         = (INPUT edt_Description)
            Note.NoteDate       = TODAY
            Note.NoteTableKey   = STRING( Image.ImageCode )
            Note.NoteTable      = "IMAGE".
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  edt_Description = "".

  IF NOT AVAILABLE(Image) THEN RETURN.

  /* set up the description (note) if attached */
  IF Image.Description <> 0 AND Image.Description <> ? THEN DO:
    FIND Note WHERE Note.NoteCode = Image.Description NO-LOCK NO-ERROR.
    IF AVAILABLE(Note) THEN edt_Description = Note.Detail.
  END.
  DISPLAY edt_Description.

  RUN display-thumbnail.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN DO:
    have-records = yes.
    CURRENT-WINDOW:TITLE = "Adding a new image".
    RUN dispatch( 'add-record':U ).
  END.

  DO WITH FRAME {&FRAME-NAME}:
    edt_Description:SENSITIVE = (mode <> "View").
    edt_Description:READ-ONLY = (mode = "View").
    edt_Description:BGCOLOR   = (IF mode = "View" THEN ? ELSE 16).
  END.

  IF mode <> "View" THEN
    RUN dispatch( 'enable-fields':U ).
  ELSE
    RUN dispatch( 'disable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND Image WHERE Image.ImageCode = 0 EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE(Image) THEN DO:
    CREATE Image.
  END.
  Image.RentalSpaceCode = INT( find-parent-key( "RentalSpaceCode" ) ).
  Image.PropertyCode = INT( find-parent-key( "PropertyCode" ) ).

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'update-record':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Image"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
 mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-image V-table-Win 
PROCEDURE verify-image :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


