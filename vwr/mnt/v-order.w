&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR mode        AS CHAR NO-UNDO.
DEF VAR prev-amount AS DEC  NO-UNDO.
DEF VAR pdf-support AS LOGICAL INIT YES NO-UNDO.
DEF VAR pdf-printing AS LOGICAL INIT NO NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set.i "Project-Groups-Property" "property-groups"}
IF NOT AVAILABLE(OfficeSetting) THEN property-groups = "PROPEX,TENEX,OWNEX".
{inc/ofc-set.i "Project-Groups-Company" "company-groups"}
IF NOT AVAILABLE(OfficeSetting) THEN company-groups = "ADMIN".

/* Determine if PDF printing is supported */
{inc/ofc-set.i "PDF-Output-Directory" "pdf-output-directory"}
IF NOT AVAILABLE(OfficeSetting) THEN
    pdf-support = NO.

{inc/ofc-set.i "Company-Order-Approvers" "company-order-approvers"}
IF NOT AVAILABLE(OfficeSetting) THEN company-order-approvers = "".
DEF VAR company-approver1 AS CHAR NO-UNDO INITIAL "".
DEF VAR company-approver2 AS CHAR NO-UNDO INITIAL "".
company-approver1 = ENTRY( 1, company-order-approvers ).
IF NUM-ENTRIES( company-order-approvers) > 1 THEN
  company-approver2 = ENTRY( 2, company-order-approvers ).

DEF VAR entity-type AS CHAR NO-UNDO INITIAL "J".
DEF VAR entity-code AS INT NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Order
&Scoped-define FIRST-EXTERNAL-TABLE Order


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Order.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Order.EntityCode Order.OrderCode ~
Order.OrderDate Order.FirstApprover Order.SecondApprover Order.CreditorCode ~
Order.OrderAmount Order.QuotedAmount Order.ApprovedAmount Order.TaxAmount ~
Order.Description Order.OverridePaid 
&Scoped-define ENABLED-TABLES Order
&Scoped-define FIRST-ENABLED-TABLE Order
&Scoped-define DISPLAYED-TABLES Order
&Scoped-define FIRST-DISPLAYED-TABLE Order
&Scoped-Define ENABLED-OBJECTS cmb_Account BtnCalculate tgl_set_paid ~
tgl_supplier fil_copies RECT-3 RECT-4 
&Scoped-Define DISPLAYED-FIELDS Order.EntityType Order.EntityCode ~
Order.OrderCode Order.OrderDate Order.FirstApprover Order.SecondApprover ~
Order.CreditorCode Order.OrderAmount Order.QuotedAmount ~
Order.ApprovedAmount Order.TaxAmount Order.Description Order.OverridePaid 
&Scoped-Define DISPLAYED-OBJECTS fil_EntityName cmb_Account fil_Approver1 ~
fil_InBudget-1 fil_OverBudget-1 fil_Approver2 fil_InBudget-2 ~
fil_OverBudget-2 fil_Creditor fil_YTD-Spent fil_Wait-Appvl fil_OS-Orders ~
fil_Total-Committed fil_YTD-Budget fil_FY-Budget tgl_set_paid tgl_supplier ~
fil_copies 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
PropertyCode||y|TTPL.Order.EntityCode
ProjectCode||y|TTPL.Order.EntityCode
OrderCode||y|TTPL.Order.OrderCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = "PropertyCode,ProjectCode,OrderCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON BtnCalculate 
     LABEL "&Calculate" 
     SIZE 9 BY .75
     BGCOLOR 8 .

DEFINE VARIABLE cmb_Account AS CHARACTER FORMAT "X(256)":U 
     LABEL "Account" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     DROP-DOWN-LIST
     SIZE 65.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Approver1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Approver2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE fil_copies AS INTEGER FORMAT "9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 2.57 BY .8 NO-UNDO.

DEFINE VARIABLE fil_Creditor AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 56.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_EntityName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 56.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_FY-Budget AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "F/Y Budget" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1.05
     FONT 8 NO-UNDO.

DEFINE VARIABLE fil_InBudget-1 AS DECIMAL FORMAT "->,>>>,>>9.99":U INITIAL 0 
     LABEL "In budget" 
     VIEW-AS FILL-IN 
     SIZE 10.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_InBudget-2 AS DECIMAL FORMAT "->,>>>,>>9.99":U INITIAL 0 
     LABEL "In budget" 
     VIEW-AS FILL-IN 
     SIZE 10.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_OS-Orders AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "O/S Orders" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1.05
     FONT 8 NO-UNDO.

DEFINE VARIABLE fil_OverBudget-1 AS DECIMAL FORMAT "->,>>>,>>9.99":U INITIAL 0 
     LABEL "Over" 
     VIEW-AS FILL-IN 
     SIZE 10.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_OverBudget-2 AS DECIMAL FORMAT "->,>>>,>>9.99":U INITIAL 0 
     LABEL "Over" 
     VIEW-AS FILL-IN 
     SIZE 10.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Total-Committed AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1.05
     FONT 8 NO-UNDO.

DEFINE VARIABLE fil_Wait-Appvl AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Waiting appvl" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1.05
     FONT 8 NO-UNDO.

DEFINE VARIABLE fil_YTD-Budget AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "YTD Budget" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1.05
     FONT 8 NO-UNDO.

DEFINE VARIABLE fil_YTD-Spent AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "YTD spent" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1.05
     FONT 8 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 1 GRAPHIC-EDGE  
     SIZE 15.43 BY .1
     BGCOLOR 0 FGCOLOR 0 .

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 78.29 BY 18.

DEFINE VARIABLE tgl_set_paid AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 2.57 BY .8
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_supplier AS LOGICAL INITIAL no 
     LABEL "Print supplier copy" 
     VIEW-AS TOGGLE-BOX
     SIZE 16 BY .8
     FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Order.EntityType AT ROW 1.2 COL 9.29 COLON-ALIGNED
          LABEL "Owner"
          VIEW-AS FILL-IN 
          SIZE 2.14 BY 1
     Order.EntityCode AT ROW 1.2 COL 11 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 6.57 BY 1
     fil_EntityName AT ROW 1.2 COL 20.14 COLON-ALIGNED NO-LABEL
     Order.OrderCode AT ROW 2.2 COL 11 COLON-ALIGNED
          LABEL "Order Code"
          VIEW-AS FILL-IN 
          SIZE 6.57 BY 1
     Order.OrderDate AT ROW 2.2 COL 64.72 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 12 BY 1
     cmb_Account AT ROW 3.2 COL 11 COLON-ALIGNED
     Order.FirstApprover AT ROW 4.2 COL 11 COLON-ALIGNED
          LABEL "Approver 1"
          VIEW-AS FILL-IN 
          SIZE 5.43 BY 1
     fil_Approver1 AT ROW 4.2 COL 19 COLON-ALIGNED NO-LABEL
     fil_InBudget-1 AT ROW 4.2 COL 51.57 COLON-ALIGNED
     fil_OverBudget-1 AT ROW 4.2 COL 66.43 COLON-ALIGNED
     Order.SecondApprover AT ROW 5.2 COL 11 COLON-ALIGNED
          LABEL "Approver 2"
          VIEW-AS FILL-IN 
          SIZE 5.43 BY 1
     fil_Approver2 AT ROW 5.2 COL 19 COLON-ALIGNED NO-LABEL
     fil_InBudget-2 AT ROW 5.2 COL 51.57 COLON-ALIGNED
     fil_OverBudget-2 AT ROW 5.2 COL 66.43 COLON-ALIGNED
     Order.CreditorCode AT ROW 6.6 COL 11 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
     fil_Creditor AT ROW 6.6 COL 20.14 COLON-ALIGNED NO-LABEL
     Order.OrderAmount AT ROW 8 COL 11 COLON-ALIGNED
          LABEL "Order"
          VIEW-AS FILL-IN 
          SIZE 15.43 BY 1
     Order.QuotedAmount AT ROW 8 COL 35 COLON-ALIGNED
          LABEL "Quote" FORMAT "->>>,>>>,>>9.99"
          VIEW-AS FILL-IN 
          SIZE 15.43 BY 1
     Order.ApprovedAmount AT ROW 8 COL 61 COLON-ALIGNED
          LABEL "Approved"
          VIEW-AS FILL-IN 
          SIZE 15.43 BY 1
     Order.TaxAmount AT ROW 9.25 COL 61 COLON-ALIGNED
          LABEL "Tax"
          VIEW-AS FILL-IN 
          SIZE 15.43 BY 1.05
     Order.Description AT ROW 9.6 COL 1.57 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 50.86 BY 8.2
          FONT 8
     BtnCalculate AT ROW 10.25 COL 67
     fil_YTD-Spent AT ROW 11 COL 61 COLON-ALIGNED
     fil_Wait-Appvl AT ROW 12 COL 61 COLON-ALIGNED
     fil_OS-Orders AT ROW 13 COL 61 COLON-ALIGNED
     fil_Total-Committed AT ROW 14.2 COL 61 COLON-ALIGNED
     fil_YTD-Budget AT ROW 15.75 COL 61 COLON-ALIGNED
     fil_FY-Budget AT ROW 16.75 COL 61 COLON-ALIGNED
     Order.OverridePaid AT ROW 17.8 COL 10.14 COLON-ALIGNED
          LABEL "Journalled"
          VIEW-AS FILL-IN 
          SIZE 13.14 BY 1
     tgl_set_paid AT ROW 18 COL 1.86
     tgl_supplier AT ROW 18 COL 35.86
     fil_copies AT ROW 18 COL 58.29 COLON-ALIGNED NO-LABEL
     RECT-3 AT ROW 14.05 COL 63
     RECT-4 AT ROW 1 COL 1
     "Print" VIEW-AS TEXT
          SIZE 3.57 BY .8 AT ROW 18 COL 56.86
          FONT 10
     "Description" VIEW-AS TEXT
          SIZE 8 BY .8 AT ROW 8.8 COL 2.14
          FONT 10
     "office copies of order." VIEW-AS TEXT
          SIZE 14.14 BY .8 AT ROW 18 COL 63.43
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Order
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 21.95
         WIDTH              = 84.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/entity.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Order.ApprovedAmount IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Order.EntityCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Order.EntityType IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN fil_Approver1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Approver2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Creditor IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_EntityName IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_FY-Budget IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_InBudget-1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_InBudget-2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_OS-Orders IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_OverBudget-1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_OverBudget-2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Total-Committed IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Wait-Appvl IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_YTD-Budget IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_YTD-Spent IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Order.FirstApprover IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Order.OrderAmount IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Order.OrderCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Order.OverridePaid IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Order.QuotedAmount IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Order.SecondApprover IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Order.TaxAmount IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME BtnCalculate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnCalculate V-table-Win
ON CHOOSE OF BtnCalculate IN FRAME F-Main /* Calculate */
DO:
  RUN show-budget-info.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Account
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Account V-table-Win
ON VALUE-CHANGED OF cmb_Account IN FRAME F-Main /* Account */
DO:
  RUN account-value-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Order.CreditorCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Order.CreditorCode V-table-Win
ON LEAVE OF Order.CreditorCode IN FRAME F-Main /* Creditor */
DO:
  {inc/selcde/cdcrd.i "fil_Creditor"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Order.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Order.EntityCode V-table-Win
ON LEAVE OF Order.EntityCode IN FRAME F-Main /* Code */
DO:
  IF SELF:MODIFIED THEN RUN entity-code-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Order.EntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Order.EntityType V-table-Win
ON LEAVE OF Order.EntityType IN FRAME F-Main /* Owner */
DO:
  IF SELF:MODIFIED THEN RUN get-accounts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Approver1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver1 V-table-Win
ON U1 OF fil_Approver1 IN FRAME F-Main
DO:
  {inc/selfil/sfapp1.i "Order" "FirstApprover"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver1 V-table-Win
ON U2 OF fil_Approver1 IN FRAME F-Main
DO:
  {inc/selfil/sfapp2.i "Order" "FirstApprover"}
  DO WITH FRAME {&FRAME-NAME}:
    RUN update-approver-limits( Order.FirstApprover:SCREEN-VALUE, fil_InBudget-1:HANDLE, fil_OverBudget-1:HANDLE ).
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver1 V-table-Win
ON U3 OF fil_Approver1 IN FRAME F-Main
DO:
  {inc/selfil/sfapp3.i "Order" "FirstApprover"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Approver2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver2 V-table-Win
ON U1 OF fil_Approver2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp1.i "Order" "SecondApprover"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver2 V-table-Win
ON U2 OF fil_Approver2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp2.i "Order" "SecondApprover"}    
  DO WITH FRAME {&FRAME-NAME}:
    RUN update-approver-limits( Order.SecondApprover:SCREEN-VALUE, fil_InBudget-2:HANDLE, fil_OverBudget-2:HANDLE ).
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Approver2 V-table-Win
ON U3 OF fil_Approver2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp3.i "Order" "SecondApprover"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Creditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U1 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd1.i "Order" "CreditorCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U2 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd2.i "Order" "CreditorCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U3 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd3.i "Order" "CreditorCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Order.FirstApprover
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Order.FirstApprover V-table-Win
ON LEAVE OF Order.FirstApprover IN FRAME F-Main /* Approver 1 */
DO:
  {inc/selcde/cdapp.i "fil_Approver1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Order.OrderAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Order.OrderAmount V-table-Win
ON LEAVE OF Order.OrderAmount IN FRAME F-Main /* Order */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
DO WITH FRAME {&FRAME-NAME}:
  ASSIGN Order.QuotedAmount:SCREEN-VALUE   = SELF:SCREEN-VALUE
         Order.ApprovedAmount:SCREEN-VALUE = SELF:SCREEN-VALUE.
  Order.TaxAmount:SCREEN-VALUE = STRING( (INPUT {&SELF-NAME} * Office.GST) / 100, Order.TaxAmount:FORMAT) .
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Order.OrderCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Order.OrderCode V-table-Win
ON ANY-PRINTABLE OF Order.OrderCode IN FRAME F-Main /* Order Code */
DO:
  IF CHR( LASTKEY ) = SELF:SCREEN-VALUE THEN
  DO:
    APPLY LASTKEY TO SELF.
    SELF:MODIFIED = No.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Order.OrderCode V-table-Win
ON LEAVE OF Order.OrderCode IN FRAME F-Main /* Order Code */
DO:
  IF SELF:MODIFIED THEN
  DO:
    run order-number-changed.
    SELF:MODIFIED = No.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Order.SecondApprover
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Order.SecondApprover V-table-Win
ON LEAVE OF Order.SecondApprover IN FRAME F-Main /* Approver 2 */
DO:
  {inc/selcde/cdapp.i "fil_Approver2"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_set_paid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_set_paid V-table-Win
ON VALUE-CHANGED OF tgl_set_paid IN FRAME F-Main
DO:
  RUN set-paid-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_supplier
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_supplier V-table-Win
ON VALUE-CHANGED OF tgl_supplier IN FRAME F-Main /* Print supplier copy */
DO:
  RUN supplier-copies-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE account-value-changed V-table-Win 
PROCEDURE account-value-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  /* RUN show-budget-info. */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Order"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Order"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN
    RUN delete-order.
  ELSE
    RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  prev-amount = Order.ApprovedAmount.

  RUN verify-order.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).
  RUN print-order.
  IF mode = "Add" THEN RUN notify( 'open-query, RECORD-SOURCE':U ).
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-order V-table-Win 
PROCEDURE delete-order :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FIND CURRENT Order EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Order THEN DELETE Order.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-code-changed V-table-Win 
PROCEDURE entity-code-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  fil_EntityName = get-entity-name( INPUT Order.EntityType, INPUT Order.EntityCode ).
  IF ( INPUT Order.EntityType = "J" ) THEN run get-accounts.

  DEF BUFFER LastOrder FOR Order.
  FIND LAST LastOrder WHERE LastOrder.EntityType = entity-type
                        AND LastOrder.EntityCode = entity-code
                        USE-INDEX XAK2Orders NO-LOCK NO-ERROR.
  Order.OrderCode:SCREEN-VALUE = STRING((IF AVAILABLE LastOrder THEN LastOrder.OrderCode ELSE 0) + 1) .

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-account V-table-Win 
PROCEDURE get-account :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER account-code LIKE ChartOfAccount.AccountCode NO-UNDO.
DEF OUTPUT PARAMETER account-description AS CHAR NO-UNDO INITIAL ?.

DO WITH FRAME {&FRAME-NAME}:
  account-code = DEC( TRIM( ENTRY( 1, cmb_account:SCREEN-VALUE, "-" ) ) ).

  account-description = get-entity-account( INPUT Order.EntityType, INPUT Order.EntityCode, account-code ).
  IF account-description = "* * * Unknown * * *" THEN RETURN "FAIL".
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-accounts V-table-Win 
PROCEDURE get-accounts :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR item AS CHAR NO-UNDO.
DEF VAR account-codes AS CHAR NO-UNDO.
DEF VAR account-descriptions AS CHAR NO-UNDO.
DEF VAR group-list AS CHAR NO-UNDO INITIAL "".
DEF VAR i AS INTEGER NO-UNDO.
DEF VAR et AS CHAR NO-UNDO INITIAL "J".

DO WITH FRAME {&FRAME-NAME}:

  cmb_Account:LIST-ITEMS = "".

  IF AVAILABLE(Order) THEN et = INPUT Order.EntityType.
                      ELSE et = entity-type.
  IF et = "J" THEN DO:
    FIND Project WHERE Project.ProjectCode = entity-code NO-LOCK NO-ERROR.
    IF NOT AVAILABLE Project THEN RETURN.

  /* most projects, the budgets must exist */  
    FOR EACH ProjectBudget NO-LOCK OF Project WHERE ProjectBudget.OriginalBudget <> 0
                                              OR ProjectBudget.CommittedBudget <> 0
                                              OR ProjectBudget.UnCommittedBudget <> 0
                                              OR ProjectBudget.Adjustment <> 0
                                              OR ProjectBudget.AgreedVariation <> 0:
      item = TRIM(ProjectBudget.Description).
      IF item = "" OR item = ? THEN DO:
        FIND ChartOfAccount OF ProjectBudget NO-LOCK NO-ERROR.
        IF AVAILABLE(ChartOfAccount) THEN item = ChartOfAccount.Name.
      END.
      account-descriptions = account-descriptions
                           + (IF account-codes = "" THEN "" ELSE "~n")
                           + " - " + item.
      account-codes = account-codes + (IF account-codes = "" THEN "" ELSE ",")
                    + STRING( ProjectBudget.AccountCode, "9999.99" ).
    END.
  END.
  ELSE DO:
    IF et = "P" THEN
      group-list = property-groups.
    ELSE IF et = "L" THEN
      group-list = company-groups.

    DO i = 1 TO NUM-ENTRIES(group-list):
      FOR EACH ChartOfAccount WHERE ChartOfAccount.AccountGroupCode = ENTRY( i, group-list) NO-LOCK:
        IF LOOKUP( STRING(ChartOfAccount.AccountCode, "9999.99"), account-codes) = 0 THEN DO:
          account-descriptions = account-descriptions + (IF account-codes = "" THEN "" ELSE "~n")
                               + " - " + ChartOfAccount.Name.
          account-codes = account-codes + (IF account-codes = "" THEN "" ELSE ",")
                        + STRING( ChartOfAccount.AccountCode, "9999.99" ).
        END.
      END.
    END.
  END.

/*  MESSAGE et group-list SKIP account-codes. */

  /* add them into the combo box. */
  cmb_account:DELIMITER = "~n".
  DO i = 1 TO NUM-ENTRIES(account-codes):
    item = ENTRY( i, account-codes) + ENTRY(i, account-descriptions,"~n").
    IF cmb_account:ADD-LAST( item ) THEN .
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-parent-keys V-table-Win 
PROCEDURE get-parent-keys :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR c-recsrc   AS CHAR NO-UNDO.
  DEF VAR parent-hdl AS HANDLE NO-UNDO.
  DEF VAR rowid-list AS CHAR NO-UNDO.

  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT c-recsrc ).

  parent-hdl = WIDGET-HANDLE(c-recsrc).
  IF NOT VALID-HANDLE( parent-hdl ) THEN RETURN.

  RUN get-attribute IN parent-hdl ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN "PropertyCode" THEN entity-type = "P".
    WHEN "ProjectCode"  THEN entity-type = "J".
    WHEN "CompanyCode"  THEN entity-type = "L".
    OTHERWISE entity-type = "J".
  END CASE.

/*  MESSAGE RETURN-VALUE entity-type. */

  RUN get-attribute IN parent-hdl ('Key-Value':U).
  entity-code = INT(RETURN-VALUE).

/*  MESSAGE entity-code . */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR account-code AS DEC NO-UNDO.
DEF VAR junk AS CHAR NO-UNDO.

IF NOT AVAILABLE(Order) THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:
  RUN get-account( OUTPUT account-code, OUTPUT junk).
  Order.AccountCode = account-code.

  ASSIGN FRAME {&FRAME-NAME} tgl_set_paid.
  IF NOT tgl_set_paid THEN Order.OverridePaid = ? .

  IF Order.EntityType = ? OR Order.EntityType = "" THEN DO:
    FIND Project WHERE Project.ProjectCode = Order.ProjectCode NO-LOCK NO-ERROR.
    IF AVAILABLE(Project) AND Project.ExpenditureType = "G" THEN DO:
      Order.EntityType = Project.EntityType.
      Order.EntityCode = Project.EntityCode.
    END.
    ELSE DO:
      Order.EntityType = "J".
      Order.EntityCode = Order.ProjectCode.
    END.
  END.
END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-disable-fields V-table-Win 
PROCEDURE inst-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  cmb_Account:SENSITIVE IN FRAME {&FRAME-NAME} = NO.
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE,
    "*", "SENSITIVE = No" ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR item AS CHAR NO-UNDO.
DEF VAR et AS CHAR NO-UNDO.


DO WITH FRAME {&FRAME-NAME}:
  IF INPUT Order.EntityType <> entity-type THEN DO:
    entity-type = INPUT Order.EntityType.
    RUN get-accounts.
  END.


  item = cmb_Account:ENTRY(1).
  IF INPUT Order.EntityType = "J" THEN DO:
    FIND ProjectBudget WHERE ProjectBudget.ProjectCode = (INPUT Order.EntityCode)
                       AND ProjectBudget.AccountCode = Order.AccountCode NO-LOCK NO-ERROR.
   
    IF AVAILABLE(ProjectBudget) THEN DO:
      item = STRING( ProjectBudget.AccountCode, "9999.99" ) + " - ".
      IF ProjectBudget.Description = "" OR ProjectBudget.Description = ? AND
          CAN-FIND( ChartOfAccount WHERE ChartOfAccount.AccountCode = Order.AccountCode )
      THEN DO:
        FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = Order.AccountCode NO-LOCK.
        item = item + ChartOfAccount.Name.
      END.
      ELSE
        item = item + projectBudget.Description .
    END.
    ELSE DO:
      FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = Order.AccountCode NO-LOCK NO-ERROR.
      IF AVAILABLE ChartOfAccount THEN
        item = STRING( ChartOfAccount.AccountCode, "9999.99" ) + " - " + ChartOfAccount.Name.
    END.
  END.
  ELSE DO:
    FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = Order.AccountCode NO-LOCK NO-ERROR.
    IF AVAILABLE ChartOfAccount THEN
      item = STRING( ChartOfAccount.AccountCode, "9999.99" ) + " - " + ChartOfAccount.Name.
  END.

  cmb_Account:SCREEN-VALUE = item.

  RUN update-approver-limits( Order.FirstApprover:SCREEN-VALUE, fil_InBudget-1:HANDLE, fil_OverBudget-1:HANDLE ).
  RUN update-approver-limits( Order.SecondApprover:SCREEN-VALUE, fil_InBudget-2:HANDLE, fil_OverBudget-2:HANDLE ).

END.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  Order.EntityType:SENSITIVE = mode = "Add".
  Order.EntityCode:SENSITIVE = mode = "Add".
  Order.OrderCode:SENSITIVE   = mode = "Add".
END.

  RUN set-paid-changed.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  fil_copies   = (IF mode = "View" THEN 0 ELSE 1).
  tgl_supplier = (mode = "Add").
  DISPLAY fil_Copies tgl_supplier WITH FRAME {&FRAME-NAME}.

  IF mode <> "Add" THEN
    tgl_set_paid = (Order.OverridePaid <> ?).
  ELSE
    tgl_set_paid = No.

  DISPLAY tgl_set_paid WITH FRAME {&FRAME-NAME}.
  RUN set-paid-changed.

  IF mode = "Add" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "View" THEN
    RUN dispatch( 'disable-fields':U ).
  ELSE
    RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE(Order) AND Order.EntityType = ? OR Order.EntityType = "" THEN DO:
    FIND CURRENT Order EXCLUSIVE-LOCK.
    FIND Project WHERE Project.ProjectCode = Order.ProjectCode NO-LOCK NO-ERROR.
    IF AVAILABLE(Project) AND Project.ExpenditureType = "G" THEN DO:
      Order.EntityType = Project.EntityType.
      Order.EntityCode = Project.EntityCode.
    END.
    ELSE DO:
      Order.EntityType = "J".
      Order.EntityCode = Order.ProjectCode.
    END.
    FIND CURRENT Order NO-LOCK.
    DISPLAY Order.EntityType Order.EntityCode WITH FRAME {&FRAME-NAME}.

    IF Order.EntityType <> entity-type THEN DO:
      entity-type = Order.EntityType.
      RUN get-accounts.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-end-update V-table-Win 
PROCEDURE local-end-update :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  ERROR-STATUS:ERROR = No.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'end-update':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE order-number-changed V-table-Win 
PROCEDURE order-number-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  fil_copies = 1.
  DISPLAY fil_Copies WITH FRAME {&FRAME-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR last-order AS INT NO-UNDO INITIAL 0.

DO WITH FRAME {&FRAME-NAME}:

  IF entity-type = "J" THEN DO:
    FIND Project WHERE Project.ProjectCode = entity-code NO-LOCK NO-ERROR.
    IF AVAILABLE Project THEN DO:
      IF Project.ExpenditureType = "G" THEN DO:
        ASSIGN
          entity-type = Project.EntityType.
          entity-code = Project.EntityCode.
/*        MESSAGE entity-type entity-code. */
      END.
    END.
  END.

  ON FIND OF Order OVERRIDE DO: END.
  DEF BUFFER LastOrder FOR Order.
  FOR EACH LastOrder WHERE LastOrder.EntityType = entity-type
                        AND LastOrder.EntityCode = entity-code
                        BY LastOrder.OrderCode DESCENDING:
    last-order = LastOrder.OrderCode.
    LEAVE.
  END.
  ON FIND OF Order REVERT.

/*  MESSAGE entity-type entity-code last-order. */

  CREATE Order.
  ASSIGN
    Order.EntityType = entity-type
    Order.EntityCode = entity-code
    Order.ProjectCode = entity-code
    Order.AccountCode = 0000.00
    Order.OrderDate   = TODAY
    Order.OrderCode   = last-order + 1 .


  IF entity-type = "J" THEN DO:
    FIND Project WHERE Project.ProjectCode = entity-code NO-LOCK NO-ERROR.
    IF AVAILABLE Project THEN DO:
      Order.ProjectCode = entity-code.
      Order.FirstApprover = Project.FirstApprover.
      Order.SecondApprover = Project.SecondApprover.
    END.
  END.
  ELSE IF entity-type = "P" THEN DO:
    FIND Property WHERE Property.PropertyCode = entity-code NO-LOCK NO-ERROR.
    IF AVAILABLE Property THEN DO:
      FIND LAST LastOrder WHERE LastOrder.EntityType = "P"
                            AND LastOrder.EntityCode = Property.PropertyCode
                            USE-INDEX XAK2Orders NO-LOCK NO-ERROR.
      FIND FIRST Approver WHERE Approver.PersonCode = Property.Manager NO-LOCK NO-ERROR.
      IF AVAILABLE(Approver) THEN Order.FirstApprover = Approver.ApproverCode.
      FIND FIRST Approver WHERE Approver.PersonCode = Property.Administrator NO-LOCK NO-ERROR.
      IF AVAILABLE(Approver) THEN Order.SecondApprover = Approver.ApproverCode.
    END.
  END.
  ELSE IF entity-type = "L" THEN DO:
    FIND Company WHERE Company.CompanyCode = entity-code NO-LOCK NO-ERROR.
    IF AVAILABLE Company THEN DO:
      Order.FirstApprover = company-approver1.
      Order.SecondApprover = company-approver2.
    END.
  END.


  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'display-fields':U ).

  CURRENT-WINDOW:TITLE = "Adding a new Order " + entity-type + STRING(entity-code, "99999").

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-row-available V-table-Win 
PROCEDURE override-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF ( mode <> "Add" ) THEN RUN adm-row-available.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE":U THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize V-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN get-parent-keys.
  RUN get-accounts.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-order V-table-Win 
PROCEDURE print-order :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

  ASSIGN FRAME {&FRAME-NAME} tgl_supplier fil_copies.
  
  IF fil_copies > 0 OR tgl_supplier THEN DO:
    MESSAGE "Reprint to PDF?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO
      TITLE "PDF Printing ?" UPDATE pdf-printing.

    report-options = "Entity," + Order.EntityType + "," + STRING(Order.EntityCode)
                   + "~nOrders," + STRING(Order.OrderCode)
                   + (IF tgl_Supplier THEN "~nSupplierCopy" ELSE "")
                   + (IF pdf-printing THEN "~nOutputPDF" ELSE "")
                   + "~nInternalCopies," + STRING( fil_copies ).
    RUN process/report/orderfrm.p( report-options ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "PropertyCode" "Order" "EntityCode"}
  {src/adm/template/sndkycas.i "ProjectCode" "Order" "EntityCode"}
  {src/adm/template/sndkycas.i "OrderCode" "Order" "OrderCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Order"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-paid-changed V-table-Win 
PROCEDURE set-paid-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

/*  ASSIGN FRAME {&FRAME-NAME} Order.OverridePaid. */

  Order.OverridePaid:SENSITIVE = INPUT tgl_set_paid .
  IF INPUT tgl_set_paid = No THEN
    DISPLAY ? @ Order.OverridePaid.
  ELSE
    DISPLAY Order.OverridePaid.
    
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE show-budget-info V-table-Win 
PROCEDURE show-budget-info :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR m1 AS INT NO-UNDO.
DEF VAR m2 AS INT NO-UNDO.
DEF VAR m3 AS INT NO-UNDO.

DEF VAR et AS CHAR NO-UNDO.
DEF VAR ec AS INT NO-UNDO.
DEF VAR ac AS DEC NO-UNDO.
DEF VAR ac-desc AS CHAR NO-UNDO.

IF NOT AVAILABLE(Order) THEN RETURN.


DO WITH FRAME {&FRAME-NAME}:
  fil_YTD-Budget = 0.
  fil_FY-Budget  = 0.

  fil_Total-Committed   = 0.
  fil_Wait-Appvl        = 0.
  fil_OS-Orders         = 0.
  fil_YTD-Spent         = 0.

  FIND Month WHERE Month.StartDate <= TODAY AND Month.EndDate >= TODAY NO-LOCK.
  m2 = Month.MonthCode.
  FIND FinancialYear OF Month NO-LOCK.
  IF Order.EntityType = 'J' THEN DO:
    FIND FIRST Month NO-LOCK.    m1 = Month.MonthCode.
  END.
  ELSE DO:
    FIND FIRST Month OF FinancialYear NO-LOCK.    m1 = Month.MonthCode.
  END.
  FIND LAST Month OF FinancialYear NO-LOCK.     m3 = Month.MonthCode.

  RUN get-account( OUTPUT ac, OUTPUT ac-desc ).
  et = INPUT Order.EntityType.
  ec = INPUT Order.EntityCode.

  /* OUTPUT TO tmp.txt . */

  FOR EACH AccountBalance WHERE AccountBalance.EntityType  = et
                            AND AccountBalance.EntityCode  = ec
                            AND AccountBalance.AccountCode = ac
                            AND AccountBalance.MonthCode >= m1
                            AND AccountBalance.MonthCode <= m3 NO-LOCK:
    fil_FY-Budget = fil_FY-Budget + AccountBalance.Budget.
    IF AccountBalance.MonthCode <= m2 THEN DO:
      fil_YTD-Budget = fil_YTD-Budget + AccountBalance.Budget.
      fil_YTD-Spent  = fil_YTD-Spent + AccountBalance.Balance.
    END.
    /*
    MESSAGE "AB:" AccountBalance.EntityType AccountBalance.EntityCode AccountBalance.AccountCode
              AccountBalance.MonthCode AccountBalance.Budget AccountBalance.Balance .
    */
  END.
DEF BUFFER tmp_Order FOR Order.
  FOR EACH tmp_Order WHERE tmp_Order.EntityType  = et
                       AND tmp_Order.EntityCode  = ec
                       AND tmp_Order.AccountCode = ac
                       AND ROWID(tmp_Order) <> ROWID(Order) NO-LOCK:
    fil_OS-Orders = fil_OS-Orders + tmp_Order.ApprovedAmount .
    /*
    MESSAGE "tmpOrder:" tmp_Order.EntityType tmp_Order.EntityCode tmp_Order.AccountCode ac
              tmp_Order.OrderCode tmp_Order.ApprovedAmount .
    */
    FOR EACH Voucher WHERE Voucher.EntityType  = et
                       AND Voucher.EntityCode  = ec
                       AND Voucher.OrderCode = tmp_Order.OrderCode NO-LOCK:
      IF Voucher.VoucherStatus = "U" THEN DO:
        fil_Wait-Appvl = fil_Wait-Appvl + Voucher.GoodsValue.
      END.
      IF Voucher.VoucherStatus <> "C" THEN DO:
        fil_OS-Orders = fil_OS-Orders - Voucher.GoodsValue.
      END.
    END.
  END.

  fil_OS-Orders = fil_OS-Orders + INPUT Order.ApprovedAmount .
  /*
  MESSAGE "my_Order:" Order.EntityType Order.EntityCode Order.AccountCode ac
           INPUT Order.OrderCode INPUT Order.ApprovedAmount .
  */
  FOR EACH Voucher WHERE Voucher.EntityType  = et
                     AND Voucher.EntityCode  = ec
                     AND Voucher.OrderCode = INPUT Order.OrderCode NO-LOCK:
    IF Voucher.VoucherStatus = "U" THEN DO:
      fil_Wait-Appvl = fil_Wait-Appvl + Voucher.GoodsValue.
    END.
    IF Voucher.VoucherStatus <> "C" THEN DO:
      fil_OS-Orders = fil_OS-Orders - Voucher.GoodsValue.
    END.
  END.
  /* OUTPUT CLOSE. */

  fil_Total-Committed = fil_YTD-Spent + fil_OS-Orders + fil_Wait-Appvl.
  DISPLAY fil_YTD-Budget fil_FY-Budget fil_OS-Orders
          fil_Total-Committed fil_Wait-Appvl fil_YTD-Spent.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE supplier-copies-changed V-table-Win 
PROCEDURE supplier-copies-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
RETURN. /* this is no longer relevant */
DO WITH FRAME {&FRAME-NAME}:

  ASSIGN FRAME {&FRAME-NAME} fil_copies.
  
  IF INPUT tgl_supplier = No THEN
    fil_Copies = IF fil_Copies = 0 THEN 0 ELSE fil_copies - 1.
  ELSE
    fil_Copies = fil_Copies + 1.
    
  DISPLAY fil_copies WITH FRAME {&FRAME-NAME}.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-approver-limits V-table-Win 
PROCEDURE update-approver-limits :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER appvr AS CHAR NO-UNDO.
DEF INPUT PARAMETER in-budget-hdl AS HANDLE NO-UNDO.
DEF INPUT PARAMETER over-budget-hdl AS HANDLE NO-UNDO.

DEF BUFFER tmp_Appvr FOR Approver.

  FIND tmp_Appvr WHERE tmp_Appvr.ApproverCode = appvr NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(tmp_Appvr) THEN RETURN.

  in-budget-hdl:SCREEN-VALUE   = STRING( tmp_Appvr.ApprovalLimit, in-budget-hdl:FORMAT ).
  over-budget-hdl:SCREEN-VALUE = STRING( tmp_Appvr.OverBudgetLimit, over-budget-hdl:FORMAT ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-order V-table-Win 
PROCEDURE verify-order :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  CASE INPUT Order.EntityType:
    WHEN "J" THEN DO:
      FIND Project WHERE Project.ProjectCode = INPUT Order.EntityCode NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(Project) THEN DO:
        MESSAGE "The project code is invalid." VIEW-AS ALERT-BOX ERROR
          TITLE "Invalid Project Code".
        APPLY 'ENTRY':U TO Order.EntityCode.
        RETURN "FAIL".
      END.
    END.
    WHEN "P" THEN DO:
      FIND Property WHERE Property.PropertyCode = INPUT Order.EntityCode NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(Property) THEN DO:
        MESSAGE "The project code is invalid." VIEW-AS ALERT-BOX ERROR
          TITLE "Invalid Property Code".
        APPLY 'ENTRY':U TO Order.EntityCode.
        RETURN "FAIL".
      END.
    END.
    WHEN "L" THEN DO:
      FIND Company WHERE Company.CompanyCode = INPUT Order.EntityCode NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(Company) THEN DO:
        MESSAGE "The project code is invalid." VIEW-AS ALERT-BOX ERROR
          TITLE "Invalid Company Code".
        APPLY 'ENTRY':U TO Order.EntityCode.
        RETURN "FAIL".
      END.
    END.
    OTHERWISE DO:
        MESSAGE "The entity type is invalid." VIEW-AS ALERT-BOX ERROR
          TITLE "Invalid Entity Type".
        APPLY 'ENTRY':U TO Order.EntityType.
        RETURN "FAIL".
    END.
  END CASE.

  IF INPUT Order.EntityType = "J" AND Project.ExpenditureType = "M" THEN DO:
    MESSAGE "Orders cannot be put against 'Major' projects" SKIP
            "but must be created against their sub-projects."
            VIEW-AS ALERT-BOX ERROR TITLE "Invalid Project Expense Type".
    APPLY 'ENTRY':U TO Order.EntityCode.
    RETURN "FAIL".
  END.

  IF TRIM(INPUT Order.Description) = "" THEN DO:
    MESSAGE "You must enter a description" VIEW-AS ALERT-BOX ERROR
      TITLE "No description".
    APPLY 'ENTRY':U TO Order.Description.
    RETURN "FAIL".
  END.

  IF INPUT Order.OrderCode = 0 THEN DO:
    MESSAGE "You must enter an order number" VIEW-AS ALERT-BOX ERROR
      TITLE "No order number entered".
    APPLY 'ENTRY':U TO Order.OrderCode.
    RETURN "FAIL".
  END.
  
  IF mode = "Add" AND INPUT Order.OrderCode <> Order.OrderCode AND
    CAN-FIND( Order WHERE
      Order.EntityType = INPUT Order.EntityType AND
      Order.EntityCode = INPUT Order.EntityCode AND
      Order.OrderCode  = INPUT Order.OrderCode ) THEN
  DO:
    MESSAGE
      "An order already exists for" INPUT Order.EntityType INPUT Order.EntityCode SKIP
      "and order code" INPUT Order.OrderCode
      VIEW-AS ALERT-BOX ERROR TITLE "Duplicate Order Error".
    APPLY 'ENTRY':U TO Order.OrderCode.
    RETURN "FAIL".
  END.

  DEF VAR account-code AS DEC NO-UNDO.
  DEF VAR account-description AS CHAR NO-UNDO.
  RUN get-account( OUTPUT account-code, OUTPUT account-description).
  IF RETURN-VALUE = "FAIL" THEN DO:
    MESSAGE "You need to select an account." VIEW-AS ALERT-BOX ERROR
            TITLE "No account selected".
    APPLY 'ENTRY':U TO cmb_Account.
    RETURN "FAIL".
  END.
  
  IF NOT CAN-FIND( FIRST Creditor WHERE Creditor.CreditorCode = INPUT Order.CreditorCode ) THEN DO:
    MESSAGE "You need to choose a creditor for this order" VIEW-AS ALERT-BOX ERROR
      TITLE "No Creditor selected".
    APPLY 'ENTRY':U TO Order.CreditorCode.
    RETURN "FAIL".
  END. 

  IF INPUT Order.EntityType = "J" AND Project.ExpenditureType <> "G" THEN DO:
    RUN verify-order-amount( account-code ).
    IF RETURN-VALUE = "FAIL" THEN RETURN RETURN-VALUE.
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-order-amount V-table-Win 
PROCEDURE verify-order-amount :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER ac AS DEC NO-UNDO.

DEF BUFFER PBJ FOR ProjectBudget.

DO WITH FRAME {&FRAME-NAME}:

  FIND PBJ WHERE PBJ.ProjectCode = INPUT Order.EntityCode
                       AND PBJ.AccountCode = ac NO-LOCK NO-ERROR.

  IF NOT AVAILABLE(PBJ) THEN DO:
    MESSAGE "Record not available".
  END.

  DEF VAR threshold AS DEC NO-UNDO.
  DEF VAR max-amount  AS DEC NO-UNDO.

  IF PBJ.CommittedBudget <> 0 THEN
    threshold = PBJ.CommittedBudget + PBJ.UncommittedBudget .
  ELSE
    threshold = PBJ.OriginalBudget.

  threshold = threshold + PBJ.Adjustment + PBJ.AgreedVariation.
  max-amount = threshold - prev-amount .
  IF max-amount < 0 THEN max-amount = 0.

  IF (( (INPUT Order.ApprovedAmount) - Order.ApprovedAmount) > max-amount ) THEN DO:
    MESSAGE "The total orders for project budget" STRING( INPUT Order.EntityCode ) + "/" + STRING( PBJ.AccountCode, "9999.99" ) SKIP
            "will exceed the budget of" TRIM( STRING( threshold, "$>>>,>>>,>>9.99CR" )) + "." SKIP(1)
            "To remain within budget, this order can have" SKIP 
            "a maximum approved amount of "
            TRIM( STRING( max-amount + Order.ApprovedAmount, "$>>>,>>>,>>9.99CR" ) ) + "."
            VIEW-AS ALERT-BOX ERROR TITLE "Budget Exceeded.".
    APPLY 'ENTRY':U TO Order.ApprovedAmount.
    RETURN "FAIL".
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

