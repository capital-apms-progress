&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tptenant.i}

DEF BUFFER BilPerson   FOR Person.
DEF BUFFER HomeDetail  FOR PhoneDetail.
DEF BUFFER FaxDetail   FOR PhoneDetail.
DEF BUFFER MobDetail   FOR PhoneDetail.

DEF VAR mode AS CHAR NO-UNDO.
DEF VAR this-contact AS CHAR NO-UNDO.
DEF VAR last-contact AS CHAR NO-UNDO.
DEF VAR this-address AS CHAR NO-UNDO.
DEF VAR last-address AS CHAR NO-UNDO.
DEF VAR this-phone   AS CHAR NO-UNDO.
DEF VAR last-phone   AS CHAR NO-UNDO.

FIND Person NO-LOCK NO-ERROR.

{inc/ofc-this.i}
{inc/ofc-set.i "Tenant-Code-Range" "tenant-code-range"}
IF NOT AVAILABLE(OfficeSetting) THEN tenant-code-range = ?.
{inc/persndtl.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Tenant
&Scoped-define FIRST-EXTERNAL-TABLE Tenant


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Tenant.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Tenant.TenantCode Tenant.Active Tenant.Name ~
Tenant.LegalName Tenant.EntityType Tenant.EntityCode Tenant.Quality 
&Scoped-define ENABLED-TABLES Tenant
&Scoped-define FIRST-ENABLED-TABLE Tenant
&Scoped-Define ENABLED-OBJECTS btn_Clear cmb_Payment cmb_DebtClass ~
cmb_VarianceClass cmb_Contact fil_FullName fil_Company cmb_Address ~
edt_address fil_City fil_state fil_Country fil_Zip cmb_Phone1 fil_Phone1 ~
cmb_Phone3 fil_Phone3 cmb_Phone2 fil_Phone2 cmb_Phone4 fil_Phone4 RECT-24 ~
RECT-4 
&Scoped-Define DISPLAYED-FIELDS Tenant.TenantCode Tenant.Active Tenant.Name ~
Tenant.LegalName Tenant.EntityType Tenant.EntityCode Tenant.Quality 
&Scoped-define DISPLAYED-TABLES Tenant
&Scoped-define FIRST-DISPLAYED-TABLE Tenant
&Scoped-Define DISPLAYED-OBJECTS fil_Entity cmb_Payment cmb_DebtClass ~
cmb_VarianceClass fil_b3 fil_b2 fil_b1 fil_b0 cmb_Contact fil_FullName ~
fil_Company cmb_Address edt_address fil_City fil_state fil_Country fil_Zip ~
cmb_Phone1 fil_Phone1 cmb_Phone3 fil_Phone3 cmb_Phone2 fil_Phone2 ~
cmb_Phone4 fil_Phone4 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD fix-contact V-table-Win 
FUNCTION fix-contact RETURNS LOGICAL
  ( INPUT contact-type AS CHAR, INPUT person-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD tenant-exists V-table-Win 
FUNCTION tenant-exists RETURNS LOGICAL
  ( INPUT tc AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Clear  NO-FOCUS
     LABEL "&Clear" 
     SIZE 10.86 BY 1.05.

DEFINE VARIABLE cmb_Address AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 25.14 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Contact AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Accounting Matters","Property Matters","After Hours Emergency 1","After Hours Emergency 2" 
     DROP-DOWN-LIST
     SIZE 20.57 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_DebtClass AS CHARACTER FORMAT "X(64)" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "x" 
     DROP-DOWN-LIST
     SIZE 29.14 BY 1.

DEFINE VARIABLE cmb_Payment AS CHARACTER FORMAT "X(64)" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "x" 
     DROP-DOWN-LIST
     SIZE 20.57 BY 1.

DEFINE VARIABLE cmb_Phone1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 11 BY 1.05 NO-UNDO.

DEFINE VARIABLE cmb_Phone2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 11 BY 1.05 NO-UNDO.

DEFINE VARIABLE cmb_Phone3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 11 BY 1.05 NO-UNDO.

DEFINE VARIABLE cmb_Phone4 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 11 BY 1.05 NO-UNDO.

DEFINE VARIABLE cmb_VarianceClass AS CHARACTER FORMAT "X(64)" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "x" 
     DROP-DOWN-LIST
     SIZE 29.14 BY 1.

DEFINE VARIABLE edt_address AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 46.86 BY 4 NO-UNDO.

DEFINE VARIABLE fil_b0 AS DECIMAL FORMAT "->>>,>>9.99":U INITIAL 0 
     LABEL "Current" 
     VIEW-AS FILL-IN 
     SIZE 11.43 BY 1.05
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_b1 AS DECIMAL FORMAT "->>>,>>9.99":U INITIAL 0 
     LABEL "1mth" 
     VIEW-AS FILL-IN 
     SIZE 11.43 BY 1.05
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_b2 AS DECIMAL FORMAT "->>>,>>9.99":U INITIAL 0 
     LABEL "2mths" 
     VIEW-AS FILL-IN 
     SIZE 11.43 BY 1.05
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_b3 AS DECIMAL FORMAT "->>>,>>9.99":U INITIAL 0 
     LABEL "3mths +" 
     VIEW-AS FILL-IN 
     SIZE 11.43 BY 1.05
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_City AS CHARACTER FORMAT "X(50)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1.

DEFINE VARIABLE fil_Company AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 61.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Country AS CHARACTER FORMAT "X(50)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1
     FONT 10.

DEFINE VARIABLE fil_Entity AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 49.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_FullName AS CHARACTER FORMAT "X(256)" 
     VIEW-AS FILL-IN 
     SIZE 61.14 BY 1.

DEFINE VARIABLE fil_Phone1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Phone2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Phone3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Phone4 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE fil_state AS CHARACTER FORMAT "X(50)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1.

DEFINE VARIABLE fil_Zip AS CHARACTER FORMAT "X(50)" 
     VIEW-AS FILL-IN 
     SIZE 15.43 BY 1
     FONT 10.

DEFINE RECTANGLE RECT-24
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 70 BY .05.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 71.43 BY 19.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     btn_Clear AT ROW 12.6 COL 37.57
     Tenant.TenantCode AT ROW 1 COL 6.43 COLON-ALIGNED
          LABEL "Tenant"
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
     Tenant.Active AT ROW 1 COL 63.86
          VIEW-AS TOGGLE-BOX
          SIZE 8 BY 1
     Tenant.Name AT ROW 2 COL 8.72 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 61.14 BY 1
     Tenant.LegalName AT ROW 3 COL 8.72 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 61.14 BY 1
     Tenant.EntityType AT ROW 4.2 COL 8.72 COLON-ALIGNED
          LABEL "Entity Type"
          VIEW-AS FILL-IN 
          SIZE 2.86 BY 1
     Tenant.EntityCode AT ROW 4.2 COL 11.57 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_Entity AT ROW 4.2 COL 20.14 COLON-ALIGNED NO-LABEL
     cmb_Payment AT ROW 5.4 COL 10.72 NO-LABEL
     cmb_DebtClass AT ROW 5.4 COL 42.72 NO-LABEL
     Tenant.Quality AT ROW 6.6 COL 8.72 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 4.29 BY 1.05
     cmb_VarianceClass AT ROW 6.6 COL 42.72 NO-LABEL
     fil_b3 AT ROW 7.8 COL 8.72 COLON-ALIGNED
     fil_b2 AT ROW 7.8 COL 24.72 COLON-ALIGNED
     fil_b1 AT ROW 7.8 COL 40.72 COLON-ALIGNED
     fil_b0 AT ROW 7.8 COL 58.43 COLON-ALIGNED
     cmb_Contact AT ROW 9.2 COL 8.72 COLON-ALIGNED NO-LABEL
     fil_FullName AT ROW 10.5 COL 8.72 COLON-ALIGNED NO-LABEL
     fil_Company AT ROW 11.5 COL 8.72 COLON-ALIGNED NO-LABEL
     cmb_Address AT ROW 12.6 COL 8.72 COLON-ALIGNED NO-LABEL
     edt_address AT ROW 13.6 COL 1.57 NO-LABEL
     fil_City AT ROW 13.6 COL 54.43 COLON-ALIGNED NO-LABEL
     fil_state AT ROW 14.6 COL 54.43 COLON-ALIGNED NO-LABEL
     fil_Country AT ROW 15.6 COL 54.43 COLON-ALIGNED NO-LABEL
     fil_Zip AT ROW 16.6 COL 54.43 COLON-ALIGNED NO-LABEL
     cmb_Phone1 AT ROW 18.4 COL 1.57 NO-LABEL
     fil_Phone1 AT ROW 18.4 COL 10.57 COLON-ALIGNED NO-LABEL
     cmb_Phone3 AT ROW 18.4 COL 37.14 NO-LABEL
     fil_Phone3 AT ROW 18.4 COL 46.14 COLON-ALIGNED NO-LABEL
     cmb_Phone2 AT ROW 19.55 COL 1.57 NO-LABEL
     fil_Phone2 AT ROW 19.55 COL 10.57 COLON-ALIGNED NO-LABEL
     cmb_Phone4 AT ROW 19.55 COL 37.14 NO-LABEL
     fil_Phone4 AT ROW 19.55 COL 46.14 COLON-ALIGNED NO-LABEL
     RECT-24 AT ROW 9 COL 2.14
     RECT-4 AT ROW 1.4 COL 1
     "Address:" VIEW-AS TEXT
          SIZE 6.29 BY .9 AT ROW 12.7 COL 1.57
     "Country:" VIEW-AS TEXT
          SIZE 5.72 BY 1 AT ROW 15.6 COL 50.14
     "City:" VIEW-AS TEXT
          SIZE 4 BY 1 AT ROW 13.6 COL 50.14
     "Pays By:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 5.4 COL 1.57
     "Entity:" VIEW-AS TEXT
          SIZE 9.14 BY 1 AT ROW 4.2 COL 1.57
     "Trading As:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 3 COL 1.57
     "Zip:" VIEW-AS TEXT
          SIZE 4 BY 1 AT ROW 16.6 COL 50.14
     "Company:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 11.5 COL 1.57
     "Person:" VIEW-AS TEXT
          SIZE 5.72 BY 1 AT ROW 10.5 COL 1.57
     "Contact:" VIEW-AS TEXT
          SIZE 6.29 BY 1 AT ROW 9.2 COL 1.57
     "Variances are:" VIEW-AS TEXT
          SIZE 9.72 BY 1 AT ROW 6.6 COL 33
     "Quality:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 6.6 COL 1.57
     "Debt Class:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 5.4 COL 33
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME F-Main
     "Telephones:" VIEW-AS TEXT
          SIZE 8.57 BY .8 AT ROW 17.6 COL 1.57
     "Legal Name:" VIEW-AS TEXT
          SIZE 9 BY 1 AT ROW 2 COL 1.57
     "State:" VIEW-AS TEXT
          SIZE 5 BY 1 AT ROW 14.6 COL 50.14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.Tenant
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 23.45
         WIDTH              = 82.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/tenant-atb.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cmb_DebtClass IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Payment IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Phone1 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Phone2 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Phone3 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_Phone4 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_VarianceClass IN FRAME F-Main
   ALIGN-L                                                              */
ASSIGN 
       edt_address:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* SETTINGS FOR FILL-IN Tenant.EntityCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Tenant.EntityType IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_b0 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_b1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_b2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_b3 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Entity IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Tenant.LegalName IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Tenant.Name IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Tenant.Quality IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Tenant.TenantCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Clear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Clear V-table-Win
ON CHOOSE OF btn_Clear IN FRAME F-Main /* Clear */
DO:
  RUN clear-address.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Address
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Address V-table-Win
ON VALUE-CHANGED OF cmb_Address IN FRAME F-Main
DO:
  RUN address-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Contact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Contact V-table-Win
ON VALUE-CHANGED OF cmb_Contact IN FRAME F-Main
DO:
  RUN contact-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_DebtClass
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_DebtClass V-table-Win
ON U1 OF cmb_DebtClass IN FRAME F-Main
DO:
  {inc/selcmb/scdebtcls1.i "Tenant" "DebtClassification"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_DebtClass V-table-Win
ON U2 OF cmb_DebtClass IN FRAME F-Main
DO:
  {inc/selcmb/scdebtcls2.i "Tenant" "DebtClassification"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Payment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Payment V-table-Win
ON U1 OF cmb_Payment IN FRAME F-Main
DO:
  {inc/selcmb/scrsty1.i "Tenant" "PaymentStyle"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Payment V-table-Win
ON U2 OF cmb_Payment IN FRAME F-Main
DO:
  {inc/selcmb/scrsty2.i "Tenant" "PaymentStyle"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone1 V-table-Win
ON VALUE-CHANGED OF cmb_Phone1 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone2 V-table-Win
ON VALUE-CHANGED OF cmb_Phone2 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone3 V-table-Win
ON VALUE-CHANGED OF cmb_Phone3 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Phone4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Phone4 V-table-Win
ON VALUE-CHANGED OF cmb_Phone4 IN FRAME F-Main
DO:
  RUN phone-changed( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_VarianceClass
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_VarianceClass V-table-Win
ON U1 OF cmb_VarianceClass IN FRAME F-Main
DO:
  {inc/selcmb/scvarcls1.i "Tenant" "VarianceClassification"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_VarianceClass V-table-Win
ON U2 OF cmb_VarianceClass IN FRAME F-Main
DO:
  {inc/selcmb/scvarcls2.i "Tenant" "VarianceClassification"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edt_address
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edt_address V-table-Win
ON LEAVE OF edt_address IN FRAME F-Main
DO:
  IF NUM-ENTRIES( SELF:SCREEN-VALUE, "~n" ) > 4 THEN DO:
    MESSAGE "Some window envelopes and/or reports may not correctly~n"
            "display addresses with more than four lines"
            VIEW-AS ALERT-BOX WARNING
            TITLE "Warning: possibly too many address lines".
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Tenant.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Tenant.EntityCode V-table-Win
ON LEAVE OF Tenant.EntityCode IN FRAME F-Main /* Co/Prop */
DO:
  CASE Tenant.EntityType:SCREEN-VALUE:
    WHEN 'P' THEN DO: {inc/selcde/cdpro.i "fil_Entity"} END.
    WHEN 'L' THEN DO: {inc/selcde/cdcmp.i "fil_Entity"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Entity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Entity V-table-Win
ON U1 OF fil_Entity IN FRAME F-Main
DO:
  CASE Tenant.EntityType:SCREEN-VALUE:
    WHEN 'P' THEN DO: {inc/selfil/sfpro1.i "Tenant" "EntityCode"} END.
    WHEN 'L' THEN DO: {inc/selfil/sfcmp1.i "Tenant" "EntityCode"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Entity V-table-Win
ON U2 OF fil_Entity IN FRAME F-Main
DO:
  CASE Tenant.EntityType:SCREEN-VALUE:
    WHEN 'P' THEN DO: {inc/selfil/sfpro2.i "Tenant" "EntityCode"} END.
    WHEN 'L' THEN DO: {inc/selfil/sfcmp2.i "Tenant" "EntityCode"} END.
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_FullName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_FullName V-table-Win
ON U1 OF fil_FullName IN FRAME F-Main
DO:
  {inc/selfil/sfpsn1.i "Tenant" "BillingContact"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_FullName V-table-Win
ON U2 OF fil_FullName IN FRAME F-Main
DO:
  {inc/selfil/sfpsn2.i "Tenant" "BillingContact"}
  FIND Contact WHERE ROWID( Contact ) = TO-ROWID( SELF:PRIVATE-DATA )
    NO-LOCK NO-ERROR.
  IF AVAILABLE Contact THEN RUN new-contact-selected( Contact.PersonCode ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_FullName V-table-Win
ON U3 OF fil_FullName IN FRAME F-Main
DO:
  /* Don't use this - assignment is done elsewhere in this screen */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone1 V-table-Win
ON LEAVE OF fil_Phone1 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone2 V-table-Win
ON LEAVE OF fil_Phone2 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone3 V-table-Win
ON LEAVE OF fil_Phone3 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Phone4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Phone4 V-table-Win
ON LEAVE OF fil_Phone4 IN FRAME F-Main
DO:
  RUN update-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Zip
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Zip V-table-Win
ON F3 OF fil_Zip IN FRAME F-Main
DO:
DEF VAR city AS CHAR NO-UNDO.
DEF VAR state AS CHAR NO-UNDO.
DEF VAR postcode AS CHAR NO-UNDO.
  DO WITH FRAME {&FRAME-NAME}:
    city = INPUT fil_City.
    state = INPUT fil_State.
    RUN process\postcode.p( city, state, OUTPUT postcode ).
/*
    IF postcode <> ? THEN DO:
      fil_Zip = postcode.
      DISPLAY fil_Zip.
    END.
*/
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Tenant.LegalName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Tenant.LegalName V-table-Win
ON LEAVE OF Tenant.LegalName IN FRAME F-Main /* Legal name */
DO:
  IF SELF:MODIFIED THEN DO:
    fil_Company:SCREEN-VALUE = INPUT FRAME {&FRAME-NAME} Tenant.LegalName.
    RUN enable-appropriate-fields.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Tenant.Name
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Tenant.Name V-table-Win
ON LEAVE OF Tenant.Name IN FRAME F-Main /* Name */
DO:
  IF SELF:MODIFIED THEN DO:
    fil_Company:SCREEN-VALUE = INPUT FRAME {&FRAME-NAME} Tenant.Name.
    RUN enable-appropriate-fields.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Tenant.TenantCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Tenant.TenantCode V-table-Win
ON LEAVE OF Tenant.TenantCode IN FRAME F-Main /* Tenant */
DO:
  IF SELF:MODIFIED THEN DO:
    RUN tenant-code-changed( INPUT FRAME {&FRAME-NAME} Tenant.TenantCode ).
    IF RETURN-VALUE = "NO-APPLY" THEN RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */


ON "ANY-PRINTABLE", "BACKSPACE", "DELETE-CHARACTER" OF fil_FullName, fil_Company
DO:
  APPLY LASTKEY TO SELF.
  RUN person-details-changed.
  RETURN NO-APPLY.
END.

{inc/ent-chg.i "Tenant.EntityType" "P,L" "entity-type-changed" "No"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE address-changed V-table-Win 
PROCEDURE address-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  this-address = INPUT cmb_Address.
  RUN update-address( last-address ).
  RUN display-address( this-address ).
  last-address = this-address.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Tenant"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Tenant"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Add" THEN RUN delete-tenant. ELSE RUN check-modified( "CLEAR" ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-address V-table-Win 
PROCEDURE clear-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  edt_Address:SCREEN-VALUE = "".
  fil_City:SCREEN-VALUE = "".
  fil_State:SCREEN-VALUE = "".
  fil_Country:SCREEN-VALUE = "".
  fil_Zip:SCREEN-VALUE = "".
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN contact-changed.
  RUN verify-tenant.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  RUN dispatch( 'update-record':U ).
  RUN fix-entity-contacts.
  IF mode = 'ADD' THEN RUN notify( 'open-query,record-source':U).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE contact-changed V-table-Win 
PROCEDURE contact-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  this-contact = INPUT cmb_Contact.  this-address = INPUT cmb_Address.

  RUN update-person( last-contact ).
  RUN update-address( last-address ).
  RUN update-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN update-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN update-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN update-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).

  RUN default-combos.
  
  RUN display-person( this-contact ).
  RUN display-address( this-address ).
  RUN get-phone-types.
  RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).

  last-contact = this-contact.  last-address = this-address.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE default-combos V-table-Win 
PROCEDURE default-combos :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-tenant V-table-Win 
PROCEDURE delete-tenant :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FIND CURRENT Tenant EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Tenant THEN DELETE Tenant.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-address V-table-Win 
PROCEDURE display-address :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER address-desc AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR postal-type LIKE PostalType.PostalType NO-UNDO.
  
  RUN get-address-code( address-desc, OUTPUT postal-type ).
  FIND PostalDetail WHERE
    PostalDetail.PersonCode = Person.PersonCode AND
    PostalDetail.PostalType = postal-type
    NO-LOCK NO-ERROR.

  ASSIGN
    edt_address = IF AVAILABLE Postaldetail THEN PostalDetail.Address ELSE ""
    fil_City    = IF AVAILABLE Postaldetail THEN PostalDetail.City ELSE ""
    fil_State   = IF AVAILABLE Postaldetail THEN PostalDetail.State ELSE ""
    fil_Zip     = IF AVAILABLE Postaldetail THEN PostalDetail.Zip ELSE ""
    fil_Country = IF AVAILABLE Postaldetail THEN PostalDetail.Country ELSE "".
    
  DISPLAY edt_address fil_City fil_State fil_Zip fil_Country WITH FRAME {&FRAME-NAME}.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-person V-table-Win 
PROCEDURE display-person :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER person-desc AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR person-code LIKE Person.PersonCode NO-UNDO.
  
  RUN get-person-code( person-desc, OUTPUT person-code ).
  FIND Person WHERE Person.PersonCode = person-code NO-LOCK NO-ERROR.

  IF AVAILABLE(Person) THEN DO:
    fil_FullName = combine-name( Person.PersonTitle, Person.FirstName, Person.MiddleName,
                                     Person.LastName, Person.NameSuffix ).
    fil_Company   = IF AVAILABLE Person THEN Person.Company   ELSE "".
  END.
  ELSE ASSIGN
    fil_FullName = ""
    fil_Company = "".
    
  DISPLAY fil_FullName fil_Company WITH FRAME {&FRAME-NAME}.
  RUN person-details-changed.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-phone V-table-Win 
PROCEDURE display-phone :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:


  IF NOT AVAILABLE(Person) THEN RETURN.

  DEF VAR idx        AS INT  NO-UNDO.
  DEF VAR phone-type AS CHAR NO-UNDO.
  DEF VAR sv         AS CHAR NO-UNDO INITIAL "".

  idx = LOOKUP( h-cmb:SCREEN-VALUE, h-cmb:LIST-ITEMS ).
  phone-type = ENTRY( idx, h-cmb:PRIVATE-DATA ).
  
  FIND PhoneDetail WHERE
    PhoneDetail.PersonCode = Person.PersonCode AND
    PhoneDetail.PhoneType  = phone-type
    NO-LOCK NO-ERROR.

  IF AVAILABLE PhoneDetail THEN
    RUN combine-phone( PhoneDetail.cCountryCode, PhoneDetail.cSTDCode, PhoneDetail.Number,
      OUTPUT sv ).
  
  h-fil:SCREEN-VALUE = sv.
  h-fil:PRIVATE-DATA = phone-type.  

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF Tenant.Name <> "" OR Tenant.LegalName <> "" OR Tenant.BillingContact > 0 
        OR CAN-FIND( FIRST AcctTran WHERE AcctTran.EntityType = "T" AND AcctTran.EntityCode = Tenant.TenantCode ) THEN
    DISABLE Tenant.TenantCode .

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-changed V-table-Win 
PROCEDURE entity-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-type-changed V-table-Win 
PROCEDURE entity-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  /* Reset the entity code / description fields */
  
  Tenant.EntityCode:PRIVATE-DATA = ",".
  Tenant.EntityCode:SCREEN-VALUE = STRING( 0 ).
  fil_Entity:PRIVATE-DATA = "".
  fil_Entity:SCREEN-VALUE = "".

  RUN update-entity-button.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fix-entity-contacts V-table-Win 
PROCEDURE fix-entity-contacts :
/*------------------------------------------------------------------------------
  Purpose: Fix the ACCT/PROP contacts to match the person linked from the Tenant
------------------------------------------------------------------------------*/
  
  fix-contact( 'ACCT', Tenant.BillingContact ).
  fix-contact( 'PROP', Tenant.PropertyContact ).
  fix-contact( 'AH1', Tenant.AH1Contact ).
  fix-contact( 'AH2', Tenant.AH2Contact ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-address-code V-table-Win 
PROCEDURE get-address-code :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT  PARAMETER address-desc AS CHAR NO-UNDO.
  DEF OUTPUT PARAMETER postal-type  AS CHAR NO-UNDO.
  
  DEF VAR idx AS INT NO-UNDO.
  idx = LOOKUP( address-desc, cmb_Address:LIST-ITEMS, cmb_Address:DELIMITER ).
  postal-type = ENTRY( idx, cmb_Address:PRIVATE-DATA ).

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-address-types V-table-Win 
PROCEDURE get-address-types :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item AS CHAR NO-UNDO.
  DEF VAR pd   AS CHAR NO-UNDO.
  
  cmb_Address:LIST-ITEMS = "".
  
  FOR EACH PostalType NO-LOCK:
    item = PostalType.Description.
    IF cmb_Address:ADD-LAST( item ) THEN.
    pd = pd + IF pd = "" THEN "" ELSE ",".
    pd = pd + PostalType.PostalType.
  END.
  
  cmb_address:SCREEN-VALUE = cmb_Address:ENTRY( 1 ).
  last-address             = cmb_address:ENTRY( 1 ).
  cmb_address:PRIVATE-DATA = pd.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-contact-types V-table-Win 
PROCEDURE get-contact-types :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  cmb_Contact:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "Accounting Matters".
  last-contact = "Accounting Matters".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-person-code V-table-Win 
PROCEDURE get-person-code :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER person-desc AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER person-code LIKE Person.PersonCode NO-UNDO.

  IF NOT AVAILABLE Tenant THEN RETURN.
  
  CASE person-desc:
    WHEN "Accounting Matters"      THEN person-code = Tenant.BillingContact.
    WHEN "Property Matters"        THEN person-code = Tenant.PropertyContact.
    WHEN "After Hours Emergency 1" THEN person-code = Tenant.AH1.
    WHEN "After Hours Emergency 2" THEN person-code = Tenant.AH2.
  END CASE.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-phone-types V-table-Win 
PROCEDURE get-phone-types :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR pd   AS CHAR NO-UNDO.
  DEF VAR li   AS CHAR NO-UNDO.
  DEF VAR sv-list   AS CHAR NO-UNDO INITIAL "".
  DEF VAR pd-list   AS CHAR NO-UNDO INITIAL "".
  DEF VAR sv-list2  AS CHAR NO-UNDO INITIAL "".
  DEF VAR pd-list2  AS CHAR NO-UNDO INITIAL "".
  DEF VAR i         AS INT  NO-UNDO.
    
  DO i = 1 TO NUM-ENTRIES( phone-types ):
    pd = ENTRY( i, phone-types ).
    FIND PhoneType WHERE PhoneType = pd NO-LOCK NO-ERROR.
    li = (IF AVAILABLE(PhoneType) THEN PhoneType.Description ELSE pd).
    IF AVAILABLE(Person) AND CAN-FIND( PhoneDetail OF Person WHERE PhoneDetail.PhoneType = pd)
    THEN DO:
      sv-list = sv-list + "," + li.
      pd-list = pd-list + "," + pd.
    END.
    ELSE DO:
      sv-list2 = sv-list2 + "," + li.
      pd-list2 = pd-list2 + "," + pd.
    END.
  END.
  IF NUM-ENTRIES(sv-list) < 5 THEN DO:
    sv-list = sv-list + sv-list2.
    pd-list = pd-list + pd-list2.
  END.
  sv-list = TRIM(sv-list, ",").
  pd-list = TRIM(pd-list, ",").

  li = "".
  pd = "".
  FOR EACH PhoneType NO-LOCK:
    li = li + (IF li = "" THEN "" ELSE ",") + PhoneType.Description.
    pd = pd + (IF pd = "" THEN "" ELSE ",") + PhoneType.PhoneType.
  END.

  cmb_Phone1:LIST-ITEMS = li.  cmb_Phone2:LIST-ITEMS = li.
  cmb_Phone3:LIST-ITEMS = li.  cmb_Phone4:LIST-ITEMS = li.

  cmb_Phone1:SCREEN-VALUE = ENTRY( 1, sv-list ).
  cmb_Phone2:SCREEN-VALUE = ENTRY( 2, sv-list ).
  cmb_Phone3:SCREEN-VALUE = ENTRY( 3, sv-list ).
  cmb_Phone4:SCREEN-VALUE = ENTRY( 4, sv-list ).
  
  fil_Phone1:PRIVATE-DATA = ENTRY( 1, phone-types ).
  fil_Phone2:PRIVATE-DATA = ENTRY( 2, phone-types ).
  fil_Phone3:PRIVATE-DATA = ENTRY( 3, phone-types ).
  fil_Phone4:PRIVATE-DATA = ENTRY( 4, phone-types ).

  cmb_Phone1:PRIVATE-DATA = pd.  cmb_Phone2:PRIVATE-DATA = pd.
  cmb_Phone3:PRIVATE-DATA = pd.  cmb_Phone4:PRIVATE-DATA = pd.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN update-person( last-contact ).
  RUN update-address( last-address ).
  RUN update-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN update-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN update-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN update-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-disable-fields V-table-Win 
PROCEDURE inst-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  DISABLE cmb_Payment cmb_DebtClass cmb_VarianceClass.
  RUN set-widget-colours( FRAME {&FRAME-NAME}:HANDLE ) .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  RUN update-entity-button.

  RUN display-person( last-contact ).
  RUN display-address( last-address ).
  RUN get-phone-types.
  RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).

  APPLY 'ENTRY':U TO Tenant.Name IN FRAME {&FRAME-NAME}.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN enable-appropriate-fields.
  IF mode = "View" THEN RUN dispatch( 'disable-fields':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = "Add" THEN
    RUN dispatch( 'add-record':U ).
  ELSE IF mode = "Maintain" THEN
    RUN dispatch( 'enable-fields' ).
  ELSE
    RUN dispatch( 'disable-fields' ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
IF NOT AVAILABLE(Tenant) THEN RETURN.

FIND LAST Month WHERE Month.StartDate <= TODAY NO-LOCK.

DO WITH FRAME {&FRAME-NAME}:
  RUN get-aged-balances( Tenant.TenantCode, Month.MonthCode, OUTPUT fil_b0, OUTPUT fil_b1, OUTPUT fil_b2, OUTPUT fil_b3 ).
  DISPLAY fil_b0 fil_b1 fil_b2 fil_b3.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE new-contact-selected V-table-Win 
PROCEDURE new-contact-selected :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER person-code LIKE Person.PersonCode NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  this-contact = INPUT cmb_Contact.  this-address = INPUT cmb_Address.

  RUN update-person( last-contact ).
  RUN update-address( last-address ).
  RUN update-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN update-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN update-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN update-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).

  FIND Person NO-LOCK WHERE Person.PersonCode = person-code NO-ERROR.
  IF NOT AVAILABLE(Person) THEN RETURN.

  DEF VAR priori-list  AS CHAR NO-UNDO.
  DEF VAR i AS INT NO-UNDO.
  priori-list = "POST,MAIN,COUR".
  FIND PostalDetail NO-LOCK OF Person WHERE PostalDetail.PostalType = "BILL" NO-ERROR.
  IF NOT AVAILABLE(PostalDetail) THEN DO:
    DEF BUFFER BILLAddress FOR PostalDetail.

     DO i = 1 TO 3 WHILE NOT AVAILABLE(PostalDetail):
       FIND PostalDetail NO-LOCK OF Person WHERE PostalDetail.PostalType = ENTRY( i, priori-list) NO-ERROR.
     END.

     IF AVAILABLE(PostalDetail) THEN DO:
       CREATE BILLAddress.
       BUFFER-COPY PostalDetail TO BILLAddress ASSIGN BILLAddress.PostalType = "BILL".
       BILLAddress.PostalType = "BILL".
     END.
  END.

  RUN update-person-code( this-contact, person-code).
  RUN default-combos.

  RUN display-person( this-contact ).
  RUN display-address( this-address ).

  RUN get-phone-types.
  RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
  RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
  RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
  RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  CREATE Tenant.
  ASSIGN Tenant.Active = Yes .

  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'display-fields':U ).

  CURRENT-WINDOW:TITLE = "Adding new tenant " + STRING( Tenant.TenantCode, "99999").
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE person-details-changed V-table-Win 
PROCEDURE person-details-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN sensitise-address-details(
    NOT ( INPUT fil_fullname = "" AND INPUT fil_Company = "" ) ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE phone-changed V-table-Win 
PROCEDURE phone-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  RUN update-phone( h-cmb, h-fil ).
  RUN display-phone( h-cmb, h-fil ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE":U THEN RUN cancel-changes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize V-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN get-contact-types.
  RUN get-phone-types.
  RUN get-address-types.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Tenant"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-address-details V-table-Win 
PROCEDURE sensitise-address-details :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER enable-it AS LOGI NO-UNDO.

DEF VAR maintainable AS LOGI NO-UNDO.
DO WITH FRAME {&FRAME-NAME}:

  maintainable = (mode <> "View").
  IF NOT maintainable THEN 
  DO:
    edt_address:READ-ONLY = Yes.
    DISABLE edt_address fil_city fil_state fil_zip fil_Country
            fil_Phone1 fil_Phone2 fil_Phone3 fil_Phone4 
            fil_FullName fil_Company btn_Clear .
    RETURN.
  END.
  
  IF cmb_address:SENSITIVE <> enable-it THEN DO:
    cmb_address:SENSITIVE   = enable-it.
    edt_address:SENSITIVE   = enable-it.
    fil_city:SENSITIVE      = enable-it.
    fil_state:SENSITIVE     = enable-it.
    fil_zip:SENSITIVE       = enable-it.
    fil_Country:SENSITIVE   = enable-it.
    cmb_Phone1:SENSITIVE    = enable-it.
    cmb_Phone2:SENSITIVE    = enable-it.
    cmb_Phone3:SENSITIVE    = enable-it.
    cmb_Phone4:SENSITIVE    = enable-it.
    fil_Phone1:SENSITIVE    = enable-it.
    fil_Phone2:SENSITIVE    = enable-it.
    fil_Phone3:SENSITIVE    = enable-it.
    fil_Phone4:SENSITIVE    = enable-it.
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-widget-colours V-table-Win 
PROCEDURE set-widget-colours :
/*------------------------------------------------------------------------------
  Purpose:  Set the colours for desensitized widgets to more reasonable ones
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER wh AS HANDLE NO-UNDO.

  wh = wh:FIRST-CHILD.
  DO WHILE VALID-HANDLE(wh):
    IF LOOKUP( wh:TYPE, "COMBO-BOX,EDITOR" ) > 0 THEN DO:
      wh:BGCOLOR = 16.
      wh:FGCOLOR = 1.
    END.
    ELSE IF LOOKUP( wh:TYPE, "FIELD-GROUP" ) > 0 THEN DO:
      RUN set-widget-colours( wh ).
    END.

    wh = wh:NEXT-SIBLING.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE tenant-code-changed V-table-Win 
PROCEDURE tenant-code-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-tc AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  IF new-tc <> Tenant.TenantCode
                AND tenant-exists( new-tc )
  THEN DO:
    MESSAGE "Tenant already exists with that tenant code"
            VIEW-AS ALERT-BOX ERROR
            TITLE "Tenant Already Exists".
    DISPLAY Tenant.TenantCode.
    SELF:MODIFIED = No.
    RETURN "NO-APPLY".
  END.
  ELSE DO:
    FIND CURRENT Tenant EXCLUSIVE-LOCK NO-ERROR.
    ASSIGN Tenant.TenantCode .
    DISPLAY Tenant.TenantCode .
    FIND CURRENT Tenant NO-LOCK NO-ERROR.
    SELF:MODIFIED = No.
    RETURN "".
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-address V-table-Win 
PROCEDURE update-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER address-desc AS CHAR NO-UNDO.

  DEF VAR postal-type LIKE PostalType.PostalType NO-UNDO.
  DEF VAR del-record AS LOGI NO-UNDO.

  IF address-desc = ""    THEN RETURN.
  IF NOT AVAILABLE Person THEN RUN update-person( last-contact ).
  IF NOT AVAILABLE Person THEN DO:
  DEF VAR person-code AS INT NO-UNDO.
    RUN get-person-code( last-contact, OUTPUT person-code ).
    FIND Person WHERE Person.PersonCode = person-code NO-LOCK NO-ERROR.
  END.
  IF NOT AVAILABLE Person THEN RETURN.

  RUN get-address-code( address-desc, OUTPUT postal-type ).
  FIND PostalDetail WHERE
    PostalDetail.PersonCode = Person.PersonCode AND
    PostalDetail.PostalType = postal-type NO-ERROR.
  ASSIGN FRAME {&FRAME-NAME} edt_address fil_state fil_zip fil_country fil_city.
  del-record = INPUT edt_address = ""
             AND INPUT fil_zip = ""
             AND INPUT fil_country = ""
             AND INPUT fil_City = "".

  IF del-record THEN DO:
    IF AVAILABLE PostalDetail THEN DELETE PostalDetail.
  END.
  ELSE DO:
    IF NOT AVAILABLE PostalDetail THEN DO:
      CREATE PostalDetail.
      ASSIGN
        PostalDetail.PersonCode = Person.PersonCode
        PostalDetail.PostalType = postal-type.
    END.

    ASSIGN
      Postaldetail.Address = TRIM( edt_Address )
      PostalDetail.City    = fil_city
      PostalDetail.State   = fil_State
      PostalDetail.Zip     = fil_Zip
      PostalDetail.Country = fil_Country.      

  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-button V-table-Win 
PROCEDURE update-entity-button :
/*------------------------------------------------------------------------------
  Purpose:     Update the select button for the entity
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR node-name    AS CHAR NO-UNDO.
  DEF VAR vwr-name     AS CHAR NO-UNDO.
  DEF VAR options      AS CHAR NO-UNDO.
  
  FIND EntityType WHERE EntityType.EntityType = INPUT Tenant.EntityType
    NO-LOCK NO-ERROR.
  IF NOT AVAILABLE EntityType THEN RETURN.
  Tenant.EntityType:LABEL = EntityType.Description.
    
  CASE INPUT Tenant.EntityType:
    WHEN 'P' THEN ASSIGN
      node-name = 'w-selpro' vwr-name = 'b-selpro'
      options = "FilterPanel = Yes~n SortPanel = Yes~n AlphabetPanel = Yes".
    WHEN 'L' THEN ASSIGN
      node-name = 'w-selcmp' vwr-name = 'b-selcmp'
      options = "FilterPanel = Yes~n SortPanel = Yes~n AlphabetPanel = Yes".
  END CASE.

  RUN set-link-attributes IN sys-mgr (
    THIS-PROCEDURE, 
    STRING( fil_Entity:HANDLE ),
    "Target = " + node-name + "," +
    "Viewer = " + vwr-name + "," +
    "Function = " + options + "," +
    "Sensitive = " + STRING( (mode <> "View"), "Yes/No" )
  ).
          
END.

  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-person V-table-Win 
PROCEDURE update-person :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER person-desc AS CHAR NO-UNDO.

  DEF VAR person-code LIKE Person.PersonCode NO-UNDO.
  DEF VAR del-record AS LOGI NO-UNDO.

  RUN get-person-code( person-desc, OUTPUT person-code ).
  FIND Person WHERE Person.PersonCode = person-code NO-ERROR.
  
  ASSIGN FRAME {&FRAME-NAME} fil_fullname fil_Company edt_Address fil_City.
  del-record = INPUT fil_fullname = ""
           AND INPUT fil_Company = ""
           AND INPUT edt_Address = ""
           AND INPUT fil_City = "".

  IF del-record THEN DO:
    IF AVAILABLE Person THEN DO:
      RUN update-person-code( person-desc, 0 ).
      DELETE Person.
    END.
  END.
  ELSE DO:
    IF NOT AVAILABLE(Person) THEN CREATE Person.
    FIND Contact OF Person WHERE Contact.ContactType = "TNNT" NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(Contact) THEN DO:
      CREATE Contact.
      ASSIGN
        Contact.PersonCode  = Person.PersonCode
        Contact.ContactType = "TNNT".
    END.
    RUN update-person-code( person-desc, Person.PersonCode ).

    RUN split-name( INPUT fil_FullName, OUTPUT Person.PersonTitle, OUTPUT Person.FirstName,
            OUTPUT Person.MiddleName, OUTPUT Person.LastName, OUTPUT Person.NameSuffix ).
    ASSIGN Person.Company   = INPUT fil_Company.

  END.
  FIND CURRENT Person NO-LOCK NO-ERROR.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-person-code V-table-Win 
PROCEDURE update-person-code :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER contact-desc AS CHAR NO-UNDO.
DEF INPUT PARAMETER contact-code LIKE Person.PersonCode NO-UNDO.

  DEF VAR prev-code LIKE Person.PersonCode NO-UNDO.
  CASE contact-desc:
    WHEN "Accounting Matters"      THEN ASSIGN prev-code = Tenant.BillingContact.
    WHEN "Property Matters"        THEN ASSIGN prev-code = Tenant.PropertyContact.
    WHEN "After Hours Emergency 1" THEN ASSIGN prev-code = Tenant.AH1.
    WHEN "After Hours Emergency 2" THEN ASSIGN prev-code = Tenant.AH2.
  END CASE.

  IF prev-code <> contact-code THEN DO:
    DEF BUFFER AltPerson FOR Person.
    DEF BUFFER AltContact FOR Contact.
    FIND AltPerson WHERE AltPerson.PersonCode = prev-code NO-ERROR.
    IF AVAILABLE(AltPerson) THEN DO:
      FIND AltContact OF AltPerson WHERE AltContact.ContactType = "TNNT" NO-ERROR.
      IF AVAILABLE(AltContact) THEN DELETE AltContact .
      IF NOT CAN-FIND( FIRST AltContact OF AltPerson ) THEN DELETE AltPerson .
    END.

    FIND CURRENT Tenant EXCLUSIVE-LOCK NO-ERROR.
    IF NOT AVAILABLE Tenant THEN RETURN.

    CASE contact-desc:
      WHEN "Accounting Matters"      THEN ASSIGN Tenant.BillingContact = contact-code.
      WHEN "Property Matters"        THEN ASSIGN Tenant.PropertyContact = contact-code.
      WHEN "After Hours Emergency 1" THEN ASSIGN Tenant.AH1 = contact-code.
      WHEN "After Hours Emergency 2" THEN ASSIGN Tenant.AH2 = contact-code.
    END CASE.

    FIND CURRENT Tenant NO-LOCK.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-phone V-table-Win 
PROCEDURE update-phone :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER h-cmb AS HANDLE NO-UNDO.
DEF INPUT PARAMETER h-fil AS HANDLE NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  
  DEF VAR del-record AS LOGI NO-UNDO.

  IF NOT AVAILABLE Person THEN RUN update-person( last-contact ).
  IF NOT AVAILABLE Person THEN DO:
  DEF VAR person-code AS INT NO-UNDO.
    RUN get-person-code( last-contact, OUTPUT person-code ).
    FIND Person WHERE Person.PersonCode = person-code NO-LOCK NO-ERROR.
  END.
  IF NOT AVAILABLE Person THEN RETURN.
  FIND PhoneDetail WHERE
    PhoneDetail.PersonCode = Person.PersonCode AND
    PhoneDetail.PhoneType = h-fil:PRIVATE-DATA   NO-ERROR.

  del-record = TRIM( h-fil:SCREEN-VALUE ) = "".

  IF del-record THEN DO:
    IF AVAILABLE PhoneDetail THEN DELETE PhoneDetail.
  END.
  ELSE DO:
    IF NOT AVAILABLE PhoneDetail THEN
    DO:
      CREATE PhoneDetail.
      ASSIGN
        PhoneDetail.PersonCode = Person.PersonCode
        PhoneDetail.PhoneType  = h-fil:PRIVATE-DATA.
    END.

    RUN split-phone( h-fil:SCREEN-VALUE, OUTPUT PhoneDetail.cCountryCode,
                      OUTPUT PhoneDetail.cSTDCode, OUTPUT PhoneDetail.Number ).
      
  END.

  /* Reflect changes to other linked numbers on screen */

  DEF VAR update-type AS CHAR NO-UNDO. /* Do not delete this variable!! */
  update-type = h-fil:PRIVATE-DATA.

  IF fil_Phone1:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone1:HANDLE, fil_Phone1:HANDLE ).
        
  IF fil_Phone2:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone2:HANDLE, fil_Phone2:HANDLE ).
        
  IF fil_Phone3:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone3:HANDLE, fil_Phone3:HANDLE ).
        
  IF fil_Phone4:PRIVATE-DATA = update-type THEN
    RUN display-phone( cmb_Phone4:HANDLE, fil_Phone4:HANDLE ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode V-table-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
  have-records = mode = "Add".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-tenant V-table-Win 
PROCEDURE verify-tenant :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR error-message AS CHAR NO-UNDO INITIAL ?.
DO WITH FRAME {&FRAME-NAME}:
  CASE INPUT Tenant.EntityType:
    WHEN "L" THEN DO:
      FIND Company WHERE Company.CompanyCode = INPUT Tenant.EntityCode NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(Company) THEN DO:
        error-message = "Company '" + STRING(Tenant.EntityCode) + "' not on file.".
        APPLY 'ENTRY' TO Tenant.EntityCode.
      END.
    END.
    WHEN "P" THEN DO:
      FIND Property WHERE Property.PropertyCode = INPUT Tenant.EntityCode NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(Property) THEN DO:
        error-message = "Property '" + STRING(Tenant.EntityCode) + "' not on file.".
        APPLY 'ENTRY' TO Tenant.EntityCode.
      END.
    END.
    OTHERWISE DO:
      error-message = "Only L or P entities are valid.".
      APPLY 'ENTRY' TO Tenant.EntityType.
    END.
  END CASE.
  IF error-message <> ? THEN DO:
    MESSAGE error-message VIEW-AS ALERT-BOX ERROR TITLE "Error With Tenant".
    RETURN "FAIL".
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION fix-contact V-table-Win 
FUNCTION fix-contact RETURNS LOGICAL
  ( INPUT contact-type AS CHAR, INPUT person-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose: Fix the ACCT/PROP contacts to match the person linked from the Tenant
------------------------------------------------------------------------------*/
  FIND FIRST EntityContact WHERE EntityContact.EntityType = "T"
                AND EntityContact.EntityCode = Tenant.TenantCode
                AND EntityContact.EntityContactType = contact-type
                EXCLUSIVE-LOCK NO-ERROR.
  IF person-code = ? OR person-code < 1 THEN DO:
    IF AVAILABLE(EntityContact) THEN DELETE EntityContact.
    RETURN TRUE.
  END.
  IF NOT AVAILABLE(EntityContact) THEN DO:
    CREATE EntityContact.
    EntityContact.EntityType = "T".
    EntityContact.EntityCode = Tenant.TenantCode.
    EntityContact.EntityContactType = contact-type.
  END.

  EntityContact.PersonCode = person-code.
  
  RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION tenant-exists V-table-Win 
FUNCTION tenant-exists RETURNS LOGICAL
  ( INPUT tc AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Test if Tenant TC already exists
------------------------------------------------------------------------------*/
  RETURN CAN-FIND( Tenant WHERE Tenant.TenantCode = tc).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

