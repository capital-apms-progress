&ANALYZE-SUSPEND _VERSION-NUMBER AB_v9r12 GUI ADM2
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/entity.i}
{inc/topic/tpvouchr.i}

{inc/ofc-this.i}
DEF VAR gst-applies    AS LOGI NO-UNDO.
gst-applies = (Office.GST <> ?).
DEF VAR gst-in-ecode           AS INT NO-UNDO.
DEF VAR sundry-creditors-ecode AS INT NO-UNDO.
{inc/ofc-acct.i "CREDITORS" "sundry-creditors"}
sundry-creditors-ecode = OfficeControlAccount.EntityCode.
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}
{inc/ofc-acct.i "GST-IN" "gst-in"}
gst-in-ecode = OfficeControlAccount.EntityCode.

{inc/ofc-set.i "Voucher-Style" "voucher-style" "WARNING"}
{inc/ofc-set.i "Voucher-Orders" "voucher-orders"}
{inc/ofc-set.i "Voucher-Printing" "print-vouchers"}
{inc/ofc-set.i "Voucher-Transaction-Date" "voucher-transaction-date"}
IF NOT AVAILABLE(OfficeSetting) THEN voucher-transaction-date = "Invoice/Due-10".
{inc/ofc-set-l.i "Voucher-Client-Checking" "client-checking"}
{inc/ofc-set-l.i "Tenant-Accounts" "tenant-accounts"}
IF tenant-accounts = ? THEN tenant-accounts = No.
{inc/ofc-set-l.i "Voucher-Trans-Due-Date" "use-due-dates"}
{inc/ofc-set.i "Voucher-Due-Next" "voucher-due-next"}
DEF VAR default-due-next AS INT NO-UNDO.
ASSIGN default-due-next = INT( voucher-due-next) NO-ERROR.
{inc/ofc-set-l.i "Voucher-Payment-Styles" "use-payment-styles"}
{inc/ofc-set-l.i "Restrict-Project-Posting" "restrict-project-posting"}
{inc/ofc-set-l.i "Voucher-Allow-Unallocated" "allow-unallocated"}
{inc/ofc-set-l.i "GST-Multi-Company"  "GST-Multi-Company" }
{inc/ofc-set.i "GST-Rate"  "gst-rate" }

/* Each creditor has attributed balances for each ledger (c.f. George Group) */
{inc/ofc-set-l.i "Multi-Ledger-Creditors" "multi-ledger-creditors"}



DEF VAR coa-list AS HANDLE NO-UNDO.
DEF VAR enabling-fields AS LOGICAL NO-UNDO INITIAL No.

DEF VAR insert-new-line AS LOGI NO-UNDO.
DEF VAR new-result-entry AS LOGICAL NO-UNDO.
DEF VAR my-new-line     AS LOGI NO-UNDO INITIAL No.
DEF VAR trn-modified    AS LOGI NO-UNDO.
DEF VAR last-line       AS LOGI NO-UNDO.
DEF VAR verifying-line  AS LOGI NO-UNDO.
DEF VAR selecting-code  AS LOGI NO-UNDO.
DEF VAR verified-batch  AS LOGI NO-UNDO INITIAL No.
DEF VAR printed-vouchers AS LOGI NO-UNDO INITIAL No.


DEF VAR mode           AS CHAR NO-UNDO.
DEF VAR last-creditor  LIKE Creditor.CreditorCode NO-UNDO.

DEF WORK-TABLE CurrentLine NO-UNDO LIKE VoucherLine.
DEF WORK-TABLE DefTrans    NO-UNDO LIKE VoucherLine.

DEF VAR last-widg AS HANDLE  NO-UNDO.
DEF VAR voucher-list AS CHAR NO-UNDO.

DEF VAR last-order-code   AS INT NO-UNDO.
DEF VAR last-entity-type AS CHAR NO-UNDO.
DEF VAR last-entity-code AS INT NO-UNDO.

    
DEF NEW SHARED TEMP-TABLE company-gst 
         FIELD company_id LIKE Company.CompanyCode
         FIELD tax_amount  LIKE NewAcctTrans.Amount 
         INDEX company_id IS PRIMARY company_id ASCENDING.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-vchlne

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Voucher
&Scoped-define FIRST-EXTERNAL-TABLE Voucher


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Voucher.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES VoucherLine

/* Definitions for BROWSE br-vchlne                                     */
&Scoped-define FIELDS-IN-QUERY-br-vchlne VoucherLine.EntityType ~
VoucherLine.EntityCode VoucherLine.AccountCode VoucherLine.Description ~
VoucherLine.Amount 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-vchlne VoucherLine.EntityType ~
VoucherLine.EntityCode VoucherLine.AccountCode VoucherLine.Description ~
VoucherLine.Amount 
&Scoped-define ENABLED-TABLES-IN-QUERY-br-vchlne VoucherLine
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br-vchlne VoucherLine
&Scoped-define QUERY-STRING-br-vchlne FOR EACH VoucherLine OF Voucher NO-LOCK
&Scoped-define OPEN-QUERY-br-vchlne OPEN QUERY br-vchlne FOR EACH VoucherLine OF Voucher NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br-vchlne VoucherLine
&Scoped-define FIRST-TABLE-IN-QUERY-br-vchlne VoucherLine


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Voucher.VoucherSeq Voucher.EntityType ~
Voucher.EntityCode Voucher.OrderCode Voucher.CreditorCode Voucher.Date ~
Voucher.ApproverCode Voucher.SecondApprover Voucher.InvoiceReference ~
Voucher.OurOrderNo Voucher.DateDue Voucher.GoodsValue Voucher.Description 
&Scoped-define ENABLED-TABLES Voucher
&Scoped-define FIRST-ENABLED-TABLE Voucher
&Scoped-Define ENABLED-OBJECTS fil_batch-no cmb_PaymentStyle br-vchlne ~
Btn_Add-Trn btn_ok btn_cancel RECT-1 RECT-2 
&Scoped-Define DISPLAYED-FIELDS Voucher.VoucherSeq Voucher.EntityType ~
Voucher.EntityCode Voucher.OrderCode Voucher.CreditorCode Voucher.Date ~
Voucher.ApproverCode Voucher.SecondApprover Voucher.InvoiceReference ~
Voucher.OurOrderNo Voucher.DateDue Voucher.GoodsValue Voucher.Description 
&Scoped-define DISPLAYED-TABLES Voucher
&Scoped-define FIRST-DISPLAYED-TABLE Voucher
&Scoped-Define DISPLAYED-OBJECTS fil_batch-no fil_batch-text fil_Creditor ~
fil_approval fil_approval-2 cmb_PaymentStyle fil_total fil_Entity ~
fil_Account 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-trn-date V-table-Win 
FUNCTION get-trn-date RETURNS DATE
  ( OUTPUT m-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validate-creditor-ledger V-table-Win 
FUNCTION validate-creditor-ledger RETURNS INTEGER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "Register &Another" 
     SIZE 16 BY 1.05 TOOLTIP "Finalise the registration of this voucher and start registering another."
     FONT 9.

DEFINE BUTTON Btn_Add-Trn 
     LABEL "Add Tr" 
     SIZE 15 BY 1.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 9.72 BY 1.05
     FONT 9.

DEFINE BUTTON btn_ok 
     LABEL "&Done" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE VARIABLE cmb_PaymentStyle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Pay by" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 23.43 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Account AS CHARACTER FORMAT "X(256)":U 
     LABEL "Account" 
     VIEW-AS FILL-IN 
     SIZE 76 BY .9
     FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE fil_approval AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 46.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_approval-2 AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 46.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_batch-no AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "Batch" 
     VIEW-AS FILL-IN 
     SIZE 6.29 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_batch-text AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Creditor AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 45.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Entity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS FILL-IN 
     SIZE 45.72 BY .9
     FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE fil_total AS DECIMAL FORMAT "-ZZ,ZZZ,ZZ9.99":U INITIAL 0 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 15.57 BY 1
     FONT 11 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 86.29 BY 19.4.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 85.14 BY 7.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-vchlne FOR 
      VoucherLine SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-vchlne
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-vchlne V-table-Win _STRUCTURED
  QUERY br-vchlne NO-LOCK DISPLAY
      VoucherLine.EntityType FORMAT "X":U COLUMN-FONT 10
      VoucherLine.EntityCode FORMAT "99999":U
      VoucherLine.AccountCode FORMAT "9999.99":U
      VoucherLine.Description FORMAT "X(50)":U
      VoucherLine.Amount FORMAT "->>>,>>>,>>9.99":U
  ENABLE
      VoucherLine.EntityType
      VoucherLine.EntityCode
      VoucherLine.AccountCode
      VoucherLine.Description
      VoucherLine.Amount
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 85.14 BY 6.5
         FONT 8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Voucher.VoucherSeq AT ROW 1.2 COL 8.72 COLON-ALIGNED
          LABEL "Voucher No"
          VIEW-AS FILL-IN 
          SIZE 9.72 BY 1
     Voucher.EntityType AT ROW 2.2 COL 8.72 COLON-ALIGNED
          LABEL "Code/Order"
          VIEW-AS FILL-IN 
          SIZE 2 BY 1
     fil_batch-no AT ROW 1.2 COL 29.29 COLON-ALIGNED
     fil_batch-text AT ROW 1.2 COL 39 COLON-ALIGNED NO-LABEL
     Voucher.EntityCode AT ROW 2.2 COL 10.72 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 5.43 BY 1
     Voucher.OrderCode AT ROW 2.2 COL 16.14 COLON-ALIGNED NO-LABEL FORMAT ">>9"
          VIEW-AS FILL-IN 
          SIZE 5.14 BY 1
     Voucher.CreditorCode AT ROW 2.2 COL 29.29 COLON-ALIGNED
          LABEL "Creditor"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_Creditor AT ROW 2.2 COL 39 COLON-ALIGNED NO-LABEL
     Voucher.Date AT ROW 4.2 COL 8.72 COLON-ALIGNED
          LABEL "Invc. Date"
          VIEW-AS FILL-IN 
          SIZE 10.86 BY 1
     Voucher.ApproverCode AT ROW 3.8 COL 29.29 COLON-ALIGNED
          LABEL "Approver"
          VIEW-AS FILL-IN 
          SIZE 5 BY 1
     fil_approval AT ROW 3.8 COL 37.86 COLON-ALIGNED NO-LABEL
     Voucher.SecondApprover AT ROW 4.8 COL 29.29 COLON-ALIGNED
          LABEL "2nd Appvr"
          VIEW-AS FILL-IN 
          SIZE 5.14 BY 1.05
     fil_approval-2 AT ROW 4.8 COL 37.86 COLON-ALIGNED NO-LABEL
     Voucher.InvoiceReference AT ROW 6.4 COL 8.72 COLON-ALIGNED
          LABEL "Invoice No"
          VIEW-AS FILL-IN 
          SIZE 25.72 BY 1
     Voucher.OurOrderNo AT ROW 7.4 COL 8.72 COLON-ALIGNED
          LABEL "Order No"
          VIEW-AS FILL-IN 
          SIZE 25.72 BY 1
     Voucher.DateDue AT ROW 8.4 COL 8.72 COLON-ALIGNED
          LABEL "Due Date"
          VIEW-AS FILL-IN 
          SIZE 11.57 BY 1
     cmb_PaymentStyle AT ROW 8.4 COL 37.86 COLON-ALIGNED
     Voucher.GoodsValue AT ROW 6.4 COL 68.29 COLON-ALIGNED
          LABEL "Value" FORMAT "-ZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 15.57 BY 1
          FONT 11
     Voucher.TaxValue AT ROW 7.4 COL 68.29 COLON-ALIGNED
          LABEL "GST" FORMAT "-ZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 15.57 BY 1
          FONT 11
     fil_total AT ROW 8.4 COL 68.29 COLON-ALIGNED
     Voucher.Description AT ROW 9.4 COL 8.72 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 75.43 BY 1
     fil_Entity AT ROW 10.8 COL 8.72 COLON-ALIGNED
     fil_Account AT ROW 11.6 COL 8.72 COLON-ALIGNED
     br-vchlne AT ROW 12.6 COL 1.57
     btn_add AT ROW 19.2 COL 1.57
     Btn_Add-Trn AT ROW 19.2 COL 32.43
     btn_ok AT ROW 19.2 COL 66.14
     btn_cancel AT ROW 19.2 COL 77
     RECT-1 AT ROW 1 COL 1
     RECT-2 AT ROW 3.6 COL 1.57
     "Invoice Details" VIEW-AS TEXT
          SIZE 14.86 BY .8 AT ROW 3.2 COL 2.14
          FONT 14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Voucher
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 22.05
         WIDTH              = 93.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/string.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
/* BROWSE-TAB br-vchlne fil_Account F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Voucher.ApproverCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR BUTTON btn_add IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Voucher.CreditorCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Voucher.Date IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Voucher.DateDue IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Voucher.EntityCode IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN Voucher.EntityType IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Account IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_approval IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_approval-2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_batch-text IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Creditor IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Entity IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_total IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       fil_total:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN Voucher.GoodsValue IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Voucher.InvoiceReference IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Voucher.OrderCode IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN Voucher.OurOrderNo IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Voucher.SecondApprover IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Voucher.TaxValue IN FRAME F-Main
   NO-DISPLAY NO-ENABLE EXP-LABEL EXP-FORMAT                            */
ASSIGN 
       Voucher.TaxValue:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN Voucher.VoucherSeq IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-vchlne
/* Query rebuild information for BROWSE br-vchlne
     _TblList          = "ttpl.VoucherLine OF ttpl.Voucher"
     _Options          = "NO-LOCK"
     _FldNameList[1]   > ttpl.VoucherLine.EntityType
"VoucherLine.EntityType" ? ? "character" ? ? 10 ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > ttpl.VoucherLine.EntityCode
"VoucherLine.EntityCode" ? ? "integer" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[3]   > ttpl.VoucherLine.AccountCode
"VoucherLine.AccountCode" ? ? "decimal" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[4]   > ttpl.VoucherLine.Description
"VoucherLine.Description" ? ? "character" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[5]   > ttpl.VoucherLine.Amount
"VoucherLine.Amount" ? ? "decimal" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br-vchlne */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON LEAVE OF FRAME F-Main
DO:
  last-widg = FOCUS.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON RETURN OF FRAME F-Main
ANYWHERE DO:
  DEF VAR curr-widg AS HANDLE NO-UNDO.
  curr-widg = IF VALID-HANDLE( FOCUS ) THEN FOCUS ELSE last-widg.

  IF curr-widg:TYPE = 'FILL-IN':U OR curr-widg:DYNAMIC THEN
  DO:
    APPLY 'TAB':U TO curr-widg.
    RETURN NO-APPLY.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.ApproverCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.ApproverCode V-table-Win
ON LEAVE OF Voucher.ApproverCode IN FRAME F-Main /* Approver */
DO:
  {inc/selcde/cdapp.i "fil_Approval"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-vchlne
&Scoped-define SELF-NAME br-vchlne
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON CTRL-DEL OF br-vchlne IN FRAME F-Main
OR 'SHIFT-DEL':U OF {&SELF-NAME} ANYWHERE DO:
  RUN delete-line.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON ENTRY OF br-vchlne IN FRAME F-Main
OR 'MOUSE-SELECT-CLICK':U OF {&SELF-NAME} DO:

  IF LOOKUP( LAST-EVENT:LABEL, 'ENTER,TAB,MOUSE-SELECT-UP,CHOOSE':U ) = 0 THEN RETURN.
  IF NOT AVAILABLE Voucher THEN RETURN.

  IF NOT ( NUM-RESULTS("br-vchlne":U) = ? OR
           NUM-RESULTS("br-vchlne":U) = 0 ) THEN
    IF br-vchlne:SELECT-FOCUSED-ROW() THEN.


  IF NUM-RESULTS("br-vchlne":U) = 0 OR NUM-RESULTS( "br-vchlne" ) = ? THEN DO:
    RUN add-new-line.
    RETURN NO-APPLY.
  END.
  ELSE IF NUM-RESULTS("br-vchlne":U) = 1 THEN DO WITH FRAME {&FRAME-NAME}:
    IF VoucherLine.Description:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = "" THEN
      VoucherLine.Description:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = Voucher.Description:SCREEN-VALUE.
    IF INTEGER( VoucherLine.Amount:SCREEN-VALUE IN BROWSE {&BROWSE-NAME}) = 0 THEN 
      RUN make-it-balance( VoucherLine.Amount:HANDLE IN BROWSE br-vchlne ).
  
    APPLY 'ENTRY':U TO VoucherLine.EntityType IN BROWSE br-vchlne.
    trn-modified = Yes.
    RETURN NO-APPLY.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON LEAVE OF br-vchlne IN FRAME F-Main
DO:
  RUN close-trn-query.
  RUN open-trn-query.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON ROW-DISPLAY OF br-vchlne IN FRAME F-Main
DO:
  RUN set-trn-color.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON ROW-ENTRY OF br-vchlne IN FRAME F-Main
DO:
  RUN check-last.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON ROW-LEAVE OF br-vchlne IN FRAME F-Main
OR 'CURSOR-DOWN':U OF {&SELF-NAME} 
OR 'CURSOR-UP':U   OF {&SELF-NAME}
OR 'ENTER':U       OF VoucherLine.Amount IN BROWSE {&SELF-NAME} ANYWHERE DO:

  IF selecting-code THEN RETURN.

  IF trn-modified THEN DO:
    RUN verify-line.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
  END.


  IF br-vchlne:NEW-ROW /* AND NOT new-result-entry */ THEN DO:
    br-vchlne:CREATE-RESULT-LIST-ENTRY() .
  END.   


  IF LAST-EVENT:FUNCTION = 'ROW-LEAVE':U AND
     LAST-EVENT:LABEL = 'ENTER':U THEN
  DO:
    IF mode <> "View" THEN DO:
      RUN check-line-total.
      IF RETURN-VALUE = "FAIL" THEN 
           APPLY 'CHOOSE':U TO btn_Add-Trn IN FRAME {&FRAME-NAME}.
      ELSE IF Mode = "Approve" OR mode = "Maintain" THEN
        APPLY 'ENTRY':U TO btn_OK IN FRAME {&FRAME-NAME}.
      ELSE IF Mode = "Register" THEN
        APPLY 'ENTRY':U TO btn_Add IN FRAME {&FRAME-NAME}.

      RETURN NO-APPLY. 
    END.
  END.

  IF br-vchlne:REFRESH() THEN .
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-vchlne V-table-Win
ON VALUE-CHANGED OF br-vchlne IN FRAME F-Main
DO:
  RUN update-trn-fields.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME VoucherLine.EntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.EntityType br-vchlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF VoucherLine.EntityType IN BROWSE br-vchlne /* T */
DO:

  RUN set-trn-color.
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.

  RUN verify-entity-type( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN DO:
    SELF:MODIFIED = No.
    RETURN NO-APPLY.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME VoucherLine.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.EntityCode br-vchlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF VoucherLine.EntityCode IN BROWSE br-vchlne /* Code */
DO:

  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
  
  RUN verify-entity-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN DO:
    SELF:MODIFIED = no.
    RETURN NO-APPLY.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME VoucherLine.AccountCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.AccountCode br-vchlne _BROWSE-COLUMN V-table-Win
ON GO OF VoucherLine.AccountCode IN BROWSE br-vchlne /* Account */
DO:
  RUN select-account-code( SELF ).
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.AccountCode br-vchlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF VoucherLine.AccountCode IN BROWSE br-vchlne /* Account */
DO:

  IF selecting-code THEN RETURN.
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
  
  RUN verify-account-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
  DO:
    SELF:MODIFIED = no.
    RETURN NO-APPLY.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME VoucherLine.Description
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.Description br-vchlne _BROWSE-COLUMN V-table-Win
ON F3 OF VoucherLine.Description IN BROWSE br-vchlne /* Description */
DO:
  RUN update-description.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.Description br-vchlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF VoucherLine.Description IN BROWSE br-vchlne /* Description */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME VoucherLine.Amount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.Amount br-vchlne _BROWSE-COLUMN V-table-Win
ON F3 OF VoucherLine.Amount IN BROWSE br-vchlne /* Amount */
DO:
  RUN make-it-balance( SELF ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL VoucherLine.Amount br-vchlne _BROWSE-COLUMN V-table-Win
ON LEAVE OF VoucherLine.Amount IN BROWSE br-vchlne /* Amount */
DO:
  IF SELF:MODIFIED THEN DO:
    trn-modified = Yes.
    RUN set-trn-color.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Register Another */
DO:
  IF AVAILABLE Voucher THEN DO:
    RUN verify-voucher.
    IF RETURN-VALUE = "FAIL" THEN RETURN.
    RUN dispatch( 'update-record':U ).
  END.
  
  RUN dispatch( 'add-record':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON RETURN OF btn_add IN FRAME F-Main /* Register Another */
DO:
  MESSAGE 'Press the space-bar to activate the button'
                 VIEW-AS ALERT-BOX INFORMATION TITLE "Useless Helpful Hint!".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Add-Trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Add-Trn V-table-Win
ON CHOOSE OF Btn_Add-Trn IN FRAME F-Main /* Add Tr */
DO:
  RUN add-new-line.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN cancel-changes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON CHOOSE OF btn_ok IN FRAME F-Main /* Done */
DO:
  RUN confirm-changes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ok V-table-Win
ON RETURN OF btn_ok IN FRAME F-Main /* Done */
DO:
  MESSAGE 'Press the space-bar to activate the button'
                 VIEW-AS ALERT-BOX INFORMATION TITLE "Useless Helpful Hint!".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PaymentStyle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON ENTER OF cmb_PaymentStyle IN FRAME F-Main /* Pay by */
DO:
  APPLY 'TAB':U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON RETURN OF cmb_PaymentStyle IN FRAME F-Main /* Pay by */
DO:
  APPLY 'TAB':U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON U1 OF cmb_PaymentStyle IN FRAME F-Main /* Pay by */
DO:
  {inc/selcmb/scpsty1.i "Voucher" "PaymentStyle"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PaymentStyle V-table-Win
ON U2 OF cmb_PaymentStyle IN FRAME F-Main /* Pay by */
DO:
  {inc/selcmb/scpsty2.i "Voucher" "PaymentStyle"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.CreditorCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.CreditorCode V-table-Win
ON ENTRY OF Voucher.CreditorCode IN FRAME F-Main /* Creditor */
DO:
  SELF:AUTO-ZAP = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.CreditorCode V-table-Win
ON LEAVE OF Voucher.CreditorCode IN FRAME F-Main /* Creditor */
DO:
  IF Voucher.CreditorCode <> INPUT Voucher.CreditorCode THEN
    RUN set-creditor-defaults.

  {inc/selcde/cdcrd.i "fil_Creditor"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.Date
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.Date V-table-Win
ON LEAVE OF Voucher.Date IN FRAME F-Main /* Invc. Date */
DO:
  RUN voucher-date-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.Description
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.Description V-table-Win
ON LEAVE OF Voucher.Description IN FRAME F-Main /* Description */
OR 'ENTER' OF ttpl.Voucher.DESCRIPTION DO:
DO WITH FRAME {&FRAME-NAME}:
DEF VAR lines-changed AS LOGICAL INITIAL No NO-UNDO.
  IF Voucher.Description:MODIFIED  THEN DO TRANSACTION:
    FOR EACH VoucherLine OF Voucher /*WHERE VoucherLine.Description = Voucher.Description*/ EXCLUSIVE-LOCK:
      VoucherLine.Description = Voucher.Description:SCREEN-VALUE .
      lines-changed = Yes.
    END.
  END.

  /*IF lines-changed AND br-vchlne:REFRESH() THEN .*/
  IF lines-changed THEN br-vchlne:REFRESH().
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.EntityCode V-table-Win
ON LEAVE OF Voucher.EntityCode IN FRAME F-Main /* Code */
DO:
  RUN order-number-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_approval
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_approval V-table-Win
ON U1 OF fil_approval IN FRAME F-Main
DO:
  {inc/selfil/sfapp1.i "Voucher" "ApproverCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_approval V-table-Win
ON U2 OF fil_approval IN FRAME F-Main
DO:
  {inc/selfil/sfapp2.i "Voucher" "ApproverCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_approval V-table-Win
ON U3 OF fil_approval IN FRAME F-Main
DO:
  {inc/selfil/sfapp3.i "Voucher" "ApproverCode"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_approval-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_approval-2 V-table-Win
ON U1 OF fil_approval-2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp1.i "Voucher" "SecondApprover"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_approval-2 V-table-Win
ON U2 OF fil_approval-2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp2.i "Voucher" "SecondApprover"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_approval-2 V-table-Win
ON U3 OF fil_approval-2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp3.i "Voucher" "SecondApprover"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_batch-no
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_batch-no V-table-Win
ON LEAVE OF fil_batch-no IN FRAME F-Main /* Batch */
DO:

/* The following code modified from selcde/cdmst.i because we want
   more TAB control, and we want to re-enable the fields and set a
   whole lot of things in motion when the user selects a valid batch */

IF {&SELF-NAME}:MODIFIED THEN DO WITH FRAME {&FRAME-NAME}:

DEF VAR old-val  AS INT NO-UNDO.
DEF VAR cur-val  AS INT NO-UNDO.
DEF VAR old-id   AS ROWID NO-UNDO.

DEF BUFFER tmp_NewBatch FOR NewBatch.

old-val  = INT( ENTRY( 2, {&SELF-NAME}:PRIVATE-DATA ) ).
cur-val  = INT( {&SELF-NAME}:SCREEN-VALUE ).
old-id   = TO-ROWID( ENTRY( 1, {&SELF-NAME}:PRIVATE-DATA ) ).

IF old-val <> cur-val THEN DO WITH FRAME {&FRAME-NAME}:

  FIND FIRST tmp_NewBatch WHERE tmp_NewBatch.BatchCode = cur-val NO-LOCK NO-ERROR.

  IF NOT AVAILABLE tmp_NewBatch THEN DO:
    
    MESSAGE "There is no NewBatch available with code " {&SELF-NAME}:SCREEN-VALUE
      VIEW-AS ALERT-BOX ERROR.
      
    FIND FIRST tmp_NewBatch WHERE ROWID( tmp_NewBatch ) = old-id NO-LOCK NO-ERROR.
    
    IF AVAILABLE tmp_NewBatch THEN
      DISPLAY tmp_NewBatch.BatchCode @ {&SELF-NAME}.
    ELSE
      DISPLAY {&SELF-NAME}.    
          
  END.
  ELSE DO:

    DEF VAR spd AS CHAR NO-UNDO.
    spd = {&SELF-NAME}:PRIVATE-DATA.    
    ENTRY( 1, spd ) = STRING( ROWID( tmp_NewBatch ) ).
    ENTRY( 2, spd ) = {&SELF-NAME}:SCREEN-VALUE.
    {&SELF-NAME}:PRIVATE-DATA = spd.
    
    fil_batch-text:PRIVATE-DATA    = STRING( ROWID( tmp_NewBatch ) ).
    fil_batch-text:SCREEN-VALUE    = tmp_Newbatch.Description.

    /* Tab past the select button for valid codes if tab was pressed*/
    IF LOOKUP( LAST-EVENT:LABEL, 'TAB,ENTER':U ) <> 0 THEN DO:
      RUN set-batch-verified.
      RETURN NO-APPLY.
    END.
  END.
END. /* IF old-val <> new-val */
END. /* Of check if modified */

/* just to make sure we don't archive the cdnbc.i that normally goes with selnbc */
&IF DEFINED(NOBODY-IN-THEIR-RIGHT-MIND-WOULD-DEFINE-THIS) &THEN
MESSAGE "Oh shit!".
{inc/selcde/cdnbc.i "fil_batch-no" "fil_batch-text"}
&ENDIF

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_batch-text
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_batch-text V-table-Win
ON U1 OF fil_batch-text IN FRAME F-Main
DO:
  {inc/selfil/sfnbc1.i "?" "fil_batch-no"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_batch-text V-table-Win
ON U2 OF fil_batch-text IN FRAME F-Main
DO:
  {inc/selfil/sfnbc2.i "?" "fil_batch-no"}
  RUN set-batch-verified.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Creditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U1 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd1.i "Voucher" "CreditorCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U2 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd2.i "Voucher" "CreditorCode"}
  ASSIGN Voucher.Description :SCREEN-VALUE = {&SELF-NAME}:SCREEN-VALUE.
  RUN set-creditor-defaults.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Creditor V-table-Win
ON U3 OF fil_Creditor IN FRAME F-Main
DO:
  {inc/selfil/sfcrd3.i "Voucher" "CreditorCode"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_total
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_total V-table-Win
ON LEAVE OF fil_total IN FRAME F-Main /* Total */
DO:
  
  IF gst-applies AND SELF:MODIFIED THEN
  DO WITH FRAME {&FRAME-NAME}:
    
    Voucher.GoodsValue:SCREEN-VALUE = STRING(
      INPUT {&SELF-NAME} / ( 1 + ( Office.GST / 100 ) ) ).
    
    Voucher.TaxValue:SCREEN-VALUE = STRING( 
      INPUT {&SELF-NAME} - INPUT Voucher.GoodsValue ).

    RUN clear-modifieds.
        
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.GoodsValue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.GoodsValue V-table-Win
ON LEAVE OF Voucher.GoodsValue IN FRAME F-Main /* Value */
DO:
  
  IF gst-applies AND SELF:MODIFIED THEN
  DO WITH FRAME {&FRAME-NAME}:
      
    Voucher.TaxValue:SCREEN-VALUE = STRING(
      INPUT {&SELF-NAME} * ( Office.GST / 100 ) ).
    fil_Total = INPUT {&SELF-NAME} + INPUT Voucher.TaxValue.
    DISPLAY fil_Total WITH FRAME {&FRAME-NAME}.
      
    RUN clear-modifieds.
      
  END.
      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.OrderCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.OrderCode V-table-Win
ON LEAVE OF Voucher.OrderCode IN FRAME F-Main /* OrderCode */
DO:
  RUN order-number-changed.
  IF RETURN-VALUE = "NO-APPLY" THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.SecondApprover
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.SecondApprover V-table-Win
ON LEAVE OF Voucher.SecondApprover IN FRAME F-Main /* 2nd Appvr */
DO:
  {inc/selcde/cdapp.i "fil_Approval-2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.TaxValue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.TaxValue V-table-Win
ON LEAVE OF Voucher.TaxValue IN FRAME F-Main /* GST */
DO:
  
  IF gst-applies AND SELF:MODIFIED THEN
  DO WITH FRAME {&FRAME-NAME}:
      
    fil_Total = INPUT {&SELF-NAME} + INPUT Voucher.GoodsValue.
    DISPLAY fil_Total WITH FRAME {&FRAME-NAME}.
    
    RUN clear-modifieds.
      
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Voucher.VoucherSeq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Voucher.VoucherSeq V-table-Win
ON LEAVE OF Voucher.VoucherSeq IN FRAME F-Main /* Voucher No */
DO:
DO WITH FRAME {&FRAME-NAME}:
DEF VAR curr-vchr AS INT NO-UNDO.

  IF INPUT Voucher.VoucherSeq = Voucher.VoucherSeq THEN RETURN.

  IF AVAILABLE(Voucher) THEN curr-vchr = Voucher.VoucherSeq.
  FIND Voucher WHERE Voucher.VoucherSeq = INPUT Voucher.VoucherSeq NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Voucher) THEN DO:
    MESSAGE "Voucher" INPUT VoucherSeq "not on file!" VIEW-AS ALERT-BOX ERROR TITLE "No Such Voucher".
    FIND Voucher WHERE Voucher.VoucherSeq = curr-vchr NO-LOCK NO-ERROR.
    IF AVAILABLE(Voucher) THEN DISPLAY Voucher.VoucherSeq.
    RETURN NO-APPLY.
  END.

  RUN save-lines.

  IF LAST-EVENT:WIDGET-ENTER = btn_Cancel:HANDLE
     OR LAST-EVENT:WIDGET-ENTER = btn_Add:HANDLE
     OR LAST-EVENT:WIDGET-ENTER = btn_OK:HANDLE THEN
    /* They pressed a button - we'll just run with that one */
    RETURN.

  IF mode = "View" THEN
    APPLY 'ENTRY':U TO btn_Cancel .
  ELSE IF verified-batch THEN
    APPLY 'ENTRY':U TO br-vchlne.
  ELSE
    APPLY 'ENTRY':U TO fil_batch-no.

  RUN dispatch( 'display-fields':U ).
  IF mode <> "View" THEN RUN dispatch( 'enable-fields':U ).

  RETURN NO-APPLY.

END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-new-line V-table-Win 
PROCEDURE add-new-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR current-row-number AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  IF NUM-RESULTS("br-vchlne":U) = ? OR
       NUM-RESULTS("br-vchlne":U) = 0 THEN
    DO:
      RUN create-line.
      RUN open-trn-query.
      RUN check-last.
    END.
    ELSE DO:
      IF NUM-RESULTS( "br-vchlne" ) > 0
          AND CURRENT-RESULT-ROW( "br-vchlne" ) < NUM-RESULTS( "br-vchlne":U ) THEN DO:
          IF br-vchlne:SET-REPOSITIONED-ROW( MINIMUM(6,NUM-RESULTS( "br-vchlne":U )),"CONDITIONAL":U ) THEN .
          REPOSITION br-vchlne TO ROW NUM-RESULTS( "br-vchlne":U ).
          IF br-vchlne:REFRESH() THEN .
      END.

      br-vchlne:INSERT-ROW("AFTER") IN FRAME f-main.
      RUN check-last.
      /*br-vchlne:CREATE-RESULT-LIST-ENTRY(). */

   END.

    APPLY 'ENTRY':U TO VoucherLine.EntityType IN BROWSE br-vchlne.
    
    /* Set the default screen-values */
    RUN set-trn-defaults.
    RUN make-it-balance( VoucherLine.Amount:HANDLE IN BROWSE br-vchlne ).

    APPLY 'ENTRY':U TO VoucherLine.EntityType IN BROWSE br-vchlne.
    trn-modified = Yes.
  
END.    /* do with frame */
 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Voucher"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Voucher"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE approve-voucher V-table-Win 
PROCEDURE approve-voucher :
/*------------------------------------------------------------------------------
  Purpose:     Approve this Voucher
------------------------------------------------------------------------------*/
DEF VAR i        AS INT  NO-UNDO.
DEF VAR vch-list AS CHAR NO-UNDO.
DEF VAR mth-code LIKE Month.MonthCode NO-UNDO INITIAL ?.
DEF VAR trn-date AS DATE NO-UNDO.
DEF VAR residual-tax AS DEC NO-UNDO.
DEF VAR creditor-ledger AS INT NO-UNDO.

        
DO TRANSACTION ON ERROR UNDO, RETURN "FAIL":

   /* Find the batch in which to approve the Vouchers */
  FIND NewBatch WHERE NewBatch.BatchCode = INPUT FRAME {&FRAME-NAME} fil_batch-no NO-LOCK.

  trn-date = get-trn-date( OUTPUT mth-code ).
    
  /* Initialise temp-table */
  FOR EACH company-gst EXCLUSIVE-LOCK:
        DELETE company-gst.
  END.

  IF multi-ledger-creditors THEN DO:
    creditor-ledger = validate-creditor-ledger().
    IF creditor-ledger = ? THEN UNDO, RETURN "FAIL".
  END.


  
  CREATE NewDocument.
  ASSIGN NewDocument.BatchCode    = NewBatch.BatchCode
         NewDocument.DocumentType = "VCHR"
         NewDocument.Description  = Voucher.Description
         NewDocument.Reference    = "VCHR" + STRING( Voucher.VoucherSeq ).

  /* Create the account debit transactions */
  FOR EACH VoucherLine OF Voucher NO-LOCK:
    CREATE NewAcctTrans.
    ASSIGN NewAcctTrans.BatchCode       = NewBatch.BatchCode
           NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
           NewAcctTrans.EntityType      = VoucherLine.EntityType
           NewAcctTrans.EntityCode      = VoucherLine.EntityCode
           NewAcctTrans.AccountCode     = VoucherLine.AccountCode
           NewAcctTrans.Amount          = VoucherLine.Amount
           NewAcctTrans.Date            = trn-date
           NewAcctTrans.MonthCode       = mth-code
           NewAcctTrans.Description     = VoucherLine.Description
           NewAcctTrans.Reference       = NewDocument.Reference.
  
    /* Store Companies proportional GST */
    IF GST-Multi-Company = YES AND 
        gst-applies AND
        Voucher.TaxValue <> 0  AND
        VoucherLine.EntityType <> 'T'  AND
        VoucherLine.EntityType <> 'C' 
    THEN RUN update-gst-table.


  END. /* Each VoucherLine */

  /* Create the GST debit transaction */ 
  FIND Creditor OF Voucher NO-LOCK.
      
  residual-tax = Voucher.TaxValue .

  IF gst-applies AND residual-tax <> 0 THEN DO:
     IF GST-multi-company = YES  THEN DO:
           FOR EACH company-gst:
               CREATE NewAcctTrans.
               ASSIGN
                  NewAcctTrans.BatchCode       = NewBatch.BatchCode
                  NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
                  NewAcctTrans.EntityType      = "L"
                  NewAcctTrans.EntityCode      = company-gst.company_id
                  NewAcctTrans.AccountCode     = gst-in
                  NewAcctTrans.Amount          = company-gst.Tax_Amount 
                  NewAcctTrans.Date            = trn-date
                  NewAcctTrans.MonthCode       = mth-code
                  NewAcctTrans.Description     = 'GST, ' + NewDocument.Description
                  NewAcctTrans.Reference       = NewDocument.Reference
                  /*document-total = document-total + NewAcctTrans.Amount.*/
                  residual-tax = residual-tax  - company-gst.Tax_Amount .
          END.   /* Each company-gst */
    END.  /* GST-multi-company */
      
     /* Remainder of tax (sometimes because of rounding) */
    CREATE NewAcctTrans.
    ASSIGN NewAcctTrans.BatchCode       = NewBatch.BatchCode
           NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
           NewAcctTrans.EntityType      = "L"
           NewAcctTrans.EntityCode      = gst-in-ecode
           NewAcctTrans.AccountCode     = gst-in
           NewAcctTrans.Amount          = residual-tax  /* Voucher.TaxValue*/
           NewAcctTrans.Date            = trn-date
           NewAcctTrans.MonthCode       = mth-code
           NewAcctTrans.Description     = "GST, " + NewDocument.Description
           NewAcctTrans.Reference       = NewDocument.Reference.
  END. /* If GST applies */

  IF multi-ledger-creditors THEN DO:
    NewAcctTrans.EntityCode     = creditor-ledger.
  END.

  /* Credit the creditor */
  CREATE NewAcctTrans.
  ASSIGN NewAcctTrans.BatchCode       = NewBatch.BatchCode
         NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
         NewAcctTrans.EntityType      = "C"
         NewAcctTrans.EntityCode      = Voucher.CreditorCode
         NewAcctTrans.AccountCode     = sundry-creditors
         NewAcctTrans.Amount          = ( Voucher.GoodsValue + Voucher.TaxValue ) * -1
         NewAcctTrans.Date            = trn-date
         NewAcctTrans.MonthCode       = mth-code
         NewAcctTrans.Description     = NewDocument.Description
         NewAcctTrans.Reference       = NewDocument.Reference.

  IF multi-ledger-creditors THEN DO:
    NewAcctTrans.AccountCode     = creditor-ledger.
  END.

  FIND CURRENT Voucher EXCLUSIVE-LOCK.
  ASSIGN Voucher.VoucherStatus = "A"
         Voucher.BatchCode     = NewDocument.BatchCode
         Voucher.DocumentCode  = NewDocument.DocumentCode.
END.

MESSAGE "Voucher Approved" VIEW-AS ALERT-BOX INFORMATION.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-approver V-table-Win 
PROCEDURE assign-approver :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER et AS CHAR NO-UNDO.
DEF INPUT PARAMETER ec AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  IF NOT AVAILABLE(VoucherLine) THEN RETURN.
  IF Mode = "View" THEN RETURN.

DEF BUFFER AltVL FOR VoucherLine.
  FIND FIRST AltVL OF Voucher NO-LOCK.
  IF ROWID(AltVL) <> ROWID(VoucherLine) THEN RETURN.

  CASE et:
    WHEN "J" THEN DO:
      FIND Project WHERE Project.ProjectCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Project) THEN DO TRANSACTION:
        Voucher.ApproverCode:SCREEN-VALUE = (IF Project.SecondApprover <> "" THEN Project.SecondApprover ELSE Project.FirstApprover).
        Voucher.ApproverCode:MODIFIED = Yes.
        APPLY "LEAVE":U TO Voucher.ApproverCode .
      END.
    END.
    WHEN "P" THEN DO:
      FIND Property WHERE Property.PropertyCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Property) THEN DO:
        FIND Approver WHERE Approver.PersonCode = Property.Manager NO-LOCK NO-ERROR.
        IF AVAILABLE(Approver) THEN DO:
          Voucher.ApproverCode:SCREEN-VALUE = Approver.ApproverCode.
          Voucher.ApproverCode:MODIFIED = Yes.
          APPLY "LEAVE":U TO Voucher.ApproverCode .
        END.
      END.
    END.
  END CASE.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-line V-table-Win 
PROCEDURE assign-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE VoucherLine THEN RETURN.
  FIND CURRENT VoucherLine EXCLUSIVE-LOCK.
  
  ASSIGN BROWSE br-vchlne
    VoucherLine.EntityType
    VoucherLine.EntityCode
    VoucherLine.AccountCode
    VoucherLine.Amount
    VoucherLine.Description.

  FIND CURRENT VoucherLine NO-LOCK.

  RUN display-line.
  trn-modified = No.
  my-new-line = No.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'hide, CONTAINER-SOURCE':U ).
  IF mode = "Register" THEN DO:
    RUN delete-voucher.
    RUN notify( 'open-query,record-source':U ).
  END.
  ELSE DO:
    RUN check-modified( "CLEAR" ).
    RUN cancel-lines.
  END.

  IF mode = "Approve" AND verified-batch THEN
    RUN notify( 'open-query,record-source':U ).

  RUN print-vouchers.
  RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-lines V-table-Win 
PROCEDURE cancel-lines :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Voucher THEN RETURN.

  FOR EACH VoucherLine OF Voucher: DELETE VoucherLine. END.
  
  FOR EACH CurrentLine:
    CREATE VoucherLine.
    ASSIGN
      VoucherLine.EntityType   = CurrentLine.EntityType
      VoucherLine.EntityCode   = CurrentLine.EntityCode
      VoucherLine.AccountCode  = CurrentLine.AccountCode
      VoucherLine.Description  = CurrentLine.Description
      VoucherLine.Amount       = CurrentLine.Amount.
      VoucherLine.VoucherSeq   = CurrentLine.VoucherSeq.
      
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-last V-table-Win 
PROCEDURE check-last :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  last-line = CURRENT-RESULT-ROW( "br-vchlne" ) = NUM-RESULTS( "br-vchlne" ) OR
    br-vchlne:NEW-ROW  IN FRAME {&FRAME-NAME} .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-line V-table-Win 
PROCEDURE check-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-entity-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( VoucherLine.EntityType:HANDLE IN BROWSE br-vchlne ).

  RUN verify-entity-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( VoucherLine.EntityCode:HANDLE IN BROWSE br-vchlne ).

  RUN verify-account-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( VoucherLine.AccountCode:HANDLE ).

  RUN verify-one-client.
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( VoucherLine.EntityCode:HANDLE IN BROWSE br-vchlne ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-line-total V-table-Win 
PROCEDURE check-line-total :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER VL FOR VoucherLine.

DEF VAR vl-total AS DECIMAL NO-UNDO INITIAL 0.
DEF VAR vl-rowid AS ROWID NO-UNDO.

  IF NOT AVAILABLE(Voucher) THEN RETURN "".

  IF AVAILABLE(VoucherLine) THEN vl-rowid = ROWID(VoucherLine) .

  vl-total = DECIMAL( VoucherLine.Amount:SCREEN-VALUE IN BROWSE br-vchlne ) .
  FOR EACH VL NO-LOCK OF Voucher WHERE ROWID(VL) <> vl-rowid:
    vl-total = vl-total + VL.Amount .
  END.

  IF vl-total = INPUT FRAME {&FRAME-NAME} Voucher.GoodsValue THEN
    RETURN "PASS".
  ELSE
    RETURN "FAIL".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-modifieds V-table-Win 
PROCEDURE clear-modifieds :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:
    Voucher.GoodsValue:MODIFIED = No.
    Voucher.TaxValue:MODIFIED = No.
    fil_Total:MODIFIED = No.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-trn-query V-table-Win 
PROCEDURE close-trn-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY br-vchlne.
  RUN update-trn-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN verify-voucher.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  RUN dispatch( 'update-record':U ).

  IF mode = "Approve" THEN DO:
    RUN approve-voucher.
    RUN dispatch( 'enable-fields':U ).
    APPLY 'ENTRY':U TO Voucher.VoucherSeq IN FRAME {&FRAME-NAME}.
  END.
  ELSE DO:
    RUN check-modified( 'clear':U ).
    RUN notify( 'open-query, record-source':U ).

    RUN check-modified( 'clear':U ).
    RUN print-vouchers.

    /* wants to re-save record for some reason */
    RUN check-modified( 'clear':U ).

    RUN dispatch( 'exit':U ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-coa-list V-table-Win 
PROCEDURE create-coa-list :
/*------------------------------------------------------------------------------
  Purpose:     Create a dynmic selection list for chart of accounts
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  DEF VAR item AS CHAR NO-UNDO.

  CREATE SELECTION-LIST coa-list
  ASSIGN
    FRAME        = FRAME {&FRAME-NAME}:HANDLE
    BGCOLOR      = 0
    FGCOLOR      = 15
    X            = 1
    Y            = 1
    WIDTH-P      = 1
    HEIGHT-P     = 1
    DELIMITER    = '|'
    FONT         = 15
    HIDDEN       = Yes
    SCROLLBAR-VERTICAL = Yes
    SCROLLBAR-VERTICAL = No
          
  TRIGGERS:
    ON 'RETURN':U, 'DEFAULT-ACTION':U PERSISTENT RUN update-account-code IN THIS-PROCEDURE.
    ON 'LEAVE':U PERSISTENT RUN hide-coa-list IN THIS-PROCEDURE.
  END TRIGGERS.

  coa-list:LIST-ITEMS = "".
  
  FOR EACH ChartOfAccount NO-LOCK:
    item = STRING( ChartOfAccount.AccountCode, "9999.99" ) + '  ' + ChartOfAccount.Name.
    IF coa-list:ADD-LAST(item) THEN.
  END.

  coa-list:WIDTH-CHARS  = 40.
  coa-list:HEIGHT-CHARS = 6.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-line V-table-Win 
PROCEDURE create-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CREATE VoucherLine.
  ASSIGN 
    VoucherLine.VoucherSeq  = Voucher.VoucherSeq
    VoucherLine.EntityType  = (IF Voucher.EntityType <> "" THEN Voucher.EntityType ELSE "P")
    VoucherLine.EntityCode  = (IF Voucher.EntityCode > 0 THEN Voucher.EntityCode ELSE 0)
    VoucherLine.AccountCode = (IF Voucher.AccountCode > 0 THEN Voucher.AccountCode ELSE 0).

  my-new-line = Yes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-line V-table-Win 
PROCEDURE delete-line :
/*------------------------------------------------------------------------------
  Purpose:     Delete the transaction line with focus
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR current-row-number AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  IF NUM-RESULTS( "br-vchlne" ) = 0 OR NUM-RESULTS( "br-vchlne" ) = ? THEN RETURN.
  IF br-vchlne:NEW-ROW THEN DO:
    IF br-vchlne:DELETE-CURRENT-ROW() THEN.
  END.
  ELSE DO:
    IF br-vchlne:SELECT-FOCUSED-ROW() THEN DO:
      IF br-vchlne:FETCH-SELECTED-ROW(1) THEN.
      IF AVAILABLE VoucherLine THEN DO:
        GET CURRENT br-vchlne EXCLUSIVE-LOCK.
        DELETE VoucherLine.
        IF br-vchlne:DELETE-CURRENT-ROW() THEN.
      END.
    END.
  END.

/*
  IF NUM-RESULTS( "br-vchlne" ) = ? OR 
     NUM-RESULTS( "br-vchlne" ) = 0 THEN DO:
    RUN close-trn-query.
    RETURN.
  END.   
*/

  IF br-vchlne:FETCH-SELECTED-ROW(1) THEN .
  RUN display-line.
  
  RUN check-last.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-voucher V-table-Win 
PROCEDURE delete-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND CURRENT Voucher EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE Voucher THEN
  DO:
    FOR EACH VoucherLine OF Voucher: DELETE VoucherLine. END.
    DELETE Voucher.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-line V-table-Win 
PROCEDURE display-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE VoucherLine THEN RETURN.

  DISPLAY   VoucherLine.EntityType
            VoucherLine.EntityCode
            VoucherLine.AccountCode
            VoucherLine.Amount
            VoucherLine.Description
            WITH BROWSE br-vchlne.
  
  RUN set-trn-color.
  RUN update-trn-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-default-trn V-table-Win 
PROCEDURE get-default-trn :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FIND FIRST DefTrans NO-ERROR.
  IF NOT AVAILABLE DefTrans THEN CREATE DefTrans.

  GET PREV br-vchlne.
  IF AVAILABLE VoucherLine THEN DO:
    ASSIGN
      DefTrans.EntityType  = VoucherLine.EntityType
      DefTrans.EntityCode  = VoucherLine.EntityCode
      DefTrans.AccountCode = VoucherLine.AccountCode
      DefTrans.Amount      = 0.00
      DefTrans.Description = VoucherLine.Description.
  END.
  ELSE DO WITH FRAME {&FRAME-NAME}:
    
    DEF BUFFER COA FOR ChartOfAccount.
    FIND FIRST COA WHERE
      COA.AccountCode = Voucher.AccountCode NO-LOCK NO-ERROR.
      
    ASSIGN
      DefTrans.EntityType  = Voucher.EntityType
      DefTrans.EntityCode  = Voucher.EntityCode
      DefTrans.AccountCode = Voucher.AccountCode
      DefTrans.Amount      = INPUT Voucher.GoodsValue
      DefTrans.Description = INPUT Voucher.Description.

    GET FIRST br-vchlne.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hide-coa-list V-table-Win 
PROCEDURE hide-coa-list :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  coa-list:VISIBLE   = No.
  coa-list:SENSITIVE = No.
  APPLY 'ENTRY':U TO VoucherLine.Description IN BROWSE br-vchlne.  
  selecting-code = No.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  last-creditor = Voucher.CreditorCode.

DEF BUFFER FirstLine FOR VoucherLine.
  FIND FIRST FirstLine NO-LOCK OF Voucher NO-ERROR.
  IF AVAILABLE(FirstLine) THEN ASSIGN
        Voucher.EntityType  = FirstLine.EntityType
        Voucher.EntityCode  = FirstLine.EntityCode
        Voucher.AccountCode = FirstLine.AccountCode.

  ASSIGN Voucher.TaxValue .

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Voucher) THEN RETURN.
  IF gst-applies THEN DO WITH FRAME {&FRAME-NAME}:
    fil_Total = Voucher.GoodsValue + Voucher.TaxValue.
    DISPLAY  Voucher.TaxValue  fil_Total .
  END.
  
  RUN open-trn-query.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  IF mode = "Register" THEN DO:
    have-records = Yes.
    ASSIGN last-creditor = INT( find-parent-key( "CreditorCode":U ) ) NO-ERROR.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE DO:
    RUN dispatch( 'enable-fields':U ).
    HIDE btn_Add .
  END.

  IF mode = "Approve" THEN DO:
    btn_OK:LABEL = "&Approve".
    btn_OK:TOOLTIP = "Approve this voucher, create the transactions and go to the next one".
    btn_cancel:LABEL = "&Close".
    btn_cancel:TOOLTIP = "Close the window - this voucher will not be approved".
    VIEW fil_batch-no fil_batch-text.
    DISABLE Voucher.EntityType Voucher.EntityCode Voucher.OrderCode Voucher.CreditorCode .
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_Creditor:HANDLE ), "HIDDEN = Yes" ).
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_batch-text:HANDLE ), "HIDDEN = No" ).
    ENABLE Voucher.VoucherSeq.
  END.
  ELSE IF mode = "View" THEN DO:
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, "ALL", "HIDDEN = Yes" ).
    HIDE btn_OK btn_Add .
    btn_cancel:LABEL = "&Close".
    btn_cancel:TOOLTIP = "Close the window".
    VoucherLine.EntityType:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
    VoucherLine.EntityCode:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
    VoucherLine.AccountCode:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
    VoucherLine.Description:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
    VoucherLine.Amount:READ-ONLY IN BROWSE {&BROWSE-NAME} = Yes.
    ENABLE Voucher.VoucherSeq.
  END.
  ELSE IF mode = "Register" THEN DO:
    btn_OK:LABEL = "&Done".
    btn_OK:TOOLTIP = "Finalise the registration of this voucher, print all registered vouchers and close the window.".
    VIEW btn_Add .
    ENABLE btn_Add .
    DISABLE Voucher.VoucherSeq .
  END.
  ELSE IF mode = "Maintain" THEN DO:
    btn_OK:LABEL = "&OK".
    btn_OK:TOOLTIP = "Apply changes to this voucher, reprint it and close the window.".
    DISABLE Voucher.VoucherSeq .
  END.

  IF mode <> "Approve" THEN DO:
    HIDE fil_batch-no fil_batch-text.
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_batch-text:HANDLE ), "HIDDEN = Yes" ).
  END.

  IF gst-applies THEN
    VIEW Voucher.TaxValue fil_Total.
  ELSE
    HIDE Voucher.TaxValue fil_Total.

  btn_Add-Trn:HIDDEN IN FRAME {&FRAME-NAME} = Yes.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN save-lines.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-apply-entry V-table-Win 
PROCEDURE local-apply-entry :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'apply-entry':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  IF mode = "Approve" AND NOT verified-batch THEN
    APPLY 'ENTRY':U TO fil_Batch-no IN FRAME {&FRAME-NAME} .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-update-record V-table-Win 
PROCEDURE local-update-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'update-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  IF LOOKUP( STRING( Voucher.VoucherSeq, "9999999" ), voucher-list ) = 0 THEN
  DO:
    voucher-list = voucher-list + IF voucher-list = "" THEN "" ELSE ",".
    voucher-list = voucher-list + STRING( Voucher.VoucherSeq, "9999999" ) .
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE make-it-balance V-table-Win 
PROCEDURE make-it-balance :
/*------------------------------------------------------------------------------
  Purpose:     Make the given transaction balance the voucher.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER amt-field AS HANDLE NO-UNDO.
  DEF BUFFER OtherTrans FOR VoucherLine.
  
  DEF VAR sub-total  AS DEC   NO-UNDO.
  DEF VAR this-rowid AS ROWID NO-UNDO.
  
  IF br-vchlne:NEW-ROW IN FRAME {&FRAME-NAME} THEN
  DO:
    FOR EACH OtherTrans OF Voucher:
      sub-total = sub-total + OtherTrans.Amount.
    END.
  END.
  ELSE
  DO:
    IF br-vchlne:SELECT-FOCUSED-ROW() THEN.
    this-rowid = ROWID( VoucherLine ).
    FOR EACH OtherTrans OF Voucher WHERE ROWID( OtherTrans ) <> this-rowid:
      sub-total = sub-total + OtherTrans.Amount.
    END.
  END.
  
  amt-field:SCREEN-VALUE = STRING( INPUT FRAME {&FRAME-NAME} Voucher.GoodsValue - sub-total ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-trn-query V-table-Win 
PROCEDURE open-trn-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE Voucher THEN RETURN.
  OPEN QUERY br-vchlne FOR EACH VoucherLine OF Voucher.
  RUN display-line.
  my-new-line = No.
  new-result-entry = Yes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE order-number-changed V-table-Win 
PROCEDURE order-number-changed :
/*------------------------------------------------------------------------------
  Purpose:     Update the coding details for the given project/order number
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  
  DEF VAR entity-type AS CHAR NO-UNDO.
  DEF VAR entity-code LIKE Project.ProjectCode NO-UNDO.
  DEF VAR order-code   LIKE Order.OrderCode NO-UNDO.
  
  entity-type = INPUT Voucher.EntityType.
  entity-code = INPUT Voucher.EntityCode.
  order-code  = INPUT Voucher.OrderCode.  

  IF last-entity-type = entity-type AND last-entity-code = entity-code AND last-order-code = order-code THEN RETURN.
  last-entity-type = entity-type.
  last-entity-code = entity-code.
  last-order-code   = order-code.
  RUN assign-approver( entity-type, entity-code ).
  
  /* Try and find the order record */
  RUN update-from-order( entity-type, entity-code, order-code ).
  IF RETURN-VALUE = "NO-APPLY" THEN APPLY 'ENTRY':U TO Voucher.DATE .
  
  RETURN RETURN-VALUE.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DO TRANSACTION:
    CREATE Voucher.
    ASSIGN  Voucher.VoucherStatus = "U".
    ASSIGN Voucher.CreditorCode:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(last-creditor) NO-ERROR.
    RUN set-creditor-defaults.
    ASSIGN Voucher.CreditorCode = last-creditor NO-ERROR.
    DISPLAY Voucher.CreditorCode WITH FRAME {&FRAME-NAME} .
    APPLY 'U1':U TO cmb_PaymentStyle .

  END.
  
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .
  ASSIGN Voucher.Description:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
    fil_Creditor:SCREEN-VALUE.  

  RUN dispatch( 'enable-fields':U ).
  
  CURRENT-WINDOW:TITLE = "Registering voucher " + STRING( Voucher.VoucherSeq, "999999").
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-enable-fields V-table-Win 
PROCEDURE override-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF voucher-orders = "Project" THEN DO:
    Voucher.EntityType:LABEL = "Project".
    HIDE Voucher.OrderCode .
  END.

  IF mode = "Approve" AND NOT verified-batch THEN DO:
    DISABLE btn_OK br-vchlne fil_batch-no Voucher.GoodsValue Voucher.TaxValue fil_Total .
    RUN dispatch ( 'disable-fields':U ).
    ENABLE fil_Batch-no .
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_batch-text:HANDLE ), "SENSITIVE = Yes" ).
    RETURN.
  END.

  /* standard toolkit method */
  RUN pre-enable-fields IN THIS-PROCEDURE NO-ERROR.

  /* Dispatch standard ADM method.                             */
  RUN adm-enable-fields IN THIS-PROCEDURE NO-ERROR .

  IF use-payment-styles = Yes THEN
    VIEW cmb_PaymentStyle.
  ELSE
    HIDE cmb_PaymentStyle.


  /* Code placed here will execute AFTER standard behavior.    */
  IF NOT CAN-DO( "U,Q", Voucher.VoucherStatus) THEN
    DISABLE btn_Add btn_OK .
  ELSE DO:
    IF mode = "View" THEN DO:
      DISABLE btn_OK br-vchlne fil_batch-no Voucher.TaxValue fil_Total cmb_PaymentStyle .
      RUN dispatch ( 'disable-fields':U ).
      ENABLE Voucher.VoucherSeq.
    END.
    ELSE
      ENABLE UNLESS-HIDDEN btn_OK fil_batch-no br-vchlne
            Voucher.Date Voucher.ApproverCode Voucher.SecondApprover Voucher.InvoiceReference
            Voucher.OurOrderNo Voucher.DateDue Voucher.Description 
            Voucher.GoodsValue Voucher.TaxValue fil_Total .

    IF mode = "Register" THEN
      ENABLE UNLESS-HIDDEN btn_Add .

    IF mode = "Maintain" OR mode = "Register" THEN
      ENABLE UNLESS-HIDDEN Voucher.EntityType Voucher.EntityCode Voucher.OrderCode Voucher.CreditorCode.
    ELSE DO:
      ENABLE Voucher.EntityType Voucher.EntityCode Voucher.OrderCode Voucher.VoucherSeq.
    END.
  END.

  /* All select buttons to sensitive, as appropriate */
  IF mode = "View" THEN
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_Creditor), "SENSITIVE = No" ).
  ELSE
    RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, "ALL", "SENSITIVE = Yes" ).


END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE":U THEN RUN cancel-changes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     If browsing from a creditor then default the creditor code
  Parameters:  <none>
  Notes:
------------------------------------------------------------------------------*/
  RETURN.
  IF mode <> "Register" THEN RETURN.
  
  DEF VAR c-recsrc   AS CHAR NO-UNDO.
  DEF VAR creditor-code AS CHAR NO-UNDO.

  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT c-recsrc ).
  RUN send-key IN WIDGET-HANDLE( c-recsrc ) ( "CreditorCode", OUTPUT creditor-code ).

  FIND FIRST Creditor WHERE Creditor.CreditorCode = INT( creditor-code )
    NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Creditor) THEN DO:
    RUN get-attribute IN WIDGET-HANDLE( c-recsrc ) ( 'external-tables':U ) NO-ERROR.
    IF LOOKUP( "Creditor", RETURN-VALUE) > 0 THEN DO:
      RUN send-records IN WIDGET-HANDLE( c-recsrc ) ( "Creditor", OUTPUT creditor-code) NO-ERROR.
      FIND Creditor WHERE ROWID(Creditor) = TO-ROWID( creditor-code ) NO-LOCK NO-ERROR.
    END.
  END.

  IF AVAILABLE Creditor THEN DO WITH FRAME {&FRAME-NAME}:
    fil_Creditor:PRIVATE-DATA = STRING( ROWID( Creditor ) ).
    APPLY 'U2':U TO fil_Creditor.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-vouchers V-table-Win 
PROCEDURE print-vouchers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR n AS INT NO-UNDO.

  IF printed-vouchers THEN RETURN.
  IF mode = 'Approve' THEN RETURN.

DEF VAR print-them AS LOGI NO-UNDO INITIAL Yes.

  n = NUM-ENTRIES( voucher-list ).
  IF  n = 0 THEN RETURN.

  IF print-vouchers = "Never" THEN RETURN.
  ELSE IF print-vouchers = "Register" AND mode = "Register" THEN /* print vouchers */ .
  ELSE IF print-vouchers = "Confirm" THEN DO:
    MESSAGE n "Vouchers to be Printed" VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL
              TITLE "Do you want to print the vouchers?"
              UPDATE print-them.
    IF NOT print-them THEN RETURN.
  END.
  ELSE
    MESSAGE n "Vouchers to be Printed" VIEW-AS ALERT-BOX INFORMATION.

  printed-vouchers = Yes.
  CASE voucher-style:
    WHEN "1" THEN RUN process/report/vchrappr.p( ?, ?, voucher-list ).
    WHEN "2" THEN RUN process/report/vchrapr2.p( ?, ?, voucher-list ).
    OTHERWISE     RUN process/report/vchrappr.p( ?, ?, voucher-list ).
  END CASE.

  RETURN "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-lines V-table-Win 
PROCEDURE save-lines :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF mode = "Register" THEN RETURN.
  IF NOT AVAILABLE Voucher THEN RETURN.
  
  FOR EACH CurrentLine: DELETE CurrentLine. END.

  FOR EACH VoucherLine OF Voucher NO-LOCK:
    CREATE CurrentLine.
    ASSIGN
      CurrentLine.EntityType  = VoucherLine.EntityType
      CurrentLine.EntityCode  = VoucherLine.EntityCode
      CurrentLine.AccountCode = VoucherLine.AccountCode
      CurrentLine.Description = VoucherLine.Description
      CurrentLine.Amount      = VoucherLine.Amount
      CurrentLine.VoucherSeq  = VoucherLine.VoucherSeq.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-account-code V-table-Win 
PROCEDURE select-account-code :
/*----------------------------------------------------------------------------
  Purpose:     Select an account code for the Transaction
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER attach-to AS HANDLE NO-UNDO.
  DEF VAR item AS CHAR NO-UNDO.

  selecting-code = Yes.
  
DO WITH FRAME {&FRAME-NAME}:
  
  IF NOT VALID-HANDLE( coa-list ) THEN RUN create-coa-list.
      
  /* Set the COA List screen value */
  FIND ChartOfAccount WHERE
    ChartOfAccount.AccountCode = DEC( attach-to:SCREEN-VALUE )
  NO-LOCK NO-ERROR.
  
  IF AVAILABLE ChartOfAccount THEN
  DO:  
    item = STRING( ChartOfAccount.AccountCode, "9999.99" ) + '  ' + ChartOfAccount.Name.
    coa-list:SCREEN-VALUE = item.
  END.
  ELSE
    coa-list:SCREEN-VALUE = coa-list:ENTRY(1).
  
  /* Set the position */

  IF br-vchlne:y + attach-to:y +
     attach-to:height-pixels + coa-list:height-pixels >
     FRAME {&FRAME-NAME}:HEIGHT-PIXELS THEN
    coa-list:y = br-vchlne:y + attach-to:y - coa-list:height-pixels.
  ELSE
    coa-list:y = br-vchlne:y + attach-to:y + attach-to:height-pixels.
    
  coa-list:x = br-vchlne:x + attach-to:x + 20.
  coa-list:visible   = Yes.
  coa-list:sensitive = Yes.
  APPLY 'ENTRY':U TO coa-list.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Voucher"}
  {src/adm/template/snd-list.i "VoucherLine"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-batch-verified V-table-Win 
PROCEDURE set-batch-verified :
/*------------------------------------------------------------------------------
  Purpose:  Just mark the batch-no as having been verified so we tab
            past it in future.  Also many extra fields are now enabled.
------------------------------------------------------------------------------*/

  IF verified-batch THEN RETURN.
  verified-batch = Yes.
  ASSIGN FRAME {&FRAME-NAME} fil_batch-no .

  RUN dispatch( 'enable-fields':U ).

  IF AVAILABLE(Voucher) THEN
    APPLY 'ENTRY':U TO br-vchlne IN FRAME {&FRAME-NAME}.
  ELSE
    APPLY 'ENTRY':U TO Voucher.VoucherSeq IN FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-creditor-defaults V-table-Win 
PROCEDURE set-creditor-defaults :
/*------------------------------------------------------------------------------
  Purpose:  Set any defaults from the creditor record
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME} TRANSACTION:
  IF Voucher.CreditorCode = INPUT Voucher.CreditorCode THEN RETURN.
  
  FIND CURRENT Voucher EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE(Voucher) THEN RETURN.
  /* IF Voucher.ApproverCode <> "" THEN RETURN. */

DEF BUFFER MyCred FOR Creditor.
  FIND MyCred NO-LOCK WHERE MyCred.CreditorCode = INPUT Voucher.CreditorCode NO-ERROR.
  IF NOT AVAILABLE(MyCred) THEN RETURN.

  Voucher.ApproverCode = MyCred.VchrApprover. 
  Voucher.CreditorCode = MyCred.CreditorCode. 
  fil_Creditor = MyCred.Name .
  Voucher.EntityType = (IF MyCred.VchrEntityType <> "" THEN MyCred.VchrEntityType ELSE "P"). 
  Voucher.EntityCode = MyCred.VchrEntityCode. 
  Voucher.AccountCode = MyCred.VchrAccountCode. 
  Voucher.PaymentStyle = MyCred.PaymentStyle . 

  FIND CURRENT Voucher NO-LOCK.

  FIND Approver WHERE Voucher.ApproverCode = Approver.ApproverCode NO-LOCK NO-ERROR.
  IF AVAILABLE(Approver) THEN FIND Person OF Approver NO-LOCK NO-ERROR.
  IF AVAILABLE(Person) THEN fil_approval = Person.FirstName + " " + Person.LastName .
  DISPLAY Voucher.CreditorCode fil_Creditor
          Voucher.ApproverCode fil_approval Voucher.EntityType Voucher.EntityCode .
  APPLY 'U1':U TO cmb_PaymentStyle .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-trn-color V-table-Win 
PROCEDURE set-trn-color :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR fcolor AS INT INIT ? NO-UNDO.
  DEF VAR bcolor AS INT INIT ? NO-UNDO.
  
  DEF VAR type   AS CHAR NO-UNDO.
  DEF VAR amount AS DEC  NO-UNDO.
  type = IF CAN-QUERY( VoucherLine.EntityType:HANDLE IN BROWSE br-vchlne, "SCREEN-VALUE" ) THEN
    VoucherLine.EntityType:SCREEN-VALUE ELSE VoucherLine.EntityType.
  amount = IF CAN-QUERY( VoucherLine.Amount:HANDLE IN BROWSE br-vchlne, "SCREEN-VALUE" ) THEN
    DEC( VoucherLine.Amount:SCREEN-VALUE ) ELSE VoucherLine.Amount.

  CASE type:
    WHEN "L" THEN ASSIGN fcolor = 1  bcolor = 15.
    WHEN "P" THEN ASSIGN fcolor = 2  bcolor = 15.
    WHEN "T" THEN ASSIGN fcolor = 4  bcolor = 15.
    WHEN "J" THEN ASSIGN fcolor = 6 bcolor = 15.
    WHEN "C" THEN ASSIGN fcolor = 5 bcolor = 15.
  END CASE.

  ASSIGN
    VoucherLine.EntityType:FGCOLOR  = fcolor
    VoucherLine.EntityCode:FGCOLOR  = fcolor
    VoucherLine.AccountCode:FGCOLOR = fcolor
    VoucherLine.Amount:FGCOLOR      = IF amount < 0 THEN 12 ELSE 0
    VoucherLine.Description:FGCOLOR = fcolor.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-trn-defaults V-table-Win 
PROCEDURE set-trn-defaults :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN get-default-trn.
  IF NOT AVAILABLE DefTrans THEN RETURN.  

  ASSIGN
    VoucherLine.EntityType:SCREEN-VALUE IN BROWSE br-vchlne = DefTrans.EntityType
    VoucherLine.EntityCode:SCREEN-VALUE  = STRING( DefTrans.EntityCode )
    VoucherLine.AccountCode:SCREEN-VALUE = STRING( DefTrans.AccountCode )
    VoucherLine.Amount:SCREEN-VALUE      = STRING( DefTrans.Amount )
    VoucherLine.Description:SCREEN-VALUE = DefTrans.Description

    VoucherLine.EntityType:MODIFIED  = No 
    VoucherLine.EntityCode:MODIFIED  = No 
    VoucherLine.AccountCode:MODIFIED = No
    VoucherLine.Amount:MODIFIED      = No     
    VoucherLine.Description:MODIFIED = No.

  RUN update-trn-fields.
  RUN update-entity-fields.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-ac-fields V-table-Win 
PROCEDURE update-ac-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN update-trn-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-account-code V-table-Win 
PROCEDURE update-account-code :
/*------------------------------------------------------------------------------
  Purpose:     Update the code value.
------------------------------------------------------------------------------*/

  VoucherLine.AccountCode:SCREEN-VALUE IN BROWSE br-vchlne = 
    ENTRY( 1, coa-list:SCREEN-VALUE, ' ').
  RUN update-ac-fields.
  RUN hide-coa-list.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-account-name V-table-Win 
PROCEDURE update-account-name :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR type AS CHAR NO-UNDO.
DEF VAR code AS INT  NO-UNDO.
DEF VAR acct AS DEC  NO-UNDO.

  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.
  acct = INPUT BROWSE br-vchlne VoucherLine.AccountCode.

  /* Check to see if the account exists */
  IF type = "J" THEN DO:
    code = INPUT BROWSE br-vchlne VoucherLine.EntityCode.
    FIND Project WHERE Project.ProjectCode = code NO-LOCK NO-ERROR.
    IF AVAILABLE(Project) AND Project.ExpenditureType <> "G" THEN DO:
      FIND ProjectBudget WHERE ProjectBudget.ProjectCode = code
                                AND ProjectBudget.AccountCode = acct NO-LOCK NO-ERROR.
      IF AVAILABLE(ProjectBudget) THEN DO WITH FRAME {&FRAME-NAME}:
        fil_Account:SCREEN-VALUE = ProjectBudget.Description.
        RETURN.
      END.
    END.
  END.

  FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = acct NO-LOCK NO-ERROR.
  ASSIGN fil_Account:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
            IF AVAILABLE ChartOfAccount THEN ChartOfAccount.Name ELSE "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-description V-table-Win 
PROCEDURE update-description :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  VoucherLine.Description:SCREEN-VALUE IN BROWSE br-vchlne = Voucher.Description.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-fields V-table-Win 
PROCEDURE update-entity-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.

  ASSIGN
    VoucherLine.AccountCode:SCREEN-VALUE =
      IF type = 'T' THEN STRING( sundry-debtors )   ELSE
      IF type = 'C' THEN STRING( sundry-creditors ) ELSE
      VoucherLine.AccountCode:SCREEN-VALUE
    VoucherLine.AccountCode:MODIFIED = No
    VoucherLine.AccountCode:READ-ONLY = ((type = "C") OR ((type = "T") AND NOT tenant-accounts))
    VoucherLine.AccountCode:MODIFIED = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-name V-table-Win 
PROCEDURE update-entity-name :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR type AS CHAR NO-UNDO.
  DEF VAR code AS INT  NO-UNDO.
  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.
  code = INPUT BROWSE br-vchlne VoucherLine.EntityCode.
  
  CASE type:
  
    WHEN 'T' THEN DO:
      FIND FIRST Tenant WHERE Tenant.TenantCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
                IF AVAILABLE Tenant THEN Tenant.Name ELSE "".
    END.

    WHEN 'C' THEN DO:
      FIND FIRST Creditor WHERE Creditor.CreditorCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
                IF AVAILABLE Creditor THEN Creditor.Name ELSE "".
    END.
  
    WHEN 'P' THEN DO:
      FIND FIRST Property WHERE Property.PropertyCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
                IF AVAILABLE Property THEN Property.Name ELSE "".
      RUN assign-approver( "P", code ).
    END.
  
    WHEN 'L' THEN DO:
      FIND FIRST Company WHERE Company.CompanyCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
                IF AVAILABLE Company THEN Company.LegalName ELSE "".
    END.
  
    WHEN 'J' THEN DO:
      FIND FIRST Project WHERE Project.ProjectCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
                IF AVAILABLE Project THEN Project.Name ELSE "".
      IF NOT AVAILABLE(Project) THEN .
      ELSE IF Project.ExpenditureType = "G" THEN
        RUN assign-approver( "P", code ).
      ELSE
        RUN assign-approver( "J", code ).
    END.

    OTHERWISE fil_Entity:SCREEN-VALUE = "".
    
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-from-order V-table-Win 
PROCEDURE update-from-order :
/*------------------------------------------------------------------------------
  Purpose:     Set the coding details for the current voucher
               from the current order
  Parameters:  <none>
  Notes:       Order must be available
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER entity-type AS CHAR NO-UNDO.
  DEF INPUT PARAMETER entity-code LIKE Project.ProjectCode NO-UNDO.
  DEF INPUT PARAMETER order-code   LIKE Order.OrderCode NO-UNDO.
  
  DEF VAR et AS CHAR NO-UNDO.
  DEF VAR ec AS INT NO-UNDO.
  DEF VAR ac AS DEC NO-UNDO.
  DEF VAR return-me AS CHAR NO-UNDO INITIAL "NOTHING".
  
  FIND FIRST Order WHERE Order.EntityType = entity-type
                     AND Order.EntityCode = entity-code
                     AND Order.OrderCode = order-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE Order THEN RETURN.

  /* Creditor */

  FIND Creditor OF Order NO-LOCK NO-ERROR.
  IF AVAILABLE Creditor THEN DO:
    fil_Creditor:PRIVATE-DATA = STRING( ROWID( Creditor ) ).
    APPLY 'U2':U TO fil_Creditor.
    APPLY 'ENTRY':U TO Voucher.Date IN FRAME {&FRAME-NAME}.
    return-me = "NO-APPLY".     /* tell the LEAVE trigger to RETURN NO-APPLY */
  END.
  ELSE DO:
    fil_Creditor:SCREEN-VALUE = "".
    fil_Creditor:PRIVATE-DATA = "".
    Voucher.CreditorCode:SCREEN-VALUE = STRING( 0 ).
    Voucher.CreditorCode:PRIVATE-DATA = "".
  END.


  /* Approvers */
  FIND Approver WHERE Approver.ApproverCode = Order.FirstApprover NO-LOCK NO-ERROR.
  IF AVAILABLE Approver THEN DO:
    fil_approval:PRIVATE-DATA = STRING( ROWID( Approver ) ).
    APPLY 'U2':U TO fil_approval.
  END.
  ELSE
  DO:
    fil_approval:SCREEN-VALUE = "".
    fil_approval:PRIVATE-DATA = "".
    Voucher.ApproverCode:SCREEN-VALUE = "".
  END.


  /* Entity Type/Code &  Account Code */
  ec = entity-code.
  et = entity-type.
  ac = Order.AccountCode.

  IF et <> "J" THEN DO:
    FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = ac NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(ChartOfAccount) THEN ac = 0.
  END.
  ELSE DO:
    FIND ProjectBudget WHERE ProjectBudget.ProjectCode = Order.ProjectCode
                        AND ProjectBudget.AccountCode = ac NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(ProjectBudget) THEN ac = 0.
  END.

  DEF BUFFER DefaultLine FOR VoucherLine.
  FIND FIRST DefaultLine OF Voucher NO-ERROR.
  IF NOT AVAILABLE DefaultLine THEN DO:
    CREATE  DefaultLine.
    ASSIGN  DefaultLine.VoucherSeq = Voucher.VoucherSeq
            DefaultLine.Description = Voucher.Description
            DefaultLine.Amount = Voucher.GoodsValue.
  END.

  ASSIGN  
    DefaultLine.EntityType = et
    DefaultLine.EntityCode = ec
    DefaultLine.AccountCode = ac.
  RELEASE DefaultLine.

  /* Our order number */
  Voucher.OurOrderNo:SCREEN-VALUE = entity-type + STRING( entity-code ) + "/" + STRING( order-code ).
  
  IF NUM-RESULTS( "br-vchlne" ) = 0 OR NUM-RESULTS( "br-vchlne" ) = ? THEN
    RUN open-trn-query.
  ELSE IF BROWSE {&BROWSE-NAME}:REFRESH() THEN.
  
END.

  RETURN return-me.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-gst-table V-table-Win 
PROCEDURE update-gst-table :
/*------------------------------------------------------------------------------
  Purpose:  Updates the GST table that stores the proportion of GST to be allocated for each company 
                    for all the transactions in the current  document.   
                    2003/03/17 GLF  
------------------------------------------------------------------------------*/
DEF VAR v_eType  LIKE EntityType.EntityType  NO-UNDO.
DEF VAR v_eCode LIKE company.CompanyCode  NO-UNDO.

ASSIGN v_EType =VoucherLine.EntityType
               v_ECode= VoucherLine.EntityCode.

/* Drill up to the first company/ledger hierarchy */
IF v_eType <> 'L'  THEN DO:
    REPEAT WHILE v_eType <> 'L':
         IF v_eType = 'J'  THEN DO:
             FIND Project WHERE Project.ProjectCode = v_eCode NO-LOCK.
              ASSIGN   v_eType =Project.EntityType
                               v_eCode =Project.EntityCode.
          END.
          ELSE DO:
              IF v_eType = 'P' THEN DO:
                FIND Property WHERE Property.PropertyCode = v_eCode NO-LOCK.
                ASSIGN v_eType = 'L'
                                v_eCode = Property.CompanyCode.
              END.
         END.
    END. /* Repeat */
END. /* Do */

/* Update amounts to company gst temporary table */
FIND company-gst WHERE company-gst.company_id  = v_eCode EXCLUSIVE-LOCK NO-ERROR.
IF AVAILABLE company-gst  THEN 
    company-gst.tax_amount  = company-gst.tax_amount + 
                                                       ((VoucherLine.Amount  - VoucherLine.TaxAmount) *
                                                         (DECIMAL(gst-rate) / 100 )) . 
ELSE DO:
   CREATE company-gst.
    ASSIGN company-gst.company_id = v_eCode
                    company-gst.tax_amount = (VoucherLine.Amount  - VoucherLine.TaxAmount) *
                                                                       (DECIMAL(gst-rate) / 100 ).
END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-line V-table-Win 
PROCEDURE update-line :
/*------------------------------------------------------------------------------
  Purpose:     update the current invoice line.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Create a new line if needed and reset the details */

  IF br-vchlne:NEW-ROW IN FRAME {&FRAME-NAME} THEN RUN create-line.
  RUN assign-line.

  trn-modified = No.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-trn-fields V-table-Win 
PROCEDURE update-trn-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NUM-RESULTS( "br-vchlne" ) = 0 OR NUM-RESULTS( "br-vchlne" ) = ? THEN DO:
    fil_Account:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
    fil_entity:SCREEN-VALUE = "".
    RETURN.
  END.
  
  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.

  ASSIGN VoucherLine.AccountCode:READ-ONLY IN BROWSE br-vchlne
    = LOOKUP( type, 'T,C':U ) <> 0.

  RUN update-entity-name.
  RUN update-account-name.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-account-code V-table-Win 
PROCEDURE verify-account-code :
/*------------------------------------------------------------------------------
  Purpose:     Verify that the transaction's account is a valid
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER mode AS CHAR NO-UNDO.
  
DEF VAR type AS CHAR NO-UNDO.
DEF VAR code AS INT  NO-UNDO.
DEF VAR acct AS DEC  NO-UNDO.
DEF VAR success AS LOGI INITIAL Yes NO-UNDO.

  type = INPUT BROWSE {&BROWSE-NAME} VoucherLine.EntityType.
  code = INPUT BROWSE {&BROWSE-NAME} VoucherLine.EntityCode.
  acct = INPUT BROWSE {&BROWSE-NAME} VoucherLine.AccountCode.

  /* Check to see if the account exists */
  IF type = "J" THEN
    success = CAN-FIND( ProjectBudget WHERE ProjectBudget.ProjectCode = code
                                AND ProjectBudget.AccountCode = acct ).
  ELSE
    success = CAN-FIND( ChartOfAccount WHERE ChartOfAccount.AccountCode = acct ).

  IF NOT success THEN DO:
    IF mode = "Verify" THEN
      MESSAGE "The account code " + STRING( acct ) + " is invalid !"
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
    RETURN "FAIL".
  END.

  IF type = "J" AND restrict-project-posting THEN DO:  
    FIND ProjectBudget WHERE ProjectBudget.ProjectCode = code
                         AND ProjectBudget.AccountCode = acct NO-LOCK.
    IF NOT ProjectBudget.AllowPosting THEN DO:
      IF mode = "Verify" THEN DO:
        FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
        MESSAGE "Project account " + STRING( acct ) + " cannot be updated to."
                 VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
      END.
      RETURN "FAIL".
    END.
  END.
  ELSE IF type <> "J" THEN DO:  
    FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = acct NO-LOCK.

    /* Check to see if we can update to this account for the given entity type */

    IF INDEX( ChartOfAccount.UpdateTo, type ) = 0 THEN DO:
      IF mode = "Verify" THEN DO:
        FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
        MESSAGE "Account " + STRING( acct ) + " cannot be updated to a " + 
                 EntityType.Description
                 VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
      END.
      RETURN "FAIL".
    END.
  END.

  IF NOT verifying-line THEN RUN update-ac-fields.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-entity-code V-table-Win 
PROCEDURE verify-entity-code :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER mode AS CHAR NO-UNDO.

  DEF VAR entity-ok AS LOGI INIT No NO-UNDO.
  DEF VAR entity-warn AS LOGI INIT No NO-UNDO.
  DEF VAR type AS CHAR NO-UNDO.
  DEF VAR code AS INT  NO-UNDO.
  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.
  code = INPUT BROWSE br-vchlne VoucherLine.EntityCode.
  
  entity-ok = 
    IF type = 'T' THEN CAN-FIND( FIRST Tenant   WHERE Tenant.TenantCode = code AND Tenant.Active)     ELSE
    IF type = 'C' THEN CAN-FIND( FIRST Creditor WHERE Creditor.CreditorCode = code AND Creditor.Active) ELSE
    IF type = 'P' THEN CAN-FIND( FIRST Property WHERE Property.PropertyCode = code AND Property.Active) ELSE
    IF type = 'L' THEN CAN-FIND( FIRST Company  WHERE Company.CompanyCode = code AND Company.Active)   ELSE
    IF type = 'J' THEN CAN-FIND( FIRST Project  WHERE Project.ProjectCode = code AND Project.Active)   ELSE
      No.

  entity-warn = 
    IF type = 'T' THEN CAN-FIND( FIRST Tenant   WHERE Tenant.TenantCode = code )     ELSE
    IF type = 'C' THEN CAN-FIND( FIRST Creditor WHERE Creditor.CreditorCode = code ) ELSE
    IF type = 'P' THEN CAN-FIND( FIRST Property WHERE Property.PropertyCode = code ) ELSE
    IF type = 'L' THEN CAN-FIND( FIRST Company  WHERE Company.CompanyCode = code )   ELSE
    IF type = 'J' THEN CAN-FIND( FIRST Project  WHERE Project.ProjectCode = code )   ELSE
      No.

      
  IF NOT entity-ok THEN
  DO:
    IF mode = "Verify" THEN
    DO:
      FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
      IF entity-warn THEN DO:
        MESSAGE "Warning: " + EntityType.Description + " " + STRING( code ) + " is inactive"
            VIEW-AS ALERT-BOX WARNING TITLE "Inactive " + EntityType.Description + " Warning - " + type + STRING(code).
      END.
      ELSE DO:
        MESSAGE "There is no " + EntityType.Description + " with code " + STRING( code )
            VIEW-AS ALERT-BOX ERROR TITLE "Entity Code Error - " + EntityType.Description.
        RETURN "FAIL".
      END.
    END.
  END.

  RUN update-entity-name.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-entity-type V-table-Win 
PROCEDURE verify-entity-type :
/*------------------------------------------------------------------------------
  Purpose:     Verify the entity type
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER mode AS CHAR NO-UNDO.

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT BROWSE br-vchlne VoucherLine.EntityType.

  IF NOT CAN-FIND( FIRST Entitytype WHERE EntityType.EntityType = type ) THEN DO:
    IF mode = "Verify" THEN
      MESSAGE "The entity type " + type + " is invalid !"
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid Entity Type".
    RETURN "FAIL".
  END.

  IF verifying-line THEN RETURN.

  VoucherLine.EntityType:SCREEN-VALUE IN BROWSE br-vchlne = CAPS( type ).  
  RUN update-entity-fields.
  RUN update-account-name.
  RUN update-entity-name.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-line V-table-Win 
PROCEDURE verify-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR err-field AS HANDLE NO-UNDO.

  verifying-line = Yes.
  RUN check-line.
  verifying-line = No.
  
  err-field = WIDGET-HANDLE( RETURN-VALUE ).

  IF VALID-HANDLE( err-field ) THEN DO:
    MESSAGE "The current transaction line is in error." SKIP
      "Do you want to abort the changes ? "
      VIEW-AS ALERT-BOX ERROR BUTTONS YES-NO UPDATE abort-it AS LOGI.
    IF abort-it THEN DO:
      trn-modified = No.
      IF AVAILABLE VoucherLine AND VoucherLine.AccountCode <> 0000.00 THEN
        RUN display-line.
      ELSE
        RUN delete-line.
    END.
    ELSE DO:
      IF AVAILABLE VoucherLine THEN GET CURRENT br-vchlne.
      APPLY 'ENTRY':U TO err-field.
      RETURN "FAIL".
    END.
  END.
  ELSE
    RUN update-line.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-one-client V-table-Win 
PROCEDURE verify-one-client :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR test-client AS CHAR NO-UNDO.

DEF BUFFER v-line FOR VoucherLine.

  IF NOT AVAILABLE(Voucher) THEN RETURN.
  IF client-checking = No THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:
  test-client = get-entity-client( INPUT BROWSE {&BROWSE-NAME} VoucherLine.EntityType,
                            INPUT BROWSE {&BROWSE-NAME} VoucherLine.EntityCode ).

  FOR EACH v-line OF Voucher NO-LOCK:
    IF NOT( {&BROWSE-NAME}:NEW-ROW ) AND AVAILABLE(VoucherLine) THEN DO:
      IF RECID(v-line) = RECID(VoucherLine) THEN NEXT.
    END.
    IF test-client <> get-entity-client( v-line.EntityType, v-line.EntityCode ) THEN DO:
      MESSAGE "The allocations of a voucher may not be split" SKIP
              "across the properties or companies of more than" SKIP
              "one client/owner."
              VIEW-AS ALERT-BOX ERROR
              TITLE "Voucher Allocated across multiple clients/owners".
      RETURN "FAIL".
    END.
  END.
END.

  RETURN "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-voucher V-table-Win 
PROCEDURE verify-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR thats-fine AS LOGICAL NO-UNDO INITIAL No.
DEF VAR must-allocate AS LOGI NO-UNDO.

&SCOP t_format "$ ->>>,>>>,>>9.99"

IF mode = "Approve" THEN must-allocate = Yes.
ELSE must-allocate = NOT allow-unallocated.

DO WITH FRAME {&FRAME-NAME}:

  IF INPUT Voucher.CreditorCode = 0
        OR NOT CAN-FIND( Creditor WHERE Creditor.CreditorCode = INPUT Voucher.CreditorCode) THEN
  DO:
    MESSAGE "You must select a creditor!" VIEW-AS ALERT-BOX ERROR
      TITLE "No creditor selected".
    APPLY 'ENTRY':U TO Voucher.CreditorCode.
    RETURN "FAIL".
  END.

  IF INPUT Voucher.Date = ? OR INPUT Voucher.Date < ( TODAY - 1100 ) THEN DO:
    MESSAGE "The date of this voucher is too old" SKIP(1)
            VIEW-AS ALERT-BOX ERROR
            TITLE "Voucher more than 3 years old".
    APPLY "ENTRY":U TO Voucher.Date.
    RETURN "FAIL".
  END.
  ELSE IF INPUT Voucher.Date < ( TODAY - 200 ) THEN DO:
    MESSAGE "The date of this voucher is some time past" SKIP(1)
            "Is that OK?"
            VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL
            TITLE "Voucher more than 7 months old" UPDATE thats-fine .
    IF NOT thats-fine THEN DO:
      APPLY "ENTRY":U TO Voucher.Date.
      RETURN "FAIL".
    END.
  END.
  ELSE IF INPUT Voucher.Date > (TODAY + 60) THEN DO:
    MESSAGE "The date of this voucher is in the future" SKIP(1)
            "Is that OK?"
            VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL
            TITLE "Voucher more than 2 months in the future" UPDATE thats-fine .
    IF NOT thats-fine THEN DO:
      APPLY "ENTRY":U TO Voucher.Date.
      RETURN "FAIL".
    END.
  END.

  IF must-allocate AND (NUM-RESULTS( "br-vchlne" ) = 0 OR NUM-RESULTS( "br-vchlne" ) = ?) THEN DO:
    MESSAGE "You must enter at least one allocation for this voucher".
    APPLY 'ENTRY':U TO br-vchlne.
    RETURN "FAIL".
  END.  
  
  DEF VAR total AS DEC NO-UNDO.
  FOR EACH VoucherLine OF Voucher NO-LOCK: total = total + VoucherLine.Amount. END.
  
  IF total <> INPUT Voucher.GoodsValue THEN
  DO:
    MESSAGE "The sum of all the transactions should" SKIP 
            "equal the goods value of the voucher" SKIP(2)
            "You can press F3 on any transaction" SKIP
            "amount to make it balance"
      VIEW-AS ALERT-BOX ERROR TITLE "Incorrect Total, " +
        TRIM( STRING( total, {&t_format} ) ) + " vs " + 
        TRIM( STRING( INPUT Voucher.GoodsValue, {&t_format} ) ).
    IF must-allocate THEN DO:
      APPLY 'ENTRY':U TO VoucherLine.Amount IN BROWSE br-vchlne.
      RETURN "FAIL".
    END.
  END.

DEF BUFFER Other FOR Voucher.
  FIND FIRST Other WHERE Other.CreditorCode = INPUT Voucher.CreditorCode
               AND Other.InvoiceReference = INPUT Voucher.InvoiceReference
               AND Other.VoucherSeq <> INPUT Voucher.VoucherSeq
               NO-LOCK NO-ERROR.
  IF AVAILABLE(Other) THEN DO:
    thats-fine = No.
    MESSAGE "IS THIS A DUPLICATE INVOICE?" SKIP(1)
            "An earlier voucher," Other.VoucherSeq "for" fil_Creditor:SCREEN-VALUE + "," SKIP
            "has the same Invoice No of '" + Other.InvoiceReference + "'" SKIP(1)
            "Is that OK?"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK-CANCEL
            TITLE "Duplicate Invoice?" UPDATE thats-fine .
    IF NOT thats-fine THEN DO:
      APPLY "ENTRY":U TO Voucher.InvoiceReference.
      RETURN "FAIL".
    END.
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE voucher-date-changed V-table-Win 
PROCEDURE voucher-date-changed :
/*------------------------------------------------------------------------------
  Purpose:     If appropriate, assign the voucher due date based on the
               supplier's invoice date
------------------------------------------------------------------------------*/
  DEF VAR voucher-due AS DATE NO-UNDO.
  DEF VAR dd AS INT NO-UNDO.
  DEF VAR mm AS INT NO-UNDO.
  DEF VAR yy AS INT NO-UNDO.
  
DO WITH FRAME {&FRAME-NAME}:
  voucher-due = INPUT Voucher.DATE .
  IF default-due-next > 0 AND voucher-due <> ? THEN DO:
      mm = MONTH(voucher-due).
      yy = YEAR(voucher-due).
      
      mm = mm + 1.
      IF mm > 12 THEN DO:
          yy = yy + 1.
          mm = 1.
      END.
      voucher-due = DATE( mm, default-due-next, yy ).
      
      Voucher.DateDue:SCREEN-VALUE = STRING(voucher-due, "99/99/9999").
      Voucher.DateDue:MODIFIED = Yes.
    
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-trn-date V-table-Win 
FUNCTION get-trn-date RETURNS DATE
  ( OUTPUT m-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF VAR spec1 AS CHAR NO-UNDO.
DEF VAR spec2 AS CHAR NO-UNDO.
DEF VAR n-diff AS INT NO-UNDO INITIAL 0.
DEF VAR result1 AS DATE NO-UNDO.
DEF VAR result2 AS DATE NO-UNDO.

  m-code = ?.
  IF use-due-dates <> Yes THEN RETURN Voucher.Date.

  spec1 = ENTRY( 1, voucher-transaction-date, "/").
  spec1 = ENTRY( 1, spec1, "-").
  IF NUM-ENTRIES( voucher-transaction-date, "/") = 2 THEN
    spec2 = ENTRY( 2, voucher-transaction-date, "/").
  ELSE
    spec2 = spec1.

  IF spec2 MATCHES "*-*" THEN ASSIGN n-diff = - INT(ENTRY( 2, spec2, "-")) NO-ERROR.
  IF n-diff < 0 THEN . ELSE n-diff = 0.
  spec2 = ENTRY( 1, spec2, "-").

  CASE spec1:
    WHEN "Due" THEN       result1 = Voucher.DateDue.
    WHEN "Invoice" THEN   result1 = Voucher.Date.
    WHEN "Created" THEN   result1 = Voucher.CreatedDate.
    OTHERWISE result1 = TODAY.
  END CASE.
  CASE spec2:
    WHEN "Due" THEN       result2 = Voucher.DateDue - n-diff.
    WHEN "Invoice" THEN   result2 = Voucher.Date - n-diff.
    WHEN "Created" THEN   result2 = Voucher.CreatedDate - n-diff.
    OTHERWISE result2 = TODAY - n-diff.
  END CASE.

  IF result1 = ? THEN result1 = TODAY.
  IF result2 = ? THEN result2 = TODAY.
  IF result2 > result1 THEN DO:
    FIND LAST Month WHERE Month.StartDate <= result2 NO-LOCK NO-ERROR.
    IF AVAILABLE(Month) THEN m-code = Month.MonthCode.
    RETURN result2.
  END.

  RETURN result1.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validate-creditor-ledger V-table-Win 
FUNCTION validate-creditor-ledger RETURNS INTEGER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR creditor-company AS INT NO-UNDO INITIAL ?.
DEF VAR this-company AS INT NO-UNDO.

  FOR EACH VoucherLine OF Voucher NO-LOCK:
    this-company = get-entity-ledger( VoucherLine.EntityType, VoucherLine.EntityCode ).
    IF creditor-company = ? THEN creditor-company = this-company. ELSE DO:
      IF creditor-company <> this-company THEN RETURN ?.  /* May not split allocations across multiple companies */
    END.
  END.

  RETURN creditor-company.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

