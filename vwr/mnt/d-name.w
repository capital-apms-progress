&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:         vwr/mnt/d-name.w
  Description:  Dialog to handle full entry of person's name
  Author:       Andrew McMillan
  Created:      7 December 1998
------------------------------------------------------------------------*/

/* Parameters Definitions ---                                           */
DEF INPUT-OUTPUT PARAMETER p-title AS CHAR NO-UNDO.
DEF INPUT-OUTPUT PARAMETER p-first AS CHAR NO-UNDO.
DEF INPUT-OUTPUT PARAMETER p-middle AS CHAR NO-UNDO.
DEF INPUT-OUTPUT PARAMETER p-last AS CHAR NO-UNDO.
DEF INPUT-OUTPUT PARAMETER p-suffix AS CHAR NO-UNDO.

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE DIALOG-BOX

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME Dialog-Frame

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Person

/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-QUERY-Dialog-Frame OPEN QUERY Dialog-Frame FOR EACH Person SHARE-LOCK.
&Scoped-define TABLES-IN-QUERY-Dialog-Frame Person
&Scoped-define FIRST-TABLE-IN-QUERY-Dialog-Frame Person


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cmb_PersonTitle RECT-29 fil_FirstName ~
fil_MiddleName fil_LastName fil_Suffix Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS cmb_PersonTitle fil_FirstName ~
fil_MiddleName fil_LastName fil_Suffix 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 10 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 10 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE cmb_PersonTitle AS CHARACTER FORMAT "X(6)" 
     LABEL "Title" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Mr","Mrs","Ms","Miss","Dr","Sir","Dame","Lord","Hon","(None)" 
     SIZE 8.57 BY 1
     FONT 10.

DEFINE VARIABLE fil_FirstName AS CHARACTER FORMAT "X(256)":U 
     LABEL "First" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fil_LastName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Last" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fil_MiddleName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Middle" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Suffix AS CHARACTER FORMAT "X(256)":U 
     LABEL "Suffix" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-29
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 30 BY 7.3.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY Dialog-Frame FOR 
      Person SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cmb_PersonTitle AT ROW 1.8 COL 6.72 COLON-ALIGNED
     fil_FirstName AT ROW 3.2 COL 6.72 COLON-ALIGNED
     fil_MiddleName AT ROW 4.4 COL 6.72 COLON-ALIGNED
     fil_LastName AT ROW 5.6 COL 6.72 COLON-ALIGNED
     fil_Suffix AT ROW 7 COL 6.72 COLON-ALIGNED
     Btn_OK AT ROW 1.7 COL 33.57
     Btn_Cancel AT ROW 2.95 COL 33.57
     RECT-29 AT ROW 1.2 COL 1.86
     SPACE(12.99) SKIP(0.29)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 10
         TITLE "Enter Name Details"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: DIALOG-BOX
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   Custom                                                               */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX Dialog-Frame
/* Query rebuild information for DIALOG-BOX Dialog-Frame
     _TblList          = "TTPL.Person"
     _Options          = "SHARE-LOCK"
     _Query            is OPENED
*/  /* DIALOG-BOX Dialog-Frame */
&ANALYZE-RESUME

 




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Enter Name Details */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

cmb_PersonTitle = p-title.
fil_FirstName   = p-first.
fil_LastName    = p-last.
fil_MiddleName  = p-middle.
fil_Suffix      = p-suffix.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  ASSIGN FRAME {&FRAME-NAME} cmb_PersonTitle fil_FirstName fil_LastName fil_MiddleName fil_Suffix.
END.
RUN disable_UI.

  p-title  = cmb_PersonTitle .
  p-first  = fil_FirstName .
  p-last   = fil_LastName .
  p-middle = fil_MiddleName .
  p-suffix = fil_Suffix .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/

  {&OPEN-QUERY-Dialog-Frame}
  GET FIRST Dialog-Frame.
  DISPLAY cmb_PersonTitle fil_FirstName fil_MiddleName fil_LastName fil_Suffix 
      WITH FRAME Dialog-Frame.
  ENABLE cmb_PersonTitle RECT-29 fil_FirstName fil_MiddleName fil_LastName 
         fil_Suffix Btn_OK Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


