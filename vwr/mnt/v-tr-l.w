&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "Andrew" NO-UNDO.
DEF VAR group-list AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Int1 RP.Char6 RP.Int2 RP.Char4 ~
RP.Dec1 RP.Char5 RP.Dec2 RP.Log3 RP.Date1 RP.Log4 RP.Int5 RP.Log1 RP.Log6 ~
RP.Log7 RP.Log8 RP.Log9 RP.Char3 RP.Log5 RP.Char2 RP.Log2 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define ENABLED-OBJECTS fil_AccountLike cb-month1 cb-month2 ~
tgl_MonthTotals tgl_NoOpeningBalances btn_Browse Btn_OK RECT-1 RECT-4 ~
RECT-5 RECT-6 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Int1 RP.Char6 RP.Int2 RP.Char4 ~
RP.Dec1 RP.Char5 RP.Dec2 RP.Log3 RP.Date1 RP.Log4 RP.Int5 RP.Log1 RP.Log6 ~
RP.Log7 RP.Log8 RP.Log9 RP.Char3 RP.Log5 RP.Char2 RP.Log2 
&Scoped-Define DISPLAYED-OBJECTS fil_comp1 fil_comp2 fil_account1 ~
fil_account2 fil_AccountLike cb-month1 cb-month2 tgl_MonthTotals ~
tgl_NoOpeningBalances 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Browse 
     LABEL "Browse" 
     SIZE 6.29 BY 1.05
     FONT 10.

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "&OK" 
     SIZE 12 BY 1.2
     BGCOLOR 8 .

DEFINE VARIABLE cb-month1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "10/07/1886" 
     DROP-DOWN-LIST
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE cb-month2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_account1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51.29 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_account2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51.29 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_AccountLike AS CHARACTER FORMAT "XXXX.XX":U INITIAL "******" 
     LABEL "Pattern" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "Enter a pattern to match the accounts to be reported" NO-UNDO.

DEFINE VARIABLE fil_comp1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51.29 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_comp2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51.29 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 89.14 BY 19.6.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 88 BY 3.8.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 88 BY 2.8.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 88 BY 7.8.

DEFINE VARIABLE tgl_MonthTotals AS LOGICAL INITIAL no 
     LABEL "running totals at end of each period" 
     VIEW-AS TOGGLE-BOX
     SIZE 27.72 BY 1 NO-UNDO.

DEFINE VARIABLE tgl_NoOpeningBalances AS LOGICAL INITIAL no 
     LABEL "Don't show opening balances" 
     VIEW-AS TOGGLE-BOX
     SIZE 24 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.4 COL 2.14 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Single company", "1":U,
"Company range", "R":U,
"Company list", "L":U
          SIZE 14.29 BY 2.4
          FONT 10
     RP.Int1 AT ROW 1.6 COL 23.57 COLON-ALIGNED
          LABEL "Company" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_comp1 AT ROW 1.6 COL 35.72 COLON-ALIGNED NO-LABEL
     RP.Char6 AT ROW 2 COL 23.57 COLON-ALIGNED HELP
          ""
          LABEL "List" FORMAT "X(100)"
          VIEW-AS COMBO-BOX INNER-LINES 10
          LIST-ITEMS "TTP" 
          DROP-DOWN-LIST
          SIZE 63.43 BY 1
     RP.Int2 AT ROW 2.6 COL 23.57 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_comp2 AT ROW 2.6 COL 35.72 COLON-ALIGNED NO-LABEL
     RP.Char4 AT ROW 4.4 COL 2.14 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Single account", "1":U,
"Account range", "R":U,
"Account group", "G":U,
"Accounts like", "L":U
          SIZE 14.86 BY 3.2
          FONT 10
     RP.Dec1 AT ROW 4.6 COL 23.57 COLON-ALIGNED HELP
          ""
          LABEL "Account" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 8 BY 1
     fil_account1 AT ROW 4.6 COL 35.72 COLON-ALIGNED NO-LABEL
     RP.Char5 AT ROW 5 COL 23.57 COLON-ALIGNED HELP
          ""
          LABEL "Group" FORMAT "X(100)"
          VIEW-AS COMBO-BOX INNER-LINES 20
          LIST-ITEMS "PROPEX - Property Expenses" 
          DROP-DOWN-LIST
          SIZE 63.43 BY 1
          FONT 8
     RP.Dec2 AT ROW 5.6 COL 23.57 COLON-ALIGNED HELP
          ""
          LABEL "to" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 8 BY 1
     fil_account2 AT ROW 5.6 COL 35.72 COLON-ALIGNED NO-LABEL
     fil_AccountLike AT ROW 6.6 COL 23.57 COLON-ALIGNED
     cb-month1 AT ROW 8.4 COL 9.29 COLON-ALIGNED
     cb-month2 AT ROW 8.4 COL 35.57 COLON-ALIGNED
     RP.Log3 AT ROW 9.8 COL 11.29
          LABEL "Only where Transaction Date greater than"
          VIEW-AS TOGGLE-BOX
          SIZE 32 BY 1
          FONT 10
     RP.Date1 AT ROW 9.8 COL 41.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     RP.Log4 AT ROW 10.8 COL 11.29
          LABEL "Only where batch no. greater or equal to"
          VIEW-AS TOGGLE-BOX
          SIZE 30.29 BY 1
          FONT 10
     RP.Int5 AT ROW 10.8 COL 39.57 COLON-ALIGNED NO-LABEL FORMAT ">>>,>>9"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     RP.Log1 AT ROW 11.8 COL 11.29 HELP
          ""
          LABEL "Show all, including for high-volume accounts"
          VIEW-AS TOGGLE-BOX
          SIZE 33.72 BY 1
          FONT 10
     RP.Log6 AT ROW 12.8 COL 11.29
          LABEL "Show only when account balance is non-zero at end of period"
          VIEW-AS TOGGLE-BOX
          SIZE 45.72 BY 1
          FONT 10
     RP.Log7 AT ROW 13.8 COL 11.29
          LABEL "Balances only"
          VIEW-AS TOGGLE-BOX
          SIZE 12.57 BY 1
          FONT 10
     RP.Log8 AT ROW 14.8 COL 11.29
          LABEL "Exclude Year End Transactions"
          VIEW-AS TOGGLE-BOX
          SIZE 24.57 BY 1
          FONT 10
.
/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME F-Main
     tgl_MonthTotals AT ROW 12.8 COL 61
     RP.Log9 AT ROW 13.8 COL 61 HELP
          ""
          LABEL "Hide related records"
          VIEW-AS TOGGLE-BOX
          SIZE 17.14 BY 1
     tgl_NoOpeningBalances AT ROW 14.8 COL 61
     RP.Char3 AT ROW 16.2 COL 10.72 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "by Company, then by Account", "L":U,
"by Account, then by Company", "A":U
          SIZE 24.57 BY 1.6
          FONT 10
     RP.Log5 AT ROW 18 COL 10.72
          LABEL "Export to"
          VIEW-AS TOGGLE-BOX
          SIZE 9.14 BY 1
          FONT 10
     RP.Char2 AT ROW 18 COL 17.86 COLON-ALIGNED NO-LABEL FORMAT "X(120)"
          VIEW-AS FILL-IN 
          SIZE 62.86 BY 1
     btn_Browse AT ROW 18 COL 83.29
     RP.Log2 AT ROW 19.2 COL 10.72 HELP
          ""
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 11.72 BY .8
          FONT 10
     Btn_OK AT ROW 19.2 COL 77.57
     RECT-1 AT ROW 1 COL 1
     RECT-4 AT ROW 4.2 COL 1.57
     RECT-5 AT ROW 1.2 COL 1.57
     RECT-6 AT ROW 8.2 COL 1.57
     "Sequence" VIEW-AS TEXT
          SIZE 7.43 BY .8 AT ROW 16.2 COL 2.72
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 21.3
         WIDTH              = 93.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit Custom                                       */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR RADIO-SET RP.Char3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR RADIO-SET RP.Char4 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR COMBO-BOX RP.Char5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR COMBO-BOX RP.Char6 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Date1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN RP.Dec1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Dec2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_account1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_account2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_comp1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_comp2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log6 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log7 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log8 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log9 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Browse V-table-Win
ON CHOOSE OF btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  DISABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char4 V-table-Win
ON VALUE-CHANGED OF RP.Char4 IN FRAME F-Main /* Char4 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Dec1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Dec1 V-table-Win
ON LEAVE OF RP.Dec1 IN FRAME F-Main /* Account */
DO:
  {inc/selcde/cdcoa.i "fil_account1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Dec2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Dec2 V-table-Win
ON LEAVE OF RP.Dec2 IN FRAME F-Main /* to */
DO:
  {inc/selcde/cdcoa.i "fil_account2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_account1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U1 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa1.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U2 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa2.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U3 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa3.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_account2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U1 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa1.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U2 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa2.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U3 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa3.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_comp1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U1 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U2 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U3 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_comp2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U1 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U2 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U3 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Company */
DO:
  {inc/selcde/cdcmp.i "fil_comp1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* To */
DO:
  {inc/selcde/cdcmp.i "fil_comp2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log3 V-table-Win
ON VALUE-CHANGED OF RP.Log3 IN FRAME F-Main /* Only where Transaction Date greater than */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Only where batch no. greater or equal to */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log5 V-table-Win
ON VALUE-CHANGED OF RP.Log5 IN FRAME F-Main /* Export to */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose: As its name!
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:
    CASE INPUT RP.Char1:   /* Company: single or range */
      WHEN "1" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_comp2:HANDLE), 'hidden = yes' ).
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_comp1:HANDLE), 'hidden = no' ).
        HIDE RP.Int2 fil_comp2 RP.Char6 .
        VIEW RP.Int1 fil_comp1 .
      END.
      WHEN "R" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_comp1:HANDLE), 'hidden = no' ).
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_comp2:HANDLE), 'hidden = no' ).
        VIEW RP.Int1 fil_comp1 RP.Int2 fil_comp2 .
        HIDE RP.Char6 .
      END.
      WHEN "L" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_comp1:HANDLE), 'hidden = yes' ).
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING(fil_comp2:HANDLE), 'hidden = yes' ).
        HIDE RP.Int1 fil_comp1 RP.Int2 fil_comp2 .
        VIEW RP.Char6 .
      END.
    END CASE.

    CASE INPUT RP.Char4:    /* Account: Single, range or group*/
      WHEN "1" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account1:HANDLE), 'hidden = no' ).
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account2:HANDLE), 'hidden = yes' ).
        VIEW RP.Dec1 fil_account1 .
        HIDE RP.Dec2 fil_account2 RP.Log1 RP.Char5 fil_AccountLike .
      END.
      WHEN "R" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account1:HANDLE), 'hidden = no' ).
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account2:HANDLE), 'hidden = no' ).
        VIEW RP.Dec1 fil_account1 RP.Dec2 fil_account2 RP.Log1 .
        HIDE RP.Char5 fil_AccountLike .
      END.
      WHEN "G" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account1:HANDLE), 'hidden = yes' ).
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account2:HANDLE), 'hidden = yes' ).
        VIEW RP.Log1 RP.Char5 .
        HIDE RP.Dec1 RP.Dec2 fil_account1 fil_account2 fil_AccountLike .
      END.
      WHEN "L" THEN DO:
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account1:HANDLE), 'hidden = no' ).
        RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_account2:HANDLE), 'hidden = no' ).
        VIEW fil_AccountLike RP.Log1 RP.Dec1 fil_account1 RP.Dec2 fil_account2 RP.Log1 .
        HIDE RP.Char5 .
      END.
    END CASE.

    /* only after date... */
    IF INPUT RP.Log3 THEN
      VIEW RP.Date1.
    ELSE
      HIDE RP.Date1.

    /* only after batch... */
    IF INPUT RP.Log4 THEN
      VIEW RP.Int5.
    ELSE
      HIDE RP.Int5.

    /* Export to... */
    IF INPUT RP.Log5 THEN
      VIEW RP.Char2 btn_Browse .
    ELSE
      HIDE RP.Char2 btn_Browse .

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/
DEF VAR month1-list AS CHAR INITIAL "" NO-UNDO.
DEF VAR month2-list AS CHAR INITIAL "" NO-UNDO.
DEF VAR delim AS CHAR INITIAL "|" NO-UNDO.

  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  FIND RP WHERE RP.ReportID = "tr-l"
                        AND RP.UserName = user-name
                        NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:
    CREATE RP.
    ASSIGN
      RP.ReportID = "tr-l"
      RP.UserName = user-name
    .
  END.

  /* Initialise the month list for the combos */
  FOR EACH Month NO-LOCK:
    month1-list = month1-list + delim + STRING( Month.StartDate, "99/99/9999" ).
    month2-list = month2-list + delim + STRING( Month.EndDate, "99/99/9999" ).
  END.
  month1-list = SUBSTRING( month1-list, 2).   /* trim initial delimiter */
  month2-list = SUBSTRING( month2-list, 2).   /* trim initial delimiter */
  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN
      cb-month1:DELIMITER = delim
      cb-month2:DELIMITER = delim
      cb-month1:LIST-ITEMS = month1-list
      cb-month2:LIST-ITEMS = month2-list
    .
  END.

  /* Initialise the account group list */
  FOR EACH AccountGroup NO-LOCK BY AccountGroup.SequenceCode:
    group-list = group-list + delim
               + STRING(AccountGroup.AccountGroupCode,"X(7)") + '- '
               + AccountGroup.Name.
  END.
  group-list = SUBSTRING( group-list, 2).
  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN
      RP.Char5:DELIMITER = delim
      RP.Char5:LIST-ITEMS = group-list
    .
  END.

  /* Initialise the consolidation list for the combos */
DEF VAR con-list AS CHAR NO-UNDO.
  FOR EACH ConsolidationList NO-LOCK:
    con-list = con-list + delim + STRING( ConsolidationList.Name, "X(5)") + "- " + ConsolidationList.Description.
  END.
  con-list = SUBSTRING( con-list, 2).   /* trim initial delimiter */
  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN  RP.Char6:DELIMITER = delim
            RP.Char6:LIST-ITEMS = con-list .
  END.


  /* Set to initially saved values */
  FIND Month WHERE Month.MonthCode = RP.Int3 NO-LOCK NO-ERROR.
  IF AVAILABLE(Month) THEN cb-month1 = STRING( Month.StartDate, "99/99/9999" ).
  FIND Month WHERE Month.MonthCode = RP.Int4 NO-LOCK NO-ERROR.
  IF AVAILABLE(Month) THEN cb-month2 = STRING( Month.EndDate, "99/99/9999" ).

  DISPLAY cb-month1 cb-month2 RP.Char5 WITH FRAME {&FRAME-NAME} IN WINDOW {&WINDOW-NAME}.

  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  RUN enable-appropriate-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program.
------------------------------------------------------------------------------*/

  DO TRANSACTION:
    RUN dispatch IN THIS-PROCEDURE ('update-record':U).
    ASSIGN FRAME {&FRAME-NAME} fil_AccountLike tgl_MonthTotals tgl_NoOpeningBalances .

    FIND CURRENT RP EXCLUSIVE-LOCK.
    FIND Month WHERE Month.StartDate = DATE( cb-month1:SCREEN-VALUE IN FRAME {&FRAME-NAME}) NO-LOCK NO-ERROR.
    IF AVAILABLE(Month) THEN RP.Int3 = Month.MonthCode.

    FIND Month WHERE Month.EndDate = DATE( cb-month2:SCREEN-VALUE IN FRAME {&FRAME-NAME}) NO-LOCK NO-ERROR.
    IF AVAILABLE(Month) THEN RP.Int4 = Month.MonthCode.
    FIND CURRENT RP NO-LOCK.
  END.

DEF VAR report-options AS CHAR NO-UNDO.
DEF VAR e1 AS INTEGER NO-UNDO.
DEF VAR e2 AS INTEGER NO-UNDO.
DEF VAR show-all AS LOGICAL NO-UNDO.
DEF VAR a1 AS DECIMAL NO-UNDO.
DEF VAR a2 AS DECIMAL NO-UNDO.
DEF VAR after AS DATE NO-UNDO.
DEF VAR earliest-batch AS INT NO-UNDO.
DEF VAR export-fname AS CHAR INITIAL ? NO-UNDO.
DEF VAR group-name AS CHAR INITIAL ? NO-UNDO.


  ASSIGN
             /* ledger opts, account opts, sort opts, zero opt */
    report-options =
      RP.Char1 + RP.Char4 + RP.Char3 + (IF RP.Log6 THEN "X" ELSE "I") +
      (IF RP.Log7 THEN "S" ELSE "V") + (IF RP.Log8 THEN "Y" ELSE "N")
    e1 = RP.Int1        e2 = (IF RP.Char1 = "1" THEN RP.Int1 ELSE RP.Int2 )
    a1 = RP.Dec1        a2 = (IF RP.Char4 = "1" THEN RP.Dec1 ELSE RP.Dec2 )
    show-all = ( RP.Log1 OR (RP.Char4 = "1"))
    after = (IF RP.Log3 THEN RP.Date1 ELSE DATE(1,1,1) /* less than anything! */ )
    earliest-batch = (IF RP.Log4 THEN RP.Int5 ELSE 0 )
    export-fname =  (IF RP.Log5 THEN RP.Char2 ELSE "" )
    group-name = (IF RP.Char4 = "G" THEN TRIM( SUBSTRING(RP.Char5, 1, 6)) ELSE "" )
  .

  report-options = report-options + "," + STRING( e1 ) + "," + STRING( e2 ) + ","
                 + STRING( RP.Int3 ) + "," + STRING( RP.Int4 ) + ","
                 + STRING( show-all, "Yes/No" ) + ","
                 + STRING( a1 ) + "," + STRING( a2 ) + ","
                 + STRING( RP.Log2, "Yes/No" ) + ","
                 + STRING( after ) + ","
                 + STRING( earliest-batch ) + ","
                 + export-fname + ","
                 + group-name
                 + (IF RP.Char1 = "L" THEN "~nConsolidationList," + TRIM(SUBSTRING(RP.Char6, 1, 4)) ELSE "" )
                 + (IF RP.Char4 = "L":U THEN "~nAccountsLike," + fil_AccountLike ELSE "")
                 + "~nSortBy," + RP.Char3
                 + (IF RP.Log9 THEN "~nNoRelations" ELSE "")
                 + (IF RP.Log6 THEN "~nNonZeroOnly" ELSE "")
                 + (IF RP.Log7 THEN "~nBalancesOnly" ELSE "")
                 + (IF RP.Log8 THEN "~nExcludeYearEnd" ELSE "")
                 + (IF tgl_MonthTotals THEN "~nMonthTotals" ELSE "")
                 + (IF tgl_NoOpeningBalances THEN "~nNoOpeningBalances" ELSE "").

{inc/bq-do.i "process/report/tr-l.p" "report-options" "(NOT RP.Log2 AND (export-fname = ''))"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char2 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated values" "*.CSV"
        SAVE-AS ASK-OVERWRITE CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV"
        INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME UPDATE select-ok.

  IF select-ok THEN DO:
    IF SUBSTRING( file-name, R-INDEX( file-name, ".") ) <> ".CSV" THEN DO:
      IF (LENGTH( file-name ) - R-INDEX( file-name, "." )) < 4 THEN
        file-name = SUBSTRING( file-name, 1, R-INDEX( file-name, ".")) + "CSV" .
    END.
    RP.Char2:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

