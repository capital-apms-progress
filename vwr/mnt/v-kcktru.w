&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:         v-kcktru.w
  Description:  Kick of transaction update
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEFINE TEMP-TABLE Update-Month NO-UNDO
      FIELD TrnCount AS INTEGER FORMAT ">>,>>9" COLUMN-LABEL "# Lines"
      FIELD Closed AS CHAR FORMAT "X(6)" COLUMN-LABEL "Status"
      FIELD MonthName AS CHAR FORMAT "X(3)" COLUMN-LABEL "Mth"
      FIELD Year AS INT FORMAT "9999" COLUMN-LABEL "Year"
      FIELD MonthCode LIKE Month.MonthCode
      FIELD ActualMonth AS CHAR FORMAT "X(8)" COLUMN-LABEL "Post to"
      FIELD MonthValue AS DEC FORMAT "->>,>>>,>>9.99" COLUMN-LABEL "Month Total"
      INDEX XPKUpdate-Month IS UNIQUE PRIMARY MonthCode Closed  .

{inc/ofc-this.i}
{inc/ofc-set-l.i "Fail-Unbalanced-Update" "fail-unbalanced-update"}

DEF VAR force-month AS INT NO-UNDO.
RUN pre-initialize.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-UpdateMonths

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES NewBatch
&Scoped-define FIRST-EXTERNAL-TABLE NewBatch


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR NewBatch.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Update-Month

/* Definitions for BROWSE br-UpdateMonths                               */
&Scoped-define FIELDS-IN-QUERY-br-UpdateMonths MonthName Year Closed TrnCount ActualMonth MonthValue   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-UpdateMonths   
&Scoped-define SELF-NAME br-UpdateMonths
&Scoped-define QUERY-STRING-br-UpdateMonths FOR EACH Update-Month             BY Update-Month.MonthCode
&Scoped-define OPEN-QUERY-br-UpdateMonths OPEN QUERY {&SELF-NAME} FOR EACH Update-Month             BY Update-Month.MonthCode .
&Scoped-define TABLES-IN-QUERY-br-UpdateMonths Update-Month
&Scoped-define FIRST-TABLE-IN-QUERY-br-UpdateMonths Update-Month


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-UpdateMonths}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br-UpdateMonths tgl_ReverseNextPeriod ~
btn_update cmb_ForceMonth tgl_ForceMonth RECT-13 
&Scoped-Define DISPLAYED-FIELDS NewBatch.Description NewBatch.BatchType ~
NewBatch.DocumentCount NewBatch.Total 
&Scoped-define DISPLAYED-TABLES NewBatch
&Scoped-define FIRST-DISPLAYED-TABLE NewBatch
&Scoped-Define DISPLAYED-OBJECTS fil_Operator tgl_ReverseNextPeriod ~
cmb_ForceMonth tgl_ForceMonth 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_update DEFAULT 
     LABEL "&Update" 
     SIZE 16 BY 1.4
     FONT 9.

DEFINE VARIABLE cmb_ForceMonth AS CHARACTER FORMAT "X(256)":U 
     LABEL "month" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 17.43 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Operator AS CHARACTER FORMAT "X(256)":U 
     LABEL "Operator" 
     VIEW-AS FILL-IN 
     SIZE 49.43 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE RECTANGLE RECT-13
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 63.72 BY 14.3.

DEFINE VARIABLE tgl_ForceMonth AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 2.29 BY .8 NO-UNDO.

DEFINE VARIABLE tgl_ReverseNextPeriod AS LOGICAL INITIAL no 
     LABEL "Reversing Journal" 
     VIEW-AS TOGGLE-BOX
     SIZE 16.57 BY 1.3 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-UpdateMonths FOR 
      Update-Month SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-UpdateMonths
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-UpdateMonths V-table-Win _FREEFORM
  QUERY br-UpdateMonths DISPLAY
      MonthName
        Year
        Closed 
        TrnCount
      ActualMonth
      MonthValue
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 49.43 BY 6.9
         FONT 8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     NewBatch.Description AT ROW 1.4 COL 12.72 COLON-ALIGNED
          LABEL "Description"
          VIEW-AS FILL-IN 
          SIZE 49.43 BY 1
          BGCOLOR 16 FONT 11
     fil_Operator AT ROW 3 COL 12.72 COLON-ALIGNED
     NewBatch.BatchType AT ROW 4.2 COL 12.72 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1
          BGCOLOR 16 
     NewBatch.DocumentCount AT ROW 4.2 COL 29.86 COLON-ALIGNED
          LABEL "Documents"
          VIEW-AS FILL-IN 
          SIZE 10.29 BY .9
          BGCOLOR 16 
     NewBatch.Total AT ROW 4.2 COL 48.72 COLON-ALIGNED FORMAT "-ZZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 13.43 BY .9
          BGCOLOR 16 
     br-UpdateMonths AT ROW 5.6 COL 14.72
     tgl_ReverseNextPeriod AT ROW 12.5 COL 15
     btn_update AT ROW 13.7 COL 48.14
     cmb_ForceMonth AT ROW 13.9 COL 17.43
     tgl_ForceMonth AT ROW 14 COL 15
     RECT-13 AT ROW 1 COL 1
     "Force posting to:" VIEW-AS TEXT
          SIZE 11.72 BY 1 AT ROW 13.9 COL 3
     "Update Months:" VIEW-AS TEXT
          SIZE 11.43 BY 1 AT ROW 5.6 COL 3.29
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_update.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.NewBatch
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 16.95
         WIDTH              = 72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-UpdateMonths Total F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN NewBatch.BatchType IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cmb_ForceMonth IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN NewBatch.Description IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN NewBatch.DocumentCount IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN fil_Operator IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN NewBatch.Total IN FRAME F-Main
   NO-ENABLE EXP-FORMAT                                                 */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-UpdateMonths
/* Query rebuild information for BROWSE br-UpdateMonths
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH Update-Month
            BY Update-Month.MonthCode .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-UpdateMonths */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_update
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_update V-table-Win
ON CHOOSE OF btn_update IN FRAME F-Main /* Update */
DO:
  {&SELF-NAME}:SENSITIVE = No.
  RUN update-batch.
  {&SELF-NAME}:SENSITIVE = Yes.
  RUN dispatch ( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_ForceMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ForceMonth V-table-Win
ON U1 OF cmb_ForceMonth IN FRAME F-Main /* month */
DO:
  {inc/selcmb/scmthe1.i "?" "force-month"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_ForceMonth V-table-Win
ON U2 OF cmb_ForceMonth IN FRAME F-Main /* month */
DO:
  {inc/selcmb/scmthe2.i "?" "force-month"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_ForceMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_ForceMonth V-table-Win
ON VALUE-CHANGED OF tgl_ForceMonth IN FRAME F-Main
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-UpdateMonths
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "NewBatch"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "NewBatch"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT tgl_ForceMonth THEN
    ENABLE cmb_ForceMonth.
  ELSE
    DISABLE cmb_ForceMonth.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE post-row-available V-table-Win 
PROCEDURE post-row-available :
/*------------------------------------------------------------------------------
  Purpose:  Set the local batch code to the current row available
------------------------------------------------------------------------------*/
DEF VAR month-status AS CHAR NO-UNDO.

DEF BUFFER xx-Month FOR Month.

  IF NOT AVAILABLE(NewBatch) THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:
  APPLY 'U1':U TO cmb_ForceMonth.
  RUN enable-appropriate-fields.
END.


  FIND Person WHERE Person.PersonCode = NewBatch.PersonCode NO-LOCK NO-ERROR.
  IF AVAILABLE(Person) AND Person.PersonCode > 0 THEN DO:
    fil_Operator = Person.FirstName + " " + Person.LastName.
    DISPLAY fil_Operator
                 WITH FRAME {&FRAME-NAME} IN WINDOW {&WINDOW-NAME}.
  END.

  FOR EACH NewAcctTrans OF NewBatch NO-LOCK:
    IF NewAcctTrans.Date = ? OR NewAcctTrans.Date < DATE(1,1,1990) THEN DO:
      MESSAGE "Transactions in Batch have bad Date field!"
           VIEW-AS ALERT-BOX ERROR TITLE "Bad Dates in Batch".
      RUN notify( 'exit':U ).
      RETURN.
    END.
    FIND Month WHERE Month.MonthCode = NewAccttrans.MonthCode NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(Month) THEN
      FIND LAST Month WHERE Month.StartDate <= NewAccttrans.Date NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(Month) THEN
      FIND FIRST Month WHERE Month.EndDate >= NewAccttrans.Date NO-LOCK NO-ERROR.
    month-status = (IF Month.MonthStatus = "CLOS" THEN (IF Month.MonthCode = NewAcctTrans.MonthCode THEN "Forced" ELSE "Closed") ELSE "").

    FIND FIRST Update-Month WHERE Update-Month.MonthCode = Month.MonthCode
                            AND Update-Month.Closed = month-status NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(Update-Month) THEN DO:
      CREATE Update-Month.
      ASSIGN
        Update-Month.MonthCode = Month.MonthCode
        Update-Month.MonthName = Month.MonthName
        Update-Month.Year      = YEAR(Month.EndDate)
        Update-Month.Closed    = month-status
        Update-Month.TrnCount  = 0 .

      IF month-status = "Closed" THEN DO:
        FIND FIRST xx-Month WHERE xx-Month.MonthCode > Month.MonthCode 
                               AND xx-Month.MonthStatus = "OPEN" NO-LOCK NO-ERROR.
        IF NOT AVAILABLE(xx-Month) THEN
          FIND LAST xx-Month WHERE xx-Month.MonthCode < Month.MonthCode 
                                AND xx-Month.MonthStatus = "OPEN" NO-LOCK NO-ERROR.
        IF NOT AVAILABLE(xx-Month) THEN
          Update-Month.ActualMonth = "Not Open".
        ELSE
          Update-Month.ActualMonth = STRING(MONTH(xx-Month.StartDate), "99") + "/"
                                   + STRING(YEAR(xx-Month.StartDate), "9999").
      END.
    END.
    Update-Month.TrnCount = Update-Month.TrnCount + 1.
    Update-Month.MonthValue = Update-Month.MonthValue + NewAcctTrans.Amount.
  END.
  OPEN QUERY br-UpdateMonths FOR EACH Update-Month BY Update-Month.MonthCode .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize V-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO:
  FIND LAST Month WHERE Month.StartDate <= TODAY NO-LOCK NO-ERROR.
  IF AVAILABLE(Month) THEN force-month = Month.MonthCode.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "NewBatch"}
  {src/adm/template/snd-list.i "Update-Month"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-batch V-table-Win 
PROCEDURE update-batch :
/*------------------------------------------------------------------------------
  Purpose:  Run the batch update program
------------------------------------------------------------------------------*/
DEF VAR update-options AS CHAR NO-UNDO.

  RUN verify-update.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:
  IF INPUT tgl_ForceMonth THEN
    APPLY 'U2':U TO cmb_ForceMonth.  /* Assign the values */

  IF NewBatch.BatchType BEGINS "PART" THEN
    /* don't muck around with partly posted ones - just let them be posted again! */.
  ELSE DO TRANSACTION:
    FIND CURRENT NewBatch EXCLUSIVE-LOCK.
    NewBatch.BatchType = "UPDT".
    FIND CURRENT NewBatch NO-LOCK.
  END.

  update-options = "DeleteAfterPosting"
                 + "~nBatchCode," + STRING(NewBatch.BatchCode)
                 + (IF INPUT tgl_ReverseNextPeriod THEN
                                                    "~nReverseNextPeriod" ELSE "")
                 + (IF INPUT tgl_ForceMonth THEN
                        ("~nForceMonth," + STRING(force-month))
                    ELSE "").
END.
  RUN make-bq-entry IN sys-mgr( "process/tru.p", update-options, ?, ?).

  DO TRANSACTION:
    FIND CURRENT NewBatch EXCLUSIVE-LOCK NO-ERROR.
    IF AVAILABLE(NewBatch) AND NewBatch.BatchType = "UPDT" THEN
      NewBatch.BatchType = "NORM".
    FIND CURRENT NewBatch NO-LOCK NO-ERROR.
  END.

  RUN notify( 'open-query,record-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-update V-table-Win 
PROCEDURE verify-update :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR mth-valid AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR warn-result AS LOGICAL INITIAL no NO-UNDO.

  IF NOT AVAILABLE(NewBatch) THEN DO:
    MESSAGE "There is no batch selected for update!"      SKIP
            "Please choose a valid batch"
      VIEW-AS ALERT-BOX ERROR TITLE "Error Running Update".
    RETURN "FAIL".
  END.

  IF NewBatch.BatchType = "ACCR" THEN DO:
    MESSAGE "Use the Batch Tool to create a 'NORM' batch and update that"
      VIEW-AS ALERT-BOX ERROR TITLE "Accruals batches cannot update directly".
    RETURN "FAIL".
  END.

  IF CAN-FIND( FIRST Update-Month WHERE Update-Month.Closed = "Closed" ) THEN DO:
    MESSAGE "Some transactions in this batch relate to closed" SKIP
            "months.  You may want to cancel this batch update" SKIP
            "and open those months for posting!" SKIP(2)
            "Or do you want to update the batch anyway, with those" SKIP
            "transactions posting to the nearest open months?"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK-CANCEL
            TITLE "Warning: Transactions relate to closed months"
            UPDATE warn-result.
    IF NOT warn-result THEN RETURN "FAIL".
  END.

  IF NewBatch.Total <> 0 THEN DO:
  DEF VAR btot AS DECIMAL NO-UNDO.

    FOR EACH NewDocument OF NewBatch NO-LOCK,
            EACH NewAcctTrans OF NewDocument NO-LOCK:
      btot = btot + ((IF NewDocument.DocumentType = "RCPT" THEN -1 ELSE 1) * NewAcctTrans.Amount).
      IF NOT CAN-FIND( Month WHERE Month.StartDate >= NewAcctTrans.Date
                               AND Month.EndDate <= NewAcctTrans.Date
                               AND Month.MonthStatus = "OPEN")
      THEN mth-valid = No.
    END.

    IF btot <> 0 AND fail-unbalanced-update AND NOT(NewBatch.BatchType BEGINS "PART") THEN DO:
      MESSAGE "Batch total does not equal zero!"
              VIEW-AS ALERT-BOX ERROR
              TITLE "Error: Non-zero batch total!".
      RETURN "FAIL".
    END.
    ELSE IF btot <> 0 THEN DO:
      warn-result = no.
      MESSAGE "Batch total does not equal zero!"      SKIP
              "Are you sure you want to update it?"
              VIEW-AS ALERT-BOX WARNING BUTTONS OK-CANCEL
              TITLE "Warning: Non-zero batch total!"
              UPDATE warn-result.
      IF NOT warn-result THEN RETURN "FAIL".
    END.

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

