&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR loc_Creditor AS CHAR FORMAT "X(27)" LABEL "Creditor" NO-UNDO.
DEF VAR loc_Total       AS DEC  FORMAT "->>,>>>,>>9.99" LABEL "Total" NO-UNDO.
DEF VAR user-name AS CHAR NO-UNDO.

DEF VAR break-1 AS CHAR FORMAT "X" LABEL "" INIT "" NO-UNDO.
DEF VAR break-2 AS CHAR FORMAT "X" LABEL "" INIT "" NO-UNDO.
DEF VAR break-3 AS CHAR FORMAT "X" LABEL "" INIT "" NO-UNDO.

DEF VAR h-msg        AS HANDLE NO-UNDO.

DEF VAR gst-applies AS LOGI NO-UNDO.
DEF VAR gst-in-ecode           AS INT NO-UNDO.
DEF VAR sundry-creditors-ecode AS INT NO-UNDO.

DEF NEW SHARED TEMP-TABLE company-gst 
         FIELD company_id LIKE Company.CompanyCode
         FIELD tax_amount  LIKE NewAcctTrans.Amount 
         INDEX company_id IS PRIMARY company_id ASCENDING.

{inc/ofc-this.i}
{inc/ofc-acct.i "CREDITORS" "sundry-creditors"}
sundry-creditors-ecode = OfficeControlAccount.EntityCode.
{inc/ofc-acct.i "GST-IN" "gst-in"}
gst-applies = AVAILABLE OfficeControlAccount.
gst-in-ecode = OfficeControlAccount.EntityCode.
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}

{inc/ofc-set-l.i "Voucher-Trans-Due-Date" "use-due-dates"}
{inc/ofc-set.i "Voucher-Transaction-Date" "voucher-transaction-date"}
IF NOT AVAILABLE(OfficeSetting) THEN voucher-transaction-date = "Invoice/Due-10".

{inc/ofc-set-l.i "Voucher-LinesIncludeGST" "lines-include-gst"}
IF NOT AVAILABLE(OfficeSetting) THEN lines-include-gst = No.

{inc/ofc-set-l.i "GST-Multi-Company"  "GST-Multi-Company" }
{inc/ofc-set.i "GST-Rate"  "gst-rate" }
  
/* Each creditor has attributed balances for each ledger (c.f. George Group) */
{inc/ofc-set-l.i "Multi-Ledger-Creditors" "multi-ledger-creditors"}


{inc/entity.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME BROWSE-3

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Voucher Creditor VoucherLine

/* Definitions for BROWSE BROWSE-3                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-3 Voucher.VoucherSeq ~
break-1 @ break-1 Voucher.Date Voucher.CreditorCode Creditor.Name ~
Voucher.Description break-2 @ break-2 Voucher.PaymentStyle ~
break-3 @ break-3 Voucher.GoodsValue + Voucher.TaxValue @ loc_Total 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-3 
&Scoped-define QUERY-STRING-BROWSE-3 FOR EACH Voucher ~
      WHERE Voucher.VoucherStatus = "U" ~
 AND (MyVouchers = NO OR Voucher.LastModifiedUser = user-name)  NO-LOCK, ~
      EACH Creditor OF Voucher NO-LOCK, ~
      FIRST VoucherLine OF Voucher NO-LOCK ~
    BY Voucher.VoucherSeq DESCENDING
&Scoped-define OPEN-QUERY-BROWSE-3 OPEN QUERY BROWSE-3 FOR EACH Voucher ~
      WHERE Voucher.VoucherStatus = "U" ~
 AND (MyVouchers = NO OR Voucher.LastModifiedUser = user-name)  NO-LOCK, ~
      EACH Creditor OF Voucher NO-LOCK, ~
      FIRST VoucherLine OF Voucher NO-LOCK ~
    BY Voucher.VoucherSeq DESCENDING.
&Scoped-define TABLES-IN-QUERY-BROWSE-3 Voucher Creditor VoucherLine
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-3 Voucher
&Scoped-define SECOND-TABLE-IN-QUERY-BROWSE-3 Creditor
&Scoped-define THIRD-TABLE-IN-QUERY-BROWSE-3 VoucherLine


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-BROWSE-3}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS BROWSE-3 btn_SelectAll btn_cancel MyVouchers ~
RECT-21 
&Scoped-Define DISPLAYED-OBJECTS MyVouchers 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-trn-date V-table-Win 
FUNCTION get-trn-date RETURNS DATE
  ( OUTPUT m-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validate-creditor-ledger V-table-Win 
FUNCTION validate-creditor-ledger RETURNS INTEGER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_approve 
     LABEL "&Approve" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_SelectAll 
     LABEL "&Select All" 
     SIZE 11.43 BY 1.05.

DEFINE RECTANGLE RECT-21
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 105.72 BY 17.

DEFINE VARIABLE MyVouchers AS LOGICAL INITIAL no 
     LABEL "Show My Vouchers Only" 
     VIEW-AS TOGGLE-BOX
     SIZE 20 BY .8 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-3 FOR 
      Voucher, 
      Creditor, 
      VoucherLine SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-3 V-table-Win _STRUCTURED
  QUERY BROWSE-3 NO-LOCK DISPLAY
      Voucher.VoucherSeq COLUMN-LABEL "Voucher" FORMAT ">>>>>>9":U
      break-1 @ break-1 FORMAT "X":U
      Voucher.Date COLUMN-LABEL "Invoice Date" FORMAT "99/99/9999":U
      Voucher.CreditorCode COLUMN-LABEL "Creditor" FORMAT "99999":U
      Creditor.Name FORMAT "X(30)":U
      Voucher.Description FORMAT "X(50)":U
      break-2 @ break-2 FORMAT "X":U
      Voucher.PaymentStyle FORMAT "X(4)":U
      break-3 @ break-3 FORMAT "X":U
      Voucher.GoodsValue + Voucher.TaxValue @ loc_Total
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE SIZE 104.57 BY 14.9
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     BROWSE-3 AT ROW 2.2 COL 1.57
     btn_SelectAll AT ROW 17.2 COL 1.57
     btn_approve AT ROW 17.2 COL 85
     btn_cancel AT ROW 17.2 COL 96.43
     MyVouchers AT ROW 17.25 COL 15
     RECT-21 AT ROW 1.4 COL 1
     "Voucher Approval" VIEW-AS TEXT
          SIZE 20.57 BY 1 AT ROW 1 COL 2.72
          FONT 13
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.8
         WIDTH              = 108.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB BROWSE-3 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON btn_approve IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-3
/* Query rebuild information for BROWSE BROWSE-3
     _TblList          = "ttpl.Voucher,ttpl.Creditor OF ttpl.Voucher,ttpl.VoucherLine OF ttpl.Voucher"
     _Options          = "NO-LOCK"
     _TblOptList       = ",, FIRST"
     _OrdList          = "ttpl.Voucher.VoucherSeq|no"
     _Where[1]         = "Voucher.VoucherStatus = ""U""
 AND (MyVouchers = NO OR Voucher.LastModifiedUser = user-name) "
     _JoinCode[2]      = "Invoice.EntityType = 'T'
  AND
Invoice.EntityCode = Tenant.TenantCode"
     _FldNameList[1]   > ttpl.Voucher.VoucherSeq
"Voucher.VoucherSeq" "Voucher" ">>>>>>9" "integer" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > "_<CALC>"
"break-1 @ break-1" ? "X" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[3]   > ttpl.Voucher.Date
"Voucher.Date" "Invoice Date" ? "date" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[4]   > ttpl.Voucher.CreditorCode
"Voucher.CreditorCode" "Creditor" ? "integer" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[5]   > ttpl.Creditor.Name
"Creditor.Name" ? "X(30)" "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[6]   = ttpl.Voucher.Description
     _FldNameList[7]   > "_<CALC>"
"break-2 @ break-2" ? "X" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[8]   = ttpl.Voucher.PaymentStyle
     _FldNameList[9]   > "_<CALC>"
"break-3 @ break-3" ? "X" ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[10]   > "_<CALC>"
"Voucher.GoodsValue + Voucher.TaxValue @ loc_Total" ? ? ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _Query            is OPENED
*/  /* BROWSE BROWSE-3 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME BROWSE-3
&Scoped-define SELF-NAME BROWSE-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-3 V-table-Win
ON ANY-PRINTABLE OF BROWSE-3 IN FRAME F-Main
DO:
  RUN toggle-current-row.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-3 V-table-Win
ON MOUSE-SELECT-DOWN OF BROWSE-3 IN FRAME F-Main
DO:
  APPLY 'MOUSE-EXTEND-DOWN' TO {&SELF-NAME}.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-3 V-table-Win
ON ROW-DISPLAY OF BROWSE-3 IN FRAME F-Main
DO:
DEF VAR row-colour AS INT NO-UNDO   INITIAL ?.
DEF VAR amt-colour AS INT NO-UNDO   INITIAL ?.

  IF Voucher.PaymentStyle = "CARD" THEN row-colour = 9.
  IF Voucher.GoodsValue + Voucher.TaxValue < 0 THEN amt-colour = 12.

  ASSIGN    loc_Total:FGCOLOR IN BROWSE {&BROWSE-NAME} = amt-colour
            Voucher.VoucherSeq:FGCOLOR = row-colour
            Voucher.Date:FGCOLOR = row-colour
            Voucher.CreditorCode:FGCOLOR = row-colour
            Creditor.Name:FGCOLOR = row-colour
            Voucher.Description:FGCOLOR = row-colour
            Voucher.PaymentStyle:FGCOLOR = row-colour .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-3 V-table-Win
ON VALUE-CHANGED OF BROWSE-3 IN FRAME F-Main
DO:
  ASSIGN btn_approve:SENSITIVE = SELF:NUM-SELECTED-ROWS <> 0.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_approve
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_approve V-table-Win
ON CHOOSE OF btn_approve IN FRAME F-Main /* Approve */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':u ).
  RUN win/w-prgmsg.w PERSISTENT SET h-msg.

  RUN approve-vouchers.

  IF VALID-HANDLE( h-msg ) THEN RUN dispatch IN h-msg ( 'destroy':U ).
  RUN notify( 'set-idle, CONTAINER-SOURCE':u ).

  IF RETURN-VALUE <> "FAIL" THEN RUN dispatch( 'exit':U ).
  ELSE DO:
    MESSAGE "There was an error approving the vouchers" SKIP(1)
            "Select different vouchers and try again," SKIP
            "or exit and fix the error."
            VIEW-AS ALERT-BOX ERROR TITLE "Voucher Approval Error".
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN notify( 'record-source,open-query':U ).
  RUN dispatch( 'exit':u ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_SelectAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_SelectAll V-table-Win
ON CHOOSE OF btn_SelectAll IN FRAME F-Main /* Select All */
DO:
  RUN select-all-vouchers.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME MyVouchers
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL MyVouchers V-table-Win
ON VALUE-CHANGED OF MyVouchers IN FRAME F-Main /* Show My Vouchers Only */
DO:
   ASSIGN MyVouchers.
  RUN open-vch-query.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE approve-vouchers V-table-Win 
PROCEDURE approve-vouchers :
/*------------------------------------------------------------------------------
  Purpose:     Approve this Voucher
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR i        AS INT  NO-UNDO.
DEF VAR creditor-ledger AS INT  NO-UNDO.
DEF VAR vch-list AS CHAR NO-UNDO.
DEF VAR vch-total AS DEC NO-UNDO.
DEF VAR mth-code LIKE Month.MonthCode NO-UNDO.
DEF VAR trn-date AS DATE NO-UNDO.
DEF VAR taxed-transactions AS LOGI NO-UNDO.
DEF VAR residual-tax AS DEC NO-UNDO.
DEF VAR document-total AS DEC NO-UNDO INITIAL 0.
DEF VAR gst-et AS CHAR NO-UNDO.
DEF VAR gst-ec AS INT NO-UNDO.

DEF BUFFER mypj FOR Project.

DO TRANSACTION ON ERROR UNDO, RETURN "FAIL":

  /* Create the batch in which to approve the Vouchers */
  CREATE NewBatch.
  ASSIGN
    NewBatch.BatchType   = 'NORM'
    NewBatch.Description = "Voucher Batch - " + STRING( TODAY, "99/99/9999" ).

  DO i = 1 TO {&BROWSE-NAME}:NUM-SELECTED-ROWS IN FRAME {&FRAME-NAME}:
    IF {&BROWSE-NAME}:FETCH-SELECTED-ROW(i) THEN.

    vch-total = 0.0 .
    FOR EACH VoucherLine OF Voucher NO-LOCK: vch-total = vch-total + VoucherLine.Amount. END.
    IF lines-include-gst THEN vch-total = vch-total - Voucher.TaxValue.

    IF vch-total <> Voucher.GoodsValue THEN DO:
      MESSAGE "Voucher" Voucher.VoucherSeq "allocations do not equal voucher." SKIP(1)
                TRIM( STRING( vch-total, "->>,>>>,>>9.99" ) ) + " vs " + 
                TRIM( STRING( Voucher.GoodsValue, "->>,>>>,>>9.99" ) )
          VIEW-AS ALERT-BOX ERROR TITLE "Incorrect Total, " +
                TRIM( STRING( vch-total, "->>,>>>,>>9.99" ) ) + " vs " + 
                TRIM( STRING( Voucher.GoodsValue, "->>,>>>,>>9.99" ) ) .
      NEXT.
    END.

    RUN update-details IN h-msg ( 'Approving Vouchers', ( i / {&BROWSE-NAME}:NUM-SELECTED-ROWS ) * 100 ).

    trn-date = get-trn-date( OUTPUT mth-code ).

    /* Initialise temp-table */
    FOR EACH company-gst EXCLUSIVE-LOCK:
          DELETE company-gst.
    END.

        
    document-total = 0.
    CREATE NewDocument.
    ASSIGN 
      NewDocument.BatchCode    = NewBatch.BatchCode
      NewDocument.DocumentType = "VCHR"
      NewDocument.Description  = Voucher.Description
      NewDocument.Reference    = "VCHR" + STRING( Voucher.VoucherSeq ).

    /* Create the account debit transactions */
    taxed-transactions = No.
    residual-tax = Voucher.TaxValue .
    
    IF multi-ledger-creditors THEN DO:
      creditor-ledger = validate-creditor-ledger().
      IF creditor-ledger = ? THEN UNDO, RETURN "FAIL".
    END.
        
    /* Create Transaction Lines */
    FOR EACH VoucherLine OF Voucher NO-LOCK:
      IF VoucherLine.TaxAmount <> 0 THEN taxed-transactions = Yes.

      CREATE NewAcctTrans.
      ASSIGN
        NewAcctTrans.BatchCode       = NewBatch.BatchCode
        NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
        NewAcctTrans.EntityType      = VoucherLine.EntityType
        NewAcctTrans.EntityCode      = VoucherLine.EntityCode
        NewAcctTrans.AccountCode     = VoucherLine.AccountCode
        NewAcctTrans.Amount          = VoucherLine.Amount - VoucherLine.TaxAmount
        NewAcctTrans.Date            = trn-date
        NewAcctTrans.MonthCode       = mth-code
        NewAcctTrans.Description     = VoucherLine.Description
        NewAcctTrans.Reference       = NewDocument.Reference
        document-total = document-total + NewAcctTrans.Amount.

      /* Store Companies proportional GST */
      IF GST-Multi-Company = YES AND 
          gst-applies AND 
          residual-tax <> 0 AND
          VoucherLine.EntityType <> 'T'  AND
          VoucherLine.EntityType <> 'C' 
         THEN RUN update-gst-table.

      IF VoucherLine.TaxAmount <> 0 THEN DO:
        gst-et = VoucherLine.EntityType.
        gst-ec = VoucherLine.EntityCode.
        IF gst-et = 'J' THEN DO:
          REPEAT WHILE gst-et <> "L":
            IF gst-et = "J" THEN DO:
              FIND mypj WHERE mypj.ProjectCode = gst-ec NO-LOCK.
              gst-et = mypj.EntityType.
              gst-ec = mypj.EntityCode.
            END.
            ELSE DO:
              IF gst-et = "P" THEN DO:
                FIND Property WHERE Property.PropertyCode = gst-ec NO-LOCK.
                gst-et = "L".
                gst-ec = Property.CompanyCode.
              END.
              ELSE DO:
                LEAVE.
              END.
            END. 
          END. /* Repeat */
        END. /* if */
        CREATE NewAcctTrans.
        ASSIGN NewAcctTrans.BatchCode       = NewBatch.BatchCode
               NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
               NewAcctTrans.EntityType      = gst-et
               NewAcctTrans.EntityCode      = gst-ec
               NewAcctTrans.AccountCode     = (IF INDEX( "TC", VoucherLine.EntityType) > 0 THEN VoucherLine.AccountCode ELSE gst-in)
               NewAcctTrans.Amount          = VoucherLine.TaxAmount
               NewAcctTrans.Date            = trn-date
               NewAcctTrans.MonthCode       = mth-code
               NewAcctTrans.Description     = "GST, " + VoucherLine.Description
               NewAcctTrans.Reference       = NewDocument.Reference
               document-total = document-total + NewAcctTrans.Amount.
        residual-tax = residual-tax - VoucherLine.TaxAmount .
      END.
    END. /* VoucherLine */

    /* Create the GST debit transaction */ 

    FIND Creditor OF Voucher NO-LOCK.

    IF gst-applies AND residual-tax <> 0 THEN DO:
        IF GST-multi-company = YES  THEN DO:
          FOR EACH company-gst:
             CREATE NewAcctTrans.
             ASSIGN
                NewAcctTrans.BatchCode       = NewBatch.BatchCode
                NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
                NewAcctTrans.EntityType      = "L"
                NewAcctTrans.EntityCode      = company-gst.company_id
                NewAcctTrans.AccountCode     = gst-in
                NewAcctTrans.Amount          = company-gst.Tax_Amount 
                NewAcctTrans.Date            = trn-date
                NewAcctTrans.MonthCode = mth-code
                NewAcctTrans.Description     = 'GST, ' + NewDocument.Description
                NewAcctTrans.Reference       = NewDocument.Reference
                document-total = document-total + NewAcctTrans.Amount.
              residual-tax = residual-tax  - company-gst.Tax_Amount .
          END.   /* Each company-gst */
      END.  /* GST-multi-company */
      
      /* Remainder of tax (sometimes because of rounding) */
      IF residual-tax <> 0 THEN DO:
        CREATE NewAcctTrans.
         ASSIGN
           NewAcctTrans.BatchCode       = NewBatch.BatchCode
           NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
           NewAcctTrans.EntityType      = "L"
           NewAcctTrans.EntityCode      = gst-in-ecode
           NewAcctTrans.AccountCode     = gst-in
           NewAcctTrans.Amount          = residual-tax
           NewAcctTrans.Date            = trn-date
           NewAcctTrans.MonthCode       = mth-code
           NewAcctTrans.Description     = "GST, " + NewDocument.Description
           NewAcctTrans.Reference       = NewDocument.Reference
           document-total = document-total + NewAcctTrans.Amount.
      END. /* residual tax */

      IF multi-ledger-creditors THEN DO:
        NewAcctTrans.EntityCode     = creditor-ledger.
      END.

    END. /* gst applies */

    /* Credit the creditor */
  
    CREATE NewAcctTrans.
    ASSIGN
      NewAcctTrans.BatchCode       = NewBatch.BatchCode
      NewAcctTrans.DocumentCode    = NewDocument.DocumentCode
      NewAcctTrans.EntityType      = "C"
      NewAcctTrans.EntityCode      = Voucher.CreditorCode
      NewAcctTrans.AccountCode     = sundry-creditors
      NewAcctTrans.Amount          = ( Voucher.GoodsValue + Voucher.TaxValue ) * -1
      NewAcctTrans.Date            = trn-date
      NewAcctTrans.MonthCode       = mth-code
      NewAcctTrans.Description     = NewDocument.Description
      NewAcctTrans.Reference       = NewDocument.Reference
      document-total = document-total + NewAcctTrans.Amount.

    IF multi-ledger-creditors THEN DO:
      NewAcctTrans.AccountCode     = creditor-ledger.
    END.


    FIND CURRENT Voucher EXCLUSIVE-LOCK.
    ASSIGN
      Voucher.VoucherStatus = "A"
      Voucher.BatchCode     = NewDocument.BatchCode
      Voucher.DocumentCode  = NewDocument.DocumentCode.

    IF document-total <> 0 THEN DO:
      MESSAGE "Error approving voucher" Voucher.VoucherSeq SKIP
              "Voucher allocations do not match invoice total"
              VIEW-AS ALERT-BOX ERROR TITLE "Voucher Allocation Error".
      UNDO, RETURN "FAIL".
    END.
  END.

END.

RUN dispatch IN h-msg( 'destroy':U ).
MESSAGE "Vouchers successfully Approved" VIEW-AS ALERT-BOX INFORMATION.
RUN open-vch-query.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  RUN open-vch-query.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-vch-query V-table-Win 
PROCEDURE open-vch-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  {&OPEN-QUERY-{&BROWSE-NAME}}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-all-vouchers V-table-Win 
PROCEDURE select-all-vouchers :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO INITIAL 1.
DEF VAR test-1 AS LOGI NO-UNDO.
DEF VAR test-2 AS LOGI NO-UNDO.

  IF NUM-RESULTS("{&BROWSE-NAME}") = 0 OR
     NUM-RESULTS("{&BROWSE-NAME}") = ? THEN RETURN.
     
  GET FIRST {&BROWSE-NAME}.
  REPOSITION {&BROWSE-NAME} TO ROWID ROWID( Voucher ).
  
  DO WHILE AVAILABLE Voucher:
   ASSIGN test-1 = BROWSE {&BROWSE-NAME}:IS-ROW-SELECTED(i) NO-ERROR.
   IF NOT test-1 THEN DO:
     ASSIGN test-2 = {&BROWSE-NAME}:SELECT-ROW(i) IN FRAME {&FRAME-NAME} NO-ERROR.
     IF NOT test-2 THEN DO:
       REPOSITION {&BROWSE-NAME} TO ROWID ROWID( Voucher ).
       i = 1.
       ASSIGN test-1 = BROWSE {&BROWSE-NAME}:IS-ROW-SELECTED(i) NO-ERROR.
       ASSIGN test-2 = {&BROWSE-NAME}:SELECT-ROW(i) IN FRAME {&FRAME-NAME} NO-ERROR.
     END.
     {&BROWSE-NAME}:SELECT-FOCUSED-ROW() IN FRAME {&FRAME-NAME}.
   END.
   GET NEXT {&BROWSE-NAME}.
   i = i + 1.
  END.  
  
  GET FIRST {&BROWSE-NAME}.
  REPOSITION {&BROWSE-NAME} TO ROWID ROWID( Voucher ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Voucher"}
  {src/adm/template/snd-list.i "Creditor"}
  {src/adm/template/snd-list.i "VoucherLine"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE toggle-current-row V-table-Win 
PROCEDURE toggle-current-row :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR row-num AS INT NO-UNDO.
DEF VAR row-sel AS LOGI NO-UNDO.
  
  row-num = BROWSE {&BROWSE-NAME}:FOCUSED-ROW .
  row-sel = BROWSE {&BROWSE-NAME}:IS-ROW-SELECTED(row-num).
  MESSAGE row-num row-sel.
  IF row-sel THEN
    {&BROWSE-NAME}:DESELECT-FOCUSED-ROW() IN FRAME {&FRAME-NAME}.
  ELSE
    {&BROWSE-NAME}:SELECT-FOCUSED-ROW() IN FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-gst-table V-table-Win 
PROCEDURE update-gst-table :
/*------------------------------------------------------------------------------
  Purpose:  Updates the GST table that stores the proportion of GST to be allocated for each company 
                    for all the transactions in the current  document.   
                    2003/03/17 GLF  
------------------------------------------------------------------------------*/
DEF VAR v_eType  LIKE EntityType.EntityType  NO-UNDO.
DEF VAR v_eCode LIKE company.CompanyCode  NO-UNDO.

ASSIGN v_EType =VoucherLine.EntityType
               v_ECode= VoucherLine.EntityCode.

/* Drill up to the first company/ledger hierarchy */
IF v_eType <> 'L'  THEN DO:
    REPEAT WHILE v_eType <> 'L':
         IF v_eType = 'J'  THEN DO:
             FIND Project WHERE Project.ProjectCode = v_eCode NO-LOCK.
              ASSIGN   v_eType =Project.EntityType
                               v_eCode =Project.EntityCode.
          END.
          ELSE DO:
              IF v_eType = 'P' THEN DO:
                FIND Property WHERE Property.PropertyCode = v_eCode NO-LOCK.
                ASSIGN v_eType = 'L'
                                v_eCode = Property.CompanyCode.
              END.
         END.
    END. /* Repeat */
END. /* Do */

/* Update amounts to company gst temporary table */
FIND company-gst WHERE company-gst.company_id  = v_eCode EXCLUSIVE-LOCK NO-ERROR.
IF AVAILABLE company-gst  THEN 
    company-gst.tax_amount  = company-gst.tax_amount + 
                                                       ((VoucherLine.Amount  - VoucherLine.TaxAmount) *
                                                         (DECIMAL(gst-rate) / 100 )) . 
ELSE DO:
   CREATE company-gst.
    ASSIGN company-gst.company_id = v_eCode
                    company-gst.tax_amount = (VoucherLine.Amount  - VoucherLine.TaxAmount) *
                                                                       (DECIMAL(gst-rate) / 100 ).
END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-trn-date V-table-Win 
FUNCTION get-trn-date RETURNS DATE
  ( OUTPUT m-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF VAR spec1 AS CHAR NO-UNDO.
DEF VAR spec2 AS CHAR NO-UNDO.
DEF VAR n-diff AS INT NO-UNDO INITIAL 0.
DEF VAR result1 AS DATE NO-UNDO.
DEF VAR result2 AS DATE NO-UNDO.

  m-code = ?.
  IF use-due-dates <> Yes THEN RETURN Voucher.Date.

  spec1 = ENTRY( 1, voucher-transaction-date, "/").
  spec1 = ENTRY( 1, spec1, "-").
  IF NUM-ENTRIES( voucher-transaction-date, "/") = 2 THEN
    spec2 = ENTRY( 2, voucher-transaction-date, "/").
  ELSE
    spec2 = spec1.

  IF spec2 MATCHES "*-*" THEN ASSIGN n-diff = - INT(ENTRY( 2, spec2, "-")) NO-ERROR.
  IF n-diff < 0 THEN . ELSE n-diff = 0.
  spec2 = ENTRY( 1, spec2, "-").

  CASE spec1:
    WHEN "Due" THEN       result1 = Voucher.DateDue.
    WHEN "Invoice" THEN   result1 = Voucher.Date.
    WHEN "Created" THEN   result1 = Voucher.CreatedDate.
    OTHERWISE result1 = TODAY.
  END CASE.
  CASE spec2:
    WHEN "Due" THEN       result2 = Voucher.DateDue - n-diff.
    WHEN "Invoice" THEN   result2 = Voucher.Date - n-diff.
    WHEN "Created" THEN   result2 = Voucher.CreatedDate - n-diff.
    OTHERWISE result2 = TODAY - n-diff.
  END CASE.

  IF result1 = ? THEN result1 = TODAY.
  IF result2 = ? THEN result2 = TODAY.
  IF result2 > result1 THEN DO:
    FIND LAST Month WHERE Month.StartDate <= result2 NO-LOCK NO-ERROR.
    IF AVAILABLE(Month) THEN m-code = Month.MonthCode.
    RETURN result2.
  END.

  RETURN result1.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validate-creditor-ledger V-table-Win 
FUNCTION validate-creditor-ledger RETURNS INTEGER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR creditor-company AS INT NO-UNDO INITIAL ?.
DEF VAR this-company AS INT NO-UNDO.

  FOR EACH VoucherLine OF Voucher NO-LOCK:
    this-company = get-entity-ledger( VoucherLine.EntityType, VoucherLine.EntityCode ).
    IF creditor-company = ? THEN creditor-company = this-company. ELSE DO:
      IF creditor-company <> this-company THEN RETURN ?.  /* May not split allocations across multiple companies */
    END.
  END.

  RETURN creditor-company.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

