&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

DEF VAR user-name AS CHAR INITIAL "Andrew" NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set-l.i "Tenant-Accounts" "tenant-accounts"}
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Log7 RP.Int5 RP.Int6 RP.Int1 ~
RP.Int2 RP.Dec2 RP.Log6 RP.Date1 RP.Log4 RP.Char2 RP.Log3 RP.Log1 RP.Char3 ~
RP.Log5 RP.Log2 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Int5 ~{&FP2}Int5 ~{&FP3}~
 ~{&FP1}Int6 ~{&FP2}Int6 ~{&FP3}~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}~
 ~{&FP1}Int2 ~{&FP2}Int2 ~{&FP3}~
 ~{&FP1}Dec2 ~{&FP2}Dec2 ~{&FP3}~
 ~{&FP1}Date1 ~{&FP2}Date1 ~{&FP3}~
 ~{&FP1}Char2 ~{&FP2}Char2 ~{&FP3}~
 ~{&FP1}Char3 ~{&FP2}Char3 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-4 rs_AccountSelection fil_From ~
cmb_AccountGroup cmb_Month1 cmb_Month2 btn_Browse Btn_OK 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Log7 RP.Int5 RP.Int6 RP.Int1 ~
RP.Int2 RP.Dec2 RP.Log6 RP.Date1 RP.Log4 RP.Char2 RP.Log3 RP.Log1 RP.Char3 ~
RP.Log5 RP.Log2 
&Scoped-Define DISPLAYED-OBJECTS fil_comp1 fil_prop1 fil_comp2 fil_prop2 ~
fil_tenant1 fil_tenant2 rs_AccountSelection fil_From fil_account1 ~
cmb_AccountGroup fil_account2 cmb_Month1 cmb_Month2 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */
&Scoped-define ADM-CREATE-FIELDS RP.Log2 
&Scoped-define ADM-ASSIGN-FIELDS RP.Log2 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Browse 
     LABEL "Browse" 
     SIZE 6.29 BY 1
     FONT 10.

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 12 BY 1
     BGCOLOR 8 FONT 9.

DEFINE VARIABLE cmb_AccountGroup AS CHARACTER FORMAT "X(100)" 
     LABEL "Group" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "PROPEX - Property Expenses" 
     SIZE 48.57 BY 1
     FONT 8.

DEFINE VARIABLE cmb_Month1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Month2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     SIZE 17.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_account1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 37 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_account2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 37 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_comp1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_comp2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_From AS DECIMAL FORMAT "9999.99" INITIAL 0 
     LABEL "From" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1.

DEFINE VARIABLE fil_prop1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_prop2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_tenant1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE fil_tenant2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1
     BGCOLOR 16  NO-UNDO.

DEFINE VARIABLE rs_AccountSelection AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Single account", "1":U,
"Account range", "R":U,
"Account group", "G":U
     SIZE 14.86 BY 2.4
     FONT 10.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 72.57 BY 20.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 71.43 BY 2.8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.2 COL 2.72 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "for a single Tenant", "T1":U,
"for a range of Tenant codes", "TR":U,
"for all Tenants of one Property", "P1":U,
"for all Debtors of one Company", "L1":U,
"by Property code and then by Tenant code", "PR":U,
"by Company code and then by Tenant code", "LR":U
          SIZE 32.57 BY 4.8
          FONT 10
     RP.Log7 AT ROW 4.6 COL 39.29
          LABEL "Include tenants of properties for company"
          VIEW-AS TOGGLE-BOX
          SIZE 32 BY .85
     RP.Int5 AT ROW 6.2 COL 10.43 COLON-ALIGNED
          LABEL "Property" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_comp1 AT ROW 6.2 COL 19.57 COLON-ALIGNED NO-LABEL
     fil_prop1 AT ROW 6.2 COL 19.57 COLON-ALIGNED NO-LABEL
     RP.Int6 AT ROW 7.2 COL 10.43 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_comp2 AT ROW 7.2 COL 19.57 COLON-ALIGNED NO-LABEL
     fil_prop2 AT ROW 7.2 COL 19.57 COLON-ALIGNED NO-LABEL
     RP.Int1 AT ROW 8.8 COL 10.43 COLON-ALIGNED HELP
          ""
          LABEL "Tenant" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_tenant1 AT ROW 8.8 COL 19.57 COLON-ALIGNED NO-LABEL
     RP.Int2 AT ROW 9.8 COL 10.43 COLON-ALIGNED HELP
          ""
          LABEL "to" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.29 BY 1
     fil_tenant2 AT ROW 9.8 COL 19.57 COLON-ALIGNED NO-LABEL
     rs_AccountSelection AT ROW 11.4 COL 2.14 NO-LABEL
     fil_From AT ROW 11.6 COL 21.86 COLON-ALIGNED
     fil_account1 AT ROW 11.6 COL 33.43 COLON-ALIGNED NO-LABEL
     cmb_AccountGroup AT ROW 12 COL 21.86 COLON-ALIGNED
     RP.Dec2 AT ROW 12.6 COL 21.86 COLON-ALIGNED HELP
          ""
          LABEL "to" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 8 BY 1
     fil_account2 AT ROW 12.6 COL 33.43 COLON-ALIGNED NO-LABEL
     cmb_Month1 AT ROW 14.2 COL 10.43 COLON-ALIGNED
     cmb_Month2 AT ROW 14.2 COL 42.43 COLON-ALIGNED
     RP.Log6 AT ROW 15.6 COL 12.43 HELP
          ""
          LABEL "Only when transaction date on or before"
          VIEW-AS TOGGLE-BOX
          SIZE 30.29 BY .8
     RP.Date1 AT ROW 15.6 COL 41.29 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99/99/9999"
          VIEW-AS FILL-IN 
          SIZE 12 BY .9
     RP.Log4 AT ROW 16.4 COL 12.43
          LABEL "Show batch code"
          VIEW-AS TOGGLE-BOX
          SIZE 15.43 BY .8
     RP.Char2 AT ROW 17.1 COL 27 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 43.43 BY .9 TOOLTIP "Enter a list of account codes, separated by commas"
     RP.Log3 AT ROW 17.2 COL 12.43 HELP
          ""
          LABEL "Related accounts"
          VIEW-AS TOGGLE-BOX
          SIZE 15.43 BY .8
     RP.Log1 AT ROW 18 COL 12.43 HELP
          ""
          LABEL "Include closed transactions"
          VIEW-AS TOGGLE-BOX
          SIZE 21.72 BY .8
          FONT 10
     RP.Char3 AT ROW 18.7 COL 20.72 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 44 BY .9
     btn_Browse AT ROW 18.7 COL 66.72
.
/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME F-Main
     RP.Log5 AT ROW 18.8 COL 12.43
          LABEL "Export to"
          VIEW-AS TOGGLE-BOX
          SIZE 9.14 BY .8
          FONT 10
     RP.Log2 AT ROW 19.6 COL 12.43 HELP
          ""
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 9.14 BY .8
          FONT 10
     Btn_OK AT ROW 19.8 COL 61
     RECT-1 AT ROW 1 COL 1
     RECT-4 AT ROW 11.2 COL 1.57
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 21.2
         WIDTH              = 78.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Char3 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Date1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Dec2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_account1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_account2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_comp1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_comp2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_prop1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_prop2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_tenant1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_tenant2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Int5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int6 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   1 2 EXP-LABEL EXP-HELP                                               */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log6 IN FRAME F-Main
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX RP.Log7 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Browse V-table-Win
ON CHOOSE OF btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  DISABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
  RUN run-report.
  ENABLE {&SELF-NAME} WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_AccountGroup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AccountGroup V-table-Win
ON U1 OF cmb_AccountGroup IN FRAME F-Main /* Group */
DO:
  {inc/selcmb/scacg1.i "RP" "Char5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AccountGroup V-table-Win
ON U2 OF cmb_AccountGroup IN FRAME F-Main /* Group */
DO:
  {inc/selcmb/scacg2.i "RP" "Char5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Month1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month1 V-table-Win
ON U1 OF cmb_Month1 IN FRAME F-Main /* Period */
DO:
  {inc/selcmb/scmths1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month1 V-table-Win
ON U2 OF cmb_Month1 IN FRAME F-Main /* Period */
DO:
  {inc/selcmb/scmths2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Month2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month2 V-table-Win
ON U1 OF cmb_Month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month2 V-table-Win
ON U2 OF cmb_Month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Dec2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Dec2 V-table-Win
ON LEAVE OF RP.Dec2 IN FRAME F-Main /* to */
DO:
  {inc/selcde/cdcoa.i "fil_account2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_account1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U1 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa1.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U2 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa2.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account1 V-table-Win
ON U3 OF fil_account1 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa3.i "RP" "Dec1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_account2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U1 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa1.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U2 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa2.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_account2 V-table-Win
ON U3 OF fil_account2 IN FRAME F-Main
DO:
  {inc/selfil/sfcoa3.i "RP" "Dec2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_comp1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U1 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U2 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp1 V-table-Win
ON U3 OF fil_comp1 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_comp2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U1 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "RP" "Int6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U2 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "RP" "Int6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_comp2 V-table-Win
ON U3 OF fil_comp2 IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "RP" "Int6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_From
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_From V-table-Win
ON LEAVE OF fil_From IN FRAME F-Main /* From */
DO:
  {inc/selcde/cdcoa.i "fil_account1"}
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_prop1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U1 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U2 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop1 V-table-Win
ON U3 OF fil_prop1 IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_prop2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U1 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U2 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_prop2 V-table-Win
ON U3 OF fil_prop2 IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_tenant1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_tenant1 V-table-Win
ON U1 OF fil_tenant1 IN FRAME F-Main
DO:
  {inc/selfil/sftnt1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_tenant1 V-table-Win
ON U2 OF fil_tenant1 IN FRAME F-Main
DO:
  {inc/selfil/sftnt2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_tenant1 V-table-Win
ON U3 OF fil_tenant1 IN FRAME F-Main
DO:
  {inc/selfil/sftnt3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_tenant2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_tenant2 V-table-Win
ON U1 OF fil_tenant2 IN FRAME F-Main
DO:
  {inc/selfil/sftnt1.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_tenant2 V-table-Win
ON U2 OF fil_tenant2 IN FRAME F-Main
DO:
  {inc/selfil/sftnt2.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_tenant2 V-table-Win
ON U3 OF fil_tenant2 IN FRAME F-Main
DO:
  {inc/selfil/sftnt3.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Tenant */
DO:
  {inc/selcde/cdtnt.i "fil_tenant1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* to */
DO:
  {inc/selcde/cdtnt.i "fil_tenant2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int5 V-table-Win
ON LEAVE OF RP.Int5 IN FRAME F-Main /* Property */
DO:
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Char1 BEGINS "P" THEN DO:
    {inc/selcde/cdpro.i "fil_prop1"}
  END.
  ELSE IF INPUT RP.Char1 BEGINS "L" THEN DO:
    {inc/selcde/cdcmp.i "fil_comp1"}
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int6 V-table-Win
ON LEAVE OF RP.Int6 IN FRAME F-Main /* To */
DO:
  IF RP.Char1 = "P" THEN DO:
    {inc/selcde/cdpro.i "fil_prop2"}
  END.
  ELSE IF RP.Char1 = "L" THEN DO:
    {inc/selcde/cdcmp.i "fil_comp2"}
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log3 V-table-Win
ON VALUE-CHANGED OF RP.Log3 IN FRAME F-Main /* Related accounts */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log5 V-table-Win
ON VALUE-CHANGED OF RP.Log5 IN FRAME F-Main /* Export to */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log6 V-table-Win
ON VALUE-CHANGED OF RP.Log6 IN FRAME F-Main /* Only when transaction date on or before */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs_AccountSelection
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs_AccountSelection V-table-Win
ON VALUE-CHANGED OF rs_AccountSelection IN FRAME F-Main
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose: As its name!
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:
    CASE INPUT RP.Char1:
      WHEN "PR" THEN DO:
          RP.Int5:LABEL = "Property".
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant1:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant2:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = Yes" ).
          VIEW RP.Int5 RP.Int6 fil_prop1 fil_prop2
                RP.Int1 RP.Int2 fil_tenant1 fil_tenant2.
          HIDE fil_comp1 fil_comp2 RP.Log7.
        END.
      WHEN "LR" THEN DO:
          RP.Int5:LABEL = "Company".
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant1:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant2:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
          VIEW RP.Int5 RP.Int6 fil_comp1 fil_comp2 RP.Log7
                RP.Int1 RP.Int2 fil_tenant1 fil_tenant2.
          HIDE fil_prop1 fil_prop2.
        END.
      WHEN "TR" THEN DO:
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant1:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant2:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
          HIDE RP.Int5 RP.Int6 fil_comp1 fil_comp2 fil_prop1 fil_prop2 RP.Log7.
          VIEW RP.Int1 RP.Int2 fil_tenant1 fil_tenant2.
        END.
      WHEN "P1" THEN DO:
          RP.Int5:LABEL = "Property".
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant1:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant2:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = Yes" ).
          VIEW RP.Int5 fil_prop1.
          HIDE RP.Int1 fil_tenant1 RP.Int2 fil_tenant2
                RP.Int6 fil_comp1 fil_comp2 fil_prop2 RP.Log7.
        END.
      WHEN "L1" THEN DO:
          RP.Int5:LABEL = "Company".
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant1:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant2:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
          VIEW RP.Int5 fil_comp1 RP.Log7.
          HIDE RP.Int1 fil_tenant1 RP.Int2 fil_tenant2
                 RP.Int6 fil_prop1 fil_prop2 fil_comp2.
        END.
      WHEN "T1" THEN DO:
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant1:HANDLE ), "HIDDEN = No" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant2:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp1:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_comp2:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop1:HANDLE ), "HIDDEN = Yes" ).
          RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_prop2:HANDLE ), "HIDDEN = Yes" ).
          VIEW RP.Int1 fil_tenant1.
          HIDE RP.Int2 fil_tenant2 RP.Log7
                 RP.Int5 RP.Int6 fil_comp1 fil_comp2 fil_prop1 fil_prop2.
        END.
    END CASE.

    /* list of related accounts */
    IF INPUT RP.Log3 THEN VIEW RP.Char2. ELSE HIDE RP.Char2.

    /* export to filename */
    IF INPUT RP.Log5 THEN VIEW RP.Char3 btn_Browse. ELSE HIDE RP.Char3 btn_Browse.

    /* on or before explicit date */
    IF INPUT RP.Log6 THEN VIEW RP.Date1. ELSE HIDE RP.Date1.

    /* account code selection */
    IF tenant-accounts <> Yes THEN DO:
      HIDE rs_AccountSelection fil_From fil_Account1 cmb_AccountGroup RP.Dec2 fil_Account2 RECT-4.
      ENABLE RP.Log1 .
    END.
    ELSE DO:
      VIEW rs_AccountSelection .
      CASE INPUT rs_AccountSelection:
        WHEN "R" THEN DO:
          HIDE cmb_AccountGroup.
          VIEW fil_From fil_Account1 RP.Dec2 fil_Account2 .
          RP.Log1:CHECKED = Yes.
          DISABLE RP.Log1.
        END.
        WHEN "G" THEN DO:
          HIDE fil_From fil_Account1 RP.Dec2 fil_Account2 .
          RP.Log1:CHECKED = Yes.
          DISABLE RP.Log1.
          VIEW cmb_AccountGroup.
        END.
        OTHERWISE DO:
          HIDE cmb_AccountGroup RP.Dec2 fil_Account2 .
          VIEW fil_From fil_Account1.
          IF INPUT fil_From <> sundry-debtors THEN DO:
            RP.Log1:CHECKED = Yes.
            DISABLE RP.Log1.
          END.
          ELSE DO:
            ENABLE RP.Log1 .
          END.
        END.
      END CASE.
      IF INPUT RP.Log1 <> RP.Log1 THEN
        RUN check-modified( 'clear':U ).
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialise the necessary bits and pieces
------------------------------------------------------------------------------*/
DEF VAR month1-list AS CHAR INITIAL "" NO-UNDO.
DEF VAR month2-list AS CHAR INITIAL "" NO-UNDO.
DEF VAR delim AS CHAR INITIAL "|" NO-UNDO.

  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  FIND RP WHERE RP.ReportID = "tr-t"
                        AND RP.UserName = user-name
                        NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:
    CREATE RP.
    ASSIGN  RP.ReportID = "tr-t"
            RP.UserName = user-name .
  END.

  rs_AccountSelection = "1".
  fil_From = sundry-debtors.

  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).
  RUN enable-appropriate-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose: Clear the modified flag.
------------------------------------------------------------------------------*/
  RUN check-modified('clear':U).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Actually run the report program.
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.
DEF VAR et AS CHAR NO-UNDO.
DEF VAR range AS CHAR NO-UNDO.
DEF VAR e1 AS INT INITIAL 0 NO-UNDO.
DEF VAR e2 AS INT INITIAL 99999 NO-UNDO.
DEF VAR c-win AS HANDLE NO-UNDO.

  RUN dispatch IN THIS-PROCEDURE ('update-record':U).
  DO WITH FRAME {&FRAME-NAME}:
    et = SUBSTRING( RP.Char1, 1, 1).
    report-options = "EntityType," + et.

    IF et = "T" THEN
      ASSIGN  e1 = RP.Int1   e2 = RP.Int2 .
    ELSE
      ASSIGN  e1 = RP.Int5   e2 = RP.Int6 .
    range = SUBSTRING( RP.Char1, 2, 1).
    IF range = "1" OR e2 < e1 THEN e2 = e1 .
    report-options = report-options + "~nEntityRange," + STRING(e1) + "," + STRING(e2).

    report-options = report-options + "~nMonthRange," + STRING(RP.Int3) + "," + STRING(RP.Int4)
                   + (IF RP.Log1 THEN "~nShow-All" ELSE "")
                   + (IF RP.Log2 THEN "~nPreview" ELSE "")
                   + (IF RP.Log3 THEN "~nAccountList," + RP.Char2 ELSE "")
                   + (IF RP.Log4 THEN "~nShow-Batch" ELSE "")
                   + (IF RP.Log5 THEN "~nExport," + RP.Char3 ELSE "")
                   + (IF RP.Log3 THEN "~nAccountList," + RP.Char2 ELSE "")
                   + (IF RP.Log6 AND RP.Date1 <> ? THEN "~nOnOrEarlier," + STRING(RP.Date1,"99/99/9999") ELSE "")
                   + (IF RP.Log7 THEN "~nTenantsOfCoProps" ELSE "")
                   + "~nAccounts," + INPUT rs_AccountSelection + ","
                                   + STRING(INPUT fil_From) + ","
                                   + STRING(RP.Dec2) + ","
                                   + RP.Char5.
  END.

  RUN notify( 'set-busy,container-source':U ).
  RUN process/report/tr-t.p ( report-options ).
  RUN notify( 'set-idle,container-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char3 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated values" "*.CSV"
        SAVE-AS ASK-OVERWRITE CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV"
        INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME UPDATE select-ok.

  IF select-ok THEN DO:
    IF SUBSTRING( file-name, R-INDEX( file-name, ".") ) <> ".CSV" THEN DO:
      IF (LENGTH( file-name ) - R-INDEX( file-name, "." )) < 4 THEN
        file-name = SUBSTRING( file-name, 1, R-INDEX( file-name, ".")) + "CSV" .
    END.
    RP.Char3:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


