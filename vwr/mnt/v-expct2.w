&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description: 
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

&SCOPED-DEFINE REPORT-ID "Property Contact Export"

DEF VAR property-list AS CHAR NO-UNDO.
DEF VAR pty-list AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Log1 RP.Log3 RP.Log2 RP.Log4 ~
RP.Log5 RP.Log6 RP.Log7 RP.Log8 RP.Char5 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Char5 ~{&FP2}Char5 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-28 cmb_PostalType-1 cmb_PostalType-2 ~
cmb_PostalType-3 cmb_EcType btn_browse btn_export btn_cancel 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Log1 RP.Log3 RP.Log2 RP.Log4 ~
RP.Log5 RP.Log6 RP.Log7 RP.Log8 RP.Char5 
&Scoped-Define DISPLAYED-OBJECTS cmb_PostalType-1 cmb_PostalType-2 ~
cmb_PostalType-3 cmb_EcType 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_browse 
     LABEL "&Browse" 
     SIZE 6.57 BY 1.05
     FONT 9.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_export DEFAULT 
     LABEL "&Export" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE VARIABLE cmb_PostalType-1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "1" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     SIZE 30.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_PostalType-2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "2" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     SIZE 30.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE cmb_PostalType-3 AS CHARACTER FORMAT "X(256)":U 
     LABEL "3" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     SIZE 30.29 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-28
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 78.86 BY 19.

DEFINE VARIABLE cmb_EcType AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
     SIZE 33.43 BY 7.4 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 2.2 COL 1.57 HELP
          "" NO-LABEL
          VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
          SIZE 41.72 BY 17.5
     cmb_PostalType-1 AT ROW 2.4 COL 47 COLON-ALIGNED
     cmb_PostalType-2 AT ROW 3.6 COL 47 COLON-ALIGNED
     cmb_PostalType-3 AT ROW 4.8 COL 47 COLON-ALIGNED
     RP.Log1 AT ROW 7.1 COL 45.86
          LABEL "Accounting matters"
          VIEW-AS TOGGLE-BOX
          SIZE 16 BY 1
          FONT 10
     RP.Log3 AT ROW 7.1 COL 66.43
          LABEL "After hours #1"
          VIEW-AS TOGGLE-BOX
          SIZE 12.57 BY 1
          FONT 10
     RP.Log2 AT ROW 8 COL 45.86
          LABEL "Property matters"
          VIEW-AS TOGGLE-BOX
          SIZE 16.57 BY 1
          FONT 10
     RP.Log4 AT ROW 8.1 COL 66.43
          LABEL "After hours #2"
          VIEW-AS TOGGLE-BOX
          SIZE 12.57 BY 1
          FONT 10
     cmb_EcType AT ROW 9 COL 45.86 NO-LABEL
     RP.Log5 AT ROW 16.6 COL 45.57
          LABEL "Split address into separate lines"
          VIEW-AS TOGGLE-BOX
          SIZE 25.72 BY .8
     RP.Log6 AT ROW 17.4 COL 45.57
          LABEL "Include 'No Mailout' contacts"
          VIEW-AS TOGGLE-BOX
          SIZE 24.29 BY .8
     RP.Log7 AT ROW 18.2 COL 45.57
          LABEL "Only export any individual once"
          VIEW-AS TOGGLE-BOX
          SIZE 32 BY .85 TOOLTIP "Only one instance for firstname, lastname and company"
     RP.Log8 AT ROW 19 COL 45.57
          LABEL "Clean addresses of company names"
          VIEW-AS TOGGLE-BOX
          SIZE 28 BY .85
     RP.Char5 AT ROW 20.2 COL 7.57 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 40 BY 1
          FONT 10
     btn_browse AT ROW 20.2 COL 50.14
     btn_export AT ROW 20.2 COL 58.72
     btn_cancel AT ROW 20.2 COL 69.57
     "Properties" VIEW-AS TEXT
          SIZE 12 BY 1 AT ROW 1.2 COL 1.57
          FONT 14
     "Which contact types to export" VIEW-AS TEXT
          SIZE 30.29 BY 1 AT ROW 6.2 COL 45
          FONT 14
     "Export To:" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 20.2 COL 1.57
          FONT 10
     "Address Selection Priority" VIEW-AS TEXT
          SIZE 26.29 BY 1 AT ROW 1.2 COL 45
          FONT 14
     RECT-28 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_export.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 21.4
         WIDTH              = 88.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR SELECTION-LIST RP.Char1 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN RP.Char5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log6 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log7 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log8 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_browse V-table-Win
ON CHOOSE OF btn_browse IN FRAME F-Main /* Browse */
DO:  
  RUN browse-file.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_export
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_export V-table-Win
ON CHOOSE OF btn_export IN FRAME F-Main /* Export */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN export-contacts.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_EcType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_EcType V-table-Win
ON U1 OF cmb_EcType IN FRAME F-Main
DO:
  {inc/selcmb/sel-ectyp1.i "RP" "Char6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_EcType V-table-Win
ON U2 OF cmb_EcType IN FRAME F-Main
DO:
  {inc/selcmb/sel-ectyp2.i "RP" "Char6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PostalType-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-1 V-table-Win
ON U1 OF cmb_PostalType-1 IN FRAME F-Main /* 1 */
DO:
  {inc/selcmb/scpdt1.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-1 V-table-Win
ON U2 OF cmb_PostalType-1 IN FRAME F-Main /* 1 */
DO:
  {inc/selcmb/scpdt2.i "RP" "Char2"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PostalType-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-2 V-table-Win
ON U1 OF cmb_PostalType-2 IN FRAME F-Main /* 2 */
DO:
  {inc/selcmb/scpdt1.i "RP" "Char3"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-2 V-table-Win
ON U2 OF cmb_PostalType-2 IN FRAME F-Main /* 2 */
DO:
  {inc/selcmb/scpdt2.i "RP" "Char3"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_PostalType-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-3 V-table-Win
ON U1 OF cmb_PostalType-3 IN FRAME F-Main /* 3 */
DO:
  {inc/selcmb/scpdt1.i "RP" "Char4"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_PostalType-3 V-table-Win
ON U2 OF cmb_PostalType-3 IN FRAME F-Main /* 3 */
DO:
  {inc/selcmb/scpdt2.i "RP" "Char4"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE browse-file V-table-Win 
PROCEDURE browse-file :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR save-as     AS CHAR NO-UNDO.
  DEF VAR file-chosen AS LOGI NO-UNDO.

  save-as = INPUT FRAME {&FRAME-NAME} RP.Char5.
  IF LENGTH( save-as ) > 0 THEN
  IF LOOKUP( SUBSTR( save-as, LENGTH( save-as ), 1 ), "\,/" ) <> 0 THEN save-as = "".
    
  SYSTEM-DIALOG GET-FILE save-as
    TITLE   "Export Contacts to file"
    FILTERS "Comma-separated values (*.csv)" "*.csv"
    DEFAULT-EXTENSION ".csv"
    SAVE-AS
    RETURN-TO-START-DIR
    USE-FILENAME
    UPDATE file-chosen.

  IF file-chosen THEN RP.Char5:SCREEN-VALUE IN FRAME {&FRAME-NAME} = 
    save-as.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE export-contacts V-table-Win 
PROCEDURE export-contacts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

  IF INPUT FRAME {&FRAME-NAME} RP.Char5 = "" OR
     INPUT FRAME {&FRAME-NAME} RP.Char5 = "" THEN
  DO:
    MESSAGE "You must choose a file to export to!" VIEW-AS ALERT-BOX ERROR
      TITLE "No file name chosen".
    RUN browse-file.
    RETURN.
  END.
  
  RUN dispatch( 'update-record':U ).
  RUN get-property-list.
  RUN get-pty-list.

  report-options = "ExportFor,PROPERTY"
                 + "~nPropertyList," + property-list
                 + "~nFileName," + RP.Char5
                 + "~nEntityContactTypes," + RP.Char6
                 + "~nAddressTypes," + pty-list
                 + (IF RP.Log5 THEN "~nSplitAddress" ELSE "")
                 + (IF RP.Log6 THEN "~nIncludeNoMailout" ELSE "")
                 + (IF RP.Log7 THEN "~nMakeUnique" ELSE "")
                 + (IF RP.Log8 THEN "~nCleanAddresses" ELSE "")
                 + "~nContactTypes,".
  IF RP.Log1 THEN report-options = report-options + "A".  /* Accounting matters */
  IF RP.Log2 THEN report-options = report-options + "P".  /* Property matters */
  IF RP.Log3 THEN report-options = report-options + "1".  /* After hours #1 */
  IF RP.Log4 THEN report-options = report-options + "2".  /* After hours #2 */

  RUN process/export/pcontact.p ( report-options ).
  
  MESSAGE "Export Complete" VIEW-AS ALERT-BOX INFORMATION TITLE "Done".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-property-list V-table-Win 
PROCEDURE get-property-list :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR delim AS CHAR NO-UNDO.
DEF VAR sv    AS CHAR NO-UNDO.
DEF VAR i     AS INT  NO-UNDO.
  
  property-list = "".
  delim    = RP.Char1:DELIMITER IN FRAME {&FRAME-NAME}.
  sv       = RP.Char1:SCREEN-VALUE.
  
  DO i = 1 TO NUM-ENTRIES( sv, delim ):
    property-list = property-list + IF property-list = "" THEN "" ELSE ",".
    property-list = property-list + TRIM( ENTRY( 1, ENTRY( i, sv, delim ), "-" ) ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-pty-list V-table-Win 
PROCEDURE get-pty-list :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  pty-list = "".
  IF RP.Char2 <> "" THEN pty-list = TRIM( pty-list + "," + RP.Char2, "," ).
  IF RP.Char3 <> "" THEN pty-list = TRIM( pty-list + "," + RP.Char3, "," ).
  IF RP.Char4 <> "" THEN pty-list = TRIM( pty-list + "," + RP.Char4, "," ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR user-name AS CHAR NO-UNDO.
  
  RUN get-username IN sys-mgr( OUTPUT user-name ).
  RUN update-properties.

  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.

  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
  
    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Char2    = "MAIN"
      RP.Char3    = "POST"
      RP.Char3    = "COUR"
      .
  END.

  RUN dispatch( 'row-changed':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-properties V-table-Win 
PROCEDURE update-properties :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR item AS CHAR NO-UNDO.

  RP.Char1:DELIMITER IN FRAME {&FRAME-NAME} = CHR(2).
  FOR EACH Property WHERE Property.Active AND NOT Property.ExternallyManaged NO-LOCK:
    item = STRING( Property.PropertyCode, ">>>>9" ) + " - " + REPLACE( Property.Name, "~n", " ").
    IF RP.Char1:ADD-LAST( item ) IN FRAME {&FRAME-NAME} THEN.
  END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


