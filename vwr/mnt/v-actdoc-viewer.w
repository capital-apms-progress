&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.
DEF VAR key-name       AS CHAR NO-UNDO.
DEF VAR key-value      AS CHAR NO-UNDO.

/*
FIND FIRST Person NO-LOCK NO-ERROR.
FIND PREV Person NO-LOCK NO-ERROR.
*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Document AcctTran
&Scoped-define FIRST-EXTERNAL-TABLE Document


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Document, AcctTran.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS AcctTran.EntityType AcctTran.EntityCode 
&Scoped-define ENABLED-TABLES AcctTran
&Scoped-define FIRST-ENABLED-TABLE AcctTran
&Scoped-Define DISPLAYED-FIELDS AcctTran.EntityType AcctTran.EntityCode 
&Scoped-define DISPLAYED-TABLES AcctTran
&Scoped-define FIRST-DISPLAYED-TABLE AcctTran
&Scoped-Define DISPLAYED-OBJECTS fil_EntityName 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
PersonCode|y||Person.PersonCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "PersonCode",
     Keys-Supplied = ':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fil_EntityName AS CHARACTER FORMAT "X(256)":U INITIAL ? 
     VIEW-AS FILL-IN 
     SIZE 99 BY 1.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_EntityName AT ROW 1.05 COL 2 NO-LABEL
     AcctTran.EntityType AT ROW 1.05 COL 74 COLON-ALIGNED HELP
          ""
          LABEL "Entity" FORMAT "X"
          VIEW-AS FILL-IN 
          SIZE 3 BY 1.05 NO-TAB-STOP 
     AcctTran.EntityCode AT ROW 1.05 COL 77.14 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 5.86 BY 1.05 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.Document,ttpl.AcctTran
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 5.35
         WIDTH              = 109.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mstvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       AcctTran.EntityCode:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN AcctTran.EntityType IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
ASSIGN 
       AcctTran.EntityType:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN fil_EntityName IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON LEAVE OF FRAME F-Main
DO:
DO WITH FRAME {&FRAME-NAME}:
  /*
  IF fil_TenantName:MODIFIED OR tgl_MailOut:MODIFIED THEN DO:
    RUN assign-changed-fields( IF AVAILABLE Person THEN Person.PersonCode ELSE ? ).
  END.
  */
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main V-table-Win
ON MOUSE-SELECT-DBLCLICK OF FRAME F-Main
DO:
  RUN special-enable-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_EntityName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_EntityName V-table-Win
ON LEAVE OF fil_EntityName IN FRAME F-Main
DO:
  IF SELF:MODIFIED THEN DO:
    RUN assign-changed-fields( IF AVAILABLE TenantCall THEN TenantCall.CallNumber ELSE ? ).
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'PersonCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Person
           &WHERE = "WHERE Person.PersonCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Document"}
  {src/adm/template/row-list.i "AcctTran"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Document"}
  {src/adm/template/row-find.i "AcctTran"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE advise-panel-location V-table-Win 
PROCEDURE advise-panel-location :
/*------------------------------------------------------------------------------
  Purpose:  Tell the window where to position us!
------------------------------------------------------------------------------*/
  RETURN "bottom".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-changed-fields V-table-Win 
PROCEDURE assign-changed-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER call-number AS INT NO-UNDO.

/*
DEF VAR mail-out AS LOGI NO-UNDO.
DEF VAR types AS CHAR NO-UNDO.
DEF VAR no-types AS INT NO-UNDO.
DEF VAR c-type AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  ASSIGN tgl_MailOut fil_Types.
  mail-out = INPUT tgl_MailOut.
  types = REPLACE( INPUT fil_Types, " ", "").
  no-types = NUM-ENTRIES( types ).

  DO TRANSACTION:
    FIND Person WHERE Person.PersonCode = person-code EXCLUSIVE-LOCK NO-ERROR.
    IF AVAILABLE( Person ) THEN DO:
      Person.MailOut = mail-out.
      FOR EACH Contact OF Person EXCLUSIVE-LOCK, FIRST ContactType OF Contact NO-LOCK:
        IF ContactType.SystemCode THEN NEXT.
        IF NOT( CAN-DO(types, Contact.ContactType) ) THEN DELETE Contact.
      END.
      DO i = 1 TO no-types:
        c-type = ENTRY( i, types ).
        IF CAN-FIND( Contact OF Person WHERE Contact.ContactType = c-type ) THEN NEXT.
        IF NOT CAN-FIND( ContactType WHERE ContactType.ContactType = c-type AND NOT(ContactType.SystemCode) ) THEN NEXT.
        CREATE Contact.
        Contact.ContactType = c-type.
        Contact.PersonCode  = Person.PersonCode.
      END.
    END.
    FIND Person WHERE Person.PersonCode = person-code NO-LOCK NO-ERROR.
  END.
END.
*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable-edits V-table-Win 
PROCEDURE disable-edits :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  Mode = 'View' .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-edits V-table-Win 
PROCEDURE enable-edits :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  Mode = 'Maintain'.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN disable-edits.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     After standard ADM method
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
DEF VAR entry_ok AS LOGI NO-UNDO INITIAL No.

  /* Code placed here will execute AFTER standard behavior.    */
  IF AVAILABLE(AcctTran) THEN DO:
    
      CASE AcctTran.EntityType:
          WHEN 'L' THEN DO:
              FIND FIRST Company WHERE Company.CompanyCode = AcctTran.EntityCode NO-LOCK.
              fil_EntityName = TRIM(STRING( Company.LegalName, "X(50)" )).
          END.
          WHEN 'P' THEN DO:
              FIND FIRST Property WHERE Property.PropertyCode = AcctTran.EntityCode NO-LOCK.
              fil_EntityName = TRIM(STRING( Property.Name, "X(50)" )).
          END.
          WHEN 'J' THEN DO:
              FIND FIRST Project WHERE Project.ProjectCode = AcctTran.EntityCode NO-LOCK.
              fil_EntityName = TRIM(STRING( Project.Name, "X(50)" )).
          END.
          WHEN 'T' THEN DO:
              FIND FIRST Tenant WHERE Tenant.TenantCode = AcctTran.EntityCode NO-LOCK.
              fil_EntityName = TRIM(STRING( Tenant.Name, "X(50)" )).
          END.
          WHEN 'C' THEN DO:
              FIND FIRST Creditor WHERE Creditor.CreditorCode = AcctTran.EntityCode NO-LOCK.
              fil_EntityName = TRIM(STRING( Creditor.Name, "X(50)" )).
          END.
      END CASE.

      FIND FIRST ChartOfAccount WHERE ChartOfAccount.AccountCode = AcctTran.AccountCode NO-LOCK NO-ERROR.

      IF AVAILABLE ChartOfAccount THEN fil_EntityName = fil_EntityName + ' - ' + TRIM(STRING( ChartOfAccount.Name, "X(50)" )).

  END.

  DISPLAY fil_EntityName.
  RUN dispatch( 'enable-fields':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-apply-entry V-table-Win 
PROCEDURE local-apply-entry :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  RETURN.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'apply-entry':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-find-using-key V-table-Win 
PROCEDURE local-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'find-using-key':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-enable-fields V-table-Win 
PROCEDURE override-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     After standard ADM method
------------------------------------------------------------------------------*/
/*
  DO WITH FRAME {&FRAME-NAME}:
DEF VAR entry_ok AS LOGI NO-UNDO INITIAL No.

  entry_ok = (mode <> "View" AND AVAILABLE(Person)).
  ASSIGN  fil_Types:SENSITIVE = entry_ok
          tgl_MailOut:SENSITIVE = entry_ok.

END.
*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  /*
  IF fil_Types:MODIFIED THEN DO:
    RUN assign-changed-fields( IF AVAILABLE Person THEN Person.PersonCode ELSE ? ).
  END.
  */
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  /* Code placed here will execute PRIOR to standard behavior. */
  /*
  IF fil_Types:MODIFIED THEN
    RUN assign-changed-fields( IF AVAILABLE Person THEN Person.PersonCode ELSE ? ).
  */
  have-records = No.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* There are no foreign keys supplied by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Document"}
  {src/adm/template/snd-list.i "AcctTran"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE special-enable-fields V-table-Win 
PROCEDURE special-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/
DEF VAR rights AS LOGICAL NO-UNDO.

  RUN get-rights IN sys-mgr( "v-person-viewer", "MODIFY", OUTPUT rights).
  IF rights THEN
    RUN enable-edits.
  ELSE
    RUN disable-edits.

  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-font V-table-Win 
PROCEDURE use-font :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER font-num AS CHAR NO-UNDO.

  FRAME {&FRAME-NAME}:FONT = INTEGER( font-num ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name V-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  Key-Name = new-key-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.

  IF mode = "View" THEN
    RUN dispatch( 'disable-fields' ).
  ELSE
    RUN dispatch( 'enable-fields' ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-ToolTip V-table-Win 
PROCEDURE use-ToolTip :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-tip AS CHAR NO-UNDO.

  fil_EntityName:TOOLTIP IN FRAME {&FRAME-NAME} = new-tip.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

