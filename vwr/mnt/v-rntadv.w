&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description: 
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* Local Variable Definitions ---                                       */

&SCOP REPORT-ID "v-apform"
DEF VAR user-name   AS CHAR NO-UNDO.

{inc/ofc-this.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char3 RP.Int1 RP.Int2 RP.Char1 RP.Log1 ~
RP.Log2 RP.Log3 RP.Date1 RP.Date2 RP.Log4 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define ENABLED-OBJECTS btn_print RECT-1 
&Scoped-Define DISPLAYED-FIELDS RP.Char3 RP.Int1 RP.Int2 RP.Char1 RP.Log1 ~
RP.Log2 RP.Log3 RP.Date1 RP.Date2 RP.Log4 
&Scoped-Define DISPLAYED-OBJECTS fil_Tenant fil_Property cmb_BankAccount 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_print DEFAULT 
     LABEL "&OK" 
     SIZE 11.43 BY 1.05
     FONT 9.

DEFINE VARIABLE cmb_BankAccount AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     DROP-DOWN-LIST
     SIZE 50.43 BY 1
     FONT 10.

DEFINE VARIABLE fil_Property AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 44.57 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Tenant AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 44.57 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 65 BY 10.8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char3 AT ROW 1.8 COL 2.72 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Tenant", "T":U,
"Property", "P":U
          SIZE 8.57 BY 2
     RP.Int1 AT ROW 1.8 COL 9.29 COLON-ALIGNED NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1
          FONT 10
     fil_Tenant AT ROW 1.8 COL 19 COLON-ALIGNED NO-LABEL
     RP.Int2 AT ROW 2.8 COL 9.29 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1.05
     fil_Property AT ROW 2.8 COL 19 COLON-ALIGNED NO-LABEL
     RP.Char1 AT ROW 5.2 COL 2.14 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Print Rental Advice and A/P Form", "Both":U,
"Print Rental Advice Only", "Advice":U,
"Print A/P Form Only", "AP":U
          SIZE 26.29 BY 2.4
          FONT 10
     RP.Log1 AT ROW 5.2 COL 36.43
          LABEL "Advice is not a GST Tax Invoice"
          VIEW-AS TOGGLE-BOX
          SIZE 25.72 BY 1
     RP.Log2 AT ROW 6 COL 36.43
          LABEL "Closing Paragraph"
          VIEW-AS TOGGLE-BOX
          SIZE 16 BY .85
     RP.Log3 AT ROW 6.7 COL 36.43
          LABEL "Charges based on Rental Spaces"
          VIEW-AS TOGGLE-BOX
          SIZE 26.86 BY .85
     cmb_BankAccount AT ROW 7.8 COL 13.14 COLON-ALIGNED NO-LABEL
     RP.Date1 AT ROW 9 COL 13.14 COLON-ALIGNED HELP
          "" NO-LABEL FORMAT "99/99/9999"
          VIEW-AS FILL-IN 
          SIZE 11.57 BY 1
          FONT 10
     RP.Date2 AT ROW 9 COL 51.57 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 12 BY 1
          FONT 10
     RP.Log4 AT ROW 10 COL 15
          LABEL "Use historical rent charge information"
          VIEW-AS TOGGLE-BOX
          SIZE 28 BY .85
     btn_print AT ROW 10.5 COL 54.14
     RECT-1 AT ROW 1 COL 1
     "Commence Date:" VIEW-AS TEXT
          SIZE 13 BY 1 AT ROW 9 COL 2.14
          FONT 10
     "Bank Account:" VIEW-AS TEXT
          SIZE 11 BY 1 AT ROW 7.8 COL 2.14
          FONT 10
     "Advice Date:" VIEW-AS TEXT
          SIZE 10 BY 1 AT ROW 9 COL 42.14
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_print.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 17.25
         WIDTH              = 69.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR RADIO-SET RP.Char3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR COMBO-BOX cmb_BankAccount IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Date1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Date2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_Property IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Tenant IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.  
  SELF:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN options-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN entity-type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_BankAccount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_BankAccount V-table-Win
ON U1 OF cmb_BankAccount IN FRAME F-Main
DO:
  {inc/selcmb/scbnk1.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_BankAccount V-table-Win
ON U2 OF cmb_BankAccount IN FRAME F-Main
DO:
  {inc/selcmb/scbnk2.i "RP" "Char2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U1 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U2 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U3 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Tenant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U1 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U2 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U3 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Int1 */
DO:
  {inc/selcde/cdtnt.i "fil_Tenant"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* Int2 */
DO:
  {inc/selcde/cdpro.i "fil_Property"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-type-changed V-table-Win 
PROCEDURE entity-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  CASE INPUT RP.Char3:
    WHEN "P" THEN DO:
      HIDE RP.Int1.
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_property:HANDLE ), "HIDDEN = No" ).
      VIEW RP.Int2.
    END.
    OTHERWISE DO:
      HIDE RP.Int2.
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_property:HANDLE ), "HIDDEN = Yes" ).
      RUN set-link-attributes IN sys-mgr ( THIS-PROCEDURE, STRING( fil_tenant:HANDLE ), "HIDDEN = No" ).
      VIEW RP.Int1.
    END.
  END CASE.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN entity-type-changed.
  RUN options-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:  Initialisation
------------------------------------------------------------------------------*/
DEF VAR yy AS INT NO-UNDO.
DEF VAR mm AS INT NO-UNDO.

  RUN get-username IN sec-mgr ( OUTPUT user-name ).
  FIND RP WHERE RP.ReportID = {&REPORT-ID}
                        AND RP.UserName = user-name
                        NO-ERROR.

  IF NOT AVAILABLE( RP ) THEN DO:

    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Char1 = "Both" .
  END.

  yy = YEAR( TODAY ).
  mm = MONTH( TODAY ).

  yy = yy + IF mm = 12 THEN 1 ELSE 0.
  mm = ( mm MODULO 12 ) + 1.

  RP.Date1 = DATE( mm, 1, yy ).
  RP.Date2 = TODAY.

  RUN dispatch ( 'display-fields':U ).
  RUN dispatch ( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE options-changed V-table-Win 
PROCEDURE options-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR opt AS CHAR NO-UNDO.

  DO WITH FRAME {&FRAME-NAME}:
    
    opt = INPUT RP.Char1.
    cmb_BankAccount:SENSITIVE = LOOKUP( opt, "Both,AP" ) <> 0.

    IF LOOKUP( opt, "Both,Advice" ) <> 0 AND Office.GST <> ? THEN
      VIEW RP.Log1 RP.Log2.
    ELSE
      HIDE RP.Log1 RP.Log2.
  END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/* Prior to destroying the window */

  RUN check-modified( 'clear':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-advice V-table-Win 
PROCEDURE print-advice :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER tenant-code AS INT NO-UNDO.

  RUN process/report/rntladvc.p ( "TenantCode," + STRING(tenant-code)
                              + "~nCommencing," + STRING(RP.Date1,"99/99/9999")
                              + "~nAdviceDate," + STRING(RP.Date2,"99/99/9999")
                              + (IF RP.Log4 THEN "~nUsePastCharges" ELSE "")
                              + (IF RP.Log3 THEN "~nUseRentalSpaces" ELSE "")
                              + (IF RP.Log2 THEN "~nClosingParagraph" ELSE "")
                              + (IF RP.Log1 THEN "~nNotTaxInvoice" ELSE "") ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-ap V-table-Win 
PROCEDURE print-ap :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER tenant-code AS INT NO-UNDO.

DEF VAR report-options AS CHAR NO-UNDO.
  report-options = "TenantCode," + STRING(tenant-code)
                 + "~nBankAccount," + RP.Char2
                 + (IF RP.Log3 THEN "~nUseRentalSpaces" ELSE "")
                 + "~nCommencing," + STRING(RP.Date1,"99/99/9999").

{inc/ofc-set.i "Payment-Authority-Program" "ap-form"}
  IF NOT AVAILABLE(OfficeSetting) THEN
    ap-form = "process/report/apform.p".

  RUN VALUE(ap-form) ( report-options ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:  Run the AP Form report
------------------------------------------------------------------------------*/

  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  RUN dispatch ( 'update-record':U ).

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  IF RP.Char3 = "T" THEN DO:
    IF LOOKUP( RP.Char1, "Both,Advice" ) <> 0 THEN     RUN print-advice( RP.Int1 ).
    IF LOOKUP( RP.Char1, "Both,AP" ) <> 0 THEN         RUN print-ap( RP.Int1 ).
  END.
  ELSE DO:
    FOR EACH Tenant NO-LOCK WHERE Tenant.Active
                      AND Tenant.EntityType = "P"
                      AND Tenant.EntityCode = RP.Int2
                      AND CAN-FIND( FIRST TenancyLease OF Tenant WHERE TenancyLease.LeaseStatus <> "PAST"):
      IF LOOKUP( RP.Char1, "Both,Advice" ) <> 0 THEN     RUN print-advice( Tenant.TenantCode ).
      IF LOOKUP( RP.Char1, "Both,AP" ) <> 0 THEN         RUN print-ap( Tenant.TenantCode ).
    END.
  END.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FIND Tenant WHERE ROWID( Tenant ) =
    TO-ROWID( fil_Tenant:PRIVATE-DATA IN FRAME {&FRAME-NAME} ) NO-LOCK NO-ERROR.
    
  IF INPUT RP.Char3 = "T" AND NOT AVAILABLE Tenant THEN DO:
    MESSAGE "You must select a Tenant" VIEW-AS ALERT-BOX
      ERROR TITLE "No Tenant selected".
    APPLY 'ENTRY':U TO RP.Int1 IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

