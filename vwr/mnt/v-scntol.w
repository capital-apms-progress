&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Scenario
&Scoped-define FIRST-EXTERNAL-TABLE Scenario


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Scenario.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 tgl_NewScenario fil_OtherCode ~
fil_NewName rs_What Btn_OK 
&Scoped-Define DISPLAYED-FIELDS Scenario.ScenarioCode Scenario.Name 
&Scoped-Define DISPLAYED-OBJECTS tgl_NewScenario fil_OtherCode ~
fil_OtherName fil_NewName rs_What 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "&OK" 
     SIZE 12 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE fil_NewName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Named" 
     VIEW-AS FILL-IN 
     SIZE 58.29 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_OtherCode AS INTEGER FORMAT ">>>>9":U INITIAL 0 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 5.72 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_OtherName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 48.57 BY 1.05 NO-UNDO.

DEFINE VARIABLE rs_What AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Manual flows", "Manual",
"Rentals", "Rental",
"Service Contracts", "Contract",
"Estimates", "Estimated",
"All cash flows", "All"
     SIZE 20 BY 4 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 65.14 BY 11.2.

DEFINE VARIABLE tgl_NewScenario AS LOGICAL INITIAL no 
     LABEL "Create New Scenario" 
     VIEW-AS TOGGLE-BOX
     SIZE 17.72 BY .8 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Scenario.ScenarioCode AT ROW 1.4 COL 5.29 COLON-ALIGNED
          LABEL "From"
          VIEW-AS FILL-IN 
          SIZE 5.72 BY 1.05
     Scenario.Name AT ROW 1.4 COL 11 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 52.57 BY 1.05
     tgl_NewScenario AT ROW 3.4 COL 7.29
     fil_OtherCode AT ROW 4.6 COL 5.29 COLON-ALIGNED
     fil_OtherName AT ROW 4.6 COL 15 COLON-ALIGNED NO-LABEL
     fil_NewName AT ROW 4.8 COL 5.29 COLON-ALIGNED
     rs_What AT ROW 7.6 COL 6.72 NO-LABEL
     Btn_OK AT ROW 11 COL 53.57
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.Scenario
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 12.95
         WIDTH              = 73.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fil_OtherName IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Scenario.Name IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN Scenario.ScenarioCode IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK V-table-Win
ON CHOOSE OF Btn_OK IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN VALUE( mode + "-scenario" ).
  IF VALID-HANDLE(SELF) THEN SELF:SENSITIVE = Yes.
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_OtherCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_OtherCode V-table-Win
ON LEAVE OF fil_OtherCode IN FRAME F-Main /* To */
DO:
  {inc/selcde/cdscn.i "fil_OtherName"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_OtherName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_OtherName V-table-Win
ON U1 OF fil_OtherName IN FRAME F-Main
DO:
  {inc/selfil/sfscn1.i "?" "fil_OtherCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_OtherName V-table-Win
ON U2 OF fil_OtherName IN FRAME F-Main
DO:
  {inc/selfil/sfscn2.i "?" "fil_OtherCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_OtherName V-table-Win
ON U3 OF fil_OtherName IN FRAME F-Main
DO:
  {inc/selfil/sfscn3.i "?" "fil_OtherCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_NewScenario
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_NewScenario V-table-Win
ON VALUE-CHANGED OF tgl_NewScenario IN FRAME F-Main /* Create New Scenario */
DO:
  RUN dispatch( 'enable-fields':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Scenario"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Scenario"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copy-scenario V-table-Win 
PROCEDURE copy-scenario :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER ToScenario FOR Scenario.
DEF BUFFER ToFlow FOR CashFlow.
DEF BUFFER ToParameter FOR ScenarioParameter.
DEF BUFFER LastScenario FOR Scenario.

  RUN notify( 'set-busy,container-source':U ).
  ASSIGN FRAME {&FRAME-NAME} tgl_NewScenario fil_NewName fil_OtherCode rs_What.
  IF tgl_NewScenario THEN DO:
    FIND LAST LastScenario NO-LOCK NO-ERROR.
    CREATE ToScenario.
    ToScenario.ScenarioCode = (IF AVAILABLE(LastScenario) THEN LastScenario.ScenarioCode ELSE 0) + 1.
    ToScenario.Name = fil_NewName.
  END.
  ELSE DO:
    FIND ToScenario WHERE ToScenario.ScenarioCode = fil_OtherCode EXCLUSIVE-LOCK NO-ERROR.
    IF NOT AVAILABLE(ToScenario) THEN DO:
      RUN notify( 'set-idle,container-source':U ).
      MESSAGE "Target Scenario not available".
      RETURN.
    END.
  END.
  BUFFER-COPY Scenario EXCEPT ScenarioCode Name TO ToScenario .
  FOR EACH ToParameter OF ToScenario:
    DELETE ToParameter.
  END.
  FOR EACH ScenarioParameter OF Scenario:
    CREATE ToParameter.
    BUFFER-COPY ScenarioParameter TO ToParameter
                ASSIGN ToParameter.ScenarioCode = ToScenario.ScenarioCode.
  END.

  FOR EACH ToFlow OF ToScenario:
    DELETE ToFlow.
  END.
  FOR EACH CashFlow OF Scenario:
    CREATE ToFlow.
    BUFFER-COPY CashFlow TO ToFlow
                ASSIGN ToFlow.ScenarioCode = ToScenario.ScenarioCode.
  END.
  RUN notify( 'set-idle,container-source':U ).
  MESSAGE "Copy Complete".
  RUN notify( 'open-query,record-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT tgl_NewScenario THEN DO:
    VIEW fil_NewName .
    HIDE fil_OtherCode fil_OtherName .
  END.
  ELSE DO:
    HIDE fil_NewName .
    VIEW fil_OtherCode fil_OtherName .
  END.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN dispatch( 'enable-fields':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Scenario"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


