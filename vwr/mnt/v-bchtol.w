&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Batch
&Scoped-define FIRST-EXTERNAL-TABLE Batch


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Batch.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-32 fil_bchdesc tgl_date fil_Date ~
tgl_ref fil_Ref tgl_desc fil_desc tgl_reverse btn_createcopy btn_cancel 
&Scoped-Define DISPLAYED-OBJECTS fil_bchdesc tgl_date fil_Date tgl_ref ~
fil_Ref tgl_desc fil_desc tgl_reverse 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_cancel 
     LABEL "Cancel" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_createcopy 
     LABEL "Create &Copy" 
     SIZE 12 BY 1.05
     FONT 10.

DEFINE VARIABLE fil_bchdesc AS CHARACTER FORMAT "X(256)":U 
     LABEL "New Description" 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Date AS DATE FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1 NO-UNDO.

DEFINE VARIABLE fil_desc AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Ref AS CHARACTER FORMAT "X(12)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-32
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 64.57 BY 8.7.

DEFINE VARIABLE tgl_date AS LOGICAL INITIAL no 
     LABEL "Date" 
     VIEW-AS TOGGLE-BOX
     SIZE 6.86 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_desc AS LOGICAL INITIAL no 
     LABEL "Description" 
     VIEW-AS TOGGLE-BOX
     SIZE 10.86 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_ref AS LOGICAL INITIAL no 
     LABEL "Reference" 
     VIEW-AS TOGGLE-BOX
     SIZE 10.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_reverse AS LOGICAL INITIAL no 
     LABEL "Reverse Transactions" 
     VIEW-AS TOGGLE-BOX
     SIZE 18.29 BY 1
     FONT 10 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_bchdesc AT ROW 2.5 COL 13 COLON-ALIGNED
     tgl_date AT ROW 5.4 COL 3.29
     fil_Date AT ROW 5.4 COL 13 COLON-ALIGNED NO-LABEL
     tgl_ref AT ROW 6.5 COL 3.29
     fil_Ref AT ROW 6.5 COL 13 COLON-ALIGNED NO-LABEL
     tgl_desc AT ROW 7.6 COL 3.29
     fil_desc AT ROW 7.6 COL 13 COLON-ALIGNED NO-LABEL
     tgl_reverse AT ROW 8.7 COL 3.29
     btn_createcopy AT ROW 10.4 COL 1
     btn_cancel AT ROW 10.4 COL 55.29
     "Modifications" VIEW-AS TEXT
          SIZE 13.14 BY .6 AT ROW 4.5 COL 2.72
          FONT 14
     RECT-32 AT ROW 1.5 COL 1
     "Posted Batch Tool" VIEW-AS TEXT
          SIZE 21.14 BY 1 AT ROW 1 COL 2.14
          FONT 13
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.Batch
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 10.65
         WIDTH              = 74.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_createcopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_createcopy V-table-Win
ON CHOOSE OF btn_createcopy IN FRAME F-Main /* Create Copy */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN copy-batch.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_date
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_date V-table-Win
ON VALUE-CHANGED OF tgl_date IN FRAME F-Main /* Date */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_desc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_desc V-table-Win
ON VALUE-CHANGED OF tgl_desc IN FRAME F-Main /* Description */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_ref
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_ref V-table-Win
ON VALUE-CHANGED OF tgl_ref IN FRAME F-Main /* Reference */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Batch"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Batch"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copy-batch V-table-Win 
PROCEDURE copy-batch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

ASSIGN FRAME {&FRAME-NAME}
  fil_bchdesc fil_date fil_ref fil_desc
  tgl_date tgl_ref tgl_desc tgl_reverse.

/*
ON CREATE OF NewAcctTrans OVERRIDE DO: END.
ON CREATE OF NewAcctTrans OVERRIDE DO: END.
*/
DEF VAR tr-num AS INTEGER NO-UNDO.       

  DEF BUFFER CopyBatch FOR NewBatch.
  DEF BUFFER CopyDoc   FOR NewDocument.
  DEF BUFFER CopyTrn   FOR NewAcctTrans.

  DO TRANSACTION:
    CREATE CopyBatch.
    ASSIGN  CopyBatch.Description = IF fil_bchdesc <> "" THEN fil_bchdesc ELSE Batch.Description
            CopyBatch.BatchType = "NORM" .
  END.
    
  FOR EACH Document OF Batch WHERE Document.DocumentType <> "ICOA" NO-LOCK:

    DO TRANSACTION:
      CREATE CopyDoc.
      ASSIGN  CopyDoc.BatchCode    = CopyBatch.BatchCode
              CopyDoc.DocumentCode = Document.DocumentCode
              CopyDoc.DocumentType = Document.DocumentType
              CopyDoc.Reference   = IF tgl_ref  THEN fil_ref  ELSE Document.Reference
              CopyDoc.Description = IF tgl_desc THEN fil_desc ELSE Document.Description.
    END.

    tr-num = 0.
    FOR EACH AcctTran OF Document WHERE AcctTran.ConsequenceOf = 0 NO-LOCK:
      tr-num = tr-num + 1.
      DO TRANSACTION:
        CREATE CopyTrn.
        ASSIGN
          CopyTrn.BatchCode     = CopyBatch.BatchCode
          CopyTrn.DocumentCode  = CopyDoc.DocumentCode
/*          CopyTrn.TransactionCode = tr-num */ /* auto assigned by trigger */
          CopyTrn.EntityType    = AcctTran.EntityType
          CopyTrn.EntityCode    = AcctTran.EntityCode
          CopyTrn.AccountCode   = AcctTran.AccountCode
          CopyTrn.MonthCode     = ?
          CopyTrn.Date        = IF tgl_date    THEN fil_date ELSE AcctTran.Date
          CopyTrn.Description = IF tgl_desc    THEN "" ELSE AcctTran.Description
          CopyTrn.Reference   = IF tgl_ref     THEN "" ELSE AcctTran.Reference
          CopyTrn.Amount      = ( AcctTran.Amount * IF tgl_reverse THEN -1 ELSE 1 )
            * IF LOOKUP( Document.DocumentType, "RCPT" ) <> 0 THEN -1 ELSE 1.
      END.
    END.
  END.  

  MESSAGE "The copy of batch" Batch.BatchCode "- Posted Batch" CopyBatch.BatchCode SKIP
    "was successfully created." VIEW-AS ALERT-BOX INFORMATION.
    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT tgl_Date THEN
    ENABLE fil_Date.
  ELSE
    DISABLE fil_Date.

  IF INPUT tgl_Ref THEN
    ENABLE fil_Ref.
  ELSE
    DISABLE fil_Ref.

  IF INPUT tgl_Desc THEN
    ENABLE fil_Desc.
  ELSE
    DISABLE fil_Desc.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  ASSIGN fil_bchdesc = Batch.Description.
  DISPLAY fil_bchdesc WITH FRAME {&FRAME-NAME}.

  RUN enable-appropriate-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Batch"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


