&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&SCOPED-DEFINE REPORT-ID "Expense Variance Report"

DEF VAR manager-list        AS CHAR NO-UNDO.
DEF VAR manager-pd          AS CHAR NO-UNDO.
DEF VAR administrator-list  AS CHAR NO-UNDO.
DEF VAR administrator-pd    AS CHAR NO-UNDO.
DEF VAR region-list         AS CHAR NO-UNDO.
DEF VAR region-pd           AS CHAR NO-UNDO.

DEF VAR consolidation-list  AS CHAR NO-UNDO.
DEF VAR consolidation-pd    AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Int1 RP.Int5 RP.Int2 RP.Int6 ~
RP.Char2 RP.Log1 RP.Log2 RP.Dec1 RP.Dec2 RP.Log3 RP.Char3 RP.Log4 RP.Char4 
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-define DISPLAYED-TABLES RP
&Scoped-define FIRST-DISPLAYED-TABLE RP
&Scoped-Define ENABLED-OBJECTS cmb_Select cmb_AccountGroups cmb_month ~
cmb_month2 btn_Browse btn_print RECT-30 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Int1 RP.Int5 RP.Int2 RP.Int6 ~
RP.Char2 RP.Log1 RP.Log2 RP.Dec1 RP.Dec2 RP.Log3 RP.Char3 RP.Log4 RP.Char4 
&Scoped-Define DISPLAYED-OBJECTS cmb_Select fil_PropertyFrom ~
fil_CompanyFrom fil_PropertyTo fil_CompanyTo cmb_AccountGroups cmb_month ~
cmb_month2 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Browse 
     LABEL "Browse" 
     SIZE 7 BY 1.

DEFINE BUTTON btn_print 
     LABEL "&OK" 
     SIZE 9 BY 1
     FONT 9.

DEFINE VARIABLE cmb_month AS CHARACTER FORMAT "X(256)":U 
     LABEL "Month Ending" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_month2 AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 14.86 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Select AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 50.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_CompanyFrom AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 41.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_CompanyTo AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 41.14 BY 1 NO-UNDO.

DEFINE VARIABLE fil_PropertyFrom AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 38.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_PropertyTo AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 38.86 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-30
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 78.29 BY 22.2.

DEFINE VARIABLE cmb_AccountGroups AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
     SIZE 63.43 BY 7 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 2 COL 2.72 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Building Manager", "Manager":U,
"Consolidation List", "List":U,
"Region", "Region":U,
"Property Range (Properties)", "Property":U,
"Property Range (Tenants)", "Tenant":U,
"Company Range", "Company":U
          SIZE 21.72 BY 4.8
     cmb_Select AT ROW 2.4 COL 26.43 COLON-ALIGNED NO-LABEL
     RP.Int1 AT ROW 4.2 COL 26.43 COLON-ALIGNED
          LABEL "From" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_PropertyFrom AT ROW 4.2 COL 35.57 COLON-ALIGNED NO-LABEL
     RP.Int5 AT ROW 4.8 COL 26.43 COLON-ALIGNED
          LABEL "From" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_CompanyFrom AT ROW 4.8 COL 35.57 COLON-ALIGNED NO-LABEL
     RP.Int2 AT ROW 5.2 COL 26.43 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_PropertyTo AT ROW 5.2 COL 35.57 COLON-ALIGNED NO-LABEL
     RP.Int6 AT ROW 5.8 COL 26.43 COLON-ALIGNED
          LABEL "To" FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6 BY 1
     fil_CompanyTo AT ROW 5.8 COL 35.57 COLON-ALIGNED NO-LABEL
     cmb_AccountGroups AT ROW 7 COL 15.29 NO-LABEL
     RP.Char2 AT ROW 14.2 COL 15.29 NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "Detail for each Property", "Detailed":U,
"Consolidate By Account", "Account":U,
"Consolidate By Property", "Property":U
          SIZE 62.86 BY 1.1
     RP.Log1 AT ROW 15.2 COL 15.29
          LABEL "Consolidate sub-accounts"
          VIEW-AS TOGGLE-BOX
          SIZE 20.57 BY 1.05
     RP.Log2 AT ROW 16 COL 15.29
          LABEL "Focus on account code range"
          VIEW-AS TOGGLE-BOX
          SIZE 23.43 BY 1
     RP.Dec1 AT ROW 16 COL 40.72 COLON-ALIGNED HELP
          ""
          LABEL "from" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
     RP.Dec2 AT ROW 16 COL 53.29 COLON-ALIGNED HELP
          ""
          LABEL "to" FORMAT "9999.99"
          VIEW-AS FILL-IN 
          SIZE 7.43 BY 1
     RP.Log3 AT ROW 16.8 COL 15.29
          LABEL "Exclude vacant space"
          VIEW-AS TOGGLE-BOX
          SIZE 21.14 BY .85
     RP.Char3 AT ROW 18.2 COL 15.29 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Normal", "Normal":U,
"For a range of months", "RANGE":U
          SIZE 18.29 BY 1.6
     cmb_month AT ROW 18.2 COL 32.43
     cmb_month2 AT ROW 18.2 COL 60.57
     RP.Log4 AT ROW 20.6 COL 2.72 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "File", ?,
"Printer", no,
"Preview", yes
          SIZE 8.57 BY 2.4
     RP.Char4 AT ROW 20.6 COL 9.29 COLON-ALIGNED NO-LABEL FORMAT "X(256)"
          VIEW-AS FILL-IN 
          SIZE 60 BY 1.05
     btn_Browse AT ROW 20.6 COL 71.29
     btn_print AT ROW 22 COL 69.57
     RECT-30 AT ROW 1 COL 1
     "Report Period:" VIEW-AS TEXT
          SIZE 10.29 BY .8 AT ROW 18.2 COL 2.14
     "Report Style:" VIEW-AS TEXT
          SIZE 9 BY 1 AT ROW 14.2 COL 2.14
     "Account Groups:" VIEW-AS TEXT
          SIZE 12.57 BY 1 AT ROW 7 COL 2.14
.
/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME F-Main
     "Report on a:" VIEW-AS TEXT
          SIZE 8.57 BY .8 AT ROW 1.2 COL 1.57
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 23.45
         WIDTH              = 88.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN RP.Char4 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR COMBO-BOX cmb_month IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX cmb_month2 IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN RP.Dec1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN RP.Dec2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN fil_CompanyFrom IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_CompanyTo IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_PropertyFrom IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_PropertyTo IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int2 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int5 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR FILL-IN RP.Int6 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log2 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR RADIO-SET RP.Log4 IN FRAME F-Main
   EXP-HELP                                                             */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Browse V-table-Win
ON CHOOSE OF btn_Browse IN FRAME F-Main /* Browse */
DO:
  RUN select-file-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
  {&SELF-NAME}:SENSITIVE = No.
  RUN run-report.
  {&SELF-NAME}:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN property-selection-changed.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char2 V-table-Win
ON VALUE-CHANGED OF RP.Char2 IN FRAME F-Main /* Char2 */
DO:
  RUN report-style-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char3 V-table-Win
ON VALUE-CHANGED OF RP.Char3 IN FRAME F-Main /* Char3 */
DO:
  RUN report-type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_AccountGroups
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AccountGroups V-table-Win
ON U1 OF cmb_AccountGroups IN FRAME F-Main
DO:
  {inc/selcmb/sel-acg1.i "RP" "Char6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AccountGroups V-table-Win
ON U2 OF cmb_AccountGroups IN FRAME F-Main
DO:
  {inc/selcmb/sel-acg2.i "RP" "Char6"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month V-table-Win
ON U1 OF cmb_month IN FRAME F-Main /* Month Ending */
DO:
  CASE INPUT RP.Char3:
    WHEN "NORMAL" THEN {inc/selcmb/scmthe1.i "RP" "Int3"}    
    WHEN "RANGE"  THEN {inc/selcmb/scmths1.i "RP" "Int3"}    
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month V-table-Win
ON U2 OF cmb_month IN FRAME F-Main /* Month Ending */
DO:
  CASE INPUT RP.Char3:
    WHEN "NORMAL" THEN {inc/selcmb/scmthe2.i "RP" "Int3"}    
    WHEN "RANGE"  THEN {inc/selcmb/scmths2.i "RP" "Int3"}    
  END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_month2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U1 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int4"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_month2 V-table-Win
ON U2 OF cmb_month2 IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int4"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_CompanyFrom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_CompanyFrom V-table-Win
ON U1 OF fil_CompanyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "RP" "Int5"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_CompanyFrom V-table-Win
ON U2 OF fil_CompanyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "RP" "Int5"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_CompanyFrom V-table-Win
ON U3 OF fil_CompanyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "RP" "Int5"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_CompanyTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_CompanyTo V-table-Win
ON U1 OF fil_CompanyTo IN FRAME F-Main
DO:
  {inc/selfil/sfcmp1.i "RP" "Int6"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_CompanyTo V-table-Win
ON U2 OF fil_CompanyTo IN FRAME F-Main
DO:
  {inc/selfil/sfcmp2.i "RP" "Int6"}      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_CompanyTo V-table-Win
ON U3 OF fil_CompanyTo IN FRAME F-Main
DO:
  {inc/selfil/sfcmp3.i "RP" "Int6"}      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_PropertyFrom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyFrom V-table-Win
ON U1 OF fil_PropertyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int1"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyFrom V-table-Win
ON U2 OF fil_PropertyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyFrom V-table-Win
ON U3 OF fil_PropertyFrom IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int1"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_PropertyTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyTo V-table-Win
ON U1 OF fil_PropertyTo IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int2"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyTo V-table-Win
ON U2 OF fil_PropertyTo IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int2"}      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PropertyTo V-table-Win
ON U3 OF fil_PropertyTo IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int2"}      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* From */
DO:
  {inc/selcde/cdpro.i "fil_PropertyFrom"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int2 V-table-Win
ON LEAVE OF RP.Int2 IN FRAME F-Main /* To */
DO:
  {inc/selcde/cdpro.i "fil_PropertyTo"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int5 V-table-Win
ON LEAVE OF RP.Int5 IN FRAME F-Main /* From */
DO:
  {inc/selcde/cdcmp.i "fil_CompanyFrom"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int6 V-table-Win
ON LEAVE OF RP.Int6 IN FRAME F-Main /* To */
DO:
  {inc/selcde/cdcmp.i "fil_CompanyTo"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Focus on account code range */
DO:
  RUN focus-accounts-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log4 V-table-Win
ON VALUE-CHANGED OF RP.Log4 IN FRAME F-Main /* Log4 */
DO:
  RUN output-type-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "RP"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "RP"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE focus-accounts-changed V-table-Win 
PROCEDURE focus-accounts-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Log2 THEN
    VIEW RP.Dec1 RP.Dec2.
  ELSE
    HIDE RP.Dec1 RP.Dec2.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-administrators V-table-Win 
PROCEDURE get-administrators :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF administrator-list <> "" THEN RETURN.
  
  FOR EACH Property WHERE Property.Active NO-LOCK
    BREAK BY Property.Administrator:
    IF FIRST-OF( Property.Administrator ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Administrator
        NO-LOCK NO-ERROR.

      administrator-list = administrator-list + IF administrator-list = "" THEN "" ELSE ",".
      administrator-list = administrator-list +
        ( IF AVAILABLE Person THEN Person.FirstName + ' ' + Person.LastName ELSE "Not Managed" ).

      administrator-pd = administrator-pd + IF administrator-pd = "" THEN "" ELSE ",".
      administrator-pd = administrator-pd + IF AVAILABLE Person THEN STRING( Person.PersonCode ) ELSE STRING( Property.Administrator ).
    END.
  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-consolidation V-table-Win 
PROCEDURE get-consolidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF consolidation-list <> "" THEN RETURN.
  
  FOR EACH ConsolidationList NO-LOCK:

    consolidation-list = consolidation-list + IF consolidation-list = "" THEN "" ELSE ",".
    consolidation-list = consolidation-list + ConsolidationList.Description.

    consolidation-pd = consolidation-pd + IF consolidation-pd = "" THEN "" ELSE ",".
    consolidation-pd = consolidation-pd + ConsolidationList.Name.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-managers V-table-Win 
PROCEDURE get-managers :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF manager-list <> "" THEN RETURN.
  
  FOR EACH Property WHERE Property.Active NO-LOCK
    BREAK BY Property.Manager:
    IF FIRST-OF( Property.Manager ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Manager
        NO-LOCK NO-ERROR.

      manager-list = manager-list + IF manager-list = "" THEN "" ELSE ",".
      manager-list = manager-list +
        (IF AVAILABLE Person THEN
            Person.FirstName + " " + Person.LastName + " - " + Person.JobTitle
         ELSE
            (IF Property.Manager > 0 THEN
               "Deleted Person " + STRING(Property.Manager)
             ELSE
               "No Building Manager Assigned") ).

      manager-pd = manager-pd + IF manager-pd = "" THEN "" ELSE ",".
      manager-pd = manager-pd + IF AVAILABLE Person THEN STRING( Person.PersonCode ) ELSE STRING( Property.Manager ).
    END.
  
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-regions V-table-Win 
PROCEDURE get-regions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF region-list <> "" THEN RETURN.
  
  FOR EACH Region NO-LOCK:

    region-list = region-list + IF region-list = "" THEN "" ELSE ",".
    region-list = region-list + Region.Name.

    region-pd = region-pd + IF region-pd = "" THEN "" ELSE ",".
    region-pd = region-pd + Region.Region.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:  Enable/disable filename for spreadsheet output
------------------------------------------------------------------------------*/
  RUN focus-accounts-changed.
  RUN output-type-changed.
  RUN report-type-changed.
  RUN report-style-changed.
  RUN property-selection-changed.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR user-name AS CHAR NO-UNDO.

  RUN get-username IN sys-mgr( OUTPUT user-name ).
  
  FIND RP WHERE
    RP.UserName = user-name AND
    RP.ReportID = {&REPORT-ID} NO-ERROR.
    
  IF NOT AVAILABLE RP THEN
  DO WITH FRAME {&FRAME-NAME}:
  
    DEF VAR first-property LIKE Property.PropertyCode NO-UNDO.
    DEF VAR last-property  LIKE Property.PropertyCode NO-UNDO.
    
    FIND FIRST Property NO-LOCK. first-property = Property.PropertyCode.
    FIND LAST  Property NO-LOCK. last-property  = Property.PropertyCode.

    CREATE RP.
    ASSIGN
      RP.ReportID = {&REPORT-ID}
      RP.UserName = user-name
      RP.Int1     = first-property
      RP.Int2     = last-property.
  END.

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE output-type-changed V-table-Win 
PROCEDURE output-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Log4 = ? THEN
    VIEW RP.Char4 btn_Browse .
  ELSE
    HIDE RP.Char4 btn_Browse .
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE property-selection-changed V-table-Win 
PROCEDURE property-selection-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT RP.Char1.
  
  CASE type:
    WHEN "Property" THEN.
    WHEN "Tenant" THEN.
    WHEN "Company" THEN.
    WHEN "MANAGER" THEN
    DO:
      RUN get-managers.
      cmb_Select:LIST-ITEMS   = manager-list.
      cmb_Select:PRIVATE-DATA = manager-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, manager-list ).
    END.
    WHEN "ADMINISTRATOR" THEN
    DO:
      RUN get-administrators.
      cmb_Select:LIST-ITEMS   = administrator-list.
      cmb_Select:PRIVATE-DATA = administrator-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, administrator-list ).
    END.
    WHEN "LIST" THEN
    DO:
      RUN get-consolidation.
      cmb_Select:LIST-ITEMS   = consolidation-list.
      cmb_Select:PRIVATE-DATA = consolidation-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, consolidation-list ).
    END.
    WHEN "REGION" THEN
    DO:
      RUN get-regions.
      cmb_Select:LIST-ITEMS   = region-list.
      cmb_Select:PRIVATE-DATA = region-pd.
      cmb_Select:SCREEN-VALUE = ENTRY( 1, region-list ).
    END.
  
  END CASE.

  /* Enable fields as appropriate */
  DEF VAR sens-attrib-p AS CHAR NO-UNDO.
  DEF VAR sens-attrib-c AS CHAR NO-UNDO.
  DEF VAR select-type AS CHAR NO-UNDO.

  IF type = "Property" OR type = "Tenant" THEN
    select-type = "Property".
  ELSE IF type = "Company" THEN
    select-type = "Company".
  ELSE
    select-type = "List".

  CASE select-type:
    WHEN "Property" THEN DO:
      HIDE RP.Int5 RP.Int6 fil_CompanyFrom fil_CompanyTo cmb_Select .
      VIEW RP.Int1 RP.Int2 fil_PropertyFrom fil_PropertyTo .
      sens-attrib-c = "HIDDEN = Yes".
      sens-attrib-p = "HIDDEN = No".
      ENABLE RP.Int1 RP.Int2 .
    END.
    WHEN "Company" THEN DO:
      HIDE RP.Int1 RP.Int2 fil_PropertyFrom fil_PropertyTo cmb_Select .
      VIEW RP.Int5 RP.Int6 fil_CompanyFrom fil_CompanyTo .
      sens-attrib-c = "HIDDEN = No".
      sens-attrib-p = "HIDDEN = Yes".
      ENABLE RP.Int5 RP.Int6 .
    END.
    OTHERWISE DO:
      HIDE RP.Int1 RP.Int2 fil_PropertyFrom fil_PropertyTo
           RP.Int5 RP.Int6 fil_CompanyFrom fil_CompanyTo.
      VIEW cmb_Select .
      ENABLE cmb_Select .
      sens-attrib-c = "HIDDEN = Yes".
      sens-attrib-p = "HIDDEN = Yes".
    END.
  END CASE.
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_PropertyFrom:HANDLE ), sens-attrib-p ).
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_PropertyTo:HANDLE ), sens-attrib-p ).
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_CompanyFrom:HANDLE ), sens-attrib-c ).
  RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_CompanyTo:HANDLE ), sens-attrib-c ).

  IF type = "Tenant" THEN
    VIEW RP.Log3.
  ELSE
    HIDE RP.Log3.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-style-changed V-table-Win 
PROCEDURE report-style-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT RP.Char2 = "Detailed" OR INPUT RP.Char2 = "Account" THEN
    VIEW RP.Log1.
  ELSE
    HIDE RP.Log1.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-type-changed V-table-Win 
PROCEDURE report-type-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  cmb_month2:HIDDEN = INPUT RP.Char3 = "NORMAL".
  cmb_month:LABEL = IF INPUT RP.Char3 = "NORMAL" THEN "Month Ending" ELSE "From".
  /* Refresh the display list */
  APPLY 'U1':U TO cmb_month.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR month-1 AS INT NO-UNDO.
DEF VAR month-n AS INT NO-UNDO.
  
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR report-options AS CHAR NO-UNDO.
  DEF VAR selection-id   AS CHAR NO-UNDO.
  
  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
  
  RUN dispatch( 'update-record':U ).

  IF RP.Char3 = "Normal" THEN DO:
    month-n = RP.Int3.
    FIND Month WHERE Month.MonthCode = RP.Int3 NO-LOCK.
    FIND FinancialYear OF Month NO-LOCK.
    FIND FIRST Month OF FinancialYear NO-LOCK.
    month-1 = Month.MonthCode.
  END.
  ELSE ASSIGN
    month-1 = RP.Int3
    month-n = RP.Int4 .

  IF RP.Char1 = "Property" OR RP.Char1 = "Tenant" THEN
    selection-id = ( STRING( RP.Int1 ) + "," + STRING( RP.Int2 ) ).
  ELSE IF RP.Char1 = "Company" THEN
    selection-id = ( STRING( RP.Int5 ) + "," + STRING( RP.Int6 ) ).
  ELSE
    selection-id = ENTRY( cmb_Select:LOOKUP( cmb_Select:SCREEN-VALUE ), cmb_Select:PRIVATE-DATA ).

  IF RP.Char1 = "List" OR RP.Char1 = "Company" THEN
    selection-id = "Company," + RP.Char1 + "," + selection-id.
  ELSE IF RP.Char1 = "Tenant" THEN
    selection-id = "Tenant," + RP.Char1 + "," + selection-id.
  ELSE
    selection-id = "Property," + RP.Char1 + "," + selection-id.

  report-options = selection-id
                 + "~nAccountGroups," + RP.Char6
                 + (IF RP.Log2 THEN "~nAccountRange," + STRING(RP.Dec1) + "," + STRING(RP.Dec2) ELSE "")
                 + "~nStyle," + RP.Char2
                 + (IF RP.Log1 THEN "~nConsolidateSubs" ELSE "")
                 + (IF RP.Log3 THEN "~nNoVacantSpace" ELSE "")
                 + (IF RP.Log4 THEN "~nPreview" ELSE "")
                 + "~nMonth," + STRING( month-1 ) + "," + STRING( month-n )
                 + (IF RP.Log4 = ? THEN "~nFile," + RP.Char4 ELSE "") .
    
  IF RP.Log4 = No THEN DO:
    RUN make-bq-entry IN sys-mgr( "process/report/expense-variance.p", report-options, ?, ? ).
  END.
  ELSE DO:
    RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
    RUN process/report/expense-variance.p ( report-options ).
    RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-file-dialog V-table-Win 
PROCEDURE select-file-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR select-ok AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR start-dir AS CHAR NO-UNDO.

  file-name = INPUT FRAME {&FRAME-NAME} RP.Char4 .
  start-dir = SUBSTRING( file-name, 1, R-INDEX(file-name, "\" ) ).

  SYSTEM-DIALOG GET-FILE file-name FILTERS "Comma-separated fields" "*.CSV" ASK-OVERWRITE
        CREATE-TEST-FILE DEFAULT-EXTENSION ".CSV" INITIAL-DIR start-dir RETURN-TO-START-DIR 
        TITLE "" USE-FILENAME  UPDATE select-ok.

  IF select-ok THEN
    RP.Char4:SCREEN-VALUE IN FRAME {&FRAME-NAME} = file-name.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF INPUT FRAME {&FRAME-NAME} RP.Int1 >
     INPUT FRAME {&FRAME-NAME} RP.Int2 THEN
  DO:
    MESSAGE "The TO property must be greater than the from property !"
      VIEW-AS ALERT-BOX ERROR.
    APPLY 'ENTRY':U TO RP.Int1 IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END. 
   
  IF cmb_month:SCREEN-VALUE = "" OR cmb_month:SCREEN-VALUE = ? THEN
  DO:
    MESSAGE "You must select a month" VIEW-AS ALERT-BOX ERROR.
    APPLY 'ENTRY':U TO cmb_month IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


