&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

&SCOPED-DEFINE REPORT-ID "Year End Process"

DEF VAR first-month LIKE Month.MonthCode NO-UNDO.
DEF VAR last-month  LIKE Month.MonthCode NO-UNDO.
DEF VAR last-month-date AS DATE NO-UNDO.
DEF VAR shcap-seq   LIKE AccountGroup.SequenceCode  NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-acct.i "YEAREND" "ye-account" "ERROR"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS sel_cmp btn_all btn_none cmb_Month btn_run ~
RECT-29 
&Scoped-Define DISPLAYED-OBJECTS sel_cmp cmb_Month 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_all 
     LABEL "Select &All" 
     SIZE 9.14 BY 1.05
     FONT 9.

DEFINE BUTTON btn_none 
     LABEL "Select &None" 
     SIZE 9.72 BY 1.05
     FONT 9.

DEFINE BUTTON btn_run 
     LABEL "&Run" 
     SIZE 10 BY 1
     FONT 9.

DEFINE VARIABLE cmb_Month AS CHARACTER FORMAT "X(12)":U INITIAL ? 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 14.86 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-29
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 66.86 BY 19.5.

DEFINE VARIABLE sel_cmp AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
     SIZE 64.57 BY 16
     FONT 8 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     sel_cmp AT ROW 3 COL 2.72 NO-LABEL
     btn_all AT ROW 19.5 COL 2.72
     btn_none AT ROW 19.5 COL 13
     cmb_Month AT ROW 19.5 COL 50.43 COLON-ALIGNED NO-LABEL
     btn_run AT ROW 21.5 COL 58.72
     RECT-29 AT ROW 1.5 COL 1.57
     "Year End Routine" VIEW-AS TEXT
          SIZE 20.57 BY 1 AT ROW 1 COL 2.72
          FONT 13
     "Companies" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 2 COL 2.72
          FONT 10
     "Year Ending:" VIEW-AS TEXT
          SIZE 9.14 BY 1 AT ROW 19.5 COL 42.72
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 21.85
         WIDTH              = 68.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_all
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_all V-table-Win
ON CHOOSE OF btn_all IN FRAME F-Main /* Select All */
DO:
  sel_cmp:SCREEN-VALUE IN FRAME {&FRAME-NAME} = sel_cmp:LIST-ITEMS.
  RUN selection-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_none
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_none V-table-Win
ON CHOOSE OF btn_none IN FRAME F-Main /* Select None */
DO:
  sel_cmp:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
  RUN selection-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_run
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_run V-table-Win
ON CHOOSE OF btn_run IN FRAME F-Main /* Run */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN year-end.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Month
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month V-table-Win
ON VALUE-CHANGED OF cmb_Month IN FRAME F-Main
DO:
  RUN month-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_cmp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_cmp V-table-Win
ON VALUE-CHANGED OF sel_cmp IN FRAME F-Main
DO:
  RUN selection-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */
sel_cmp:DELIMITER IN FRAME {&FRAME-NAME} = CHR(9).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN refresh-companies.
  RUN refresh-year-ends.

  FIND AccountGroup WHERE AccountGroup.AccountGroupCode = "SHCAP"
    NO-LOCK NO-ERROR.
  
  IF NOT AVAILABLE AccountGroup THEN
  DO:
    MESSAGE "Cannot find account group 'SHCAP'" SKIP(1)
      "Exiting...."
      VIEW-AS ALERT-BOX ERROR.
    RUN dispatch( 'exit':U ).
  END.
  ELSE shcap-seq = AccountGroup.SequenceCode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE month-changed V-table-Win 
PROCEDURE month-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  first-month  = ?.
  last-month   = ?.
  FIND FIRST Month WHERE Month.EndDate = DATE( INPUT cmb_Month )
    NO-LOCK NO-ERROR.
  IF AVAILABLE Month THEN
  DO:
    last-month = Month.MonthCode.
    last-month-date = Month.EndDate.
    FIND FinancialYear OF Month NO-LOCK NO-ERROR.
    IF AVAILABLE FinancialYear THEN
    DO:
      FIND FIRST Month OF FinancialYear NO-LOCK NO-ERROR.
      first-month = Month.MonthCode.
    END.
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-companies V-table-Win 
PROCEDURE refresh-companies :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item AS CHAR NO-UNDO.
  
  FOR EACH Company NO-LOCK:
    item = STRING( Company.CompanyCode, "99999" ) + " - " + Company.LegalName.
    IF sel_Cmp:ADD-LAST( item ) THEN.  
  END.
  
  RUN selection-changed.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-year-ends V-table-Win 
PROCEDURE refresh-year-ends :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR sv   AS CHAR NO-UNDO.
DEF VAR item AS CHAR NO-UNDO.
  
  FOR EACH FinancialYear NO-LOCK:
  
    FIND LAST Month OF FinancialYear NO-LOCK NO-ERROR.
    IF AVAILABLE Month THEN
    DO WITH FRAME {&FRAME-NAME}:
      item = STRING( Month.Enddate, "99/99/9999" ).
      IF cmb_Month:ADD-LAST( item ) THEN
        IF YEAR( TODAY ) = YEAR( Month.EndDate ) THEN sv = item.
    END.
    
  END.

  cmb_Month:SCREEN-VALUE IN FRAME {&FRAME-NAME} = sv.
  RUN month-changed.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selection-changed V-table-Win 
PROCEDURE selection-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  btn_run:SENSITIVE IN FRAME {&FRAME-NAME} =
    NOT( sel_cmp:SCREEN-VALUE = "" OR sel_cmp:SCREEN-VALUE = ? ) AND
    NOT( first-month = ? OR last-month = ? ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartViewer, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-run V-table-Win 
PROCEDURE verify-run :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF first-month = ? OR last-month = ? THEN
  DO:
    MESSAGE "You need to select a year to run against"
      VIEW-AS ALERT-BOX ERROR.
    APPLY 'ENTRY' TO cmb_Month IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE year-end V-table-Win 
PROCEDURE year-end :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-run.
  IF RETURN-VALUE = "FAIL" THEN RETURN.
 
  RUN year-end-routine.
  IF return-value <> "FAIL" THEN
  DO:
    MESSAGE "Year end run to batch" NewBatch.BatchCode "completed!"
      VIEW-AS ALERT-BOX INFORMATION.
    RUN dispatch( 'exit':U ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE year-end-routine V-table-Win 
PROCEDURE year-end-routine :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

year-end:
DO TRANSACTION ON ERROR UNDO year-end, RETURN "FAIL":

  DEF VAR trn-amount AS DEC NO-UNDO.
  DEF VAR rev-amount AS DEC NO-UNDO.
  DEF VAR i          AS INT NO-UNDO.
  DEF VAR sv         AS CHAR NO-UNDO.
  DEF VAR delim      AS CHAR NO-UNDO.
  CREATE NewBatch.
  ASSIGN
    NewBatch.BatchType = "AUTO"
    NewBatch.Description = "Year End Transfers - " + INPUT FRAME {&FRAME-NAME} cmb_Month
    

  sv    = sel_cmp:SCREEN-VALUE IN FRAME {&FRAME-NAME}.
  delim = sel_cmp:DELIMITER.
  
  DO i = 1 TO NUM-ENTRIES( sv, delim ):

    rev-amount = 0.
    FIND FIRST Company WHERE Company.CompanyCode = 
      INT( TRIM( ENTRY( 1, ENTRY( i, sv, delim ), "-" ) ) ) NO-LOCK.

    CREATE NewDocument.
    ASSIGN
      NewDocument.BatchCode    = NewBatch.BatchCode
      NewDocument.DocumentType = "YEND"
      NewDocument.Reference    = "YEAR END".
      NewDocument.Description  = "Year End Transfers - L" +
        STRING( Company.CompanyCode, "9999" ) + ", " + INPUT FRAME {&FRAME-NAME} cmb_Month.
    
    FOR EACH AccountGroup NO-LOCK WHERE AccountGroup.SequenceCode < shcap-seq,
      EACH ChartOfAccount OF AccountGroup NO-LOCK:

      trn-amount = 0.00.      
      FOR EACH AccountBalance NO-LOCK WHERE
        AccountBalance.EntityType   = "L" AND
        AccountBalance.EntityCode   = Company.CompanyCode AND
        AccountBalance.AccountCode  = ChartOfAccount.AccountCode AND
        AccountBalance.MonthCode   >= first-month AND
        AccountBalance.MonthCode   <= last-month:
        trn-amount = trn-amount + AccountBalance.Balance.
      END.

      IF trn-amount <> 0 THEN
      DO:
      
        CREATE NewAcctTrans.
        ASSIGN
          NewAcctTrans.BatchCode    = NewDocument.BatchCode
          NewAcctTrans.DocumentCode = NewDocument.DocumentCode
          NewAcctTrans.EntityType   = "L"
          NewAcctTrans.EntityCode   = Company.CompanyCode
          NewAcctTrans.AccountCode  = ChartOfAccount.AccountCode
          NewAcctTrans.MonthCode    = last-month
          NewAcctTrans.Amount       = trn-amount * -1
          NewAcctTrans.Date         = last-month-date
          NewAcctTrans.Description  = "Y/E Transfer to Retained Earnings".
        
        rev-amount = rev-amount + trn-amount.
      
      END.
        
    END.

    /* Create the reversal */
    
    CREATE NewAcctTrans.
    ASSIGN
      NewAcctTrans.BatchCode    = NewDocument.BatchCode
      NewAcctTrans.DocumentCode = NewDocument.DocumentCode
      NewAcctTrans.EntityType   = "L"
      NewAcctTrans.EntityCode   = Company.CompanyCode
      NewAcctTrans.AccountCode  = ye-account
      NewAcctTrans.MonthCode    = last-month
      NewAcctTrans.Amount       = rev-amount
      NewAcctTrans.Date         = last-month-date
      NewAcctTrans.Description  = "Y/E Accumulated P&L".
        
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

