&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialog 
/*------------------------------------------------------------------------

  File: 

  Description: Edit alternate company names

  Input Parameters:
      Person Code and Main Company

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
DEF INPUT PARAMETER person-code AS INT NO-UNDO.
DEF INPUT PARAMETER person-default AS CHAR NO-UNDO.
DEF INPUT PARAMETER alternate-type AS CHAR NO-UNDO.

/* Local Variable Definitions ---                                       */

FIND Person WHERE Person.PersonCode = person-code
                    NO-LOCK NO-ERROR.
IF NOT AVAILABLE(Person) THEN DO:
  MESSAGE "No person record available for" person-code
            VIEW-AS ALERT-BOX ERROR.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialog

&Scoped-define ADM-CONTAINER DIALOG-BOX

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME D-Dialog
&Scoped-define BROWSE-NAME br_Details

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Person
&Scoped-define FIRST-EXTERNAL-TABLE Person


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Person.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES PersonDetail

/* Definitions for BROWSE br_Details                                    */
&Scoped-define FIELDS-IN-QUERY-br_Details PersonDetail.ContactType PersonDetail.DataValue   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_Details PersonDetail.ContactType ~
PersonDetail.DataValue   
&Scoped-define FIELD-PAIRS-IN-QUERY-br_Details~
 ~{&FP1}ContactType ~{&FP2}ContactType ~{&FP3}~
 ~{&FP1}DataValue ~{&FP2}DataValue ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_Details PersonDetail
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_Details PersonDetail
&Scoped-define SELF-NAME br_Details
&Scoped-define OPEN-QUERY-br_Details OPEN QUERY {&SELF-NAME} FOR EACH PersonDetail OF Person       WHERE PersonDetail.PersonDetailType = "ALT"  AND PersonDetail.SupplementaryType = alternate-type NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br_Details PersonDetail
&Scoped-define FIRST-TABLE-IN-QUERY-br_Details PersonDetail


/* Definitions for DIALOG-BOX D-Dialog                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-D-Dialog ~
    ~{&OPEN-QUERY-br_Details}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-2 br_Details btn_add btn_remove Btn_OK ~
Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS default-value 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 10 BY 1.

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 9.14 BY 1.15
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "OK" 
     SIZE 7.43 BY 1.15
     BGCOLOR 8 .

DEFINE BUTTON btn_remove 
     LABEL "&Remove" 
     SIZE 10 BY 1.

DEFINE VARIABLE default-value AS CHARACTER FORMAT "X(80)" 
     LABEL "Default" 
     VIEW-AS FILL-IN 
     SIZE 44 BY 1.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 52 BY 11.4.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_Details FOR 
      PersonDetail SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_Details D-Dialog _FREEFORM
  QUERY br_Details DISPLAY
      PersonDetail.ContactType FORMAT "X(6)" WIDTH 6
      PersonDetail.DataValue COLUMN-LABEL "Company" FORMAT "X(50)" WIDTH 41
  ENABLE
      PersonDetail.ContactType
      PersonDetail.DataValue
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 50.86 BY 7.5
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialog
     default-value AT ROW 1.3 COL 6.43 COLON-ALIGNED
     br_Details AT ROW 3.6 COL 1.57
     btn_add AT ROW 11.2 COL 31.57
     btn_remove AT ROW 11.2 COL 42.14
     Btn_OK AT ROW 12.5 COL 1
     Btn_Cancel AT ROW 12.5 COL 10.14
     "Enter the contact types and the overrides which apply." VIEW-AS TEXT
          SIZE 49.43 BY .8 AT ROW 2.6 COL 2.14
     RECT-2 AT ROW 1 COL 1
     SPACE(0.13) SKIP(1.59)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 10
         TITLE "Address Details"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialog
   External Tables: TTPL.Person
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX D-Dialog
                                                                        */
/* BROWSE-TAB br_Details default-value D-Dialog */
ASSIGN 
       FRAME D-Dialog:SCROLLABLE       = FALSE
       FRAME D-Dialog:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN default-value IN FRAME D-Dialog
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_Details
/* Query rebuild information for BROWSE br_Details
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH PersonDetail OF Person
      WHERE PersonDetail.PersonDetailType = "ALT"
 AND PersonDetail.SupplementaryType = alternate-type NO-LOCK.
     _END_FREEFORM
     _Where[1]         = "PersonDetail.PersonDetailType = ""ALT""
 AND PersonDetail.SupplementaryType = alternate-type"
     _Query            is OPENED
*/  /* BROWSE br_Details */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialog
/* Query rebuild information for DIALOG-BOX D-Dialog
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialog */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialog 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON WINDOW-CLOSE OF FRAME D-Dialog /* Address Details */
DO:  
  /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br_Details
&Scoped-define SELF-NAME br_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_Details D-Dialog
ON VALUE-CHANGED OF br_Details IN FRAME D-Dialog /* Browse 1 */
DO:
  RUN display-alternate.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add D-Dialog
ON CHOOSE OF btn_add IN FRAME D-Dialog /* Add */
DO:
  RUN add-alternate.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_remove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_remove D-Dialog
ON CHOOSE OF btn_remove IN FRAME D-Dialog /* Remove */
DO:
  RUN remove-alternate.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialog 


/* ***************************  Main Block  *************************** */
default-value = person-default.
DataValue:LABEL IN BROWSE {&BROWSE-NAME} = alternate-type.

ON LEAVE OF PersonDetail.ContactType IN BROWSE {&BROWSE-NAME} DO:
  RUN assign-alternate.
END.

ON LEAVE OF PersonDetail.DataValue IN BROWSE {&BROWSE-NAME} DO:
  RUN assign-alternate.
END.

RUN open-alt-query.
RUN display-alternate.

{src/adm/template/dialogmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-alternate D-Dialog 
PROCEDURE add-alternate :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR reposition-row   AS INT   NO-UNDO.
  DEF VAR reposition-rowid AS ROWID NO-UNDO.
  
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN DO:
    RUN open-alt-query.
  END.
    
  IF AVAILABLE PersonDetail THEN RUN assign-alternate.
  
  CREATE PersonDetail.
  PersonDetail.PersonCode = person-code.
  PersonDetail.PersonDetailType = "ALT".
  PersonDetail.SupplementaryType = alternate-type.
    
  reposition-rowid = ROWID( PersonDetail ).
  
  reposition-row = {&BROWSE-NAME}:GET-REPOSITIONED-ROW().
  reposition-row =  INT( {&BROWSE-NAME}:HEIGHT-CHARS ) - 1.
  OPEN QUERY {&BROWSE-NAME} FOR EACH PersonDetail.
  IF {&BROWSE-NAME}:SET-REPOSITIONED-ROW( reposition-row, "CONDITIONAL" ) THEN.
  REPOSITION {&BROWSE-NAME} TO ROWID reposition-rowid.

  RUN display-alternate.
  RUN assign-alternate.
  APPLY 'ENTRY':U TO PersonDetail.ContactType IN BROWSE {&BROWSE-NAME}.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialog _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialog _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Person"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Person"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-alternate D-Dialog 
PROCEDURE assign-alternate :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE PersonDetail THEN DO:
    FIND CURRENT PersonDetail EXCLUSIVE-LOCK.
    ASSIGN BROWSE {&BROWSE-NAME}
      PersonDetail.DataValue
      PersonDetail.ContactType .

    PersonDetail.PersonCode = person-code.
    PersonDetail.PersonDetailType = "ALT".
    PersonDetail.SupplementaryType = alternate-type.
    FIND CURRENT PersonDetail NO-LOCK.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-alt-query D-Dialog 
PROCEDURE close-alt-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY {&BROWSE-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialog _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialog.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-alternate D-Dialog 
PROCEDURE display-alternate :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE PersonDetail THEN DO:
    DISPLAY {&FIELDS-IN-QUERY-{&BROWSE-NAME}} WITH BROWSE {&BROWSE-NAME}.
  END.
  RUN sensitise-buttons.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialog _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY default-value 
      WITH FRAME D-Dialog.
  ENABLE RECT-2 br_Details btn_add btn_remove Btn_OK Btn_Cancel 
      WITH FRAME D-Dialog.
  VIEW FRAME D-Dialog.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialog}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-alt-query D-Dialog 
PROCEDURE open-alt-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  {&OPEN-QUERY-{&BROWSE-NAME}}
  RUN display-alternate.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-alternate D-Dialog 
PROCEDURE remove-alternate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN RETURN.

  IF {&BROWSE-NAME}:SELECT-FOCUSED-ROW() AND
     {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN
  DO:
    GET CURRENT {&BROWSE-NAME}.
    DELETE PersonDetail.
    IF {&BROWSE-NAME}:DELETE-CURRENT-ROW() THEN.
  END.

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = ? OR 
     NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 THEN
  DO:
    RUN close-alt-query.
    RUN sensitise-buttons.
    RETURN.
  END.

  IF {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN RUN display-alternate.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialog _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Person"}
  {src/adm/template/snd-list.i "PersonDetail"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-buttons D-Dialog 
PROCEDURE sensitise-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  
  DO WITH FRAME {&FRAME-NAME}:
    btn_remove:SENSITIVE  = AVAILABLE PersonDetail.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialog 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


