&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEFINE TEMP-TABLE MyDoc LIKE NewDocument
    FIELD Maximum AS DEC
    INDEX XPKMyDocs IS PRIMARY Maximum
    INDEX XAK1MyDocs DocumentCode.

DEFINE TEMP-TABLE MyTran LIKE NewAcctTran
    INDEX XPKMyTran IS PRIMARY DocumentCode Amount
    INDEX XAK1MyTran DocumentCode Amount DESCENDING .

DEF VAR doc-code AS INT NO-UNDO.
DEF VAR trn-code AS INT NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES NewBatch
&Scoped-define FIRST-EXTERNAL-TABLE NewBatch


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR NewBatch.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-32 fil_bchdesc rs_Sort tgl_date ~
fil_Date tgl_ref fil_Ref tgl_desc fil_desc tgl_reverse cmb_BatchType ~
btn_MOdify btn_createcopy btn_cancel 
&Scoped-Define DISPLAYED-OBJECTS fil_bchdesc rs_Sort tgl_date fil_Date ~
tgl_ref fil_Ref tgl_desc fil_desc tgl_reverse cmb_BatchType 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_cancel 
     LABEL "Cancel" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_createcopy 
     LABEL "Create &Copy" 
     SIZE 12.86 BY 1.05
     FONT 10.

DEFINE BUTTON btn_MOdify 
     LABEL "&Modify" 
     SIZE 10 BY 1.05
     FONT 10.

DEFINE VARIABLE cmb_BatchType AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     SIZE 27.43 BY 1 NO-UNDO.

DEFINE VARIABLE fil_bchdesc AS CHARACTER FORMAT "X(256)":U 
     LABEL "New Description" 
     VIEW-AS FILL-IN 
     SIZE 50.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Date AS DATE FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1 NO-UNDO.

DEFINE VARIABLE fil_desc AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 50 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Ref AS CHARACTER FORMAT "X(12)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE rs_Sort AS LOGICAL 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Unsorted", ?,
"Sort descending", no,
"Sort ascending", yes
     SIZE 50.29 BY .8 NO-UNDO.

DEFINE RECTANGLE RECT-32
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 64.57 BY 11.2.

DEFINE VARIABLE tgl_date AS LOGICAL INITIAL no 
     LABEL "Date" 
     VIEW-AS TOGGLE-BOX
     SIZE 6.86 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_desc AS LOGICAL INITIAL no 
     LABEL "Description" 
     VIEW-AS TOGGLE-BOX
     SIZE 10.86 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_ref AS LOGICAL INITIAL no 
     LABEL "Reference" 
     VIEW-AS TOGGLE-BOX
     SIZE 10.29 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_reverse AS LOGICAL INITIAL no 
     LABEL "Reverse Transactions" 
     VIEW-AS TOGGLE-BOX
     SIZE 18.29 BY 1
     FONT 10 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_bchdesc AT ROW 2.5 COL 13 COLON-ALIGNED
     rs_Sort AT ROW 5.4 COL 3.29 NO-LABEL
     tgl_date AT ROW 6.8 COL 3.29
     fil_Date AT ROW 6.8 COL 13 COLON-ALIGNED NO-LABEL
     tgl_ref AT ROW 7.9 COL 3.29
     fil_Ref AT ROW 7.9 COL 13 COLON-ALIGNED NO-LABEL
     tgl_desc AT ROW 9 COL 3.29
     fil_desc AT ROW 9 COL 13 COLON-ALIGNED NO-LABEL
     tgl_reverse AT ROW 10.1 COL 3.29
     cmb_BatchType AT ROW 11.4 COL 13.29 COLON-ALIGNED NO-LABEL
     btn_MOdify AT ROW 12.8 COL 1
     btn_createcopy AT ROW 12.8 COL 12.43
     btn_cancel AT ROW 12.8 COL 55.29
     "Modifications" VIEW-AS TEXT
          SIZE 13.14 BY .6 AT ROW 4.5 COL 2.72
          FONT 14
     RECT-32 AT ROW 1.4 COL 1
     "Copy Batch type:" VIEW-AS TEXT
          SIZE 12 BY .65 AT ROW 11.6 COL 3.29
     "Unposted Batch Tool" VIEW-AS TEXT
          SIZE 25.14 BY 1 AT ROW 1 COL 2.14
          FONT 13
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.NewBatch
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.8
         WIDTH              = 73.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel V-table-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_createcopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_createcopy V-table-Win
ON CHOOSE OF btn_createcopy IN FRAME F-Main /* Create Copy */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN copy-batch.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_MOdify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_MOdify V-table-Win
ON CHOOSE OF btn_MOdify IN FRAME F-Main /* Modify */
DO:
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  RUN modify-batch.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_date
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_date V-table-Win
ON VALUE-CHANGED OF tgl_date IN FRAME F-Main /* Date */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_desc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_desc V-table-Win
ON VALUE-CHANGED OF tgl_desc IN FRAME F-Main /* Description */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_ref
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_ref V-table-Win
ON VALUE-CHANGED OF tgl_ref IN FRAME F-Main /* Reference */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "NewBatch"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "NewBatch"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copy-batch V-table-Win 
PROCEDURE copy-batch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  ASSIGN FRAME {&FRAME-NAME} fil_bchdesc fil_date fil_ref fil_desc tgl_date
                             tgl_ref tgl_desc tgl_reverse cmb_BatchType.
       
  DEF BUFFER CopyBatch FOR NewBatch.
  DEF BUFFER CopyDoc   FOR NewDocument.
  DEF BUFFER CopyTrn   FOR NewAcctTrans.
  
  DEF VAR batch-type AS CHAR NO-UNDO.
  batch-type = TRIM( ENTRY( 1, cmb_BatchType, "-" ) ).
  batch-type = ( IF batch-type <> "" THEN batch-type ELSE "NORM" ).
  
  DO TRANSACTION:
    CREATE  CopyBatch.
    ASSIGN  CopyBatch.BatchType = batch-type
            CopyBatch.Description = fil_bchdesc.
  END.    

  FOR EACH NewDocument OF NewBatch WHERE CAN-FIND( FIRST NewAcctTrans OF NewDocument) NO-LOCK:
  
    DO TRANSACTION:
      CREATE CopyDoc.
      ASSIGN    CopyDoc.BatchCode    = CopyBatch.BatchCode
                CopyDoc.DocumentType = NewDocument.DocumentType
                CopyDoc.Reference   = IF tgl_ref  THEN fil_ref  ELSE NewDocument.Reference
                CopyDoc.Description = IF tgl_desc THEN fil_desc ELSE NewDocument.Description.
    END.

    FOR EACH NewAcctTrans OF NewDocument NO-LOCK:
      DO TRANSACTION:
        CREATE CopyTrn.
        ASSIGN  CopyTrn.BatchCode     = CopyBatch.BatchCode
                CopyTrn.DocumentCode  = CopyDoc.DocumentCode
                CopyTrn.EntityType    = NewAcctTrans.EntityType
                CopyTrn.EntityCode    = NewAcctTrans.EntityCode
                CopyTrn.AccountCode   = NewAcctTrans.AccountCode
                CopyTrn.ConsequenceOf = NewAcctTrans.ConsequenceOf
                CopyTrn.MonthCode     = NewAcctTrans.MonthCode 
                CopyTrn.Date        = IF tgl_date    THEN fil_date ELSE NewAcctTrans.Date
                CopyTrn.Description = IF tgl_desc    THEN "" ELSE NewAcctTrans.Description
                CopyTrn.Reference   = IF tgl_ref     THEN "" ELSE NewAcctTrans.Reference
                CopyTrn.Amount      = NewAcctTrans.Amount * IF tgl_reverse THEN -1 ELSE 1.
      END.
    END.
  END.  

  RUN notify( 'open-query,record-source':U ).
  MESSAGE "The copy of batch" NewBatch.BatchCode "- New Batch" CopyBatch.BatchCode SKIP
    "was successfully created." VIEW-AS ALERT-BOX INFORMATION.

  IF rs_Sort <> ? THEN RUN sort-batch( CopyBatch.BatchCode ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT tgl_Date THEN
    ENABLE fil_Date.
  ELSE
    DISABLE fil_Date.

  IF INPUT tgl_Ref THEN
    ENABLE fil_Ref.
  ELSE
    DISABLE fil_Ref.

  IF INPUT tgl_Desc THEN
    ENABLE fil_Desc.
  ELSE
    DISABLE fil_Desc.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  rs_Sort = ?.
  DISPLAY rs_Sort WITH FRAME {&FRAME-NAME}.
  RUN populate-batch-type.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  ASSIGN fil_bchdesc = NewBatch.Description.
  DISPLAY fil_bchdesc WITH FRAME {&FRAME-NAME}.
  
  RUN enable-appropriate-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE make-sorted-document V-table-Win 
PROCEDURE make-sorted-document :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  doc-code = doc-code + 1.
  CREATE NewDocument.
  BUFFER-COPY MyDoc TO NewDocument ASSIGN
            NewDocument.DocumentCode = doc-code.
  trn-code = 0.

  IF rs_Sort THEN DO:
    FOR EACH MyTran WHERE MyTran.DocumentCode = MyDoc.DocumentCode
                        BY DocumentCode BY Amount :
      RUN make-sorted-transaction.
    END.
  END.
  ELSE DO:
    FOR EACH MyTran WHERE MyTran.DocumentCode = MyDoc.DocumentCode
                        BY MyTran.DocumentCode BY MyTran.Amount DESCENDING :
      RUN make-sorted-transaction.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE make-sorted-transaction V-table-Win 
PROCEDURE make-sorted-transaction :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  trn-code = trn-code + 1.
  CREATE NewAcctTran.
  BUFFER-COPY MyTran TO NewAcctTran ASSIGN
          NewAcctTran.DocumentCode = doc-code
          NewAcctTran.TransactionCode = trn-code.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modify-batch V-table-Win 
PROCEDURE modify-batch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

ASSIGN FRAME {&FRAME-NAME}
  fil_bchdesc fil_date fil_ref fil_desc
  tgl_date tgl_ref tgl_desc tgl_reverse cmb_BatchType rs_Sort.
       
DO TRANSACTION:

  DEF VAR batch-type AS CHAR NO-UNDO.
  batch-type = TRIM( ENTRY( 1, cmb_BatchType, "-" ) ).
  batch-type = ( IF batch-type <> "" THEN batch-type ELSE "NORM" ).
  FIND CURRENT NewBatch EXCLUSIVE-LOCK.
  ASSIGN NewBatch.Description = fil_bchdesc
         NewBatch.BatchType = batch-type.

  IF tgl_date OR tgl_ref OR tgl_desc OR tgl_reverse THEN
  FOR EACH NewDocument OF NewBatch EXCLUSIVE-LOCK:
    IF tgl_ref  THEN ASSIGN NewDocument.Reference   = fil_ref.
    IF tgl_desc THEN ASSIGN NewDocument.Description = fil_desc.
    
    FOR EACH NewAcctTrans OF NewDocument EXCLUSIVE-LOCK:
      IF tgl_date    THEN ASSIGN NewAcctTrans.Date        = fil_Date.
      IF tgl_desc    THEN ASSIGN NewAcctTrans.Description = "".
      IF tgl_ref     THEN ASSIGN NewAcctTrans.Reference   = "".
      IF tgl_reverse THEN ASSIGN NewAcctTrans.Amount      = NewAcctTrans.Amount * -1.
    END.
  END.  
  FIND CURRENT NewBatch NO-LOCK.
END.

  IF rs_Sort <> ? THEN RUN sort-batch( NewBatch.BatchCode ).

  RUN notify( 'open-query,record-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE populate-batch-type V-table-Win 
PROCEDURE populate-batch-type :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
    DEF VAR def-desc AS CHAR NO-UNDO INIT "".
    DEF VAR type-desc AS CHAR NO-UNDO.
    DEF VAR batch-types AS CHAR NO-UNDO INIT "NORM,ACCR".
    
    cmb_BatchType:LIST-ITEMS = "".
    FOR EACH BatchType NO-LOCK:
        IF ( LOOKUP( BatchType.BatchType, batch-types ) <> 0 ) THEN DO:
            type-desc = STRING( BatchType.BatchType, "X(4)" ) + " - " + BatchType.Description.
            cmb_BatchType:ADD-LAST( type-desc ).
            IF ( BatchType.BatchType = "NORM" ) THEN def-desc = type-desc.
          END.
    END.
    cmb_BatchType:SCREEN-VALUE = def-desc.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "NewBatch"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sort-batch V-table-Win 
PROCEDURE sort-batch :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER batch-code AS INT NO-UNDO.

DEF VAR is-rp-batch AS LOGI NO-UNDO INITIAL No.
DEF VAR doc-max AS DEC NO-UNDO.

  /* first figure if it's a receipts or payments batch */
  FIND LAST NewDocument WHERE NewDocument.BatchCode = batch-code NO-LOCK.
  IF (NewDocument.DocumentType = "RCBK" OR NewDocument.DocumentType = "PYBK")
      AND NewDocument.DocumentCode = 2 THEN is-rp-batch = Yes.

  FOR EACH NewDocument WHERE NewDocument.BatchCode = batch-code NO-LOCK:
    CREATE MyDoc.
    BUFFER-COPY NewDocument TO MyDoc.
    doc-max = 0.
    FOR EACH NewAcctTran OF NewDocument NO-LOCK:
      IF NOT is-rp-batch THEN doc-max = MAX(doc-max, ABS(NewAcctTran.Amount)).
      CREATE MyTran.
      BUFFER-COPY NewAcctTran TO MyTran.
    END.
    IF is-rp-batch THEN
      MyDoc.Maximum = MyDoc.DocumentCode.
    ELSE
      MyDoc.Maximum = doc-max.
  END.

  ON WRITE OF NewDocument OVERRIDE DO: END.
  ON WRITE OF NewAcctTran OVERRIDE DO: END.
  ON DELETE OF NewDocument OVERRIDE DO: END.
  ON DELETE OF NewAcctTran OVERRIDE DO: END.
  ON CREATE OF NewDocument OVERRIDE DO: END.
  ON CREATE OF NewAcctTran OVERRIDE DO: END.

DO TRANSACTION:

  FOR EACH NewDocument WHERE NewDocument.BatchCode = batch-code:
    FOR EACH NewAcctTran OF NewDocument:
      DELETE NewAcctTran.
    END.
    DELETE NewDocument.
  END.

  doc-code = 0.  
  IF is-rp-batch THEN DO:
    FOR EACH MyDoc BY MyDoc.DocumentCode:
      RUN make-sorted-document.
    END.
  END.
  ELSE IF rs_Sort THEN DO:
    FOR EACH MyDoc BY MyDoc.Maximum:
      RUN make-sorted-document.
    END.
  END.
  ELSE DO:
    FOR EACH MyDoc BY MyDoc.Maximum DESCENDING:
      RUN make-sorted-document.
    END.
  END.

END.

  ON WRITE OF NewDocument REVERT.
  ON WRITE OF NewAcctTrans REVERT.
  ON DELETE OF NewDocument REVERT.
  ON DELETE OF NewAcctTrans REVERT.
  ON CREATE OF NewDocument REVERT.
  ON CREATE OF NewAcctTrans REVERT.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


