&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR this-win AS HANDLE NO-UNDO.

DEF VAR code-format AS CHAR NO-UNDO INITIAL "X(6)".
DEF VAR type-length AS INT NO-UNDO.
type-length = LENGTH( STRING( "", code-format)).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS sel_cavail btn_add sel_ccurr btn_remove 
&Scoped-Define DISPLAYED-OBJECTS sel_cavail sel_ccurr 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
PersonCode|y|y|TTPL.Person.PersonCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "PersonCode",
     Keys-Supplied = "PersonCode"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 8.57 BY 1.05
     FONT 9.

DEFINE BUTTON btn_remove 
     LABEL "&Remove" 
     SIZE 8.57 BY 1.05
     FONT 9.

DEFINE VARIABLE sel_cavail AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SORT SCROLLBAR-VERTICAL 
     SIZE 34.57 BY 13.6
     FONT 8 NO-UNDO.

DEFINE VARIABLE sel_ccurr AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SORT SCROLLBAR-VERTICAL 
     SIZE 35.14 BY 13.6
     FONT 8 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     sel_cavail AT ROW 2 COL 1 NO-LABEL
     btn_add AT ROW 2 COL 35.57
     sel_ccurr AT ROW 2 COL 44.14 NO-LABEL
     btn_remove AT ROW 3.5 COL 35.57
     "Current Contact Types" VIEW-AS TEXT
          SIZE 16 BY 1 AT ROW 1 COL 53.29
          FONT 10
     "Available Contact Types" VIEW-AS TEXT
          SIZE 17 BY 1 AT ROW 1 COL 8.72
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 14.65
         WIDTH              = 78.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN add-contact-types.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_remove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_remove V-table-Win
ON CHOOSE OF btn_remove IN FRAME F-Main /* Remove */
DO:
  RUN remove-contact-types.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_cavail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_cavail V-table-Win
ON MOUSE-SELECT-DBLCLICK OF sel_cavail IN FRAME F-Main
DO:
  APPLY 'CHOOSE':U TO btn_add IN FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_cavail V-table-Win
ON VALUE-CHANGED OF sel_cavail IN FRAME F-Main
DO:
  RUN update-buttons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_ccurr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_ccurr V-table-Win
ON MOUSE-SELECT-DBLCLICK OF sel_ccurr IN FRAME F-Main
DO:
  APPLY 'CHOOSE':U TO btn_remove IN FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_ccurr V-table-Win
ON VALUE-CHANGED OF sel_ccurr IN FRAME F-Main
DO:
  RUN update-buttons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-contact-types V-table-Win 
PROCEDURE add-contact-types :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  
  IF NOT AVAILABLE Person THEN RETURN.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
    
  DEF VAR i AS INT NO-UNDO.
  DEF VAR sv AS CHAR NO-UNDO.
  DEF VAR delim AS CHAR NO-UNDO.
  DEF VAR contact-type AS CHAR NO-UNDO.
  DEF VAR item AS CHAR NO-UNDO.
  
  sv = INPUT sel_cavail.
  delim = sel_cavail:DELIMITER.
  
  DO i = 1 TO NUM-ENTRIES( sv, delim ):
    item = ENTRY( i, sv, delim ).
    contact-type = TRIM( SUBSTR( item, 1, type-length ) ).
    CREATE Contact.
    ASSIGN
      Contact.PersonCode  = Person.PersonCode
      Contact.ContactType = contact-type.
    IF sel_cavail:DELETE( item ) THEN.
    IF sel_ccurr:ADD-LAST( item ) THEN.
  END.

  sel_ccurr:SCREEN-VALUE = sv.
  RUN update-buttons.
  RUN notify( 'display-contact-types, RECORD-SOURCE':U ).
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'PersonCode':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = Person
           &WHERE = "WHERE Person.PersonCode eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN dispatch( 'find-using-key':U ).
  IF NOT AVAILABLE Person THEN RETURN.  

  DEF VAR person-id AS CHAR NO-UNDO.
  person-id = Person.FirstName + ' ' + Person.LastName.
  person-id = IF person-id <> "" THEN person-id ELSE Person.Company.
  
  this-win = IF VALID-HANDLE( this-win ) THEN this-win ELSE CURRENT-WINDOW.
  IF VALID-HANDLE( this-win ) THEN this-win:TITLE = "Contact Types - " + person-id.
  RUN refresh-contact-types.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-contact-types V-table-Win 
PROCEDURE refresh-contact-types :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item AS CHAR NO-UNDO.
  
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  sel_cavail:LIST-ITEMS = "".
  sel_ccurr:LIST-ITEMS  = "".
  
  IF NOT AVAILABLE Person THEN RETURN.
  
  FOR EACH ContactType NO-LOCK:
  
    item = STRING( ContactType.ContactType, code-format ) + " - " + ContactType.Description.

    IF NOT CAN-FIND( Contact OF Person WHERE
      Contact.ContactType = ContactType.ContactType ) THEN
    DO:
      IF sel_cavail:ADD-LAST( item ) THEN.
    END.
    ELSE
      IF sel_ccurr:ADD-LAST( item ) THEN.
    
  END.

  RUN update-buttons.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-contact-types V-table-Win 
PROCEDURE remove-contact-types :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  
  IF NOT AVAILABLE Person THEN RETURN.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
    
  DEF VAR i AS INT NO-UNDO.
  DEF VAR sv AS CHAR NO-UNDO.
  DEF VAR delim AS CHAR NO-UNDO.
  DEF VAR contact-type AS CHAR NO-UNDO.
  DEF VAR item AS CHAR NO-UNDO.
  
  sv = INPUT sel_ccurr.
  delim = sel_ccurr:DELIMITER.
  
  DO i = 1 TO NUM-ENTRIES( sv, delim ):
    item = ENTRY( i, sv, delim ).
    contact-type = TRIM( SUBSTR( item, 1, type-length ) ).

    FIND Contact OF Person WHERE Contact.ContactType = contact-type
      EXCLUSIVE-LOCK NO-ERROR.
    IF AVAILABLE Contact THEN DELETE Contact.

    IF sel_ccurr:DELETE( item ) THEN.
    IF sel_cavail:ADD-LAST( item ) THEN.
    
  END.

  sel_cavail:SCREEN-VALUE = sv.
  RUN update-buttons.
  RUN notify( 'display-contact-types, RECORD-SOURCE':U ).
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "PersonCode" "Person" "PersonCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartViewer, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-buttons V-table-Win 
PROCEDURE update-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  btn_add:SENSITIVE = INPUT sel_cavail <> "" AND INPUT sel_cavail <> ?.
  btn_remove:SENSITIVE = INPUT sel_ccurr <> "" AND INPUT sel_ccurr <> ?.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


