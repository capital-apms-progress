&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.

DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR project-code AS INT NO-UNDO.
DEF VAR variation-code AS INT NO-UNDO.

DEF VAR got-accounts AS LOGI NO-UNDO INITIAL No.
DEF VAR account-code AS DEC NO-UNDO.

DEF VAR got-months AS LOGI NO-UNDO INITIAL No.
DEF VAR month-code AS INT NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartObject

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-flow

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Variation Project
&Scoped-define FIRST-EXTERNAL-TABLE Variation


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Variation, Project.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES VariationFlow Month

/* Definitions for BROWSE br-flow                                       */
&Scoped-define FIELDS-IN-QUERY-br-flow VariationFlow.Amount VariationFlow.AccountCode VariationFlow.MonthCode Month.StartDate Month.EndDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-flow VariationFlow.Amount ~
VariationFlow.AccountCode ~
VariationFlow.MonthCode   
&Scoped-define FIELD-PAIRS-IN-QUERY-br-flow~
 ~{&FP1}Amount ~{&FP2}Amount ~{&FP3}~
 ~{&FP1}AccountCode ~{&FP2}AccountCode ~{&FP3}~
 ~{&FP1}MonthCode ~{&FP2}MonthCode ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br-flow VariationFlow
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br-flow VariationFlow
&Scoped-define SELF-NAME br-flow
&Scoped-define OPEN-QUERY-br-flow OPEN QUERY {&SELF-NAME} FOR EACH VariationFlow OF Variation EXCLUSIVE-LOCK, ~
                FIRST Month OF VariationFlow NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br-flow VariationFlow Month
&Scoped-define FIRST-TABLE-IN-QUERY-br-flow VariationFlow


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-flow}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS Variation.VariationDate Variation.Reason ~
Variation.Approvers[1] Variation.Approvers[2] Variation.Approvers[3] 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}VariationDate ~{&FP2}VariationDate ~{&FP3}~
 ~{&FP1}Approvers[1] ~{&FP2}Approvers[1] ~{&FP3}~
 ~{&FP1}Approvers[2] ~{&FP2}Approvers[2] ~{&FP3}~
 ~{&FP1}Approvers[3] ~{&FP2}Approvers[3] ~{&FP3}
&Scoped-define ENABLED-TABLES Variation
&Scoped-define FIRST-ENABLED-TABLE Variation
&Scoped-Define ENABLED-OBJECTS cmb_VariationType br-flow Btn_Add Btn_Del ~
cmb_accounts cmb_months 
&Scoped-Define DISPLAYED-FIELDS Variation.ProjectCode Project.Name ~
Variation.VariationCode Variation.VariationDate Variation.Reason ~
Variation.Approvers[1] Variation.Approvers[2] Variation.Approvers[3] 
&Scoped-Define DISPLAYED-OBJECTS cmb_VariationType fil_Ap1 fil_Ap2 fil_Ap3 ~
cmb_accounts cmb_months 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Add 
     LABEL "&Add" 
     SIZE 9.14 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn_Del 
     LABEL "&Delete" 
     SIZE 9.14 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE cmb_accounts AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEMS " "
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_months AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEMS " "
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_VariationType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     SIZE 42.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Ap1 AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 42.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Ap2 AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 42.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Ap3 AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 42.86 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-flow FOR 
      VariationFlow, 
      Month SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-flow
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-flow V-table-Win _FREEFORM
  QUERY br-flow NO-LOCK DISPLAY
      VariationFlow.Amount
      VariationFlow.AccountCode
      VariationFlow.MonthCode
      Month.StartDate WIDTH 13
      Month.EndDate WIDTH 13
ENABLE
      VariationFlow.Amount
      VariationFlow.AccountCode
      VariationFlow.MonthCode
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 48 BY 6.2
         BGCOLOR 16 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Variation.ProjectCode AT ROW 1 COL 7.57 COLON-ALIGNED
          LABEL "Project"
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
     Project.Name AT ROW 1 COL 17.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 46.86 BY 1.05
     Variation.VariationCode AT ROW 2.2 COL 7.57 COLON-ALIGNED
          LABEL "Variation"
          VIEW-AS FILL-IN 
          SIZE 6.43 BY 1
     cmb_VariationType AT ROW 2.2 COL 21.86 COLON-ALIGNED
     Variation.VariationDate AT ROW 3.6 COL 7.57 COLON-ALIGNED
          LABEL "Date"
          VIEW-AS FILL-IN 
          SIZE 11.43 BY 1
     Variation.Reason AT ROW 4.8 COL 9.57 NO-LABEL
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 56.57 BY 5
          BGCOLOR 15 
     Variation.Approvers[1] AT ROW 10 COL 12.14 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 5.43 BY 1
     fil_Ap1 AT ROW 10 COL 21.29 COLON-ALIGNED NO-LABEL
     Variation.Approvers[2] AT ROW 11 COL 12.14 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 5.43 BY 1
     fil_Ap2 AT ROW 11 COL 21.29 COLON-ALIGNED NO-LABEL
     Variation.Approvers[3] AT ROW 12 COL 12.14 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 5.43 BY 1
     fil_Ap3 AT ROW 12 COL 21.29 COLON-ALIGNED NO-LABEL
     br-flow AT ROW 13.8 COL 8.43
     Btn_Add AT ROW 13.8 COL 57
     Btn_Del AT ROW 15 COL 57
     cmb_accounts AT ROW 17.2 COL 2.72 NO-LABEL
     cmb_months AT ROW 18 COL 1.29 COLON-ALIGNED NO-LABEL
     "Variation Cashflows:" VIEW-AS TEXT
          SIZE 14.29 BY .65 AT ROW 13.2 COL 2.14
     "(date of agreement or date of entry/change)" VIEW-AS TEXT
          SIZE 31.43 BY .65 AT ROW 3.8 COL 22.14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartObject
   External Tables: ttpl.Variation,ttpl.Project
   Allow: Basic,Browse,DB-Fields
   Frames: 1
   Add Fields to: External-Tables
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 20.75
         WIDTH              = 79.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-flow fil_Ap3 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cmb_accounts IN FRAME F-Main
   ALIGN-L                                                              */
ASSIGN 
       cmb_accounts:HIDDEN IN FRAME F-Main           = TRUE.

ASSIGN 
       cmb_months:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN fil_Ap1 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Ap2 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Ap3 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Project.Name IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN Variation.ProjectCode IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN Variation.VariationCode IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN Variation.VariationDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-flow
/* Query rebuild information for BROWSE br-flow
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH VariationFlow OF Variation EXCLUSIVE-LOCK,
         FIRST Month OF VariationFlow NO-LOCK.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is OPENED
*/  /* BROWSE br-flow */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Variation.Approvers[1]
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Variation.Approvers[1] V-table-Win
ON LEAVE OF Variation.Approvers[1] IN FRAME F-Main /* Approvers[1] */
DO:
  {inc/selcde/cdapp.i "fil_Ap1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Variation.Approvers[2]
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Variation.Approvers[2] V-table-Win
ON LEAVE OF Variation.Approvers[2] IN FRAME F-Main /* Approvers[2] */
DO:
  {inc/selcde/cdapp.i "fil_Ap2"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Variation.Approvers[3]
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Variation.Approvers[3] V-table-Win
ON LEAVE OF Variation.Approvers[3] IN FRAME F-Main /* Approvers[3] */
DO:
  {inc/selcde/cdapp.i "fil_Ap3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-flow
&Scoped-define SELF-NAME br-flow
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-flow V-table-Win
ON VALUE-CHANGED OF br-flow IN FRAME F-Main /* Variation Flows */
DO:
  RUN display-flow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Add V-table-Win
ON CHOOSE OF Btn_Add IN FRAME F-Main /* Add */
DO:
  RUN add-flow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Del
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Del V-table-Win
ON CHOOSE OF Btn_Del IN FRAME F-Main /* Delete */
DO:
  RUN delete-flow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_accounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON LEAVE OF cmb_accounts IN FRAME F-Main
DO:
  SELF:HIDDEN = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON TAB OF cmb_accounts IN FRAME F-Main
OR 'BACK-TAB' OF {&SELF-NAME} DO:
  APPLY LAST-EVENT:FUNCTION TO VariationFlow.AccountCode IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON U1 OF cmb_accounts IN FRAME F-Main
DO:
  FIND ProjectBudget WHERE ProjectBudget.ProjectCode = Variation.ProjectCode
                       AND ProjectBudget.AccountCode = account-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(ProjectBudget) THEN RETURN.
  {&SELF-NAME}:SCREEN-VALUE = STRING( account-code, '9999.99' ) + ' - '
                       + ProjectBudget.Description .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON U2 OF cmb_accounts IN FRAME F-Main
DO:
  account-code = DEC( SUBSTR( INPUT {&SELF-NAME}, 1, 7 ) ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON VALUE-CHANGED OF cmb_accounts IN FRAME F-Main
DO:
  APPLY 'U2' TO SELF.
  IF AVAILABLE VariationFlow THEN DO:
    VariationFlow.AccountCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( account-code ).
    RUN account-changed.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_months
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_months V-table-Win
ON LEAVE OF cmb_months IN FRAME F-Main
DO:
  SELF:HIDDEN = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_months V-table-Win
ON TAB OF cmb_months IN FRAME F-Main
OR 'BACK-TAB' OF {&SELF-NAME} DO:
  APPLY LAST-EVENT:FUNCTION TO VariationFlow.MonthCode IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_months V-table-Win
ON U1 OF cmb_months IN FRAME F-Main
DO:
  FIND Month WHERE Month.MonthCode = month-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Month) THEN RETURN.
  {&SELF-NAME}:SCREEN-VALUE = STRING( Month.StartDate, '99/99/9999' ) + ' to '
                       + STRING( Month.EndDate, '99/99/9999' ) .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_months V-table-Win
ON U2 OF cmb_months IN FRAME F-Main
DO:
  FIND Month WHERE Month.StartDate = DATE( SUBSTR( INPUT {&SELF-NAME}, 1, 10 ) )
                AND Month.EndDate = DATE( SUBSTR( INPUT {&SELF-NAME}, 15, 10 ) )
                NO-LOCK NO-ERROR.
  IF AVAILABLE(Month) THEN month-code = Month.MonthCode .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_months V-table-Win
ON VALUE-CHANGED OF cmb_months IN FRAME F-Main
DO:
  APPLY 'U2' TO SELF.
  IF AVAILABLE VariationFlow THEN DO:
    VariationFlow.MonthCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( month-code ).
    RUN month-changed.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_VariationType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_VariationType V-table-Win
ON U1 OF cmb_VariationType IN FRAME F-Main /* Type */
DO:
  {inc/selcmb/scvty1.i "Variation" "VariationType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_VariationType V-table-Win
ON U2 OF cmb_VariationType IN FRAME F-Main /* Type */
DO:
  {inc/selcmb/scvty2.i "Variation" "VariationType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Ap1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Ap1 V-table-Win
ON U1 OF fil_Ap1 IN FRAME F-Main
DO:
  {inc/selfil/sfapp1.i "Variation" "Approvers[1]"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Ap1 V-table-Win
ON U2 OF fil_Ap1 IN FRAME F-Main
DO:
  {inc/selfil/sfapp2.i "Variation" "Approvers[1]"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Ap1 V-table-Win
ON U3 OF fil_Ap1 IN FRAME F-Main
DO:
  {inc/selfil/sfapp3.i "Variation" "Approvers[1]"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Ap2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Ap2 V-table-Win
ON U1 OF fil_Ap2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp1.i "Variation" "Approvers[2]"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Ap2 V-table-Win
ON U2 OF fil_Ap2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp2.i "Variation" "Approvers[2]"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Ap2 V-table-Win
ON U3 OF fil_Ap2 IN FRAME F-Main
DO:
  {inc/selfil/sfapp3.i "Variation" "Approvers[2]"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Ap3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Ap3 V-table-Win
ON U1 OF fil_Ap3 IN FRAME F-Main
DO:
  {inc/selfil/sfapp1.i "Variation" "Approvers[3]"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Ap3 V-table-Win
ON U2 OF fil_Ap3 IN FRAME F-Main
DO:
  {inc/selfil/sfapp2.i "Variation" "Approvers[3]"}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Ap3 V-table-Win
ON U3 OF fil_Ap3 IN FRAME F-Main
DO:
  {inc/selfil/sfapp3.i "Variation" "Approvers[3]"}    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

ON LEAVE OF VariationFlow.AccountCode IN BROWSE {&BROWSE-NAME}
DO:
  RUN account-changed.
END.

/* Bring up the account code combo */
ON ENTRY OF VariationFlow.AccountCode IN BROWSE {&BROWSE-NAME} DO:
  IF NOT got-accounts THEN RUN get-accounts.
  got-accounts = Yes.
  cmb_accounts:X IN FRAME {&FRAME-NAME} = SELF:X + {&BROWSE-NAME}:X.
  cmb_accounts:Y IN FRAME {&FRAME-NAME} = SELF:Y + {&BROWSE-NAME}:Y.
  cmb_accounts:HIDDEN IN FRAME {&FRAME-NAME} = No.
  cmb_accounts:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  account-code = DEC( VariationFlow.AccountCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME}  ).
  APPLY 'U1':U TO cmb_accounts.
  IF cmb_accounts:MOVE-TO-TOP() THEN.
  APPLY 'ENTRY':U TO cmb_accounts.
  RETURN NO-APPLY.
END.


ON LEAVE OF VariationFlow.MonthCode IN BROWSE {&BROWSE-NAME}
DO:
  RUN month-changed.
END.

/* Bring up the month code combo */
ON ENTRY OF VariationFlow.MonthCode IN BROWSE {&BROWSE-NAME} DO:
  IF NOT got-months THEN RUN get-months.
  got-months = Yes.
  cmb_months:X IN FRAME {&FRAME-NAME} = SELF:X + {&BROWSE-NAME}:X.
  cmb_months:Y IN FRAME {&FRAME-NAME} = SELF:Y + {&BROWSE-NAME}:Y.
  cmb_months:HIDDEN IN FRAME {&FRAME-NAME} = No.
  cmb_months:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  month-code = DEC( VariationFlow.MonthCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME}  ).
  APPLY 'U1':U TO cmb_months.
  IF cmb_months:MOVE-TO-TOP() THEN.
  APPLY 'ENTRY':U TO cmb_months.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE account-changed V-table-Win 
PROCEDURE account-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  RUN assign-flow.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-flow V-table-Win 
PROCEDURE add-flow :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
IF NOT AVAILABLE(Variation) THEN RETURN.
DO WITH FRAME {&FRAME-NAME}:

  DEF BUFFER NewVariationFlow FOR VariationFlow.  
  DEF VAR reposition-row   AS INT   NO-UNDO.
  DEF VAR reposition-rowid AS ROWID NO-UNDO.
  
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN
    RUN open-flow-query.
  IF AVAILABLE VariationFlow THEN RUN assign-flow.

  FIND Project OF Variation NO-LOCK.
  FIND LAST Month WHERE Month.StartDate <= Project.StartDate NO-LOCK.

  DO TRANSACTION:
    CREATE NewVariationFlow.
    NewVariationFlow.ProjectCode = Variation.ProjectCode.
    NewVariationFlow.VariationCode = Variation.VariationCode.
    NewVariationFlow.MonthCode = Month.MonthCode.
    NewVariationFlow.AccountCode = Project.EntityAccount.
  END.

  reposition-rowid = ROWID( NewVariationFlow ).
  
  reposition-row = {&BROWSE-NAME}:GET-REPOSITIONED-ROW().
  reposition-row =  INT( {&BROWSE-NAME}:HEIGHT-CHARS ) - 1.
  OPEN QUERY {&BROWSE-NAME} FOR EACH VariationFlow OF Variation EXCLUSIVE-LOCK,
         FIRST Month OF VariationFlow NO-LOCK.
  IF {&BROWSE-NAME}:SET-REPOSITIONED-ROW( reposition-row, "CONDITIONAL" ) THEN.
  REPOSITION {&BROWSE-NAME} TO ROWID reposition-rowid.

  RUN display-flow.
  RUN assign-flow.
  APPLY 'ENTRY':U TO VariationFlow.Amount IN BROWSE {&BROWSE-NAME}.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Variation"}
  {src/adm/template/row-list.i "Project"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Variation"}
  {src/adm/template/row-find.i "Project"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-flow V-table-Win 
PROCEDURE assign-flow :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE VariationFlow THEN DO TRANSACTION:
    FIND CURRENT VariationFlow EXCLUSIVE-LOCK.
    ASSIGN BROWSE {&BROWSE-NAME}
      VariationFlow.Amount
      VariationFlow.MonthCode
      VariationFlow.AccountCode.

    VariationFlow.VariationCode = Variation.VariationCode.
    VariationFlow.ProjectCode   = Variation.ProjectCode.
    FIND CURRENT VariationFlow NO-LOCK.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF mode = "Add" THEN RUN delete-variation.
  RUN dispatch( 'exit':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-flow-query V-table-Win 
PROCEDURE close-flow-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  CLOSE QUERY {&BROWSE-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR record-changed AS LOGICAL NO-UNDO.

  RUN test-modified( OUTPUT record-changed ).

  IF record-changed THEN DO:
    RUN verify-variation.
    IF RETURN-VALUE = "FAIL" THEN RETURN.
  
    RUN dispatch( 'update-record':U ).
    IF mode = "Add" THEN RUN notify( 'open-query,record-source':U ).
  END.

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-flow V-table-Win 
PROCEDURE delete-flow :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN RETURN.

  IF {&BROWSE-NAME}:SELECT-FOCUSED-ROW() AND
     {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN
  DO:
    GET CURRENT {&BROWSE-NAME}.
    DELETE VariationFlow.
    IF {&BROWSE-NAME}:DELETE-CURRENT-ROW() THEN.
  END.

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = ? OR 
     NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 THEN
  DO:
    RUN close-flow.
    RUN account-changed.
    RETURN.
  END.

  IF {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN RUN display-flow.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-variation V-table-Win 
PROCEDURE delete-variation :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF AVAILABLE(Variation) THEN RETURN.
  FIND CURRENT Variation EXCLUSIVE-LOCK.
  FOR EACH VariationFlow OF Variation EXCLUSIVE-LOCK:
    DELETE VariationFlow.
  END.
  DELETE Variation.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-flow V-table-Win 
PROCEDURE display-flow :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE VariationFlow THEN DO:
    DISPLAY 
      VariationFlow.Amount
      VariationFlow.AccountCode
      VariationFlow.MonthCode
    WITH BROWSE {&BROWSE-NAME}.
    account-code   = VariationFlow.AccountCode.
  END.
  RUN account-changed.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-accounts V-table-Win 
PROCEDURE get-accounts :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  /* Populate the account combo */

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  FOR EACH ProjectBudget WHERE ProjectBudget.ProjectCode = Variation.ProjectCode NO-LOCK:
    IF cmb_accounts:ADD-LAST( STRING( ProjectBudget.AccountCode, "9999.99" ) + 
      " - " + ProjectBudget.Description ) IN FRAME {&FRAME-NAME} THEN.
  END.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-months V-table-Win 
PROCEDURE get-months :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  /* Populate the month combo */

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  FOR EACH Month WHERE Month.EndDate >= Project.StartDate NO-LOCK:
    IF cmb_months:ADD-LAST( STRING( Month.StartDate, "99/99/9999" ) + 
      " to " + STRING( Month.EndDate, "99/99/9999" ) ) IN FRAME {&FRAME-NAME} THEN.
  END.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF mode = "Add" THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE
    have-records = No.

  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN open-flow-query.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE month-changed V-table-Win 
PROCEDURE month-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  RUN assign-flow.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-flow-query V-table-Win 
PROCEDURE open-flow-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  OPEN QUERY {&BROWSE-NAME} FOR EACH VariationFlow OF Variation EXCLUSIVE-LOCK,
         FIRST Month OF VariationFlow NO-LOCK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR c-recsrc   AS CHAR NO-UNDO.
DEF VAR ext-tables AS CHAR NO-UNDO.

  RUN request-attribute IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE':U, 'EXTERNAL-TABLES':U ).
  ext-tables = RETURN-VALUE.
 
  RUN get-link-handle IN adm-broker-hdl( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT c-recsrc ).

  DEF VAR rowid-list AS CHAR NO-UNDO.
  IF LOOKUP( "Project", ext-tables ) <> 0 THEN DO:
    RUN send-records IN WIDGET-HANDLE( c-recsrc ) ( "Project", OUTPUT rowid-list ).
    FIND Project WHERE ROWID( Project ) = TO-ROWID( rowid-list ) NO-LOCK NO-ERROR.
  END.
  ELSE DO:
    DEF VAR Project-code AS CHAR NO-UNDO.
    RUN send-key IN WIDGET-HANDLE( c-recsrc ) ( "ProjectCode", OUTPUT Project-code ).
    FIND Project WHERE Project.ProjectCode = INT( Project-code ) NO-LOCK NO-ERROR.
  END.

  IF AVAILABLE(Project) THEN
    FIND Variation WHERE Variation.ProjectCode = Project.ProjectCode
                       AND Variation.VariationCode = 0 EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE( Variation ) THEN CREATE Variation.

  IF AVAILABLE(Project) THEN
    ASSIGN Variation.ProjectCode = Project.ProjectCode
           Variation.Approvers[1] = Project.FirstApprover.

  Variation.VariationDate = TODAY.

  FIND CURRENT Variation NO-LOCK.
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

  CURRENT-WINDOW:TITLE = "Adding new Variation " + STRING( Variation.VariationCode, "99999").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN check-modified( 'clear':U ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Variation"}
  {src/adm/template/snd-list.i "Project"}
  {src/adm/template/snd-list.i "VariationFlow"}
  {src/adm/template/snd-list.i "Month"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name V-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-name AS CHAR NO-UNDO.

  key-name = new-name.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value V-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key AS CHAR NO-UNDO.

  RUN get-attribute( 'key-name':U ).
/*  MESSAGE 'key-name: ' RETURN-VALUE. */
  IF key-name = "ProjectCode" THEN DO:
    project-code = INT( new-key ).
    FIND Project NO-LOCK WHERE Project.ProjectCode = project-code NO-ERROR.
  END.
  ELSE IF key-name = "VariationCode" THEN DO:
    variation-code = INT(new-key).
    IF AVAILABLE(Project) THEN
      FIND Variation NO-LOCK OF Project WHERE Variation.VariationCode = variation-code NO-ERROR.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.

  mode = new-mode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-variation V-table-Win 
PROCEDURE verify-variation :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


