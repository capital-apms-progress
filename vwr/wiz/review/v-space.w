&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:
  Description:  Wizard viewer for allocating rental areas to lease

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF TEMP-TABLE RSP NO-UNDO LIKE RentalSpace
  FIELD rsp-list AS CHAR  /* The list of RentalSpace ROWIDS that this rental
                             space rental space */
  FIELD Sequence AS INT   /* This will uniquely identify any rental space
                             whether it has been split or combined or neither */
  FIELD Lease    LIKE TenancyLease.TenancyLeaseCode
  
  INDEX Sequence IS UNIQUE PRIMARY
    Sequence.
    
DEF VAR break-1 AS CHAR FORMAT "X" INIT " " LABEL " " NO-UNDO.
DEF VAR primary-space     AS INT  NO-UNDO INIT ?.
DEF VAR attached-to       AS CHAR NO-UNDO FORMAT "X(10)" LABEL "Assignment".
DEF VAR this-lease        LIKE TenancyLease.TenancyLeaseCode INIT -1 NO-UNDO.

DEF VAR seq-ctr           AS INT NO-UNDO INIT 0. /* Keep track of the rsp seq index */

DEF VAR property-code AS INT NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-rntspc

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES RSP

/* Definitions for BROWSE br-rntspc                                     */
&Scoped-define FIELDS-IN-QUERY-br-rntspc attached-to @ attached-to RSP.Level RSP.LevelSequence RSP.AreaSize RSP.ContractedRental RSP.Description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-rntspc   
&Scoped-define FIELD-PAIRS-IN-QUERY-br-rntspc
&Scoped-define SELF-NAME br-rntspc
&Scoped-define OPEN-QUERY-br-rntspc OPEN QUERY br-rntspc FOR EACH RSP NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br-rntspc RSP
&Scoped-define FIRST-TABLE-IN-QUERY-br-rntspc RSP


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-rntspc}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-22 btn_split btn_combine btn_restart ~
btn_primary btn_remove btn_add br-rntspc 
&Scoped-Define DISPLAYED-OBJECTS fil_description 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD first-selected-row V-table-Win 
FUNCTION first-selected-row RETURNS INTEGER
  ( INPUT h-browse AS HANDLE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-property-code V-table-Win 
FUNCTION get-property-code RETURNS INTEGER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-rent-amount V-table-Win 
FUNCTION get-rent-amount RETURNS DECIMAL
  (  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 9.72 BY .9
     FONT 9.

DEFINE BUTTON btn_combine 
     LABEL "&Merge" 
     SIZE 6.86 BY .9.

DEFINE BUTTON btn_primary 
     LABEL "Set As &Primary" 
     SIZE 11.43 BY .9
     FONT 9.

DEFINE BUTTON btn_remove 
     LABEL "&Remove" 
     SIZE 9.72 BY .9
     FONT 9.

DEFINE BUTTON btn_restart 
     LABEL "&Start Again" 
     SIZE 11.43 BY .9.

DEFINE BUTTON btn_split 
     LABEL "&Split" 
     SIZE 5.72 BY .9.

DEFINE VARIABLE fil_description AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 62.29 BY .8
     FGCOLOR 1  NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 72 BY 14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-rntspc FOR 
      RSP SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-rntspc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-rntspc V-table-Win _FREEFORM
  QUERY br-rntspc NO-LOCK DISPLAY
      attached-to @ attached-to
      RSP.Level COLUMN-LABEL 'Lvl ' FORMAT '->>>9'
      RSP.LevelSequence COLUMN-LABEL 'Seq ' FORMAT '->>9'
      RSP.AreaSize FORMAT '->,>>>,>>9.99' COLUMN-LABEL '  Area '
      RSP.ContractedRental FORMAT '->>>,>>>,>>9.99' COLUMN-LABEL 'Contracted'
      RSP.Description FORMAT 'X(40)' COLUMN-LABEL 'Description'
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS MULTIPLE SIZE 70.86 BY 9.8
         FONT 10 TOOLTIP "Select each space and click on 'Add' to assign them to this lease".


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     btn_split AT ROW 3.4 COL 1.57
     btn_combine AT ROW 3.4 COL 7.86
     btn_restart AT ROW 3.4 COL 15.29
     btn_primary AT ROW 3.4 COL 40.43
     btn_remove AT ROW 3.4 COL 52.43
     btn_add AT ROW 3.4 COL 62.72
     br-rntspc AT ROW 4.2 COL 1.57
     fil_description AT ROW 14 COL 8.14 COLON-ALIGNED NO-LABEL
     "Description:" VIEW-AS TEXT
          SIZE 8 BY .9 AT ROW 14 COL 2.14
     "Select changed spaces from the list and then 'Add' or 'Remove' them from the lease" VIEW-AS TEXT
          SIZE 56.57 BY .7 AT ROW 2.4 COL 3.29
          FGCOLOR 1 FONT 10
     RECT-22 AT ROW 1 COL 1
     "Which rental spaces are being leased ?" VIEW-AS TEXT
          SIZE 54.86 BY 1.25 AT ROW 1.2 COL 1.57
          FGCOLOR 1 FONT 12
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18.2
         WIDTH              = 75.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-rntspc btn_add F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       btn_combine:HIDDEN IN FRAME F-Main           = TRUE.

ASSIGN 
       btn_restart:HIDDEN IN FRAME F-Main           = TRUE.

ASSIGN 
       btn_split:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN fil_description IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       fil_description:HIDDEN IN FRAME F-Main           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-rntspc
/* Query rebuild information for BROWSE br-rntspc
     _START_FREEFORM
OPEN QUERY br-rntspc FOR EACH RSP NO-LOCK.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is OPENED
*/  /* BROWSE br-rntspc */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/method/m-mntvwr.i}
{inc/null.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br-rntspc
&Scoped-define SELF-NAME br-rntspc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-rntspc V-table-Win
ON ROW-DISPLAY OF br-rntspc IN FRAME F-Main
DO:
  RUN set-line-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-rntspc V-table-Win
ON VALUE-CHANGED OF br-rntspc IN FRAME F-Main
DO:
  RUN display-space.
  RUN sensitise-buttons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN add-spaces.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_combine
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_combine V-table-Win
ON CHOOSE OF btn_combine IN FRAME F-Main /* Merge */
DO:
  RUN combine-spaces.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_primary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_primary V-table-Win
ON CHOOSE OF btn_primary IN FRAME F-Main /* Set As Primary */
DO:
  RUN set-primary.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_remove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_remove V-table-Win
ON CHOOSE OF btn_remove IN FRAME F-Main /* Remove */
DO:
  RUN remove-spaces.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_restart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_restart V-table-Win
ON CHOOSE OF btn_restart IN FRAME F-Main /* Start Again */
DO:
  RUN open-rntspc-query.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_split
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_split V-table-Win
ON CHOOSE OF btn_split IN FRAME F-Main /* Split */
DO:
  RUN split-space.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-spaces V-table-Win 
PROCEDURE add-spaces :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR j AS INT NO-UNDO.
DEF VAR lease-list AS CHAR NO-UNDO.
DEF VAR lease-msg  AS CHAR NO-UNDO.
  
  /* For each selected space - add it to this lease */
  space-iteration:
  DO i = 1 TO br-rntspc:NUM-SELECTED-ROWS IN FRAME {&FRAME-NAME}:
    IF br-rntspc:FETCH-SELECTED-ROW(i) THEN.

    /* Confirm any space already assigned to other leases */
    lease-msg = "".
    
    IF RSP.Lease <> this-lease THEN
    DO j = 1 TO NUM-ENTRIES( RSP.rsp-list ):
      FIND RentalSpace WHERE ROWID( RentalSpace ) = TO-ROWID( ENTRY( j, RSP.rsp-list ) )
        NO-LOCK NO-ERROR.
      FIND FIRST TenancyLease WHERE TenancyLease.TenancyLeaseCode = RSP.TenancyLeaseCode 
                              AND TenancyLease.TenancyLeaseCode <> this-lease
                              AND TenancyLease.LeaseStatus <> "PAST" NO-LOCK NO-ERROR.
      IF AVAILABLE TenancyLease THEN DO:
        FIND Tenant OF TenancyLease NO-LOCK NO-ERROR.
        lease-msg = lease-msg + IF lease-msg = "" THEN "" ELSE "~n".
        lease-msg = lease-msg + Tenant.Name + " ( Lease " + STRING( TenancyLease.TenancyLeaseCode ) + " )".
      END.
    END.

    IF lease-msg <> "" THEN DO:
      MESSAGE
        "The selected space -" RSP.Description SKIP
        "is already assigned to one or more leases:" SKIP(1)
        lease-msg SKIP(1)
        "Do you want to override the previous assignment(s) ?"
        VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "Confirm Rental Space"
          UPDATE confirm-it AS LOGI.
      IF NOT confirm-it THEN NEXT space-iteration.
    END.

    ASSIGN RSP.Lease = this-lease.
    RUN sensitise-buttons.

  END.

  IF {&BROWSE-NAME}:DESELECT-ROWS() THEN.
  IF {&BROWSE-NAME}:REFRESH() THEN.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE combine-spaces V-table-Win 
PROCEDURE combine-spaces :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

DEF VAR i       AS INT NO-UNDO.
DEF VAR new-seq AS INT NO-UNDO.
DEF VAR total-area AS DEC NO-UNDO.
DEF VAR reposition-row AS INT NO-UNDO.
DEF VAR space-desc AS CHAR NO-UNDO.
DEF BUFFER CombinedSpace FOR RSP.

  reposition-row = null-int( first-selected-row( br-rntspc:HANDLE ), 0 ).
  
  DO i = 1 TO br-rntspc:NUM-SELECTED-ROWS IN FRAME {&FRAME-NAME}:
    IF br-rntspc:FETCH-SELECTED-ROW(i) THEN.

    space-desc = space-desc + ( IF space-desc = "" THEN "" ELSE " & " ) +
      RSP.Description.
    IF i = 1 THEN DO:
      CREATE CombinedSpace.
      BUFFER-COPY RSP TO CombinedSpace
      ASSIGN
        CombinedSpace.Lease    = this-lease
        CombinedSpace.Sequence = 0
        CombinedSpace.Description = "Merge " + RSP.Description.
        new-seq = RSP.Sequence.
    END.
    ELSE DO:
      ASSIGN
        CombinedSpace.AreaSize = CombinedSpace.AreaSize + RSP.AreaSize
        CombinedSpace.rsp-list = CombinedSpace.rsp-list + 
          ( IF CombinedSpace.rsp-list = "" THEN "" ELSE "," ) + RSP.rsp-list.
    END.

    DELETE RSP.
  END.

  IF br-rntspc:DESELECT-ROWS() THEN.
  ASSIGN
    CombinedSpace.Sequence    = new-seq
    CombinedSpace.Description = "Merge " + space-desc.

  OPEN QUERY br-rntspc FOR EACH RSP.
  IF {&BROWSE-NAME}:SET-REPOSITIONED-ROW( reposition-row, "CONDITIONAL" ) THEN.
  REPOSITION {&BROWSE-NAME} TO ROWID ROWID( CombinedSpace ).
  IF {&BROWSE-NAME}:SELECT-FOCUSED-ROW() THEN.

  RUN display-space.
  RUN set-line-fields.
  RUN sensitise-buttons.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE description-changed V-table-Win 
PROCEDURE description-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  fil_description:SCREEN-VALUE =
    IF NUM-RESULTS( "br-rntspc" ) = 0 OR
       NUM-RESULTS( "br-rntspc" ) = ?
    THEN ""
    ELSE RSP.Description.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-space V-table-Win 
PROCEDURE display-space :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN description-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  this-lease = INT( get-result( 'Current-Lease':U ) ).
  IF this-lease = ? THEN this-lease = -1.
  property-code = get-property-code().
  IF NOT( CAN-FIND(FIRST RSP WHERE RSP.PropertyCode = property-code) ) THEN
    RUN initialise-rental-spaces.
  RUN sensitise-buttons.
  test-validated().
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialise-rental-spaces V-table-Win 
PROCEDURE initialise-rental-spaces :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN open-rntspc-query.
  test-validated().
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-rntspc-query V-table-Win 
PROCEDURE open-rntspc-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  FOR EACH RSP: DELETE RSP. END.
  FIND Property WHERE Property.PropertyCode = property-code NO-LOCK NO-ERROR.

  IF NOT AVAILABLE Property THEN
  DO:
    CLOSE QUERY br-rntspc.
    RETURN.
  END.
  
  FOR EACH RentalSpace OF Property NO-LOCK
        BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence:
    CREATE RSP.
    BUFFER-COPY RentalSpace TO RSP
    ASSIGN
      RSP.rsp-list         = STRING( ROWID( RentalSpace ) )
      RSP.Lease            = RentalSpace.TenancyLeaseCode
      seq-ctr              = seq-ctr + 1
      RSP.Sequence         = seq-ctr.
  END.

  OPEN QUERY br-rntspc FOR EACH RSP.
  RUN display-space.
  RUN set-line-fields.
  RUN sensitise-buttons.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-spaces V-table-Win 
PROCEDURE remove-spaces :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR i AS INT NO-UNDO.
  
  DO i = 1 TO br-rntspc:NUM-SELECTED-ROWS IN FRAME {&FRAME-NAME}:
    IF br-rntspc:FETCH-SELECTED-ROW(i) THEN.
    IF primary-space = RSP.Sequence THEN primary-space = ?.

    IF NUM-ENTRIES( RSP.rsp-list ) > 1 THEN DO:
      ASSIGN RSP.Lease = ?.
    END.
    ELSE IF NUM-ENTRIES( RSP.rsp-list ) = 1 THEN DO:
      FIND RentalSpace WHERE ROWID( RentalSpace ) = TO-ROWID( RSP.rsp-list ) NO-LOCK NO-ERROR.
      RSP.Lease = (IF AVAILABLE(RentalSpace) AND RentalSpace.TenancyLeaseCode <> this-lease
                   THEN RentalSpace.TenancyLeaseCode ELSE 0).
    END.
    
  END.

  IF {&BROWSE-NAME}:DESELECT-ROWS() THEN.
  IF {&BROWSE-NAME}:REFRESH() THEN.
  RUN sensitise-buttons.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RSP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-buttons V-table-Win 
PROCEDURE sensitise-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  DO WITH FRAME {&FRAME-NAME}:
    btn_add:SENSITIVE     = AVAILABLE RSP AND RSP.Lease <> this-lease.
    btn_remove:SENSITIVE  = AVAILABLE RSP AND RSP.Lease =  this-lease.
    btn_primary:SENSITIVE = AVAILABLE RSP AND RSP.Lease =  this-lease
                                          AND RSP.RentalSpaceCode  <>  primary-space
                                          AND br-rntspc:NUM-SELECTED-ROWS = 1.
    btn_split:SENSITIVE   = br-rntspc:NUM-SELECTED-ROWS = 1.
    btn_combine:SENSITIVE = br-rntspc:NUM-SELECTED-ROWS > 1.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-line-fields V-table-Win 
PROCEDURE set-line-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE RSP THEN RETURN.
  
  DEF VAR fgcolor AS INT NO-UNDO.
  fgcolor =
    IF RSP.Sequence         = primary-space THEN 2  ELSE
    IF RSP.Lease            = this-lease    THEN 12 ELSE
    IF RSP.TenancyLeaseCode = this-lease    THEN 10 ELSE
    IF NUM-ENTRIES( RSP.rsp-list ) > 1      THEN 1 ELSE
    IF CAN-FIND( FIRST TenancyLease WHERE TenancyLease.TenancyLeaseCode = RSP.TenancyLeaseCode
        AND TenancyLease.LeaseStatus <> "PAST" ) THEN 8 ELSE ?.
        
  attached-to = 
    IF RSP.Sequence         = primary-space THEN "Primary" ELSE
    IF RSP.Lease            = this-lease THEN "This lease" ELSE
    IF RSP.TenancyLeaseCode = this-lease THEN "To Vacate" ELSE
    IF NUM-ENTRIES( RSP.rsp-list ) > 1   THEN "N/A" ELSE
    IF CAN-FIND( FIRST TenancyLease WHERE TenancyLease.TenancyLeaseCode = RSP.TenancyLeaseCode
        AND TenancyLease.LeaseStatus <> "PAST" ) THEN "to " + STRING( RSP.TenancyleaseCode, "9999" )
    ELSE "".
    
  ASSIGN
    RSP.Level:FGCOLOR IN BROWSE br-rntspc = fgcolor
    RSP.Description:FGCOLOR = fgcolor
    attached-to:FGCOLOR = fgcolor.
  DISPLAY attached-to WITH BROWSE {&BROWSE-NAME} NO-ERROR.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-primary V-table-Win 
PROCEDURE set-primary :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/

  IF br-rntspc:NUM-SELECTED-ROWS IN FRAME {&FRAME-NAME} > 0 THEN DO:
    IF br-rntspc:FETCH-SELECTED-ROW(1) THEN.  
    primary-space = RSP.Sequence.
    OPEN QUERY br-rntspc FOR EACH RSP.
    RUN sensitise-buttons.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE split-space V-table-Win 
PROCEDURE split-space :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

DEF VAR i       AS INT NO-UNDO.
DEF VAR new-seq AS INT NO-UNDO.
DEF VAR reposition-row   AS INT NO-UNDO.
DEF VAR reposition-rowid AS ROWID NO-UNDO.
DEF BUFFER SplitSpace FOR RSP.
DEF BUFFER OtherSpace FOR RSP.

  IF br-rntspc:NUM-SELECTED-ROWS > 1 THEN RETURN.
  reposition-row = null-int( first-selected-row( br-rntspc:HANDLE ), 0 ).
  
  IF br-rntspc:FETCH-SELECTED-ROW(1) THEN.
  reposition-rowid = ROWID( RSP ).

  /* Make room for the new space sequence number */

  FOR EACH OtherSpace WHERE OtherSpace.Sequence > RSP.Sequence
    BY OtherSpace.Sequence DESCENDING:
    ASSIGN OtherSpace.Sequence = OtherSpace.Sequence + 1.
  END.
  
  /* Create a secondary space and divide appropriately */
  ASSIGN
    RSP.AreaSize = RSP.AreaSize / 2
    RSP.ChargedRental = RSP.ChargedRental / 2
    RSP.ContractedRental = RSP.ContractedRental / 2
    RSP.Description        =
      ( IF SUBSTR( RSP.Description, 1, 5 ) = "Part " THEN "" ELSE  "Part " ) 
      + RSP.Description
    RSP.Lease = this-lease.
        
  CREATE SplitSpace.
  BUFFER-COPY RSP TO SplitSpace
  ASSIGN SplitSpace.Sequence = RSP.Sequence + 1.

  /* Reopen the query and display the new rows */
  IF br-rntspc:DESELECT-ROWS() THEN.
  OPEN QUERY br-rntspc FOR EACH RSP.
  IF {&BROWSE-NAME}:SET-REPOSITIONED-ROW( reposition-row, "CONDITIONAL" ) THEN.
  REPOSITION {&BROWSE-NAME} TO ROWID reposition-rowid.
  IF {&BROWSE-NAME}:SELECT-FOCUSED-ROW() THEN.

  RUN display-space.
  RUN set-line-fields.
  RUN sensitise-buttons.
  
END.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR rsp-list AS CHAR NO-UNDO.
  
  FOR EACH RSP WHERE RSP.Lease = this-lease:
    rsp-list = rsp-list + STRING( RSP.RentalSpaceCode ) + ",".
  END.

  set-result( 'Rental-Spaces', TRIM(rsp-list, ",") ).
  set-result( 'Primary-Space', STRING( primary-space ) ).
  set-result( 'Rent-Amount', STRING( get-rent-amount() ) ).

  RETURN test-validated().

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION first-selected-row V-table-Win 
FUNCTION first-selected-row RETURNS INTEGER
  ( INPUT h-browse AS HANDLE ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  DEF VAR browse-row AS INT INIT 1 NO-UNDO.
  
  DO WHILE browse-row <= h-browse:HEIGHT-CHARS:
    IF h-browse:IS-ROW-SELECTED( browse-row ) THEN RETURN browse-row.
    browse-row = browse-row + 1.
  END.
    
  RETURN ?.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-property-code V-table-Win 
FUNCTION get-property-code RETURNS INTEGER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Get the property code
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR p-code AS INT NO-UNDO.
  p-code = INT( get-result( 'Property':U ) ).
  IF p-code > 0 THEN RETURN p-code.

  p-code = INT( get-result( 'Current-Lease':U ) ).
  IF p-code > 0 THEN DO:
    FIND TenancyLease WHERE TenancyLease.TenancyLeaseCode = p-code NO-LOCK NO-ERROR.
    IF AVAILABLE(TenancyLease) THEN RETURN TenancyLease.PropertyCode.
  END.
  RETURN ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-rent-amount V-table-Win 
FUNCTION get-rent-amount RETURNS DECIMAL
  (  ) :
/*------------------------------------------------------------------------------
  Purpose:  Get a default rental amount based on the selected rental spaces
    Notes:  
------------------------------------------------------------------------------*/

  DEF BUFFER Prop FOR Property.
  DEF BUFFER RentSpace FOR RentalSpace.
  DEF VAR rent-amount AS DEC NO-UNDO.
  DEF VAR property-code LIKE Property.PropertyCode NO-UNDO.
  DEF VAR rsp-list AS CHAR NO-UNDO.
  DEF VAR i AS INT NO-UNDO.
  
  property-code = INT( get-result( 'Property' ) ).
  FIND FIRST Prop WHERE Prop.PropertyCode = property-code
    NO-LOCK NO-ERROR.
  IF NOT AVAILABLE Prop THEN RETURN 0.00.
  
  rsp-list = get-result( 'Rental-Spaces' ).
  IF rsp-list = ? THEN RETURN 0.00.
  
  DO i = 1 TO NUM-ENTRIES( rsp-list ):
    FIND RentSpace OF Prop
    WHERE RentSpace.RentalSpaceCode = INT( ENTRY( i, rsp-list ) )
      NO-LOCK NO-ERROR.
    IF AVAILABLE RentSpace AND RentSpace.ChargedRental <> ? THEN 
      rent-amount = rent-amount + RentSpace.ContractedRental.
  END.
    
  RETURN rent-amount.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  set-validated( Yes ).
  RETURN "".   /* Function return value. */

END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


