&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR debug-mode AS LOGI NO-UNDO INITIAL No.
{inc/ofc-this.i}
{inc/ofc-acct.i "RENT" "rent-account"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES TenancyLease
&Scoped-define FIRST-EXTERNAL-TABLE TenancyLease


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR TenancyLease.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-22 tgl_Invoice fil_InvoiceFrom ~
fil_InvoiceTo 
&Scoped-Define DISPLAYED-OBJECTS tgl_Invoice fil_InvoiceFrom fil_InvoiceTo 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD include-lease V-table-Win 
FUNCTION include-lease RETURNS LOGICAL
  ( INPUT lease-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fil_InvoiceFrom AS DATE FORMAT "99/99/9999":U 
     LABEL "For the period from" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_InvoiceTo AS DATE FORMAT "99/99/9999":U 
     LABEL "to" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1.05 NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 54.29 BY 12.

DEFINE VARIABLE tgl_Invoice AS LOGICAL INITIAL ? 
     LABEL "Generate rental invoice now?" 
     VIEW-AS TOGGLE-BOX
     SIZE 24 BY .8 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     tgl_Invoice AT ROW 10.6 COL 3.86
     fil_InvoiceFrom AT ROW 11.6 COL 19 COLON-ALIGNED
     fil_InvoiceTo AT ROW 11.6 COL 33.86 COLON-ALIGNED
     RECT-22 AT ROW 1 COL 1
     "Rent Review Complete !" VIEW-AS TEXT
          SIZE 33.72 BY 1 AT ROW 1 COL 12.43
          FGCOLOR 1 FONT 12
     "Congratulations !" VIEW-AS TEXT
          SIZE 12 BY 1 AT ROW 2.4 COL 22.14
          FGCOLOR 1 
     "You have entered enough information to complete this rent review" VIEW-AS TEXT
          SIZE 45.14 BY 1 AT ROW 3.4 COL 5.57
          FGCOLOR 1 
     "If you are satisfied that you have entered all of the review details then" VIEW-AS TEXT
          SIZE 47.43 BY 1 AT ROW 5.4 COL 4.43
          FGCOLOR 1 
     "you can click the 'Finish' button and a rent review will be created using" VIEW-AS TEXT
          SIZE 49.14 BY 1 AT ROW 6.2 COL 3.86
          FGCOLOR 1 
     "the information you have entered" VIEW-AS TEXT
          SIZE 23.43 BY 1 AT ROW 7 COL 15.86
          FGCOLOR 1 
     "Otherwise you can go back to previous steps using the 'Back' button" VIEW-AS TEXT
          SIZE 46.86 BY 1 AT ROW 9 COL 4.43
          FGCOLOR 1 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.TenancyLease
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 15.55
         WIDTH              = 58.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/method/m-mntvwr.i}
{inc/null.i}
{inc/rentchrg.i}
{inc/string.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME tgl_Invoice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_Invoice V-table-Win
ON VALUE-CHANGED OF tgl_Invoice IN FRAME F-Main /* Generate rental invoice now? */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE action-review V-table-Win 
PROCEDURE action-review :
/*------------------------------------------------------------------------------
  Purpose:     Try to complete the rent review using the given details
               If there is an error then return.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

action-review:
DO TRANSACTION ON ERROR UNDO action-review, RETURN "FAIL":

  ON WRITE OF RentCharge OVERRIDE DO: END.
  
  DEF VAR current-lease LIKE TenancyLease.TenancyLeaseCode NO-UNDO.
  DEF VAR current-review AS ROWID NO-UNDO.
  
  current-review = TO-ROWID( get-result( 'Current-Review' ) ).
  current-lease = INT( get-result( 'Current-Lease' ) ).
  FIND TenancyLease WHERE TenancyLease.TenancyLeaseCode = current-lease
    NO-LOCK.
  
  /* Create the rent review */
  FIND RentReview WHERE ROWID( RentReview ) = current-review
    EXCLUSIVE-LOCK.

  ASSIGN
    RentReview.DateComplete =
             DATE( null-str( get-result( 'Date-Complete' ), STRING( TODAY ) ) )
    RentReview.ReviewStatus = "DONE"
    RentReview.MarketRentalDate =
             DATE( null-str( get-result( 'Market-Date' ), STRING( TODAY ) ) )
    RentReview.MarketRental =
             DEC( null-str( get-result( 'Market-Rental' ), "0.00" ) )
  .

  /* Apply the changes to the charging schedule */
  DEF VAR rental-charges AS CHAR NO-UNDO.
  DEF VAR charge-line    AS CHAR NO-UNDO.
  DEF VAR type           AS CHAR NO-UNDO.
  DEF VAR i AS INT NO-UNDO.
  DEF VAR ld AS CHAR NO-UNDO.
  DEF VAR fd AS CHAR NO-UNDO.
  ld = "~n". fd = "~~".
  
  rental-charges = null-str( get-result( 'Rental-Charges' ), "" ).

  /* Iterate over all the charges and apply the changes */
  DO i = 1 TO NUM-ENTRIES( rental-charges, ld ):

    charge-line = ENTRY( i, rental-charges, ld ).
    type        = ENTRY( 1, charge-line, fd ).

    CASE type:
      WHEN "New" OR WHEN "Related" THEN DO:
        
        /* Schedule a new charge */
        IF type = "New" THEN DO:
            DEF BUFFER LastCharge FOR RentCharge.
            /* Create a new set of rent charges */
            FIND LAST LastCharge OF TenancyLease NO-LOCK NO-ERROR.
            CREATE RentCharge.
            ASSIGN
              RentCharge.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
              RentCharge.SequenceCode     = (IF AVAILABLE LastCharge THEN LastCharge.SequenceCode + 1 ELSE 1)
              RentCharge.EntityType       = "P"
              RentCharge.EntityCode       = TenancyLease.PropertyCode.
            RentCharge.Description      = null-str( ENTRY( 10, charge-line, fd ), "" ).
            RentCharge.RentChargeType   = null-str( ENTRY( 5, charge-line, fd ), "" ).
            RentCharge.AccountCode      = null-dec( DEC( ENTRY( 9, charge-line, fd ) ), 0000.00 ).
        END.
        ELSE DO:
            /* Find the existing set */
            FIND  RentCharge
            WHERE RentCharge.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
              AND RentCharge.SequenceCode     = INT( ENTRY( 2, charge-line, fd ) )
              NO-LOCK.
        END.

        /* Add an entry to the schedule */

        CREATE RentChargeLine.
        BUFFER-COPY RentReview TO RentChargeLine.
        BUFFER-COPY Rentcharge TO RentChargeLine
        ASSIGN
          RentChargeLine.RentChargeLineStatus = "R" /* Under Review */
          RentChargeLine.StartDate       = DATE( ENTRY( 3, charge-line, fd ) )
          RentChargeLine.EndDate         = DATE( ENTRY( 4, charge-line, fd ) ).
        RentChargeLine.Amount          = null-dec( DEC( ENTRY( 7, charge-line, fd ) ), 0.00 ).
        RentChargeLine.FrequencyCode   = null-str( ENTRY( 6, charge-line, fd ), "" ) .
        
        RUN commit-charge-entry( RentReview.TenancyLeaseCode,
                          RentChargeLine.SequenceCode, RentChargeLine.StartDate ).
      END.

      WHEN "Current" THEN DO:
        FIND RentChargeLine
        WHERE RentChargeLine.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
          AND RentChargeLine.SequenceCode = INT( ENTRY( 2, charge-line, fd ) )
          AND RentChargeLine.StartDate    = DATE( ENTRY( 3, charge-line, fd ) )
          AND RentchargeLine.RentChargeLineStatus = "C"
          EXCLUSIVE-LOCK.
        ASSIGN RentChargeLine.EndDate    = DATE( ENTRY( 4, charge-line, fd ) ).
      END.
 
    END.
  
  END.

END. /* Transaction */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "TenancyLease"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "TenancyLease"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE default-invoice-dates V-table-Win 
PROCEDURE default-invoice-dates :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR rental-charges AS CHAR NO-UNDO.
DEF VAR charge-line    AS CHAR NO-UNDO.
DEF VAR type           AS CHAR NO-UNDO.
DEF VAR def-start      AS DATE NO-UNDO.
DEF VAR this-start     AS DATE NO-UNDO.
DEF VAR this-end       AS DATE NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.
DEF VAR ld AS CHAR NO-UNDO.
DEF VAR fd AS CHAR NO-UNDO.
ld = "~n". fd = "~~".
def-start = last-of-month( TODAY + 1000 ) + 1.

  IF tgl_Invoice <> ? THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:

  rental-charges = null-str( get-result( 'Rental-Charges' ), "" ).

  /* Iterate over all the charges and apply the changes */
  n = NUM-ENTRIES( rental-charges, ld ).
  DO i = 1 TO n:

    charge-line = ENTRY( i, rental-charges, ld ).
    type        = ENTRY( 1, charge-line, fd ).
    this-start  = DATE( ENTRY( 3, charge-line, fd )).
    this-end    = DATE( ENTRY( 4, charge-line, fd )).

    IF type = "New" OR type = "Related" THEN DO:
      IF this-start < def-start THEN def-start = this-start.
    END.
    ELSE DO:
      IF this-end < def-start THEN def-start = this-end + 1.
    END.
  END.

  tgl_Invoice = (def-start <= last-of-month(TODAY)).
  fil_InvoiceFrom = def-start.
  fil_InvoiceTo = last-of-month( def-start ).
  IF last-of-month(TODAY) > fil_InvoiceTo THEN
    fil_InvoiceTo = last-of-month( TODAY ).

  DISPLAY       tgl_Invoice      fil_InvoiceFrom     fil_InvoiceTo .

END.

  RUN enable-appropriate-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  set-validated( Yes ).
  RUN default-invoice-dates.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT tgl_Invoice THEN
    ENABLE fil_InvoiceFrom fil_InvoiceTo .
  ELSE
    DISABLE fil_InvoiceFrom fil_InvoiceTo .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE finish-wizard V-table-Win 
PROCEDURE finish-wizard :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR complete-it AS LOGICAL NO-UNDO INIT Yes.
  
  DO WHILE complete-it:
    RUN action-review.
    IF RETURN-VALUE = "FAIL" THEN DO:
      MESSAGE
        "There was an error actioning the rent review!" SKIP(2)
        "Do you want to try again ?"
        VIEW-AS ALERT-BOX QUESTION BUTTONS RETRY-CANCEL TITLE "Database Error" 
        UPDATE complete-it.
    END.
    ELSE DO:
      IF INPUT FRAME {&FRAME-NAME} tgl_Invoice THEN RUN generate-invoice.
      MESSAGE "The rent review has been successfully actioned."
        VIEW-AS ALERT-BOX INFORMATION TITLE "Done!".
      complete-it = No.
    END.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE generate-invoice V-table-Win 
PROCEDURE generate-invoice :
/*------------------------------------------------------------------------------
  Purpose:  Calculate all of the details of the tenants rental for the
            period indicated.
------------------------------------------------------------------------------*/
DEF VAR current-lease AS INT NO-UNDO.
DEF VAR period-d1 AS DATE NO-UNDO.
DEF VAR period-dn AS DATE NO-UNDO.
DEF VAR paid-to-list AS CHAR NO-UNDO.

DEF VAR invoice-total  AS DEC NO-UNDO     INITIAL 0.
DEF VAR invoice-tax    AS DEC NO-UNDO     INITIAL 0.
DEF VAR invoice-amount AS DEC NO-UNDO.
DEF VAR invoice-blurb  AS CHAR NO-UNDO    INITIAL "".

{inc/ofc-this.i}
{inc/ofc-set.i "Invoice-Terms" "default-terms" "WARNING"}

  IF ( INPUT FRAME {&FRAME-NAME} fil_InvoiceFrom = ?
                 OR INPUT FRAME {&FRAME-NAME} fil_InvoiceTo = ? ) THEN RETURN.


  current-lease = INT( get-result( 'Current-Lease' ) ).
  FIND TenancyLease WHERE TenancyLease.TenancyLeaseCode = current-lease NO-LOCK.
  FIND Tenant OF TenancyLease NO-LOCK.
  period-d1   = (INPUT FRAME {&FRAME-NAME} fil_Invoicefrom).
  period-dn   = (INPUT FRAME {&FRAME-NAME} fil_Invoiceto).

  RUN build-tenant-charges( Tenant.TenantCode, period-d1, period-dn ).
  RUN make-blurb( "charges", period-d1, period-dn, OUTPUT invoice-blurb,
                OUTPUT invoice-amount, OUTPUT invoice-tax, OUTPUT invoice-total ).

/******** Now actually create the invoice **********/
  CREATE    Invoice.
  ASSIGN    Invoice.InvoiceDate = TODAY
            Invoice.TaxApplies  = Office.GST <> ?
            Invoice.InvoiceStatus = "U"
            Invoice.Terms         = default-terms
            Invoice.InvoiceType   = "RENT"
            Invoice.EntityType    = "T"
            Invoice.EntityCode    = Tenant.TenantCode
            Invoice.ToDetail = "Rent charges from "
                             + STRING( period-d1, "99/99/9999" )
                             + " to " + STRING( period-dn, "99/99/9999" )
            Invoice.Blurb     = invoice-blurb
            Invoice.TaxAmount = invoice-tax
            Invoice.Total     = invoice-total.

  paid-to-list = "".
  FOR EACH ChargeDetail:
    paid-to-list = paid-to-list
                 + STRING( ChargeDetail.TenancyLeaseCode ) + "|"
                 + STRING( ChargeDetail.ChargeSeq) + "|"
                 + STRING( ChargeDetail.ChargeStart, "99/99/9999") + "|"
                 + STRING( ChargeDetail.ChargedUpTo, "99/99/9999")
                 + ",".
  END.
  Invoice.ToPay = RIGHT-TRIM( paid-to-list, ",").


DEF VAR last-type AS CHAR NO-UNDO INITIAL "not a possible type".
DEF VAR last-account AS DEC NO-UNDO INITIAL -999999.999 .
DEF VAR charge-from AS DATE NO-UNDO .

  FOR EACH InvoiceLine OF Invoice:      DELETE InvoiceLine.     END.

ON ASSIGN OF InvoiceLine.YourShare OVERRIDE DO: END.
  FOR EACH ChargeDetail:
    CREATE  InvoiceLine.
    ASSIGN  InvoiceLine.InvoiceNo   = Invoice.InvoiceNo
            InvoiceLine.EntityType  = Tenant.EntityType
            InvoiceLine.EntityCode  = Tenant.EntityCode
            InvoiceLine.AccountCode = ChargeDetail.AccountCode
            InvoiceLine.Percent     = 100.00
            InvoiceLine.AccountText = ChargeDetail.Description
                                    + " from " + STRING( ChargeDetail.ChargedFrom, "99/99/9999")
                                    + " to " + STRING( ChargeDetail.ChargedUpTo, "99/99/9999")
            InvoiceLine.Amount      = ChargeDetail.ChargeAmount
            InvoiceLine.YourShare   = InvoiceLine.Amount.
  END.
ON ASSIGN OF InvoiceLine.YourShare REVERT.

/*
  MESSAGE "Invoice" Invoice.InvoiceNo "created!" VIEW-AS ALERT-BOX INFORMATION
            TITLE "Invoice Created".
*/
  RUN process/report/invcappr.p( Invoice.InvoiceNo, Invoice.InvoiceNo, ? ).
                
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "TenancyLease"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  set-validated( No ).
  RETURN "FAIL".
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION include-lease V-table-Win 
FUNCTION include-lease RETURNS LOGICAL
  ( INPUT lease-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  Dummy function.
------------------------------------------------------------------------------*/

  RETURN Yes.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


