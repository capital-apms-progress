&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF TEMP-TABLE RSP NO-UNDO LIKE RentalSpace
  INDEX RSP-Sequence IS PRIMARY Level LevelSequence 
  INDEX RSP-Code IS UNIQUE RentalSpaceCode .

DEF VAR property-code LIKE Property.PropertyCode NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-rntspc

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES RSP

/* Definitions for BROWSE br-rntspc                                     */
&Scoped-define FIELDS-IN-QUERY-br-rntspc RSP.Level RSP.LevelSequence RSP.AreaSize RSP.ContractedRental RSP.Description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-rntspc RSP.ContractedRental   
&Scoped-define ENABLED-TABLES-IN-QUERY-br-rntspc RSP
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br-rntspc RSP
&Scoped-define SELF-NAME br-rntspc
&Scoped-define OPEN-QUERY-br-rntspc OPEN QUERY br-rntspc FOR EACH RSP NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br-rntspc RSP
&Scoped-define FIRST-TABLE-IN-QUERY-br-rntspc RSP


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-rntspc}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br-rntspc RECT-22 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD first-selected-row V-table-Win 
FUNCTION first-selected-row RETURNS INTEGER
  ( INPUT h-browse AS HANDLE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 65.14 BY 11.4.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-rntspc FOR 
      RSP SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-rntspc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-rntspc V-table-Win _FREEFORM
  QUERY br-rntspc NO-LOCK DISPLAY
      RSP.Level COLUMN-LABEL 'Lvl ' FORMAT '->>>9'
      RSP.LevelSequence COLUMN-LABEL 'Seq ' FORMAT '->>9'
      RSP.AreaSize FORMAT '->,>>>,>>9.99' COLUMN-LABEL '  Area '
      RSP.ContractedRental FORMAT '->>>,>>>,>>9.99' COLUMN-LABEL 'Contracted'
      RSP.Description FORMAT 'X(40)' COLUMN-LABEL 'Description'
ENABLE
      RSP.ContractedRental
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS MULTIPLE SIZE 62.86 BY 9.8
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br-rntspc AT ROW 2.4 COL 1.57
     RECT-22 AT ROW 1 COL 1
     "Allocate rental across the areas leased." VIEW-AS TEXT
          SIZE 56 BY 1 AT ROW 1.2 COL 1.57
          FGCOLOR 1 FONT 12
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.4
         WIDTH              = 76.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/method/m-mntvwr.i}
{inc/null.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-rntspc 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-rntspc
/* Query rebuild information for BROWSE br-rntspc
     _START_FREEFORM
OPEN QUERY br-rntspc FOR EACH RSP NO-LOCK.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is OPENED
*/  /* BROWSE br-rntspc */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 

&Scoped-define BROWSE-NAME br-rntspc

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR rsp-list AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  FOR EACH RSP:     DELETE RSP.     END.
  rsp-list = get-result( 'Rental-Spaces' ).
  property-code = INT( get-result("Property")).

  rsp-list = get-result( 'Rental-Spaces' ).
/*  MESSAGE property-code rsp-list VIEW-AS ALERT-BOX. */
  DO i = 1 TO NUM-ENTRIES(rsp-list):
    FIND RentalSpace WHERE RentalSpace.PropertyCode = property-code
                     AND RentalSpace.RentalSpaceCode = INT(ENTRY(i,rsp-list))
                     NO-LOCK NO-ERROR.
    IF AVAILABLE(RentalSpace) THEN DO:
      CREATE RSP NO-ERROR.
      BUFFER-COPY RentalSpace TO RSP NO-ERROR.
    END.
  END.

  rsp-list = null-str( get-result( 'Contracted-Rentals' ), "").
  /* MESSAGE rsp-list VIEW-AS ALERT-BOX. */
  DO i = 1 TO NUM-ENTRIES(rsp-list):
    FIND RSP WHERE RSP.RentalSpaceCode = INT(ENTRY(1,ENTRY(i,rsp-list),"/")) NO-ERROR.
    IF AVAILABLE(RSP) THEN
      RSP.ContractedRental = DEC(ENTRY(2,ENTRY(i,rsp-list),"/")).
  END.
/*
  FOR EACH RSP:
    MESSAGE RSP.Level SKIP
            RSP.LevelSequence SKIP
            RSP.AreaSize SKIP
            RSP.ContractedRental SKIP
            RSP.Description .
  END.
*/
  test-validated().
  RUN open-rntspc-query.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-rntspc-query V-table-Win 
PROCEDURE open-rntspc-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  OPEN QUERY br-rntspc FOR EACH RSP.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "RSP"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR rsp-list AS CHAR NO-UNDO.
  
  FOR EACH RSP:
    rsp-list = rsp-list + IF rsp-list = "" THEN "" ELSE ",".
    rsp-list = rsp-list + STRING( RSP.RentalSpaceCode ) + "/" + STRING(RSP.ContractedRental).
  END.
  set-result( 'Contracted-Rentals', rsp-list ).

  RETURN test-validated().

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION first-selected-row V-table-Win 
FUNCTION first-selected-row RETURNS INTEGER
  ( INPUT h-browse AS HANDLE ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  DEF VAR browse-row AS INT INIT 1 NO-UNDO.
  
  DO WHILE browse-row <= h-browse:HEIGHT-CHARS:
    IF h-browse:IS-ROW-SELECTED( browse-row ) THEN RETURN browse-row.
    browse-row = browse-row + 1.
  END.
    
  RETURN ?.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  set-validated( Yes ).
  RETURN "".

END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

