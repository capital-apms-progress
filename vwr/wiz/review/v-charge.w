&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
DEF VAR debug-mode AS LOGI NO-UNDO INITIAL No.

DEF WORK-TABLE Charge NO-UNDO
  FIELD Type           AS   CHAR FORMAT "X(8)" LABEL "Type"
  FIELD SequenceCode   LIKE RentCharge.SequenceCode FORMAT ">>9" LABEL "Seq"
  FIELD RentChargeType LIKE RentCharge.RentChargeType LABEL "Type     "
  FIELD StartDate      LIKE RentChargeLine.StartDate  LABEL "Start            "  
  FIELD EndDate        LIKE RentChargeLine.EndDate    LABEL "End               "
  FIELD FrequencyCode  LIKE RentChargeLine.FrequencyCode LABEL "Freq      "
  FIELD Amount         LIKE RentChargeLine.Amount LABEL "Period Amount"
  FIELD Annual         LIKE RentChargeLine.Amount LABEL "Annual"
  FIELD AccountCode    LIKE RentCharge.AccountCode LABEL "Account"
  FIELD Description    LIKE RentCharge.Description FORMAT "X(35)".
  
DEF VAR break-1 AS CHAR FORMAT "X" INIT " " LABEL " " NO-UNDO.
DEF VAR charge-type LIKE RentCharge.RentChargeType NO-UNDO.
DEF VAR frequency-code LIKE RentChargeLine.FrequencyCode NO-UNDO.

DEF VAR account-code LIKE ChartOfAccount.AccountCode NO-UNDO.
DEF VAR got-accounts AS LOGI NO-UNDO.
DEF VAR last-lease   LIKE TenancyLease.TenancyLeaseCode NO-UNDO INIT ?.
  /* Used to keep track of the last lease code to re-initialize the browse */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-charge

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Charge

/* Definitions for BROWSE br-charge                                     */
&Scoped-define FIELDS-IN-QUERY-br-charge Type SequenceCode StartDate EndDate RentChargeType FrequencyCode Annual Amount AccountCode Description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-charge /* Type  SequenceCode */  StartDate  EndDate  RentChargeType  FrequencyCode  Annual  Amount  AccountCode  Description   
&Scoped-define SELF-NAME br-charge
&Scoped-define OPEN-QUERY-br-charge OPEN QUERY {&SELF-NAME} FOR EACH Charge.
&Scoped-define TABLES-IN-QUERY-br-charge Charge
&Scoped-define FIRST-TABLE-IN-QUERY-br-charge Charge


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-charge}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btn_addrel btn_addnew btn_remove br-charge ~
cmb_chargetype cmb_freq cmb_accounts fil_Account RECT-22 
&Scoped-Define DISPLAYED-OBJECTS cmb_chargetype cmb_freq cmb_accounts ~
fil_Account 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD annual-to-freq V-table-Win 
FUNCTION annual-to-freq RETURNS DECIMAL
  ( INPUT freq AS CHAR, INPUT annual AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD freq-to-annual V-table-Win 
FUNCTION freq-to-annual RETURNS DECIMAL
  ( INPUT freq AS CHAR, INPUT amount AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD include-lease V-table-Win 
FUNCTION include-lease RETURNS LOGICAL
  ( INPUT lease-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_addnew 
     LABEL "&Add New" 
     SIZE 11.72 BY 1.

DEFINE BUTTON btn_addrel 
     LABEL "Add &Related" 
     SIZE 11.72 BY 1.

DEFINE BUTTON btn_remove 
     LABEL "&Remove" 
     SIZE 10 BY 1.

DEFINE VARIABLE cmb_accounts AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEMS "?" 
     DROP-DOWN-LIST
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_chargetype AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "?" 
     DROP-DOWN-LIST
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_freq AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "?" 
     DROP-DOWN-LIST
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Account AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 53.14 BY 1
     FGCOLOR 12  NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 100 BY 12.

DEFINE RECTANGLE RECT-35
     EDGE-PIXELS 0  
     SIZE 2 BY .6
     BGCOLOR 1 .

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 0  
     SIZE 2 BY .6
     BGCOLOR 2 .

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 0  
     SIZE 2 BY .6
     BGCOLOR 9 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-charge FOR 
      Charge SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-charge
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-charge V-table-Win _FREEFORM
  QUERY br-charge DISPLAY
      Type
  SequenceCode
  StartDate
  EndDate
  RentChargeType
  FrequencyCode
  Annual
  Amount
  AccountCode
  Description
ENABLE
/*  Type
  SequenceCode */
  StartDate
  EndDate
  RentChargeType
  FrequencyCode
  Annual
  Amount
  AccountCode
  Description
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 98.86 BY 6.8
         BGCOLOR 16 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     btn_addrel AT ROW 5 COL 65
     btn_addnew AT ROW 5 COL 77.57
     btn_remove AT ROW 5 COL 90.14
     br-charge AT ROW 6 COL 1.57
     cmb_chargetype AT ROW 7.4 COL 19 COLON-ALIGNED NO-LABEL
     cmb_freq AT ROW 7.8 COL 29.29 COLON-ALIGNED NO-LABEL
     cmb_accounts AT ROW 9 COL 20.72 COLON-ALIGNED NO-LABEL
     fil_Account AT ROW 5 COL 8.14 COLON-ALIGNED NO-LABEL
     RECT-37 AT ROW 2.8 COL 98.14
     RECT-35 AT ROW 2 COL 98.14
     RECT-36 AT ROW 3.6 COL 98.14
     RECT-22 AT ROW 1 COL 1
     "Account:" VIEW-AS TEXT
          SIZE 6.86 BY 1 AT ROW 5 COL 2.14
     "To add a completely new charge, click on &Add New" VIEW-AS TEXT
          SIZE 36 BY .8 AT ROW 4 COL 2.72
          FGCOLOR 1 
     "Completely new charges are" VIEW-AS TEXT
          SIZE 20 BY 1 AT ROW 3.4 COL 78.14
          FGCOLOR 1 
     "To add a charge related to an active charge, click on Add &Related" VIEW-AS TEXT
          SIZE 46.29 BY .8 AT ROW 3.2 COL 2.72
          FGCOLOR 1 
     "New charges related to currently active charges are" VIEW-AS TEXT
          SIZE 35.43 BY 1 AT ROW 2.6 COL 62.14
          FGCOLOR 1 
     "To terminate a currently active charge, simply set the end date" VIEW-AS TEXT
          SIZE 43.43 BY .8 AT ROW 2.4 COL 2.72
          FGCOLOR 1 
     "Enter the new charging details for this lease" VIEW-AS TEXT
          SIZE 59.43 BY 1.2 AT ROW 1.2 COL 1.57
          FGCOLOR 1 FONT 12
     "Currently active charges are this colour" VIEW-AS TEXT
          SIZE 26.86 BY 1 AT ROW 1.8 COL 70.72
          FGCOLOR 1 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 12.8
         WIDTH              = 101.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/method/m-mntvwr.i}
{inc/null.i}
{inc/rentchrg.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-charge btn_remove F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       cmb_accounts:HIDDEN IN FRAME F-Main           = TRUE.

ASSIGN 
       cmb_chargetype:HIDDEN IN FRAME F-Main           = TRUE.

ASSIGN 
       cmb_freq:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR RECTANGLE RECT-35 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-37 IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-charge
/* Query rebuild information for BROWSE br-charge
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH Charge.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-charge */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br-charge
&Scoped-define SELF-NAME br-charge
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-charge V-table-Win
ON ROW-DISPLAY OF br-charge IN FRAME F-Main
DO:
  RUN set-line-color.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-charge V-table-Win
ON VALUE-CHANGED OF br-charge IN FRAME F-Main
DO:
  RUN display-charge.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_addnew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_addnew V-table-Win
ON CHOOSE OF btn_addnew IN FRAME F-Main /* Add New */
DO:
  RUN add-charge( "New" ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_addrel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_addrel V-table-Win
ON CHOOSE OF btn_addrel IN FRAME F-Main /* Add Related */
DO:
  RUN add-charge( "Related" ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_remove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_remove V-table-Win
ON CHOOSE OF btn_remove IN FRAME F-Main /* Remove */
DO:
  RUN remove-charge.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_accounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON LEAVE OF cmb_accounts IN FRAME F-Main
DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.AccountCode IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON TAB OF cmb_accounts IN FRAME F-Main
OR 'BACK-TAB' OF {&SELF-NAME} DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.AccountCode IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON U2 OF cmb_accounts IN FRAME F-Main
DO:
  account-code = DEC( SUBSTR( INPUT {&SELF-NAME}, 1, 7 ) ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON VALUE-CHANGED OF cmb_accounts IN FRAME F-Main
DO:
  APPLY 'U2' TO SELF.
  IF AVAILABLE Charge THEN DO:
    Charge.AccountCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( account-code ).
    RUN account-changed.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_chargetype
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_chargetype V-table-Win
ON LEAVE OF cmb_chargetype IN FRAME F-Main
DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.RentChargeType IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_chargetype V-table-Win
ON TAB OF cmb_chargetype IN FRAME F-Main
OR 'BACK-TAB' OF {&SELF-NAME} DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.RentChargeType IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_chargetype V-table-Win
ON U1 OF cmb_chargetype IN FRAME F-Main
DO:
  {inc/selcmb/scrcht1.i "?" "charge-type"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_chargetype V-table-Win
ON U2 OF cmb_chargetype IN FRAME F-Main
DO:
  {inc/selcmb/scrcht2.i "?" "charge-type"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_chargetype V-table-Win
ON VALUE-CHANGED OF cmb_chargetype IN FRAME F-Main
DO:
  APPLY 'U2' TO SELF.
  IF AVAILABLE Charge THEN DO:
    Charge.RentChargeType:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = charge-type.
    RUN chargetype-changed.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_freq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON LEAVE OF cmb_freq IN FRAME F-Main
DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.FrequencyCode IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON TAB OF cmb_freq IN FRAME F-Main
OR 'BACK-TAB' OF {&SELF-NAME} DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.FrequencyCode IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON U1 OF cmb_freq IN FRAME F-Main
DO:
  {inc/selcmb/scfty1.i "?" "frequency-code"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON U2 OF cmb_freq IN FRAME F-Main
DO:
  {inc/selcmb/scfty2.i "?" "frequency-code"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON VALUE-CHANGED OF cmb_freq IN FRAME F-Main
DO:
  APPLY 'U2' TO SELF.
  IF AVAILABLE Charge THEN DO:

    IF Charge.FrequencyCode <> frequency-code THEN DO:
      /* Update the frequency amount */
      Charge.Amount:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} =
        STRING( annual-to-freq( frequency-code,
            (INPUT BROWSE {&BROWSE-NAME} Charge.Annual) ) ).
    END.

    Charge.FrequencyCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = frequency-code.
    RUN assign-charge.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* Bring up the charge type combo */
ON ENTRY OF Charge.RentChargeType IN BROWSE {&BROWSE-NAME} DO:
  cmb_chargetype:X IN FRAME {&FRAME-NAME} = SELF:X.
  cmb_chargetype:Y IN FRAME {&FRAME-NAME} = SELF:Y + {&BROWSE-NAME}:Y.
  cmb_chargetype:HIDDEN IN FRAME {&FRAME-NAME} = No.
  cmb_chargetype:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  charge-type = INPUT BROWSE {&BROWSE-NAME} Charge.RentChargeType.
  APPLY 'U1':U TO cmb_chargetype.
  IF cmb_chargetype:MOVE-TO-TOP() THEN.
  APPLY 'ENTRY':U TO cmb_chargetype.
  RETURN NO-APPLY.
END.

/* Bring up the frequency type combo */
ON ENTRY OF Charge.FrequencyCode IN BROWSE {&BROWSE-NAME} DO:
  cmb_freq:X IN FRAME {&FRAME-NAME} = SELF:X.
  cmb_freq:Y IN FRAME {&FRAME-NAME} = SELF:Y + {&BROWSE-NAME}:Y.
  cmb_freq:HIDDEN IN FRAME {&FRAME-NAME} = No.
  cmb_freq:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  frequency-code = INPUT BROWSE {&BROWSE-NAME} Charge.FrequencyCode.
  APPLY 'U1':U TO cmb_freq.
  IF cmb_freq:MOVE-TO-TOP() THEN.
  APPLY 'ENTRY':U TO cmb_freq.
  RETURN NO-APPLY.
END.

ON LEAVE OF Charge.AccountCode IN BROWSE {&BROWSE-NAME}
DO:
  RUN account-changed.
END.

/* Bring up the account code combo */
ON ENTRY OF Charge.AccountCode IN BROWSE {&BROWSE-NAME} DO:
  IF NOT got-accounts THEN RUN get-accounts.
  got-accounts = Yes.
  cmb_accounts:X IN FRAME {&FRAME-NAME} = SELF:X + {&BROWSE-NAME}:X.
  cmb_accounts:Y IN FRAME {&FRAME-NAME} = SELF:Y + {&BROWSE-NAME}:Y.
  cmb_accounts:HIDDEN IN FRAME {&FRAME-NAME} = No.
  cmb_accounts:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  account-code = INPUT BROWSE {&BROWSE-NAME} Charge.AccountCode.
  APPLY 'U1':U TO cmb_accounts.
  IF cmb_accounts:MOVE-TO-TOP() THEN.
  APPLY 'ENTRY':U TO cmb_accounts.
  RETURN NO-APPLY.
END.


/* Make sure they can't blank the start date */
ON ANY-PRINTABLE OF Charge.StartDate IN BROWSE {&BROWSE-NAME} DO:
  IF LAST-EVENT:LABEL = "?" THEN DO:
    BELL.
    RETURN NO-APPLY.
  END.
END.

/* Ensure that the end date is always at least the start date */
&SCOPED-DEFINE SELF-NAME BROWSE {&BROWSE-NAME} Charge.EndDate
ON LEAVE OF Charge.EndDate IN BROWSE {&BROWSE-NAME} DO:
  IF INPUT {&SELF-NAME} <> ? AND
     INPUT {&SELF-NAME} < INPUT Charge.StartDate THEN DO:
    BELL.
    SELF:SCREEN-VALUE = ?.
    RUN assign-charge.
  END.

END.

&SCOPED-DEFINE SELF-NAME BROWSE {&BROWSE-NAME} Charge.StartDate
ON LEAVE OF Charge.StartDate IN BROWSE {&BROWSE-NAME} DO:
  IF INPUT BROWSE {&BROWSE-NAME} Charge.EndDate <> ? AND
     INPUT Charge.EndDate < INPUT {&SELF-NAME} THEN DO:
    BELL.
    SELF:SCREEN-VALUE = STRING( Charge.StartDate ).
  END.
  RUN assign-charge.
END.

/* Make sue the description field format is set */
ON ENTRY OF Charge.Description IN BROWSE {&BROWSE-NAME} DO:
  Charge.Description:FORMAT IN BROWSE {&BROWSE-NAME} = "X(50)".
END.


/* Triggers to update the annual and frequency amounts */

ON LEAVE OF Charge.Annual IN BROWSE {&BROWSE-NAME} DO:
  IF NOT AVAILABLE Charge OR
    INPUT BROWSE {&BROWSE-NAME} Charge.Annual =
      Charge.Annual THEN RETURN.
  
  Charge.Amount:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} =
    STRING( annual-to-freq( (INPUT Charge.FrequencyCode), (INPUT Charge.Annual) ) ).
  RUN assign-charge.
END.

ON LEAVE OF Charge.Amount IN BROWSE {&BROWSE-NAME} DO:

  IF NOT AVAILABLE Charge OR
    INPUT BROWSE {&BROWSE-NAME} Charge.Amount = Charge.Amount
  THEN RETURN.

  Charge.Annual:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} =
    STRING( freq-to-annual( (INPUT Charge.FrequencyCode), (INPUT Charge.Amount ) ) ).
  RUN assign-charge.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE account-changed V-table-Win 
PROCEDURE account-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  fil_Account = "".
  FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = INPUT BROWSE {&BROWSE-NAME}
    Charge.AccountCode NO-LOCK NO-ERROR.
  fil_Account = IF AVAILABLE ChartOfAccount THEN ChartOfAccount.Name ELSE "".
  DISPLAY fil_account WITH FRAME {&FRAME-NAME}.
  RUN assign-charge.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-charge V-table-Win 
PROCEDURE add-charge :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
DO WITH FRAME {&FRAME-NAME}:

  DEF INPUT PARAMETER type AS CHAR NO-UNDO.

  DEF BUFFER NewCharge FOR Charge.  
  DEF VAR reposition-row   AS INT   NO-UNDO.
  DEF VAR reposition-rowid AS ROWID NO-UNDO.
  
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN
    RUN open-charge-query.
  IF AVAILABLE Charge THEN RUN assign-charge.
  
  CREATE NewCharge.

  IF type = "Related" AND AVAILABLE Charge THEN
    BUFFER-COPY Charge TO NewCharge.
  ELSE
    ASSIGN
      Newcharge.RentChargeType = "RENT"
      NewCharge.FrequencyCode = null-str( get-result( 'Rent-Frequency' ), "MNTH"  ).

  ASSIGN
    NewCharge.Type      = type
    NewCharge.StartDate = DATE( get-result( 'Rent-Start' ) )
    NewCharge.StartDate = IF NewCharge.StartDate = ? THEN TODAY ELSE NewCharge.StartDate
    NewCharge.EndDate   = DATE( get-result( 'Rent-End' ) ).
  
  charge-type      = NewCharge.RentChargeType.
  reposition-rowid = ROWID( NewCharge ).
  
  reposition-row = {&BROWSE-NAME}:GET-REPOSITIONED-ROW().
  reposition-row =  INT( {&BROWSE-NAME}:HEIGHT-CHARS ) - 1.
  OPEN QUERY {&BROWSE-NAME} FOR EACH Charge.
  IF {&BROWSE-NAME}:SET-REPOSITIONED-ROW( reposition-row, "CONDITIONAL" ) THEN.
  REPOSITION {&BROWSE-NAME} TO ROWID reposition-rowid.

  RUN display-charge.
  RUN chargetype-changed.
  RUN assign-charge.
  test-validated().
  APPLY 'ENTRY':U TO Charge.StartDate IN BROWSE {&BROWSE-NAME}.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-charge V-table-Win 
PROCEDURE assign-charge :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE Charge THEN
    ASSIGN BROWSE {&BROWSE-NAME}
      Charge.Type
      Charge.SequenceCode
      Charge.StartDate
      Charge.EndDate
      Charge.RentChargeType
      Charge.FrequencyCode
      Charge.Amount
      Charge.Annual
      Charge.AccountCode
      Charge.Description.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE chargetype-changed V-table-Win 
PROCEDURE chargetype-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  FIND FIRST RentChargeType WHERE RentChargeType.RentChargeType = charge-type
    NO-LOCK NO-ERROR.
  IF AVAILABLE RentChargeType THEN DO:
    ASSIGN Charge.AccountCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME}
                                           = STRING( RentChargeType.AccountCode ).
           Charge.Description:SCREEN-VALUE = RentChargeType.Description.
    RUN account-changed.
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-charge-query V-table-Win 
PROCEDURE close-charge-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY {&BROWSE-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-charge V-table-Win 
PROCEDURE display-charge :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE Charge THEN DO:
    DISPLAY Charge WITH BROWSE {&BROWSE-NAME}.
    charge-type    = Charge.RentChargeType.
    account-code   = Charge.AccountCode.
    frequency-code = Charge.FrequencyCode.
  END.
  RUN account-changed.
  RUN set-line-color.
  RUN enable-charge.
  RUN sensitise-buttons.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  /* Re-read the charge list if the lease changes */
  DEF VAR current-lease LIKE TenancyLease.Tenancyleasecode NO-UNDO.
  current-lease = null-int( INT( get-result( 'Current-Lease' ) ), 0 ).
  IF current-lease <> last-lease THEN DO:
    FOR EACH Charge: DELETE Charge. END.
    RUN get-current-charges.
  END.
  
  RUN display-select-combos.
  RUN open-charge-query.
  test-validated().

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-charge V-table-Win 
PROCEDURE enable-charge :
/*------------------------------------------------------------------------------
  Purpose:     Enable fields on the charged based on whether it is a
               current charge, related charge or new charge
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  IF NOT AVAILABLE Charge THEN RETURN.
  type = INPUT BROWSE {&BROWSE-NAME} Charge.Type.

  ASSIGN
    Charge.StartDate:READ-ONLY IN BROWSE {&BROWSE-NAME}
                                    = type = "Current"
    Charge.EndDate:READ-ONLY        = No
    Charge.RentChargeType:READ-ONLY = LOOKUP( type, "Current,Related" ) <> 0
    Charge.FrequencyCode:READ-ONLY  = type = "Current"
    Charge.Amount:READ-ONLY         = type = "Current"
    Charge.Annual:READ-ONLY         = type = "Current"
    Charge.AccountCode:READ-ONLY    = LOOKUP( type, "Current,Related" ) <> 0
    Charge.Description:READ-ONLY    = LOOKUP( type, "Current,Related" ) <> 0.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-accounts V-table-Win 
PROCEDURE get-accounts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Populate the account combo */

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  FOR EACH ChartOfAccount NO-LOCK:
    IF cmb_accounts:ADD-LAST( STRING( ChartOfAccount.AccountCode, "9999.99" ) + 
      " " + ChartOfAccount.Name ) IN FRAME {&FRAME-NAME} THEN.
  END.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-current-charges V-table-Win 
PROCEDURE get-current-charges :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR lease-code LIKE RentCharge.TenancyLeaseCode NO-UNDO.
  DEF BUFFER LastChargeEntry FOR RentChargeLine.
  
  lease-code = INT( get-result( 'Current-Lease' ) ).
  
  FOR EACH RentCharge WHERE RentCharge.TenancyLeaseCode = lease-code NO-LOCK:
    /* Get the last existing chargeable line and create a Charge record from it */

    FIND FIRST LastChargeEntry
    WHERE LastChargeEntry.TenancyLeaseCode = RentCharge.TenancyLeaseCode
      AND LastChargeEntry.SequenceCode     = RentCharge.SequenceCode
      AND LastChargeEntry.RentChargeLineStatus = "C"
      AND LastChargeEntry.EndDate = ?
    NO-LOCK NO-ERROR.
  
    IF AVAILABLE LastChargeEntry THEN DO:
      CREATE Charge.
      BUFFER-COPY LastChargeEntry TO Charge.
      BUFFER-COPY RentCharge TO Charge.
      ASSIGN
        Charge.Type = "Current"
        Charge.Annual = freq-to-annual( Charge.FrequencyCode, Charge.Amount ).
    END.
    
  END.
  
  last-lease = lease-code.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-charge-query V-table-Win 
PROCEDURE open-charge-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  OPEN QUERY {&BROWSE-NAME} FOR EACH Charge.
  RUN display-charge.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-charge V-table-Win 
PROCEDURE remove-charge :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN RETURN.

  IF {&BROWSE-NAME}:SELECT-FOCUSED-ROW() AND
     {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN
  DO:
    GET CURRENT {&BROWSE-NAME}.
    DELETE Charge.
    IF {&BROWSE-NAME}:DELETE-CURRENT-ROW() THEN.
  END.

  test-validated().
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = ? OR 
     NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 THEN
  DO:
    RUN close-charge-query.
    RUN account-changed.
    RUN sensitise-buttons.
    RETURN.
  END.

  IF {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN RUN display-charge.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Charge"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-buttons V-table-Win 
PROCEDURE sensitise-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  DO WITH FRAME {&FRAME-NAME}:
    DEF BUFFER OtherCharge FOR Charge.
    DEF VAR type AS CHAR NO-UNDO.
    IF AVAILABLE Charge THEN type = INPUT BROWSE {&BROWSE-NAME} Charge.Type.
    btn_remove:SENSITIVE = AVAILABLE Charge AND type <> "Current".
    btn_addrel:SENSITIVE = AVAILABLE Charge AND type = "Current"
      AND NOT CAN-FIND( FIRST OtherCharge
        WHERE OtherCharge.SequenceCode = Charge.SequenceCode
          AND Othercharge.Type = "Related"
          AND ROWID( OtherCharge ) <> ROWID( Charge ) ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-line-color V-table-Win 
PROCEDURE set-line-color :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE Charge THEN RETURN.
  
  DEF VAR fgcolor AS INT NO-UNDO.
  fgcolor =
    IF Charge.Type = "Current" THEN 1 ELSE
    IF Charge.Type = "Related" THEN 9 ELSE
    IF Charge.Type = "New    " THEN 2 ELSE ?.
    
  ASSIGN
    Charge.Type:FGCOLOR IN BROWSE {&BROWSE-NAME} = fgcolor
    Charge.SequenceCode:FGCOLOR = fgcolor
    Charge.StartDate:FGCOLOR = fgcolor
    Charge.EndDate:FGCOLOR = fgcolor
    Charge.RentChargeType:FGCOLOR = fgcolor
    Charge.FrequencyCode:FGCOLOR = fgcolor
    Charge.Amount:FGCOLOR = fgcolor
    Charge.Annual:FGCOLOR = fgcolor
    Charge.AccountCode:FGCOLOR = fgcolor
    Charge.Description:FGCOLOR = fgcolor.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF VAR charge-result AS CHAR NO-UNDO.
  DEF VAR charge-line   AS CHAR NO-UNDO.
  DEF VAR fd AS CHAR NO-UNDO. /* Field delimiter */
  DEF VAR ld AS CHAR NO-UNDO. /* Line delimiter */
  fd = '~~'. ld = '~n'.
  RUN assign-charge.
  
  FOR EACH Charge:
    charge-line =
      null-str( Charge.Type, "" ) + fd +
      null-str( STRING( Charge.SequenceCode ), "0" ) + fd +
      null-str( STRING( Charge.StartDate ), "?" ) + fd +
      null-str( STRING( Charge.EndDate ), "?" )   + fd +
      null-str( Charge.RentChargeType, "" ) + fd + 
      null-str( Charge.FrequencyCode, "" ) + fd +
      null-str( STRING( Charge.Amount ), "0" ) + fd + 
      null-str( STRING( Charge.Annual ), "0" ) + fd +
      null-str( STRING( Charge.AccountCode ), "?" ) + fd +
      null-str( Charge.Description, "" ).
    charge-result = charge-result + IF charge-result = "" THEN "" ELSE ld.
    charge-result = charge-result + charge-line.
  END.

  set-result( 'Rental-Charges', charge-result ).
  RETURN test-validated().
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION annual-to-freq V-table-Win 
FUNCTION annual-to-freq RETURNS DECIMAL
  ( INPUT freq AS CHAR, INPUT annual AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  DEF VAR n-days   AS INT NO-UNDO.
  DEF VAR n-months AS INT NO-UNDO.
  DEF VAR amount   AS DEC NO-UNDO INIT 0.00.
  
  n-months = get-freq-months( freq ).
  n-days   = get-freq-days( freq ).
  IF n-months <> ? THEN DO:
    amount = DEC( n-months ) / 12 * annual.
  END.
  ELSE IF n-days <> ? THEN DO:
    amount = DEC( n-days ) / 365 * annual.
  END.
  
  RETURN amount.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION freq-to-annual V-table-Win 
FUNCTION freq-to-annual RETURNS DECIMAL
  ( INPUT freq AS CHAR, INPUT amount AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  DEF VAR n-days   AS INT NO-UNDO.
  DEF VAR n-months AS INT NO-UNDO.
  DEF VAR annual   AS DEC NO-UNDO INIT 0.00.
  
  n-months = get-freq-months( freq ).
  n-days   = get-freq-days( freq ).
  IF n-months <> ? THEN DO:
    annual = 12 / DEC( n-months ) * amount.
  END.
  ELSE IF n-days <> ? THEN DO:
    annual = 365 / DEC( n-days ) * amount.
  END.
  
  RETURN annual.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION include-lease V-table-Win 
FUNCTION include-lease RETURNS LOGICAL
  ( INPUT lease-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  Dummy function.
------------------------------------------------------------------------------*/

  RETURN Yes.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NOT CAN-FIND( FIRST Charge ) THEN DO:
    set-validated( No ).
    RETURN "FAIL".
  END.
  
  set-validated( Yes ).
  RETURN "".   /* Function return value. */

END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

