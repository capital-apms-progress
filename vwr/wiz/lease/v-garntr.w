&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:
  Description: 

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF WORK-TABLE tmp_Guarantor NO-UNDO LIKE Guarantor
  FIELD Name    AS CHAR FORMAT "X(35)"
  FIELD Company AS CHAR FORMAT "X(35)"
  FIELD PostalType  AS CHAR
  FIELD Address AS CHAR
  FIELD City    AS CHAR FORMAT "X(18)"
  FIELD State   AS CHAR
  FIELD Zip     AS CHAR
  FIELD Country AS CHAR
  FIELD G-Type  AS CHAR LABEL "Type" FORMAT "   X(2)"
  FIELD G-Value AS DEC  LABEL "Value" FORMAT ">>>,>>>,>>9"
  FIELD Expiry  AS DATE LABEL "     Expiry     " FORMAT "99/99/9999".

DEF VAR person-code LIKE Person.PersonCode NO-UNDO INIT ?.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-guarantor

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tmp_Guarantor

/* Definitions for BROWSE br-guarantor                                  */
&Scoped-define FIELDS-IN-QUERY-br-guarantor Name City G-Type G-Value Expiry   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-guarantor   
&Scoped-define FIELD-PAIRS-IN-QUERY-br-guarantor
&Scoped-define SELF-NAME br-guarantor
&Scoped-define OPEN-QUERY-br-guarantor OPEN QUERY {&SELF-NAME} FOR EACH tmp_Guarantor.
&Scoped-define TABLES-IN-QUERY-br-guarantor tmp_Guarantor
&Scoped-define FIRST-TABLE-IN-QUERY-br-guarantor tmp_Guarantor


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-guarantor}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-22 btn_add btn_remove br-guarantor ~
cmb_GuarantorType fil_Name fil_Organisation fil_Address fil_City fil_State ~
fil_Zip fil_Country fil_Value fil_Expiry 
&Scoped-Define DISPLAYED-OBJECTS cmb_GuarantorType fil_Name ~
fil_Organisation fil_Address fil_City fil_State fil_Zip fil_Country ~
fil_Value fil_Expiry 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 8 BY .9.

DEFINE BUTTON btn_remove 
     LABEL "&Remove" 
     SIZE 8 BY .9.

DEFINE VARIABLE cmb_GuarantorType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Bank Guarantee","Individual","Company","Security Deposit" 
     SIZE 16.57 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_Address AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 26.29 BY 3.2 NO-UNDO.

DEFINE VARIABLE fil_City AS CHARACTER FORMAT "X(256)":U 
     LABEL "City" 
     VIEW-AS FILL-IN 
     SIZE 14.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Country AS CHARACTER FORMAT "X(256)":U 
     LABEL "Country" 
     VIEW-AS FILL-IN 
     SIZE 12.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Expiry AS DATE FORMAT "99/99/9999":U 
     LABEL "Expires" 
     VIEW-AS FILL-IN 
     SIZE 10.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Name AS CHARACTER FORMAT "X(256)":U 
     LABEL "Person" 
     VIEW-AS FILL-IN 
     SIZE 32.57 BY 1 TOOLTIP "Enter the name of the guarantor, or the name of a contact person" NO-UNDO.

DEFINE VARIABLE fil_Organisation AS CHARACTER FORMAT "X(256)":U 
     LABEL "Organisation" 
     VIEW-AS FILL-IN 
     SIZE 32.57 BY 1 NO-UNDO.

DEFINE VARIABLE fil_State AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 6.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Value AS DECIMAL FORMAT ">>,>>>,>>9":U INITIAL 0 
     LABEL "Value" 
     VIEW-AS FILL-IN 
     SIZE 9.72 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Zip AS CHARACTER FORMAT "X(256)":U 
     LABEL "Zip" 
     VIEW-AS FILL-IN 
     SIZE 7.43 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 65.72 BY 12.8.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-guarantor FOR 
      tmp_Guarantor SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-guarantor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-guarantor V-table-Win _FREEFORM
  QUERY br-guarantor DISPLAY
      Name
  City
  G-Type
  G-Value
  Expiry
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 64.57 BY 5
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     btn_add AT ROW 2.5 COL 49.57
     btn_remove AT ROW 2.5 COL 58.14
     br-guarantor AT ROW 3.4 COL 1.57
     cmb_GuarantorType AT ROW 8.4 COL 3.57 COLON-ALIGNED
     fil_Name AT ROW 8.4 COL 31.57 COLON-ALIGNED
     fil_Organisation AT ROW 9.4 COL 31.57 COLON-ALIGNED
     fil_Address AT ROW 10.4 COL 7.29 NO-LABEL
     fil_City AT ROW 10.6 COL 37.86 COLON-ALIGNED
     fil_State AT ROW 10.6 COL 57.29 COLON-ALIGNED
     fil_Zip AT ROW 11.6 COL 37.86 COLON-ALIGNED
     fil_Country AT ROW 11.6 COL 51.57 COLON-ALIGNED
     fil_Value AT ROW 12.6 COL 37.86 COLON-ALIGNED
     fil_Expiry AT ROW 12.6 COL 53.86 COLON-ALIGNED
     "Are there any guarantors for this lease ?" VIEW-AS TEXT
          SIZE 54.86 BY 1.2 AT ROW 1.2 COL 1.57
          FGCOLOR 1 FONT 12
     "If there are guarantors then enter them below, otherwise click Next" VIEW-AS TEXT
          SIZE 46.29 BY .7 AT ROW 2.4 COL 2.14
          FGCOLOR 1 FONT 10
     RECT-22 AT ROW 1 COL 1
     "Address:" VIEW-AS TEXT
          SIZE 5.72 BY .65 AT ROW 10.4 COL 1.57
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.5
         WIDTH              = 67.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-guarantor btn_remove F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       fil_Address:RETURN-INSERTED IN FRAME F-Main  = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-guarantor
/* Query rebuild information for BROWSE br-guarantor
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tmp_Guarantor.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-guarantor */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/method/m-mntvwr.i}
{inc/null.i}
{inc/persndtl.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br-guarantor
&Scoped-define SELF-NAME br-guarantor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-guarantor V-table-Win
ON VALUE-CHANGED OF br-guarantor IN FRAME F-Main
DO:
  RUN display-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN add-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_remove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_remove V-table-Win
ON CHOOSE OF btn_remove IN FRAME F-Main /* Remove */
DO:
  RUN remove-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_GuarantorType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_GuarantorType V-table-Win
ON VALUE-CHANGED OF cmb_GuarantorType IN FRAME F-Main /* Type */
DO:
  IF SELF:MODIFIED THEN RUN assign-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Address
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Address V-table-Win
ON LEAVE OF fil_Address IN FRAME F-Main
DO:
  IF SELF:MODIFIED THEN RUN assign-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_City
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_City V-table-Win
ON LEAVE OF fil_City IN FRAME F-Main /* City */
DO:
  IF SELF:MODIFIED THEN RUN assign-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Country
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Country V-table-Win
ON LEAVE OF fil_Country IN FRAME F-Main /* Country */
DO:
  IF SELF:MODIFIED THEN RUN assign-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Expiry
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Expiry V-table-Win
ON LEAVE OF fil_Expiry IN FRAME F-Main /* Expires */
DO:
  IF SELF:MODIFIED THEN RUN assign-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Name
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Name V-table-Win
ON LEAVE OF fil_Name IN FRAME F-Main /* Person */
DO:
  IF SELF:MODIFIED THEN RUN assign-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Name V-table-Win
ON U1 OF fil_Name IN FRAME F-Main /* Person */
DO:
  {inc/selfil/sfpsn1.i "?" "person-code"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Name V-table-Win
ON U2 OF fil_Name IN FRAME F-Main /* Person */
DO:
  {inc/selfil/sfpsn2.i "?" "person-code"}
  RUN contact-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Name V-table-Win
ON U3 OF fil_Name IN FRAME F-Main /* Person */
DO:
  {inc/selfil/sfpsn3.i "?" "person-code"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Organisation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Organisation V-table-Win
ON LEAVE OF fil_Organisation IN FRAME F-Main /* Organisation */
DO:
  IF SELF:MODIFIED THEN RUN assign-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_State
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_State V-table-Win
ON LEAVE OF fil_State IN FRAME F-Main /* State */
DO:
  IF SELF:MODIFIED THEN RUN assign-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Value
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Value V-table-Win
ON LEAVE OF fil_Value IN FRAME F-Main /* Value */
DO:
  IF SELF:MODIFIED THEN RUN assign-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Zip
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Zip V-table-Win
ON LEAVE OF fil_Zip IN FRAME F-Main /* Zip */
DO:
  IF SELF:MODIFIED THEN RUN assign-guarantor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-guarantor V-table-Win 
PROCEDURE add-guarantor :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR reposition-row   AS INT NO-UNDO.
  DEF VAR reposition-rowid AS ROWID NO-UNDO.
  
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN
    RUN open-guarantor-query.
    
  IF AVAILABLE(tmp_Guarantor) THEN RUN assign-guarantor.
  
  CREATE tmp_Guarantor.
  tmp_Guarantor.PersonCode = ?.
  tmp_Guarantor.PostalType = "POST".
  reposition-rowid = ROWID( tmp_Guarantor ).
  reposition-row =  INT( {&BROWSE-NAME}:HEIGHT-CHARS ) - 1.
  OPEN QUERY {&BROWSE-NAME} FOR EACH tmp_Guarantor.
  IF {&BROWSE-NAME}:SET-REPOSITIONED-ROW( reposition-row, "CONDITIONAL" ) THEN.
  REPOSITION {&BROWSE-NAME} TO ROWID reposition-rowid.

  RUN display-guarantor.
  APPLY 'ENTRY':U TO tmp_Guarantor.Name IN BROWSE {&BROWSE-NAME}.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-guarantor V-table-Win 
PROCEDURE assign-guarantor :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF AVAILABLE(tmp_Guarantor) THEN DO:
    ASSIGN cmb_GuarantorType fil_Organisation fil_Address fil_City fil_Country 
           fil_Expiry fil_Name fil_State fil_Value fil_Zip .
    tmp_Guarantor.Name = fil_Name.
    tmp_Guarantor.Company = fil_Organisation.
    tmp_Guarantor.Address = fil_Address.
    tmp_Guarantor.City = fil_City.
    tmp_Guarantor.State = fil_State.
    tmp_Guarantor.Zip = fil_Zip.
    tmp_Guarantor.Country = fil_Country.
    tmp_Guarantor.G-Value = fil_Value.
    tmp_Guarantor.G-Type = SUBSTRING( cmb_GuarantorType, 1, 1).
    tmp_Guarantor.Expiry = fil_Expiry.
    RUN display-guarantor.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-guarantor-query V-table-Win 
PROCEDURE close-guarantor-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY {&BROWSE-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE contact-changed V-table-Win 
PROCEDURE contact-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  APPLY 'U3' TO fil_Name.
  FIND Person WHERE Person.PersonCode = person-code NO-LOCK NO-ERROR.
  IF AVAILABLE Person AND AVAILABLE(tmp_Guarantor) THEN DO:
    RUN combine-fullname( Person.Firstname, Person.LastName,
      OUTPUT tmp_Guarantor.Name ).
    FIND PostalDetail OF Person WHERE PostalDetail.PostalType = "POST" NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(PostalDetail) THEN
      FIND PostalDetail OF Person WHERE PostalDetail.PostalType = "POST" NO-LOCK NO-ERROR.

    IF AVAILABLE(PostalDetail) THEN DO:
      tmp_Guarantor.PostalType = PostalDetail.PostalType .
      fil_Address = PostalDetail.Address.
      fil_City    = PostalDetail.City.
      fil_State   = PostalDetail.State.
      fil_Zip     = PostalDetail.Zip.
      fil_Country = PostalDetail.Country.
      DISPLAY fil_Address fil_City fil_Country fil_Name fil_State fil_Zip.
    END.

    tmp_Guarantor.PersonCode = person-code.
    tmp_Guarantor.Company = Person.Company.
    RUN display-guarantor.
  END.
    
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-guarantor V-table-Win 
PROCEDURE display-guarantor :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE(tmp_Guarantor) THEN DO:
    DISPLAY tmp_Guarantor.Name 
            tmp_Guarantor.City
            tmp_Guarantor.G-Type
            tmp_Guarantor.G-Value
            tmp_Guarantor.Expiry
            WITH BROWSE {&BROWSE-NAME}.

    fil_Name = tmp_Guarantor.Name.
    fil_Organisation = tmp_Guarantor.Company.
    fil_Address = tmp_Guarantor.Address.
    fil_City = tmp_Guarantor.City.
    fil_State = tmp_Guarantor.State.
    fil_Zip = tmp_Guarantor.Zip.
    fil_Country = tmp_Guarantor.Country.
    fil_Value = tmp_Guarantor.G-Value.
    fil_Expiry = tmp_Guarantor.Expiry.
    cmb_GuarantorType = (IF tmp_Guarantor.G-Type = "D" THEN "Deposit" ELSE
                            (IF tmp_Guarantor.G-Type = "C" THEN "Company" ELSE "Individual")).
    DISPLAY cmb_GuarantorType fil_Organisation fil_Address fil_City fil_Country 
            fil_Expiry fil_Name fil_State fil_Value fil_Zip
            WITH FRAME {&FRAME-NAME} .
  END.
  RUN enable-guarantor.
  RUN sensitise-buttons.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  /* There is no real need to pick up values from the results
     as they will not be changed by other viewers */
  RUN open-guarantor-query.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-guarantor V-table-Win 
PROCEDURE enable-guarantor :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF AVAILABLE(tmp_Guarantor) THEN DO:
    ENABLE  btn_remove cmb_GuarantorType fil_Address fil_City fil_Country 
            fil_Expiry fil_Name fil_Organisation fil_State fil_Value fil_Zip.
  END.
  ELSE DO:
    DISABLE btn_remove cmb_GuarantorType fil_Address fil_City fil_Country 
            fil_Expiry fil_Name fil_Organisation fil_State fil_Value fil_Zip.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-guarantor-query V-table-Win 
PROCEDURE open-guarantor-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  OPEN QUERY {&BROWSE-NAME} FOR EACH tmp_Guarantor.
  RUN display-guarantor.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-guarantor V-table-Win 
PROCEDURE remove-guarantor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN RETURN.

  IF {&BROWSE-NAME}:SELECT-FOCUSED-ROW() AND
     {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN
  DO:
    GET CURRENT {&BROWSE-NAME}.
    DELETE tmp_Guarantor.
    IF {&BROWSE-NAME}:DELETE-CURRENT-ROW() THEN.
  END.

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = ? OR 
     NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 THEN
  DO:
    RUN close-guarantor-query.
    RUN sensitise-buttons.
    RETURN.
  END.

  IF {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN RUN display-guarantor.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "tmp_Guarantor"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-buttons V-table-Win 
PROCEDURE sensitise-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  
  DO WITH FRAME {&FRAME-NAME}:
    btn_remove:SENSITIVE  = AVAILABLE tmp_Guarantor.
    RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Name:HANDLE ),
      "SENSITIVE = " +
        IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR
           NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN "No" ELSE "Yes"
    ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DEF VAR guarantor-result AS CHAR NO-UNDO.
DEF VAR guarantor-line   AS CHAR NO-UNDO.
DEF VAR fd AS CHAR NO-UNDO INITIAL '~~'. /* Field delimiter */
DEF VAR ld AS CHAR NO-UNDO INITIAL '~n'. /* Line delimiter */

DO WITH FRAME {&FRAME-NAME}:
  RUN assign-guarantor.
  
  FOR EACH tmp_Guarantor:
    guarantor-line = null-str( STRING( tmp_Guarantor.PersonCode ), "" ) + fd
                   + null-str( tmp_Guarantor.Name, "" ) + fd 
                   + null-str( tmp_Guarantor.PostalType, "" ) + fd
                   + null-str( REPLACE( tmp_Guarantor.Address, '~n', '|' ), "" ) + fd
                   + null-str( tmp_Guarantor.City, "" ) + fd
                   + null-str( tmp_Guarantor.State, "" ) + fd
                   + null-str( tmp_Guarantor.Zip, "" ) + fd
                   + null-str( tmp_Guarantor.Country, "" ) + fd
                   + null-str( STRING( tmp_Guarantor.G-Value ), "" ) + fd
                   + null-str( STRING( tmp_Guarantor.Expiry, "99/99/9999" ), "" ) + fd
                   + null-str( tmp_Guarantor.G-Type, "" ) + fd
                   + null-str( tmp_Guarantor.Company, "" ) + fd 
                   .
    guarantor-result = guarantor-result + IF guarantor-result = "" THEN "" ELSE ld.
    guarantor-result = guarantor-result + guarantor-line.
  END.

  set-result( 'Guarantors', guarantor-result ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


