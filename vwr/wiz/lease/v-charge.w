&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF WORK-TABLE Charge NO-UNDO
  FIELD RentChargeType LIKE RentCharge.RentChargeType LABEL "Type   "
  FIELD StartDate      LIKE RentChargeLine.StartDate  LABEL "Start            "  
  FIELD EndDate        LIKE RentChargeLine.EndDate    LABEL "End               "
  FIELD FrequencyCode  LIKE RentChargeLine.FrequencyCode LABEL "Freq    "
  FIELD Amount         LIKE RentChargeLine.Amount LABEL "Period Amt" FORMAT "->,>>>,>>9.99"
  FIELD Annual         LIKE RentChargeLine.Amount LABEL "Annual Amt" FORMAT "->,>>>,>>9.99"
  FIELD NetAnnual      LIKE RentChargeLine.Amount LABEL "Est. Net P.A." FORMAT "->,>>>,>>9.99"
  FIELD PaidTo         LIKE RentChargeLine.LastChargedDate LABEL "Paid Up To  "
  FIELD AccountCode    LIKE RentCharge.AccountCode LABEL "Account"
  FIELD Description    LIKE RentCharge.Description FORMAT "X(25)".

  
DEF VAR break-1 AS CHAR FORMAT "X" INIT " " LABEL " " NO-UNDO.
DEF VAR charge-type LIKE RentCharge.RentChargeType NO-UNDO.
DEF VAR frequency-code LIKE RentChargeLine.FrequencyCode NO-UNDO.

DEF VAR account-code LIKE ChartOfAccount.AccountCode NO-UNDO.
DEF VAR got-accounts AS LOGI NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set.i "RentCharge-Outgoings" "outgoings-type"}
{inc/ofc-set-l.i "Property-MarketPerUnit" "market-per-unit"}
{inc/ofc-set-l.i "RentalSpace-Estimates" "contract-is-estimate"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-charge

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Charge

/* Definitions for BROWSE br-charge                                     */
&Scoped-define FIELDS-IN-QUERY-br-charge StartDate RentChargeType Annual Amount NetAnnual PaidTo FrequencyCode EndDate AccountCode Description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-charge StartDate ~
RentChargeType ~
Annual ~
Amount ~
NetAnnual ~
PaidTo ~
FrequencyCode ~
EndDate ~
AccountCode ~
Description   
&Scoped-define SELF-NAME br-charge
&Scoped-define OPEN-QUERY-br-charge OPEN QUERY {&SELF-NAME} FOR EACH Charge.
&Scoped-define TABLES-IN-QUERY-br-charge Charge
&Scoped-define FIRST-TABLE-IN-QUERY-br-charge Charge


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-charge}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btn_add btn_remove br-charge cmb_chargetype ~
cmb_freq cmb_accounts fil_Account RECT-22 
&Scoped-Define DISPLAYED-OBJECTS cmb_chargetype cmb_freq cmb_accounts ~
fil_Account 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD annual-to-freq V-table-Win 
FUNCTION annual-to-freq RETURNS DECIMAL
  ( INPUT freq AS CHAR, INPUT annual AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD freq-to-annual V-table-Win 
FUNCTION freq-to-annual RETURNS DECIMAL
  ( INPUT freq AS CHAR, INPUT amount AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 10 BY 1.

DEFINE BUTTON btn_remove 
     LABEL "&Remove" 
     SIZE 10 BY 1.

DEFINE VARIABLE cmb_accounts AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEMS "?" 
     DROP-DOWN-LIST
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_chargetype AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "?" 
     DROP-DOWN-LIST
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_freq AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "?" 
     DROP-DOWN-LIST
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Account AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 40 BY 1
     FGCOLOR 12  NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 99.43 BY 12.6.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-charge FOR 
      Charge SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-charge
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-charge V-table-Win _FREEFORM
  QUERY br-charge DISPLAY
      StartDate
      RentChargeType
      Annual
      Amount
      NetAnnual
      PaidTo
      FrequencyCode
      EndDate
      AccountCode
      Description
ENABLE
      StartDate
      RentChargeType
      Annual
      Amount
      NetAnnual
      PaidTo
      FrequencyCode
      EndDate
      AccountCode
      Description
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 98.29 BY 10
         BGCOLOR 16 FGCOLOR 12 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     btn_add AT ROW 2 COL 78.72
     btn_remove AT ROW 2 COL 89.57
     br-charge AT ROW 3.4 COL 1.57
     cmb_chargetype AT ROW 10 COL 19 COLON-ALIGNED NO-LABEL
     cmb_freq AT ROW 10.4 COL 29.29 COLON-ALIGNED NO-LABEL
     cmb_accounts AT ROW 11.6 COL 20.72 COLON-ALIGNED NO-LABEL
     fil_Account AT ROW 2.4 COL 8.14 COLON-ALIGNED NO-LABEL
     RECT-22 AT ROW 1 COL 1
     "Enter the charging details for this lease" VIEW-AS TEXT
          SIZE 52.57 BY 1.2 AT ROW 1.2 COL 2.72
          FGCOLOR 1 FONT 12
     "Account:" VIEW-AS TEXT
          SIZE 6.86 BY 1 AT ROW 2.4 COL 2.14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.6
         WIDTH              = 108.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/method/m-mntvwr.i}
{inc/null.i}
{inc/date.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-charge btn_remove F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       cmb_accounts:HIDDEN IN FRAME F-Main           = TRUE.

ASSIGN 
       cmb_chargetype:HIDDEN IN FRAME F-Main           = TRUE.

ASSIGN 
       cmb_freq:HIDDEN IN FRAME F-Main           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-charge
/* Query rebuild information for BROWSE br-charge
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH Charge.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-charge */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br-charge
&Scoped-define SELF-NAME br-charge
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-charge V-table-Win
ON VALUE-CHANGED OF br-charge IN FRAME F-Main
DO:
  RUN display-charge.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN add-charge.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_remove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_remove V-table-Win
ON CHOOSE OF btn_remove IN FRAME F-Main /* Remove */
DO:
  RUN remove-charge.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_accounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON LEAVE OF cmb_accounts IN FRAME F-Main
DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.AccountCode IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON TAB OF cmb_accounts IN FRAME F-Main
OR 'BACK-TAB' OF {&SELF-NAME} DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.AccountCode IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON U2 OF cmb_accounts IN FRAME F-Main
DO:
  account-code = DEC( SUBSTR( INPUT {&SELF-NAME}, 1, 7 ) ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON VALUE-CHANGED OF cmb_accounts IN FRAME F-Main
DO:
  APPLY 'U2' TO SELF.
  IF AVAILABLE Charge THEN DO:
    Charge.AccountCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( account-code ).
    RUN account-changed.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_chargetype
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_chargetype V-table-Win
ON LEAVE OF cmb_chargetype IN FRAME F-Main
DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.RentChargeType IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_chargetype V-table-Win
ON TAB OF cmb_chargetype IN FRAME F-Main
OR 'BACK-TAB' OF {&SELF-NAME} DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.RentChargeType IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_chargetype V-table-Win
ON U1 OF cmb_chargetype IN FRAME F-Main
DO:
  {inc/selcmb/scrcht1.i "?" "charge-type"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_chargetype V-table-Win
ON U2 OF cmb_chargetype IN FRAME F-Main
DO:
  {inc/selcmb/scrcht2.i "?" "charge-type"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_chargetype V-table-Win
ON VALUE-CHANGED OF cmb_chargetype IN FRAME F-Main
DO:
  APPLY 'U2' TO SELF.
  IF AVAILABLE Charge THEN DO:
    Charge.RentChargeType:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = charge-type.
    RUN chargetype-changed.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_freq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON LEAVE OF cmb_freq IN FRAME F-Main
DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.FrequencyCode IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON TAB OF cmb_freq IN FRAME F-Main
OR 'BACK-TAB' OF {&SELF-NAME} DO:
  APPLY LAST-EVENT:FUNCTION TO Charge.FrequencyCode IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON U1 OF cmb_freq IN FRAME F-Main
DO:
  {inc/selcmb/scfty1.i "?" "frequency-code"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON U2 OF cmb_freq IN FRAME F-Main
DO:
  {inc/selcmb/scfty2.i "?" "frequency-code"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_freq V-table-Win
ON VALUE-CHANGED OF cmb_freq IN FRAME F-Main
DO:
  APPLY 'U2' TO SELF.
  IF AVAILABLE Charge THEN DO:

    IF Charge.FrequencyCode <> frequency-code THEN DO:
      /* Update the frequency amount */
      Charge.Amount:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} =
        STRING( annual-to-freq( frequency-code,
            (INPUT BROWSE {&BROWSE-NAME} Charge.Annual) ) ).
    END.

    Charge.FrequencyCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = frequency-code.
    RUN assign-charge.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* Bring up the charge type combo */
ON ENTRY OF Charge.RentChargeType IN BROWSE {&BROWSE-NAME} DO:
  cmb_chargetype:X IN FRAME {&FRAME-NAME} = SELF:X.
  cmb_chargetype:Y IN FRAME {&FRAME-NAME} = SELF:Y + {&BROWSE-NAME}:Y.
  cmb_chargetype:HIDDEN IN FRAME {&FRAME-NAME} = No.
  cmb_chargetype:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  charge-type = INPUT BROWSE {&BROWSE-NAME} Charge.RentChargeType.
  APPLY 'U1':U TO cmb_chargetype.
  IF cmb_chargetype:MOVE-TO-TOP() THEN.
  APPLY 'ENTRY':U TO cmb_chargetype.
  RETURN NO-APPLY.
END.

/* Bring up the frequency type combo */
ON ENTRY OF Charge.FrequencyCode IN BROWSE {&BROWSE-NAME} DO:
  cmb_freq:X IN FRAME {&FRAME-NAME} = SELF:X.
  cmb_freq:Y IN FRAME {&FRAME-NAME} = SELF:Y + {&BROWSE-NAME}:Y.
  cmb_freq:HIDDEN IN FRAME {&FRAME-NAME} = No.
  cmb_freq:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  frequency-code = INPUT BROWSE {&BROWSE-NAME} Charge.FrequencyCode.
  APPLY 'U1':U TO cmb_freq.
  IF cmb_freq:MOVE-TO-TOP() THEN.
  APPLY 'ENTRY':U TO cmb_freq.
  RETURN NO-APPLY.
END.

ON LEAVE OF Charge.AccountCode IN BROWSE {&BROWSE-NAME}
DO:
  RUN account-changed.
END.

/* Bring up the account code combo */
ON ENTRY OF Charge.AccountCode IN BROWSE {&BROWSE-NAME} DO:
  IF NOT got-accounts THEN RUN get-accounts.
  got-accounts = Yes.

DO WITH FRAME {&FRAME-NAME}:
  IF (SELF:X + {&BROWSE-NAME}:X + cmb_accounts:WIDTH ) < {&BROWSE-NAME}:WIDTH THEN
    cmb_accounts:X = SELF:X + {&BROWSE-NAME}:X.
  ELSE
    cmb_accounts:X = SELF:X - cmb_accounts:WIDTH.
  cmb_accounts:Y = SELF:Y + {&BROWSE-NAME}:Y.
  cmb_accounts:HIDDEN = No.
  cmb_accounts:SENSITIVE = Yes.
END.

  account-code = INPUT BROWSE {&BROWSE-NAME} Charge.AccountCode.
  APPLY 'U1':U TO cmb_accounts.
  IF cmb_accounts:MOVE-TO-TOP() THEN.
  APPLY 'ENTRY':U TO cmb_accounts.
  RETURN NO-APPLY.
END.


/* Make sure they can't blank the start date */
ON ANY-PRINTABLE OF Charge.StartDate IN BROWSE {&BROWSE-NAME} DO:
  IF LAST-EVENT:LABEL = "?" THEN DO:
    BELL.
    RETURN NO-APPLY.
  END.
END.

/* Ensure that the end date is always at least the start date */
&SCOPED-DEFINE SELF-NAME BROWSE {&BROWSE-NAME} Charge.EndDate
ON LEAVE OF Charge.EndDate IN BROWSE {&BROWSE-NAME} DO:
  IF INPUT {&SELF-NAME} <> ? AND
     INPUT {&SELF-NAME} < INPUT Charge.StartDate THEN DO:
    BELL.
    SELF:SCREEN-VALUE = ?.
    RUN assign-charge.
  END.

END.

&SCOPED-DEFINE SELF-NAME BROWSE {&BROWSE-NAME} Charge.StartDate
ON LEAVE OF Charge.StartDate IN BROWSE {&BROWSE-NAME} DO:
  IF INPUT BROWSE {&BROWSE-NAME} Charge.EndDate <> ? AND
     INPUT Charge.EndDate < INPUT {&SELF-NAME} THEN DO:
    BELL.
    SELF:SCREEN-VALUE = STRING( Charge.StartDate ).
  END.
  RUN assign-charge.
END.

/* Make sue the description field format is set */
ON ENTRY OF Charge.Description IN BROWSE {&BROWSE-NAME} DO:
  Charge.Description:FORMAT IN BROWSE {&BROWSE-NAME} = "X(50)".
END.

/* Triggers to update the annual and frequency amounts */

ON LEAVE OF Charge.Annual IN BROWSE {&BROWSE-NAME} DO:
  IF NOT AVAILABLE Charge OR
    INPUT BROWSE {&BROWSE-NAME} Charge.Annual =
      Charge.Annual THEN RETURN.
  
  Charge.Amount:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} =
    STRING( annual-to-freq( (INPUT Charge.FrequencyCode), (INPUT Charge.Annual) ) ).
  RUN assign-charge.
END.

ON LEAVE OF Charge.Amount IN BROWSE {&BROWSE-NAME} DO:

  IF NOT AVAILABLE Charge OR
    INPUT BROWSE {&BROWSE-NAME} Charge.Amount = Charge.Amount
  THEN RETURN.

  Charge.Annual:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} =
    STRING( freq-to-annual( (INPUT Charge.FrequencyCode), (INPUT Charge.Amount ) ) ).
  RUN assign-charge.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE account-changed V-table-Win 
PROCEDURE account-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  fil_Account = "".
  FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = INPUT BROWSE {&BROWSE-NAME}
    Charge.AccountCode NO-LOCK NO-ERROR.
  fil_Account = IF AVAILABLE ChartOfAccount THEN ChartOfAccount.Name ELSE "".
  DISPLAY fil_account WITH FRAME {&FRAME-NAME}.
  RUN assign-charge.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-charge V-table-Win 
PROCEDURE add-charge :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR reposition-row   AS INT   NO-UNDO.
  DEF VAR reposition-rowid AS ROWID NO-UNDO.
  DEF VAR first-charge     AS LOGI  NO-UNDO.
  
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN DO:
    RUN open-charge-query.
    first-charge = Yes.
  END.
    
  IF AVAILABLE Charge THEN RUN assign-charge.
  
  CREATE Charge.
  ASSIGN 
    Charge.StartDate = DATE( get-result( 'Rent-Start' ) )
    Charge.StartDate = IF Charge.StartDate = ? THEN DATE( get-result( 'Lease-Start' ) ) ELSE Charge.StartDate
    Charge.StartDate = IF Charge.Startdate = ? THEN TODAY ELSE Charge.StartDate
    Charge.EndDate   = DATE( get-result( 'Rent-End' ) )
    Charge.FrequencyCode = null-str( get-result( 'Rent-Frequency' ), "MNTH"  ).
    
  /* If this is the first charge then base its values around a standard
     rental charge */
  IF first-charge THEN DO:
    ASSIGN
      Charge.RentChargeType = ""
      Charge.Annual = null-dec( DEC( get-result( 'Rent-Amount' ) ), 0.00 )
      Charge.Amount = annual-to-freq( Charge.FrequencyCode, Charge.Annual ).
  END.
  
  charge-type      = Charge.RentChargeType.
  reposition-rowid = ROWID( Charge ).
  
  reposition-row = {&BROWSE-NAME}:GET-REPOSITIONED-ROW().
  reposition-row =  INT( {&BROWSE-NAME}:HEIGHT-CHARS ) - 1.
  OPEN QUERY {&BROWSE-NAME} FOR EACH Charge.
  IF {&BROWSE-NAME}:SET-REPOSITIONED-ROW( reposition-row, "CONDITIONAL" ) THEN.
  REPOSITION {&BROWSE-NAME} TO ROWID reposition-rowid.

  RUN display-charge.
  RUN chargetype-changed.
  RUN assign-charge.
  test-validated().
  APPLY 'ENTRY':U TO Charge.StartDate IN BROWSE {&BROWSE-NAME}.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-charge V-table-Win 
PROCEDURE assign-charge :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE Charge THEN
    ASSIGN BROWSE {&BROWSE-NAME}
      Charge.StartDate
      Charge.EndDate
      Charge.RentChargeType
      Charge.FrequencyCode
      Charge.Amount
      Charge.NetAnnual
      Charge.PaidTo
      Charge.AccountCode
      Charge.Description.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE chargetype-changed V-table-Win 
PROCEDURE chargetype-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  FIND FIRST RentChargeType WHERE RentChargeType.RentChargeType = charge-type
    NO-LOCK NO-ERROR.
  IF AVAILABLE RentChargeType THEN DO:
    ASSIGN Charge.AccountCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME}
                                           = STRING( RentChargeType.AccountCode ).
           Charge.Description:SCREEN-VALUE = RentChargeType.Description.
    RUN account-changed.
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-charge-query V-table-Win 
PROCEDURE close-charge-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY {&BROWSE-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-default-charges V-table-Win 
PROCEDURE create-default-charges :
/*------------------------------------------------------------------------------
  Purpose:  Create some default charges first time in
------------------------------------------------------------------------------*/
DEF VAR area-list AS CHAR NO-UNDO.
DEF VAR outgoings AS CHAR NO-UNDO.
DEF VAR property-code AS INT NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.

  property-code = INT( get-result( 'Property' ) ).
  area-list = null-str( get-result( 'Rental-Spaces' ), "" ).
  n = NUM-ENTRIES( area-list ).
  DO i = 1 TO n:
    FIND RentalSpace WHERE RentalSpace.PropertyCode = property-code
                      AND RentalSpace.RentalSpaceCode = INT( ENTRY( i, area-list ) )
                      NO-LOCK.
    FIND FIRST Charge WHERE Charge.RentChargeType = RentalSpace.AreaType NO-ERROR.
    IF NOT AVAILABLE(Charge) THEN DO:
      CREATE Charge.
      Charge.RentChargeType = RentalSpace.AreaType.
      Charge.StartDate = DATE( get-result( 'Rent-Start' ) ).
      Charge.StartDate = IF Charge.StartDate = ? THEN DATE( get-result( 'Lease-Start' ) ) ELSE Charge.StartDate.
      Charge.StartDate = IF Charge.Startdate = ? THEN TODAY ELSE Charge.StartDate.
      Charge.EndDate   = DATE( get-result( 'Rent-End' ) ).
      Charge.FrequencyCode = null-str( get-result( 'Rent-Frequency' ), "MNTH"  ).
    END.
    IF contract-is-estimate THEN
      Charge.Annual = Charge.Annual + RentalSpace.ContractedRental.
    ELSE IF market-per-unit THEN
      Charge.Annual = Charge.Annual + RentalSpace.ContractedRental.
    ELSE
      Charge.Annual = Charge.Annual + RentalSpace.MarketRental.
  END.
  
  FOR EACH Charge:
    Charge.Amount = annual-to-freq( Charge.FrequencyCode, Charge.Annual ).
  END.  

  /* Now see if we have to set up an outgoings charge */
  outgoings = get-result( "Recovery-Type" ) .
  IF outgoings = "B" OR outgoings = "F" THEN DO:
    CREATE Charge.
    Charge.RentChargeType = outgoings-type.
    Charge.StartDate = DATE( get-result( 'Rent-Start' ) ).
    Charge.StartDate = IF Charge.StartDate = ? THEN DATE( get-result( 'Lease-Start' ) ) ELSE Charge.StartDate.
    Charge.StartDate = IF Charge.Startdate = ? THEN TODAY ELSE Charge.StartDate.
    Charge.EndDate   = DATE( get-result( 'Rent-End' ) ).
    Charge.FrequencyCode = null-str( get-result( 'Rent-Frequency' ), "MNTH"  ).
    Charge.Annual = null-dec( DEC(get-result('Outgoings-Budget')), 0.0).
    Charge.Amount = annual-to-freq( Charge.FrequencyCode, Charge.Annual ).
  END.

  test-validated().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-charge V-table-Win 
PROCEDURE display-charge :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE Charge THEN DO:
    DISPLAY Charge WITH BROWSE {&BROWSE-NAME}.
    charge-type    = Charge.RentChargeType.
    account-code   = Charge.AccountCode.
    frequency-code = Charge.FrequencyCode.
  END.
  RUN account-changed.
  RUN sensitise-buttons.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  /* create some default charges if there aren't any yet */
  IF NOT CAN-FIND(FIRST Charge) THEN RUN create-default-charges.

DO WITH FRAME {&FRAME-NAME}:
  /* There is no real need to pick up values from the results
     as they will not be changed by other viewers */
  RUN display-select-combos.
  RUN open-charge-query.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-accounts V-table-Win 
PROCEDURE get-accounts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Populate the account combo */
  cmb_accounts:DELIMITER IN FRAME {&FRAME-NAME} = "~n".
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  FOR EACH ChartOfAccount NO-LOCK:
    IF cmb_accounts:ADD-LAST( STRING( ChartOfAccount.AccountCode, "9999.99" ) + 
      " " + ChartOfAccount.Name ) IN FRAME {&FRAME-NAME} THEN.
  END.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-charge-query V-table-Win 
PROCEDURE open-charge-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  OPEN QUERY {&BROWSE-NAME} FOR EACH Charge.
  RUN display-charge.
END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-charge V-table-Win 
PROCEDURE remove-charge :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN RETURN.

  IF {&BROWSE-NAME}:SELECT-FOCUSED-ROW() AND
     {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN
  DO:
    GET CURRENT {&BROWSE-NAME}.
    DELETE Charge.
    IF {&BROWSE-NAME}:DELETE-CURRENT-ROW() THEN.
  END.

  test-validated().
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = ? OR 
     NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 THEN
  DO:
    RUN close-charge-query.
    RUN account-changed.
    RUN sensitise-buttons.
    RETURN.
  END.

  IF {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN RUN display-charge.

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Charge"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-buttons V-table-Win 
PROCEDURE sensitise-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  DO WITH FRAME {&FRAME-NAME}:
    btn_remove:SENSITIVE  = AVAILABLE Charge.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF VAR charge-result AS CHAR NO-UNDO.
  DEF VAR charge-line   AS CHAR NO-UNDO.
  DEF VAR fd AS CHAR NO-UNDO. /* Field delimiter */
  DEF VAR ld AS CHAR NO-UNDO. /* Line delimiter */
  fd = '~~'. ld = '~n'.
  RUN assign-charge.
  
  FOR EACH Charge:
    charge-line =
      null-str( STRING( Charge.StartDate ), "?" ) + fd +
      null-str( STRING( Charge.EndDate ), "?" )   + fd +
      null-str( Charge.RentChargeType, "" ) + fd + 
      null-str( Charge.FrequencyCode, "" ) + fd +
      null-str( STRING( Charge.Amount ), "0" ) + fd + 
      null-str( STRING( Charge.PaidTo ), "?" ) + fd +
      null-str( STRING( Charge.AccountCode ), "?" ) + fd +
      null-str( Charge.Description, "" ) + fd +
      null-str( STRING( Charge.NetAnnual ), "0" ) .
    charge-result = charge-result + IF charge-result = "" THEN "" ELSE ld.
    charge-result = charge-result + charge-line.
  END.

  set-result( 'Rental-Charges', charge-result ).
  RETURN test-validated().
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION annual-to-freq V-table-Win 
FUNCTION annual-to-freq RETURNS DECIMAL
  ( INPUT freq AS CHAR, INPUT annual AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  DEF VAR n-days   AS INT NO-UNDO.
  DEF VAR n-months AS INT NO-UNDO.
  DEF VAR amount   AS DEC NO-UNDO INIT 0.00.
  
  n-months = get-freq-months( freq ).
  n-days   = get-freq-days( freq ).
  IF n-months <> ? THEN DO:
    amount = DEC( n-months ) / 12 * annual.
  END.
  ELSE IF n-days <> ? THEN DO:
    amount = DEC( n-days ) / 365 * annual.
  END.
  
  RETURN ROUND( amount, 2).   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION freq-to-annual V-table-Win 
FUNCTION freq-to-annual RETURNS DECIMAL
  ( INPUT freq AS CHAR, INPUT amount AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  DEF VAR n-days   AS INT NO-UNDO.
  DEF VAR n-months AS INT NO-UNDO.
  DEF VAR annual   AS DEC NO-UNDO INIT 0.00.
  
  n-months = get-freq-months( freq ).
  n-days   = get-freq-days( freq ).
  IF n-months <> ? THEN DO:
    annual = 12 / DEC( n-months ) * amount.
  END.
  ELSE IF n-days <> ? THEN DO:
    annual = 365 / DEC( n-days ) * amount.
  END.
  
  RETURN ROUND( annual, 2).   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NOT CAN-FIND( FIRST Charge ) THEN DO:
    set-validated( No ).
    RETURN "FAIL".
  END.
  
  set-validated( Yes ).
  RETURN "".   /* Function return value. */

END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

