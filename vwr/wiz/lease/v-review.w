&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF WORK-TABLE Review NO-UNDO
  FIELD Date          AS DATE FORMAT "99/99/9999" LABEL "Review Date    "
  FIELD ReviewType    LIKE RentReview.ReviewType LABEL " Type "
  FIELD Earliest      LIKE RentReview.Earliest  LABEL "Earliest Date"
  FIELD Latest        LIKE RentReview.Latest    LABEL "Latest Date"
  FIELD EstimateBasis LIKE RentReview.EstimateBasis
                      LABEL "Estimation Basis for Review" FORMAT "X(40)".
  
DEF VAR break-1 AS CHAR FORMAT "X" INIT " " LABEL " " NO-UNDO.
DEF VAR break-2 AS CHAR FORMAT "X" INIT " " LABEL " " NO-UNDO.
DEF VAR break-3 AS CHAR FORMAT "X" INIT " " LABEL " " NO-UNDO.

DEF VAR review-type LIKE RentReview.ReviewType NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-review

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Review

/* Definitions for BROWSE br-review                                     */
&Scoped-define FIELDS-IN-QUERY-br-review Date ReviewType Earliest Latest EstimateBasis   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-review Date  ReviewType  Earliest  Latest  EstimateBasis   
&Scoped-define FIELD-PAIRS-IN-QUERY-br-review~
 ~{&FP1}Date ~{&FP2}Date ~{&FP3}~
 ~{&FP1}ReviewType ~{&FP2}ReviewType ~{&FP3}~
 ~{&FP1}Earliest ~{&FP2}Earliest ~{&FP3}~
 ~{&FP1}Latest ~{&FP2}Latest ~{&FP3}~
 ~{&FP1}EstimateBasis ~{&FP2}EstimateBasis ~{&FP3}
&Scoped-define SELF-NAME br-review
&Scoped-define OPEN-QUERY-br-review OPEN QUERY {&SELF-NAME} FOR EACH Review.
&Scoped-define TABLES-IN-QUERY-br-review Review
&Scoped-define FIRST-TABLE-IN-QUERY-br-review Review


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-review}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-22 btn_add btn_remove br-review ~
cmb_review-type 
&Scoped-Define DISPLAYED-OBJECTS cmb_review-type 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 8 BY 1.15.

DEFINE BUTTON btn_remove 
     LABEL "&Remove" 
     SIZE 8 BY 1.15.

DEFINE VARIABLE cmb_review-type AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS " "
     SIZE 32 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 68 BY 12.8.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-review FOR 
      Review SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-review
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-review V-table-Win _FREEFORM
  QUERY br-review DISPLAY
      Date
  ReviewType
  Earliest
  Latest
  EstimateBasis
ENABLE
  Date
  ReviewType
  Earliest
  Latest
  EstimateBasis
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 66.86 BY 9.2
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     btn_add AT ROW 3.2 COL 51.86
     btn_remove AT ROW 3.2 COL 60.43
     br-review AT ROW 4.4 COL 1.57
     cmb_review-type AT ROW 10.4 COL 29.29 COLON-ALIGNED NO-LABEL
     "Are there any rent reviews for this lease ?" VIEW-AS TEXT
          SIZE 56 BY 1.2 AT ROW 1.6 COL 2.14
          FGCOLOR 1 FONT 12
     "If there are then enter the details below" VIEW-AS TEXT
          SIZE 27.43 BY 1 AT ROW 2.8 COL 7.29
          FGCOLOR 1 
     RECT-22 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 14.25
         WIDTH              = 74.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-review btn_remove F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       cmb_review-type:HIDDEN IN FRAME F-Main           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-review
/* Query rebuild information for BROWSE br-review
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH Review.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-review */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/method/m-mntvwr.i}
{inc/null.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN add-review.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_remove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_remove V-table-Win
ON CHOOSE OF btn_remove IN FRAME F-Main /* Remove */
DO:
  RUN remove-review.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_review-type
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_review-type V-table-Win
ON LEAVE OF cmb_review-type IN FRAME F-Main
DO:
  SELF:HIDDEN = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_review-type V-table-Win
ON TAB OF cmb_review-type IN FRAME F-Main
OR 'BACK-TAB' OF {&SELF-NAME} DO:
  APPLY LAST-EVENT:FUNCTION TO Review.ReviewType IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_review-type V-table-Win
ON U1 OF cmb_review-type IN FRAME F-Main
DO:
  {inc/selcmb/scrrt1.i "?" "review-type"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_review-type V-table-Win
ON U2 OF cmb_review-type IN FRAME F-Main
DO:
  {inc/selcmb/scrrt2.i "?" "review-type"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_review-type V-table-Win
ON VALUE-CHANGED OF cmb_review-type IN FRAME F-Main
DO:
  APPLY 'U2' TO SELF.
  IF AVAILABLE Review THEN DO:

    Review.ReviewType:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = review-type.
    RUN assign-review.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-review
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* Bring up the review type combo */
ON ENTRY OF Review.ReviewType IN BROWSE {&BROWSE-NAME} DO:
  cmb_review-type:X IN FRAME {&FRAME-NAME} = SELF:X.
  cmb_review-type:Y IN FRAME {&FRAME-NAME} = SELF:Y + {&BROWSE-NAME}:Y.
  cmb_review-type:HIDDEN IN FRAME {&FRAME-NAME} = No.
  cmb_review-type:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  review-type = INPUT BROWSE {&BROWSE-NAME} Review.ReviewType.
  APPLY 'U1':U TO cmb_review-type.
  IF cmb_review-type:MOVE-TO-TOP() THEN.
  APPLY 'ENTRY':U TO cmb_review-type.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-review V-table-Win 
PROCEDURE add-review :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR reposition-row   AS INT NO-UNDO.
  DEF VAR reposition-rowid AS ROWID NO-UNDO.
  
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN
    RUN open-review-query.
    
  IF AVAILABLE Review THEN RUN assign-review.
  
  CREATE Review.
  reposition-rowid = ROWID( Review ).
  
  reposition-row = {&BROWSE-NAME}:GET-REPOSITIONED-ROW().
  reposition-row =  INT( {&BROWSE-NAME}:HEIGHT-CHARS ) - 1.
  OPEN QUERY {&BROWSE-NAME} FOR EACH Review.
  IF {&BROWSE-NAME}:SET-REPOSITIONED-ROW( reposition-row, "CONDITIONAL" ) THEN.
  REPOSITION {&BROWSE-NAME} TO ROWID reposition-rowid.

  RUN display-review.
  APPLY 'ENTRY':U TO Review.Date IN BROWSE {&BROWSE-NAME}.

END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-review V-table-Win 
PROCEDURE assign-review :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE Review THEN 
    ASSIGN BROWSE {&BROWSE-NAME}
      Review.Date
      Review.ReviewType
      Review.Earliest
      Review.Latest
      Review.EstimateBasis.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-review-query V-table-Win 
PROCEDURE close-review-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY {&BROWSE-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-review V-table-Win 
PROCEDURE display-review :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE Review THEN
    DISPLAY 
      Review.Date
      Review.ReviewType
      Review.Earliest
      Review.Latest
      Review.EstimateBasis
    WITH BROWSE {&BROWSE-NAME}.
  RUN sensitise-buttons.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  /* There is no real need to pick up values from the results
     as they will not be changed by other viewers */
  RUN open-review-query.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-review-query V-table-Win 
PROCEDURE open-review-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  OPEN QUERY {&BROWSE-NAME} FOR EACH Review.
  RUN display-review.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-review V-table-Win 
PROCEDURE remove-review :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN RETURN.

  IF {&BROWSE-NAME}:SELECT-FOCUSED-ROW() AND
     {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN
  DO:
    GET CURRENT {&BROWSE-NAME}.
    DELETE Review.
    IF {&BROWSE-NAME}:DELETE-CURRENT-ROW() THEN.
  END.

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = ? OR 
     NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 THEN
  DO:
    RUN close-review-query.
    RUN sensitise-buttons.
    RETURN.
  END.

  IF {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN RUN display-review.

END.
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Review"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-buttons V-table-Win 
PROCEDURE sensitise-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
    btn_add:SENSITIVE     = Yes.
    btn_remove:SENSITIVE  = AVAILABLE Review.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR review-result AS CHAR NO-UNDO.
  DEF VAR review-line   AS CHAR NO-UNDO.
  DEF VAR fd AS CHAR NO-UNDO. /* Field delimiter */
  DEF VAR ld AS CHAR NO-UNDO. /* Line delimiter */
  fd = '~~'. ld = '~n'.
  RUN assign-review.
  
  FOR EACH Review:
    review-line = 
       null-str( STRING( Review.Date ), "?" ) + fd +
       null-str( STRING( Review.ReviewType ), "?" ) + fd +
       null-str( STRING( Review.Earliest ), "?" ) + fd +
       null-str( STRING( Review.Latest ), "?" ) + fd +
       null-str( STRING( Review.EstimateBasis ), "?" )
    .
      
    review-result = review-result + IF review-result = "" THEN "" ELSE ld.
    review-result = review-result + review-line.
  END.

  set-result( 'Rent-Reviews', review-result ).

  RETURN test-validated().

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF No THEN DO:
    set-validated( No ).
    RETURN "FAIL".
  END.
  
  set-validated( Yes ).
  RETURN "".   /* Function return value. */

END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


