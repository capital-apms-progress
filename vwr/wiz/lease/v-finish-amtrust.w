&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&GLOB USE-DEBUG 1
DEF VAR debug AS LOGI NO-UNDO INITIAL No.

{inc/ofc-this.i}
{inc/ofc-acct.i "RENT" "rent-account"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES TenancyLease
&Scoped-define FIRST-EXTERNAL-TABLE TenancyLease


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR TenancyLease.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-22 tgl_Invoice fil_InvoiceFrom ~
fil_InvoiceTo fil_Success-1 fil_Success-2 
&Scoped-Define DISPLAYED-OBJECTS tgl_Invoice fil_InvoiceFrom fil_InvoiceTo ~
fil_Success-1 fil_Success-2 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD include-lease V-table-Win 
FUNCTION include-lease RETURNS LOGICAL
  ( INPUT lease-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Goto-Error 
     LABEL "Find First Error" 
     SIZE 15 BY 1.15.

DEFINE VARIABLE fil_InvoiceFrom AS DATE FORMAT "99/99/9999":U 
     LABEL "For the period from" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_InvoiceTo AS DATE FORMAT "99/99/9999":U 
     LABEL "to" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_Success-1 AS CHARACTER FORMAT "X(256)":U INITIAL "Congratulations!" 
      VIEW-AS TEXT 
     SIZE 14.29 BY .8 NO-UNDO.

DEFINE VARIABLE fil_Success-2 AS CHARACTER FORMAT "X(256)":U INITIAL "You may now click the <Finish> button to commit your changes." 
      VIEW-AS TEXT 
     SIZE 46.29 BY .8 NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 54.29 BY 12.

DEFINE VARIABLE tgl_Invoice AS LOGICAL INITIAL ? 
     LABEL "Generate rental invoice now?" 
     VIEW-AS TOGGLE-BOX
     SIZE 24 BY .8 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     btn_Goto-Error AT ROW 4.2 COL 20.43
     tgl_Invoice AT ROW 10.6 COL 3.86
     fil_InvoiceFrom AT ROW 11.6 COL 19 COLON-ALIGNED
     fil_InvoiceTo AT ROW 11.6 COL 33.86 COLON-ALIGNED
     fil_Success-1 AT ROW 2.4 COL 19.57 COLON-ALIGNED NO-LABEL
     fil_Success-2 AT ROW 3.2 COL 6.72 NO-LABEL
     "the information you have entered" VIEW-AS TEXT
          SIZE 23.43 BY 1 AT ROW 7.6 COL 16.43
          FGCOLOR 1 
     RECT-22 AT ROW 1 COL 1
     "Lease Details Complete" VIEW-AS TEXT
          SIZE 32 BY 1 AT ROW 1.2 COL 12.43
          FGCOLOR 1 FONT 12
     "you can click the 'Finish' button and a new lease will be created using" VIEW-AS TEXT
          SIZE 47.43 BY 1 AT ROW 6.8 COL 4.43
          FGCOLOR 1 
     "If you are satisfied that you have entered all of the lease details then" VIEW-AS TEXT
          SIZE 46.29 BY 1 AT ROW 6 COL 5
          FGCOLOR 1 
     "Otherwise you can go back to previous steps using the 'Back' button" VIEW-AS TEXT
          SIZE 46.86 BY 1 AT ROW 9.4 COL 4.43
          FGCOLOR 1 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.TenancyLease
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.2
         WIDTH              = 58.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON btn_Goto-Error IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       btn_Goto-Error:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN fil_Success-2 IN FRAME F-Main
   ALIGN-L                                                              */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/method/m-mntvwr.i}
{inc/rentchrg.i}
{inc/string.i}
{inc/null.i}
{inc/persndtl.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Goto-Error
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Goto-Error V-table-Win
ON CHOOSE OF btn_Goto-Error IN FRAME F-Main /* Find First Error */
DO:
  RUN goto-first-error IN wizard-hdl.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_Invoice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_Invoice V-table-Win
ON VALUE-CHANGED OF tgl_Invoice IN FRAME F-Main /* Generate rental invoice now? */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "TenancyLease"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "TenancyLease"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE default-invoice-dates V-table-Win 
PROCEDURE default-invoice-dates :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR rental-charges AS CHAR NO-UNDO.
DEF VAR charge-line    AS CHAR NO-UNDO.
DEF VAR type           AS CHAR NO-UNDO.
DEF VAR def-start      AS DATE NO-UNDO.
DEF VAR this-start     AS DATE NO-UNDO.
DEF VAR this-end       AS DATE NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.
DEF VAR ld AS CHAR NO-UNDO.
DEF VAR fd AS CHAR NO-UNDO.
ld = "~n". fd = "~~".
def-start = last-of-month( TODAY + 1000 ) + 1.

  IF tgl_Invoice <> ? THEN RETURN.

DO WITH FRAME {&FRAME-NAME}:

  rental-charges = null-str( get-result( 'Rental-Charges' ), "" ).

  /* Iterate over all the charges and apply the changes */
  n = NUM-ENTRIES( rental-charges, ld ).
  DO i = 1 TO n:
    charge-line = ENTRY( i, rental-charges, ld ).

    this-start  = DATE( ENTRY( 1, charge-line, fd )).
    this-end    = DATE( ENTRY( 2, charge-line, fd )).

    IF this-start < def-start THEN def-start = this-start.
    IF this-end < def-start THEN def-start = this-end + 1.
  END.

  tgl_Invoice = (def-start <= last-of-month(TODAY)).
  IF tgl_Invoice = ? THEN ASSIGN
    tgl_Invoice = No
    fil_InvoiceFrom = ?
    fil_InvoiceTo = ?.
  ELSE DO:
    fil_InvoiceFrom = def-start.
    fil_InvoiceTo = last-of-month( def-start ).
    IF last-of-month(TODAY + 6) > fil_InvoiceTo THEN
      fil_InvoiceTo = last-of-month( TODAY + 6 ).
  END.

  DISPLAY       tgl_Invoice      fil_InvoiceFrom     fil_InvoiceTo .

END.

  RUN enable-appropriate-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  /* make sure this at least is validated! */
  set-validated( Yes ).
  RUN default-invoice-dates.

DO WITH FRAME {&FRAME-NAME}:
  IF NOT can-finish() THEN DO:
    fil_Success-1 = "    Wooops!".
    fil_Success-2 = "     Click on the button below to go to the screen in error".
    ENABLE btn_Goto-Error.
    VIEW btn_Goto-Error.
  END.
  ELSE DO:
    fil_Success-1 = "Congratulations!".
    fil_Success-2 = "You may now click the <Finish> button to apply your changes".
    DISABLE btn_Goto-Error.
    HIDE btn_Goto-Error.
  END.
  DISPLAY fil_Success-1 fil_Success-2 .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF INPUT tgl_Invoice THEN
    ENABLE fil_InvoiceFrom fil_InvoiceTo .
  ELSE
    DISABLE fil_InvoiceFrom fil_InvoiceTo .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE finish-wizard V-table-Win 
PROCEDURE finish-wizard :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR complete-it AS LOGICAL NO-UNDO INIT Yes.
  
  DO WHILE complete-it:
    RUN make-lease.
    IF RETURN-VALUE = "FAIL" THEN DO:
      MESSAGE
        "There was an error creating the lease!" SKIP(2)
        "Do you want to try again ?"
        VIEW-AS ALERT-BOX QUESTION BUTTONS RETRY-CANCEL TITLE "Database Error" 
        UPDATE complete-it.
    END.
    ELSE DO:
      IF INPUT FRAME {&FRAME-NAME} tgl_Invoice THEN RUN generate-invoice.
      MESSAGE "The lease has been created successfully."
        VIEW-AS ALERT-BOX INFORMATION TITLE "Done!".
      complete-it = No.
    END.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE generate-invoice V-table-Win 
PROCEDURE generate-invoice :
/*------------------------------------------------------------------------------
  Purpose:  Calculate all of the details of the tenants rental for the
            period indicated.
------------------------------------------------------------------------------*/
DEF VAR current-lease AS INT NO-UNDO.
DEF VAR period-d1 AS DATE NO-UNDO.
DEF VAR period-dn AS DATE NO-UNDO.
DEF VAR paid-to-list AS CHAR NO-UNDO.

DEF VAR invoice-total  AS DEC NO-UNDO     INITIAL 0.
DEF VAR invoice-tax    AS DEC NO-UNDO     INITIAL 0.
DEF VAR invoice-amount AS DEC NO-UNDO.
DEF VAR invoice-blurb  AS CHAR NO-UNDO    INITIAL "".

{inc/ofc-this.i}
{inc/ofc-set.i "Invoice-Terms" "default-terms" "WARNING"}

  IF ( INPUT FRAME {&FRAME-NAME} fil_InvoiceFrom = ?
                 OR INPUT FRAME {&FRAME-NAME} fil_InvoiceTo = ? ) THEN RETURN.


  current-lease = INT( get-result( 'Current-Lease' ) ).
  FIND TenancyLease WHERE TenancyLease.TenancyLeaseCode = current-lease NO-LOCK.
  FIND Tenant OF TenancyLease NO-LOCK.
  period-d1   = (INPUT FRAME {&FRAME-NAME} fil_Invoicefrom).
  period-dn   = (INPUT FRAME {&FRAME-NAME} fil_Invoiceto).

  RUN build-tenant-charges( Tenant.TenantCode, period-d1, period-dn ).
  RUN make-blurb( "charges", period-d1, period-dn, OUTPUT invoice-blurb,
                OUTPUT invoice-amount, OUTPUT invoice-tax, OUTPUT invoice-total ).


/******** Now actually create the invoice **********/
  CREATE    Invoice.
  ASSIGN    Invoice.InvoiceDate = TODAY
            Invoice.TaxApplies  = Office.GST <> ?
            Invoice.InvoiceStatus = "U"
            Invoice.Terms         = default-terms
            Invoice.InvoiceType   = "RENT"
            Invoice.EntityType    = "T"
            Invoice.EntityCode    = Tenant.TenantCode
            Invoice.ToDetail = "Rent charges from "
                             + STRING( period-d1, "99/99/9999" )
                             + " to " + STRING( period-dn, "99/99/9999" )
            Invoice.Blurb     = invoice-blurb
            Invoice.TaxAmount = invoice-tax
            Invoice.Total     = invoice-total.

  paid-to-list = "".
  FOR EACH ChargeDetail:
    paid-to-list = paid-to-list
                 + STRING( ChargeDetail.TenancyLeaseCode ) + "|"
                 + STRING( ChargeDetail.ChargeSeq) + "|"
                 + STRING( ChargeDetail.ChargeStart, "99/99/9999") + "|"
                 + STRING( ChargeDetail.ChargedUpTo, "99/99/9999")
                 + ",".
  END.
  Invoice.ToPay = RIGHT-TRIM( paid-to-list, ",").


DEF VAR last-type AS CHAR NO-UNDO INITIAL "not a possible type".
DEF VAR last-account AS DEC NO-UNDO INITIAL -999999.999 .
DEF VAR charge-from AS DATE NO-UNDO .

  FOR EACH InvoiceLine OF Invoice:      DELETE InvoiceLine.     END.

ON ASSIGN OF InvoiceLine.YourShare OVERRIDE DO: END.
  FOR EACH ChargeDetail:
    CREATE  InvoiceLine.
    ASSIGN  InvoiceLine.InvoiceNo   = Invoice.InvoiceNo
            InvoiceLine.EntityType  = Tenant.EntityType
            InvoiceLine.EntityCode  = Tenant.EntityCode
            InvoiceLine.AccountCode = ChargeDetail.AccountCode
            InvoiceLine.Percent     = 100.00
            InvoiceLine.AccountText = ChargeDetail.Description
                                    + " from " + STRING( ChargeDetail.ChargedFrom, "99/99/9999")
                                    + " to " + STRING( ChargeDetail.ChargedUpTo, "99/99/9999")
            InvoiceLine.Amount      = ChargeDetail.ChargeAmount
            InvoiceLine.YourShare   = InvoiceLine.Amount.
  END.
ON ASSIGN OF InvoiceLine.YourShare REVERT.

/*
  MESSAGE "Invoice" Invoice.InvoiceNo "created!" VIEW-AS ALERT-BOX INFORMATION
            TITLE "Invoice Created".
*/
  RUN process/report/invcappr.p( Invoice.InvoiceNo, Invoice.InvoiceNo, ? ).
                
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE make-guarantors V-table-Win 
PROCEDURE make-guarantors :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR guarantors AS CHAR NO-UNDO.
DEF VAR guarantor-line   AS CHAR NO-UNDO.
DEF VAR guarantor-person LIKE Person.PersonCode NO-UNDO.
DEF VAR guarantor-name   AS CHAR NO-UNDO.
DEF VAR guarantor-postaltype AS CHAR NO-UNDO.
DEF VAR ld AS CHAR NO-UNDO INITIAL '~n'. /* Charge Delimiter */
DEF VAR fd AS CHAR NO-UNDO INITIAL '~~'. /* Field Delimiter */
DEF VAR i AS INT NO-UNDO.
  
  guarantors = null-str( get-result( 'Guarantors' ), "" ).
  DO i = 1 TO NUM-ENTRIES( guarantors, ld ):

    guarantor-line = ENTRY( i, guarantors, ld ).

    guarantor-name   = TRIM( ENTRY( 2, guarantor-line, fd ) ).
    guarantor-person = INT( TRIM( ENTRY( 1, guarantor-line, fd ) ) ).
    FIND Person WHERE Person.PersonCode = guarantor-person 
                AND Person.PersonCode > 0 EXCLUSIVE-LOCK NO-ERROR.
    IF NOT AVAILABLE(Person) THEN DO:
      CREATE Person.
      Person.LastName  = guarantor-name.
    END.
    RUN split-name( guarantor-name, OUTPUT Person.PersonTitle, OUTPUT Person.FirstName,
                OUTPUT Person.MiddleNames, OUTPUT Person.LastName, OUTPUT Person.NameSuffix ).
    Person.Company = TRIM( ENTRY( 12, guarantor-line, fd)).

    guarantor-postaltype = TRIM( ENTRY( 3, guarantor-line, fd ) ).
    FIND PostalDetail OF Person WHERE PostalDetail.PostalType = guarantor-postaltype EXCLUSIVE-LOCK NO-ERROR.
    IF NOT AVAILABLE(PostalDetail) THEN DO:
      CREATE PostalDetail.
      PostalDetail.PersonCode = Person.PersonCode.
      PostalDetail.PostalType = guarantor-postaltype.
    END.

    PostalDetail.Address = REPLACE( ENTRY( 4, guarantor-line, fd ), "|", "~n").
    PostalDetail.City    = ENTRY( 5, guarantor-line, fd ).
    PostalDetail.State   = ENTRY( 6, guarantor-line, fd ).
    PostalDetail.Zip     = ENTRY( 7, guarantor-line, fd ).
    PostalDetail.Country = ENTRY( 8, guarantor-line, fd ).

    CREATE Guarantor.
    Guarantor.PersonCode = Person.PersonCode.
    Guarantor.TenancyLeaseCode = TenancyLease.TenancyLeaseCode.
    Guarantor.Type       = ENTRY( 11, guarantor-line, fd ).
    Guarantor.Limit      = DECIMAL( ENTRY( 9, guarantor-line, fd )).
    Guarantor.AnyData    = "Expiry," + ENTRY( 10, guarantor-line, fd ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE make-lease V-table-Win 
PROCEDURE make-lease :
/*------------------------------------------------------------------------------
  Purpose:     Try to complete the lease using the given details
               If there is an error then return.
------------------------------------------------------------------------------*/

ON WRITE OF RentCharge OVERRIDE DO: END.
DEF VAR charge-seq AS INT INIT 1 NO-UNDO.
DEF VAR i          AS INT  NO-UNDO.

OUTPUT TO VALUE(SESSION:TEMP-DIRECTORY + "/lease-wizard-log.log") APPEND.
debug = Yes.

DO TRANSACTION ON ERROR UNDO, RETURN "FAIL":

  MESSAGE " " SKIP "Creating Lease...".

  /* Create the lease */
  CREATE TenancyLease.
  TenancyLease.PropertyCode        = INT( get-result( 'Property' ) ) .
  TenancyLease.TenantCode          = INT( get-result( 'Tenant' ) ) .
  TenancyLease.AreaDescription     = get-result( 'Description' ) .
  TenancyLease.ElectricityUnitRate = null-dec( DEC( get-result( 'Electricity-Rate' ) ), 0.00 ) .
  TenancyLease.GrossLease          = null-log( get-result( 'Gross-Lease' ) = "Yes", Yes ) .
  TenancyLease.LeaseStatus         = "NORM" .
  TenancyLease.RecoveryType        = get-result( "Recovery-Type" ) .
  TenancyLease.LeaseType           = null-str( get-result( 'Lease-Type' ), 'STD' ) .
  TenancyLease.PrimarySpace        = INT( get-result( 'Primary-Space' ) ) .
  TenancyLease.LeaseStartDate      = DATE( get-result( 'Lease-Start' ) ) .
  TenancyLease.LeaseEndDate        = DATE( get-result( 'Lease-End' ) ) .
  TenancyLease.RentStartDate       = DATE( get-result( 'Rent-Start' ) ) .
  TenancyLease.RentEndDate         = DATE( get-result( 'Rent-End' ) ) .
  TenancyLease.RightsOfRenewal     = null-str( get-result( 'ROR' ), "" ) .
  TenancyLease.RORNoticePeriod     = null-int( INT( get-result( 'ROR-notice' ) ), 0 ) .
  TenancyLease.ReviewNoticePeriod  = null-int( INT( get-result( 'Review-Notice' ) ), 0 ) .
  TenancyLease.RatchetClause       = null-str( get-result( 'Ratchet-Clause' ), "" ) .
  TenancyLease.TaxApplies          = null-log( get-result( 'Exclude-GST' ) = "No", Yes ) .

  IF TenancyLease.LeaseStartDate = ? OR TenancyLease.LeaseEndDate = ? THEN DO:
    ASSIGN TenancyLease.TermYears  = 0
           TenancyLease.TermMonths = 0
           TenancyLease.TermDays   = 0.
  END.
  ELSE DO:
    IF TenancyLease.LeaseStartDate > TenancyLease.LeaseEndDate THEN
      TenancyLease.LeaseEndDate = TenancyLease.LeaseStartDate.
    date-diff( TenancyLease.LeaseStartDate, TenancyLease.LeaseEndDate - 1, 
               OUTPUT TenancyLease.TermYears, OUTPUT TenancyLease.TermMonths, OUTPUT TenancyLease.TermDays ).
  END.

  set-result( 'Current-Lease', STRING(TenancyLease.TenancyLeaseCode)).

  /* Point the rental spaces to this lease */    
  DEF VAR rsp-list AS CHAR NO-UNDO.
  rsp-list = null-str( get-result( 'Rental-Spaces' ), "" ).
  ON WRITE OF RentalSpace OVERRIDE DO: END.
  DO i = 1 TO NUM-ENTRIES( rsp-list ):
    FIND RentalSpace
    WHERE RentalSpace.PropertyCode = TenancyLease.PropertyCode
      AND RentalSpace.RentalSpaceCode = INT( ENTRY( i, rsp-list ) )
      EXCLUSIVE-LOCK.
    ASSIGN RentalSpace.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
           RentalSpace.AreaStatus       = "L".
  END.
  
  rsp-list = null-str( get-result( 'Contracted-Rentals' ), "" ).
  DO i = 1 TO NUM-ENTRIES( rsp-list ):
    FIND RentalSpace WHERE RentalSpace.PropertyCode = TenancyLease.PropertyCode
                    AND RentalSpace.RentalSpaceCode = INT( ENTRY( 1, ENTRY( i, rsp-list ), "/") )
                    EXCLUSIVE-LOCK.
    RentalSpace.ContractedRental = DEC( ENTRY( 2, ENTRY( i, rsp-list ), "/") ).
  END.
  ON WRITE OF RentalSpace REVERT.

  /* Create the rental charges */
  DEF VAR rental-charges AS CHAR NO-UNDO.
  DEF VAR charge-line   AS CHAR NO-UNDO.
  DEF VAR ld AS CHAR NO-UNDO. /* Charge Delimiter */
  DEF VAR fd AS CHAR NO-UNDO. /* Field Delimiter */
  ld = '~n'. fd = '~~'.
  
  rental-charges = null-str( get-result( 'Rental-charges' ), "" ).
  DO i = 1 TO NUM-ENTRIES( rental-charges, ld ):

    charge-line = ENTRY( i, Rental-charges, ld ).
    FIND RentCharge WHERE RentCharge.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
                      AND RentCharge.SequenceCode     = charge-seq NO-ERROR.
    IF NOT AVAILABLE(RentCharge) THEN CREATE RentCharge.

    ASSIGN RentCharge.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
           RentCharge.AccountCode      = DEC( ENTRY( 7, charge-line, fd ) )
           RentCharge.Description      = ENTRY( 8, charge-line, fd )
           RentCharge.EntityType       = "P"
           RentCharge.EntityCode       = TenancyLease.PropertyCode
           RentCharge.RentChargeType   = ENTRY( 3, charge-line, fd )
           RentCharge.SequenceCode     = charge-seq.
         
    CREATE RentChargeLine.
    ASSIGN RentChargeLine.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
           RentChargeLine.SequenceCode     = charge-seq
           RentChargeLine.DateCommitted    = TODAY
           RentChargeLine.Amount           = DEC( ENTRY( 5, charge-line, fd ) )
           RentChargeLine.StartDate        = DATE( ENTRY( 1, charge-line, fd ) )
           RentChargeLine.EndDate          = DATE( ENTRY( 2, charge-line, fd ) )
           RentChargeLine.LastChargedDate  = DATE( ENTRY( 6, charge-line, fd ) )
           RentChargeLine.FrequencyCode    = ENTRY( 4, charge-line, fd )
           RentChargeLine.RentChargeLineStatus = "C"
           RentChargeLine.EstimatedNetRentAmount = DEC( ENTRY( 9, charge-line, fd ) )
           charge-seq                  = charge-seq + 1.

  END.

  RUN make-guarantors.
  
  /* Do something with the assignment details */
  
  /*
  TenancyLease.AssignedLeaseCode
  */
  
  ASSIGN TenancyLease.AssignmentDate = DATE( get-result( 'Assignment-Date' ) ).
  
  /*                                          */
  
  /* Create the rent reviews */
  DEF VAR reviews AS CHAR NO-UNDO.
  DEF VAR review-line   AS CHAR NO-UNDO.
  ld = '~n'. fd = '~~'.
  
  reviews = null-str( get-result( 'Rent-Reviews' ), "" ).
  DO i = 1 TO NUM-ENTRIES( reviews, ld ):
    review-line = ENTRY( i, reviews, ld ).
    CREATE RentReview.
    ASSIGN
      RentReview.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
      RentReview.DateDue          = DATE( ENTRY( 1, review-line, fd ) )
      RentReview.ReviewType       = ENTRY( 2, review-line, fd )
      RentReview.Earliest         = DATE( ENTRY( 3, review-line, fd ) )
      RentReview.Latest           = DATE( ENTRY( 4, review-line, fd ) )
      RentReview.EstimateBasis    = ENTRY( 5, review-line, fd )
      .
    RentReview.NewRental        = null-dec( DEC( ENTRY( 3, review-line, fd ) ), 0.00 ).
    RentReview.NewRentStart     = DATE( ENTRY( 4, review-line, fd ) ).
    RentReview.ReviewStatus     = "TODO".
    RentReview.ReviewType       = "NORM".
  END.
 

  /* Set up the outgoings information */
  DEF VAR outgoings AS CHAR NO-UNDO.
  DEF VAR outgoing-line   AS CHAR NO-UNDO.
  DEF VAR outgoings-notes AS CHAR NO-UNDO.
  ld = '~n'. fd = '~~'.

  ASSIGN
    TenancyLease.OutgoingsBudget = null-dec( DEC( get-result( 'Outgoings-Budget' ) ), 0.00 )
    TenancyLease.OutgoingsRate   = null-dec( DEC( get-result( 'Outgoings-Rate' ) ), 0.00 ).
  
  outgoings = null-str( get-result( 'Outgoings' ), "" ).
  DO i = 1 TO NUM-ENTRIES( outgoings, ld ):
    outgoing-line = ENTRY( i, outgoings, ld ).
    CREATE TenancyOutgoing.
    TenancyOutgoing.TenancyLeaseCode = TenancyLease.TenancyLeaseCode.
    TenancyOutgoing.AccountCode    = null-dec( DEC( ENTRY( 1, outgoing-line, fd ) ), 0.00 ).
    TenancyOutgoing.Percentage     = null-dec( DEC( ENTRY( 2, outgoing-line, fd ) ), 0.00 ) .
    TenancyOutgoing.BaseYear       = DATE( ENTRY( 3, outgoing-line, fd ) ).
    TenancyOutgoing.BaseYearAmount = null-dec( DEC( ENTRY( 4, outgoing-line, fd ) ), 0.00 ).
    TenancyOutgoing.FixedAmount    = null-dec( DEC( ENTRY( 5, outgoing-line, fd ) ), 0.00 ).
    TenancyOutgoing.ReconciliationDue = DATE( ENTRY( 6, outgoing-line, fd ) ).
    
  END.

  outgoings-notes = TRIM( null-str( get-result( 'OutGoings-Notes' ), "" ) ).
  IF outgoings-notes <> "" THEN DO:
    DEF BUFFER LastNote FOR Note.
    FIND LAST LastNote NO-LOCK NO-ERROR.
    CREATE Note.
    ASSIGN
      Note.NoteCode = IF AVAILABLE LastNote THEN LastNote.NoteCode + 1 ELSE 1
      Note.Details = outgoings-notes
      TenancyLease.NoteCode = Note.NoteCode.
  END.
    
  MESSAGE "Lease generation completed successfully" SKIP " " SKIP.
END.  
debug = No.
OUTPUT CLOSE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "TenancyLease"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  set-validated( No ).
  RETURN "FAIL".
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION include-lease V-table-Win 
FUNCTION include-lease RETURNS LOGICAL
  ( INPUT lease-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  Dummy function.
------------------------------------------------------------------------------*/

  RETURN Yes.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


