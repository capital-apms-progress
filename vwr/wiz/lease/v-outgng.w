&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF WORK-TABLE OutGoing NO-UNDO LIKE TenancyOutGoing.
  
DEF VAR break-1 AS CHAR FORMAT "X" INIT " " LABEL " " NO-UNDO.
DEF VAR account-code LIKE ChartOfAccount.AccountCode NO-UNDO.
DEF VAR got-accounts AS LOGI NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set.i "AcctGroup-Opex" "acctgroup-opex" "ERROR"}
{inc/ofc-set.i "Default-Lease-OGType" "default-outgoings"}
IF NOT AVAILABLE(OfficeSetting) THEN default-outgoings = "F".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-og

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES TenancyLease
&Scoped-define FIRST-EXTERNAL-TABLE TenancyLease


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR TenancyLease.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES OutGoing

/* Definitions for BROWSE br-og                                         */
&Scoped-define FIELDS-IN-QUERY-br-og AccountCode Percentage BaseYear BaseYearAmount FixedAmount ReconciliationDue   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-og AccountCode  Percentage  BaseYear  BaseYearAmount  FixedAmount  ReconciliationDue   
&Scoped-define FIELD-PAIRS-IN-QUERY-br-og~
 ~{&FP1}AccountCode ~{&FP2}AccountCode ~{&FP3}~
 ~{&FP1}Percentage ~{&FP2}Percentage ~{&FP3}~
 ~{&FP1}BaseYear ~{&FP2}BaseYear ~{&FP3}~
 ~{&FP1}BaseYearAmount ~{&FP2}BaseYearAmount ~{&FP3}~
 ~{&FP1}FixedAmount ~{&FP2}FixedAmount ~{&FP3}~
 ~{&FP1}ReconciliationDue ~{&FP2}ReconciliationDue ~{&FP3}
&Scoped-define SELF-NAME br-og
&Scoped-define OPEN-QUERY-br-og OPEN QUERY {&SELF-NAME} FOR EACH OutGoing.
&Scoped-define TABLES-IN-QUERY-br-og OutGoing
&Scoped-define FIRST-TABLE-IN-QUERY-br-og OutGoing


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-og}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-22 rs_RecoveryType Outgoings-Budget ~
Btn_Refresh Outgoings-Rate btn_add btn_remove br-og cmb_accounts edt-notes ~
fil_Account 
&Scoped-Define DISPLAYED-OBJECTS rs_RecoveryType Outgoings-Budget ~
Outgoings-Rate cmb_accounts edt-notes fil_Account 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD annual-outgoings V-table-Win 
FUNCTION annual-outgoings RETURNS DECIMAL
  ( /* no parameters */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "&Add" 
     SIZE 8 BY 1.

DEFINE BUTTON Btn_Refresh AUTO-GO DEFAULT 
     LABEL "Refresh" 
     SIZE 10.29 BY 1
     BGCOLOR 8 .

DEFINE BUTTON btn_remove 
     LABEL "&Remove" 
     SIZE 8 BY 1.

DEFINE VARIABLE cmb_accounts AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEMS " "
     SIZE 39.43 BY 1 NO-UNDO.

DEFINE VARIABLE edt-notes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 62.29 BY 3.6 NO-UNDO.

DEFINE VARIABLE fil_Account AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 37.72 BY 1
     FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE Outgoings-Budget AS DECIMAL FORMAT "$>,>>>,>>9.99" INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 14.57 BY 1.

DEFINE VARIABLE Outgoings-Rate AS DECIMAL FORMAT ">>9.99%" INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 9.14 BY 1.

DEFINE VARIABLE rs_RecoveryType AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Budgeted cost", "B",
"Fixed mthly charge", "F",
"Actual costs", "A"
     SIZE 41.72 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 64.57 BY 19.8.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-og FOR 
      OutGoing SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-og
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-og V-table-Win _FREEFORM
  QUERY br-og DISPLAY
      AccountCode LABEL "Account"
  Percentage  LABEL "Percentage" FORMAT ">>9.99"
  BaseYear    LABEL "Base Year     "
  BaseYearAmount LABEL "Base Amount"
  FixedAmount LABEL "Fixed Amount"
  ReconciliationDue LABEL "Reconl Due     "
ENABLE
  AccountCode
  Percentage
  BaseYear
  BaseYearAmount
  FixedAmount
  ReconciliationDue
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 62.29 BY 7.2
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     rs_RecoveryType AT ROW 2.8 COL 23.29 NO-LABEL
     Outgoings-Budget AT ROW 4.2 COL 37.86 COLON-ALIGNED NO-LABEL
     Btn_Refresh AT ROW 4.2 COL 54.72
     Outgoings-Rate AT ROW 5.6 COL 52.72 COLON-ALIGNED NO-LABEL
     btn_add AT ROW 7.4 COL 47.86
     btn_remove AT ROW 7.4 COL 56.43
     br-og AT ROW 8.4 COL 2.14
     cmb_accounts AT ROW 9.4 COL 20.72 COLON-ALIGNED NO-LABEL
     edt-notes AT ROW 17 COL 2.14 NO-LABEL
     fil_Account AT ROW 7.4 COL 7 COLON-ALIGNED NO-LABEL
     "If there are any other notes about the outgoings for this lease, enter them below" VIEW-AS TEXT
          SIZE 56 BY 1 AT ROW 16 COL 2.14
          FGCOLOR 1 
     "Enter the annual outgoings budget for this lease here" VIEW-AS TEXT
          SIZE 37.14 BY 1 AT ROW 4.2 COL 2.14
          FGCOLOR 1 
     "How will expenses be charged:" VIEW-AS TEXT
          SIZE 21.14 BY 1 AT ROW 2.8 COL 2.14
          FGCOLOR 1 
     "What default percentage will the tenant pay on all outgoings for this lease ?" VIEW-AS TEXT
          SIZE 51.43 BY 1 AT ROW 5.6 COL 2.14
          FGCOLOR 1 FONT 10
     "What Outgoings will the tenant pay ?" VIEW-AS TEXT
          SIZE 48.57 BY 1.2 AT ROW 1.2 COL 4.43
          FGCOLOR 1 FONT 12
     "Add any outgoings that differ from the default percentage below" VIEW-AS TEXT
          SIZE 48 BY .8 AT ROW 6.6 COL 2.14
          FGCOLOR 1 FONT 10
     "Account:" VIEW-AS TEXT
          SIZE 6.29 BY 1 AT ROW 7.4 COL 2.14
     RECT-22 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON Btn_Refresh.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.TenancyLease
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 22.95
         WIDTH              = 72.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-og btn_remove F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       cmb_accounts:HIDDEN IN FRAME F-Main           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-og
/* Query rebuild information for BROWSE br-og
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH OutGoing.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-og */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/method/m-mntvwr.i}
{inc/null.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN add-outgoing.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Refresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Refresh V-table-Win
ON CHOOSE OF Btn_Refresh IN FRAME F-Main /* Refresh */
DO:
  RUN refresh-outgoings-budget.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_remove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_remove V-table-Win
ON CHOOSE OF btn_remove IN FRAME F-Main /* Remove */
DO:
  RUN remove-outgoing.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_accounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON LEAVE OF cmb_accounts IN FRAME F-Main
DO:
  SELF:HIDDEN = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON TAB OF cmb_accounts IN FRAME F-Main
OR 'BACK-TAB' OF {&SELF-NAME} DO:
  APPLY LAST-EVENT:FUNCTION TO OutGoing.AccountCode IN BROWSE {&BROWSE-NAME}.
  SELF:HIDDEN = Yes.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON U2 OF cmb_accounts IN FRAME F-Main
DO:
  account-code = DEC( SUBSTR( INPUT {&SELF-NAME}, 1, 7 ) ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_accounts V-table-Win
ON VALUE-CHANGED OF cmb_accounts IN FRAME F-Main
DO:
  APPLY 'U2' TO SELF.
  IF AVAILABLE OutGoing THEN DO:
    OutGoing.AccountCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( account-code ).
    RUN account-changed.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Outgoings-Rate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Outgoings-Rate V-table-Win
ON LEAVE OF Outgoings-Rate IN FRAME F-Main
DO:
  IF INPUT {&SELF-NAME} > 100.00 THEN DO:
    SELF:SCREEN-VALUE = STRING( 100.00 ).
    BELL.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs_RecoveryType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs_RecoveryType V-table-Win
ON VALUE-CHANGED OF rs_RecoveryType IN FRAME F-Main
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-og
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

ON LEAVE OF OutGoing.AccountCode IN BROWSE {&BROWSE-NAME}
DO:
  RUN account-changed.
END.

ON LEAVE OF OutGoing.Percentage IN BROWSE {&BROWSE-NAME}
DO:
  IF INPUT BROWSE {&BROWSE-NAME} OutGoing.Percentage > 100 THEN DO:
    ASSIGN OutGoing.Percentage:SCREEN-VALUE = get-result( 'Outgoings-Rate' ).
    BELL.
  END.
END.

/* Bring up the account code combo */
ON ENTRY OF Outgoing.AccountCode IN BROWSE {&BROWSE-NAME} DO:
  IF NOT got-accounts THEN RUN get-accounts.
  got-accounts = Yes.
  cmb_accounts:X IN FRAME {&FRAME-NAME} = SELF:X + {&BROWSE-NAME}:X.
  cmb_accounts:Y IN FRAME {&FRAME-NAME} = SELF:Y + {&BROWSE-NAME}:Y.
  cmb_accounts:HIDDEN IN FRAME {&FRAME-NAME} = No.
  cmb_accounts:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
  account-code = INPUT BROWSE {&BROWSE-NAME} OutGoing.AccountCode.
  APPLY 'U1':U TO cmb_accounts.
  IF cmb_accounts:MOVE-TO-TOP() THEN.
  APPLY 'ENTRY':U TO cmb_accounts.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE account-changed V-table-Win 
PROCEDURE account-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  fil_Account = "".
  FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = INPUT BROWSE {&BROWSE-NAME}
    OutGoing.AccountCode NO-LOCK NO-ERROR.
  IF AVAILABLE ChartOfAccount THEN fil_Account = ChartOfAccount.Name.
  DISPLAY fil_account WITH FRAME {&FRAME-NAME}.
  RUN assign-outgoing.
END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-outgoing V-table-Win 
PROCEDURE add-outgoing :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR reposition-row   AS INT NO-UNDO.
  DEF VAR reposition-rowid AS ROWID NO-UNDO.
  
  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN
    RUN open-outgoings-query.
    
  IF AVAILABLE OutGoing THEN RUN assign-outgoing.
  
  CREATE OutGoing.
  ASSIGN OutGoing.Percentage = INPUT OutGoings-Rate.
  reposition-rowid = ROWID( OutGoing ).
  
  reposition-row = {&BROWSE-NAME}:GET-REPOSITIONED-ROW().
  reposition-row =  INT( {&BROWSE-NAME}:HEIGHT-CHARS ) - 1.
  OPEN QUERY {&BROWSE-NAME} FOR EACH OutGoing.
  IF {&BROWSE-NAME}:SET-REPOSITIONED-ROW( reposition-row, "CONDITIONAL" ) THEN.
  REPOSITION {&BROWSE-NAME} TO ROWID reposition-rowid.

  RUN display-outgoing.
  APPLY 'ENTRY':U TO OutGoing.AccountCode IN BROWSE {&BROWSE-NAME}.

END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "TenancyLease"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "TenancyLease"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-outgoing V-table-Win 
PROCEDURE assign-outgoing :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE OutGoing THEN 
    ASSIGN BROWSE {&BROWSE-NAME}
      Outgoing.AccountCode
      Outgoing.Percentage
      Outgoing.BaseYear
      Outgoing.BaseYearAmount
      Outgoing.FixedAmount
      Outgoing.ReconciliationDue.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-outgoing-query V-table-Win 
PROCEDURE close-outgoing-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY {&BROWSE-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-outgoing V-table-Win 
PROCEDURE display-outgoing :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF AVAILABLE OutGoing THEN
    DISPLAY 
      Outgoing.AccountCode
      Outgoing.Percentage
      Outgoing.BaseYear
      Outgoing.BaseYearAmount
      Outgoing.FixedAmount
      Outgoing.ReconciliationDue
    WITH BROWSE {&BROWSE-NAME}.
  RUN account-changed.
  RUN sensitise-buttons.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR outgoing-result AS CHAR NO-UNDO.
DEF VAR outgoing-line   AS CHAR NO-UNDO.
DEF VAR fd AS CHAR NO-UNDO. /* Field delimiter */
DEF VAR ld AS CHAR NO-UNDO. /* Line delimiter */
fd = '~~'. ld = '~n'.
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.
  
  FOR EACH OutGoing:  DELETE Outgoing. END.
  outgoing-result = null-str( get-result( 'Outgoings' ), "").
  n = NUM-ENTRIES( outgoing-result, ld ).
  IF outgoing-result <> "" THEN DO i = 1 TO n:
    outgoing-line = ENTRY( i, outgoing-result, ld ).
    CREATE Outgoing.
    OutGoing.AccountCode = DECIMAL( ENTRY( 1, outgoing-line, fd ) ).
    OutGoing.Percentage = DECIMAL( ENTRY( 2, outgoing-line, fd ) ).
    OutGoing.BaseYear = DATE( ENTRY( 3, outgoing-line, fd ) ).
    OutGoing.BaseYearAmount = DECIMAL( ENTRY( 4, outgoing-line, fd ) ).
    OutGoing.FixedAmount = DECIMAL( ENTRY( 5, outgoing-line, fd ) ).
    OutGoing.ReconciliationDue = DATE( ENTRY( 6, outgoing-line, fd ) ).
  END.

DO WITH FRAME {&FRAME-NAME}:
  rs_RecoveryType  = null-str( get-result( 'Recovery-Type' ), default-outgoings ).
  outgoings-rate   = null-dec( DEC( get-result( 'Outgoings-Rate' ) ), 0.00 ).
  outgoings-budget = null-dec( DEC( get-result( 'Outgoings-Budget' ) ), 0.00 ).
  edt-notes = null-str( get-result( 'Outgoings-Notes'), "" ).
  DISPLAY rs_RecoveryType outgoings-rate outgoings-budget WITH FRAME {&FRAME-NAME}.
  RUN open-outgoings-query.
  RUN enable-appropriate-fields.
  RUN sensitise-buttons.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  CASE INPUT rs_RecoveryType:
    WHEN "F" THEN DO:
      ENABLE Outgoings-Budget.
      HIDE btn_Refresh.
    END.
    WHEN "B" THEN DO:
      DISABLE Outgoings-Budget.
      VIEW btn_Refresh.
    END.
    OTHERWISE DO:
      DISABLE Outgoings-Budget.
      HIDE btn_Refresh.
    END.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-accounts V-table-Win 
PROCEDURE get-accounts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Populate the account combo */

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  FOR EACH AccountGroup NO-LOCK
          WHERE LOOKUP( AccountGroup.AccountGroupCode, acctgroup-opex)  > 0:
    FOR EACH ChartOfAccount NO-LOCK OF AccountGroup:
      IF cmb_accounts:ADD-LAST( STRING( ChartOfAccount.AccountCode, "9999.99" ) + 
        " " + ChartOfAccount.Name ) IN FRAME {&FRAME-NAME} THEN .
    END.
  END.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-outgoings-query V-table-Win 
PROCEDURE open-outgoings-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
/*
DEF VAR og-list AS CHAR NO-UNDO.
  og-list      = get-result( 'Outgoings':U ).
  IF og-list = ? THEN og-list = "".
  
  FOR EACH Outgoing: DELETE Outgoing. END.

  IF NOT AVAILABLE Property THEN DO:
    RUN close-outgoing-query.
    RETURN.
  END.
*/

  OPEN QUERY br-og FOR EACH Outgoing.
  RUN sensitise-buttons.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-outgoings-budget V-table-Win 
PROCEDURE refresh-outgoings-budget :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  Outgoings-Budget:SCREEN-VALUE = STRING( annual-outgoings(), Outgoings-Budget:FORMAT).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-outgoing V-table-Win 
PROCEDURE remove-outgoing :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 OR NUM-RESULTS( "{&BROWSE-NAME}" ) = ? THEN RETURN.

  IF {&BROWSE-NAME}:SELECT-FOCUSED-ROW() AND
     {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN
  DO:
    GET CURRENT {&BROWSE-NAME}.
    DELETE OutGoing.
    IF {&BROWSE-NAME}:DELETE-CURRENT-ROW() THEN.
  END.

  IF NUM-RESULTS( "{&BROWSE-NAME}" ) = ? OR 
     NUM-RESULTS( "{&BROWSE-NAME}" ) = 0 THEN
  DO:
    RUN close-outgoing-query.
    RUN sensitise-buttons.
    RETURN.
  END.

  IF {&BROWSE-NAME}:FETCH-SELECTED-ROW(1) THEN RUN display-outgoing.

END.
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "TenancyLease"}
  {src/adm/template/snd-list.i "OutGoing"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-buttons V-table-Win 
PROCEDURE sensitise-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
    btn_add:SENSITIVE     = Yes.
    btn_remove:SENSITIVE  = AVAILABLE Outgoing.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR outgoing-result AS CHAR NO-UNDO.
  DEF VAR outgoing-line   AS CHAR NO-UNDO.
  DEF VAR fd AS CHAR NO-UNDO. /* Field delimiter */
  DEF VAR ld AS CHAR NO-UNDO. /* Line delimiter */
  fd = '~~'. ld = '~n'.
  RUN assign-outgoing.
  
  FOR EACH OutGoing:
    outgoing-line = 
       null-str( STRING( OutGoing.AccountCode ), "0000.00" )    + fd +
       null-str( STRING( OutGoing.Percentage ), "0" )      + fd +
       null-str( STRING( OutGoing.BaseYear ), "?" )        + fd +
       null-str( STRING( OutGoing.BaseYearAmount ), "0" )  + fd +
       null-str( STRING( OutGoing.FixedAmount ), "0" )     + fd +
       null-str( STRING( OutGoing.ReconciliationDue ), "?" )
    .
      
    outgoing-result = outgoing-result + IF outgoing-result = "" THEN "" ELSE ld.
    outgoing-result = outgoing-result + outgoing-line.
  END.

  ASSIGN FRAME {&FRAME-NAME} OutGoings-Budget OutGoings-Rate rs_RecoveryType .
  set-result( 'Recovery-Type', null-str( rs_RecoveryType, "B") ).
  set-result( 'Outgoings-Budget', STRING( null-dec( OutGoings-Budget, 0.00 ) ) ).
  set-result( 'Outgoings-Rate',   STRING( null-dec( OutGoings-Rate, 0.00 ) ) ).
  set-result( 'Outgoings', outgoing-result ).
  set-result( 'Outgoings-Notes', null-str( edt-notes:SCREEN-VALUE, "" ) ).

  RETURN test-validated().

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION annual-outgoings V-table-Win 
FUNCTION annual-outgoings RETURNS DECIMAL
  ( /* no parameters */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Calculate the estimated annual outgoings charges
------------------------------------------------------------------------------*/
DEF BUFFER Pog FOR PropertyOutgoing.
DEF BUFFER Tog FOR Outgoing.

DEF VAR property-code AS INT NO-UNDO.
DEF VAR fixed AS DEC NO-UNDO.
DEF VAR percent AS DEC NO-UNDO.
DEF VAR amount AS DEC NO-UNDO   INITIAL 0.0 .

DO WITH FRAME {&FRAME-NAME}:
  property-code = INT( get-result("Property" ) ).
  FOR EACH Pog WHERE Pog.PropertyCode = property-code NO-LOCK:
    FIND FIRST Tog WHERE Tog.AccountCode = Pog.AccountCode NO-LOCK NO-ERROR.
    percent = 0.
    fixed = 0.
    IF AVAILABLE(Tog) THEN DO:
      IF Tog.FixedAmount > 0 THEN
        fixed = ROUND(Tog.FixedAmount / 12, 2).
      ELSE
        percent = Tog.Percentage.
    END.
    ELSE
      percent = INPUT Outgoings-Rate.

    IF percent = ? THEN percent = 0.
    IF Pog.BudgetAmount = ? THEN DO:
      MESSAGE Pog.BudgetAmount "is not a valid budget!".
      NEXT.
    END.
    amount = amount + fixed + ROUND((percent * Pog.BudgetAmount) / 1200, 2).
  END.

  FOR EACH Tog WHERE NOT CAN-FIND( Pog WHERE Pog.PropertyCode = property-code
                                         AND Tog.AccountCode = Pog.AccountCode )
                 NO-LOCK:
    fixed = ROUND(Tog.FixedAmount / 12, 2).
    amount = amount + fixed.
  END.

  RETURN amount * 12.
END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF No THEN DO:
    set-validated( No ).
    RETURN "FAIL".
  END.
  
  set-validated( Yes ).
  RETURN "".   /* Function return value. */

END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


