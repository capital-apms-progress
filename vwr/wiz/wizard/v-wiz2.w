&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW

/* Temp-Table and Buffer definitions                                    */
DEFINE TEMP-TABLE WizardViewer NO-UNDO LIKE ttpl.OfficeSettings
       FIELD StepViewer AS CHAR FORMAT 'X(40)' LABEL 'Viewer'
       FIELD StepHelp AS CHAR FORMAT 'X(30)' LABEL 'Help'
       FIELD Sequence AS INT FORMAT '>>9' LABEL 'Seq'
       FIELD Mandatory AS LOGICAL FORMAT 'Yes/No' LABEL 'Mand'
       INDEX x1 IS PRIMARY Sequence StepViewer
       INDEX x2 IS UNIQUE StepViewer.


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF BUFFER OtherViewer FOR WizardViewer.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-viewers

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES WizardViewer

/* Definitions for BROWSE br-viewers                                    */
&Scoped-define FIELDS-IN-QUERY-br-viewers StepViewer StepHelp Sequence Mandatory   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-viewers StepViewer   StepHelp   Sequence   Mandatory   
&Scoped-define FIELD-PAIRS-IN-QUERY-br-viewers~
 ~{&FP1}StepViewer ~{&FP2}StepViewer ~{&FP3}~
 ~{&FP1}StepHelp ~{&FP2}StepHelp ~{&FP3}~
 ~{&FP1}Sequence ~{&FP2}Sequence ~{&FP3}~
 ~{&FP1}Mandatory ~{&FP2}Mandatory ~{&FP3}
&Scoped-define SELF-NAME br-viewers
&Scoped-define OPEN-QUERY-br-viewers OPEN QUERY {&SELF-NAME} FOR EACH WizardViewer         BY WizardViewer.Sequence BY WizardViewer.StepViewer.
&Scoped-define TABLES-IN-QUERY-br-viewers WizardViewer
&Scoped-define FIRST-TABLE-IN-QUERY-br-viewers WizardViewer


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-viewers}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-21 fil_WizName fil_Description ~
fil_ButtonLabel btn_Add btn_Del br-viewers fil_WizFinish tgl_jump 
&Scoped-Define DISPLAYED-OBJECTS fil_WizName fil_Description ~
fil_ButtonLabel fil_WizFinish tgl_jump 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Add 
     LABEL "&Add" 
     SIZE 8 BY 1.

DEFINE BUTTON btn_Del 
     LABEL "&Del" 
     SIZE 8 BY 1.

DEFINE VARIABLE fil_ButtonLabel AS CHARACTER FORMAT "X(256)":U 
     LABEL "Default Button" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Description AS CHARACTER FORMAT "X(256)":U 
     LABEL "Tool tip" 
     VIEW-AS FILL-IN 
     SIZE 52 BY 1 NO-UNDO.

DEFINE VARIABLE fil_WizFinish AS CHARACTER FORMAT "X(256)":U 
     LABEL "Finish Process" 
     VIEW-AS FILL-IN 
     SIZE 26.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_WizName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Wizard Title" 
     VIEW-AS FILL-IN 
     SIZE 52 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-21
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 64.57 BY 13.6.

DEFINE VARIABLE tgl_jump AS LOGICAL INITIAL no 
     LABEL "Show 'Go-To Step' Combo" 
     VIEW-AS TOGGLE-BOX
     SIZE 21.72 BY .95 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-viewers FOR 
      WizardViewer SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-viewers
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-viewers V-table-Win _FREEFORM
  QUERY br-viewers DISPLAY
      StepViewer
    StepHelp
    Sequence
    Mandatory
ENABLE
    StepViewer
    StepHelp
    Sequence
    Mandatory
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 63.43 BY 8.6
         BGCOLOR 15 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_WizName AT ROW 1.2 COL 11 COLON-ALIGNED
     fil_Description AT ROW 2.2 COL 11 COLON-ALIGNED
     fil_ButtonLabel AT ROW 3.2 COL 11 COLON-ALIGNED
     btn_Add AT ROW 3.4 COL 48.43
     btn_Del AT ROW 3.4 COL 57
     br-viewers AT ROW 4.4 COL 1.57
     fil_WizFinish AT ROW 13.2 COL 10.43 COLON-ALIGNED
     tgl_jump AT ROW 13.2 COL 42.72
     RECT-21 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
   Temp-Tables and Buffers:
      TABLE: WizardViewer T "?" NO-UNDO ttpl OfficeSettings
      ADDITIONAL-FIELDS:
          FIELD StepViewer AS CHAR FORMAT 'X(40)' LABEL 'Viewer'
          FIELD StepHelp AS CHAR FORMAT 'X(30)' LABEL 'Help'
          FIELD Sequence AS INT FORMAT '>>9' LABEL 'Seq'
          FIELD Mandatory AS LOGICAL FORMAT 'Yes/No' LABEL 'Mand'
          INDEX x1 IS PRIMARY Sequence StepViewer
          INDEX x2 IS UNIQUE StepViewer
      END-FIELDS.
   END-TABLES.
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 16.3
         WIDTH              = 89.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-viewers btn_Del F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-viewers
/* Query rebuild information for BROWSE br-viewers
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH WizardViewer
        BY WizardViewer.Sequence BY WizardViewer.StepViewer.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-viewers */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/null.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_Add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Add V-table-Win
ON CHOOSE OF btn_Add IN FRAME F-Main /* Add */
DO:
  RUN open-viewer-query( "A" ).
  APPLY 'ENTRY':U TO WizardViewer.StepViewer IN BROWSE {&BROWSE-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_Del
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Del V-table-Win
ON CHOOSE OF btn_Del IN FRAME F-Main /* Del */
DO:
  RUN open-viewer-query( "D" ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-viewers
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR viewer-list AS CHAR NO-UNDO.
DEF VAR no-1 AS CHAR NO-UNDO.
DEF VAR no-2 AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR j AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  fil_WizName       = get-result( "Temp-Wizard-Name" ).
  fil_WizFinish     = get-result( "Temp-Wizard-Finish" ).
  fil_Description   = get-result( "Temp-Description" ).
  fil_ButtonLabel   = get-result( "Temp-ButtonLabel" ).
  tgl_jump          = null-log( get-result( 'Temp-GoTo-Combo' ) = "Yes", Yes ).

  FOR EACH WizardViewer: DELETE WizardViewer. END.
  viewer-list = get-result( "Temp-Wizard-Viewers" ).

  DO i = 1 TO NUM-ENTRIES( viewer-list, "@" ):
    CREATE WizardViewer.
    WizardViewer.StepViewer = ENTRY( 1, ENTRY( i, viewer-list, "@"), "|").
    WizardViewer.StepHelp   = ENTRY( 2, ENTRY( i, viewer-list, "@"), "|").
    WizardViewer.Mandatory  = (SUBSTRING( ENTRY( 3, ENTRY( i, viewer-list, "@"), "|"), 1,1) = "Y").
    Wizardviewer.Sequence   = ?.
  END.

DEF BUFFER v-1 FOR WizardViewer.
DEF BUFFER v-2 FOR WizardViewer.
  viewer-list = get-result( "Temp-Viewer-Links" ).
  DO i = 1 TO NUM-ENTRIES( viewer-list, "@" ):
    no-1  = ENTRY( 1, ENTRY( i, viewer-list, "@"), "|").
    no-2 = ENTRY( 2, ENTRY( i, viewer-list, "@"), "|").
    FIND v-1 WHERE v-1.StepViewer = no-1 NO-ERROR.
    FIND v-2 WHERE v-2.StepViewer = no-2 NO-ERROR.
    IF NOT (AVAILABLE(v-1) AND AVAILABLE(v-2)) THEN DO:
      MESSAGE AVAILABLE(v-1) no-1 SKIP AVAILABLE(v-2) no-2.
      NEXT.
    END.

    IF v-1.Sequence <> ? AND v-2.Sequence <> ? THEN DO:
      IF v-1.Sequence >= v-2.Sequence THEN DO:
        j = v-1.Sequence + 1.
        FOR EACH WizardViewer WHERE WizardViewer.Sequence >= v-2.Sequence
                                AND WizardViewer.Sequence <= v-1.Sequence
                                AND ROWID(WizardViewer) <> ROWID(v-1)
                                BY WizardViewer.Sequence:
          WizardViewer.Sequence = j.
          j = j + 1.
        END.
      END.
    END.
    ELSE IF v-1.Sequence <> ? THEN
      v-2.Sequence = v-1.Sequence + 1 .
    ELSE IF v-2.Sequence <> ? THEN
      v-1.Sequence = v-2.Sequence - 1 .
    ELSE DO:
      FIND LAST WizardViewer WHERE WizardViewer.Sequence <> ? USE-INDEX x2 NO-ERROR.
      v-1.Sequence = (IF AVAILABLE(WizardViewer) THEN (WizardViewer.Sequence + 1) ELSE 1).
      v-2.Sequence = v-1.Sequence + 1.
    END.
  END.

  j = 0.
  FOR EACH WizardViewer WHERE WizardViewer.Sequence <> ? BY WizardViewer.Sequence:
    j = j + 1.
    WizardViewer.Sequence = j.
  END.

  DISPLAY fil_WizName fil_WizFinish tgl_Jump fil_Description fil_ButtonLabel
            WITH FRAME {&FRAME-NAME}.
  RUN open-viewer-query( "" ).
  set-validated( Yes ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-viewer-query V-table-Win 
PROCEDURE open-viewer-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER br-action AS CHAR NO-UNDO.
DEF VAR reposition-row AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  reposition-row = CURRENT-RESULT-ROW("br-viewers").
  IF br-action = "D" THEN DO:
    IF br-viewers:FETCH-SELECTED-ROW(1) THEN .
    DELETE WizardViewer.
  END.

  CLOSE QUERY br-viewers.
  IF br-action = "A" THEN DO:
    CREATE WizardViewer.
    reposition-row = 1.
  END.

  IF reposition-row = ? THEN reposition-row = 1.
  ELSE IF reposition-row > NUM-RESULTS( "br-viewers" ) THEN
    reposition-row = NUM-RESULTS( "br-viewers" ).

  OPEN QUERY br-viewers FOR EACH WizardViewer.
  IF br-viewers:SET-REPOSITIONED-ROW( 2, "CONDITIONAL" ) THEN .
  REPOSITION br-viewers TO ROW reposition-row.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "WizardViewer"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR viewer-list AS CHAR NO-UNDO.
DEF VAR no-1 AS CHAR NO-UNDO INITIAL ?.

DO WITH FRAME {&FRAME-NAME}:
  set-result( "Temp-Wizard-Name",   INPUT FRAME {&FRAME-NAME} fil_WizName ).
  set-result( "Temp-Wizard-Finish", INPUT FRAME {&FRAME-NAME} fil_WizFinish ).
  set-result( "Temp-GoTo-Combo", IF INPUT FRAME {&FRAME-NAME} tgl_jump THEN "Yes" ELSE "No" ).
  set-result( "Temp-Description",   INPUT FRAME {&FRAME-NAME} fil_Description ).
  set-result( "Temp-ButtonLabel",   INPUT FRAME {&FRAME-NAME} fil_ButtonLabel ).

  FOR EACH WizardViewer:
    viewer-list = viewer-list + WizardViewer.StepViewer + "|"
                + WizardViewer.StepHelp + "|"
                + STRING( WizardViewer.Mandatory, "Y/N" ) + "@".
  END.
  set-result( "Temp-Wizard-Viewers", TRIM( viewer-list, "@") ).

  viewer-list = "".
  FOR EACH WizardViewer WHERE WizardViewer.Sequence <> ? BY WizardViewer.Sequence:
    IF no-1 <> ? THEN
      viewer-list = viewer-list + no-1 + "|" + WizardViewer.StepViewer + "@".
    no-1 = WizardViewer.StepViewer.
  END.
  set-result( "Temp-Viewer-Links", TRIM( viewer-list, "@") ).

END.

  RETURN.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


