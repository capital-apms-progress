&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF WORK-TABLE Lease NO-UNDO
  FIELD PropertyCode     LIKE TenancyLease.PropertyCode LABEL "Property"
  FIELD TenancyLeaseCode LIKE TenancyLease.TenancyLeaseCode LABEL "Code"
  FIELD PropertyName     LIKE Property.Name LABEL "Property"
  FIELD TenantCode       LIKE Tenant.TenantCode LABEL "Tnnt  "
  FIELD TenantName       LIKE Tenant.Name LABEL "Tenant" FORMAT "X(30)"
  FIELD Description      LIKE TenancyLease.AreaDescription LABEL "Description" FORMAT "X(40)"
  FIELD LeaseStartDate   LIKE TenancyLease.LeaseStartDate LABEL "Start Date    "
  FIELD LeaseEndDate     LIKE TenancyLease.LeaseendDate  LABEL "End Date    ".
  
DEF VAR break-1 AS CHAR NO-UNDO FORMAT "X" INIT " " LABEL " ".
DEF VAR break-2 AS CHAR NO-UNDO FORMAT "X" INIT " " LABEL " ".
DEF VAR current-lease LIKE TenancyLease.TenancyLeaseCode NO-UNDO INIT ?.
DEF VAR lease-property LIKE TenancyLease.PropertyCode NO-UNDO INIT ?.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-lease

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Lease

/* Definitions for BROWSE br-lease                                      */
&Scoped-define FIELDS-IN-QUERY-br-lease PropertyCode break-1 @ break-1 TenantCode TenantName break-2 @ break-2 Description LeaseStartDate LeaseEndDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-lease   
&Scoped-define FIELD-PAIRS-IN-QUERY-br-lease
&Scoped-define SELF-NAME br-lease
&Scoped-define OPEN-QUERY-br-lease OPEN QUERY {&SELF-NAME} FOR EACH Lease.
&Scoped-define TABLES-IN-QUERY-br-lease Lease
&Scoped-define FIRST-TABLE-IN-QUERY-br-lease Lease


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-lease}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-22 tenant-code btn_tntclear ~
property-code btn_tntclear-2 br-lease 
&Scoped-Define DISPLAYED-OBJECTS tenant-code fil_Tenant property-code ~
fil_Property 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD add-lease V-table-Win 
FUNCTION add-lease RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_tntclear 
     LABEL "Clear" 
     SIZE 5.14 BY 1.15.

DEFINE BUTTON btn_tntclear-2 
     LABEL "Clear" 
     SIZE 5.14 BY 1.15.

DEFINE VARIABLE fil_Property AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 36.57 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_Tenant AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 36.57 BY 1.05 NO-UNDO.

DEFINE VARIABLE property-code AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "Property" 
     VIEW-AS FILL-IN 
     SIZE 5.72 BY 1.05 NO-UNDO.

DEFINE VARIABLE tenant-code AS INTEGER FORMAT "99999":U INITIAL 0 
     LABEL "Tenant" 
     VIEW-AS FILL-IN 
     SIZE 5.72 BY 1.05 NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 88 BY 18.6.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-lease FOR 
      Lease SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-lease
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-lease V-table-Win _FREEFORM
  QUERY br-lease DISPLAY
      PropertyCode
  break-1 @ break-1
  TenantCode
  TenantName
  break-2 @ break-2
  Description
  LeaseStartDate
  LeaseEndDate
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 86.86 BY 11.6
         BGCOLOR 16 FGCOLOR 12 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     tenant-code AT ROW 4 COL 6.43 COLON-ALIGNED
     fil_Tenant AT ROW 4 COL 15 COLON-ALIGNED NO-LABEL
     btn_tntclear AT ROW 4 COL 54.14
     property-code AT ROW 6.4 COL 6.43 COLON-ALIGNED
     fil_Property AT ROW 6.4 COL 15 COLON-ALIGNED NO-LABEL
     btn_tntclear-2 AT ROW 6.4 COL 54.14
     br-lease AT ROW 7.8 COL 1.57
     "Which lease is being changed ?" VIEW-AS TEXT
          SIZE 48 BY 1 AT ROW 1.4 COL 2.14
          FGCOLOR 1 FONT 12
     "If you know the tenant who has the lease then enter it below" VIEW-AS TEXT
          SIZE 41.14 BY 1 AT ROW 2.8 COL 6.72
          FGCOLOR 1 FONT 10
     "OR" VIEW-AS TEXT
          SIZE 4.57 BY 1 AT ROW 5.2 COL 2.72
          FGCOLOR 1 FONT 12
     "If you know the property which the lease is in then enter it below" VIEW-AS TEXT
          SIZE 43.43 BY 1 AT ROW 5.2 COL 7.86
          FGCOLOR 1 FONT 10
     RECT-22 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 20.4
         WIDTH              = 97.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br-lease btn_tntclear-2 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fil_Property IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Tenant IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-lease
/* Query rebuild information for BROWSE br-lease
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH Lease.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-lease */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/method/m-mntvwr.i}
{inc/date.i}
{inc/null.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br-lease
&Scoped-define SELF-NAME br-lease
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-lease V-table-Win
ON VALUE-CHANGED OF br-lease IN FRAME F-Main
DO:
  RUN lease-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_tntclear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_tntclear V-table-Win
ON CHOOSE OF btn_tntclear IN FRAME F-Main /* Clear */
DO:
  ASSIGN tenant-code:SCREEN-VALUE = STRING( 0 )
         fil_tenant:PRIVATE-DATA = ","
         fil_tenant:SCREEN-VALUE = "".
  RUN open-lease-query.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_tntclear-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_tntclear-2 V-table-Win
ON CHOOSE OF btn_tntclear-2 IN FRAME F-Main /* Clear */
DO:
  ASSIGN property-code:SCREEN-VALUE = STRING( 0 )
         fil_property:PRIVATE-DATA = ","
         fil_property:SCREEN-VALUE = "".
  RUN open-lease-query.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U1 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "?" "property-code"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U2 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "?" "property-code"}
  RUN open-lease-query.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U3 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "?" "property-code"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Tenant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U1 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt1.i "?" "tenant-code"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U2 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt2.i "?" "tenant-code"}
  RUN open-lease-query.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Tenant V-table-Win
ON U3 OF fil_Tenant IN FRAME F-Main
DO:
  {inc/selfil/sftnt3.i "?" "tenant-code"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME property-code
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL property-code V-table-Win
ON LEAVE OF property-code IN FRAME F-Main /* Property */
DO:
  IF INPUT {&SELF-NAME} = 0 THEN RETURN.
  {inc/selcde/cdpro.i "fil_Property"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tenant-code
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tenant-code V-table-Win
ON LEAVE OF tenant-code IN FRAME F-Main /* Tenant */
DO:
  IF INPUT {&SELF-NAME} = 0 THEN RETURN.
  {inc/selcde/cdtnt.i "fil_Tenant"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE lease-changed V-table-Win 
PROCEDURE lease-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  current-lease = IF AVAILABLE Lease THEN Lease.TenancyLeaseCode ELSE ?.
  lease-property = IF AVAILABLE Lease THEN Lease.PropertyCode ELSE ?.
  test-validated().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-lease-query V-table-Win 
PROCEDURE open-lease-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR current-rowid AS ROWID NO-UNDO.
  current-rowid = ?.
  
  FOR EACH Lease: DELETE Lease. END.

  IF NOT CAN-FIND( Property WHERE Property.PropertyCode = INPUT property-code ) AND
     NOT CAN-FIND( Tenant   WHERE Tenant.TenantCode     = INPUT tenant-code ) AND
     current-lease = ?
  THEN DO:
    CLOSE QUERY br-lease.
    RETURN.
  END.
  
  /* Create the current lease ( if any ) */
  FIND TenancyLease WHERE TenancyLease.TenancyLeaseCode = current-lease NO-LOCK NO-ERROR.
  IF AVAILABLE TenancyLease THEN DO:
    FIND Property OF TenancyLease NO-LOCK.
    FIND Tenant OF TenancyLease NO-LOCK NO-ERROR.
    add-lease().
    current-rowid = ROWID( Lease ).
  END.
  
  /* Create tenant based leases */
  FIND Tenant WHERE Tenant.TenantCode = INPUT tenant-code NO-LOCK NO-ERROR.
  IF AVAILABLE Tenant THEN
  FOR EACH TenancyLease OF Tenant WHERE Tenancylease.LeaseStatus <> "PAST" NO-LOCK,
    FIRST Property OF tenancyLease NO-LOCK:
    add-lease().
  END.
  
  /* Create property based leases */
  FIND Property WHERE Property.PropertyCode = INPUT property-code NO-LOCK NO-ERROR.
  IF AVAILABLE Property THEN
  FOR EACH TenancyLease OF Property NO-LOCK,
    FIRST Tenant OF TenancyLease NO-LOCK:
    add-lease().  
  END.

  OPEN QUERY br-lease FOR EACH Lease BY Lease.PropertyCode BY Lease.TenantCode.
  IF current-rowid <> ? THEN DO:
    IF br-lease:SET-REPOSITIONED-ROW( 3, "CONDITIONAL" ) THEN.
    REPOSITION br-lease TO ROWID( current-rowid ).
    IF br-lease:SELECT-FOCUSED-ROW() THEN.
  END.
  RUN lease-changed.

END.
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize V-table-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Set up the default values for the wizard */

  set-result( 'Lease-Start', STRING( TODAY ) ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Lease"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF BUFFER Outgoing FOR TenancyOutgoing.

DEF VAR outgoing-result AS CHAR NO-UNDO.
DEF VAR outgoing-line   AS CHAR NO-UNDO.
DEF VAR fd AS CHAR NO-UNDO. /* Field delimiter */
DEF VAR ld AS CHAR NO-UNDO. /* Line delimiter */
fd = '~~'. ld = '~n'.

DO WITH FRAME {&FRAME-NAME}:
  set-result( 'Current-Lease', STRING( current-lease ) ).
  set-result( 'Property', STRING(lease-property) ).

  FIND TenancyLease WHERE TenancyLease.TenancyLeaseCode = current-lease NO-LOCK NO-ERROR.
  IF AVAILABLE(TenancyLease) THEN DO:
    set-result( 'Recovery-Type', TenancyLease.RecoveryType ).
    set-result( 'Outgoings-Budget', STRING(TenancyLease.OutgoingsBudget) ).
    set-result( 'Outgoings-Rate', STRING(TenancyLease.OutgoingsRate) ).


    FOR EACH OutGoing NO-LOCK OF TenancyLease:
      outgoing-line = 
         null-str( STRING( OutGoing.AccountCode ), "0000.00" )    + fd +
         null-str( STRING( OutGoing.Percentage ), "0" )      + fd +
         null-str( STRING( OutGoing.BaseYear ), "?" )        + fd +
         null-str( STRING( OutGoing.BaseYearAmount ), "0" )  + fd +
         null-str( STRING( OutGoing.FixedAmount ), "0" )     + fd +
         null-str( STRING( OutGoing.ReconciliationDue ), "?" ) .

      outgoing-result = outgoing-result + IF outgoing-result = "" THEN "" ELSE ld.
      outgoing-result = outgoing-result + outgoing-line.
    END.

    set-result( 'Outgoings', outgoing-result ).
    set-result( 'Outgoings-Notes', "" ).
  END.

  RETURN test-validated().
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION add-lease V-table-Win 
FUNCTION add-lease RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Add a Lease record
    Notes:  Property, Tenant and TenancyLease must be available.
------------------------------------------------------------------------------*/

  IF CAN-FIND( FIRST Lease WHERE Lease.TenancyLeaseCode = TenancyLease.TenancyLeaseCode )
    THEN RETURN "".
    
  CREATE Lease.
  ASSIGN Lease.PropertyCode     = Property.PropertyCode
         Lease.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
         Lease.PropertyName     = Property.Name
         Lease.TenantCode       = Tenant.TenantCode
         Lease.TenantName       = Tenant.Name
         Lease.Description      = TenancyLease.AreaDescription
         Lease.LeaseStartDate   = TenancyLease.LeaseStartDate
         Lease.LeaseEndDate     = TenancyLease.LeaseEndDate.
         
  
  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF current-lease = ? OR
    NOT CAN-FIND( TenancyLease WHERE TenancyLease.TenancyLeaseCode = current-lease ) THEN
  DO:
    set-validated( No ).
    RETURN "FAIL".
  END.
  
  set-validated( Yes ).
  RETURN "".   /* Function return value. */

END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


