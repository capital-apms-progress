&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR current-lease LIKE TenancyLease.TenancyLeaseCode NO-UNDO INIT ?.
DEF VAR re-lease-years  AS INT NO-UNDO.
DEF VAR re-lease-months AS INT NO-UNDO.
DEF VAR re-lease-days   AS INT NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set.i "Default-ReLease-Term" "Re-Lease-Term"}
IF NOT AVAILABLE(OfficeSetting) THEN Re-Lease-Term = "6y0m0d".

ASSIGN
  re-lease-years  = INT( ENTRY( 1, Re-Lease-Term, "y" ) )
  re-lease-months = INT( ENTRY( 1, ENTRY( 2, Re-Lease-Term, "y" ), "m" ) )
  re-lease-days   = INT( ENTRY( 1, ENTRY( 2, Re-Lease-Term, "m" ), "d" ) )
  NO-ERROR.
IF ERROR-STATUS:ERROR THEN DO:
  MESSAGE "The default re-leasing term: " Re-Lease-Term SKIP
          "is invalid." SKIP(1)
          "It must have the format NNyNNmNNd"
          VIEW-AS ALERT-BOX ERROR TITLE "Invalid Re-Leasing Term".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-22 rs_Variation start-date end-date ~
term-years term-months term-days 
&Scoped-Define DISPLAYED-OBJECTS rs_Variation start-date end-date ~
term-years term-months term-days 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE end-date AS DATE FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 10.14 BY 1.05 NO-UNDO.

DEFINE VARIABLE start-date AS DATE FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 10.14 BY 1.05
     BGCOLOR 16 FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE term-days AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 4 BY 1.05 NO-UNDO.

DEFINE VARIABLE term-months AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 4 BY 1.05 NO-UNDO.

DEFINE VARIABLE term-years AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 4 BY 1.05 NO-UNDO.

DEFINE VARIABLE rs_Variation AS LOGICAL 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Variation of terms / spaces", yes,
"Re-Lease from existing lease", no
     SIZE 22.29 BY 1.6 NO-UNDO.

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 54.29 BY 9.4.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     rs_Variation AT ROW 3.4 COL 32.43 NO-LABEL
     start-date AT ROW 5.6 COL 34.43 COLON-ALIGNED NO-LABEL
     end-date AT ROW 7.4 COL 34.43 COLON-ALIGNED NO-LABEL
     term-years AT ROW 8.8 COL 17.29 COLON-ALIGNED NO-LABEL
     term-months AT ROW 8.8 COL 29.29 COLON-ALIGNED NO-LABEL
     term-days AT ROW 8.8 COL 41.86 COLON-ALIGNED NO-LABEL
     "What is the term for this lease ?" VIEW-AS TEXT
          SIZE 43.43 BY 1 AT ROW 1.4 COL 8.43
          FGCOLOR 1 FONT 12
     "Enter either the lease end date :" VIEW-AS TEXT
          SIZE 23.43 BY 1 AT ROW 7.4 COL 5.57
          FGCOLOR 1 
     "Or the lease term:" VIEW-AS TEXT
          SIZE 12.57 BY 1 AT ROW 8.8 COL 5.57
          FGCOLOR 1 
     RECT-22 AT ROW 1 COL 1
     "What is the start date of the lease ?" VIEW-AS TEXT
          SIZE 25.72 BY 1 AT ROW 5.6 COL 5.57
          FGCOLOR 1 
     "Is this a variation to the existing lease ?" VIEW-AS TEXT
          SIZE 26.86 BY 1 AT ROW 3.6 COL 5.57
          FGCOLOR 1 
     "years" VIEW-AS TEXT
          SIZE 5.14 BY 1 AT ROW 8.8 COL 23.86
     "months" VIEW-AS TEXT
          SIZE 5.14 BY 1 AT ROW 8.8 COL 35.86
     "days" VIEW-AS TEXT
          SIZE 5.14 BY 1 AT ROW 8.8 COL 48.43
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 11.2
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-wizvwr.i}
{inc/date.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME end-date
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL end-date V-table-Win
ON LEAVE OF end-date IN FRAME F-Main
DO:
  RUN dates-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs_Variation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs_Variation V-table-Win
ON VALUE-CHANGED OF rs_Variation IN FRAME F-Main
DO:
  RUN set-default-dates.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME start-date
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL start-date V-table-Win
ON ANY-PRINTABLE OF start-date IN FRAME F-Main
DO:
  IF LAST-EVENT:LABEL = "?" THEN DO:
    BELL.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL start-date V-table-Win
ON LEAVE OF start-date IN FRAME F-Main
DO:
  RUN dates-changed.
  test-validated().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME term-days
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL term-days V-table-Win
ON LEAVE OF term-days IN FRAME F-Main
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  RUN term-changed.
  SELF:MODIFIED = No.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME term-months
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL term-months V-table-Win
ON LEAVE OF term-months IN FRAME F-Main
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  RUN term-changed.
  SELF:MODIFIED = No.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME term-years
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL term-years V-table-Win
ON LEAVE OF term-years IN FRAME F-Main
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  RUN term-changed.
  SELF:MODIFIED = No.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE dates-changed V-table-Win 
PROCEDURE dates-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  ASSIGN FRAME {&FRAME-NAME} start-date end-date.
  
  IF start-date = ? OR end-date = ? THEN DO:
    ASSIGN term-years = 0
           term-months = 0
           term-days = 0.
  END.
  ELSE DO:
    IF start-date > end-date THEN end-date = start-date.
    date-diff( start-date, end-date, 
               OUTPUT term-years, OUTPUT term-months, OUTPUT term-days ).
  END.
  
  DISPLAY term-years term-months term-days end-date WITH FRAME {&FRAME-NAME}.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-step V-table-Win 
PROCEDURE display-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR lease-code LIKE TenancyLease.TenancyLeaseCode NO-UNDO.

  lease-code = INT( get-result( 'Current-Lease' ) ).
  IF lease-code <> ? AND lease-code <> current-lease THEN DO:
    current-lease = lease-code.
    RUN set-default-dates.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartViewer, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-default-dates V-table-Win 
PROCEDURE set-default-dates :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  FIND TenancyLease WHERE TenancyLease.TenancyLeaseCode = current-lease NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(TenancyLease) THEN RETURN.

  start-date = (IF INPUT rs_Variation THEN TenancyLease.LeaseStartDate
                                      ELSE (TenancyLease.LeaseEndDate + 1)).
  IF start-date = ? THEN start-date = TODAY.
  
  end-date = IF INPUT rs_Variation THEN TenancyLease.LeaseEndDate
                                   ELSE add-date( start-date, re-lease-years, re-lease-months, re-lease-days - 1 ).
  IF end-date < start-date THEN end-date = ?.

  DISPLAY start-date end-date WITH FRAME {&FRAME-NAME}.

  RUN dates-changed.
  test-validated().
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE term-changed V-table-Win 
PROCEDURE term-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  ASSIGN FRAME {&FRAME-NAME} term-years term-months term-days.

  IF INPUT start-date = ? THEN
    end-date = ?.
  ELSE IF term-years = 0 AND term-months = 0 AND term-days = 0 THEN
    end-date = ?.
  ELSE
    end-date =  add-date( start-date,
                term-years, term-months, term-days - 1
                     ).
  DISPLAY end-date WITH FRAME {&FRAME-NAME}.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validate-step V-table-Win 
PROCEDURE validate-step :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  set-result( 'Lease-Start', STRING( INPUT start-date ) ).
  set-result( 'Lease-End',   STRING( INPUT end-date ) ).
  set-result( 'Change-Type', (IF INPUT rs_Variation THEN "Variation" ELSE "Re-Lease") ).
  RETURN test-validated().
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION test-validated V-table-Win 
FUNCTION test-validated RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF INPUT start-date = ? THEN DO:
    set-validated( No ).
    RETURN "FAIL".
  END.
  
  set-validated( Yes ).
  RETURN "".   /* Function return value. */

END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


