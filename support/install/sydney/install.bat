echo off
p:
cd \INSTALL

set WINROOT=C:\WINDOWS
if NOT exist %WINROOT%\WIN.COM set WINROOT=C:\WIN95
if exist C:\WIN95\WIN.COM set WINROOT=C:\WIN95

:SaveLast
: Save the previous AUTOEXEC.BAT
if "%TEMP%" == "" move c:\autoexec.bat c:\autoexec.bak
if "%DLC%" == ""  move c:\autoexec.bat c:\autoexec.bak

: Append bits defining TEMP and DLC if needed
if "%TEMP%" == "" copy c:\autoexec.bak + autoexec.end c:\autoexec.bat
if "%DLC%" == ""  copy c:\autoexec.bak + autoexec.dlc c:\autoexec.bat

cd WINDOWS
xcopy * %WINROOT% /s
cd ..\FONTS
FOR %%A IN (*.TTF) DO IF NOT exist %WINROOT%\FONTS\%%A COPY %%A %WINROOT%\FONTS
explorer %WINROOT%\FONTS
