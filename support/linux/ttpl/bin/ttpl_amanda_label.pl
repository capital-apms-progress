#!/usr/bin/perl -w 
#
# Compute the tape label for the day, based on some wierd chart
# worked out by Nick Griffin.
#

use strict;

my %config = (
   "request"  => 'normal',
   "prefix"   => 'BINNEY',
   "style"    => 'month',
   "date"     => 0
);

my $site_status = "up";

# -------------- READ COMMAND LINE --------------
for ( @ARGV ) {
  /\+([0-9-]+)/       && do { $config{"date"}     = $1;                          };
  /--tomorrow/        && do { $config{"date"}     = +1;                          };

  /--week/            && do { $config{"request"}  = 'week';                      };
  /--fortnightly/     && do { $config{"style"}    = '2week';                     };
  /--day/             && do { $config{"request"}  = 'day';                       };
}

#
# We subtract one from the date offset, because the tape is
# normally inserted 'yesterday' when we actually do the dump.
$config{"date"}--;

my $now = time() + (86400 * $config{"date"} );
my ($sec, $min, $hr, $mday, $mon, $year, $wday, $yday, $isdst ) = localtime($now);

my $mweek = int($mday / 7);


#
# We have a schedule that arbitrarily dictates the tape number
# according to the day of week, and week of month.
#
my $weektapes  = [
  [ 0,  1,  5,  6,  7,  8,  0 ],
  [ 0, 11, 12, 13, 14, 15,  0 ],
  [ 0, 16, 17, 18, 19, 20,  0 ],
  [ 0, 25,  9, 10,  2,  3,  0 ],
  [ 0,  4, 21, 22, 23, 24,  0 ]
];

my $fortnightly = [
  [ 0,  1,  2,  3,  4,  5,  0 ],
  [ 0,  6,  7,  8,  9, 10,  0 ]
];

if ( $config{"request"} eq "day" ) {
  printf( "%02d\n", $wday );
}
elsif ( $config{"style"} eq "month" ) {
  if ( $config{"request"} eq "normal" ) {
    printf( "%s%02d\n", $config{"prefix"}, $weektapes->[$mweek][$wday] );
  }
  elsif ( $config{"request"} eq "week" ) {
    printf( "%02d\n", $mweek + 1 );  # More human readable...
  }
}
else {
  $mweek = int( $yday / 7 ) % 2;
  if ( $config{"request"} eq "normal" ) {
    printf( "%s%02d\n", $config{"prefix"}, $fortnightly->[$mweek][$wday] );
  }
  elsif ( $config{"request"} eq "week" ) {
    printf( "%02d\n", $mweek + 1 );
  }
}

