#!/bin/sh

SEND_TO="craig@jackspoint.com,tracey@jackspoint.com"
HOME=/home/apms
MIMETYPE="application/vnd.ms-excel"

CONVERTPATH="`dirname $0`"
CONVERTPROG="${CONVERTPATH}/export-2-promon.pl"

for DB in jacks ; do
  DIR="${HOME}/shared/${DB}/export"
  [ -d "${DIR}" ] || continue
  
  mkdir -p "$DIR/sent"
  cd "$DIR"

  for F in b*.csv ; do
    [ -f "${F}" ] || break
    BNUM="`basename $F .csv`"
    mv -- "${F}" "${DIR}/sent/${F}"
    "$CONVERTPROG" <"${DIR}/sent/${F}" >"${F}"
    mpack -s "APMS Batch $BNUM processed" -c "$MIMETYPE" "${F}" "$SEND_TO"
    rm -- "${F}"
  done
  
done
