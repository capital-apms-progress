#!/usr/bin/perl -w 
#
# Watches the spooler directory for print jobs.
#

use strict;

my $spooldir = shift;

usage(1) unless ( defined($spooldir) );
die "$spooldir must be a directory" unless ( -d $spooldir );

my $logfile = $spooldir . "print.log";
my $spoolfile = $spooldir . "print.list";
my $spooltemp = $spooldir . "print.processing";

while( 1 ) {
  if ( -f $spoolfile && -s $spoolfile > 26 ) {
    rename $spoolfile, $spooltemp;
    open( PFILES, "<", $spooltemp );
    open( LOG, ">>", $logfile );
    while( <PFILES> ) {
      chomp;
      print LOG scalar localtime, ": $_\n";
      my @files = split(/\s+/);
      my $printcommand = "/usr/bin/xpp -l ";
      foreach ( @files ) {
        my $thisfile = sprintf( "$spooldir%s", $_ );
        print LOG scalar localtime, ": File to print >>$thisfile<<\n";
        if ( -s $thisfile ) {
          $printcommand .= " $thisfile";
        }
      }
      print LOG scalar localtime, ": Print command: $printcommand\n";
      system $printcommand;
    }
    close(PFILES);
    close(LOG);
    # unlink $spooltemp;
  }
  sleep 3;
}

sub usage {
  my $exitlevel = shift;
  print STDERR <<EOUSAGE ;

Usage:
  watchspool.pl <spool directory>

Watches the specified directory for APMS spool files, and sends
them to the printer.

EOUSAGE

  exit($exitlevel);
}

