#!/bin/bash

PROPATH=.,trigger
TEMP=/tmp
DLC=/usr/dlc
PROPUSER="BatchQueue"

(
  echo "`date` Stating batch queue processor"
  _progres -b -db /home/apms/data/amtrust/amtrust -ld ttpl -p process/bq-processor.p -T /tmp &
  echo "`date` Started as PID $!"
) >>/tmp/bq.log 2>&1

