@echo off
rem SETLOCAL
set START=%_TIME%

@rem...sInitial settings:0:
set OFFICE=%1
if "%1" == "" set OFFICE=Empty

set DBROOT=D:\DATABASE
set CDROOT=D:\DEV\CODE
set DMROOT=D:\DUMP
@rem...e
echo Working with %DBROOT%\%OFFICE%\TTPL
md %DMROOT%\%OFFICE%
@rem...sDump existing database:0:
Echo Dumping %DBROOT%\%OFFICE%\TTPL
_proutil %DBROOT%\%OFFICE%\TTPL -C truncate bi
prowin32 -b -db %DBROOT%\%OFFICE%\ttpl -TB 31 -TM 31 -p %CDROOT%\process\dumpall.r -1 -RO
@rem...e
@rem...sCreate empty database:0:
echo Creating empty database %DBROOT%\%OFFICE%\TTPL
move %DBROOT%\%OFFICE\TTPL.* %DBROOT%\%OFFICE%\TTPBAK.*
prodb %DBROOT%\%OFFICE%\TTPL empty
copy %DBROOT%\%OFFICE%\TTPL.* %DBROOT%\EMPTY\EMPTY.*
@rem...e

D:
CD %DMROOT%\%OFFICE%
@rem...sLoad database description:0:
Echo Loading database description for %DBROOT%\%OFFICE%\TTPL
set DUMPDIR=%DMROOT%\%OFFICE%
prowin32 -b -db %DBROOT%\%OFFICE%\ttpl -TB 31 -TM 31 -p %CDROOT%\process\one-off\loadall.r -1
@rem...e
@rem...sBulkload data files:0:
echo Bulk loading data files
_proutil %DBROOT%\%OFFICE%\ttpl -C bulkload %DMROOT%\%OFFICE%\all.fd -TB 30 -TM 30
@rem...e

CD %DBROOT%\%OFFICE%
@rem...sRebuild indexes:0:
Echo Rebuilding indexes
_proutil %DBROOT%\%OFFICE%\ttpl -C idxbuild all -TB 30 -TM 30
@rem...e
echo Recreate of database %DBROOT%\%OFFICE%\TTPL complete
echo Started at %START%, finished at %_TIME%
