@echo off
call ..\..\amtrust\code\support\belfry\amtrust\setvars.cmd

@echo on
:startQ
echo Starting batch queue processor
if NOT exist %DBROOT%\%AGASDB%.lk goto :Restart
%DLCBIN%\prowin32.exe -b -db %DBROOT%\%AGASDB% -ld TTPL -d dmy -yy 1980 -p process/bq-processor.p -T %TEMP% -ininame aquagas.ini

IF NOT exist %TEMP%\AGAS-Batch-Queue.LCK goto :DoneQ

:Restart
echo Lock file still exists - recycling
sleep 5
goto :startQ

:DoneQ
dir %TEMP%
pause

exit
