D:
CD D:\capital\amtrust\code\support\belfry\amtrust
call setvars.cmd
call setbkdir.cmd
call dbdir.cmd

md %BKUPROOT%\DATABASE
md %BKUPROOT%\CODE
DIR %BKUPROOT%\DATABASE
DIR %BKUPROOT%\CODE

echo "Starting back-up of databases"
cmd /c date /t
cmd /c time /t

:*** Backup Production database
%DLCBIN%\_mprshut.exe %PRODDB% -C backup online fullprod.bak
ZIP fullprod.zip fullprod.bak
sleep 5
COPY fullprod.zip %BKUPROOT%\DATABASE
echo Error levels returned were: External=%? Internal=%_? System=%_SYSERR


:*** Copy over development database from production
%DLCBIN%\PCCMD.EXE Databases Stop %DEVNM%
sleep 10
XCOPY %PRODDB%.* %DEVDB%.*
DEL %DEVDB%.lg
DEL %DEVDB%.lk
sleep 5
%DLCBIN%\_proutil.exe %DEVDB% -C truncate bi -biblocksize 1 -bi 16 -G 15

DATE /T >>%BKUPROOT%\ATTEMPTS.TXT
TIME /T >>%BKUPROOT%\ATTEMPTS.TXT

set ATTEMPTS=
:StartDev
sleep 5
ECHO Attempting to start development database
%DLCBIN%\PCCMD.EXE Databases Start %DEVNM%
sleep 5
set ATTEMPTS=%ATTEMPTS%Z
IF "%ATTEMPTS%" == "ZZZZZZZZZZ" %DLCBIN%\PCCMD.EXE ProService Start
%DLCBIN%\PCCMD.EXE Databases Status %DEVNM%
IF "%ATTEMPTS%" == "ZZZZZZZZZZZZZZZZZZZZ" GOTO :IGiveUp
IF ERRORLEVEL 1 GOTO :StartDev

GOTO :Successful

:IGiveUp
echo "Error levels returned were: External=%? Internal=%_? System=%_SYSERR" >>%BKUPROOT%\ATTEMPTS.TXT
echo %ATTEMPTS% >>%BKUPROOT%\ATTEMPTS.TXT
DIR $DEVDB%.* >>%BKUPROOT%\ATTEMPTS.TXT

:Successful
cd %PRODCODE%
ZIP -Ru %BKUPROOT%\CODE\PRODCODE.ZIP *
echo Error levels returned were: External=%? Internal=%_? System=%_SYSERR

cd %AGASCODE%
ZIP -Ru %BKUPROOT%\CODE\AGASCODE.ZIP *
echo Error levels returned were: External=%? Internal=%_? System=%_SYSERR


:Completed

DIR %BKUPROOT%\DATABASE
DIR %BKUPROOT%\CODE

echo "Completed back-up of databases"
cmd /c date /t
cmd /c time /t

sleep 10

net use %BKUPROOT% /DELETE

