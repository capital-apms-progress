D:
CD D:\CAPITAL\AMTRUST\code\support\belfry\AMTRUST
call setvars.cmd
call setbkdir.cmd
call dbdir.cmd

echo "Starting tape back-up of databases"
cmd /c date /t
cmd /c time /t

md BACK-ME
MOVE fullprod.bak BACK-ME
MOVE fullagas.bak BACK-ME
MOVE ProService* BACK-ME
MOVE *.CMD BACK-ME
MOVE *.BAT BACK-ME
MOVE *.DAT BACK-ME
MOVE *.PF  BACK-ME
MOVE *.DSN BACK-ME

DEL D:\CAPITAL\TAPE.LOG

NTBACKUP.EXE backup %DBROOT%\BACK-ME %DLC% D:\CAPITAL /v /d "Capital APMS Backup" /hc:on /t normal /e /tape:0 /l "D:\CAPITAL\TAPE.LOG"
echo Error levels returned were: External=%? Internal=%_? System=%_SYSERR

MOVE BACK-ME\* .
rd BACK-ME

NTBACKUP.EXE backup C:\WINNT /a /v /d "WINNT Backup" /hc:on /t normal /e /tape:0 /l "D:\CAPITAL\TAPE.LOG"
echo Error levels returned were: External=%? Internal=%_? System=%_SYSERR
if %? GT 0 goto :BadError
goto :Completed

:BadError
echo "An error occurred backing up to tape"
copy D:\CAPITAL\TAPE.LOG D:\CAPITAL\TAPE-ERR-%_YEAR%-%_MONTH%-%_DAY%-%_HOUR%-%_MINUTE%-%_SECOND%.LOG

:Completed

echo "Completed tape back-up of databases"
cmd /c date /t
cmd /c time /t

sleep 10
