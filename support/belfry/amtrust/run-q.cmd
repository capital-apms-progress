@echo off
:Restart
cd p:\amtrust\code
echo Sleeping for 30 seconds ...
sleep 30

:startQ
call p:\amtrust\code\support\belfry\amtrust\setvars.cmd

date /t
time /t
echo Starting batch queue for %PRODDB% ...
%DLCBIN%\prowin32.exe -b -H bodhi -N tcp -S amtrust-1 -db amtrust -ld TTPL -d dmy -yy 1980 -p process/bq-processor.p -T %TEMP% -ininame amtrust.ini

IF NOT exist %TEMP%\Batch-Queue.LCK goto :DoneQ

:Restart
echo Lock file still exists - recycling
sleep 5
goto :startQ

:DoneQ
dir %TEMP%
pause

exit
