D:
CD D:\PropertyUser\code\support\belfry\sydney
call setvars.cmd
call novell.cmd
call dbdir.cmd

md %NOVELLROOT%\DATABASE
md %NOVELLROOT%\CODE

echo "***************************************************************" >>%BKLOG%
DATE /T >>%BKLOG%
TIME /T >>%BKLOG%

:*** Backup Global Trust database
%DLCBIN%\_mprshut.exe %PRODDB% -C backup online fullprod.bak >>%BKLOG%
PKZIP fullprod.zip fullprod.bak
XCOPY fullprod.zip %NOVELLROOT%\DATABASE


:*** Copy over development database from production
%DLCBIN%\PCCMD.EXE Databases Stop %DEVNM%
XCOPY %PRODDB%.* %DEVDB%.*
DEL %DEVDB%.LK
%DLCBIN%\_proutil.exe %DEVDB% -C truncate bi


:*** Backup Global Trust database
%DLCBIN%\_mprshut.exe %GLBLDB% -C backup online fullglbl.bak >>%BKLOG%
PKZIP fullglbl.zip fullglbl.bak
XCOPY fullglbl.zip %NOVELLROOT%\DATABASE


:*** Backup SEA Island Holdings database
%DLCBIN%\_mprshut.exe %ISLEDB% -C backup online fullisle.bak >>%BKLOG%
PKZIP fullisle.zip fullisle.bak
XCOPY fullisle.zip %NOVELLROOT%\DATABASE


set TRIES=
:StartDev
set TRIES=%TRIES%.
sleep 3
ECHO Attempting to start development database
%DLC%\BIN\PCCMD.EXE Databases Start %DEVNM% >>%BKLOG%
sleep 3
%DLC%\BIN\PCCMD.EXE Databases Status %DEVNM% >>%BKLOG%
IF "%TRIES%" == ".........." GOTO :GiveUp
IF ERRORLEVEL 1 GOTO :StartDev

:GiveUp
XCOPY %PRODCODE% %NOVELLROOT%\CODE /S/H/R/Z/C

:Completed
sleep 10
