if "%DLCBIN%" == "" goto :SetVars

goto :VarsDone

:SetVars
set DLC=D:\DLC.83A
set DLCBIN=%DLC%\BIN
set PATH=%PATH%;%DLCBIN%

set DBROOT=D:\DATABASE
set BKLOG=%DBROOT%\BKUP-DB.LOG

set DEVDB=dev\ttp-dev
set DEVNM=dev-1
set DEVCODE=D:\DEV\CODE

set PRODDB=prod\agp
set PRODNM=prod-2
set PRODCODE=D:\PropertyUser\CODE

set GLBLDB=global\global
set GLBLNM=global-3
set GLBLCODE=D:\PropertyUser\GLOBAL\CODE

set ISLEDB=ISLAND\ISLAND
set ISLENM=island-4
set ISLECODE=D:\PropertyUser\ISLAND\CODE

:VarsDone

