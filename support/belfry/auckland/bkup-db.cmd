D:
CD D:\PropertyUser\code\support\belfry\auckland
call setvars.cmd
call novell.cmd
call dbdir.cmd

md %NOVELLROOT%\DATABASE
md %NOVELLROOT%\CODE

:*** Backup TTP Production database
%DLCBIN%\_mprshut.exe %PRODDB% -C backup online fullprod.bak
PKZIP fullprod.zip fullprod.bak
XCOPY fullprod.zip %NOVELLROOT%\DATABASE
DEL fullprod.bak


:*** Copy over development database from production
%DLCBIN%\PCCMD.EXE Databases Stop %DEVNM%
XCOPY %PRODDB%.* %DEVDB%.*
DEL %DEVDB%.LK
%DLCBIN%\_proutil.exe %DEVDB% -C truncate bi -biblocksize 1 -bi 16 -G 15



:*** Backup SEA (NZ) Holdings database
%DLCBIN%\_mprshut.exe %HLDSDB% -C backup online fullhlds.bak
PKZIP fullhlds.zip fullhlds.bak
XCOPY fullhlds.zip %NOVELLROOT%\DATABASE


:StartDev
set ATTEMPTS=
sleep 3
ECHO Attempting to start development database
%DLC%\BIN\PCCMD.EXE Databases Start %DEVNM%
sleep 3
set ATTEMPTS=%ATTEMPTS%Z
%DLC%\BIN\PCCMD.EXE Databases Status %DEVNM%
if "%ATTEMPTS%" == "ZZZZZZZZZ" GOTO :GiveUp
IF ERRORLEVEL 1 GOTO :StartDev

:GiveUp
XCOPY %PRODCODE% %NOVELLROOT%\CODE /S/H/R/Z/C

sleep 300
net use %NOVELLROOT% /DEL

:Completed
sleep 300
