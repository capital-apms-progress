D:
CD D:\PropertyUser\code\support\belfry\auckland
call setvars.cmd
call novell.cmd
call dbdir.cmd

md %NOVELLROOT%\DATABASE
md %NOVELLROOT%\CODE
set ALTBKUP=E:\AKLD02.BAK
md %ALTBKUP%\DATABASE
md %ALTBKUP%\CODE

%DLCBIN%\PCCMD.EXE Databases Stop %PRODNM%
%DLCBIN%\PCCMD.EXE Databases Stop %DEVNM%

%DLCBIN%\_proutil.exe %PRODDB% -C truncate bi
%DLCBIN%\_dbutil.exe probkup %PRODDB% fullprod.bak
PKZIP fullprod.zip fullprod.bak
XCOPY fullprod.zip %NOVELLROOT%\DATABASE
XCOPY fullprod.zip %ALTBKUP%\DATABASE
XCOPY %PRODDB%.* %DEVDB%.*
XCOPY %PRODCODE% %NOVELLROOT%\CODE /S/H/R/Z/C
XCOPY %PRODCODE% %ALTBKUP%\CODE /S/H/R/Z/C


:StartProd
sleep 3
DEL PROD\*.LK
ECHO Attempting to start production database
%DLC%\BIN\PCCMD.EXE Databases Start %PRODNM%
sleep 3
%DLC%\BIN\PCCMD.EXE Databases Status %PRODNM%
IF ERRORLEVEL 1 GOTO :StartProd

:StartDev
sleep 3
ECHO Attempting to start development database
%DLC%\BIN\PCCMD.EXE Databases Start %DEVNM%
sleep 3
%DLC%\BIN\PCCMD.EXE Databases Status %DEVNM%
IF ERRORLEVEL 1 GOTO :StartDev

:Completed
sleep 10
