c:
cd \database
net use q: \\akld01\vol1 /Y
SET DLC=C:\DLC.81A

%DLC%\BIN\PCCMD.EXE Databases Stop holdings-3

%DLC%\BIN\_proutil.exe holdings\holdings -C truncate bi
%DLC%\BIN\_dbutil.exe probkup holdings\holdings holdings.bak
ZIP holdings.zip holdings.bak
XCOPY holdings.zip Q:\AKLD02.BAK\DATABASE


:StartDB
sleep 3
DEL HOLDINGS\*.LK
ECHO Attempting to start holdings database
%DLC%\BIN\PCCMD.EXE Databases Start holdings-3
sleep 3
%DLC%\BIN\PCCMD.EXE Databases Status holdings-3
IF ERRORLEVEL 1 GOTO :StartDB


:Completed
