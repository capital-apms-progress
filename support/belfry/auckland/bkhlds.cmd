D:
CD D:\PropertyUser\code\support\belfry\auckland
call setvars.cmd
call novell.cmd
call dbdir.cmd

md %NOVELLROOT%\DATABASE
md %NOVELLROOT%\CODE

%DLCBIN%\PCCMD.EXE Databases Stop %HLDSNM%

%DLCBIN%\_proutil.exe %HLDSDB% -C truncate bi
%DLCBIN%\_dbutil.exe probkup %HLDSDB% fullhlds.bak
PKZIP fullhlds.zip fullhlds.bak
XCOPY fullhlds.zip %NOVELLROOT%\DATABASE


:StartHlds
sleep 3
DEL %DBROOT%\%HLDSDB%.LK
ECHO Attempting to start holdings database
%DLC%\BIN\PCCMD.EXE Databases Start %HLDSNM%
sleep 3
%DLC%\BIN\PCCMD.EXE Databases Status %HLDSNM%
IF ERRORLEVEL 1 GOTO :StartHlds

:Completed
sleep 10
