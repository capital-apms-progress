if "%DLCBIN%" == "" goto :SetVars

goto :VarsDone

:SetVars
set DLC=D:\DLC.83A
set DLCBIN=%DLC%\BIN
set PATH=%PATH%;%DLCBIN%

set DBROOT=D:\DATABASE

set DEVDB=dev\ttpl
set DEVNM=dev-1
set DEVCODE=D:\Development\CODE

set PRODDB=prod\ttpl
set PRODNM=prod-2
set PRODCODE=D:\PropertyUser\CODE

set HLDSDB=holdings\holdings
set HLDSNM=hlds-3
set HLDSCODE=D:\PropertyUser\CODE

set NZGPDB=nzgp\nzgp
set NZGPNM=nzgp-4
set NZGPCODE=D:\PropertyUser\CODE

:VarsDone

