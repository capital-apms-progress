@echo off
call support\belfry\auckland\setvars.cmd

:startQ
echo Starting batch queue processor
%DLCBIN%\prowin32.exe -b -db %DBROOT%\%PRODDB% -ld TTPL -d dmy -yy 1980 -p process/bq-processor.p -T %TEMP% -ininame akld02.ini
echo on
IF NOT exist %TEMP%\Batch-Queue.LCK goto :DoneQ

echo Lock file still exists - recycling
goto :startQ

:DoneQ
pause
exit
