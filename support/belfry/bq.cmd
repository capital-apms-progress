@echo off

:QueueManager

echo -db %1
echo -ld %2
echo -ininame %3
echo -T %4

echo %DLC%\BIN\PROWIN32.EXE -b -db %1 -ld %2 -ininame %3 -T %4 -p process/bq-processor.p
%DLC%\BIN\PROWIN32.EXE -b -db %1 -ld %2 -ininame %3 -T %4 -p process/bq-processor.p

IF NOT EXIST %4\Batch-Queue.LCK goto :NoLockFile

echo Lock file %4\Batch-Queue.LCK still present - re-starting queue manager.
sleep 10
goto :QueueManager


:NoLockFile
echo Lock file %4\Batch-Queue.LCK is not present - exiting.
sleep 30
