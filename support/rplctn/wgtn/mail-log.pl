#! /perl/bin/perl
#
# Uploader: upload the "rplctn\dump\log\seq-err.log" file to a (local)
# SMTP server for on-delivery.  Addressees for the message are
# specified in a err-conf.ph file.
#
# Written by Andrew McMillan, 2/5/1998.  All credit to Ewen McNeill, for
# the programs (encoder.pl / uploader.pl) which this is plagiarised from,
# and of course the errors are all mine.
#
# This program looks for the log file which (if it exists) is converted into
# an internet e-mail message and then instantiated as a Mail::Internet object,
# to be delivered with SMTP to the local SMTP server.
#
#---------------------------------------------------------------------------

use 5;

use DirHandle;                      # Read directories (the OO way)
use Mail::Header;
use strict;
use Net::SMTP;                      # Simple Mail Transfer Protocol

require "sendconf.ph";              # Sender configuration information
require "recvconf.ph";              # Receiver configuration information
require "err-conf.ph";              # Error notification configuration

#---------------------------------------------------------------------------

# Subroutines

# SendMessage: expects filename of message to send (and that the file will
#              exist in the outbound directory).
#
# Sends the message to the SMTP server for on-delivery, and if that is successful
# moves the original file into the sent area.
#
# Returns 1 if the file is delivered to the SMTP server, and 0 otherwise.

sub sendmessage
{
  my ($filename) = @_;
  die "No file to send\n" if (! defined($filename));

  if ( ! open(MSG, "<$SendConf::outgoing/$filename") )
  {  
    warn "Unable to open \"$SendConf::outgoing/$filename\" -- skipping file\n";
    return 0;
  }

  # Note: need to specify who to say on the HELO line, because Windows isn't
  # particularly good at figuring it out.
  my $smtp = Net::SMTP->new($SendConf::SMTPserver, 
			    Hello => $SendConf::myhostname);
  if (!defined($smtp))
  { 
    warn "Cannot contact SMTP server: $SendConf::SMTPserver\n";
    warn "Skipping file: $filename\n";
    return 0;
  }

  my $rc;
  
  # Do the envelope stuff
  $rc = $smtp -> mail($SendConf::myemail);
  if (! $rc)
  { warn "Error encountered sending envelope from\n"; }
  else
  {
    $rc = $smtp -> to($SendConf::theiremail);
    if (! $rc) 
    { warn "Error encountered sending envelope to\n"; }
  }

  if (! $rc)
  { 
    warn "Cannot start mail.  Abandoning mail transfer ($filename).\n";
    $smtp->reset();
    $smtp->quit();
    return 0;
  }

  # Send out the body
  $rc = $smtp->data();
  if (! $rc)
  {
    warn "Problems starting message data ($filename).  Abandoning transfer.\n";
    $smtp->reset();
    $smtp->quit();
    return 0;
  }

  while(<MSG>)
  {
    $rc = $smtp->datasend($_);
    if (! $rc)
    { warn "Problems sending data line: $_\n";  
      warn "Attempting to continue.\n";       # XXX -- is this wise?
    }
  }
  $rc = $smtp->dataend();
  if ($rc)
  { # Message was delivered correctly, tidy up.
    close(MSG);
    $smtp->quit();

    unlink("$SendConf::outgoingprocessed/$filename") &&
      warn "Removed older copy of \"$filename\" from processed directory\n";

    rename("$SendConf::outgoing/$filename", 
	   "$SendConf::outgoingprocessed/$filename") ||
      warn "Message transfered correctly, unable to move out of way ($filename)\n";
   
    return 1;
  }
  else
  { # Problems sending the message -- leave it alone.
    close(MSG);
    $smtp->reset();
    $smtp->quit();
    warn "Problems sending message ($filename).  Leaving to try again later.\n";

    return 0;
  }
}



#---------------------------------------------------------------------------
# constructmessage: expects a filename to process (including directory);
#
# At present this simply prepends suitable RFC822 headers and cutesy bits

# Returns 1 if the file is processed correctly, 0 otherwise.

sub constructmessage
{
  my ($pathname) = @_;
  die "Filename undefined" if (!defined($pathname));

  my @fileparts = split( '/', $pathname );
  my $filename = "";
  my $remainder = "";
  ($filename, $remainder) = reverse @fileparts;

  print "Filename to process = ", $pathname, "\n";
  print "Base filename = ", $filename, "\n";

  # Now we have the filename we can start building the message.

  if (open MESSAGE, ">$SendConf::outgoing/$filename")
  {
    # Now we can rock and roll.  We've got an input file and an output file.
    # Let's build the header first.

    my $header = new Mail::Header;
    $header->add("From",    $SendConf::myemail);
    $header->add("To",      $ErrorConf::sendwarnto);
    $header->add("Subject", "Daily log files\!");

    # And spit it out to the file.
    $header->print(\*MESSAGE);
    print MESSAGE "\n";
    print MESSAGE "\n";
    print MESSAGE "Attached is today's processing log file.\n";
    print MESSAGE "\n";
    print MESSAGE "------------------- start of error messages -----------------\n";

    # Now we need to append the file of errors
    if (open FROM, "<$pathname")
    {
      my $filebuf;
      while( read( FROM, $filebuf, 4096)) {
	print MESSAGE $filebuf;
      }
    }

    # Now some trailer stuff.
    print MESSAGE "-------------------- end of error messages ------------------\n";
    print MESSAGE "\n";
    print MESSAGE "\n";

    # And we're done -- clean things up.
    close(FROM);
    close(MESSAGE);
    return 1;
  }
  else
  {
    warn "Could not open output file: ", $SendConf::outgoing, "/", $filename, "\n";
    return 0;    
  }
}


#---------------------------------------------------------------------------

# Mainline

# Check configuration values are set

die "No outgoing directory" if (! defined($SendConf::outgoing));
die "No processed directory" if (! defined($SendConf::outgoingprocessed));
die "No SMTP server" if (! defined($SendConf::SMTPserver));
die "No local email address" if (! defined($SendConf::myemail));
die "No remote email address" if (! defined($SendConf::theiremail));
die "Who am I anyway?" if (! defined($SendConf::myhostname));

my $numprocessed = 0;

if (-f "$ErrorConf::seqlogfile")
{
  constructmessage($ErrorConf::seqlogfile);

  my $dir = new DirHandle $SendConf::outgoing;
  if (defined $dir) 
  {
    print "Scanning directory: ", $SendConf::outgoing, "\n";
    my $file = "";
    while (defined($file = $dir->read))
    {
      if (-f "$SendConf::outgoing/$file")
      { if (sendmessage($file))
	{
	  $numprocessed++;
	  unlink $ErrorConf::seqlogfile;
	}
      }
    }
  }
  else
  {
    warn "Unable to read from directory: ", $SendConf::senddir, "\n";
    print "0 files processed.\n";
    exit 0;
  }
}


if ($numprocessed != 1)
{ print "$numprocessed messages processsed.\n"; }
else
{ print "1 message processed.\n"; }

exit $numprocessed;
