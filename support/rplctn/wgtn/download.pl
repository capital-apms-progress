#! /perl/bin/perl -w
#
# Downloader: download RFC822 format messages from a POP server.
#
# Written by Ewen McNeill, 19/11/1997.
#
# All messages at the POP server are downloaded and stored in files
# named by date, time, and sequence number (intended to give a unique
# filename).  WARNING: filenames will be longer than 8.3.
#
# As each message is successfully downloaded it is marked for deletion.
# If all messages are successfully downloaded a QUIT is done, which will
# delete all the messages marked for deletion; otherwise the connection
# is reset (leaving all messages intact).
#
#---------------------------------------------------------------------------

use 5;

use Net::POP3;                    # Talk nicely to a POP server
use strict; 

require "recvconf.ph";            # Configuration of Receiver

#---------------------------------------------------------------------------

# Mainline

my $seq = 0;

die "No inbound directory" if (! defined($RecvConf::downloaded));
die "No POP server"        if (! defined($RecvConf::POPserver));
die "No POP user"          if (! defined($RecvConf::login));
die "No POP password"      if (! defined($RecvConf::password));

# Connect to the POP server
my $pop = Net::POP3->new($RecvConf::POPserver);

if (! defined($pop))
{
  die "Unable to establish connection to POP server \"$RecvConf::POPserver\": $!\n";
}

# Login to the POP server

my $msgs = $pop -> login($RecvConf::login, $RecvConf::password);

# Cycle through the messages (if any) downloading them

my $i;
my $problems = 0;
for ($i = 1; $i <= $msgs; $i++)
{
  # Figure out a filename to save this message in.
  #
  # WARNING: "long" filename; WARNING: race condition!
  #
  my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
  $year += 1900; $mon++;

  my $filename;
  do    # Scan for a "unique" filename.
  {
    $seq++;
    $filename = sprintf("%04d-%02d-%02d-%02d-%02d-%03d.msg",
                        $year, $mon, $mday, $hour, $min, $seq);
  } while (-r "$RecvConf::downloaded/$filename" ||
           -f "$RecvConf::downloaded/$filename" ||
           -d "$RecvConf::downloaded/$filename");

  if (open(MSG, ">$RecvConf::downloaded/$filename"))
  {
    # Grab a reference to the whole message.
    my $messageref = $pop->get($i);

    # And save it out to our file.
    if (defined($messageref))
    {
      if (print MSG @$messageref)
      { # Message saved away, we can mark it for deletion.
        if (close(MSG))
        { $pop->delete($i) ||
            warn "Error encountered marking message $i for deletion\n";
        }
        else
        { warn "Errors closing file ($filename), message not deleted.\n"; 
          $problems++
        }
      }
      else
      { 
        warn "Eeeek!  Failed to write message $i to $filename\n";
        $problems++;
      }
    }
    else
    { 
      warn "Problems downloading message $i -- skipping\n";
      close(MSG);
      unlink("$RecvConf::downloaded/$filename") ||
        warn "Unable to remove partial message: $filename\n";
    }
  }
  else
  { 
    warn "Unable to open file to save message $i -- skipping ($filename)\n";
  }
}

if ($problems)
{ # Reset connection to try to leave messages on server.
  warn "Problems during download: resetting connection.\n";
  $pop -> reset();
}

# Leave nicely
$pop->quit();

if ($msgs > 0)
{  exit $msgs;  }
else
{  exit 0; }

