@ECHO OFF

c:

cd \temp

SET APMS=C:\APMS
SET PATH=%DLC%\bin;%PATH%
set PROCFG=%DLC%\proserve.cfg

set DB=island
set PORT=6012

_mprosrv.exe -N tcp -S %PORT% -B 500 -d dmy -db %APMS%\db\%DB%\%DB% -ld ttpl

echo.
echo.
echo Server for database %DB% now running
echo.

cd %APMS%\local\server
