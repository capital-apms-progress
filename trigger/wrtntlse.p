&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
TRIGGER PROCEDURE FOR WRITE OF TenancyLease OLD BUFFER OldLease.

/*--------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

{inc/ofc-this.i}
{inc/ofc-set-l.i "Use-Rent-Charges" "use-rent-charges"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .4
         WIDTH              = 40.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

IF TenancyLease.LeaseStatus <> "PAST" THEN
  RUN mark-rental-spaces( "L", ? ).

IF NOT NEW(TenancyLease) THEN DO:
  IF TenancyLease.LeaseStatus  = "PAST" AND OldLease.LeaseStatus <> "PAST" THEN
    RUN check-rent-end-valid.

  RUN make-lease-history.

  IF TenancyLease.LeaseStartDate > OldLease.LeaseStartDate
            AND TenancyLease.LeaseStartDate > OldLease.FirstLeaseStart
            AND TenancyLease.LeaseEndDate <> OldLease.LeaseEndDate
            AND TenancyLease.LeaseStatus <> "PAST" THEN
    RUN re-lease-completed.
  ELSE IF TenancyLease.LeaseEndDate <> OldLease.LeaseEndDate THEN
    RUN lease-end-changed.

END.
ELSE DO:
  RUN schedule-lease-end.
END.

IF TenancyLease.FirstLeaseStart = ? THEN
  TenancyLease.FirstLeaseStart = TenancyLease.LeaseStartDate.

{inc/audit.i "TenancyLease"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-rent-end-valid Procedure 
PROCEDURE check-rent-end-valid :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR rent-end AS DATE NO-UNDO.

  IF use-rent-charges THEN DO:
    FIND FIRST RentChargeLine OF TenancyLease WHERE RentChargeLine.RentChargeLineStatus = "C"
                                                AND RentChargeLine.EndDate = ? NO-LOCK NO-ERROR.
    IF AVAILABLE(RentChargeLine) THEN
      rent-end = ?.
    ELSE DO:
      rent-end = TODAY - 10000.
      FOR EACH RentChargeLine OF TenancyLease NO-LOCK:
        IF RentChargeLine.EndDate > rent-end THEN rent-end = RentChargeLine.EndDate.
      END.
      IF rent-end = (TODAY - 10000) THEN rent-end = today.
    END.
  END.
  ELSE
    rent-end = TenancyLease.RentEndDate.

  IF rent-end = ? THEN DO:
    MESSAGE "Cannot set lease status to 'PAST' without" SKIP
            "Setting an end date for the rental."
            VIEW-AS ALERT-BOX ERROR
            TITLE "Rent End not set".
    RETURN ERROR.
  END.
  RUN mark-rental-spaces( "V", rent-end ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE lease-end-changed Procedure 
PROCEDURE lease-end-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR date-due AS DATE NO-UNDO.
DEF VAR task-no AS INT NO-UNDO.
DEF VAR results AS CHAR NO-UNDO.

  FIND FlowTask WHERE FlowTask.DueDate = OldLease.LeaseEndDate
                  AND FlowTask.EntityType = "T"
                  AND FlowTask.EntityCode = OldLease.TenantCode
                  AND FlowTask.FlowTaskType = "LE" NO-LOCK NO-ERROR.
  IF AVAILABLE(FlowTask) THEN DO:
    date-due = FlowTask.DueDate.
    task-no  = FlowTask.FlowTaskNo.
    IF TenancyLease.LeaseEndDate = ? THEN DO:
      FIND CURRENT FlowTask EXCLUSIVE-LOCK NO-ERROR.
      DELETE FlowTask NO-ERROR.
      FIND CURRENT FlowTask NO-LOCK NO-ERROR.
    END.
    ELSE DO:
      RUN workflow/update-task.p( INPUT-OUTPUT date-due, INPUT-OUTPUT task-no, "MoveTask",
                        STRING(TenancyLease.LeaseEndDate),
                        OUTPUT results ).
    END.
  END.
  ELSE DO:
    date-due = TenancyLease.LeaseEndDate.
    RUN workflow/update-task.p( INPUT-OUTPUT date-due, INPUT-OUTPUT task-no, "NewTask",
                        "T," + STRING(TenancyLease.TenantCode) + ",LE",
                        OUTPUT results ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE make-lease-history Procedure 
PROCEDURE make-lease-history :
/*------------------------------------------------------------------------------
  Purpose:  Check to see if we need to write a LeaseHistory Record
------------------------------------------------------------------------------*/
DEF VAR leases-same AS LOGI NO-UNDO.

  BUFFER-COMPARE OldLease TO TenancyLease SAVE leases-same.

  IF NOT leases-same THEN DO:

    IF NOT CAN-FIND( FIRST LeaseHistory WHERE
          LeaseHistory.TenancyLeaseCode = TenancyLease.TenancyLeaseCode AND
          LeaseHistory.DateChanged = TODAY ) THEN
    DO:
      CREATE LeaseHistory.
      BUFFER-COPY OldLease TO LeaseHistory ASSIGN
        LeaseHistory.DateChanged = TODAY.
      RELEASE LeaseHistory.
    END.

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE mark-rental-spaces Procedure 
PROCEDURE mark-rental-spaces :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-status AS CHAR NO-UNDO.
DEF INPUT PARAMETER new-vacate-date AS DATE NO-UNDO.

  ON WRITE OF TenancyLease OVERRIDE DO: END.
  ON WRITE OF RentalSpace OVERRIDE DO: END.

  FOR EACH RentalSpace OF TenancyLease 
           WHERE RentalSpace.AreaStatus <> new-status EXCLUSIVE-LOCK:
    ASSIGN  RentalSpace.AreaStatus   = new-status
            RentalSpace.VacationDate = new-vacate-date.
  END.

  ON WRITE OF RentalSpace REVERT.
  ON WRITE OF TenancyLease REVERT.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE re-lease-completed Procedure 
PROCEDURE re-lease-completed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR date-due AS DATE NO-UNDO.
DEF VAR task-no AS INT NO-UNDO.
DEF VAR results AS CHAR NO-UNDO.

  FIND FlowTask WHERE FlowTask.DueDate = OldLease.LeaseEndDate
                  AND FlowTask.EntityType = "T"
                  AND FlowTask.EntityCode = OldLease.TenantCode
                  AND FlowTask.FlowTaskType = "LE" NO-LOCK NO-ERROR.
  IF AVAILABLE(FlowTask) THEN DO:
    date-due = FlowTask.DueDate.
    task-no  = FlowTask.FlowTaskNo.
    RUN workflow/update-task.p( INPUT-OUTPUT date-due, INPUT-OUTPUT task-no, "TaskDone",
                        "", OUTPUT results ).
  END.
  ELSE DO:
    /* if the date has changed then it will get fixed in the next screen */
    date-due = OldLease.LeaseEndDate.
    RUN workflow/update-task.p( INPUT-OUTPUT date-due, INPUT-OUTPUT task-no, "NewTask",
                        "T," + STRING(TenancyLease.TenantCode) + ",LE",
                        OUTPUT results ).

    /* might not have been created with the right status on it */
    RUN workflow/update-task.p( INPUT-OUTPUT date-due, INPUT-OUTPUT task-no, "TaskDone",
                        "", OUTPUT results ).
  END.

  date-due = TenancyLease.LeaseEndDate.
  RUN workflow/update-task.p( INPUT-OUTPUT date-due, INPUT-OUTPUT task-no, "NewTask",
                      "T," + STRING(TenancyLease.TenantCode) + ",LE",
                      OUTPUT results ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE schedule-lease-end Procedure 
PROCEDURE schedule-lease-end :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF TenancyLease.LeaseEndDate = ? THEN RETURN.

DEF VAR date-due AS DATE NO-UNDO.
DEF VAR task-no AS INT NO-UNDO.
DEF VAR results AS CHAR NO-UNDO.

  date-due = TenancyLease.LeaseEndDate.
  RUN workflow/update-task.p( INPUT-OUTPUT date-due, INPUT-OUTPUT task-no, "NewTask",
                        "T," + STRING(TenancyLease.TenantCode) + ",LE",
                        OUTPUT results ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


