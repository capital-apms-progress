TRIGGER PROCEDURE FOR CREATE OF Tenant.

DEF VAR tenant-code   LIKE Tenant.TenantCode NO-UNDO.
DEF BUFFER LastTenant FOR Tenant.

FIND LAST LastTenant NO-LOCK NO-ERROR.
IF AVAILABLE(LastTenant) AND LastTenant.TenantCode >= 99999 THEN DO:
  FIND LAST LastTenant WHERE LastTenant.TenantCode < 9990 NO-LOCK NO-ERROR.
  IF AVAILABLE(LastTenant) AND LastTenant.TenantCode = 9989 THEN DO:
    FIND LAST LastTenant WHERE LastTenant.TenantCode < 90000 NO-LOCK NO-ERROR.
    /* sort this one out when we need to */
  END.
END.
tenant-code = IF AVAILABLE LastTenant THEN LastTenant.TenantCode + 1 ELSE 1.

ASSIGN Tenant.TenantCode = tenant-code.
