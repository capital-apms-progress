TRIGGER PROCEDURE FOR ASSIGN OF RentalSpace.PropertyCode.

IF RentalSpace.RentalSpaceCode = 0 THEN
DO:
  DEF BUFFER LastSpace FOR RentalSpace.
  FIND LAST LastSpace WHERE LastSpace.PropertyCode = RentalSpace.PropertyCode NO-LOCK NO-ERROR.
  ASSIGN RentalSpace.RentalSpaceCode = 
    IF AVAILABLE LastSpace THEN LastSpace.RentalSpaceCode + 1 ELSE 1.
END.

/* Only applies if it's leased */
IF RentalSpace.AreaStatus = 'L' THEN DO:
  FIND TenancyLease OF RentalSpace EXCLUSIVE-LOCK.
  TenancyLease.PropertyCode = RentalSpace.PropertyCode.
END.
