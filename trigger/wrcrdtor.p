TRIGGER PROCEDURE FOR WRITE OF Creditor OLD BUFFER OldCr.

IF OldCr.CreditorCode <> 0 AND OldCr.CreditorCode <> Creditor.CreditorCode THEN DO:
  MESSAGE "Creditor codes cannot be re-assigned."
               VIEW-AS ALERT-BOX ERROR TITLE "Error Writing Creditor".
  RETURN ERROR.
END.
IF OldCr.Active AND NOT Creditor.Active THEN DO:
  IF CAN-FIND( FIRST Voucher OF Creditor WHERE Voucher.VoucherStatus = "U"
                                            OR Voucher.VoucherStatus = "A")
  THEN DO:
    MESSAGE "Cannot de-activate a creditor with unpaid vouchers."
                VIEW-AS ALERT-BOX ERROR TITLE "Error Writing Creditor".
    RETURN ERROR.
  END.
  FIND Office WHERE ThisOffice NO-LOCK.
  FIND OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "CREDITORS" NO-LOCK.
  IF CAN-FIND( AccountSummary WHERE AccountSummary.EntityType = "C"
                                AND AccountSummary.EntityCode = Creditor.CreditorCode
                                AND AccountSummary.AccountCode = OfficeControlAccount.AccountCode)
  THEN DO:
    MESSAGE "Cannot de-activate a creditor with non-zero balance."
                VIEW-AS ALERT-BOX ERROR TITLE "Error Writing Creditor".
    RETURN ERROR.
  END.
END.