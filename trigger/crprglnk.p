TRIGGER PROCEDURE FOR CREATE OF ProgramLink.

DO:
  DEF VAR link-code AS INT NO-UNDO.
  
  DEF BUFFER Lastlink FOR ProgramLink.
  FIND LAST LastLink NO-LOCK NO-ERROR.
  
  link-code = IF AVAILABLE LastLink THEN LastLink.LinkCode + 1 ELSE 1.
  
  ASSIGN ProgramLink.LinkCode = link-code.

END.
