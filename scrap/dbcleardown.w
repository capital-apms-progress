
ON DELETE OF accttran OVERRIDE DO: END.
FOR each accttran:
  delete accttran.
END.


ON DELETE OF document OVERRIDE DO: END.
FOR each document:
  delete document.
END.

ON DELETE OF accountbalance OVERRIDE DO: END.
FOR each accountbalance:
  delete accountbalance.
END.

ON DELETE OF voucher OVERRIDE DO: END.
FOR each voucher:
  delete voucher.
END.

ON DELETE OF voucherline OVERRIDE DO: END.
FOR each voucherline:
  delete voucherline.
END.

ON DELETE OF invoice OVERRIDE DO: END.
FOR each invoice:
  delete invoice.
END.

ON DELETE OF invoiceline OVERRIDE DO: END.
FOR each invoiceline:
  delete invoiceline.
END.

/* ON DELETE OF tenant OVERRIDE DO: END.   */
/* FOR each tenant:                        */
/*   delete tenant.                        */
/* END.                                    */
/*                                         */
/* ON DELETE OF creditor OVERRIDE DO: END. */
/* FOR each creditor:                      */
/*   delete creditor.                      */
/* END.                                    */

/*
ON DELETE OF note OVERRIDE DO: END.   
FOR each note:
  delete note.
END.
 */

ON DELETE OF cheque OVERRIDE DO: END.
FOR each cheque:
  delete cheque.
END.

/* ON DELETE OF contact OVERRIDE DO: END.      */
/* FOR each contact:                           */
/*   delete contact.                           */
/* END.                                        */
/*                                             */
/* ON DELETE OF person OVERRIDE DO: END.       */
/* FOR each person:                            */
/*   delete person.                            */
/* END.                                        */
/*                                             */
/* ON DELETE OF postaldetail OVERRIDE DO: END. */
/* FOR each postaldetail:                      */
/*   delete postaldetail.                      */
/* END.                                        */
/*                                             */
/* ON DELETE OF phonedetail OVERRIDE DO: END.  */
/* FOR each phonedetail:                       */
/*   delete phonedetail.                       */
/* END.                                        */

ON DELETE OF accountsummary OVERRIDE DO: END.
FOR each accountsummary:
  delete accountsummary.
END.

ON DELETE OF closinggroup OVERRIDE DO: END.
FOR each closinggroup:
  delete closinggroup.
END.

ON DELETE OF periodicdetail OVERRIDE DO: END.
FOR each periodicdetail:
  delete periodicdetail.
END.

/* ON DELETE OF approver OVERRIDE DO: END. */
/* FOR each approver:                      */
/*   delete approver.                      */
/* END.                                    */
/*                                         */

ON DELETE OF invoiceline OVERRIDE DO: END.
FOR each invoiceline:
  delete invoiceline.
END.

/* ON DELETE OF bankaccount OVERRIDE DO: END. */
/* FOR each bankaccount:                      */
/*   delete bankaccount.                      */
/* END.                                       */

/* ON DELETE OF bankimportexception OVERRIDE DO: END. */
/* FOR each bankimportexception:                      */
/*   delete bankimportexception.                      */
/* END.                                               */

ON DELETE OF BATCH OVERRIDE DO: END.
FOR each batch:
  delete batch.
END.

/* ON DELETE OF company OVERRIDE DO: END.      */
/* FOR each company:                           */
/*   delete company.                           */
/* END.                                        */
/*                                             */
/* ON DELETE OF contract OVERRIDE DO: END.     */
/* FOR each contract:                          */
/*   delete contract.                          */
/* END.                                        */
/*                                             */
/* ON DELETE OF directorship OVERRIDE DO: END. */
/* FOR each directorship:                      */
/*   delete directorship.                      */
/* END.                                        */
/*                                             */
/* ON DELETE OF guarantor OVERRIDE DO: END.    */
/* FOR each guarantor:                         */
/*   delete guarantor.                         */
/* END.                                        */

ON DELETE OF leasehistory OVERRIDE DO: END.
FOR each leasehistory:
  delete leasehistory.
END.

ON DELETE OF newaccttrans OVERRIDE DO: END.
FOR each newaccttrans:
  delete newaccttrans.
END.

ON DELETE OF newdocument OVERRIDE DO: END.
FOR each newdocument:
  delete newdocument.
END.

ON DELETE OF newbatch OVERRIDE DO: END.
FOR each newbatch:
  delete newbatch.
END.

ON DELETE OF order OVERRIDE DO: END.
FOR each order:
  delete order.
END.

/* ON DELETE OF outgoingdescription OVERRIDE DO: END. */
/* FOR each outgoingdescription:                      */
/*   delete outgoingdescription.                      */
/* END.                                               */
/*                                                    */
/* ON DELETE OF outgoingsbasis OVERRIDE DO: END.      */
/* FOR each outgoingsbasis:                           */
/*   delete outgoingsbasis.                           */
/* END.                                               */
/*                                                    */

ON DELETE OF project OVERRIDE DO: END.
FOR each project:
  delete project.
END.

ON DELETE OF projectbudget OVERRIDE DO: END.
FOR each projectbudget:
  delete projectbudget.
END.

/* ON DELETE OF property OVERRIDE DO: END.          */
/* FOR each property:                               */
/*   delete property.                               */
/* END.                                             */
/*                                                  */
/* ON DELETE OF propertyoutgoing OVERRIDE DO: END.  */
/* FOR each propertyoutgoing:                       */
/*   delete propertyoutgoing.                       */
/* END.                                             */
/*                                                  */
/* ON DELETE OF propertytitle OVERRIDE DO: END.     */
/* FOR each propertytitle:                          */
/*   delete propertytitle.                          */
/* END.                                             */
/*                                                  */
/* ON DELETE OF propertyview OVERRIDE DO: END.      */
/* FOR each propertyview:                           */
/*   delete propertyview.                           */
/* END.                                             */
/*                                                  */
/* ON DELETE OF propforecast OVERRIDE DO: END.      */
/* FOR each propforecast:                           */
/*   delete propforecast.                           */
/* END.                                             */
/*                                                  */
/* ON DELETE OF region OVERRIDE DO: END.            */
/* FOR each region:                                 */
/*   delete region.                                 */
/* END.                                             */
/*                                                  */
/* ON DELETE OF rentreview OVERRIDE DO: END.        */
/* FOR each rentreview:                             */
/*   delete rentreview.                             */
/* END.                                             */
/*                                                  */
/* ON DELETE OF rentalspace OVERRIDE DO: END.       */
/* FOR each rentalspace:                            */
/*   delete rentalspace.                            */
/* END.                                             */
/*                                                  */
/* ON DELETE OF rentspacehistory OVERRIDE DO: END.  */
/* FOR each rentspacehistory:                       */
/*   delete rentspacehistory.                       */
/* END.                                             */
/*                                   */
/* ON DELETE OF rp OVERRIDE DO: END. */
/*  FOR each rp:                     */
/*    delete rp.                     */
/*  END.                             */
/*                                   */
/* ON DELETE OF scenario OVERRIDE DO: END.          */
/* FOR each scenario:                               */
/*   delete scenario.                               */
/* END.                                             */
/*                                                  */
/* ON DELETE OF scenarioparameter OVERRIDE DO: END. */
/* FOR each scenarioparameter:                      */
/*   delete scenarioparameter.                      */
/* END.                                             */
/*                                                  */
/* ON DELETE OF shareholder OVERRIDE DO: END.       */
/* FOR each shareholder:                            */
/*   delete shareholder.                            */
/* END.                                             */
/*                                                  */
/* ON DELETE OF streetfrontage OVERRIDE DO: END.    */
/* FOR each streetfrontage:                         */
/*   delete streetfrontage.                         */
/* END.                                             */
/*                                                  */
/* ON DELETE OF sublease OVERRIDE DO: END.          */
/* FOR each sublease:                               */
/*   delete sublease.                               */
/* END.                                             */
/*                                                  */
/* ON DELETE OF supplymeter OVERRIDE DO: END.       */
/* FOR each supplymeter:                            */
/*   delete supplymeter.                            */
/* END.                                             */
/*                                                  */
/* ON DELETE OF tenancylease OVERRIDE DO: END.      */
/* FOR each tenancylease:                           */
/*   delete tenancylease.                           */
/* END.                                             */
/*                                                  */
/* ON DELETE OF tenancyoutgoing OVERRIDE DO: END.   */
/* FOR each tenancyoutgoing:                        */
/*   delete tenancyoutgoing.                        */
/* END.                                             */
/*                                                  */
/* ON DELETE OF valuation OVERRIDE DO: END.         */
/* FOR each valuation:                              */
/*   delete valuation.                              */
/* END.                                             */

ON DELETE OF variation OVERRIDE DO: END.
FOR each variation:
  delete variation.
END.

ON DELETE OF variationflow OVERRIDE DO: END.
FOR each variationflow:
  delete variationflow.
END.

/* ON DELETE OF persondetail OVERRIDE DO: END. */
/* FOR each persondetail:                           */
/*   delete persondetail.                           */
/* END.                                             */

ON DELETE OF audittrail OVERRIDE DO: END.
FOR each audittrail:
  delete audittrail.
END.





































