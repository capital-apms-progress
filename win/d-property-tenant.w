&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

DEF INPUT-OUTPUT PARAMETER property-code AS INT NO-UNDO.
DEF OUTPUT PARAMETER tenant-code AS INT NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE DIALOG-BOX

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME br_Tenants

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Tenant

/* Definitions for BROWSE br_Tenants                                    */
&Scoped-define FIELDS-IN-QUERY-br_Tenants Tenant.TenantCode Tenant.Active ~
Tenant.Name 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_Tenants 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_Tenants
&Scoped-define OPEN-QUERY-br_Tenants OPEN QUERY br_Tenants FOR EACH Tenant ~
      WHERE Tenant.Active = tgl_Active AND ~
Tenant.EntityType = "P" AND Tenant.EntityCode = property-code NO-LOCK ~
    BY Tenant.Active ~
       BY Tenant.EntityType ~
        BY Tenant.EntityCode ~
         BY Tenant.Name.
&Scoped-define TABLES-IN-QUERY-br_Tenants Tenant
&Scoped-define FIRST-TABLE-IN-QUERY-br_Tenants Tenant


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-br_Tenants}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_Tenants Btn_OK Btn_Cancel tgl_Active 
&Scoped-Define DISPLAYED-OBJECTS tgl_Active 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "&Cancel" 
     SIZE 15 BY 1.15
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "&OK" 
     SIZE 15 BY 1.15
     BGCOLOR 8 .

DEFINE VARIABLE tgl_Active AS LOGICAL INITIAL yes 
     LABEL "Active Tenants" 
     VIEW-AS TOGGLE-BOX
     SIZE 16.57 BY .8 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_Tenants FOR 
      Tenant SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_Tenants
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_Tenants Dialog-Frame _STRUCTURED
  QUERY br_Tenants NO-LOCK DISPLAY
      Tenant.TenantCode COLUMN-LABEL "Tenant"
      Tenant.Active
      Tenant.Name FORMAT "X(70)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SIZE 63.43 BY 12.2
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     br_Tenants AT ROW 1.4 COL 1.57
     Btn_OK AT ROW 13.8 COL 1.57
     Btn_Cancel AT ROW 13.8 COL 17.57
     tgl_Active AT ROW 14 COL 36.43
     SPACE(12.42) SKIP(0.29)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 10
         TITLE "Find"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: DIALOG-BOX
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
                                                                        */
/* BROWSE-TAB br_Tenants 1 Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_Tenants
/* Query rebuild information for BROWSE br_Tenants
     _TblList          = "TTPL.Tenant"
     _Options          = "NO-LOCK"
     _OrdList          = "TTPL.Tenant.Active|yes,TTPL.Tenant.EntityType|yes,TTPL.Tenant.EntityCode|yes,TTPL.Tenant.Name|yes"
     _Where[1]         = "Tenant.Active = tgl_Active AND
Tenant.EntityType = ""P"" AND Tenant.EntityCode = property-code"
     _FldNameList[1]   > TTPL.Tenant.TenantCode
"Tenant.TenantCode" "Tenant" ? "integer" ? ? ? ? ? ? no ?
     _FldNameList[2]   = TTPL.Tenant.Active
     _FldNameList[3]   > TTPL.Tenant.Name
"Tenant.Name" ? "X(70)" "character" ? ? ? ? ? ? no ?
     _Query            is OPENED
*/  /* BROWSE br_Tenants */
&ANALYZE-RESUME

 




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Find */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br_Tenants
&Scoped-define SELF-NAME br_Tenants
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_Tenants Dialog-Frame
ON MOUSE-SELECT-DBLCLICK OF br_Tenants IN FRAME Dialog-Frame
DO:
  APPLY 'CHOOSE':U TO Btn_OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  APPLY 'GO':U TO FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  RUN get-values.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_Active
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_Active Dialog-Frame
ON VALUE-CHANGED OF tgl_Active IN FRAME Dialog-Frame /* Active Tenants */
DO:
  ASSIGN FRAME {&FRAME-NAME} tgl_Active .
  RUN open-tenant-query.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

FIND Property WHERE Property.PropertyCode = property-code NO-LOCK.
DO WITH FRAME {&FRAME-NAME}:
  tgl_Active = Property.Active.
  DISPLAY tgl_Active.
  RUN open-tenant-query.
  DISPLAY {&ENABLED-FIELDS}.
END.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tgl_Active 
      WITH FRAME Dialog-Frame.
  ENABLE br_Tenants Btn_OK Btn_Cancel tgl_Active 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-values Dialog-Frame 
PROCEDURE get-values :
/*------------------------------------------------------------------------------
  Purpose:  Get current values and set parameters from them
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  ASSIGN {&ENABLED-FIELDS}.

  IF NOT AVAILABLE(Tenant) THEN RETURN.
  tenant-code = Tenant.TenantCode .

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-tenant-query Dialog-Frame 
PROCEDURE open-tenant-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  {&OPEN-QUERY-{&BROWSE-NAME}}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


