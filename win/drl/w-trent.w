&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS W-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR add-new-doc   AS LOGI INIT No NO-UNDO.
DEF VAR add-new-trn   AS LOGI INIT No NO-UNDO.
DEF VAR last-doc-type AS CHAR NO-UNDO.
DEF VAR last-trn-type AS CHAR NO-UNDO.
DEF VAR trn-modified  AS LOGI NO-UNDO.
DEF VAR doc-modified  AS LOGI NO-UNDO.

DEF VAR last-doc-row  AS LOGI NO-UNDO.
DEF VAR last-trn-row  AS LOGI NO-UNDO.

DEFINE VARIABLE coa-list AS HANDLE NO-UNDO.
DEFINE VARIABLE ecode-list AS HANDLE NO-UNDO.
DEFINE VARIABLE selecting-code  AS LOGICAL INITIAL FALSE NO-UNDO.
DEFINE VARIABLE selecting-ecode AS LOGICAL INITIAL FALSE NO-UNDO.

DEF VAR sundry-debtors   LIKE ChartOfAccount.AccountCode NO-UNDO.
DEF VAR sundry-creditors LIKE ChartOfAccount.AccountCode NO-UNDO.

DEF WORK-TABLE DefTrans NO-UNDO LIKE NewAcctTrans.

DEF VAR have-records AS LOGI INIT No NO-UNDO.
DEF VAR trn-inserted AS LOGI INIT No NO-UNDO.
DEF VAR trn-before   LIKE NewAcctTrans.TransactionCode.
  /* This is used to re-number transactions when one is inserted */

{inc/ofc-this.i}
{inc/ofc-set-l.i "Restrict-Project-Posting" "restrict-project-posting"}
{inc/ofc-set-l.i "Tenant-Accounts" "tenant-accounts"}
{inc/ofc-set-l.i "Multi-Ledger-Creditors" "multi-ledger-creditors"}
IF tenant-accounts <> Yes THEN tenant-accounts = No.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-doc

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES NewBatch
&Scoped-define FIRST-EXTERNAL-TABLE NewBatch


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR NewBatch.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES NewDocument NewAcctTrans

/* Definitions for BROWSE br-doc                                        */
&Scoped-define FIELDS-IN-QUERY-br-doc NewDocument.DocumentType ~
NewDocument.Reference NewDocument.Description 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-doc NewDocument.DocumentType ~
NewDocument.Reference NewDocument.Description 
&Scoped-define ENABLED-TABLES-IN-QUERY-br-doc NewDocument
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br-doc NewDocument
&Scoped-define QUERY-STRING-br-doc FOR EACH NewDocument OF NewBatch NO-LOCK
&Scoped-define OPEN-QUERY-br-doc OPEN QUERY br-doc FOR EACH NewDocument OF NewBatch NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br-doc NewDocument
&Scoped-define FIRST-TABLE-IN-QUERY-br-doc NewDocument


/* Definitions for BROWSE br-trn                                        */
&Scoped-define FIELDS-IN-QUERY-br-trn NewAcctTrans.EntityType ~
NewAcctTrans.EntityCode NewAcctTrans.AccountCode NewAcctTrans.Date ~
NewAcctTrans.Amount NewAcctTrans.Reference NewAcctTrans.Description 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-trn NewAcctTrans.EntityType ~
NewAcctTrans.EntityCode NewAcctTrans.AccountCode NewAcctTrans.Date ~
NewAcctTrans.Amount NewAcctTrans.Reference NewAcctTrans.Description 
&Scoped-define ENABLED-TABLES-IN-QUERY-br-trn NewAcctTrans
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br-trn NewAcctTrans
&Scoped-define QUERY-STRING-br-trn FOR EACH NewAcctTrans OF NewDocument NO-LOCK
&Scoped-define OPEN-QUERY-br-trn OPEN QUERY br-trn FOR EACH NewAcctTrans OF NewDocument NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br-trn NewAcctTrans
&Scoped-define FIRST-TABLE-IN-QUERY-br-trn NewAcctTrans


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br-doc btn_add-doc Btn_Add-Trn btn_Ins-Trn ~
br-trn fil_EntityType 
&Scoped-Define DISPLAYED-FIELDS NewBatch.Total 
&Scoped-define DISPLAYED-TABLES NewBatch
&Scoped-define FIRST-DISPLAYED-TABLE NewBatch
&Scoped-Define DISPLAYED-OBJECTS fil_Entity fil_Account fil_EntityType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR W-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add-doc 
     LABEL "Add document" 
     SIZE 15 BY 1.15.

DEFINE BUTTON Btn_Add-Trn 
     LABEL "&Add transaction" 
     SIZE 15 BY 1.15
     BGCOLOR 8 .

DEFINE BUTTON btn_Ins-Trn 
     LABEL "Insert transaction" 
     SIZE 15 BY 1.15.

DEFINE VARIABLE fil_Account AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 61.72 BY .8
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Entity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 61.72 BY .8
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fil_EntityType AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 9.72 BY .8 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-doc FOR 
      NewDocument SCROLLING.

DEFINE QUERY br-trn FOR 
      NewAcctTrans SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-doc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-doc W-Win _STRUCTURED
  QUERY br-doc NO-LOCK DISPLAY
      NewDocument.DocumentType COLUMN-LABEL "Type" FORMAT "X(4)":U
            WIDTH 4
      NewDocument.Reference FORMAT "X(12)":U WIDTH 12
      NewDocument.Description FORMAT "X(50)":U WIDTH 50
  ENABLE
      NewDocument.DocumentType
      NewDocument.Reference
      NewDocument.Description
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 71 BY 7.6
         BGCOLOR 16 FONT 10.

DEFINE BROWSE br-trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-trn W-Win _STRUCTURED
  QUERY br-trn NO-LOCK DISPLAY
      NewAcctTrans.EntityType FORMAT "X":U COLUMN-FONT 10
      NewAcctTrans.EntityCode FORMAT "99999":U WIDTH 5
      NewAcctTrans.AccountCode FORMAT "9999.99":U WIDTH 7
      NewAcctTrans.Date FORMAT "99/99/9999":U WIDTH 10
      NewAcctTrans.Amount FORMAT "->>>,>>>,>>9.99":U WIDTH 15
      NewAcctTrans.Reference COLUMN-LABEL "Reference" FORMAT "X(12)":U
            WIDTH 12
      NewAcctTrans.Description FORMAT "X(50)":U WIDTH 50
  ENABLE
      NewAcctTrans.EntityType
      NewAcctTrans.EntityCode
      NewAcctTrans.AccountCode
      NewAcctTrans.Date
      NewAcctTrans.Amount
      NewAcctTrans.Reference
      NewAcctTrans.Description
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 108 BY 11.75
         BGCOLOR 16 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br-doc AT ROW 1.2 COL 1.57
     btn_add-doc AT ROW 1.6 COL 74.72
     Btn_Add-Trn AT ROW 3.4 COL 74.72
     btn_Ins-Trn AT ROW 3.4 COL 92.43
     fil_Entity AT ROW 8.8 COL 9.29 COLON-ALIGNED NO-LABEL
     NewBatch.Total AT ROW 9.5 COL 91 COLON-ALIGNED NO-LABEL FORMAT "-ZZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 16.57 BY .8
          FONT 11
     fil_Account AT ROW 9.6 COL 9.29 COLON-ALIGNED NO-LABEL
     br-trn AT ROW 10.4 COL 1.57
     fil_EntityType AT ROW 8.8 COL 1.57 NO-LABEL
     "Account :" VIEW-AS TEXT
          SIZE 7.43 BY .8 AT ROW 9.6 COL 1.57
          FONT 10
     "Batch Total :" VIEW-AS TEXT
          SIZE 8.57 BY .8 AT ROW 9.5 COL 84
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   External Tables: TTPL.NewBatch
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW W-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Transaction Entry"
         HEIGHT             = 21.35
         WIDTH              = 109
         MAX-HEIGHT         = 37.8
         MAX-WIDTH          = 140.29
         VIRTUAL-HEIGHT     = 37.8
         VIRTUAL-WIDTH      = 140.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB W-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{inc/method/m-drlwin.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW W-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   Size-to-Fit                                                          */
/* BROWSE-TAB br-doc 1 F-Main */
/* BROWSE-TAB br-trn fil_Account F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE.

ASSIGN 
       br-trn:NUM-LOCKED-COLUMNS IN FRAME F-Main     = 1.

ASSIGN 
       btn_add-doc:HIDDEN IN FRAME F-Main           = TRUE.

ASSIGN 
       Btn_Add-Trn:HIDDEN IN FRAME F-Main           = TRUE.

ASSIGN 
       btn_Ins-Trn:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN fil_Account IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Entity IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_EntityType IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN NewBatch.Total IN FRAME F-Main
   NO-ENABLE EXP-LABEL EXP-FORMAT                                       */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
THEN W-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-doc
/* Query rebuild information for BROWSE br-doc
     _TblList          = "ttpl.NewDocument OF ttpl.NewBatch"
     _Options          = "NO-LOCK"
     _FldNameList[1]   > ttpl.NewDocument.DocumentType
"NewDocument.DocumentType" "Type" ? "character" ? ? ? ? ? ? yes "" no no "4" yes no no "U" "" ""
     _FldNameList[2]   > ttpl.NewDocument.Reference
"NewDocument.Reference" ? ? "character" ? ? ? ? ? ? yes ? no no "12" yes no no "U" "" ""
     _FldNameList[3]   > ttpl.NewDocument.Description
"NewDocument.Description" ? ? "character" ? ? ? ? ? ? yes ? no no "50" yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br-doc */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-trn
/* Query rebuild information for BROWSE br-trn
     _TblList          = "ttpl.NewAcctTrans OF ttpl.NewDocument"
     _Options          = "NO-LOCK"
     _FldNameList[1]   > ttpl.NewAcctTrans.EntityType
"NewAcctTrans.EntityType" ? ? "character" ? ? 10 ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > ttpl.NewAcctTrans.EntityCode
"NewAcctTrans.EntityCode" ? ? "integer" ? ? ? ? ? ? yes ? no no "5" yes no no "U" "" ""
     _FldNameList[3]   > ttpl.NewAcctTrans.AccountCode
"NewAcctTrans.AccountCode" ? ? "decimal" ? ? ? ? ? ? yes ? no no "7" yes no no "U" "" ""
     _FldNameList[4]   > ttpl.NewAcctTrans.Date
"NewAcctTrans.Date" ? ? "date" ? ? ? ? ? ? yes ? no no "10" yes no no "U" "" ""
     _FldNameList[5]   > ttpl.NewAcctTrans.Amount
"NewAcctTrans.Amount" ? "->>>,>>>,>>9.99" "decimal" ? ? ? ? ? ? yes ? no no "15" yes no no "U" "" ""
     _FldNameList[6]   > ttpl.NewAcctTrans.Reference
"NewAcctTrans.Reference" "Reference" "X(12)" "character" ? ? ? ? ? ? yes ? no no "12" yes no no "U" "" ""
     _FldNameList[7]   > ttpl.NewAcctTrans.Description
"NewAcctTrans.Description" ? "X(50)" "character" ? ? ? ? ? ? yes ? no no "50" yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br-trn */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON END-ERROR OF W-Win /* Transaction Entry */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON WINDOW-CLOSE OF W-Win /* Transaction Entry */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
     
  IF trn-modified THEN
  DO:
    RUN verify-trn.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
  END.
     
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main W-Win
ON RETURN OF FRAME F-Main
ANYWHERE DO:
  IF FOCUS:TYPE = 'FILL-IN':U THEN DO:
    APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-doc
&Scoped-define SELF-NAME br-doc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-doc W-Win
ON CTRL-DEL OF br-doc IN FRAME F-Main
OR 'SHIFT-DEL':U OF {&SELF-NAME} ANYWHERE DO:
  RUN delete-doc.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-doc W-Win
ON ENTRY OF br-doc IN FRAME F-Main
OR 'MOUSE-SELECT-DOWN':U OF {&SELF-NAME}
OR 'MOUSE-SELECT-CLICK':U OF {&SELF-NAME} DO:

  IF LOOKUP( LAST-EVENT:LABEL, 'ENTER,TAB,MOUSE-SELECT-UP,MOUSE-SELECT-DOWN':U ) = 0 THEN RETURN.
  IF NOT AVAILABLE NewBatch THEN RETURN.
    
  IF NOT ( NUM-RESULTS("br-doc":U) = ? OR
           NUM-RESULTS("br-doc":U) = 0 ) THEN
    IF br-doc:SELECT-FOCUSED-ROW() THEN.

  IF NUM-RESULTS("br-doc":U) = 0 OR NUM-RESULTS( "br-doc" ) = ? THEN DO:
    RUN add-new-doc.
    RETURN NO-APPLY.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-doc W-Win
ON ROW-DISPLAY OF br-doc IN FRAME F-Main
DO:
  RUN set-doc-color.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-doc W-Win
ON ROW-ENTRY OF br-doc IN FRAME F-Main
DO:
  RUN check-last-doc.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-doc W-Win
ON ROW-LEAVE OF br-doc IN FRAME F-Main
OR 'CURSOR-DOWN':U OF {&SELF-NAME} 
OR 'CURSOR-UP':U   OF {&SELF-NAME} ANYWHERE DO:

  IF doc-modified THEN  DO:
    RUN verify-doc.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
  END.

  IF LOOKUP( LAST-EVENT:FUNCTION, "ROW-LEAVE,RETURN" ) <> 0 THEN
    RUN display-doc.
    
  IF LAST-EVENT:FUNCTION = 'ROW-LEAVE':U AND
     LAST-EVENT:LABEL = 'TAB':U THEN
  DO:
    IF last-doc-row THEN APPLY 'CHOOSE':U TO btn_Add-Doc. ELSE APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-doc W-Win
ON VALUE-CHANGED OF br-doc IN FRAME F-Main
DO:
  RUN open-trn-query.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewDocument.DocumentType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewDocument.DocumentType br-doc _BROWSE-COLUMN W-Win
ON GO OF NewDocument.DocumentType IN BROWSE br-doc /* Type */
DO:
  RUN next-doc-type.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewDocument.DocumentType br-doc _BROWSE-COLUMN W-Win
ON LEAVE OF NewDocument.DocumentType IN BROWSE br-doc /* Type */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  doc-modified = Yes.
  
  RUN verify-doc-type( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
  DO:
    SELF:MODIFIED = no.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewDocument.Reference
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewDocument.Reference br-doc _BROWSE-COLUMN W-Win
ON LEAVE OF NewDocument.Reference IN BROWSE br-doc /* Reference */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  doc-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewDocument.Description
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewDocument.Description br-doc _BROWSE-COLUMN W-Win
ON LEAVE OF NewDocument.Description IN BROWSE br-doc /* Description */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  doc-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewDocument.Description br-doc _BROWSE-COLUMN W-Win
ON TAB OF NewDocument.Description IN BROWSE br-doc /* Description */
DO:
  IF doc-modified THEN
  DO:
    RUN verify-doc.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
  END.

  IF LAST-EVENT:LABEL = 'ENTER':U THEN
  DO:
    APPLY 'ENTRY':U TO br-trn IN FRAME {&FRAME-NAME}.
    RETURN NO-APPLY.
  END.
/*  ELSE MESSAGE LAST-EVENT:LABEL VIEW-AS ALERT-BOX. */

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-trn
&Scoped-define SELF-NAME br-trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON CTRL-DEL OF br-trn IN FRAME F-Main
OR 'SHIFT-DEL':U OF {&SELF-NAME} ANYWHERE DO:
  RUN delete-trn.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON CTRL-INS OF br-trn IN FRAME F-Main
OR "SHIFT-INS":U OF {&SELF-NAME}
ANYWHERE DO:
  trn-before = IF AVAILABLE NewacctTrans THEN NewacctTrans.TransactionCode ELSE 1.
  trn-inserted = Yes.
  APPLY 'CHOOSE':U TO btn_Ins-Trn.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON ENTRY OF br-trn IN FRAME F-Main
OR 'MOUSE-SELECT-CLICK':U OF {&SELF-NAME} DO:

  IF LOOKUP( LAST-EVENT:LABEL, 'ENTER,TAB,MOUSE-SELECT-UP':U ) = 0 THEN RETURN.
  IF NOT AVAILABLE NewDocument THEN RETURN.
    
  IF NOT ( NUM-RESULTS("br-trn":U) = ? OR
           NUM-RESULTS("br-trn":U) = 0 ) THEN
    IF br-trn:SELECT-FOCUSED-ROW() THEN.

  IF NUM-RESULTS("br-trn":U) = 0 OR NUM-RESULTS( "br-trn" ) = ? THEN
  DO:
    RUN add-new-trn.
    RETURN NO-APPLY.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON ROW-DISPLAY OF br-trn IN FRAME F-Main
DO:
  RUN set-trn-color.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON ROW-ENTRY OF br-trn IN FRAME F-Main
DO:
  RUN check-last-trn.
  RUN update-trn-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON ROW-LEAVE OF br-trn IN FRAME F-Main
OR 'CURSOR-DOWN':U OF {&SELF-NAME} 
OR 'CURSOR-UP':U   OF {&SELF-NAME}
ANYWHERE DO:

  IF trn-modified THEN
  DO:
    RUN verify-trn.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
  END.

  IF LAST-EVENT:FUNCTION = 'ROW-LEAVE':U AND
     LAST-EVENT:LABEL = 'ENTER':U THEN
  DO:
    IF last-trn-row THEN RUN add-new-trn. ELSE APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.EntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.EntityType br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.EntityType IN BROWSE br-trn /* T */
DO:
  RUN set-trn-color.
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.

  RUN verify-entity-type( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN NO-APPLY.

  APPLY 'ENTRY':U TO NewAcctTrans.EntityCode IN BROWSE br-trn.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.EntityCode br-trn _BROWSE-COLUMN W-Win
ON GO OF NewAcctTrans.EntityCode IN BROWSE br-trn /* Entity */
DO:
  RUN select-entity-code( SELF ).
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.EntityCode br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.EntityCode IN BROWSE br-trn /* Entity */
DO:
  IF selecting-ecode THEN RETURN.
  IF NOT SELF:MODIFIED
     AND NOT NewAcctTrans.EntityType:MODIFIED IN BROWSE br-trn THEN RETURN.
  trn-modified = Yes.      
  
  RUN verify-entity-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
  DO:
    SELF:MODIFIED = no.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.AccountCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.AccountCode br-trn _BROWSE-COLUMN W-Win
ON GO OF NewAcctTrans.AccountCode IN BROWSE br-trn /* Account */
DO:
  IF NOT multi-ledger-creditors THEN
    RUN select-account-code( SELF ).
  RETURN NO-APPLY.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.AccountCode br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.AccountCode IN BROWSE br-trn /* Account */
DO:
  IF selecting-code THEN RETURN.
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
  
  RUN verify-account-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN DO:
    SELF:MODIFIED = no.
    RETURN NO-APPLY.
  END.
  RUN update-account-name("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.Date
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.Date br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.Date IN BROWSE br-trn /* Date */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
  
  RUN verify-date( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.Amount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.Amount br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.Amount IN BROWSE br-trn /* Amount */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
  RUN set-trn-color.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.Reference
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.Reference br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.Reference IN BROWSE br-trn /* Reference */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.Description
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.Description br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.Description IN BROWSE br-trn /* Description */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.Description br-trn _BROWSE-COLUMN W-Win
ON TAB OF NewAcctTrans.Description IN BROWSE br-trn /* Description */
OR 'ENTER':U OF {&SELF-NAME}
ANYWHERE DO:
  IF AVAILABLE NewAcctTrans THEN last-trn-type = NewAcctTrans.EntityType.

  IF br-trn:CURRENT-ROW-MODIFIED IN FRAME {&FRAME-NAME} THEN
  DO:
    RUN verify-trn.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
  END.

  IF last-trn-row AND LAST-EVENT:LABEL = 'TAB':U THEN
  DO:
    GET LAST br-doc.
    IF br-doc:SET-REPOSITIONED-ROW( 1, "CONDITIONAL" ) THEN.
    REPOSITION br-doc TO ROWID ROWID ( NewDocument ).
    RUN add-new-doc.
    RETURN NO-APPLY.
  END.
  
  IF last-trn-row AND LAST-EVENT:LABEL = 'ENTER':U THEN
  DO:
/*    RUN add-new-trn. */
    APPLY 'CHOOSE':U TO Btn_Add-Trn.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add-doc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add-doc W-Win
ON CHOOSE OF btn_add-doc IN FRAME F-Main /* Add document */
DO:
  RUN add-new-doc.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Add-Trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Add-Trn W-Win
ON CHOOSE OF Btn_Add-Trn IN FRAME F-Main /* Add transaction */
DO:
  RUN add-new-trn.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_Ins-Trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Ins-Trn W-Win
ON CHOOSE OF btn_Ins-Trn IN FRAME F-Main /* Insert transaction */
DO:
  RUN insert-new-trn.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-doc
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK W-Win 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-new-doc W-Win 
PROCEDURE add-new-doc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  add-new-doc = Yes.  

  IF NUM-RESULTS("br-doc":U) = ? OR
     NUM-RESULTS("br-doc":U) = 0 THEN
  DO:
    RUN create-doc.
    RUN open-doc-query.
    RUN check-last-doc.
  END.
  ELSE
  DO WITH FRAME {&FRAME-NAME}:
    IF br-doc:INSERT-ROW("AFTER") THEN RUN check-last-doc.
    RUN close-trn-query.
  END.

  APPLY 'ENTRY':U TO NewDocument.DocumentType IN BROWSE br-doc.

  /* Set the default screen-values */
  RUN set-doc-defaults.

  APPLY 'ENTRY':U to NewDocument.DocumentType.
  doc-modified = Yes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-new-trn W-Win 
PROCEDURE add-new-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   add-new-trn = Yes.  

  IF NUM-RESULTS("br-trn":U) > 0 THEN DO WITH FRAME {&FRAME-NAME}:
    IF br-trn:REFRESH() THEN.
    IF NOT br-trn:INSERT-ROW( "AFTER":U )  THEN MESSAGE "Row insert failed".
    /* IF br-trn:CREATE-RESULT-LIST-ENTRY() THEN.*/
  END.
  ELSE DO:
    RUN create-trn.
    RUN open-trn-query.
  END.

  RUN check-last-trn.

  /* Set the default screen-values */
  APPLY 'ENTRY':U TO NewAcctTrans.EntityType IN BROWSE br-trn.

  RUN set-trn-defaults.  

  APPLY 'ENTRY':U TO NewAcctTrans.EntityType IN BROWSE br-trn.
  trn-modified = Yes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects W-Win  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available W-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "NewBatch"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "NewBatch"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-doc W-Win 
PROCEDURE assign-doc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE NewDocument THEN RETURN.
  
  FIND CURRENT NewDocument EXCLUSIVE-LOCK.
  
  ASSIGN
    NewDocument.BatchCode = NewBatch.BatchCode.

  ASSIGN BROWSE br-doc
    NewDocument.DocumentType
    NewDocument.Reference
    NewDocument.Description.
      
  FIND CURRENT NewDocument NO-LOCK.
  RUN display-doc.
  doc-modified = No.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-trn W-Win 
PROCEDURE assign-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE NewAcctTrans THEN RETURN.

  FIND CURRENT NewAcctTrans EXCLUSIVE-LOCK.
  
  ASSIGN
    NewAcctTrans.BatchCode    = NewBatch.BatchCode
    NewAcctTrans.DocumentCode = NewDocument.DocumentCode.

  ASSIGN BROWSE br-trn
    NewAcctTrans.EntityType
    NewAcctTrans.EntityCode
    NewAcctTrans.AccountCode
    NewAcctTrans.Date
    NewAcctTrans.Amount
    NewAcctTrans.Reference
    NewAcctTrans.Description.

  /* If this transaction has been inserted and not added then we must
     re-order all subsequent transactions */
     
  IF trn-inserted THEN
  DO:
    DEF BUFFER RenumberTrn FOR NewAcctTrans.

    /* Ensure there are no renumbering collisions with the current transaction */
    ASSIGN NewAcctTrans.TransactionCode = 0.
        
    FOR EACH RenumberTrn WHERE
      RenumberTrn.BatchCode       = NewAcctTrans.BatchCode AND
      RenumberTrn.DocumentCode    = NewAcctTrans.DocumentCode AND
      RenumberTrn.TransactionCode >= trn-before AND
      RenumberTrn.TransactionCode <> NewAcctTrans.TransactionCode
      EXCLUSIVE-LOCK
      BY RenumberTrn.TransactionCode DESCENDING:

      ASSIGN RenumberTrn.TransactionCode = RenumberTrn.TransactionCode + 1.
      
    END.

    ASSIGN NewAcctTrans.TransactionCode = trn-before.

  END.

  FIND CURRENT NewAcctTrans NO-LOCK.  
  RUN display-trn.
  
  trn-modified = No.
  trn-inserted = No.
  
  RUN update-batch-total.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-doc W-Win 
PROCEDURE check-doc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-doc-type( "Check" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( NewDocument.DocumentType:HANDLE IN BROWSE br-doc ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-last-doc W-Win 
PROCEDURE check-last-doc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  last-doc-row  = CURRENT-RESULT-ROW( "br-doc" ) = NUM-RESULTS( "br-doc" ).  
  last-doc-type = INPUT BROWSE br-doc NewDocument.DocumentType.
  last-doc-type = IF last-doc-type = ? THEN "" ELSE last-doc-type.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-last-trn W-Win 
PROCEDURE check-last-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  last-trn-row = IF trn-inserted THEN No ELSE
                    CURRENT-RESULT-ROW( "br-trn" ) = NUM-RESULTS( "br-trn" ) OR
                    br-trn:NEW-ROW IN FRAME {&FRAME-NAME}.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-trn W-Win 
PROCEDURE check-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-entity-code( "Check" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( NewAcctTrans.EntityCode:HANDLE IN BROWSE br-trn ).

  RUN verify-account-code( "Check" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( NewAcctTrans.AccountCode:HANDLE ).
  
  RUN verify-date( "Check").
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( NewAcctTrans.Date:HANDLE ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-doc-query W-Win 
PROCEDURE close-doc-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY br-doc.
  last-doc-type = "".
  RUN close-trn-query.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-trn-query W-Win 
PROCEDURE close-trn-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY br-trn.
  last-trn-type = "".
  fil_entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
  fil_account:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
  RUN update-batch-total.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-coa-list W-Win 
PROCEDURE create-coa-list :
/*------------------------------------------------------------------------------
  Purpose:     Create a dynmic selection list for chart of accounts
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  DEF VAR item AS CHAR NO-UNDO.

  CREATE SELECTION-LIST coa-list
  ASSIGN
    FRAME        = FRAME {&FRAME-NAME}:HANDLE
    BGCOLOR      = 0
    FGCOLOR      = 15
    X            = 1
    Y            = 1
    WIDTH-P      = 1
    HEIGHT-P     = 1
    DELIMITER    = '|'
    FONT         = 15
    HIDDEN       = Yes
    SCROLLBAR-VERTICAL = Yes
    SCROLLBAR-VERTICAL = No
          
  TRIGGERS:
    ON 'RETURN':U, 'DEFAULT-ACTION':U PERSISTENT RUN update-account-code IN THIS-PROCEDURE.
    ON 'LEAVE':U PERSISTENT RUN hide-coa-list IN THIS-PROCEDURE.
  END TRIGGERS.

  coa-list:LIST-ITEMS = "".
  
  FOR EACH ChartOfAccount NO-LOCK:
    item = STRING( ChartOfAccount.AccountCode, "9999.99" ) + '  ' + ChartOfAccount.Name.
    IF coa-list:ADD-LAST(item) THEN.
  END.

  coa-list:WIDTH-CHARS  = 40.
  coa-list:HEIGHT-CHARS = 6.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-doc W-Win 
PROCEDURE create-doc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CREATE NewDocument.
  ASSIGN
    NewDocument.BatchCode    = NewBatch.BatchCode
    NewDocument.DocumentType = "*"
    NewDocument.Reference    = ""
    NewDocument.Description  = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-ecode-list W-Win 
PROCEDURE create-ecode-list :
/*------------------------------------------------------------------------------
  Purpose:     Create a dynmic selection list for chart of accounts
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  DEF VAR item AS CHAR NO-UNDO.

  CREATE SELECTION-LIST ecode-list
  ASSIGN
    FRAME        = FRAME {&FRAME-NAME}:HANDLE
    BGCOLOR      = 0
    FGCOLOR      = 15
    X            = 1
    Y            = 1
    WIDTH-P      = 1
    HEIGHT-P     = 1
    DELIMITER    = '|'
    FONT         = 15
    HIDDEN       = Yes
    SCROLLBAR-VERTICAL = Yes
    SCROLLBAR-VERTICAL = No
          
  TRIGGERS:
    ON 'RETURN':U, 'DEFAULT-ACTION':U PERSISTENT RUN update-entity-code IN THIS-PROCEDURE.
    ON 'LEAVE':U PERSISTENT RUN hide-ecode-list IN THIS-PROCEDURE.
  END TRIGGERS.

  ecode-list:LIST-ITEMS = "".
  
  CASE INPUT BROWSE br-trn NewAcctTrans.EntityType:
      WHEN 'L' THEN DO:
          FOR EACH Company NO-LOCK:
            item = STRING( Company.CompanyCode, "99999" ) + '  ' + STRING( Company.LegalName, "X(50)" ).
            IF ecode-list:ADD-LAST(item) THEN.
          END.
      END.
      WHEN 'P' THEN DO:
          FOR EACH Property NO-LOCK:
            ITEM = STRING( Property.PropertyCode, "99999" ) + '  ' + STRING( Property.Name, "X(50)" ).
            IF ecode-list:ADD-LAST(item) THEN.
          END.
      END.
      WHEN 'J' THEN DO:
          FOR EACH Project NO-LOCK:
            ITEM = STRING( Project.ProjectCode, "99999" ) + '  ' + STRING( Project.Name, "X(50)" ).
            IF ecode-list:ADD-LAST(item) THEN.
          END.
      END.
      WHEN 'T' THEN DO:
          FOR EACH Tenant NO-LOCK:
            ITEM = STRING( Tenant.TenantCode, "99999" ) + '  ' + STRING( Tenant.Name, "X(50)" ).
            IF ecode-list:ADD-LAST(item) THEN.
          END.
      END.
      WHEN 'C' THEN DO:
          FOR EACH Creditor NO-LOCK:
            ITEM = STRING( Creditor.CreditorCode, "99999" ) + '  ' + STRING( Creditor.Name, "X(50)" ).
            IF ecode-list:ADD-LAST(item) THEN.
          END.
      END.
  END CASE.

  ecode-list:WIDTH-CHARS  = 60.
  ecode-list:HEIGHT-CHARS = 6.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-trn W-Win 
PROCEDURE create-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CREATE NewAcctTrans.
  ASSIGN
    NewAcctTrans.BatchCode    = NewBatch.BatchCode
    NewAcctTrans.DocumentCode = NewDocument.DocumentCode
    NewAcctTrans.EntityType   = "*"
    NewAcctTrans.Date         = TODAY.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-doc W-Win 
PROCEDURE delete-doc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NUM-RESULTS( "br-doc" ) = 0 OR NUM-RESULTS( "br-doc" ) = ? THEN RETURN.
  
  IF br-doc:SELECT-FOCUSED-ROW() IN FRAME {&FRAME-NAME} THEN
  DO:

    IF NOT br-doc:NEW-ROW THEN
    DO:
      IF br-doc:FETCH-SELECTED-ROW(1) THEN.
      IF AVAILABLE NewDocument THEN
      DO:
        GET CURRENT br-doc EXCLUSIVE-LOCK.
        DELETE NewDocument.
        IF br-doc:DELETE-CURRENT-ROW() THEN.
      END.
        
    END.
    ELSE
      IF br-doc:DELETE-CURRENT-ROW() THEN.    
  END.
  doc-modified = No.
  
  IF NUM-RESULTS( "br-doc" ) = ? OR 
     NUM-RESULTS( "br-doc" ) = 0 THEN
  DO:
    RUN close-doc-query.
    RETURN.
  END.
     
  IF br-doc:FETCH-SELECTED-ROW(1) THEN RUN display-doc.
  RUN check-last-doc.
  RUN update-batch-total.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-trn W-Win 
PROCEDURE delete-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NUM-RESULTS( "br-trn" ) = 0 OR NUM-RESULTS( "br-trn" ) = ? THEN RETURN.

  IF br-trn:SELECT-FOCUSED-ROW() IN FRAME {&FRAME-NAME} THEN
  DO:

    IF NOT br-trn:NEW-ROW THEN
    DO:
      IF br-trn:FETCH-SELECTED-ROW(1) THEN.
      IF AVAILABLE NewAcctTrans THEN
      DO:
        GET CURRENT br-trn EXCLUSIVE-LOCK.
        DELETE NewAcctTrans.
        IF br-trn:DELETE-CURRENT-ROW() THEN.
      END.
        
    END.
    ELSE
      IF br-trn:DELETE-CURRENT-ROW() THEN.    
  END.

  trn-modified = No.
  IF NUM-RESULTS( "br-trn" ) = ? OR 
     NUM-RESULTS( "br-trn" ) = 0 THEN
  DO:
    RUN close-trn-query.
    RETURN.
  END.

  IF br-trn:FETCH-SELECTED-ROW(1) THEN RUN display-trn.

  RUN check-last-trn.
  RUN update-batch-total.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI W-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
  THEN DELETE WIDGET W-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-doc W-Win 
PROCEDURE display-doc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE NewDocument THEN RETURN.
  
  DISPLAY
    NewDocument.DocumentType
    NewDocument.Reference
    NewDocument.Description
  WITH BROWSE br-doc.
  RUN set-doc-color.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-trn W-Win 
PROCEDURE display-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE NewAcctTrans THEN RETURN.
  
  DISPLAY
    NewAcctTrans.EntityType
    NewAcctTrans.EntityCode
    NewAcctTrans.AccountCode
    NewAcctTrans.Date
    NewAcctTrans.Amount
    NewAcctTrans.Reference
    NewAcctTrans.Description
  WITH BROWSE br-trn.
  
  RUN set-trn-color.
  RUN update-trn-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI W-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fil_Entity fil_Account fil_EntityType 
      WITH FRAME F-Main IN WINDOW W-Win.
  IF AVAILABLE NewBatch THEN 
    DISPLAY NewBatch.Total 
      WITH FRAME F-Main IN WINDOW W-Win.
  ENABLE br-doc btn_add-doc Btn_Add-Trn btn_Ins-Trn br-trn fil_EntityType 
      WITH FRAME F-Main IN WINDOW W-Win.
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
  VIEW W-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-default-trn W-Win 
PROCEDURE get-default-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND FIRST DefTrans NO-ERROR.
  IF NOT AVAILABLE DefTrans THEN CREATE DefTrans.

  IF trn-inserted THEN
  DO:
    ASSIGN
      DefTrans.EntityType  = "L"
      DefTrans.EntityCode  = 0
      DefTrans.AccountCode = 0000.00
      DefTrans.Date        = TODAY
      DefTrans.Amount      = 0.00
      DefTrans.Reference   = IF AVAILABLE NewDocument THEN
        NewDocument.Reference ELSE ""
      DefTrans.Description = IF AVAILABLE NewDocument THEN
        NewDocument.Description ELSE "".
    RETURN.
  END.

  GET PREV br-trn.
  IF AVAILABLE NewAcctTrans THEN
  DO:
    ASSIGN
      DefTrans.EntityType  = NewAcctTrans.EntityType
      DefTrans.EntityCode  = NewAcctTrans.EntityCode
      DefTrans.AccountCode = NewAcctTrans.AccountCode
      DefTrans.Date        = NewAcctTrans.Date
      DefTrans.Amount      = 0.00
      DefTrans.Reference   = NewAcctTrans.Reference
      DefTrans.Description = NewAcctTrans.Description.

    GET NEXT br-trn.
  END.
  ELSE
  DO:

    DEF BUFFER LastDoc FOR NewDocument.
    DEF BUFFER LastTrn FOR NewacctTrans.
    FIND LAST LastDoc OF NewBatch WHERE
      LastDoc.DocumentCode < NewDocument.DocumentCode NO-LOCK NO-ERROR.
    FIND LAST LastTrn OF LastDoc  NO-LOCK NO-ERROR.
   
    ASSIGN
      DefTrans.EntityType  = "L"
      DefTrans.EntityCode  = 0
      DefTrans.AccountCode = 0000.00
      DefTrans.Date        = IF AVAILABLE LastTrn THEN LastTrn.Date ELSE TODAY
      DefTrans.Amount      = 0.00
      DefTrans.Reference   = IF AVAILABLE NewDocument THEN
        NewDocument.Reference ELSE ""
      DefTrans.Description = IF AVAILABLE NewDocument THEN
        NewDocument.Description ELSE "".

    GET FIRST br-trn.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hide-coa-list W-Win 
PROCEDURE hide-coa-list :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  coa-list:VISIBLE   = No.
  coa-list:SENSITIVE = No.
  APPLY 'ENTRY':U TO NewAcctTrans.Date IN BROWSE br-trn.
  selecting-code = No.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hide-ecode-list W-Win 
PROCEDURE hide-ecode-list :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  ecode-list:VISIBLE   = No.
  ecode-list:SENSITIVE = No.
  APPLY 'ENTRY':U TO NewAcctTrans.AccountCode IN BROWSE br-trn.
  selecting-ecode = No.
  
  ecode-list = ?.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE insert-new-trn W-Win 
PROCEDURE insert-new-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:
    IF br-trn:REFRESH() THEN.
    IF br-trn:INSERT-ROW("BEFORE") THEN.
  END.

  RUN check-last-trn.

  /* Set the default screen-values */
  APPLY 'ENTRY':U TO NewAcctTrans.EntityType IN BROWSE br-trn.

  RUN set-trn-defaults.  

  APPLY 'ENTRY':U TO NewAcctTrans.EntityType IN BROWSE br-trn.
  trn-modified = Yes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize W-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Get the sundry debtors and creditors control accounts */
  
  FIND FIRST Office WHERE Office.ThisOffice NO-LOCK.
  
  FIND FIRST OfficeControlAccount OF Office
    WHERE OfficeControlAccount.Name = "Debtors" NO-LOCK.
    
  sundry-debtors = OfficeControlAccount.AccountCode.
  
  FIND FIRST OfficeControlAccount OF Office
    WHERE OfficeControlAccount.Name = "Creditors" NO-LOCK.
    
  sundry-creditors = OfficeControlAccount.AccountCode.

DO WITH FRAME {&FRAME-NAME}:
  HIDE Btn_Add-Trn Btn_Add-Doc Btn_Ins-trn.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit W-Win 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
   APPLY "CLOSE":U TO THIS-PROCEDURE.
   
   RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available W-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT have-records THEN
  DO:
    have-records = Yes.
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .
    IF AVAILABLE NewBatch THEN RUN open-doc-query.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE next-doc-type W-Win 
PROCEDURE next-doc-type :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT BROWSE br-doc NewDocument.DocumentType.
  
  FIND FIRST DocumentType WHERE DocumentType.DocumentType > type
    NO-LOCK NO-ERROR.
    
  IF NOT AVAILABLE DocumentType THEN
   FIND FIRST DocumentType NO-LOCK NO-ERROR.
   
  NewDocument.DocumentType:SCREEN-VALUE IN BROWSE br-doc = 
    IF AVAILABLE DocumentType THEN DocumentType.DocumentType ELSE "".
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-doc-query W-Win 
PROCEDURE open-doc-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE NewBatch THEN
  DO:
    OPEN QUERY br-doc FOR EACH NewDocument OF NewBatch NO-LOCK.
    RUN display-doc.
    RUN update-batch-total.
    RUN open-trn-query.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-trn-query W-Win 
PROCEDURE open-trn-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF AVAILABLE NewDocument THEN
  DO:
    OPEN QUERY br-trn FOR EACH NewAcctTrans OF NewDocument NO-LOCK.
    RUN display-trn.
    RUN update-trn-fields.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-account-code W-Win 
PROCEDURE select-account-code :
/*----------------------------------------------------------------------------
  Purpose:     Select an account code for the Transaction
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER attach-to AS HANDLE NO-UNDO.
  DEF VAR item AS CHAR NO-UNDO.

  selecting-code = Yes.
  
DO WITH FRAME {&FRAME-NAME}:
  
  IF NOT VALID-HANDLE( coa-list ) THEN RUN create-coa-list.
      
  /* Set the COA List screen value */
  FIND ChartOfAccount WHERE
    ChartOfAccount.AccountCode = DEC( attach-to:SCREEN-VALUE )
  NO-LOCK NO-ERROR.
  
  IF AVAILABLE ChartOfAccount THEN
  DO:  
    item = STRING( ChartOfAccount.AccountCode, "9999.99" ) + '  ' + ChartOfAccount.Name.
    coa-list:SCREEN-VALUE = item.
  END.
  ELSE
    coa-list:SCREEN-VALUE = coa-list:ENTRY(1).
  
  /* Set the position */

  IF br-trn:y + attach-to:y +
     attach-to:height-pixels + coa-list:height-pixels >
     FRAME {&FRAME-NAME}:HEIGHT-PIXELS THEN
    coa-list:y = br-trn:y + attach-to:y - coa-list:height-pixels.
  ELSE
    coa-list:y = br-trn:y + attach-to:y + attach-to:height-pixels.
    
  coa-list:x = br-trn:x + attach-to:x + 20.
  coa-list:VISIBLE   = Yes.
  coa-list:SENSITIVE = Yes.  
  APPLY 'ENTRY':U TO coa-list.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-entity-code W-Win 
PROCEDURE select-entity-code :
/*----------------------------------------------------------------------------
  Purpose:     Select an account code for the Transaction
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER attach-to AS HANDLE NO-UNDO.
  DEF VAR item AS CHAR NO-UNDO.

  selecting-ecode = Yes.
  
DO WITH FRAME {&FRAME-NAME}:
  
  IF NOT VALID-HANDLE( ecode-list ) THEN RUN create-ecode-list.
      
  /* Set the COA List screen value */
  CASE INPUT BROWSE br-trn NewAcctTrans.EntityType:
      WHEN 'L' THEN DO:
          FIND Company WHERE
            Company.CompanyCode = INTEGER( attach-to:SCREEN-VALUE ) NO-LOCK NO-ERROR.

          IF AVAILABLE Company THEN DO:  
            item = STRING( Company.CompanyCode, "99999" ) + '  ' + STRING( Company.LegalName, "X(50)" ).
            ecode-list:SCREEN-VALUE = item.
          END.
          ELSE
            ecode-list:SCREEN-VALUE = ecode-list:ENTRY(1).
      END.
      WHEN 'P' THEN DO:
          FIND Property WHERE
            Property.PropertyCode = INTEGER( attach-to:SCREEN-VALUE ) NO-LOCK NO-ERROR.

          IF AVAILABLE Property THEN DO:  
            item = STRING( Property.PropertyCode, "99999" ) + '  ' + STRING( Property.Name, "X(50)" ).
            ecode-list:SCREEN-VALUE = item.
          END.
          ELSE
            ecode-list:SCREEN-VALUE = ecode-list:ENTRY(1).
      END.
      WHEN 'J' THEN DO:
          FIND Project WHERE
            Project.ProjectCode = INTEGER( attach-to:SCREEN-VALUE ) NO-LOCK NO-ERROR.

          IF AVAILABLE Project THEN DO:  
            item = STRING( Project.ProjectCode, "99999" ) + '  ' + STRING( Project.Name, "X(50)" ).
            ecode-list:SCREEN-VALUE = item.
          END.
          ELSE
            ecode-list:SCREEN-VALUE = ecode-list:ENTRY(1).
      END.
      WHEN 'T' THEN DO:
          FIND Tenant WHERE
            Tenant.TenantCode = INTEGER( attach-to:SCREEN-VALUE ) NO-LOCK NO-ERROR.

          IF AVAILABLE Tenant THEN DO:  
            item = STRING( Tenant.TenantCode, "99999" ) + '  ' + STRING( Tenant.Name, "X(50)" ).
            ecode-list:SCREEN-VALUE = item.
          END.
          ELSE
            ecode-list:SCREEN-VALUE = ecode-list:ENTRY(1).
      END.
      WHEN 'C' THEN DO:
          FIND Creditor WHERE
            Creditor.CreditorCode = INTEGER( attach-to:SCREEN-VALUE ) NO-LOCK NO-ERROR.

          IF AVAILABLE Creditor THEN DO:  
            item = STRING( Creditor.CreditorCode, "99999" ) + '  ' + STRING( Creditor.Name, "X(50)" ).
            ecode-list:SCREEN-VALUE = item.
          END.
          ELSE
            ecode-list:SCREEN-VALUE = ecode-list:ENTRY(1).
      END.
  END CASE.
  
  /* Set the position */

  IF br-trn:y + attach-to:y +
     attach-to:height-pixels + ecode-list:height-pixels >
     FRAME {&FRAME-NAME}:HEIGHT-PIXELS THEN
    ecode-list:y = br-trn:y + attach-to:y - ecode-list:height-pixels.
  ELSE
    ecode-list:y = br-trn:y + attach-to:y + attach-to:height-pixels.
    
  ecode-list:x = br-trn:x + attach-to:x + 20.
  ecode-list:visible   = Yes.
  ecode-list:sensitive = Yes.
  APPLY 'ENTRY':U TO ecode-list.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records W-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "NewBatch"}
  {src/adm/template/snd-list.i "NewAcctTrans"}
  {src/adm/template/snd-list.i "NewDocument"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-account-code W-Win 
PROCEDURE sensitise-account-code :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE NewAcctTrans THEN RETURN.
  
  DEF VAR type AS CHAR NO-UNDO.
  type = IF CAN-QUERY( NewAcctTrans.EntityType:HANDLE IN BROWSE br-trn, "SCREEN-VALUE" ) THEN
    NewAcctTrans.EntityType:SCREEN-VALUE ELSE NewAcctTrans.EntityType.

  IF NOT multi-ledger-creditors THEN
    ASSIGN
      NewAcctTrans.AccountCode:READ-ONLY = ((type = "C") OR ((type = "T" AND NOT tenant-accounts))).
  ELSE
    ASSIGN
      NewAcctTrans.AccountCode:READ-ONLY = (type = "T" AND NOT tenant-accounts).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-doc-color W-Win 
PROCEDURE set-doc-color :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR fcolor AS INT INIT ? NO-UNDO.
  DEF VAR bcolor AS INT INIT ? NO-UNDO.
  
  ASSIGN
    NewDocument.DocumentType:FGCOLOR IN BROWSE br-doc = fcolor
    NewDocument.Reference:FGCOLOR    = fcolor
    NewDocument.Description:FGCOLOR  = fcolor

    NewDocument.DocumentType:BGCOLOR = bcolor
    NewDocument.Reference:BGCOLOR    = bcolor
    NewDocument.Description:BGCOLOR  = bcolor.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-doc-defaults W-Win 
PROCEDURE set-doc-defaults :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  ASSIGN
    last-doc-type = IF LOOKUP( last-doc-type, "*," ) <> 0 THEN
      "JRNL" ELSE last-doc-type
    NewDocument.DocumentType:SCREEN-VALUE IN BROWSE br-doc = last-doc-type
    NewDocument.Reference:SCREEN-VALUE = ""
    NewDocument.Description:SCREEN-VALUE = NewBatch.Description
    
    NewDocument.DocumentType:MODIFIED = No
    NewDocument.Reference:MODIFIED    = No
    NewDocument.Description:MODIFIED  = No.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-trn-color W-Win 
PROCEDURE set-trn-color :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR fcolor AS INT INIT ? NO-UNDO.
  DEF VAR bcolor AS INT INIT ? NO-UNDO.
  
  DEF VAR type   AS CHAR NO-UNDO.
  DEF VAR amount AS DEC NO-UNDO.
  type = IF CAN-QUERY( NewAcctTrans.EntityType:HANDLE IN BROWSE br-trn, "SCREEN-VALUE" ) THEN
    NewAcctTrans.EntityType:SCREEN-VALUE ELSE NewAcctTrans.EntityType.
  amount = IF CAN-QUERY( NewAcctTrans.Amount:HANDLE IN BROWSE br-trn, "SCREEN-VALUE" ) THEN
    DEC( NewAcctTrans.Amount:SCREEN-VALUE ) ELSE NewAcctTrans.Amount.

  CASE type:
    WHEN "L" THEN ASSIGN fcolor = 1  bcolor = 15.
    WHEN "P" THEN ASSIGN fcolor = 2  bcolor = 15.
    WHEN "T" THEN ASSIGN fcolor = 4  bcolor = 15.
    WHEN "J" THEN ASSIGN fcolor = 6 bcolor = 15.
    WHEN "C" THEN ASSIGN fcolor = 5 bcolor = 15.
  END CASE.

  ASSIGN
    NewAcctTrans.EntityType:FGCOLOR  = fcolor
    NewAcctTrans.EntityCode:FGCOLOR  = fcolor
    NewAcctTrans.AccountCode:FGCOLOR = fcolor
    NewAcctTrans.Date:FGCOLOR        = fcolor
    NewAcctTrans.Amount:FGCOLOR      = IF amount < 0 THEN 12 ELSE 0
    NewAcctTrans.Reference:FGCOLOR   = fcolor
    NewAcctTrans.Description:FGCOLOR = fcolor

    NewAcctTrans.EntityType:BGCOLOR  = bcolor
    NewAcctTrans.EntityCode:BGCOLOR  = bcolor
    NewAcctTrans.AccountCode:BGCOLOR = bcolor
    NewAcctTrans.Date:BGCOLOR        = bcolor
    NewAcctTrans.Amount:BGCOLOR      = bcolor
    NewAcctTrans.Reference:BGCOLOR   = bcolor
    NewAcctTrans.Description:BGCOLOR = bcolor.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-trn-defaults W-Win 
PROCEDURE set-trn-defaults :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  RUN get-default-trn.
  IF NOT AVAILABLE DefTrans THEN RETURN.  

  ASSIGN
    NewAcctTrans.EntityType:SCREEN-VALUE IN BROWSE br-trn = DefTrans.EntityType
    NewAcctTrans.EntityCode:SCREEN-VALUE  = STRING( DefTrans.EntityCode )
    NewAcctTrans.AccountCode:SCREEN-VALUE = STRING( DefTrans.AccountCode )
    NewAcctTrans.Date:SCREEN-VALUE        = STRING( DefTrans.Date )
    NewAcctTrans.Amount:SCREEN-VALUE      = STRING( DefTrans.Amount )
    NewAcctTrans.Reference:SCREEN-VALUE   = DefTrans.Reference
    NewAcctTrans.Description:SCREEN-VALUE = DefTrans.Description

    NewAcctTrans.EntityType:MODIFIED  = No 
    NewAcctTrans.EntityCode:MODIFIED  = No 
    NewAcctTrans.AccountCode:MODIFIED = No
    NewAcctTrans.Date:MODIFIED        = No       
    NewAcctTrans.Amount:MODIFIED      = No     
    NewAcctTrans.Reference:MODIFIED   = No  
    NewAcctTrans.Description:MODIFIED = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed W-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-account-code W-Win 
PROCEDURE update-account-code :
/*------------------------------------------------------------------------------
  Purpose:     Update the code value.
------------------------------------------------------------------------------*/

  NewAcctTrans.AccountCode:SCREEN-VALUE IN BROWSE br-trn = 
    ENTRY( 1, coa-list:SCREEN-VALUE, ' ').
  RUN update-trn-fields.
  RUN hide-coa-list.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-account-name W-Win 
PROCEDURE update-account-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-name AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  IF new-name = ? OR new-name = "" THEN DO:
    IF NUM-RESULTS( "br-trn" ) = 0 OR NUM-RESULTS( "br-trn" ) = ? THEN DO:
      fil_Account:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
      RETURN.
    END.

    DEF VAR type  AS CHAR NO-UNDO.
    DEF VAR e-code AS INT NO-UNDO.
    DEF VAR a-code AS DEC NO-UNDO.

    type = INPUT BROWSE br-trn NewacctTrans.EntityType.
    a-code = INPUT BROWSE br-trn NewAcctTrans.AccountCode.

    IF type = "J" AND CAN-FIND( ProjectBudget WHERE ProjectBudget.ProjectCode = e-code
                            AND ProjectBudget.AccountCode = a-code
                            AND ProjectBudget.Description <> "") THEN DO:
      FIND ProjectBudget NO-LOCK WHERE ProjectBudget.ProjectCode = e-code
                            AND ProjectBudget.AccountCode = a-code
                            AND ProjectBudget.Description <> "".
      new-name = ProjectBudget.Description .
    END.
    ELSE IF type = "C" THEN DO:
      IF NOT multi-ledger-creditors THEN DO:
        FIND FIRST ChartOfAccount WHERE ChartOfAccount.AccountCode = a-code NO-LOCK NO-ERROR.
        new-name = IF AVAILABLE ChartOfAccount THEN ChartOfAccount.Name ELSE "" .
      END.
      ELSE DO:
        FIND FIRST Company WHERE Company.CompanyCode = INT( a-code ) NO-LOCK NO-ERROR.
        new-name = IF AVAILABLE Company THEN Company.LegalName ELSE "".
      END.
    END.
    ELSE DO:
      FIND FIRST ChartOfAccount WHERE ChartOfAccount.AccountCode = a-code NO-LOCK NO-ERROR.
      new-name = IF AVAILABLE ChartOfAccount THEN ChartOfAccount.Name ELSE "" .
    END.
  END.

  ASSIGN fil_Account:SCREEN-VALUE IN FRAME {&FRAME-NAME} = new-name .
END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-batch-total W-Win 
PROCEDURE update-batch-total :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE NewBatch THEN RETURN.
  FIND CURRENT NewBatch NO-LOCK.
  DISPLAY NewBatch.Total WITH FRAME {&FRAME-NAME}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-doc W-Win 
PROCEDURE update-doc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Create a new line if needed and update the details */
  IF br-doc:NEW-ROW IN FRAME {&FRAME-NAME} THEN
  DO:
    RUN create-doc.
    RUN assign-doc.
    IF br-doc:CREATE-RESULT-LIST-ENTRY() THEN.
    RUN open-trn-query.  
  END.
  ELSE
    RUN assign-doc.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-code W-Win 
PROCEDURE update-entity-code :
/*------------------------------------------------------------------------------
  Purpose:     Update the code value.
------------------------------------------------------------------------------*/

  NewAcctTrans.EntityCode:SCREEN-VALUE IN BROWSE br-trn = 
    ENTRY( 1, ecode-list:SCREEN-VALUE, ' ').
  RUN update-trn-fields.
  RUN hide-ecode-list.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-fields W-Win 
PROCEDURE update-entity-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR type AS CHAR NO-UNDO.

  type = INPUT BROWSE br-trn NewacctTrans.EntityType.

  IF NOT multi-ledger-creditors THEN DO:
    ASSIGN
      NewAcctTrans.EntityCode:SCREEN-VALUE  IN BROWSE br-trn = STRING( 0 )
      NewAcctTrans.EntityCode:MODIFIED = No
      NewAcctTrans.AccountCode:SCREEN-VALUE =
        IF type = 'T' THEN STRING( sundry-debtors )   ELSE
        IF type = 'C' THEN STRING( sundry-creditors ) ELSE STRING( 0000.00 )
      NewAcctTrans.AccountCode:MODIFIED = No
      NewAcctTrans.AccountCode:READ-ONLY = ((type = "C") OR ((type = "T" AND NOT tenant-accounts)))
      NewAcctTrans.AccountCode:MODIFIED = No.
  END.
  ELSE DO:
    ASSIGN
      NewAcctTrans.EntityCode:SCREEN-VALUE  IN BROWSE br-trn = STRING( 0 )
      NewAcctTrans.EntityCode:MODIFIED = No
      NewAcctTrans.AccountCode:SCREEN-VALUE =
        IF type = 'T' THEN STRING( sundry-debtors )   ELSE STRING( 0000.00 )
      NewAcctTrans.AccountCode:MODIFIED = No
      NewAcctTrans.AccountCode:READ-ONLY = (type = "T" AND NOT tenant-accounts)
      NewAcctTrans.AccountCode:MODIFIED = No.
  END.
  
  RUN update-trn-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-name W-Win 
PROCEDURE update-entity-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NUM-RESULTS( "br-trn" ) = 0 OR NUM-RESULTS( "br-trn" ) = ? THEN DO:
    fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
    RETURN.
  END.

  DEF VAR et AS CHAR NO-UNDO.
  DEF VAR ec AS INT NO-UNDO.
  DEF VAR ac AS DEC NO-UNDO.
  DEF VAR e-type AS CHAR NO-UNDO  INITIAL "".
  DEF VAR e-name AS CHAR NO-UNDO  INITIAL "".
  et = INPUT BROWSE br-trn NewAcctTrans.EntityType.
  ec = INPUT BROWSE br-trn NewAcctTrans.EntityCode.
  
  CASE et:
    WHEN 'T' THEN DO:
      FIND Tenant WHERE Tenant.TenantCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Tenant) THEN ASSIGN
         e-type = "Tenant:"
         e-name = Tenant.Name.
    END.

    WHEN 'C' THEN DO:
      FIND Creditor WHERE Creditor.CreditorCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE Creditor THEN ASSIGN 
         e-type = "Creditor:"
         e-name = Creditor.Name.
    END.

    WHEN 'P' THEN DO:
      FIND Property WHERE Property.PropertyCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE Property THEN ASSIGN 
         e-type = "Property:"
         e-name = Property.Name .
    END.

    WHEN 'L' THEN DO:
      FIND Company WHERE Company.CompanyCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE Company THEN ASSIGN 
         e-type = "Ledger:"
         e-name = Company.LegalName .
    END.

    WHEN 'J' THEN DO:
      FIND Project WHERE Project.ProjectCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE Project THEN ASSIGN 
         e-type = "Project:"
         e-name = Project.Name.
    END.
  END CASE.

  ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} = e-name
        fil_EntityType:SCREEN-VALUE = e-type .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-trn W-Win 
PROCEDURE update-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Create a new line if needed and update the details */

  IF br-trn:NEW-ROW IN FRAME {&FRAME-NAME} THEN
  DO:
    RUN create-trn.
    RUN assign-trn.
    IF br-trn:CREATE-RESULT-LIST-ENTRY() THEN.
  END.
  ELSE
    RUN assign-trn.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-trn-fields W-Win 
PROCEDURE update-trn-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR type AS CHAR NO-UNDO.

  type = INPUT BROWSE br-trn NewAcctTrans.EntityType.

  IF NOT multi-ledger-creditors THEN
    ASSIGN
      NewAcctTrans.AccountCode:READ-ONLY = ((type = "C") OR ((type = "T" AND NOT tenant-accounts))).
  ELSE
    ASSIGN
      NewAcctTrans.AccountCode:READ-ONLY = (type = "T" AND NOT tenant-accounts).

  RUN update-entity-name.
  RUN update-account-name("").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE user-layout W-Win 
PROCEDURE user-layout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-account-code W-Win 
PROCEDURE verify-account-code :
/*------------------------------------------------------------------------------
  Purpose:     Verify that the transaction's account is a valid
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER mode AS CHAR NO-UNDO.
  
DEF VAR type AS CHAR NO-UNDO.
DEF VAR code AS INT  NO-UNDO.
DEF VAR acct AS DEC  NO-UNDO.
DEF VAR success AS LOGI INITIAL Yes NO-UNDO.
DEF VAR pre-message AS CHARACTER INIT "The account code " NO-UNDO.

  type = INPUT BROWSE br-trn NewAcctTrans.EntityType.
  code = INPUT BROWSE br-trn NewAcctTrans.EntityCode.
  acct = INPUT BROWSE br-trn NewAcctTrans.AccountCode.

  /* Check to see if the account exists. If creditor and creditors can be paid from
  multiple companies, then the account code is actually the company number. */
  IF multi-ledger-creditors THEN DO:
    IF type = "J" THEN
      success = CAN-FIND( ProjectBudget WHERE ProjectBudget.ProjectCode = code
                                  AND ProjectBudget.AccountCode = acct ).
    ELSE IF  type = "C" THEN DO:
      success = CAN-FIND( Company WHERE Company.CompanyCode = INT( acct ) ).
      pre-message = "The company code ".
    END.
    ELSE
      success = CAN-FIND( ChartOfAccount WHERE ChartOfAccount.AccountCode = acct ).
  END.
  ELSE DO:
    IF type = "J" THEN
      success = CAN-FIND( ProjectBudget WHERE ProjectBudget.ProjectCode = code
                                  AND ProjectBudget.AccountCode = acct ).
    ELSE
      success = CAN-FIND( ChartOfAccount WHERE ChartOfAccount.AccountCode = acct ).
  END.

  /* If creditors can be paid from multiple companies, then the account code is
  actually the company number. */

  IF NOT success THEN DO:
    IF mode = "Verify" THEN
      MESSAGE pre-message + STRING( acct ) + " is invalid !"
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
    RETURN "FAIL".
  END.

  IF type = "J" AND restrict-project-posting THEN DO:  
    FIND ProjectBudget WHERE ProjectBudget.ProjectCode = code
                         AND ProjectBudget.AccountCode = acct NO-LOCK.
    IF NOT ProjectBudget.AllowPosting THEN DO:
      IF mode = "Verify" THEN DO:
        FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
        MESSAGE "Project account " + STRING( acct ) + " cannot be updated to."
                 VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
      END.
      RETURN "FAIL".
    END.
  END.
  ELSE IF type <> "J" THEN DO:
    IF NOT multi-ledger-creditors THEN DO:
      FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = acct NO-LOCK.

      /* Check to see if we can update to this account for the given entity type */

      IF INDEX( ChartOfAccount.UpdateTo, type ) = 0 THEN DO:
        IF mode = "Verify" THEN DO:
          FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
          MESSAGE "Account " + STRING( acct ) + " cannot be updated to a " + 
                   EntityType.Description
                   VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
        END.
        RETURN "FAIL".
      END.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-date W-Win 
PROCEDURE verify-date :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER mode AS CHAR NO-UNDO.

  DEF VAR trn-date      AS DATE NO-UNDO.
  DEF VAR date-min      AS DATE INITIAL 1/1/1990 NO-UNDO.
  DEF VAR date-max      AS DATE NO-UNDO.
  DEF VAR warn-date-max AS DATE NO-UNDO.
  DEF VAR beep-date-min AS DATE NO-UNDO.
  DEF VAR beep-date-max AS DATE NO-UNDO.
  
  trn-date = INPUT BROWSE br-trn NewacctTrans.Date.
  date-min = TODAY - 660.
  date-max = TODAY + 300.
  warn-date-max = TODAY + 30.
  beep-date-min = TODAY - 30.
  beep-date-max = TODAY.
  
  IF trn-date < date-min OR trn-date > warn-date-max THEN
  DO:
    IF mode = "Verify" THEN DO:
      IF trn-date > date-max THEN DO:
        MESSAGE "The date you have entered is either too old " SKIP
                "or to far in the future."
                VIEW-AS ALERT-BOX ERROR.
        RETURN "FAIL".
      END.
      ELSE
        MESSAGE "The date you have entered is somewhat" SKIP
                "in the future." SKIP(1)
                "Please check that it is correct"
                VIEW-AS ALERT-BOX WARNING.
    END.
  END.
  
  IF mode = "Verify" AND 
   ( trn-date < beep-date-min OR trn-date > beep-date-max ) THEN BELL.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-doc W-Win 
PROCEDURE verify-doc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR err-field AS HANDLE NO-UNDO.

  RUN check-doc.
  err-field = WIDGET-HANDLE( RETURN-VALUE ).

  IF VALID-HANDLE( err-field ) THEN
  DO:
    MESSAGE "The current document is in error." SKIP
      "Do you want to abort the changes ? "
      VIEW-AS ALERT-BOX ERROR BUTTONS YES-NO UPDATE abort-it AS LOGI.
    IF abort-it THEN
    DO:
      IF AVAILABLE NewDocument AND NewDocument.DocumentType <> "*" THEN
        RUN display-doc.
      ELSE
        RUN delete-doc.
      doc-modified = No.
    END.
    ELSE
    DO:
      IF AVAILABLE NewDocument THEN GET CURRENT br-doc.
      APPLY 'ENTRY':U TO err-field.
      RETURN "FAIL".
    END.
  END.
  ELSE
    RUN update-doc.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-doc-type W-Win 
PROCEDURE verify-doc-type :
/*------------------------------------------------------------------------------
  Purpose:     Verify the document type
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER mode AS CHAR NO-UNDO.
  
  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT BROWSE br-doc NewDocument.DocumentType.
  
  IF NOT CAN-FIND( FIRST DocumentType WHERE DocumentType.DocumentType = type ) THEN
  DO:
    IF mode = "Verify" THEN
    DO:
      MESSAGE "The document type" type "is invalid !"
        VIEW-AS ALERT-BOX ERROR TITLE 'Document Type Error':U.
    END.
    RETURN "FAIL".
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-entity-code W-Win 
PROCEDURE verify-entity-code :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER mode AS CHAR NO-UNDO.

DEF VAR entity-ok AS LOGI INIT No NO-UNDO.
DEF VAR entity-warn AS LOGI INIT No NO-UNDO.
DEF VAR msgtext AS CHAR NO-UNDO.
DEF VAR type AS CHAR NO-UNDO.
DEF VAR code AS INT NO-UNDO.

  type = INPUT BROWSE br-trn NewAcctTrans.EntityType.
  code = INPUT BROWSE br-trn NewAcctTrans.EntityCode.

  entity-ok = 
    IF type = 'T' THEN CAN-FIND( FIRST Tenant   WHERE Tenant.TenantCode = code AND Tenant.Active)     ELSE
    IF type = 'C' THEN CAN-FIND( FIRST Creditor WHERE Creditor.CreditorCode = code AND Creditor.Active) ELSE
    IF type = 'P' THEN CAN-FIND( FIRST Property WHERE Property.PropertyCode = code AND Property.Active) ELSE
    IF type = 'L' THEN CAN-FIND( FIRST Company  WHERE Company.CompanyCode = code AND Company.Active)   ELSE
    IF type = 'J' THEN CAN-FIND( FIRST Project  WHERE Project.ProjectCode = code AND Project.Active)   ELSE
      No.

  entity-warn = 
    IF type = 'T' THEN CAN-FIND( FIRST Tenant   WHERE Tenant.TenantCode = code )     ELSE
    IF type = 'C' THEN CAN-FIND( FIRST Creditor WHERE Creditor.CreditorCode = code ) ELSE
    IF type = 'P' THEN CAN-FIND( FIRST Property WHERE Property.PropertyCode = code ) ELSE
    IF type = 'L' THEN CAN-FIND( FIRST Company  WHERE Company.CompanyCode = code )   ELSE
    IF type = 'J' THEN CAN-FIND( FIRST Project  WHERE Project.ProjectCode = code )   ELSE
      No.


  IF NOT entity-ok THEN DO:
    IF mode = "Verify" THEN DO:
      FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
      IF entity-warn THEN DO:
        MESSAGE "Warning: " + EntityType.Description + " " + STRING( code ) + " is inactive"
            VIEW-AS ALERT-BOX WARNING TITLE "Inactive " + EntityType.Description + " Warning - " + type + STRING(code).
      END.
      ELSE DO:
        MESSAGE "There is no " + EntityType.Description + " with code " + STRING( code )
            VIEW-AS ALERT-BOX ERROR TITLE "Entity Code Error - " + EntityType.Description.
        RETURN "FAIL".
      END.
    END.
  END.

  /* Destroy ecode-list */
  ecode-list = ?.

  RUN update-entity-name.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-entity-type W-Win 
PROCEDURE verify-entity-type :
/*------------------------------------------------------------------------------
  Purpose:     Verify that the entity type is valid
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER mode AS CHAR NO-UNDO.

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT BROWSE br-trn NewAcctTrans.EntityType.

  IF NOT CAN-FIND( FIRST Entitytype WHERE EntityType.EntityType = type ) THEN
  DO:
    IF mode = "Verify" THEN
      MESSAGE "The entity type " + type + " is invalid !"
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid Entity Type".
    RETURN "FAIL".
  END.

  NewAcctTrans.EntityType:SCREEN-VALUE IN BROWSE br-trn = 
    CAPS( INPUT BROWSE br-trn NewAcctTrans.EntityType ).
    
  RUN update-entity-fields.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-trn W-Win 
PROCEDURE verify-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR err-field AS HANDLE NO-UNDO.

  IF selecting-code OR selecting-ecode THEN RETURN NO-APPLY.

  RUN check-trn.
  err-field = WIDGET-HANDLE( RETURN-VALUE ).

  IF VALID-HANDLE( err-field ) THEN
  DO:
    MESSAGE "The current transaction is in error." SKIP
      "Do you want to abort the changes ? "
      VIEW-AS ALERT-BOX ERROR BUTTONS YES-NO UPDATE abort-it AS LOGI.
    IF abort-it THEN
    DO:
      IF AVAILABLE NewAcctTrans AND NewAcctTrans.EntityType <> "*" THEN
        RUN display-trn.
      ELSE
        RUN delete-trn.
      trn-modified = No.
    END.
    ELSE
    DO:
      IF AVAILABLE NewAcctTrans THEN GET CURRENT br-trn.
      APPLY 'ENTRY':U TO err-field.
      RETURN "FAIL".
    END.
  END.
  ELSE
    RUN update-trn.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

