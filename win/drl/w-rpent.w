&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS W-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode          AS CHAR NO-UNDO.

DEF VAR add-new-trn   AS LOGI INIT No NO-UNDO.
DEF VAR last-trn-type AS CHAR NO-UNDO.
DEF VAR trn-modified  AS LOGI NO-UNDO.
DEF VAR trn-inserted  AS LOGI NO-UNDO.
DEF VAR trn-before    AS INT  NO-UNDO.
DEF VAR last-trn-row  AS LOGI NO-UNDO.
DEF VAR display-amount   LIKE NewAcctTrans.Amount NO-UNDO.
DEF VAR batch-total   AS DEC NO-UNDO.

/* bank account */
DEF VAR bank-et AS CHAR NO-UNDO INITIAL "L".
DEF VAR bank-ec AS INT  NO-UNDO.
DEF VAR bank-ac AS DEC  NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}
{inc/ofc-acct.i "CREDITORS" "sundry-creditors"}
{inc/ofc-set.i "Receipts-Description" "receipts-description"}
IF NOT AVAILABLE(OfficeSetting) THEN receipts-description = "Entity".
{inc/ofc-set.i "Payments-Description" "payments-description"}
IF NOT AVAILABLE(OfficeSetting) THEN payments-description = "Entity".
{inc/ofc-set-l.i "Tenant-Accounts" "tenant-accounts"}
IF tenant-accounts <> Yes THEN tenant-accounts = No.
{inc/ofc-set-l.i "Restrict-Project-Posting" "restrict-project-posting"}

DEF BUFFER trn-doc FOR NewDocument.
DEF BUFFER rev-doc FOR NewDocument.

DEF BUFFER ReverseTrans FOR NewAcctTrans.

DEF WORK-TABLE DefTrans NO-UNDO LIKE NewAcctTrans.

DEF VAR summary AS LOGI NO-UNDO.
DEF VAR have-records AS LOGI INIT No NO-UNDO.


DEF VAR signed-amount LIKE NewAcctTrans.Amount NO-UNDO.
ON FIND OF NewAcctTrans DO:
  IF NewAcctTrans.DocumentCode <> 1 THEN RETURN.
  FIND NewDocument OF NewAcctTrans NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(NewDocument) THEN RETURN.
  IF NewDocument.DocumentType = "RCPT" THEN
    signed-amount = - NewAcctTrans.Amount .
END.
DEF BUFFER RT FOR NewAcctTrans.


ON WRITE OF NewAcctTrans NEW NewTrans OLD OldTrans OVERRIDE DO:
  IF NOT summary AND NewTrans.DocumentCode <> 1 THEN RETURN.

  IF NEW NewAcctTrans THEN DO:
    DEF BUFFER LastTrans FOR NewAcctTrans.
    DEF VAR trans-code AS INT NO-UNDO.
  
    FIND LAST LastTrans WHERE LastTrans.BatchCode  = NewAcctTrans.BatchCode
                        AND LastTrans.DocumentCode = NewAcctTrans.DocumentCode
                        NO-LOCK NO-ERROR.

    trans-code = IF AVAILABLE LastTrans THEN LastTrans.TransactionCode + 5 ELSE 1.
    ASSIGN NewAcctTrans.TransactionCode = trans-code.
  END.

  IF summary THEN RETURN.

  FIND RT WHERE RT.BatchCode = OldTrans.BatchCode
                AND RT.DocumentCode = 2
                AND RT.TransactionCode = OldTrans.TransactionCode
                EXCLUSIVE-LOCK NO-ERROR.
  IF NOT AVAILABLE(RT) THEN DO:
    CREATE RT.
    ASSIGN RT.BatchCode = OldTrans.BatchCode
           RT.DocumentCode = 2
           RT.TransactionCode = OldTrans.TransactionCode
           RT.EntityType = bank-et
           RT.EntityCode   = bank-ec
           RT.AccountCode  = bank-ac
            .
    FIND CURRENT RT NO-LOCK.
    FIND CURRENT RT EXCLUSIVE-LOCK.
  END.
  BUFFER-COPY NewTrans TO RT ASSIGN
      RT.DocumentCode = 2
      RT.TransactionCode = NewTrans.TransactionCode
      RT.Description = "BK, " + NewTrans.Description
      RT.EntityType   = bank-et
      RT.EntityCode   = bank-ec
      RT.AccountCode  = bank-ac
      RT.Amount       = NewTrans.Amount * IF mode = "Receipt" THEN 1 ELSE -1 .
  FIND CURRENT RT NO-LOCK.

END.

ON DELETE OF NewAcctTrans DO:

  IF summary THEN RETURN.
  IF NewAcctTrans.DocumentCode <> 1 THEN RETURN.
  DEF BUFFER RT FOR NewAcctTrans.
  FIND RT WHERE RT.BatchCode = NewAcctTrans.BatchCode
                AND RT.DocumentCode = 2
                AND RT.TransactionCode = NewAcctTrans.TransactionCode
                EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE(RT) THEN DELETE RT.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-trn

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES NewBatch
&Scoped-define FIRST-EXTERNAL-TABLE NewBatch


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR NewBatch.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES NewAcctTrans

/* Definitions for BROWSE br-trn                                        */
&Scoped-define FIELDS-IN-QUERY-br-trn NewAcctTrans.EntityType ~
NewAcctTrans.EntityCode NewAcctTrans.AccountCode NewAcctTrans.Amount ~
NewAcctTrans.Date NewAcctTrans.Reference NewAcctTrans.Description 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-trn NewAcctTrans.EntityType ~
NewAcctTrans.EntityCode NewAcctTrans.AccountCode NewAcctTrans.Amount ~
NewAcctTrans.Date NewAcctTrans.Reference NewAcctTrans.Description 
&Scoped-define ENABLED-TABLES-IN-QUERY-br-trn NewAcctTrans
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br-trn NewAcctTrans
&Scoped-define OPEN-QUERY-br-trn OPEN QUERY br-trn FOR EACH NewAcctTrans OF NewDocument NO-LOCK.
&Scoped-define TABLES-IN-QUERY-br-trn NewAcctTrans
&Scoped-define FIRST-TABLE-IN-QUERY-br-trn NewAcctTrans


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br-trn tgl_summary btn_AddTrn btn_Ins-Trn 
&Scoped-Define DISPLAYED-FIELDS NewBatch.Total 
&Scoped-Define DISPLAYED-OBJECTS cmb_bnkact tgl_summary fil_Entity ~
fil_Account 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR W-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_AddTrn 
     LABEL "Add transaction" 
     SIZE 15 BY 1.1.

DEFINE BUTTON btn_Ins-Trn 
     LABEL "Insert" 
     SIZE 5.86 BY 1.15.

DEFINE VARIABLE cmb_bnkact AS CHARACTER FORMAT "X(256)":U 
     LABEL "Bank Account" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     DROP-DOWN-LIST
     SIZE 58.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Account AS CHARACTER FORMAT "X(35)":U 
     VIEW-AS FILL-IN 
     SIZE 58.86 BY .9
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fil_Entity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 58.86 BY .9
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_summary AS LOGICAL INITIAL no 
     LABEL "Banking Summary Only" 
     VIEW-AS TOGGLE-BOX
     SIZE 18.86 BY 1
     FONT 10 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-trn FOR 
      NewAcctTrans SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-trn W-Win _STRUCTURED
  QUERY br-trn NO-LOCK DISPLAY
      NewAcctTrans.EntityType FORMAT "X":U COLUMN-FONT 10
      NewAcctTrans.EntityCode COLUMN-LABEL "Code" FORMAT "99999":U
            WIDTH 5
      NewAcctTrans.AccountCode FORMAT "9999.99":U WIDTH 7
      NewAcctTrans.Amount FORMAT "->>>,>>>,>>9.99":U WIDTH 15
      NewAcctTrans.Date COLUMN-LABEL "  Date" FORMAT "99/99/9999":U
            WIDTH 10
      NewAcctTrans.Reference COLUMN-LABEL "Reference" FORMAT "X(12)":U
            WIDTH 12
      NewAcctTrans.Description FORMAT "X(50)":U WIDTH 50
  ENABLE
      NewAcctTrans.EntityType
      NewAcctTrans.EntityCode
      NewAcctTrans.AccountCode
      NewAcctTrans.Amount
      NewAcctTrans.Date
      NewAcctTrans.Reference
      NewAcctTrans.Description
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 108.43 BY 13.1
         BGCOLOR 16 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     cmb_bnkact AT ROW 1.2 COL 9.86 COLON-ALIGNED
     br-trn AT ROW 4.2 COL 1.57
     tgl_summary AT ROW 1.2 COL 73
     btn_AddTrn AT ROW 1.2 COL 94.86
     fil_Entity AT ROW 2.4 COL 11.86 NO-LABEL
     btn_Ins-Trn AT ROW 2.8 COL 73.57
     NewBatch.Total AT ROW 2.8 COL 91.43 COLON-ALIGNED
          LABEL "Batch Total" FORMAT "-ZZZ,ZZZ,ZZ9.99"
          VIEW-AS FILL-IN 
          SIZE 16.57 BY 1
          FONT 11
     fil_Account AT ROW 3.3 COL 9.86 COLON-ALIGNED NO-LABEL
     "Entity Name :" VIEW-AS TEXT
          SIZE 9.86 BY 1 AT ROW 2.35 COL 2.14
          FONT 10
     "Account :" VIEW-AS TEXT
          SIZE 6.86 BY 1 AT ROW 3.2 COL 4.43
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 112.86 BY 17.8
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   External Tables: TTPL.NewBatch
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW W-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Transaction Entry"
         HEIGHT             = 16.4
         WIDTH              = 109.43
         MAX-HEIGHT         = 37.8
         MAX-WIDTH          = 140.14
         VIRTUAL-HEIGHT     = 37.8
         VIRTUAL-WIDTH      = 140.14
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB W-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{inc/method/m-drlwin.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW W-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   Custom                                                               */
/* BROWSE-TAB br-trn cmb_bnkact F-Main */
ASSIGN 
       br-trn:NUM-LOCKED-COLUMNS IN FRAME F-Main     = 1.

ASSIGN 
       btn_AddTrn:HIDDEN IN FRAME F-Main           = TRUE.

ASSIGN 
       btn_Ins-Trn:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR COMBO-BOX cmb_bnkact IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Account IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_Entity IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
ASSIGN 
       tgl_summary:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN NewBatch.Total IN FRAME F-Main
   NO-ENABLE EXP-LABEL EXP-FORMAT                                       */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
THEN W-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-trn
/* Query rebuild information for BROWSE br-trn
     _TblList          = "TTPL.NewAcctTrans OF TTPL.NewDocument"
     _Options          = "NO-LOCK"
     _FldNameList[1]   > ttpl.NewAcctTrans.EntityType
"NewAcctTrans.EntityType" ? ? "character" ? ? 10 ? ? ? yes ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > ttpl.NewAcctTrans.EntityCode
"NewAcctTrans.EntityCode" "Code" ? "integer" ? ? ? ? ? ? yes ? no no "5" yes no no "U" "" ""
     _FldNameList[3]   > ttpl.NewAcctTrans.AccountCode
"NewAcctTrans.AccountCode" ? ? "decimal" ? ? ? ? ? ? yes ? no no "7" yes no no "U" "" ""
     _FldNameList[4]   > TTPL.NewAcctTrans.Amount
"NewAcctTrans.Amount" ? "->>>,>>>,>>9.99" "decimal" ? ? ? ? ? ? yes ? no no "15" yes no no "U" "" ""
     _FldNameList[5]   > ttpl.NewAcctTrans.Date
"NewAcctTrans.Date" "  Date" ? "date" ? ? ? ? ? ? yes ? no no "10" yes no no "U" "" ""
     _FldNameList[6]   > ttpl.NewAcctTrans.Reference
"NewAcctTrans.Reference" "Reference" "X(12)" "character" ? ? ? ? ? ? yes ? no no "12" yes no no "U" "" ""
     _FldNameList[7]   > ttpl.NewAcctTrans.Description
"NewAcctTrans.Description" ? "X(50)" "character" ? ? ? ? ? ? yes ? no no "50" yes no no "U" "" ""
     _Query            is NOT OPENED
*/  /* BROWSE br-trn */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON END-ERROR OF W-Win /* Transaction Entry */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON WINDOW-CLOSE OF W-Win /* Transaction Entry */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */

  IF trn-modified THEN DO:
    RUN verify-trn.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
  END.

  RUN notify( 'open-query,record-source':U ).

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main W-Win
ON RETURN OF FRAME F-Main
ANYWHERE DO:
  IF FOCUS:TYPE = 'FILL-IN':U THEN
  DO:
    APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-trn
&Scoped-define SELF-NAME br-trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON CTRL-DEL OF br-trn IN FRAME F-Main
OR 'SHIFT-DEL':U OF {&SELF-NAME} ANYWHERE DO:
  RUN delete-trn.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON CTRL-INS OF br-trn IN FRAME F-Main
OR "SHIFT-INS":U OF {&SELF-NAME}
ANYWHERE DO:
  trn-before = IF AVAILABLE NewacctTrans THEN NewacctTrans.TransactionCode ELSE 1.
  trn-inserted = Yes.
  APPLY 'CHOOSE':U TO btn_Ins-Trn.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON ENTRY OF br-trn IN FRAME F-Main
OR 'MOUSE-SELECT-CLICK':U OF {&SELF-NAME} DO:

  IF LOOKUP( LAST-EVENT:LABEL, 'ENTER,TAB,MOUSE-SELECT-UP':U ) = 0 THEN RETURN.
  IF NOT AVAILABLE trn-doc THEN RETURN.
    
  IF NOT ( NUM-RESULTS("br-trn":U) = ? OR
           NUM-RESULTS("br-trn":U) = 0 ) THEN
    IF br-trn:SELECT-FOCUSED-ROW() THEN.

  IF NUM-RESULTS("br-trn":U) = 0 OR NUM-RESULTS( "br-trn" ) = ? THEN
  DO:
    RUN add-new-trn.
    RETURN NO-APPLY.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON ROW-DISPLAY OF br-trn IN FRAME F-Main
DO:
  RUN set-trn-color.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON ROW-ENTRY OF br-trn IN FRAME F-Main
DO:
  RUN check-last.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON ROW-LEAVE OF br-trn IN FRAME F-Main
OR 'CURSOR-DOWN':U OF {&SELF-NAME} 
OR 'CURSOR-UP':U   OF {&SELF-NAME}
OR 'ENTER':U       OF NewAcctTrans.Description IN BROWSE {&SELF-NAME} ANYWHERE DO:

  IF trn-modified THEN
  DO:
    RUN verify-trn.
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
  END.

  IF LAST-EVENT:FUNCTION = 'ROW-LEAVE':U AND
     LAST-EVENT:LABEL = 'ENTER':U THEN
  DO:
    IF last-trn-row THEN
      APPLY 'CHOOSE':U TO Btn_AddTrn IN FRAME {&FRAME-NAME}.
    ELSE
      APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON VALUE-CHANGED OF br-trn IN FRAME F-Main
DO:
  RUN update-trn-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.EntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.EntityType br-trn _BROWSE-COLUMN W-Win
ON ANY-PRINTABLE OF NewAcctTrans.EntityType IN BROWSE br-trn /* T */
DO:
  SELF:SCREEN-VALUE = CAPS( SELF:SCREEN-VALUE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.EntityType br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.EntityType IN BROWSE br-trn /* T */
DO:
  RUN set-trn-color.
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.

  RUN verify-entity-type( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.EntityCode br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.EntityCode IN BROWSE br-trn /* Code */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
  
  RUN verify-entity-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN DO:
    SELF:MODIFIED = no.
    RETURN NO-APPLY.
  END.

  RUN update-description.
  RUN update-entity-name.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.AccountCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.AccountCode br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.AccountCode IN BROWSE br-trn /* Account */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
  
  RUN verify-account-code( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
  DO:
    SELF:MODIFIED = no.
    RETURN NO-APPLY.
  END.

  RUN update-account-name.
  RUN update-description.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.Amount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.Amount br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.Amount IN BROWSE br-trn /* Amount */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
  RUN set-trn-color.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.Date
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.Date br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.Date IN BROWSE br-trn /*   Date */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
  
  RUN verify-date( "Verify" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.Reference
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.Reference br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.Reference IN BROWSE br-trn /* Reference */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME NewAcctTrans.Description
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.Description br-trn _BROWSE-COLUMN W-Win
ON ENTRY OF NewAcctTrans.Description IN BROWSE br-trn /* Description */
DO:
  SELF:FORMAT = "X(100)".
  SELF:SCREEN-VALUE = RIGHT-TRIM( SELF:SCREEN-VALUE ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL NewAcctTrans.Description br-trn _BROWSE-COLUMN W-Win
ON LEAVE OF NewAcctTrans.Description IN BROWSE br-trn /* Description */
DO:
  IF NOT SELF:MODIFIED THEN RETURN.
  trn-modified = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_AddTrn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_AddTrn W-Win
ON CHOOSE OF btn_AddTrn IN FRAME F-Main /* Add transaction */
DO:
  RUN add-new-trn.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_Ins-Trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Ins-Trn W-Win
ON CHOOSE OF btn_Ins-Trn IN FRAME F-Main /* Insert */
DO:
  RUN insert-new-trn.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_bnkact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact W-Win
ON ENTER OF cmb_bnkact IN FRAME F-Main /* Bank Account */
DO:
  APPLY 'TAB':U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_bnkact W-Win
ON VALUE-CHANGED OF cmb_bnkact IN FRAME F-Main /* Bank Account */
DO:
  RUN get-bank-account.
  RUN summary-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_summary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_summary W-Win
ON VALUE-CHANGED OF tgl_summary IN FRAME F-Main /* Banking Summary Only */
DO:
  RUN summary-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK W-Win 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-new-trn W-Win 
PROCEDURE add-new-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE BankAccount THEN
  DO:
    MESSAGE "You must select a bank account before" SKIP
            "entering transactions"
      VIEW-AS ALERT-BOX ERROR TITLE "No Bank Account selected".
    RETURN.
  END.
  
  add-new-trn = Yes.  

  IF NUM-RESULTS("br-trn":U) = ? OR
     NUM-RESULTS("br-trn":U) = 0 THEN
  DO:
    RUN create-trn.
    RUN open-trn-query.
  END.
  ELSE
  DO WITH FRAME {&FRAME-NAME}:
    IF br-trn:REFRESH() THEN.
    IF br-trn:INSERT-ROW("AFTER") THEN.
  END.

  RUN check-last.

  /* Set the default screen-values */
  APPLY 'ENTRY':U TO NewAcctTrans.EntityType IN BROWSE br-trn.

  RUN set-trn-defaults.  

  APPLY 'ENTRY':U TO NewAcctTrans.EntityType IN BROWSE br-trn.
  trn-modified = Yes.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects W-Win  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available W-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "NewBatch"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "NewBatch"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-trn W-Win 
PROCEDURE assign-trn :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR batch-date AS DATE NO-UNDO.
DEF BUFFER RenumberTrn FOR NewAcctTrans.
  
  IF NOT AVAILABLE NewAcctTrans THEN RETURN.
  IF NOT AVAILABLE BankAccount THEN RUN get-bank-account.
  
  FIND CURRENT NewAcctTrans EXCLUSIVE-LOCK.

  ASSIGN
    NewAcctTrans.BatchCode    = NewBatch.BatchCode
    NewAcctTrans.DocumentCode = trn-doc.DocumentCode.

  ASSIGN BROWSE br-trn
    NewAcctTrans.EntityType
    NewAcctTrans.EntityCode
    NewAcctTrans.AccountCode
    NewAcctTrans.Amount
    NewAcctTrans.Date
    NewAcctTrans.Reference
    NewAcctTrans.Description.

  /* If this transaction has been inserted and not added then we must
     re-order all subsequent transactions */
     
  IF trn-inserted THEN DO:

    /* Ensure there are no renumbering collisions with the current transaction */
    ASSIGN NewAcctTrans.TransactionCode = 0.
        
    FOR EACH RenumberTrn WHERE
      RenumberTrn.BatchCode       = NewAcctTrans.BatchCode AND
      RenumberTrn.DocumentCode    = NewAcctTrans.DocumentCode AND
      RenumberTrn.TransactionCode >= trn-before AND
      RenumberTrn.TransactionCode <> NewAcctTrans.TransactionCode
      EXCLUSIVE-LOCK
      BY RenumberTrn.TransactionCode DESCENDING:

      ASSIGN RenumberTrn.TransactionCode = RenumberTrn.TransactionCode + 1.
      
    END.

    ASSIGN NewAcctTrans.TransactionCode = trn-before.

  END.

  FIND CURRENT NewAcctTrans NO-LOCK.

  DEF BUFFER trn FOR NewAcctTrans.
  batch-total = 0.
  FOR EACH trn WHERE trn.BatchCode = NewBatch.BatchCode
                 AND trn.DocumentCode = NewAcctTrans.DocumentCode NO-LOCK:
    batch-total = batch-total + trn.Amount.
  END.
  FIND FIRST trn OF trn-doc NO-LOCK NO-ERROR.

  IF summary THEN DO:
    batch-date = IF AVAILABLE(trn) THEN trn.Date ELSE TODAY.
    FIND ReverseTrans OF rev-doc EXCLUSIVE-LOCK.
    ASSIGN  ReverseTrans.BatchCode    = NewBatch.BatchCode
            ReverseTrans.DocumentCode = rev-doc.DocumentCode
            ReverseTrans.EntityType   = "L"
            ReverseTrans.EntityCode   = BankAccount.CompanyCode
            ReverseTrans.AccountCode  = BankAccount.AccountCode
            ReverseTrans.Date         = batch-date
            ReverseTrans.Amount       = batch-total
            ReverseTrans.Reference    = NewAcctTrans.Reference
            ReverseTrans.Description  = "Banking Summary " + STRING( batch-date, "99/99/9999" ).
  END.
  ELSE
    /* all done with triggers - see DEFINITIONS section */
    
  RUN update-batch-total.
  RUN display-trn.
  trn-modified = No.
  trn-inserted = No.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-last W-Win 
PROCEDURE check-last :
/*------------------------------------------------------------------------------
  Purpose:     Checks to see if the current browse row is the last row
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  last-trn-row = IF trn-inserted THEN No ELSE
                    CURRENT-RESULT-ROW( "br-trn" ) = NUM-RESULTS( "br-trn" ) OR
                    br-trn:NEW-ROW IN FRAME {&FRAME-NAME}.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-trn W-Win 
PROCEDURE check-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN verify-entity-code( "Check" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( NewAcctTrans.EntityCode:HANDLE IN BROWSE br-trn ).

  RUN verify-account-code( "Check" ).
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( NewAcctTrans.AccountCode:HANDLE ).

  RUN verify-date( "Check").
  IF RETURN-VALUE = "FAIL" THEN
    RETURN STRING( NewAcctTrans.Date:HANDLE ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-trn-query W-Win 
PROCEDURE close-trn-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY br-trn.
  last-trn-type = "".
  RUN update-trn-fields.
  RUN update-batch-total.
  RUN update-bank-account.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-trn W-Win 
PROCEDURE create-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CREATE NewAcctTrans.
  ASSIGN
    NewAcctTrans.BatchCode    = NewBatch.BatchCode
    NewAcctTrans.DocumentCode = trn-doc.DocumentCode
    NewAcctTrans.EntityType   = "*"
    NewAcctTrans.Date         = TODAY.

  IF NOT summary THEN RETURN.  /* all done by triggers - see DEFINITIONS section */

  IF NOT CAN-FIND( FIRST ReverseTrans OF rev-doc ) THEN DO:
    /* Create the reversal */
    CREATE ReverseTrans.
    ASSIGN
      ReverseTrans.BatchCode    = NewBatch.BatchCode
      ReverseTrans.DocumentCode = rev-doc.DocumentCode
      ReverseTrans.EntityType   = "L".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-trn W-Win 
PROCEDURE delete-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NUM-RESULTS( "br-trn" ) = 0 OR NUM-RESULTS( "br-trn" ) = ? THEN RETURN.

  IF br-trn:SELECT-FOCUSED-ROW() IN FRAME {&FRAME-NAME} THEN
  DO:

    IF NOT br-trn:NEW-ROW THEN DO:
      IF br-trn:FETCH-SELECTED-ROW(1) THEN.
      IF AVAILABLE NewAcctTrans THEN
      DO TRANSACTION:
        
        GET CURRENT br-trn EXCLUSIVE-LOCK.
 
        IF summary THEN DO:
          IF NUM-RESULTS ( "br-trn" ) = 1 THEN  /* last line so delete reversal */
          DO:
            FIND ReverseTrans OF rev-doc EXCLUSIVE-LOCK NO-ERROR.
            DELETE ReverseTrans.
          END.
          ELSE ReverseTrans.Amount = ReverseTrans.Amount - NewAcctTrans.Amount.
        END.
          /* else done with triggers */
 
        DELETE NewAcctTrans.
        IF br-trn:DELETE-CURRENT-ROW() THEN.
      END.
        
    END.
    ELSE
      IF br-trn:DELETE-CURRENT-ROW() THEN.    
  END.

  IF NUM-RESULTS( "br-trn" ) = ? OR 
     NUM-RESULTS( "br-trn" ) = 0 THEN
  DO:
    RUN close-trn-query.
    RETURN.
  END.
     
  IF br-trn:FETCH-SELECTED-ROW(1) THEN.
  RUN display-trn.

  RUN check-last.
  RUN update-batch-total.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI W-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
  THEN DELETE WIDGET W-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-trn W-Win 
PROCEDURE display-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE(NewAcctTrans) THEN RETURN.

  DISPLAY
    NewAcctTrans.EntityType
    NewAcctTrans.EntityCode
    NewAcctTrans.AccountCode
    NewAcctTrans.Amount
    NewAcctTrans.Date
    NewAcctTrans.Reference
    NewAcctTrans.Description
  WITH BROWSE br-trn.
  
  RUN set-trn-color.
  RUN update-trn-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI W-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cmb_bnkact tgl_summary fil_Entity fil_Account 
      WITH FRAME F-Main IN WINDOW W-Win.
  IF AVAILABLE NewBatch THEN 
    DISPLAY NewBatch.Total 
      WITH FRAME F-Main IN WINDOW W-Win.
  ENABLE br-trn tgl_summary btn_AddTrn btn_Ins-Trn 
      WITH FRAME F-Main IN WINDOW W-Win.
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
  VIEW W-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-bank-account W-Win 
PROCEDURE get-bank-account :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR item-idx AS INT NO-UNDO.

  cmb_bnkact = cmb_bnkact:SCREEN-VALUE .
  item-idx = cmb_bnkact:LOOKUP( cmb_bnkact:SCREEN-VALUE ).
  IF item-idx = 0 THEN RETURN.
  
  FIND BankAccount WHERE ROWID( BankAccount ) =
    TO-ROWID( ENTRY( item-idx, cmb_bnkact:PRIVATE-DATA ) ) NO-LOCK NO-ERROR.

  ASSIGN    bank-ec = BankAccount.CompanyCode
            bank-ac = BankAccount.AccountCode.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-bank-accounts W-Win 
PROCEDURE get-bank-accounts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR item    AS CHAR NO-UNDO.
DEF VAR id-list AS CHAR NO-UNDO.
DEF VAR default-bnkact AS INTEGER NO-UNDO.
DEF VAR current-bnkact AS INTEGER NO-UNDO.

  cmb_bnkact:LIST-ITEMS IN FRAME {&FRAME-NAME} = "".
  default-bnkact = 1.
  current-bnkact = 1.

  IF mode = "Receipt" THEN
    FIND OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "RCPTBANK" NO-LOCK NO-ERROR.
  ELSE
    FIND OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = "PYMTBANK" NO-LOCK NO-ERROR.

  FOR EACH BankAccount NO-LOCK:
    item = BankAccount.BankAccountCode + " - "
         + STRING( BankAccount.CompanyCode, ">>>>9" )   + ' ' 
         + STRING( BankAccount.AccountCode, "9999.99" ) + ' ' +
                   BankAccount.AccountName.
    IF cmb_bnkact:ADD-LAST( item ) THEN.
    id-list = id-list + IF id-list = "" THEN "" ELSE ",".
    id-list = id-list + STRING( ROWID( BankAccount ) ).
    IF AVAILABLE(OfficeControlAccount)
         AND ( BankAccount.BankAccountCode = OfficeControlAccount.Description
             OR (BankAccount.CompanyCode = OfficeControlAccount.EntityCode
                  AND BankAccount.AccountCode = OfficeControlAccount.AccountCode)) THEN DO:
      default-bnkact = current-bnkact.
    END.
    current-bnkact = current-bnkact + 1.
  END.
  cmb_bnkact:PRIVATE-DATA = id-list.
  cmb_bnkact:SCREEN-VALUE = cmb_bnkact:ENTRY( default-bnkact ).

  RUN get-bank-account.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-default-trn W-Win 
PROCEDURE get-default-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND FIRST DefTrans NO-ERROR.
  IF NOT AVAILABLE DefTrans THEN CREATE DefTrans.

  GET PREV br-trn.
  IF AVAILABLE NewAcctTrans THEN DO:
    ASSIGN
      DefTrans.EntityType  = NewAcctTrans.EntityType
      DefTrans.EntityCode  = NewAcctTrans.EntityCode
      DefTrans.AccountCode = NewAcctTrans.AccountCode
      DefTrans.Date        = NewAcctTrans.Date
      DefTrans.Amount      = 0.00
      DefTrans.Reference   = NewAcctTrans.Reference
      DefTrans.Description = NewAcctTrans.Description.

    GET NEXT br-trn.
  END.
  ELSE DO:
  
    DEF BUFFER LastTrn FOR NewacctTrans.
    FIND LAST LastTrn OF trn-doc NO-LOCK NO-ERROR.
   
    ASSIGN
      DefTrans.EntityType  = IF mode = "Receipt" THEN "T" ELSE
                             IF mode = "Payment" THEN "C" ELSE "L"
      DefTrans.EntityCode  = 0
      DefTrans.AccountCode = IF DefTrans.EntityType = "T" THEN sundry-debtors ELSE
                             IF DefTrans.EntityType = "C" THEN sundry-creditors ELSE 0000.00
      DefTrans.Date        = IF AVAILABLE LastTrn THEN LastTrn.Date ELSE TODAY
      DefTrans.Amount      = 0.00
      DefTrans.Reference   = ""
      DefTrans.Description = "".

      IF (mode = "Receipt" AND receipts-description BEGINS "Entity")
            OR (mode = "Payment" AND payments-description BEGINS "Entity")
      THEN .    /* default from entity in update-description */
      ELSE IF mode = "Receipt" THEN
        DefTrans.Description = receipts-description.
      ELSE
        DefTrans.Description = payments-description.

    GET FIRST br-trn.
  END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE insert-new-trn W-Win 
PROCEDURE insert-new-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DO WITH FRAME {&FRAME-NAME}:
    IF br-trn:REFRESH() THEN.
    IF br-trn:INSERT-ROW("BEFORE") THEN.
  END.

  RUN check-last.

  /* Set the default screen-values */
  APPLY 'ENTRY':U TO NewAcctTrans.EntityType IN BROWSE br-trn.

  RUN set-trn-defaults.  

  APPLY 'ENTRY':U TO NewAcctTrans.EntityType IN BROWSE br-trn.
  trn-modified = Yes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize W-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  tgl_summary:HIDDEN IN FRAME {&FRAME-NAME} = (mode <> "Receipt").
  summary = (mode = "Receipt") AND INPUT FRAME {&FRAME-NAME} tgl_summary.

  HIDE btn_AddTrn btn_Ins-Trn.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit W-Win 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
   APPLY "CLOSE":U TO THIS-PROCEDURE.
   
   RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available W-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  IF mode = "" THEN RETURN.
  IF have-records THEN RETURN.

  have-records = Yes.
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .
  RUN setup.
  IF RETURN-VALUE = "FAIL" THEN RUN dispatch( 'exit':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-trn-query W-Win 
PROCEDURE open-trn-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE trn-doc THEN RETURN.
  OPEN QUERY br-trn FOR EACH NewAcctTrans OF trn-doc NO-LOCK.
  RUN display-trn.
  RUN update-batch-total.
  RUN update-trn-fields.
  RUN update-bank-account.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE post-apply-entry W-Win 
PROCEDURE post-apply-entry :
/*------------------------------------------------------------------------------
  Purpose:
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
DEF BUFFER FT FOR NewAcctTrans.
  IF NOT AVAILABLE trn-doc THEN RETURN.
  FIND FIRST FT OF trn-doc NO-LOCK NO-ERROR.
  IF AVAILABLE(FT) THEN
    APPLY 'ENTRY':U TO BROWSE {&BROWSE-NAME}.
  ELSE
    APPLY 'ENTRY':U TO cmb_bnkact.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records W-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "NewBatch"}
  {src/adm/template/snd-list.i "NewAcctTrans"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sensitise-account-code W-Win 
PROCEDURE sensitise-account-code :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR type AS CHAR NO-UNDO
.
  type = INPUT BROWSE br-trn NewAcctTrans.EntityType.

  ASSIGN 
    NewAcctTrans.AccountCode:READ-ONLY = ((type = "C") OR ((type = "T" AND NOT tenant-accounts))).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-trn-color W-Win 
PROCEDURE set-trn-color :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR fcolor AS INT INIT ? NO-UNDO.
DEF VAR bcolor AS INT INIT ? NO-UNDO.
  
DEF VAR type   AS CHAR NO-UNDO.
DEF VAR amount AS DEC  NO-UNDO.

  type = IF CAN-QUERY( NewAcctTrans.EntityType:HANDLE IN BROWSE br-trn, "SCREEN-VALUE" ) THEN
    NewAcctTrans.EntityType:SCREEN-VALUE ELSE NewAcctTrans.EntityType.

  amount = IF CAN-QUERY( NewAcctTrans.Amount:HANDLE IN BROWSE br-trn, "SCREEN-VALUE" ) THEN
    DEC( NewAcctTrans.Amount:SCREEN-VALUE ) ELSE NewAcctTrans.Amount.

  CASE type:
    WHEN "L" THEN ASSIGN fcolor = 1  bcolor = 15.
    WHEN "P" THEN ASSIGN fcolor = 2  bcolor = 15.
    WHEN "T" THEN ASSIGN fcolor = 4  bcolor = 15.
    WHEN "J" THEN ASSIGN fcolor = 6 bcolor = 15.
    WHEN "C" THEN ASSIGN fcolor = 5 bcolor = 15.
  END CASE.

  ASSIGN
    NewAcctTrans.EntityType:FGCOLOR  = fcolor
    NewAcctTrans.EntityCode:FGCOLOR  = fcolor
    NewAcctTrans.AccountCode:FGCOLOR = fcolor
    NewAcctTrans.Date:FGCOLOR        = fcolor
    NewAcctTrans.Amount:FGCOLOR      = IF amount < 0 THEN 12 ELSE 0
    NewAcctTrans.Reference:FGCOLOR   = fcolor
    NewAcctTrans.Description:FGCOLOR = fcolor

    NewAcctTrans.EntityType:BGCOLOR  = bcolor
    NewAcctTrans.EntityCode:BGCOLOR  = bcolor
    NewAcctTrans.AccountCode:BGCOLOR = bcolor
    NewAcctTrans.Date:BGCOLOR        = bcolor
    NewAcctTrans.Amount:BGCOLOR      = bcolor
    NewAcctTrans.Reference:BGCOLOR   = bcolor
    NewAcctTrans.Description:BGCOLOR = bcolor  .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-trn-defaults W-Win 
PROCEDURE set-trn-defaults :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN get-default-trn.
  IF NOT AVAILABLE DefTrans THEN RETURN.  

  ASSIGN
    NewAcctTrans.EntityType:SCREEN-VALUE IN BROWSE br-trn = DefTrans.EntityType
    NewAcctTrans.EntityCode:SCREEN-VALUE  = STRING( DefTrans.EntityCode )
    NewAcctTrans.AccountCode:SCREEN-VALUE = STRING( DefTrans.AccountCode )
    NewAcctTrans.Date:SCREEN-VALUE        = STRING( DefTrans.Date )
    NewAcctTrans.Amount:SCREEN-VALUE      = STRING( DefTrans.Amount )
    NewAcctTrans.Reference:SCREEN-VALUE   = DefTrans.Reference
    NewAcctTrans.Description:SCREEN-VALUE = DefTrans.Description

    NewAcctTrans.EntityType:MODIFIED  = No 
    NewAcctTrans.EntityCode:MODIFIED  = No 
    NewAcctTrans.AccountCode:MODIFIED = No
    NewAcctTrans.Date:MODIFIED        = No       
    NewAcctTrans.Amount:MODIFIED      = No     
    NewAcctTrans.Reference:MODIFIED   = No  
    NewAcctTrans.Description:MODIFIED = No.

  RUN update-trn-fields.
  RUN update-description.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setup W-Win 
PROCEDURE setup :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR trn-type AS CHAR NO-UNDO.
  DEF VAR rev-type AS CHAR NO-UNDO.
    
  trn-type = IF mode = "Receipt" THEN "RCPT" ELSE "PYMT".
  rev-type = IF mode = "Receipt" THEN "RCBK" ELSE "PYBK".

  FIND FIRST trn-doc OF NewBatch NO-LOCK NO-ERROR.
  batch-total = NewBatch.Total.
  
  IF NOT AVAILABLE trn-doc THEN DO:
    
    CREATE trn-doc.
    ASSIGN
      trn-doc.BatchCode    = NewBatch.BatchCode
      trn-doc.DocumentType = trn-type
      trn-doc.Reference    = ""
      trn-doc.Description  = NewBatch.Description.

    CREATE rev-doc.
    ASSIGN
      rev-doc.BatchCode    = NewBatch.BatchCode
      rev-doc.DocumentType = rev-type
      rev-doc.Reference    = trn-doc.Reference
      rev-doc.Description = 'BK, ' + trn-doc.Description.

    FIND CURRENT NewBatch EXCLUSIVE-LOCK.
    NewBatch.DocumentCount = 2.
    FIND CURRENT NewBatch NO-LOCK.
  END.
  ELSE IF trn-doc.DocumentType <> trn-type THEN DO:
    MESSAGE mode + "s cannot be added to this batch"
      VIEW-AS ALERT-BOX ERROR TITLE "Non " + mode + " Batch Selected".
    RETURN "FAIL".
    
    /* Need to return fail and run destroy in caller because there is
       still a transaction in scope ? */
    /* Cannot destroy the current procedure without doing this */
  END.
  ELSE
    FIND FIRST rev-doc OF NewBatch WHERE rev-doc.DocumentType = rev-type 
      NO-LOCK NO-ERROR.

  RUN open-trn-query.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed W-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE summary-changed W-Win 
PROCEDURE summary-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF BUFFER trn FOR NewAcctTrans.
  
  summary = mode = "Receipt" AND INPUT FRAME {&FRAME-NAME} tgl_summary.
  FOR EACH ReverseTrans OF rev-doc: DELETE ReverseTrans. END.
  
  IF summary THEN
  DO:
    FIND FIRST trn OF trn-doc NO-LOCK NO-ERROR.
    CREATE ReverseTrans.
    ASSIGN
      ReverseTrans.BatchCode    = NewBatch.BatchCode
      ReverseTrans.DocumentCode = rev-doc.DocumentCode
      ReverseTrans.EntityType   = "L"
      ReverseTrans.EntityCode   = BankAccount.CompanyCode
      ReverseTrans.AccountCode  = BankAccount.AccountCode
      ReverseTrans.Amount       = 0
      ReverseTrans.Date         = (IF AVAILABLE(trn) THEN trn.Date ELSE TODAY)
      ReverseTrans.Reference    = ""
      ReverseTrans.Description  = "Banking Summary " + STRING( (IF AVAILABLE(trn) THEN trn.Date ELSE TODAY), "99/99/9999" ).

    FOR EACH trn OF trn-doc NO-LOCK:
      ReverseTrans.Amount = ReverseTrans.Amount + trn.Amount.
    END.
  END.
  ELSE
  DO:
    ON WRITE OF NewAcctTrans OVERRIDE DO: END.
    FOR EACH trn OF trn-doc NO-LOCK:
      CREATE ReverseTrans.
      ASSIGN
        ReverseTrans.BatchCode    = NewBatch.BatchCode
        ReverseTrans.DocumentCode = rev-doc.DocumentCode
        ReverseTrans.TransactionCode = trn.TransactionCode
        ReverseTrans.EntityType   = "L"
        ReverseTrans.EntityCode   = BankAccount.CompanyCode
        ReverseTrans.AccountCode  = BankAccount.AccountCode
        ReverseTrans.Date         = trn.Date
        ReverseTrans.Amount       = trn.Amount
        ReverseTrans.Reference    = trn.Reference
        ReverseTrans.Description  = "BK, " + trn.Description.
    END.
    ON WRITE OF NewAcctTrans REVERT.        
  END.
    
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-account-name W-Win 
PROCEDURE update-account-name :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NUM-RESULTS( "br-trn" ) = 0 OR NUM-RESULTS( "br-trn" ) = ? THEN
  DO:
    fil_Account:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
    RETURN.
  END.

  DEF VAR code AS DEC NO-UNDO.
  code = INPUT BROWSE br-trn NewAcctTrans.AccountCode.

  FIND FIRST ChartOfAccount WHERE ChartOfAccount.AccountCode = code NO-LOCK NO-ERROR.
  
  ASSIGN fil_Account:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
    IF AVAILABLE ChartOfAccount THEN ChartOfAccount.Name ELSE "".
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-bank-account W-Win 
PROCEDURE update-bank-account :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NOT AVAILABLE trn-doc OR NOT AVAILABLE rev-doc THEN RETURN.
  
  DEF BUFFER FirstTrans FOR NewAcctTrans.
    
  FIND FIRST FirstTrans OF rev-doc NO-LOCK NO-ERROR.
  ASSIGN cmb_bnkact:SENSITIVE IN FRAME {&FRAME-NAME} = Yes.
/*  ASSIGN cmb_bnkact:SENSITIVE IN FRAME {&FRAME-NAME} = NOT AVAILABLE FirstTrans. */

  IF AVAILABLE FirstTrans THEN
  DO WITH FRAME {&FRAME-NAME}:

    FIND FIRST BankAccount WHERE
      BankAccount.CompanyCode = FirstTrans.EntityCode  AND
      BankAccount.AccountCode = FirstTrans.AccountCode NO-LOCK NO-ERROR.
      
    IF NOT AVAILABLE BankAccount THEN RETURN.
    
    DEF VAR item-idx AS INT NO-UNDO.
    item-idx = LOOKUP( STRING( ROWID( BankAccount ) ), cmb_bnkact:PRIVATE-DATA ).
    cmb_bnkact:SCREEN-VALUE = IF item-idx = 0 THEN "" ELSE cmb_bnkact:ENTRY( item-idx ).
    
    /* Check the banking only summary toggle if there is only one banking
       record and the description begins with "Banking Summary" */
    
    IF mode = "Receipt" THEN DO:
      IF FirstTrans.Description BEGINS "Banking Summary" THEN DO:
        FIND NEXT FirstTrans OF rev-doc NO-LOCK NO-ERROR.
        tgl_summary = NOT AVAILABLE FirstTrans.
      END.
      ELSE tgl_summary = No.
      summary = tgl_summary.
      DISPLAY tgl_summary WITH FRAME {&FRAME-NAME}.
    END.
    
  END.

  RUN get-bank-account.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-batch-total W-Win 
PROCEDURE update-batch-total :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  NewBatch.Total:SCREEN-VALUE = STRING( batch-total, NewBatch.Total:FORMAT ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-description W-Win 
PROCEDURE update-description :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR account-code AS DEC NO-UNDO.
DEF VAR entity-type LIKE EntityType.EntityType   NO-UNDO.
DEF VAR entity-code LIKE NewAcctTrans.EntityCode NO-UNDO.
DEF VAR trn-desc AS CHAR NO-UNDO.
   
  account-code = INPUT BROWSE br-trn NewAcctTrans.AccountCode.
  entity-type  = INPUT BROWSE br-trn NewAcctTrans.EntityType.
  entity-code  = INPUT BROWSE br-trn NewAcctTrans.EntityCode.

  IF (mode = "Receipt" AND receipts-description BEGINS "Entity")
            OR (mode = "Payment" AND payments-description BEGINS "Entity")
  THEN DO:
    CASE entity-type:
      WHEN 'T' THEN DO:
        FIND Tenant WHERE Tenant.TenantCode = entity-code NO-LOCK NO-ERROR.
        IF AVAILABLE Tenant THEN trn-desc = /* "(T" + STRING( Tenant.TenantCode, "99999" ) + ") - " + */
                                            Tenant.Name.
      END.
  
      WHEN 'C' THEN DO:
        FIND Creditor WHERE Creditor.CreditorCode = entity-code NO-LOCK NO-ERROR.
        IF AVAILABLE Creditor THEN trn-desc = /* "(C" + STRING( Creditor.CreditorCode, "99999" ) + ") - " + */
                                              Creditor.Name.
      END.
    
      OTHERWISE DO:
        FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = account-code NO-LOCK NO-ERROR.
        IF AVAILABLE ChartOfAccount THEN trn-desc = ChartOfAccount.Name.
      END.
    END.
  END.
  ELSE IF mode = "Receipt" THEN
    trn-desc = receipts-description.
  ELSE 
    trn-desc = payments-description.

  ASSIGN NewAcctTrans.Description:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = trn-desc.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-fields W-Win 
PROCEDURE update-entity-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT BROWSE br-trn NewacctTrans.EntityType.

  ASSIGN
    NewAcctTrans.EntityCode:SCREEN-VALUE  IN BROWSE br-trn = STRING( 0 )
    NewAcctTrans.EntityCode:MODIFIED = No
    NewAcctTrans.AccountCode:SCREEN-VALUE =
      IF type = 'T' THEN STRING( sundry-debtors )   ELSE
      IF type = 'C' THEN STRING( sundry-creditors ) ELSE STRING( 0000.00 )
    NewAcctTrans.AccountCode:MODIFIED = No
    NewAcctTrans.AccountCode:MODIFIED = No.
  
  RUN update-trn-fields.
  RUN update-description.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-entity-name W-Win 
PROCEDURE update-entity-name :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF NUM-RESULTS( "br-trn" ) = 0 OR NUM-RESULTS( "br-trn" ) = ? THEN DO:
    fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
    RETURN.
  END.
 
  DEF VAR type AS CHAR NO-UNDO.
  DEF VAR code AS INT  NO-UNDO.
  type = INPUT BROWSE br-trn NewAcctTrans.EntityType.
  code = INPUT BROWSE br-trn NewAcctTrans.EntityCode.
  
  CASE type:
  
    WHEN 'T' THEN
    DO:
      FIND FIRST Tenant WHERE Tenant.TenantCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
        IF AVAILABLE Tenant THEN Tenant.Name ELSE "".
    END.

    WHEN 'C' THEN
    DO:
      FIND FIRST Creditor WHERE Creditor.CreditorCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
        IF AVAILABLE Creditor THEN Creditor.Name ELSE "".
    END.
  
    WHEN 'P' THEN
    DO:
      FIND FIRST Property WHERE Property.PropertyCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
        IF AVAILABLE Property THEN Property.Name ELSE "".
    END.
  
    WHEN 'L' THEN
    DO:
      FIND FIRST Company WHERE Company.CompanyCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
        IF AVAILABLE Company THEN Company.LegalName ELSE "".
    END.
  
    WHEN 'J' THEN
    DO:
      FIND FIRST Project WHERE Project.ProjectCode = code NO-LOCK NO-ERROR.
      ASSIGN fil_Entity:SCREEN-VALUE IN FRAME {&FRAME-NAME} =
        IF AVAILABLE Project THEN Project.Name ELSE "".
    END.

    OTHERWISE fil_Entity:SCREEN-VALUE = "".
    
  END CASE.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-trn W-Win 
PROCEDURE update-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Create a new line if needed and update the details */

  IF br-trn:NEW-ROW IN FRAME {&FRAME-NAME} THEN DO:
    RUN create-trn.
    IF br-trn:CREATE-RESULT-LIST-ENTRY() THEN.
  END.

  RUN assign-trn.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-trn-fields W-Win 
PROCEDURE update-trn-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN sensitise-account-code.
  RUN update-entity-name.
  RUN update-account-name.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-mode W-Win 
PROCEDURE use-mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.

  mode = new-mode.

  RUN get-bank-accounts.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE user-layout W-Win 
PROCEDURE user-layout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-account-code W-Win 
PROCEDURE verify-account-code :
/*------------------------------------------------------------------------------
  Purpose:     Verify that the transaction's account is a valid
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER mode AS CHAR NO-UNDO.
  
DEF VAR type AS CHAR NO-UNDO.
DEF VAR code AS INT  NO-UNDO.
DEF VAR acct AS DEC  NO-UNDO.
DEF VAR success AS LOGI INITIAL Yes NO-UNDO.

  type = INPUT BROWSE br-trn NewAcctTrans.EntityType.
  code = INPUT BROWSE br-trn NewAcctTrans.EntityCode.
  acct = INPUT BROWSE br-trn NewAcctTrans.AccountCode.

  /* Check to see if the account exists */
  IF type = "J" THEN
    success = CAN-FIND( ProjectBudget WHERE ProjectBudget.ProjectCode = code
                                AND ProjectBudget.AccountCode = acct ).
  ELSE
    success = CAN-FIND( ChartOfAccount WHERE ChartOfAccount.AccountCode = acct ).

  IF NOT success THEN DO:
    IF mode = "Verify" THEN
      MESSAGE "The account code " + STRING( acct ) + " is invalid !"
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
    RETURN "FAIL".
  END.

  IF type = "J" AND restrict-project-posting THEN DO:  
    FIND ProjectBudget WHERE ProjectBudget.ProjectCode = code
                         AND ProjectBudget.AccountCode = acct NO-LOCK.
    IF NOT ProjectBudget.AllowPosting THEN DO:
      IF mode = "Verify" THEN DO:
        FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
        MESSAGE "Project account " + STRING( acct ) + " cannot be updated to."
                 VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
      END.
      RETURN "FAIL".
    END.
  END.
  ELSE IF type <> "J" THEN DO:  
    FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = acct NO-LOCK.

    /* Check to see if we can update to this account for the given entity type */

    IF INDEX( ChartOfAccount.UpdateTo, type ) = 0 THEN DO:
      IF mode = "Verify" THEN DO:
        FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
        MESSAGE "Account " + STRING( acct ) + " cannot be updated to a " + 
                 EntityType.Description
                 VIEW-AS ALERT-BOX ERROR TITLE "Invalid Account Code".
      END.
      RETURN "FAIL".
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-date W-Win 
PROCEDURE verify-date :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER v-mode AS CHAR NO-UNDO.

  DEF VAR trn-date      AS DATE NO-UNDO.
  DEF VAR date-min      AS DATE NO-UNDO.
  DEF VAR date-max      AS DATE NO-UNDO.
  DEF VAR beep-date-min AS DATE NO-UNDO.
  DEF VAR beep-date-max AS DATE NO-UNDO.
  
  trn-date = INPUT BROWSE br-trn NewacctTrans.Date.
  date-min = TODAY - 330.
  date-max = TODAY + 30.
  beep-date-min = TODAY - 30.
  beep-date-max = TODAY.
  
  IF trn-date < date-min OR trn-date > date-max THEN
  DO:
    IF v-mode = "Verify" THEN
      MESSAGE "The date you have entered is either too old " SKIP
              "or to far in the future."
        VIEW-AS ALERT-BOX ERROR.
    RETURN "FAIL".
  END.
  
  IF v-mode = "Verify" AND 
    ( trn-date < beep-date-min OR trn-date > beep-date-max ) THEN BELL.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-entity-code W-Win 
PROCEDURE verify-entity-code :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER v-mode AS CHAR NO-UNDO.

  DEF VAR entity-ok AS LOGI INIT No NO-UNDO.
  DEF VAR type AS CHAR NO-UNDO.
  DEF VAR code AS INT NO-UNDO.
  type = INPUT BROWSE br-trn NewAcctTrans.EntityType.
  code = INPUT BROWSE br-trn NewAcctTrans.EntityCode.

  entity-ok = 
    IF type = 'T' THEN CAN-FIND( FIRST Tenant   WHERE Tenant.TenantCode = code )     ELSE
    IF type = 'C' THEN CAN-FIND( FIRST Creditor WHERE Creditor.CreditorCode = code ) ELSE
    IF type = 'P' THEN CAN-FIND( FIRST Property WHERE Property.PropertyCode = code ) ELSE
    IF type = 'L' THEN CAN-FIND( FIRST Company  WHERE Company.CompanyCode = code )   ELSE
    IF type = 'J' THEN CAN-FIND( FIRST Project  WHERE Project.ProjectCode = code )   ELSE
      No.
      
  IF NOT entity-ok THEN
  DO:
    IF v-mode = "Verify" THEN
    DO:
      FIND FIRST EntityType WHERE EntityType.EntityType = type NO-LOCK.
      MESSAGE "Therse is no " + EntityType.Description + " with code " + STRING( code )
        VIEW-AS ALERT-BOX ERROR TITLE "Entity Code Error - " + EntityType.Description.
    END.
    RETURN "FAIL".
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-entity-type W-Win 
PROCEDURE verify-entity-type :
/*------------------------------------------------------------------------------
  Purpose:     Verify that the entity type is valid
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER v-mode AS CHAR NO-UNDO.

  DEF VAR type AS CHAR NO-UNDO.
  type = INPUT BROWSE br-trn NewacctTrans.EntityType.

  IF NOT CAN-FIND( FIRST Entitytype WHERE EntityType.EntityType = type ) THEN
  DO:
    IF v-mode = "Verify" THEN
      MESSAGE "The entity type " + type + " is invalid !"
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid Entity Type".
    RETURN "FAIL".
  END.

  IF v-mode = "Verify" AND (
     mode = "Receipt" AND type <> "T" OR
     mode = "Payment" AND type <> "C" ) THEN BELL.

  NewAcctTrans.EntityType:SCREEN-VALUE IN BROWSE br-trn =
    CAPS( INPUT BROWSE br-trn NewAcctTrans.EntityType ).
         
  RUN update-entity-fields.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-trn W-Win 
PROCEDURE verify-trn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR err-field AS HANDLE NO-UNDO.

  RUN check-trn.
  err-field = WIDGET-HANDLE( RETURN-VALUE ).

  IF VALID-HANDLE( err-field ) THEN
  DO:
    DEF VAR abort-it AS LOGI NO-UNDO INITIAL Yes.
    MESSAGE "The current transaction is in error." SKIP
      "Do you want to abort the changes ? "
      VIEW-AS ALERT-BOX ERROR BUTTONS YES-NO UPDATE abort-it.
    IF abort-it THEN
    DO:
      IF AVAILABLE NewAcctTrans AND NewAcctTrans.EntityType <> "*" THEN
      DO:
        RUN display-trn.
      END.
      ELSE
        RUN delete-trn.
      trn-modified = No.
      RETURN "FAIL".
    END.
    ELSE
    DO:
      IF AVAILABLE NewAcctTrans  THEN GET CURRENT br-trn.
      APPLY 'ENTRY':U TO err-field.
      RETURN "FAIL".
    END.
  END.
  ELSE
    RUN update-trn.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

