&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS W-Win 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR reference   AS CHAR FORMAT "X(12)" LABEL "Reference" NO-UNDO.
DEF VAR description AS CHAR FORMAT "X(60)" LABEL "Description" NO-UNDO.

DEF VAR sundry-creditors LIKE ChartOfAccount.AccountCode NO-UNDO.
DEF VAR sundry-debtors   LIKE ChartOfAccount.AccountCode NO-UNDO.

DEF VAR source-table AS CHAR NO-UNDO.


DEFINE WORK-TABLE OpenTrans NO-UNDO
  FIELD rowid AS ROWID
  FIELD state AS CHAR FORMAT "X" COLUMN-LABEL "S"
  FIELD Date LIKE AcctTran.Date
  FIELD Reference LIKE AcctTran.Reference
  FIELD Description LIKE AcctTran.Description
  FIELD Amount LIKE AcctTran.Amount.

{inc/ofc-this.i}
FIND OfficeControlAccount NO-LOCK OF Office WHERE OfficeControlAccount.Name = "DEBTORS".
sundry-debtors = OfficeControlAccount.AccountCode.
FIND OfficeControlAccount NO-LOCK OF Office WHERE OfficeControlAccount.Name = "CREDITORS".
sundry-creditors = OfficeControlAccount.AccountCode.

{inc/ofc-set-l.i "Multi-Ledger-Creditors" "multi-ledger-creditors"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-trn

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES Tenant Creditor
&Scoped-define FIRST-EXTERNAL-TABLE Tenant


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR Tenant, Creditor.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES OpenTrans

/* Definitions for BROWSE br-trn                                        */
&Scoped-define FIELDS-IN-QUERY-br-trn OpenTrans.State OpenTrans.Date OpenTrans.Reference OpenTrans.Description OpenTrans.Amount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-trn   
&Scoped-define SELF-NAME br-trn
&Scoped-define QUERY-STRING-br-trn FOR EACH OpenTrans
&Scoped-define OPEN-QUERY-br-trn OPEN QUERY BROWSE-2 FOR EACH OpenTrans.
&Scoped-define TABLES-IN-QUERY-br-trn OpenTrans
&Scoped-define FIRST-TABLE-IN-QUERY-br-trn OpenTrans


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-br-trn}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fil_Entity br-trn btn_all btn_none ~
btn_ReOpen fil_PartCloseAs btn_close-part btn_close btn_cancel ~
fil_EntityType RECT-4 
&Scoped-Define DISPLAYED-OBJECTS fil_Entity fil_entityname fil_total ~
fil_PartCloseAs fil_EntityType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR W-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_all 
     LABEL "Select &All" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 10 BY 1.05
     FONT 9.

DEFINE BUTTON btn_close 
     LABEL "Close &Group" 
     SIZE 10.86 BY 1.05
     FONT 9.

DEFINE BUTTON btn_close-part 
     LABEL "&Partial Close" 
     SIZE 11.43 BY 1.05
     FONT 9.

DEFINE BUTTON btn_none 
     LABEL "&Select &None" 
     SIZE 12.57 BY 1.05
     FONT 9.

DEFINE BUTTON btn_ReOpen 
     LABEL "&Re-Open Group" 
     SIZE 13.14 BY 1.05
     FONT 10.

DEFINE VARIABLE fil_Entity AS INTEGER FORMAT "999999":U INITIAL ? 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_entityname AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49.14 BY 1
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fil_EntityType AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 6.86 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_PartCloseAs AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49.14 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_total AS DECIMAL FORMAT "ZZZ,ZZZ,ZZ9.99CR":U INITIAL 0 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 18.29 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 97.14 BY 19.4.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-trn FOR 
      OpenTrans SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-trn W-Win _FREEFORM
  QUERY br-trn NO-LOCK DISPLAY
      OpenTrans.State
    OpenTrans.Date
    OpenTrans.Reference
    OpenTrans.Description
    OpenTrans.Amount
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS MULTIPLE SIZE 96 BY 16.6 ROW-HEIGHT-CHARS .5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_Entity AT ROW 1.4 COL 7 COLON-ALIGNED NO-LABEL
     fil_entityname AT ROW 1.4 COL 16.14 COLON-ALIGNED NO-LABEL
     br-trn AT ROW 2.6 COL 2.14
     btn_all AT ROW 19.4 COL 2.14
     btn_none AT ROW 19.4 COL 13.57
     btn_ReOpen AT ROW 19.4 COL 27.86
     fil_total AT ROW 19.4 COL 77.86 COLON-ALIGNED
     fil_PartCloseAs AT ROW 20.8 COL 8.72 COLON-ALIGNED NO-LABEL
     btn_close-part AT ROW 20.8 COL 59.86
     btn_close AT ROW 20.8 COL 74.14
     btn_cancel AT ROW 20.8 COL 87.86
     fil_EntityType AT ROW 1.4 COL 2.14 NO-LABEL
     RECT-4 AT ROW 1.2 COL 1.57
     "Part Close As" VIEW-AS TEXT
          SIZE 9.14 BY 1 AT ROW 20.8 COL 1.57
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   External Tables: TTPL.Tenant,TTPL.Creditor
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW W-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Manual Transaction Closing"
         HEIGHT             = 21.05
         WIDTH              = 98.14
         MAX-HEIGHT         = 50.3
         MAX-WIDTH          = 182.86
         VIRTUAL-HEIGHT     = 50.3
         VIRTUAL-WIDTH      = 182.86
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB W-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{inc/method/m-drlwin.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW W-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   Size-to-Fit                                                          */
/* BROWSE-TAB br-trn fil_entityname F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE.

/* SETTINGS FOR FILL-IN fil_entityname IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_EntityType IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fil_total IN FRAME F-Main
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
THEN W-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-trn
/* Query rebuild information for BROWSE br-trn
     _START_FREEFORM
OPEN QUERY BROWSE-2 FOR EACH OpenTrans.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is OPENED
*/  /* BROWSE br-trn */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON END-ERROR OF W-Win /* Manual Transaction Closing */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON WINDOW-CLOSE OF W-Win /* Manual Transaction Closing */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-trn
&Scoped-define SELF-NAME br-trn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON ROW-DISPLAY OF br-trn IN FRAME F-Main
DO:
  IF AVAILABLE OpenTrans THEN
    ASSIGN OpenTrans.Amount:FGCOLOR IN BROWSE br-trn =
      IF OpenTrans.Amount < 0 THEN 12 ELSE ?.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-trn W-Win
ON VALUE-CHANGED OF br-trn IN FRAME F-Main
DO:
  RUN selection-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_all
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_all W-Win
ON CHOOSE OF btn_all IN FRAME F-Main /* Select All */
DO:
  RUN select-all.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_cancel W-Win
ON CHOOSE OF btn_cancel IN FRAME F-Main /* Cancel */
DO:
  RUN dispatch( 'exit':U ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_close
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_close W-Win
ON CHOOSE OF btn_close IN FRAME F-Main /* Close Group */
DO:
  DEF VAR retry-it AS LOGI INIT Yes.
  
  DO WHILE retry-it:
  
    RUN close-transactions("F").
    IF RETURN-VALUE = "FAIL" THEN
      MESSAGE "A database error has occured" VIEW-AS ALERT-BOX ERROR
        BUTTONS RETRY-CANCEL TITLE "Error closing transactions" 
        UPDATE retry-it.
    ELSE
    DO:
      MESSAGE "Transactions successfully closed" VIEW-AS ALERT-BOX INFORMATION.
      RUN open-trn-query.
      RUN selection-changed.
      retry-it = No.
    END.
  END.
  APPLY 'ENTRY':U TO fil_Entity .
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_close-part
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_close-part W-Win
ON CHOOSE OF btn_close-part IN FRAME F-Main /* Partial Close */
DO:
  DEF VAR retry-it AS LOGI INIT Yes NO-UNDO.
  
  DO WHILE retry-it:
  
    RUN close-transactions("P").
    IF RETURN-VALUE = "FAIL" THEN
      MESSAGE "A database error has occured" VIEW-AS ALERT-BOX ERROR
        BUTTONS RETRY-CANCEL TITLE "Error closing transactions" 
        UPDATE retry-it.
    ELSE
    DO:
      MESSAGE "Transactions successfully closed" VIEW-AS ALERT-BOX INFORMATION.
      RUN open-trn-query.
      RUN selection-changed.
      retry-it = No.
    END.
  END.
  APPLY 'ENTRY':U TO fil_Entity .
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_none
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_none W-Win
ON CHOOSE OF btn_none IN FRAME F-Main /* Select None */
DO:
  RUN select-none.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_ReOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_ReOpen W-Win
ON CHOOSE OF btn_ReOpen IN FRAME F-Main /* Re-Open Group */
DO:
  RUN re-open-group.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Entity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Entity W-Win
ON LEAVE OF fil_Entity IN FRAME F-Main
DO:
  IF fil_entity <> INPUT FRAME {&FRAME-NAME} fil_entity THEN
    RUN entity-changed.
  IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_PartCloseAs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PartCloseAs W-Win
ON ANY-PRINTABLE OF fil_PartCloseAs IN FRAME F-Main
DO:
  IF TRIM(SELF:SCREEN-VALUE) <> "" THEN
    ENABLE btn_close-part WITH FRAME {&FRAME-NAME}.
  ELSE
    DISABLE btn_close-part WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_PartCloseAs W-Win
ON LEAVE OF fil_PartCloseAs IN FRAME F-Main
DO:
  RUN selection-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK W-Win 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects W-Win  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available W-Win 
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  IF {inc/availrow.i "list" "Tenant"}
  ELSE IF {inc/availrow.i "list" "Creditor"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  IF {inc/availrow.i "find" "Tenant"}
  ELSE IF {inc/availrow.i "find" "Creditor"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-transactions W-Win 
PROCEDURE close-transactions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-state AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  DEF VAR i AS INT NO-UNDO.
  DEF VAR closing-group LIKE AcctTran.ClosingGroup NO-UNDO.
  DEF VAR entity-type   LIKE AcctTran.EntityType   NO-UNDO.
  DEF VAR entity-code   LIKE AcctTran.EntityCode   NO-UNDO.
  DEF VAR account-code  LIKE AcctTran.AccountCode  NO-UNDO.
  DEF VAR last-date     AS DATE NO-UNDO.
  
  DEF BUFFER LastAcctTran FOR AcctTran.

  entity-type  = IF AVAILABLE Tenant THEN 'T' ELSE 'C'.
  entity-code  = IF AVAILABLE Tenant THEN Tenant.TenantCode ELSE Creditor.CreditorCode.
  account-code = IF AVAILABLE Tenant THEN sundry-debtors ELSE sundry-creditors.

  last-date = DATE( 1, 1, 1).
  close-transactions:
  DO TRANSACTION ON ERROR UNDO close-transactions, RETURN "FAIL":

    closing-group = ?.
    DO i = 1 TO br-trn:NUM-SELECTED-ROWS:
      IF br-trn:FETCH-SELECTED-ROW( i ) THEN.
      FIND CURRENT OpenTrans.
      IF OpenTrans.State = "P" THEN DO:
        FIND ClosingGroup EXCLUSIVE-LOCK WHERE ROWID(ClosingGroup) = OpenTrans.rowid.
        FOR EACH AcctTran EXCLUSIVE-LOCK OF ClosingGroup:
          ASSIGN    AcctTran.ClosingGroup = (IF closing-group = ? THEN ClosingGroup.ClosingGroup ELSE closing-group)
                    AcctTran.ClosedState = new-state.
          last-date = MAXIMUM( AcctTran.Date, last-date ).
        END.
        IF closing-group = ? THEN ASSIGN
          closing-group = ClosingGroup.ClosingGroup
          ClosingGroup.ClosedStatus = new-state
          ClosingGroup.Description  = (IF new-state = "P" THEN INPUT fil_PartCloseAs ELSE "").
        ELSE DO:
          DELETE ClosingGroup.
        END.
      END.
    END.

    IF closing-group = ? THEN DO:
      FIND LAST ClosingGroup WHERE ClosingGroup.EntityType  = entity-type
        AND ClosingGroup.EntityCode  = entity-code
        AND ClosingGroup.AccountCode = account-code
        AND ClosingGroup.ClosingGroup > 0
        NO-LOCK NO-ERROR.

      closing-group = IF AVAILABLE(ClosingGroup) AND ClosingGroup.ClosingGroup <> ? THEN
                         ClosingGroup.ClosingGroup + 1 ELSE 1.

      CREATE ClosingGroup.
      ASSIGN
        ClosingGroup.EntityType   = entity-type
        ClosingGroup.EntityCode   = entity-code
        ClosingGroup.AccountCode  = account-code
        ClosingGroup.ClosingGroup = closing-group
        ClosingGroup.DateClosed   = TODAY
        ClosingGroup.Description  = (IF new-state = "P" THEN INPUT fil_PartCloseAs ELSE "").
    END.
    ELSE
      FIND ClosingGroup WHERE ClosingGroup.EntityType  = entity-type
                    AND ClosingGroup.EntityCode  = entity-code
                    AND ClosingGroup.AccountCode = account-code
                    AND ClosingGroup.ClosingGroup = closing-group NO-LOCK.

    DO i = 1 TO br-trn:NUM-SELECTED-ROWS:
      IF br-trn:FETCH-SELECTED-ROW(i) THEN.
      FIND CURRENT OpenTrans.
      IF OpenTrans.State = "O" THEN DO:
        FIND AcctTran EXCLUSIVE-LOCK WHERE ROWID(AcctTran) = OpenTrans.rowid.
        ASSIGN  AcctTran.ClosingGroup = closing-group
                AcctTran.ClosedState = new-state.
        last-date = MAXIMUM( AcctTran.Date, last-date ).
        FIND CURRENT AcctTran NO-LOCK.
      END.
    END.

    FIND ClosingGroup WHERE ClosingGroup.EntityType  = entity-type
                    AND ClosingGroup.EntityCode  = entity-code
                    AND ClosingGroup.AccountCode = account-code
                    AND ClosingGroup.ClosingGroup = closing-group EXCLUSIVE-LOCK.

    ClosingGroup.ClosedStatus = new-state.
    ClosingGroup.DateClosed = last-date.

    FIND ClosingGroup WHERE ClosingGroup.EntityType  = entity-type
                    AND ClosingGroup.EntityCode  = entity-code
                    AND ClosingGroup.AccountCode = account-code
                    AND ClosingGroup.ClosingGroup = closing-group NO-LOCK.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE close-trn-query W-Win 
PROCEDURE close-trn-query :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  CLOSE QUERY br-trn.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI W-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
  THEN DELETE WIDGET W-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI W-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fil_Entity fil_entityname fil_total fil_PartCloseAs fil_EntityType 
      WITH FRAME F-Main IN WINDOW W-Win.
  ENABLE fil_Entity br-trn btn_all btn_none btn_ReOpen fil_PartCloseAs 
         btn_close-part btn_close btn_cancel fil_EntityType RECT-4 
      WITH FRAME F-Main IN WINDOW W-Win.
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
  VIEW W-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-changed W-Win 
PROCEDURE entity-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR unavailable AS LOGI INIT Yes NO-UNDO.
  DEF VAR entity-name AS CHAR NO-UNDO.
  
  IF source-table = 'Creditor' THEN
  DO:
    FIND Creditor WHERE Creditor.CreditorCode = INPUT FRAME {&FRAME-NAME}
      fil_entity NO-LOCK NO-ERROR.
    ASSIGN entity-name = Creditor.Name NO-ERROR.  
    unavailable = NOT AVAILABLE Creditor.
  END.
  ELSE IF source-table = 'Tenant' THEN
  DO:
    FIND Tenant WHERE Tenant.TenantCode = INPUT FRAME {&FRAME-NAME}
      fil_entity NO-LOCK NO-ERROR.
    ASSIGN entity-name = Tenant.Name NO-ERROR.  
    unavailable = NOT AVAILABLE Tenant.
  END.

  IF unavailable THEN
  DO:
    MESSAGE "There is no" source-table "with code" INPUT FRAME {&FRAME-NAME} fil_entity
      VIEW-AS ALERT-BOX ERROR.
    DISPLAY fil_entity WITH FRAME {&FRAME-NAME}.
    APPLY 'ENTRY':U TO fil_entity IN FRAME {&FRAME-NAME}.
    RETURN "FAIL".
  END.
  
  ASSIGN fil_entity.
  ASSIGN fil_entityname = entity-name.
  DISPLAY fil_entityname WITH FRAME {&FRAME-NAME}.
    
  RUN open-trn-query.
  RUN selection-changed.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit W-Win 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
   APPLY "CLOSE":U TO THIS-PROCEDURE.
   
   RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available W-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  RUN pre-row-available.
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  fil_entity =
    IF source-table = 'Creditor' THEN Creditor.CreditorCode ELSE
    IF source-table = 'Tenant'   THEN Tenant.TenantCode ELSE 0.

  DISPLAY fil_entity WITH FRAME {&FRAME-NAME}.
  RUN entity-changed.

  RUN refresh-window-title IN sys-mgr ( THIS-PROCEDURE ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE open-trn-query W-Win 
PROCEDURE open-trn-query :
/*------------------------------------------------------------------------------
  Purpose:     Open the transaction query
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO.
DEF VAR et AS CHAR NO-UNDO.
DEF VAR ec AS INT NO-UNDO.
DEF VAR ac AS DEC NO-UNDO.

  RUN get-attribute( 'Key-Name':U ).
  key-name = RETURN-VALUE.
  RUN get-attribute( 'Key-Value':U ).
  key-value = RETURN-VALUE.

  IF key-name = "CreditorCode" THEN     ASSIGN  et = "C"    ec = INT(key-value) .
  ELSE IF key-name = "TenantCode" THEN  ASSIGN  et = "T"    ec = INT(key-value) .
  ELSE IF AVAILABLE(Creditor) THEN      ASSIGN  et = "C"    ec = Creditor.CreditorCode .
  ELSE IF AVAILABLE(Tenant) THEN        ASSIGN  et = "T"    ec = Tenant.TenantCode .
  ELSE RETURN "FAIL".

  IF et = "C" THEN  ac = sundry-creditors.
  ELSE              ac = sundry-debtors.


&SCOP TRN-BY-CLAUSE     

  /* Clear the existing work table */
  FOR EACH OpenTrans: DELETE OpenTrans. END.

  /* create entries in the work table for each partly closed group */
  FOR EACH ClosingGroup NO-LOCK WHERE ClosingGroup.EntityType = et
            AND ClosingGroup.EntityCode = ec 
            AND (ClosingGroup.AccountCode = ac OR (multi-ledger-creditors AND et = 'C'))
            AND ClosingGroup.ClosedStatus = "P",
      LAST AcctTran NO-LOCK OF ClosingGroup
      BY ClosingGroup.EntityType BY ClosingGroup.EntityCode BY ClosingGroup.AccountCode
      BY ClosingGroup.ClosedStatus BY ClosingGroup.Date DESCENDING:
    CREATE  OpenTrans.
    ASSIGN  OpenTrans.rowid = ROWID(ClosingGroup)
            OpenTrans.state = "P"
            OpenTrans.Date = ClosingGroup.Date
            OpenTrans.Reference = "CG " + STRING( ClosingGroup.ClosingGroup)
            OpenTrans.Description = (IF ClosingGroup.Description <> "" THEN ClosingGroup.Description ELSE AcctTran.Description)
            OpenTrans.Amount = 0 .

    IF OpenTrans.Description = "" THEN DO:
      FIND Document NO-LOCK OF AcctTran.
      IF OpenTrans.Description = "" THEN OpenTrans.Description = Document.Description.
/*      IF OpenTrans.Reference  =  "" THEN OpenTrans.Reference  =  Document.Reference.*/
    END.

    DEF BUFFER AltTran FOR AcctTran.
    FOR EACH AltTran NO-LOCK OF ClosingGroup:
      OpenTrans.Amount = OpenTrans.Amount + AltTran.Amount.
    END.
  END.

  /* create entries in the work table for each open transaction */
  FOR EACH AcctTran NO-LOCK WHERE AcctTran.EntityType = et AND AcctTran.EntityCode = ec
            AND (AcctTran.AccountCode = ac OR (multi-ledger-creditors AND et = 'C'))
            AND AcctTran.ClosingGroup = ?
            BY AcctTran.EntityType BY AcctTran.EntityCode BY AcctTran.AccountCode 
            BY AcctTran.MonthCode DESCENDING BY AcctTran.Date DESCENDING 
            BY AcctTran.BatchCode DESCENDING BY AcctTran.DocumentCode DESCENDING BY AcctTran.TransactionCode DESCENDING:

    CREATE  OpenTrans.
    ASSIGN  OpenTrans.rowid = ROWID(AcctTran)
            OpenTrans.state = "O"
            OpenTrans.Date = AcctTran.Date
            OpenTrans.Reference = AcctTran.Reference
            OpenTrans.Description = AcctTran.Description
            OpenTrans.Amount = AcctTran.Amount .

    IF OpenTrans.Description = "" OR OpenTrans.Reference = "" THEN DO:
      FIND Document NO-LOCK OF AcctTran.
      IF OpenTrans.Description = "" THEN OpenTrans.Description = Document.Description.
      IF OpenTrans.Reference  =  "" THEN OpenTrans.Reference  =  Document.Reference.
    END.
  END.

  OPEN QUERY br-trn FOR EACH OpenTrans BY OpenTrans.Date DESCENDING.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available W-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR source-link-name AS CHAR NO-UNDO.
  
  RUN get-link-handle IN adm-broker-hdl ( INPUT THIS-PROCEDURE, 'RECORD-SOURCE':U,
                                           OUTPUT source-link-name ).
  RUN get-attribute IN WIDGET-HANDLE(source-link-name) ('INTERNAL-TABLES':U ).
  source-table = RETURN-VALUE.

  source-table =
    IF LOOKUP( "Tenant", source-table, " " )   <> 0 THEN "Tenant"   ELSE
    IF LOOKUP( "Creditor", source-table, " " ) <> 0 THEN "Creditor" ELSE "".

  DO WITH FRAME {&FRAME-NAME}:
    fil_EntityType = source-table.
    DISPLAY fil_EntityType.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE re-open-group W-Win 
PROCEDURE re-open-group :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF NOT AVAILABLE(OpenTrans) OR OpenTrans.state <> "P" THEN RETURN.

  FIND ClosingGroup EXCLUSIVE-LOCK WHERE ROWID(ClosingGroup) = OpenTrans.rowid.
  FOR EACH AcctTran EXCLUSIVE-LOCK OF ClosingGroup:
    ASSIGN  AcctTran.ClosingGroup = ?
            AcctTran.ClosedState = "O".
  END.
  DELETE ClosingGroup.
  RUN open-trn-query.
  RUN selection-changed.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-all W-Win 
PROCEDURE select-all :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO INITIAL 1.
DEF VAR test-1 AS LOGI NO-UNDO.
DEF VAR test-2 AS LOGI NO-UNDO.

  IF NUM-RESULTS("br-trn") = 0 OR
     NUM-RESULTS("br-trn") = ? THEN RETURN.
     
  GET FIRST br-trn.
  REPOSITION br-trn TO ROWID ROWID( OpenTrans ).
  
  DO WHILE AVAILABLE OpenTrans:
   ASSIGN test-1 = BROWSE {&BROWSE-NAME}:IS-ROW-SELECTED(i) NO-ERROR.
   IF NOT test-1 THEN DO:
     ASSIGN test-2 = {&BROWSE-NAME}:SELECT-ROW(i) IN FRAME {&FRAME-NAME} NO-ERROR.
     IF NOT test-2 THEN DO:
       REPOSITION br-trn TO ROWID ROWID( OpenTrans ).
       i = 1.
       ASSIGN test-1 = BROWSE {&BROWSE-NAME}:IS-ROW-SELECTED(i) NO-ERROR.
       ASSIGN test-2 = {&BROWSE-NAME}:SELECT-ROW(i) IN FRAME {&FRAME-NAME} NO-ERROR.
     END.
     br-trn:SELECT-FOCUSED-ROW() IN FRAME {&FRAME-NAME}.
   END.
   GET NEXT br-trn.
   i = i + 1.
  END.  
  
  GET FIRST br-trn.
  REPOSITION br-trn TO ROWID ROWID( OpenTrans ).

  RUN selection-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-none W-Win 
PROCEDURE select-none :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF br-trn:DESELECT-ROWS() IN FRAME {&FRAME-NAME} THEN.

  RUN selection-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selection-changed W-Win 
PROCEDURE selection-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:  
  fil_Total = 0.
  
  DO i = 1 TO br-trn:NUM-SELECTED-ROWS:
    IF br-trn:FETCH-SELECTED-ROW(i) THEN fil_Total = fil_Total + OpenTrans.Amount.
  END.

  DISPLAY fil_Total.

  btn_close:SENSITIVE = br-trn:NUM-SELECTED-ROWS > 0 AND fil_Total = 0.
  fil_PartCloseAs:SENSITIVE = NOT(btn_close:SENSITIVE).
  btn_close-part:SENSITIVE = NOT(btn_close:SENSITIVE) AND TRIM(INPUT fil_PartCloseAs) <> "".
  btn_all:SENSITIVE   = br-trn:NUM-SELECTED-ROWS <> NUM-RESULTS( "br-trn" ).
  btn_none:SENSITIVE  = br-trn:NUM-SELECTED-ROWS > 0.
END.  
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records W-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Tenant"}
  {src/adm/template/snd-list.i "Creditor"}
  {src/adm/template/snd-list.i "OpenTrans"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed W-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE user-layout W-Win 
PROCEDURE user-layout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

