&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME W-ReportViewer
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS W-ReportViewer 
/*------------------------------------------------------------------------
  File: 
  Description: 
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.

/* Local Variable Definitions ---                                       */

DEFINE STREAM prnt.

DEF VAR os-text-name AS CHAR NO-UNDO.
DEF VAR os-ctrl-name AS CHAR NO-UNDO.

DEF VAR save-directory AS CHAR INITIAL "C:\" NO-UNDO.
DEF VAR search-text    AS CHAR NO-UNDO     INITIAL "".
DEF VAR search-flags   AS INT  NO-UNDO     INITIAL 1.

DEF VAR window-w AS INT NO-UNDO.
DEF VAR window-h AS INT NO-UNDO.

DEF VAR display-font AS INT INITIAL 28 NO-UNDO.
DEF VAR printout-font AS INT INITIAL 29 NO-UNDO.
DEF VAR printout-dlg AS LOGICAL INITIAL No NO-UNDO.
DEF VAR printout-lscp AS LOGICAL INITIAL No NO-UNDO.
DEF VAR printout-w32 AS LOGICAL INITIAL No NO-UNDO.

DEF VAR CURRENT-PRINTER-PORT AS CHAR NO-UNDO.

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
FIND RP WHERE RP.UserName = user-name
      AND RP.ReportID = "Report Viewer" NO-LOCK NO-ERROR.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS ed-report 
&Scoped-Define DISPLAYED-OBJECTS ed-report 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR W-ReportViewer AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_File 
       MENU-ITEM m_Save         LABEL "&Save"          ACCELERATOR "SHIFT-F12"
       MENU-ITEM m_Print        LABEL "&Print"         ACCELERATOR "CTRL-SHIFT-F12"
       RULE
       MENU-ITEM m_Save_Settings LABEL "Save Se&ttings"
       RULE
       MENU-ITEM m_Exit         LABEL "E&xit"          ACCELERATOR "CTRL-F4".

DEFINE SUB-MENU m_Edit 
       MENU-ITEM m_Copy         LABEL "&Copy"          ACCELERATOR "CTRL-INS"
       MENU-ITEM m_Select_All   LABEL "Select &All"    ACCELERATOR "CTRL-A"
       MENU-ITEM m_Display_Font LABEL "Display Fo&nt"  ACCELERATOR "ALT-CTRL-F"
       MENU-ITEM m_Printout_Font LABEL "&Printout Font" ACCELERATOR "ALT-CTRL-P"
       MENU-ITEM m_Find         LABEL "&Find"          ACCELERATOR "CTRL-F".

DEFINE SUB-MENU m_Help 
       MENU-ITEM m_Contents     LABEL "&Contents"     
       RULE
       MENU-ITEM m_About        LABEL "&About"        .

DEFINE MENU MENU-BAR-W-Win MENUBAR
       SUB-MENU  m_File         LABEL "&File"         
       SUB-MENU  m_Edit         LABEL "&Edit"         
       SUB-MENU  m_Help         LABEL "&Help"         .


/* Definitions of the field level widgets                               */
DEFINE VARIABLE ed-report AS CHARACTER 
     VIEW-AS EDITOR NO-WORD-WRAP SCROLLBAR-HORIZONTAL SCROLLBAR-VERTICAL LARGE
     SIZE 42.29 BY 6.2
     FONT 28 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     ed-report AT ROW 1 COL 1 NO-LABEL
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW W-ReportViewer ASSIGN
         HIDDEN             = YES
         TITLE              = "Report Viewer"
         HEIGHT             = 18.95
         WIDTH              = 89.29
         MAX-HEIGHT         = 60
         MAX-WIDTH          = 150
         VIRTUAL-HEIGHT     = 60
         VIRTUAL-WIDTH      = 150
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         FONT               = 33
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-W-Win:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB W-ReportViewer 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW W-ReportViewer
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   Size-to-Fit                                                          */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE.

ASSIGN 
       ed-report:AUTO-RESIZE IN FRAME F-Main      = TRUE
       ed-report:RETURN-INSERTED IN FRAME F-Main  = TRUE
       ed-report:READ-ONLY IN FRAME F-Main        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-ReportViewer)
THEN W-ReportViewer:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME W-ReportViewer
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-ReportViewer W-ReportViewer
ON END-ERROR OF W-ReportViewer /* Report Viewer */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  APPLY 'WINDOW-CLOSE':U TO {&WINDOW-NAME}.
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-ReportViewer W-ReportViewer
ON ENTRY OF W-ReportViewer /* Report Viewer */
DO:
  APPLY 'ENTRY':U TO ed-report IN FRAME {&FRAME-NAME}.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-ReportViewer W-ReportViewer
ON WINDOW-CLOSE OF W-ReportViewer /* Report Viewer */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */

  OS-DELETE VALUE( os-ctrl-name ).
  OS-DELETE VALUE( os-text-name ).

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-ReportViewer W-ReportViewer
ON WINDOW-MAXIMIZED OF W-ReportViewer /* Report Viewer */
DO:
  RUN resize-editor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-ReportViewer W-ReportViewer
ON WINDOW-RESIZED OF W-ReportViewer /* Report Viewer */
DO:
  RUN resize-editor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-ReportViewer W-ReportViewer
ON WINDOW-RESTORED OF W-ReportViewer /* Report Viewer */
DO:
  RUN resize-editor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Copy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Copy W-ReportViewer
ON CHOOSE OF MENU-ITEM m_Copy /* Copy */
DO:
  OUTPUT TO "CLIPBOARD":U KEEP-MESSAGES.
  PUT UNFORMATTED ed-report:SELECTION-TEXT IN FRAME {&FRAME-NAME}.
  OUTPUT CLOSE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Display_Font
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Display_Font W-ReportViewer
ON CHOOSE OF MENU-ITEM m_Display_Font /* Display Font */
DO:
  RUN set-display-font.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit W-ReportViewer
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  APPLY 'WINDOW-CLOSE':U TO {&WINDOW-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Find
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Find W-ReportViewer
ON CHOOSE OF MENU-ITEM m_Find /* Find */
DO:
  RUN find-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Print W-ReportViewer
ON CHOOSE OF MENU-ITEM m_Print /* Print */
DO:
  RUN print-report.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Printout_Font
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Printout_Font W-ReportViewer
ON CHOOSE OF MENU-ITEM m_Printout_Font /* Printout Font */
DO:
  RUN set-printout-font.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Save
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Save W-ReportViewer
ON CHOOSE OF MENU-ITEM m_Save /* Save */
DO:
  RUN save-report.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Save_Settings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Save_Settings W-ReportViewer
ON CHOOSE OF MENU-ITEM m_Save_Settings /* Save Settings */
DO:
  RUN save-settings.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Select_All
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Select_All W-ReportViewer
ON CHOOSE OF MENU-ITEM m_Select_All /* Select All */
DO:
  IF ed-report:SET-SELECTION( 1, ed-report:LENGTH ) IN FRAME {&FRAME-NAME} THEN .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK W-ReportViewer 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects W-ReportViewer  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available W-ReportViewer  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI W-ReportViewer  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-ReportViewer)
  THEN DELETE WIDGET W-ReportViewer.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI W-ReportViewer  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY ed-report 
      WITH FRAME F-Main IN WINDOW W-ReportViewer.
  ENABLE ed-report 
      WITH FRAME F-Main IN WINDOW W-ReportViewer.
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
  VIEW W-ReportViewer.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE find-dialog W-ReportViewer 
PROCEDURE find-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR search-flags AS INT NO-UNDO    INITIAL 33.

  RUN win/d-find.w( INPUT-OUTPUT search-text, INPUT-OUTPUT search-flags ).

  IF search-flags < 1 THEN
    search-flags = ABS( search-flags ).
  ELSE IF NOT( ed-report:SEARCH( search-text, search-flags ) IN FRAME {&FRAME-NAME} ) THEN
    MESSAGE "'" + search-text + "' not found." VIEW-AS ALERT-BOX INFORMATION
                TITLE "Search text not found" .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-current-printer W-ReportViewer 
PROCEDURE get-current-printer :
/*------------------------------------------------------------------------------
  Purpose:     Set the value of CURRENT-PRINTER-??? to reflect the current windows
               printer.
------------------------------------------------------------------------------*/
DEF VAR CURRENT-PRINTER-NAME AS CHAR NO-UNDO.

  /* Set the value of the port ( more robust than printer name for batches ) */
  
  DEF BUFFER CurrPrinter FOR RP.
  DEF VAR user-name AS CHAR NO-UNDO.
  {inc/username.i "user-name"}

  FIND FIRST CurrPrinter WHERE
    CurrPrinter.UserName = user-name AND
    CurrPrinter.ReportID = "Current Printer" NO-LOCK NO-ERROR.
    
  IF NOT AVAILABLE CurrPrinter THEN
  DO:
    DEF VAR success AS LOGI NO-UNDO.
    RUN aderb/_prdef.p ( OUTPUT CURRENT-PRINTER-NAME, OUTPUT CURRENT-PRINTER-PORT, OUTPUT success ).

    CREATE CurrPrinter.
    ASSIGN
      CurrPrinter.UserName = user-name
      CurrPrinter.ReportID = "Current Printer"
      CurrPrinter.Char1    = CURRENT-PRINTER-NAME
      CurrPrinter.Char2    = CURRENT-PRINTER-PORT.
  END.

  CURRENT-PRINTER-NAME = CurrPrinter.Char1.
  CURRENT-PRINTER-PORT = CurrPrinter.Char2.

  RELEASE CurrPrinter.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE load-editor W-ReportViewer 
PROCEDURE load-editor :
/*------------------------------------------------------------------------------
  Purpose:     Load the editor widget from a file
------------------------------------------------------------------------------*/
DEF INPUT PARAM text-file-name AS CHAR NO-UNDO.
DEF INPUT PARAM ctrl-file-name  AS CHAR NO-UNDO.
DEF VAR success AS LOGICAL NO-UNDO.

  IF SESSION:SET-WAIT-STATE("GENERAL") THEN .
  ASSIGN
    os-text-name = text-file-name
    os-ctrl-name = ctrl-file-name
    success = ed-report:READ-FILE( text-file-name ) IN FRAME {&FRAME-NAME}
  .
  
  IF SESSION:SET-WAIT-STATE("") THEN .
  APPLY 'ENTRY':U TO {&WINDOW-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit W-ReportViewer 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/

  OS-DELETE VALUE( os-ctrl-name ).
  OS-DELETE VALUE( os-text-name ).

  APPLY "CLOSE":U TO THIS-PROCEDURE.
   
  RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize W-ReportViewer 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  {inc/username.i "user-name"}
  FIND RP WHERE RP.UserName = user-name
          AND RP.ReportID = "Report Viewer" NO-LOCK NO-ERROR.
  IF AVAILABLE(RP) THEN DO:
    window-w = (IF RP.Int1 > 200 THEN RP.Int1 ELSE ((SESSION:WIDTH-PIXELS * 4) / 5)).
    window-h = (IF RP.Int2 > 200 THEN RP.Int2 ELSE ((SESSION:HEIGHT-PIXELS * 4) / 5)).
    
    ASSIGN
      {&WINDOW-NAME}:VIRTUAL-WIDTH-PIXELS = window-w + 50
      {&WINDOW-NAME}:VIRTUAL-HEIGHT-PIXELS = window-h + 50
      {&WINDOW-NAME}:WIDTH-PIXELS = window-w
      {&WINDOW-NAME}:HEIGHT-PIXELS = window-h
      save-directory = RP.Char1
      search-text = RP.Char2
      search-flags = RP.Int3
    .
  END.
  RUN resize-editor.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-report W-ReportViewer 
PROCEDURE print-report :
/*------------------------------------------------------------------------------
  Purpose: Print the report and close the window
------------------------------------------------------------------------------*/
  IF printout-w32 THEN DO:
    DEF VAR success AS INT INITIAL 0 NO-UNDO.
    DEF VAR lsuccess AS LOGICAL INITIAL No NO-UNDO.
    DEF VAR pr-flags AS INT NO-UNDO.

    pr-flags = (IF printout-dlg  THEN 1 ELSE 0)
             + (IF printout-lscp THEN 2 ELSE 0).

    RUN adecomm/_osprint.p ( {&WINDOW-NAME}:HANDLE, os-text-name, printout-font,
                    pr-flags, 0, 0, OUTPUT lsuccess ).

  END.
  ELSE DO:
    DEF VAR dir-name AS CHAR NO-UNDO.
    DEF VAR control-name AS CHAR NO-UNDO.
    DEF VAR print-name AS CHAR NO-UNDO.
    DEF VAR cmd AS CHAR NO-UNDO.

    RUN get-current-printer.

    dir-name = SUBSTRING( os-text-name, 1, R-INDEX(os-text-name, "\") ).
    control-name = SUBSTRING( os-ctrl-name, R-INDEX(os-ctrl-name, "\") + 1 ).
    print-name = SUBSTRING( os-text-name, R-INDEX(os-text-name, "\") + 1 ).
    cmd = "PP " + CURRENT-PRINTER-PORT + " " + dir-name + " " + control-name + " " + print-name.
    OS-COMMAND SILENT VALUE( cmd ).
  END.

  APPLY 'WINDOW-CLOSE':U TO {&WINDOW-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resize-editor W-ReportViewer 
PROCEDURE resize-editor :
/*------------------------------------------------------------------------------
  Purpose:     Resize the editor widget to match the window size
------------------------------------------------------------------------------*/
  DO WITH FRAME {&FRAME-NAME}:

    /*
     * The error-free order of assignment depends on whether the window
     * size is increasing or diminishing in each direction.
     */
    IF {&WINDOW-NAME}:WIDTH-PIXELS > ed-report:WIDTH-PIXELS THEN ASSIGN
      FRAME {&FRAME-NAME}:WIDTH-PIXELS = {&WINDOW-NAME}:WIDTH-PIXELS + 8
      ed-report:WIDTH-PIXELS = {&WINDOW-NAME}:WIDTH-PIXELS.
    ELSE ASSIGN
      ed-report:WIDTH-PIXELS = {&WINDOW-NAME}:WIDTH-PIXELS
      FRAME {&FRAME-NAME}:WIDTH-PIXELS = {&WINDOW-NAME}:WIDTH-PIXELS + 8.

    IF {&WINDOW-NAME}:HEIGHT-PIXELS > ed-report:HEIGHT-PIXELS THEN ASSIGN
      FRAME {&FRAME-NAME}:HEIGHT-PIXELS = {&WINDOW-NAME}:HEIGHT-PIXELS + 8
      ed-report:HEIGHT-PIXELS = {&WINDOW-NAME}:HEIGHT-PIXELS.
    ELSE ASSIGN
      ed-report:HEIGHT-PIXELS = {&WINDOW-NAME}:HEIGHT-PIXELS
      FRAME {&FRAME-NAME}:HEIGHT-PIXELS = {&WINDOW-NAME}:HEIGHT-PIXELS + 8.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-report W-ReportViewer 
PROCEDURE save-report :
/*------------------------------------------------------------------------------
  Purpose: Save the report to a file and close the window
------------------------------------------------------------------------------*/
DEF VAR cmd-line AS CHAR NO-UNDO.
DEF VAR new-file-name AS CHAR NO-UNDO.
DEF VAR dialog-ok AS LOGI INITIAL Yes NO-UNDO.


  SYSTEM-DIALOG GET-FILE new-file-name FILTERS "Text files" "*.TXT"
            INITIAL-FILTER 1 ASK-OVERWRITE CREATE-TEST-FILE
            DEFAULT-EXTENSION ".TXT" INITIAL-DIR save-directory
            SAVE-AS RETURN-TO-START-DIR UPDATE dialog-ok.

  IF dialog-ok THEN DO:
    OS-COPY VALUE( os-text-name ) VALUE( new-file-name ).
    MESSAGE '"' + new-file-name + '" saved.' VIEW-AS ALERT-BOX INFORMATION
                TITLE "Report Viewer Action".

    save-directory = SUBSTRING( new-file-name, 1, R-INDEX( new-file-name, "\")).
    RUN save-settings.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-settings W-ReportViewer 
PROCEDURE save-settings :
/*------------------------------------------------------------------------------
  Purpose:  Save current settings of the window (x, y, save directory ).
------------------------------------------------------------------------------*/
DEF VAR user-name AS CHAR NO-UNDO.
DEF VAR DisplayFontString AS CHAR FORMAT "x(128)" NO-UNDO.
DEF VAR PrinterFontString AS CHAR FORMAT "x(128)" NO-UNDO.
    
  PUT-KEY-VALUE FONT display-font NO-ERROR.
  PUT-KEY-VALUE FONT printout-font NO-ERROR.
  GET-KEY-VALUE SECTION "Fonts" KEY "Font28" VALUE DisplayFontString.
  GET-KEY-VALUE SECTION "Fonts" KEY "Font29" VALUE PrinterFontString.

  {inc/username.i "user-name"}
  DO TRANSACTION:
    FIND RP WHERE RP.UserName = user-name 
            AND RP.ReportID = "Report Viewer" EXCLUSIVE-LOCK NO-ERROR.
    IF NOT AVAILABLE(RP) THEN CREATE RP.
    ASSIGN window-w = {&WINDOW-NAME}:WIDTH-PIXELS
           window-h = {&WINDOW-NAME}:HEIGHT-PIXELS.
    ASSIGN
      RP.UserName = user-name
      RP.ReportID = "Report Viewer"
      RP.Int1 = window-w
      RP.Int2 = window-h
      RP.Char1 = save-directory
      RP.Char2 = search-text
      RP.Int3  = search-flags
      RP.Char3 = DisplayFontString
      RP.Char4 = PrinterFontString
    .
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records W-ReportViewer  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartWindow, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-display-font W-ReportViewer 
PROCEDURE set-display-font :
/*------------------------------------------------------------------------------
  Purpose: Set the font used in the editor window
------------------------------------------------------------------------------*/
DEF VAR dialog-ok AS LOGI INITIAL Yes NO-UNDO.

  SYSTEM-DIALOG FONT display-font FIXED-ONLY MAX-SIZE 14 MIN-SIZE 6 UPDATE dialog-ok.

  IF dialog-ok THEN DO:
    ed-report:FONT IN FRAME {&FRAME-NAME} = display-font.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-print-details W-ReportViewer 
PROCEDURE set-print-details :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER font-num AS INTEGER NO-UNDO.
DEF INPUT PARAMETER landscape AS LOGICAL NO-UNDO.
DEF INPUT PARAMETER show-dialog AS LOGICAL NO-UNDO.

  ASSIGN
    printout-font = (IF font-num <> ? THEN font-num ELSE printout-font)
    printout-lscp = landscape
    printout-dlg  = show-dialog
    printout-w32  = Yes
  .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-printout-font W-ReportViewer 
PROCEDURE set-printout-font :
/*------------------------------------------------------------------------------
  Purpose: Set the font used in the editor window
------------------------------------------------------------------------------*/

  SYSTEM-DIALOG FONT printout-font FIXED-ONLY MAX-SIZE 14 MIN-SIZE 5.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-title W-ReportViewer 
PROCEDURE set-title :
/*------------------------------------------------------------------------------
  Purpose:  Set the subsidiary title of the viewer
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER title-text AS CHAR NO-UNDO.

  {&WINDOW-NAME}:TITLE = "Report Viewer - " + title-text + " - " + STRING( TIME, "HH:MM:SS").
  APPLY 'ENTRY':U TO ed-report IN FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed W-ReportViewer 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

