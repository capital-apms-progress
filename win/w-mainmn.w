&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS W-Win 
/*------------------------------------------------------------------------
  File:         win/w-mainmn.w
  Description:  Main Menu process
------------------------------------------------------------------------*/
DEF VAR rem-user-name AS CHAR NO-UNDO.
{inc/username.i "rem-user-name"}

RUN setup-environment.

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

IF SEARCH( "update-in-progress.LCK" ) <> ? THEN RUN process/linkimpt.p( "RESTART" ).

DEF VAR img_mainmenu AS WIDGET-HANDLE NO-UNDO.

DEF VAR abbrev AS CHAR NO-UNDO.
DEF VAR org-name AS CHAR NO-UNDO.
DEF VAR icon-file AS CHAR NO-UNDO.
DEF VAR bitmap-file AS CHAR NO-UNDO.

DEF VAR initialized AS INT NO-UNDO  INITIAL 0.

DEF VAR remote-user AS LOGI NO-UNDO INITIAL No.

IF CAN-FIND( UsrGroupMember WHERE UsrGroupMember.GroupName = "RemoteUser" AND UsrGroupMember.UserName = rem-user-name) THEN
  remote-user = Yes.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-MASTER 
&Scoped-Define DISPLAYED-OBJECTS fil_CompanyName 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR W-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE fil_CompanyName AS CHARACTER FORMAT "X(256)":U INITIAL "Trans Tasman Properties Limited" 
      VIEW-AS TEXT 
     SIZE 69.72 BY 1
     FGCOLOR 0 FONT 13 NO-UNDO.

DEFINE RECTANGLE RECT-MASTER
     EDGE-PIXELS 0  NO-FILL 
     SIZE 71.43 BY 1.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_CompanyName AT ROW 1 COL 2.14 NO-LABEL
     RECT-MASTER AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 103.29 BY 34.05
         FONT 9.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW W-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Capital APMS Main Menu"
         COLUMN             = 30.86
         ROW                = 10.55
         HEIGHT             = 21.7
         WIDTH              = 87.43
         MAX-HEIGHT         = 34.3
         MAX-WIDTH          = 103.72
         VIRTUAL-HEIGHT     = 34.3
         VIRTUAL-WIDTH      = 103.72
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         PRIVATE-DATA       = "w-mainmn"
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

&IF '{&WINDOW-SYSTEM}' NE 'TTY' &THEN
IF NOT W-Win:LOAD-ICON("BITMAPS\capital":U) THEN
    MESSAGE "Unable to load icon: BITMAPS\capital"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
&ENDIF
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB W-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{inc/method/m-mnuwin.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW W-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
                                                                        */
/* SETTINGS FOR FILL-IN fil_CompanyName IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
ASSIGN 
       RECT-MASTER:HIDDEN IN FRAME F-Main           = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
THEN W-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON CLOSE OF W-Win /* Capital APMS Main Menu */
DO:
  MESSAGE "Close" SESSION:IMMEDIATE-DISPLAY.
  SESSION:IMMEDIATE-DISPLAY = No.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON END-ERROR OF W-Win /* Capital APMS Main Menu */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON WINDOW-CLOSE OF W-Win /* Capital APMS Main Menu */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  {&WINDOW-NAME}:HIDDEN = Yes.
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON WINDOW-MAXIMIZED OF W-Win /* Capital APMS Main Menu */
DO:
  {&WINDOW-NAME}:WINDOW-STATE = WINDOW-NORMAL.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK W-Win 


/* ***************************  Main Block  *************************** */

RUN start-splash-screen.

{&WINDOW-NAME}:VISIBLE = FALSE.
IF NOT VALID-HANDLE(sys-mgr) THEN MESSAGE "sys-mgr not initialised".
RUN get-system-information IN sys-mgr ( OUTPUT abbrev, OUTPUT fil_CompanyName,
                                     OUTPUT icon-file, OUTPUT bitmap-file ).
{&WINDOW-NAME}:VISIBLE = FALSE.

/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects W-Win  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available W-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI W-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
  THEN DELETE WIDGET W-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI W-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fil_CompanyName 
      WITH FRAME F-Main IN WINDOW W-Win.
  ENABLE RECT-MASTER 
      WITH FRAME F-Main IN WINDOW W-Win.
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-destroy W-Win 
PROCEDURE inst-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  SESSION:IMMEDIATE-DISPLAY = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-create-controls W-Win 
PROCEDURE local-create-controls :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR jurgen-hdl AS HANDLE NO-UNDO.

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'create-controls':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN stop-splash-screen.

  SESSION:IMMEDIATE-DISPLAY = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit W-Win 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
  MESSAGE "local-exit" SESSION:IMMEDIATE-DISPLAY.
  SESSION:IMMEDIATE-DISPLAY = No.

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE post-resize-window W-Win 
PROCEDURE post-resize-window :
/*------------------------------------------------------------------------------
  Purpose:     Override the m-layout.i resize-window so that the window is
               guaranteed to be big enough for img_mainmenu.
------------------------------------------------------------------------------*/
DEF VAR x-min AS INTEGER NO-UNDO.
DEF VAR y-min AS INTEGER NO-UNDO.

  OUTPUT TO VALUE( SESSION:TEMP-DIRECTORY + "/xxx.out" ) KEEP-MESSAGES.
  MESSAGE "post-resize-window".

DO WITH FRAME {&FRAME-NAME}:

  MESSAGE "post-resize-window a.0".
  IF VALID-HANDLE(img_mainmenu) THEN DO:
    img_mainmenu:X = 0.
    x-min = img_mainmenu:X + img_mainmenu:WIDTH-PIXELS.
    y-min = img_mainmenu:Y + img_mainmenu:HEIGHT-PIXELS.
  END.

  MESSAGE "post-resize-window a.1".
  IF FRAME {&FRAME-NAME}:WIDTH-PIXELS < x-min THEN
           FRAME {&FRAME-NAME}:WIDTH-PIXELS = x-min.
  MESSAGE "post-resize-window b".
  IF FRAME {&FRAME-NAME}:HEIGHT-PIXELS < y-min THEN
           FRAME {&FRAME-NAME}:HEIGHT-PIXELS = y-min.
           /* img_mainmenu:HEIGHT-PIXELS = FRAME {&FRAME-NAME}:HEIGHT-PIXELS - img_mainmenu:Y. */

  MESSAGE "post-resize-window c".
  IF {&WINDOW-NAME}:WIDTH-PIXELS < x-min THEN
           {&WINDOW-NAME}:WIDTH-PIXELS = x-min.
  MESSAGE "post-resize-window d".
  IF {&WINDOW-NAME}:HEIGHT-PIXELS < y-min THEN
           {&WINDOW-NAME}:HEIGHT-PIXELS = y-min.

  {&WINDOW-NAME}:X = (SESSION:WIDTH-PIXELS - (FRAME {&FRAME-NAME}:WIDTH-PIXELS)) / 2.
  {&WINDOW-NAME}:Y = (SESSION:HEIGHT-PIXELS - (FRAME {&FRAME-NAME}:HEIGHT-PIXELS)) / 2.

  MESSAGE "post-resize-window e".
  {&WINDOW-NAME}:VISIBLE = TRUE.
  {&WINDOW-NAME}:HIDDEN = FALSE.
  SESSION:IMMEDIATE-DISPLAY = Yes.

END.

OUTPUT CLOSE.
/* OS-DELETE VALUE( SESSION:TEMP-DIRECTORY + "/xxx.out" ) . */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-initialize W-Win 
PROCEDURE pre-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Start the window manager
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*  MESSAGE "pre-initialize". */
DO WITH FRAME {&FRAME-NAME}:

  {&WINDOW-NAME}:VISIBLE = FALSE.
  IF SESSION:SET-WAIT-STATE("") THEN .
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records W-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartWindow, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setup-environment W-Win 
PROCEDURE setup-environment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR j AS INT NO-UNDO.
DEF VAR k AS INT NO-UNDO.

j = COLOR-TABLE:NUM-ENTRIES.
DEF VAR colors AS INT EXTENT 64 NO-UNDO.
DO i = 0 TO j:
  colors[i + 1] = COLOR-TABLE:GET-RGB-VALUE(i).
END.
  
j = FONT-TABLE:NUM-ENTRIES.
DEF VAR fonts AS CHAR EXTENT 64 NO-UNDO.
DEF VAR font-string AS CHAR NO-UNDO.
DEF VAR font-name AS CHAR NO-UNDO.
DO i = 0 TO j:
  font-name = "Font" + TRIM(STRING(i,">>9")).
  GET-KEY-VALUE SECTION "Fonts" KEY font-name VALUE font-string.
  fonts[i + 1] = font-string.
END.

LOAD "apms" NO-ERROR.
USE "apms" NO-ERROR.

j = COLOR-TABLE:NUM-ENTRIES.
IF j < 32 THEN DO:
  COLOR-TABLE:NUM-ENTRIES = 32.
  DO i = j TO 32:
    COLOR-TABLE:SET-DYNAMIC( i, TRUE ) NO-ERROR.
    COLOR-TABLE:SET-RGB-VALUE( i, colors[i + 1]) NO-ERROR.
  END.
END.

FIND RP WHERE RP.UserName = rem-user-name
      AND RP.ReportID = "Report Viewer" NO-LOCK NO-ERROR.

j = FONT-TABLE:NUM-ENTRIES.
IF j < 35 THEN DO:
  FONT-TABLE:NUM-ENTRIES = 35.
  DO i = j TO 35:
    font-string = fonts[i + 1].
    IF AVAILABLE(RP) THEN DO:
      IF i = 28 AND RP.Char3 <> ? THEN font-string = RP.Char3.
      IF i = 29 AND RP.Char4 <> ? THEN font-string = RP.Char4.
    END.
    font-name = "Font" + TRIM(STRING(i,">>9")).
    PUT-KEY-VALUE SECTION "Fonts" KEY font-name VALUE font-string NO-ERROR.
  END.
END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE start-splash-screen W-Win 
PROCEDURE start-splash-screen :
/*------------------------------------------------------------------------------
  Purpose:     Start the splash screen.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*  MESSAGE "start-splash-screen". */

  IF remote-user OR SEARCH( "win\w-splash.w" ) = ? THEN DO:
    splash = ?.
    RETURN.
  END.

DEF VAR wh-splash AS HANDLE NO-UNDO.

  RUN win\w-splash.w PERSISTENT SET splash.
  RUN dispatch IN splash ( 'Initialize':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed W-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE stop-splash-screen W-Win 
PROCEDURE stop-splash-screen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR splash-win AS HANDLE NO-UNDO.

/*  MESSAGE "stop-splash-screen". */

IF splash = ? THEN RETURN.
  splash-win = splash:CURRENT-WINDOW.
  APPLY 'WINDOW-CLOSE':U TO splash-win.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE user-layout W-Win 
PROCEDURE user-layout :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
/*  MESSAGE "user-layout". */
DO WITH FRAME {&FRAME-NAME}:

  RUN set-window-icon IN sys-mgr ( {&WINDOW-NAME} ).
  {&WINDOW-NAME}:TITLE = abbrev + (IF abbrev <> "" THEN " " ELSE "") + "Capital APMS Main Menu".

/* OUTPUT TO VALUE( SESSION:TEMP-DIRECTORY + "/xxx.out" ) KEEP-MESSAGES. */

  IF NOT remote-user THEN DO:
    CREATE IMAGE img_mainmenu NO-ERROR
    ASSIGN  X       = 1       
            Y       = 23
            FRAME   = FRAME {&FRAME-NAME}:HANDLE .

    img_mainmenu:LOAD-IMAGE( bitmap-file ).
    FRAME {&FRAME-NAME}:WIDTH-PIXELS = img_mainmenu:WIDTH-PIXELS + 5.
    FRAME {&FRAME-NAME}:HEIGHT-PIXELS = img_mainmenu:HEIGHT-PIXELS + 25.

    RECT-MASTER:WIDTH-PIXELS = img_mainmenu:WIDTH-PIXELS - ( {&GAP} * 2 ).
    RECT-MASTER:HEIGHT-PIXELS = img_mainmenu:HEIGHT-PIXELS - ( ({&GAP} + 24)  * 3 ) + fil_CompanyName:HEIGHT-PIXELS.
  END.
/*  ELSE */

/*  MESSAGE "user-layout a". */
  fil_CompanyName:WIDTH-PIXELS = RECT-MASTER:WIDTH-PIXELS - 25.
/*  MESSAGE "user-layout b". */

  RUN get-default-groups.
/*  MESSAGE "user-layout c". */
  RUN stack( STRING( RECT-MASTER:HANDLE ) + ",btn-group", "mst-group", "T", Yes ).
/*  MESSAGE "user-layout d". */

/*  img_mainmenu:X = 0. */
  /* NOTE: the window size is set in post-resize-window */
/* OUTPUT CLOSE. */
/* OS-DELETE VALUE( SESSION:TEMP-DIRECTORY + "/xxx.out" ) . */

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

