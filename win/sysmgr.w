&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS W-Win 
/*------------------------------------------------------------------------
  File:         win/winmgr.w
  Description:  System window manager
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

&SCOPED-DEFINE MAX-WINS 200
&SCOPED-DEFINE GAP      8

&SCOPED-DEFINE MAX-X    1280
&SCOPED-DEFINE MAX-Y    1024

DEF VAR root       AS HANDLE NO-UNDO.
DEF VAR pool-name  AS CHAR INITIAL "MANAGER" NO-UNDO.

&GLOB MAXIMUM-BUTTONS 50
DEF VAR btn        AS HANDLE EXTENT {&MAXIMUM-BUTTONS} NO-UNDO.
DEF VAR n          AS INT INIT 0 NO-UNDO.

DEF VAR max-x AS INT NO-UNDO.
DEF VAR max-y AS INT NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-set-l.i "Focus-Parent-On-Close" "focus-parent-on-close"}
IF NOT AVAILABLE(OfficeSetting) THEN focus-parent-on-close = Yes.

DEF VAR window-title-prefix AS CHAR NO-UNDO INITIAL ?.
DEF VAR system-description AS CHAR NO-UNDO INITIAL ?.
DEF VAR window-icon-file AS CHAR NO-UNDO INITIAL ?.
DEF VAR menu-bitmap-file AS CHAR NO-UNDO INITIAL ?.
DEF VAR last-drawn AS HANDLE NO-UNDO.
DEF VAR user-name  AS CHAR NO-UNDO.

RUN initialise.

DEFINE TEMP-TABLE MyUG NO-UNDO LIKE UsrGroup.

DEFINE TEMP-TABLE Node NO-UNDO
  FIELD proc-hdl  AS HANDLE
  FIELD vwr-hdl   AS HANDLE
  FIELD frm-hdl   AS HANDLE
  FIELD node-code LIKE LinkNode.NodeCode
  FIELD btn-hdl   AS HANDLE
  FIELD link-list AS CHAR
  FIELD context   AS CHAR

  INDEX proc-hdl IS UNIQUE PRIMARY
    proc-hdl

  INDEX vwr-hdl IS UNIQUE
    vwr-hdl

  INDEX Context
    node-code
    context
    
  INDEX node-code
    node-code.
    
DEFINE TEMP-TABLE Link NO-UNDO
  LIKE ProgramLink
  FIELD src-hdl  AS HANDLE
  FIELD dst-hdl  AS HANDLE
  FIELD btn-hdl  AS HANDLE
  FIELD fil-hdl  AS HANDLE
  FIELD cde-hdl  AS HANDLE
  FIELD link-id  AS CHAR

  INDEX btn-hdl IS UNIQUE PRIMARY
    btn-hdl

  INDEX fil-hdl IS UNIQUE
    fil-hdl

  INDEX link-code
    LinkCode
    
  INDEX src-hdl
    src-hdl
    
  INDEX link-id
    src-hdl
    link-id.

DEF BUFFER RL FOR Link. /* Buffer used when processing (running a link)
                           Avoids incorrect Link buffer re-reads */

/******************* Triggers which apply _anywhere_ ****************/
ON F3 ANYWHERE DO:
  RUN expand-autotext .
END.

ON F5 ANYWHERE DO:
  RUN refresh-current-browser .
END.

ON CTRL-ALT-F10 ANYWHERE DO:
  RUN win/d-andrew.w NO-ERROR.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD extract-attribute W-Win 
FUNCTION extract-attribute RETURNS CHARACTER
  ( INPUT attribute-list AS CHAR, INPUT attribute-name AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-window-program W-Win 
FUNCTION get-window-program RETURNS HANDLE
  ( INPUT window-hdl AS WIDGET-HANDLE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-window-title W-Win 
FUNCTION get-window-title RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD link-running W-Win 
FUNCTION link-running RETURNS LOGICAL
  ( OUTPUT window-title AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD unique-db-identifier W-Win 
FUNCTION unique-db-identifier RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR W-Win AS WIDGET-HANDLE NO-UNDO.

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SCROLLABLE SIZE 320 BY 320.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW W-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "SYSMGR"
         HEIGHT             = 8.4
         WIDTH              = 54.86
         MAX-HEIGHT         = 200
         MAX-WIDTH          = 200
         VIRTUAL-HEIGHT     = 200
         VIRTUAL-WIDTH      = 200
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

&IF '{&WINDOW-SYSTEM}' NE 'TTY' &THEN
IF NOT W-Win:LOAD-ICON("BITMAPS\capital":U) THEN
    MESSAGE "Unable to load icon: BITMAPS\capital"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
&ENDIF
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB W-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{inc/method/m-sysmgr.i}
{inc/method/m-utils.i}
{inc/string.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW W-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
                                                                        */
ASSIGN 
       FRAME F-Main:HEIGHT           = 38
       FRAME F-Main:WIDTH            = 146.29.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
THEN W-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON END-ERROR OF W-Win /* SYSMGR */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON WINDOW-CLOSE OF W-Win /* SYSMGR */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */

  /* hiding the root window makes the exit look tidier */
  DEF VAR root-window AS HANDLE NO-UNDO.
  root-window = {&WINDOW-NAME}:HANDLE .
  /* root-window = root:CURRENT-WINDOW. */
  root-window:HIDDEN = Yes.

  RUN kill-children( root ).
  APPLY 'CLOSE':U TO root.
  APPLY 'CLOSE':U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON WINDOW-MAXIMIZED OF W-Win /* SYSMGR */
DO:
  {&WINDOW-NAME}:WINDOW-STATE = WINDOW-NORMAL.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK W-Win 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

ASSIGN
  {&WINDOW-NAME}:WIDTH-P  = 1
  {&WINDOW-NAME}:HEIGHT-P = 1
  {&WINDOW-NAME}:X = SESSION:WIDTH-PIXELS
  {&WINDOW-NAME}:Y = {&GAP}
  {&WINDOW-NAME}:TITLE = window-title-prefix + user-name + " - " + PDBNAME(1) + " - Window Manager".

RUN resize-frame IN THIS-PROCEDURE( {&MAX-X}, {&MAX-Y} ).

RUN set-window-icon( {&WINDOW-NAME} ).

SESSION:MULTITASKING-INTERVAL = 20.
{&WINDOW-NAME}:KEEP-FRAME-Z-ORDER = focus-parent-on-close.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-node W-Win 
PROCEDURE add-node :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-hdl  AS HANDLE NO-UNDO.

  DEF BUFFER ThisNode FOR Node.
  
  IF NOT VALID-HANDLE( proc-hdl ) OR
     CAN-FIND( ThisNode WHERE ThisNode.Proc-hdl = proc-hdl ) THEN RETURN.
  IF NOT VALID-HANDLE( root ) THEN root = proc-hdl.

    
  DEF VAR frm-hdl   AS HANDLE NO-UNDO.
  DEF VAR proc-name AS CHAR   NO-UNDO.
  
  RUN get-attribute IN proc-hdl( 'frame-handle' ) NO-ERROR.
  frm-hdl   = WIDGET-HANDLE( RETURN-VALUE ).
  RUN get-attribute IN proc-hdl( 'FileName' ) NO-ERROR.
  proc-name = RETURN-VALUE.

  FIND LinkNode WHERE LinkNode.File = proc-name NO-LOCK NO-ERROR.
  
  CREATE ThisNode.
  ASSIGN
    ThisNode.proc-hdl  = proc-hdl
    ThisNode.frm-hdl   = frm-hdl
    ThisNode.node-code = IF AVAILABLE LinkNode THEN LinkNode.NodeCode ELSE ?.

  IF AVAILABLE LinkNode AND LinkNode.NodeType <> "MV" THEN
  DO:
    RUN create-link-objects( proc-hdl ).
    RUN create-button ( ThisNode.proc-hdl, OUTPUT ThisNode.btn-hdl ).
    IF proc-hdl = root THEN RUN redraw IN THIS-PROCEDURE.
  END.
    
  RUN get-links( proc-hdl, OUTPUT ThisNode.link-list ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects W-Win  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available W-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE apms-system-manager W-Win 
PROCEDURE apms-system-manager :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* DO NOT DELETE THIS PROCEDURE !!!!!! */
  /* IT IS USED TO IDENTIFY THIS PERSISTENT PROCEDURE AS
     THE SYSTEM MANAGER */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE back-tab W-Win 
PROCEDURE back-tab :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  APPLY 'BACK-TAB':U TO SELF.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE check-rights W-Win 
PROCEDURE check-rights :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER application AS CHAR NO-UNDO.
DEF INPUT PARAMETER action AS CHAR NO-UNDO.

DEF VAR rights AS LOGICAL NO-UNDO.

  RUN get-rights IN THIS-PROCEDURE( application, action, OUTPUT rights).
  IF rights THEN RETURN.

  /* NOT rights */
  MESSAGE "You do not have sufficient access rights." SKIP(1)
          "Please contact your system administrator"
          VIEW-AS ALERT-BOX WARNING
          TITLE "No Rights to " + action + " in " + application.
  RETURN "FAIL".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-button W-Win 
PROCEDURE create-button :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER  proc-hdl AS HANDLE NO-UNDO.
DEF OUTPUT PARAMETER btn-hdl  AS HANDLE NO-UNDO.
  
  DEF VAR proc-win AS HANDLE NO-UNDO.
  /* proc-win = proc-hdl:CURR-WINDOW. */
  RUN get-window-handle IN proc-hdl NO-ERROR.
  proc-win = WIDGET-HANDLE( RETURN-VALUE ).

  n = n + 1.
  IF n > {&MAXIMUM-BUTTONS} THEN DO:
    MESSAGE "Maximum buttons exceeded!" SKIP(1)
            "How can you cope with all of those on the screen?" SKIP
            "I recommend you close some windows!"
            VIEW-AS ALERT-BOX ERROR.
    RETURN.
  END.

  DEF VAR btn-label AS CHAR NO-UNDO.
  btn-label = proc-win:TITLE.
  IF btn-label = "" THEN
    btn-label = "Capital APMS".
  IF SUBSTRING( btn-label, 1, LENGTH(window-title-prefix)) = window-title-prefix THEN
    btn-label = SUBSTRING( btn-label, LENGTH(window-title-prefix) + 1).

DO WITH FRAME {&FRAME-NAME}:
  CREATE BUTTON btn[ n ]
    ASSIGN
      X            = 1
      Y            = 1
      FONT         = 15
      LABEL        = btn-label
      PRIVATE-DATA = STRING( proc-hdl )
      FRAME        = FRAME {&FRAME-NAME}:HANDLE
      HIDDEN       = Yes

    TRIGGERS:
      ON CHOOSE PERSISTENT RUN focus-from-button IN THIS-PROCEDURE.
      ON 'CURSOR-UP':U,   'CURSOR-LEFT':U  PERSISTENT RUN back-tab IN THIS-PROCEDURE.
      ON 'CURSOR-DOWN':U, 'CURSOR-RIGHT':U PERSISTENT RUN fwd-tab  IN THIS-PROCEDURE.
    END TRIGGERS.

  RUN crop-button( btn[ n ] ).

  proc-hdl:PRIVATE-DATA = STRING( btn[n]:HANDLE ).
  btn-hdl = btn[ n ].
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-link-objects W-Win 
PROCEDURE create-link-objects :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-hdl AS HANDLE NO-UNDO.
  
  DEF BUFFER ThisLink FOR Link.
  
  RUN get-attribute IN proc-hdl ( 'LinkId' ).
  FIND ThisLink WHERE ROWID( ThisLink ) = TO-ROWID( RETURN-VALUE ) NO-ERROR.
  
  /* Link Object exit conditions */
  IF NOT AVAILABLE ThisLink OR LOOKUP( ThisLink.LinkType, "MSG" ) <> 0 THEN RETURN.
  
  /***********************************************
   
    Instantiate and link all link objects
    relevant to the given procedure object
    
   ***********************************************/

  DEF VAR opt-pnl AS HANDLE NO-UNDO.
  DEF BUFFER Src FOR Node. FIND Src WHERE Src.proc-hdl = ThisLink.src-hdl.
  DEF BUFFER Dst FOR Node. FIND Dst WHERE Dst.proc-hdl = ThisLink.dst-hdl.
  

  IF ThisLink.CreateViewer AND TRIM(ThisLink.Viewer) <> "" THEN DO: /* Create the viewer */
    DEF VAR vwr-name AS CHAR NO-UNDO.
    RUN verify-prog( ThisLink.Viewer, OUTPUT vwr-name ).
    IF RETURN-VALUE <> "FAIL" THEN
      RUN init-object IN ThisLink.dst-hdl ( vwr-name, Dst.frm-hdl, "", OUTPUT Dst.vwr-hdl ).
  END.

  RUN set-attribute-list IN ThisLink.dst-hdl ( "Viewer = " + 
    IF VALID-HANDLE( Dst.vwr-hdl) THEN STRING( Dst.vwr-hdl ) ELSE ThisLink.Viewer ).
  RUN get-viewer IN ThisLink.dst-hdl( OUTPUT Dst.vwr-hdl ).
  RUN set-attribute-list IN Dst.vwr-hdl( ThisLink.Function ).
  IF RL.LinkType = "SEL" THEN DO:
    DEF VAR ctr-hdl AS CHAR NO-UNDO.
    RUN get-link-handle IN adm-broker-hdl ( Src.proc-hdl, 'CONTAINER-SOURCE', OUTPUT ctr-hdl ).
    RUN set-attribute-list IN Dst.vwr-hdl( "Source-Window = " + ctr-hdl ).
    RUN set-attribute-list IN Dst.vwr-hdl( "Source-Viewer = " + STRING(Src.proc-hdl) ).
  END.
  ELSE  
    RUN set-attribute-list IN Dst.vwr-hdl( "Source-Viewer = " + STRING(Src.vwr-hdl) ).

  RUN get-attribute IN Dst.vwr-hdl( "SortPanel" ).
  IF ThisLink.SortPanel OR RETURN-VALUE = "Yes" THEN    /* Create a Sort Panel */
  DO:
    RUN init-object IN ThisLink.dst-hdl ( 'panel/p-option.w':U, Dst.frm-hdl,
      'Style = Horizontal Radio-Set, Label = Sort By, Style-Attribute = SortBy-Style,
       Link-Name = SortBy-Target, Options-Attribute = SortBy-Options, Case-Attribute = SortBy-case':U,
       OUTPUT opt-pnl ).
    RUN add-link IN adm-broker-hdl ( opt-pnl, 'SortBy':U, Dst.vwr-hdl ).
    RUN dispatch IN opt-pnl ( 'Initialize':U ).
    RUN set-attribute-list IN ThisLink.dst-hdl( 'SortBy-Panel = ' + STRING( opt-pnl ) ).
  END.

  RUN get-attribute IN Dst.vwr-hdl( "FilterPanel" ).
  IF ThisLink.FilterPanel OR RETURN-VALUE = "Yes" THEN    /* Create a Filter Panel */
  DO:
    RUN init-object IN ThisLink.dst-hdl ( 'panel/p-option.w':U, Dst.frm-hdl,
      'Style = Horizontal Radio-Set, Label = Filter By, Style-Attribute = FilterBy-Style,
       Link-Name = FilterBy-Target, Options-Attribute = FilterBy-Options, Case-Attribute = FilterBy-Case':U,
       OUTPUT opt-pnl ).
    RUN add-link IN adm-broker-hdl ( opt-pnl, 'FilterBy':U, Dst.vwr-hdl ).
    RUN dispatch IN opt-pnl ( 'Initialize':U ).
    RUN set-attribute-list IN ThisLink.dst-hdl( 'FilterBy-Panel = ' + STRING( opt-pnl ) ).
  END.

  RUN get-attribute IN Dst.vwr-hdl( "OptionPanel" ).
  IF RETURN-VALUE <> ? AND RETURN-VALUE <> "" THEN DO:
    /* Create a generic Option Panel */
    DEF VAR panel-name AS CHAR NO-UNDO.
    panel-name = RETURN-VALUE.
    RUN init-object IN ThisLink.dst-hdl ( 'panel/p-option.w':U, Dst.frm-hdl,
      'Style = Horizontal Radio-Set, Label = ' + panel-name + ', Style-Attribute = ' + panel-name + '-Style,
       Link-Name = ' + panel-name + '-Target, Options-Attribute = ' + panel-name + '-Options, Case-Attribute = ' + panel-name + '-case':U,
       OUTPUT opt-pnl ).
    RUN add-link IN adm-broker-hdl ( opt-pnl, panel-name, Dst.vwr-hdl ).
    RUN dispatch IN opt-pnl ( 'Initialize':U ).
    RUN set-attribute-list IN ThisLink.dst-hdl( 'Option-Panel = ' + STRING( opt-pnl ) + ", " + panel-name + " = " + STRING( opt-pnl ) ).
  END.

  RUN get-attribute IN Dst.vwr-hdl( "SearchPanel" ).
  IF RETURN-VALUE = "Yes" THEN    /* Create a Search Panel */
  DO:
    RUN init-object IN ThisLink.dst-hdl ( 'panel/p-search.w':U, Dst.frm-hdl,
      'Label = Search for, Link-Name = SearchBy-Target, Value-Attribute = SearchBy-Case':U,
       OUTPUT opt-pnl ).
    RUN add-link IN adm-broker-hdl ( opt-pnl, 'SearchBy':U, Dst.vwr-hdl ).
    RUN dispatch IN opt-pnl ( 'Initialize':U ).
    RUN set-attribute-list IN ThisLink.dst-hdl( 'SearchBy-Panel = ' + STRING( opt-pnl ) ).
  END.

  RUN get-attribute IN Dst.vwr-hdl( "AlphabetPanel" ).
  IF RETURN-VALUE = "Yes" THEN    /* Create an Alphabet Panel */
  DO:
    RUN init-object IN ThisLink.dst-hdl ( 'panel/p-abc.w':U, Dst.frm-hdl,
            'Button-Font = 9, Dispatch-Open-Query = yes, Margin-Pixels = 0,
             Edge-Pixels = 0':U, OUTPUT opt-pnl ).
    RUN add-link IN adm-broker-hdl ( opt-pnl, 'Filter':U, Dst.vwr-hdl ).
    RUN dispatch IN opt-pnl ( 'Initialize':U ).
    RUN set-attribute-list IN ThisLink.dst-hdl( 'Filter-Panel = ' + STRING( opt-pnl ) ).
  END.

  RUN get-attribute IN Dst.vwr-hdl( "AlphabetPanel2" ).
  IF RETURN-VALUE = "Yes" THEN    /* Create a second Alphabet Panel */
  DO:
    RUN init-object IN ThisLink.dst-hdl ( 'panel/p-abc2.w':U, Dst.frm-hdl,
            'Button-Font = 9, Dispatch-Open-Query = yes, Margin-Pixels = 0,
             Edge-Pixels = 0':U, OUTPUT opt-pnl ).
    RUN add-link IN adm-broker-hdl ( opt-pnl, 'Filter2':U, Dst.vwr-hdl ).
    RUN dispatch IN opt-pnl ( 'Initialize':U ).
    RUN set-attribute-list IN ThisLink.dst-hdl( 'Filter2-Panel = ' + STRING( opt-pnl ) ).
  END.

  RUN get-attribute IN Dst.vwr-hdl( "NotesViewer" ).
  IF RETURN-VALUE = "Yes" THEN
  DO:
    RUN init-object IN ThisLink.dst-hdl ( 'vwr/mnt/v-note.w':U, Dst.frm-hdl,
            'Font = 10':U, OUTPUT opt-pnl ).
    RUN add-link IN adm-broker-hdl ( Dst.vwr-hdl, 'RECORD':U, opt-pnl ).
    RUN dispatch IN opt-pnl ( 'Initialize':U ).
    RUN set-attribute-list IN ThisLink.dst-hdl( 'Notes-Handle = ' + STRING( opt-pnl ) ).
  END.

  /****** BEGIN Experiment for linking objects ******/

  DEF VAR i AS INT NO-UNDO.
  DEF VAR j AS INT NO-UNDO.
  DEF VAR direction  AS CHAR NO-UNDO.
  DEF VAR parameters AS CHAR NO-UNDO.
  DEF VAR link-type  AS CHAR NO-UNDO.
  DEF VAR attribute  AS CHAR NO-UNDO.
  DEF VAR object     AS CHAR NO-UNDO.
  DEF VAR object-hdl AS HANDLE NO-UNDO.

  DO i = 1 TO 2:
    direction = ( IF i = 1 THEN "From" ELSE "To" ).
    DO j = 1 TO 3:
      attribute = "Link-" + direction + "-" + STRING( j ).
      RUN get-attribute IN Dst.vwr-hdl( attribute ).
      parameters = RETURN-VALUE.
      IF parameters <> ? THEN DO:
        object     = ENTRY( 1, parameters, "|" ).
        link-type  = ENTRY( 2, parameters, "|" ).
        RUN init-object IN ThisLink.dst-hdl ( object, Dst.frm-hdl,' ' , OUTPUT object-hdl ).
        IF direction <> "To" THEN direction = "From".
        IF direction = "To" THEN
          RUN add-link IN adm-broker-hdl ( Dst.vwr-hdl, link-type, object-hdl ).
        ELSE
          RUN add-link IN adm-broker-hdl ( object-hdl, link-type, Dst.vwr-hdl ).
        RUN dispatch IN object-hdl ( 'Initialize':U ).
        RUN set-attribute-list IN ThisLink.dst-hdl( attribute + " = " + STRING( object-hdl ) ).
      END.
    END.
  END.

  /****** END Experiment for linking objects ******/
  
  /* Link the viewers */

  IF LOOKUP( ThisLink.LinkType, "DRL,MNT" ) <> 0 THEN DO:
    IF VALID-HANDLE( Src.vwr-hdl ) AND Src.proc-hdl <> Src.vwr-hdl AND
       VALID-HANDLE( Dst.vwr-hdl ) THEN
      RUN add-link IN adm-broker-hdl ( Src.vwr-hdl, 'RECORD':U, Dst.vwr-hdl ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE crop-button W-Win 
PROCEDURE crop-button :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER h-btn AS HANDLE NO-UNDO.
  
  h-btn:HEIGHT-PIXELS = h-btn:HEIGHT-PIXELS - 6.
  h-btn:WIDTH-PIXELS  = h-btn:WIDTH-PIXELS + 4.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI W-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
  THEN DELETE WIDGET W-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-link W-Win 
PROCEDURE each-link :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER attribs AS CHAR NO-UNDO.
  
  DEF VAR i       AS INT  NO-UNDO.
  DEF VAR setting AS CHAR NO-UNDO.
  DEF VAR attrib  AS CHAR NO-UNDO.
  DEF VAR val     AS CHAR NO-UNDO.
  
  DO i = 1 TO NUM-ENTRIES( attribs ):
    setting = ENTRY( i, attribs ).
    attrib  = TRIM( ENTRY( 1, setting, "=" ) ).
    val     = TRIM( SUBSTR( setting, INDEX( setting, "=" ) + 1 ) ).
    
    CASE attrib:
      WHEN "HIDDEN"       THEN Link.btn-hdl:HIDDEN = val = "Yes".    
      WHEN "SENSITIVE"    THEN Link.btn-hdl:SENSITIVE = val = "Yes".
      WHEN "FILTER-PANEL" THEN Link.FilterPanel = val = "Yes".
      WHEN "SORT-PANEL"   THEN Link.SortPanel = val = "Yes".
      WHEN "FUNCTION"     THEN Link.Function = REPLACE( val, "~n", "," ).
      WHEN "VIEWER"       THEN Link.Viewer   = val.
      WHEN "TARGET"       THEN DO:
      DEF BUFFER LN FOR LinkNode.
        FIND LN WHERE LN.File = val NO-LOCK NO-ERROR.
        IF AVAILABLE(LN) THEN Link.Target = LN.NodeCode.
      END.
    END CASE.
  
  END.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-program-link W-Win 
PROCEDURE each-program-link :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER button-label AS CHAR NO-UNDO.
DEF INPUT-OUTPUT PARAMETER link-list AS CHAR NO-UNDO.
  
  CREATE Link.
  BUFFER-COPY ProgramLink TO Link ASSIGN Link.src-hdl = Node.proc-hdl.

  IF LinkNode.NodeType = "MV" THEN DO:
    RUN find-sibling ( Node.frm-hdl, Link.FillName, OUTPUT Link.fil-hdl ).
    RUN find-sibling ( Node.frm-hdl, Link.CodeName, OUTPUT Link.cde-hdl ). 
    ASSIGN Link.link-id = STRING( Link.fil-hdl ) .
  END.
  ELSE IF Link.LinkType = "MSG" THEN
    Link.Link-Id = extract-attribute( Link.Function, 'Message' ).
  ELSE DO:
    Link.Link-Id = extract-attribute( Link.Function, 'Link-Id' ).
    IF Link.Link-Id = "" THEN Link.Link-ID = extract-attribute( Link.Function, 'Mode' ).
    IF Link.Link-Id = "" THEN Link.Link-Id = Link.ButtonLabel.
  END.

  IF ( LinkNode.NodeType = "MV" AND VALID-HANDLE( Link.fil-hdl ) ) OR
     LinkNode.NodeType <> "MV"
  THEN

  CREATE BUTTON Link.btn-hdl
  ASSIGN
    FRAME        = Node.frm-hdl
    X            = 0
    Y            = 0
    FONT         = IF LinkNode.NodeType = "MV" THEN 16  ELSE 9
    LABEL        = IF LinkNode.NodeType = "MV" THEN "F" ELSE button-label
    PRIVATE-DATA = ""
    TOOLTIP      = ProgramLink.Description
    HIDDEN       = Yes
    SENSITIVE    = Yes

  TRIGGERS:
    ON CHOOSE PERSISTENT RUN run-link IN THIS-PROCEDURE.
  END TRIGGERS.

  IF LinkNode.NodeType = "MV" THEN  /* Position the button in the viewer */
  DO:
  
    DEF VAR x-off AS INT NO-UNDO. DEF VAR y-off AS INT NO-UNDO.
    DEF VAR width AS INT NO-UNDO. DEF VAR height AS INT NO-UNDO.

    IF NOT VALID-HANDLE(Link.btn-hdl) THEN DO:
      MESSAGE "Invalid Button" Link.FillName Link.CodeName "on" LinkNode.Description "Link"
                    VIEW-AS ALERT-BOX TITLE "Invalid Link Button".
      NEXT.
    END.
    Link.btn-hdl:HEIGHT-PIXELS = Link.fil-hdl:HEIGHT-PIXELS.
    Link.btn-hdl:WIDTH-PIXELS  = Link.btn-hdl:HEIGHT-PIXELS.
    
    IF Link.fil-hdl:label = ? THEN DO:
      Link.btn-hdl:X = Link.fil-hdl:X - Link.btn-hdl:WIDTH-PIXELS - 2.
      IF Link.btn-hdl:MOVE-BEFORE-TAB-ITEM( Link.fil-hdl ) THEN.
    END.
    ELSE DO:
      Link.fil-hdl:WIDTH-PIXELS = Link.fil-hdl:WIDTH-PIXELS -
                                  Link.btn-hdl:WIDTH-PIXELS.
      Link.btn-hdl:X = Link.fil-hdl:X + Link.fil-hdl:WIDTH-PIXELS.
      IF Link.btn-hdl:MOVE-AFTER-TAB-ITEM( Link.fil-hdl ) THEN.
    END.

    Link.btn-hdl:Y      = Link.fil-hdl:Y.
    Link.btn-hdl:HIDDEN = No.
    
  END.
  
  add-to-list( link-list, STRING( Link.btn-hdl ) ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI W-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME F-Main IN WINDOW W-Win.
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
  VIEW W-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE expand-autotext W-Win 
PROCEDURE expand-autotext :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR obj AS HANDLE NO-UNDO.
  obj = SELF.
  
  IF VALID-HANDLE( obj ) AND
    ( obj:TYPE = "FILL-IN" OR ( obj:TYPE = "EDITOR" /* AND obj:LARGE = no */) ) THEN
  DO:

    DEF VAR val   AS CHAR   NO-UNDO.
    DEF VAR h-prt AS HANDLE NO-UNDO.
    DEF VAR pos1  AS INT    NO-UNDO.
    DEF VAR pos2  AS INT    NO-UNDO.

    val = obj:SCREEN-VALUE.
    h-prt = obj:PARENT.

    IF h-prt:TYPE = "BROWSE" THEN pos2 = LENGTH(val) + 1.
                             ELSE pos2 = obj:CURSOR-OFFSET.
    pos1 = R-INDEX( val, " ", pos2) + 1.
    IF pos2 < 1 THEN pos2 = 1.
    
    DEF VAR atxt-lookup LIKE AutoText.AutoTextCode NO-UNDO.
    atxt-lookup = SUBSTRING( val, pos1, pos2 - pos1).


    /* Try to find the autotext */
    
    FIND FIRST AutoText WHERE AutoText.AutoTextCode = atxt-lookup NO-LOCK NO-ERROR.

    IF AVAILABLE AutoText THEN DO:

      obj:SCREEN-VALUE = SUBSTRING( val, 1, pos1 - 1) + AutoText.Description + SUBSTRING( val, pos2 + 1 ).
      IF h-prt:TYPE = "BROWSE" THEN APPLY "END" TO obj.
                               ELSE obj:CURSOR-OFFSET = pos1 + LENGTH(AutoText.Description).
    END.
    ELSE
    DO:
      MESSAGE "Couldn't find '" + atxt-lookup + "'"  VIEW-AS ALERT-BOX
        INFORMATION TITLE "Autotext Problem".
      APPLY 'ENTRY':U TO obj.
      IF h-prt:TYPE = "BROWSE" THEN obj:AUTO-ZAP = No.
                               ELSE obj:CURSOR-OFFSET = pos2.
    END.

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE focus-from-button W-Win 
PROCEDURE focus-from-button :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR node AS HANDLE NO-UNDO.

  FIND Node NO-LOCK WHERE Node.btn-hdl = FOCUS NO-ERROR.
  IF AVAILABLE Node THEN
    RUN focus-window IN THIS-PROCEDURE ( Node.proc-hdl ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE focus-parent W-Win 
PROCEDURE focus-parent :
/*------------------------------------------------------------------------------
  Purpose:     Focus on the immediate parent of the given window
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER node AS HANDLE NO-UNDO.

DEF VAR par-str AS CHAR NO-UNDO.
DEF VAR par     AS HANDLE NO-UNDO.

  RUN get-link-handle IN adm-broker-hdl ( node, 'WINMGR-SOURCE':U, OUTPUT par-str ).
  par = WIDGET-HANDLE( ENTRY( 1, par-str ) ).
  RUN focus-window IN THIS-PROCEDURE ( par ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE focus-root W-Win 
PROCEDURE focus-root :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN dispatch IN root ('apply-entry':U ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE focus-this-button W-Win 
PROCEDURE focus-this-button :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-hdl AS HANDLE NO-UNDO.

  IF {&WINDOW-NAME}:WINDOW-STATE = WINDOW-MINIMIZED THEN
     {&WINDOW-NAME}:WINDOW-STATE = WINDOW-NORMAL.

  FIND Node WHERE Node.proc-hdl = proc-hdl NO-LOCK NO-ERROR.
  RUN set-idle .
  IF AVAILABLE Node AND VALID-HANDLE( Node.btn-hdl ) THEN
    APPLY 'ENTRY':U TO Node.btn-hdl.
  ELSE
    APPLY 'ENTRY':U TO btn[1].

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE focus-window W-Win 
PROCEDURE focus-window :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-hdl AS WIDGET-HANDLE NO-UNDO.

  IF VALID-HANDLE( proc-hdl ) THEN DO:
    DEF VAR proc-win AS HANDLE NO-UNDO.
    RUN get-window-handle IN proc-hdl NO-ERROR.
    proc-win = WIDGET-HANDLE( RETURN-VALUE ).
    IF proc-win:WINDOW-STATE = WINDOW-MINIMIZED THEN
       proc-win:WINDOW-STATE = WINDOW-NORMAL.
    RUN set-idle .
    APPLY 'ENTRY':U TO proc-win.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fwd-tab W-Win 
PROCEDURE fwd-tab :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  APPLY 'TAB':U TO FOCUS.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-btn-list W-Win 
PROCEDURE get-btn-list :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER proc-hdl AS HANDLE NO-UNDO.
DEF OUTPUT PARAMETER btn-list AS CHAR NO-UNDO.

  FIND Node NO-LOCK WHERE Node.proc-hdl = proc-hdl NO-ERROR.
  IF AVAILABLE Node THEN btn-list = Node.link-list.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-links W-Win 
PROCEDURE get-links :
/*------------------------------------------------------------------------------
  Purpose:  Gets and instantiates the links for a particular node
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER proc-hdl  AS HANDLE NO-UNDO.
DEF OUTPUT PARAMETER link-list AS CHAR   NO-UNDO.
  
  DEF VAR user-name AS CHAR NO-UNDO.
  DEF VAR btn-list  AS CHAR NO-UNDO.
  
  FIND Node NO-LOCK WHERE Node.proc-hdl = proc-hdl NO-ERROR.
  IF NOT AVAILABLE Node THEN RETURN.
  
  FIND LinkNode WHERE LinkNode.NodeCode = Node.node-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE LinkNode THEN RETURN.

  IF LinkNode.NodeType = "MV" THEN DO:
    FOR EACH ProgramLink NO-LOCK WHERE ProgramLink.Source = LinkNode.NodeCode:
      RUN each-program-link( "Lookup", INPUT-OUTPUT link-list ).
    END.
  END.
  ELSE DO:  
    RUN get-username ( OUTPUT user-name ).

    FOR EACH MyUG,
      FIRST UsrGroupMenu OF MyUG NO-LOCK WHERE UsrGroupMenu.NodeCode = node-code,
        EACH  UsrGroupMenuItem OF UsrgroupMenu NO-LOCK,
        FIRST ProgramLink OF UsrGroupMenuItem NO-LOCK
        BY MyUG.Sequence BY UsrGroupMenuItem.SequenceCode:

      IF LOOKUP( STRING( ROWID( ProgramLink ) ), btn-list) = 0 THEN DO:
        btn-list = btn-list + STRING( ROWID( ProgramLink ) ) + ",".
        RUN each-program-link( UsrGroupMenuItem.ButtonLabel, INPUT-OUTPUT link-list ).
      END.
    END.

  END.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-rights W-Win 
PROCEDURE get-rights :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER app-code AS CHAR NO-UNDO.
DEF INPUT PARAMETER action AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER rights AS LOGICAL NO-UNDO INITIAL No.


  FIND FIRST UsrGroupMember WHERE CAN-FIND( UsrGroupRights
                 WHERE UsrGroupMember.GroupName = UsrGroupRights.GroupName
                 AND UsrGroupMember.UserName    = user-name
                 AND UsrGroupRights.ApplicationCode = app-code 
                 AND UsrGroupRights.Action      = action
                 AND UsrGroupRights.Rights )
                 NO-LOCK NO-ERROR.
  
  rights = AVAILABLE(UsrGroupMember).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-system-information W-Win 
PROCEDURE get-system-information :
/*------------------------------------------------------------------------------
  Purpose:  Returns system description information
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER system-abbreviation AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER organisation-name   AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER icon-filename       AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER menu-filename       AS CHAR NO-UNDO.

  system-abbreviation = window-title-prefix.
  organisation-name = system-description.
  icon-filename = window-icon-file.
  menu-filename = menu-bitmap-file.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-username W-Win 
PROCEDURE get-username :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER name AS CHAR NO-UNDO.

  name = user-name.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialise W-Win 
PROCEDURE initialise :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

{inc/username.i "user-name"}
  RUN set-db-identifiers.

  FIND Usr WHERE Usr.UserName = user-name NO-LOCK NO-ERROR.

  IF NOT AVAILABLE Usr THEN DO:
    MESSAGE "You are not registered for this system." SKIP
            "Contact your System Administrator."
        VIEW-AS ALERT-BOX ERROR TITLE "Invalid User - '" + user-name + "'".
    QUIT.
  END.

  FOR EACH MyUG:    DELETE MyUG.        END.
  FOR EACH UsrGroup NO-LOCK WHERE CAN-FIND( FIRST UsrGroupMember OF UsrGroup WHERE UsrGroupMember.UserName = user-name ):
    CREATE MyUG.
    BUFFER-COPY UsrGroup TO MyUG.
  END.

  FOR EACH Node:    DELETE Node.        END.
  FOR EACH Link:    DELETE Link.        END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE iterate-widgets W-Win 
PROCEDURE iterate-widgets :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER wh AS WIDGET-HANDLE NO-UNDO.
DEF INPUT PARAMETER proc-hdl  AS HANDLE NO-UNDO.
DEF INPUT PARAMETER proc-name AS CHAR   NO-UNDO.
  
  RUN VALUE( proc-name ) IN proc-hdl ( wh ) NO-ERROR.
  IF CAN-QUERY( wh, "FIRST-CHILD":U) AND
     VALID-HANDLE( wh:FIRST-CHILD ) THEN
    RUN iterate-widgets( wh:FIRST-CHILD, proc-hdl, proc-name ).
    
  IF CAN-QUERY( wh, "NEXT-SIBLING":U) AND 
      VALID-HANDLE( wh:NEXT-SIBLING) THEN
    RUN iterate-widgets( wh:NEXT-SIBLING, proc-hdl, proc-name ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE kill-children W-Win 
PROCEDURE kill-children :
/*------------------------------------------------------------------------------
  Purpose:  Kill all windows subsidiary to the one specified
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-hdl AS HANDLE NO-UNDO.

DEF VAR i        AS INT INIT 1  NO-UNDO.
DEF VAR children AS CHAR        NO-UNDO.
DEF VAR child    AS HANDLE      NO-UNDO.
    
  RUN get-link-handle IN adm-broker-hdl ( proc-hdl, 'WINMGR-TARGET':U, OUTPUT children ).
  
  DO i = 1 TO NUM-ENTRIES( children ):
    child = WIDGET-HANDLE( ENTRY( i, children ) ).
    RUN kill-children IN THIS-PROCEDURE( child ).
  END.
  
  FOR EACH Link NO-LOCK WHERE Link.src-hdl = proc-hdl: DELETE Link. END.
  FIND Node NO-LOCK WHERE Node.proc-hdl = proc-hdl. DELETE Node.

  RUN remove-button IN THIS-PROCEDURE( proc-hdl ).

  DEF VAR proc-window AS HANDLE NO-UNDO.
  RUN get-window-handle IN proc-hdl NO-ERROR.
  proc-window = WIDGET-HANDLE( RETURN-VALUE ).
  proc-window:HIDDEN = Yes.

  RUN adm-destroy   IN proc-hdl.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit W-Win 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
   APPLY "CLOSE":U TO THIS-PROCEDURE.
   
   RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE make-bq-entry W-Win 
PROCEDURE make-bq-entry :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER bq-program AS CHAR NO-UNDO.
DEF INPUT PARAMETER bq-params AS CHAR NO-UNDO.
DEF INPUT PARAMETER bq-date AS DATE NO-UNDO.
DEF INPUT PARAMETER bq-time AS INT NO-UNDO.

DEF VAR last-active-date AS DATE NO-UNDO.
DEF VAR last-active-time AS INT NO-UNDO INITIAL -20000.

{inc/ofc-set.i "Batch-Queue-Enabled" "batch-queue-enabled"}

IF AVAILABLE(OfficeSetting) THEN DO:
  last-active-date = DATE( ENTRY(1,batch-queue-enabled, " ") ).
  last-active-time = INT( ENTRY(2,batch-queue-enabled, " ") ).
END.
IF last-active-date < TODAY OR last-active-time < (TIME - 180) THEN DO:
  RUN set-busy.
  RUN VALUE( bq-program ) ( bq-params ).
  RUN set-idle.
  RETURN .
END.

DEF BUFFER BQ FOR BatchQueue.
DEF VAR printer-setting AS CHAR NO-UNDO.

  FIND RP NO-LOCK WHERE RP.UserName = user-name
                    AND RP.ReportID = "Current Printer" NO-ERROR.

  IF AVAILABLE(RP) AND RP.Char4 <> "" THEN
    printer-setting = RP.Char4.
  ELSE IF AVAILABLE(RP) THEN
    printer-setting = RP.Char2.
  ELSE
    printer-setting = SESSION:PRINTER-PORT.

  DO TRANSACTION:
    CREATE BQ.
    ASSIGN  BQ.RunDate        = bq-date
            BQ.RunTime        = bq-time
            BQ.RunProgram     = bq-program
            BQ.RunParameters  = bq-params
            BQ.RunStatus      = "TODO"
            BQ.RunOutput      = printer-setting
            BQ.UserName       = user-name .

    /* now ensure we have no 'batch queue' scoped or locked */
    FIND FIRST BQ NO-LOCK NO-ERROR.
    FIND PREV BQ NO-LOCK NO-ERROR.
  END.

  MESSAGE "Program Queued for Processing" SKIP "to" printer-setting VIEW-AS ALERT-BOX INFORMATION
          TITLE "Batch Queue".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rec-draw W-Win 
PROCEDURE rec-draw :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT        PARAMETER proc-hdl AS HANDLE NO-UNDO.
DEF INPUT        PARAMETER x AS INT NO-UNDO.
DEF INPUT-OUTPUT PARAMETER y AS INT NO-UNDO.

  DEF VAR node-btn   AS HANDLE NO-UNDO.
  DEF VAR i          AS INT INIT 1 NO-UNDO.
  DEF VAR children   AS CHAR NO-UNDO.
  DEF VAR child      AS HANDLE NO-UNDO.

  node-btn = WIDGET-HANDLE( proc-hdl:PRIVATE-DATA ).
  node-btn:x = x. node-btn:y = y.
  
  IF VALID-HANDLE( last-drawn ) AND node-btn:MOVE-AFTER-TAB-ITEM( last-drawn ) THEN.
  last-drawn = node-btn.
    
  RUN update-maxes IN THIS-PROCEDURE( node-btn ).

  RUN get-link-handle IN adm-broker-hdl ( proc-hdl, 'WINMGR-TARGET', OUTPUT children ). 

  y = y + node-btn:height-pixels.

  DO i = 1 TO NUM-ENTRIES( children ):
    child = WIDGET-HANDLE( ENTRY( i, children ) ).
    RUN rec-draw IN THIS-PROCEDURE( child, x + 20, INPUT-OUTPUT y ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE redraw W-Win 
PROCEDURE redraw :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR x AS INT INIT {&GAP} NO-UNDO.
  DEF VAR y AS INT INIT {&GAP} NO-UNDO.

  max-x = 120.
  max-y = 0.
  last-drawn = ?.
  
  HIDE FRAME {&FRAME-NAME}.
  RUN rec-draw IN THIS-PROCEDURE( root, x, INPUT-OUTPUT y ).
  RUN resize-window IN THIS-PROCEDURE( max-x , max-y ).
  ENABLE ALL WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-current-browser W-Win 
PROCEDURE refresh-current-browser :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR obj AS HANDLE NO-UNDO.
DEF VAR attribute-list AS CHAR NO-UNDO.

DEF VAR wh AS WIDGET-HANDLE NO-UNDO.
DEF VAR proc-hdl  AS HANDLE NO-UNDO.
DEF VAR proc-name AS CHAR   NO-UNDO.

  
  obj = get-window-program( CURRENT-WINDOW ).
  
  IF VALID-HANDLE( obj ) THEN DO:
    RUN notify-viewers IN obj ( "open-query":U ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-window-title W-Win 
PROCEDURE refresh-window-title :
/*------------------------------------------------------------------------------
  Purpose:     Get the description of the topic for this
               viewer and fix the window and button titles appropriately
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-hdl AS WIDGET-HANDLE NO-UNDO.

DEF VAR proc-win AS HANDLE NO-UNDO.
DEF VAR new-label AS CHAR NO-UNDO.
DEF VAR rs-links AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR rs-hdl AS WIDGET-HANDLE NO-UNDO.

  IF NOT VALID-HANDLE(proc-hdl) THEN RETURN.
  proc-win = proc-hdl:CURRENT-WINDOW.
  IF NOT VALID-HANDLE(proc-win) THEN RETURN.
  RUN get-link-handle IN adm-broker-hdl( proc-hdl, "record-source", OUTPUT rs-links).
  DO i = 1 TO NUM-ENTRIES(rs-links):
    rs-hdl = WIDGET-HANDLE( ENTRY( i, rs-links )).
    IF VALID-HANDLE(rs-hdl) THEN
      RUN get-topic-desc IN rs-hdl (OUTPUT new-label) NO-ERROR.
    IF new-label <> "" THEN LEAVE.
  END.

  proc-win:TITLE = window-title-prefix + new-label.

  RUN update-node( proc-hdl ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-button W-Win 
PROCEDURE remove-button :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-hdl AS HANDLE NO-UNDO.
  
  DEF VAR i AS INT INIT 0 NO-UNDO.
  DEF VAR j AS INT NO-UNDO.
  DEF VAR found AS LOGI INIT No NO-UNDO.
  
  /* Try to find the button */
  
  DO WHILE i <= n AND NOT found:
    i = i + 1.
    found = btn[i] = WIDGET-HANDLE( proc-hdl:PRIVATE-DATA ).
  END.
  
  IF found THEN DO:
    DELETE WIDGET btn[ i ].
    DO j = i TO n - 1:
      btn[ j ] = btn[ j + 1 ].
    END.
    n = n - 1.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-node W-Win 
PROCEDURE remove-node :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-hdl AS HANDLE NO-UNDO.

  IF proc-hdl = root THEN DO:
    APPLY 'CLOSE':U TO THIS-PROCEDURE.
    IF uib-operational-mode THEN RETURN. ELSE QUIT.
  END.

  FIND Node WHERE Node.proc-hdl = proc-hdl NO-ERROR.
  IF NOT AVAILABLE Node THEN RETURN.
  FIND LinkNode WHERE LinkNode.NodeCode = Node.node-code NO-LOCK NO-ERROR. 

  IF AVAILABLE LinkNode AND LinkNode.NodeType <> "MV" THEN DO:

    DEF VAR par-str   AS CHAR   NO-UNDO.
    DEF VAR par       AS HANDLE NO-UNDO.
    DEF VAR par-win   AS HANDLE NO-UNDO.

    RUN get-link-handle IN adm-broker-hdl ( proc-hdl, 'WINMGR-SOURCE':U, OUTPUT par-str ).
    par = WIDGET-HANDLE( ENTRY( 1, par-str ) ).
    IF VALID-HANDLE(par) THEN DO:
      IF focus-parent-on-close THEN RUN focus-this-button IN THIS-PROCEDURE ( par ).
      RUN get-window-handle IN par NO-ERROR.
      par-win = WIDGET-HANDLE( RETURN-VALUE ).

      RUN remove-link IN adm-broker-hdl ( par, 'WINMGR', proc-hdl ).
      RUN kill-children IN THIS-PROCEDURE( proc-hdl ).
    END.
    RUN redraw IN THIS-PROCEDURE.

    /* focus on the parent of the window just deleted */
    RUN set-idle.
    IF focus-parent-on-close AND VALID-HANDLE( par ) THEN DO:
      CURRENT-WINDOW = par-win.
      APPLY 'ENTRY':U TO par-win.
    END.
  END.  
  ELSE
  DO:
    FOR EACH Link WHERE Link.src-hdl = proc-hdl: DELETE Link. END.
    DELETE Node.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resize-frame W-Win 
PROCEDURE resize-frame :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER w AS INT NO-UNDO.
  DEF INPUT PARAMETER h AS INT NO-UNDO.
  
  FRAME {&FRAME-NAME}:WIDTH-PIXELS  = w.
  FRAME {&FRAME-NAME}:HEIGHT-PIXELS = h.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resize-window W-Win 
PROCEDURE resize-window :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER w AS INT NO-UNDO.
DEF INPUT PARAMETER h AS INT NO-UNDO.

DEF VAR x-from-right AS INT NO-UNDO.

  x-from-right = ( w + {&GAP} ).
  IF x-from-right < 100 THEN x-from-right = 100.
  ASSIGN
    {&WINDOW-NAME}:X        = SESSION:WIDTH-PIXELS - x-from-right
    {&WINDOW-NAME}:Y        = {&GAP}
    {&WINDOW-NAME}:WIDTH-P  = w
    {&WINDOW-NAME}:HEIGHT-P = h.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-link W-Win 
PROCEDURE run-link :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR window-title AS CHAR NO-UNDO.

  FIND RL WHERE RL.btn-hdl = SELF. /* Jesus!! - SELF works, FOCUS doesn't !!! */
  RUN set-busy IN RL.src-hdl.
  
  IF RL.LinkType = "MSG" THEN DO:
    DEF VAR message-name AS CHAR NO-UNDO.
    DEF BUFFER Src FOR Node.
    FIND Src  WHERE Src.proc-hdl = RL.src-hdl NO-LOCK.
    RUN dispatch IN Src.vwr-hdl ( extract-attribute( RL.Function, "MESSAGE" ) ) NO-ERROR.

    /* Running the link may have caused the parent to close so check first */
    IF AVAILABLE RL AND VALID-HANDLE( RL.src-hdl ) THEN
      RUN set-idle IN RL.src-hdl.
  END.
  ELSE IF LOOKUP( RL.LinkType, "DRL,MNT,MNU,SEL" ) <> 0 THEN DO:

    IF link-running( OUTPUT window-title ) THEN RETURN.

    DEF BUFFER Dst FOR Node.
    DEF VAR prog-name AS CHAR NO-UNDO.
    
    FIND LinkNode WHERE LinkNode.NodeCode = RL.Target NO-LOCK NO-ERROR. 
    IF NOT AVAILABLE LinkNode THEN RETURN.
    IF TRIM(LinkNode.RunFile) = "" AND TRIM(LinkNode.File) = "" THEN RETURN "FAIL".
    RUN verify-prog( IF LinkNode.RunFile <> "" THEN LinkNode.RunFile ELSE LinkNode.File, OUTPUT prog-name ).
    IF RETURN-VALUE <> "FAIL" THEN
      RUN VALUE( prog-name ) PERSISTENT SET RL.dst-hdl.
    ELSE
      RETURN "FAIL".

    IF RL.LinkType = "SEL" THEN DO:
      DEF VAR ctr-hdl AS CHAR NO-UNDO.
      RUN get-link-handle IN adm-broker-hdl ( RL.src-hdl, 'CONTAINER-SOURCE', OUTPUT ctr-hdl ).
      RUN add-link IN adm-broker-hdl( WIDGET-HANDLE( ctr-hdl ), 'WINMGR', RL.dst-hdl ).
    END.
    ELSE  
      RUN add-link IN adm-broker-hdl ( RL.src-hdl, 'WINMGR':U, RL.dst-hdl ).

    RUN set-attribute-list IN RL.dst-hdl(
      "FileName = " + LinkNode.File + "," +
      "LinkId = "   + STRING( ROWID( RL ) ) ).

    RUN set-window-title( window-title ).      
    RUN dispatch IN RL.dst-hdl ( 'Initialize':U ).
    RUN update-node( RL.dst-hdl ).

    FIND Dst WHERE Dst.proc-hdl = RL.dst-hdl.
    ASSIGN Dst.Context = window-title.
    IF RL.LinkType <> "SEL" THEN RUN dispatch IN Dst.vwr-hdl ( 'row-available':U ).
    RUN iterate-widgets( Dst.frm-hdl, THIS-PROCEDURE, "set-db-tool-tip" ).

    /* Running the link may have caused the parent to close so check first */
    IF AVAILABLE RL AND VALID-HANDLE( RL.src-hdl ) THEN RUN set-idle IN RL.src-hdl.

    RUN dispatch IN (IF VALID-HANDLE( Dst.vwr-hdl ) THEN Dst.vwr-hdl ELSE RL.dst-hdl) ( 'apply-entry' ) NO-ERROR.
  END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-ok W-Win 
PROCEDURE select-ok :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-hdl AS HANDLE NO-UNDO.
  
DEF VAR new-id  AS ROWID NO-UNDO.
DEF VAR enabled AS LOGI  NO-UNDO.
DEF VAR fill-in-hdl AS HANDLE NO-UNDO.

  FIND Node WHERE Node.proc-hdl = proc-hdl.
  RUN get-curr-id IN Node.vwr-hdl ( OUTPUT new-id ).
  
  RUN get-attribute IN proc-hdl ( 'LinkId':U ).
  FIND Link WHERE ROWID( Link ) = TO-ROWID( RETURN-VALUE ).
  
  fill-in-hdl = Link.fil-hdl .
  fill-in-hdl:PRIVATE-DATA = STRING( new-id ).
    
  enabled = fill-in-hdl:SENSITIVE.
  fill-in-hdl:SENSITIVE = Yes.
  APPLY 'U2':U TO fill-in-hdl.
  fill-in-hdl:SENSITIVE = enabled.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records W-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartWindow, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-busy W-Win 
PROCEDURE set-busy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  SESSION:SET-WAIT-STATE("WAIT").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-db-identifiers W-Win 
PROCEDURE set-db-identifiers :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
{inc/ofc-this.i}
IF AVAILABLE(Office) THEN DO:
  FIND OfficeSettings OF Office WHERE OfficeSettings.SetName = unique-db-identifier() NO-LOCK NO-ERROR.
  IF AVAILABLE(OfficeSettings) THEN ASSIGN
        window-title-prefix = TRIM(ENTRY(1,OfficeSettings.SetValue,"|")) + " "
        system-description  = TRIM(ENTRY(2,OfficeSettings.SetValue,"|")) + " "
        window-icon-file    = TRIM(ENTRY(3,OfficeSettings.SetValue,"|")) + " "
        menu-bitmap-file    = TRIM(ENTRY(4,OfficeSettings.SetValue,"|")) + " " NO-ERROR .
  {inc/ofc-set.i "Last-Replicate-Data" "last-data"}
  IF NOT AVAILABLE(OfficeSetting) THEN last-data = "".
  {inc/ofc-set.i "Last-Replicate-Run" "last-run"}
  IF NOT AVAILABLE(OfficeSetting) THEN last-run = "".

  IF last-data <> "" THEN DO:
    system-description = "Change: " + last-data.
    IF last-run <> "" THEN
      system-description = system-description + ",   Load: " + last-run.
  END.
END.

IF window-title-prefix = ? THEN window-title-prefix = "".
IF system-description = ? THEN  system-description  = "Capital APMS" + " - " + unique-db-identifier() + " not set".
IF window-icon-file = ? THEN    window-icon-file    = "bitmaps/capital.ico".
IF menu-bitmap-file = ? THEN    menu-bitmap-file    = "bitmaps/mainmenu.bmp".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-db-tool-tip W-Win 
PROCEDURE set-db-tool-tip :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER wh AS HANDLE.
  
  DEF VAR tool-tip AS CHAR NO-UNDO.
  
  IF CAN-QUERY( wh, "TABLE" ) AND wh:TABLE <> ? THEN DO:
    FIND _File WHERE _File._File-Name = wh:TABLE NO-LOCK.
    FIND _Field
    WHERE _Field._File-RecID = RECID( _File )
      AND _Field._Field-Name = wh:NAME
      NO-LOCK NO-ERROR.
    IF AVAILABLE _Field THEN tool-tip =
      IF _Field._Help <> "" THEN _Field._Help ELSE
      IF _Field._Desc <> "" THEN _Field._Desc ELSE "".

    IF tool-tip <> ? AND tool-tip <> "" THEN wh:TOOLTIP = tool-tip.  
  END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-idle W-Win 
PROCEDURE set-idle :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  SESSION:SET-WAIT-STATE("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-link-attributes W-Win 
PROCEDURE set-link-attributes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-hdl AS HANDLE NO-UNDO.
DEF INPUT PARAMETER id-list AS CHAR    NO-UNDO.
DEF INPUT PARAMETER attribs AS CHAR    NO-UNDO.

DEF VAR i AS INT NO-UNDO.
DEF VAR id-entry AS CHAR NO-UNDO.

  FIND FIRST Node WHERE Node.proc-hdl = proc-hdl OR Node.vwr-hdl = proc-hdl NO-ERROR.
  IF NOT AVAILABLE Node THEN DO:
    MESSAGE "Node not valid - unable to set link attributes: " SKIP
           id-list SKIP
           attribs .
    RETURN.
  END.

  /* Modify the appropriate link from the given id ( Node Type dependent ) */  
  DO i = 1 TO NUM-ENTRIES( id-list ):
    id-entry = ENTRY( i, id-list ).
    IF id-entry = "ALL" THEN id-entry = "*".
    CASE id-entry:
      WHEN "Viewer" THEN DO:
          FOR EACH Link WHERE Link.src-hdl = Node.vwr-hdl:
            RUN each-link( attribs ).
          END.
        END.
      WHEN "Window" THEN DO:
          FOR EACH Link WHERE Link.src-hdl = Node.proc-hdl:
            RUN each-link( attribs ).
          END.
        END.
      OTHERWISE DO:
          FOR EACH Link WHERE Link.link-id MATCHES ENTRY( i, id-list )
              AND (Link.src-hdl = Node.proc-hdl OR Link.src-hdl = Node.vwr-hdl):
            RUN each-link( attribs ).
          END.
        END.
    END CASE.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-uib-modus-operandi W-Win 
PROCEDURE set-uib-modus-operandi :
/*------------------------------------------------------------------------------
  Purpose:  So that we act slightly differently if the uib is running
------------------------------------------------------------------------------*/
  uib-operational-mode = Yes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-window-icon W-Win 
PROCEDURE set-window-icon :
/*------------------------------------------------------------------------------
  Purpose: Set the window icon to the specified icon file.    
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER win-hdl AS HANDLE NO-UNDO.

  IF win-hdl:LOAD-ICON( window-icon-file ) THEN .

  win-hdl:KEEP-FRAME-Z-ORDER = focus-parent-on-close.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-window-title W-Win 
PROCEDURE set-window-title :
/*------------------------------------------------------------------------------
  Purpose:     Called from run link to set the window title
  Notes:       RL must be available
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-title AS CHAR NO-UNDO.

DEF VAR proc-win   AS HANDLE NO-UNDO.

  RUN get-window-handle IN RL.dst-hdl.
  proc-win = WIDGET-HANDLE( RETURN-VALUE ).
  IF VALID-HANDLE(proc-win) THEN
    proc-win:TITLE = window-title-prefix + proc-title.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed W-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE temp-filename W-Win 
PROCEDURE temp-filename :
/*------------------------------------------------------------------------------
  Purpose:  Return a temp file name
------------------------------------------------------------------------------*/

  DEF OUTPUT PARAMETER file-name AS CHAR NO-UNDO.

  file-name = OS-GETENV("TEMP") + "/proptemp.txt".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-maxes W-Win 
PROCEDURE update-maxes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER node-btn AS HANDLE NO-UNDO.

  IF node-btn:x + node-btn:width-pixels + {&GAP} > max-x THEN
    max-x = node-btn:x + node-btn:width-pixels + {&GAP}.
    
  IF node-btn:y + node-btn:height-pixels + {&GAP} > max-y THEN
    max-y = node-btn:y + node-btn:height-pixels + {&GAP}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-node W-Win 
PROCEDURE update-node :
/*------------------------------------------------------------------------------
  Purpose:     Updates the button description of the node
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER proc-hdl AS HANDLE NO-UNDO.

DEF VAR proc-btn AS HANDLE NO-UNDO.
DEF VAR proc-win AS HANDLE NO-UNDO.
DEF VAR new-label AS CHAR NO-UNDO.

  proc-btn = WIDGET-HANDLE( proc-hdl:PRIVATE-DATA ).
  IF NOT VALID-HANDLE(proc-btn) THEN DO:
  DEF BUFFER MyNode FOR Node.
    FIND FIRST MyNode WHERE MyNode.vwr-hdl = proc-hdl NO-ERROR.
    IF AVAILABLE(MyNode) THEN proc-hdl = MyNode.proc-hdl .
    proc-btn = WIDGET-HANDLE( proc-hdl:PRIVATE-DATA ).
  END.

  proc-win = proc-hdl:CURRENT-WINDOW.
  IF VALID-HANDLE(proc-win) THEN DO:
    new-label = proc-win:TITLE.
    IF VALID-HANDLE( proc-btn ) THEN DO:
      IF SUBSTRING( new-label, 1, LENGTH(window-title-prefix)) = window-title-prefix THEN
        new-label = SUBSTRING( new-label, LENGTH(window-title-prefix) + 1).
      proc-btn:HIDDEN = Yes.
      proc-btn:AUTO-RESIZE = Yes.
      proc-btn:LABEL = new-label.
      RUN crop-button( proc-btn ).
      RUN redraw IN THIS-PROCEDURE.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION extract-attribute W-Win 
FUNCTION extract-attribute RETURNS CHARACTER
  ( INPUT attribute-list AS CHAR, INPUT attribute-name AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR i       AS INT  NO-UNDO.
DEF VAR n       AS INT  NO-UNDO.
DEF VAR setting AS CHAR NO-UNDO.

  n = NUM-ENTRIES( attribute-list ).
  DO i = 1 TO n:
    setting = ENTRY( i, attribute-list ).
    IF TRIM( ENTRY( 1, setting, "=" ) ) = attribute-name
                       AND NUM-ENTRIES( setting, "=" ) > 1
    THEN
       RETURN TRIM( ENTRY( 2, setting, "=" ) ).
  END.
  
  RETURN "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-window-program W-Win 
FUNCTION get-window-program RETURNS HANDLE
  ( INPUT window-hdl AS WIDGET-HANDLE ) :
/*------------------------------------------------------------------------------
  Purpose:  Find the program for this window
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR proc-hdl AS WIDGET-HANDLE NO-UNDO.

  proc-hdl = SESSION:FIRST-PROCEDURE.
  REPEAT:
    IF VALID-HANDLE(proc-hdl) AND proc-hdl:CURRENT-WINDOW = window-hdl THEN
      RETURN proc-hdl.
    proc-hdl = proc-hdl:NEXT-SIBLING.
  END.

  RETURN ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-window-title W-Win 
FUNCTION get-window-title RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  RL Must be available
------------------------------------------------------------------------------*/

  DEF VAR proc-title AS CHAR   NO-UNDO.
  DEF VAR proc-win   AS HANDLE NO-UNDO.
  DEF VAR topic-vwr  AS HANDLE NO-UNDO.
  DEF VAR topic-desc AS CHAR   NO-UNDO.

  IF RL.LinkType = "SEL" THEN
  DO:
    DEF VAR select-item AS CHAR NO-UNDO.

    select-item =
      IF VALID-HANDLE( RL.fil-hdl ) AND RL.fil-hdl:label <> ? THEN RL.fil-hdl:label ELSE
      IF VALID-HANDLE( RL.cde-hdl ) AND RL.cde-hdl:label <> ? THEN RL.cde-hdl:label ELSE "".
      
    IF select-item = "" AND VALID-HANDLE( RL.cde-hdl ) THEN
    DO:
      DEF VAR prev-tab AS HANDLE NO-UNDO.
      prev-tab = RL.cde-hdl:PREV-TAB-ITEM.
      IF VALID-HANDLE( prev-tab ) THEN select-item = prev-tab:LABEL.
    END.
    
    topic-vwr = RL.src-hdl.
    proc-title = TRIM( "Select " + select-item ).
    
  END.
  ELSE
  DO:
    DEF BUFFER Src FOR Node.
    FIND Src WHERE Src.proc-hdl = RL.src-hdl.
    topic-vwr  = Src.vwr-hdl.
    proc-title = REPLACE( RL.btn-hdl:LABEL, "&", "" ).
  END.


  IF VALID-HANDLE( topic-vwr ) THEN DO:
    RUN get-topic-desc IN topic-vwr ( OUTPUT topic-desc ) NO-ERROR.
    IF NOT ERROR-STATUS:ERROR AND topic-desc <> "" THEN
      proc-title = proc-title + " - " + topic-desc.
  END.

  IF SUBSTRING( proc-title, 1, LENGTH(window-title-prefix) ) = window-title-prefix THEN
    proc-title = SUBSTRING( proc-title, LENGTH(window-title-prefix) + 1).

  RETURN proc-title.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION link-running W-Win 
FUNCTION link-running RETURNS LOGICAL
  ( OUTPUT window-title AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  RL must be available ( The run link record )
------------------------------------------------------------------------------*/
DEF VAR dest-win AS HANDLE NO-UNDO.
DEF BUFFER TestNode FOR Node.

  window-title = get-window-title().

  IF RL.LinkType = "MSG" /* Message links are never running */
     THEN RETURN No.

  /* Check to see if the link is already running in the given context */
  FIND FIRST TestNode WHERE TestNode.Node-Code = RL.Target 
                        AND TestNode.Context = window-title 
                        AND VALID-HANDLE( TestNode.proc-hdl ) NO-LOCK NO-ERROR.
  IF AVAILABLE TestNode THEN DO:
    IF LOOKUP( RL.LinkType, "SEL,DRL,MNT" ) <> 0
      /* AND There is no topic description in the current viewer */
      AND NUM-ENTRIES( window-title, '-' ) <= 1
      /* AND The origin of the link is not a menu */
      AND NOT CAN-FIND( FIRST LinkNode WHERE
        LinkNode.NodeCode = RL.Source AND
        LinkNode.NodeType = "MN" )
      THEN RETURN No.

    RUN set-idle IN RL.src-hdl.
    RUN focus-window IN THIS-PROCEDURE ( TestNode.proc-hdl ).
    RETURN Yes.
    
  END.

  RETURN No.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION unique-db-identifier W-Win 
FUNCTION unique-db-identifier RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Returns a DB identifier which is built in a structured way from the
            various components of the DBPARM() list.
    Notes:  Only looks at the first database connected - this should be the most
            unique one, certainly currently.  It is possible to envisage a
            situation where this assumption is false.
------------------------------------------------------------------------------*/
DEF VAR db-parm AS CHAR NO-UNDO.
DEF VAR net-details AS CHAR NO-UNDO INITIAL "Local".
DEF VAR svr-name AS CHAR NO-UNDO INITIAL "".
DEF VAR svc-code AS CHAR NO-UNDO INITIAL "".
DEF VAR db-name AS CHAR NO-UNDO INITIAL "".
DEF VAR single-user AS LOGICAL NO-UNDO INITIAL No.
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.
DEF VAR parm AS CHAR NO-UNDO.

  db-parm = DBPARAM(1).
  IF db-parm = ? THEN RETURN ?.
  n = NUM-ENTRIES(db-parm).
  DO i = 1 TO n:
    parm = ENTRY(i,db-parm).
    CASE ENTRY(1,parm," "):
      WHEN "-1" THEN    single-user = Yes.
      WHEN "-db" THEN   db-name = ENTRY(2, parm, " ").
      WHEN "-N" THEN    net-details = ENTRY(2, parm, " ").
      WHEN "-H" THEN    svr-name = ENTRY(2, parm, " ").
      WHEN "-S" THEN    svc-code = ENTRY(2, parm, " ").
    END CASE.
  END.

  IF SUBSTRING( db-name, LENGTH(db-name) - 2) = ".DB" THEN
    db-name = SUBSTRING( db-name, 1, LENGTH(db-name) - 3).

  IF net-details <> "Local" THEN
    net-details = net-details + ":" + svr-name + ":" + svc-code.

  RETURN net-details + "-" + db-name + (IF single-user THEN "-1" ELSE "").

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

