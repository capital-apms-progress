&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------
------------------------------------------------------------------------*/

DEF INPUT-OUTPUT PARAMETER search-text AS CHAR NO-UNDO.
DEF INPUT-OUTPUT PARAMETER search-flag AS INT NO-UNDO.

DEF VAR search-onwards   AS LOGICAL NO-UNDO.
DEF VAR search-backwards AS LOGICAL NO-UNDO.
DEF VAR case-sense       AS LOGICAL NO-UNDO.
DEF VAR wrap-round       AS LOGICAL NO-UNDO.
DEF VAR fnd-select       AS LOGICAL NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE DIALOG-BOX

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fil_SearchFor tgl_MatchCase rs_Direction ~
tgl_WrapAtEnd Btn_OK Btn_Cancel Btn_Help 
&Scoped-Define DISPLAYED-OBJECTS fil_SearchFor tgl_MatchCase rs_Direction ~
tgl_WrapAtEnd 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "&Cancel" 
     SIZE 15 BY 1.15
     BGCOLOR 8 .

DEFINE BUTTON Btn_Help 
     LABEL "&Help" 
     SIZE 15 BY 1.15
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO DEFAULT 
     LABEL "&OK" 
     SIZE 15 BY 1.15
     BGCOLOR 8 .

DEFINE VARIABLE fil_SearchFor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Find what" 
     VIEW-AS FILL-IN 
     SIZE 55.43 BY 1 NO-UNDO.

DEFINE VARIABLE rs_Direction AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Backwards", "Up",
"Forwards", "Down"
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE tgl_MatchCase AS LOGICAL INITIAL no 
     LABEL "Match Case" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.57 BY .8 NO-UNDO.

DEFINE VARIABLE tgl_WrapAtEnd AS LOGICAL INITIAL no 
     LABEL "Wrap at end" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.57 BY .8 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fil_SearchFor AT ROW 1.4 COL 7 COLON-ALIGNED
     tgl_MatchCase AT ROW 3 COL 9
     rs_Direction AT ROW 3.6 COL 40.43 NO-LABEL
     tgl_WrapAtEnd AT ROW 4 COL 9
     Btn_OK AT ROW 5.6 COL 1.57
     Btn_Cancel AT ROW 5.6 COL 17.57
     Btn_Help AT ROW 5.6 COL 49.57
     "Direction:" VIEW-AS TEXT
          SIZE 6.86 BY .65 AT ROW 3 COL 38.72
     SPACE(19.55) SKIP(3.39)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 10
         TITLE "Find"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: DIALOG-BOX
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
                                                                        */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Find */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  search-flag = - search-flag .
  APPLY 'GO':U TO FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Help
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Help Dialog-Frame
ON CHOOSE OF Btn_Help IN FRAME Dialog-Frame /* Help */
OR HELP OF FRAME {&FRAME-NAME}
DO: /* Call Help Function (or a simple message). */
  MESSAGE "Help for File: {&FILE-NAME}" VIEW-AS ALERT-BOX INFORMATION.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  RUN get-values.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

RUN split-flags.
DO WITH FRAME {&FRAME-NAME}:
  ASSIGN  rs_Direction  = (IF search-onwards THEN "Down" ELSE "Up")
          tgl_MatchCase = case-sense
          tgl_WrapAtEnd = wrap-round
          fil_SearchFor = search-text.
  DISPLAY {&ENABLED-FIELDS}.
END.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fil_SearchFor tgl_MatchCase rs_Direction tgl_WrapAtEnd 
      WITH FRAME Dialog-Frame.
  ENABLE fil_SearchFor tgl_MatchCase rs_Direction tgl_WrapAtEnd Btn_OK 
         Btn_Cancel Btn_Help 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-values Dialog-Frame 
PROCEDURE get-values :
/*------------------------------------------------------------------------------
  Purpose:  Get current values and set parameters from them
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  ASSIGN {&ENABLED-FIELDS}.

  search-text = INPUT fil_SearchFor.

  search-flag = (IF INPUT rs_Direction = "Up" THEN  2 ELSE 1)
              + (IF INPUT tgl_MatchCase       THEN  4 ELSE 0)
              + (IF INPUT tgl_WrapAtEnd       THEN 16 ELSE 0)
              + (IF fnd-select                THEN 32 ELSE 0).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE split-flags Dialog-Frame 
PROCEDURE split-flags :
/*------------------------------------------------------------------------------
  Purpose:  Split the flags byte into individual flags
------------------------------------------------------------------------------*/
DEF VAR flg AS DECIMAL NO-UNDO.

  flg = search-flag / 2.
  search-onwards    = (TRUNCATE( flg, 0 ) <> flg) .     /*  1 */
  flg = TRUNCATE(flg, 0) / 2.
  search-backwards  = (TRUNCATE( flg, 0 ) <> flg) .     /*  2 */
  flg = TRUNCATE(flg, 0) / 2.
  case-sense        = (TRUNCATE( flg, 0 ) <> flg) .     /*  4 */
  flg = TRUNCATE(flg, 0) / 2.
                                              /* '8' is not used. */
  flg = TRUNCATE(flg, 0) / 2.
  wrap-round        = (TRUNCATE( flg, 0 ) <> flg) .     /* 16 */
  flg = TRUNCATE(flg, 0) / 2.
  fnd-select        = (TRUNCATE( flg, 0 ) <> flg) .     /* 32 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


