&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES FlowTaskType
&Scoped-define FIRST-EXTERNAL-TABLE FlowTaskType


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR FlowTaskType.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS FlowTaskType.FlowTaskType ~
FlowTaskType.Description FlowTaskType.InitialStep 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}FlowTaskType ~{&FP2}FlowTaskType ~{&FP3}~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}~
 ~{&FP1}InitialStep ~{&FP2}InitialStep ~{&FP3}
&Scoped-define ENABLED-TABLES FlowTaskType
&Scoped-define FIRST-ENABLED-TABLE FlowTaskType
&Scoped-Define ENABLED-OBJECTS RECT-29 
&Scoped-Define DISPLAYED-FIELDS FlowTaskType.FlowTaskType ~
FlowTaskType.Description FlowTaskType.InitialStep 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
FlowTaskType|y|y|TTPL.FlowTaskType.FlowTaskType
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "FlowTaskType",
     Keys-Supplied = "FlowTaskType"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE RECTANGLE RECT-29
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 60.57 BY 4.4.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     FlowTaskType.FlowTaskType AT ROW 1.2 COL 4.72 COLON-ALIGNED
          LABEL "Task"
          VIEW-AS FILL-IN 
          SIZE 6.86 BY 1.05
     FlowTaskType.Description AT ROW 3 COL 9.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 49.14 BY 1.05
     FlowTaskType.InitialStep AT ROW 4.2 COL 9.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9.86 BY 1.05
     RECT-29 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.FlowTaskType
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 8.5
         WIDTH              = 76.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN FlowTaskType.FlowTaskType IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'FlowTaskType':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = FlowTaskType
           &WHERE = "WHERE FlowTaskType.FlowTaskType eq key-value"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "FlowTaskType"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "FlowTaskType"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:  Quit without applying any changes
------------------------------------------------------------------------------*/
  IF mode = "Add" THEN DO TRANSACTION:
    FIND CURRENT FlowTaskType NO-ERROR.
    IF AVAILABLE(FlowTaskType) THEN DO:
      FOR EACH FlowStepType OF FlowTaskType:
        FOR EACH FlowRule OF FlowStepType:
          DELETE FlowRule.
        END.
        DELETE FlowStepType.
      END.
      DELETE FlowTaskType.
    END.
  END.

  RUN check-modified( 'clear':U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN verify-flow-task.
  IF RETURN-VALUE <> "" THEN RETURN.

  RUN dispatch( 'update-record':U ).
  DO TRANSACTION:
    FIND CURRENT FlowTaskType EXCLUSIVE-LOCK.
    RUN create-default-step( FlowTaskType.FlowTaskType,
                             INPUT-OUTPUT FlowTaskType.InitialStep ).
    FIND CURRENT FlowTaskType NO-LOCK.
  END.
  IF mode = "add" THEN RUN notify( 'open-query,record-source':U ).
  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-default-step V-table-Win 
PROCEDURE create-default-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER task-type AS CHAR NO-UNDO.
DEF INPUT-OUTPUT PARAMETER step-type AS CHAR NO-UNDO.

  FIND FIRST FlowStepType WHERE FlowStepType.FlowTaskType = task-type NO-LOCK NO-ERROR.
  IF AVAILABLE(FlowStepType) THEN DO:
    step-type = FlowStepType.FlowStepType.
    RETURN.
  END.

  FIND FlowStepType WHERE FlowStepType.FlowTaskType = task-type
                      AND FlowStepType.FlowStepType = step-type NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(FlowStepType) THEN DO:
    CREATE FlowStepType.
    ASSIGN FlowStepType.FlowTaskType = task-type
           FlowStepType.FlowStepType = step-type
           FlowStepType.Description  = "Perform Task"
           FlowStepType.InitialStatus = "TODO".
  END.

  FIND FIRST FlowRule OF FlowStepType NO-LOCK NO-ERROR.
  IF AVAILABLE(FlowRule) THEN RETURN.

  CREATE FlowRule.
  ASSIGN FlowRule.FlowTaskType = task-type
         FlowRule.FlowStepType = step-type
         FlowRule.FlowRuleCode = "FINISH"
         FlowRule.Description  = "Mark task as completed"
         FlowRule.Priority     = 9
         FlowRule.FlowRuleType = "MANUAL"
         FlowRule.ConditionProgram      = "workflow/rules/always.p"
         FlowRule.ConditionProgramParam = "test"
         FlowRule.ActionProgram         = "workflow/rules/always.p"
         FlowRule.ActionProgramParam    = "action" .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = 'Add' THEN DO:
    have-records = Yes.
    CURRENT-WINDOW:TITLE = "Adding a New Workflow Task".
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "Maintain" THEN
    RUN dispatch( 'enable-fields':U ).
  ELSE
    RUN dispatch( 'disable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  FIND FlowTaskType WHERE FlowTaskType.FlowTaskType = ? NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(FlowTaskType) THEN CREATE FlowTaskType.

  ASSIGN FlowTaskType.FlowTaskType = ?
         FlowTaskType.InitialStep  = "TASK" .

  CURRENT-WINDOW:TITLE = "Adding a New Workflow Task".
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "FlowTaskType" "FlowTaskType" "FlowTaskType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "FlowTaskType"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-flow-task V-table-Win 
PROCEDURE verify-flow-task :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF INPUT FlowTaskType.FlowTaskType = ? OR TRIM(INPUT FlowTaskType.FlowTaskType) = "" THEN DO:
    MESSAGE "You must choose a unique Flow Task code."
            VIEW-AS ALERT-BOX ERROR TITLE "Unassigned Flow Task Code".
    APPLY 'ENTRY':U TO FlowTaskType.FlowTaskType.
    RETURN "FAIL".
  END.

  IF INPUT FlowTaskType.FlowTaskType <> FlowTaskType.FlowTaskType AND
          CAN-FIND( FlowTaskType WHERE FlowTaskType.FlowTaskType = INPUT FlowTaskType.FlowTaskType ) THEN DO:
    MESSAGE "A Flow Task already exists with the code" INPUT FlowTaskType.FlowTaskType "!" SKIP
            "You must choose a unique FlowTaskType code."
            VIEW-AS ALERT-BOX ERROR TITLE "Duplicate Flow Task Code".
    APPLY 'ENTRY':U TO FlowTaskType.FlowTaskType.
    RETURN "FAIL".
  END.

  IF TRIM(INPUT FlowTaskType.Description) = "" THEN DO:
    MESSAGE "You must supply a description for the Flow Task!"
            VIEW-AS ALERT-BOX ERROR TITLE "Incomplete Name".
    APPLY 'ENTRY':U TO FlowTaskType.Description.
    RETURN "FAIL".
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


