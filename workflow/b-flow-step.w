&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------

  File:         workflow/b-flow-task.w

  Description:  Browser for work flow tasks

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{inc/topic/tpflwtsk.i}

DEF VAR entity-type-filter AS CHAR NO-UNDO INITIAL "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES FlowTask
&Scoped-define FIRST-EXTERNAL-TABLE FlowTask


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR FlowTask.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES FlowStep

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table FlowStep.StepDueDate ~
FlowStep.NotifyDate FlowStep.Priority FlowStep.ActivityStatus ~
FlowStep.FlowTaskType FlowStep.FlowStepType FlowStep.EntityType ~
FlowStep.EntityCode FlowStep.Description 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table 
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH FlowStep OF FlowTask WHERE ~{&KEY-PHRASE} NO-LOCK ~
    ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table FlowStep
&Scoped-define FIRST-TABLE-IN-QUERY-br_table FlowStep


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
Joblogno|y|y|TTPL.FlowTask.Joblogno
EntityCode|y|y|TTPL.FlowTask.EntityCode
ProjectCode|y|y|TTPL.FlowTask.EntityCode
TenantCode|y|y|TTPL.FlowTask.EntityCode
CompanyCode|y|y|TTPL.FlowTask.EntityCode
PropertyCode|y|y|TTPL.FlowTask.EntityCode
EntityType|y|y|TTPL.FlowTask.EntityType
PersonCode|y|y|TTPL.FlowTask.AllocatedTo
FlowTaskType|y|y|TTPL.FlowTask.FlowTaskType
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "Joblogno,EntityCode,ProjectCode,TenantCode,CompanyCode,PropertyCode,EntityType,PersonCode,FlowTaskType",
     Keys-Supplied = "Joblogno,EntityCode,ProjectCode,TenantCode,CompanyCode,PropertyCode,EntityType,PersonCode,FlowTaskType"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
Due Date|y||TTPL.FlowTask.DueDate|yes
Entity|||TTPL.FlowTask.EntityType|yes,TTPL.FlowTask.EntityCode|yes
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = "Due Date,Entity",
     SortBy-Case = Due Date':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).

/* This SmartObject is a valid SortBy-Target. */
&IF '{&user-supported-links}':U ne '':U &THEN
  &Scoped-define user-supported-links {&user-supported-links},SortBy-Target
&ELSE
  &Scoped-define user-supported-links SortBy-Target
&ENDIF

/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      FlowStep SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      FlowStep.StepDueDate COLUMN-LABEL "Step Due On"
      FlowStep.NotifyDate COLUMN-LABEL " Notify  After"
      FlowStep.Priority COLUMN-LABEL "Pri"
      FlowStep.ActivityStatus COLUMN-LABEL "Status"
      FlowStep.FlowTaskType COLUMN-LABEL "  Task"
      FlowStep.FlowStepType COLUMN-LABEL "Step Type"
      FlowStep.EntityType COLUMN-LABEL "T"
      FlowStep.EntityCode COLUMN-LABEL " Entity" FORMAT "99999"
      FlowStep.Description
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 85.72 BY 12.4
         BGCOLOR 16 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   External Tables: TTPL.FlowTask
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 19.05
         WIDTH              = 89.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "TTPL.FlowStep OF TTPL.FlowTask"
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _TblOptList       = ", OUTER"
     _FldNameList[1]   > TTPL.FlowStep.StepDueDate
"FlowStep.StepDueDate" "Step Due On" ? "date" ? ? ? ? ? ? no ?
     _FldNameList[2]   > TTPL.FlowStep.NotifyDate
"FlowStep.NotifyDate" " Notify  After" ? "date" ? ? ? ? ? ? no ?
     _FldNameList[3]   > TTPL.FlowStep.Priority
"FlowStep.Priority" "Pri" ? "integer" ? ? ? ? ? ? no ?
     _FldNameList[4]   > TTPL.FlowStep.ActivityStatus
"FlowStep.ActivityStatus" "Status" ? "character" ? ? ? ? ? ? no ?
     _FldNameList[5]   > TTPL.FlowStep.FlowTaskType
"FlowStep.FlowTaskType" "  Task" ? "character" ? ? ? ? ? ? no ?
     _FldNameList[6]   > TTPL.FlowStep.FlowStepType
"FlowStep.FlowStepType" "Step Type" ? "character" ? ? ? ? ? ? no ?
     _FldNameList[7]   > TTPL.FlowStep.EntityType
"FlowStep.EntityType" "T" ? "character" ? ? ? ? ? ? no ?
     _FldNameList[8]   > TTPL.FlowStep.EntityCode
"FlowStep.EntityCode" " Entity" "99999" "integer" ? ? ? ? ? ? no ?
     _FldNameList[9]   = TTPL.FlowStep.Description
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

RUN set-attribute-list( 'SortBy-Options = Due Date|Entity, SortBy-Case = Due Date':U ).
RUN set-attribute-list( 'FilterBy-Options = All|To do|Completed, FilterBy-Case = To do':U ).
RUN set-attribute-list( 'SearchBy-Case = ':U ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'Joblogno':U THEN DO:
       &Scope KEY-PHRASE FlowTask.Joblogno eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Due Date':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.DueDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.EntityType BY FlowTask.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* Joblogno */
    WHEN 'EntityCode':U THEN DO:
       &Scope KEY-PHRASE FlowTask.EntityCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Due Date':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.DueDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.EntityType BY FlowTask.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* EntityCode */
    WHEN 'ProjectCode':U THEN DO:
       &Scope KEY-PHRASE FlowTask.EntityCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Due Date':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.DueDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.EntityType BY FlowTask.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* ProjectCode */
    WHEN 'TenantCode':U THEN DO:
       &Scope KEY-PHRASE FlowTask.EntityCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Due Date':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.DueDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.EntityType BY FlowTask.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* TenantCode */
    WHEN 'CompanyCode':U THEN DO:
       &Scope KEY-PHRASE FlowTask.EntityCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Due Date':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.DueDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.EntityType BY FlowTask.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* CompanyCode */
    WHEN 'PropertyCode':U THEN DO:
       &Scope KEY-PHRASE FlowTask.EntityCode eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Due Date':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.DueDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.EntityType BY FlowTask.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* PropertyCode */
    WHEN 'EntityType':U THEN DO:
       &Scope KEY-PHRASE FlowTask.EntityType eq key-value
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Due Date':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.DueDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.EntityType BY FlowTask.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* EntityType */
    WHEN 'PersonCode':U THEN DO:
       &Scope KEY-PHRASE FlowTask.AllocatedTo eq INTEGER(key-value)
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Due Date':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.DueDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.EntityType BY FlowTask.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* PersonCode */
    WHEN 'FlowTaskType':U THEN DO:
       &Scope KEY-PHRASE FlowTask.FlowTaskType eq key-value
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Due Date':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.DueDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.EntityType BY FlowTask.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* FlowTaskType */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       RUN get-attribute ('SortBy-Case':U).
       CASE RETURN-VALUE:
         WHEN 'Due Date':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.DueDate
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         WHEN 'Entity':U THEN DO:
           &Scope SORTBY-PHRASE BY FlowTask.EntityType BY FlowTask.EntityCode
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END.
         OTHERWISE DO:
           &Undefine SORTBY-PHRASE
           {&OPEN-QUERY-{&BROWSE-NAME}}
         END. /* OTHERWISE...*/
       END CASE.
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "FlowTask"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "FlowTask"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE mark-as-completed B-table-Win 
PROCEDURE mark-as-completed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR due-date AS DATE NO-UNDO.
DEF VAR task-no AS INT NO-UNDO.
DEF VAR results AS CHAR NO-UNDO.

  IF NOT AVAILABLE(FlowTask) THEN RETURN.
  due-date = FlowTask.DueDate.
  task-no = FlowTask.FlowTaskNo.
  RUN workflow/update-task.p( INPUT-OUTPUT due-date, INPUT-OUTPUT task-no, "TaskDone",
                                "", OUTPUT results ).

  {&BROWSE-NAME}:REFRESH() IN FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE mark-as-todo B-table-Win 
PROCEDURE mark-as-todo :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(FlowTask) THEN RETURN.

DO TRANSACTION:
  FIND CURRENT FlowTask EXCLUSIVE-LOCK NO-ERROR.
  FOR EACH FlowStep OF FlowTask WHERE FlowStep.ActivityStatus = "DONE" EXCLUSIVE-LOCK:
    FlowStep.ActivityStatus = "TODO".
    FlowStep.StatusUpdateDate = TODAY.
  END.
  FlowTask.ActivityStatus = "TODO".
  FlowTask.StatusUpdateDate = TODAY.
  FIND CURRENT FlowTask NO-LOCK.
END.

  {&BROWSE-NAME}:REFRESH() IN FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "Joblogno" "FlowTask" "Joblogno"}
  {src/adm/template/sndkycas.i "EntityCode" "FlowTask" "EntityCode"}
  {src/adm/template/sndkycas.i "ProjectCode" "FlowTask" "EntityCode"}
  {src/adm/template/sndkycas.i "TenantCode" "FlowTask" "EntityCode"}
  {src/adm/template/sndkycas.i "CompanyCode" "FlowTask" "EntityCode"}
  {src/adm/template/sndkycas.i "PropertyCode" "FlowTask" "EntityCode"}
  {src/adm/template/sndkycas.i "EntityType" "FlowTask" "EntityType"}
  {src/adm/template/sndkycas.i "PersonCode" "FlowTask" "AllocatedTo"}
  {src/adm/template/sndkycas.i "FlowTaskType" "FlowTask" "FlowTaskType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "FlowTask"}
  {src/adm/template/snd-list.i "FlowStep"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.

  IF new-key-name = "ProjectCode" THEN          entity-type-filter = "J".
  ELSE IF new-key-name = "TenantCode" THEN      entity-type-filter = "T".
  ELSE IF new-key-name = "CompanyCode" THEN     entity-type-filter = "L".
  ELSE IF new-key-name = "PropertyCode" THEN    entity-type-filter = "P".
  ELSE IF new-key-name = "EntityCode" THEN
    entity-type-filter = find-parent-key( "EntityType" ).
  ELSE
    entity-type-filter = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


