&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.
DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO   INITIAL ?.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES FlowStepType
&Scoped-define FIRST-EXTERNAL-TABLE FlowStepType


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR FlowStepType.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS FlowStepType.FlowStepType ~
FlowStepType.Description FlowStepType.InitialPriority ~
FlowStepType.InitialStatus FlowStepType.WarnPeriod FlowStepType.WarnUnits 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}FlowStepType ~{&FP2}FlowStepType ~{&FP3}~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}~
 ~{&FP1}InitialPriority ~{&FP2}InitialPriority ~{&FP3}~
 ~{&FP1}InitialStatus ~{&FP2}InitialStatus ~{&FP3}~
 ~{&FP1}WarnPeriod ~{&FP2}WarnPeriod ~{&FP3}~
 ~{&FP1}WarnUnits ~{&FP2}WarnUnits ~{&FP3}
&Scoped-define ENABLED-TABLES FlowStepType
&Scoped-define FIRST-ENABLED-TABLE FlowStepType
&Scoped-Define ENABLED-OBJECTS cmb_FlowTaskType 
&Scoped-Define DISPLAYED-FIELDS FlowStepType.FlowStepType ~
FlowStepType.Description FlowStepType.InitialPriority ~
FlowStepType.InitialStatus FlowStepType.WarnPeriod FlowStepType.WarnUnits 
&Scoped-Define DISPLAYED-OBJECTS cmb_FlowTaskType 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
FlowStepType|y|y|TTPL.FlowStepType.FlowStepType
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "FlowStepType",
     Keys-Supplied = "FlowStepType"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_FlowTaskType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Task Type" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "Item 1" 
     SIZE 44.57 BY 1 TOOLTIP "The type of step which the rule applies against" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     FlowStepType.FlowStepType AT ROW 1 COL 4.72 COLON-ALIGNED
          LABEL "Step"
          VIEW-AS FILL-IN 
          SIZE 8.57 BY 1.05
     cmb_FlowTaskType AT ROW 2.4 COL 8.14 COLON-ALIGNED
     FlowStepType.Description AT ROW 3.6 COL 8.14 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 44.57 BY 1.05
     FlowStepType.InitialPriority AT ROW 5.4 COL 12.14 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 5.14 BY 1.05
     FlowStepType.InitialStatus AT ROW 6.45 COL 12.14 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 5.14 BY 1.05
     FlowStepType.WarnPeriod AT ROW 7.8 COL 12.14 COLON-ALIGNED
          LABEL "Warn"
          VIEW-AS FILL-IN 
          SIZE 9.14 BY 1.05
     FlowStepType.WarnUnits AT ROW 7.8 COL 23 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 2.14 BY 1.05
     "before step is due" VIEW-AS TEXT
          SIZE 12.57 BY .65 AT ROW 8 COL 27.86
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.FlowStepType
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 9.8
         WIDTH              = 63.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN FlowStepType.FlowStepType IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN FlowStepType.WarnPeriod IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN FlowStepType.WarnUnits IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME cmb_FlowTaskType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_FlowTaskType V-table-Win
ON U1 OF cmb_FlowTaskType IN FRAME F-Main /* Task Type */
DO:
  {workflow/inc/scFwTskTyp1.i "FlowStepType" "FlowTaskType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_FlowTaskType V-table-Win
ON U2 OF cmb_FlowTaskType IN FRAME F-Main /* Task Type */
DO:
  {workflow/inc/scFwTskTyp2.i "FlowStepType" "FlowTaskType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'FlowStepType':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = FlowStepType
           &WHERE = "WHERE FlowStepType.FlowStepType eq key-value"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "FlowStepType"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "FlowStepType"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:  Quit without applying any changes
------------------------------------------------------------------------------*/
  IF mode = "Add" THEN DO TRANSACTION:
    FIND CURRENT FlowStepType NO-ERROR.
    IF AVAILABLE(FlowStepType) THEN DELETE FlowStepType.
  END.

  RUN check-modified( 'clear':U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN verify-flow-step.
  IF RETURN-VALUE <> "" THEN RETURN.

  RUN dispatch( 'update-record':U ).
  RUN notify( 'open-query,record-source':U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = 'Add' THEN DO:
    have-records = Yes.
    CURRENT-WINDOW:TITLE = "Adding a New Workflow Step".
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "Maintain" THEN
    RUN dispatch( 'enable-fields':U ).
  ELSE
    RUN dispatch( 'disable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  FIND FlowStepType WHERE FlowTaskType = key-value
                    AND FlowStepType.FlowStepType = ? NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(FlowStepType) THEN CREATE FlowStepType.

  ASSIGN FlowStepType.FlowTaskType = key-value 
         FlowStepType.FlowStepType = ?.
  
  CURRENT-WINDOW:TITLE = "Adding a New Workflow Step".
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'update-record':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "FlowStepType" "FlowStepType" "FlowStepType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "FlowStepType"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name V-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  key-name = new-key-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value V-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-value AS CHAR NO-UNDO.
  key-value = new-key-value.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-flow-step V-table-Win 
PROCEDURE verify-flow-step :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR input-task-type AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  input-task-type = TRIM( ENTRY( 1, cmb_FlowTaskType:SCREEN-VALUE, " ") ).
  IF input-task-type = ? OR TRIM(input-task-type) = "" THEN DO:
    MESSAGE "You must choose a unique Flow Task code."
            VIEW-AS ALERT-BOX ERROR TITLE "Unassigned Flow Task Code".
    APPLY 'ENTRY':U TO FlowStepType.FlowStepType.
    RETURN "FAIL".
  END.

  IF INPUT FlowStepType.FlowStepType = ? OR TRIM(INPUT FlowStepType.FlowStepType) = "" THEN DO:
    MESSAGE "You must choose a unique Flow Step code."
            VIEW-AS ALERT-BOX ERROR TITLE "Unassigned Flow Step Code".
    APPLY 'ENTRY':U TO FlowStepType.FlowStepType.
    RETURN "FAIL".
  END.

DEF BUFFER OtherStep FOR FlowStepType.
  IF ( INPUT FlowStepType.FlowStepType <> FlowStepType.FlowStepType
        OR input-task-type <> FlowStepType.FlowTaskType )
        AND CAN-FIND( OtherStep WHERE OtherStep.FlowStepType = INPUT FlowStepType.FlowStepType
                               AND OtherStep.FlowTaskType = input-task-type ) THEN DO:
    MESSAGE "A Flow Step already exists with the code" INPUT FlowStepType.FlowStepType "!" SKIP
            "for task" input-task-type SKIP(1)
            "You must choose a unique FlowStep code."
            VIEW-AS ALERT-BOX ERROR TITLE "Duplicate Flow Step Code".
    APPLY 'ENTRY':U TO FlowStepType.FlowStepType.
    RETURN "FAIL".
  END.

  IF TRIM(INPUT FlowStepType.Description) = "" THEN DO:
    MESSAGE "You must supply a description for the Flow Step!"
            VIEW-AS ALERT-BOX ERROR TITLE "Incomplete Name".
    APPLY 'ENTRY':U TO FlowStepType.Description.
    RETURN "FAIL".
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


