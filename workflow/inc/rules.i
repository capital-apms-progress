&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*--------------------------------------------------------------------------
    File        : workflow/inc/rules.i
    Purpose     : Standard include for workflow rules

    Syntax      : Just include it at the very beginning of a rule

    Description : Only defines the parameters as yet.  Might do more later.

    Author(s)   : Andrew McMillan
    Created     : 17/1/1999
    Notes       :
  ------------------------------------------------------------------------*/

DEF INPUT PARAMETER rule-params AS CHAR NO-UNDO.
DEF INPUT PARAMETER task-rowid AS ROWID NO-UNDO.
DEF INPUT PARAMETER step-rowid AS ROWID NO-UNDO.
DEF INPUT PARAMETER rule-rowid AS ROWID NO-UNDO.
DEF OUTPUT PARAMETER rule-success AS LOGI INITIAL No NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = .33
         WIDTH              = 38.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


