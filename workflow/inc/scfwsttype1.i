/* scFwStType1.i */
/* selection combo for an FlowStepType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="FlowStepType"
                     &ExtKey  ="FlowStepType"
                     &DV      ="FlowStepType.FlowStepType + ' - ' + FlowStepType.Description"
                     &Clause  ="{3}" }
