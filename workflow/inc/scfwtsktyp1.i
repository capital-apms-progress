/* scFwTskTyp1.i */
/* selection combo for an FlowTaskType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="FlowTaskType"
                     &ExtKey  ="FlowTaskType"
                     &DV      ="FlowTaskType.FlowTaskType + ' - ' + FlowTaskType.Description"}
