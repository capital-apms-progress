/* scFwRlType1.i */
/* selection combo for an FlowRuleType */

{inc/selcmb/scmst1.i &IntTable="{1}"
                     &IntCode ="{2}"
                     &ExtTable="FlowRuleType"
                     &ExtKey  ="FlowRuleType"
                     &DV      ="FlowRuleType.FlowRuleType + ' - ' + FlowRuleType.Description"}
