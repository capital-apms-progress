&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------

  File:

  Description: from VIEWER.W - Template for SmartViewer Objects

  Input Parameters:
      <none>

  Output Parameters:
      <none>

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.

DEF VAR parent-key-type AS CHAR NO-UNDO.
DEF VAR parent-task-key AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES FlowRule
&Scoped-define FIRST-EXTERNAL-TABLE FlowRule


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR FlowRule.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS FlowRule.FlowRuleCode FlowRule.Description ~
FlowRule.Priority FlowRule.NextTaskType FlowRule.NextStepType ~
FlowRule.ConditionProgram FlowRule.ConditionProgramParam ~
FlowRule.ActionProgram FlowRule.ActionProgramParam 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}FlowRuleCode ~{&FP2}FlowRuleCode ~{&FP3}~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}~
 ~{&FP1}Priority ~{&FP2}Priority ~{&FP3}~
 ~{&FP1}NextTaskType ~{&FP2}NextTaskType ~{&FP3}~
 ~{&FP1}NextStepType ~{&FP2}NextStepType ~{&FP3}~
 ~{&FP1}ConditionProgram ~{&FP2}ConditionProgram ~{&FP3}~
 ~{&FP1}ConditionProgramParam ~{&FP2}ConditionProgramParam ~{&FP3}~
 ~{&FP1}ActionProgram ~{&FP2}ActionProgram ~{&FP3}~
 ~{&FP1}ActionProgramParam ~{&FP2}ActionProgramParam ~{&FP3}
&Scoped-define ENABLED-TABLES FlowRule
&Scoped-define FIRST-ENABLED-TABLE FlowRule
&Scoped-Define ENABLED-OBJECTS RECT-8 cmb_FlowRuleType cmb_FlowTaskType ~
cmb_FlowStepType 
&Scoped-Define DISPLAYED-FIELDS FlowRule.FlowRuleCode FlowRule.Description ~
FlowRule.Priority FlowRule.NextTaskType FlowRule.NextStepType ~
FlowRule.ConditionProgram FlowRule.ConditionProgramParam ~
FlowRule.ActionProgram FlowRule.ActionProgramParam 
&Scoped-Define DISPLAYED-OBJECTS cmb_FlowRuleType cmb_FlowTaskType ~
cmb_FlowStepType 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD find-parent-keytype V-table-Win 
FUNCTION find-parent-keytype RETURNS CHARACTER
  ( /* no parameters */  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_FlowRuleType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rule Type" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "Item 1" 
     SIZE 44.57 BY 1 TOOLTIP "The type of rule - select the appropriate value from the drop-down list" NO-UNDO.

DEFINE VARIABLE cmb_FlowStepType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Step Type" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "Item 1" 
     SIZE 44.57 BY 1 TOOLTIP "The type of step which the rule applies against" NO-UNDO.

DEFINE VARIABLE cmb_FlowTaskType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Task Type" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "Item 1" 
     SIZE 44.57 BY 1 TOOLTIP "The type of step which the rule applies against" NO-UNDO.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 56 BY 12.2.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     FlowRule.FlowRuleCode AT ROW 1 COL 6.43 COLON-ALIGNED
          LABEL "Code"
          VIEW-AS FILL-IN 
          SIZE 8.57 BY 1.05
     FlowRule.Description AT ROW 2.2 COL 9.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 44.57 BY 1.05
     cmb_FlowRuleType AT ROW 3.6 COL 9.86 COLON-ALIGNED
     cmb_FlowTaskType AT ROW 4.8 COL 9.86 COLON-ALIGNED
     cmb_FlowStepType AT ROW 6 COL 9.86 COLON-ALIGNED
     FlowRule.Priority AT ROW 7.2 COL 9.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 4.57 BY 1.05
     FlowRule.NextTaskType AT ROW 7.2 COL 27 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 5.72 BY 1.05
     FlowRule.NextStepType AT ROW 7.2 COL 45.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 8.57 BY 1.05
     FlowRule.ConditionProgram AT ROW 8.6 COL 9.86 COLON-ALIGNED
          LABEL "Test for rule"
          VIEW-AS FILL-IN NATIVE 
          SIZE 44.57 BY 1.05
     FlowRule.ConditionProgramParam AT ROW 9.65 COL 9.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 44.57 BY 1.05
     FlowRule.ActionProgram AT ROW 11 COL 9.86 COLON-ALIGNED
          LABEL "Apply rule"
          VIEW-AS FILL-IN 
          SIZE 44.57 BY 1.05
     FlowRule.ActionProgramParam AT ROW 12.05 COL 9.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 44.57 BY 1.05
     RECT-8 AT ROW 1.2 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.FlowRule
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 17.15
         WIDTH              = 78.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN FlowRule.ActionProgram IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN FlowRule.ConditionProgram IN FRAME F-Main
   EXP-LABEL                                                            */
ASSIGN 
       FlowRule.ConditionProgram:MANUAL-HIGHLIGHT IN FRAME F-Main = TRUE.

/* SETTINGS FOR FILL-IN FlowRule.FlowRuleCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME cmb_FlowRuleType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_FlowRuleType V-table-Win
ON U1 OF cmb_FlowRuleType IN FRAME F-Main /* Rule Type */
DO:
  {workflow/inc/scFwRlType1.i "FlowRule" "FlowRuleType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_FlowRuleType V-table-Win
ON U2 OF cmb_FlowRuleType IN FRAME F-Main /* Rule Type */
DO:
  {workflow/inc/scFwRlType2.i "FlowRule" "FlowRuleType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_FlowStepType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_FlowStepType V-table-Win
ON U1 OF cmb_FlowStepType IN FRAME F-Main /* Step Type */
DO:
DEF VAR input-flow-task AS CHAR NO-UNDO.
  input-flow-task = TRIM( ENTRY( 1, cmb_FlowTaskType:SCREEN-VALUE, " ")).
/* MESSAGE "PTK" parent-task-key SKIP "IFT" input-flow-task. */
  IF parent-task-key <> "" AND parent-task-key <> ? THEN
    input-flow-task = parent-task-key.
  {workflow/inc/scFwStType1.i "FlowRule" "FlowStepType" "FlowStepType.FlowTaskType = input-flow-task"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_FlowStepType V-table-Win
ON U2 OF cmb_FlowStepType IN FRAME F-Main /* Step Type */
DO:
  {workflow/inc/scFwStType2.i "FlowRule" "FlowStepType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_FlowTaskType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_FlowTaskType V-table-Win
ON U1 OF cmb_FlowTaskType IN FRAME F-Main /* Task Type */
DO:
  {workflow/inc/scFwTskTyp1.i "FlowRule" "FlowTaskType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_FlowTaskType V-table-Win
ON U2 OF cmb_FlowTaskType IN FRAME F-Main /* Task Type */
DO:
  {workflow/inc/scFwTskTyp2.i "FlowRule" "FlowTaskType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME FlowRule.ConditionProgram
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FlowRule.ConditionProgram V-table-Win
ON ENTRY OF FlowRule.ConditionProgram IN FRAME F-Main /* Test for rule */
DO:
DO WITH FRAME {&FRAME-NAME}:
  FOCUS = {&SELF-NAME}:HANDLE.
  {&SELF-NAME}:MANUAL-HIGHLIGHT = Yes.
  {&SELF-NAME}:AUTO-ZAP = No.
  {&SELF-NAME}:CURSOR-OFFSET = LENGTH(TRIM({&SELF-NAME}:SCREEN-VALUE)).
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FlowRule.ConditionProgram V-table-Win
ON LEAVE OF FlowRule.ConditionProgram IN FRAME F-Main /* Test for rule */
DO:
DO WITH FRAME {&FRAME-NAME}:
  IF FlowRule.ConditionProgram:MODIFIED THEN DO:
    FlowRule.ActionProgram:SCREEN-VALUE = FlowRule.ConditionProgram:SCREEN-VALUE.
    FlowRule.ConditionProgram:MODIFIED = No.
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "FlowRule"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "FlowRule"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:  Quit without applying any changes
------------------------------------------------------------------------------*/
  IF mode = "Add" THEN DO TRANSACTION:
    FIND CURRENT FlowRule NO-ERROR.
    IF AVAILABLE(FlowRule) THEN DELETE FlowRule.
  END.

  RUN check-modified( 'clear':U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN verify-flow-rule.
  IF RETURN-VALUE <> "" THEN RETURN.

  RUN dispatch( 'update-record':U ).
  RUN notify( 'open-query,record-source':U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  CASE parent-key-type:
    WHEN "NextStepType" THEN DISABLE FlowRule.NextTaskType FlowRule.NextStepType .
    WHEN "FlowStepType" THEN DISABLE cmb_FlowTaskType cmb_FlowStepType .
  END CASE.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  parent-key-type = find-parent-keytype().
  IF mode = 'Add' THEN DO:
    have-records = Yes.
    CURRENT-WINDOW:TITLE = "Adding a New Workflow Rule".
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "Maintain" THEN
    RUN dispatch( 'enable-fields':U ).
  ELSE
    RUN dispatch( 'disable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
DEF VAR parent-step-key AS CHAR NO-UNDO.

  IF parent-key-type = "NextStepType" THEN ASSIGN
    parent-task-key = find-parent-key( "NextTaskType" )
    parent-step-key = find-parent-key( parent-key-type ).
  ELSE IF parent-key-type = ? THEN ASSIGN
    parent-task-key = find-parent-key( "FlowTaskType" )
    parent-step-key = find-parent-key( "FlowStepType" ).
  ELSE ASSIGN
    parent-task-key = find-parent-key( "FlowTaskType" )
    parent-step-key = find-parent-key( parent-key-type ).

  FIND FlowRule WHERE FlowRule.FlowRuleCode = ? NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(FlowRule) THEN CREATE FlowRule.

  IF parent-key-type = "NextStepType" THEN ASSIGN
    FlowRule.NextTaskType = parent-task-key
    FlowRule.NextStepType = parent-step-key.
  ELSE ASSIGN
    FlowRule.FlowTaskType = parent-task-key
    FlowRule.FlowStepType = parent-step-key.

  ASSIGN FlowRule.FlowRuleCode          = ?
         FlowRule.ConditionProgram      = "workflow/rules/"
         FlowRule.ConditionProgramParam = "Test"
         FlowRule.ActionProgram         = "workflow/rules/"
         FlowRule.ActionProgramParam    = "Action" .

  CURRENT-WINDOW:TITLE = "Adding a New Workflow Rule".
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'update-record':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "FlowRule"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-flow-rule V-table-Win 
PROCEDURE verify-flow-rule :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF INPUT FlowRule.FlowRuleCode = ? OR TRIM(INPUT FlowRule.FlowRuleCode) = "" THEN DO:
    MESSAGE "You must choose a unique Flow Rule code."
            VIEW-AS ALERT-BOX ERROR TITLE "Unassigned Flow Rule Code".
    APPLY 'ENTRY':U TO FlowRule.FlowRuleCode.
    RETURN "FAIL".
  END.

DEF BUFFER OtherRule FOR FlowRule.
  IF INPUT FlowRule.FlowRuleCode <> FlowRule.FlowRuleCode AND
          CAN-FIND( OtherRule WHERE OtherRule.FlowRuleCode = INPUT FlowRule.FlowRuleCode ) THEN DO:
    MESSAGE "A Flow Rule already exists with the code" INPUT FlowRule.FlowRuleCode "!" SKIP
            "You must choose a unique FlowRule code."
            VIEW-AS ALERT-BOX ERROR TITLE "Duplicate Flow Rule Code".
    APPLY 'ENTRY':U TO FlowRule.FlowRuleCode.
    RETURN "FAIL".
  END.

  IF TRIM(INPUT FlowRule.Description) = "" THEN DO:
    MESSAGE "You must supply a description for the Flow Rule!"
            VIEW-AS ALERT-BOX ERROR TITLE "Incomplete Name".
    APPLY 'ENTRY':U TO FlowRule.Description.
    RETURN "FAIL".
  END.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION find-parent-keytype V-table-Win 
FUNCTION find-parent-keytype RETURNS CHARACTER
  ( /* no parameters */  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR key-type  AS CHAR   NO-UNDO INITIAL ?.
DEF VAR curr-hdl  AS HANDLE NO-UNDO.
DEF VAR c-rec-src AS CHAR   NO-UNDO.
  
  RUN get-link-handle IN adm-broker-hdl
                     ( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT c-rec-src ).
  ASSIGN curr-hdl = WIDGET-HANDLE( c-rec-src ) NO-ERROR.

  IF VALID-HANDLE( curr-hdl ) THEN DO:
    RUN get-attribute IN curr-hdl( 'Key-Name':U ).
    key-type = RETURN-VALUE.
  END.

  RETURN key-type.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


