&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.

DEF VAR parent-key-type AS CHAR NO-UNDO.
DEF VAR parent-task-key AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES FlowTask
&Scoped-define FIRST-EXTERNAL-TABLE FlowTask


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR FlowTask.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS FlowTask.DueDate FlowTask.Description ~
FlowTask.Priority FlowTask.EntityCode 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}DueDate ~{&FP2}DueDate ~{&FP3}~
 ~{&FP1}Description ~{&FP2}Description ~{&FP3}~
 ~{&FP1}EntityCode ~{&FP2}EntityCode ~{&FP3}
&Scoped-define ENABLED-TABLES FlowTask
&Scoped-define FIRST-ENABLED-TABLE FlowTask
&Scoped-Define ENABLED-OBJECTS RECT-1 cmb_TaskType cmb_EntityType 
&Scoped-Define DISPLAYED-FIELDS FlowTask.DueDate FlowTask.FlowTaskNo ~
FlowTask.JobLogNo FlowTask.Description FlowTask.Priority ~
FlowTask.EntityCode 
&Scoped-Define DISPLAYED-OBJECTS cmb_TaskType cmb_EntityType fil_EntityName ~
fil_AllocatedTo 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD find-parent-keytype V-table-Win 
FUNCTION find-parent-keytype RETURNS CHARACTER
  ( /* no parameters */  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_EntityType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     SIZE 14.86 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_TaskType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Task Type" 
     VIEW-AS COMBO-BOX INNER-LINES 25
     LIST-ITEMS "Item 1" 
     SIZE 46.29 BY 1 NO-UNDO.

DEFINE VARIABLE fil_AllocatedTo AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 53.14 BY 1
     FONT 10 NO-UNDO.

DEFINE VARIABLE fil_EntityName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 36 BY 1.05 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 66.29 BY 8.8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     FlowTask.DueDate AT ROW 1.2 COL 8.14 COLON-ALIGNED
          LABEL "Due Date"
          VIEW-AS FILL-IN 
          SIZE 10.86 BY 1.05
     FlowTask.FlowTaskNo AT ROW 1.2 COL 31.57 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 7.14 BY 1.05
     FlowTask.JobLogNo AT ROW 1.2 COL 53.86 COLON-ALIGNED
          LABEL "Job number"
          VIEW-AS FILL-IN 
          SIZE 8 BY 1.05
     cmb_TaskType AT ROW 2.6 COL 8.14 COLON-ALIGNED
     FlowTask.Description AT ROW 3.8 COL 8.14 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 46.29 BY 1.05
     FlowTask.Priority AT ROW 5 COL 10.14 HELP
          "Priority of task" NO-LABEL
          VIEW-AS SLIDER MIN-VALUE -10 MAX-VALUE 10 HORIZONTAL 
          TIC-MARKS BOTTOM FREQUENCY 2
          SIZE 42.29 BY 1.8 TOOLTIP "Adjust the priority of the task by moving the slider"
     cmb_EntityType AT ROW 7.4 COL 8.14 COLON-ALIGNED
     FlowTask.EntityCode AT ROW 7.4 COL 23 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 5.72 BY 1.05
     fil_EntityName AT ROW 7.4 COL 28.72 COLON-ALIGNED NO-LABEL
     fil_AllocatedTo AT ROW 8.6 COL 11.57 COLON-ALIGNED NO-LABEL
     "Person:" VIEW-AS TEXT
          SIZE 5.14 BY 1 AT ROW 8.6 COL 4.43
     "Relaxed" VIEW-AS TEXT
          SIZE 6.29 BY 1 AT ROW 6 COL 4.43
     "Urgent" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 6 COL 52.43
     RECT-1 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.FlowTask
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 12.05
         WIDTH              = 69.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN FlowTask.DueDate IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN FlowTask.EntityCode IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fil_AllocatedTo IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fil_EntityName IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN FlowTask.FlowTaskNo IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN FlowTask.JobLogNo IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR SLIDER FlowTask.Priority IN FRAME F-Main
   EXP-HELP                                                             */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}
{inc/entity.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME cmb_EntityType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_EntityType V-table-Win
ON U1 OF cmb_EntityType IN FRAME F-Main /* Entity */
DO:
  {inc/selcmb/scety1.i "FlowTask" "EntityType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_EntityType V-table-Win
ON U2 OF cmb_EntityType IN FRAME F-Main /* Entity */
DO:
  {inc/selcmb/scety2.i "FlowTask" "EntityType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_EntityType V-table-Win
ON VALUE-CHANGED OF cmb_EntityType IN FRAME F-Main /* Entity */
DO:
DO WITH FRAME {&FRAME-NAME}:
  APPLY "ENTRY":U TO FlowTask.EntityCode .
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_TaskType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_TaskType V-table-Win
ON U1 OF cmb_TaskType IN FRAME F-Main /* Task Type */
DO:
  {workflow/inc/SCFwTskTyp1.i "FlowTask" "FlowTaskType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_TaskType V-table-Win
ON U2 OF cmb_TaskType IN FRAME F-Main /* Task Type */
DO:
  {workflow/inc/SCFwTskTyp2.i "FlowTask" "FlowTaskType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME FlowTask.EntityCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FlowTask.EntityCode V-table-Win
ON LEAVE OF FlowTask.EntityCode IN FRAME F-Main /* EntityCode */
DO:
  RUN entity-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_AllocatedTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_AllocatedTo V-table-Win
ON U1 OF fil_AllocatedTo IN FRAME F-Main
DO:
  {inc/selfil/sfpsn1.i "FlowTask" "AllocatedTo"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_AllocatedTo V-table-Win
ON U2 OF fil_AllocatedTo IN FRAME F-Main
DO:
  {inc/selfil/sfpsn2.i "FlowTask" "AllocatedTo"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_AllocatedTo V-table-Win
ON U3 OF fil_AllocatedTo IN FRAME F-Main
DO:
  {inc/selfil/sfpsn3.i "FlowTask" "AllocatedTo"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "FlowTask"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "FlowTask"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:  Quit without applying any changes
------------------------------------------------------------------------------*/
  IF mode = "Add" THEN DO TRANSACTION:
    FIND CURRENT FlowTask NO-ERROR.
    IF AVAILABLE(FlowTask) THEN DELETE FlowTask.
  END.

  RUN check-modified( 'clear':U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN verify-flow-task.
  IF RETURN-VALUE <> "" THEN RETURN.

  RUN dispatch( 'update-record':U ).
  RUN notify( 'open-query,record-source':U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-changed V-table-Win 
PROCEDURE entity-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  fil_EntityName:SCREEN-VALUE = get-entity-name( SUBSTRING(INPUT cmb_EntityType, 1, 1), INPUT FlowTask.EntityCode ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-display-fields V-table-Win 
PROCEDURE inst-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN entity-changed.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  parent-key-type = find-parent-keytype().
  IF mode = 'Add' THEN DO:
    have-records = Yes.
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "Maintain" THEN
    RUN dispatch( 'enable-fields':U ).
  ELSE
    RUN dispatch( 'disable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
DEF VAR date-due AS DATE NO-UNDO.
DEF VAR task-no AS INT NO-UNDO INITIAL 0.
DEF VAR results AS CHAR NO-UNDO.
DEF VAR et AS CHAR NO-UNDO.
DEF VAR ec AS INT NO-UNDO.

  date-due = TODAY + 28.
  RUN workflow/update-task.p( INPUT-OUTPUT date-due, INPUT-OUTPUT task-no, "NewTask", 
                            ",,EV", OUTPUT results ).

  FIND FlowTask WHERE FlowTask.DueDate = date-due
                  AND FlowTask.FlowTaskNo = task-no NO-LOCK NO-ERROR.
  IF NOT AVAILABLE( FlowTask ) THEN
    MESSAGE "{&FILE-NAME}: Couldn't create new task?" SKIP results.

  et = find-parent-key( "EntityType" ).
  ec = INT( find-parent-key( "EntityCode" ) ).

  DO TRANSACTION:
    FIND CURRENT FlowTask EXCLUSIVE-LOCK.
    IF et = "Person" THEN
      FlowTask.AllocatedTo = ec.
    ELSE ASSIGN
      FlowTask.EntityType = et
      FlowTask.EntityCode = ec.
    FIND CURRENT FlowTask NO-LOCK.
  END.

  CURRENT-WINDOW:TITLE = "Adding a New Task".
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'update-record':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-assign-statement V-table-Win 
PROCEDURE pre-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR due-date AS DATE NO-UNDO.
DEF VAR task-no AS INT NO-UNDO.
DEF VAR results AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  due-date = FlowTask.DueDate.
  task-no  = FlowTask.FlowTaskNo.
  IF (INPUT FlowTask.DueDate <> due-date) THEN DO:
    FIND CURRENT FlowTask NO-LOCK.
    RUN workflow/update-task.p( INPUT-OUTPUT due-date, INPUT-OUTPUT task-no, "MoveTask", 
                            STRING(INPUT FlowTask.DueDate,"99/99/9999"), OUTPUT results ).
    FIND CURRENT FlowTask EXCLUSIVE-LOCK.
    FlowTask.DueDate = due-date.
    FlowTask.FlowTaskNo = task-no.
    DISPLAY FlowTask.DueDate FlowTask.FlowTaskNo .
  END.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "FlowTask"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-flow-task V-table-Win 
PROCEDURE verify-flow-task :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF TRIM(INPUT FlowTask.Description) = "" THEN DO:
    MESSAGE "You must supply a description for the Task!"
            VIEW-AS ALERT-BOX ERROR TITLE "Incomplete Description".
    APPLY 'ENTRY':U TO FlowTask.Description.
    RETURN "FAIL".
  END.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION find-parent-keytype V-table-Win 
FUNCTION find-parent-keytype RETURNS CHARACTER
  ( /* no parameters */  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR key-type  AS CHAR   NO-UNDO INITIAL ?.
DEF VAR curr-hdl  AS HANDLE NO-UNDO.
DEF VAR c-rec-src AS CHAR   NO-UNDO.
  
  RUN get-link-handle IN adm-broker-hdl
                     ( THIS-PROCEDURE, 'RECORD-SOURCE':U, OUTPUT c-rec-src ).
  ASSIGN curr-hdl = WIDGET-HANDLE( c-rec-src ) NO-ERROR.

  IF VALID-HANDLE( curr-hdl ) THEN DO:
    RUN get-attribute IN curr-hdl( 'Key-Name':U ).
    key-type = RETURN-VALUE.
  END.

  RETURN key-type.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


