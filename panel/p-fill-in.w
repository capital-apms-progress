&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS s-object 
/*------------------------------------------------------------------------

  File: p-fill-in.w

  Description: 
      A SmartPanel that a viewer can send a value into.

      Use:
        RUN set-link-attributes( THIS-PROCEDURE, 'Value-Target':U, <attribute-list> ).
      to set the appropriate attributes after initialisation.

      Attributes accepted are:
        Edge-Pixels             2
        Margin-Pixels           2
        Width                   15
        Value-Attribute         Value-Case
        Type                    CHAR
        Format                  X(20)
        Value-Changed-Event     Open-Query
        Dispatch-Value-Changed  yes
        Label                   &Value
        Link-Name               Value-Source
        Font                    10
        Read-Only               Yes

      If the read-only attribute is set to "No" then the user can type values into
      the fill-in and events may optionally be dispatched when the user leaves the
      field.  This sort of behaviour is better handled by the p-search panel though.
               
  Authors: Andrew McMillan
  Created: August 1998
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEF VAR c_text AS CHAR NO-UNDO.                    /* List of options.         */
DEF VAR c_initial-text AS CHAR NO-UNDO.            /* Initial Text             */
DEF VAR c_delimiter AS CHAR NO-UNDO INITIAL ",":U. /* The delimiter to use.    */

/* ADM Preprocessor Defintions ---                                      */
&Scoped-define adm-attribute-dlg adm/support/optiond.w

DEF VAR key-pressed AS LOGI NO-UNDO INIT No.
DEF VAR text-sent   AS LOGI NO-UNDO INIT Yes.

DEF VAR value-format AS CHAR NO-UNDO.
DEF VAR value-type AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartPanel
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 
&Scoped-Define DISPLAYED-OBJECTS fil_value 

/* Custom List Definitions                                              */
/* Box-Rectangle,Label,List-3,List-4,List-5,List-6                      */
&Scoped-define Box-Rectangle RECT-1 
&Scoped-define Label c-Label 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD format-date-value s-object 
FUNCTION format-date-value RETURNS CHARACTER
  ( INPUT inval AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD format-decimal-value s-object 
FUNCTION format-decimal-value RETURNS CHARACTER
  ( INPUT inval AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD format-integer-value s-object 
FUNCTION format-integer-value RETURNS CHARACTER
  ( INPUT inval AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD format-logical-value s-object 
FUNCTION format-logical-value RETURNS CHARACTER
  ( INPUT inval AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD format-time-value s-object 
FUNCTION format-time-value RETURNS CHARACTER
  ( INPUT inval AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD is-null s-object 
FUNCTION is-null RETURNS LOGICAL
  ( INPUT str AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE c-Label AS CHARACTER FORMAT "X(256)":U INITIAL "&Value" 
      VIEW-AS TEXT 
     SIZE 8 BY .6 NO-UNDO.

DEFINE VARIABLE fil_value AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 14.29 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16 BY 1.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fil_value AT ROW 1.6 COL 2.14 NO-LABEL
     c-Label AT ROW 1 COL 2 NO-LABEL
     RECT-1 AT ROW 1.3 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FGCOLOR 1 .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartPanel
   Allow: Basic
   Frames: 1
   Add Fields to: NEITHER
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW s-object ASSIGN
         HEIGHT             = 2.3
         WIDTH              = 34.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB s-object 
/* ************************* Included-Libraries *********************** */

{src/adm/method/smart.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW s-object
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN c-Label IN FRAME F-Main
   NO-DISPLAY NO-ENABLE ALIGN-L 2                                       */
ASSIGN 
       c-Label:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN fil_value IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR RECTANGLE RECT-1 IN FRAME F-Main
   1                                                                    */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fil_value
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_value s-object
ON ANY-PRINTABLE OF fil_value IN FRAME F-Main
OR "DELETE-CHARACTER" OF {&SELF-NAME}
OR "BACKSPACE" OF {&SELF-NAME}
DO:
  APPLY LASTKEY TO SELF.
  text-sent = No.
  key-pressed    = Yes.
  RETURN NO-APPLY.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_value s-object
ON RETURN OF fil_value IN FRAME F-Main
OR "ENTER" OF {&SELF-NAME}
DO:
  APPLY 'TAB':U TO {&SELF-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK s-object 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN  
    /* Code needed to test this object (when run directly from the UIB) */       
    RUN set-default-attributes.
    RUN dispatch IN THIS-PROCEDURE ('initialize').        
  &ENDIF

  RUN set-default-attributes.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI s-object  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields s-object 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN get-attribute ('Read-Only':U).
  IF RETURN-VALUE BEGINS "Y" THEN
    DISABLE fil_Value.
  ELSE
    ENABLE fil_Value.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize s-object 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  /* Retrieve the list of options (at run-time). */

  RUN get-attribute ('UIB-Mode':U).
  IF RETURN-VALUE eq ? THEN DO:
    RUN retrieve-text.
  END.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .
  RUN get-attribute ('Hide-On-Init':U).
  IF RETURN-VALUE NE "YES":U THEN VIEW FRAME {&FRAME-NAME}.

  /* Code placed here will execute AFTER standard behavior.    
     These attributes don't effect the visualization of the object, so we
     process them last. */

END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-UIB-mode s-object 
PROCEDURE local-UIB-mode :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method. If the object has just been drawn
               in the UIB, then make sure it is sized correctly.
               
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'UIB-mode':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN get-attribute ('Drawn-in-UIB':U).
  IF RETURN-VALUE eq ? THEN DO:
       
    /* Mark this as having been drawn. */
    RUN set-attribute-list ('Drawn-in-UIB=yes':U).
    
    RUN set-default-attributes.
    
    /* Allow the developer to edit these initial values at Design time
       (i.e. when UIB-Mode ne "Preview".) */
    RUN get-attribute ('UIB-Mode':U).
    IF RETURN-VALUE eq 'Design':U THEN RUN dispatch ('edit-attribute-list':U).
   
    /* Use the new values for these attributes. */
    RUN use-label.
    
    /* Make sure the object has been sized correctly. (This will process
       edge-pixels and margin-pixels.) */
    RUN set-size IN THIS-PROCEDURE 
        (FRAME {&FRAME-NAME}:HEIGHT, FRAME {&FRAME-NAME}:WIDTH).
 
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE retrieve-text s-object 
PROCEDURE retrieve-text :
/*------------------------------------------------------------------------------
  Purpose:     This procedure asks the relevant linked object to return the
               current text string.
  Parameters:  <none>
  Notes:       Their can only be a single linked object.
------------------------------------------------------------------------------*/

  DEF VAR c_list       AS CHAR NO-UNDO.
  DEF VAR value-attr   AS CHAR NO-UNDO.
  DEF VAR h_target     AS HANDLE NO-UNDO.
  DEF VAR link-name    AS CHAR NO-UNDO.
   
  /* STEP 1 - Get the name of the link, and the attribute to set.
   *          Check for errors. */

  RUN get-attribute ('Link-Name':U).          link-name    = RETURN-VALUE.
  RUN get-attribute ('Value-Attribute':U).    value-attr   = RETURN-VALUE.
  
  IF is-null( link-name ) OR is-null( value-attr ) THEN
  
    MESSAGE THIS-PROCEDURE:FILE-NAME SKIP
      "This SmartObject was not correctly initialized."
      IF is-null( link-name)   THEN CHR(10) + " - Link-Name not set"  ELSE ""
      IF is-null( value-attr ) THEN CHR(10) + " - Value-Attribute not set" ELSE ""
       VIEW-AS ALERT-BOX ERROR.

  ELSE
  DO:

    /* STEP 2 - get the linked object.
     *          Check for errors. */
     
    RUN get-link-handle IN adm-broker-hdl (THIS-PROCEDURE, link-name, OUTPUT c_list).

    IF NUM-ENTRIES( c_list ) > 1 THEN 
      MESSAGE "Multiple" link-name + "s exist for this SmartObject." 
              "Therefore the list of options cannot be retrieved."
              VIEW-AS ALERT-BOX WARNING.
    ELSE IF NUM-ENTRIES( c_list ) = 0 THEN RETURN.
    ELSE
    DO WITH FRAME {&FRAME-NAME}:
    
      /* STEP 3 - Get the option attribute and case attribute in the linked object.
       *          Check for errors. */
      h_target = WIDGET-HANDLE (c_list).
      IF VALID-HANDLE (h_target) THEN DO:

          /* Get the initial value of the 'Value-Attribute'.
           * Assign the list of options, and this initial condition.  
           */
          RUN get-attribute IN h_target (value-attr).
          c_initial-text = RETURN-VALUE.
          fil_value:SCREEN-VALUE = c_initial-text.
          
      END. /* IF VALID...h_target... */
    END. /* ELSE DO...STEP 3 ... */
  END. /* ELSE DO...STEP 2 ... */    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-case-across-link s-object 
PROCEDURE send-case-across-link :
/*------------------------------------------------------------------------------
  Purpose:     Sends the curren text to the linked target, and optionally 
               dispatches and event (such as 'open-query') to it.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR c_value        AS CHAR NO-UNDO.
  DEF VAR c_list        AS CHAR NO-UNDO.
  DEF VAR value-attr    AS CHAR NO-UNDO.
  DEF VAR h_target  AS HANDLE NO-UNDO.
  DEF VAR link-name     AS CHAR NO-UNDO.
  
  /* STEP 1 - get the name of the link, and the attribute to set.
   *          Check for errors. */
  RUN get-attribute ('Link-Name':U).        link-name  = RETURN-VALUE.
  RUN get-attribute ('Value-Attribute':U).  value-attr = RETURN-VALUE.
  
 /* Verify that everything is valid. [Errors would have been reported in
     the retrieve-options-list procedure.] */
     
  IF NOT ( is-null( link-name ) OR is-null( value-attr ) ) THEN
  DO:
  
    /* STEP 2 - Get the list of linked objects.
     *          Check for errors. */
    RUN get-link-handle IN adm-broker-hdl (THIS-PROCEDURE, link-name, OUTPUT c_list).

    IF NUM-ENTRIES( c_list ) = 0 THEN 
      MESSAGE "No" link-name "exists for this object." VIEW-AS ALERT-BOX WARNING.

    ELSE IF NUM-ENTRIES( c_list ) > 1 THEN
      MESSAGE "Multiple" link-name + "s exist for this object." SKIP(1)
              "Command cancelled." 
              VIEW-AS ALERT-BOX WARNING.
    ELSE
    DO WITH FRAME {&FRAME-NAME}:
    
      /* STEP 3 - Set the value attribute in the linked object. */

      ASSIGN h_target = WIDGET-HANDLE (c_list).
    
      IF VALID-HANDLE (h_target) THEN
      DO:
        RUN set-attribute-list IN h_target (value-attr + '=':U + INPUT fil_value ).
        RUN get-attribute IN THIS-PROCEDURE ('Value-Changed-Event':U).
        IF NUM-ENTRIES(RETURN-VALUE) > 0 THEN RUN dispatch IN h_target (RETURN-VALUE).
        /* RUN dispatch IN h_target ( 'apply-entry':U ). */

      END. /* IF VALID...h_target... */
    END. /* ELSE DO...STEP 3 ... */
  END. /* ELSE DO...STEP 2 ... */    

  text-sent = Yes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-default-attributes s-object 
PROCEDURE set-default-attributes :
/*------------------------------------------------------------------------------
  Purpose:     Set the default attributes for this amrt panel
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Set default values for Parameters */
  RUN get-attribute ('Edge-Pixels':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Edge-Pixels = 2':U).
  RUN get-attribute ('Margin-Pixels':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Margin-Pixels = 2':U).

  RUN get-attribute ('Width':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Width = 15':U).
  RUN get-attribute ('Width':U).
  RUN set-size( 1, INT(RETURN-VALUE) ).

  RUN get-attribute ('Value-Attribute':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Value-Attribute = Value-Case':U).
  RUN get-attribute ('Type':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Type = CHAR':U).
  RUN get-attribute ('Format':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Format = X(20)':U).
  RUN get-attribute ('Value-Changed-Event':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Value-Changed-Event = Open-Query':U).
  RUN get-attribute ('Dispatch-Open-Query':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Dispatch-Value-Changed = yes':U).
  RUN get-attribute ('Label':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Label = &Value':U).
  RUN get-attribute ('Link-Name':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Link-Name = Value-Source':U).
  RUN get-attribute ('Font':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Font = 10':U).
  RUN get-attribute ('Read-Only':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Read-Only = Yes':U).

  RUN get-attribute ('Value':U).
  IF is-null( RETURN-VALUE ) THEN RUN set-attribute-list ('Value = ':U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-size s-object 
PROCEDURE set-size :
/*------------------------------------------------------------------------------
  Purpose: Changes the size and shape of the panel.  This routine
           spaces the buttons to fill the available space.  
  Parameters: 
           pd-height - the desired height (in rows)
           pd-width  - the desired width (in columns)
  Notes:       
           If pd-width or pd-height are ? then use the current values.
           (i.e. RUN set-size (?,?) resets the current size).
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER pd-height AS DECIMAL NO-UNDO.
  DEFINE INPUT PARAMETER pd-width  AS DECIMAL NO-UNDO.

  IF pd-height eq ? THEN pd-height = FRAME {&FRAME-NAME}:HEIGHT.
  IF pd-width  eq ? THEN pd-width  = FRAME {&FRAME-NAME}:WIDTH.

  DO WITH FRAME {&FRAME-NAME}:
    FRAME {&FRAME-NAME}:SCROLLABLE = No.
    ASSIGN FRAME {&FRAME-NAME}:WIDTH-C  = pd-width NO-ERROR.
    RUN get-attribute( 'Edge-Pixels':U ).
    ASSIGN  pd-width = (FRAME {&FRAME-NAME}:WIDTH-P) - (INT(RETURN-VALUE) * 2)
            RECT-1:WIDTH-P = pd-width
            RECT-1:X = (FRAME {&FRAME-NAME}:X) + INT(RETURN-VALUE).

    RUN get-attribute( 'Margin-Pixels':U ).
    ASSIGN  pd-width = pd-width - (INT(RETURN-VALUE) * 2)
            fil_Value:WIDTH-P = pd-width.
            fil_Value:X = RECT-1:X + INT(RETURN-VALUE) .

    FRAME {&FRAME-NAME}:SCROLLABLE = Yes.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed s-object 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-font s-object 
PROCEDURE use-font :
/*------------------------------------------------------------------------------
  Purpose:  Get the current value of 'font' and set the font of the FRAME to this value.   
  Parameters:  <none>
  Notes:    
------------------------------------------------------------------------------*/
DEFINE INPUT PARAMETER p_attr-value AS CHAR NO-UNDO.

DEFINE VAR iFont AS INTEGER NO-UNDO INITIAL ?.
DEFINE VAR h AS WIDGET NO-UNDO.
  
  /* Convert attribute string to an INTEGER font value. */
  iFont = IF p_attr-value eq "?" THEN ? ELSE INTEGER(p_attr-value) NO-ERROR.
  
  /* Is the correct font set already? */
  IF iFont ne FRAME {&FRAME-NAME}:FONT THEN FRAME {&FRAME-NAME}:FONT = iFont.
  /* If there is a LABEL, then resize the frame. */
  IF {&Label}:SCREEN-VALUE ne '':U THEN RUN set-size IN THIS-PROCEDURE (?,?).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Format s-object 
PROCEDURE use-Format :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-format AS CHAR NO-UNDO.
  value-format = REPLACE(new-format, "|", ",").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-label s-object 
PROCEDURE use-label :
/*------------------------------------------------------------------------------
  Purpose:     Get the current value of 'Label' and set the Label object to this
               value.   
  Parameters:  <none>
  Notes:    
------------------------------------------------------------------------------*/  
DEFINE INPUT PARAMETER p_attr-value AS CHAR NO-UNDO.

DEFINE VAR h AS WIDGET NO-UNDO.
  
  {&Label} = IF p_attr-value eq "?" THEN '':U ELSE p_attr-value NO-ERROR.

  /* Reset the Label and its width. (If this FAILS, then we may need to run
     the whole resize logic again. */
  DO WITH FRAME {&FRAME-NAME}:
    ASSIGN {&Label}:SCREEN-VALUE = {&Label}
           {&Label}:HIDDEN = {&Label}:SCREEN-VALUE eq '':U
           {&Label}:WIDTH-P = FONT-TABLE:GET-TEXT-WIDTH-P
                                ({&Label}, FRAME {&FRAME-NAME}:FONT)
       NO-ERROR.
    IF ERROR-STATUS:ERROR THEN RUN set-size IN THIS-PROCEDURE (?,?).
  END. /* DO WITH FRAME... */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-link-name s-object 
PROCEDURE use-link-name :
/*------------------------------------------------------------------------------
  Purpose:  Get the current value of 'Link-Name' and set the supported-links to
            be the inverse of this link.
  Parameters:  <none>
  Notes:    
------------------------------------------------------------------------------*/
DEFINE INPUT PARAMETER p_attr-value AS CHAR NO-UNDO.

DEF VAR ch          AS CHAR NO-UNDO.
DEF VAR i           AS INTEGER NO-UNDO.
DEF VAR new-link    AS CHAR NO-UNDO.

  new-link = IF p_attr-value eq "?" THEN ? ELSE p_attr-value NO-ERROR.

  ASSIGN i = NUM-ENTRIES (new-link,"-":U)
         ch = IF i < 2 THEN '':U ELSE ENTRY(i,new-link, "-":U)
         .
  IF ch eq 'Source':U THEN ENTRY(i,new-link,"-":U) = 'Target':U.
  ELSE IF ch eq 'Target' THEN ENTRY(i,new-link,"-":U) = 'Source':U.
  ELSE DO:
    new-link = IF LENGTH(new-link) > 0 /* Could be empty or ? */
                          THEN new-link + '-Source':U
                          ELSE 'Option-Source':U.
                     
    /* Whoops! The Link-Name seems invalid...set it to something reasonable. */
    MESSAGE 'The Link-Name of the SmartObject does not indicate direction.'
            '(It does not specify "Target" or "-Source".)' SKIP(1)
            'The ADM Supported Links will be set to' new-link + '.':U
            VIEW-AS ALERT-BOX WARNING.
  END.

  RUN set-attribute-list ( 'SUPPORTED-LINKS = ':U + new-link ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Read-Only s-object 
PROCEDURE use-Read-Only :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-setting AS CHAR NO-UNDO.

  fil_value:SENSITIVE IN FRAME {&FRAME-NAME} = NOT (new-setting BEGINS "Y").
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-tick-interval s-object 
PROCEDURE use-tick-interval :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  DEF INPUT PARAMETER millisec AS CHAR NO-UNDO.

  DEF VAR ch-timer AS COM-HANDLE NO-UNDO.
  DEF VAR interval AS INT NO-UNDO.
  
  interval = INT( millisec ).

  /* The following code for some reason does not work under 8.2!!! */
  /*
  ch-timer = Timer:COM-HANDLE.
  ch-timer:Interval = interval.
  */
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Type s-object 
PROCEDURE use-Type :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-type AS CHAR NO-UNDO.

  value-type = SUBSTRING( new-type, 1, 3).
  RUN get-attribute( "Format":U ).
  IF is-null(RETURN-VALUE) THEN DO:
    CASE value-type:
      WHEN "DEC" THEN RUN set-attribute-list ('Format = >>|>>>|>>9.99CR':U).
      WHEN "INT" THEN RUN set-attribute-list ('Format = ->>>|>>>|>>9':U).
      WHEN "LOG" THEN RUN set-attribute-list ('Format = Yes/No':U).
      WHEN "DAT" THEN RUN set-attribute-list ('Format = 99/99/9999':U).
      WHEN "TIM" THEN RUN set-attribute-list ('Format = HH:MM:SS':U).
      OTHERWISE
        RUN set-attribute-list ('Format = X(20)':U).
    END CASE.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Value s-object 
PROCEDURE use-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-value AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

  RUN get-attribute( 'Type':U ).
  CASE SUBSTRING(RETURN-VALUE, 1, 3):
    WHEN "DEC" THEN fil_value   = format-decimal-value( new-value ).
    WHEN "INT" THEN fil_value   = format-integer-value( new-value ).
    WHEN "LOG" THEN fil_value   = format-logical-value( new-value ).
    WHEN "DAT" THEN fil_value   = format-date-value( new-value ).
    WHEN "TIM" THEN fil_value   = format-time-value( new-value ).
    OTHERWISE
      fil_value   = new-value.
  END CASE.

  DISPLAY fil_value.
/*  fil_value:SCREEN-VALUE = fil-value . */

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION format-date-value s-object 
FUNCTION format-date-value RETURNS CHARACTER
  ( INPUT inval AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR outval AS DATE NO-UNDO.

  ASSIGN outval = DATE(inval) NO-ERROR.
  IF outval = ? THEN inval = "".
  ELSE
    inval = STRING( outval, value-format).

  RETURN inval.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION format-decimal-value s-object 
FUNCTION format-decimal-value RETURNS CHARACTER
  ( INPUT inval AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR outval AS DEC NO-UNDO.

  ASSIGN outval = DEC(inval) NO-ERROR.
  IF outval = ? THEN outval = 0.0 .
  inval = STRING( outval, value-format).

  RETURN inval.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION format-integer-value s-object 
FUNCTION format-integer-value RETURNS CHARACTER
  ( INPUT inval AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR outval AS INT NO-UNDO.

  ASSIGN outval = INT(inval) NO-ERROR.
  IF outval = ? THEN outval = 0 .
  inval = STRING( outval, value-format).

  RETURN inval.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION format-logical-value s-object 
FUNCTION format-logical-value RETURNS CHARACTER
  ( INPUT inval AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR outval AS LOGICAL NO-UNDO.

  ASSIGN outval = (inval BEGINS "Y") NO-ERROR.
  IF outval = ? THEN inval = "?".
  ELSE
    inval = STRING( outval, value-format).

  RETURN inval.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION format-time-value s-object 
FUNCTION format-time-value RETURNS CHARACTER
  ( INPUT inval AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR outval AS INT NO-UNDO.

  ASSIGN outval = INT(inval) NO-ERROR.
  IF outval = ? THEN inval = "".
  ELSE
    inval = STRING( outval, value-format).

  RETURN inval.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION is-null s-object 
FUNCTION is-null RETURNS LOGICAL
  ( INPUT str AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN (str = "" OR str = ?).
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

