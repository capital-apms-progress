&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  AmTrust Rental Forecasting Run
  ------------------------------------------------------------------------*/
&SCOPED-DEFINE REPORT-ID "Amtrust Rental Run"

DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

DEF VAR all-ratchets AS LOGI NO-UNDO INIT No.
DEF VAR clear-all AS LOGI NO-UNDO    INIT No.
DEF VAR preview   AS LOGI NO-UNDO    INIT No.
DEF VAR audit-report AS LOGI NO-UNDO INIT No.
DEF VAR rent-report AS LOGI NO-UNDO  INIT No.
DEF VAR skip-agents-fees AS LOGI NO-UNDO  INIT No.
DEF VAR regenerate AS LOGI NO-UNDO   INIT Yes.
DEF VAR regenerate-rents AS LOGI NO-UNDO INIT No.
DEF VAR regenerate-expenses AS LOGI NO-UNDO INIT No.
DEF VAR regenerate-recoveries AS LOGI NO-UNDO INIT No.
DEF VAR property-1 AS INT NO-UNDO    INIT ?.
DEF VAR property-n AS INT NO-UNDO    INIT ?.
DEF VAR month-1 AS INT NO-UNDO       INIT ?.
DEF VAR month-n AS INT NO-UNDO       INIT ?.
DEF VAR forecast-start AS DATE NO-UNDO.
DEF VAR forecast-end AS DATE NO-UNDO.
RUN parse-parameters.
IF ERROR-STATUS:ERROR THEN RETURN.

DEF VAR time-pos AS INT NO-UNDO INITIAL 1.
DEF VAR timings AS INT NO-UNDO EXTENT 10 INITIAL 0 .
ETIME(Yes).     /* start the timer */

DEF VAR in-rent-report AS LOGI INITIAL No NO-UNDO.
DEF VAR round-up-after AS INT NO-UNDO INIT 1.
DEF VAR one-twelfth AS DEC NO-UNDO.
one-twelfth = 1.0 / 12.0 .

DEF VAR rr-sequences AS CHAR NO-UNDO INITIAL "BASE,GBAS,NEW,GNEW,VACT,GVAC,ONE,AGNT".

DEF VAR mfmt AS CHAR NO-UNDO INITIAL "->>>,>>>,>>9.99".
DEF VAR pfmt AS CHAR NO-UNDO INITIAL "->>9.99999".
DEF VAR i AS INT NO-UNDO.

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
DEF VAR timeStamp AS CHAR FORMAT "X(44)" NO-UNDO.
timeStamp = STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.

DEF VAR base-font AS CHAR NO-UNDO   INITIAL "fixed,courier,cpi,24,lpi,10,normal".
DEF VAR header-font AS CHAR NO-UNDO   INITIAL "proportional,helvetica,point,10,lpi,7,bold".

{inc/ofc-this.i}
{inc/ofc-set.i "Default-Forecast-Accounts" "forecast-accounts" "ERROR"}
DEF VAR floor-ac AS DEC NO-UNDO.
DEF VAR other-ac AS DEC NO-UNDO.
DEF VAR parks-ac AS DEC NO-UNDO.
floor-ac = DEC(ENTRY(1,forecast-accounts)).
parks-ac = DEC(ENTRY(1,forecast-accounts)).
other-ac = DEC(ENTRY(1,forecast-accounts)).

{inc/ofc-set.i "Default-Forecast-Property" "default-forecast-property"}
DEF VAR default-property AS INT NO-UNDO.
ASSIGN default-property = INT( default-forecast-property ) NO-ERROR.
{inc/ofc-set.i "Default-Forecast-Fixed-OG-Account" "default-fixed-og"}
DEF VAR fixed-og-account AS DEC NO-UNDO.
ASSIGN fixed-og-account = DEC( default-fixed-og ) NO-ERROR.

{inc/ofc-set.i "Default-Forecast-Recovery-Offset" "default-forecast-rec-offset"}
DEF VAR recovery-offset AS DEC NO-UNDO.
ASSIGN recovery-offset = DEC( default-forecast-rec-offset ) NO-ERROR.

{inc/ofc-set.i "Default-Forecast-Agents-Fees-Account" "default-agents-fees"}
DEF VAR agents-fees-account AS DEC NO-UNDO.
ASSIGN agents-fees-account = DEC( default-agents-fees ) NO-ERROR.

{inc/ofc-set.i "Default-Forecast-Agents-Fees-Rate" "default-agents-rate"}
DEF VAR agents-fees-rate AS DEC NO-UNDO.
ASSIGN agents-fees-rate = DEC( default-agents-rate ) NO-ERROR.
IF agents-fees-rate > 1 THEN agents-fees-rate = agents-fees-rate / 100 .

DEFINE TEMP-TABLE Action NO-UNDO
            FIELD ActionDate AS DATE
            FIELD MonthCode AS INT
            FIELD TenancyLeaseCode AS INT
            FIELD RentalSpaceCode AS INT
            FIELD ActionType AS CHAR
            FIELD ActionDescription AS CHAR
            FIELD ActionFloor AS DEC
            FIELD ActionParks AS DEC
            FIELD ActionOther AS DEC
            FIELD ActionFCFloor AS DEC
            FIELD ActionFCParks AS DEC
            FIELD ActionFCOther AS DEC
            INDEX XPKAction IS PRIMARY MonthCode ActionType
            INDEX XAK1Action ActionType MonthCode
            INDEX XAK2Action TenancyLeaseCode RentalSpaceCode ActionType MonthCode
            INDEX XAK3Action RentalSpaceCode TenancyLeaseCode ActionType MonthCode .

DEF WORK-TABLE Lease NO-UNDO LIKE TenancyLease.
CREATE Lease.
DEF WORK-TABLE Area NO-UNDO LIKE RentalSpace.
CREATE Area.

DEF TEMP-TABLE growth-month NO-UNDO LIKE Month
        FIELD floor AS DEC
        FIELD parks AS DEC
        FIELD other AS DEC.

DEF TEMP-TABLE rr-line NO-UNDO
        FIELD pcode AS INT
        FIELD et AS CHAR
        FIELD ec AS INT
        FIELD lseq AS INT
        FIELD ltype AS CHAR
        FIELD years AS DEC EXTENT 5
        INDEX XPKrr-lines IS UNIQUE PRIMARY pcode et ec lseq
        INDEX XAK1rr-lines IS UNIQUE pcode et ec ltype.

DEF WORK-TABLE rates NO-UNDO
    FIELD b-floor AS DEC FIELD b-parks AS DEC FIELD b-other AS DEC
    FIELD g-floor AS DEC FIELD g-parks AS DEC FIELD g-other AS DEC
    FIELD m-floor AS DEC FIELD m-parks AS DEC FIELD m-other AS DEC
    FIELD lg-floor AS DEC FIELD lg-parks AS DEC FIELD lg-other AS DEC
    FIELD pro-rate AS DEC
    FIELD last-action AS CHAR
    FIELD et AS CHAR FIELD ec AS INT.
CREATE rates.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-from-annual) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD from-annual Procedure 
FUNCTION from-annual RETURNS DECIMAL
  ( INPUT freq-code AS CHAR, INPUT annual-amount AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-area-rental) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-area-rental Procedure 
FUNCTION get-area-rental RETURNS DECIMAL
  ( /* no parameters */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-int-parameter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-int-parameter Procedure 
FUNCTION get-int-parameter RETURNS INTEGER
  ( INPUT param-id AS CHAR, INPUT in-date AS DATE, INPUT default-no AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-lease-recovery) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-lease-recovery Procedure 
FUNCTION get-lease-recovery RETURNS DECIMAL
  ( INPUT ac AS DEC, INPUT lease-ended AS LOGICAL  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-parameter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-parameter Procedure 
FUNCTION get-parameter RETURNS CHARACTER
  ( INPUT param-id AS CHAR, INPUT in-date AS DATE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-part-of-period) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD part-of-period Procedure 
FUNCTION part-of-period RETURNS DECIMAL
  ( INPUT part-1 AS DATE, INPUT part-n AS DATE )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-end-after) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD process-action-end-after Procedure 
FUNCTION process-action-end-after RETURNS CHARACTER
  ( /* no parameters */  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-end-before) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD process-action-end-before Procedure 
FUNCTION process-action-end-before RETURNS CHARACTER
  ( /* no parameters */  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-new) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD process-action-new Procedure 
FUNCTION process-action-new RETURNS CHARACTER
  ( /* no parameters */  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-old) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD process-action-old Procedure 
FUNCTION process-action-old RETURNS CHARACTER
  ( /* no parameters */  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-one) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD process-action-one Procedure 
FUNCTION process-action-one RETURNS CHARACTER
  ( /* no parameters */  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-rvw) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD process-action-rvw Procedure 
FUNCTION process-action-rvw RETURNS CHARACTER
  ( /* no parameters */  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-to-annual) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD to-annual Procedure 
FUNCTION to-annual RETURNS DECIMAL
  ( INPUT freq-code AS CHAR, INPUT period-amount AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-to-month-rate) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD to-month-rate Procedure 
FUNCTION to-month-rate RETURNS DECIMAL
  ( INPUT raw-percent AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 42
         WIDTH              = 44.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/null.i}
{inc/date.i}
{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

RUN pclrep-start( preview, "reset,landscape,tm,3,a4,lm,2," + base-font).
OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

RUN reset-growth-rates.
IF regenerate THEN DO:
  RUN clear-forecast-data.
  RUN stamp-time.
  RUN build-forecast-data.
  RUN stamp-time.
END.

IF rent-report THEN DO:
  RUN rr-rent-report.
  RUN stamp-time.
END.

IF audit-report THEN DO:
  DO i = 1 TO (time-pos - 1):
    RUN pclrep-line( base-font, "Timing point #" + STRING(i) + " - " + STRING( DEC(timings[i]) / 1000.0, ">>,>>9.999") + "mS").
  END.
END.

OUTPUT CLOSE.

RUN pclrep-finish.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-audit-down-by) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE audit-down-by Procedure 
PROCEDURE audit-down-by :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER lines AS DEC NO-UNDO.
  IF audit-report THEN RUN pclrep-down-by( lines ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-audit-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE audit-line Procedure 
PROCEDURE audit-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER in-font AS CHAR NO-UNDO.
DEF INPUT PARAMETER line AS CHAR NO-UNDO.

  IF audit-report THEN
    RUN pclrep-line( in-font, line ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-build-forecast-data) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-forecast-data Procedure 
PROCEDURE build-forecast-data :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FOR EACH Property WHERE Active AND Property.PropertyCode >= property-1
                      AND Property.PropertyCode <= property-n NO-LOCK:
    RUN audit-line( header-font, "Property " + STRING(Property.PropertyCode) ).
    RUN reset-growth-rates.
    IF regenerate-rents THEN DO:
      IF Property.ExternallyManaged THEN DO:
        /* Deal with it as if it was vacant */
        FOR EACH RentalSpace OF Property NO-LOCK:
          RUN each-vacant-space.
          RUN audit-down-by( 0.2 ).
        END.
      END.
      ELSE DO:
        FOR EACH Tenant WHERE Tenant.EntityType = "P" AND Tenant.EntityCode = Property.PropertyCode
                        AND Tenant.Active NO-LOCK:
          RUN audit-down-by( 0.3 ).
          RUN each-tenant.
        END.
        FOR EACH RentalSpace OF Property WHERE RentalSpace.AreaStatus = "V" NO-LOCK:
          RUN each-vacant-space.
          RUN audit-down-by( 0.2 ).
        END.
      END.
    END.
    IF regenerate-expenses THEN DO:
      FOR EACH CashFlow WHERE CashFlow.ScenarioCode = 1
                          AND CashFlow.EntityType = "P"
                          AND CashFlow.EntityCode = Property.PropertyCode NO-LOCK:
        RUN each-cash-flow.
        RUN audit-down-by( 0.2 ).
      END.
    END.
    IF regenerate-recoveries THEN DO:
      FOR EACH Tenant WHERE Tenant.EntityType = "P" AND Tenant.EntityCode = Property.PropertyCode
                      AND Tenant.Active NO-LOCK:
        RUN audit-down-by( 0.3 ).
        RUN each-tenant-recoveries.
      END.
      FOR EACH RentalSpace OF Property WHERE RentalSpace.AreaStatus = "V" NO-LOCK:
        RUN each-vacant-space-recoveries.
        RUN audit-down-by( 0.2 ).
      END.
    END.
    RUN audit-down-by( 0.7 ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-build-lease-actions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-lease-actions Procedure 
PROCEDURE build-lease-actions :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR vacant-months AS INT NO-UNDO.
DEF VAR review-months AS INT NO-UNDO.
DEF VAR action-date AS DATE NO-UNDO.

  FOR EACH Action:      DELETE Action.          END.

  IF NOT AVAILABLE(TenancyLease) THEN RETURN.

  IF NOT( Property.ExternallyManaged ) THEN DO:
    /* Only do manual overrides for externally managed properties */
    FOR EACH RentReview OF TenancyLease WHERE RentReview.DateDue >= forecast-start
                                          AND RentReview.DateDue < forecast-end NO-LOCK:
      RUN make-action( RentReview.DateDue, "Rvw", "RentReview").
    END.

    IF Lease.LeaseEndDate <> ? AND Lease.LeaseEndDate < forecast-end THEN DO:
      action-date = Lease.LeaseEndDate.
      IF action-date < forecast-start THEN action-date = forecast-start - 1.
      RUN make-action( action-date, "End", "Lease end").
    END.
  END.

  FOR EACH PropForecastParam OF Property WHERE PropForecastParam.MonthCode >= month-1
                    AND PropForecastParam.MonthCode <= month-n
                    AND PropForecastParam.ParameterID = "Override" NO-LOCK:
    IF NUM-ENTRIES(PropForecastParam.ParameterValue) < 9 THEN DO:
      MESSAGE PropForecastParam.ParameterValue " - insufficient parameters for override action (RecordType,Code,Date,Old-Type,New-Type,Floor,Parks,Other,Description) are all needed.".
      NEXT.
    END.
    IF ENTRY(1,PropForecastParam.ParameterValue) = "T"
            AND INT( ENTRY(2,PropForecastParam.ParameterValue) ) = TenancyLease.TenantCode THEN
      RUN override-action( PropForecastParam.ParameterValue ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-build-vacant-actions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-vacant-actions Procedure 
PROCEDURE build-vacant-actions :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR vacant-months AS INT NO-UNDO.
DEF VAR review-months AS INT NO-UNDO.
DEF VAR action-date AS DATE NO-UNDO.

  FOR EACH Action:      DELETE Action.          END.

  FIND AreaType OF RentalSpace NO-LOCK.
/*  IF AreaType.IsCarPark THEN RETURN. */
  IF NOT( Property.ExternallyManaged) AND NOT( AreaType.IsCarPark) THEN DO:
    action-date = forecast-start.
    vacant-months = get-int-parameter("Vacant-Months", action-date, 1).
    action-date = add-months( action-date, vacant-months ).
    RUN make-action(  action-date, "New", "Re leasing").
    review-months = get-int-parameter("Review-Months", action-date, 12).
    DO WHILE action-date < forecast-end:
      action-date = add-months( action-date, review-months ).
      IF action-date < forecast-end THEN
        RUN make-action( action-date, "Rvw", "Review on new lease").
    END.
  END.

  FOR EACH PropForecastParam OF Property WHERE PropForecastParam.MonthCode >= month-1
                    AND PropForecastParam.MonthCode <= month-n
                    AND PropForecastParam.ParameterID = "Override" NO-LOCK:
    IF NUM-ENTRIES(PropForecastParam.ParameterValue) < 9 THEN DO:
      MESSAGE PropForecastParam.ParameterValue " - insufficient parameters for override action (RecordType,Code,Date,Old-Action,New-Action,Floor,Parks,Other,Description) are all needed.".
      NEXT.
    END.
    IF ENTRY(1,PropForecastParam.ParameterValue) = "R"
            AND INT( ENTRY(2,PropForecastParam.ParameterValue) ) = RentalSpace.RentalSpaceCode THEN
      RUN override-action( PropForecastParam.ParameterValue ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-clear-forecast-data) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-forecast-data Procedure 
PROCEDURE clear-forecast-data :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF clear-all THEN DO:
    FOR EACH PropForecast EXCLUSIVE-LOCK:
      DELETE PropForecast.
    END.
  END.
  ELSE DO:
DEF VAR regenerate-types AS CHAR NO-UNDO INITIAL "".
    IF regenerate-rents THEN
      regenerate-types = "BASE,GBAS,NEW,GNEW,ONE,VACT,GVAC,AGNT".
    ELSE IF regenerate-expenses THEN
      regenerate-types = "EXP,NREC".
    ELSE IF regenerate-recoveries THEN
      regenerate-types = "RCBS,RCNW".

    FOR EACH PropForecast WHERE PropForecast.PropertyCode >= property-1
                            AND  PropForecast.PropertyCode <= property-n EXCLUSIVE-LOCK:
      IF (regenerate-rents AND regenerate-expenses AND regenerate-recoveries)
            OR LOOKUP( PropForecast.PropForecastType, regenerate-types ) > 0 THEN
        DELETE PropForecast.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-dump-action) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE dump-action Procedure 
PROCEDURE dump-action :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR line AS CHAR NO-UNDO.

  IF NOT audit-report THEN RETURN.
  line = "Action: " + STRING(Action.ActionDate) + " "
       + STRING( Action.MonthCode, ">>>9")
       + (IF Action.TenancyLeaseCode = ? THEN "" ELSE "  Lse" + STRING(Action.TenancyLeaseCode,"99999") + "  ")
       + (IF Action.RentalSpaceCode = ? THEN "" ELSE "  Rsp" + STRING(Action.RentalSpaceCode ,"99999") + "  ")
       + STRING( Action.ActionType, "X(6)")
       + STRING( Action.ActionDescription, "X(15)")
       + STRING( Action.ActionFloor + Action.ActionParks + Action.ActionOther, mfmt)
       + STRING( Action.ActionFCFloor + Action.ActionFCParks + Action.ActionFCOther, mfmt).

  RUN audit-line( base-font, line ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-dump-prop-forecast) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE dump-prop-forecast Procedure 
PROCEDURE dump-prop-forecast :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR line AS CHAR NO-UNDO.

  IF NOT audit-report THEN RETURN.
  line = STRING( PropForecast.PropertyCode, "99999") + " "
       + PropForecast.EntityType 
       + STRING( PropForecast.EntityCode, "99999") + "/"
       + STRING( PropForecast.AccountCode, "9999.99") + " "
       + STRING( PropForecast.MonthCode, ">>>>9") + " "
       + STRING( PropForecast.PropForecastType, "X(5)")
       + STRING( PropForecast.Amount, mfmt).
  RUN audit-line( base-font, line ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-dump-rates) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE dump-rates Procedure 
PROCEDURE dump-rates :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER p-type AS CHAR NO-UNDO.

DEF VAR line AS CHAR NO-UNDO.

  IF NOT audit-report THEN RETURN.
  line = "  " + STRING(p-type, "X(5)" )
       + STRING(growth-month.StartDate)
       + STRING(growth-month.MonthCode, ">>>9")
       + " " + STRING( growth-month.floor, pfmt)
       + " " + STRING( growth-month.parks, pfmt)
       + " " + STRING( growth-month.other, pfmt)
       + "  " + rates.et
       + STRING( rates.ec, "99999") + " - "
       + STRING( rates.b-floor + rates.b-parks + rates.b-other, mfmt )
       + STRING( rates.g-floor + rates.g-parks + rates.g-other, mfmt )
       + STRING( rates.m-floor + rates.m-parks + rates.m-other, mfmt )
       + STRING( rates.pro-rate, pfmt)
       + " " + rates.last-action .
  RUN audit-line( base-font, line ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-cash-flow) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-cash-flow Procedure 
PROCEDURE each-cash-flow :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR part AS DEC NO-UNDO.
DEF VAR amount AS DEC NO-UNDO.
DEF VAR line AS CHAR NO-UNDO.

  RUN audit-line( header-font, "CashFlow " + CashFlow.EntityType + STRING(CashFlow.EntityCode,"99999") + "/" + STRING(CashFlow.AccountCode,"9999.99") + " " + STRING(CashFlow.StartDate,"99/99/9999") + " " + CashFlow.Description ).

  FOR EACH growth-month WHERE growth-month.StartDate >= CashFlow.StartDate
                        AND (growth-month.EndDate <= CashFlow.EndDate
                            OR CashFlow.EndDate = ?):
    part = part-of-period( CashFlow.StartDate, CashFlow.EndDate).
    amount = from-annual( CashFlow.FrequencyCode, CashFlow.Amount) * part.

    IF audit-report AND ((CashFlow.StartDate >= growth-month.StartDate
                            AND CashFlow.StartDate <= growth-month.EndDate)
                          OR (CashFlow.EndDate >= growth-month.StartDate
                            AND CashFlow.EndDate <= growth-month.EndDate))
    THEN DO:
      line = "Cashflow from: " + null-str(STRING(CashFlow.StartDate, "99/99/9999"), "--/--/----")
           + " to " + null-str(STRING(CashFlow.EndDate,"99/99/9999"), "++/++/++++") + " "
           + null-str( STRING( growth-month.StartDate, "99/99/9999"), "??/??/????") + "  "
           + null-str( STRING( part, ">9.9999"), "?") + "  "
           + null-str( STRING( amount, mfmt), "?").

      RUN audit-line( base-font, line ).
    END.

    RUN update-forecast( CashFlow.EntityType, CashFlow.EntityCode, CashFlow.AccountCode, CashFlow.CashFlowType, amount).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-tenant) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-tenant Procedure 
PROCEDURE each-tenant :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN audit-line( header-font, "Tenant " + STRING(Tenant.TenantCode) + " " + Tenant.Name ).
  FOR EACH TenancyLease OF Tenant WHERE TenancyLease.LeaseStatus <> "PAST" NO-LOCK:
    BUFFER-COPY TenancyLease TO Lease.
    RUN audit-line( base-font, "Lease " + STRING( TenancyLease.TenancyLeaseCode ) + " of " + TenancyLease.AreaDescription ).
    /* normal tenants (lease ends after forecast start) */
    RUN reset-lease-rates.
    RUN build-lease-actions.
    IF Lease.LeaseEndDate < forecast-start THEN DO:
      rates.b-floor = 0.        rates.b-parks = 0.      rates.b-other = 0.
      rates.last-action = "".
      RUN process-actions( "VACT" ).
    END.
    ELSE
      RUN process-actions( "BASE" ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-tenant-recoveries) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-tenant-recoveries Procedure 
PROCEDURE each-tenant-recoveries :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN audit-line( header-font, "Tenant " + STRING(Tenant.TenantCode) + " " + Tenant.Name ).
  FOR EACH TenancyLease OF Tenant WHERE TenancyLease.LeaseStatus <> "PAST" NO-LOCK:
    BUFFER-COPY TenancyLease TO Lease.
    RUN audit-line( base-font, "Lease " + STRING( TenancyLease.TenancyLeaseCode ) + " of " + TenancyLease.AreaDescription ).
    /* normal tenants (lease ends after forecast start) */
    RUN reset-lease-rates.
    RUN build-lease-actions.
    IF Lease.LeaseEndDate < forecast-start THEN DO:
      rates.b-floor = 0.        rates.b-parks = 0.      rates.b-other = 0.
      rates.last-action = "".
      RUN process-recoveries-actions( "VACT" ).
    END.
    ELSE
      RUN process-recoveries-actions( "RCBS" ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-vacant-space) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-vacant-space Procedure 
PROCEDURE each-vacant-space :
/*------------------------------------------------------------------------------
  Purpose:  Handle spaces that are currently vacant, or due to be prior to the
            end of the forecast.
------------------------------------------------------------------------------*/
  RUN audit-line( header-font, "Vacant area: " + STRING(RentalSpace.Level) + "/" + STRING(RentalSpace.LevelSequence) + RentalSpace.Description ).
  RUN reset-area-rates.
  RUN build-vacant-actions.
  RUN process-actions( "VACT" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-vacant-space-recoveries) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-vacant-space-recoveries Procedure 
PROCEDURE each-vacant-space-recoveries :
/*------------------------------------------------------------------------------
  Purpose:  Handle spaces that are currently vacant, or due to be prior to the
            end of the forecast.
------------------------------------------------------------------------------*/
  RUN audit-line( header-font, "Vacant area: " + STRING(RentalSpace.Level) + "/" + STRING(RentalSpace.LevelSequence) + RentalSpace.Description ).
  RUN reset-area-rates.
  RUN build-vacant-actions.
  RUN process-recoveries-actions( "VACT" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-inst-page-footer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-footer Procedure 
PROCEDURE inst-page-footer :
/*------------------------------------------------------------------------------
  Purpose:  Print any page footer
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-inst-page-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-header Procedure 
PROCEDURE inst-page-header :
/*------------------------------------------------------------------------------
  Purpose:  Print any page header
------------------------------------------------------------------------------*/
  IF in-rent-report THEN DO:
    RUN rr-page-header.
    RETURN.
  END.

  RUN pclrep-line( "univers,Point,7,bold,Proportional", TimeStamp).
  RUN pclrep-line( "univers,Point,11,bold,Proportional", SPC(70) + "AmTrust Forecasting Run" ).
  RUN pclrep-line( "", "" ).

  RUN pclrep-line( base-font + ",bold", "  Type   From   Mth   Floor      Parks      Other      Against         Base         Growth         Market     Pro-rata  Last action").

  RUN pclrep-line( "", "" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-make-action) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE make-action Procedure 
PROCEDURE make-action :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER act-date AS DATE NO-UNDO.
DEF INPUT PARAMETER act-type AS CHAR NO-UNDO.
DEF INPUT PARAMETER act-desc AS CHAR NO-UNDO.

  FIND Month WHERE Month.StartDate <= act-date AND Month.EndDate >= act-date NO-ERROR.
  IF NOT AVAILABLE(Month) THEN RETURN.  /* must be outside of system periods! */

  CREATE Action.
  Action.TenancyLeaseCode = (IF AVAILABLE(TenancyLease) THEN TenancyLease.TenancyLeaseCode ELSE ?).
  Action.RentalSpaceCode = (IF AVAILABLE(RentalSpace) THEN RentalSpace.RentalSpaceCode ELSE ?).
  Action.ActionDate = act-date.
  Action.MonthCode = Month.MonthCode.
  Action.ActionType = act-type.
  Action.ActionDescription = act-desc.
  Action.ActionFloor = 0.0 .
  Action.ActionParks = 0.0 .
  Action.ActionOther = 0.0 .
  Action.ActionFCFloor = 0.0 .
  Action.ActionFCParks = 0.0 .
  Action.ActionFCOther = 0.0 .

  RUN dump-action.

  IF act-type = "End" THEN DO:
    DEF VAR vacant-months AS INT NO-UNDO.
    IF act-date < forecast-start THEN
      /* apply vacant-months from the start of the forecast */
      vacant-months = get-int-parameter("Vacant-Months", forecast-start, 1 ).
    ELSE
      vacant-months = get-int-parameter("Vacant-Months", act-date, 1 ).

    act-date = add-months( act-date + 1, vacant-months ).
    IF act-date < forecast-end THEN DO:
      IF act-date < forecast-start THEN act-date = forecast-start.
      RUN make-action(  act-date, "New", "Re leasing").
    END.
  END.
  ELSE IF act-type = "New" THEN DO:
    DEF VAR review-months AS INT NO-UNDO.
    /* add rent reviews */
    review-months = get-int-parameter("Review-Months", act-date, 12).
    DO WHILE act-date < forecast-end:
      act-date = add-months( act-date, review-months ).
      IF act-date < forecast-end THEN
        RUN make-action( act-date, "Rvw", "Review on new lease").
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-override-action) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-action Procedure 
PROCEDURE override-action :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER override-action AS CHAR NO-UNDO.

DEF VAR act-date AS DATE NO-UNDO.
DEF VAR old-type AS CHAR NO-UNDO.
DEF VAR new-type AS CHAR NO-UNDO.
DEF VAR floor-amt AS DEC NO-UNDO.
DEF VAR parks-amt AS DEC NO-UNDO.
DEF VAR other-amt AS DEC NO-UNDO.
DEF VAR act-desc AS CHAR NO-UNDO.

  ASSIGN act-date = DATE( ENTRY( 3, override-action) )
         old-type = ENTRY(4, override-action)
         new-type = ENTRY(5, override-action)
         floor-amt = DEC( ENTRY(6, override-action) )
         parks-amt = DEC( ENTRY(7, override-action) )
         other-amt = DEC( ENTRY(8, override-action) )
         act-desc = ENTRY(9, override-action) NO-ERROR.

  IF act-desc = ? THEN act-desc = "".
  IF act-date = ? THEN DO:
    /* use the start date of the month applying */
    FIND Month WHERE Month.MonthCode = PropForecastParam.MonthCode NO-LOCK.
    act-date = Month.StartDate.
  END.

  FIND FIRST growth-month WHERE growth-month.StartDate > (act-date - round-up-after) NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(growth-month) THEN RETURN.  /* must be outside of forecast */

  IF old-type <> "" THEN DO:
    FIND Action WHERE Action.MonthCode = growth-month.MonthCode AND
                      Action.ActionType = old-type NO-ERROR.
  END.
  ELSE DO:
    CREATE Action.
    Action.MonthCode = growth-month.MonthCode.
    Action.ActionType = new-type.
  END.
  Action.ActionDate = act-date.
  Action.ActionType = new-type.
  Action.ActionDescription = act-desc.
  Action.ActionFloor = floor-amt.
  Action.ActionParks = parks-amt.
  Action.ActionOther = other-amt.
  Action.ActionFCFloor = floor-amt.
  Action.ActionFCParks = parks-amt.
  Action.ActionFCOther = other-amt.

  RUN dump-action.

  IF new-type = "End" THEN DO:
    FOR EACH Action WHERE Action.ActionDate > act-date:
      DELETE Action.
    END.
    DEF VAR vacant-months AS INT NO-UNDO.
    IF act-date < forecast-start THEN
      /* apply vacant-months from the start of the forecast */
      vacant-months = get-int-parameter("Vacant-Months", forecast-start, 1 ).
    ELSE
      vacant-months = get-int-parameter("Vacant-Months", act-date, 1 ).

    act-date = add-months( act-date + 1, vacant-months ).
    IF act-date < forecast-end THEN DO:
      IF act-date < forecast-start THEN act-date = forecast-start.
      RUN make-action(  act-date, "New", "Re leasing").
    END.
  END.
  ELSE IF new-type = "New" THEN DO:
    FOR EACH Action WHERE Action.ActionDate > act-date:
      DELETE Action.
    END.
    DEF VAR review-months AS INT NO-UNDO.
    /* add rent reviews */
    review-months = get-int-parameter("Review-Months", act-date, 12).
    DO WHILE act-date < forecast-end:
      act-date = add-months( act-date, review-months ).
      IF act-date < forecast-end THEN
        RUN make-action( act-date, "Rvw", "Review on new lease").
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR token AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.

{inc/showopts.i "report-options"}

  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).

    /* Preview and Property options are set for each run */    
    CASE ENTRY( 1, token ):
      WHEN "Preview" THEN               preview = Yes.

      WHEN "ClearAll" THEN              clear-all = Yes.
      WHEN "AuditReport" THEN           audit-report = Yes.
      WHEN "RentReport" THEN            rent-report = Yes.
      WHEN "SkipAgentsFees" THEN        skip-agents-fees = Yes.
      WHEN "ReportOnly" THEN            regenerate = No.

      WHEN "Regenerate" THEN DO:
        regenerate = Yes.
        CASE ENTRY(2,token):
          WHEN "Rents" THEN         regenerate-rents = Yes.
          WHEN "Expenses" THEN      regenerate-expenses = Yes.
          WHEN "Recoveries" THEN    regenerate-recoveries = Yes.
          OTHERWISE ASSIGN
            regenerate-rents = Yes
            regenerate-expenses = Yes
            regenerate-recoveries = Yes.
        END CASE.
      END.

      WHEN "All" THEN ASSIGN
        property-1 = 0
        property-n = 999999.

      WHEN "Properties" THEN ASSIGN
        property-1 = INT(ENTRY(2,token))
        property-n = INT(ENTRY(2,token)).
    END CASE.
  END.

  FIND RP WHERE RP.UserName = "Amtrust-Forecast"
            AND RP.ReportID = {&REPORT-ID} NO-LOCK NO-ERROR.

  IF AVAILABLE RP THEN report-options = RP.Char6.

  /* inc/showopts doesn't work twice */
  IF AVAILABLE(UsrGroupMember) THEN MESSAGE report-options.

  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).

    /* months & ratchet options apply until forecast is cleared */    
    CASE ENTRY( 1, token ):
      WHEN "AllRatchets" THEN           all-ratchets = Yes.
      WHEN "Months" THEN ASSIGN
        month-1 = INT(ENTRY(2,token))
        month-n = INT(ENTRY(3,token)).

    END CASE.
  END.

  FIND Month WHERE Month.MonthCode = month-1 NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Month) THEN DO:
    MESSAGE "Invalid starting month for forecast"
            VIEW-AS ALERT-BOX ERROR.
    RETURN ERROR.
  END.
  forecast-start = Month.StartDate.
  FIND Month WHERE Month.MonthCode = month-n NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Month) THEN DO:
    MESSAGE "Invalid ending month for forecast"
            VIEW-AS ALERT-BOX ERROR.
    RETURN ERROR.
  END.
  forecast-end = Month.EndDate.

  IF clear-all THEN ASSIGN
    regenerate            = Yes
    regenerate-rents      = Yes
    regenerate-expenses   = Yes
    regenerate-recoveries = Yes .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-actions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE process-actions Procedure 
PROCEDURE process-actions :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER pfc-type AS CHAR NO-UNDO.

DEF VAR lease-ended AS LOGI NO-UNDO.

  IF audit-report THEN RUN audit-line( base-font, "Processing Lease Actions" ).

  FOR EACH growth-month :
    rates.pro-rate = 1.0 .
    lease-ended = (pfc-type = "VACT").
    rates.lg-floor = rates.g-floor.
    rates.lg-parks = rates.g-parks.
    rates.lg-other = rates.g-other.

    FOR EACH Action OF growth-month BY Action.ActionDate:
      CASE Action.ActionType:
        WHEN "End" THEN DO:
          IF NOT lease-ended THEN process-action-end-before( ).
        END.
        WHEN "Old" THEN DO:
          pfc-type = process-action-old( ).
          lease-ended = No.
        END.
        WHEN "New" THEN DO:
          IF lease-ended THEN pfc-type = process-action-new( ).
          lease-ended = No.
        END.
        WHEN "Rvw" THEN DO:
          IF NOT lease-ended THEN process-action-rvw().
        END.
        WHEN "One" THEN     process-action-one( ).
      END CASE.
      RUN dump-action.
      rates.last-action = Action.ActionType.
    END.

    IF rates.lg-floor + rates.lg-other + rates.b-floor + rates.b-other = 0 THEN DO:
      /* Increase carparks  */
      /* rates.b-parks = rates.m-parks. */
      rates.g-parks = (rates.b-parks * growth-month.parks) - rates.b-parks.
      RUN update-pfc-entity( parks-ac, "G" + pfc-type, rates.lg-parks + ((rates.g-parks - rates.lg-parks) * rates.pro-rate) ).
    END.
    ELSE DO:
      RUN update-pfc-entity( floor-ac, "G" + pfc-type, rates.lg-floor + ((rates.g-floor - rates.lg-floor) * rates.pro-rate) ).
      RUN update-pfc-entity( parks-ac, "G" + pfc-type, rates.lg-parks + ((rates.g-parks - rates.lg-parks) * rates.pro-rate) ).
      RUN update-pfc-entity( other-ac, "G" + pfc-type, rates.lg-other + ((rates.g-other - rates.lg-other) * rates.pro-rate) ).
    END.

    IF rates.last-action = "Rvw" THEN rates.pro-rate = 1.0 .
    RUN update-pfc-entity( floor-ac, pfc-type, rates.b-floor * rates.pro-rate).
    RUN update-pfc-entity( parks-ac, pfc-type, rates.b-parks * rates.pro-rate).
    RUN update-pfc-entity( other-ac, pfc-type, rates.b-other * rates.pro-rate).
    RUN dump-rates( pfc-type ).

    IF rates.last-action = "End" THEN DO:
      FIND LAST Action OF growth-month WHERE Action.ActionType = "End".
      pfc-type = process-action-end-after().
      rates.last-action = "".
    END.

/*
    FOR EACH PropForecast OF Property WHERE PropForecast.EntityType = rates.et
                                        AND PropForecast.EntityCode = rates.ec
                                        AND PropForecast.MonthCode = growth-month.MonthCode NO-LOCK:
      RUN dump-prop-forecast.
    END.
*/
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-recoveries-actions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE process-recoveries-actions Procedure 
PROCEDURE process-recoveries-actions :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER pfc-type AS CHAR NO-UNDO.

DEF VAR lease-ended AS LOGI NO-UNDO.
DEF VAR new-lease AS LOGI NO-UNDO INITIAL No.
DEF VAR part AS DEC NO-UNDO.
DEF VAR percentage AS DEC NO-UNDO.

DEF VAR et AS CHAR NO-UNDO.
DEF VAR ec AS INT NO-UNDO.
DEF VAR line AS CHAR NO-UNDO.

et = "P".
ec = Property.PropertyCode.

DEF BUFFER ExpPFC FOR PropForecast.

  FOR EACH growth-month :
    lease-ended = (pfc-type = "VACT").
    new-lease = new-lease OR lease-ended.
    part = (IF lease-ended THEN 0.0 ELSE -1.0 ).

    FOR EACH Action OF growth-month BY Action.ActionDate:
      CASE Action.ActionType:
        WHEN "End" THEN DO:
          part = - part-of-period( growth-month.StartDate, Action.ActionDate ).
          lease-ended = Yes.
        END.
        WHEN "Old" THEN DO:
          IF lease-ended THEN
            part = - part-of-period( Action.ActionDate, growth-month.EndDate ).
          lease-ended = No.
          pfc-type = "RCBS".
        END.
        WHEN "New" THEN DO:
          new-lease = Yes.
          IF lease-ended THEN
            part = - part-of-period( Action.ActionDate, growth-month.EndDate ).
          lease-ended = No.
          pfc-type = "RCNW".
        END.
      END CASE.
      RUN dump-action.
    END.

    line = pfc-type + " " + STRING(growth-month.StartDate, "99/99/9999")
         + STRING(growth-month.MonthCode, ">>>9") + "  "
         + STRING( - part, ">9.9999") + " ".
    RUN audit-line( base-font, line ).

    IF NOT(new-lease) AND AVAILABLE(TenancyLease) AND TenancyLease.RecoveryType = "F" THEN DO:
      RUN update-forecast( et, ec, fixed-og-account,
                              pfc-type, (TenancyLease.OutgoingsBudget / 12) * part ).
    END.
    ELSE DO:
      FOR EACH ExpPFC WHERE ExpPFC.PropertyCode = Property.PropertyCode
                        AND ExpPFC.EntityType = "P"
                        AND ExpPFC.EntityCode = Property.PropertyCode
                        AND ExpPFC.MonthCode = growth-month.MonthCode
                        AND ExpPFC.PropForecastType = "EXP" NO-LOCK:
        IF NOT(new-lease) AND AVAILABLE(TenancyLease) THEN
          percentage = get-lease-recovery( ExpPFC.AccountCode, lease-ended ).
        ELSE DO:
          IF new-lease AND AVAILABLE(TenancyLease) THEN DO:
            percentage = 0.0 .
            IF TenancyLease.OutgoingsRate > 0.0 THEN DO:
              FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = ExpPFC.AccountCode NO-LOCK NO-ERROR.
              IF AVAILABLE(ChartOfAccount) AND ChartOfAccount.ExpenseRecoveryType = "P" THEN
                percentage = TenancyLease.OutgoingsRate.
            END.
            /* MESSAGE ExpPFC.MonthCode  ExpPFC.AccountCode percentage . */
            /* MESSAGE ExpPFC.MonthCode  ExpPFC.AccountCode percentage VIEW-AS ALERT-BOX . */
          END.
          ELSE DO:
            percentage = RentalSpace.OutgoingsPercentage.
          END.
        END.

        line = FILL(" ",20) + STRING(ExpPFC.AccountCode, "9999.99")
             + STRING( percentage / 100.0, ">9.9999")
             + STRING( ExpPFC.Amount, ">>,>>>,>>9.99")
             + STRING( ExpPFC.Amount * (percentage / 100.0) * part, "->>,>>>,>>9.99") .
        RUN audit-line( base-font, line ).
        RUN update-forecast( et, ec, ExpPFC.AccountCode + recovery-offset,
                              pfc-type, ExpPFC.Amount * (percentage / 100.0) * part ).
      END.
      IF /* NOT(new-lease) AND */ AVAILABLE(TenancyLease) THEN DO:
        FOR EACH ChartOfAccount WHERE ChartOfAccount.ExpenseRecoveryType = "O" NO-LOCK,
                  FIRST TenancyOutgoing OF TenancyLease WHERE TenancyOutgoing.AccountCode = ChartOfAccount.AccountCode NO-LOCK:
          RUN update-forecast( et, ec, ChartOfAccount.AccountCode + recovery-offset,
                              pfc-type, (TenancyOutgoing.FixedAmount / 12) * part ).
        END.
      END.
    END.
    IF lease-ended THEN pfc-type = "VACT".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reset-area-rates) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-area-rates Procedure 
PROCEDURE reset-area-rates :
/*------------------------------------------------------------------------------
  Purpose:  Set up the 'rates' record for the current lease
------------------------------------------------------------------------------*/
DEF VAR mkt-pc AS DEC NO-UNDO.

    rates.b-floor = 0.      rates.b-parks = 0.      rates.b-other = 0.
    rates.g-floor = 0.      rates.g-parks = 0.      rates.g-other = 0.
    rates.m-floor = 0.      rates.m-parks = 0.      rates.m-other = 0.

    /* Don't do any rates if it's externally managed - its all manual then */
    IF Property.ExternallyManaged THEN RETURN.

    FIND FIRST AreaType OF RentalSpace NO-LOCK.
    mkt-pc = (IF RentalSpace.MarketRental > 0 THEN RentalSpace.MarketRental / 100.0 ELSE 1.0).
    IF AreaType.IsFloorArea THEN
      rates.m-floor = (RentalSpace.AreaSize * Property.MarketRental * mkt-pc) / 12.
    ELSE IF AreaType.IsCarPark THEN
      rates.m-parks = (RentalSpace.AreaSize * Property.MarketCarpark * mkt-pc * 52) / 12.

    rates.et = "R".
    rates.ec = RentalSpace.RentalSpaceCode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reset-growth-rates) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-growth-rates Procedure 
PROCEDURE reset-growth-rates :
/*------------------------------------------------------------------------------
  Purpose:  Calculate all of the compounding rgwoth rates in effect for this property
------------------------------------------------------------------------------*/
DEF VAR floor-growth AS DEC NO-UNDO INITIAL 1.0 .
DEF VAR parks-growth AS DEC NO-UNDO INITIAL 1.0 .
DEF VAR other-growth AS DEC NO-UNDO INITIAL 1.0 .

  FIND FIRST growth-month NO-ERROR.
  IF NOT AVAILABLE(growth-month) THEN DO:
    /* create the table for all months of the forecast */
    FOR EACH Month WHERE Month.MonthCode >= month-1 AND Month.MonthCode <= month-n NO-LOCK:
      CREATE growth-month.
      BUFFER-COPY Month TO growth-month.
    END.
  END.

  /* update the interest rates from the start of the forecast */
  FOR EACH growth-month:
    growth-month.floor = floor-growth.
    growth-month.parks = parks-growth.
    growth-month.other = floor-growth.      /* not 'other' growth because we don't do that */
    floor-growth = floor-growth * to-month-rate( get-parameter( "Rental-Growth", growth-month.StartDate)).
    parks-growth = parks-growth * to-month-rate( get-parameter( "Carpark-Growth", growth-month.StartDate)).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reset-lease-rates) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-lease-rates Procedure 
PROCEDURE reset-lease-rates :
/*------------------------------------------------------------------------------
  Purpose:  Set up the 'rates' record for the current lease
------------------------------------------------------------------------------*/
DEF VAR mkt-pc AS DEC NO-UNDO.
DEF VAR other-size AS DEC NO-UNDO INITIAL 0.0 .
DEF VAR other-mult AS DEC NO-UNDO INITIAL 0.0 .

  rates.b-floor = 0.      rates.b-parks = 0.      rates.b-other = 0.
  rates.g-floor = 0.      rates.g-parks = 0.      rates.g-other = 0.
  rates.m-floor = 0.      rates.m-parks = 0.      rates.m-other = 0.

  /* Don't do any rates if it's externally managed - its all manual then */
  IF Property.ExternallyManaged THEN RETURN.

  /* do market rentals */
  FOR EACH RentalSpace WHERE RentalSpace.TenancyLeaseCode = Lease.TenancyLeaseCode NO-LOCK,
                      FIRST AreaType OF RentalSpace:
    mkt-pc = (IF RentalSpace.MarketRental > 0 THEN RentalSpace.MarketRental / 100.0 ELSE 1.0).
    IF AreaType.IsFloorArea THEN DO:
      rates.m-floor = rates.m-floor + (mkt-pc * RentalSpace.AreaSize * Property.MarketRental / 12).
    END.
    ELSE IF AreaType.IsCarPark THEN ASSIGN
      rates.m-parks = rates.m-parks + (mkt-pc * RentalSpace.AreaSize * Property.MarketCarpark * 52 / 12).
    ELSE ASSIGN
      /* for market we use actual, which is set below, but we need to be able to handle a %age */
      other-size = other-size + RentalSpace.AreaSize
      other-mult = other-mult + (RentalSpace.AreaSize * mkt-pc).
  END.

  /* do actual rentals */
  FOR EACH RentCharge OF TenancyLease NO-LOCK,
                    FIRST AreaType WHERE AreaType.AreaType = RentCharge.RentChargeType:
    FOR EACH RentChargeLine OF RentCharge WHERE RentChargeLine.RentChargeLineStatus = "C"
                              AND RentChargeLine.StartDate <= forecast-start
                              AND (RentChargeLine.EndDate >= forecast-start
                                    OR RentChargeLine.EndDate = ?) NO-LOCK:
      IF AreaType.IsFloorArea THEN
        rates.b-floor = rates.b-floor + to-annual( RentChargeLine.FrequencyCode, RentChargeLine.Amount ).
      ELSE IF AreaType.IsCarPark THEN ASSIGN
        rates.b-parks = rates.b-parks + to-annual( RentChargeLine.FrequencyCode, RentChargeLine.Amount ).
      ELSE ASSIGN
        rates.b-other = rates.b-other + to-annual( RentChargeLine.FrequencyCode, RentChargeLine.Amount ).
    END.
  END.

  rates.b-floor = rates.b-floor / 12.
  rates.b-parks = rates.b-parks / 12.
  rates.b-other = rates.b-other / 12.
  rates.m-other = rates.b-other * (IF other-size > 0 THEN other-mult / other-size ELSE 1.0).

  rates.et = "T".
  rates.ec = Tenant.TenantCode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-rr-build-table) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rr-build-table Procedure 
PROCEDURE rr-build-table :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR first-year AS INT NO-UNDO.
FIND FIRST growth-month.
first-year = growth-month.FinancialYearCode.

  RUN audit-line( header-font, "Building temp table for Rent Report").
  FOR EACH Property WHERE Active AND Property.PropertyCode >= property-1
                      AND Property.PropertyCode <= property-n NO-LOCK:
    RUN audit-line( base-font, "Temp table for Property " + STRING(Property.PropertyCode)).
    FOR EACH PropForecast OF Property NO-LOCK:
      FIND growth-month WHERE growth-month.MonthCode = PropForecast.MonthCode.
      i = (growth-month.FinancialYear - first-year) + 1.
      FIND rr-line WHERE rr-line.pcode = PropForecast.PropertyCode
                     AND rr-line.et    = PropForecast.EntityType
                     AND rr-line.ec    = PropForecast.EntityCode
                     AND rr-line.ltype = PropForecast.PropForecastType NO-ERROR.

      IF NOT AVAILABLE(rr-line) THEN DO:
        CREATE rr-line.
        rr-line.pcode = PropForecast.PropertyCode.
        rr-line.et    = PropForecast.EntityType.
        rr-line.ec    = PropForecast.EntityCode.
        rr-line.ltype = PropForecast.PropForecastType.
        IF skip-agents-fees AND rr-line.ltype = "AGNT" THEN NEXT.
        rr-line.lseq = LOOKUP( PropForecast.PropForecastType, rr-sequences).
        IF rr-line.lseq = 0 THEN DO:
          rr-sequences = rr-sequences + "," + PropForecast.PropForecastType.
          rr-line.lseq = LOOKUP( PropForecast.PropForecastType, rr-sequences).
          RUN audit-line( base-font, "Couldn't locate '" + PropForecast.PropForecastType + "' in line type list, using default").
        END.
      END.
      IF PropForecast.PropForecastType = "AGNT" THEN
        rr-line.years[i] = rr-line.years[i] - PropForecast.Amount.
      ELSE
        rr-line.years[i] = rr-line.years[i] + PropForecast.Amount.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-rr-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rr-line Procedure 
PROCEDURE rr-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER rr-total FOR rr-line.

DEF VAR t1 AS CHAR NO-UNDO.
DEF VAR t2 AS CHAR NO-UNDO.
DEF VAR is-total-line AS LOGI NO-UNDO.
DEF VAR i AS INT NO-UNDO.

  IF rr-line.pcode > 99999 THEN
    t1 = "Grand totals".
  ELSE IF rr-line.et = "Z" THEN
    t1 = "Property " + STRING(Property.PropertyCode) + " totals".
  ELSE
    t1 = rr-line.et + STRING( rr-line.ec, ">99999").

  IF rr-line.ltype = "TOTAL" THEN DO: /* entity total line */
    RUN pclrep-line( base-font, FILL(" ",20) + FILL( " " + FILL( "-", LENGTH(mfmt) - 1), 5)).
  END.
  ELSE DO:
    /* find and add to total of this group */
    FIND rr-total WHERE rr-total.pcode = rr-line.pcode
                    AND rr-total.et = rr-line.et
                    AND rr-total.ec = rr-line.ec
                    AND rr-total.ltype = "TOTAL" NO-ERROR.
    IF NOT AVAILABLE(rr-total) THEN DO:
      CREATE rr-total.
      rr-total.pcode = rr-line.pcode.
      rr-total.et = rr-line.et.
      rr-total.ec = rr-line.ec.
      rr-total.ltype = "TOTAL".
      rr-total.lseq = 9.
    END.
    DO i = 1 TO 5:
      rr-total.years[i] = rr-total.years[i] + rr-line.years[i].
    END.

    IF rr-line.pcode <= 99999 THEN DO:
      /* find and add to total at level above */
      FIND rr-total WHERE rr-total.pcode = (IF rr-line.et = "Z" THEN 999999 ELSE rr-line.pcode)
                      AND rr-total.et = "Z"
                      AND rr-total.ec = 999999
                      AND rr-total.ltype = rr-line.ltype NO-ERROR.
      IF NOT AVAILABLE(rr-total) THEN DO:
        CREATE rr-total.
        rr-total.pcode = (IF rr-line.et = "Z" THEN 999999 ELSE rr-line.pcode).
        rr-total.et = "Z".
        rr-total.ec = 999999.
        rr-total.ltype = rr-line.ltype.
        rr-total.lseq = rr-line.lseq.
      END.
      DO i = 1 TO 5:
        rr-total.years[i] = rr-total.years[i] + rr-line.years[i].
      END.
    END.
  END.

  ASSIGN t2 = ENTRY( rr-line.lseq, "Base rental,Growth on Base Rental,New leases,Growth on new leases,Vacant costs,Growth on vacant costs,One-off income/costs,Agents Fees,Total,,,,,,") NO-ERROR.
/*  IF t2 = ? THEN MESSAGE rr-line.lseq VIEW-AS ALERT-BOX. */
  IF t2 = "" OR t2 = ? THEN t2 = rr-line.ltype .
  RUN pclrep-line( base-font, STRING( t1, "X(20)")
                            + STRING( rr-line.years[1], mfmt)
                            + STRING( rr-line.years[2], mfmt)
                            + STRING( rr-line.years[3], mfmt)
                            + STRING( rr-line.years[4], mfmt)
                            + STRING( rr-line.years[5], mfmt)
                            + "  " + t2 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-rr-page-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rr-page-header Procedure 
PROCEDURE rr-page-header :
/*------------------------------------------------------------------------------
  Purpose:  Print page header for the rent report
------------------------------------------------------------------------------*/
DEF VAR tfmt AS CHAR NO-UNDO.
DEF VAR first-year AS INT NO-UNDO.
DEF BUFFER gmth FOR growth-month.
FIND FIRST gmth.
first-year = gmth.FinancialYearCode.

  RUN pclrep-line( "univers,Point,7,bold,Proportional", TimeStamp).
  RUN pclrep-line( "univers,Point,11,bold,Proportional", SPC(70) + "AmTrust Forecasting Run - Rental Report" ).
  RUN pclrep-line( "", "" ).

  tfmt = STRING( "    9999    ", "X(" + STRING(LENGTH(mfmt)) + ")").
  RUN pclrep-line( base-font + ",bold", FILL(" ",25) + STRING(first-year, tfmt)
                      + STRING(first-year + 1, tfmt) + STRING(first-year + 2, tfmt)
                      + STRING(first-year + 3, tfmt) + STRING(first-year + 4, tfmt) ).

  RUN pclrep-line( "", "" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-rr-rent-report) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rr-rent-report Procedure 
PROCEDURE rr-rent-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR years AS DEC NO-UNDO EXTENT 5 INITIAL 0.
DEF VAR etot AS DEC NO-UNDO EXTENT 5 INITIAL 0.
DEF VAR ptot AS DEC NO-UNDO EXTENT 5 INITIAL 0.

DEF VAR last-type AS CHAR NO-UNDO INITIAL "".
DEF VAR last-entity AS CHAR NO-UNDO INITIAL "".
DEF VAR this-entity AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR first-year AS INT NO-UNDO.
FIND FIRST growth-month.
first-year = growth-month.FinancialYearCode.


  RUN rr-build-table.
  in-rent-report = Yes.
  IF audit-report THEN RUN pclrep-page-break.
  FOR EACH Property WHERE Active AND Property.PropertyCode >= property-1
                      AND Property.PropertyCode <= property-n NO-LOCK:
    RUN pclrep-line( header-font, "Property " + STRING(Property.PropertyCode) + " " + Property.Name ).

    /* tenants */
    FOR EACH Tenant NO-LOCK WHERE Tenant.EntityType = "P"
                    AND Tenant.EntityCode = Property.PropertyCode
                    AND CAN-FIND( FIRST rr-line WHERE rr-line.pcode = Property.PropertyCode
                                    AND rr-line.et = "T" AND rr-line.ec = Tenant.TenantCode):
      RUN pclrep-line( header-font, "Tenant " + STRING(Tenant.TenantCode) + " " + Tenant.Name ).
      FOR EACH rr-line WHERE rr-line.pcode = Property.PropertyCode
                        AND rr-line.et = "T" 
                        AND rr-line.ec = Tenant.TenantCode:
        RUN rr-line.
      END.
      RUN pclrep-line(base-font,"").
    END.

    /* vacant spaces */
    FOR EACH RentalSpace NO-LOCK OF Property
                WHERE CAN-FIND( FIRST rr-line WHERE rr-line.pcode = Property.PropertyCode
                                    AND rr-line.et = "R" AND rr-line.ec = RentalSpace.RentalSpaceCode):
      RUN pclrep-line( header-font, "Vacant space " + STRING(RentalSpace.Level) + "/" + STRING(RentalSpace.LevelSequence) + " " + RentalSpace.Description ).
      FOR EACH rr-line WHERE rr-line.pcode = Property.PropertyCode
                                    AND rr-line.et = "R" 
                                    AND rr-line.ec = RentalSpace.RentalSpaceCode:
        RUN rr-line.
      END.
      RUN pclrep-line(base-font,"").
    END.

    FOR EACH rr-line WHERE rr-line.pcode = Property.PropertyCode
                                    AND rr-line.et = "Z":
      RUN rr-line.
    END.
    RUN pclrep-down-by(2).

  END.

  IF property-1 <> property-n THEN FOR EACH rr-line WHERE rr-line.pcode = 999999:
    RUN rr-line.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-stamp-time) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE stamp-time Procedure 
PROCEDURE stamp-time :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  timings[time-pos] = ETIME.
  time-pos = time-pos + 1.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-update-forecast) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-forecast Procedure 
PROCEDURE update-forecast :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER et AS CHAR NO-UNDO.
DEF INPUT PARAMETER ec AS INT NO-UNDO.
DEF INPUT PARAMETER ac AS DEC NO-UNDO.
DEF INPUT PARAMETER pf-linetype AS CHAR NO-UNDO.
DEF INPUT PARAMETER amount AS DEC NO-UNDO.

  IF amount = 0 OR amount = ? THEN RETURN.

DO TRANSACTION:
  FIND PropForecast WHERE PropForecast.PropertyCode = Property.PropertyCode
                      AND PropForecast.EntityType = et
                      AND PropForecast.EntityCode = ec
                      AND PropForecast.AccountCode = ac
                      AND PropForecast.MonthCode = growth-month.MonthCode
                      AND PropForecast.PropForecastType = SUBSTRING(pf-linetype, 1, 4)
                      EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE(PropForecast) THEN
    PropForecast.Amount = PropForecast.Amount + amount.
  ELSE DO:
    CREATE PropForecast.
    PropForecast.PropertyCode = Property.PropertyCode.
    PropForecast.EntityType = et.
    PropForecast.EntityCode = ec.
    PropForecast.AccountCode = ac.
    PropForecast.MonthCode = growth-month.MonthCode.
    PropForecast.PropForecastType = SUBSTRING(pf-linetype, 1, 4).
    PropForecast.Amount = amount.
  END.

  FIND PropForecast OF Property WHERE PropForecast.EntityType = et
                                    AND PropForecast.EntityCode = ec
                                    AND PropForecast.AccountCode = ac
                                    AND PropForecast.MonthCode = growth-month.MonthCode
                                    AND PropForecast.PropForecastType = SUBSTRING(pf-linetype, 1, 4)
                                    NO-LOCK NO-ERROR.
END.
/*   RUN dump-prop-forecast. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-update-pfc-entity) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-pfc-entity Procedure 
PROCEDURE update-pfc-entity :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER ac AS DEC NO-UNDO.
DEF INPUT PARAMETER pf-linetype AS CHAR NO-UNDO.
DEF INPUT PARAMETER amount AS DEC NO-UNDO.

  IF amount = 0 OR amount = ? THEN RETURN.

  RUN update-forecast( rates.et, rates.ec, ac, pf-linetype, amount ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-from-annual) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION from-annual Procedure 
FUNCTION from-annual RETURNS DECIMAL
  ( INPUT freq-code AS CHAR, INPUT annual-amount AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert annual amount to annual figure
    Notes:  
------------------------------------------------------------------------------*/
  /* short circuit for most cases */
  IF freq-code = "MNTH" THEN RETURN (annual-amount / 12.0).

  FIND FrequencyType WHERE FrequencyType.FrequencyCode = freq-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(FrequencyType) THEN RETURN (annual-amount / 12.0). /* assume monthly! */

  IF FrequencyType.RepeatUnits BEGINS "M" THEN
    RETURN ((annual-amount * DEC(FrequencyType.UnitCount)) / 12.0 ).

  RETURN ((annual-amount * DEC(FrequencyType.UnitCount)) / 365.0 ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-area-rental) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-area-rental Procedure 
FUNCTION get-area-rental RETURNS DECIMAL
  ( /* no parameters */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Return the base rental for the current area, at the forecast start date
    Notes:  Rental is returned as a monthly amount
------------------------------------------------------------------------------*/
DEF VAR base-rental AS DEC NO-UNDO.

  IF NOT AVAILABLE(RentalSpace) THEN DO:
    MESSAGE "Trying to obtain rental for unknown area!".
    RETURN 0.0 .
  END.

  IF NOT AVAILABLE(Property) THEN FIND Property OF RentalSpace NO-LOCK.

  FIND AreaType OF RentalSpace NO-LOCK.
  IF AreaType.IsCarPark THEN
    base-rental = RentalSpace.AreaSize * Property.MarketCarpark * 52 .
  ELSE IF AreaType.IsFloorArea THEN
    base-rental = RentalSpace.AreaSize * Property.MarketRental .
  ELSE
    base-rental = ?.

  base-rental = base-rental / 12.

  RETURN base-rental.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-int-parameter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-int-parameter Procedure 
FUNCTION get-int-parameter RETURNS INTEGER
  ( INPUT param-id AS CHAR, INPUT in-date AS DATE, INPUT default-no AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR r-str AS CHAR NO-UNDO.
DEF VAR result AS INT NO-UNDO.

  r-str = get-parameter( param-id, in-date ).
  IF r-str = "" THEN
    result = default-no.
  ELSE
    ASSIGN result = INT( r-str ) NO-ERROR.

  RETURN result.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-lease-recovery) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-lease-recovery Procedure 
FUNCTION get-lease-recovery RETURNS DECIMAL
  ( INPUT ac AS DEC, INPUT lease-ended AS LOGICAL  ) :
/*------------------------------------------------------------------------------
  Purpose:  Get the recovery percentage for the current lease for this account
    Notes:  
------------------------------------------------------------------------------*/

  IF ( NOT(lease-ended) ) THEN DO:
    FIND TenancyOutgoing OF TenancyLease WHERE TenancyOutgoing.AccountCode = ac NO-LOCK NO-ERROR.
    IF AVAILABLE(TenancyOutgoing) THEN RETURN TenancyOutgoing.Percentage.
  END.

  IF TenancyLease.OutgoingsRate > 0.0 THEN DO:
    FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = ac NO-LOCK NO-ERROR.
    IF AVAILABLE(ChartOfAccount) AND ChartOfAccount.ExpenseRecoveryType = "P" THEN
      RETURN TenancyLease.OutgoingsRate.
  END.

  RETURN 0.0 .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-parameter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-parameter Procedure 
FUNCTION get-parameter RETURNS CHARACTER
  ( INPUT param-id AS CHAR, INPUT in-date AS DATE ) :
/*------------------------------------------------------------------------------
  Purpose:  Get the parameter in effect on the date given
    Notes:  
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE(Property) THEN RETURN "".
  IF in-date > forecast-end THEN RETURN "".
  FIND growth-month WHERE growth-month.StartDate <= in-date AND growth-month.EndDate >= in-date NO-ERROR.
  IF NOT AVAILABLE(growth-month) THEN DO:
    IF in-date < forecast-end THEN
      RUN audit-line( base-font, "Can't find month for " + (IF in-date = ? THEN "?" ELSE STRING(in-date,"99/99/9999")) + " parameter " + param-id ).
    RETURN "".
  END.

  FIND LAST PropForecastParam WHERE PropForecastParam.PropertyCode = Property.PropertyCode
                AND PropForecastParam.ParameterID BEGINS param-id
                AND PropForecastParam.MonthCode <= growth-month.MonthCode NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(PropForecastParam) THEN DO:
    RUN audit-line( base-font, "Can't find property parameter " + param-id + " for month " + STRING(growth-month.StartDate) + ", property " + STRING(Property.PropertyCode) + " month=" + STRING(growth-month.MonthCode) ).
    FIND LAST PropForecastParam WHERE PropForecastParam.PropertyCode = default-property
                AND PropForecastParam.ParameterID BEGINS param-id
                AND PropForecastParam.MonthCode <= growth-month.MonthCode NO-LOCK NO-ERROR.
  END.
  IF AVAILABLE(PropForecastParam) THEN DO:
/*    RUN audit-line( base-font, "Using '" + PropForecastParam.ParameterValue + "' for " + param-id + " from date " + STRING( in-date ) ). */
    RETURN PropForecastParam.ParameterValue.
  END.

  RUN audit-line( base-font, "Parameter " + param-id + " not set for date " + STRING( in-date ) ).
  RETURN "".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-part-of-period) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION part-of-period Procedure 
FUNCTION part-of-period RETURNS DECIMAL
  ( INPUT part-1 AS DATE, INPUT part-n AS DATE ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR period-days AS DEC NO-UNDO.
DEF VAR part-days AS DEC NO-UNDO.

  period-days   = (growth-month.EndDate - growth-month.StartDate) + 1.

  IF part-n = ? OR part-n > growth-month.EndDate THEN
    part-n = growth-month.EndDate.

  IF part-1 = ? OR part-1 < growth-month.StartDate THEN
    part-1 = growth-month.StartDate.

  part-days = (part-n - part-1) + 1.

  RETURN (part-days / period-days).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-end-after) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION process-action-end-after Procedure 
FUNCTION process-action-end-after RETURNS CHARACTER
  ( /* no parameters */  ) :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  rates.b-floor = Action.ActionFloor.
  rates.b-parks = Action.ActionParks.
  rates.b-other = Action.ActionOther.

  Action.ActionFCFloor = rates.b-floor * growth-month.floor .
  Action.ActionFCParks = rates.b-parks * growth-month.parks .
  Action.ActionFCOther = rates.b-other * growth-month.other .

  rates.g-floor = Action.ActionFCFloor - Action.ActionFloor.
  rates.g-parks = Action.ActionFCParks - Action.ActionParks.
  rates.g-other = Action.ActionFCOther - Action.ActionOther.

  RETURN "VACT".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-end-before) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION process-action-end-before Procedure 
FUNCTION process-action-end-before RETURNS CHARACTER
  ( /* no parameters */  ) :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  rates.pro-rate = part-of-period( growth-month.StartDate, Action.ActionDate).

  RETURN "VAC".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-new) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION process-action-new Procedure 
FUNCTION process-action-new RETURNS CHARACTER
  ( /* no parameters */  ) :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF Action.ActionFCFloor = 0 AND Action.ActionFCParks = 0
        AND Action.ActionFCOther = 0
/*      AND Action.ActionFloor = 0
        AND Action.ActionParks = 0 AND Action.ActionOther = 0 */
  THEN DO:
    Action.ActionFCFloor = rates.m-floor * growth-month.floor .
    Action.ActionFCParks = rates.m-parks * growth-month.parks .
    Action.ActionFCOther = rates.m-other * growth-month.other .
    Action.ActionFloor = Action.ActionFCFloor.
    Action.ActionParks = Action.ActionFCParks.
    Action.ActionOther = Action.ActionFCOther.
  END.

  rates.b-floor = rates.m-floor.
  rates.b-parks = rates.m-parks.
  rates.b-other = rates.m-other.


  rates.g-floor = Action.ActionFCFloor - rates.b-floor.
  rates.g-parks = Action.ActionFCParks - rates.b-parks.
  rates.g-other = Action.ActionFCOther - rates.b-other.

  rates.pro-rate = part-of-period( Action.ActionDate, growth-month.EndDate).

DEF VAR annual-rental AS DEC NO-UNDO .
DEF VAR agents-fees AS DEC NO-UNDO .
DEF VAR agents-fees-param AS CHAR NO-UNDO .
DEF VAR these-agents-fees AS DEC NO-UNDO .
  annual-rental = (Action.ActionFCFloor + Action.ActionFCParks + Action.ActionFCOther) * 12 .
  agents-fees-param = get-parameter( 'Agents-Fees', Action.ActionDate).
  IF agents-fees-param = "" THEN
    these-agents-fees = agents-fees-rate.
  ELSE
    these-agents-fees = DEC( agents-fees-param ) NO-ERROR.
  IF these-agents-fees = ? THEN these-agents-fees = agents-fees-rate .
  agents-fees = annual-rental *  these-agents-fees .
  IF agents-fees > 0 THEN DO:
    RUN update-pfc-entity( agents-fees-account, "AGNT", agents-fees ).
    RUN audit-line( base-font, "Agents fees of " + STRING(agents-fees) + " applied to A/c " + STRING(agents-fees-account,"9999.99") ).
  END.
  ELSE DO:
    RUN audit-line( base-font, "Agents fees of 0 ("
        + TRIM(STRING(these-agents-fees * 100, "->>>9.9%"))
        + " of " + TRIM(STRING( annual-rental, "->>>,>>>,>>9.99"))
        + " - not applied" ).
  END.

  RETURN "NEW".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-old) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION process-action-old Procedure 
FUNCTION process-action-old RETURNS CHARACTER
  ( /* no parameters */  ) :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF Action.ActionFCFloor = 0 AND Action.ActionFCParks = 0
        AND Action.ActionFCOther = 0 AND Action.ActionFCFloor = 0
        AND Action.ActionFCParks = 0 AND Action.ActionFCOther = 0
  THEN DO:
    Action.ActionFCFloor = rates.m-floor * growth-month.floor .
    Action.ActionFCParks = rates.m-parks * growth-month.parks .
    Action.ActionFCOther = rates.m-other * growth-month.other .
    Action.ActionFloor = Action.ActionFCFloor.
    Action.ActionParks = Action.ActionFCParks.
    Action.ActionOther = Action.ActionFCOther.
  END.

  rates.b-floor = Action.ActionFloor.
  rates.b-parks = Action.ActionParks.
  rates.b-other = Action.ActionOther.

  rates.g-floor = Action.ActionFCFloor - rates.b-floor.
  rates.g-parks = Action.ActionFCParks - rates.b-parks.
  rates.g-other = Action.ActionFCOther - rates.b-other.

  rates.pro-rate = part-of-period( Action.ActionDate, growth-month.EndDate).

  RETURN "BASE".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-one) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION process-action-one Procedure 
FUNCTION process-action-one RETURNS CHARACTER
  ( /* no parameters */  ) :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  rates.b-floor = Action.ActionFCFloor.
  rates.b-parks = Action.ActionFCParks.
  rates.b-other = Action.ActionFCOther.

  rates.g-floor = Action.ActionFCFloor - Action.ActionFloor.
  rates.g-parks = Action.ActionFCParks - Action.ActionParks.
  rates.g-other = Action.ActionFCOther - Action.ActionOther.

  RETURN ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-process-action-rvw) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION process-action-rvw Procedure 
FUNCTION process-action-rvw RETURNS CHARACTER
  ( /* no parameters */  ) :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR total-rental AS DEC NO-UNDO.
DEF VAR total-fc AS DEC NO-UNDO.

DEF VAR apply-increase AS LOGI NO-UNDO INITIAL Yes.
  Action.ActionFCFloor = rates.m-floor * growth-month.floor .
  Action.ActionFCParks = rates.m-parks * growth-month.parks .
  Action.ActionFCOther = rates.m-other * growth-month.other .

  IF all-ratchets OR NOT AVAILABLE(TenancyLease) OR TenancyLease.HasRatchet THEN DO: 
    total-rental = rates.b-floor + rates.b-parks + rates.b-other .
    total-fc = Action.ActionFCFloor + Action.ActionFCParks + Action.ActionFCOther .

    IF total-fc <= total-rental THEN DO:
      RUN audit-line( base-font, "Forecast of " + STRING(total-fc) + " is not greater than current rental of " + STRING( total-rental ) + " so increase not applied." ).
      apply-increase = No.
    END.

  END.

  IF apply-increase THEN DO:
    ASSIGN
      Action.ActionFloor = Action.ActionFCFloor
      Action.ActionParks = Action.ActionFCParks
      Action.ActionOther = Action.ActionFCOther
      rates.g-floor = Action.ActionFloor - rates.b-floor
      rates.g-parks = Action.ActionParks - rates.b-parks
      rates.g-other = Action.ActionOther - rates.b-other.

      rates.pro-rate = part-of-period( Action.ActionDate, growth-month.EndDate).

  END.

  RETURN ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-to-annual) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION to-annual Procedure 
FUNCTION to-annual RETURNS DECIMAL
  ( INPUT freq-code AS CHAR, INPUT period-amount AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert period amount to annual figure
    Notes:  
------------------------------------------------------------------------------*/
  /* short circuit for most cases */
  IF freq-code = "MNTH" THEN RETURN (period-amount * 12.0).

  FIND FrequencyType WHERE FrequencyType.FrequencyCode = freq-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(FrequencyType) THEN RETURN (period-amount * 12.0). /* assume monthly! */

  IF FrequencyType.RepeatUnits BEGINS "M" THEN
    RETURN ((period-amount / DEC(FrequencyType.UnitCount)) * 12.0 ).

  RETURN ((period-amount / DEC(FrequencyType.UnitCount)) * 365.0 ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-to-month-rate) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION to-month-rate Procedure 
FUNCTION to-month-rate RETURNS DECIMAL
  ( INPUT raw-percent AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR pc AS DEC NO-UNDO.
DEF VAR line AS CHAR NO-UNDO.

  IF ( raw-percent MATCHES '*%*' ) THEN DO:
    raw-percent = ENTRY( 1, raw-percent, '%' ).
  END.
  pc = 1.0 + ( DEC(raw-percent) / 100.0 ).
  
/*   line = "Raw percent of: " + raw-percent + " converts to " + STRING( pc ). */
  pc = EXP( pc, one-twelfth ).

/*   line = line + " which is: " + STRING( pc ) + " each month".
  RUN audit-line( base-font, line ). */

  RETURN pc.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

