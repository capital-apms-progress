&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  AmTrust Rental Forecasting Run
  ------------------------------------------------------------------------*/
&SCOPED-DEFINE REPORT-ID "Amtrust Rental Run"

DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

DEF VAR preview   AS LOGI NO-UNDO    INIT No.
DEF VAR summary-level  AS CHAR NO-UNDO INIT "Detail".
DEF VAR property-1 AS INT NO-UNDO    INIT ?.
DEF VAR property-n AS INT NO-UNDO    INIT ?.
DEF VAR month-1 AS INT NO-UNDO       INIT ?.
DEF VAR month-n AS INT NO-UNDO       INIT ?.
DEF VAR monthly AS LOGI NO-UNDO      INIT No.
DEF VAR show-agents-fees AS LOGI NO-UNDO      INIT No.
DEF VAR show-expenses AS LOGI NO-UNDO      INIT No.
DEF VAR consolidated AS LOGI NO-UNDO INIT No.
DEF VAR forecast-start AS DATE NO-UNDO.
DEF VAR forecast-end AS DATE NO-UNDO.
DEF VAR first-year AS INT NO-UNDO.
DEF VAR first-month-start AS DATE NO-UNDO.
DEF VAR report-on AS CHAR NO-UNDO.
DEF VAR report-types AS CHAR NO-UNDO.
DEF VAR report-descs AS CHAR NO-UNDO.
RUN parse-parameters.
IF ERROR-STATUS:ERROR THEN RETURN.

DEF VAR mfmt AS CHAR NO-UNDO INITIAL "->>>,>>>,>>9".
DEF VAR pfmt AS CHAR NO-UNDO INITIAL "->>9.99999".
DEF VAR i AS INT NO-UNDO.

DEF VAR user-name AS CHAR NO-UNDO.
DEF VAR timeStamp AS CHAR FORMAT "X(44)" NO-UNDO.

DEF VAR base-font AS CHAR NO-UNDO   INITIAL "fixed,courier,cpi,18,lpi,9,normal".
DEF VAR header-font AS CHAR NO-UNDO   INITIAL "proportional,helvetica,point,10,lpi,7,bold".

{inc/ofc-this.i}
{inc/ofc-set.i "Default-Forecast-Property" "default-forecast-pcode"}
DEF VAR default-pcode AS INT NO-UNDO.
ASSIGN default-pcode = INT(default-forecast-pcode) NO-ERROR.
IF default-pcode < 1 THEN default-pcode = 99999.

DEF TEMP-TABLE rr-line NO-UNDO
        FIELD pcode AS INT
        FIELD et AS CHAR
        FIELD ec AS INT
        FIELD ac AS DEC
        FIELD lseq AS INT
        FIELD ltype AS CHAR
        FIELD years AS DEC EXTENT 15
        FIELD months AS DEC EXTENT 12
        INDEX XAK2rr-lines pcode ac ltype
        INDEX XPKrr-lines IS UNIQUE PRIMARY pcode et ec ac lseq
        INDEX XAK1rr-lines IS UNIQUE pcode et ec ac ltype .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-exclude-ltype) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD exclude-ltype Procedure 
FUNCTION exclude-ltype RETURNS LOGICAL
  ( INPUT ltype AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 17.6
         WIDTH              = 40.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/date.i}
{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
{inc/username.i "user-name"}
timeStamp = STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.

IF monthly THEN
  RUN pclrep-start( preview, "reset,landscape,tm,2,a4,lm,6," + base-font).
ELSE
  RUN pclrep-start( preview, "reset,portrait,tm,2,a4,lm,6," + base-font).
OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

RUN rent-report.

OUTPUT CLOSE.

RUN pclrep-finish.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-inst-page-footer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-footer Procedure 
PROCEDURE inst-page-footer :
/*------------------------------------------------------------------------------
  Purpose:  Print any page footer
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-inst-page-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-header Procedure 
PROCEDURE inst-page-header :
/*------------------------------------------------------------------------------
  Purpose:  Print page header for the rent report
------------------------------------------------------------------------------*/
DEF VAR tfmt AS CHAR NO-UNDO.
DEF VAR line AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.

  RUN pclrep-line( "univers,Point,7,bold,Proportional", TimeStamp).
  RUN pclrep-line( "univers,Point,11,bold,Proportional", SPC(20) + "AmTrust Forecasting Report - " + report-on).
  RUN pclrep-line( "", "" ).

  IF monthly THEN DO:
    tfmt = FILL(" ",LENGTH(mfmt) - 7).
    FIND Month WHERE Month.MonthCode = month-1 NO-LOCK.
    line = FILL(" ",5) + SUBSTRING( STRING( Month.StartDate, "99/99/9999"), 4) + tfmt.
    DO i = 2 TO 12:
      FIND NEXT Month NO-LOCK.
      line = line + SUBSTRING( STRING( Month.StartDate, "99/99/9999"), 4) + tfmt.
    END.
    line = line + "  Total" + tfmt.
    RUN pclrep-line( base-font + ",bold", line).
  END.
  ELSE DO:
    tfmt = STRING( "    9999    ", "X(" + STRING(LENGTH(mfmt)) + ")").
    RUN pclrep-line( base-font + ",bold", FILL(" ",4) + STRING(first-year, tfmt)
                        + STRING(first-year + 1, tfmt) + STRING(first-year + 2, tfmt)
                        + STRING(first-year + 3, tfmt) + STRING(first-year + 4, tfmt) ).
  END.

  RUN pclrep-line( "", "" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR token AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.

  /* ensure this is run as early as possible (its included from txtrep) */
  RUN get-bq-routine IN THIS-PROCEDURE.

{inc/showopts.i "report-options"}

  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).

    /* Preview and Property options are set for each run */    
    CASE ENTRY( 1, token ):
      WHEN "Preview" THEN               preview = Yes.
      WHEN "Monthly" THEN               monthly = Yes.
      WHEN "Consolidated" THEN          consolidated = Yes.
      WHEN "ShowAgentsFees" THEN        show-agents-fees = Yes.
      WHEN "SummariseTo" THEN           summary-level = ENTRY(2,token).

      WHEN "All" THEN ASSIGN
        property-1 = 0
        property-n = 999999.

      WHEN "Properties" THEN ASSIGN
        property-1 = INT(ENTRY(2,token))
        property-n = INT(ENTRY(3,token)).

      WHEN "ReportOn" THEN report-on = ENTRY(2,token).

      WHEN "Months" THEN ASSIGN
        month-1 = INT(ENTRY(2,token))
        month-n = INT(ENTRY(3,token)).

    END CASE.
  END.

  FIND Month WHERE Month.MonthCode = month-1 NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Month) THEN DO:
    MESSAGE "Invalid starting month for forecast"
            VIEW-AS ALERT-BOX ERROR.
    RETURN ERROR.
  END.
  forecast-start = Month.StartDate.
  first-year = Month.FinancialYearCode.
  first-month-start = Month.StartDate.

  FIND Month WHERE Month.MonthCode = month-n NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Month) THEN DO:
    MESSAGE "Invalid ending month for forecast"
            VIEW-AS ALERT-BOX ERROR.
    RETURN ERROR.
  END.
  forecast-end = Month.EndDate.

  CASE report-on:
    WHEN "Rents" THEN ASSIGN
      report-types = "BASE,GBAS,NEW,GNEW,VACT,GVAC,ONE"
      report-descs = "Base rental,Growth on Base Rental,New leases,Growth on new leases,Vacant costs,Growth on vacant costs,One-off income/costs".
    WHEN "Expenses" THEN ASSIGN
      report-types = "EXP,NREC,AEXP"
      report-descs = "Recoverable Expenses,Non-recoverable Expenses,Administration Expenses"
      show-expenses = Yes.
    WHEN "Recoveries" THEN ASSIGN
      report-types = "RCBS,RCNW"
      report-descs = "Base Recoveries,Recoveries on new leases".
    WHEN "NetExpenses" THEN ASSIGN
      report-types = "EXP,NREC,RCBS,RCNW,AEXP"
      report-descs = "Recoverable Expenses,Non-recoverable Expenses,Base Recoveries,Recoveries on new leases,Administration Expenses"
      show-expenses = Yes.
    OTHERWISE DO:
      MESSAGE "Unknown report option '" + report-on + "'"
                VIEW-AS ALERT-BOX ERROR
                TITLE "The Programmer Blew It! (tm)".
      RETURN "FAIL".
    END.
  END CASE.
  IF show-agents-fees THEN DO:
    report-types = report-types + ",AGNT".
    report-descs = report-descs + ",Agents Fees".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-rent-report) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rent-report Procedure 
PROCEDURE rent-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR years AS DEC NO-UNDO EXTENT 5 INITIAL 0.
DEF VAR etot AS DEC NO-UNDO EXTENT 5 INITIAL 0.
DEF VAR ptot AS DEC NO-UNDO EXTENT 5 INITIAL 0.
DEF VAR pcode AS INT NO-UNDO INITIAL 0.

DEF VAR last-type AS CHAR NO-UNDO INITIAL "".
DEF VAR last-entity AS CHAR NO-UNDO INITIAL "".
DEF VAR this-entity AS CHAR NO-UNDO.
DEF VAR last-account AS DEC NO-UNDO INITIAL ?.

DEF VAR first-year AS INT NO-UNDO.
FIND Month WHERE Month.MonthCode = month-1 NO-LOCK.
first-year = Month.FinancialYearCode.


  RUN rr-build-table.
  FOR EACH Property WHERE Active AND Property.PropertyCode >= property-1
                      AND Property.PropertyCode <= property-n NO-LOCK:
    pcode = Property.PropertyCode.
    IF consolidated THEN DO:
      pcode = default-pcode.

    END.
    ELSE DO:
      IF LOOKUP( summary-level, "Detail,LinePerLease,FourPerProperty") > 0 THEN
        RUN pclrep-line( header-font, "Property " + STRING(pcode) + " " + Property.Name ).

      /* tenants */
      FOR EACH Tenant NO-LOCK WHERE Tenant.EntityType = "P"
                      AND Tenant.EntityCode = pcode
                      AND CAN-FIND( FIRST rr-line WHERE rr-line.pcode = pcode
                                      AND rr-line.et = "T" AND rr-line.ec = Tenant.TenantCode):
        IF summary-level = "Detail" THEN
          RUN pclrep-line( header-font, "Tenant " + STRING(Tenant.TenantCode) + " " + Tenant.Name ).
        FOR EACH rr-line WHERE rr-line.pcode = pcode
                          AND rr-line.et = "T" 
                          AND rr-line.ec = Tenant.TenantCode:
          IF rr-line.ltype = 'AGNT' AND show-expenses THEN NEXT.
          RUN rr-line.
        END.
        IF summary-level = "Detail" THEN RUN pclrep-line(base-font,"").
      END.

      /* vacant spaces */
      FOR EACH RentalSpace NO-LOCK OF Property
                  WHERE CAN-FIND( FIRST rr-line WHERE rr-line.pcode = pcode
                                      AND rr-line.et = "R" AND rr-line.ec = RentalSpace.RentalSpaceCode):
        IF summary-level = "Detail" THEN RUN pclrep-line( header-font, "Vacant space " + STRING(RentalSpace.Level) + "/" + STRING(RentalSpace.LevelSequence) + " " + RentalSpace.Description ).
        FOR EACH rr-line WHERE rr-line.pcode = pcode
                                      AND rr-line.et = "R" 
                                      AND rr-line.ec = RentalSpace.RentalSpaceCode:
          IF rr-line.ltype = 'AGNT' AND show-expenses THEN NEXT.
          RUN rr-line.
        END.
        IF summary-level = "Detail" THEN RUN pclrep-line(base-font,"").
      END.
    END.

    /* property accounts (expenses) */
    last-account = ?.
    FOR EACH rr-line WHERE rr-line.pcode = pcode
                       AND (rr-line.et = "P"
                           OR (rr-line.ltype = 'AGNT' AND show-agents-fees AND show-expenses))
                     BY rr-line.pcode BY rr-line.ac:

      FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = rr-line.ac NO-LOCK NO-ERROR.
      IF summary-level = "Detail" AND last-account <> rr-line.ac THEN DO:
        IF last-account <> ? THEN RUN pclrep-line(base-font,"").
        RUN pclrep-line( header-font, "Account " + STRING(rr-line.ac,"9999.99") + " "
                            + (IF AVAILABLE(ChartOfAccount) THEN ChartOfAccount.Name ELSE "* * * Account not on file! * * *")).
        last-account = rr-line.ac.
      END.
      RUN rr-line.
    END.
    IF summary-level = "Detail" AND last-account <> ? THEN
      RUN pclrep-line(base-font,"").

    FOR EACH rr-line WHERE rr-line.pcode = pcode
                                    AND rr-line.et = "Z":
      RUN rr-line.
    END.
    IF LOOKUP( summary-level, "Detail,LinePerLease") > 0 THEN
      RUN pclrep-down-by(2).
    ELSE IF summary-level = "FourPerProperty" THEN
      RUN pclrep-line(base-font,"").

    IF consolidated THEN LEAVE.
  END.

  IF property-1 <> property-n THEN FOR EACH rr-line WHERE rr-line.pcode = 999999:
    RUN rr-line.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-rr-build-table) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rr-build-table Procedure 
PROCEDURE rr-build-table :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR j AS INT NO-UNDO.

DEF VAR pcode AS INT NO-UNDO.
DEF VAR ecode AS INT NO-UNDO.
DEF VAR entity-type AS CHAR NO-UNDO.
DEF VAR my-amount AS DEC NO-UNDO.

  FOR EACH Property WHERE Active AND Property.PropertyCode >= property-1
                      AND Property.PropertyCode <= property-n NO-LOCK:
    FOR EACH PropForecast OF Property NO-LOCK:
      IF exclude-ltype(PropForecast.PropForecastType) THEN NEXT.

      FIND Month WHERE Month.MonthCode = PropForecast.MonthCode.
      i = (Month.FinancialYear - first-year) + 1.
      j = diff-months( first-month-start, Month.StartDate) + 1.
      IF NOT( i > 0 ) THEN NEXT.
      pcode = PropForecast.PropertyCode .
      IF consolidated THEN pcode = default-pcode.

      my-amount = PropForecast.Amount .
      IF PropForecast.PropForecastType = "AGNT" AND show-expenses AND show-agents-fees THEN DO:
        entity-type = "P".
        ecode = pcode.
      END.
      ELSE IF consolidated AND PropForecast.EntityType = "P" THEN DO:
        entity-type = "P".
        ecode = pcode.
      END.
      ELSE DO:
        entity-type = PropForecast.EntityType .
        ecode = PropForecast.EntityCode.
      END.

      IF PropForecast.PropForecastType = "AGNT" AND show-agents-fees AND report-on = "Rents" THEN DO:
        my-amount = - my-amount.
      END.

      FIND rr-line WHERE rr-line.pcode = pcode
                     AND rr-line.et    = entity-type
                     AND rr-line.ec    = ecode
                     AND rr-line.ltype = PropForecast.PropForecastType
                     AND rr-line.ac    = INT(PropForecast.AccountCode) NO-ERROR.

      IF NOT AVAILABLE(rr-line) THEN DO:
        CREATE rr-line.
        rr-line.pcode = pcode.
        rr-line.et    = entity-type.
        rr-line.ec    = ecode.
        rr-line.ltype = PropForecast.PropForecastType.
        rr-line.ac    = INT(PropForecast.AccountCode).
        rr-line.lseq = LOOKUP( PropForecast.PropForecastType, report-types).
        IF rr-line.lseq = 0 THEN DO:
          RUN pclrep-line( base-font, "Couldn't locate '" + PropForecast.PropForecastType + "' in line type list.").
        END.
      END.
      rr-line.years[i] = rr-line.years[i] + my-amount .
      IF j > 0 AND j <= 12 THEN
        rr-line.months[j] = rr-line.months[j] + my-amount .
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-rr-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rr-line Procedure 
PROCEDURE rr-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER rr-total FOR rr-line.

DEF VAR t2 AS CHAR NO-UNDO.
DEF VAR is-total-line AS LOGI NO-UNDO INITIAL No.
DEF VAR i AS INT NO-UNDO.
DEF VAR thisline-total AS DEC NO-UNDO INITIAL 0.0 .

  IF rr-line.ltype = "TOTAL" THEN DO: /* entity total line */
    is-total-line = Yes.
    IF summary-level = "LinePerProperty" THEN
      is-total-line = (rr-line.pcode > 99999).
    ELSE IF summary-level = "FourPerProperty" THEN
      is-total-line = (rr-line.et = "Z").
    ELSE IF summary-level = "LinePerLease" THEN
      is-total-line = (rr-line.et = "Z").
    ELSE
      is-total-line = Yes.

    IF is-total-line AND monthly THEN
      RUN pclrep-line( base-font, FILL( " " + FILL( "-", LENGTH(mfmt) - 1), 12)).
    ELSE IF is-total-line THEN
      RUN pclrep-line( base-font, FILL( " " + FILL( "-", LENGTH(mfmt) - 1), 5)).
    
    IF monthly THEN DO i = 1 TO 12:
      thisline-total = thisline-total + rr-line.months[i].
    END.
  END.
  ELSE DO:
    /* find and add to total of this group */
    FIND rr-total WHERE rr-total.pcode = rr-line.pcode
                    AND rr-total.et = rr-line.et
                    AND rr-total.ec = rr-line.ec
                    AND rr-total.ltype = "TOTAL"
                    AND (rr-total.ac = rr-line.ac OR report-on = "Rents") NO-ERROR.
    IF NOT AVAILABLE(rr-total) THEN DO:
      CREATE rr-total.
      rr-total.pcode = rr-line.pcode.
      rr-total.et = rr-line.et.
      rr-total.ec = rr-line.ec.
      rr-total.ac = rr-line.ac.
      rr-total.ltype = "TOTAL".
      rr-total.lseq = 9.
    END.
    DO i = 1 TO 5:
      rr-total.years[i] = rr-total.years[i] + rr-line.years[i].
    END.
    IF monthly THEN DO i = 1 TO 12:
      rr-total.months[i] = rr-total.months[i] + rr-line.months[i].
    END.

    IF rr-line.pcode <= 99999 THEN DO:
      /* find and add to total at level above */
      FIND rr-total WHERE rr-total.pcode = (IF rr-line.et = "Z" THEN 999999 ELSE rr-line.pcode)
                      AND rr-total.et = "Z"
                      AND rr-total.ec = 999999
                      AND rr-total.ltype = rr-line.ltype NO-ERROR.
      IF NOT AVAILABLE(rr-total) THEN DO:
        CREATE rr-total.
        rr-total.pcode = (IF rr-line.et = "Z" THEN 999999 ELSE rr-line.pcode).
        rr-total.et = "Z".
        rr-total.ec = 999999.
        rr-total.ltype = rr-line.ltype.
        rr-total.lseq = rr-line.lseq.
      END.
      DO i = 1 TO 5:
        rr-total.years[i] = rr-total.years[i] + rr-line.years[i].
      END.
      IF monthly THEN DO i = 1 TO 12:
        rr-total.months[i] = rr-total.months[i] + rr-line.months[i].
      END.
    END.
  END.

  IF LOOKUP(summary-level,"LinePerLease,LinePerProperty") > 0 AND rr-line.ltype <> "TOTAL" THEN RETURN.
  IF LOOKUP(summary-level,"FourPerProperty,LinePerProperty") > 0 AND rr-line.et <> "Z" THEN RETURN.

  IF rr-line.lseq = 9 THEN
    t2 = "Total".
  ELSE
    t2 = ENTRY( rr-line.lseq, report-descs).

  IF rr-line.et = "P" AND AVAILABLE(ChartOfAccount) THEN
    t2 = STRING(rr-line.ltype, "X(4)") + " "
       + STRING(ChartOfAccount.AccountCode, "9999.99") + " "
       + ChartOfAccount.Name.

  IF summary-level = "LinePerLease" THEN DO:
    IF rr-line.et = "T" THEN
      t2 = "T" + STRING(Tenant.TenantCode,"99999") + " " + Tenant.Name.
    ELSE IF rr-line.et = "R" THEN
      t2 = "Vacant: " + RentalSpace.Description.
  END.
  ELSE IF summary-level = "LinePerProperty" THEN DO:
    IF rr-line.pcode > 99999 THEN
      t2 = "Grand totals".
    ELSE IF rr-line.et = "Z" AND rr-line.pcode = default-pcode THEN
      t2 = "Consolidated Total".
    ELSE IF rr-line.et = "Z" THEN
      t2 = "P" + STRING(Property.PropertyCode,"99999") + " " + Property.Name.
  END.

  IF monthly THEN DO:
    RUN pclrep-line( base-font, STRING( rr-line.months[1], mfmt)
                              + STRING( rr-line.months[2], mfmt)
                              + STRING( rr-line.months[3], mfmt)
                              + STRING( rr-line.months[4], mfmt)
                              + STRING( rr-line.months[5], mfmt)
                              + STRING( rr-line.months[6], mfmt)
                              + STRING( rr-line.months[7], mfmt)
                              + STRING( rr-line.months[8], mfmt)
                              + STRING( rr-line.months[9], mfmt)
                              + STRING( rr-line.months[10], mfmt)
                              + STRING( rr-line.months[11], mfmt)
                              + STRING( rr-line.months[12], mfmt)
                              + STRING( thisline-total, mfmt)
                              + "  " + t2 ).
/*                              STRING(t2,"X(40)")
                              + " " + STRING(rr-line.pcode)
                              + " " + rr-line.et
                              + " " + rr-line.ltype ). */
  END.
  ELSE DO:
    RUN pclrep-line( base-font, STRING( rr-line.years[1], mfmt)
                              + STRING( rr-line.years[2], mfmt)
                              + STRING( rr-line.years[3], mfmt)
                              + STRING( rr-line.years[4], mfmt)
                              + STRING( rr-line.years[5], mfmt)
                              + "  " + t2 ).
/*                              STRING(t2,"X(40)")
                              + " " + STRING(rr-line.pcode)
                              + " " + rr-line.et
                              + " " + rr-line.ltype ). */
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-exclude-ltype) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION exclude-ltype Procedure 
FUNCTION exclude-ltype RETURNS LOGICAL
  ( INPUT ltype AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Decide whether we're including this line in the report
------------------------------------------------------------------------------*/

  RETURN (LOOKUP( ltype, report-types) = 0).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

