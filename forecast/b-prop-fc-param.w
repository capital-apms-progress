&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------

  File:  

  Description:

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO.

DEF WORK-TABLE SavePFP NO-UNDO LIKE PropForecastParam.
CREATE SavePFP.
FIND Month WHERE Month.StartDate <= TODAY AND Month.EndDate >= TODAY NO-LOCK NO-ERROR.
IF AVAILABLE(Month) THEN DO:
  SavePFP.MonthCode = Month.MonthCode.
  FIND FIRST FinancialYear WHERE FinancialYear.FinancialYearCode > Month.FinancialYearCode NO-LOCK NO-ERROR.
  IF AVAILABLE(FinancialYear) THEN
    FIND FIRST Month OF FinancialYear NO-LOCK NO-ERROR.
  IF AVAILABLE(Month) THEN SavePFP.MonthCode = Month.MonthCode.
END.

DEF VAR line-modified AS LOGI NO-UNDO INITIAL No.
DEF VAR last-line  AS LOGI NO-UNDO INITIAL No.
DEF VAR new-record AS LOGI NO-UNDO INITIAL No.
DEF VAR entry-date AS DATE NO-UNDO.
DEF BUFFER tmp_Month2 FOR Month.

DEFINE TEMP-TABLE tmp_Month NO-UNDO LIKE Month
            FIELD EnteredDate AS DATE FORMAT "99/99/9999" INITIAL TODAY.

ON FIND OF PropForecastParam DO:
  FIND tmp_Month2 WHERE tmp_Month2.MonthCode = PropForecastParam.MonthCode NO-LOCK NO-ERROR.
  IF AVAILABLE(tmp_Month2) THEN
    entry-date = tmp_Month2.StartDate.
  ELSE
    entry-date = DATE( MONTH(entry-date), 1, YEAR(entry-date)).

  FIND tmp_Month WHERE tmp_Month.MonthCode = PropForecastParam.MonthCode NO-ERROR.
  IF NOT AVAILABLE(tmp_Month) THEN FIND FIRST tmp_Month.
  tmp_Month.EnteredDate = entry-date.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME f_browser
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES PropForecastParam

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table PropForecastParam.PropertyCode tmp_Month.EnteredDate PropForecastParam.ParameterID PropForecastParam.ParameterValue   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table PropForecastParam.PropertyCode ~
tmp_Month.EnteredDate ~
PropForecastParam.ParameterID ~
PropForecastParam.ParameterValue   
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}PropertyCode ~{&FP2}PropertyCode ~{&FP3}~
 ~{&FP1}EnteredDate ~{&FP2}EnteredDate ~{&FP3}~
 ~{&FP1}ParameterID ~{&FP2}ParameterID ~{&FP3}~
 ~{&FP1}ParameterValue ~{&FP2}ParameterValue ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table PropForecastParam tmp_Month
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table PropForecastParam
&Scoped-define SECOND-ENABLED-TABLE-IN-QUERY-br_table tmp_Month
&Scoped-define SELF-NAME br_table
&Scoped-define OPEN-QUERY-br_table OPEN QUERY {&SELF-NAME} FOR EACH PropForecastParam WHERE ~{&KEY-PHRASE} NO-LOCK     ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table PropForecastParam
&Scoped-define FIRST-TABLE-IN-QUERY-br_table PropForecastParam


/* Definitions for FRAME f_browser                                      */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table btn_add-line 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
MonthCode|y|y|TTPL.PropForecastParam.MonthCode
PropertyCode|y|y|TTPL.PropForecastParam.PropertyCode
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "MonthCode,PropertyCode",
     Keys-Supplied = "MonthCode,PropertyCode"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add-line 
     LABEL "Button 1" 
     SIZE 15 BY 1.15.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      PropForecastParam SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _FREEFORM
  QUERY br_table NO-LOCK DISPLAY
      PropForecastParam.PropertyCode COLUMN-LABEL "Property" FORMAT "99999":C
      tmp_Month.EnteredDate COLUMN-LABEL "Starting On" WIDTH 9
      PropForecastParam.ParameterID FORMAT "X(50)" WIDTH 14
      PropForecastParam.ParameterValue FORMAT "X(1024)" WIDTH 45
  ENABLE
      PropForecastParam.PropertyCode
      tmp_Month.EnteredDate HELP "The first day of the parameters effects"
      PropForecastParam.ParameterID
      PropForecastParam.ParameterValue
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 82.29 BY 14.2
         BGCOLOR 15 FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f_browser
     br_table AT ROW 1 COL 1
     btn_add-line AT ROW 2.4 COL 35.29
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 16 FGCOLOR 0 FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 14.35
         WIDTH              = 106.86.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME f_browser
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 f_browser */
ASSIGN 
       FRAME f_browser:SCROLLABLE       = FALSE
       FRAME f_browser:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH PropForecastParam WHERE ~{&KEY-PHRASE} NO-LOCK
    ~{&SORTBY-PHRASE}.
     _END_FREEFORM
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _TblOptList       = ", OUTER,"
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f_browser
/* Query rebuild information for FRAME f_browser
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME f_browser */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME f_browser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f_browser B-table-Win
ON RETURN OF FRAME f_browser
ANYWHERE DO:
  IF LOOKUP( FOCUS:TYPE, 'FILL-IN,COMBO-BOX,EDITOR' ) <> 0 OR
     (FOCUS:TYPE = 'BUTTON':U AND FOCUS:DYNAMIC) THEN
  DO:
    APPLY 'TAB':U TO FOCUS.
    RETURN NO-APPLY.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ENTRY OF br_table IN FRAME f_browser
DO:

DEF VAR n-results AS INT NO-UNDO.
DEF VAR s-focused AS LOGI NO-UNDO INIT ?.
DEF VAR n-row AS LOGI NO-UNDO INIT ?.
  IF LOOKUP( LAST-EVENT:LABEL, 'ENTER,TAB,MOUSE-SELECT-UP':U ) = 0 THEN RETURN.

  n-row = {&BROWSE-NAME}:NEW-ROW IN FRAME {&FRAME-NAME}.

  n-results = NUM-RESULTS("{&BROWSE-NAME}":U).
  IF NOT ( n-results = ? OR n-results = 0 ) THEN
    s-focused = {&BROWSE-NAME}:SELECT-FOCUSED-ROW().

  line-modified = No.
  IF n-row THEN DO:
    /* Set the default screen-values */
    RUN set-default-values.
    line-modified = Yes.
  END.
  ELSE IF NOT AVAILABLE PropForecastParam THEN RETURN.

  IF TRIM(PropForecastParam.ParameterID:SCREEN-VALUE IN BROWSE {&BROWSE-NAME}) = "" THEN
    APPLY 'ENTRY':U TO PropForecastParam.ParameterID IN BROWSE {&BROWSE-NAME}.
  ELSE
    APPLY 'ENTRY':U TO PropForecastParam.PropertyCode IN BROWSE {&BROWSE-NAME}.

  RETURN NO-APPLY.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME f_browser
DO:
  IF {&BROWSE-NAME}:NEW-ROW IN FRAME {&FRAME-NAME} THEN DO:
    RUN set-default-values.
    line-modified = Yes.
  END.
  ELSE
    line-modified = No.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME f_browser
OR 'CURSOR-DOWN':U OF {&SELF-NAME} 
OR 'CURSOR-UP':U   OF {&SELF-NAME}
OR 'ENTER':U       OF PropForecastParam.ParameterValue IN BROWSE {&SELF-NAME}
   ANYWHERE DO:

  IF line-modified THEN DO:
    RUN verify-line. /* and update it */
    IF RETURN-VALUE = "FAIL" THEN RETURN NO-APPLY.
  END.

  RUN save-these-values.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_add-line
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add-line B-table-Win
ON CHOOSE OF btn_add-line IN FRAME f_browser /* Button 1 */
DO:
  RUN add-new-line.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

/* create the tmp_Month table from the Month table */
FOR EACH Month NO-LOCK:
  CREATE tmp_Month.
  BUFFER-COPY Month TO tmp_Month
        ASSIGN tmp_Month.EnteredDate = Month.StartDate.
END.
CREATE tmp_Month.

HIDE btn_add-line IN FRAME {&FRAME-NAME}.

&Scoped-define SELF-NAME PropForecastParam.PropertyCode
ON LEAVE OF {&SELF-NAME} IN BROWSE {&BROWSE-NAME}
DO:
  IF line-modified THEN RETURN.
  IF INPUT BROWSE {&BROWSE-NAME} {&SELF-NAME} <> {&SELF-NAME} THEN
    line-modified = Yes.
END.


&SCOP SELF-NAME tmp_Month.EnteredDate
ON LEAVE OF {&SELF-NAME} IN BROWSE {&BROWSE-NAME} DO:
  IF line-modified THEN RETURN.
  IF (INPUT BROWSE {&BROWSE-NAME} {&SELF-NAME}) <> {&SELF-NAME} THEN
    line-modified = Yes.
END.

&SCOP SELF-NAME PropForecastParam.ParameterID
ON LEAVE OF {&SELF-NAME} IN BROWSE {&BROWSE-NAME} DO:
  IF line-modified THEN RETURN.
  IF (INPUT BROWSE {&BROWSE-NAME} {&SELF-NAME}) <> {&SELF-NAME} THEN
    line-modified = Yes.
END.

&SCOP SELF-NAME PropForecastParam.ParameterValue
ON LEAVE OF {&SELF-NAME} IN BROWSE {&BROWSE-NAME} DO:
  IF line-modified THEN RETURN.
  IF (INPUT BROWSE {&BROWSE-NAME} {&SELF-NAME}) <> {&SELF-NAME} THEN
    line-modified = Yes.
END.

&UNDEFINE SELF-NAME

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-new-line B-table-Win 
PROCEDURE add-new-line :
/*------------------------------------------------------------------------------
  Purpose:     Inserts a new line at the end of the browse
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF NUM-RESULTS("{&BROWSE-NAME}":U) = ? OR
     NUM-RESULTS("{&BROWSE-NAME}":U) = 0 THEN
  DO:
    DO TRANSACTION:
      FIND FIRST PropForecastParam WHERE NOT(PropForecastParam.PropertyCode > 0)
                                      OR NOT(PropForecastParam.MonthCode > 0)
                                      EXCLUSIVE-LOCK NO-ERROR.
      IF NOT AVAILABLE(PropForecastParam) THEN CREATE PropForecastParam.
      RUN assign-default-values.
    END.
    RUN local-open-query-cases.
  END.
  ELSE DO:
    IF {&BROWSE-NAME}:INSERT-ROW("AFTER") THEN .
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.

  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'MonthCode':U THEN DO:
       &Scope KEY-PHRASE PropForecastParam.MonthCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* MonthCode */
    WHEN 'PropertyCode':U THEN DO:
       &Scope KEY-PHRASE PropForecastParam.PropertyCode eq INTEGER(key-value)
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* PropertyCode */
    OTHERWISE DO:
       &Scope KEY-PHRASE TRUE
       {&OPEN-QUERY-{&BROWSE-NAME}}
    END. /* OTHERWISE...*/
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-default-values B-table-Win 
PROCEDURE assign-default-values :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER tmp_Prop FOR Property.
DEF BUFFER tmp_month2 FOR Month.
  IF NOT AVAILABLE(PropForecastParam) THEN RETURN.
DO WITH FRAME {&FRAME-NAME}:
  PropForecastParam.ParameterID = SavePFP.ParameterID.
  PropForecastParam.ParameterValue  = SavePFP.ParameterValue.
  CASE key-name:
    WHEN 'PropertyCode' THEN DO:
      PropForecastParam.PropertyCode = INT(key-value).
      FIND FIRST tmp_month2 WHERE tmp_Month2.MonthCode > SavePFP.MonthCode NO-LOCK NO-ERROR.
      PropForecastParam.MonthCode = tmp_Month2.MonthCode.
    END.
    WHEN 'MonthCode' THEN DO:
      FIND tmp_month2 WHERE tmp_Month2.MonthCode = INT(key-value) NO-LOCK NO-ERROR.
      PropForecastParam.MonthCode = tmp_Month2.MonthCode.
      FIND FIRST tmp_Prop WHERE tmp_Prop.Active AND tmp_Prop.PropertyCode > SavePFP.PropertyCode NO-LOCK NO-ERROR.
      PropForecastParam.PropertyCode  = (IF AVAILABLE(tmp_Prop) THEN tmp_Prop.PropertyCode ELSE 0).
    END.
    WHEN 'FinancialYearCode' THEN DO:
      FIND FIRST tmp_month2 WHERE tmp_Month2.MonthCode > SavePFP.MonthCode NO-LOCK NO-ERROR.
      PropForecastParam.MonthCode = tmp_Month2.MonthCode.
      FIND FIRST tmp_Prop WHERE tmp_Prop.Active AND tmp_Prop.PropertyCode > SavePFP.PropertyCode NO-LOCK NO-ERROR.
      PropForecastParam.PropertyCode  = (IF AVAILABLE(tmp_Prop) THEN tmp_Prop.PropertyCode ELSE 0).
    END.
  END CASE.
  FIND tmp_Month OF PropForecastParam NO-ERROR.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assign-line B-table-Win 
PROCEDURE assign-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER tmp_tmp FOR tmp_Month.
DEF BUFFER tmp_Month2 FOR Month.
DEF VAR in-date AS DATE NO-UNDO.

  /* Code placed here will execute PRIOR to standard behavior. */
  IF NOT AVAILABLE PropForecastParam THEN RETURN.
  in-date = INPUT BROWSE {&BROWSE-NAME} tmp_Month.EnteredDate .

DO TRANSACTION WITH FRAME {&FRAME-NAME}:

  IF NOT NEW PropForecastParam THEN FIND CURRENT PropForecastParam EXCLUSIVE-LOCK.

  ASSIGN BROWSE {&BROWSE-NAME}
    PropForecastParam.PropertyCode
    PropForecastParam.ParameterID
    PropForecastParam.ParameterValue.

  FIND tmp_Month2 WHERE tmp_Month2.StartDate <= in-date
                    AND tmp_Month2.EndDate >= in-date NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(tmp_Month2) THEN DO:
    MESSAGE "No financial period (Month) found for the date" in-date
            VIEW-AS ALERT-BOX ERROR.
    RETURN ERROR.
  END.
  PropForecastParam.MonthCode = tmp_Month2.MonthCode.
  tmp_Month.EnteredDate = tmp_Month2.StartDate.
  FIND CURRENT PropForecastParam NO-LOCK.
END.

RUN display-line.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f_browser.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE display-line B-table-Win 
PROCEDURE display-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE PropForecastParam THEN RETURN.

  IF NUM-RESULTS("{&BROWSE-NAME}":U) > 1 THEN
    DISPLAY {&ENABLED-FIELDS-IN-QUERY-{&BROWSE-NAME}} WITH BROWSE {&BROWSE-NAME}.
  ELSE
    RUN local-open-query-cases.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-cancel-record B-table-Win 
PROCEDURE local-cancel-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  IF new-record THEN DO TRANSACTION WITH FRAME {&FRAME-NAME}:
    FIND CURRENT PropForecastParam EXCLUSIVE-LOCK NO-ERROR.
    IF AVAILABLE(PropForecastParam) THEN DELETE PropForecastParam.

    IF {&BROWSE-NAME}:DELETE-CURRENT-ROW() THEN .
    new-record = No.
  END.
  ELSE DO TRANSACTION WITH FRAME {&FRAME-NAME}:
    FIND CURRENT PropForecastParam EXCLUSIVE-LOCK NO-ERROR.
    IF AVAILABLE(PropForecastParam) THEN
      BUFFER-COPY SavePFP TO PropForecastParam.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-record B-table-Win 
PROCEDURE local-display-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  RUN display-line.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query-cases B-table-Win 
PROCEDURE local-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR m-1 AS INT NO-UNDO.
DEF VAR m-2 AS INT NO-UNDO.

  IF key-name = 'MonthCode':U THEN DO:
    /* Show everything from that month on */
     &Scope KEY-PHRASE PropForecastParam.MonthCode >= INTEGER(key-value)
     {&OPEN-QUERY-{&BROWSE-NAME}}
     RETURN.
  END.
  ELSE IF key-name = 'FinancialYearCode':U THEN DO:
    /* Show everything during that financial year */
     FIND FIRST Month WHERE Month.FinancialYearCode = INT(key-value) NO-LOCK.
     FIND LAST Month WHERE Month.FinancialYearCode = INT(key-value) NO-LOCK.
     &Scope KEY-PHRASE PropForecastParam.MonthCode >= m-1 AND PropForecastParam.MonthCode <= m-2
     {&OPEN-QUERY-{&BROWSE-NAME}}
     RETURN.
  END.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'open-query-cases':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-these-values B-table-Win 
PROCEDURE save-these-values :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF AVAILABLE(PropForecastParam) THEN DO:
    BUFFER-COPY PropForecastParam TO SavePFP.
    new-record = No.
  END.
  ELSE
    new-record = Yes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "MonthCode" "PropForecastParam" "MonthCode"}
  {src/adm/template/sndkycas.i "PropertyCode" "PropForecastParam" "PropertyCode"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "PropForecastParam"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-default-values B-table-Win 
PROCEDURE set-default-values :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER tmp_Prop FOR Property.
DEF BUFFER tmp_month2 FOR Month.
DO WITH FRAME {&FRAME-NAME}:
  PropForecastParam.ParameterID:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = SavePFP.ParameterID.
  PropForecastParam.ParameterValue:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = SavePFP.ParameterValue.
  CASE key-name:
    WHEN 'PropertyCode' THEN DO:
      PropForecastParam.PropertyCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = key-value.
      FIND FIRST tmp_month2 WHERE tmp_Month2.MonthCode > SavePFP.MonthCode NO-LOCK NO-ERROR.
      tmp_Month.EnteredDate:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( tmp_Month2.StartDate, tmp_Month.EnteredDate:FORMAT IN BROWSE {&BROWSE-NAME}).
    END.
    WHEN 'MonthCode' THEN DO:
      FIND tmp_month2 WHERE tmp_Month2.MonthCode = INT(key-value) NO-LOCK NO-ERROR.
      tmp_Month.EnteredDate:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( tmp_Month2.StartDate, tmp_Month.EnteredDate:FORMAT IN BROWSE {&BROWSE-NAME}).
      FIND FIRST tmp_Prop WHERE tmp_Prop.Active AND tmp_Prop.PropertyCode > SavePFP.PropertyCode NO-LOCK NO-ERROR.
      PropForecastParam.PropertyCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( (IF AVAILABLE(tmp_Prop) THEN tmp_Prop.PropertyCode ELSE ?), PropForecastParam.PropertyCode:FORMAT IN BROWSE {&BROWSE-NAME}).
    END.
    WHEN 'FinancialYearCode' THEN DO:
      FIND FIRST tmp_month2 WHERE tmp_Month2.MonthCode > SavePFP.MonthCode NO-LOCK NO-ERROR.
      tmp_Month.EnteredDate:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( tmp_Month2.StartDate, tmp_Month.EnteredDate:FORMAT IN BROWSE {&BROWSE-NAME}).
      FIND FIRST tmp_Prop WHERE tmp_Prop.Active AND tmp_Prop.PropertyCode > SavePFP.PropertyCode NO-LOCK NO-ERROR.
      PropForecastParam.PropertyCode:SCREEN-VALUE IN BROWSE {&BROWSE-NAME} = STRING( (IF AVAILABLE(tmp_Prop) THEN tmp_Prop.PropertyCode ELSE ?), PropForecastParam.PropertyCode:FORMAT IN BROWSE {&BROWSE-NAME}).
    END.
  END CASE.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-line B-table-Win 
PROCEDURE update-line :
/*------------------------------------------------------------------------------
  Purpose:     Update the current invoice line.
------------------------------------------------------------------------------*/

  /* Create a new line if needed and update the details */
  IF {&BROWSE-NAME}:NEW-ROW IN FRAME {&FRAME-NAME} THEN DO:
    FIND FIRST PropForecastParam WHERE NOT(PropForecastParam.PropertyCode > 0)
                                    OR NOT(PropForecastParam.MonthCode > 0)
                                    EXCLUSIVE-LOCK NO-ERROR.
    IF NOT AVAILABLE(PropForecastParam) THEN CREATE PropForecastParam.
    IF {&BROWSE-NAME}:CREATE-RESULT-LIST-ENTRY() THEN.
    RUN assign-line.
  END.
  ELSE
    RUN assign-line.

  line-modified = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  key-name = new-key-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value B-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-value AS CHAR NO-UNDO.
  key-value = new-key-value.

  IF key-name = "PropertyCode" THEN
    SavePFP.PropertyCode = INT(key-value).
  ELSE IF key-name = "MonthCode" THEN
    SavePFP.MonthCode = INT(key-value).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-line B-table-Win 
PROCEDURE verify-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR err-field AS HANDLE NO-UNDO.

  err-field = WIDGET-HANDLE( RETURN-VALUE ).

  RUN update-line.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


