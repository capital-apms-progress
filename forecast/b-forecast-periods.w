&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW

/* Temp-Table and Buffer definitions                                    */
DEFINE TEMP-TABLE Forecast NO-UNDO LIKE TTPL.CashFlow
       FIELD FromDate AS DATE
       FIELD Periods AS DEC EXTENT 12.


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*------------------------------------------------------------------------
  File:  
  Description: 

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */


DEF VAR forecast-start AS DATE NO-UNDO.
DEF VAR period-size AS CHAR NO-UNDO.
DEF VAR parent-type AS CHAR NO-UNDO INITIAL "".
DEF VAR parent-code AS INT NO-UNDO INITIAL 0.
DEF VAR parent-property AS INT NO-UNDO INITIAL 0.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Forecast

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table Forecast.Description Forecast.Periods[1] Forecast.Periods[2] Forecast.Periods[3] Forecast.Periods[4] Forecast.Periods[5] Forecast.Periods[6] Forecast.Periods[7] Forecast.Periods[8] Forecast.Periods[9] Forecast.Periods[10] Forecast.Periods[11] Forecast.Periods[12]   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table Forecast.Periods[1]  Forecast.Periods[2]  Forecast.Periods[3]  Forecast.Periods[4]  Forecast.Periods[5]  Forecast.Periods[6]  Forecast.Periods[7]  Forecast.Periods[8]  Forecast.Periods[9]  Forecast.Periods[10]  Forecast.Periods[11]  Forecast.Periods[12]   
&Scoped-define FIELD-PAIRS-IN-QUERY-br_table~
 ~{&FP1}Periods[1] ~{&FP2}Periods[1] ~{&FP3}~
 ~{&FP1}Periods[2] ~{&FP2}Periods[2] ~{&FP3}~
 ~{&FP1}Periods[3] ~{&FP2}Periods[3] ~{&FP3}~
 ~{&FP1}Periods[4] ~{&FP2}Periods[4] ~{&FP3}~
 ~{&FP1}Periods[5] ~{&FP2}Periods[5] ~{&FP3}~
 ~{&FP1}Periods[6] ~{&FP2}Periods[6] ~{&FP3}~
 ~{&FP1}Periods[7] ~{&FP2}Periods[7] ~{&FP3}~
 ~{&FP1}Periods[8] ~{&FP2}Periods[8] ~{&FP3}~
 ~{&FP1}Periods[9] ~{&FP2}Periods[9] ~{&FP3}~
 ~{&FP1}Periods[10] ~{&FP2}Periods[10] ~{&FP3}~
 ~{&FP1}Periods[11] ~{&FP2}Periods[11] ~{&FP3}~
 ~{&FP1}Periods[12] ~{&FP2}Periods[12] ~{&FP3}
&Scoped-define ENABLED-TABLES-IN-QUERY-br_table Forecast
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br_table Forecast
&Scoped-define SELF-NAME br_table
&Scoped-define OPEN-QUERY-br_table OPEN QUERY {&SELF-NAME} FOR EACH Forecast WHERE ~{&KEY-PHRASE} NO-LOCK     ~{&SORTBY-PHRASE}.
&Scoped-define TABLES-IN-QUERY-br_table Forecast
&Scoped-define FIRST-TABLE-IN-QUERY-br_table Forecast


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS>
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS> 
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE> 
<FILTER-ATTRIBUTES>
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      Forecast SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _FREEFORM
  QUERY br_table NO-LOCK DISPLAY
      Forecast.Description
  Forecast.Periods[1]
  Forecast.Periods[2]
  Forecast.Periods[3]
  Forecast.Periods[4]
  Forecast.Periods[5]
  Forecast.Periods[6]
  Forecast.Periods[7]
  Forecast.Periods[8]
  Forecast.Periods[9]
  Forecast.Periods[10]
  Forecast.Periods[11]
  Forecast.Periods[12]
ENABLE
  Forecast.Periods[1]
  Forecast.Periods[2]
  Forecast.Periods[3]
  Forecast.Periods[4]
  Forecast.Periods[5]
  Forecast.Periods[6]
  Forecast.Periods[7]
  Forecast.Periods[8]
  Forecast.Periods[9]
  Forecast.Periods[10]
  Forecast.Periods[11]
  Forecast.Periods[12]
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 73.14 BY 14.8
         FONT 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 16 FGCOLOR 0 FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
   Temp-Tables and Buffers:
      TABLE: Forecast T "?" NO-UNDO TTPL CashFlow
      ADDITIONAL-FIELDS:
          FIELD FromDate AS DATE
          FIELD Periods AS DEC EXTENT 12
      END-FIELDS.
   END-TABLES.
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 14.9
         WIDTH              = 73.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:NUM-LOCKED-COLUMNS IN FRAME F-Main = 1.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH Forecast WHERE ~{&KEY-PHRASE} NO-LOCK
    ~{&SORTBY-PHRASE}.
     _END_FREEFORM
     _Options          = "NO-LOCK KEY-PHRASE SORTBY-PHRASE"
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/browser.i}
{inc/method/m-drlvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-current-forecast B-table-Win 
PROCEDURE clear-current-forecast :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FOR EACH Forecast: DELETE Forecast. END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE forecast-for-area B-table-Win 
PROCEDURE forecast-for-area :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-records AS LOGI NO-UNDO.
DEF INPUT PARAMETER property-code AS INT NO-UNDO.
DEF INPUT PARAMETER fc-for AS INT NO-UNDO.

  IF NOT AVAILABLE(Property) THEN
    FIND Property WHERE Property.PropertyCode = property-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Property) THEN RETURN.

  FIND RentalSpace OF Property WHERE RentalSpace.RentalSpaceCode = fc-for NO-LOCK.
  IF NOT AVAILABLE(RentalSpace) THEN RETURN.

  IF new-records THEN CREATE Forecast.
  Forecast.Description = RentalSpace.Description .

  /* now we're fucked, aren't we?! */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE forecast-for-lease B-table-Win 
PROCEDURE forecast-for-lease :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-records AS LOGI NO-UNDO.
DEF INPUT PARAMETER fc-for AS INT NO-UNDO.

  FIND TenancyLease WHERE TenancyLease.TenancyLeaseCode = fc-for NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(TenancyLease) THEN RETURN.
  FIND Tenant WHERE Tenant.TenantCode = TenancyLease.TenantCode NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Tenant) THEN RETURN.

  IF NOT AVAILABLE(Property) THEN
    FIND Property WHERE Property.PropertyCode = TenancyLease.PropertyCode NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Property) THEN RETURN.

DEF BUFFER tmp_Space FOR RentalSpace.
  FOR EACH tmp_Space NO-LOCK OF TenancyLease WHERE tmp_Space.AreaStatus <> "X":
    IF new-records THEN CREATE Forecast.
    Forecast.Description = RentalSpace.Description .
    RUN forecast-for-area( no, tmp_Space.PropertyCode, tmp_Space.RentalSpaceCode ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE forecast-for-property B-table-Win 
PROCEDURE forecast-for-property :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-records AS LOGI NO-UNDO.
DEF INPUT PARAMETER fc-for AS INT NO-UNDO.

  FIND Property WHERE Property.PropertyCode = fc-for NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Property) THEN RETURN.

DEF BUFFER tmp_Lease FOR TenancyLease.
  FOR EACH tmp_Lease NO-LOCK OF Property WHERE tmp_Lease.LeaseStatus <> "PAST":
    CREATE Forecast.
    Forecast.Description = TenancyLease.AreaDescription.
    RUN forecast-for-lease( no, tmp_Lease.TenancyLeaseCode ).
  END.

DEF BUFFER tmp_Space FOR RentalSpace.
  FOR EACH tmp_Space NO-LOCK OF Property WHERE tmp_Space.AreaStatus = "V":
    CREATE Forecast.
    Forecast.Description = "Vacant: " + RentalSpace.Description.
    RUN forecast-for-area( no, tmp_Space.PropertyCode, tmp_Space.RentalSpaceCode ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE forecast-for-tenant B-table-Win 
PROCEDURE forecast-for-tenant :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-records AS LOGI NO-UNDO.
DEF INPUT PARAMETER fc-for AS INT NO-UNDO.

  FIND Tenant WHERE Tenant.TenantCode = fc-for NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Tenant) OR Tenant.EntityType <> "P" THEN RETURN.

  IF NOT AVAILABLE(Property) THEN
    FIND Property WHERE Property.PropertyCode = Tenant.EntityCode NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Property) THEN RETURN.

DEF BUFFER tmp_Space FOR RentalSpace.
  FOR EACH TenancyLease NO-LOCK OF Tenant,
        EACH tmp_Space NO-LOCK OF TenancyLease WHERE tmp_Space.AreaStatus <> "X":
    IF new-records THEN CREATE Forecast.
    Forecast.Description = RentalSpace.Description .
    RUN forecast-for-area( no, tmp_Space.PropertyCode, tmp_Space.RentalSpaceCode ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-open-query B-table-Win 
PROCEDURE pre-open-query :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN rebuild-forecast.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rebuild-forecast B-table-Win 
PROCEDURE rebuild-forecast :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN clear-current-forecast.
  CASE parent-type:
    WHEN "Property" THEN    RUN forecast-for-property( yes, parent-code ).
    WHEN "Lease" THEN       RUN forecast-for-lease( yes, parent-code ).
    WHEN "Tenant" THEN      RUN forecast-for-tenant( yes, parent-code ).
    WHEN "RentalSpace" THEN RUN forecast-for-area( yes, parent-property, parent-code ).
    OTHERWISE MESSAGE parent-type "not implemented yet.".
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Forecast"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-forecast-start B-table-Win 
PROCEDURE use-forecast-start :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR new-start AS CHAR NO-UNDO.
  forecast-start = DATE(new-start).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name B-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  parent-type = new-key-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-parent-code B-table-Win 
PROCEDURE use-parent-code :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR new-code AS CHAR NO-UNDO.
  parent-code = INT(new-code).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-parent-property B-table-Win 
PROCEDURE use-parent-property :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR new-property AS CHAR NO-UNDO.
  parent-property = INT(new-property).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Parent-Type B-table-Win 
PROCEDURE use-Parent-Type :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR new-type AS CHAR NO-UNDO.
  parent-type = new-type.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-period-size B-table-Win 
PROCEDURE use-period-size :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR new-size AS CHAR NO-UNDO.
  period-size = new-size.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


