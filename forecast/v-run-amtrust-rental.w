&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description: 
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */

&SCOPED-DEFINE REPORT-ID "Amtrust Rental Run"

{inc/ofc-this.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES RP
&Scoped-define FIRST-EXTERNAL-TABLE RP


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR RP.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS RP.Char1 RP.Int1 RP.Log2 RP.Char3 RP.Log3 ~
RP.Log4 RP.Log5 RP.Log6 RP.Log1 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}Int1 ~{&FP2}Int1 ~{&FP3}
&Scoped-define ENABLED-TABLES RP
&Scoped-define FIRST-ENABLED-TABLE RP
&Scoped-Define ENABLED-OBJECTS RECT-23 cmb_MonthFrom cmb_MonthTo btn_print 
&Scoped-Define DISPLAYED-FIELDS RP.Char1 RP.Int1 RP.Log2 RP.Char3 RP.Log3 ~
RP.Log4 RP.Log5 RP.Log6 RP.Log1 
&Scoped-Define DISPLAYED-OBJECTS fil_Property cmb_MonthFrom cmb_MonthTo 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_print 
     LABEL "&OK" 
     SIZE 10.29 BY 1
     FONT 9.

DEFINE VARIABLE cmb_MonthFrom AS CHARACTER FORMAT "X(256)":U 
     LABEL "From month" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "Item 1" 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_MonthTo AS CHARACTER FORMAT "X(256)":U 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "Item 1" 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fil_Property AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 42.86 BY 1
     FONT 10 NO-UNDO.

DEFINE RECTANGLE RECT-23
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 68 BY 12.6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     RP.Char1 AT ROW 1.2 COL 2 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "All properties", "All":U,
"Single Property", "OneProperty":U
          SIZE 13.86 BY 1.6
          FONT 10
     RP.Int1 AT ROW 2 COL 13.86 COLON-ALIGNED NO-LABEL FORMAT "99999"
          VIEW-AS FILL-IN 
          SIZE 6.57 BY 1
          FONT 10
     fil_Property AT ROW 2 COL 23.57 COLON-ALIGNED NO-LABEL
     RP.Log2 AT ROW 3.2 COL 6.14 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "Clear all and regenerate", yes,
"Regenerate these properties", ?,
"Report only", no
          SIZE 60.57 BY 1
     RP.Char3 AT ROW 4.4 COL 6.14 HELP
          "" NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "Everything", "All":U,
"Rents", "Rents":U,
"Expenses", "Expenses":U,
"Recoveries", "Recoveries":U
          SIZE 45.72 BY 1
     cmb_MonthFrom AT ROW 8.2 COL 13.86 COLON-ALIGNED
     cmb_MonthTo AT ROW 8.2 COL 37.86 COLON-ALIGNED
     RP.Log3 AT ROW 10 COL 6.14
          LABEL "Apply ratchet to all leases"
          VIEW-AS TOGGLE-BOX
          SIZE 22.72 BY .9
     RP.Log4 AT ROW 10.8 COL 6.14
          LABEL "Audit report"
          VIEW-AS TOGGLE-BOX
          SIZE 17.29 BY .9
     RP.Log5 AT ROW 11.6 COL 6.14
          LABEL "Rental report"
          VIEW-AS TOGGLE-BOX
          SIZE 17.29 BY .9
     RP.Log6 AT ROW 11.6 COL 24.43
          LABEL "Skip agents fees"
          VIEW-AS TOGGLE-BOX
          SIZE 16.57 BY .85
     RP.Log1 AT ROW 12.4 COL 6.14
          LABEL "Preview"
          VIEW-AS TOGGLE-BOX
          SIZE 14.29 BY .8
          FONT 10
     btn_print AT ROW 12.4 COL 58.14
     RECT-23 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10
         DEFAULT-BUTTON btn_print.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: ttpl.RP
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 16.25
         WIDTH              = 84.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR RADIO-SET RP.Char3 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR FILL-IN fil_Property IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN RP.Int1 IN FRAME F-Main
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX RP.Log1 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR RADIO-SET RP.Log2 IN FRAME F-Main
   EXP-HELP                                                             */
/* SETTINGS FOR TOGGLE-BOX RP.Log3 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log4 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log5 IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX RP.Log6 IN FRAME F-Main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print V-table-Win
ON CHOOSE OF btn_print IN FRAME F-Main /* OK */
DO:
  SELF:SENSITIVE = No.
  RUN run-report.
  SELF:SENSITIVE = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Char1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Char1 V-table-Win
ON VALUE-CHANGED OF RP.Char1 IN FRAME F-Main /* Char1 */
DO:
  RUN select-options-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_MonthFrom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_MonthFrom V-table-Win
ON U1 OF cmb_MonthFrom IN FRAME F-Main /* From month */
DO:
  {inc/selcmb/scmths1.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_MonthFrom V-table-Win
ON U2 OF cmb_MonthFrom IN FRAME F-Main /* From month */
DO:
  {inc/selcmb/scmths2.i "RP" "Int3"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_MonthTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_MonthTo V-table-Win
ON U1 OF cmb_MonthTo IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe1.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_MonthTo V-table-Win
ON U2 OF cmb_MonthTo IN FRAME F-Main /* To */
DO:
  {inc/selcmb/scmthe2.i "RP" "Int4"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fil_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U1 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro1.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U2 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro2.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fil_Property V-table-Win
ON U3 OF fil_Property IN FRAME F-Main
DO:
  {inc/selfil/sfpro3.i "RP" "Int1"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Int1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Int1 V-table-Win
ON LEAVE OF RP.Int1 IN FRAME F-Main /* Int1 */
DO:
  {inc/selcde/cdpro.i "fil_Property"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log2 V-table-Win
ON VALUE-CHANGED OF RP.Log2 IN FRAME F-Main /* Log2 */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RP.Log5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RP.Log5 V-table-Win
ON VALUE-CHANGED OF RP.Log5 IN FRAME F-Main /* Rental report */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win 
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override (thoroughly!) the Progress adm-row-available
------------------------------------------------------------------------------*/

 /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "Creditor"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "Creditor"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  RUN select-options-changed.

  IF INPUT RP.Log2 THEN DO:
    DISABLE RP.Char3 .
    ENABLE cmb_MonthFrom cmb_MonthTo RP.Log3 RP.Log4 RP.Char3.
  END.
  ELSE IF NOT INPUT RP.Log2 THEN
    DISABLE cmb_MonthFrom cmb_MonthTo RP.Log3 RP.Log4 RP.Char3.
  ELSE DO:
    DISABLE cmb_MonthFrom cmb_MonthTo RP.Log3 .
    ENABLE RP.Log4 RP.Char3.
  END.

  IF INPUT RP.Log5 THEN
    ENABLE RP.Log6.
  ELSE
    DISABLE RP.Log6.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN enable-appropriate-fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FIND RP WHERE RP.UserName = "Amtrust-Forecast"
            AND RP.ReportID = {&REPORT-ID} NO-ERROR.

  IF NOT AVAILABLE RP THEN DO WITH FRAME {&FRAME-NAME}:
    CREATE RP.
    ASSIGN  RP.ReportID = {&REPORT-ID}
            RP.UserName = "Amtrust-Forecast"
            RP.Char1 = "All"
            RP.Log1 = Yes
            RP.Log2 = Yes.
  END.

  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN check-modified( "CLEAR" ).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE run-report V-table-Win 
PROCEDURE run-report :
/*------------------------------------------------------------------------------
  Purpose:     Actually run the report through RB engine.
------------------------------------------------------------------------------*/
DEF VAR report-options AS CHAR NO-UNDO.

  RUN verify-report.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  RUN dispatch( 'update-record':U ).

  report-options = RP.Char1
                 + (IF RP.Char1 = "OneProperty" THEN
                       "~nProperties," + STRING(RP.Int1) + "," + STRING(RP.Int1)
                     ELSE "")
                 + (IF RP.Log2 = ? THEN "~nRegenerate," + RP.Char3 ELSE "")
                 + (IF RP.Log1 THEN "~nPreview" ELSE "")
                 + (IF RP.Log2 THEN "~nClearAll" ELSE "")
                 + (IF NOT RP.Log2 THEN "~nReportOnly" ELSE "")
                 + (IF RP.Log3 THEN "~nAllRatchets" ELSE "")
                 + (IF RP.Log4 THEN "~nAuditReport" ELSE "")
                 + (IF RP.Log5 THEN "~nRentReport" ELSE "")
                 + (IF RP.Log5 AND RP.Log6 THEN "~nSkipAgentsFees" ELSE "")
                 + "~nMonths," + STRING( RP.Int3 ) + "," + STRING( RP.Int4 ) .

  IF RP.Log2 THEN DO TRANSACTION:
    /* save the options for future runs */
    FIND CURRENT RP EXCLUSIVE-LOCK.
    RP.Char6 = report-options.
    FIND CURRENT RP EXCLUSIVE-LOCK.
  END.

  {inc/bq-do.i "forecast/amtrust-forecast.p" "report-options" "NOT RP.Log1"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE select-options-changed V-table-Win 
PROCEDURE select-options-changed :
/*------------------------------------------------------------------------------
  Purpose:
------------------------------------------------------------------------------*/
DEF VAR options AS CHAR NO-UNDO.

  options = INPUT FRAME {&FRAME-NAME} RP.Char1.

  CASE options:
    WHEN "All" THEN
    DO WITH FRAME {&FRAME-NAME}:
      HIDE RP.Int1 fil_Property.
      RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = Yes":U ).
    END.

    WHEN "OneProperty" THEN
    DO WITH FRAME {&FRAME-NAME}:
      VIEW RP.Int1 fil_Property.
      RUN set-link-attributes IN sys-mgr( THIS-PROCEDURE, STRING( fil_Property:HANDLE ), "HIDDEN = No":U ).
    END.

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win 
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "Creditor"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-report V-table-Win 
PROCEDURE verify-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  CASE INPUT FRAME {&FRAME-NAME} RP.Char1:

    WHEN 'OneProperty' THEN
      IF NOT CAN-FIND( FIRST Property WHERE Property.PropertyCode =
        INPUT FRAME {&FRAME-NAME} RP.Int1 ) THEN
      DO:
        MESSAGE "You must select a property" VIEW-AS ALERT-BOX ERROR.
        APPLY 'ENTRY':U TO RP.Int1 IN FRAME {&FRAME-NAME}.
        RETURN "FAIL".
      END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


