&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          ttpl             PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
  Description:
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR mode AS CHAR NO-UNDO.
DEF VAR key-name AS CHAR NO-UNDO.
DEF VAR key-value AS CHAR NO-UNDO   INITIAL ?.

DEF VAR frequency-type AS CHAR NO-UNDO.
DEF VAR region-code AS CHAR NO-UNDO.

DEF VAR type-scope AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES ScenarioAssumption
&Scoped-define FIRST-EXTERNAL-TABLE ScenarioAssumption


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR ScenarioAssumption.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS ScenarioAssumption.Scope ~
ScenarioAssumption.TypeScope ScenarioAssumption.AssumptionValue 
&Scoped-define FIELD-PAIRS~
 ~{&FP1}AssumptionValue ~{&FP2}AssumptionValue ~{&FP3}
&Scoped-define ENABLED-TABLES ScenarioAssumption
&Scoped-define FIRST-ENABLED-TABLE ScenarioAssumption
&Scoped-Define ENABLED-OBJECTS cmb_Region cmb_Property cmb_Month ~
cmb_AssumptionType fil_MonthsVacant fil_PerAnnum fil_PSM cmb_Frequency 
&Scoped-Define DISPLAYED-FIELDS ScenarioAssumption.ScenarioCode ~
ScenarioAssumption.Scope ScenarioAssumption.TypeScope ~
ScenarioAssumption.AssumptionValue 
&Scoped-Define DISPLAYED-OBJECTS cmb_Region cmb_Property cmb_Month ~
cmb_AssumptionType fil_MonthsVacant fil_PerAnnum fil_PSM cmb_Frequency 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
FlowStepType|y|y|TTPL.FlowStepType.FlowStepType
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "FlowStepType",
     Keys-Supplied = "FlowStepType"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE cmb_AssumptionType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Assumption type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     SIZE 42.86 BY 1.05 NO-UNDO.

DEFINE VARIABLE cmb_Frequency AS CHARACTER FORMAT "X(256)":U 
     LABEL "per" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Month AS CHARACTER FORMAT "X(256)":U 
     LABEL "Applying from" 
     VIEW-AS COMBO-BOX INNER-LINES 24
     LIST-ITEMS "Item 1" 
     SIZE 15.43 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Property AS CHARACTER FORMAT "X(256)":U 
     LABEL "Property" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "Item 1" 
     SIZE 42.86 BY 1 NO-UNDO.

DEFINE VARIABLE cmb_Region AS CHARACTER FORMAT "X(256)":U 
     LABEL "Region" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Item 1" 
     SIZE 42.86 BY 1 NO-UNDO.

DEFINE VARIABLE fil_MonthsVacant AS INTEGER FORMAT "->,>>>,>>9":U 
     LABEL "Months vacant" 
     VIEW-AS FILL-IN 
     SIZE 4.57 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_PerAnnum AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Per annum" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1.05 NO-UNDO.

DEFINE VARIABLE fil_PSM AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "PSM" 
     VIEW-AS FILL-IN 
     SIZE 10.14 BY 1.05 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     ScenarioAssumption.ScenarioCode AT ROW 2 COL 13.29 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 5.72 BY 1.05
     ScenarioAssumption.Scope AT ROW 3.2 COL 10
          VIEW-AS COMBO-BOX INNER-LINES 5
          LIST-ITEMS "G","R","P" 
          SIZE 5.72 BY 1
     cmb_Region AT ROW 4.4 COL 13.29 COLON-ALIGNED
     cmb_Property AT ROW 5.6 COL 13.29 COLON-ALIGNED
     cmb_Month AT ROW 7.4 COL 13.29 COLON-ALIGNED
     ScenarioAssumption.TypeScope AT ROW 8.4 COL 7
          LABEL "Applying to" FORMAT "X(20)"
          VIEW-AS COMBO-BOX INNER-LINES 5
          LIST-ITEMS "REVU - Rent Review","NLSE - New Lease" 
          SIZE 21.14 BY 1.05
     cmb_AssumptionType AT ROW 9.6 COL 13.29 COLON-ALIGNED
     fil_MonthsVacant AT ROW 12.4 COL 13.29 COLON-ALIGNED
     fil_PerAnnum AT ROW 13.4 COL 13.29 COLON-ALIGNED
     fil_PSM AT ROW 13.4 COL 36.14 COLON-ALIGNED
     cmb_Frequency AT ROW 13.4 COL 49.86 COLON-ALIGNED
     ScenarioAssumption.AssumptionValue AT ROW 15 COL 13.29 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 37.14 BY 1.05
     "onwards" VIEW-AS TEXT
          SIZE 8 BY 1 AT ROW 7.4 COL 30.72
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 10.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: TTPL.ScenarioAssumption
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 16.05
         WIDTH              = 75.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN ScenarioAssumption.ScenarioCode IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX ScenarioAssumption.Scope IN FRAME F-Main
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX ScenarioAssumption.TypeScope IN FRAME F-Main
   ALIGN-L EXP-LABEL EXP-FORMAT                                         */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME cmb_AssumptionType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AssumptionType V-table-Win
ON U1 OF cmb_AssumptionType IN FRAME F-Main /* Assumption type */
DO:
  {inc/selcmb/scrcht1.i "ScenarioAssumption" "AssumptionType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AssumptionType V-table-Win
ON U2 OF cmb_AssumptionType IN FRAME F-Main /* Assumption type */
DO:
  {inc/selcmb/scrcht2.i "ScenarioAssumption" "AssumptionType"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_AssumptionType V-table-Win
ON VALUE-CHANGED OF cmb_AssumptionType IN FRAME F-Main /* Assumption type */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Frequency
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Frequency V-table-Win
ON U1 OF cmb_Frequency IN FRAME F-Main /* per */
DO:
  {inc/selcmb/scfty1.i "?" "frequency-type"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Frequency V-table-Win
ON U2 OF cmb_Frequency IN FRAME F-Main /* per */
DO:
  {inc/selcmb/scfty2.i "?" "frequency-type"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Month
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month V-table-Win
ON U1 OF cmb_Month IN FRAME F-Main /* Applying from */
DO:
  {inc/selcmb/scmths1.i "ScenarioAssumption" "MonthCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Month V-table-Win
ON U2 OF cmb_Month IN FRAME F-Main /* Applying from */
DO:
  {inc/selcmb/scmths2.i "ScenarioAssumption" "MonthCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Property
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Property V-table-Win
ON U1 OF cmb_Property IN FRAME F-Main /* Property */
DO:
DO WITH FRAME {&FRAME-NAME}:
  region-code = ENTRY( 1, cmb_Region:SCREEN-VALUE, " ").
  {inc/selcmb/scprop1.i "ScenarioAssumption" "EntityCode" "region-code"}
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Property V-table-Win
ON U2 OF cmb_Property IN FRAME F-Main /* Property */
DO:
  {inc/selcmb/scprop2.i "ScenarioAssumption" "EntityCode"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmb_Region
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Region V-table-Win
ON U1 OF cmb_Region IN FRAME F-Main /* Region */
DO:
  {inc/selcmb/scrgn1.i "ScenarioAssumption" "Region"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Region V-table-Win
ON U2 OF cmb_Region IN FRAME F-Main /* Region */
DO:
  {inc/selcmb/scrgn2.i "ScenarioAssumption" "Region"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmb_Region V-table-Win
ON VALUE-CHANGED OF cmb_Region IN FRAME F-Main /* Region */
DO:
  RUN region-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ScenarioAssumption.Scope
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ScenarioAssumption.Scope V-table-Win
ON VALUE-CHANGED OF ScenarioAssumption.Scope IN FRAME F-Main /* Scope */
DO:
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ScenarioAssumption.TypeScope
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ScenarioAssumption.TypeScope V-table-Win
ON VALUE-CHANGED OF ScenarioAssumption.TypeScope IN FRAME F-Main /* Applying to */
DO:
  RUN type-scope-changed.
  RUN enable-appropriate-fields.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'FlowStepType':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = FlowStepType
           &WHERE = "WHERE FlowStepType.FlowStepType eq key-value"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "ScenarioAssumption"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "ScenarioAssumption"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancel-changes V-table-Win 
PROCEDURE cancel-changes :
/*------------------------------------------------------------------------------
  Purpose:  Quit without applying any changes
------------------------------------------------------------------------------*/
  IF mode = "Add" THEN DO TRANSACTION:
    FIND CURRENT ScenarioAssumption NO-ERROR.
    IF AVAILABLE(ScenarioAssumption) THEN DELETE ScenarioAssumption.
  END.

  RUN check-modified( 'clear':U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirm-changes V-table-Win 
PROCEDURE confirm-changes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN verify-record.
  IF RETURN-VALUE <> "" THEN RETURN.

  RUN dispatch( 'update-record':U ).
  RUN notify( 'open-query,record-source':U ).

  RUN dispatch( 'exit':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-appropriate-fields V-table-Win 
PROCEDURE enable-appropriate-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  /* changing scope */
  CASE INPUT ScenarioAssumption.Scope:
    WHEN "R" THEN DO:
      IF cmb_Region:SCREEN-VALUE = ? THEN DO:
        cmb_Region:SCREEN-VALUE = cmb_Region:ENTRY(1).
        RUN region-changed.
      END.
      ENABLE cmb_Region.
      DISABLE cmb_Property.
    END.
    WHEN "P" THEN DO:
      IF cmb_Region:SCREEN-VALUE = ? THEN DO:
        cmb_Region:SCREEN-VALUE = cmb_Region:ENTRY(1).
        RUN region-changed.
      END.
      IF cmb_Property:SCREEN-VALUE = ? THEN cmb_Property:SCREEN-VALUE = cmb_Property:ENTRY(1).
      ENABLE cmb_Region cmb_Property.
    END.
    OTHERWISE DO:
      DISABLE cmb_Region cmb_Property.
    END.
  END CASE.

  /* type of assumption */
DEF VAR assumption-type AS CHAR NO-UNDO.
DEF VAR type-scope AS CHAR NO-UNDO.
  type-scope = ENTRY( 1, INPUT ScenarioAssumption.TypeScope, " ").
  assumption-type = ENTRY( 1, INPUT cmb_AssumptionType, " ").
  IF type-scope = "REVU" OR type-scope = "NLSE" THEN DO:
    FIND AreaType WHERE AreaType.AreaType = assumption-type NO-LOCK.
    IF AreaType.IsCarPark THEN DO:
      fil_PSM:LABEL = "Per park".
      ENABLE fil_PerAnnum fil_PSM cmb_Frequency.
    END.
    ELSE IF AreaType.IsFloorArea THEN DO:
    END.
    ELSE DO:
    END.
  END.
  ELSE DO:
    MESSAGE "Service Contracts not supported yet" SKIP(1)
            "What should it do?"
            VIEW-AS ALERT-BOX ERROR.
  END.

  /* scope of assumption type */
/*
  CASE INPUT ScenarioAssumption.TypeScope:
    WHEN "Rent Review" THEN DO:
    END.
    WHEN "New Leasing" THEN DO:
    END.
  END.
*/
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-assign-statement V-table-Win 
PROCEDURE inst-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  IF ScenarioAssumption.Scope = "G" THEN ASSIGN
    ScenarioAssumption.Region       = ?
    ScenarioAssumption.EntityType   = ?
    ScenarioAssumption.EntityCode   = ?.
  ELSE IF ScenarioAssumption.Scope = "R" THEN ASSIGN
    ScenarioAssumption.EntityType   = ?
    ScenarioAssumption.EntityCode   = ?.

DEF VAR type-scope AS CHAR NO-UNDO.
  type-scope = ENTRY( 1, ScenarioAssumption.TypeScope, " ").
  CASE type-scope:
    WHEN "REVU" THEN DO:
    END.
    WHEN "NLSE" THEN DO:
    END.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-enable-fields V-table-Win 
PROCEDURE inst-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN enable-appropriate-fields.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF mode = 'Add' THEN DO:
    have-records = Yes.
    key-name = "ScenarioCode":U.
    key-value = find-parent-key( key-name ).
    CURRENT-WINDOW:TITLE = "Adding a New Scenario Assumption".
    RUN dispatch( 'add-record':U ).
  END.
  ELSE IF mode = "Maintain" THEN
    RUN dispatch( 'enable-fields':U ).
  ELSE
    RUN dispatch( 'disable-fields':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE override-add-record V-table-Win 
PROCEDURE override-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  FIND ScenarioAssumption WHERE ScenarioAssumption.ScenarioCode = INT(key-value)
                    AND ScenarioAssumption.Scope = ? NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(ScenarioAssumption) THEN CREATE ScenarioAssumption.

  FIND LAST Month WHERE Month.StartDate < TODAY NO-LOCK.
  FIND FinancialYear OF Month NO-LOCK.
  FIND FIRST Month OF FinancialYear NO-LOCK.

  ASSIGN ScenarioAssumption.ScenarioCode = INT(key-value) 
         ScenarioAssumption.Scope = ?
         ScenarioAssumption.EntityType = "P"
         ScenarioAssumption.Scope     = ScenarioAssumption.Scope:ENTRY(1)
         ScenarioAssumption.TypeScope = ScenarioAssumption.TypeScope:ENTRY(1)
         ScenarioAssumption.MonthCode = Month.MonthCode .
  
  CURRENT-WINDOW:TITLE = "Adding a New Scenario Assumption".
  RUN dispatch( 'display-fields':U ).
  RUN dispatch( 'enable-fields':U ).
  RUN dispatch( 'update-record':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-destroy V-table-Win 
PROCEDURE pre-destroy :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF LAST-EVENT:FUNCTION = "WINDOW-CLOSE" THEN RUN cancel-changes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE region-changed V-table-Win 
PROCEDURE region-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:
  APPLY "U1":U TO cmb_Property.
  DISPLAY cmb_Property.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "FlowStepType" "FlowStepType" "FlowStepType"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ScenarioAssumption"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE type-scope-changed V-table-Win 
PROCEDURE type-scope-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR new-type AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
  new-type = INPUT ScenarioAssumption.TypeScope.
  IF LOOKUP( type-scope, "REVU,NLSE") > 0 THEN DO:
    IF LOOKUP( new-type, "REVU,NLSE") = 0 THEN APPLY "U1":U TO cmb_AssumptionType .
  END.
  ELSE IF LOOKUP( new-type, "REVU,NLSE") > 0 THEN
     APPLY "U1":U TO cmb_AssumptionType .

  type-scope = new-type.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Name V-table-Win 
PROCEDURE use-Key-Name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-name AS CHAR NO-UNDO.
  key-name = new-key-name.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Key-Value V-table-Win 
PROCEDURE use-Key-Value :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-key-value AS CHAR NO-UNDO.
  key-value = new-key-value.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE use-Mode V-table-Win 
PROCEDURE use-Mode :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER new-mode AS CHAR NO-UNDO.
  mode = new-mode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-record V-table-Win 
PROCEDURE verify-record :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR input-task-type AS CHAR NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


