&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF WORK-TABLE MenuNode NO-UNDO
  FIELD NodeCode    LIKE LinkNode.NodeCode
  FIELD Description LIKE LinkNode.Description.

DEF WORK-TABLE ItemNode NO-UNDO
  FIELD ButtonLabel LIKE ProgramLink.ButtonLabel
  FIELD LinkCode    LIKE ProgramLink.LinkCode
  FIELD ItemStatus  AS CHAR.

DEF VAR menu-altered AS LOGI   NO-UNDO.
DEF VAR last-id      AS ROWID  NO-UNDO.
DEF VAR this-win     AS HANDLE NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tgl_topmost sel_mavail btn_madd sel_mcurr ~
btn_mrem sel_iavail btn_save sel_icurr btn_iadd btn_irem btn_iup btn_idown 
&Scoped-Define DISPLAYED-OBJECTS tgl_topmost sel_mavail sel_mcurr ~
sel_iavail sel_icurr 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
GroupName|y|y|TTPL.UsrGroup.GroupName
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "GroupName",
     Keys-Supplied = "GroupName"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-sel_iavail 
       MENU-ITEM m_AvailEdit    LABEL "Edit Link"     .

DEFINE MENU POPUP-MENU-sel_icurr 
       MENU-ITEM m_Edit         LABEL "&Edit Link"    
       MENU-ITEM m_Edit_i-Target LABEL "Edit &Target"  
       MENU-ITEM m_Change_i-Group LABEL "Change &Group" .

DEFINE MENU POPUP-MENU-sel_mcurr 
       MENU-ITEM m_Edit_m-node  LABEL "&Edit node"    
       MENU-ITEM m_Change_m-Group LABEL "Change &Group" .


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_iadd 
     LABEL "&Add" 
     SIZE 10.29 BY 1.05
     FONT 10.

DEFINE BUTTON btn_idown 
     LABEL "Move &Down" 
     SIZE 10.29 BY 1.05
     FONT 10.

DEFINE BUTTON btn_irem 
     LABEL "&Remove" 
     SIZE 10.29 BY 1.05
     FONT 10.

DEFINE BUTTON btn_iup 
     LABEL "Move &Up" 
     SIZE 10.29 BY 1.05
     FONT 10.

DEFINE BUTTON btn_madd 
     LABEL "Add" 
     SIZE 10.29 BY 1.05
     FONT 10.

DEFINE BUTTON btn_mrem 
     LABEL "Remove" 
     SIZE 10.29 BY 1.05
     FONT 10.

DEFINE BUTTON btn_save 
     LABEL "&Save" 
     SIZE 10 BY 1.05
     FONT 10.

DEFINE VARIABLE sel_iavail AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SORT SCROLLBAR-VERTICAL 
     SIZE 30 BY 13
     FONT 10 NO-UNDO.

DEFINE VARIABLE sel_icurr AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
     SIZE 30 BY 13
     FONT 10 NO-UNDO.

DEFINE VARIABLE sel_mavail AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SORT SCROLLBAR-VERTICAL 
     SIZE 30 BY 8.5
     FONT 10 NO-UNDO.

DEFINE VARIABLE sel_mcurr AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SORT SCROLLBAR-VERTICAL 
     SIZE 30 BY 8.5
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_topmost AS LOGICAL INITIAL no 
     LABEL "On Top ?" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY 1
     FONT 10 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     tgl_topmost AT ROW 1 COL 65
     sel_mavail AT ROW 2 COL 2 NO-LABEL
     btn_madd AT ROW 2 COL 33
     sel_mcurr AT ROW 2 COL 45 NO-LABEL
     btn_mrem AT ROW 3.2 COL 33
     sel_iavail AT ROW 11.5 COL 2 NO-LABEL
     btn_save AT ROW 11.5 COL 33
     sel_icurr AT ROW 11.5 COL 45 NO-LABEL
     btn_iadd AT ROW 16 COL 33
     btn_irem AT ROW 17.2 COL 33
     btn_iup AT ROW 20.5 COL 33
     btn_idown AT ROW 21.7 COL 33
     "Current Items" VIEW-AS TEXT
          SIZE 10 BY 1 AT ROW 10.5 COL 45
          FONT 10
     "Available Items" VIEW-AS TEXT
          SIZE 11 BY 1 AT ROW 10.5 COL 2
          FONT 10
     "Current Menus" VIEW-AS TEXT
          SIZE 13.72 BY 1 AT ROW 1 COL 45
          FONT 10
     "Available Menus" VIEW-AS TEXT
          SIZE 16 BY 1 AT ROW 1 COL 2
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 23.95
         WIDTH              = 75.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       sel_iavail:POPUP-MENU IN FRAME F-Main       = MENU POPUP-MENU-sel_iavail:HANDLE.

ASSIGN 
       sel_icurr:POPUP-MENU IN FRAME F-Main       = MENU POPUP-MENU-sel_icurr:HANDLE.

ASSIGN 
       sel_mcurr:POPUP-MENU IN FRAME F-Main       = MENU POPUP-MENU-sel_mcurr:HANDLE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_iadd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_iadd V-table-Win
ON CHOOSE OF btn_iadd IN FRAME F-Main /* Add */
DO:
  RUN add-item.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_idown
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_idown V-table-Win
ON CHOOSE OF btn_idown IN FRAME F-Main /* Move Down */
DO:
  RUN move-item-down.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_irem
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_irem V-table-Win
ON CHOOSE OF btn_irem IN FRAME F-Main /* Remove */
DO:
  RUN remove-item.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_iup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_iup V-table-Win
ON CHOOSE OF btn_iup IN FRAME F-Main /* Move Up */
DO:
  RUN move-item-up.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_madd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_madd V-table-Win
ON CHOOSE OF btn_madd IN FRAME F-Main /* Add */
DO:
  RUN add-menu.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_mrem
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_mrem V-table-Win
ON CHOOSE OF btn_mrem IN FRAME F-Main /* Remove */
DO:
  RUN remove-menu.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_save
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_save V-table-Win
ON CHOOSE OF btn_save IN FRAME F-Main /* Save */
DO:
  RUN save-items.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_AvailEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_AvailEdit V-table-Win
ON CHOOSE OF MENU-ITEM m_AvailEdit /* Edit Link */
DO:
DO WITH FRAME {&FRAME-NAME}:
  FIND FIRST ItemNode WHERE ItemNode.ButtonLabel = INPUT sel_iavail.
  RUN edit-link-dialog( ItemNode.LinkCode ).
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Change_i-Group
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Change_i-Group V-table-Win
ON CHOOSE OF MENU-ITEM m_Change_i-Group /* Change Group */
DO:
  RUN item-group-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Change_m-Group
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Change_m-Group V-table-Win
ON CHOOSE OF MENU-ITEM m_Change_m-Group /* Change Group */
DO:
  RUN menu-group-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Edit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Edit V-table-Win
ON CHOOSE OF MENU-ITEM m_Edit /* Edit Link */
DO:
DO WITH FRAME {&FRAME-NAME}:
  FIND FIRST ItemNode WHERE ItemNode.ButtonLabel = INPUT sel_icurr.
  RUN edit-link-dialog( ItemNode.LinkCode ).
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Edit_i-Target
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Edit_i-Target V-table-Win
ON CHOOSE OF MENU-ITEM m_Edit_i-Target /* Edit Target */
DO:
  RUN edit-target-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Edit_m-node
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Edit_m-node V-table-Win
ON CHOOSE OF MENU-ITEM m_Edit_m-node /* Edit node */
DO:
  RUN edit-node-dialog.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_iavail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_iavail V-table-Win
ON CURSOR-RIGHT OF sel_iavail IN FRAME F-Main
OR "CURSOR-LEFT" OF {&SELF-NAME} DO:
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_iavail V-table-Win
ON MOUSE-SELECT-DBLCLICK OF sel_iavail IN FRAME F-Main
DO:
  APPLY 'CHOOSE':U TO btn_iadd .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_iavail V-table-Win
ON VALUE-CHANGED OF sel_iavail IN FRAME F-Main
DO:
  RUN update-item-buttons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_icurr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_icurr V-table-Win
ON CURSOR-RIGHT OF sel_icurr IN FRAME F-Main
OR "CURSOR-LEFT" OF {&SELF-NAME} DO:
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_icurr V-table-Win
ON MOUSE-SELECT-DBLCLICK OF sel_icurr IN FRAME F-Main
DO:
  APPLY 'CHOOSE':U TO btn_irem .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_icurr V-table-Win
ON VALUE-CHANGED OF sel_icurr IN FRAME F-Main
DO:
  RUN update-item-buttons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_mavail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_mavail V-table-Win
ON MOUSE-SELECT-DBLCLICK OF sel_mavail IN FRAME F-Main
DO:
  APPLY 'CHOOSE':U TO btn_madd .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_mavail V-table-Win
ON VALUE-CHANGED OF sel_mavail IN FRAME F-Main
DO:
  RUN update-menu-buttons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_mcurr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_mcurr V-table-Win
ON MOUSE-SELECT-DBLCLICK OF sel_mcurr IN FRAME F-Main
DO:
  APPLY 'CHOOSE':U TO btn_mrem .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_mcurr V-table-Win
ON VALUE-CHANGED OF sel_mcurr IN FRAME F-Main
DO:
  RUN current-menu-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_topmost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_topmost V-table-Win
ON VALUE-CHANGED OF tgl_topmost IN FRAME F-Main /* On Top ? */
DO:
  RUN top-most-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-item V-table-Win 
PROCEDURE add-item :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN move-item( sel_iavail:HANDLE, sel_icurr:HANDLE, "C" ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-menu V-table-Win 
PROCEDURE add-menu :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NOT AVAILABLE UsrGroup THEN RETURN.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  FIND FIRST MenuNode WHERE MenuNode.Description = INPUT sel_mavail.
  FIND LinkNode WHERE LinkNode.NodeCode = MenuNode.NodeCode NO-LOCK.
  
  CREATE UsrGroupMenu.
  ASSIGN
    UsrGroupMenu.GroupName = UsrGroup.GroupName
    UsrGroupMenu.MenuName  = LinkNode.Description
    UsrGroupMenu.NodeCode  = LinkNode.NodeCode.
    
  IF sel_mcurr:ADD-LAST( UsrGroupMenu.MenuName ) THEN.
  sel_mcurr:SCREEN-VALUE = UsrGroupMenu.MenuName.
  IF sel_mavail:DELETE( INPUT sel_mavail ) THEN.

  RUN update-menu-buttons.
  RUN refresh-items.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'GroupName':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = UsrGroup
           &WHERE = "WHERE UsrGroup.GroupName eq key-value"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE current-menu-changed V-table-Win 
PROCEDURE current-menu-changed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  menu-altered = No.
  RUN refresh-items.
  RUN update-menu-buttons.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE edit-link-dialog V-table-Win 
PROCEDURE edit-link-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER link-code AS INT NO-UNDO.

  FIND ProgramLink WHERE ProgramLink.LinkCode = link-code NO-LOCK.

  RUN lnk/d-edtlnk.w ( ProgramLink.LinkCode ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE edit-node-dialog V-table-Win 
PROCEDURE edit-node-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  FIND FIRST MenuNode WHERE MenuNode.Description = INPUT sel_mcurr.
  FIND LinkNode WHERE LinkNode.NodeCode = MenuNode.NodeCode NO-LOCK.

  RUN lnk/d-edtnod.w ( LinkNode.NodeCode ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE edit-target-dialog V-table-Win 
PROCEDURE edit-target-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  FIND FIRST ItemNode WHERE ItemNode.ButtonLabel = INPUT sel_icurr.
  FIND ProgramLink WHERE ProgramLink.LinkCode = ItemNode.LinkCode NO-LOCK.

  RUN lnk/d-edtnod.w ( ProgramLink.Target ).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  this-win = {&WINDOW-NAME}:HANDLE .
/*  tgl_topmost:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "Yes". */
  RUN top-most-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN dispatch( 'find-using-key':u ).
  IF NOT AVAILABLE UsrGroup THEN RETURN.

  CURRENT-WINDOW:TITLE = "Menus - " + UsrGroup.GroupName.  
  IF ROWID( UsrGroup ) <> last-id THEN RUN refresh-menus.
  last-id = ROWID( UsrGroup ).
  RUN update-node IN sys-mgr ( THIS-PROCEDURE ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE item-group-dialog V-table-Win 
PROCEDURE item-group-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  FIND FIRST MenuNode WHERE MenuNode.Description = INPUT sel_mcurr.
  FIND FIRST ItemNode WHERE ItemNode.ButtonLabel = INPUT sel_icurr.

  RUN lnk/d-movlnk.w ( UsrGroup.GroupName, MenuNode.NodeCode, ItemNode.LinkCode ).
  IF RETURN-VALUE = "CHANGED" THEN RUN current-menu-changed.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE menu-group-dialog V-table-Win 
PROCEDURE menu-group-dialog :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DO WITH FRAME {&FRAME-NAME}:

  FIND FIRST MenuNode WHERE MenuNode.Description = INPUT sel_mcurr.

  RUN lnk/d-movnod.w ( UsrGroup.GroupName, MenuNode.NodeCode ).
  IF RETURN-VALUE = "CHANGED" THEN RUN refresh-menus.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE move-item V-table-Win 
PROCEDURE move-item :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER h-from   AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER h-to     AS HANDLE NO-UNDO.
  DEF INPUT PARAMETER i-status AS CHAR   NO-UNDO.
  
DO WITH FRAME {&FRAME-NAME}:

  DEF VAR i     AS INT  NO-UNDO.
  DEF VAR sv    AS CHAR NO-UNDO.
  DEF VAR delim AS CHAR NO-UNDO.
  DEF VAR item  AS CHAR NO-UNDO.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  delim = h-from:DELIMITER.
  sv    = h-from:SCREEN-VALUE.
  
  DO i = 1 TO NUM-ENTRIES( sv, delim ):
    
    item = ENTRY( i, sv, delim ).
    FIND FIRST ItemNode WHERE ItemNode.ButtonLabel = item.
    ASSIGN ItemNode.ItemStatus = i-status.
    
    IF h-to:ADD-LAST( item ) THEN.
    IF h-from:DELETE( item ) THEN.
    
  END.

  menu-altered = Yes.
  RUN update-item-buttons.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
    
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE move-item-down V-table-Win 
PROCEDURE move-item-down :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR i AS INT NO-UNDO.
  DEF VAR sv AS CHAR NO-UNDO.
  DEF VAR li AS CHAR NO-UNDO.
  DEF VAR delim AS CHAR NO-UNDO.
 
  sv = INPUT sel_icurr.
  li = sel_icurr:LIST-ITEMS.
  delim = sel_icurr:DELIMITER.
  
  DEF VAR pos AS INT NO-UNDO.
  DEF VAR curr-val AS CHAR NO-UNDO.

  DO i = NUM-ENTRIES( sv, delim ) TO 1 BY -1:
    curr-val = ENTRY( i, sv, delim ).
    pos = LOOKUP( curr-val, li, delim ).
    IF sel_icurr:REPLACE( sel_icurr:ENTRY( pos + 1 ), pos ) THEN.
    IF sel_icurr:REPLACE( curr-val, pos + 1 ) THEN.
  END.

  menu-altered = Yes.  
  sel_icurr:SCREEN-VALUE = sv.
  RUN update-item-buttons.

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE move-item-up V-table-Win 
PROCEDURE move-item-up :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR i AS INT NO-UNDO.
  DEF VAR sv AS CHAR NO-UNDO.
  DEF VAR li AS CHAR NO-UNDO.
  DEF VAR delim AS CHAR NO-UNDO.
 
  sv = INPUT sel_icurr.
  li = sel_icurr:LIST-ITEMS.
  delim = sel_icurr:DELIMITER.
  
  DEF VAR pos AS INT NO-UNDO.
  DEF VAR curr-val AS CHAR NO-UNDO.

  DO i = 1 TO NUM-ENTRIES( sv, delim ):
    curr-val = ENTRY( i, sv, delim ).
    pos = LOOKUP( curr-val, li, delim ).
    IF sel_icurr:REPLACE( sel_icurr:ENTRY( pos - 1 ), pos ) THEN.
    IF sel_icurr:REPLACE( curr-val, pos - 1 ) THEN.
  END.

  menu-altered = Yes.  
  sel_icurr:SCREEN-VALUE = sv.
  RUN update-item-buttons.
                                                                                                                                                                                                                                                                                                               
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  have-records = No.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-available-items V-table-Win 
PROCEDURE refresh-available-items :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  sel_iavail:LIST-ITEMS = "".
  DEF VAR screen-label AS CHAR NO-UNDO.

  IF NOT AVAILABLE UsrGroup THEN RETURN.

  FIND FIRST MenuNode WHERE MenuNode.Description = INPUT sel_mcurr NO-ERROR.
  IF NOT AVAILABLE MenuNode THEN RETURN.
  
  FIND LinkNode WHERE LinkNode.NodeCode = MenuNode.NodeCode NO-LOCK.
  FIND UsrGroupMenu OF UsrGroup WHERE
    UsrGroupMenu.NodeCode = LinkNode.NodeCode NO-LOCK.

  IF LinkNode.NodeType = "MN" THEN
    FIND LinkNode WHERE LinkNode.File = 'win/w-mainmn.w':U NO-LOCK.

  FOR EACH ProgramLink NO-LOCK WHERE
    ProgramLink.Source =  LinkNode.NodeCode AND
    NOT CAN-FIND( FIRST UsrGroupMenuItem OF UsrGroupMenu WHERE
      UsrGroupMenuItem.LinkCode = ProgramLink.LinkCode ):
    
/*    IF ProgramLink.Target = LinkNode.NodeCode THEN NEXT. */

    /* This will ensure uniqueness by tagging with the linkcode */
    screen-label = ProgramLink.ButtonLabel + FILL( " ", 100 ) + STRING( ProgramLink.LinkCode ).

    IF sel_iavail:ADD-LAST( screen-label ) THEN DO:
      CREATE ItemNode.
      ASSIGN
        ItemNode.LinkCode    = ProgramLink.LinkCode
        ItemNode.ButtonLabel = screen-label
        ItemNode.ItemStatus  = "A".
    END.

  END.

  RUN update-item-buttons.
        
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-current-items V-table-Win 
PROCEDURE refresh-current-items :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR screen-label AS CHAR NO-UNDO.
  
  sel_icurr:LIST-ITEMS = "".

  IF NOT AVAILABLE UsrGroup THEN RETURN.

  FIND FIRST MenuNode WHERE MenuNode.Description = INPUT sel_mcurr NO-ERROR.
  IF NOT AVAILABLE MenuNode THEN RETURN.
  
  FIND LinkNode WHERE LinkNode.NodeCode = MenuNode.NodeCode NO-LOCK.
  FIND UsrGroupMenu OF UsrGroup WHERE
    UsrGroupMenu.NodeCode = LinkNode.NodeCode NO-LOCK.

  FOR EACH UsrGroupMenuItem OF UsrGroupMenu NO-LOCK
    BY UsrGroupMenuItem.SequenceCode:
    
    /* This will ensure label uniqueness by tagging with the linkcode */
    screen-label = UsrGroupMenuItem.ButtonLabel + FILL( " ", 100 ) + STRING( UsrGroupMenuItem.LinkCode ).
    IF sel_icurr:ADD-LAST( screen-label ) THEN
    DO:
      CREATE ItemNode.
      ASSIGN
        ItemNode.LinkCode    = UsrGroupMenuItem.LinkCode
        ItemNode.ButtonLabel = screen-label
        ItemNode.ItemStatus  = "C".
    END.

  END.

  RUN update-item-buttons.
        
END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-items V-table-Win 
PROCEDURE refresh-items :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  sel_iavail:LIST-ITEMS = "".
  sel_icurr:LIST-ITEMS = "".

  FOR EACH ItemNode: DELETE ItemNode. END.
  
  RUN refresh-available-items.
  RUN refresh-current-items.
  RUN update-item-buttons.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-menus V-table-Win 
PROCEDURE refresh-menus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  DEF VAR stat AS LOGI NO-UNDO.
  
  sel_mavail:LIST-ITEMS = "".
  sel_mcurr:LIST-ITEMS = "".
  
  IF NOT AVAILABLE UsrGroup THEN RETURN.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  FOR EACH MenuNode: DELETE MenuNode. END.
    
  FOR EACH LinkNode NO-LOCK WHERE
    LinkNode.NodeType <> "MV":

    IF CAN-FIND( FIRST UsrGroupMenu WHERE
      UsrGroupMenu.Groupname = UsrGroup.GroupName AND
      UsrGroupMenu.NodeCode  = LinkNode.NodeCode ) THEN
    
      stat = sel_mcurr:ADD-LAST( LinkNode.Description ).

    ELSE
    IF ( 
        LinkNode.NodeType = 'MN' OR
        CAN-FIND( FIRST ProgramLink WHERE ProgramLink.Source = LinkNode.NodeCode )
       ) THEN

      stat = sel_mavail:ADD-LAST( LinkNode.Description ).

    IF stat THEN
    DO:
      CREATE MenuNode.
      ASSIGN
        MenuNode.NodeCode    = LinkNode.NodeCode
        MenuNode.Description = LinkNode.Description.
    END.

  END.
      
  RUN update-buttons.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-item V-table-Win 
PROCEDURE remove-item :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  RUN move-item( sel_icurr:HANDLE, sel_iavail:HANDLE, "A" ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-menu V-table-Win 
PROCEDURE remove-menu :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NOT AVAILABLE UsrGroup THEN RETURN.
  
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  
  FIND FIRST MenuNode WHERE MenuNode.Description = INPUT sel_mcurr.
  FIND LinkNode WHERE LinkNode.NodeCode = MenuNode.NodeCode NO-LOCK.
  FIND UsrGroupMenu OF UsrGroup WHERE
    UsrGroupMenu.NodeCode = LinkNode.NodeCode EXCLUSIVE-LOCK.
  DELETE UsrGroupMenu.

  IF sel_mavail:ADD-LAST( INPUT sel_mcurr ) THEN.
  sel_mavail:SCREEN-VALUE = INPUT sel_mcurr.
  IF sel_mcurr:DELETE( INPUT sel_mcurr ) THEN.
  
  RUN update-menu-buttons.
  RUN refresh-items.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-items V-table-Win 
PROCEDURE save-items :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NOT AVAILABLE UsrGroup THEN RETURN.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  
  FIND FIRST MenuNode WHERE MenuNode.Description = INPUT sel_mcurr NO-ERROR.
  IF NOT AVAILABLE MenuNode THEN RETURN.
  
  FIND LinkNode WHERE LinkNode.NodeCode = MenuNode.NodeCode NO-LOCK.
  FIND UsrGroupMenu OF UsrGroup WHERE
    UsrGroupMenu.NodeCode = LinkNode.NodeCode NO-LOCK.

  FOR EACH UsrGroupMenuItem OF UsrGroupMenu:
    FIND FIRST ItemNode WHERE ItemNode.LinkCode = UsrGroupMenuItem.LinkCode.
    IF ItemNode.ItemStatus = "A" THEN  DELETE UsrGroupMenuItem.
  END.
  
  DEF VAR i     AS INT  NO-UNDO.
  DEF VAR li    AS CHAR NO-UNDO.
  DEF VAR delim AS CHAR NO-UNDO.
  DEF VAR item  AS CHAR NO-UNDO.

  li    = sel_icurr:LIST-ITEMS.
  delim = sel_icurr:DELIMITER.
  
  DO i = 1 TO NUM-ENTRIES( li, delim ):
    item = ENTRY( i, li, delim ).
    FIND FIRST ItemNode WHERE ItemNode.ButtonLabel = item.
    FIND ProgramLink WHERE ProgramLink.LinkCode = ItemNode.LinkCode NO-LOCK.
    FIND FIRST UsrGroupMenuItem OF UsrGroupMenu
      WHERE UsrGroupMenuItem.LinkCode = ItemNode.LinkCode NO-ERROR.
    
    IF NOT AVAILABLE UsrGroupMenuItem THEN
    DO:
      CREATE UsrGroupMenuItem.
      ASSIGN
        UsrGroupMenuItem.GroupName   = UsrGroup.GroupName
        UsrGroupMenuItem.MenuName    = UsrGroupMenu.MenuName
        UsrGroupMenuItem.LinkCode    = ProgramLink.LinkCode
        UsrGroupMenuItem.ButtonLabel = ProgramLink.ButtonLabel.
    END.
    
    ASSIGN UsrGroupMenuItem.SequenceCode = i.
    
  END.

  menu-altered = No.
  RUN update-item-buttons.
  
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "GroupName" "UsrGroup" "GroupName"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartViewer, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE top-most-changed V-table-Win 
PROCEDURE top-most-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF INPUT FRAME {&FRAME-NAME} tgl_topmost THEN
    RUN notify( 'set-topmost,container-source':U ).
  ELSE
    RUN notify( 'reset-topmost,container-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-buttons V-table-Win 
PROCEDURE update-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN update-menu-buttons.
  RUN update-item-buttons.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-item-buttons V-table-Win 
PROCEDURE update-item-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  btn_iadd:SENSITIVE = sel_iavail:SCREEN-VALUE <> "" AND sel_iavail:SCREEN-VALUE <> ?.
  btn_irem:SENSITIVE = sel_icurr:SCREEN-VALUE <> ""  AND sel_icurr:SCREEN-VALUE <> ?.

  /* Move buttons */
  
  DEF VAR delim AS CHAR NO-UNDO.
  delim = sel_icurr:DELIMITER.

  btn_iup:SENSITIVE =
    ( sel_icurr:SCREEN-VALUE <> "" AND sel_icurr:SCREEN-VALUE <> ? ) AND
    ENTRY( 1, INPUT sel_icurr, delim ) <> ENTRY( 1, sel_icurr:LIST-ITEMS, delim ).
  btn_idown:SENSITIVE =
    ( sel_icurr:SCREEN-VALUE <> "" AND sel_icurr:SCREEN-VALUE <> ? ) AND
    ENTRY( NUM-ENTRIES( INPUT sel_icurr, delim ), INPUT sel_icurr, delim ) <>
    ENTRY( NUM-ENTRIES( sel_icurr:LIST-ITEMS, delim ), sel_icurr:LIST-ITEMS, delim ).
    
  btn_save:SENSITIVE = menu-altered.
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-menu-buttons V-table-Win 
PROCEDURE update-menu-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  btn_madd:SENSITIVE = sel_mavail:SCREEN-VALUE <> "" AND sel_mavail:SCREEN-VALUE <> ?.
  btn_mrem:SENSITIVE = sel_mcurr:SCREEN-VALUE <> ""  AND sel_mcurr:SCREEN-VALUE <> ?.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


