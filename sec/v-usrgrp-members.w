&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*------------------------------------------------------------------------
  File:
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

DEF VAR last-id      AS ROWID  NO-UNDO.
DEF VAR this-win     AS HANDLE NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tgl_topmost sel_avail btn_add sel_curr ~
btn_rem 
&Scoped-Define DISPLAYED-OBJECTS tgl_topmost sel_avail sel_curr 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,List-3,List-4,List-5,List-6      */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
GroupName|y|y|TTPL.UsrGroup.GroupName
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "GroupName",
     Keys-Supplied = "GroupName"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_add 
     LABEL "Add" 
     SIZE 10.29 BY 1.05
     FONT 10.

DEFINE BUTTON btn_rem 
     LABEL "Remove" 
     SIZE 10.29 BY 1.05
     FONT 10.

DEFINE VARIABLE sel_avail AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SORT SCROLLBAR-VERTICAL 
     SIZE 30 BY 17
     FONT 10 NO-UNDO.

DEFINE VARIABLE sel_curr AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SORT SCROLLBAR-VERTICAL 
     SIZE 30 BY 17
     FONT 10 NO-UNDO.

DEFINE VARIABLE tgl_topmost AS LOGICAL INITIAL no 
     LABEL "On Top ?" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY 1
     FONT 10 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     tgl_topmost AT ROW 1 COL 65
     sel_avail AT ROW 2 COL 2 NO-LABEL
     btn_add AT ROW 2 COL 33
     sel_curr AT ROW 2 COL 45 NO-LABEL
     btn_rem AT ROW 3.2 COL 33
     "Available Users" VIEW-AS TEXT
          SIZE 16 BY 1 AT ROW 1 COL 2
          FONT 10
     "Current Members" VIEW-AS TEXT
          SIZE 13.72 BY 1 AT ROW 1 COL 45
          FONT 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 23.95
         WIDTH              = 75.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{inc/method/m-mntvwr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME btn_add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_add V-table-Win
ON CHOOSE OF btn_add IN FRAME F-Main /* Add */
DO:
  RUN add-member.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_rem
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_rem V-table-Win
ON CHOOSE OF btn_rem IN FRAME F-Main /* Remove */
DO:
  RUN remove-member.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_avail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_avail V-table-Win
ON MOUSE-SELECT-DBLCLICK OF sel_avail IN FRAME F-Main
DO:
  APPLY 'CHOOSE':U TO btn_add .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sel_curr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sel_curr V-table-Win
ON MOUSE-SELECT-DBLCLICK OF sel_curr IN FRAME F-Main
DO:
  APPLY 'CHOOSE':U TO btn_rem .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl_topmost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl_topmost V-table-Win
ON VALUE-CHANGED OF tgl_topmost IN FRAME F-Main /* On Top ? */
DO:
  RUN top-most-changed.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-member V-table-Win 
PROCEDURE add-member :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NOT AVAILABLE UsrGroup THEN RETURN.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  FIND FIRST Usr WHERE Usr.UserName = INPUT sel_avail.
  CREATE UsrGroupMember.
  ASSIGN
    UsrGroupMember.GroupName = UsrGroup.GroupName
    UsrGroupMember.UserName  = Usr.UserName .
    
  sel_curr:ADD-LAST( UsrGroupMember.UserName ) .
  sel_curr:SCREEN-VALUE = UsrGroupMember.UserName.
  sel_avail:DELETE( INPUT sel_avail ) .

  RUN update-buttons.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).
  
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'GroupName':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = UsrGroup
           &WHERE = "WHERE UsrGroup.GroupName eq key-value"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-initialize V-table-Win 
PROCEDURE inst-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  this-win = {&WINDOW-NAME}:HANDLE .
  RUN top-most-changed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-row-available V-table-Win 
PROCEDURE inst-row-available :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN dispatch( 'find-using-key':u ).
  IF NOT AVAILABLE UsrGroup THEN RETURN.

  CURRENT-WINDOW:TITLE = "Members - " + UsrGroup.GroupName.  
  IF ROWID( UsrGroup ) <> last-id THEN RUN refresh-members.
  last-id = ROWID( UsrGroup ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pre-row-available V-table-Win 
PROCEDURE pre-row-available :
/*------------------------------------------------------------------------------
  Purpose:  Override toolkit behaviour
------------------------------------------------------------------------------*/

  have-records = No.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-members V-table-Win 
PROCEDURE refresh-members :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  sel_avail:LIST-ITEMS = "".
  sel_curr:LIST-ITEMS = "".
  
  IF NOT AVAILABLE UsrGroup THEN RETURN.

  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).

  FOR EACH Usr NO-LOCK:
    IF CAN-FIND( UsrGroupMember WHERE UsrGroupMember.UserName = Usr.UserName
                     AND UsrGroupMember.GroupName = UsrGroup.GroupName ) THEN
      sel_curr:ADD-LAST( Usr.UserName ).
    ELSE
      sel_avail:ADD-LAST( Usr.UserName ).
  END.

  RUN update-buttons.

  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-member V-table-Win 
PROCEDURE remove-member :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:

  IF NOT AVAILABLE UsrGroup THEN RETURN.
  
  RUN notify( 'set-busy, CONTAINER-SOURCE':U ).
  
  FIND FIRST Usr WHERE Usr.UserName = INPUT sel_curr.
  FIND UsrGroupMember OF UsrGroup WHERE UsrGroupMember.UserName = Usr.UserName EXCLUSIVE-LOCK.
  DELETE UsrGroupMember.

  sel_avail:ADD-LAST( INPUT sel_curr ) .
  sel_avail:SCREEN-VALUE = INPUT sel_curr.
  sel_curr:DELETE( INPUT sel_curr ) .
  
  RUN update-buttons.
  RUN notify( 'set-idle, CONTAINER-SOURCE':U ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "GroupName" "UsrGroup" "GroupName"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartViewer, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE top-most-changed V-table-Win 
PROCEDURE top-most-changed :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF INPUT FRAME {&FRAME-NAME} tgl_topmost THEN
    RUN notify( 'set-topmost,container-source':U ).
  ELSE
    RUN notify( 'reset-topmost,container-source':U ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE update-buttons V-table-Win 
PROCEDURE update-buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO WITH FRAME {&FRAME-NAME}:
  btn_add:SENSITIVE = sel_avail:SCREEN-VALUE <> "" AND sel_avail:SCREEN-VALUE <> ?.
  btn_rem:SENSITIVE = sel_curr:SCREEN-VALUE <> ""  AND sel_curr:SCREEN-VALUE <> ?.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


