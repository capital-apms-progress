&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : rentchg.p
    Purpose     : Create monthly charges for Rent and Outgoings
    Author(s)   : Andrew McMillan
    Created     : 27/11/96
  ------------------------------------------------------------------------*/
DEF INPUT PARAMETER process-options AS CHAR NO-UNDO.

DEF VAR charge-type AS CHAR NO-UNDO.
DEF VAR charge-period LIKE Month.MonthCode NO-UNDO.
DEF VAR preview AS LOGICAL NO-UNDO.
DEF VAR frequency-list AS CHAR NO-UNDO.
RUN parse-options.

DEF VAR this-document LIKE NewDocument.DocumentCode NO-UNDO.
DEF VAR this-transaction LIKE NewAcctTrans.TransactionCode NO-UNDO.
DEF VAR debit-total LIKE AcctTran.Amount NO-UNDO.
DEF VAR og-total LIKE AcctTran.Amount NO-UNDO.
DEF VAR rent-total LIKE AcctTran.Amount NO-UNDO.
DEF VAR tax-total LIKE AcctTran.Amount NO-UNDO.
DEF VAR untaxed-total LIKE AcctTran.Amount NO-UNDO.
DEF VAR tax-amount LIKE AcctTran.Amount NO-UNDO.
DEF VAR property-total LIKE AcctTran.Amount NO-UNDO.
DEF VAR tenant-total LIKE AcctTran.Amount NO-UNDO.
DEF VAR rent-description AS CHAR NO-UNDO.
DEF VAR og-description AS CHAR NO-UNDO.
DEF VAR line AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR rows AS INT NO-UNDO.
DEF VAR cols AS INT NO-UNDO.
DEF VAR last-property AS INT INITIAL -1 NO-UNDO.
DEF VAR partial-period AS LOGICAL INITIAL No NO-UNDO.


{inc/ofc-this.i}
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}
{inc/ofc-acct.i "RENT" "rent-account"}
{inc/ofc-acct.i "OUTGOINGS" "og-account"}
DEF VAR gst-entity      LIKE AcctTran.EntityCode  NO-UNDO.
IF Office.GST <> ? THEN DO:
  {inc/ofc-acct.i "GST-OUT" "gst-account"}
  gst-entity = OfficeControlAccount.EntityCode.
END.
{inc/ofc-set.i "RentCharge-Type" "set-type"}
IF set-type <> "Accounts" THEN set-type = "Single".
{inc/ofc-set.i "RentCharge-Sequence" "set-sequence"}
IF set-sequence <> "PropertyCode" AND set-sequence <> "TenantCode" THEN
  set-sequence = "Region".
{inc/ofc-set.i "RentCharge-Partial" "set-partial"}
{inc/ofc-set-l.i "GST-Multi-Company"  "GST-Multi-Company" }


DEF VAR rent-charge-routine AS CHAR NO-UNDO.
rent-charge-routine = "charge-rental-" + set-type.
IF THIS-PROCEDURE:GET-SIGNATURE( rent-charge-routine ) = "" THEN DO:
  MESSAGE "The rent charge type '" + set-type "' is not valid." SKIP(1)
          "Set it to 'Single' or 'Accounts'"
          VIEW-AS ALERT-BOX TITLE "Invalid RentCharge-Type setting for office.".
  RETURN.
END.

DEF VAR batch-string AS CHAR NO-UNDO.
DEF VAR date-string AS CHAR NO-UNDO.
FIND Month WHERE Month.MonthCode = charge-period NO-LOCK.
batch-string = (IF INDEX( charge-type, "R") > 0 THEN "Rent" ELSE "").
IF INDEX( charge-type, "O") > 0 THEN
  batch-string = batch-string + (IF INDEX( charge-type, "R") > 0 THEN " & " ELSE "") + "Outgoings".
batch-string = batch-string + " to " + STRING( Month.EndDate, "99/99/9999").

date-string = " " + STRING( Month.StartDate, "99/99/9999") + " to " + STRING( Month.EndDate, "99/99/9999").
DEF VAR tenant-date-string AS CHAR NO-UNDO.

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
FIND Usr WHERE Usr.UserName = user-name NO-LOCK NO-ERROR.

/* Doing this here ensures that Batch is scoped to the whole program */
DEF VAR batch-code LIKE NewBatch.BatchCode NO-UNDO.
FIND LAST Batch NO-LOCK NO-ERROR.
batch-code = 1 + (IF AVAILABLE(Batch) THEN Batch.BatchCode ELSE 0).
FIND LAST NewBatch NO-LOCK NO-ERROR.
IF AVAILABLE(NewBatch) AND NewBatch.BatchCode >= batch-code THEN batch-code = NewBatch.BatchCode + 1.
IF NOT preview THEN DO:
  CREATE NewBatch.
  ASSIGN
    NewBatch.BatchCode = batch-code
    NewBatch.BatchType = "AUTO"
    NewBatch.Description = batch-string
    NewBatch.PersonCode = (IF AVAILABLE(Usr) THEN Usr.PersonCode ELSE 0)
    NewBatch.DocumentCount = 0
    NewBatch.Total = 0
  .
END.

/* Doing this here ensures that NewAcctTrans is scoped to the whole program */
FIND FIRST NewAcctTrans NO-LOCK NO-ERROR.

&SCOPED-DEFINE page-width 180
&SCOPED-DEFINE with-clause NO-BOX USE-TEXT NO-LABELS WIDTH {&page-width}

DEF VAR timeStamp AS CHAR FORMAT "X(44)" NO-UNDO.
timeStamp = STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.
DEF VAR hline2 AS CHAR FORMAT "X({&page-width})" NO-UNDO.
hline2 = "Rent Charge Preview: " + batch-string.
hline2 = SUBSTRING( STRING("","X({&page-width})"), 1, INTEGER(({&page-width} - LENGTH(hline2) ) / 2)) + hline2.

DEFINE FRAME heading-frame WITH 1 DOWN {&with-clause} PAGE-TOP.
FORM HEADER
    timeStamp  "Page " + STRING( PAGE-NUMBER ) TO {&page-width} SKIP (1)
    hline2 FORMAT "X({&page-width})"
    SKIP (1)
    WITH FRAME heading-frame.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 25.15
         WIDTH              = 39.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
ON WRITE OF NewAcctTrans OVERRIDE DO: END.
ON WRITE OF NewDocument OVERRIDE DO: END.

RUN make-control-string ( "PCL", "reset,landscape,tm,1,a4,lm,4,courier,cpi,18,lpi,9",
                OUTPUT line, OUTPUT rows, OUTPUT cols ).

RUN output-control-file ( line ).
OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE VALUE(rows).
IF preview THEN VIEW FRAME heading-frame.

this-document = 0.
debit-total = 0.

&SCOPED-DEFINE F-E-DEFINITION ~
FOR EACH Property NO-LOCK ~
    WHERE Property.Active AND (NOT Property.ExternallyManaged),~
    EACH Tenant NO-LOCK~
        WHERE Tenant.Active AND Tenant.EntityType = "P"~
        AND Tenant.EntityCode = Property.PropertyCode~
        AND CAN-FIND( FIRST TenancyLease~
             WHERE TenancyLease.TenantCode = Tenant.TenantCode~
             AND TenancyLease.LeaseStatus <> "PAST"~
             AND CAN-FIND( FIRST RentalSpace OF TenancyLease WHERE RentalSpace.AreaStatus <> "V") )~
/* SCOPED-DEFINE ends */

IF set-sequence = "PropertyCode":U THEN DO:
  {&F-E-DEFINITION} BY Property.PropertyCode BY Tenant.TenantCode:
    RUN each-tenant.
  END.
END.
ELSE IF set-sequence = "TenantCode":U THEN DO:
  {&F-E-DEFINITION} BY Tenant.TenantCode:
    RUN each-tenant.
  END.
END.
ELSE DO:
  {&F-E-DEFINITION} BY Property.Region BY Property.ShortName:
    RUN each-tenant.
  END.
END.

IF preview THEN DO:

  RUN after-each-property( last-property, property-total ).

  line = "".
  RUN output-line.
  line = "Total outgoings = " + STRING( og-total, "->>>,>>>,>>9.99").
  RUN output-line.
  line = "Total rentals   = " + STRING( rent-total, "->>>,>>>,>>9.99").
  RUN output-line.
  line = "Total un-taxed  = " + STRING( untaxed-total, "->>>,>>>,>>9.99").
  RUN output-line.
  line = "Total GST       = " + STRING( tax-total, "->>>,>>>,>>9.99").
  RUN output-line.
  line = "".
  RUN output-line.
  line = "Total for batch = " + STRING( debit-total, "->>>,>>>,>>9.99").
  RUN output-line.
  OUTPUT CLOSE.
  RUN view-output-file ( preview ).
END.
ELSE DO:

  OUTPUT CLOSE.

  OS-DELETE VALUE( txtrep-control-file ).
  OS-DELETE VALUE( txtrep-print-file ).

  ASSIGN
    NewBatch.DocumentCount = this-document
    NewBatch.Total = debit-total
  .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-after-each-property) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE after-each-property Procedure 
PROCEDURE after-each-property :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER last-code AS INT NO-UNDO.
DEF INPUT PARAMETER total-amt AS DEC NO-UNDO.

  IF last-code > -1 THEN DO:
    line = "Total rental for P" + STRING(last-code,"99999") + " = " + STRING( total-amt, "->>>,>>>,>>9.99") + "      " + STRING( (total-amt * 12), "->>>,>>>,>>9.99") + " p.a.".
    RUN output-line.
    line = "".
    RUN output-line.
    RUN output-line.
  END.

  IF AVAILABLE(Property) THEN DO:
    line = STRING( Property.PropertyCode, "99999") + "  "
         + Property.Name .
    RUN output-line.
    line = FILL( "=", LENGTH(line)).
    RUN output-line.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-calculate-outgoings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculate-outgoings Procedure 
PROCEDURE calculate-outgoings :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER  tenant-code LIKE Tenant.TenantCode NO-UNDO.
DEF OUTPUT PARAMETER TotalOutGoings AS DEC NO-UNDO INITIAL 0.

DEF VAR part-of-month AS DEC NO-UNDO.
DEF VAR og-amount AS DEC NO-UNDO.

  FOR EACH TenancyLease WHERE TenancyLease.TenantCode = tenant-code
                        AND TenancyLease.PropertyCode = Property.PropertyCode
                        AND TenancyLease.LeaseStatus <> "PAST" 
                        AND (TenancyLease.LeaseStartDate < Month.EndDate OR
                             TenancyLease.LeaseStartDate = ?) NO-LOCK:
    RUN calculate-part-of-month( OUTPUT part-of-month).
    og-amount = ROUND(((TenancyLease.OutgoingsBudget * part-of-month) / 12), 2).
    TotalOutgoings = TotalOutgoings + og-amount.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-calculate-part-of-month) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculate-part-of-month Procedure 
PROCEDURE calculate-part-of-month :
/*------------------------------------------------------------------------------
  Purpose:  Calculate the portion of the month for which rent is due.
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER part-of-month AS DECIMAL NO-UNDO.

/* 27 days = 2219.18 (where full month = $2500) */

DEF VAR start-day AS DATE NO-UNDO.
DEF VAR stop-day AS DATE NO-UNDO.
DEF VAR month-days AS INT NO-UNDO.
month-days = (Month.EndDate - Month.StartDate) + 1.

ASSIGN  part-of-month = 1
        stop-day = Month.EndDate
        start-day = Month.StartDate.

IF TenancyLease.RentEndDate <> ?  THEN stop-day = MINIMUM( stop-day, TenancyLease.RentEndDate ).

IF TenancyLease.FirstLeaseStart <> ? THEN start-day = MAXIMUM( start-day, TenancyLease.FirstLeaseStart ).
ELSE IF TenancyLease.LeaseStartDate <> ? THEN start-day = MAXIMUM( start-day, TenancyLease.LeaseStartDate ).
IF TenancyLease.RentStartDate <> ?  THEN start-day = MAXIMUM( start-day, TenancyLease.RentStartDate ).

IF start-day = Month.StartDate AND stop-day = Month.EndDate THEN RETURN.

part-of-month = ((1 + stop-day - start-day) / 365 ) * 12.
IF part-of-month >= 1 THEN
  part-of-month = 1.
ELSE IF part-of-month <= 0 THEN
  part-of-month = 0.

/* IF we're set to not do it then just return with part = 1 */
IF set-partial = "No" AND part-of-month <> 0 THEN ASSIGN
  start-day = Month.StartDate
  stop-day = Month.EndDate
  part-of-month = 1.

IF part-of-month > 0 AND part-of-month < 1 THEN DO:
  partial-period = Yes.
  tenant-date-string = " " + STRING( start-day, "99/99/9999") + " to " + STRING( stop-day, "99/99/9999").
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-calculate-rental) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculate-rental Procedure 
PROCEDURE calculate-rental :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER tenant-code    LIKE Tenant.TenantCode NO-UNDO.
DEF OUTPUT PARAMETER rent-amount    LIKE AcctTran.Amount   NO-UNDO.
DEF OUTPUT PARAMETER contract-rent  LIKE AcctTran.Amount   NO-UNDO.
DEF OUTPUT PARAMETER taxable-amount AS DECIMAL NO-UNDO INITIAL 0.

DEF VAR part-of-month AS DEC NO-UNDO.
DEF VAR month-charge AS DEC NO-UNDO.
DEF VAR month-contract AS DEC NO-UNDO.

  rent-amount = 0.
  contract-rent = 0.
  FOR EACH TenancyLease WHERE TenancyLease.TenantCode = tenant-code
                        AND TenancyLease.PropertyCode = Property.PropertyCode
                        AND TenancyLease.LeaseStatus <> "PAST" 
                        AND (TenancyLease.LeaseStartDate < Month.EndDate OR
                             TenancyLease.LeaseStartDate = ?) NO-LOCK,
      EACH RentalSpace WHERE RentalSpace.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
                        AND RentalSpace.AreaStatus <> "V" NO-LOCK:

    RUN calculate-part-of-month( OUTPUT part-of-month).

    ASSIGN
      rent-amount   = rent-amount + (RentalSpace.ChargedRental * part-of-month)
      contract-rent = contract-rent + (RentalSpace.ContractedRental * part-of-month)
      taxable-amount = taxable-amount + (IF TenancyLease.TaxApplies THEN (RentalSpace.ChargedRental * part-of-month) ELSE 0)
    .
  END.
  rent-amount = INTEGER((rent-amount / 12) * 100) / 100.
  contract-rent = INTEGER((contract-rent / 12) * 100) / 100.
  taxable-amount = INTEGER((taxable-amount / 12) * 100) / 100.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-calculate-typed-rental) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculate-typed-rental Procedure 
PROCEDURE calculate-typed-rental :
/*------------------------------------------------------------------------------
  Purpose:  Calculate rentals broken down by area type
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER tenant-code LIKE Tenant.TenantCode NO-UNDO.
DEF OUTPUT PARAMETER rental-types AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER rental-amounts AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER rental-descriptions AS CHAR NO-UNDO.
DEF OUTPUT PARAMETER taxable-amount AS DECIMAL NO-UNDO INITIAL 0.

DEF VAR area-type AS CHAR NO-UNDO.
DEF VAR type-pos AS INT NO-UNDO.
DEF VAR rent-amount AS DECIMAL NO-UNDO.
DEF VAR part-of-month AS DEC NO-UNDO.

  FOR EACH TenancyLease WHERE TenancyLease.TenantCode = tenant-code
                        AND TenancyLease.LeaseStatus <> "PAST"
                        AND TenancyLease.PropertyCode = Property.PropertyCode 
                        AND (TenancyLease.LeaseStartDate < Month.EndDate OR
                             TenancyLease.LeaseStartDate = ?) NO-LOCK,
      EACH RentalSpace WHERE RentalSpace.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
                        AND RentalSpace.AreaStatus <> "V" NO-LOCK:
    area-type = (IF RentalSpace.ChargedRental < 0 THEN "-" ELSE "+")
              + RentalSpace.Areatype.
    type-pos = INDEX( rental-types, area-type) / 2.
    IF type-pos = 0 OR RentalSpace.AreaType = "S" THEN DO:
      rental-types = rental-types + area-type.
      type-pos = LENGTH( rental-types ) / 2.
      rental-amounts = rental-amounts + (IF rental-amounts = "" THEN "" ELSE ",") + "0".
      rental-descriptions = rental-descriptions + (IF rental-descriptions = "" THEN "" ELSE ",") + RentalSpace.Description.
    END.
    RUN calculate-part-of-month( OUTPUT part-of-month).
    rent-amount = DECIMAL( ENTRY( type-pos, rental-amounts)).
    IF TenancyLease.TaxApplies THEN
      taxable-amount = taxable-amount + ROUND( (RentalSpace.ChargedRental * part-of-month) / 12, 2).
    rent-amount = rent-amount + ROUND( (RentalSpace.ChargedRental * part-of-month) / 12, 2).
    ENTRY( type-pos, rental-amounts) = STRING( rent-amount, "-999999999.99").
    IF preview THEN DO:
      line = FILL(" ", 6) + RentalSpace.AreaType + "  "
                          + STRING( RentalSpace.Description, "X(40)") + " "
                          + STRING( ROUND( ((RentalSpace.ChargedRental * part-of-month) / 12), 2), "->>,>>>,>>9.99").
      RUN output-line.
    END.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-charge-outgoings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE charge-outgoings Procedure 
PROCEDURE charge-outgoings :
/*------------------------------------------------------------------------------
  Purpose:  Create a transaction charging outgoings
------------------------------------------------------------------------------*/
DEF VAR og-amount     AS DECIMAL NO-UNDO.
DEF VAR part-of-month AS DEC NO-UNDO.

  RUN calculate-outgoings( Tenant.TenantCode, OUTPUT og-amount ).

  IF og-amount <> 0 THEN DO:
    property-total = property-total + og-amount.
    og-total = og-total + og-amount.

    og-description = "Outgoings from" + tenant-date-string.
    RUN create-transaction( "T", Tenant.TenantCode, sundry-debtors,
                            Month.StartDate,
                            "T" + STRING( Tenant.TenantCode, "99999"),
                            og-amount,
                            og-description,
                            og-amount
                          ).
    RUN create-transaction( Tenant.EntityType, Tenant.EntityCode, og-account,
                            Month.StartDate,
                            "T" + STRING( Tenant.TenantCode, "99999"),
                            - og-amount,
                            og-description,
                            - og-amount
                          ).

    RUN charge-tax( "T", Tenant.TenantCode, sundry-debtors,
                            Month.StartDate,
                            "T" + STRING( Tenant.TenantCode, "99999"),
                            og-amount,
                            og-description
                          ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-charge-rental-accounts) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE charge-rental-accounts Procedure 
PROCEDURE charge-rental-accounts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR rental-types AS CHAR NO-UNDO.
DEF VAR rental-amounts AS CHAR NO-UNDO.
DEF VAR area-descriptions AS CHAR NO-UNDO.

DEF VAR no-types AS INT NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR this-type AS CHAR NO-UNDO.
DEF VAR this-rent AS DEC NO-UNDO.
DEF VAR tenant-rent-total AS DEC INITIAL 0.00 NO-UNDO.
DEF VAR taxable AS DECIMAL NO-UNDO.
DEF VAR descr AS CHAR NO-UNDO.

  RUN calculate-typed-rental( Tenant.TenantCode, OUTPUT rental-types, OUTPUT rental-amounts, OUTPUT area-descriptions, OUTPUT taxable).

  line = "".
  RUN output-line.

  rent-description = "Rental from" + tenant-date-string.
  no-types = LENGTH( rental-types ) / 2.
  DO i = 1 TO no-types:
    this-type = SUBSTRING( rental-types, (2 * i), 1).
    this-rent = DECIMAL( ENTRY( i, rental-amounts) ).
    FIND AreaType WHERE AreaType.AreaType = this-type NO-LOCK.
    IF this-type = "S" THEN
      descr = ENTRY( i, area-descriptions) + tenant-date-string .
    ELSE
      descr = AreaType.Description + (IF this-rent < 0 THEN " credit" ELSE "") + tenant-date-string .

    RUN create-transaction( "T", Tenant.TenantCode, sundry-debtors,
                            Month.StartDate,
                            "T" + STRING( Tenant.TenantCode, "99999"),
                            this-rent,
                            descr,
                            this-rent
                          ).

    RUN create-transaction( Tenant.EntityType, Tenant.EntityCode, AreaType.AccountCode,
                            Month.StartDate,
                            "T" + STRING( Tenant.TenantCode, "99999"),
                            - this-rent,
                            descr,
                            - this-rent
                          ).
    tenant-rent-total = tenant-rent-total + this-rent.
  END.
  property-total = property-total + tenant-rent-total.
  rent-total = rent-total + tenant-rent-total.

  IF taxable <> 0 THEN
    RUN charge-tax( "T", Tenant.TenantCode, sundry-debtors, Month.StartDate,
            "T" + STRING( Tenant.TenantCode, "99999"), taxable, rent-description ).

  IF Office.GST <> ? THEN
    untaxed-total = untaxed-total + this-rent - taxable.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-charge-rental-single) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE charge-rental-single Procedure 
PROCEDURE charge-rental-single :
/*------------------------------------------------------------------------------
  Purpose:  Create a transaction charging rental.
------------------------------------------------------------------------------*/
DEF VAR rent-amount     AS DECIMAL NO-UNDO.
DEF VAR contract-rent   AS DECIMAL NO-UNDO.
DEF VAR taxable         AS DECIMAL NO-UNDO.

  RUN calculate-rental( Tenant.TenantCode, OUTPUT rent-amount, OUTPUT contract-rent, OUTPUT taxable ).
  IF rent-amount <> 0 THEN DO:
    property-total = property-total + rent-amount.
    rent-total = rent-total + rent-amount.

    rent-description = "Rental from" + tenant-date-string.
    RUN create-transaction( "T", Tenant.TenantCode, sundry-debtors,
                            Month.StartDate,
                            "T" + STRING( Tenant.TenantCode, "99999"),
                            rent-amount,
                            rent-description,
                            taxable
                          ).

    RUN create-transaction( Tenant.EntityType, Tenant.EntityCode, rent-account,
                            Month.StartDate,
                            "T" + STRING( Tenant.TenantCode, "99999"),
                            - rent-amount,
                            rent-description,
                            - taxable
                          ).

    IF taxable > 0 THEN
      RUN charge-tax( "T", Tenant.TenantCode, sundry-debtors, Month.StartDate,
            "T" + STRING( Tenant.TenantCode, "99999"), taxable, rent-description ).

    IF Office.GST <> ? THEN
      untaxed-total = untaxed-total + rent-amount - taxable.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-charge-tax) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE charge-tax Procedure 
PROCEDURE charge-tax :
/*------------------------------------------------------------------------------
  Purpose:  Create a transaction charging tax
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER et LIKE NewAcctTrans.EntityType NO-UNDO.
DEF INPUT PARAMETER ec LIKE NewAcctTrans.EntityCode NO-UNDO.
DEF INPUT PARAMETER ac LIKE NewAcctTrans.AccountCode NO-UNDO.
DEF INPUT PARAMETER dat LIKE NewAcctTrans.Date NO-UNDO.
DEF INPUT PARAMETER ref LIKE NewAcctTrans.Reference NO-UNDO.
DEF INPUT PARAMETER amt LIKE NewAcctTrans.Amount NO-UNDO.
DEF INPUT PARAMETER dsc LIKE NewAcctTrans.Description NO-UNDO.

DEF VAR v-gst-entity LIKE NewAcctTrans.EntityCode NO-UNDO.

  IF Office.GST = 0 OR amt = 0 THEN RETURN.
  amt = amt * Office.GST / 100.
  tax-total = tax-total + amt.
  RUN create-transaction ( et, ec, ac,
                           dat, ref, amt, "GST, " + dsc, amt ).

  /* Find the ledger to allocate GST  */
  IF GST-Multi-company = YES  THEN DO:
      /*FIND Property WHERE Property.PropertyCode = ec NO-LOCK.*/
      ASSIGN v-gst-entity  = Property.CompanyCode.
  END.
  ELSE   v-gst-entity = gst-entity.

  RUN create-transaction ( "L", v-gst-entity, gst-account,
                           dat, ref, - amt, "GST, " + dsc, - amt ).

 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-create-transaction) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-transaction Procedure 
PROCEDURE create-transaction :
/*------------------------------------------------------------------------------
  Purpose:  Create a NewTransaction record
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER et LIKE NewAcctTrans.EntityType NO-UNDO.
DEF INPUT PARAMETER ec LIKE NewAcctTrans.EntityCode NO-UNDO.
DEF INPUT PARAMETER ac LIKE NewAcctTrans.AccountCode NO-UNDO.
DEF INPUT PARAMETER dat LIKE NewAcctTrans.Date NO-UNDO.
DEF INPUT PARAMETER ref LIKE NewAcctTrans.Reference NO-UNDO.
DEF INPUT PARAMETER amt LIKE NewAcctTrans.Amount NO-UNDO.
DEF INPUT PARAMETER dsc LIKE NewAcctTrans.Description NO-UNDO.
DEF INPUT PARAMETER tax-amount LIKE NewAcctTrans.Amount NO-UNDO.

  IF amt > 0 THEN debit-total = debit-total + amt.
  IF preview THEN DO:
    IF et = "P" OR
      (et = "L" AND ec = gst-entity AND ac = gst-account AND set-type = "ACCOUNTS":U)
    THEN DO:
      line = "    T"
           + STRING( Tenant.TenantCode, "99999") + "   "
           + STRING( ac, "9999.99") + "  "
           + STRING( dsc, "X(50)" ) + "  "
           + STRING( - amt, "->>>,>>>,>>9.99" ) + "  "
           + STRING( Tenant.Name, "X(50)")
           + (IF partial-period THEN " part period" ELSE FILL(" ", 12) ) + " "
           + (IF Office.GST <> ? AND tax-amount <> amt THEN STRING( - (tax-amount * Office.GST / 100), "->>>,>>>,>>9.99" ) ELSE "").
      RUN output-line.
      tenant-total = tenant-total - amt.
    END.
    RETURN.
  END.
  IF this-transaction = 0 THEN DO:
    this-document = this-document + 1.
    CREATE NewDocument.
    ASSIGN
      NewDocument.BatchCode = batch-code
      NewDocument.DocumentCode = this-document
      NewDocument.DocumentType = "RENT"
      NewDocument.Reference = ( et + STRING( ec, "99999") )
      NewDocument.Description = NewBatch.Description
    .
    RELEASE NewDocument.
  END.

  this-transaction = this-transaction + 1.
  CREATE NewAcctTrans.
   ASSIGN
    NewAcctTrans.BatchCode       = batch-code
    NewAcctTrans.DocumentCode    = this-document
    NewAcctTrans.TransactionCode = this-transaction
    NewAcctTrans.EntityType      = et
    NewAcctTrans.EntityCode      = ec
    NewAcctTrans.AccountCode     = ac
    NewAcctTrans.Date            = dat
    NewAcctTrans.Reference       = ref
    NewAcctTrans.Amount          = amt
    NewAcctTrans.Description     =  dsc
  .

  RELEASE NewAcctTrans.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-tenant) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-tenant Procedure 
PROCEDURE each-tenant :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  this-transaction = 0.
  partial-period = No.

  IF preview THEN DO:
    IF Property.PropertyCode <> last-property THEN DO:
      RUN after-each-property( last-property, property-total ).
      last-property = Property.PropertyCode.
      property-total = 0.
    END.
    IF set-type = "ACCOUNTS":U THEN DO:
      line = Tenant.Name + "  (T" + STRING( Tenant.TenantCode, "99999") + ")".
      RUN output-line.
    END.
    tenant-total = 0.
  END.

  tenant-date-string = date-string.
  IF INDEX( charge-type, "R") > 0 THEN RUN VALUE(rent-charge-routine) IN THIS-PROCEDURE.
  IF INDEX( charge-type, "O") > 0 THEN RUN charge-outgoings.

  IF preview AND set-type = "ACCOUNTS":U THEN DO:
    line = FILL( " ", 74) + "===============".
    RUN output-line.
    line = "Total charge for tenant " + STRING( Tenant.TenantCode, "99999")
         + FILL( " ", 45) + STRING( tenant-total, "->>>,>>>,>>9.99").
    RUN output-line.
    line = "".
    RUN output-line.
    RUN output-line.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-output-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE output-line Procedure 
PROCEDURE output-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF line = "" THEN
    PUT SKIP(1).
  ELSE
    PUT UNFORMATTED line SKIP.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-options) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-options Procedure 
PROCEDURE parse-options :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
/*
DEF VAR charge-type AS CHAR NO-UNDO.
DEF VAR charge-period LIKE Month.MonthCode NO-UNDO.
DEF VAR preview AS LOGICAL NO-UNDO.
DEF VAR frequency-list AS CHAR NO-UNDO.
*/

{inc/showopts.i "process-options"}

  charge-type = ENTRY( 1, process-options, "~n").
  charge-period = INT( ENTRY( 2, process-options, "~n")).
  preview = (ENTRY( 3, process-options, "~n") = "Yes").
  frequency-list = ENTRY( 4, process-options, "~n").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


