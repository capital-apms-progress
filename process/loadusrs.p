&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : process/loadusrs.p
    Purpose     : Reload local user files after a copy from another site
  ------------------------------------------------------------------------*/

DEF VAR file-list AS CHAR NO-UNDO.
file-list = "usr.d,usrgrpmm.d".

DEF VAR this-office  AS CHAR NO-UNDO     INITIAL "".
DEF VAR prefix       AS CHAR NO-UNDO     INITIAL "".
DEF VAR cur-printer  AS CHAR NO-UNDO     INITIAL "".
DEF VAR cur-port     AS CHAR NO-UNDO     INITIAL "".
DEF VAR collision-detect AS LOGI NO-UNDO INITIAL No.
DEF VAR override-rules AS LOGI NO-UNDO   INITIAL No.
DEF VAR batch-mode AS LOGICAL NO-UNDO.
DEF VAR debug-mode AS LOGICAL NO-UNDO INITIAL Yes.

{inc/ofc-this.i}
DEF VAR old-office-code AS CHAR NO-UNDO.
old-office-code = Office.OfficeCode.

RUN parse-parameters.

DEFINE STREAM debug-stream.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-debug-event) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD debug-event Procedure 
FUNCTION debug-event RETURNS CHARACTER
  ( INPUT event-text AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 25.05
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

debug-event("Starting").

RUN load-local-users.
RUN copy-office.
RUN reset-printers.
RUN set-replication.

debug-event("Finished").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-copy-office) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copy-office Procedure 
PROCEDURE copy-office :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF this-office = "" THEN RETURN.

DEF BUFFER old-office FOR Office.
DEF BUFFER old-setting FOR OfficeSetting.
DEF BUFFER old-control FOR OfficeControlAccount.

  FIND old-office WHERE old-office.OfficeCode = old-office-code NO-LOCK NO-ERROR.
  FIND Office WHERE Office.OfficeCode = this-office NO-ERROR.
  IF NOT AVAILABLE(Office) THEN DO:
    CREATE Office.
    BUFFER-COPY old-office EXCEPT old-office.ThisOffice TO Office
                ASSIGN Office.OfficeCode = this-office.
  END.

  FOR EACH old-setting OF old-office NO-LOCK:
    FIND OfficeSetting OF Office WHERE OfficeSetting.SetName = old-setting.SetName NO-ERROR.
    IF NOT AVAILABLE(OfficeSetting) THEN DO:
      CREATE OfficeSetting.
      OfficeSetting.OfficeCode = this-office.
      OfficeSetting.SetName = old-setting.SetName.
    END.
    BUFFER-COPY old-setting EXCEPT old-setting.OfficeCode TO OfficeSetting.
  END.

  FOR EACH old-control OF old-office NO-LOCK:
    FIND OfficeControlAccount OF Office WHERE OfficeControlAccount.Name = old-control.Name NO-ERROR.
    IF NOT AVAILABLE(OfficeControlAccount) THEN DO:
      CREATE OfficeControlAccount.
      OfficeControlAccount.OfficeCode = this-office.
      OfficeControlAccount.Name = old-control.Name.
    END.
    BUFFER-COPY old-control EXCEPT old-control.OfficeCode TO OfficeControlAccount.
  END.

  Office.ThisOffice = Yes.
  debug-event( "Office settings copied").

  FIND OfficeSetting OF Office WHERE OfficeSetting.SetName = "WindowTitlePrefix" NO-ERROR.
  IF NOT AVAILABLE(OfficeSetting) THEN DO:
    CREATE OfficeSetting.
    OfficeSetting.OfficeCode = this-office.
    OfficeSetting.SetName = "WindowTitlePrefix".
  END.
  OfficeSetting.SetValue = prefix.
  debug-event( 'Prefix set to "' + prefix + '"' ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-delete-current-users) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-current-users Procedure 
PROCEDURE delete-current-users :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  debug-event( 'Deleting existing user records.' ).
  ON DELETE OF Usr OVERRIDE DO: END.
  FOR EACH Usr EXCLUSIVE-LOCK TRANSACTION:
    DELETE Usr.
  END.

  debug-event( 'Deleting existing usergroup membership records.' ).
  ON DELETE OF UsrGroupMember OVERRIDE DO: END.
  FOR EACH UsrGroupMember TRANSACTION:
    DELETE UsrGroupMember.
  END.

  debug-event( 'Existing user records deleted.' ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-load-local-users) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE load-local-users Procedure 
PROCEDURE load-local-users :
/*------------------------------------------------------------------------------
  Purpose:     Reload users local to the current site from the 'lnk' directory
------------------------------------------------------------------------------*/

  IF NOT batch-mode THEN DO:
    MESSAGE "Are you sure you want to load the users in?" SKIP
            "All current user information will be lost!"
            VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO
            TITLE "Confirm Reload" UPDATE reload-it AS LOGI.
  
    IF NOT reload-it THEN RETURN.
  END.

  RUN verify-files.
  IF RETURN-VALUE = "FAIL" THEN RETURN.

  RUN delete-current-users.

  DEF VAR i AS INT NO-UNDO.

  ON WRITE OF Usr OVERRIDE DO: END.
  INPUT FROM usr.d .
  REPEAT TRANSACTION:
    CREATE Usr.
    IMPORT Usr.
    debug-event("Added user '" + Usr.UserName + "'" ).
  END.
  INPUT CLOSE.


  /* There's a bit of mucking around here because when a usr record is created
   * some memberships may also be created automatically
   */
  ON WRITE OF UsrGroupMember OVERRIDE DO: END.
  INPUT FROM usrgrpmm.d .
  REPEAT ON ERROR UNDO,NEXT TRANSACTION:
    CREATE UsrGroupMember.
    IMPORT UsrGroupMember.
    debug-event("Added user group member '" + UsrGroupMember.UserName + "' of '" + UsrGroupMember.GroupName + "'" ).
  END.
  INPUT CLOSE.

  debug-event( 'Local users loaded' ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR token AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR process-options AS CHAR NO-UNDO.

  process-options = SESSION:PARAMETER .
  DO i = 1 TO NUM-ENTRIES( process-options, "|" ):
    token = ENTRY( i, process-options, "|" ).
    debug-event("Found token: " + token ).

    CASE ENTRY( 1, token ):
      WHEN "Office"  THEN this-office = ENTRY(2,token).
      WHEN "FromOffice"  THEN old-office-code = ENTRY(2,token).
      WHEN "Prefix"  THEN prefix = ENTRY(2,token).
      WHEN "Printer" THEN ASSIGN
        cur-printer = ENTRY(2,token)
        cur-port    = ENTRY(3,token).
      WHEN "CollisionDetect" THEN collision-detect = Yes.
      WHEN "OverrideRules" THEN override-rules = Yes.
    END CASE.

  END.

  IF prefix = "" AND this-office <> "" THEN prefix = TRIM(this-office) + ":".
  batch-mode = SESSION:BATCH-MODE .
  debug-mode = batch-mode OR debug-mode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reset-printers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-printers Procedure 
PROCEDURE reset-printers :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF cur-printer = "" THEN RETURN.
  FOR EACH RP WHERE RP.ReportID = "Current Printer":
    RP.Char1 = cur-printer.
    RP.Char2 = cur-port.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-replication) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-replication Procedure 
PROCEDURE set-replication :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR no-delete-tables AS CHAR NO-UNDO INITIAL "" /*"NewAcctTrans,NewBatch,NewDocument" */.
DEF VAR activity AS CHAR NO-UNDO.

  IF this-office = "" THEN RETURN.

  FOR EACH ReplTrigger NO-LOCK TRANSACTION:
    FIND ReplLoadRule WHERE ReplLoadRule.SourceSystem = old-office-code
                        AND ReplLoadRule.TableToLoad = ReplTrigger.TableToRepl
                        EXCLUSIVE-LOCK NO-ERROR.

    activity = ReplTrigger.Activity.
    IF LOOKUP(ReplTrigger.TableToRepl, no-delete-tables) > 0 THEN
      activity = REPLACE( activity, "D", "").

    IF NOT AVAILABLE(ReplLoadRule) THEN DO:
      CREATE ReplLoadRule.
      ASSIGN    ReplLoadRule.TableToLoad = ReplTrigger.TableToRepl
                ReplLoadRule.Activity = activity
                ReplLoadRule.SourceSystem = old-office-code
                ReplLoadRule.CollisionDetect = collision-detect.
    END.
    ELSE IF override-rules THEN DO:
      ASSIGN    ReplLoadRule.TableToLoad = ReplTrigger.TableToRepl
                ReplLoadRule.SourceSystem = old-office-code
                ReplLoadRule.Activity = activity
                ReplLoadRule.CollisionDetect = collision-detect.
    END.
  END.
  debug-event( 'Replication rules set.' ).

  FOR EACH ReplLog TRANSACTION:
    DELETE ReplLog.
  END.

  debug-event( 'Replication log cleared.' ).

DO TRANSACTION:
  FIND RP WHERE RP.UserName = "Replication-" + old-office-code
                AND RP.ReportID = "Last Replication File" EXCLUSIVE-LOCK NO-ERROR.
  IF AVAILABLE(RP) THEN DELETE RP.
END.
  debug-event( 'Last successful replication record deleted' ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-verify-files) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verify-files Procedure 
PROCEDURE verify-files :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR all-found AS LOGI NO-UNDO INIT Yes.
DEF VAR i         AS INT  NO-UNDO.
  
  debug-event("Verifying existence of usr files").
  DO i = 1 TO NUM-ENTRIES( file-list ):
    all-found = all-found AND SEARCH( ENTRY( i, file-list ) ) <> ?.
  END.
  
  IF NOT all-found THEN DO:
    debug-event("Files are missing").
    MESSAGE "At least one of the following files could not be found:" SKIP(1)
            file-list
            VIEW-AS ALERT-BOX ERROR
            TITLE "Import files missing".
    RETURN "FAIL".
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-debug-event) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION debug-event Procedure 
FUNCTION debug-event RETURNS CHARACTER
  ( INPUT event-text AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
IF NOT debug-mode THEN RETURN event-text.

DEF VAR time-text AS CHAR NO-UNDO.

  time-text = STRING( TODAY, "99/99/9999") + " " + STRING( TIME, "HH:MM:SS") + ": ".

  OUTPUT STREAM debug-stream TO loadusrs.log PAGE-SIZE 0 KEEP-MESSAGES APPEND.
  PUT STREAM debug-stream UNFORMATTED time-text event-text SKIP.
  OUTPUT STREAM debug-stream CLOSE.

  RETURN time-text + event-text.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

