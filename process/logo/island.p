&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : process/logo/agp.p
    Purpose     : Return the AGP logo or address

    Author(s)   : Andrew McMillan
    Created     : 8/12/97
    Notes       : Called from the office-address and office-logo routines
                  in inc/method/m-hpgl.i .
                  Note that these routines also need to use m-hpgl.i, just
                  not the routines that call themselves.
  ------------------------------------------------------------------------*/

DEF INPUT PARAMETER entity-type AS CHAR NO-UNDO.
DEF INPUT PARAMETER entity-code AS INT NO-UNDO.
DEF INPUT PARAMETER logo-type AS CHAR NO-UNDO.
DEF INPUT PARAMETER output-type AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-get-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-address Procedure 
FUNCTION get-address RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-logo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-logo Procedure 
FUNCTION get-logo RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15.63
         WIDTH              = 57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-hpgl.i}
{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

IF output-type = "pdf" THEN DO:
    RUN txtrep-output-mode( "pdf" ).
    RUN hpgl-output-mode( "pdf" ).
END.

CASE logo-type:
  WHEN "Address":U THEN RETURN get-address().
  WHEN "Logo":U THEN    RETURN get-logo().
END CASE.

RETURN "'" + logo-type + "' not understood".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-get-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-address Procedure 
FUNCTION get-address RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Get the office address
    Notes:  
------------------------------------------------------------------------------*/
  RUN hpgl-moveto( 77.5, 275 ).
/*
  RUN hpgl-text( 'helvetica,point,11,proportional,bold',
                                    "AUSTRALIAN GROWTH PROPERTIES LIMITED").

  RUN hpgl-move-relative( 35, -3 ).
  RUN hpgl-text( 'helvetica,point,5,proportional', "ACN 003 354 443" ).

  RUN hpgl-move-relative( -37.5, -2.5 ).
*/
  RUN hpgl-text( 'helvetica,point,5,proportional',
  "LEVEL 11, ATANASKOVIC HARTNELL HOUSE, 75-85 ELIZABETH STREET, SYDNEY NSW 2000 AUSTRALIA" ).

  RUN hpgl-move-relative( 9.5, -2 ).
  RUN hpgl-text( hpgl-last-font,
  "GPO BOX 4079, SYDNEY 2001, TELEPHONE 61-2-9223 5601, FACSIMILE 61-2-9223 5561" ).

  RUN hpgl-moveto( 0, 250 ).
  RETURN hpgl-codes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-logo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-logo Procedure 
FUNCTION get-logo RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RUN hpgl-moveto( 26.5, 272 ).
  RUN process/logo/sea.p ( entity-type, entity-code, "Logo", output-type ).
  RUN hpgl-append( RETURN-VALUE ).
  RUN hpgl-move-relative( 13, 8.4).
  RUN hpgl-text( 'helvetica,point,13.5,proportional,bold', "SEA Island").
  RUN hpgl-move-relative( 0, -4.2).
  RUN hpgl-text( 'helvetica,point,13.5,proportional,bold', "Holdings").
  RUN hpgl-move-relative( 0, -4.2).
  RUN hpgl-text( 'helvetica,point,13.5,proportional,bold', "Pty. Ltd.").

  RUN hpgl-moveto( 0, 250 ).
  RETURN hpgl-codes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

