&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : process/logo/ttpl.p
    Purpose     : Return the TTPL logo or address - 2006 version

    Author(s)   : Andrew McMillan
    Created     : 8/12/97
    Notes       : Called from the office-address and office-logo routines
                  in inc/method/m-hpgl.i .
                  Note that these routines also need to use m-hpgl.i, just
                  not the routines that call themselves.
  ------------------------------------------------------------------------*/

DEF INPUT PARAMETER entity-type AS CHAR NO-UNDO.
DEF INPUT PARAMETER entity-code AS INT NO-UNDO.
DEF INPUT PARAMETER logo-type AS CHAR NO-UNDO.
DEF INPUT PARAMETER output-type AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-get-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-address Procedure 
FUNCTION get-address RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-logo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-logo Procedure 
FUNCTION get-logo RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 10.42
         WIDTH              = 68.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-hpgl.i}
{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

IF output-type = "pdf" THEN DO:
    RUN txtrep-output-mode( "pdf" ).
    RUN hpgl-output-mode( "pdf" ).
END.

CASE logo-type:
  WHEN "Address":U THEN RETURN get-address().
  WHEN "Logo":U THEN    RETURN get-logo().
END CASE.

RETURN "'" + logo-type + "' not understood".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-get-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-address Procedure 
FUNCTION get-address RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Get the office address
    Notes:  
------------------------------------------------------------------------------*/
  RUN hpgl-moveto( 20, 8 ).

  RUN hpgl-move-relative( 0, -3 ).
  RUN hpgl-text( 'helvetica,point,6,proportional,normal',
    "t +64 9 303 3800  f +64 9 303 3927  www.ttp.co.nz").
  
  RUN hpgl-move-relative( 140, 0 ).
  RUN hpgl-text( hpgl-last-font,
    "A member of").

  RUN hpgl-move-relative( 13.5, 0 ).
  RUN hpgl-text( 'helvetica,point,6,proportional,bold',
    "SEA Group").
  
  RUN hpgl-moveto( 0, 250 ).
  RETURN hpgl-codes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-logo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-logo Procedure 
FUNCTION get-logo RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RUN hpgl-moveto( 27, 265 ).

  RUN hpgl-start-polygon.
  RUN hpgl-line-relative( -7, 0 ).
  RUN hpgl-line-relative( 0, 7 ).
  RUN hpgl-line-relative( 3, 0 ).
  RUN hpgl-line-relative( 0, -4 ).
  RUN hpgl-line-relative( 4, 0 ).
  RUN hpgl-line-relative( 0, -3 ).
  RUN hpgl-finish-polygon.
  RUN hpgl-fill-polygon( 70 ).

  RUN hpgl-move-relative( 1, 0).
  RUN hpgl-start-polygon.
  RUN hpgl-line-relative( 3, 3 ).
  RUN hpgl-line-relative( 0, 8 ).
  RUN hpgl-line-relative( -8, 0 ).
  RUN hpgl-line-relative( -3, -3 ).
  RUN hpgl-line-relative( 8, 0 ).
  RUN hpgl-line-relative( 0, -8 ).
  RUN hpgl-finish-polygon.
  RUN hpgl-fill-polygon( 100 ).

  RUN hpgl-move-relative( -1, 4.5).
  RUN hpgl-start-polygon.
  RUN hpgl-line-relative( -2.5, 0 ).
  RUN hpgl-line-relative( 0, 2.5 ).
  RUN hpgl-line-relative( 2.5, 0 ).
  RUN hpgl-line-relative( 0, -2.5 ).
  RUN hpgl-finish-polygon.
  RUN hpgl-fill-polygon( 100 ).

  RUN hpgl-move-relative( -3, 7).
  RUN hpgl-start-polygon.
  RUN hpgl-line-relative( 0, 0.5 ).
  RUN hpgl-line-relative( 3, 3 ).
  RUN hpgl-line-relative( 0, -3.5 ).
  RUN hpgl-line-relative( -3, 0 ).
  RUN hpgl-finish-polygon.
  RUN hpgl-fill-polygon( 100 ).

  RUN hpgl-move-relative( 4, 0.5).
  RUN hpgl-start-polygon.
  RUN hpgl-line-relative( 4, 0 ).
  RUN hpgl-line-relative( 0, -4 ).
  RUN hpgl-line-relative( 3, 0 ).
  RUN hpgl-line-relative( 0, 7 ).
  RUN hpgl-line-relative( -7, 0 ).
  RUN hpgl-line-relative( 0, -3 ).
  RUN hpgl-finish-polygon.
  RUN hpgl-fill-polygon( 50 ).

  RUN hpgl-move-relative( 3.5, -5).
  RUN hpgl-start-polygon.
  RUN hpgl-line-relative( 3.5, 0 ).
  RUN hpgl-line-relative( -3, -3 ).
  RUN hpgl-line-relative( -0.5, 0 ).
  RUN hpgl-line-relative( 0, 3 ).
  RUN hpgl-finish-polygon.
  RUN hpgl-fill-polygon( 100 ).

  RUN hpgl-move-relative( 8, -7).
  RUN hpgl-move-relative( 0, 5 ).
  RUN hpgl-text( 'helvetica,point,24,proportional,normal,bold',"TTP").
  RUN hpgl-move-relative( 75, 0).
  RUN hpgl-text( 'helvetica,point,11,proportional,normal,bold',"TRANS TASMAN PROPERTIES LIMITED").

  RUN hpgl-moveto( 0, 250 ).
  RETURN hpgl-codes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

