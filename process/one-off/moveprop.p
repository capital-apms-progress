DEF VAR old-code AS INT INITIAL 1027 NO-UNDO.
DEF VAR new-code AS INT INITIAL 201 NO-UNDO.
/*
DEF VAR old-code AS INT INITIAL 201 NO-UNDO.
DEF VAR new-code AS INT INITIAL 1035 NO-UNDO.
*/

/*FIND Property WHERE Property.PropertyCode = old-code. */

DO TRANSACTION:
/*
  FOR EACH RentalSpace OF Property:
    RentalSpace.PropertyCode = new-code.
  END.
  FOR EACH TenancyLease OF Property:
    TenancyLease.PropertyCode = new-code.
  END.
  FOR EACH Tenant WHERE Tenant.EntityCode = old-code:
    Tenant.EntityCode = new-code.
  END.
  FOR EACH PropertyOutgoing WHERE PropertyOutgoing.PropertyCode = old-code:
    PropertyOutgoing.PropertyCode = new-code.
  END.
  FOR EACH AcctTran WHERE AcctTran.EntityType = "P"
                      AND AcctTran.EntityCode = old-code:
    AcctTran.EntityCode = new-code.
  END.
  Property.PropertyCode = new-code.
*/
  FOR EACH AccountBalance WHERE AccountBalance.EntityType = "P"
                      AND AccountBalance.EntityCode = old-code:
    AccountBalance.EntityCode = new-code.
  END.
  FOR EACH AccountSummary WHERE AccountSummary.EntityType = "P"
                      AND AccountSummary.EntityCode = old-code:
    AccountSummary.EntityCode = new-code.
  END.
END.
