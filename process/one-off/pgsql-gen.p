&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : pgsql-gen.p
    Purpose     : Dump database suitable for loading into PostgreSQL

    Syntax      : Just run it :-)

    Description :

    Author(s)   : Andrew McMillan
    Created     :
    Notes       : (c) 1999 Andrew McMillan. Licensed under the GNU General
                  Public License version 2.
  ------------------------------------------------------------------------*/

DEF VAR dump-dir AS CHAR NO-UNDO INITIAL "D:/dump/sql".
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR bad-table AS LOGI NO-UNDO.

DEF VAR keywords AS CHAR NO-UNDO INITIAL "order".

DEFINE TEMP-TABLE MyIndex NO-UNDO
    FIELD Index-name AS CHAR
    FIELD seq AS INT
    FIELD actual-name AS CHAR
    INDEX xpk-index-name IS UNIQUE index-name seq.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD create-myindex Procedure 
FUNCTION create-myindex RETURNS CHARACTER
  ( INPUT new-index AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD munged Procedure 
FUNCTION munged RETURNS CHARACTER
  ( INPUT from-name AS CHAR, INPUT bad_suffix AS CHAR)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .35
         WIDTH              = 37.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

FOR EACH _File /* WHERE _File._File-name BEGINS "Var" */:
  IF _File._File-name BEGINS "Repl" THEN NEXT.
  IF _File._File-name BEGINS "_" THEN NEXT.

  bad-table = No.
  file-name = dump-dir + "/" + _File._File-Name.
  OUTPUT TO VALUE( file-name + ".sql" ) PAGE-SIZE 0.
  RUN dump-tabledef( _File._File-Name ).
  OUTPUT CLOSE.

  RUN dump-tabledat( _File._File-Name ).

  OUTPUT TO VALUE( file-name + ".sql" ) PAGE-SIZE 0 APPEND.
  RUN dump-indexdef( _File._File-Name ).
  OUTPUT CLOSE.

  IF bad-table THEN
    OS-RENAME VALUE( file-name + ".sql" ) VALUE( file-name + ".sqx" ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE dump-fielddef Procedure 
PROCEDURE dump-fielddef :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER file-name AS CHAR NO-UNDO.
DEF INPUT PARAMETER field-name AS CHAR NO-UNDO.

  PUT UNFORMATTED "  " + munged(field-name, "fld") + " ".
  CASE SUBSTRING( _Field._Data-Type, 1, 3):
    WHEN "CHA" THEN PUT "TEXT".
    WHEN "DEC" THEN PUT "FLOAT8".
    WHEN "LOG" THEN PUT "BOOL".
    WHEN "INT" THEN PUT "INT4".
    WHEN "DAT" THEN PUT "DATETIME".
    WHEN "RAW" THEN PUT "xxxRAW".
    WHEN "ROW" THEN PUT "xxxROWID".
    OTHERWISE PUT "xxx" + _Field._Data-Type .
  END CASE.

  IF _Field._Extent > 1 THEN PUT "[]".
  IF _Field._Mandatory THEN PUT " NOT NULL".
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE dump-indexdef Procedure 
PROCEDURE dump-indexdef :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER fname AS CHAR NO-UNDO.

DEF VAR idx-name AS CHAR NO-UNDO.
DEF VAR trailing AS CHAR NO-UNDO.
  FOR EACH _Index OF _File:
    PUT UNFORMATTED "CREATE ".
    IF _Index._Unique THEN PUT "UNIQUE ".
    idx-name = create-myindex( _Index._Index-Name ).
    PUT UNFORMATTED "INDEX " + idx-name.
    IF idx-name = fname THEN PUT UNFORMATTED "_key".
    PUT UNFORMATTED " ON " + munged(fname, "tbl") + " (".
    trailing = "".
    FOR EACH _Index-Field OF _Index, FIRST _Field OF _Index-Field:
      PUT UNFORMATTED trailing + " " + munged(_Field._Field-Name, "fld").
      IF _Field._Data-Type BEGINS "LOG" THEN PUT UNFORMATTED " int4_ops".
      trailing = ", ".
    END.
    PUT UNFORMATTED " ) ;" SKIP.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE dump-tabledat Procedure 
PROCEDURE dump-tabledat :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER fname AS CHAR NO-UNDO.

DEF VAR trailing AS CHAR NO-UNDO.
DEF VAR fld-type AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR line AS CHAR NO-UNDO.
  OUTPUT TO VALUE( file-name + ".p" ).
  PUT UNFORMATTED 
        "~{inc/tabledump.i~}" SKIP(1)
        "PROCEDURE DumpTableData:" SKIP(1)
        "  OUTPUT TO " + file-name + ".sql KEEP-MESSAGES APPEND." SKIP
        "  FOR EACH " + fname + " NO-LOCK:" SKIP
        "    PUT UNFORMATTED ~"INSERT INTO " + munged(fname, "tbl") + " VALUES( ~"." SKIP .
  trailing = "".
  FOR EACH _Field OF _File:
    fld-type = SUBSTRING( _Field._Data-type, 1, 3).
    IF _Field._Extent > 1 THEN DO:
      line = '    PUT UNFORMATTED "' + trailing + ' ~~~{".'.
      PUT UNFORMATTED line SKIP.
      trailing = "".
      DO i = 1 TO _Field._Extent:
        PUT UNFORMATTED "    PUT UNFORMATTED ".
        IF trailing <> "" THEN PUT UNFORMATTED '"' + trailing + '" +'.
        PUT UNFORMATTED " convert-" + fld-type + "( " + _Field._Field-name + "[" + STRING(i) + "] )." SKIP.
        trailing = ", ".
      END.
      PUT UNFORMATTED '    PUT UNFORMATTED "' + '~~~}".' SKIP.
    END.
    ELSE
      PUT UNFORMATTED '    PUT UNFORMATTED "' + trailing + '" + convert-' + fld-type + "( " + _Field._Field-name + " )." SKIP.

    trailing = ", ".
  END.
  PUT UNFORMATTED "    PUT UNFORMATTED ~" ); ~" SKIP." SKIP.
  PUT UNFORMATTED "  END." SKIP "  OUTPUT CLOSE." SKIP(1) "END PROCEDURE.".
  OUTPUT CLOSE.

  RUN VALUE( file-name + ".p" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE dump-tabledef Procedure 
PROCEDURE dump-tabledef :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER fname AS CHAR NO-UNDO.

DEF VAR trailing AS CHAR NO-UNDO INITIAL " (".
  PUT UNFORMATTED "CREATE TABLE " + munged(fname, "tbl").
  FOR EACH _Field OF _File:
    PUT UNFORMATTED trailing SKIP.
    RUN dump-fielddef( fname, _Field._Field-Name ).
    trailing = ",".
  END.
  PUT UNFORMATTED SKIP ") ;" SKIP.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION create-myindex Procedure 
FUNCTION create-myindex RETURNS CHARACTER
  ( INPUT new-index AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF BUFFER LastIndex FOR MyIndex.

  FIND LAST LastIndex WHERE LastIndex.Index-name = new-index NO-LOCK NO-ERROR.
  CREATE MyIndex.
  MyIndex.index-name = new-index.
  MyIndex.seq = (IF AVAILABLE(LastIndex) THEN LastIndex.seq + 1 ELSE 0).
  MyIndex.actual-name = munged( new-index
                     + (IF AVAILABLE(LastIndex) THEN "_" + STRING(MyIndex.seq) ELSE "")
                     , "idx").

  RETURN MyIndex.actual-name.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION munged Procedure 
FUNCTION munged RETURNS CHARACTER
  ( INPUT from-name AS CHAR, INPUT bad_suffix AS CHAR) :
/*------------------------------------------------------------------------------
  Purpose:  Munge a name into an SQL friendlier version
    Notes:  Not strictly necessary, but a good idea.
------------------------------------------------------------------------------*/
DEF VAR result AS CHAR NO-UNDO.

  result = REPLACE( from-name, "-", "_").
  result = LOWER(result).

  IF CAN-DO( keywords, result ) THEN result = result + "_" + bad_suffix.

  RETURN result.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


