&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : Dump all tables and definitions
    Purpose     : Used to re-create the database from scratch

    Syntax      : RUN process/dumpall.p

    Author(s)   : Andrew McMillan
  ------------------------------------------------------------------------*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .42
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
DEF VAR dump-dir AS CHAR NO-UNDO.
dump-dir = "G:\clients\Invincible\dump-990610".

OUTPUT TO VALUE( dump-dir + "/dumpout.log" ) .
RUN prodict/dump_d.r ( "_user", dump-dir, ? ).
/* RUN prodict/dump_d.r ( "ALL", dump-dir, ? ).
RUN prodict/dump_df.r( "ALL", dump-dir + "/all.df", ?).
RUN prodict/dump_fd.r( "ALL", dump-dir + "/all.fd" ). */
OUTPUT CLOSE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-dump-dir Procedure 
PROCEDURE get-dump-dir :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  dump-dir = OS-GETENV("DUMPDIR").
  IF dump-dir <> "" AND dump-dir <> ? THEN RETURN.  /* used env value */

  dump-dir = "/DUMP".       /* basic default value */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


