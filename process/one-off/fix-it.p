&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r11
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 2
         WIDTH              = 40.
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

RUN fix-property-address.
RUN fix-lease-term.
RUN fix-rental-space-nulls.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calc-date-diff Procedure 
PROCEDURE calc-date-diff :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER start-date AS DATE NO-UNDO.
DEF INPUT PARAMETER end-date   AS DATE NO-UNDO.
DEF OUTPUT PARAMETER yy AS INT NO-UNDO.
DEF OUTPUT PARAMETER mm AS INT NO-UNDO.
DEF OUTPUT PARAMETER dd AS INT NO-UNDO.

DEF VAR dpm AS INT NO-UNDO.
DEF VAR start-yy AS INT NO-UNDO.
DEF VAR start-mm AS INT NO-UNDO.

  dd = DAY( end-date ).  mm = MONTH( end-date ).  yy = YEAR( end-date ).
  RUN get-number-of-days( yy, mm, OUTPUT dpm).

  dd = 1 + dd - DAY( start-date ).
  start-mm = MONTH( start-date ).
  start-yy = YEAR( start-date ).
  mm = mm - start-mm.
  yy = yy - start-yy.
  IF dd = dpm THEN
    ASSIGN     dd = 0      mm = mm + 1  .
  ELSE IF dd < 0 THEN DO:
    RUN get-number-of-days( start-yy, start-mm, OUTPUT dpm).
    ASSIGN      dd = dd + dpm       mm = mm - 1  .
  END.
  IF mm < 0 THEN ASSIGN mm = mm + 12      yy = yy - 1.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fix-lease-term Procedure 
PROCEDURE fix-lease-term :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH TenancyLease:
    RUN calc-date-diff( 
        TenancyLease.LeaseStartDate, TenancyLease.LeaseEndDate,
        OUTPUT TenancyLease.TermYears,
        OUTPUT TenancyLease.TermMonths,
        OUTPUT TenancyLease.TermDays ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fix-property-address Procedure 
PROCEDURE fix-property-address :
/*------------------------------------------------------------------------------
  Purpose:  Fix the CHR(253) in the property street address
------------------------------------------------------------------------------*/
  FOR EACH Property:
    DO WHILE INDEX( Property.StreetAddress, CHR(218)) > 0:
      SUBSTRING( Property.StreetAddress, INDEX( Property.StreetAddress, CHR(218))) = CHR(10).
    END.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fix-rental-space-nulls Procedure 
PROCEDURE fix-rental-space-nulls :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FOR EACH RentalSpace:
    IF RentalSpace.AreaSize = ? THEN RentalSpace.AreaSize = 0.
    IF RentalSpace.ContractedRental = ? THEN RentalSpace.ContractedRental = 0.
    IF RentalSpace.ChargedRental = ? THEN RentalSpace.ChargedRental = 0.
    IF RentalSpace.MarketRental = ? THEN RentalSpace.ContractedRental = 0.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-number-of-days Procedure 
PROCEDURE get-number-of-days :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER yy AS INT NO-UNDO.
DEF INPUT PARAMETER mm AS INT NO-UNDO.
DEF OUTPUT PARAMETER days AS INT NO-UNDO.

  CASE mm:
    WHEN  1 THEN days = 31.
    WHEN  2 THEN DO:
      days = 28.
      IF (yy MODULO 4) = 0 THEN days = days + 1.
    END.
    WHEN  3 THEN days = 31.
    WHEN  4 THEN days = 30.
    WHEN  5 THEN days = 31.
    WHEN  6 THEN days = 30.
    WHEN  7 THEN days = 31.
    WHEN  8 THEN days = 31.
    WHEN  9 THEN days = 30.
    WHEN 10 THEN days = 31.
    WHEN 11 THEN days = 30.
    WHEN 12 THEN days = 31.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


