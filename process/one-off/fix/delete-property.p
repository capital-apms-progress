/* 
DEF VAR property-code AS INT NO-UNDO INIT 0.
UPDATE property-code.

FIND Property WHERE Property.PropertyCode = property-code.
*/
FOR EACH Property:

IF PropertyCode = 534 THEN NEXT.
IF PropertyCode = 601 THEN NEXT.
IF PropertyCode = 745 THEN NEXT.
IF PropertyCode = 746 THEN NEXT.
IF PropertyCode = 765 THEN NEXT.
IF PropertyCode = 767 THEN NEXT.
IF PropertyCode = 768 THEN NEXT.
IF PropertyCode = 780 THEN NEXT.
IF PropertyCode = 802 THEN NEXT.
IF PropertyCode = 804 THEN NEXT.
IF PropertyCode = 805 THEN NEXT.
IF PropertyCode = 809 THEN NEXT.
IF PropertyCode = 922 THEN NEXT.
IF PropertyCode = 1000 THEN NEXT.
IF PropertyCode = 9999 THEN NEXT.
IF PropertyCode = 99999 THEN NEXT.



FOR EACH BuildingEvent OF Property:
  DELETE BuildingEvent.
END.
FOR EACH BuildingSystem OF Property:
  DELETE BuildingSystem.
END.
FOR EACH BuildingTask OF Property:
  DELETE BuildingTask.
END.
FOR EACH Contract OF Property:
  DELETE Contract.
END.
FOR EACH GroundLease OF Property:
  DELETE GroundLease.
END.
FOR EACH Image OF Property:
  DELETE Image.
END.
FOR EACH LeaseImage OF Property:
  DELETE LeaseImage.
END.
FOR EACH PropertyTitle OF Property:
  DELETE PropertyTitle.
END.
FOR EACH StreetFrontage OF Property:
  DELETE StreetFrontage.
END.
FOR EACH Valuation OF Property:
  DELETE Valuation.
END.
FOR EACH PropertyOutgoing OF Property:
  DELETE PropertyOutgoing.
END.
FOR EACH SupplyMeter OF Property:
  DELETE SupplyMeter.
END.
FOR EACH RentSpaceHistory OF Property:
  DELETE RentSpaceHistory.
END.
FOR EACH ConstructionDetails OF Property:
  DELETE ConstructionDetails.
END.
FOR EACH LeaseHistory OF Property:
  DELETE LeaseHistory.
END.
FOR EACH PropForecast OF Property:
  DELETE PropForecast.
END.
FOR EACH PropForecastParam OF Property:
  DELETE PropForecastParam.
END.
FOR EACH SupplyMeterReading OF Property:
  DELETE SupplyMeterReading.
END.
FOR EACH TenantCall OF Property:
  DELETE TenantCall.
END.
FOR EACH PropertyView OF Property:
  DELETE PropertyView.
END.
FOR EACH BuildingEvent OF Property:
  DELETE BuildingEvent.
END.
FOR EACH BuildingEvent OF Property:
  DELETE BuildingEvent.
END.
FOR EACH Note OF Property:
  DELETE Note.
END.

FOR EACH Tenant WHERE Tenant.EntityType = 'P' AND Tenant.EntityCode = Property.PropertyCode:
  DELETE Tenant.
END.
FOR EACH RentalSpace OF Property:
  DELETE RentalSpace.
END.
FOR EACH TenancyLease OF Property:
  DELETE TenancyLease.
END.
/* DO TRANSACTION:   */
DELETE Property.
END.
