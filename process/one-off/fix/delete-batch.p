DEF VAR batch-code AS INT NO-UNDO.
DEF BUFFER LastBatch FOR Batch.
FIND LAST LastBatch NO-LOCK.
batch-code = LastBatch.BatchCode.
UPDATE batch-code.

FIND Batch WHERE Batch.BatchCode = batch-code.
FOR EACH Document OF Batch:
  FOR EACH AcctTran OF Document TRANSACTION:
    AcctTran.Amount = 0.
  END.
END.
FOR EACH Document OF Batch TRANSACTION:
  FOR EACH AcctTran OF Document:
    DELETE AcctTran.
  END.
  DELETE Document.
END.
DO TRANSACTION:
DELETE Batch.
END.
