&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

{inc/ofc-this.i}
{inc/ofc-set.i "RentCharge-Type" "charge-type" "ERROR"}
IF LOOKUP( charge-type, "Accounts,Single" ) = 0 THEN RETURN.

{inc/ofc-acct.i "RENT" "rent-account"}
{inc/ofc-acct.i "OUTGOINGS" "outgoings-account"}
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}

DEF VAR default-last-date AS DATE NO-UNDO.
default-last-date = TODAY.
IF DAY(default-last-date) > 20 THEN /* put date into next month */
  default-last-date = default-last-date + 15.

FIND LAST Month WHERE Month.StartDate <= default-last-date NO-LOCK NO-ERROR.
IF AVAILABLE Month THEN default-last-date = Month.StartDate - 1.

DEF WORK-TABLE og-acct NO-UNDO
    FIELD AccountCode LIKE ChartOfAccount.AccountCode
    FIELD ShortName AS CHAR FORMAT "X(9)"
    FIELD Recovered AS DECIMAL
    FIELD Gross AS DECIMAL
    FIELD Vacant AS DECIMAL.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-convert-from-yearly) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD convert-from-yearly Procedure 
FUNCTION convert-from-yearly RETURNS DECIMAL
  ( INPUT amount AS DEC, INPUT freq AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-prop-og) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-prop-og Procedure 
FUNCTION get-prop-og RETURNS DECIMAL
  ( INPUT doit AS LOGICAL, INPUT pc AS INTEGER, INPUT ac AS DECIMAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-last-trans-date) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD last-trans-date Procedure 
FUNCTION last-trans-date RETURNS DATE
  ( INPUT account-code AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 16.75
         WIDTH              = 39.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/date.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

RUN convert-area-types.
RUN convert-rent-charges.
RUN convert-rent-reviews.

MESSAGE "Conversion of RentalSpace rents to Rent Charges completed!".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-build-property-details) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-property-details Procedure 
PROCEDURE build-property-details :
/*------------------------------------------------------------------------------
  Purpose:  Build details of property outgoings &c.
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER times-amt AS LOGICAL NO-UNDO.

DEF VAR out-line AS CHAR NO-UNDO.
DEF VAR no-accounts AS INTEGER NO-UNDO.
DEF VAR i AS INTEGER NO-UNDO.
DEF VAR percentage AS DECIMAL NO-UNDO.
DEF VAR acct-amnt AS DECIMAL NO-UNDO.
DEF VAR money-fmt AS CHAR NO-UNDO.
DEF VAR non-zero AS LOGICAL NO-UNDO.

DEF BUFFER PrimarySpace FOR RentalSpace.

money-fmt = IF times-amt = Yes THEN "->>>,>>9" ELSE "->>>9.99".

  no-accounts = 0.
  FOR EACH TenancyLease NO-LOCK OF Property WHERE TenancyLease.LeaseStatus <> "PAST",
            EACH TenancyOutgoing NO-LOCK OF TenancyLease WHERE TenancyOutgoing.Percentage > 0:
    FIND FIRST og-acct WHERE og-acct.AccountCode = TenancyOutgoing.AccountCode NO-ERROR.
    IF AVAILABLE(og-acct) THEN
      og-acct.Recovered = og-acct.Recovered + TenancyOutgoing.Percentage.
    ELSE DO:
      no-accounts = no-accounts + 1.
      FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = TenancyOutgoing.AccountCode NO-LOCK NO-ERROR.
      CREATE og-acct.
      ASSIGN
        og-acct.AccountCode = TenancyOutgoing.AccountCode
        og-acct.ShortName   = (IF AVAILABLE(ChartOfAccount) THEN STRING( (IF TRIM(ChartOfAccount.ShortName) <> "" THEN ChartOfAccount.ShortName ELSE ChartOfAccount.Name), "X(7)") ELSE "???????")
        og-acct.Recovered   = TenancyOutgoing.Percentage
        og-acct.Vacant      = 0
      .
      og-acct.ShortName = TRIM(og-acct.ShortName).
      og-acct.ShortName = FILL( " ", 7 - LENGTH(og-acct.ShortName)) + og-acct.ShortName .
    END.
  END.

  /* fill in those where a default percentage applies from the lease record */
  FOR EACH TenancyLease NO-LOCK OF Property WHERE TenancyLease.LeaseStatus <> "PAST",
        EACH og-acct WHERE NOT CAN-FIND( FIRST TenancyOutgoing OF TenancyLease WHERE TenancyOutgoing.AccountCode = og-acct.AccountCode):
    og-acct.Recovered = og-acct.Recovered + TenancyLease.OutgoingsRate .
  END.
/*
  col-head1 = FILL(" ", 33).
  col-head2 = STRING( "Tenant / Area", "X(33)").
  FOR EACH og-acct BY og-acct.AccountCode:
    col-head1 = col-head1 + " " + og-acct.ShortName.
    col-head2 = col-head2 + " " + STRING( og-acct.AccountCode, "9999.99").
  END.

  IF print-main-schedule THEN PAGE. ELSE RUN set-headings( col-head1, col-head2).
*/

  /* Net Leases */
  FOR EACH TenancyLease NO-LOCK OF Property WHERE TenancyLease.LeaseStatus <> "PAST"
                                    AND NOT TenancyLease.GrossLease,
            FIRST PrimarySpace OF TenancyLease /* WHERE PrimarySpace.RentalSpaceCode = TenancyLease.PrimarySpace */
                        BY PrimarySpace.Level BY PrimarySpace.LevelSequence :
    FIND Tenant WHERE Tenant.TenantCode = TenancyLease.TenantCode NO-LOCK NO-ERROR.
    non-zero = No.
    out-line = STRING( Tenant.tenantCode, "99999 ")
             + STRING( Tenant.Name, "X(15)") + " "
             + STRING( TenancyLease.AreaDescription, "X(11)") .
    FOR EACH og-acct BY og-acct.AccountCode:
      FIND TenancyOutgoing NO-LOCK OF TenancyLease WHERE TenancyOutgoing.AccountCode = og-acct.AccountCode NO-ERROR.
      percentage = (IF AVAILABLE(TenancyOutgoing) THEN TenancyOutgoing.Percentage ELSE TenancyLease.OutgoingsRate ).
      IF percentage = ? THEN percentage = 0. ELSE
        percentage = percentage * get-prop-og( times-amt, Property.PropertyCode, og-acct.AccountCode ).
      out-line = out-line + (IF percentage <> 0 THEN STRING( percentage, money-fmt) ELSE "     -  ").
      IF percentage <> 0 THEN non-zero = Yes.
    END.
    IF PrimarySpace.AreaType <> "C" OR non-zero THEN RUN print-line( out-line, No).
  END.
  RUN print-line( FILL(" ", 33) + FILL( " " + FILL("-",7), no-accounts), No).

  out-line = STRING( "Total Recoverable", "X(33)").
  FOR EACH og-acct BY og-acct.AccountCode:
    out-line = out-line + STRING( og-acct.Recovered * get-prop-og( times-amt, Property.PropertyCode, og-acct.AccountCode ),
                                             money-fmt) .
  END.
  RUN print-line( out-line, No).


  /* Gross Leases */
  RUN print-line( " ", No ).
  RUN print-line( " ", No ).
  FOR EACH TenancyLease NO-LOCK OF Property WHERE TenancyLease.LeaseStatus <> "PAST"
                                    AND TenancyLease.GrossLease,
            FIRST PrimarySpace OF TenancyLease /* WHERE PrimarySpace.RentalSpaceCode = TenancyLease.PrimarySpace */
                        BY PrimarySpace.Level BY PrimarySpace.LevelSequence :
    FIND Tenant WHERE Tenant.TenantCode = TenancyLease.TenantCode NO-LOCK NO-ERROR.
    non-zero = No.
    out-line = STRING( Tenant.tenantCode, "99999 ")
             + STRING( Tenant.Name, "X(15)") + " "
             + STRING( TenancyLease.AreaDescription, "X(11)") .
    percentage = 0.
    FOR EACH RentalSpace OF TenancyLease:
      percentage = percentage + RentalSpace.OutgoingsPercentage.
    END.
    IF percentage = 0 THEN NEXT.

    FOR EACH og-acct BY og-acct.AccountCode:
      acct-amnt = percentage * get-prop-og( times-amt, Property.PropertyCode, og-acct.AccountCode ).
      out-line = out-line + (IF acct-amnt <> 0 THEN STRING( acct-amnt, money-fmt) ELSE "     -  ") .
      og-acct.Gross = og-acct.Gross + percentage.
    END.
    RUN print-line( out-line, No).

  END.
  RUN print-line( FILL(" ", 33) + FILL( " " + FILL("-",7), no-accounts), No).

  out-line = STRING( "Total Gross Leases", "X(33)").
  FOR EACH og-acct BY og-acct.AccountCode:
    out-line = out-line + STRING( og-acct.Gross * get-prop-og( times-amt, Property.PropertyCode, og-acct.AccountCode ),
                                             money-fmt) .
  END.
  RUN print-line( out-line, No).


  RUN print-line( " ", No ).
  RUN print-line( " ", No ).
  FOR EACH RentalSpace NO-LOCK OF Property WHERE RentalSpace.AreaStatus = "V"
                AND RentalSpace.OutgoingsPercentage <> 0
                BY Level BY LevelSequence:
    out-line = STRING( TRIM( STRING( RentalSpace.Level, "->>>9/")) + TRIM( STRING( RentalSpace.LevelSequence, ">>>9 ")), "X(8)")
             + STRING( RentalSpace.Description, "X(25)").
    FOR EACH og-acct BY og-acct.AccountCode:
      percentage = RentalSpace.OutgoingsPercentage * get-prop-og( times-amt, Property.PropertyCode, og-acct.AccountCode ).
      out-line = out-line + (IF percentage <> 0 THEN STRING( percentage, money-fmt) ELSE "     -  ") .
      og-acct.Vacant = og-acct.Vacant + RentalSpace.OutgoingsPercentage.
    END.
    RUN print-line( out-line, No).
  END.

  RUN print-line( FILL(" ", 33) + FILL( " " + FILL("-",7), no-accounts), No).
  out-line = STRING( "Total Vacant", "X(33)").
  FOR EACH og-acct BY og-acct.AccountCode:
    out-line = out-line + STRING( og-acct.Vacant * get-prop-og( times-amt, Property.PropertyCode, og-acct.AccountCode ),
                                                 money-fmt) .
  END.
  RUN print-line( out-line, No).


  RUN print-line( " ", No ).
  RUN print-line( " ", No ).
  RUN print-line( FILL(" ", 33) + FILL( " " + FILL("=",7), no-accounts), No).
  out-line = STRING( "Total", "X(33)").
  FOR EACH og-acct BY og-acct.AccountCode:
    out-line = out-line + STRING( (og-acct.Recovered + og-acct.Gross + og-acct.Vacant) * get-prop-og( times-amt, Property.PropertyCode, og-acct.AccountCode ),
                                         money-fmt) .
  END.
  RUN print-line( out-line, No).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-area-types) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE convert-area-types Procedure 
PROCEDURE convert-area-types :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FOR EACH RentChargeType: DELETE RentChargeType. END.

  FOR EACH AreaType NO-LOCK:
    CREATE RentChargeType.
    ASSIGN
      RentChargeType.RentchargeType = AreaType.AreaType
      RentchargeType.Description    = AreaType.Description + " Rental"
      RentChargeType.AccountCode    = AreaType.AccountCode.
  END.
  
  /* Create the default "rental" rent charge type */
  CREATE RentChargeType.
  ASSIGN
    RentChargeType.RentChargeType = "RENT"
    RentChargeType.Description    = "Rental"
    RentChargeType.AccountCode    = rent-account.

  /* Create the default "outgoings" rent charge type */
  CREATE RentChargeType.
  ASSIGN
    RentChargeType.RentChargeType = "O/G"
    RentChargeType.Description    = "Outgoings"
    RentChargeType.AccountCode    = outgoings-account.

  IF NOT CAN-FIND( RentChargeLineStatus WHERE RentChargeLineStatus.RentChargeLineStatus = "C") THEN DO:
    /* Create the "current" rent charge line status */
    CREATE RentChargeLineStatus.
    ASSIGN  RentChargeLineStatus.RentChargeLineStatus = "C"
            RentChargeLineStatus.Description    = "Current".
  END.

  IF NOT CAN-FIND( RentChargeLineStatus WHERE RentChargeLineStatus.RentChargeLineStatus = "P") THEN DO:
    /* Create the "Past" rent charge line status */
    CREATE RentChargeLineStatus.
    ASSIGN  RentChargeLineStatus.RentChargeLineStatus = "P"
            RentChargeLineStatus.Description    = "Past".
  END.

  IF NOT CAN-FIND( RentChargeLineStatus WHERE RentChargeLineStatus.RentChargeLineStatus = "I") THEN DO:
    /* Create the "Past" rent charge line status */
    CREATE RentChargeLineStatus.
    ASSIGN  RentChargeLineStatus.RentChargeLineStatus = "I"
            RentChargeLineStatus.Description    = "Initial".
  END.

  IF NOT CAN-FIND( RentChargeLineStatus WHERE RentChargeLineStatus.RentChargeLineStatus = "N") THEN DO:
    /* Create the "Future" rent charge line status */
    CREATE RentChargeLineStatus.
    ASSIGN  RentChargeLineStatus.RentChargeLineStatus = "N"
            RentChargeLineStatus.Description    = "Not finalised".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-og-charge) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE convert-og-charge Procedure 
PROCEDURE convert-og-charge :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER seq-code AS INT NO-UNDO.

  IF (TenancyLease.OutgoingsBudget = 0 OR TenancyLease.OutgoingsBudget = ?)
       AND TenancyLease.RecoveryType <> "B" THEN DO:
    .
  END.
  ELSE DO TRANSACTION:
    CREATE RentCharge.
    ASSIGN  RentCharge.TenancyLeaseCode = TenancyLease.TenancyLeaseCode   
            RentCharge.SequenceCode     = seq-code
            RentCharge.RentChargeType   = "O/G"
            RentCharge.Description      = "Outgoings"
            RentCharge.EntityType       = ""
            RentCharge.EntityCode       = 0
            RentCharge.AccountCode      = outgoings-account.

    CREATE RentChargeLine.
    ASSIGN  RentChargeLine.DateCommitted    = TODAY
            RentChargeLine.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
            RentChargeLine.SequenceCode     = RentCharge.SequenceCode
            RentChargeLine.RentChargeLineStatus = "C"
            RentChargeLine.EndDate          = TenancyLease.RentEndDate .

    RentChargeLine.FrequencyCode = (IF TenancyLease.PaymentFrequency <> "" THEN TenancyLease.PaymentFrequency ELSE "MNTH").
    RentChargeLine.LastChargedDate = last-trans-date( RentCharge.AccountCode ).
    RentChargeLine.StartDate = (IF TenancyLease.RentStartDate <> ? THEN TenancyLease.RentStartDate ELSE RentChargeLine.LastChargedDate + 1).
    RentChargeLine.Amount = convert-from-yearly( TenancyLease.OutgoingsBudget, RentChargeLine.FrequencyCode ).
  END.    /* if TenancyLease.OutgoingsBudget <> 0 then */


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-rent-charges) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE convert-rent-charges Procedure 
PROCEDURE convert-rent-charges :
/*------------------------------------------------------------------------------
  Purpose:     Create rent charge records from the current structure
------------------------------------------------------------------------------*/

  DEF VAR rent-chg-amount AS DEC NO-UNDO.
  DEF VAR outgoings-amount AS DEC NO-UNDO.
  DEF VAR last-charged-date AS DATE NO-UNDO.
  DEF VAR seq-code      AS INT NO-UNDO.
  
  /* Clear all rent charges */
  FOR EACH RentChargeLine: DELETE RentChargeLine. END.
  FOR EACH RentCharge:     DELETE RentCharge.     END.

  ON WRITE OF RentCharge OVERRIDE DO: END.

  IF charge-type = "Single" THEN DO:
    
    FOR EACH Property WHERE Property.Active AND NOT Property.ExternallyManaged NO-LOCK:
      lease-loop:
      FOR EACH TenancyLease WHERE TenancyLease.PropertyCode = Property.PropertyCode
                              AND TenancyLease.LeaseStatus <> "PAST" NO-LOCK :
        FIND FIRST Tenant WHERE Tenant.TenantCode = TenancyLease.TenantCode
                            AND Tenant.Active NO-LOCK NO-ERROR.
        IF NOT AVAILABLE(Tenant) THEN NEXT lease-loop.

        seq-code = 1.
        rent-chg-amount = 0.
        FOR EACH RentalSpace WHERE RentalSpace.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
                               AND RentalSpace.AreaStatus <> "V" NO-LOCK:
          rent-chg-amount = rent-chg-amount + ( IF RentalSpace.ChargedRental <> ? THEN RentalSpace.ChargedRental ELSE 0 ).
        END.

        IF rent-chg-amount = 0 OR rent-chg-amount = ? THEN DO:
          /* MESSAGE TenancyLease.TenancyLeaseCode " has zero rental value". */
          .
        END.
        ELSE DO TRANSACTION:
          CREATE RentCharge.
          ASSIGN
            RentCharge.TenancyLeaseCode = TenancyLease.TenancyLeaseCode   
            RentCharge.SequenceCode     = seq-code
            RentCharge.RentChargeType   = "RENT"
            RentCharge.Description      = "Rental"
            RentCharge.EntityType       = ""
            RentCharge.EntityCode       = 0
            RentCharge.AccountCode      = rent-account.

          CREATE RentChargeLine.
          ASSIGN
            RentChargeLine.DateCommitted    = TODAY
            RentChargeLine.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
            RentChargeLine.SequenceCode     = RentCharge.SequenceCode
            RentChargeLine.RentChargeLineStatus = "C"
            RentChargeLine.EndDate          = TenancyLease.RentEndDate .

          RentChargeLine.FrequencyCode    = "MNTH".
/* (IF TenancyLease.PaymentFrequency <> "" THEN TenancyLease.PaymentFrequency ELSE "MNTH"). */
          RentChargeLine.LastChargedDate  = last-trans-date( RentCharge.AccountCode ).
          RentChargeLine.StartDate = TenancyLease.RentStartDate.
          IF RentChargeLine.StartDate = ? THEN RentChargeLine.StartDate = TenancyLease.LeaseStartDate.
          IF RentChargeLine.StartDate = ? THEN RentChargeLine.StartDate = RentChargeLine.LastChargedDate + 1.
          IF RentChargeLine.StartDate = ? THEN RentChargeLine.StartDate = last-of-month(TODAY - 3) + 1.
          RentChargeLine.Amount           = convert-from-yearly( rent-chg-amount, RentChargeLine.FrequencyCode ).

          seq-code = seq-code + 1.
        END.    /* if rent-chg-amount <> 0 then */

        RUN convert-og-charge( seq-code ).

      END.   /* each lease */
    END.  /* each property */
  END.
  ELSE DO: /* Charge-Type = "Accounts" */
  
    /* For Australia - split rent charges based on rent charge type */
    FOR EACH Property NO-LOCK WHERE Property.Active AND NOT Property.ExternallyManaged,
      EACH TenancyLease OF Property NO-LOCK WHERE TenancyLease.LeaseStatus <> "PAST",
      FIRST Tenant OF TenancyLease WHERE Tenant.Active:

      seq-code = 1.
      FOR EACH RentalSpace OF TenancyLease WHERE RentalSpace.AreaStatus <> "V" NO-LOCK
        BREAK BY RentalSpace.AreaType:
        
        /* For area types "S" create individual charges */
        IF FIRST-OF( RentalSpace.AreaType ) OR RentalSpace.AreaType = "S" THEN
          rent-chg-amount = 0.

        rent-chg-amount = rent-chg-amount + ( IF RentalSpace.ChargedRental <> ? THEN RentalSpace.ChargedRental ELSE 0 ).
        
        IF LAST-OF( RentalSpace.AreaType ) OR RentalSpace.AreaType = "S" THEN DO:
          FIND AreaType WHERE AreaType.AreaType = RentalSpace.AreaType NO-LOCK NO-ERROR.

          CREATE RentCharge.
          ASSIGN
            RentCharge.TenancyLeaseCode = TenancyLease.TenancyLeaseCode   
            RentCharge.SequenceCode     = seq-code
            seq-code                    = seq-code + 1   
            RentCharge.RentChargeType   = RentalSpace.AreaType
            RentCharge.Description      = IF NOT AVAILABLE(AreaType) OR RentalSpace.AreaType = "S" THEN RentalSpace.Description ELSE AreaType.Description
            RentCharge.EntityType       = ""
            RentCharge.EntityCode       = 0
            RentCharge.AccountCode      = IF AVAILABLE(AreaType) THEN AreaType.AccountCode ELSE rent-account.
      
          CREATE RentChargeLine.
          ASSIGN
            RentChargeLine.DateCommitted    = TODAY
            RentChargeLine.TenancyLeaseCode = TenancyLease.TenancyLeaseCode
            RentChargeLine.SequenceCode     = RentCharge.SequenceCode
            RentChargeLine.FrequencyCode    = IF TenancyLease.PaymentFrequency <> "" THEN TenancyLease.PaymentFrequency ELSE "MNTH".

          RentChargeLine.Amount           = convert-from-yearly( rent-chg-amount, RentChargeLine.FrequencyCode ).
          ASSIGN
            RentChargeLine.RentChargeLineStatus = "C"
            RentChargeLine.LastChargedDate  = last-trans-date( RentCharge.AccountCode )
            RentChargeLine.StartDate        = IF TenancyLease.RentStartDate <> ?
              THEN TenancyLease.RentStartDate
              ELSE RentChargeLine.LastChargedDate + 1
            RentChargeLine.EndDate          = TenancyLease.RentEndDate  .
        END.
        
      END.  /* for each rental space */

      RUN convert-og-charge( seq-code ).

    END.  /* for each lease */

  END.  /* aussie style */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-convert-rent-reviews) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE convert-rent-reviews Procedure 
PROCEDURE convert-rent-reviews :
/*------------------------------------------------------------------------------
  Purpose:     Create incomplete rent charge records from the 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
RETURN.
  MESSAGE "Converting Rent Reviews" VIEW-AS ALERT-BOX.

  ON WRITE OF RentCharge OVERRIDE DO: END.
  
  FOR EACH Property NO-LOCK WHERE Property.Active AND NOT Property.ExternallyManaged,
                EACH TenancyLease NO-LOCK OF Property WHERE TenancyLease.LeaseStatus <> "PAST",
                FIRST Tenant NO-LOCK OF TenancyLease WHERE Tenant.Active,
                EACH RentReview NO-LOCK OF TenancyLease:

    /* Find the rent charge that the review should belong to */
    IF charge-type = "Single" THEN DO:
      FIND FIRST RentCharge WHERE RentCharge.TenancyLeaseCode = RentReview.TenancyLeaseCode
                AND RentCharge.RentChargeType   = "RENT" NO-LOCK NO-ERROR.
      RUN create-review-change.
    END.
    ELSE DO:
      FOR EACH RentCharge WHERE RentCharge.TenancyLeaseCode = RentReview.TenancyLeaseCode NO-LOCK:
        RUN create-review-change.
      END.
    END.
  END.

  ON WRITE OF RentCharge REVERT .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-create-review-change) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-review-change Procedure 
PROCEDURE create-review-change :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER LastCharge FOR RentCharge.

  IF NOT AVAILABLE RentCharge THEN DO:
    FIND LAST LastCharge  WHERE LastCharge.TenancyLeaseCode = RentReview.TenancyLeaseCode
                                    NO-LOCK NO-ERROR.
    CREATE RentCharge.
    ASSIGN  RentCharge.TenancyLeaseCode = TenancyLease.TenancyLeaseCode   
            RentCharge.SequenceCode     = IF AVAILABLE LastCharge THEN LastCharge.SequenceCode + 1 ELSE 1
            RentCharge.RentChargeType   = "RENT"
            RentCharge.Description      = "Rental"
            RentCharge.EntityType       = ""
            RentCharge.EntityCode       = 0
            RentCharge.AccountCode      = rent-account.
  END.

  CREATE RentChargeLine.
  ASSIGN  RentChargeLine.TenancyLeaseCode      = RentCharge.TenancyLeaseCode
          RentChargeLine.SequenceCode          = RentCharge.SequenceCode
          RentChargeLine.RentChargeLineStatus  = "R"
          RentChargeLine.StartDate             = RentReview.NewRentStart
          RentChargeLine.EndDate               = ?
          RentChargeLine.Amount                = RentReview.NewRental
          RentChargeLine.FrequencyCode         = IF TenancyLease.PaymentFrequency <> ""
                          THEN TenancyLease.PaymentFrequency ELSE "MNTH" .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-convert-from-yearly) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION convert-from-yearly Procedure 
FUNCTION convert-from-yearly RETURNS DECIMAL
  ( INPUT amount AS DEC, INPUT freq AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  DEF VAR n-months AS INT NO-UNDO INITIAL ?.
  DEF VAR n-days   AS INT NO-UNDO INITIAL ?.

  /* mostly we are dealing with months, so to be slightly more efficient... */
  IF freq = "MNTH" THEN n-months = 1. ELSE n-months = get-freq-months( freq ).
  IF n-months <> ? THEN RETURN ROUND( amount / 12 * n-months, 2 ).
  
  n-days = get-freq-days( freq ).
  IF n-days <> ? THEN RETURN ROUND( amount / 365 * n-days, 2 ).
  
  RETURN 0.00.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-prop-og) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-prop-og Procedure 
FUNCTION get-prop-og RETURNS DECIMAL
  ( INPUT doit AS LOGICAL, INPUT pc AS INTEGER, INPUT ac AS DECIMAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  IF doit <> Yes THEN RETURN 1.0 .
  FIND PropertyOutgoing WHERE PropertyOutgoing.PropertyCode = pc
                        AND PropertyOutgoing.AccountCode = ac NO-LOCK NO-ERROR.
  IF AVAILABLE(PropertyOutgoing) THEN
    RETURN PropertyOutgoing.BudgetAmount / 100 .

  RETURN 0.00 .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-last-trans-date) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION last-trans-date Procedure 
FUNCTION last-trans-date RETURNS DATE
  ( INPUT account-code AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Return a logical last charged date for the given tenant and account
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR default-date AS DATE NO-UNDO.

  default-date = IF TenancyLease.RentEndDate <> ?
    THEN MINIMUM( TenancyLease.RentEndDate, default-last-date )
    ELSE default-last-date.
    
  DEF VAR charge-month LIKE Month.MonthCode INIT ? NO-UNDO.
  FIND Tenant OF TenancyLease NO-LOCK NO-ERROR.
  IF NOT AVAILABLE Tenant THEN RETURN default-date.
  
  FIND LAST AcctTran WHERE AcctTran.EntityType  = Tenant.EntityType
                     AND AcctTran.EntityCode  = Tenant.EntityCode
                     AND AcctTran.AccountCode = account-code
                     AND AcctTran.Reference = "T" + STRING(Tenant.TenantCode)
                     NO-LOCK NO-ERROR.
  IF AVAILABLE AcctTran THEN charge-month = AcctTran.MonthCode.
  
  IF charge-month <> ? THEN DO:
    FIND Month WHERE Month.MonthCode = charge-month NO-LOCK NO-ERROR.
    IF AVAILABLE Month THEN RETURN MAX(Month.EndDate, default-date).
  END.
  
  RETURN default-date.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

