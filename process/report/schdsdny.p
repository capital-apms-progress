&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : 
    Purpose     : Property Schedule for AGP
    Author(s)   : Andrew McMillan
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

/* for accumulating outgoing details over a property */
DEF WORK-TABLE bldg-og NO-UNDO LIKE OutgoingDescription.
DEF WORK-TABLE bldg-sublease NO-UNDO LIKE SubLease.

/* report option variables */
DEF VAR preview AS LOGICAL NO-UNDO.
DEF VAR total-all-levels AS LOGICAL NO-UNDO.
DEF VAR property-1 AS INTEGER NO-UNDO.
DEF VAR property-2 AS INTEGER NO-UNDO.
/*
DEF VAR max-level-sequence AS INT INITIAL 999 NO-UNDO.
DEF VAR show-x-spaces AS LOGICAL INITIAL No NO-UNDO.
      WHEN "Max Sequence" THEN          max-level-sequence = INT(ENTRY(2,token)).
      WHEN "Include 'X' spaces" THEN    show-x-spaces = (ENTRY(2,token) =  "Yes").
*/
DEF VAR agent-version AS LOGICAL NO-UNDO.
RUN parse-parameters.

DEF VAR first-building AS LOGICAL INITIAL yes NO-UNDO.
DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}

&GLOB PAGE-WIDTH 245
DEF VAR timeStamp AS CHAR FORMAT "X(44)" NO-UNDO.
timeStamp = STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.
DEF VAR hline2 AS CHAR NO-UNDO.
DEF VAR hline3 AS CHAR NO-UNDO.
DEF VAR hline4 AS CHAR NO-UNDO.
DEF VAR col-head1-base AS CHAR NO-UNDO.
DEF VAR col-head2-base AS CHAR NO-UNDO.
DEF VAR col-head1 AS CHAR NO-UNDO.
DEF VAR col-head2 AS CHAR NO-UNDO.
DEF VAR u-line AS CHAR NO-UNDO.
u-line = FILL("-",{&page-width} ).
DEF VAR e-line AS CHAR NO-UNDO.
e-line = FILL("=",{&page-width}).
&GLOB VERT-POS 73
col-head1-base = "                                                               <----------------------------------- Income Details ---------------------------------------->     <---------- Lease Details ---------->     <------------- Outgoings ------------>".
col-head2-base = "Level Lessee                                         Area SqM     Rental    PSM       Sundry    PSM         Gross    PSM     Parking    No  -----P.C.M.-----                                                 %age    Description    Base   Amount".


&GLOB FIELD-COUNT 12
DEF VAR f AS CHAR EXTENT {&FIELD-COUNT} NO-UNDO. /* an array of the fields we will print */
DEF VAR f-fmt AS CHAR EXTENT {&FIELD-COUNT} NO-UNDO.
DEF VAR notes-fmt AS CHAR NO-UNDO.
DEF VAR notes-queue AS CHAR NO-UNDO.
ASSIGN
/************************************************************/
  notes-fmt = "X(62)"        /* equiv to f[1] + f[2] + f[3] */
/************************************************************/
  f-fmt[1] = "X(6)"         f-fmt[2] = "X(45)"  /* level, name          */
  f-fmt[3] = "X(11)"        f-fmt[4] = "X(41)"  /* area, 1st rentals    */
  f-fmt[5] = "X(36)"        f-fmt[6] = "X(22)"  /* 2nd rentals, rent pcm*/
  f-fmt[7] = "X(20)"        f-fmt[8] = "X(33)"  /* 1st lease, 2nd lease */
  f-fmt[9] = "X(30)"        f-fmt[10] = "X(8)"  /* outgoings */
  f-fmt[11] = "X(10)"       f-fmt[12] = "X(4)"  /* */
.

DEF VAR page-reset AS CHAR NO-UNDO  INITIAL "reset,landscape,tm,1,a4,lm,4".
DEF VAR time-font AS CHAR NO-UNDO INITIAL "Helvetica,point,5,proportional,bold".
DEF VAR base-font AS CHAR NO-UNDO   INITIAL "fixed,courier,cpi,23,lpi,12,normal".
DEF VAR property-header-font AS CHAR NO-UNDO INITIAL "Helvetica,point,16,proportional,bold".
DEF VAR page-header-font AS CHAR NO-UNDO INITIAL "Helvetica,point,12,proportional,bold".
DEF VAR error-font1 AS CHAR NO-UNDO   INITIAL "courier,lpi,10,cpi,18,normal,bold,fixed".
DEF VAR error-font2 AS CHAR NO-UNDO   INITIAL "courier,lpi,9,cpi,18,normal,italic,fixed".

/* Accumulators */
DEF VAR l-common AS DEC NO-UNDO    INITIAL 0.
DEF VAR l-occupied AS DEC NO-UNDO    INITIAL 0.
DEF VAR l-vacant AS DEC NO-UNDO    INITIAL 0.
DEF VAR l-rental AS DEC NO-UNDO    INITIAL 0.
DEF VAR l-area AS DEC NO-UNDO    INITIAL 0.
DEF VAR l-og AS DEC NO-UNDO    INITIAL 0.
DEF VAR l-sundry AS DEC NO-UNDO    INITIAL 0.
DEF VAR l-pkrent AS DEC NO-UNDO    INITIAL 0.
DEF VAR l-pkcount AS INT NO-UNDO    INITIAL 0.
DEF VAR l-rent-yr AS DEC NO-UNDO    INITIAL 0.
DEF VAR l-area-yr AS DEC NO-UNDO    INITIAL 0.
DEF VAR l-percent AS DEC NO-UNDO    INITIAL 0.
DEF VAR level-lines AS INT NO-UNDO    INITIAL 0.

DEF VAR lease-years AS DEC NO-UNDO    INITIAL 0.
DEF VAR lease-notes AS CHAR NO-UNDO.
DEF VAR lease-outgoings AS DEC NO-UNDO INITIAL 0.

DEF VAR b-common AS DEC NO-UNDO    INITIAL 0.
DEF VAR b-occupied AS DEC NO-UNDO    INITIAL 0.
DEF VAR b-vacant AS DEC NO-UNDO    INITIAL 0.
DEF VAR b-rental AS DEC NO-UNDO    INITIAL 0.
DEF VAR b-area AS DEC NO-UNDO    INITIAL 0.
DEF VAR b-og AS DEC NO-UNDO    INITIAL 0.
DEF VAR b-sundry AS DEC NO-UNDO    INITIAL 0.
DEF VAR b-pkrent AS DEC NO-UNDO    INITIAL 0.
DEF VAR b-pkcount AS INT NO-UNDO    INITIAL 0.
DEF VAR b-rent-yr AS DEC NO-UNDO    INITIAL 0.
DEF VAR b-area-yr AS DEC NO-UNDO    INITIAL 0.
DEF VAR b-percent AS DEC NO-UNDO    INITIAL 0.

DEF VAR last-level AS INT INITIAL -99999 NO-UNDO.
DEF VAR out-line AS CHAR NO-UNDO.
DEF VAR total-break AS INTEGER NO-UNDO    INITIAL 90.

DEF VAR areas-processed AS CHAR NO-UNDO.
DEF VAR primary-space AS LOGICAL NO-UNDO.

DEF TEMP-TABLE SundryCharge NO-UNDO
    FIELD LeaseCode AS INT        INITIAL 999
    FIELD Amount AS DEC         INITIAL 0
    FIELD Description AS CHAR   INITIAL "Sundry"
    INDEX XPKSundryCharges IS PRIMARY LeaseCode.

DEF TEMP-TABLE XRentReview NO-UNDO
    FIELD LeaseCode AS INT      INITIAL ?
    FIELD ReviewDate AS DATE    INITIAL ?
    FIELD ReviewType AS CHAR    INITIAL ""
    INDEX XPKRentReviews IS PRIMARY LeaseCode ReviewDate.

DEF TEMP-TABLE LeaseDone NO-UNDO
    FIELD LeaseCode AS INT
    FIELD LevelNo AS INT
    FIELD LevelSeq AS INT
    FIELD OGBudget AS DEC INITIAL 0
    FIELD OGPercent AS DEC INITIAL 0
    FIELD LeasedArea AS DEC INITIAL 0
    FIELD LeaseNotes AS CHAR
    INDEX XPKLeasesDone IS UNIQUE PRIMARY LeaseCode
    INDEX XAK1LevelSeq /* IS UNIQUE */ LevelNo LevelSeq.

DEF TEMP-TABLE HeadLease NO-UNDO
    FIELD TenancyLeaseCode AS INT
    FIELD HeadLeaseCode AS INT
    FIELD AreaSize AS DEC INITIAL 0.0 
    FIELD AnnualRent AS DEC INITIAL 0.0 
    INDEX XPKLCode IS UNIQUE PRIMARY TenancyLeaseCode
    INDEX XAKLCode HeadLeaseCode TenancyLeaseCode.

{inc/ofc-this.i}
{inc/ofc-set.i "Statutory-Expense-Accounts" "stat-accounts"}
{inc/ofc-set-l.i "Use-Rent-Charges" "use-rent-charges"}
{inc/ofc-set.i "Schedule-AGP-Agent-Excludes" "agent-exclude-types"}

{inc/ofc-set.i "RentCharge-Outgoings" "og-method"}
IF NOT AVAILABLE(OfficeSetting) THEN og-method = "".
{inc/ofc-set-l.i "RentCharge-OG-By-Lease" "charge-og-lease"}
IF NOT AVAILABLE(OfficeSetting) THEN charge-og-lease = Yes.
/* IF NOT charge-og-lease THEN og-method = "". */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-head-lease) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD head-lease Procedure 
FUNCTION head-lease RETURNS INTEGER
  ( INPUT lease-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-to-annual) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD to-annual Procedure 
FUNCTION to-annual RETURNS DECIMAL
  ( INPUT freq-type AS CHAR, INPUT period-amount AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 24.55
         WIDTH              = 36.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/null.i}
{inc/convert.i}
{inc/method/m-txtrep.i}
{inc/method/m-lease-rentals.i}
{inc/method/m-charged-rent.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

RUN pclrep-start( preview, page-reset + "," + base-font ).
OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES APPEND.

FOR EACH Property WHERE Property.Active AND Property.PropertyCode >= property-1
                    AND Property.PropertyCode <= property-2 
                    AND Property.Active NO-LOCK:
  RUN property-schedule.
END.

OUTPUT CLOSE.

RUN pclrep-finish.

ERROR-STATUS:ERROR = False.
RETURN "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-build-head-leases) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-head-leases Procedure 
PROCEDURE build-head-leases :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER OtherSpace FOR RentalSpace.
DEF BUFFER OtherLease FOR HeadLease.

  DEF VAR hlease AS INT NO-UNDO.
  FOR EACH RentalSpace OF Property WHERE RentalSpace.AreaStatus = "L" NO-LOCK:
    FIND TenancyLease OF RentalSpace NO-LOCK NO-ERROR.

    IF NOT( AVAILABLE(TenancyLease) ) THEN DO:
      RUN pclrep-line( error-font1, "Error: Rental space at level " + STRING(RentalSpace.Level) + "/" + STRING(RentalSpace.LevelSeq) + " "
                                       + " - " + RentalSpace.Description + " - marked as 'leased', but no lease record is associated!").
      NEXT.
    END.

    hlease = head-lease( RentalSpace.TenancyLeaseCode ).
    IF hlease <> ? THEN NEXT.
other-space-loop:
    FOR EACH OtherSpace OF Property WHERE OtherSpace.Level = RentalSpace.Level
                    AND OtherSpace.LevelSeq = RentalSpace.LevelSeq
                    AND OtherSpace.AreaStatus = "L"  NO-LOCK:
      hlease = head-lease( OtherSpace.TenancyLeaseCode ).
      IF hlease <> ? THEN LEAVE other-space-loop.
    END.

    IF hlease = ? THEN hlease = RentalSpace.TenancyLeaseCode.
    CREATE HeadLease.
    HeadLease.TenancyLeaseCode = RentalSpace.TenancyLeaseCode.
    HeadLease.HeadLeaseCode = hlease.
  END.

  FOR EACH HeadLease BY HeadLease.HeadLeaseCode:
    FIND LeaseDone WHERE LeaseDone.LeaseCode = HeadLease.HeadLeaseCode NO-ERROR.
    IF NOT AVAILABLE(LeaseDone) THEN DO:
      CREATE LeaseDone.
      LeaseDone.LeaseCode = HeadLease.HeadLeaseCode.
      LeaseDone.LevelNo   = ?.
      LeaseDone.LevelSeq  = ?.
    END.
    FIND TenancyLease OF HeadLease NO-LOCK.
    IF CAN-FIND( FIRST RentalSpace OF TenancyLease WHERE CAN-FIND(FIRST AreaType OF RentalSpace WHERE AreaType.IsFloorArea) ) THEN ASSIGN
      LeaseDone.OGPercent = LeaseDone.OGPercent + null-dec( TenancyLease.OutgoingsRate, 0.0).

    FIND Note OF TenancyLease NO-LOCK NO-ERROR.
    IF AVAILABLE(Note) THEN
      LeaseDone.LeaseNotes = LeaseDone.LeaseNotes + TRIM(Note.Detail) + "~n".

    FOR EACH RentCharge OF TenancyLease NO-LOCK:
      IF agent-version AND LOOKUP( RentCharge.RentChargeType, agent-exclude-types) > 0 THEN
        NEXT. 
      ELSE IF NOT charge-og-lease AND RentCharge.RentChargeType = og-method THEN DO:
        IF show-future-rentals THEN DO:
          FIND LAST RentChargeLine OF RentCharge WHERE RentChargeLine.RentChargeLineStatus = "C"
                                                 NO-LOCK NO-ERROR.
          IF NOT AVAILABLE(RentChargeLine) OR (RentChargeLine.EndDate = RentChargeLine.LastChargedDate 
              AND RentChargeLine.LastChargedDate <> ? ) THEN
            NEXT.
          LeaseDone.OGBudget  = LeaseDone.OGBudget + to-annual( RentChargeLine.FrequencyCode, RentChargeLine.Amount ).
        END.
        ELSE
          LeaseDone.OGBudget  = LeaseDone.OGBudget + RentCharge.CurrentAnnualRent .
      END.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-clear-property) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-property Procedure 
PROCEDURE clear-property :
/*------------------------------------------------------------------------------
  Purpose:  Clear the work-tables we are using for each property
------------------------------------------------------------------------------*/
  FOR EACH bldg-og: DELETE bldg-og. END.
  FOR EACH bldg-sublease: DELETE bldg-sublease. END.
  FOR EACH SundryCharge:    DELETE SundryCharge.    END.
  FOR EACH XRentReview:     DELETE XRentReview.     END.
  FOR EACH LeaseDone:       DELETE LeaseDone.       END.
  FOR EACH HeadLease:       DELETE HeadLease.       END.
  FOR EACH LeaseRental:     DELETE LeaseRental.     END.
  FOR EACH AreaRental:      DELETE AreaRental.     END.

  ASSIGN
    b-common        = 0    b-occupied      = 0
    b-vacant        = 0    b-rental        = 0
    b-area          = 0    b-sundry        = 0
    b-pkrent        = 0    b-pkcount       = 0
    b-rent-yr       = 0    b-area-yr       = 0
    b-percent       = 0
  .

  notes-queue = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-rental-space) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-rental-space Procedure 
PROCEDURE each-rental-space :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.

  DO i = 1 TO {&FIELD-COUNT}:           f[i] = "".          END.
  ASSIGN
    lease-notes = ""
    lease-years = 0
    lease-outgoings = 0.

  RUN set-lease-details.
  RUN set-area-details.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-last-notes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-last-notes Procedure 
PROCEDURE get-last-notes :
/*------------------------------------------------------------------------------
  Purpose:  Collect together various pieces of inforrmation as notes
------------------------------------------------------------------------------*/
DEF VAR a-line AS CHAR NO-UNDO.
DEF VAR various-notes AS CHAR NO-UNDO INITIAL "".


  /* outgoings for the building */
  IF CAN-FIND( FIRST bldg-og ) THEN DO:
    various-notes = various-notes + CHR(10) + "Outgoings" + CHR(10) + "---------" + CHR(10).
    FOR EACH bldg-og BY bldg-og.Description:
      various-notes = various-notes + STRING( bldg-og.Percentage, ">>>>>>9.99 ") + bldg-og.Description + CHR(10).
    END.
  END.

  /* sub leases for the building */
  IF CAN-FIND( FIRST bldg-sublease ) THEN DO:
    various-notes = various-notes + CHR(10) + "Sub Leases" + CHR(10) + "----------" + CHR(10).
    FOR EACH bldg-sublease:
      FIND TenancyLease OF bldg-sublease NO-LOCK.
      FIND Tenant OF Tenancylease NO-LOCK NO-ERROR.
      various-notes = various-notes + (IF AVAILABLE(Tenant) THEN Tenant.Name ELSE "Tenant not on file") + CHR(10).
      various-notes = various-notes + "  To: " + bldg-sublease.Name + CHR(10).
      a-line = (IF bldg-sublease.AnnualRental > 0 THEN ("  Rent:" + STRING( bldg-sublease.AnnualRental, ">>>>>>>>>.99")) ELSE "")
             + (IF bldg-sublease.LeaseStartDate > DATE(1,1,1) THEN ("  Start:" + STRING( bldg-sublease.LeaseStartDate, "99/99/9999")) ELSE "")
             + (IF bldg-sublease.LeaseEndDate > DATE(1,1,1) THEN ("  End:" + STRING( bldg-sublease.LeaseEndDate, "99/99/9999")) ELSE "").
      IF TRIM(a-line) <> "" THEN various-notes = various-notes + a-line + CHR(10).
      IF TRIM(bldg-sublease.Detail) <> "" THEN various-notes = various-notes + bldg-sublease.Detail + CHR(10).
      various-notes = various-notes + CHR(10).
    END.
    various-notes = various-notes + CHR(10).
  END.

  /* frontages for the building */
  IF CAN-FIND( FIRST StreetFrontage WHERE StreetFrontage.PropertyCode = Property.PropertyCode ) THEN DO:
    various-notes = various-notes + CHR(10) + "Frontages" + CHR(10) + "---------" + CHR(10).
    FOR EACH StreetFrontage WHERE StreetFrontage.PropertyCode = Property.PropertyCode NO-LOCK:
      various-notes = various-notes + STRING( StreetFrontage.Length, ">>>>>>9.99m  ") + StreetFrontage.Description + CHR(10).
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-notes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-notes Procedure 
PROCEDURE get-notes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER note-code AS INTEGER NO-UNDO.
DEF OUTPUT PARAMETER note-detail AS CHAR NO-UNDO.

DEF VAR i AS INT NO-UNDO.
DEF VAR no-notes AS INT NO-UNDO.

  /* search the list of notes already display for this property
     *** no longer used ***
  no-notes = NUM-ENTRIES( notes-list).
  DO i = 1 TO no-notes:
    IF note-code = INTEGER(ENTRY(i,notes-list)) THEN DO:
      note-detail = "see note [" + TRIM( STRING( i, ">>9")) + "] above.".
      note-code = 0.
      RETURN.
    END.
  END.
  */

  IF note-code <> ? THEN DO:
/*    IF notes-list <> "" THEN notes-list = notes-list + ",".
    notes-list = notes-list + TRIM(STRING( note-code, ">>>>>>>9")).
*/
    FIND Note WHERE Note.NoteCode = note-code NO-LOCK NO-ERROR.
    note-detail = (IF AVAILABLE(Note)
         THEN /* "[" + TRIM( STRING(no-notes + 1, ">>9")) + "] "
            + */ TRIM( REPLACE( Note.Detail, "~r", "") )
         ELSE "" ).
  END.
  ELSE
    note-detail = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-inst-page-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-header Procedure 
PROCEDURE inst-page-header :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN pclrep-line( time-font, timeStamp ).
  RUN pclrep-line( base-font, hline2 ).
  RUN pclrep-line( base-font, hline3 ).
  RUN pclrep-line( base-font, hline4 ).
  RUN pclrep-down-by( 1 ).
  RUN pclrep-line( base-font, col-head1 ).
  RUN pclrep-line( base-font, col-head2 ).
  RUN pclrep-down-by( 0.6 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-new-level) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE new-level Procedure 
PROCEDURE new-level :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER last-level AS INT NO-UNDO.
DEF INPUT PARAMETER next-level AS INT NO-UNDO.

DEF VAR out-line AS CHAR NO-UNDO.
DEF VAR level-units AS CHAR NO-UNDO.
DEF VAR level-code AS CHAR NO-UNDO.

  IF last-level <> -99999 AND (level-lines > 1 OR total-all-levels) THEN DO:
    RUN print-line( u-line ).
    IF l-area = 0 THEN l-area = 2000000001.
    out-line = "Level    Common:" + STRING( l-common, "->>,>>9.999")
             + "   Occupied:" + STRING( l-occupied, "->>,>>9.999")
             + FILL( " ", 10)
             + STRING( l-rental, "->>>>>>>9.99")
             + STRING( (l-rental / l-area), "->>>>9.99")
             + STRING( l-sundry, "->>>>>>9.99")
             + STRING( (l-sundry / l-area), "->>>>9.99")
             + STRING( (l-rental + l-og), "->>>>>>>9.99")
             + STRING( null-dec((l-rental + l-og) / l-occupied, 0.0), "->>>>9.99")
             + STRING( l-pkrent, "->>>>>9.99")
             + STRING( l-pkcount, "->>>>9") + FILL(" ",5)
             + STRING( ((l-rental + l-sundry + l-pkrent) / 12), "->,>>>,>>9.99") + FILL(" ",47)
             + STRING( l-percent, ">>9.99" )
                .
    RUN print-line( out-line ).
    level-code = IF last-level = 0 THEN "  G" ELSE
             ( IF last-level = 90 THEN " PK" ELSE
             ( IF last-level = 80 THEN "  B" ELSE
             ( IF last-level > 80 AND last-level < 90 THEN " B" + STRING(last-level - 80) ELSE
                STRING( last-level, "->9")))).
    out-line = level-code
             + "       Total:" + STRING( l-occupied + l-vacant + l-common, "->>,>>9.999")
             + "     Vacant:" + STRING( l-vacant, "->>,>>9.999") + FILL(" ",93)
             + STRING( (l-rental + l-sundry + l-pkrent), "->,>>>,>>9.99") + " P.A."
                .
    RUN print-line( out-line ).
  END.
  IF (level-lines > 1 OR total-all-levels) THEN RUN print-line( u-line ).
  RUN pclrep-down-by(1).

  ASSIGN
    b-common        = b-common      + l-common
    b-occupied      = b-occupied    + l-occupied
    b-vacant        = b-vacant      + l-vacant
    b-rental        = b-rental      + l-rental
    b-og            = b-og          + l-og
    b-area          = b-area        + (IF l-area > 2000000000 THEN 0 ELSE l-area)
    b-sundry        = b-sundry      + l-sundry
    b-pkrent        = b-pkrent      + l-pkrent
    b-pkcount       = b-pkcount     + l-pkcount
    b-rent-yr       = b-rent-yr     + l-rent-yr
    b-area-yr       = b-area-yr     + l-area-yr
    b-percent       = b-percent     + l-percent

    l-common        = 0
    l-occupied      = 0
    l-vacant        = 0
    l-og            = 0
    l-rental        = 0
    l-area          = 0
    l-sundry        = 0
    l-pkrent        = 0
    l-pkcount       = 0
    l-rent-yr       = 0
    l-area-yr       = 0
    l-percent       = 0
  .

  level-lines = 0.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-new-sundrycharge) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE new-sundrycharge Procedure 
PROCEDURE new-sundrycharge :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER contract-rental AS DEC NO-UNDO.
DEF INPUT PARAMETER area-type AS CHAR NO-UNDO.
DEF INPUT PARAMETER default-description AS CHAR NO-UNDO.
DEF INPUT PARAMETER lease-code AS INT NO-UNDO.

  CREATE SundryCharge.
  SundryCharge.LeaseCode    = lease-code.
  SundryCharge.Amount       = contract-rental.

  IF default-description = "" THEN DO:
    FIND RentChargeType WHERE RentChargeType = area-type NO-LOCK NO-ERROR.
    IF AVAILABLE(RentChargeType) THEN
      default-description = RentChargeType.Description.
  END.

  FIND AreaType WHERE AreaType.AreaType = area-type NO-LOCK NO-ERROR.
  IF area-type = "S" AND default-description <> "" THEN
    SundryCharge.Description  = default-description.
  ELSE IF AVAILABLE(AreaType) THEN
    SundryCharge.Description  = AreaType.Description.
  ELSE
    SundryCharge.Description  = default-description.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR token AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.

{inc/showopts.i "report-options"}
  
  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).

   CASE ENTRY( 1, token ):
      WHEN "Preview" THEN               preview = (ENTRY(2,token) =  "Yes").
      WHEN "Total Every Level" THEN     total-all-levels = (ENTRY(2,token) =  "Yes").
      WHEN "Show Future Rentals" THEN   show-future-rentals = (ENTRY(2,token) =  "Yes").
      WHEN "Properties" THEN ASSIGN
        property-1 = INT(ENTRY(2,token))
        property-2 = INT(ENTRY(3,token)) .
    END CASE.
    
  END.
  all-future-rentals = show-future-rentals.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-areas) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-areas Procedure 
PROCEDURE print-areas :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
DEF VAR fval AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR notes-lines AS INT NO-UNDO.
DEF VAR lines-done AS LOGICAL INITIAL No NO-UNDO.
DEF VAR l-no AS INT NO-UNDO.
DEF VAR notes-start AS INT NO-UNDO.

  areas-processed = "".

  RUN build-head-leases.

  rental-space-loop:
  FOR EACH RentalSpace OF Property NO-LOCK WHERE (RentalSpace.AreaStatus <> "X")
            BY RentalSpace.PropertyCode BY RentalSpace.Level BY RentalSpace.LevelSequence:
    IF LOOKUP( STRING( RentalSpace.RentalSpaceCode), areas-processed ) > 0 THEN NEXT.
    IF RentalSpace.Level <> last-level THEN DO:
      RUN new-level ( last-level, RentalSpace.Level ).
      last-level = RentalSpace.Level.
    END.
    ELSE
      RUN pclrep-down-by( 1 ).

/*  MESSAGE RentalSpace.Level RentalSpace.LevelSequence 
            RentalSpace.AreaType RentalSpace.Description . */
    RUN each-rental-space.

    IF RentalSpace.AreaStatus = "C" THEN DO:
      level-lines = level-lines + 1.
      NEXT.  /* skip printout of common areas */
    END.
    RUN word-wrap( f[2], 50, OUTPUT f[2]).

    lines-done = No.
    l-no = 1.
    notes-start = NUM-ENTRIES( f[2], "~n") + 2.
    notes-lines = NUM-ENTRIES( lease-notes, "~n" ).
    IF TRIM(lease-notes) = "" THEN notes-start = ?.
    IF notes-lines > 4 THEN DO:
      notes-queue = notes-queue + STRING(RentalSpace.TenancyLeaseCode) + ",".
      lease-notes = "See detailed notes at end of report".
    END.
    DO WHILE NOT lines-done:
      out-line = "".
      DO i = 1 TO {&FIELD-COUNT}:
        ASSIGN  fval = ""
                fval = ENTRY( l-no, f[i], "~n") NO-ERROR.
        IF fval = ? THEN fval = "".
        IF i < 3 AND notes-start <> ? AND l-no >= notes-start THEN DO:
          i = 3.
          fval = ENTRY( 1 + l-no - notes-start, lease-notes, "~n") NO-ERROR.
          IF fval = ? THEN fval = "".
          out-line = out-line + STRING( fval, notes-fmt ).
        END.
        ELSE
          out-line = out-line + STRING( fval, f-fmt[i] ).
      END.
/*      MESSAGE out-line. */

      IF TRIM(out-line) = "" AND (notes-start = ? OR l-no > notes-start) THEN DO:
        lines-done = Yes.
      END.
      ELSE
        RUN print-line( out-line ).

      l-no = l-no + 1.
    END.
    level-lines = level-lines + 1.
  END.
  RUN new-level( last-level, 99999 ).
  RUN pclrep-down-by(2).

  /* Print building totals */
  RUN print-line( e-line ).
  IF l-area = 0 THEN l-area = 2000000001.
  out-line = "Grand    Common:" + STRING( b-common, "->>,>>9.999")
           + "   Occupied:" + STRING( b-occupied, "->>,>>9.999")
           + FILL( " ", 10)
           + STRING( b-rental, "->>>>>>>9.99")
           + STRING( (b-rental / b-area), "->>>>9.99")
           + STRING( b-sundry, "->>>>>>9.99")
           + STRING( (b-sundry / b-area), "->>>>9.99")
           + STRING( (b-rental + b-og), "->>>>>>>9.99")
           + STRING( ((b-rental + b-og) / b-occupied), "->>>>9.99")
           + STRING( b-pkrent, "->>>>>9.99")
           + STRING( b-pkcount, "->>>>9") + FILL(" ",5)
           + STRING( ((b-rental + b-sundry + b-pkrent) / 12), "->,>>>,>>9.99") + FILL(" ",47)
           + STRING( b-percent, ">>9.99" )
                .
  RUN print-line( out-line ).
  out-line = "Total     Total:" + STRING( b-occupied + b-vacant + b-common, "->>,>>9.999")
           + "     Vacant:" + STRING( b-vacant, "->>,>>9.999") + FILL(" ",93)
             + STRING( (b-rental + b-sundry + b-pkrent), "->,>>>,>>9.99") + " P.A."
              .
  RUN print-line( out-line ).
  RUN print-line( e-line ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-bldg-notes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-bldg-notes Procedure 
PROCEDURE print-bldg-notes :
/*------------------------------------------------------------------------------
  Purpose:  Print the building notes page
------------------------------------------------------------------------------*/
DEF VAR no-of AS INT NO-UNDO.
DEF VAR no-queue AS INT NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR j AS INT NO-UNDO.

DEF VAR notes AS CHAR NO-UNDO.

  no-queue = NUM-ENTRIES(notes-queue) - 1. /* has a trailing comma */
  DO j = 1 TO no-queue:
    FIND TenancyLease WHERE TenancyLease.TenancyLeaseCode = INT(ENTRY( j, notes-queue )) NO-LOCK NO-ERROR.
    IF AVAILABLE(TenancyLease) THEN DO:
      FIND Note WHERE Note.NoteCode = TenancyLease.NoteCode NO-LOCK NO-ERROR.
      IF AVAILABLE(Note) THEN DO:
        RUN pclrep-down-by(2).
        out-line = "Detailed Lease notes for T"
                 + TRIM(STRING(TenancyLease.TenantCode))
                 + " - "
                 + TenancyLease.AreaDescription .
        RUN print-line( out-line ).
        out-line = FILL( "-", LENGTH(out-line) ).
        RUN print-line( out-line ).
        RUN pclrep-down-by(0.45).
        RUN word-wrap( Note.Detail, 150, OUTPUT notes).
        no-of = NUM-ENTRIES( notes, "~n").
        DO i = 1 TO no-of:
          out-line = FILL( " ", 20) + ENTRY( i, notes, "~n").
          RUN print-line( out-line ).
        END.
      END.
    END.
  END.

  FIND Note WHERE Note.NoteCode = Property.NoteCode NO-LOCK NO-ERROR.
  IF AVAILABLE(Note) THEN DO:
    RUN pclrep-down-by(2).
    out-line = "Building notes for P"
             + TRIM(STRING(Property.PropertyCode))
             + " - "
             + Property.Name .
    RUN print-line( out-line ).
    out-line = FILL( "-", LENGTH(out-line) ).
    RUN print-line( out-line ).
    RUN pclrep-down-by(0.8).
    RUN word-wrap( Note.Detail, 150, OUTPUT notes).
    no-of = NUM-ENTRIES( notes, "~n").
    DO i = 1 TO no-of:
      out-line = FILL( " ", 20) + ENTRY( i, notes, "~n").
      RUN print-line( out-line ).
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-line Procedure 
PROCEDURE print-line :
/*------------------------------------------------------------------------------
  Purpose:  Print a line of output
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER the-line AS CHAR NO-UNDO.

  RUN pclrep-line( base-font, RIGHT-TRIM(the-line)).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-notes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-notes Procedure 
PROCEDURE print-notes :
/*------------------------------------------------------------------------------
  Purpose:  Print the notes
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER left-notes AS CHAR NO-UNDO.
DEF INPUT PARAMETER right-notes AS CHAR NO-UNDO.

DEF VAR out-line AS CHAR NO-UNDO.
DEF VAR one-note AS CHAR NO-UNDO.

DEF VAR no-lines AS INT NO-UNDO.
  no-lines = NUM-ENTRIES( left-notes, CHR(10)).
  IF no-lines < NUM-ENTRIES( right-notes, CHR(10)) THEN no-lines = NUM-ENTRIES( right-notes, CHR(10)).

  DEF VAR l-no AS INTEGER INITIAL 1 NO-UNDO.
  DO l-no = 1 TO no-lines:
    ASSIGN  one-note = ""
            one-note = ENTRY( l-no, left-notes, CHR(10)) NO-ERROR.
    IF one-note = ? THEN one-note = "".
    out-line = '          ' + STRING( one-note, "X(70)") + '     '.

    ASSIGN  one-note = ""
            one-note = ENTRY( l-no, right-notes, CHR(10)) NO-ERROR.
    IF one-note = ? THEN one-note = "".
    out-line = out-line + '          ' + STRING( one-note, "X(100)").

    RUN print-line( out-line ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-property-schedule) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE property-schedule Procedure 
PROCEDURE property-schedule :
/*------------------------------------------------------------------------------
  Purpose:  Schedule for a single building
------------------------------------------------------------------------------*/

  RUN clear-property.
  RUN set-headings.
  RUN build-lease-rentals.

  RUN print-areas.
  RUN print-bldg-notes.
  RUN pclrep-page-break.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-put-blank-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE put-blank-line Procedure 
PROCEDURE put-blank-line :
/*------------------------------------------------------------------------------
  Purpose: Put a blank line - with a vertical in it?
------------------------------------------------------------------------------*/
DEF VAR out-line AS CHAR NO-UNDO.
  out-line = FILL(' ',{&VERT-POS}).
  RUN print-line( out-line, ?).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-area-details) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-area-details Procedure 
PROCEDURE set-area-details :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER AltSpace FOR RentalSpace.

DEF VAR area-rental AS DECIMAL NO-UNDO      INITIAL 0.
DEF VAR area-area AS DECIMAL NO-UNDO      INITIAL 0.
DEF VAR area-og AS DECIMAL NO-UNDO      INITIAL 0.
DEF VAR level-area AS DECIMAL NO-UNDO       INITIAL 0.
DEF VAR level-og AS DECIMAL NO-UNDO         INITIAL 0.
DEF VAR level-rental AS DECIMAL NO-UNDO     INITIAL 0.
DEF VAR level-sundry AS DECIMAL NO-UNDO     INITIAL 0.
DEF VAR level-parks AS INTEGER NO-UNDO      INITIAL 0.
DEF VAR level-pk-rent AS DECIMAL NO-UNDO    INITIAL 0.
DEF VAR sundry-desc AS CHAR NO-UNDO         INITIAL "".
DEF VAR sundry-amnt AS CHAR NO-UNDO         INITIAL "".
DEF VAR parking-desc AS CHAR NO-UNDO        INITIAL "".

DEF VAR lease-rental AS DECIMAL NO-UNDO     INITIAL 0.
DEF VAR lease-og AS DECIMAL NO-UNDO         INITIAL 0.
DEF VAR lease-sundry AS DECIMAL NO-UNDO     INITIAL 0.
DEF VAR lease-cleaning AS DECIMAL NO-UNDO   INITIAL 0.
DEF VAR lease-area AS DECIMAL NO-UNDO       INITIAL 0.
DEF VAR space-type AS CHAR NO-UNDO.
DEF VAR future-values AS LOGI NO-UNDO INITIAL NO.

  f[1] = TRIM( IF RentalSpace.Level = 0  THEN "G"  ELSE
             ( IF RentalSpace.Level = 90 THEN "PK" ELSE
             ( IF RentalSpace.Level = 80 THEN "B" ELSE
             ( IF RentalSpace.Level > 80 AND RentalSpace.Level < 90 THEN "B" + STRING(RentalSpace.Level - 80) ELSE
                  STRING(RentalSpace.Level) ))))
       + "-" + TRIM(STRING(RentalSpace.LevelSequence)).
  RUN test-floor-space( RentalSpace.AreaType, RentalSpace.AreaSize ).
  area-rental = get-area-rental( RentalSpace.PropertyCode, RentalSpace.RentalSpaceCode ).
  IF RETURN-VALUE = "Park" THEN DO:
    parking-desc = parking-desc + "~nCar parking fees"
                 + STRING( area-rental, "->>>>>>>>9.99")
                 + STRING( RentalSpace.AreaSize, ">>>>>9").
    level-parks = level-parks + RentalSpace.AreaSize.
    level-pk-rent = level-pk-rent + area-rental.
  END.
  ELSE IF RETURN-VALUE = "Yes" THEN DO:
    level-area = level-area + RentalSpace.AreaSize.
    level-rental = level-rental + area-rental.
    area-area = RentalSpace.AreaSize.
  END.
  ELSE
    RUN set-sundry-space( RentalSpace.AreaType, RentalSpace.Description, area-rental, INPUT-OUTPUT sundry-desc, INPUT-OUTPUT sundry-amnt, INPUT-OUTPUT level-sundry ).

  IF primary-space THEN DO:
    FOR EACH LeaseRental WHERE LeaseRental.LeaseCode = TenancyLease.TenancyLeaseCode
                           AND LeaseRental.AreaCount = 0:
      IF LeaseRental.AreaType BEGINS "X" THEN NEXT.
      FIND AreaType WHERE AreaType.AreaType = LeaseRental.AreaType NO-LOCK NO-ERROR.
      IF AVAILABLE(AreaType) THEN
        space-type = AreaType.Description.
      ELSE DO:
        FIND RentChargeType WHERE RentChargeType.RentChargeType = LeaseRental.AreaType NO-LOCK NO-ERROR.
        IF AVAILABLE(RentChargeType) THEN
          space-type = RentChargeType.Description.
        ELSE
          space-type = LeaseRental.AreaType.
      END.
/*      MESSAGE LeaseRental.AreaType og-method LeaseRental.AnnualRent . */
      IF LeaseRental.AreaType = og-method THEN ASSIGN
        level-og = level-og + LeaseRental.AnnualRent
        lease-og = lease-og + LeaseRental.AnnualRent.
      RUN set-sundry-space( "S", space-type, LeaseRental.AnnualRent, INPUT-OUTPUT sundry-desc, INPUT-OUTPUT sundry-amnt, INPUT-OUTPUT level-sundry ).
    END.
    future-values = CAN-FIND( FIRST LeaseRental WHERE LeaseRental.LeaseCode = TenancyLease.TenancyLeaseCode AND LeaseRental.Future ).
  END.

  IF RentalSpace.AreaStatus <> "C" AND RentalSpace.AreaStatus <> "V" THEN DO:
    areas-processed = (IF areas-processed = "" THEN "" ELSE (areas-processed + ",")) + STRING(RentalSpace.RentalSpaceCode).
    FOR EACH AltSpace WHERE RentalSpace.PropertyCode = AltSpace.PropertyCode
                        AND RentalSpace.Level = AltSpace.Level
                        AND (AltSpace.AreaStatus <> "X")
                        AND RentalSpace.TenancyLeaseCode = AltSpace.TenancyLeaseCode
                        AND AltSpace.TenancyLeaseCode > 0
                        AND RentalSpace.RentalSpaceCode <> AltSpace.RentalSpaceCode
                        NO-LOCK:
      IF LOOKUP( STRING( AltSpace.RentalSpaceCode), areas-processed ) > 0 THEN NEXT.

      RUN test-floor-space( AltSpace.AreaType, AltSpace.AreaSize).
      space-type = RETURN-VALUE.
      IF level-area <> 0 AND space-type = "Yes" THEN NEXT.

      areas-processed = areas-processed + "," + STRING(AltSpace.RentalSpaceCode).
      area-rental = get-area-rental( AltSpace.PropertyCode, AltSpace.RentalSpaceCode ).
      IF space-type = "Park" THEN DO:
        parking-desc = parking-desc + "~nCar parking fees"
                     + STRING( area-rental, "->>>>>>>>9.99")
                     + STRING( AltSpace.AreaSize, ">>>>>9").
        level-parks = level-parks + AltSpace.AreaSize.
        level-pk-rent = level-pk-rent + area-rental.
      END.
      ELSE IF space-type = "Yes" THEN DO:
        level-area = level-area + AltSpace.AreaSize.
        level-rental = level-rental + area-rental.
      END.
      ELSE DO:
        RUN set-sundry-space( AltSpace.AreaType, AltSpace.Description, area-rental, INPUT-OUTPUT sundry-desc, INPUT-OUTPUT sundry-amnt, INPUT-OUTPUT level-sundry ).
      END.
    END.
  END.

  /* Work out totals for whole of lease in this building */
  ASSIGN    lease-area = 0      lease-rental = 0    lease-sundry = 0   lease-cleaning = 0 .
  FOR EACH AltSpace WHERE RentalSpace.PropertyCode = AltSpace.PropertyCode
                      AND RentalSpace.TenancyLeaseCode = AltSpace.TenancyLeaseCode
                      NO-LOCK:
    RUN test-floor-space( AltSpace.AreaType, AltSpace.AreaSize).
    space-type = RETURN-VALUE.
    area-rental = get-area-rental( AltSpace.PropertyCode, AltSpace.RentalSpaceCode ).
    IF space-type = "Yes" THEN ASSIGN
      lease-area = lease-area + AltSpace.AreaSize
      lease-rental = lease-rental + area-rental.
    ELSE IF space-type = "No" THEN DO:
      lease-sundry = lease-sundry + area-rental.
      IF AltSpace.AreaType = 'L' THEN lease-cleaning = lease-cleaning + area-rental.
    END.
  END.
  /* Charges which are not associatable with an area must be sundry (e.g. O/G) */
  FOR EACH LeaseRental WHERE LeaseRental.LeaseCode = RentalSpace.TenancyLeaseCode
                         AND LeaseRental.AreaCount = 0:
    IF LeaseRental.AreaType BEGINS "X" THEN NEXT.
    lease-sundry = lease-sundry + LeaseRental.AnnualRent.
    IF LeaseRental.AreaType = 'L' THEN lease-cleaning = lease-cleaning + LeaseRental.AnnualRent.
  END.

  IF level-area <> 0 THEN f[3] = STRING( level-area, ">>,>>9.999").
  f[4] = STRING( level-rental, "->>>>>9.99")
       + STRING( (IF level-area <> 0 THEN (level-rental / level-area) ELSE 0), "->>>>9.99")
       + (IF level-sundry = 0
          THEN ""
          ELSE ( STRING( level-sundry, "->>>>>>9.99")
/*               + STRING( (IF lease-area <> 0 THEN (level-sundry / lease-area) ELSE 0 ), "->>>>9.99") */ )
         ).

  IF level-sundry <> 0 THEN DO:
    DEF VAR i AS INT NO-UNDO.
    DEF VAR tmp-amount AS DECIMAL NO-UNDO.

    sundry-amnt = SUBSTRING( sundry-amnt, 2).
    sundry-desc = SUBSTRING( sundry-desc, 2).
    DO i = 1 TO NUM-ENTRIES(sundry-amnt):
      tmp-amount = DECIMAL( ENTRY( i, sundry-amnt) ).
      f[4] = f[4] + "~n" + STRING( ENTRY( i, sundry-desc, "~n"), "X(20)")
           + STRING( tmp-amount, "->>>>>9.99")
           + STRING( (IF lease-area <> 0 THEN (tmp-amount / (IF ENTRY( i, sundry-desc, "~n") = "Cleaning Charges" THEN level-area ELSE lease-area)) ELSE 0 ), "->>>>9.99").
    END.

    IF primary-space THEN
      f[5] = STRING( lease-rental + lease-og, "->>>>>9.99")
           + STRING( (IF lease-area <> 0 THEN ((lease-rental + lease-og) / lease-area) ELSE 0 ), "->>>>9.99").
  END.
  ELSE
    f[5] = FILL( " ", 19).

  IF level-parks <> 0 THEN  
    f[5] = f[5] + STRING( level-pk-rent, ">>>>>>9.99")
                + STRING( level-parks , ">>>>>9").
                
  f[5] = f[5] + parking-desc.

  f[6] = (IF level-rental <> 0 THEN
                 ("~nRental:" + STRING( level-rental / 12, "->>>>>9.99")) ELSE "")
       + (IF level-sundry <> 0 THEN
                 ("~nSundry:" + STRING( level-sundry / 12, "->>>>>9.99")) ELSE "")
       + (IF (level-rental <> 0) AND (level-sundry <> 0) THEN
                 ("~n Total:" + STRING( (level-rental + level-sundry) / 12, "->>>>>9.99")) ELSE "")
       + (IF level-pk-rent <> 0 THEN
                 ("~n  Park:" + STRING( level-pk-rent / 12, "->>>>>9.99")) ELSE "")
       + (IF level-parks <> 0 THEN
                 ("~n  P.Av:" + STRING( (level-pk-rent / level-parks) / 12, "->>>>>9.99")) ELSE "").
  f[6] = SUBSTRING( f[6], 2).
  IF future-values THEN
    f[6] = f[6] + "~n(future rental)".

  l-area = l-area + level-area.
  l-pkcount = l-pkcount + level-parks.

  IF RentalSpace.AreaStatus = "V" THEN ASSIGN
    l-vacant = l-vacant + level-area
    f[4] = ""     f[6] = ""      f[5] = ""
    f[8] = FILL( " ", 22) + (IF RentalSpace.OutgoingsPercentage <> 0 THEN STRING( RentalSpace.OutgoingsPercentage, ">>9.99") ELSE "")
    l-percent = l-percent + RentalSpace.OutgoingsPercentage.
  ELSE IF RentalSpace.AreaStatus = "C" THEN ASSIGN
    l-common = l-common + level-area
    f[4] = ""     f[6] = ""    .
  ELSE ASSIGN
    l-rental = l-rental + level-rental
    l-og = l-og + level-og
    l-sundry = l-sundry + level-sundry
    l-pkrent = l-pkrent + level-pk-rent
    l-occupied = l-occupied + level-area.

/*
  MESSAGE f[1] REPLACE(f[2],"~n"," ") level-rental level-sundry level-rental level-area level-pk-rent level-parks level-og.
*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-headings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-headings Procedure 
PROCEDURE set-headings :
/*------------------------------------------------------------------------------
  Purpose:  Set the headings for the building schedule report.
------------------------------------------------------------------------------*/
DEF VAR pos AS INT NO-UNDO.
DEF VAR pw AS INT NO-UNDO.
DEF VAR pwformat AS CHAR NO-UNDO.

  pw = {&page-width}.
  pwformat = "X(" + STRING( pw ) + ")".

  FIND Company WHERE Company.CompanyCode = Property.CompanyCode NO-LOCK NO-ERROR.
  ASSIGN
    hline2 = (IF AVAILABLE(Company) THEN Company.LegalName ELSE "No Company Assigned")
    hline3 = Property.Name + "  (P" + TRIM( STRING( Property.PropertyCode, ">>>>9")) + ")"
    hline4 = Property.StreetAddress
    col-head1 = col-head1-base
    col-head2 = col-head2-base
  .

  Replace-EOL-With-Commas:
  DO WHILE TRUE:
    pos = INDEX( hline4, CHR(10)).
    IF pos > 0 THEN
      hline4 = SUBSTRING( hline4, 1, pos - 1) + ", " + SUBSTRING( hline4, pos + 1).
    ELSE
      LEAVE Replace-EOL-With-Commas.
  END.
  hline2 = SUBSTRING( STRING("", pwformat), 1, INTEGER((pw - LENGTH(hline2) ) / 2)) + hline2.
  hline3 = SUBSTRING( STRING("", pwformat), 1, INTEGER((pw - LENGTH(hline3) ) / 2)) + hline3.
  hline4 = SUBSTRING( STRING("", pwformat), 1, INTEGER((pw - LENGTH(hline4) ) / 2)) + hline4.

  IF first-building THEN DO:
    VIEW FRAME heading-frame.
    first-building = no.
  END.
  ELSE DO:
    PAGE.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-lease-details) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-lease-details Procedure 
PROCEDURE set-lease-details :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.
DEF VAR options AS CHAR NO-UNDO.
DEF VAR opt-ex AS DATE NO-UNDO.

  IF RentalSpace.AreaStatus = "V" THEN DO:
    f[2] = "* * * Vacant * * *~n" + RentalSpace.Description.
    RETURN.
  END.
  IF RentalSpace.AreaStatus = "C" THEN DO:
    f[2] = "* * * Common * * *~n" + RentalSpace.Description.
    RETURN.
  END.

  FIND TenancyLease OF RentalSpace WHERE TenancyLease.LeaseStatus <> "PAST" NO-LOCK NO-ERROR.

  IF NOT AVAILABLE(TenancyLease) THEN DO:
    f[2] = "Lease not on file!~n" + RentalSpace.Description.
    RETURN.
  END.
  RUN set-tenant-details.
  FIND LeaseDone WHERE LeaseDone.LeaseCode = TenancyLease.TenancyLeaseCode NO-ERROR.
  IF NOT AVAILABLE(LeaseDone) THEN DO:
    CREATE LeaseDone.
    LeaseDone.LeaseCode = TenancyLease.TenancyLeaseCode.
  END.

  lease-years = TenancyLease.LeaseEndDate - TODAY.
  IF lease-years < 0 THEN lease-years = 1 / 12.

  primary-space = (TenancyLease.PrimarySpace = RentalSpace.RentalSpaceCode).
  IF NOT primary-space THEN DO:
    DEF BUFFER OtherSpace FOR RentalSpace.
    FIND FIRST OtherSpace OF TenancyLease
                        WHERE OtherSpace.RentalSpaceCode = TenancyLease.PrimarySpace 
                            OR (OtherSpace.LevelSequence <= RentalSpace.LevelSequence
                            AND OtherSpace.RentalSpaceCode < RentalSpace.RentalSpaceCode
                            AND OtherSpace.Level <= RentalSpace.Level)
        NO-LOCK NO-ERROR .
    primary-space = NOT AVAILABLE(OtherSpace).
  END.
  IF NOT primary-space THEN RETURN.

  RUN get-notes( TenancyLease.NoteCode, OUTPUT lease-notes ).
  RUN word-wrap( lease-notes, 62, OUTPUT lease-notes ).

  f[7] = "Commence:" + STRING( TenancyLease.LeaseStartDate, "99/99/9999")
       + "~n Expires:" + (IF TenancyLease.LeaseEndDate = ? OR TenancyLease.LeaseEndDate < TODAY THEN " monthly"
                        ELSE STRING( TenancyLease.LeaseEndDate, "99/99/9999")).
  FIND FIRST RentReview OF TenancyLease WHERE RentReview.ReviewStatus <> "DONE" 
                        AND RentReview.DateDue <> ? NO-LOCK NO-ERROR.
  IF AVAILABLE(RentReview) THEN DO:
    f[7] = f[7]   + "~n  Review:" + STRING( RentReview.DateDue, "99/99/9999").
    IF RentReview.Earliest <> ? THEN 
      f[7] = f[7] + "~nEarliest:" + STRING( RentReview.Earliest, "99/99/9999").
    IF RentReview.Latest <> ? THEN 
      f[7] = f[7] + "~n  Latest:" + STRING( RentReview.Latest, "99/99/9999").
  END.

  FIND FIRST LeaseType WHERE LeaseType.LeaseType = TenancyLease.LeaseType NO-LOCK NO-ERROR.
  RUN set-outgoing-list.  /* sets f[8] to min outgoings */

  options = TenancyLease.RightsOfRenewal.
  IF options = "none" OR TenancyLease.RORNoticePeriod = ? OR options = "" THEN DO:
    options = "none".
    opt-ex = ?.
  END.
  ELSE DO:
  DEF VAR yy AS INT NO-UNDO.
  DEF VAR mm AS INT NO-UNDO.
  DEF VAR dd AS INT NO-UNDO.
    yy = YEAR(TenancyLease.LeaseEndDate).
    mm = MONTH(TenancyLease.LeaseEndDate) - TenancyLease.RORNoticePeriod .
    dd = DAY(TenancyLease.LeaseEndDate).
    IF mm < 1 THEN ASSIGN   mm = mm + 12    yy = yy - 1 .
    ASSIGN opt-ex = DATE( mm, dd, yy) NO-ERROR.
    IF opt-ex = ? OR
        MONTH(TenancyLease.LeaseEndDate) <> MONTH(TenancyLease.LeaseEndDate + 1) THEN DO:
      ASSIGN mm = mm + 1
             dd = 1.
      IF mm > 12 THEN ASSIGN  mm = 1    yy = yy + 1.
      opt-ex = DATE( mm, dd, yy ) - 1.
    END.
  END.

  n = TRUNCATE( (TenancyLease.LeaseEndDate - TenancyLease.LeaseStartDate) / 365.25, 0 ).
  i = INTEGER((((TenancyLease.LeaseEndDate - TenancyLease.LeaseStartDate) / 365.25) - n ) * 12).
  IF n = ? THEN n = 0.
  IF i = ? THEN i = 1.
  IF i = 12 THEN    ASSIGN     n = n + 1       i = 0  .
  f[8] = "  Life:" + STRING( TRIM( STRING( n, ">>>9")) + "-" + TRIM( STRING( i, ">9")), "X(15)")
                    + f[8]
       + "~nOption:" + TenancyLease.RightsOfRenewal
       + (IF options = "none" THEN "" ELSE ("~n OptEx:" + STRING( opt-ex, "99/99/9999")))
       + "~n Basis:" + (IF AVAILABLE(RentReview) THEN RentReview.EstimateBasis ELSE "")
       + "~n  Type:" + (IF AVAILABLE(LeaseType) THEN LeaseType.Description ELSE "").

  i = NUM-ENTRIES(f[7], "~n").
  n = NUM-ENTRIES(f[8], "~n").
  
  IF (  i < n ) THEN
    f[7] = f[7] + FILL("~n", n - i) .
  ELSE IF n < i THEN
    f[8] = f[8] + FILL("~n", i - n) .

  i = LENGTH( STRING("", f-fmt[7]) ).
  FOR EACH Guarantor NO-LOCK OF TenancyLease, FIRST Person NO-LOCK OF Guarantor :
    options = "Guarantee:" + Guarantor.Type + " "
            + TRIM(STRING( Guarantor.Limit, ">>,>>>,>>9")) + " ".
    options = options + (IF Guarantor.Type = "I" THEN Person.FirstName + " " + Person.LastName ELSE Person.Company).
    f[7] = f[7] + "~n" + SUBSTRING(options, 1, i).
    f[8] = f[8] + "~n" + SUBSTRING(options, i + 1).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-outgoing-list) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-outgoing-list Procedure 
PROCEDURE set-outgoing-list :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR base-names AS CHAR NO-UNDO INITIAL " Council,   Water,Land Tax, General".
DEF VAR base-dates AS DATE EXTENT 4 INITIAL ? NO-UNDO.
DEF VAR base-amnts AS DEC EXTENT 4 INITIAL 0 NO-UNDO.
DEF VAR min-percent AS DEC INITIAL 0 NO-UNDO.
DEF VAR offset AS INT NO-UNDO.
DEF VAR og-total AS DEC INITIAL 0 NO-UNDO.

  FOR EACH TenancyOutgoing OF TenancyLease NO-LOCK:

    IF TenancyOutgoing.Percentage <> ? THEN DO:
      IF TenancyOutgoing.Percentage <> 0 AND
         (TenancyOutgoing.Percentage < min-percent OR min-percent = 0)
      THEN
        min-percent = TenancyOutgoing.Percentage.
    END.

    CASE TenancyOutgoing.AccountCode:
      WHEN 201.00 THEN /* Council */       offset = 1.
      WHEN 202.00 THEN /* Water */         offset = 2.
      WHEN 203.00 THEN /* Land Tax */      offset = 3.
      OTHERWISE        /* General */       offset = 4.
    END CASE.

    IF TenancyOutgoing.BaseYear <> ?
       AND (base-dates[offset] = ?
            OR base-dates[offset] > TenancyOutgoing.BaseYear)
    THEN
      base-dates[offset] = TenancyOutgoing.BaseYear.

    base-amnts[offset] = base-amnts[offset] + TenancyOutgoing.BaseYearAmount.
  END.

  DO offset = 1 TO 4:
    f[9] = f[9] + "~n" + ENTRY( offset, base-names) + ":"
         + (IF base-dates[offset] <> ? THEN STRING( base-dates[offset], "99/99/9999") ELSE FILL(" ",10) )
         + (IF base-amnts[offset] <> 0 THEN STRING( base-amnts[offset], ">>>>>>>>9") ELSE "" ).
    og-total = og-total + base-amnts[offset].
  END.
  f[9] = SUBSTRING( f[9], 2).
  IF og-total <> 0 THEN DO:
    f[9] = f[9] + "~n                   ---------"
                + "~n   Total:          " + STRING( og-total, ">>>>>>>>9").
  END.

  IF TenancyLease.OutgoingsRate <> ? AND TenancyLease.OutgoingsRate <> 0 THEN
        min-percent = TenancyLease.OutgoingsRate.
  f[8] = (IF min-percent <> 0 THEN STRING( min-percent, ">>9.99") ELSE "").
  l-percent = l-percent + min-percent.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-sundry-space) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-sundry-space Procedure 
PROCEDURE set-sundry-space :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER itype AS CHAR NO-UNDO.
DEF INPUT PARAMETER idesc AS CHAR NO-UNDO.
DEF INPUT PARAMETER irent AS DECIMAL NO-UNDO.
DEF INPUT-OUTPUT PARAMETER s-desc AS CHAR NO-UNDO.
DEF INPUT-OUTPUT PARAMETER s-amnt AS CHAR NO-UNDO.
DEF INPUT-OUTPUT PARAMETER s-rent AS DECIMAL NO-UNDO.

DEF VAR area-desc AS CHAR NO-UNDO.
  CASE itype:
    WHEN "S" THEN   area-desc = idesc.
    WHEN "L" THEN   area-desc = "Cleaning Charges".
    OTHERWISE DO:
      FIND AreaType WHERE AreaType.AreaType = itype NO-LOCK NO-ERROR.
      area-desc = (IF AVAILABLE(AreaType) THEN AreaType.Description ELSE idesc).
    END.
  END CASE.

  s-desc = s-desc + "~n" + area-desc.
  s-amnt = s-amnt + "," + STRING( irent, "->>>>>9.99").
  s-rent = s-rent + irent.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-tenant-details) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-tenant-details Procedure 
PROCEDURE set-tenant-details :
/*------------------------------------------------------------------------------
  Purpose:  Get the tenant of the lease.
------------------------------------------------------------------------------*/
  FIND Tenant OF TenancyLease NO-LOCK NO-ERROR.
  IF AVAILABLE(Tenant) THEN DO:
    f[2] = Tenant.Name + " (T" + STRING( Tenant.TenantCode ) + ")~n"
         + RentalSpace.Description.
  END.
  ELSE
    f[2] = "Tenant not on file!~n" + RentalSpace.Description.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-test-floor-space) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE test-floor-space Procedure 
PROCEDURE test-floor-space :
/*------------------------------------------------------------------------------
  Purpose:  Decide if this is actual floor space
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER type AS CHAR NO-UNDO.
DEF INPUT PARAMETER area AS DECIMAL NO-UNDO.

DEF BUFFER LocAreaType FOR AreaType.

  IF area = ? THEN RETURN "No".
  FIND LocAreaType WHERE LocAreaType.AreaType = type NO-LOCK NO-ERROR.
  IF AVAILABLE(LocAreaType) THEN DO:
    IF LocAreaType.IsCarPark THEN RETURN "Park".
    IF LocAreaType.IsFloorArea THEN RETURN "Yes".
    RETURN "No".
  END.

  CASE type:
    WHEN "C" THEN RETURN "Park".
    WHEN "O" THEN RETURN "Yes".
    WHEN "R" THEN RETURN "Yes".
    WHEN "W" THEN RETURN "Yes".
    WHEN "N" THEN RETURN "No".
/*    OTHERWISE
      IF area > 10 OR (area <> INTEGER(area)) THEN RETURN "Yes". */
  END CASE.

  RETURN "No".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-head-lease) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION head-lease Procedure 
FUNCTION head-lease RETURNS INTEGER
  ( INPUT lease-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Convert a lease code into the appropriate head lease
------------------------------------------------------------------------------*/
  FIND HeadLease WHERE HeadLease.TenancyLeaseCode = lease-code NO-ERROR.
  IF AVAILABLE(HeadLease) THEN
    RETURN HeadLease.HeadLeaseCode.
  ELSE
    RETURN ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-to-annual) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION to-annual Procedure 
FUNCTION to-annual RETURNS DECIMAL
  ( INPUT freq-type AS CHAR, INPUT period-amount AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Return an annualised conversion of the amount.
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR annual-amount AS DEC NO-UNDO.

  FIND FrequencyType WHERE FrequencyType.FrequencyCode = freq-type NO-LOCK.

  annual-amount = ((period-amount / FrequencyType.UnitCount) * (IF FrequencyType.RepeatUnits = "D" THEN 365 ELSE 12)).

  RETURN annual-amount.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

