&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/
DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

DEF VAR bank-account-code AS CHAR NO-UNDO.
DEF VAR cheque-1 AS INTEGER NO-UNDO.
DEF VAR cheque-n AS INTEGER NO-UNDO.
DEF VAR date-1 AS DATE NO-UNDO.
DEF VAR date-n AS DATE NO-UNDO.
DEF VAR range-type AS CHAR NO-UNDO INITIAL "cheque".
DEF VAR hide-opex AS LOGICAL NO-UNDO INITIAL No.
DEF VAR authorisation AS LOGICAL NO-UNDO.
DEF VAR preview AS LOGICAL NO-UNDO INITIAL No.
RUN parse-parameters.

FIND BankAccount WHERE BankAccount.BankAccountCode = bank-account-code NO-LOCK NO-ERROR.
IF NOT AVAILABLE(BankAccount) THEN DO:
  MESSAGE "Bank account '" + bank-account-code + "' is not available!"
                   VIEW-AS ALERT-BOX ERROR.
  RETURN.
END.

/* page control */
DEF VAR prt-ctrl AS CHAR NO-UNDO.
DEF VAR cols AS INT NO-UNDO.
DEF VAR rows AS INT NO-UNDO.
DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
{inc/ofc-this.i}
{inc/ofc-set.i "AcctGroup-Opex" "opex-list"}
{inc/ofc-set.i "AcctGroup-Tenex" "tenex-list"}

/* print control */
DEF VAR normal-font AS CHAR NO-UNDO     INITIAL "".
DEF VAR big-font AS CHAR NO-UNDO        INITIAL "".
DEF VAR bold-font AS CHAR NO-UNDO       INITIAL "".
DEF VAR italic-font AS CHAR NO-UNDO     INITIAL "".
IF NOT preview THEN RUN set-font-strings.

/* page header */
&SCOPED-DEFINE page-width 195
&SCOPED-DEFINE with-clause NO-BOX USE-TEXT WIDTH {&page-width}

DEF VAR timeStamp AS CHAR FORMAT "X(54)" NO-UNDO.
timeStamp = "Printed " + STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.
DEF VAR hline2 AS CHAR FORMAT "X({&page-width})" NO-UNDO.
DEF VAR hline3 AS CHAR FORMAT "X({&page-width})" NO-UNDO.
IF range-type = "cheque" THEN
  hline2 = "Payment Summary for cheque numbers " + STRING( cheque-1,"999999") + " to " + STRING( cheque-n,"999999").
ELSE
  hline2 = "Payment Summary for dates " + STRING( date-1,"99/99/99") + " to " + STRING( date-n,"99/99/99").
hline2 = SUBSTRING( STRING("","X({&page-width})"), 1, INTEGER(({&page-width} - LENGTH(hline2) ) / 2)) + hline2.
hline3 = "Drawn on: " + BankAccount.BankName + ", " + BankAccount.BankBranchName + " by " + BankAccount.AccountName.
hline3 = SUBSTRING( STRING("","X({&page-width})"), 1, INTEGER(({&page-width} - LENGTH(hline3) ) / 2)) + hline3.

DEFINE FRAME heading-frame WITH 1 DOWN NO-LABELS {&with-clause} PAGE-TOP.
FORM HEADER
    timeStamp  "Page " + STRING( PAGE-NUMBER ) TO {&page-width} SKIP (1)
    hline2 FORMAT "X({&page-width})"
    hline3 FORMAT "X({&page-width})"
    SKIP (1) /* "column headings" */
    WITH FRAME heading-frame.

DEF VAR grand-total AS DECIMAL NO-UNDO.

DEF VAR EntityName AS CHAR NO-UNDO.
DEF VAR AmountText AS CHAR NO-UNDO.
DEFINE FRAME listing-frame WITH DOWN {&with-clause}.
FORM    Cheque.Date COLUMN-LABEL "   Date   "
        Cheque.ChequeNo  COLUMN-LABEL "Cheque"
        Cheque.Payee COLUMN-LABEL "Payee                                             "
        Voucher.VoucherSeq FORMAT ">>>>>>9" COLUMN-LABEL "Voucher"
        Voucher.ApproverCode FORMAT " X(4)" COLUMN-LABEL "Appvr"
        EntityName FORMAT "X(40)" COLUMN-LABEL "Entity                                  "
        Voucher.Description COLUMN-LABEL "Description                                       "
        AmountText FORMAT "X(16)" COLUMN-LABEL "       Value    "
        WITH FRAME listing-frame.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 20.25
         WIDTH              = 30.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

RUN make-control-string ( "PCL", "reset,landscape,tm,2,a4,lm,6,courier,cpi,18.5,lpi,9",
                OUTPUT prt-ctrl, OUTPUT rows, OUTPUT cols ).

RUN output-control-file ( prt-ctrl ).
OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE VALUE(rows).

VIEW FRAME heading-frame.

IF authorisation THEN RUN authorisation-page.

IF range-type = "cheque" THEN DO:
  FOR EACH Cheque WHERE BankAccount.BankAccountCode = Cheque.BankAccountCode
                  AND Cheque.ChequeNo >= cheque-1
                  AND Cheque.ChequeNo <= cheque-n NO-LOCK:
    RUN each-cheque.
    grand-total = grand-total + Cheque.Amount.
  END.
END.
ELSE DO:
  FOR EACH Cheque WHERE BankAccount.BankAccountCode = Cheque.BankAccountCode
                  AND Cheque.Date >= date-1
                  AND Cheque.Date <= date-n NO-LOCK:
    RUN each-cheque.
    grand-total = grand-total + Cheque.Amount.
  END.
END.

DISPLAY "" @ EntityName WITH FRAME listing-frame.
DOWN WITH FRAME listing-frame.
DISPLAY "===============" @ AmountText WITH FRAME listing-frame.
DOWN WITH FRAME listing-frame.
DISPLAY STRING( grand-total, "->>>,>>>,>>9.99") @ AmountText WITH FRAME listing-frame.
DOWN WITH FRAME listing-frame.

OUTPUT CLOSE.
RUN view-output-file ( preview ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-authorisation-page) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE authorisation-page Procedure 
PROCEDURE authorisation-page :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  PUT SKIP(1) " ".
  PUT CONTROL big-font.
  PUT SKIP(4) "Cheque Authorisation" SKIP "--------------------" SKIP(7).
  PUT UNFORMATTED "Documentation correct:          __________________________________________________________________     date: _____ / _____ / _____" SKIP (8).
  PUT UNFORMATTED "Cheques approved for signature: __________________________________________________________________     date: _____ / _____ / _____" .
  PUT CONTROL normal-font.
  PUT SKIP(4) " ".
  PAGE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-cheque) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-cheque Procedure 
PROCEDURE each-cheque :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR vouchers-total AS DEC NO-UNDO   INITIAL 0.
DEF VAR vouchers-count AS INT NO-UNDO   INITIAL 0.

  IF hide-opex THEN DO:
    FOR EACH Voucher WHERE Voucher.BankAccountCode = Cheque.BankAccountCode
                             AND Voucher.ChequeNo = Cheque.ChequeNo NO-LOCK,
              FIRST VoucherLine OF Voucher NO-LOCK,
              FIRST ChartOfAccount WHERE ChartOfAccount.AccountCode = VoucherLine.AccountCode NO-LOCK:
      IF VoucherLine.EntityType = "P" AND CAN-DO( opex-list, ChartOfAccount.AccountGroupCode ) THEN NEXT.
      IF VoucherLine.EntityType = "P" AND CAN-DO( tenex-list, ChartOfAccount.AccountGroupCode ) THEN NEXT.
      vouchers-count = vouchers-count + 1.
    END.
    IF vouchers-count = 0 THEN RETURN.
  END.

  DISPLAY Cheque.Date  Cheque.ChequeNo Cheque.Payee WITH FRAME listing-frame.
  vouchers-count = 0.
  FOR EACH Voucher WHERE Voucher.BankAccountCode = Cheque.BankAccountCode
                           AND Voucher.ChequeNo = Cheque.ChequeNo NO-LOCK:
    RUN each-voucher( INPUT-OUTPUT vouchers-total ).
    vouchers-count = vouchers-count + 1.
  END.
  IF vouchers-count <> 1 OR vouchers-total <> Cheque.Amount THEN DO:
    DISPLAY "---------------" @ AmountText WITH FRAME listing-frame.
    DOWN WITH FRAME listing-frame.

    IF vouchers-total <> Cheque.Amount THEN DO:
      DISPLAY ("Vouchers = " + TRIM(STRING( vouchers-total, "->>>,>>>,>>9.99")) ) @ Voucher.Description WITH FRAME listing-frame.
    END.
    DISPLAY STRING( Cheque.Amount, "->>>,>>>,>>9.99") @ AmountText WITH FRAME listing-frame.
    DOWN WITH FRAME listing-frame.
  END.
  DISPLAY "" @ AmountText WITH FRAME listing-frame.
  DOWN WITH FRAME listing-frame.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-voucher Procedure 
PROCEDURE each-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT-OUTPUT PARAMETER running-total AS DECIMAL NO-UNDO.

DEF VAR vchr-value AS DECIMAL NO-UNDO.

  vchr-value = Voucher.GoodsValue + Voucher.TaxValue.
  running-total = running-total + vchr-value.

  RUN set-entity-name( Voucher.EntityType, Voucher.EntityCode).
  DISPLAY Voucher.VoucherSeq
          Voucher.ApproverCode
          Voucher.Description
          RETURN-VALUE @ EntityName
          STRING( vchr-value, "->>>,>>>,>>9.99") @ AmountText
          WITH FRAME listing-frame.
  DOWN WITH FRAME listing-frame.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR token AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR fin-year AS INT NO-UNDO.

{inc/showopts.i "report-options"}

  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).
    
    CASE ENTRY( 1, token ):
      WHEN "Preview" THEN               preview = Yes.
      WHEN "NoOpex" THEN                hide-opex = Yes.
      WHEN "AuthorisationPage" THEN     authorisation = Yes.
      WHEN "BankAccount" THEN           bank-account-code = ENTRY(2,token).
      WHEN "ChequeRange" THEN ASSIGN
        range-type = "cheque"
        cheque-1 = INT( ENTRY(2,token) )
        cheque-n = INT( ENTRY(3,token) ).
      WHEN "DateRange" THEN ASSIGN
        range-type = "date"
        date-1 = DATE( ENTRY(2,token) )
        date-n = DATE( ENTRY(3,token) ).
    END CASE.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-entity-name) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-entity-name Procedure 
PROCEDURE set-entity-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER et AS CHAR NO-UNDO.
DEF INPUT PARAMETER ec AS INT NO-UNDO.

  CASE et:
    WHEN "L" THEN DO:
      FIND Company WHERE Company.CompanyCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Company) THEN RETURN Company.LegalName.
    END.
    WHEN "P" THEN DO:
      FIND Property WHERE Property.PropertyCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Property) THEN RETURN Property.Name.
    END.
    WHEN "J" THEN DO:
      FIND Project WHERE Project.ProjectCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Project) THEN RETURN Project.Description.
    END.
    WHEN "T" THEN DO:
      FIND Tenant WHERE Tenant.TenantCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Tenant) THEN RETURN Tenant.Name.
    END.
    WHEN "C" THEN DO:
      FIND Creditor WHERE Creditor.CreditorCode = ec NO-LOCK NO-ERROR.
      IF AVAILABLE(Creditor) THEN RETURN Creditor.Name.
    END.
  END.
  RETURN "Entity " + et + "-" + STRING( ec, "99999") + " not on file!".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-set-font-strings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE set-font-strings Procedure 
PROCEDURE set-font-strings :
/*------------------------------------------------------------------------------
  Purpose:  Set up the font strings
------------------------------------------------------------------------------*/
DEF VAR r AS DEC NO-UNDO.
DEF VAR c AS DEC NO-UNDO.

RUN make-control-string ( "PCL", "courier,cpi,18.5,lpi,9", OUTPUT normal-font, OUTPUT r, OUTPUT c ).
RUN make-control-string ( "PCL", "courier,cpi,15,lpi,9", OUTPUT big-font, OUTPUT r, OUTPUT c ).
RUN make-control-string ( "PCL", "courier,cpi,18.5,lpi,9,bold", OUTPUT bold-font, OUTPUT r, OUTPUT c ).
RUN make-control-string ( "PCL", "courier,cpi,18.5,lpi,9,italic", OUTPUT italic-font, OUTPUT r, OUTPUT c ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

