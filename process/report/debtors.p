&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : debtors.p
    Purpose     : Debtors report
    Author(s)   : Andrew McMillan
  ------------------------------------------------------------------------*/
/* ***************************  Definitions  ************************** */
DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

/* report control parameters */
DEF VAR show-inactives AS LOGICAL NO-UNDO INITIAL Yes.
DEF VAR summarise-part AS LOGICAL NO-UNDO INITIAL No.
DEF VAR sort-by AS CHAR NO-UNDO.
DEF VAR report-all-records AS LOGICAL NO-UNDO INITIAL No.
DEF VAR record-1 AS CHAR NO-UNDO.
DEF VAR record-n AS CHAR NO-UNDO.
DEF VAR exporting AS LOGI NO-UNDO INITIAL No.
DEF VAR exclude-rent AS LOGI NO-UNDO INITIAL No.
DEF VAR file-name AS CHAR NO-UNDO.
DEF VAR list-name AS CHAR NO-UNDO.
DEF VAR min-abs-balance LIKE AccountSummary.Balance NO-UNDO INITIAL ?.
DEF VAR min-debit-balance LIKE AccountSummary.Balance NO-UNDO INITIAL ?.
DEF VAR overdue-balance LIKE AccountSummary.Balance NO-UNDO INITIAL ?.
DEF VAR overdue-periods AS INT NO-UNDO INITIAL ?.
DEF VAR asat-date LIKE AcctTran.Date NO-UNDO.
DEF VAR asat-month LIKE AcctTran.MonthCode NO-UNDO.
DEF VAR asat-month1 LIKE AcctTran.MonthCode NO-UNDO.
DEF VAR asat-month2 LIKE AcctTran.MonthCode NO-UNDO.
DEF VAR preview AS LOGICAL NO-UNDO INITIAL No.
RUN parse-parameters.

DEF VAR base-entity-type AS CHAR INITIAL "T" NO-UNDO.
DEF VAR header-not-displayed AS LOGICAL NO-UNDO INITIAL Yes.
DEF VAR need-break-header AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR need-entity-header AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR need-tenant-header AS LOGICAL INITIAL Yes NO-UNDO.

DEF VAR prt-ctrl AS CHAR NO-UNDO. 
DEF VAR rows AS INT NO-UNDO.
DEF VAR cols AS INT NO-UNDO.

DEF VAR grand-total LIKE    AcctTran.Amount INITIAL 0 NO-UNDO.
DEF VAR break-total LIKE    AcctTran.Amount INITIAL 0 NO-UNDO.
DEF VAR entity-total LIKE   AcctTran.Amount INITIAL 0 NO-UNDO.
DEF VAR tenant-total LIKE   AcctTran.Amount INITIAL 0 NO-UNDO.

DEF VAR t-other-debits LIKE AcctTran.Amount INITIAL 0 NO-UNDO.
DEF VAR t-credits LIKE    AcctTran.Amount INITIAL 0 NO-UNDO.
DEF VAR t-0 LIKE AcctTran.Amount INITIAL 0 NO-UNDO.
DEF VAR t-1 LIKE AcctTran.Amount INITIAL 0 NO-UNDO.
DEF VAR t-2 LIKE AcctTran.Amount INITIAL 0 NO-UNDO.
DEF VAR t-3 LIKE AcctTran.Amount INITIAL 0 NO-UNDO.
DEF VAR amt-fmt               AS CHAR INIT "(>>,>>>,>>9.99)" NO-UNDO.

DEF VAR this-break AS CHAR NO-UNDO.
DEF VAR this-entity AS CHAR NO-UNDO.
DEF VAR money-format AS CHAR INITIAL ">,>>>,>>9.99CR" NO-UNDO.

DEF VAR user-name AS CHAR NO-UNDO.
DEF VAR timeStamp AS CHAR FORMAT "X(54)" NO-UNDO.
DEF VAR dispIntro       AS CHAR FORMAT "X(35)" NO-UNDO.
DEF VAR dispDate        AS CHAR FORMAT "X(8)"  NO-UNDO.
DEF VAR dispReference   AS CHAR FORMAT "X(12)" NO-UNDO.
DEF VAR dispDescription AS CHAR FORMAT "X(50)" NO-UNDO.
DEF VAR dispAmount      AS CHAR FORMAT "X(14)" NO-UNDO.
DEF VAR dispNotes       AS CHAR FORMAT "X(45)" NO-UNDO.
DEF VAR tenant-name     AS CHAR NO-UNDO.
DEF VAR tenant-notes    AS CHAR NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}


&SCOPED-DEFINE page-width 185
DEF VAR hline2 AS CHAR FORMAT "X({&page-width})" NO-UNDO.
DEF VAR hline3 AS CHAR FORMAT "X({&page-width})" NO-UNDO.
hline2 = "Tenant Debtors Report to " + STRING(asat-date,"99/99/9999").
hline2 = SUBSTRING( STRING("","X({&page-width})"), 1, INTEGER(({&page-width} - LENGTH(hline2) ) / 2)) + hline2.


/* define all the frames for the report */
&SCOPED-DEFINE with-clause NO-BOX USE-TEXT NO-LABELS WIDTH {&page-width}

DEFINE FRAME default-frame WITH DOWN {&with-clause}.

DEFINE FRAME heading-frame WITH 1 DOWN {&with-clause} PAGE-TOP.
FORM HEADER
    timeStamp  "Page " + STRING( PAGE-NUMBER ) TO {&page-width} SKIP (1)
    hline2 FORMAT "X({&page-width})" SKIP
    hline3 FORMAT "X({&page-width})" SKIP
    WITH FRAME heading-frame.

DEFINE FRAME report-line WITH DOWN {&with-clause}.
FORM dispIntro FORMAT "X(45)"
     dispDate  FORMAT "X(8)"
     dispReference FORMAT "X(12)"
     dispDescription FORMAT "X(50)"
     dispAmount FORMAT "X(14)"
     dispNotes
    WITH FRAME report-line.

DEFINE WORK-TABLE Manager NO-UNDO
    FIELD Manager LIKE Property.Manager
    FIELD LastName AS CHAR 
    FIELD FullName AS CHAR.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 20.6
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}
{inc/convert.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
{inc/username.i "user-name"}
timeStamp = "Printed at " + STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.

RUN make-control-string ( "PCL", "reset,landscape,tm,2,a4,lm,6,courier,cpi,18,lpi,9",
                OUTPUT prt-ctrl, OUTPUT rows, OUTPUT cols ).

RUN output-control-file ( prt-ctrl ).
OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE VALUE(rows).

RUN debtors-report.

OUTPUT CLOSE.

IF exporting THEN
  MESSAGE "Export Complete" VIEW-AS ALERT-BOX INFORMATION TITLE "Finished".
ELSE
  RUN view-output-file ( preview ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-break-footing) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE break-footing Procedure 
PROCEDURE break-footing :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER brk-name AS CHAR NO-UNDO.

  RUN disp-totals( break-total, "Total for " + brk-name ).
  grand-total = grand-total + break-total.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-break-heading) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE break-heading Procedure 
PROCEDURE break-heading :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER brk-name AS CHAR NO-UNDO.

  IF NOT need-break-header THEN RETURN.
  need-break-header = No.

  hline3 = sort-by + " - " + brk-name.
  hline3 = SUBSTRING( STRING("","X({&page-width})"), 1, INTEGER(({&page-width} - LENGTH(hline3) ) / 2)) + hline3.
  IF header-not-displayed THEN DO:
    VIEW FRAME heading-frame.
    header-not-displayed = No.
  END.
  ELSE
    PAGE.

  break-total = 0.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-build-manager-table) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-manager-table Procedure 
PROCEDURE build-manager-table :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FOR EACH Property NO-LOCK:
    FIND FIRST Manager WHERE Property.Manager = Manager.Manager NO-ERROR.
    IF NOT AVAILABLE(Manager) THEN DO:
      CREATE Manager.
      Manager.Manager = Property.Manager.
      FIND Person WHERE Person.PersonCode = Manager.Manager NO-LOCK NO-ERROR.
      IF AVAILABLE(Person) THEN ASSIGN
        Manager.LastName = Person.LastName 
        Manager.FullName = Person.FirstName + " " + Person.LastName .
      ELSE ASSIGN
        Manager.LastName = "ZZZZZZZZZZZZZZZZZZZZZ" 
        Manager.FullName = "Manager Record not on file" .
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-debtors-report) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE debtors-report Procedure 
PROCEDURE debtors-report :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

IF sort-by = "Manager" THEN
  RUN report-by-manager.
ELSE IF sort-by = "Region" THEN
  RUN report-by-region.
ELSE IF sort-by = "Property" THEN
  RUN report-by-property.
ELSE IF sort-by = "Company" THEN
  RUN report-by-company.

IF report-all-records AND sort-by <> "Company" THEN RUN non-property-debtors.

IF min-abs-balance > 0.01 OR min-abs-balance = ? OR grand-total <> 0 THEN
DO WITH FRAME report-line:
  RUN disp-totals( grand-total, "Total for entire report" ).

  IF min-abs-balance >= 0.01 OR min-abs-balance = ? THEN DO:
    DISPLAY   "Other debtors not shown" @ dispDescription
              STRING( t-other-debits, money-format) @ dispAmount.
    DOWN.
    RUN disp-totals( grand-total + t-other-debits, "This report + other debit balances").

    DISPLAY " " @ dispIntro " " @ dispDate " " @ dispReference " " @ dispDescription "============  " @ dispAmount " " @ dispNotes. DOWN.
    DISPLAY " " @ dispAmount. DOWN.

    DISPLAY   "Credit balances not shown" @ dispDescription
              STRING( t-credits, money-format) @ dispAmount.
    DOWN.
    RUN disp-totals( grand-total + t-other-debits + t-credits, "Grand Total Debtors").

    DISPLAY " " @ dispIntro " " @ dispDate " " @ dispReference " " @ dispDescription "============  " @ dispAmount " " @ dispNotes. DOWN.
    DISPLAY " " @ dispAmount. DOWN.

    DISPLAY   "Total 3 Months and over" @ dispDescription STRING( t-3, money-format) @ dispAmount.    DOWN.
    DISPLAY   "Total 2 Months " @ dispDescription STRING( t-2, money-format) @ dispAmount.    DOWN.
    DISPLAY   "Total 1 Month" @ dispDescription STRING( t-1, money-format) @ dispAmount.    DOWN.
    DISPLAY   "Total Current Period" @ dispDescription STRING( t-0, money-format) @ dispAmount.    DOWN.
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-disp-totals) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp-totals Procedure 
PROCEDURE disp-totals :
/*------------------------------------------------------------------------------
  Purpose:  Display a totals line
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER this-total LIKE AcctTran.Amount NO-UNDO.
DEF INPUT PARAMETER this-text LIKE dispDescription NO-UNDO.

  DISPLAY
            " " @ dispIntro
            " " @ dispDate
            " " @ dispReference
            " " @ dispDescription
            "------------  " @ dispAmount
            " " @ dispNotes
            WITH FRAME report-line.
  DOWN WITH FRAME report-line.
  DISPLAY   this-text @ dispDescription
            STRING( this-total, money-format) @ dispAmount
            WITH FRAME report-line.
  DOWN WITH FRAME report-line.
  PUT UNFORMATTED SKIP.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-account) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-account Procedure 
PROCEDURE each-account :
/*------------------------------------------------------------------------------
  Purpose:  Find each suitable transaction for the tenant
------------------------------------------------------------------------------*/
DEF VAR l-no AS INTEGER INITIAL 0 NO-UNDO.
DEF VAR no-notes AS INTEGER NO-UNDO.
DEF VAR no-names AS INTEGER NO-UNDO.

DEF VAR bal-t AS DEC NO-UNDO.
DEF VAR bal-0 AS DEC NO-UNDO.
DEF VAR bal-1 AS DEC NO-UNDO.
DEF VAR bal-2 AS DEC NO-UNDO.
DEF VAR bal-3 AS DEC NO-UNDO.
DEF VAR bal-overdue AS DEC NO-UNDO.

  tenant-total = 0.
  need-tenant-header = Yes.
  dispNotes = "".

  IF min-abs-balance >= 0.01 OR min-debit-balance >= 0.01 THEN DO:
    /* using some new options */
    RUN get-balances( Tenant.TenantCode, 
                        OUTPUT bal-t, OUTPUT bal-0, OUTPUT bal-1, OUTPUT bal-2, OUTPUT bal-3,
                        OUTPUT bal-overdue ).

    IF (overdue-balance <> ? AND bal-overdue >= overdue-balance) THEN DO:
      IF bal-t < 0 THEN DO:
        t-credits = t-credits + bal-t.
        RETURN.
      END.
      /* else fall through to show debtor on report */
    END.
    ELSE IF (min-debit-balance <> ? AND bal-t < min-debit-balance) THEN DO:
      IF bal-t < 0 THEN t-credits = t-credits + bal-t.
                   ELSE ASSIGN t-other-debits = t-other-debits + bal-t
                               t-0 = t-0 + bal-0
                               t-1 = t-1 + bal-1
                               t-2 = t-2 + bal-2
                               t-3 = t-3 + bal-3 .
      RETURN.
    END.
    ELSE IF (min-abs-balance <> ? AND ABS(bal-t) < ABS(min-abs-balance)) THEN DO:
      IF bal-t < 0 THEN t-credits = t-credits + bal-t.
                   ELSE ASSIGN t-other-debits = t-other-debits + bal-t
                               t-0 = t-0 + bal-0
                               t-1 = t-1 + bal-1
                               t-2 = t-2 + bal-2
                               t-3 = t-3 + bal-3 .
      RETURN.
    END.
  END.

  t-0 = t-0 + bal-0.
  t-1 = t-1 + bal-1.
  t-2 = t-2 + bal-2.
  t-3 = t-3 + bal-3.

  FIND Note WHERE Note.NoteCode = Tenant.NoteCode NO-LOCK NO-ERROR.
  RUN word-wrap( (IF AVAILABLE(Note) THEN Note.Detail ELSE ""), 45, OUTPUT tenant-notes).
  RUN word-wrap( STRING(Tenant.TenantCode,"99999") + " " + Tenant.Name, 29, OUTPUT tenant-name).
  no-notes = NUM-ENTRIES( tenant-notes, CHR(10) ).
  no-names = NUM-ENTRIES( tenant-name, CHR(10) ).

  FOR EACH AcctTran NO-LOCK
            WHERE AcctTran.EntityType = base-entity-type
              AND AcctTran.EntityCode = Tenant.TenantCode
              AND AcctTran.AccountCode = sundry-debtors
              AND AcctTran.MonthCode <= asat-month
              AND (AcctTran.ClosingGroup = ? OR AcctTran.ClosingGroup = 0 OR (IF summarise-part THEN No ELSE AcctTran.ClosedState = "P") ),
            FIRST Document OF AcctTran NO-LOCK
            BY AcctTran.EntityType BY AcctTran.EntityCode BY AcctTran.AccountCode
            BY AcctTran.MonthCode BY AcctTran.Date
            BY AcctTran.BatchCode BY AcctTran.DocumentCode BY AcctTran.TransactionCode:
    l-no = l-no + 1.
    IF l-no <= no-notes THEN dispNotes = ENTRY( l-no, tenant-notes, CHR(10)).
    IF l-no <= no-names THEN dispIntro = FILL(' ', 6) + ENTRY( l-no, tenant-name, CHR(10)).

    RUN for-each-transaction.
    ASSIGN      dispIntro = ""          dispNotes = ""  .
  END.

 /* catch transactions which have closed, but where part of the group is after
  * the end of the report
  */
  DEF VAR group-list AS CHAR NO-UNDO INITIAL "".
  DEF VAR group-code AS CHAR NO-UNDO.
  DEF BUFFER ClosedTran FOR AcctTran.
  
  next-closed-tran:
  FOR EACH ClosedTran NO-LOCK
            WHERE ClosedTran.EntityType = base-entity-type
              AND ClosedTran.EntityCode = Tenant.TenantCode
              AND ClosedTran.AccountCode = sundry-debtors
              AND ClosedTran.MonthCode > asat-month
              AND ClosedTran.ClosingGroup > 0
              AND ClosedTran.ClosedState = "F"
            BY ClosedTran.EntityType BY ClosedTran.EntityCode
            BY ClosedTran.AccountCode BY ClosedTran.ClosingGroup:

    group-code = STRING(ClosedTran.ClosingGroup).
    IF LOOKUP( group-code, group-list ) > 0 THEN NEXT next-closed-tran.
    group-list = group-list + group-code + ",".

    FOR EACH AcctTran WHERE AcctTran.EntityType = base-entity-type
                      AND AcctTran.EntityCode = Tenant.TenantCode
                      AND AcctTran.AccountCode = sundry-debtors
                      AND AcctTran.ClosingGroup = ClosedTran.ClosingGroup
                      AND AcctTran.MonthCode <= asat-month NO-LOCK,
            FIRST Document OF AcctTran NO-LOCK
            BY AcctTran.EntityType BY AcctTran.EntityCode BY AcctTran.AccountCode
            BY AcctTran.MonthCode BY AcctTran.Date
            BY AcctTran.BatchCode BY AcctTran.DocumentCode BY AcctTran.TransactionCode:

      l-no = l-no + 1.
      IF l-no <= no-notes THEN dispNotes = ENTRY( l-no, tenant-notes, CHR(10)).
      IF l-no <= no-names THEN dispIntro = FILL(' ', 6) + ENTRY( l-no, tenant-name, CHR(10)).

      RUN for-each-transaction.
      ASSIGN      dispIntro = ""          dispNotes = ""  .
    END.
  END.

  IF summarise-part THEN DO:
    FOR EACH ClosingGroup NO-LOCK
            WHERE ClosingGroup.EntityType = base-entity-type
              AND ClosingGroup.EntityCode = Tenant.TenantCode
              AND ClosingGroup.AccountCode = sundry-debtors
              AND ClosingGroup.ClosedStatus = "P"
              BY ClosingGroup.EntityType BY ClosingGroup.EntityCode
              BY ClosingGroup.AccountCode BY ClosingGroup.Date:
      l-no = l-no + 1.
      IF l-no <= no-notes THEN dispNotes = ENTRY( l-no, tenant-notes, CHR(10)).
      IF l-no <= no-names THEN dispIntro = FILL(' ', 6) + ENTRY( l-no, tenant-name, CHR(10)).
      RUN for-each-closing-group.
      ASSIGN      dispIntro = ""          dispNotes = ""  .
    END.
  END.

  IF l-no < 1 THEN RETURN.

  DO WHILE ( l-no < no-notes ):
    IF need-tenant-header THEN RUN tenant-heading.

    l-no = l-no + 1.
    dispNotes = (IF l-no <= no-notes THEN ENTRY( l-no, tenant-notes, CHR(10)) ELSE "").
    dispIntro = (IF l-no <= no-names THEN
                      FILL(' ', 6) + 
                      STRING( Tenant.TenantCode, "99999") + " " + 
                      ENTRY( l-no, tenant-name, CHR(10)) ELSE "").

    DISPLAY
        dispIntro
        "" @ dispDate
        "" @ dispReference
        "" @ dispDescription
        "" @ dispAmount
        dispNotes
        WITH FRAME report-line.
    DOWN WITH FRAME report-line.

  END.

  RUN disp-totals( tenant-total, "" ).
  entity-total = entity-total + tenant-total.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-company) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-company Procedure 
PROCEDURE each-company :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  need-break-header = Yes.
  this-break = STRING(Company.CompanyCode, "99999") + " " + Company.LegalName.
  FOR EACH Property OF Company NO-LOCK BY Property.PropertyCode:
    need-entity-header = Yes.
    this-entity = STRING(Property.PropertyCode, "99999") + " " + Property.Name.
    FOR EACH Tenant NO-LOCK WHERE (Tenant.Active OR show-inactives)
                            AND Tenant.EntityType = "P"
                            AND Tenant.EntityCode = Property.PropertyCode
                            BY Tenant.TenantCode:
      RUN each-tenant.
    END.
    IF NOT(need-entity-header) THEN RUN entity-footing( this-entity ).
  END.
  need-entity-header = Yes.
  this-entity = "L" + STRING(Company.CompanyCode) + " - Non-Property Debtors".
  FOR EACH Tenant NO-LOCK WHERE (Tenant.Active OR show-inactives)
                          AND Tenant.EntityType = "L"
                          AND Tenant.EntityCode = Company.CompanyCode
                          BY Tenant.TenantCode:
    RUN each-tenant.
  END.
  IF NOT(need-entity-header) THEN RUN entity-footing( this-entity ).

  IF NOT(need-break-header) THEN RUN break-footing( this-break ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-tenant) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-tenant Procedure 
PROCEDURE each-tenant :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER AltTran FOR AcctTran.

/* This IF statement is so horridible I decided to put it in it's own routine */    
  FIND FIRST AcctTran WHERE AcctTran.EntityType = base-entity-type
                    AND AcctTran.EntityCode = Tenant.TenantCode
                    AND AcctTran.AccountCode = sundry-debtors
                    AND AcctTran.MonthCode <= asat-month
                    AND (AcctTran.ClosingGroup = ?
                         OR AcctTran.ClosedState = "P"
                         OR CAN-FIND(FIRST AltTran WHERE AltTran.EntityType = base-entity-type
                                        AND AltTran.EntityCode = Tenant.TenantCode
                                        AND AltTran.AccountCode = sundry-debtors
                                        AND AltTran.MonthCode > asat-month
                                        AND AltTran.ClosedState = "F")
                        ) NO-LOCK NO-ERROR.

  IF AVAILABLE(AcctTran) THEN RUN each-account.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-entity-footing) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-footing Procedure 
PROCEDURE entity-footing :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER e-name AS CHAR NO-UNDO.

  RUN disp-totals( entity-total, "Total for " + e-name ).
  break-total = break-total + entity-total.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-entity-heading) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE entity-heading Procedure 
PROCEDURE entity-heading :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER e-name AS CHAR NO-UNDO.

  IF NOT need-entity-header THEN RETURN.
  need-entity-header = No.
  IF need-break-header THEN RUN break-heading( this-break ).

  DISPLAY
    FILL( ' ', 3) + e-name @ dispIntro
    ' ' @ dispDate
    ' ' @ dispReference
    ' ' @ dispDescription
    ' ' @ dispAmount
    ' ' @ dispNotes
    WITH FRAME report-line.
  DOWN WITH FRAME report-line.
  DISPLAY
    FILL( ' ', 3) + FILL('-', LENGTH(e-name)) @ dispIntro
    ' ' @ dispDate
    ' ' @ dispReference
    ' ' @ dispDescription
    ' ' @ dispAmount
    ' ' @ dispNotes
    WITH FRAME report-line.
  DOWN WITH FRAME report-line.

  entity-total = 0.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-for-each-closing-group) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE for-each-closing-group Procedure 
PROCEDURE for-each-closing-group :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR grp-amount AS DEC NO-UNDO INITIAL 0.

  FIND LAST AcctTran NO-LOCK OF ClosingGroup WHERE AcctTran.MonthCode <= asat-month NO-ERROR.
  IF NOT AVAILABLE(AcctTran) THEN RETURN.

  ASSIGN    dispDate = STRING( ClosingGroup.Date, "99/99/99" )
            dispReference   = "CG " + STRING( ClosingGroup.ClosingGroup)
            dispDescription = (IF ClosingGroup.Description <> "" THEN ClosingGroup.Description ELSE AcctTran.Description) .

  IF dispDescription = "" /* OR dispReference = "" */ THEN DO:
    FIND Document NO-LOCK OF AcctTran.
    IF dispDescription = "" THEN dispDescription = Document.Description.
/*     IF dispReference  =  "" THEN dispReference  =  Document.Reference. */
  END.

  DEF BUFFER AltTran FOR AcctTran.
  FOR EACH AltTran NO-LOCK OF ClosingGroup WHERE AltTran.MonthCode <= asat-month:
    grp-amount = grp-amount + AltTran.Amount.
  END.

  dispAmount = STRING( grp-amount, money-format ).
  tenant-total = tenant-total + grp-amount.

  IF need-tenant-header THEN RUN tenant-heading.
  DISPLAY
        dispIntro
        dispDate
        dispReference
        dispDescription
        dispAmount
        dispNotes
        WITH FRAME report-line.

  DOWN WITH FRAME report-line.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-for-each-transaction) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE for-each-transaction Procedure 
PROCEDURE for-each-transaction :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF need-tenant-header THEN RUN tenant-heading.
      ASSIGN
        dispDate = STRING( AcctTran.Date, "99/99/99" )
        dispReference =   IF (AcctTran.Reference = "")   THEN (Document.Reference)   ELSE (AcctTran.Reference)
        dispDescription = IF (AcctTran.Description = "") THEN (Document.Description) ELSE (AcctTran.Description)
        dispAmount = STRING( AcctTran.Amount, money-format )
        tenant-total = tenant-total + AcctTran.Amount .

      DISPLAY
        dispIntro
        dispDate
        dispReference
        dispDescription
        dispAmount
        dispNotes
        WITH FRAME report-line.

      DOWN WITH FRAME report-line.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-balances) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-balances Procedure 
PROCEDURE get-balances :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER ec AS INT NO-UNDO.
DEF OUTPUT PARAMETER bt AS DEC NO-UNDO INITIAL 0.0 .
DEF OUTPUT PARAMETER b0 AS DEC NO-UNDO INITIAL 0.0 .
DEF OUTPUT PARAMETER b1 AS DEC NO-UNDO INITIAL 0.0 .
DEF OUTPUT PARAMETER b2 AS DEC NO-UNDO INITIAL 0.0 .
DEF OUTPUT PARAMETER b3 AS DEC NO-UNDO INITIAL 0.0 .
DEF OUTPUT PARAMETER overdue AS DEC NO-UNDO INITIAL 0.0 .

  FOR EACH AcctTran NO-LOCK WHERE AcctTran.EntityType   = 'T'
                AND AcctTran.EntityCode   = ec
                AND AcctTran.AccountCode  = sundry-debtors
                AND AcctTran.MonthCode   <= asat-month
                AND (AcctTran.ClosingGroup = 0 OR AcctTran.ClosingGroup = ?
                     OR (IF summarise-part THEN No ELSE AcctTran.ClosedState = "P") ),
                FIRST Document OF AcctTran NO-LOCK:
    IF exclude-rent AND Document.DocumentType = "RENT" THEN NEXT.

    IF AcctTran.MonthCode >= asat-month THEN
      b0 = b0 + AcctTran.Amount.
    ELSE IF AcctTran.MonthCode = asat-month1 THEN
      b1 = b1 + AcctTran.Amount.
    ELSE IF AcctTran.MonthCode = asat-month2 THEN
      b2 = b2 + AcctTran.Amount.
    ELSE
      b3 = b3 + AcctTran.Amount.
  END.

 /* catch transactions which have closed, but where part of the group is after
  * the end of the report
  */
  DEF VAR group-list AS CHAR NO-UNDO INITIAL "".
  DEF VAR group-code AS CHAR NO-UNDO.
  DEF BUFFER ClosedTran FOR AcctTran.
  DEF VAR d-min AS INT NO-UNDO.
  DEF VAR d-max AS INT NO-UNDO.
  DEF VAR group-total AS DEC NO-UNDO.

  IF summarise-part THEN DO:
    part-closed-loop:
    FOR EACH ClosedTran NO-LOCK
              WHERE ClosedTran.EntityType = base-entity-type
                AND ClosedTran.EntityCode = ec
                AND ClosedTran.AccountCode = sundry-debtors
                AND ClosedTran.MonthCode <= asat-month
                AND ClosedTran.ClosingGroup > 0
                AND ClosedTran.ClosedState = "P":

      group-code = STRING(ClosedTran.ClosingGroup).
      IF LOOKUP( group-code, group-list ) > 0 THEN NEXT part-closed-loop.
      group-list = group-list + group-code + ",".

      d-min = ClosedTran.MonthCode.
      d-max = ClosedTran.MonthCode.
      group-total = 0.
      FOR EACH AcctTran WHERE AcctTran.EntityType   = 'T'
                  AND AcctTran.EntityCode   = Tenant.TenantCode
                  AND AcctTran.AccountCode  = sundry-debtors
                  AND AcctTran.MonthCode <= asat-month
                  AND AcctTran.ClosingGroup = ClosedTran.ClosingGroup NO-LOCK,
                FIRST Document OF AcctTran NO-LOCK:
        d-min = MIN(d-min, AcctTran.MonthCode).
        d-max = MAX(d-max, AcctTran.MonthCode).
        IF exclude-rent AND Document.DocumentType = "RENT" THEN NEXT.
        group-total = group-total + AcctTran.Amount.
      END.
      IF group-total > 0 THEN d-min = d-max.

      IF d-min >= asat-month THEN
        b0 = b0 + group-total.
      ELSE IF d-min = asat-month1 THEN
        b1 = b1 + group-total.
      ELSE IF d-min = asat-month2 THEN
        b2 = b2 + group-total.
      ELSE
        b3 = b3 + group-total.

    END.
  END.

  group-list = "".
  next-closed-tran:
  FOR EACH ClosedTran NO-LOCK
            WHERE ClosedTran.EntityType = base-entity-type
              AND ClosedTran.EntityCode = ec
              AND ClosedTran.AccountCode = sundry-debtors
              AND ClosedTran.MonthCode > asat-month
              AND ClosedTran.ClosingGroup > 0
              AND ClosedTran.ClosedState = "F":

    group-code = STRING(ClosedTran.ClosingGroup).
    IF LOOKUP( group-code, group-list ) > 0 THEN NEXT next-closed-tran.
    group-list = group-list + group-code + ",".

    FOR EACH AcctTran WHERE AcctTran.EntityType = base-entity-type
                      AND AcctTran.EntityCode = ec
                      AND AcctTran.AccountCode = sundry-debtors
                      AND AcctTran.ClosingGroup = ClosedTran.ClosingGroup
                      AND AcctTran.MonthCode <= asat-month NO-LOCK,
                FIRST Document OF AcctTran NO-LOCK:
      IF exclude-rent AND Document.DocumentType = "RENT" THEN NEXT.

      IF AcctTran.MonthCode >= asat-month THEN
        b0 = b0 + AcctTran.Amount.
      ELSE IF AcctTran.MonthCode = asat-month1 THEN
        b1 = b1 + AcctTran.Amount.
      ELSE IF AcctTran.MonthCode = asat-month2 THEN
        b2 = b2 + AcctTran.Amount.
      ELSE
        b3 = b3 + AcctTran.Amount.

    END.
  END.

  overdue = b3.
  IF overdue-periods < 3 THEN overdue = overdue + b2.
  IF overdue-periods < 2 THEN overdue = overdue + b1.

  bt = b0 + b1 + b2 + b3.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-non-property-debtors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE non-property-debtors Procedure 
PROCEDURE non-property-debtors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DO:
  this-break = "Non-property debtors".
  FOR EACH Company NO-LOCK:
    need-entity-header = Yes.
    this-entity = STRING(Company.CompanyCode, "999") + " " + Company.LegalName.
    FOR EACH Tenant NO-LOCK WHERE Tenant.EntityType = "L"
                          AND Tenant.EntityCode = Company.CompanyCode
                          BY Company.CompanyCode BY Tenant.Name:
      RUN each-tenant.
    END.
    IF NOT need-entity-header THEN RUN entity-footing( this-entity ).
  END.

  need-entity-header = Yes.
  this-entity = "Tenants that are completely screwed up".

  FOR EACH Tenant NO-LOCK:
    /* Skip to next record if we have already processed it */
    IF Tenant.EntityType = "P" AND CAN-FIND( Property WHERE Property.PropertyCode = Tenant.EntityCode) THEN NEXT.
    IF Tenant.EntityType = "L" AND CAN-FIND( Company WHERE Company.CompanyCode = Tenant.EntityCode) THEN NEXT.

    RUN each-tenant.
  END.
  IF NOT need-entity-header THEN RUN entity-footing( this-entity ).

  IF NOT need-break-header THEN RUN break-footing( this-break ).

END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i           AS INT NO-UNDO.
DEF VAR token       AS CHAR NO-UNDO.

  {inc/showopts.i "report-options"}

  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).
    CASE( ENTRY( 1, token ) ):
      WHEN "AsAtDate" THEN          asat-date = DATE( ENTRY(2,token) ).
      WHEN "SummarisePart" THEN     summarise-part = Yes.
      WHEN "HideInactives" THEN     show-inactives = No.
      WHEN "Preview" THEN           preview = Yes.
      WHEN "ExcludeRent" THEN       exclude-rent = Yes.
      WHEN "SortBy" THEN            sort-by = ENTRY(2, token).
      WHEN "ConsolidationList" THEN list-name = ENTRY(2, token).
      WHEN "MinAbsBalance" THEN     min-abs-balance = DEC( ENTRY(2, token) ).
      WHEN "MinDebitBalance" THEN   min-debit-balance = DEC( ENTRY(2, token) ).
      WHEN "OverdueBalance" THEN    overdue-balance = DEC( ENTRY(2, token) ).
      WHEN "OverduePeriods" THEN    overdue-periods = INT( ENTRY(2, token) ).

      WHEN "AllRecords" THEN    report-all-records = Yes.

      WHEN "RecordRange" THEN ASSIGN
        record-1 = ENTRY(2, token) 
        record-n = ENTRY(3, token) .

      WHEN "Export" THEN  ASSIGN
        exporting = Yes
        file-name = SUBSTRING( token, INDEX( token, ",") + 1 ).

    END CASE.
  END.

  IF report-all-records THEN ASSIGN
    record-1 = (IF sort-by = "Region" THEN "0000" ELSE "0")
    record-n  = (IF sort-by = "Region" THEN "ZZZZ" ELSE "99999").

  FIND LAST Month WHERE Month.StartDate <= asat-date NO-LOCK.
  asat-month = Month.MonthCode.
  FIND PREV Month NO-LOCK.
  asat-month1 = Month.MonthCode.
  FIND PREV Month NO-LOCK.
  asat-month2 = Month.MonthCode.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-report-by-company) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-by-company Procedure 
PROCEDURE report-by-company :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.

  IF LENGTH(list-name) > 0 THEN
    FIND ConsolidationList WHERE ConsolidationList.Name = list-name NO-LOCK NO-ERROR.

  IF AVAILABLE(ConsolidationList) THEN DO:
    this-break = "Consolidation List " + ConsolidationList.Name
               + " - " + ConsolidationList.Description.
    DO i = 1 TO NUM-ENTRIES(ConsolidationList.CompanyList):
      FIND Company WHERE Company.CompanyCode = INT( ENTRY(i, ConsolidationList.CompanyList)) NO-LOCK.
      RUN each-company.
    END.
  END.
  ELSE DO:
    this-break = "Company " + record-1 + " to " + record-n.
    FOR EACH Company NO-LOCK WHERE Company.CompanyCode >= INT(record-1) AND Company.CompanyCode <= INT(record-n):
      RUN each-company.
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-report-by-manager) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-by-manager Procedure 
PROCEDURE report-by-manager :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN build-manager-table.

  FOR EACH Manager WHERE (Manager.Manager >= INT(record-1) AND Manager.Manager <= INT(record-n))
                        BY Manager.Manager:
    need-break-header = Yes.
    this-break = "Managed by " + Manager.FullName.

    FOR EACH Property WHERE Property.Manager = Manager.Manager NO-LOCK BY Property.PropertyCode:
      need-entity-header = Yes.
      this-entity = STRING(Property.PropertyCode, "99999") + " " + Property.Name.
      FOR EACH Tenant NO-LOCK WHERE (Tenant.Active OR show-inactives)
                              AND Tenant.EntityType = "P"
                              AND Tenant.EntityCode = Property.PropertyCode 
                              BY Tenant.Name:
        RUN each-tenant.
      END.
      IF need-entity-header = No THEN RUN entity-footing( this-entity ).
    END.
    IF need-break-header = No THEN RUN break-footing( this-break ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-report-by-property) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-by-property Procedure 
PROCEDURE report-by-property :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  this-break = "Property " + record-1 + " to " + record-n.
  need-break-header = Yes.

  FOR EACH Property NO-LOCK WHERE Property.PropertyCode  >= INT(record-1) AND Property.PropertyCode <= INT(record-n)
               BY Property.Manager BY Property.PropertyCode:
    need-entity-header = Yes.
    this-entity = STRING(Property.PropertyCode, "99999") + " " + Property.Name.
    FOR EACH Tenant NO-LOCK WHERE (Tenant.Active OR show-inactives)
                    AND Tenant.EntityType = "P"
                    AND Tenant.EntityCode = Property.PropertyCode 
                    BY Tenant.Name:
      RUN each-tenant.
    END.
    IF need-entity-header = No THEN RUN entity-footing( this-entity ).
  END.
  IF need-break-header = No THEN RUN break-footing( this-break ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-report-by-region) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-by-region Procedure 
PROCEDURE report-by-region :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FOR EACH Region WHERE Region.Region >= record-1 AND Region.Region <= record-n NO-LOCK:
    need-break-header = Yes.
    this-break = Region.Name .
    FOR EACH Property NO-LOCK WHERE Property.Region = Region.Region BY Property.Name:
      need-entity-header = Yes.
      this-entity = STRING(Property.PropertyCode, "99999") + " " + Property.Name.
      FOR EACH Tenant NO-LOCK WHERE (Tenant.Active OR show-inactives)
                          AND Tenant.EntityType = "P"
                          AND Tenant.EntityCode = Property.PropertyCode 
                          BY Tenant.Name:
        RUN each-tenant.
      END.
      IF NOT need-entity-header THEN RUN entity-footing( this-entity ).
    END.
    IF NOT need-break-header THEN RUN break-footing( this-break ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-report-by-tenant) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-by-tenant Procedure 
PROCEDURE report-by-tenant :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  this-break = "Tenant " + record-1 + " to " + record-n.
  need-break-header = Yes.

  need-entity-header = No.
  this-entity = "".

  FOR EACH Tenant NO-LOCK WHERE (Tenant.Active OR show-inactives)
                  BY Tenant.TenantCode:
    RUN each-tenant.
  END.
  IF NOT need-break-header THEN RUN break-footing( this-break ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-tenant-heading) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE tenant-heading Procedure 
PROCEDURE tenant-heading :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  IF NOT header-not-displayed THEN PUT SKIP(1).
  IF need-entity-header THEN RUN entity-heading( this-entity ).
  need-tenant-header = No.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

