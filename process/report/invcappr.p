&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

&SCOPED-DEFINE trn-per-page 49

DEF INPUT PARAMETER from-invoice LIKE Invoice.InvoiceNo NO-UNDO.
DEF INPUT PARAMETER to-invoice   LIKE Invoice.InvoiceNo NO-UNDO.
DEF INPUT PARAMETER invoice-list AS CHAR NO-UNDO.

/* Report counters */
DEF VAR ln AS DEC INIT 0.00 NO-UNDO.

/* Line definitions */
&GLOB ADDRESS-LINES 10
DEF VAR date-in-words  AS CHAR NO-UNDO.
DEF VAR tenant-address AS CHAR NO-UNDO EXTENT {&ADDRESS-LINES}.
DEF VAR trn-line       AS CHAR NO-UNDO.
DEF VAR trn-no         AS INT  NO-UNDO.
DEF VAR regarding      AS CHAR NO-UNDO.

DEF VAR reset-page            AS CHAR NO-UNDO.
DEF VAR half-line             AS CHAR NO-UNDO. half-line = CHR(27) + "=".
DEF VAR time-font             AS CHAR NO-UNDO.
DEF VAR title-font            AS CHAR NO-UNDO.
DEF VAR num-font              AS CHAR NO-UNDO.
DEF VAR prompt-font           AS CHAR NO-UNDO.
DEF VAR tenant-address-font   AS CHAR NO-UNDO.
DEF VAR re-font               AS CHAR NO-UNDO.
DEF VAR to-font               AS CHAR NO-UNDO.
DEF VAR line-printer          AS CHAR NO-UNDO.
DEF VAR credit-font           AS CHAR NO-UNDO.

DEF VAR page-no               AS INT NO-UNDO.
DEF VAR gst-applies           AS LOGI NO-UNDO.
DEF VAR display-percent       AS LOGI NO-UNDO.

DEF VAR time-stamp AS CHAR NO-UNDO.
DEF VAR user-name  AS CHAR NO-UNDO.
{inc/username.i "user-name"}

 {inc/ofc-this.i}

 /* Check Office settings for the use of Due-Date on Invoice 'late charge message' */ 
 {inc/ofc-set.i "LateChargeBasis"  "late-charge-basis"}
     
time-stamp = STRING( TODAY, "99/99/9999" ) + STRING( " - ", "X(3)" ) +  STRING( TIME, "HH:MM:SS" ) + ", " + user-name.

FIND FIRST Tenant NO-LOCK.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .08
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}
{inc/convert.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

FIND Office WHERE Office.ThisOffice NO-LOCK.
gst-applies = Office.GST <> ?.

DEF VAR preview AS LOGI INIT No NO-UNDO.

RUN get-control-strings.
OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

RUN invoice-approval-forms.

OUTPUT CLOSE.
RUN view-output-file ( preview ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE carriage-return Procedure 
PROCEDURE carriage-return :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  PUT CONTROL CHR(13).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-invoice Procedure 
PROCEDURE each-invoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  page-no = 1.
  RUN get-tenant-details.

  display-percent = No.
  FOR EACH invoiceLine OF Invoice NO-LOCK:
    display-percent = display-percent OR InvoiceLine.Percent <> 100.00.
  END.

  RUN page-header.
  
  FOR EACH InvoiceLine OF Invoice NO-LOCK:
    RUN each-line.  
  END.

  RUN total-line.    
  RUN page-footer.
  RUN page-feed.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-line Procedure 
PROCEDURE each-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN next-line.

  FIND ChartOfAccount OF InvoiceLine NO-LOCK.
  
  PUT UNFORMATTED
    InvoiceLine.EntityType + STRING(InvoiceLine.EntityCode,"99999")  SPACE(1)
    STRING( InvoiceLine.AccountCode + IF ChartOfAccount.AccountGroupCode = "PROPEX" AND gst-applies
      THEN 0.20 ELSE 0.00, "9999.99" )             SPACE(2)
    STRING( SUBSTR( ChartOfAccount.Name, 1, 13 ) , "X(13)" )
      SPACE(2)
    STRING( InvoiceLine.AccountText, "X(51)" )
    STRING( InvoiceLine.Amount, ">,>>>,>>9.99CR" ) SPACE(2)
    STRING( IF display-percent THEN STRING( InvoiceLine.Percent, ">>9.99" )
                               ELSE "", "X(6)" )        SPACE(2)
    STRING( InvoiceLine.YourShare, ">,>>>,>>9.99CR" ).
  RUN skip-line(1).
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-control-strings Procedure 
PROCEDURE get-control-strings :
/*------------------------------------------------------------------------------
  Purpose:     Get all control strings for this report
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR rows AS DEC NO-UNDO.
  DEF VAR cols AS DEC NO-UNDO.
  
  RUN make-control-string( "PCL", "reset,simplex,portrait,a4,tm,0,lm,6",
    OUTPUT reset-page, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,20,Bold,Proportional",
                  OUTPUT title-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Times,Point,6,Normal,Proportional",
                  OUTPUT time-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,10,Proportional,Normal",
                  OUTPUT prompt-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Courier,Fixed,cpi,12,Bold",
                  OUTPUT num-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,10,Proportional,Normal",
                  OUTPUT tenant-address-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,14,Proportional,Bold",
                  OUTPUT re-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,8,Proportional,Normal",
                  OUTPUT to-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "LinePrinter,lpi,9.54",
                  OUTPUT line-printer, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,14,Proportional,Bold",
                  OUTPUT credit-font, OUTPUT rows, OUTPUT cols ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-tenant-details Procedure 
PROCEDURE get-tenant-details :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR i AS INT NO-UNDO.
  DEF VAR addr AS CHAR NO-UNDO.
    
  FIND Tenant WHERE Tenant.TenantCode = Invoice.EntityCode NO-LOCK.
  
  regarding = "".
  IF Tenant.EntityType = 'P' THEN
  DO:
    FIND FIRST Property WHERE Property.PropertyCode = Tenant.EntityCode
      NO-LOCK NO-ERROR.
    IF AVAILABLE Property THEN regarding = Property.Name.
  END.
  ELSE IF Tenant.EntityType = 'L' THEN
  DO:
    FIND FIRST Company WHERE Company.CompanyCode = Tenant.EntityCode
      NO-LOCK NO-ERROR.
    IF AVAILABLE Company THEN regarding = Company.LegalName.
  END.
 
  DO i = 1 TO {&ADDRESS-LINES}: tenant-address[i] = "". END.
  IF AVAILABLE Invoice AND Invoice.AttnTo <> "" AND Invoice.AttnTo <> ? THEN
    RUN process/getaddr.p ( "T," + Invoice.AttnTo, Tenant.TenantCode, "BILL", OUTPUT addr ).
  ELSE
    RUN process/getaddr.p ( "T", Tenant.TenantCode, "BILL", OUTPUT addr ).

  DO i = 1 TO NUM-ENTRIES( addr, "~n" ):
    IF i > {&ADDRESS-LINES} THEN LEAVE.
    tenant-address[i] = ENTRY( i, addr, "~n" ).
  END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE invoice-approval-forms Procedure 
PROCEDURE invoice-approval-forms :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR i AS INT NO-UNDO.
  
  RUN get-control-strings.
  RUN date-to-word( TODAY, OUTPUT date-in-words ).

  IF invoice-list <> ? THEN
  DO i = 1 TO NUM-ENTRIES( invoice-list ):  
    FIND Invoice WHERE
      Invoice.InvoiceStatus = "U" AND
      Invoice.InvoiceNo = INT( ENTRY( i, invoice-list ) )
    NO-LOCK NO-ERROR.
    IF AVAILABLE Invoice THEN RUN each-invoice.
  END.
  
  IF from-invoice <> ? AND to-invoice <> ? THEN
  FOR EACH Invoice NO-LOCK WHERE
    Invoice.InvoiceStatus = "U" AND  
    Invoice.InvoiceNo >= from-invoice AND
    Invoice.InvoiceNo <= to-invoice:
    RUN each-invoice.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE next-line Procedure 
PROCEDURE next-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  trn-no = trn-no + 1.

  IF trn-no >= {&trn-per-page} THEN
  DO:
    RUN page-footer.
    RUN page-feed.
    RUN page-header.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE page-feed Procedure 
PROCEDURE page-feed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  PUT CONTROL CHR(12).
  page-no = page-no + 1.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE page-footer Procedure 
PROCEDURE page-footer :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR fmt-1 AS CHAR INIT "X(50)" NO-UNDO.
DEF VAR fmt-2 AS CHAR INIT "X(13)" NO-UNDO.
DEF VAR amount-due AS CHAR NO-UNDO.
  
  PUT CONTROL line-printer.
  RUN pcl-moveto( 87, 11 ).

  IF Invoice.Total < 0 THEN
  DO:
    PUT CONTROL credit-font.
    PUT UNFORMATTED "This is a credit note.".
  END.
  ELSE
  DO:
    DEF VAR n-days AS INT NO-UNDO.
    PUT CONTROL prompt-font.
    ASSIGN n-days = INT( Invoice.TermsCode ) NO-ERROR.
    IF n-days > 0 THEN DO:
        PUT UNFORMATTED "Interest may be charged on payments received after " +
          IF late-charge-basis = 'DueDate' AND Invoice.DueDate <> ? 
          THEN STRING(Invoice.DueDate, "99/99/9999") + "." 
          ELSE STRING( Invoice.InvoiceDate
                       + (IF Invoice.TermsCode = "M" THEN 29 ELSE n-days), "99/99/9999" )
                       + ".".
    END.
END.
      
  PUT CONTROL line-printer.
  RUN skip-line( 1.5 ).

  PUT UNFORMATTED FILL( "_ ", 60 ).
  RUN skip-line(5).
  PUT CONTROL prompt-font.
  PUT UNFORMATTED "Approved:". RUN carriage-return.
  RUN pcl-move-relative( 0, 25 ).
  PUT UNFORMATTED
    FILL( "_", 36 ) SPACE( 15 )
    STRING( "Date:  " +  FILL( "_", 12 ), "X(50)" ).
  RUN skip-line(4).
  PUT UNFORMATTED "Invoice Sent:". RUN carriage-return.
  RUN pcl-move-relative( 0, 25 ).
  PUT UNFORMATTED
    FILL( "_", 36 ) SPACE( 15 )
    STRING( "Date:  " +  FILL( "_", 12 ), "X(50)" ).
  RUN skip-line(3).
  PUT UNFORMATTED "Notes:".
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE page-header Procedure 
PROCEDURE page-header :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  RUN reset-page.
  RUN print-title.
  RUN print-address.
  
  RUN skip-line(1).
  PUT UNFORMATTED FILL( '_', 102).
  RUN skip-line(3).
  PUT CONTROL re-font.
  PUT UNFORMATTED "RE:".
  RUN carriage-return.
  PUT CONTROL line-printer.
  PUT UNFORMATTED SPACE(9).
  PUT CONTROL re-font.
  PUT UNFORMATTED STRING( regarding, "X(80)" ).
  RUN skip-line(3).
  PUT CONTROL to-font.
  PUT UNFORMATTED "GL Text:".
  RUN carriage-return.
  PUT CONTROL line-printer.
  PUT UNFORMATTED SPACE(9).
  PUT CONTROL to-font.
  PUT UNFORMATTED STRING( Invoice.ToDetail, "X(100)" ).
  RUN skip-line(2).

  RUN print-invoice-header.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-address Procedure 
PROCEDURE print-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.
DEF VAR invoice-date AS CHAR NO-UNDO.
  
  RUN skip-line(2).
  PUT CONTROL tenant-address-font.
  
  PUT UNFORMATTED STRING( tenant-address[1], "X(50)" ). RUN carriage-return.
  PUT UNFORMATTED SPACE( 100 ) "Invoice No:". RUN carriage-return.
  PUT CONTROL num-font.
  PUT UNFORMATTED SPACE( 78 ) STRING( Invoice.InvoiceNo, ">>>>9" ).
  PUT CONTROL tenant-address-font.
  RUN skip-line(2).
  
  PUT UNFORMATTED STRING( tenant-address[2], "X(50)" ). RUN carriage-return.
  PUT UNFORMATTED SPACE( 100 ) IF Tenant.EntityType = "P" THEN "Property No:" ELSE "Company No:".
  RUN carriage-return.
  PUT CONTROL num-font.
  PUT UNFORMATTED SPACE( 78 ) STRING( Tenant.EntityCode, ">9999" ).
  PUT CONTROL tenant-address-font.
  RUN skip-line(2).

  PUT UNFORMATTED STRING( tenant-address[3], "X(50)" ). RUN carriage-return.
  PUT UNFORMATTED SPACE( 100 ) "Tenant No:".  RUN carriage-return.
  PUT CONTROL num-font.
  PUT UNFORMATTED SPACE( 78 ) STRING( Tenant.TenantCode, "99999" ).
  PUT CONTROL tenant-address-font.
  RUN skip-line(2).

  PUT UNFORMATTED STRING( tenant-address[4], "X(50)" ). RUN skip-line(2).

  PUT UNFORMATTED STRING( tenant-address[5], "X(50)" ). RUN carriage-return.
  
  RUN date-to-word( Invoice.InvoiceDate, OUTPUT invoice-date ).
  PUT UNFORMATTED SPACE(118) STRING( invoice-date, "X(20)" ).
  RUN skip-line(2).

  PUT UNFORMATTED STRING( tenant-address[6], "X(50)" ). RUN skip-line(2).
  PUT UNFORMATTED STRING( tenant-address[7], "X(50)" ). RUN skip-line(2).
  RUN skip-line(0.5).
  PUT UNFORMATTED SPACE(138) STRING( "Page " + STRING( page-no ) ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-invoice-header Procedure 
PROCEDURE print-invoice-header :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR blrb AS CHAR NO-UNDO.

  PUT CONTROL line-printer.

  blrb = (IF Invoice.Blurb = ? OR TRIM(Invoice.Blurb) = ""
                             THEN Invoice.ToDetail ELSE Invoice.Blurb).

  IF page-no = 1 THEN DO.  
    DEF VAR i AS INT NO-UNDO.
    DO i = 1 TO NUM-ENTRIES( blrb, "~n" ):
      RUN next-line.
      PUT UNFORMATTED (IF i > 1 THEN FILL(" ",9) ELSE "TO:      ")
        ENTRY( i, blrb, "~n" ).
      RUN skip-line(1).
    END.
  END.

  trn-no = trn-no + 3.
  RUN next-line.  
  RUN skip-line(2).
  PUT UNFORMATTED SPACE(16)
    STRING( "Acct. Desc", "X(13)" )     SPACE(2)
    STRING( "Description", "X(51)" )
    STRING( "      Amount  ", "X(14)" ) SPACE(1)
    STRING( IF display-percent THEN "Percent" ELSE "", "X(7)" )         SPACE(2)
    STRING( "       Share  ", "X(14)" ).
  RUN skip-line(2).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-title Procedure 
PROCEDURE print-title :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  PUT CONTROL line-printer.
  RUN skip-line(2).
  PUT CONTROL time-font.
  PUT UNFORMATTED time-stamp.
  PUT CONTROL line-printer.
  RUN skip-line(4).
  RUN pcl-move-relative( 0, 29 ).
  PUT CONTROL title-font.
  PUT UNFORMATTED STRING( "INVOICE APPROVAL FORM", "X(25)" ).
  PUT CONTROL line-printer.
  RUN skip-line(4).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-page Procedure 
PROCEDURE reset-page :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  PUT CONTROL reset-page.
  trn-no = 0.
  ln = 0.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE skip-line Procedure 
PROCEDURE skip-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER n AS DEC NO-UNDO.

  DEF VAR int-part AS INT NO-UNDO.
  DEF VAR dec-part AS DEC NO-UNDO.
  
  int-part = TRUNCATE( n, 0 ).
  IF int-part < 0 THEN RETURN.
  dec-part = n - int-part.
  IF int-part = 0 AND dec-part = 0 THEN RETURN.

  /* Need to have this like the following - do not touch */  
  IF int-part <> 0 THEN PUT CONTROL FILL( CHR(10), int-part ).
  IF dec-part <> 0 THEN PUT CONTROL half-line.
    
  ln = ln + n.

  RUN carriage-return.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE skip-to-line Procedure 
PROCEDURE skip-to-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER line-no AS DEC NO-UNDO.

  DEF VAR int-part AS INT NO-UNDO.
  DEF VAR dec-part AS DEC NO-UNDO.
  
  int-part = TRUNCATE( line-no - ln, 0 ).
  IF int-part < 0 THEN RETURN.
  dec-part = ( line-no - ln ) - int-part.
  IF int-part = 0 AND dec-part = 0 THEN RETURN.
  
  IF int-part <> 0 THEN PUT CONTROL FILL( CHR(10), int-part ).
  IF dec-part <> 0 THEN PUT CONTROL half-line.
    
  ln = line-no.

  RUN carriage-return.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE total-line Procedure 
PROCEDURE total-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  trn-no = trn-no + IF gst-applies THEN 4 ELSE 1.
  RUN next-line.
  
  RUN skip-line(1).
  IF gst-applies THEN DO:
    PUT UNFORMATTED SPACE(106) FILL( '-', 12 ). RUN skip-line(1).
    PUT UNFORMATTED SPACE(74)
      STRING( "Your total share", "X(30)" ) SPACE(2)
      STRING( Invoice.Total, ">,>>>,>>9.99CR" ). RUN skip-line(1).
    PUT UNFORMATTED SPACE(74)
      STRING( "Plus GST", "X(30)" ) SPACE(2)
      STRING( Invoice.TaxAmount, ">,>>>,>>9.99CR" ). RUN skip-line(1).
  END.
  
  PUT UNFORMATTED SPACE(106) FILL( '-', 12 ).  RUN skip-line(1).
  PUT UNFORMATTED SPACE(74)
    STRING( "Amount now due", "X(30)" ) SPACE(2)
    STRING( Invoice.Total + Invoice.TaxAmount, ">,>>>,>>9.99CR" ). RUN skip-line(1).
  PUT UNFORMATTED SPACE(106) FILL( '=', 12 ).  RUN skip-line(1).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


