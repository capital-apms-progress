&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  Report:   To Do List printout (and export)
  Author:   Andrew McMillan
  Date:     21/3/1999
  ------------------------------------------------------------------------*/

DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

DEF VAR preview   AS LOGI NO-UNDO INIT No.
DEF VAR show-rebates AS LOGI NO-UNDO INIT No.
DEF VAR period-1 AS DATE NO-UNDO INIT ?.
DEF VAR period-n AS DATE NO-UNDO INIT ?.
DEF VAR selection-style AS CHAR NO-UNDO.
DEF VAR half-line AS CHAR NO-UNDO.
DEF VAR ln AS DEC INIT 0.00 NO-UNDO.
DEF VAR output-to-pdf AS LOG INIT NO.
DEF VAR sort-order AS CHAR INIT "D".
DEF VAR line-c AS INT NO-UNDO.
DEF VAR recs-per-page AS INT NO-UNDO.

DEF VAR reset-page AS CHAR NO-UNDO.

RUN parse-parameters.

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
DEF VAR timeStamp AS CHAR FORMAT "X(44)" NO-UNDO.
timeStamp = STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.

DEF VAR pr-line AS CHAR INIT "" NO-UNDO.        /* used everywhere to hold print line */

DEF VAR title-font  AS CHAR NO-UNDO INITIAL "Proportional,Helvetica,Bold,Point,12".
DEF VAR time-font   AS CHAR NO-UNDO INITIAL "proportional,helv,point,6,normal".
DEF VAR break1-font AS CHAR NO-UNDO INITIAL "proportional,helv,point,12,lpi,7,bold".
DEF VAR break2-font AS CHAR NO-UNDO INITIAL "proportional,helv,point,8,bold".
DEF VAR base-font   AS CHAR NO-UNDO INITIAL "fixed,courier,cpi,18,lpi,9,light".
DEF VAR header-font AS CHAR NO-UNDO INITIAL "fixed,courier,cpi,18,lpi,9,bold".

{inc/ofc-this.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 28.25
         WIDTH              = 36.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}
{inc/null.i}
{inc/entity.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

IF output-to-pdf THEN DO:
  RUN txtrep-pdf-filename( "Tenant-Calls-Report" ).
  RUN txtrep-output-mode("pdf").
END.

OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

DEFINE QUERY q FOR TenantCall, Tenant, Creditor, ServiceType.

RUN pclrep-start( preview, "reset,landscape,tm,2,a4,lm,6,courier,cpi,18,lpi,9" ).
RUN report-headers.

IF period-1 = ? THEN
  period-1 = DATE( 1, 1, 1960 ).
IF period-n = ? THEN
  period-n = TODAY.

IF selection-style = "Closed" THEN
  RUN setup-closed-calls.
ELSE IF selection-style = "Open" THEN
  RUN setup-open-calls.
ELSE
  RUN setup-all-calls.

recs-per-page = 59.
GET FIRST q.
DO WHILE AVAILABLE(TenantCall):
  RUN each-call.

  line-c = line-c + 1.
  IF line-c = recs-per-page THEN DO:
    PUT UNFORMATTED CHR(12).
    line-c = 0.
    recs-per-page = 67.
  END.

  GET NEXT q.
END.

RUN report-footers.

OUTPUT CLOSE.

IF NOT output-to-pdf THEN
  RUN pclrep-finish.

/* RUN view-output-file( preview ). */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-each-call) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-call Procedure 
PROCEDURE each-call :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR problem-text AS CHAR NO-UNDO.

  PUT UNFORMATTED STRING( TenantCall.DateOfCall,"99/99/9999" ) ", " STRING( TenantCall.TimeOfCall, 'HH:MM' ) SPACE(3).
  PUT UNFORMATTED STRING( TenantCall.CallStatusCode, "X(7)" ) SPACE(3).
  IF AVAILABLE(Tenant) THEN
    PUT UNFORMATTED STRING( Tenant.Name, "X(20)" ) SPACE(3).
  ELSE
    PUT UNFORMATTED SPACE(23).

  PUT UNFORMATTED STRING( TenantCall.PropertyCode, "99999" ) SPACE(5).
  PUT UNFORMATTED STRING( TenantCall.OrderNo, "99999" ) SPACE(5).

  IF AVAILABLE(ServiceType) THEN
    PUT UNFORMATTED STRING( ServiceType.AccountCode, "9999.99" ) SPACE(3).
  ELSE
    PUT UNFORMATTED SPACE(10).

  problem-text = TenantCall.Problem.
  problem-text = REPLACE( problem-text, "~n", " ").
  PUT UNFORMATTED STRING( problem-text, "X(60)" ) SPACE(3).

  IF AVAILABLE(Creditor) THEN
    PUT UNFORMATTED STRING( Creditor.Name, "X(30)" ) SPACE(3).
  ELSE
    PUT UNFORMATTED SPACE(33).

  PUT UNFORMATTED "~n".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR token AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.

{inc/showopts.i "report-options"}
period-1 = TODAY - 180.
period-n = TODAY + 90.

  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).
    
    CASE ENTRY( 1, token ):
      WHEN "Preview" THEN               preview = Yes.
      WHEN "SortOrder" THEN             sort-order = ENTRY(2,token).
      WHEN "Selection" THEN             selection-style = ENTRY(2,token).
      WHEN "Period" THEN ASSIGN
        period-1 = DATE( ENTRY(2,token) )
        period-n = DATE( ENTRY(3,token) ).
      WHEN "OutputPDF" THEN output-to-pdf = YES.
    END CASE.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-report-footers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-footers Procedure 
PROCEDURE report-footers :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

RUN skip-line(1).
PUT UNFORMATTED "Report prepared at " timeStamp.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-report-headers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-headers Procedure 
PROCEDURE report-headers :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN pclrep-line( title-font, "" ).
  PUT UNFORMATTED "          Tenant Calls Listing - ".
  IF selection-style = 'Closed' THEN
    PUT UNFORMATTED "Closed calls".
  ELSE IF selection-style = 'Open' THEN
    PUT UNFORMATTED "Open calls".
  ELSE
    PUT UNFORMATTED "All calls".

  IF period-1 <> ? AND period-n <> ? THEN
    PUT UNFORMATTED " (from " STRING( period-1, "99/99/9999" ) " to " STRING( period-n, "99/99/9999" ) ")".

  RUN pclrep-line( header-font, "" ).
  RUN skip-line(2).

  PUT UNFORMATTED
    "Date/Time"  SPACE(11)
    "Status"     SPACE(4)
    "Tenant"     SPACE(17)
    "Building"   SPACE(2)
    "Order No"   SPACE(2)
    "Account"    SPACE(3)
    "Problem"    SPACE(56)
    "Contractor" SPACE(23).

  RUN pclrep-line( base-font, "" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-skip-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE skip-line Procedure
PROCEDURE skip-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER n AS DEC NO-UNDO.

  DEF VAR int-part AS INT NO-UNDO.
  DEF VAR dec-part AS DEC NO-UNDO.

  int-part = TRUNCATE( n, 0 ).
  IF int-part < 0 THEN RETURN.
  dec-part = n - int-part.
  IF int-part = 0 AND dec-part = 0 THEN RETURN.

  /* Need to have this like the following - do not touch */
  IF int-part <> 0 THEN PUT CONTROL FILL( CHR(10), int-part ).
  IF dec-part <> 0 THEN PUT CONTROL half-line.

  ln = ln + n.

  RUN carriage-return.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-carriage-return) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE carriage-return Procedure
PROCEDURE carriage-return :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:     
------------------------------------------------------------------------------*/

PUT CONTROL CHR(13).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-control-strings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-control-strings Procedure
PROCEDURE get-control-strings :
/*------------------------------------------------------------------------------
  Purpose:     Get all control strings for this report
------------------------------------------------------------------------------*/

  DEF VAR rows AS DEC NO-UNDO.
  DEF VAR cols AS DEC NO-UNDO.

  RUN make-control-string( "PCL", "reset,simplex,landscape,a4,tm,0,lm,6",
    OUTPUT reset-page, OUTPUT rows, OUTPUT cols ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-setup-closed-calls) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setup-closed-calls Procedure
PROCEDURE setup-closed-calls :
/*------------------------------------------------------------------------------
  Purpose:     Closed calls
------------------------------------------------------------------------------*/

  CASE sort-order:
    WHEN "D" THEN
      OPEN QUERY q
        FOR EACH TenantCall WHERE TenantCall.CallStatusCode = 'Closed' AND
          TenantCall.DateOfCall >= period-1 AND TenantCall.DateOfCall <= period-n,
          FIRST Tenant WHERE TenantCall.TenantCode = Tenant.TenantCode OUTER-JOIN,
          FIRST Creditor WHERE Creditor.CreditorCode = TenantCall.CreditorCode OUTER-JOIN,
          FIRST ServiceType WHERE ServiceType.ServiceType = TenantCall.CallCategoryCode OUTER-JOIN
          BY TenantCall.DateOfCall.
    WHEN "B" THEN
      OPEN QUERY q
        FOR EACH TenantCall WHERE TenantCall.CallStatusCode = 'Closed' AND
          TenantCall.DateOfCall >= period-1 AND TenantCall.DateOfCall <= period-n,
          FIRST Tenant WHERE TenantCall.TenantCode = Tenant.TenantCode OUTER-JOIN,
          FIRST Creditor WHERE Creditor.CreditorCode = TenantCall.CreditorCode OUTER-JOIN,
          FIRST ServiceType WHERE ServiceType.ServiceType = TenantCall.CallCategoryCode OUTER-JOIN
          BY TenantCall.PropertyCode.
    WHEN "A" THEN
      OPEN QUERY q
        FOR EACH TenantCall WHERE TenantCall.CallStatusCode = 'Closed' AND
          TenantCall.DateOfCall >= period-1 AND TenantCall.DateOfCall <= period-n,
          FIRST Tenant WHERE TenantCall.TenantCode = Tenant.TenantCode OUTER-JOIN,
          FIRST Creditor WHERE Creditor.CreditorCode = TenantCall.CreditorCode OUTER-JOIN,
          FIRST ServiceType WHERE ServiceType.ServiceType = TenantCall.CallCategoryCode OUTER-JOIN
          BY ServiceType.AccountCode.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-setup-open-calls) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setup-open-calls Procedure
PROCEDURE setup-open-calls :
/*------------------------------------------------------------------------------
  Purpose:     Open calls
------------------------------------------------------------------------------*/

  CASE sort-order:
    WHEN "D" THEN
      OPEN QUERY q
        FOR EACH TenantCall WHERE TenantCall.CallStatusCode <> 'Closed' AND
          TenantCall.DateOfCall >= period-1 AND TenantCall.DateOfCall <= period-n,
          FIRST Tenant WHERE TenantCall.TenantCode = Tenant.TenantCode OUTER-JOIN,
          FIRST Creditor WHERE Creditor.CreditorCode = TenantCall.CreditorCode OUTER-JOIN,
          FIRST ServiceType WHERE ServiceType.ServiceType = TenantCall.CallCategoryCode OUTER-JOIN
          BY TenantCall.DateOfCall.
    WHEN "B" THEN
      OPEN QUERY q
        FOR EACH TenantCall WHERE TenantCall.CallStatusCode <> 'Closed' AND
          TenantCall.DateOfCall >= period-1 AND TenantCall.DateOfCall <= period-n,
          FIRST Tenant WHERE TenantCall.TenantCode = Tenant.TenantCode OUTER-JOIN,
          FIRST Creditor WHERE Creditor.CreditorCode = TenantCall.CreditorCode OUTER-JOIN,
          FIRST ServiceType WHERE ServiceType.ServiceType = TenantCall.CallCategoryCode OUTER-JOIN
          BY TenantCall.PropertyCode.
    WHEN "A" THEN
      OPEN QUERY q
        FOR EACH TenantCall WHERE TenantCall.CallStatusCode <> 'Closed' AND
          TenantCall.DateOfCall >= period-1 AND TenantCall.DateOfCall <= period-n,
          FIRST Tenant WHERE TenantCall.TenantCode = Tenant.TenantCode OUTER-JOIN,
          FIRST Creditor WHERE Creditor.CreditorCode = TenantCall.CreditorCode OUTER-JOIN,
          FIRST ServiceType WHERE ServiceType.ServiceType = TenantCall.CallCategoryCode OUTER-JOIN
          BY ServiceType.AccountCode.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-setup-all-calls) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setup-all-calls Procedure
PROCEDURE setup-all-calls :
/*------------------------------------------------------------------------------
  Purpose:     Get all control strings for this report
------------------------------------------------------------------------------*/

  CASE sort-order:
    WHEN "D" THEN
      OPEN QUERY q
        FOR EACH TenantCall WHERE TenantCall.DateOfCall >= period-1 AND TenantCall.DateOfCall <= period-n,
          FIRST Tenant WHERE TenantCall.TenantCode = Tenant.TenantCode OUTER-JOIN,
          FIRST Creditor WHERE Creditor.CreditorCode = TenantCall.CreditorCode OUTER-JOIN,
          FIRST ServiceType WHERE ServiceType.ServiceType = TenantCall.CallCategoryCode OUTER-JOIN
          BY TenantCall.DateOfCall.
    WHEN "B" THEN
      OPEN QUERY q
        FOR EACH TenantCall WHERE TenantCall.DateOfCall >= period-1 AND TenantCall.DateOfCall <= period-n,
          FIRST Tenant WHERE TenantCall.TenantCode = Tenant.TenantCode OUTER-JOIN,
          FIRST Creditor WHERE Creditor.CreditorCode = TenantCall.CreditorCode OUTER-JOIN,
          FIRST ServiceType WHERE ServiceType.ServiceType = TenantCall.CallCategoryCode OUTER-JOIN
          BY TenantCall.PropertyCode.
    WHEN "A" THEN
      OPEN QUERY q
        FOR EACH TenantCall WHERE TenantCall.DateOfCall >= period-1 AND TenantCall.DateOfCall <= period-n,
          FIRST Tenant WHERE TenantCall.TenantCode = Tenant.TenantCode OUTER-JOIN,
          FIRST Creditor WHERE Creditor.CreditorCode = TenantCall.CreditorCode OUTER-JOIN,
          FIRST ServiceType WHERE ServiceType.ServiceType = TenantCall.CallCategoryCode OUTER-JOIN
          BY ServiceType.AccountCode.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
