&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

DEF VAR project-from LIKE Project.ProjectCode NO-UNDO.
DEF VAR project-to   LIKE Project.ProjectCode NO-UNDO.
DEF VAR upto-month      AS INT  NO-UNDO.
DEF VAR upto-date       AS DATE NO-UNDO.
DEF VAR from-month      AS INT  NO-UNDO     INITIAL 0.
DEF VAR from-date       AS DATE NO-UNDO.
DEF VAR preview         AS LOGICAL NO-UNDO          INITIAL No.
DEF VAR summarise       AS LOGI NO-UNDO     INITIAL No.
RUN parse-parameters.

DEF VAR pending-headers AS CHAR NO-UNDO INITIAL "".
DEF VAR ptot AS DEC NO-UNDO INITIAL 0.
DEF VAR gtot AS DEC NO-UNDO INITIAL 0.

/* Report counters */
DEF VAR ln AS DEC INIT 0.00 NO-UNDO.
DEF VAR lines-per-page AS INT NO-UNDO.

/* Line definitions */

DEF VAR page-no               AS INT INIT 1 NO-UNDO.
DEF VAR reset-page            AS CHAR NO-UNDO.
DEF VAR half-line             AS CHAR NO-UNDO.
DEF VAR title-font            AS CHAR NO-UNDO.
DEF VAR time-font             AS CHAR NO-UNDO.
DEF VAR project-font          AS CHAR EXTENT 3 NO-UNDO.
DEF VAR line-font             AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR money-fmt AS CHAR NO-UNDO   INITIAL   ">>>,>>>,>>9.99CR".

DEF VAR money-width AS INT NO-UNDO.
money-width = LENGTH(STRING(0,money-fmt)).

DEF VAR now AS CHAR NO-UNDO.
now = STRING( TODAY, "99/99/9999" ) + " " + STRING( TIME, "HH:MM:SS" ).
DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}

RUN get-control-strings.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD sum-project-orders Procedure 
FUNCTION sum-project-orders RETURNS DECIMAL
  ( INPUT project-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .1
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

RUN order-tracking.

OUTPUT CLOSE.

RUN view-output-file ( preview ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE column-header Procedure 
PROCEDURE column-header :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR line-1 AS CHAR NO-UNDO.
DEF VAR line-2 AS CHAR NO-UNDO.

  line-1 = "   Order" + FILL(" ", 84) + "1st".
  line-2 = "    Date    Reference        Amount     Description" + FILL(" ", 41) + "Appvr        Supplier".

  RUN print-line( line-1 ).
  RUN print-line( line-2 ).
  RUN skip-line( 1.5 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE detail-sub-projects Procedure 
PROCEDURE detail-sub-projects :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER project-code LIKE Project.ProjectCode NO-UNDO.

DEF BUFFER SubProj FOR Project.

  FOR EACH SubProj WHERE SubProj.EntityType = "J"
                     AND SubProj.EntityCode = project-code NO-LOCK:
    FOR EACH Order WHERE Order.ProjectCode = SubProj.ProjectCode NO-LOCK:
      RUN each-order.  
    END.
    RUN detail-sub-projects( SubProj.ProjectCode ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-order Procedure 
PROCEDURE each-order :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR var-text AS CHAR NO-UNDO.
DEF VAR cred-name AS CHAR NO-UNDO.

DEF VAR detail-line AS CHAR NO-UNDO.

  IF Order.OrderDate < from-date OR Order.OrderDate > upto-date THEN RETURN.

  var-text = WRAP( Order.Description, 50).
  FIND Creditor OF Order NO-LOCK NO-ERROR.
  IF AVAILABLE(Creditor) THEN
    cred-name = Creditor.Name.
  ELSE
    cred-name = "Creditor not on file!".

  detail-line = (IF Order.OrderDate = ? THEN "??/??/????" ELSE STRING( Order.OrderDate, "99/99/9999" )) + "  "
              + STRING( STRING( Order.ProjectCode ) + "/" + STRING( Order.OrderCode ), "X(10)")
              + STRING( Order.OrderAmount , money-fmt ) + "  "
              + STRING( ENTRY( 1, var-text, "~n"), "X(52)" )
              + STRING( Order.FirstApprover, "X(6)" )
              + STRING( Order.CreditorCode, "99999" ) + "  "
              + cred-name
               .

  RUN project-header.
  RUN print-line( detail-line ).

  ptot = ptot + Order.OrderAmount .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-project Procedure 
PROCEDURE each-project :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER project-code LIKE Project.ProjectCode NO-UNDO.

DEF VAR project-name AS CHAR NO-UNDO.
DEF VAR save-gtot AS DEC NO-UNDO.
DEF BUFFER SubProj FOR Project.

  FIND Project WHERE Project.ProjectCode = project-code NO-LOCK.
  project-name = Project.Name.
  RUN pending-project-header( project-code ).

  ptot = 0.
  FOR EACH Order WHERE Order.ProjectCode = project-code NO-LOCK:
    RUN each-order.  
  END.
  IF summarise THEN
    RUN summarise-sub-projects( project-code ).
  ELSE
    RUN detail-sub-projects( project-code ).

  IF pending-headers = "" THEN DO:
    RUN print-totals( "Total", ptot).
    RUN skip-line(1).
    gtot = gtot + ptot.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-control-strings Procedure 
PROCEDURE get-control-strings :
/*------------------------------------------------------------------------------
  Purpose:     Get all control strings for this report
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR rows AS DEC NO-UNDO.
DEF VAR cols AS DEC NO-UNDO.
  
  RUN make-control-string ( "PCL", "reset,portrait,a4,tm,0,lm,0,Fixed,Courier,cpi,18,lpi,9.54",
                  OUTPUT reset-page, OUTPUT lines-per-page, OUTPUT cols ).

  IF preview THEN RETURN.
  half-line = CHR(27) + "=".

  RUN make-control-string ( "PCL", "Fixed,Courier,cpi,18,lpi,9.54",
                  OUTPUT line-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string ( "PCL", "Proportional,Helvetica,Bold,Point,12",
                  OUTPUT title-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string ( "PCL", "Proportional,Helvetica,Bold,Point,6",
                  OUTPUT time-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string ( "PCL", "Proportional,Helvetica,Bold,Point,10",
                  OUTPUT project-font[1], OUTPUT rows, OUTPUT cols ).

  RUN make-control-string ( "PCL", "Proportional,Helvetica,Bold,Point,10",
                  OUTPUT project-font[2], OUTPUT rows, OUTPUT cols ).

  RUN make-control-string ( "PCL", "Proportional,Helvetica,Bold,Point,10",
                  OUTPUT project-font[3], OUTPUT rows, OUTPUT cols ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-entity-name Procedure 
PROCEDURE get-entity-name :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT  PARAMETER entity-type AS CHAR NO-UNDO.
DEF INPUT  PARAMETER entity-code AS INT  NO-UNDO.
DEF OUTPUT PARAMETER entity-name AS CHAR NO-UNDO.

  DEF BUFFER OtherProject FOR Project.
    
  CASE entity-type:
    WHEN "P" THEN DO:
      FIND FIRST Property WHERE Property.PropertyCode = entity-code
        NO-LOCK NO-ERROR.
      IF AVAILABLE Property THEN entity-name = Property.Name.
    END.
    
    WHEN "L" THEN DO:
      FIND FIRST Company WHERE Company.CompanyCode = entity-code
        NO-LOCK NO-ERROR.
      IF AVAILABLE Company THEN entity-name = Company.LegalName.
    END.
    
    WHEN "T" THEN DO:
      FIND FIRST Tenant WHERE Tenant.TenantCode = entity-code
        NO-LOCK NO-ERROR.
      IF AVAILABLE Tenant THEN entity-name = Tenant.Name.
    END.
    
    WHEN "C" THEN DO:
      FIND FIRST Creditor WHERE Creditor.CreditorCode = entity-code
        NO-LOCK NO-ERROR.
      IF AVAILABLE Creditor THEN entity-name = Creditor.Name.
    END.
    
    WHEN "J" THEN DO:
      FIND FIRST OtherProject WHERE OtherProject.ProjectCode = entity-code
        NO-LOCK NO-ERROR.
      IF AVAILABLE OtherProject THEN entity-name = OtherProject.Name.
    END.

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE order-tracking Procedure 
PROCEDURE order-tracking :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN page-header.

  FOR EACH Project NO-LOCK WHERE Project.ProjectCode >= project-from
                             AND Project.ProjectCode <= project-to:
    RUN each-project( Project.ProjectCode ).
  END.

  IF project-from < project-to THEN DO:
    RUN skip-line(2).
    RUN print-totals( "Grand total", gtot ).
  END.

  RUN page-feed.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE page-feed Procedure 
PROCEDURE page-feed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  page-no = page-no + 1.
  PUT CONTROL CHR(12).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE page-header Procedure 
PROCEDURE page-header :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  RUN reset-page.
  RUN print-title.
  RUN column-header.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i           AS INT NO-UNDO.
DEF VAR token       AS CHAR NO-UNDO.

  FIND FIRST Month NO-LOCK.                 from-month = Month.MonthCode.
  FIND LAST Month NO-LOCK.                  upto-month = Month.MonthCode.

  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).
    CASE( ENTRY( 1, token ) ):
      WHEN "Range" THEN ASSIGN
        project-from = INT( ENTRY(2,token) )
        project-to   = INT( ENTRY(3,token) ).

      WHEN "Summarise" THEN         summarise = Yes.
      WHEN "upto" THEN              upto-month = INT( ENTRY(2,token)).
      WHEN "From" THEN              from-month = INT( ENTRY(2,token)).
      WHEN "Preview" THEN           preview = Yes.
    END CASE.
  END.
  IF project-to < project-from THEN project-to = project-from.

  FIND Month WHERE Month.MonthCode = from-month NO-LOCK.
  from-date = Month.StartDate.

  FIND Month WHERE Month.MonthCode = upto-month NO-LOCK.
  upto-date = Month.EndDate.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pending-project-header Procedure 
PROCEDURE pending-project-header :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER project-code LIKE Project.ProjectCode NO-UNDO.

  FIND Project WHERE Project.ProjectCode = project-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE Project THEN RETURN.
  
  DEF VAR entity-name AS CHAR NO-UNDO.
  RUN get-entity-name( Project.EntityType, Project.EntityCode, OUTPUT entity-name ).

  pending-headers = project-font[1]
                  + "(" + STRING( Project.ProjectCode, "99999" ) + ") - "
                  + Project.Name + ", " + entity-name
                  + line-font + "~n".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-line Procedure 
PROCEDURE print-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER out-line AS CHAR NO-UNDO.

  PUT UNFORMATTED out-line.
  RUN skip-line(1).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-title Procedure 
PROCEDURE print-title :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN skip-line(2).
  PUT CONTROL time-font.
  PUT UNFORMATTED
    STRING( "Printed: " + now + " for " + user-name, "X(100)" ) SPACE(120)
    STRING( "Page: " + STRING( page-no ), "X(20)" ).
  RUN skip-line(2).
  PUT CONTROL title-font.
  PUT UNFORMATTED "Project Orders/Commitments Tracking Report".

  FIND FIRST Month NO-LOCK.
  IF from-month > Month.MonthCode THEN
    PUT UNFORMATTED ", from " STRING( from-date, "99/99/9999").
  FIND LAST Month NO-LOCK.
  IF upto-month < Month.MonthCode THEN
    PUT UNFORMATTED ", up to " STRING( upto-date, "99/99/9999").

  RUN skip-line(2).
  PUT CONTROL line-font.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-totals Procedure 
PROCEDURE print-totals :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER title-str AS CHAR NO-UNDO.
DEF INPUT PARAMETER this-total AS DEC NO-UNDO.

  RUN print-line( FILL(" ",22) + FILL("-", money-width) ).
  RUN print-line( STRING( title-str, "X(22)") + STRING(this-total, money-fmt) ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE project-header Procedure 
PROCEDURE project-header :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF pending-headers = "" THEN RETURN.

DEF VAR i AS INT NO-UNDO.
DEF VAR hdr-line AS CHAR NO-UNDO.

  DO WHILE ENTRY( 1, pending-headers, "~n") <> "":
    hdr-line = ENTRY( 1, pending-headers, "~n").
    pending-headers = SUBSTRING( pending-headers, INDEX( pending-headers, "~n") + 1).
    IF hdr-line = ? THEN NEXT.
    PUT CONTROL hdr-line.
    RUN skip-line(1.5).
  END.

  pending-headers = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-page Procedure 
PROCEDURE reset-page :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  PUT CONTROL reset-page.
  ln = 0.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE skip-line Procedure 
PROCEDURE skip-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER n AS DEC NO-UNDO.

  IF ln + n >= lines-per-page THEN
  DO:
    RUN page-feed.
    RUN page-header.
    RETURN.
  END.

  DEF VAR int-part AS INT NO-UNDO.
  DEF VAR dec-part AS DEC NO-UNDO.
  
  int-part = TRUNCATE( n, 0 ).
  IF int-part < 0 THEN RETURN.
  dec-part = n - int-part.
  IF int-part = 0 AND dec-part = 0 THEN RETURN.

  IF dec-part <> 0 THEN PUT CONTROL half-line.

  IF int-part = 1 THEN PUT UNFORMATTED " " SKIP.
  ELSE IF int-part > 1 THEN PUT SKIP(int-part).

  ln = ln + n.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE skip-to-line Procedure 
PROCEDURE skip-to-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER line-no AS DEC NO-UNDO.

  DEF VAR int-part AS INT NO-UNDO.
  DEF VAR dec-part AS DEC NO-UNDO.
  
  int-part = TRUNCATE( line-no - ln, 0 ).
  IF int-part < 0 THEN RETURN.
  dec-part = ( line-no - ln ) - int-part.
  IF int-part = 0 AND dec-part = 0 THEN RETURN.
  
  IF dec-part <> 0 THEN PUT CONTROL half-line.

  IF int-part = 1 THEN PUT " " SKIP.
  ELSE IF int-part > 1 THEN PUT SKIP( int-part ).
    
  ln = line-no.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE summarise-sub-projects Procedure 
PROCEDURE summarise-sub-projects :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER project-code LIKE Project.ProjectCode NO-UNDO.

DEF VAR orders AS DEC NO-UNDO.
DEF VAR var-text AS CHAR NO-UNDO.

DEF VAR detail-line AS CHAR NO-UNDO.
DEF BUFFER SubProj FOR Project.

  FOR EACH SubProj WHERE SubProj.EntityType = "J"
                     AND SubProj.EntityCode = project-code NO-LOCK:
    var-text = "Orders for " + SubProj.Name.
    orders = sum-project-orders( SubProj.ProjectCode ).

    detail-line = FILL( " ", 12)
                + STRING( STRING(SubProj.ProjectCode) + "/sub" , "X(10)")
                + STRING( orders, money-fmt ) + "  "
                + STRING( ENTRY( 1, var-text, "~n"), "X(62)" ) .

    IF orders <> 0 THEN DO:
      RUN project-header.
      RUN print-line( detail-line ).
    END.
    ptot = ptot + orders.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION sum-project-orders Procedure 
FUNCTION sum-project-orders RETURNS DECIMAL
  ( INPUT project-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Sum orders (or adjustments) up to the upto-date
------------------------------------------------------------------------------*/
DEF VAR order-total AS DEC NO-UNDO INITIAL 0.

DEF BUFFER SubProj FOR Project.

  FOR EACH Order NO-LOCK WHERE Order.ProjectCode = project-code
                            AND Order.OrderDate >= from-date
                            AND Order.OrderDate <= upto-date:
    order-total = order-total + Order.OrderAmount .
  END.

  FOR EACH SubProj WHERE SubProj.EntityType = "J"
                     AND SubProj.EntityCode = project-code NO-LOCK:
    order-total = order-total + sum-project-orders( SubProj.ProjectCode ).
  END.

  RETURN order-total.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


