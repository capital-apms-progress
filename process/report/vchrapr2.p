&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ------------------------------------------------------------------------*/

DEF INPUT PARAMETER voucher-start LIKE Voucher.VoucherSeq NO-UNDO.
DEF INPUT PARAMETER voucher-end   LIKE Voucher.VoucherSeq NO-UNDO.
DEF INPUT PARAMETER voucher-list AS CHAR NO-UNDO.

DEF VAR fixed-codes       AS CHAR NO-UNDO.
DEF VAR variable-codes    AS CHAR NO-UNDO.
DEF VAR voucher-codes     AS CHAR NO-UNDO.
DEF VAR transaction-codes AS CHAR NO-UNDO.
DEF VAR eject-string      AS CHAR NO-UNDO.

DEF VAR preview        AS LOGICAL INITIAL No NO-UNDO.
DEF VAR prt-ctrl       AS CHAR NO-UNDO.
DEF VAR rows           AS INT  NO-UNDO.
DEF VAR cols           AS INT  NO-UNDO.

DEF VAR gst-applies    AS LOGI NO-UNDO.

DEF VAR continued-on-attached AS LOGICAL NO-UNDO    INITIAL No.

&GLOB MAX-P1-ALLOC 11

{inc/ofc-this.i}
{inc/ofc-set.i "Voucher-Sign" "approver-list" "ERROR"}
{inc/ofc-set.i "Voucher-Approved-Prompt" "approved-prompt"}
IF NOT AVAILABLE(OfficeSetting) THEN
  approved-prompt = "Approved for Payment:".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .1
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}
{inc/method/m-hpgl.i}
{inc/entity.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

FIND Office WHERE Office.ThisOffice NO-LOCK.
gst-applies = Office.GST <> ?.

RUN make-control-string ( "PCL", "eject", OUTPUT eject-string, OUTPUT rows, OUTPUT cols ).
RUN make-control-string ( "PCL", "reset,simplex,portrait,tm,0,a4,lm,0,courier,cpi,18,lpi,9",
                  OUTPUT prt-ctrl, OUTPUT rows, OUTPUT cols ).
RUN output-control-file ( prt-ctrl ).
OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

RUN voucher-approval-forms.

OUTPUT CLOSE.
RUN view-output-file ( No ).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE approval-prompts Procedure 
PROCEDURE approval-prompts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Approval Prompts */

  RUN hpgl-moveto( 23, 42 ).
  RUN hpgl-text( "Helvetica,Point,12,Proportional", "Extensions Checked:" ).
  RUN hpgl-move-relative( 44, 0 ).
  RUN hpgl-line-relative( 43, 0 ).
  RUN hpgl-move-relative( 4, 0 ).
  RUN hpgl-text( hpgl-last-font, "Coding Verified:" ).
  RUN hpgl-move-relative( 34, 0 ).
  RUN hpgl-line-relative( 52, 0 ).

  RUN hpgl-move-relative( -177, -8 ).
  RUN hpgl-text( hpgl-last-font, "Certified Correct:" ).
  RUN hpgl-move-relative( 50, 0 ).
  RUN hpgl-line-relative( 77, 0 ).
  RUN hpgl-move-relative( 4, 0 ).
  RUN hpgl-text( hpgl-last-font, "Date:" ).
  RUN hpgl-move-relative( 14, 0 ).
  RUN hpgl-line-relative( 32, 0 ).

  RUN hpgl-move-relative( -177, -8 ).
  RUN hpgl-text( "Helvetica,Point,12,Proportional", approved-prompt ).
  RUN hpgl-move-relative( 50, 0 ).
  RUN hpgl-line-relative( 77, 0 ).
  RUN hpgl-move-relative( 4, 0 ).
  RUN hpgl-text( hpgl-last-font, "Date:" ).
  RUN hpgl-move-relative( 14, 0 ).
  RUN hpgl-line-relative( 32, 0 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE approval-variables Procedure 
PROCEDURE approval-variables :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  
  DEF VAR n          AS DEC NO-UNDO.
  DEF VAR box-width  AS DEC NO-UNDO.
  DEF VAR box-height AS DEC NO-UNDO.
  DEF VAR gap-width  AS DEC NO-UNDO.
  
  n = NUM-ENTRIES( approver-list ).
  box-width = ( 3 / ( 4 * n - 1 ) ) * 180.
  gap-width = box-width / 3.
  box-height = 9.
  
  DEF VAR i AS INT NO-UNDO.
  RUN hpgl-moveto( 21, 13 ).
  RUN hpgl-append( "LT" ). /* Solid Line */
  DO i = 1 TO n:
    RUN hpgl-box-relative( box-width, box-height ).
    RUN hpgl-move-relative( 1, 1 ).
    RUN hpgl-text( "Helvetica,Point,10,Proportional,Normal", ENTRY( i, approver-list ) ).
    RUN hpgl-move-relative( box-width + gap-width - 1 , -1 ).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE budget-variables Procedure 
PROCEDURE budget-variables :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  /* Budget Prompts */
  RUN hpgl-moveto( 23, 176 ).
  RUN hpgl-text( "Helvetica,Point,10,Proportional", "@YTD@ Actual:" ).
  RUN hpgl-move-relative( 60, 0 ).
  RUN hpgl-text( hpgl-last-font, "@YTD@ Budget:" ).
  RUN hpgl-move-relative( 58, 0 ).
  RUN hpgl-text( hpgl-last-font, "@FullYear@ Budget:" ).

  /* Budget Variables */
  RUN hpgl-moveto( 23, 176 ).
  RUN hpgl-move-relative( 21, 0 ).
  RUN hpgl-text( "Times,Point,12,Proportional,Bold", "@YTD Actual@" ).
  RUN hpgl-move-relative( 62, 0 ).
  RUN hpgl-text( hpgl-last-font, "@YTD Budget@" ).
  RUN hpgl-move-relative( 64, 0 ).
  RUN hpgl-text( hpgl-last-font, "@FY Budget@" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE continuation-footer Procedure 
PROCEDURE continuation-footer :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR continuation-codes AS CHAR NO-UNDO.

  RUN hpgl-get-codes( no, yes, OUTPUT continuation-codes ).
  PUT CONTROL continuation-codes + eject-string.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE continuation-header Procedure 
PROCEDURE continuation-header :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN hpgl-initialize.
  RUN office-logo.
  RUN hpgl-set-line-width( 0.25 ). 
  RUN hpgl-moveto( 70, 256 ).
  RUN hpgl-text( 'helvetica,point,20,proportional,bold', "Voucher Continuation" ).
  RUN hpgl-moveto( 151, 281 ).
  RUN hpgl-text( "Helvetica,Point,13,Proportional,Normal", "Voucher No:" ).
  RUN hpgl-moveto( 181, 281 ).
  RUN hpgl-text( "Times,Point,16,Proportional,Bold", STRING( Voucher.VoucherSeq, ">>>>>>>9" ) ).

  RUN hpgl-moveto( 22, 240 ).
  RUN hpgl-text( "Helvetica,Point,9,Proportional", "T" ).
  RUN hpgl-move-relative( 6, 0 ).
  RUN hpgl-text( hpgl-last-font, "Code" ).
  RUN hpgl-move-relative( 12, 0 ).
  RUN hpgl-text( hpgl-last-font, "Account" ).
  RUN hpgl-move-relative( 19, 0 ).
  RUN hpgl-text( hpgl-last-font, "Narrative" ).
  RUN hpgl-move-relative( 116.5, 0 ).
  RUN hpgl-text( hpgl-last-font, "$" ).
  RUN hpgl-move-relative( 17.5, 0 ).
  RUN hpgl-text( hpgl-last-font, "c" ).

  RUN hpgl-moveto( 21.5, 230 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-voucher Procedure 
PROCEDURE each-voucher :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR printer-codes AS CHAR NO-UNDO.

  PUT CONTROL fixed-codes.
  RUN variable-replacements.
  PUT CONTROL hpgl-codes.
  hpgl-codes = "".

  RUN client-logo(
    Voucher.EntityType,
    Voucher.EntityCode,
    get-entity-client( Voucher.EntityType, Voucher.EntityCode )
  ).
  RUN hpgl-get-codes( no, yes, OUTPUT printer-codes ).
  PUT CONTROL printer-codes.

  RUN print-transactions.
  PUT CONTROL transaction-codes.
  
  PUT CONTROL eject-string.

  IF continued-on-attached THEN DO:
    RUN print-continuation.
    continued-on-attached = No.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-voucherline Procedure 
PROCEDURE each-voucherline :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR entity-name  AS CHAR NO-UNDO.
  DEF VAR account-desc AS CHAR NO-UNDO.
  
  RUN hpgl-text( "Courier,Point,9,Proportional,Normal",
    IF VoucherLine.EntityType = ? THEN "" ELSE VoucherLine.EntityType ).
  RUN hpgl-move-relative( 5.5, 1.5 ).

  RUN hpgl-text( hpgl-last-font, 
    IF VoucherLine.EntityCode = ? OR VoucherLine.EntityCode = 0 THEN "" ELSE
    STRING( VoucherLine.EntityCode, "99999" ) ).
  RUN hpgl-move-relative( -1, -2 ).
  
  RUN voucher-entity-name( OUTPUT entity-name ).
  RUN hpgl-text( "Point,4", entity-name ).
  RUN hpgl-move-relative( 14, IF LOOKUP( VoucherLine.EntityType, "C,T" ) <> 0 
    THEN 0.5 ELSE 2 ).
  
  
  RUN hpgl-text( "Point,9", 
    IF LOOKUP( VoucherLine.EntityType, "C,T" ) <> 0 THEN "XXXXXX" ELSE
    IF VoucherLine.AccountCode = ? OR VoucherLine.AccountCode = 0 THEN "" ELSE
      STRING( VoucherLine.AccountCode, "9999.99" ) ).
  RUN hpgl-move-relative( -2,   IF LOOKUP( VoucherLine.EntityType, "C,T" ) <> 0 
    THEN -0.5 ELSE -2 ).

  RUN voucher-account-desc( OUTPUT account-desc ).
  RUN hpgl-text( "Point,4", account-desc ).
  RUN hpgl-move-relative( 21, 0.5 ).

  RUN hpgl-text( "Point,9", VoucherLine.Description ).
  RUN hpgl-move-relative( 104, 0 ).

  RUN hpgl-text( "Courier,Fixed,cpi,12",
    STRING( TRUNCATE( VoucherLine.Amount, 0 ), "->>,>>>,>>9" ) ).
  RUN hpgl-move-relative( 27, 0 ).
  
  RUN hpgl-text( hpgl-last-font, 
    STRING( ABSOLUTE( ( VoucherLine.Amount - TRUNCATE( VoucherLine.Amount, 0 )) * 100 ), "99" ) ).

  RUN hpgl-move-relative( -168.5, -6 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-fixed-codes Procedure 
PROCEDURE get-fixed-codes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  /* The constant parts of the page */
  RUN hpgl-initialize.
  RUN hpgl-set-line-width( 0.25 ). 
  RUN hpgl-moveto( 55, 256 ).
  RUN hpgl-text( 'helvetica,point,20,proportional,bold', "EXPENSE APPROVAL VOUCHER" ).
  RUN print-boxes.
  RUN print-prompts.
  RUN hpgl-get-codes( no, yes, OUTPUT fixed-codes ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-variables Procedure 
PROCEDURE get-variables :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN invoice-detail-variables.
  RUN budget-variables.
  RUN recoverable-variables.
  RUN approval-variables.

  RUN hpgl-get-codes( no, yes, OUTPUT variable-codes ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE invoice-detail-prompts Procedure 
PROCEDURE invoice-detail-prompts :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN hpgl-moveto( 153, 281 ).
  RUN hpgl-text( "Helvetica,Point,13,Proportional,Normal", "Voucher No:" ).

  /* Invoice Detail Prompts */
  RUN hpgl-moveto( 23, 247 ).
  RUN hpgl-text( "Helvetica,Point,10,Proportional", "Creditor Number:" ).
  RUN hpgl-move-relative( 51, 0 ).
  RUN hpgl-text( hpgl-last-font, "Creditor Name:" ).
  RUN hpgl-move-relative( -51, -6.5 ).
  RUN hpgl-text( "Helvetica,Point,12,Proportional", "Invoice Details" ).
  RUN hpgl-move-relative( 0, -7.2 ).
  RUN hpgl-text( "Helvetica,Point,10,Proportional", "Invoice Number:" ).
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-text( hpgl-last-font, "Order Number:" ).
  RUN hpgl-move-relative( 0, -16 ).
  RUN hpgl-text( hpgl-last-font, "Description:" ).
  
  RUN hpgl-move-relative( 68.5, 16 ).
  RUN hpgl-text( hpgl-last-font, "Date:" ).
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-text( hpgl-last-font, "Approval:" ).
  
  RUN hpgl-move-relative( 53.5, 12 ).
  RUN hpgl-text( "Helvetica,Point,13,Proportional", "Value" ).
  RUN hpgl-move-relative( 0, -6 ).
  IF gst-applies THEN RUN hpgl-text( hpgl-last-font, "GST" ).
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-text( hpgl-last-font, "Total" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE invoice-detail-variables Procedure 
PROCEDURE invoice-detail-variables :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN hpgl-moveto( 24, 289 ).
  RUN hpgl-text( "Times,Point,6,Normal", "@LastModified@" ).

  RUN hpgl-moveto( 181, 281 ).
  RUN hpgl-text( "Times,Point,16,Proportional,Bold", "@VoucherNo@" ).

  /* Invoice Detail Variables */
  RUN hpgl-moveto( 53, 247 ).
  RUN hpgl-text( "Times,Point,12,Proportional", "@Creditor@" ).
  RUN hpgl-move-relative( 47, 0 ).
  RUN hpgl-text( hpgl-last-font, "@CreditorName@" ).
  RUN hpgl-move-relative( -43, -13 ).
  RUN hpgl-text( hpgl-last-font, "@InvoiceNo@" ).
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-text( hpgl-last-font, "@OrderNo@" ).
  RUN hpgl-move-relative( 0, -17 ).
  RUN hpgl-text( hpgl-last-font, "@VoucherDescription@" ).
  
  RUN hpgl-move-relative( 53, 16 ).
  RUN hpgl-text( hpgl-last-font, "@Date@" ).
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-text( hpgl-last-font, "@Approval@" ).
  
  RUN hpgl-move-relative( 54, 13 ).
  RUN hpgl-text( "Courier,Fixed,cpi,12", "@Value@" ).
  RUN hpgl-move-relative( 0, -6 ).
  IF gst-applies THEN RUN hpgl-text( hpgl-last-font, "@GST@" ).
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-text( hpgl-last-font, "@Total@" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE notes-prompts Procedure 
PROCEDURE notes-prompts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Notes */
  DEF VAR i AS INT INIT 0 NO-UNDO.  
  RUN hpgl-moveto( 23, 201 ).
  RUN hpgl-text( "Helvetica,Point,12,Proportional", "Notes:" ).
  RUN hpgl-move-relative( 14, 0 ).
  RUN hpgl-set-line-type( -2, 1, 1 ).
  RUN hpgl-line-relative( 163, 0 ).
  DO i = 1 TO 3:
    RUN hpgl-move-relative( -177, -6 ).
    RUN hpgl-line-relative( 177, 0 ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-boxes Procedure 
PROCEDURE print-boxes :
/*------------------------------------------------------------------------------
  Purpose:  Print the voucher approval boxes.
------------------------------------------------------------------------------*/

RUN hpgl-moveto( 20, 208).

/* box 1 */
RUN hpgl-line-relative( 0, 34 ).
RUN hpgl-line-relative( 2, 0 ).
RUN hpgl-move-relative( 32, 0 ).
RUN hpgl-line-relative( 146, 0 ).
RUN hpgl-line-relative( 0, -34 ).
RUN hpgl-line-relative( -180, 0 ).

RUN hpgl-move-relative( 37, 19 ).
RUN hpgl-line-relative( 27, 0).
RUN hpgl-move-relative( -27, 6 ).
RUN hpgl-line-relative( 27, 0).
RUN hpgl-move-relative( 74.5, -6.5 ).
RUN hpgl-line-relative( 33, 0).
RUN hpgl-move-relative( 0, -8).
RUN hpgl-set-line-width( 0.5 ).
RUN hpgl-line-relative( -33, 0).
RUN hpgl-set-line-width( 0.25 ).

/* box 2 */
RUN hpgl-move-relative( -138.5, -44.5).
RUN hpgl-box-relative( 180, 7).

/* box 3 */
RUN hpgl-move-relative( 0, -34).
RUN hpgl-move-relative( 0, 6).
RUN hpgl-box-relative( 93, 25).
RUN hpgl-line-relative( 93, 0).
RUN hpgl-move-relative( -53, 0).
RUN hpgl-line-relative( 0, 25).
RUN hpgl-move-relative( 16.5, 0).
RUN hpgl-box-relative( 21.5, -25).

/* box 4 */
RUN hpgl-move-relative( -56, -45 ).
RUN hpgl-box-relative( 180, -76 ).
RUN hpgl-move-relative( 5, 0).
RUN hpgl-box-relative( 12, -76 ).
RUN hpgl-move-relative( 30, 0).
RUN hpgl-box-relative( 108, -76 ).
RUN hpgl-move-relative( 132, 0 ).
RUN hpgl-line-relative( 0, -76 ).
RUN hpgl-move-relative( 13, 72 ).
RUN hpgl-line-relative( -180, 0 ).

DEF VAR i AS INT NO-UNDO.
RUN hpgl-set-line-type( 2, 1, 1).
DO i = 1 TO 12:
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-line-relative( 180, 0 ).
  RUN hpgl-move-relative( -180, 0 ).
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-continuation Procedure 
PROCEDURE print-continuation :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO INITIAL 0.
DEF VAR ln AS INT NO-UNDO INITIAL 0.
DEF VAR header-codes AS CHAR NO-UNDO.

&SCOP MAX-P2-ALLOC 35

  RUN continuation-header.
  RUN hpgl-get-codes( no, yes, OUTPUT header-codes ).
  PUT CONTROL header-codes.
  RUN hpgl-clear.

  FOR EACH VoucherLine OF Voucher NO-LOCK:
    i = i + 1.
    IF i <= {&MAX-P1-ALLOC} THEN NEXT.
    IF ln > {&MAX-P2-ALLOC} THEN DO:
      RUN hpgl-move-relative( 48, 0 ).
      RUN hpgl-text( "Helvetica,Point,12,Proportional,Bold",
                     "Continued On Attached Sheet" ).
      RUN continuation-footer.
      PUT CONTROL header-codes.
      ln = 0.
    END.
    RUN each-voucherline.
    ln = ln + 1.
  END.

  RUN continuation-footer.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-prompts Procedure 
PROCEDURE print-prompts :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN invoice-detail-prompts.
  RUN notes-prompts.
  RUN recoverable-prompts.
  RUN verification-prompts.
  RUN transaction-prompts.
  RUN approval-prompts.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-transactions Procedure 
PROCEDURE print-transactions :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i AS INT NO-UNDO.

  hpgl-codes = "".
  RUN hpgl-moveto( 21.5, 117.5 ).
  FOR EACH VoucherLine NO-LOCK OF Voucher BY VoucherLine.LineSeq:
    i = i + 1.
    IF i > {&MAX-P1-ALLOC} THEN DO:
      continued-on-attached = Yes.
      RUN hpgl-move-relative( 48, 0 ).
      RUN hpgl-text( "Helvetica,Point,12,Proportional,Bold",
                     "Continued On Attached Sheet" ).
      LEAVE.
    END.
    RUN each-voucherline.
  END.

  RUN hpgl-get-codes( no, yes, OUTPUT transaction-codes ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE recoverable-prompts Procedure 
PROCEDURE recoverable-prompts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Recoverable Prompts */
  RUN hpgl-moveto( 23, 167 ).
  RUN hpgl-text( "Helvetica,Point,10,Proportional", "Recoverable:" ).
  RUN hpgl-move-relative( 135, 0 ).
  RUN hpgl-text( "Helvetica,Point,12,Proportional", "Due:" ).
  RUN hpgl-move-relative( 0, -8 ).
  RUN hpgl-text( "Helvetica,Point,12,Proportional", "Pay by:" ).
  RUN hpgl-move-relative( -135, 2 ).
  RUN hpgl-text( "Helvetica,Point,10,Proportional", "Source:" ).
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-text( hpgl-last-font, "By Annual Rec:" ).
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-text( hpgl-last-font, "Outgoings Received:" ).
  RUN hpgl-move-relative( 0, -7 ).
/*  RUN hpgl-text( hpgl-last-font, "Percentage Recoverable:" ). */
  
  RUN hpgl-move-relative( 44, 25 ).
  RUN hpgl-text( hpgl-last-font, "Y" ).
  RUN hpgl-move-relative( -4, -6 ).
  RUN hpgl-text( hpgl-last-font, "Lessee" ).
  RUN hpgl-move-relative( 4, -6 ).
  RUN hpgl-text( hpgl-last-font, "Y" ).
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-text( hpgl-last-font, "Y" ).

  RUN hpgl-move-relative( 19, 18 ).
  RUN hpgl-text( hpgl-last-font, "N" ).
  RUN hpgl-move-relative( -6.5, -6 ).
  RUN hpgl-text( hpgl-last-font, "Insurance" ).
  RUN hpgl-move-relative( 6.5, -6 ).
  RUN hpgl-text( hpgl-last-font, "N" ).
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-text( hpgl-last-font, "N" ).
      
  RUN hpgl-move-relative( 16.5, 18 ).
  RUN hpgl-text( hpgl-last-font, "Part" ).
  RUN hpgl-move-relative( -1, -6 ).
  RUN hpgl-text( hpgl-last-font, "Other" ).
  RUN hpgl-move-relative( 1, -6 ).
  RUN hpgl-text( hpgl-last-font, "N/A" ).
  RUN hpgl-move-relative( 0, -6 ).
  RUN hpgl-text( hpgl-last-font, "N/A" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE recoverable-variables Procedure 
PROCEDURE recoverable-variables :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN hpgl-moveto( 170, 167 ).
  RUN hpgl-text( "Times,Point,16,Proportional,Bold", "@DueDate@" ).
  RUN hpgl-move-relative( 5, -8 ).
  RUN hpgl-text( hpgl-last-font, "@PayBy@" ).
  RUN hpgl-text( "Normal", "" ).

/*
  RUN hpgl-moveto( 65, 142 ).
  RUN hpgl-text( "Times,Point,12,Proportional,Bold", "@Percentage@" ).
*/
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE replace-budgets Procedure 
PROCEDURE replace-budgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR ytd-actual AS DEC NO-UNDO.
  DEF VAR ytd-budget AS DEC NO-UNDO.
  DEF VAR fy-budget  AS DEC NO-UNDO.
  
  DEF VAR first-month LIKE Month.MonthCode NO-UNDO.
  DEF VAR last-month  LIKE Month.MonthCode NO-UNDO.
  DEF VAR this-month  LIKE Month.MonthCode NO-UNDO.

  IF LOOKUP( Voucher.EntityType, "P,L,J" ) = 0 THEN DO:
    RUN hpgl-replace( "@YTD@", "YTD" ).
    RUN hpgl-replace( "@FullYear@", "Full Year" ).
    RUN hpgl-replace( "@YTD Actual@", "" ).
    RUN hpgl-replace( "@YTD Budget@", "" ).
    RUN hpgl-replace( "@FY Budget@",  "" ).
    RETURN.
  END.

  FIND FIRST Month WHERE Month.StartDate <= Voucher.Date AND Month.EndDate >= Voucher.Date
    NO-LOCK NO-ERROR.
  IF Voucher.EntityType = "J" THEN DO:
    RUN hpgl-replace( "@YTD@", "PTD" ).
    RUN hpgl-replace( "@FullYear@", "Project" ).
    last-month = Month.MonthCode.

    FOR EACH AccountBalance NO-LOCK WHERE AccountBalance.EntityType = Voucher.EntityType
                         AND AccountBalance.EntityCode = Voucher.EntityCode
                         AND AccountBalance.AccountCode = Voucher.AccountCode:
      IF AccountBalance.MonthCode <= last-month THEN ASSIGN
            ytd-actual = ytd-actual + AccountBalance.Balance
            ytd-budget = ytd-budget + AccountBalance.Budget.
      fy-budget = fy-budget + AccountBalance.Budget.
    END.
  END.
  ELSE DO:
    RUN hpgl-replace( "@YTD@", "YTD" ).
    RUN hpgl-replace( "@FullYear@", "Full Year" ).
    FIND FinancialYear OF Month NO-LOCK NO-ERROR.
  
    IF AVAILABLE FinancialYear THEN DO:
      this-month = Month.MonthCode.
      FIND FIRST Month OF FinancialYear NO-LOCK. first-month = Month.MonthCode.
      FIND LAST  Month OF FinancialYear NO-LOCK. last-month = Month.MonthCode.

      FOR EACH AccountBalance NO-LOCK WHERE
        AccountBalance.EntityType = Voucher.EntityType AND
        AccountBalance.EntityCode = Voucher.EntityCode AND
        AccountBalance.AccountCode = Voucher.AccountCode AND
        AccountBalance.MonthCode >= first-month AND
        AccountBalance.MonthCode <= last-month:
        IF AccountBalance.MonthCode <= this-month THEN
          ASSIGN
            ytd-actual = ytd-actual + AccountBalance.Balance
            ytd-budget = ytd-budget + AccountBalance.Budget.
        fy-budget = fy-budget + AccountBalance.Budget.
      END.
    END.
  END.
  
  RUN hpgl-replace( "@YTD Actual@", TRIM( STRING( ytd-actual, "$>>>,>>>,>>9.99CR" ) ) ).
  RUN hpgl-replace( "@YTD Budget@", TRIM( STRING( ytd-budget, "$>>>,>>>,>>9.99CR" ) ) ).
  RUN hpgl-replace( "@FY Budget@",  TRIM( STRING( fy-budget,  "$>>>,>>>,>>9.99CR" ) ) ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE replace-percentage-recoverable Procedure 
PROCEDURE replace-percentage-recoverable :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  DEF VAR percent-recoverable AS DEC NO-UNDO.
  
  FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = Voucher.AccountCode
    NO-LOCK NO-ERROR.
  FIND AccountGroup OF ChartOfAccount NO-LOCK NO-ERROR.
  
  IF AVAILABLE AccountGroup AND AccountGroup.AccountGroupCode = "TENEX" THEN
    percent-recoverable = 100.00.
  ELSE IF Voucher.EntityType = "P" THEN DO:
    FIND Property WHERE Property.PropertyCode = Voucher.EntityCode
      NO-LOCK NO-ERROR.
    FOR EACH TenancyLease OF Property NO-LOCK WHERE TenancyLease.LeaseStatus <> "PAST" AND NOT TenancyLease.GrossLease:
      FIND FIRST TenancyOutgoing OF TenancyLease WHERE TenancyOutgoing.AccountCode = Voucher.AccountCode NO-LOCK NO-ERROR.
      percent-recoverable = percent-recoverable + (IF AVAILABLE(TenancyOutgoing) THEN TenancyOutgoing.Percentage ELSE TenancyLease.OutgoingsRate ).
    END.
  END.

  RUN hpgl-replace( "@Percentage@", TRIM( STRING( percent-recoverable, ">>9.99%" ) ) ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE transaction-prompts Procedure 
PROCEDURE transaction-prompts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Transaction Prompts */
  RUN hpgl-moveto( 22, 123.3 ).
  RUN hpgl-text( "Helvetica,Point,9,Proportional", "T" ).
  RUN hpgl-move-relative( 6, 0 ).
  RUN hpgl-text( hpgl-last-font, "Code" ).
  RUN hpgl-move-relative( 12, 0 ).
  RUN hpgl-text( hpgl-last-font, "Account" ).
  RUN hpgl-move-relative( 19, 0 ).
  RUN hpgl-text( hpgl-last-font, "Narrative" ).
  RUN hpgl-move-relative( 116.5, 0 ).
  RUN hpgl-text( hpgl-last-font, "$" ).
  RUN hpgl-move-relative( 17.5, 0 ).
  RUN hpgl-text( hpgl-last-font, "c" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE variable-replacements Procedure 
PROCEDURE variable-replacements :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  hpgl-codes = variable-codes.

  RUN hpgl-replace( "@LastModified@",
    IF Voucher.LastModifiedUser = "" THEN "" ELSE Voucher.LastModifiedUser + ' - ' +
    STRING( Voucher.LastModifiedDate, "99/99/9999" ) + ' ' +
    STRING( Voucher.LastModifiedTime, "HH:MM:SS" ) ).
  RUN hpgl-replace( "@VoucherNo@", STRING( Voucher.VoucherSeq ) ).

  FIND Creditor OF Voucher NO-LOCK NO-ERROR.
  RUN hpgl-replace( "@Creditor@", IF AVAILABLE Creditor THEN 
    STRING( Creditor.CreditorCode, "99999" ) ELSE "" ).
  RUN hpgl-replace( "@CreditorName@", IF AVAILABLE Creditor THEN 
    Creditor.Name ELSE "" ).

  RUN hpgl-replace( "@InvoiceNo@", Voucher.InvoiceReference ).
  RUN hpgl-replace( "@OrderNo@", IF Voucher.OurOrderNo = ? THEN "" ELSE Voucher.OurOrderNo ).
  RUN hpgl-replace( "@CapexRef@", STRING( Voucher.CapexCode, "99999" ) ).
  RUN hpgl-replace( "@VoucherDescription@", Voucher.Description ).
  RUN hpgl-replace( "@Date@", IF Voucher.Date = ? THEN "" ELSE
    STRING( Voucher.Date, "99/99/9999" ) ).
  RUN hpgl-replace( "@Approval@", Voucher.ApproverCode ).
  RUN hpgl-replace( "@Value@", STRING( Voucher.GoodsValue, ">>,>>>,>>9.99CR" ) ).
  IF gst-applies THEN
    RUN hpgl-replace( "@GST@",   STRING( Voucher.TaxValue, ">>,>>>,>>9.99CR" ) ).
  RUN hpgl-replace( "@Total@", STRING( Voucher.GoodsValue + Voucher.TaxValue, ">>,>>>,>>9.99CR" ) ).

  RUN replace-budgets.
  RUN replace-percentage-recoverable.

  RUN hpgl-replace( "@DueDate@", IF Voucher.DateDue = ? THEN "" ELSE
    STRING( Voucher.DateDue, "99/99/9999" ) ).
  RUN hpgl-replace( "@PayBy@", IF Voucher.PaymentStyle = ? THEN "" ELSE
    Voucher.PaymentStyle ).

  voucher-codes = hpgl-codes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verification-prompts Procedure 
PROCEDURE verification-prompts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* Verification Prompts */
  RUN hpgl-moveto( 23, 132 ).
  RUN hpgl-text( "Helvetica,Point,12,Proportional", "Invoice Numbers( office use only ):" ).
  RUN hpgl-move-relative( 71, 0 ).
  RUN hpgl-line-relative( 106, 0 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE voucher-account-desc Procedure 
PROCEDURE voucher-account-desc :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER account-desc AS CHAR NO-UNDO INITIAL "".
  
  IF LOOKUP( VoucherLine.EntityType, "C,T" ) <> 0 THEN RETURN.
  account-desc = SUBSTR( get-entity-account( VoucherLine.EntityType, VoucherLine.EntityCode, VoucherLine.AccountCode ), 1, 18).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE voucher-approval-forms Procedure 
PROCEDURE voucher-approval-forms :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN get-fixed-codes.
  RUN get-variables.

  /* The voucher-dependent parts of the page */
  DEF VAR i AS INT NO-UNDO.
  
  IF voucher-list <> ? THEN
  DO i = 1 TO NUM-ENTRIES( voucher-list ):
    FIND Voucher WHERE
      Voucher.VoucherSeq    = INT ( ENTRY( i, voucher-list ) ) NO-LOCK NO-ERROR.
    IF AVAILABLE Voucher THEN RUN each-voucher.
  END.
  
  IF voucher-start <> ? AND voucher-end <> ? THEN
  FOR EACH Voucher NO-LOCK WHERE
    Voucher.VoucherSeq >= voucher-start AND
    Voucher.VoucherSeq <= voucher-end:
    RUN each-voucher.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE voucher-entity-name Procedure 
PROCEDURE voucher-entity-name :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER entity-name AS CHAR NO-UNDO  INITIAL "Unknown".

  IF VoucherLine.EntityType = "L" THEN DO:
    FIND Company WHERE Company.CompanyCode = VoucherLine.EntityCode NO-LOCK NO-ERROR.
    IF AVAILABLE Company THEN entity-name = SUBSTR( Company.ShortName, 1, 12 ).
  END.
  ELSE
    entity-name = get-entity-name( VoucherLine.EntityType, VoucherLine.EntityCode ).

  entity-name = SUBSTR( entity-name, 1, 12 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


