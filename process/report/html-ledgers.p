&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/
DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

DEF VAR preview AS LOGICAL NO-UNDO.
DEF VAR company-1 AS INTEGER NO-UNDO.
DEF VAR company-n AS INTEGER NO-UNDO.

DEF VAR selection-type AS CHAR NO-UNDO.
DEF VAR print-p-and-l AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR print-balance-sheet AS LOGICAL INITIAL Yes NO-UNDO.
DEF VAR exclude-yend AS LOGICAL INITIAL NO NO-UNDO.

DEF VAR to-month AS INT NO-UNDO.
DEF VAR from-month AS INT NO-UNDO INITIAL ?.
RUN parse-parameters.

FIND MONTH WHERE MONTH.MonthCode = to-month NO-LOCK.
DEF VAR to-month-end AS CHAR NO-UNDO.
to-month-end = STRING(MONTH.EndDate,"99/99/9999").
IF from-month = ? THEN DO:
  DEF VAR financial-year AS INT NO-UNDO.
  financial-year = MONTH.FinancialYearCode.
  FIND FIRST MONTH WHERE MONTH.FinancialYearCode = financial-year NO-LOCK.
  from-month = MONTH.MonthCode.
END.
ELSE DO:
  FIND FIRST MONTH WHERE MONTH.MonthCode = from-month NO-LOCK.
END.
DEF VAR from-month-start AS CHAR NO-UNDO.
from-month-start = STRING(MONTH.StartDate,"99/99/9999").

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}

{inc/ofc-this.i}

DEF VAR timeStamp AS CHAR FORMAT "X(40)" NO-UNDO.
timeStamp = STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name .


/* Formats */
DEF VAR money-format AS CHAR NO-UNDO INITIAL "->>,>>>,>>>,>>9.99".

/* Accumulators */
DEF VAR ledger-income AS DECIMAL NO-UNDO.
DEF VAR ledger-expenses AS DECIMAL NO-UNDO.
DEF VAR ledger-assets AS DECIMAL NO-UNDO.
DEF VAR ledger-liabilities AS DECIMAL NO-UNDO.
DEF VAR ledger-earnings AS DECIMAL NO-UNDO.

DEF VAR first-page AS LOGICAL INITIAL YES.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-bracketed) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD bracketed Procedure 
FUNCTION bracketed RETURNS CHARACTER
  ( amount AS DECIMAL, sect AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-div) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD div Procedure 
FUNCTION div RETURNS CHARACTER
  ( INPUT content AS CHAR, INPUT attributes AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-do-balance-sheet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD do-balance-sheet Procedure 
FUNCTION do-balance-sheet RETURNS CHARACTER
  (  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-do-p-and-l-report) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD do-p-and-l-report Procedure 
FUNCTION do-p-and-l-report RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-do-report-section) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD do-report-section Procedure 
FUNCTION do-report-section RETURNS CHARACTER
  ( INPUT grouptype AS CHAR, INPUT report-section AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-empty) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD empty Procedure 
FUNCTION empty RETURNS CHARACTER
  ( INPUT num AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-account-balance) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-account-balance Procedure 
FUNCTION get-account-balance RETURNS DECIMAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-page-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD page-header Procedure 
FUNCTION page-header RETURNS CHARACTER
  ( INPUT page-title AS CHAR, INPUT period-display AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-row-after) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD row-after Procedure 
FUNCTION row-after RETURNS CHARACTER
  ( INPUT level AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-row-ledger) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD row-ledger Procedure 
FUNCTION row-ledger RETURNS CHARACTER
  ( INPUT level AS INT, INPUT line-description AS CHAR, INPUT amount AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-row-title) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD row-title Procedure 
FUNCTION row-title RETURNS CHARACTER
  ( INPUT level AS INT, INPUT line-description AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-start-report) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD start-report Procedure 
FUNCTION start-report RETURNS CHARACTER
  ( INPUT header-report-type AS CHAR, INPUT period-display AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-tbody) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD tbody Procedure 
FUNCTION tbody RETURNS CHARACTER
  ( INPUT content AS CHAR, INPUT attributes AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-td) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD td Procedure 
FUNCTION td RETURNS CHARACTER
  ( INPUT content AS CHAR, INPUT attributes AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-td-amount) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD td-amount Procedure 
FUNCTION td-amount RETURNS CHARACTER
  ( INPUT amount AS DECIMAL, INPUT fmt AS CHAR, INPUT class AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-tr) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD tr Procedure 
FUNCTION tr RETURNS CHARACTER
  ( INPUT content AS CHAR, INPUT attributes AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .1
         WIDTH              = 33.43.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

txtrep-file-suffix = ".html".
RUN txtrep-initialise.

OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

RUN htmlrep-start('Ledger Report').
RUN report-headers.
RUN htmlrep-body.

RUN for-each-company.

/* OUTPUT CLOSE. */
RUN htmlrep-finish.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-for-each-company) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE for-each-company Procedure 
PROCEDURE for-each-company :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  FOR EACH Company WHERE Company.Active AND Company.CompanyCode >= company-1
                      AND Company.CompanyCode <= company-n
                      NO-LOCK:
    
    ledger-earnings = ?.
  
    IF print-p-and-l THEN do-p-and-l-report().
    IF print-balance-sheet THEN do-balance-sheet().
  
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i           AS INT NO-UNDO.
DEF VAR token       AS CHAR NO-UNDO.

DEF VAR report-type AS CHAR NO-UNDO.

{inc/showopts.i "report-options"}
  
  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).

    CASE( ENTRY( 1, token ) ):
      WHEN "EntityRange" THEN ASSIGN
        company-1 = INT( ENTRY(2,token) )
        company-n   = INT( ENTRY(3,token) ).

      WHEN "ReportType" THEN        report-type = ENTRY(2,token).
      WHEN "SelectionType" THEN     selection-type = ENTRY(2,token).
      WHEN "EndMonth" THEN          to-month = INT(ENTRY(2,token)).
      WHEN "StartMonth" THEN        from-month = INT(ENTRY(2,token)).
      WHEN "ExcludeYearEnd" THEN    exclude-yend = ENTRY(2,token) BEGINS "Y".
    END CASE.
  END.

  IF company-n < company-1 THEN company-n = company-1.

  print-p-and-l = (report-type = '*' OR report-type = 'P').
  print-balance-sheet = (report-type = '*' OR report-type = 'B').
        
  IF selection-type = "*" THEN ASSIGN 
            company-1 = 0
            company-n = 999999.
  ELSE IF selection-type = "1" THEN ASSIGN 
            company-n = company-1.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-line Procedure 
PROCEDURE print-line :
/*------------------------------------------------------------------------------
  Purpose:  Print a line of output
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER the-line AS CHAR NO-UNDO.

  PUT CONTROL REPLACE(the-line, '&', '&amp;').

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-report-headers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-headers Procedure 
PROCEDURE report-headers :
/*------------------------------------------------------------------------------
  Purpose:  Print the headers for the whole report.  The style sheet link, mainly
------------------------------------------------------------------------------*/
  
  PUT CONTROL '<link rel="stylesheet" type="text/css" href="html-ledgers.css">~n' .
  htmlrep-css-file = 'support/css/html-ledgers.css'.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-bracketed) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION bracketed Procedure 
FUNCTION bracketed RETURNS CHARACTER
  ( amount AS DECIMAL, sect AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR answer AS CHAR NO-UNDO INITIAL ''.
  IF sect = 'I' OR sect = 'L' OR sect = 'Q' THEN amount = - amount.

  IF amount < 0 THEN DO:
    answer = '(' + TRIM(STRING(abs(amount), '>>,>>>,>>>,>>9.99')) + ')'.
    answer = '<span class="negative">' + answer + '</span>'.
  END.
  ELSE DO:
    answer = '<span class="positive">' + TRIM(STRING(abs(amount), '>>,>>>,>>>,>>9.99')) + '</span>'.
  END.

  RETURN answer.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-div) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION div Procedure 
FUNCTION div RETURNS CHARACTER
  ( INPUT content AS CHAR, INPUT attributes AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Shorthand for specific html tag
------------------------------------------------------------------------------*/

  RETURN tag("div", content, attributes ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-do-balance-sheet) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION do-balance-sheet Procedure 
FUNCTION do-balance-sheet RETURNS CHARACTER
  (  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR period-display AS CHAR NO-UNDO INITIAL ''.

  period-display = "As of " + to-month-end.
  
  start-report( 'Balance Sheet', period-display ).

  IF ledger-earnings = ? THEN do-p-and-l-report().

  do-report-section( 'B', 'A' ).
  do-report-section( 'B', 'L' ).
  do-report-section( 'B', 'Q' ).

  RUN print-line( "</table>~n</div>~n" ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-do-p-and-l-report) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION do-p-and-l-report Procedure 
FUNCTION do-p-and-l-report RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR period-display AS CHAR NO-UNDO INITIAL ''.

  IF print-p-and-l THEN DO:
    period-display = "From " + from-month-start + " to " + to-month-end.
    
    start-report( 'Profit & Loss Statement', period-display ).
  END.
  
  ledger-earnings = 0.0.
  do-report-section( 'P', 'I' ).
  do-report-section( 'P', 'E' ).

  IF print-p-and-l THEN DO:
    RUN print-line( row-ledger( 0, 'Net Profit/Loss', bracketed(ledger-earnings, 'I')) ).
    RUN print-line( row-after( 0 ) ).
  END.

  IF print-p-and-l THEN DO:
    RUN print-line( "</table>~n</div>~n" ).
  END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-do-report-section) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION do-report-section Procedure 
FUNCTION do-report-section RETURNS CHARACTER
  ( INPUT grouptype AS CHAR, INPUT report-section AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR section-name AS CHAR NO-UNDO.
DEF VAR section-total AS DECIMAL NO-UNDO INITIAL 0.0.
DEF VAR group-total AS DECIMAL NO-UNDO.
DEF VAR found-one AS LOGI NO-UNDO.
DEF VAR account-balance AS DECIMAL NO-UNDO.
DEF VAR account-rows AS CHAR NO-UNDO.

  CASE report-section:
    WHEN 'I' THEN section-name = 'Income'.
    WHEN 'E' THEN section-name = 'Expenses'.
    WHEN 'A' THEN section-name = 'Assets'.
    WHEN 'L' THEN section-name = 'Liabilities'.
    WHEN 'Q' THEN section-name = 'Equity'.
  END CASE.

  IF (grouptype <> 'P' OR print-p-and-l) THEN DO:
    RUN print-line( "<tbody class='" + section-name + "'>" ).
    RUN print-line( row-title( 0, section-name ) ).
  END.
  
  FOR EACH AccountGroup NO-LOCK WHERE GroupType = grouptype AND ReportSection = report-section BY SequenceCode:
    group-total = 0.
    found-one = NO.
    account-rows = ''.
    FOR EACH ChartOfAccount NO-LOCK OF AccountGroup BY AccountCode:
      account-balance = get-account-balance().
      IF account-balance <> 0 THEN DO:
        found-one = YES.
        section-total = section-total + account-balance.
        IF grouptype = 'P' THEN DO:
          ledger-earnings = ledger-earnings + account-balance.
        END.
        account-rows = account-rows + row-ledger( 2, ChartOfAccount.Name, bracketed( account-balance, report-section) ).
        group-total = group-total + account-balance.
      END.
    END.
    
    /* Fake the ledger earnings into the REARNS group */
    IF ( report-section = 'Q' AND AccountGroup.AccountGroupCode = 'REARNS' ) THEN DO:
      account-rows = account-rows + row-ledger( 2, 'Current Earnings', bracketed(ledger-earnings,'I') ).
      group-total = group-total + ledger-earnings.
      found-one = YES.
      section-total = section-total + ledger-earnings.
    END.
    
    IF found-one AND ( grouptype <> 'P' OR print-p-and-l) THEN DO:
      RUN print-line( row-title( 1, AccountGroup.Name ) ).
      RUN print-line( account-rows ).
      RUN print-line( row-ledger( 1, 'Total ' + AccountGroup.Name, bracketed( group-total, report-section) ) ).
      RUN print-line( row-after( 1 ) ).
    END.
  END.
  
  CASE report-section:
    WHEN 'I' THEN ledger-income = section-total.
    WHEN 'E' THEN ledger-expenses = section-total.
    WHEN 'A' THEN ledger-assets = section-total.
    WHEN 'L' THEN ledger-liabilities = section-total.
  END CASE.
  
  IF (grouptype <> 'P' OR print-p-and-l) THEN DO:
/*
    IF  THEN section-total = 0 - section-total.
*/
    RUN print-line( row-ledger( 0, 'Total ' + section-name, bracketed( section-total, report-section) ) ).
    RUN print-line( row-after( 0 ) ).
    RUN print-line( "</tbody>" ).
  END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-empty) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION empty Procedure 
FUNCTION empty RETURNS CHARACTER
  ( INPUT num AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Return some empty cells
------------------------------------------------------------------------------*/

  RETURN td("", 'colspan="' + TRIM(STRING(num,">>9")) + '" class="empty"').

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-account-balance) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-account-balance Procedure 
FUNCTION get-account-balance RETURNS DECIMAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR account-balance AS DECIMAL NO-UNDO INITIAL 0.0 .  
  
IF AccountGroup.GroupType = 'B' THEN DO:    
    FIND FIRST AccountSummary NO-LOCK OF ChartOfAccount WHERE AccountSummary.Entitytype = 'L' AND AccountSummary.EntityCode = Company.CompanyCode NO-ERROR.
    IF AVAILABLE(AccountSummary) THEN account-balance = AccountSummary.Balance. ELSE account-balance = 0.
    FOR EACH AccountBalance NO-LOCK OF ChartOfAccount WHERE AccountBalance.Entitytype = 'L' AND AccountBalance.EntityCode = Company.CompanyCode AND MonthCode > to-month:
      account-balance = account-balance - AccountBalance.Balance.
    END.
  END.
  ELSE DO:
    FOR EACH AccountBalance NO-LOCK OF ChartOfAccount WHERE AccountBalance.Entitytype = 'L' AND AccountBalance.EntityCode = Company.CompanyCode AND MonthCode >= from-month AND MonthCode <= to-month:
      account-balance = account-balance + AccountBalance.Balance.
    END.
  END.

  RETURN account-balance.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-page-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION page-header Procedure 
FUNCTION page-header RETURNS CHARACTER
  ( INPUT page-title AS CHAR, INPUT period-display AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  The per-company headers for the report.
------------------------------------------------------------------------------*/
DEF VAR report AS CHAR NO-UNDO INITIAL ''.
  
  report = report + tr( td( Company.LegalName, 'colspan="2" class="companyname"'), '').
  report = report + tr( td( page-title, 'colspan="2" class="reportheader"'), '' ).
  report = report + tr( td( period-display,'colspan="2" class="perioddisplay"'), '').
  
  report = report + tr( empty(2), 'class="afterheader"').
  
  report = tag("thead", report, '').

  RETURN report.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-row-after) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION row-after Procedure 
FUNCTION row-after RETURNS CHARACTER
  ( INPUT level AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Format a row in the p&l or balance sheet table.
------------------------------------------------------------------------------*/
DEF VAR ln AS CHAR NO-UNDO.
DEF VAR style-suffix AS CHAR NO-UNDO.

  style-suffix = "-L" + TRIM( STRING(level,'>>>9') ) + "'".

  ln = "<tr class='after" + style-suffix + "'><td colspan='2' class='after" + style-suffix + '> </td></tr>'.
  
  RETURN ln.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-row-ledger) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION row-ledger Procedure 
FUNCTION row-ledger RETURNS CHARACTER
  ( INPUT level AS INT, INPUT line-description AS CHAR, INPUT amount AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Format a row in the p&l or balance sheet table.
------------------------------------------------------------------------------*/
DEF VAR ln AS CHAR NO-UNDO.
DEF VAR style-suffix AS CHAR NO-UNDO.

  style-suffix = "-L" + TRIM( STRING(level,'>>>9') ) + "'".

/*
  IF AVAILABLE(AccountGroup) AND (AccountGroup.ReportSection = 'I' OR AccountGroup.ReportSection = 'A') THEN amount = 0 - amount.
*/

  ln = tr(
           td( line-description, "class='description" + style-suffix ) +
           td( amount, "class='amount" + style-suffix )
       , "class='ledger-row" + style-suffix ).

  RETURN ln.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-row-title) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION row-title Procedure 
FUNCTION row-title RETURNS CHARACTER
  ( INPUT level AS INT, INPUT line-description AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Format a row in the p&l or balance sheet table.
------------------------------------------------------------------------------*/
DEF VAR ln AS CHAR NO-UNDO.
DEF VAR style-suffix AS CHAR NO-UNDO.

  style-suffix = "-L" + TRIM( STRING(level,'>>>9') ) + "'".

  ln = tr( td( line-description, "colspan='2' class='title" + style-suffix )
                   , "class='title" + style-suffix ).

  RETURN ln.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-start-report) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION start-report Procedure 
FUNCTION start-report RETURNS CHARACTER
  ( INPUT header-report-type AS CHAR, INPUT period-display AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  IF NOT(first-page) THEN DO:
    RUN print-line( '<hr class="pagebreak">~n<div class="report">~n' ).
  END.
  ELSE DO:
    first-page = NO.
  END.
  RUN print-line( '<table class="ledger">~n' ).

  RUN print-line( page-header( header-report-type, period-display ) ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-tbody) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION tbody Procedure 
FUNCTION tbody RETURNS CHARACTER
  ( INPUT content AS CHAR, INPUT attributes AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/

  RETURN tag("tbody", content, attributes) + "~n".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-td) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION td Procedure 
FUNCTION td RETURNS CHARACTER
  ( INPUT content AS CHAR, INPUT attributes AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Shorthand for specific html tag
------------------------------------------------------------------------------*/

  RETURN "    " + tag("td", content, attributes ) + "~n".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-td-amount) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION td-amount Procedure 
FUNCTION td-amount RETURNS CHARACTER
  ( INPUT amount AS DECIMAL, INPUT fmt AS CHAR, INPUT class AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Return an appropriate attribute string for negative amounts.
------------------------------------------------------------------------------*/
DEF VAR attributes AS CHAR NO-UNDO INITIAL ''.

  attributes = "class='" + class + "'".

  IF amount < 0.0 THEN attributes = attributes + ' style="color:red"'.

  RETURN td( TRIM(STRING(amount,fmt)), attributes ).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-tr) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION tr Procedure 
FUNCTION tr RETURNS CHARACTER
  ( INPUT content AS CHAR, INPUT attributes AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  Shorthand for specific html tag
------------------------------------------------------------------------------*/

  RETURN "  " + tag("tr", content, attributes ) + "~n".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

