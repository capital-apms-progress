&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  Report:   Creditor List printout
  Author:   Andrew McMillan
  Date:     16/7/2001
  ------------------------------------------------------------------------*/

DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

DEF VAR last-used AS DATE NO-UNDO.
DEF VAR active-only AS LOGI NO-UNDO INITIAL No.
RUN parse-parameters.

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
DEF VAR timeStamp AS CHAR FORMAT "X(44)" NO-UNDO.
timeStamp = STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.

DEF VAR pr-line AS CHAR INIT "" NO-UNDO.        /* used everywhere to hold print line */
DEF VAR rowcounter AS INT INIT 0.

DEF VAR title-font  AS CHAR NO-UNDO   INITIAL "font-family: sans-serif; font-weight: 700; font-size: 14pt".
DEF VAR time-font   AS CHAR NO-UNDO   INITIAL "font-family: tahoma,sans-serif; font-size: 8pt;".
DEF VAR break1-font AS CHAR NO-UNDO   INITIAL "font-family: serif; font-size: 14pt; font-weight: 700;".
DEF VAR break2-font AS CHAR NO-UNDO   INITIAL "font-family: serif; font-size: 11pt; font-weight: 700;".
DEF VAR base-font   AS CHAR NO-UNDO   INITIAL "font-family: serif; font-size: 10pt;".

{inc/ofc-this.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 28.25
         WIDTH              = 36.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}
{inc/null.i}
{inc/entity.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* Re-initialise with a more appropriate suffix for HTML files */
txtrep-file-suffix = ".html".
RUN txtrep-initialise.

OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

RUN htmlrep-start('Creditors List').

RUN report-headers.

FOR EACH Creditor NO-LOCK,
         LAST AcctTran WHERE EntityType = 'C' AND EntityCode = CreditorCode NO-LOCK
         BY Creditor.Name:
  RUN each-creditor.
END.
RUN report-footers.

OUTPUT CLOSE.

RUN htmlrep-finish.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-each-creditor) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-creditor Procedure 
PROCEDURE each-creditor :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  IF AcctTran.Date < last-used THEN RETURN.
  IF NOT(Creditor.Active) AND active-only THEN RETURN.
  
  PUT UNFORMATTED '<tr class=row' (rowcounter MODULO 2) '>~n'.
  PUT UNFORMATTED "<td align=right>" STRING(Creditor.CreditorCode) "</td>~n".
  PUT UNFORMATTED "<td>" Creditor.Name  "</td>~n".
  PUT UNFORMATTED "<td>" STRING( AcctTran.Date, '99/99/9999') "</td>~n".
  PUT CONTROL "</tr>~n".
  rowcounter = rowcounter + 1.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR token AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.

{inc/showopts.i "report-options"}
last-used = TODAY - 800.

  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).
    
    CASE ENTRY( 1, token ):
      WHEN "Selection" THEN ASSIGN
        active-only = ENTRY(2,token) = "Active".
      WHEN "LastUsed" THEN ASSIGN
        last-used = DATE( ENTRY(2,token) ).
    END CASE.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-report-footers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-footers Procedure 
PROCEDURE report-footers :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
PUT UNFORMATTED "</table>~n".

PUT UNFORMATTED '<p><p style="' time-font '">Report prepared at ' timeStamp .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-report-headers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE report-headers Procedure 
PROCEDURE report-headers :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

PUT UNFORMATTED '<style type="text/css"><!--~n'.
PUT UNFORMATTED '.base, p, td ~{' base-font '}~n'.
PUT UNFORMATTED '.title ~{' title-font '}~n'.
PUT UNFORMATTED '.row0 ~{ background: #f0f0c0; }~n'.
PUT UNFORMATTED '.row1 ~{ background: #fafbe8; }~n'.
PUT UNFORMATTED '.break1 ~{' break1-font '}~n'.
PUT UNFORMATTED '.break2 ~{' break2-font '}~n'.
PUT UNFORMATTED '--></style>~n'.

RUN htmlrep-body.
PUT UNFORMATTED '<h2 style="' + title-font + '">Creditor Listing </h2>~n'.


PUT UNFORMATTED "<table>~n".
  
PUT UNFORMATTED "<tr>~n".
PUT UNFORMATTED "<th>#</th>~n".
PUT UNFORMATTED "<th>Name</th>~n".
PUT UNFORMATTED "<th>Last Used</th>~n".
PUT CONTROL "</tr>~n".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

