DEF TEMP-TABLE Expiring NO-UNDO
        FIELD PropertyCode LIKE Property.PropertyCode
        FIELD ExpiringOn LIKE TenancyLease.LeaseEndDate
        FIELD ValueExpiring LIKE RentalSpace.ContractedRental
        FIELD AreaExpiring LIKE RentalSpace.AreaSize
        INDEX XPKExpiries IS PRIMARY PropertyCode ExpiringOn.

DEF VAR end-date AS DATE NO-UNDO.
DEF VAR month-name AS CHAR NO-UNDO      FORMAT "X(7)" LABEL " Month ".
DEF VAR month-value AS DEC NO-UNDO      FORMAT "->>,>>>,>>>,>>9" LABEL "     Value".
DEF VAR month-area AS DEC NO-UNDO       FORMAT "->>,>>>,>>9" LABEL "    Area".

FOR EACH Property WHERE Property.Active:
  FOR EACH TenancyLease OF Property WHERE TenancyLease.LeaseStatus <> "PAST"
                        NO-LOCK BY TenancyLease.LeaseEndDate :
    FIND Expiring WHERE Expiring.PropertyCode = Property.PropertyCode
                    AND Expiring.ExpiringOn = TenancyLease.LeaseEndDate
                    NO-ERROR.
    end-date = TenancyLease.LeaseEndDate.
    IF end-date < TODAY THEN end-date = ?.
    IF NOT AVAILABLE(Expiring) THEN DO:
      CREATE Expiring.
      Expiring.PropertyCode = Property.PropertyCode.
      Expiring.ExpiringOn = TenancyLease.LeaseEndDate.
    END.
    FOR EACH RentalSpace OF TenancyLease NO-LOCK:
      Expiring.ValueExpiring = Expiring.ValueExpiring + RentalSpace.ContractedRental .
      IF INDEX( "CIORW", RentalSpace.AreaType) > 0 THEN
        Expiring.AreaExpiring = Expiring.AreaExpiring + RentalSpace.AreaSize .
    END.
  END.
END.

DEFINE FRAME x WITH DOWN.

FOR EACH Property WHERE Property.Active:
  FIND Expiring WHERE Expiring.PropertyCode = Property.PropertyCode NO-LOCK NO-ERROR.
  IF AVAILABLE(Expiring) THEN DO:
    DISPLAY Property.PropertyCode
            "Monthly"                   @ month-name
            Expiring.AreaExpiring       @ month-area
            Expiring.ValueExpiring      @ month-value
            WITH FRAME x .
    DOWN WITH FRAME x.
  END.
  FOR EACH Month NO-LOCK WHERE CAN-FIND( FIRST Expiring OF Property
                                 WHERE Expiring.ExpiringOn >= Month.StartDate
                                 AND Expiring.ExpiringOn <= Month.EndDate) :
    month-name = Month.MonthName + " " + SUBSTRING( STRING( YEAR(Month.StartDate), "9999"), 3).
    month-area = 0.
    month-value = 0.
    FOR EACH Expiring OF Property WHERE Expiring.ExpiringOn >= Month.StartDate
                                  AND Expiring.ExpiringOn <= Month.EndDate:
      month-area = month-area + Expiring.AreaExpiring.
      month-value = month-value + Expiring.ValueExpiring.
    END.
    DISPLAY Property.PropertyCode month-name month-area month-value WITH FRAME x .
    DOWN WITH FRAME x.
  END.
END.
