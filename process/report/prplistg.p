&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

&IF DEFINED(UIB_IS_RUNNING) &THEN
DEF VAR report-options AS CHAR NO-UNDO  INITIAL "".
&ELSE
DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.
&ENDIF

DEF VAR active-flag AS LOGI NO-UNDO INIT Yes.
DEF VAR by-propertycode AS LOGI NO-UNDO.
DEF VAR by-region AS LOGI NO-UNDO.
DEF VAR by-manager AS LOGI NO-UNDO.
DEF VAR by-portfolio AS LOGI NO-UNDO.
DEF VAR by-shortname AS LOGI NO-UNDO.

DEF VAR sort-desc AS CHAR NO-UNDO.
DEF VAR filt-desc AS CHAR NO-UNDO.

DEF VAR preview   AS LOGI NO-UNDO INIT Yes.
DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
DEF VAR timeStamp AS CHAR FORMAT "X(44)" NO-UNDO.
timeStamp = STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.


/* $History: prplistg.p $
 * 
 * *****************  Version 4  *****************
 * User: Andrew       Date: 22/12/97   Time: 14:49
 * Updated in $/PROCESS/REPORT
 * Should only check in a couple of items - let's see!
 * 
 * *****************  Version 3  *****************
 * User: Andrew       Date: 22/12/97   Time: 9:44
 * Updated in $/PROCESS/REPORT
 * Testing new SS version
 * 
 * *****************  Version 2  *****************
 * User: Andrew       Date: 19/12/97   Time: 10:46
 * Updated in $/PROCESS/REPORT
 * Sending an update to OZ and to Auckland
 * 
 * *****************  Version 1  *****************
 * User: Andrew       Date: 21/07/97   Time: 0:08
 * Created in $/PROCESS/REPORT
 * 
 * *****************  Version 3  *****************
 * User: Tyrone       Date: 7/15/97    Time: 10:52a
 * Updated in $/PROCESS/REPORT
 * 
 * *****************  Version 2  *****************
 * User: Tyrone       Date: 7/11/97    Time: 2:23p
 * Updated in $/PROCESS/REPORT
 * 
 * *****************  Version 1  *****************
 * User: Tyrone       Date: 7/10/97    Time: 9:35a
 * Created in $/PROCESS/REPORT
 * 
 * *****************  Version 5  *****************
 * User: Andrew       Date: 16/06/97   Time: 11:15
 * Updated in $/PROCESS/REPORT
 * Back from Auckland
 * 
 * *****************  Version 10  *****************
 * User: Andrew       Date: 11/06/97   Time: 10:24
 * Updated in $/PROCESS/REPORT
 * In Auckland
 * 
 * *****************  Version 9  *****************
 * User: Andrew       Date: 28/03/97   Time: 17:27
 * Updated in $/PROCESS/REPORT
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .29
         WIDTH              = 30.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

RUN parse-parameters.

OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

RUN pclrep-start( preview, "reset,portrait,tm,2,a4,lm,4,courier,cpi,18,lpi,9").

RUN property-listing.

OUTPUT CLOSE.

RUN pclrep-finish.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-property Procedure 
PROCEDURE each-property :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN pclrep-line( "courier,Fixed,cpi,18,lpi,9,Normal", 
    STRING( Property.PropertyCode, "99999" ) + SPC(4) +
    STRING( Property.Active, "Yes/No" ) + SPC( 4 ) +
    STRING( Property.ShortName, "X(10)" ) + SPC( 4 ) +
    STRING( Property.Name, "X(30)" ) + SPC( 4 ) +
    STRING( Property.StreetAddress, "X(50)" ) + SPC( 4 ) +
    STRING( Property.City, "X(15)" )
  ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE group-header Procedure 
PROCEDURE group-header :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER header-text AS CHAR NO-UNDO.
  
  RUN pclrep-line( "Helvetica,Point,8,Bold,Proportional", header-text ).
  RUN pclrep-line( "", "" ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-footer Procedure 
PROCEDURE inst-page-footer :
/*------------------------------------------------------------------------------
  Purpose:  Print any page footer
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-header Procedure 
PROCEDURE inst-page-header :
/*------------------------------------------------------------------------------
  Purpose:  Print any page header
------------------------------------------------------------------------------*/

  RUN pclrep-line( "univers,Point,7,bold,proportional", TimeStamp).
  RUN pclrep-line( "univers,Point,12,bold,proportional",
    FILL( " ", 45) + "Property Listing by " + sort-desc
  ).
  
  RUN pclrep-line( "", "").
  RUN pclrep-line( "courier,Fixed,cpi,18,lpi,9,Bold",
    STRING( "Code", "X(5)" ) + SPC(4) +
    STRING( "Active", "X(5)" ) + SPC( 2 ) +
    STRING( "Short Name", "X(10)" ) + SPC( 4 ) +
    STRING( "Name", "X(30)" ) + SPC( 4 ) +
    STRING( "Street Address", "X(50)" ) + SPC( 4 ) +
    STRING( "City", "X(15)" )
  ).
  RUN pclrep-line( "", "").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR i AS INT NO-UNDO.
  DEF VAR token AS CHAR NO-UNDO.
  
  DO i  = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).

    CASE ENTRY( 1, token ):

      WHEN "Sort" THEN
      DO:
        CASE ENTRY( 2,token ):
          WHEN "Code"      THEN ASSIGN by-propertycode = Yes sort-desc = "Property Code".
          WHEN "Region"    THEN ASSIGN by-region = Yes sort-desc = "Region".
          WHEN "ShortName" THEN ASSIGN by-shortname = Yes sort-desc = "Short Name".
          WHEN "Manager"   THEN ASSIGN by-manager = Yes sort-desc = "Property Manager".
        END CASE.
      END.

      WHEN "Active" THEN
      CASE ENTRY( 2, token ):
          WHEN "Yes" THEN ASSIGN active-flag = Yes filt-desc = "Active Only".
          WHEN "No"  THEN ASSIGN active-flag = No filt-desc = "Inactive Only".
          OTHERWISE ASSIGN active-flag = ? filt-desc = "".
      END CASE.

      WHEN "Preview" THEN preview = ENTRY( 2, token ) = "Yes".
      
    END CASE.
  
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE property-listing Procedure 
PROCEDURE property-listing :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

&SCOP WHERE-PHRASE WHERE ( IF active-flag = ? THEN True ELSE Property.Active = active-flag )

  IF by-propertycode THEN
  FOR EACH Property NO-LOCK {&WHERE-PHRASE}:
    RUN each-property.
  END.

  ELSE IF by-region THEN
  FOR EACH Property NO-LOCK {&WHERE-PHRASE}
    BREAK BY Property.Region:
    
    IF FIRST-OF( Property.Region ) THEN
    DO:
      FIND Region WHERE Region.Region = Property.Region NO-LOCK NO-ERROR.
      RUN group-header( IF AVAILABLE Region THEN Region.Name ELSE "Region - Unknown" ).
    END.

    RUN each-property.

    IF LAST-OF( Property.Region ) THEN RUN pclrep-line( ?, ? ).
    
  END.

  ELSE IF by-shortname THEN
  FOR EACH Property NO-LOCK {&WHERE-PHRASE} BY Property.ShortName:
    RUN each-property.
  END.

  ELSE IF by-manager THEN
  FOR EACH Property NO-LOCK {&WHERE-PHRASE}
    BREAK BY Property.Manager:
    
    IF FIRST-OF( Property.Manager ) THEN
    DO:
      FIND Person WHERE Person.PersonCode = Property.Manager NO-LOCK NO-ERROR.
      RUN group-header( IF AVAILABLE Person
        THEN ( "Managed by - " + TRIM( Person.FirstName + " " + Person.LastName ) )
        ELSE "Not managed" ).
    END.
    
    RUN each-property.

    IF LAST-OF( Property.Manager ) THEN RUN pclrep-line( ?, ? ).

  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


