&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

DEF VAR preview             AS LOGI NO-UNDO INIT No.
DEF VAR reconcile-types AS CHAR NO-UNDO.
DEF VAR m-1     AS INT NO-UNDO.
DEF VAR m-n     AS INT NO-UNDO.
DEF VAR ec-1    AS INT NO-UNDO.
DEF VAR ec-n    AS INT NO-UNDO.
DEF VAR ac-1    AS DEC NO-UNDO.
DEF VAR ac-n    AS DEC NO-UNDO.
DEF VAR hdr-run-description AS CHAR NO-UNDO.
RUN parse-parameters.
IF RETURN-VALUE = "FAIL" THEN RETURN.

DEF VAR reconcile-procedure AS CHAR NO-UNDO.
DEF VAR proc-n AS INT NO-UNDO.
DEF VAR money-fmt AS CHAR NO-UNDO INITIAL ">>>,>>>,>>9.99CR".
DEF VAR items-processed AS INT NO-UNDO.
DEF VAR not-in-balance AS INT NO-UNDO.

{inc/ofc-this.i}
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}
{inc/ofc-acct.i "CREDITORS" "sundry-creditors"}
DEF VAR creditors-ledger AS INT NO-UNDO.
creditors-ledger = OfficeControlAccount.EntityCode . 

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
DEF VAR timeStamp AS CHAR FORMAT "X(44)" NO-UNDO.
timeStamp = STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.

DEF TEMP-TABLE CtrlAcct NO-UNDO
        FIELD et AS CHAR
        FIELD ec AS INT
        FIELD ac AS DEC
        FIELD mth AS INT
        FIELD act-bal AS DEC INITIAL 0
        FIELD act-bud AS DEC INITIAL 0
        FIELD act-rbd AS DEC INITIAL 0
        FIELD sum-bal AS DEC INITIAL 0
        FIELD sum-bud AS DEC INITIAL 0
        FIELD sum-rbd AS DEC INITIAL 0
        INDEX XPKCtrlAcct IS UNIQUE PRIMARY et ec ac mth
        INDEX XAKCtrlAcct mth ec ac .

DEF WORK-TABLE CtrlTotal NO-UNDO
        FIELD et AS CHAR
        FIELD ec AS INT
        FIELD mth AS INT
        FIELD act-bal AS DEC INITIAL 0
        FIELD act-bud AS DEC INITIAL 0
        FIELD act-rbd AS DEC INITIAL 0
        FIELD sum-bal AS DEC INITIAL 0
        FIELD sum-bud AS DEC INITIAL 0
        FIELD sum-rbd AS DEC INITIAL 0.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD verify-account Procedure 
FUNCTION verify-account RETURNS DECIMAL
  ( INPUT et-p AS CHAR, INPUT ec-p AS INT, INPUT et-s AS CHAR, INPUT ac AS DEC, INPUT mnth AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .33
         WIDTH              = 40.29.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

RUN pclrep-start( preview, "reset,landscape,tm,2,a4,lm,6,courier,cpi,18,lpi,9").

DO proc-n = 1 TO NUM-ENTRIES(reconcile-types):
  reconcile-procedure = "reconcile-" + ENTRY( proc-n, reconcile-types).
  not-in-balance = 0.
  items-processed = 0.
  IF LOOKUP( reconcile-procedure, THIS-PROCEDURE:INTERNAL-ENTRIES ) > 0 THEN DO:
    RUN pclrep-line( "", "" ).
    RUN VALUE( reconcile-procedure ).
  END.
  ELSE
    MESSAGE "Reconciliation of " + SUBSTRING( reconcile-procedure, 11) + " not available."
            VIEW-AS ALERT-BOX ERROR
            TITLE "Huh!?!".
END.

OUTPUT CLOSE.

RUN pclrep-finish.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE account-line Procedure 
PROCEDURE account-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER descr AS CHAR NO-UNDO.

DEF VAR line AS CHAR NO-UNDO.

  FIND Month WHERE Month.MonthCode = CtrlAcct.mth NO-LOCK.
  line = STRING( CtrlAcct.et, "X(2)")
       + STRING( CtrlAcct.ec, "99999" ) + " "
       + STRING( CtrlAcct.ac, "9999.99") + " "
       + STRING( descr, "X(50)") + " "
       + STRING( SUBSTRING( STRING(Month.StartDate,"99/99/9999"), 4), "X(7)") + " "
       + STRING( CtrlAcct.act-bal, money-fmt) + " "
       + STRING( CtrlAcct.sum-bal, money-fmt) + " "
       + STRING( CtrlAcct.act-bal - CtrlAcct.sum-bal, money-fmt) + " "
       + STRING( CtrlAcct.act-bud, money-fmt) + " "
       + STRING( CtrlAcct.sum-bud, money-fmt) + " "
       + STRING( CtrlAcct.act-bud - CtrlAcct.sum-bud, money-fmt) .

  RUN pclrep-line( "Courier,cpi,19,lpi,8,Fixed,Normal", line ).

  not-in-balance = not-in-balance + 1.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE add-balance Procedure 
PROCEDURE add-balance :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR et AS CHAR NO-UNDO.
DEF VAR ec AS INT NO-UNDO.
DEF VAR ac AS DEC NO-UNDO.
DEF VAR tot-ec AS INT NO-UNDO.

DEF BUFFER CtrlBalance FOR AccountBalance.

  RUN get-account-codes( OUTPUT et, OUTPUT ec, OUTPUT ac, OUTPUT tot-ec).

  FIND FIRST CtrlTotal WHERE CtrlTotal.et = et
                 AND CtrlTotal.ec = tot-ec
                 AND CtrlTotal.mth = AccountBalance.MonthCode NO-ERROR.
  IF NOT AVAILABLE(CtrlTotal) THEN DO:
    CREATE CtrlTotal.
    ASSIGN CtrlTotal.et = et
           CtrlTotal.ec = tot-ec
           CtrlTotal.mth = AccountBalance.MonthCode.
  END.


  FIND CtrlAcct WHERE CtrlAcct.et = et AND CtrlAcct.ec = ec AND CtrlAcct.ac = ac
                AND CtrlAcct.mth = AccountBalance.MonthCode NO-ERROR.
  IF NOT AVAILABLE( CtrlAcct ) THEN DO:
    CREATE CtrlAcct.
    ASSIGN CtrlAcct.et = et
           CtrlAcct.ec = ec
           CtrlAcct.ac = ac
           CtrlAcct.mth = AccountBalance.MonthCode.

    items-processed = items-processed + 1.

    FIND CtrlBalance WHERE CtrlBalance.EntityType = et
                       AND CtrlBalance.EntityCode = ec
                       AND CtrlBalance.AccountCode = ac
                       AND CtrlBalance.MonthCode = AccountBalance.MonthCode NO-LOCK NO-ERROR.
    IF AVAILABLE(CtrlBalance) THEN ASSIGN
      CtrlAcct.act-bal  = CtrlBalance.Balance
      CtrlAcct.act-bud  = CtrlBalance.Budget
      CtrlAcct.act-rbd  = CtrlBalance.RevisedBudget
      CtrlTotal.act-bal = CtrlTotal.act-bal + CtrlBalance.Balance
      CtrlTotal.act-bud = CtrlTotal.act-bud + CtrlBalance.Budget
      CtrlTotal.act-rbd = CtrlTotal.act-rbd + CtrlBalance.RevisedBudget .
  END.
  CtrlAcct.sum-bal  = CtrlAcct.sum-bal  + AccountBalance.Balance.
  CtrlAcct.sum-bud  = CtrlAcct.sum-bud  + AccountBalance.Budget.
  CtrlAcct.sum-rbd  = CtrlAcct.sum-rbd  + AccountBalance.RevisedBudget.
  CtrlTotal.sum-bal = CtrlTotal.sum-bal + AccountBalance.Balance.
  CtrlTotal.sum-bud = CtrlTotal.sum-bud + AccountBalance.Budget.
  CtrlTotal.sum-rbd = CtrlTotal.sum-rbd + AccountBalance.RevisedBudget.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-account-codes Procedure 
PROCEDURE get-account-codes :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF OUTPUT PARAMETER et AS CHAR NO-UNDO INITIAL ?.
DEF OUTPUT PARAMETER ec AS INT  NO-UNDO INITIAL ?.
DEF OUTPUT PARAMETER ac AS DEC  NO-UNDO INITIAL ?.
DEF OUTPUT PARAMETER tot-ec AS INT  NO-UNDO INITIAL ?.

  IF NOT AVAILABLE(AccountBalance) THEN RETURN "FAIL".

  CASE AccountBalance.EntityType:
    WHEN "T" THEN DO:
      et = "L".
      tot-ec = 0.
      ac = sundry-debtors.
      FIND Tenant WHERE Tenant.TenantCode = AccountBalance.EntityCode NO-LOCK.
      IF Tenant.EntityType = "P" THEN DO:
        FIND Property WHERE Property.PropertyCode = Tenant.EntityCode NO-LOCK.
        ec = Property.CompanyCode .
      END.
      ELSE IF Tenant.EntityType = "L" THEN
        ec = Tenant.EntityCode .
      ELSE
        MESSAGE "Tenant " + STRING(Tenant.TenantCode) + " updates to other than property or GL!  Reconciliation not supported!".
    END.

    WHEN "C" THEN DO:
      et = "L".
      tot-ec = 0.
      ec = creditors-ledger.
      ac = sundry-creditors.
    END.

    WHEN "P" THEN DO:
      et = "L".
      ac = AccountBalance.AccountCode .
      FIND Property WHERE Property.PropertyCode = AccountBalance.EntityCode NO-LOCK.
      ec = Property.CompanyCode .
      tot-ec = ec.
    END.

  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-footer Procedure 
PROCEDURE inst-page-footer :
/*------------------------------------------------------------------------------
  Purpose:  Print any page footer
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-header Procedure 
PROCEDURE inst-page-header :
/*------------------------------------------------------------------------------
  Purpose:  Print any page header
------------------------------------------------------------------------------*/
DEF VAR money-column-format AS CHAR NO-UNDO.
DEF VAR line AS CHAR NO-UNDO.

  RUN pclrep-line( "univers,Point,7,bold,Proportional", TimeStamp).
  RUN pclrep-line( "", "" ).
  RUN pclrep-line( "univers,Point,12,bold,Proportional",
        FILL(" ",40) + "Sub-Ledgers / Control Accounts - Reconciliation Report" ).
  RUN pclrep-line( "univers,Point,7,normal,Proportional", "").
  RUN pclrep-line( "univers,Point,9,normal,Proportional", hdr-run-description ).
  RUN pclrep-line( "", "" ).

  money-column-format = "X(" + STRING(LENGTH( STRING(0,money-fmt))) + ")".
  line = "Entity/Account  "
       + STRING( "Description", "X(50)") + " "
       + STRING( " Month ", "X(7)") + " "
       + STRING( "       Balance", money-column-format) + " "
       + STRING( "  Subs Balance", money-column-format) + " "
       + STRING( " Bal. Variance", money-column-format) + " "
       + STRING( "        Budget", money-column-format) + " "
       + STRING( "   Subs Budget", money-column-format) + " "
       + STRING( " Bud. Variance", money-column-format) .

  RUN pclrep-line( "Courier,cpi,19,lpi,8,Fixed,bold", line ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR token AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
  
  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).
    
    CASE ENTRY( 1, token ):
      WHEN "Preview" THEN               preview = Yes.

      WHEN "Reconcile" THEN             reconcile-types = SUBSTRING( token, INDEX(token,",") + 1).

      WHEN "MonthRange" THEN ASSIGN
        m-1 = INT( ENTRY( 2, token))
        m-n = INT( ENTRY( 3, token)) .

      WHEN "EntityRange" THEN ASSIGN
        ec-1 = INT( ENTRY( 2, token))
        ec-n = INT( ENTRY( 3, token)) .

      WHEN "AccountRange" THEN ASSIGN
        ac-1 = DEC( ENTRY( 2, token))
        ac-n = DEC( ENTRY( 3, token)) .

    END CASE.
  END.

/*  MESSAGE report-options SKIP(1)
          "Months" m-1 m-n SKIP
          "Types" reconcile-types. */

  FIND Month WHERE Month.MonthCode = m-1 NO-LOCK.
  hdr-run-description = "Reconciling: " + reconcile-types
           + ",  Period from: " + STRING( Month.StartDate, "99/99/9999").
  FIND Month WHERE Month.MonthCode = m-n NO-LOCK.
  hdr-run-description = hdr-run-description + " to " + STRING( Month.EndDate, "99/99/9999").
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reconcile-creditors Procedure 
PROCEDURE reconcile-creditors :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR diff AS DEC NO-UNDO.

  FOR EACH AccountBalance WHERE AccountBalance.EntityType = "C"
                            AND AccountBalance.MonthCode >= m-1
                            AND AccountBalance.MonthCode <= m-n NO-LOCK:
    RUN add-balance.
  END.

  FOR EACH CtrlAcct BY CtrlAcct.mth BY CtrlAcct.ec BY CtrlAcct.ac:
    IF      CtrlAcct.act-bal <> CtrlAcct.sum-bal
/*         OR CtrlAcct.act-bud <> CtrlAcct.sum-bud
         OR CtrlAcct.act-rbd <> CtrlAcct.sum-rbd */
    THEN RUN account-line( "Creditors control" ).
    DELETE CtrlAcct.
  END.

  IF CAN-FIND( FIRST CtrlTotal WHERE CtrlTotal.act-bal <> CtrlTotal.sum-bal
                                /* OR CtrlTotal.act-bud <> CtrlTotal.sum-bud
                                OR CtrlTotal.act-rbd <> CtrlTotal.sum-rbd */ )
  THEN DO:
    RUN underline-line.
    FOR EACH CtrlTotal BY CtrlTotal.mth BY CtrlTotal.ec:
      IF      CtrlTotal.act-bal <> CtrlTotal.sum-bal
/*           OR CtrlTotal.act-bud <> CtrlTotal.sum-bud
           OR CtrlTotal.act-rbd <> CtrlTotal.sum-rbd */
      THEN RUN total-line( "Creditors control" ).
      DELETE CtrlTotal.
    END.
  END.

  IF not-in-balance > 0 THEN RUN pclrep-line( "", "" ).
  RUN summary-line( "Creditors" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reconcile-debtors Procedure 
PROCEDURE reconcile-debtors :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR diff AS DEC NO-UNDO.

  FOR EACH AccountBalance WHERE AccountBalance.EntityType = "T"
                            AND AccountBalance.MonthCode >= m-1
                            AND AccountBalance.MonthCode <= m-n NO-LOCK:
    RUN add-balance.
  END.

  FOR EACH CtrlAcct BY CtrlAcct.mth BY CtrlAcct.ec BY CtrlAcct.ac:
    IF      CtrlAcct.act-bal <> CtrlAcct.sum-bal
/*         OR CtrlAcct.act-bud <> CtrlAcct.sum-bud */
    THEN RUN account-line( "Sundry debtors" ).
    DELETE CtrlAcct.
  END.

  IF CAN-FIND( FIRST CtrlTotal WHERE CtrlTotal.act-bal <> CtrlTotal.sum-bal
                                /* OR CtrlTotal.act-bud <> CtrlTotal.sum-bud */ )
  THEN DO:
    RUN underline-line.
    FOR EACH CtrlTotal BY CtrlTotal.mth BY CtrlTotal.ec:
      IF      CtrlTotal.act-bal <> CtrlTotal.sum-bal
/*           OR CtrlTotal.act-bud <> CtrlTotal.sum-bud */
      THEN RUN total-line( "Debtors control" ).
      DELETE CtrlTotal.
    END.
  END.

  IF not-in-balance > 0 THEN RUN pclrep-line( "", "" ).
  RUN summary-line( "Tenants" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reconcile-property Procedure 
PROCEDURE reconcile-property :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR diff AS DEC NO-UNDO.

  next-sub-account-balance:
  FOR EACH AccountBalance WHERE AccountBalance.EntityType = "P"
                            AND AccountBalance.MonthCode >= m-1
                            AND AccountBalance.MonthCode <= m-n NO-LOCK:
    IF AccountBalance.Balance = 0 THEN DO:
      FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = AccountBalance.AccountCode NO-LOCK NO-ERROR.
      IF AVAILABLE(ChartOfAccount) THEN DO:
        IF INDEX( ChartOfAccount.UpdateTo, "P") = 0 THEN NEXT.
      END.
    END.
    RUN add-balance.
  END.

  FOR EACH CtrlAcct BY CtrlAcct.mth BY CtrlAcct.ec BY CtrlAcct.ac:
    IF      CtrlAcct.act-bal <> CtrlAcct.sum-bal
/*         OR CtrlAcct.act-bud <> CtrlAcct.sum-bud
         OR CtrlAcct.act-rbd <> CtrlAcct.sum-rbd */
    THEN DO:
      FIND ChartOfAccount WHERE ChartOfAccount.AccountCode = CtrlAcct.ac NO-LOCK NO-ERROR.
      RUN account-line( ChartOfAccount.Name ).
    END.
    DELETE CtrlAcct.
  END.

  IF CAN-FIND( FIRST CtrlTotal WHERE CtrlTotal.act-bal <> CtrlTotal.sum-bal
                                /* OR CtrlTotal.act-bud <> CtrlTotal.sum-bud
                                OR CtrlTotal.act-rbd <> CtrlTotal.sum-rbd */ )
  THEN DO:
    RUN underline-line.
    FOR EACH CtrlTotal BY CtrlTotal.mth BY CtrlTotal.ec:
      IF      CtrlTotal.act-bal <> CtrlTotal.sum-bal
/*           OR CtrlTotal.act-bud <> CtrlTotal.sum-bud
           OR CtrlTotal.act-rbd <> CtrlTotal.sum-rbd */
      THEN DO:
        FIND Company WHERE Company.CompanyCode = CtrlTotal.ec NO-LOCK.
        RUN total-line( Company.LegalName ).
      END.
      DELETE CtrlTotal.
    END.
  END.

  IF not-in-balance > 0 THEN RUN pclrep-line( "", "" ).
  RUN summary-line( "Property Accounts" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE summary-line Procedure 
PROCEDURE summary-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER descr AS CHAR NO-UNDO.

DEF VAR line AS CHAR NO-UNDO.

  line = "Summary - " + descr + ":  "
       + STRING( not-in-balance ) + " unbalanced items of "
       + STRING( items-processed ) + " items processed.".
  RUN pclrep-line( "univers,Point,9,bold,Proportional", line ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE total-line Procedure 
PROCEDURE total-line :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER descr AS CHAR NO-UNDO.

DEF VAR line AS CHAR NO-UNDO.

  FIND Month WHERE Month.MonthCode = CtrlTotal.mth NO-LOCK.
  line = STRING( CtrlTotal.et, "X(2)")
       + (IF CtrlTotal.ec > 0 THEN STRING( CtrlTotal.ec, "99999" ) ELSE "     ") + " "
       + "Total - "
       + STRING( descr, "X(50)") + " "
       + STRING( SUBSTRING( STRING(Month.StartDate,"99/99/9999"), 4), "X(7)") + " "
       + STRING( CtrlTotal.act-bal, money-fmt) + " "
       + STRING( CtrlTotal.sum-bal, money-fmt) + " "
       + STRING( CtrlTotal.act-bal - CtrlTotal.sum-bal, money-fmt) + " "
       + STRING( CtrlTotal.act-bud, money-fmt) + " "
       + STRING( CtrlTotal.sum-bud, money-fmt) + " "
       + STRING( CtrlTotal.act-bud - CtrlTotal.sum-bud, money-fmt) .

  RUN pclrep-line( "Courier,cpi,19,lpi,8,Fixed,Normal", line ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE underline-line Procedure 
PROCEDURE underline-line :
/*------------------------------------------------------------------------------
  Purpose:  Print underlines for the totals
------------------------------------------------------------------------------*/
DEF VAR money-length AS INT NO-UNDO.
DEF VAR line AS CHAR NO-UNDO.

  money-length = LENGTH( STRING(0,money-fmt)).

  line = FILL( " ", 74 ) + FILL( " " + FILL( "-", money-length), 6).
  RUN pclrep-line( "Courier,cpi,19,lpi,8,Fixed,Normal", line ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION verify-account Procedure 
FUNCTION verify-account RETURNS DECIMAL
  ( INPUT et-p AS CHAR, INPUT ec-p AS INT, INPUT et-s AS CHAR, INPUT ac AS DEC, INPUT mnth AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF BUFFER PrimAcct FOR AccountBalance.
DEF BUFFER SubAcct FOR AccountBalance.

DEF VAR primary AS DEC NO-UNDO.
DEF VAR secondary AS DEC NO-UNDO.

  FIND PrimAcct NO-LOCK WHERE PrimAcct.EntityType = et-p
                        AND PrimAcct.EntityCode = ec-p
                        AND PrimAcct.AccountCode = ac
                        AND PrimAcct.MonthCode = mnth NO-ERROR.
  IF AVAILABLE(PrimAcct) THEN
    primary = PrimAcct.Balance.
  ELSE
    primary = 0.

  secondary = 0.
  FOR EACH SubAcct NO-LOCK WHERE SubAcct.EntityType = et-s
                           AND SubAcct.AccountCode = ac
                           AND SubAcct.MonthCode = mnth:
    secondary = secondary + SubAcct.Balance.
  END.

  RETURN primary - secondary.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


