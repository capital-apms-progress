&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/
DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.
&SCOPED-DEFINE trn-per-page 41

DEF VAR from-invoice LIKE Invoice.InvoiceNo NO-UNDO INITIAL 0.
DEF VAR to-invoice   LIKE Invoice.InvoiceNo NO-UNDO INITIAL 0.
DEF VAR invoice-list AS CHAR NO-UNDO    INITIAL "".
DEF VAR copy-only AS LOGICAL NO-UNDO    INITIAL No.
DEF VAR output-to-pdf AS LOGICAL NO-UNDO INITIAL No.
RUN parse-parameters.

/* Report counters */
DEF VAR ln AS DEC INIT 0.00 NO-UNDO.

/* Line definitions */

DEF VAR date-in-words  AS CHAR NO-UNDO.
DEF VAR tenant-address AS CHAR NO-UNDO EXTENT 10.
DEF VAR client-code    AS CHAR NO-UNDO.
DEF VAR entity-type    AS CHAR NO-UNDO.
DEF VAR entity-code    AS INT  NO-UNDO.
DEF VAR office-address AS CHAR NO-UNDO EXTENT 5.
DEF VAR trn-line       AS CHAR NO-UNDO.
DEF VAR trn-no         AS INT  NO-UNDO.
DEF VAR regarding      AS CHAR NO-UNDO.
DEF VAR client-name    AS CHAR NO-UNDO FORMAT "X(80)".

DEF VAR reset-page            AS CHAR NO-UNDO.
DEF VAR half-line             AS CHAR NO-UNDO. half-line = CHR(27) + "=".
DEF VAR time-font             AS CHAR NO-UNDO.
DEF VAR title-font            AS CHAR NO-UNDO.
DEF VAR num-font              AS CHAR NO-UNDO.
DEF VAR prompt-font           AS CHAR NO-UNDO.
DEF VAR tenant-address-font   AS CHAR NO-UNDO.
DEF VAR re-font               AS CHAR NO-UNDO.
DEF VAR to-font               AS CHAR NO-UNDO.
DEF VAR line-printer          AS CHAR NO-UNDO.
DEF VAR credit-font           AS CHAR NO-UNDO.
DEF VAR remittance-title-font AS CHAR NO-UNDO.
DEF VAR remittance-font       AS CHAR NO-UNDO.
DEF VAR remittance-font-bold  AS CHAR NO-UNDO.
DEF VAR remittance-prompt     AS CHAR NO-UNDO.
DEF VAR stamp-font            AS CHAR NO-UNDO.
DEF VAR remit-out-codes       AS CHAR NO-UNDO.

DEF VAR page-no               AS INT NO-UNDO.

DEF VAR logo-codes            AS CHAR NO-UNDO.
DEF VAR copy-no               AS INT  NO-UNDO.
DEF VAR display-percent       AS LOGI NO-UNDO.

DEF VAR gst-applies           AS LOGI NO-UNDO.

DEF VAR time-stamp AS CHAR NO-UNDO.
DEF VAR user-name  AS CHAR NO-UNDO.
{inc/username.i "user-name"}

time-stamp =
  STRING( TODAY, "99/99/9999" ) + " - " + STRING( TIME, "HH:MM:SS" ) + 
    " for " + user-name.

{inc/ofc-this.i}
gst-applies = Office.GST <> ?.

{inc/ofc-set.i "Invoice-terms-routine" "invoice-terms-routine"}
IF NOT AVAILABLE(OfficeSetting) THEN
    invoice-terms-routine = "".

{inc/ofc-set.i "Remittance-account" "remittance-account"}
IF NOT AVAILABLE(OfficeSetting) THEN
    remittance-account = "".

{inc/ofc-set.i "GST-Number" "gst-number"}
IF NOT AVAILABLE(OfficeSetting) THEN
  gst-number = REPLACE( STRING( Office.GSTNo, ">99,999,999"),",","-") .
{inc/ofc-set.i "GST-Number-Name" "gst-number-name"}
IF NOT AVAILABLE(OfficeSetting) THEN
  gst-number-name = "GST NO.".
 
/* Check Office settings for the use of Due-Date on Invoice 'late charge message' */ 
 {inc/ofc-set.i "LateChargeBasis"  "late-charge-basis"}

{inc/ofc-set-l.i "GST-Multi-Company" "gst-multi-company"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-pdf-file-name) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pdf-file-name Procedure 
FUNCTION pdf-file-name RETURNS CHARACTER
(
    from-num AS CHAR,
    to-num AS CHAR,
    num-list AS CHAR
)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 23.4
         WIDTH              = 77.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}
{inc/method/m-hpgl.i}
{inc/entity.i}
{inc/convert.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */


DEF VAR preview AS LOGI INIT No NO-UNDO.

IF output-to-pdf THEN DO:
  /* Procedure pdf-to modifies txtrep-print-file */
  RUN txtrep-pdf-filename( pdf-file-name( STRING( from-invoice ), STRING( to-invoice ), invoice-list) ).
  RUN txtrep-output-mode("pdf").
  RUN hpgl-output-mode("pdf").
END.

IF NOT output-to-pdf THEN
    OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

RUN tax-invoices.

IF NOT output-to-pdf THEN DO:
    OUTPUT CLOSE.
    RUN view-output-file ( preview ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-carriage-return) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE carriage-return Procedure 
PROCEDURE carriage-return :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:     
------------------------------------------------------------------------------*/

PUT CONTROL CHR(13).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-invoice) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-invoice Procedure 
PROCEDURE each-invoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR count-from AS INTEGER NO-UNDO INIT 1.
  DEF VAR count-to AS INTEGER NO-UNDO INIT 2.

  IF copy-only THEN
    count-from = 2.

  /* If PDF then do not print the COPY watermarked page */
  IF output-to-pdf THEN
    ASSIGN
      count-from = 1
      count-to = 1.

  DO copy-no = count-from TO count-to:
  
    page-no = 1.
    RUN get-tenant-details.
    
    display-percent = No.
    FOR EACH InvoiceLine OF Invoice NO-LOCK:
      display-percent = display-percent OR InvoiceLine.Percent <> 100.00.
    END.
    
    RUN page-header.
  
    FOR EACH InvoiceLine OF Invoice NO-LOCK:
      RUN each-line.  
    END.

    RUN total-line.    
    RUN page-footer.
    RUN page-feed.
    
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-each-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-line Procedure 
PROCEDURE each-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN next-line.

  PUT UNFORMATTED
    SPACE(9)
    STRING( InvoiceLine.AccountText, "X(50)" )     SPACE(2)
    STRING( InvoiceLine.Amount, ">,>>>,>>9.99CR" ) SPACE(7)
    STRING( IF display-percent THEN STRING( InvoiceLine.Percent, ">>9.99" )
                               ELSE "", "X(6)" )        SPACE(4)
    STRING( InvoiceLine.YourShare, ">,>>>,>>9.99CR" ).
  RUN skip-line(1).
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-client-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-client-address Procedure 
PROCEDURE get-client-address :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER client-code AS CHAR NO-UNDO.

DEF VAR i AS INT NO-UNDO.
DEF VAR client-address AS CHAR NO-UNDO INITIAL "".

  FIND Client WHERE Client.ClientCode = client-code NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Client) THEN RETURN.

  IF Client.Name <> Client.LegalName AND Client.LegalName <> ? AND Client.LegalName <> "" THEN
    client-name = Client.LegalName.
  ELSE
    client-name = Client.Name.

  client-address = TRIM( REPLACE(Client.RemittanceAddress, CHR(13), "") ).
  IF client-address = ? OR client-address = "" THEN RETURN.

  DO i = 1 TO 5:
    IF i <= NUM-ENTRIES( client-address, "~n" ) THEN
      ASSIGN office-address[i] = ENTRY( i, client-address, "~n" ) NO-ERROR.
    ELSE
      office-address[i] = "".

    IF office-address[i] = ? THEN office-address[i] = "".
  END.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-control-strings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-control-strings Procedure 
PROCEDURE get-control-strings :
/*------------------------------------------------------------------------------
  Purpose:     Get all control strings for this report
------------------------------------------------------------------------------*/

  DEF VAR rows AS DEC NO-UNDO.
  DEF VAR cols AS DEC NO-UNDO.
  
  RUN make-control-string( "PCL", "reset,simplex,portrait,a4,tm,0,lm,6",
    OUTPUT reset-page, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,20,Bold,Proportional",
                  OUTPUT title-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Times,Point,6,Normal,Proportional",
                  OUTPUT time-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,10,Proportional,Normal",
                  OUTPUT prompt-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Courier,Fixed,cpi,12,Bold",
                  OUTPUT num-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,10,Proportional,Normal",
                  OUTPUT tenant-address-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,11,Proportional,Bold",
                  OUTPUT re-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,8,Proportional,Normal",
                  OUTPUT to-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "LinePrinter,lpi,9.54",
                  OUTPUT line-printer, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string( "PCL", "Helvetica,Point,14,Proportional,Bold",
                  OUTPUT credit-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string ( "PCL", "Proportional,Times,Bold,Point,16",
                  OUTPUT remittance-title-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string ( "PCL", "Times,Proportional,point,9,normal,lpi,7",
                  OUTPUT remittance-font, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string ( "PCL", "Times,Proportional,point,8,normal,lpi,7",
                  OUTPUT remittance-prompt, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string ( "PCL", "Times,Proportional,point,10,bold",
                  OUTPUT remittance-font-bold, OUTPUT rows, OUTPUT cols ).

  RUN make-control-string ( "PCL", "Times,Proportional,Point,4,Normal",
                  OUTPUT stamp-font, OUTPUT rows, OUTPUT cols ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-office-details) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-office-details Procedure 
PROCEDURE get-office-details :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND Office WHERE Office.ThisOffice NO-LOCK.
  
  DEF VAR i AS INT NO-UNDO.
     
  DO i = 1 TO 5:
    ASSIGN office-address[i] = "".
    ASSIGN 
      office-address[i] = ENTRY( i, Office.StreetAddress, CHR(10) ) NO-ERROR.
    IF office-address[i] = ? THEN office-address[i] = "".
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-tenant-details) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-tenant-details Procedure 
PROCEDURE get-tenant-details :
/*------------------------------------------------------------------------------
  Purpose:  
------------------------------------------------------------------------------*/
  
  DEF VAR i         AS INT  NO-UNDO.
  DEF VAR addr      AS CHAR NO-UNDO.
  DEF VAR company-code AS INT NO-UNDO.

  FIND Tenant WHERE Tenant.TenantCode = Invoice.EntityCode NO-LOCK.
  
  regarding = "".
  IF Tenant.EntityType = 'P' THEN
  DO:
    /* Note: find fails if lease is not unique - this is deliberate! */
    FIND TenancyLease WHERE TenancyLease.TenantCode = Tenant.TenantCode NO-LOCK NO-ERROR.
    IF AVAILABLE(TenancyLease) THEN DO:
      regarding = TenancyLease.AreaDescription.
      IF regarding = ? THEN regarding = "".
    END.

    FIND FIRST Property WHERE Property.PropertyCode = Tenant.EntityCode
      NO-LOCK NO-ERROR.
    IF AVAILABLE Property THEN DO:
      regarding = (IF regarding <> "" THEN TRIM(regarding) + ", " ELSE "")
                + Property.Name
                + (IF Property.Name <> Property.StreetAddress THEN ", " + Property.StreetAddress ELSE "" ).
    END.
  END.
  ELSE IF Tenant.EntityType = 'L' THEN
  DO:
    FIND FIRST Company WHERE Company.CompanyCode = Tenant.EntityCode
      NO-LOCK NO-ERROR.
    IF AVAILABLE Company THEN regarding = Company.LegalName.
  END.

  entity-type = Tenant.EntityType.
  entity-code = Tenant.EntityCode.
  client-code = get-entity-client( entity-type, entity-code ).
  RUN get-client-address( client-code ).
  
  IF gst-multi-company THEN DO:
    company-code = get-entity-ledger( Tenant.EntityType, Tenant.EntityCode ).
    FIND FIRST Company WHERE Company.CompanyCode = company-code NO-LOCK NO-ERROR.
    IF AVAILABLE(Company) THEN DO:
      IF Company.OperationalCountry = 'AUS' THEN
        gst-number = Company.BusinessNo.
      ELSE
        gst-number = Company.TaxNo. 
    END.
  END.

  /* If the user has entered Attn text, send it to be constructed into the address */
  IF AVAILABLE Invoice AND Invoice.AttnTo <> "" AND Invoice.AttnTo <> ? THEN
    RUN process/getaddr.p( "T," + Invoice.AttnTo, Tenant.TenantCode, "BILL", OUTPUT addr ).
  ELSE
    RUN process/getaddr.p( "T", Tenant.TenantCode, "BILL", OUTPUT addr ).

  DO i = 1 TO 10: tenant-address[i] = "". END.
  DO i = 1 TO NUM-ENTRIES( addr, "~n" ):
    tenant-address[i] = ENTRY( i, addr, "~n" ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-next-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE next-line Procedure 
PROCEDURE next-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  trn-no = trn-no + 1.

  IF trn-no >= {&trn-per-page} THEN
  DO:
    RUN page-footer.
    RUN page-feed.
    RUN page-header.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-page-feed) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE page-feed Procedure 
PROCEDURE page-feed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  PUT CONTROL CHR(12).
  page-no = page-no + 1.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-page-footer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE page-footer Procedure 
PROCEDURE page-footer :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR amount-due AS CHAR NO-UNDO.

  IF Invoice.Total < 0 THEN
  DO:
    PUT CONTROL line-printer.
    RUN pcl-moveto( 90, 11 ). RUN carriage-return.
    PUT CONTROL credit-font.
    PUT UNFORMATTED "This is a credit note. Please do not pay.".
  END.
  ELSE
  DO:
    DEF VAR n-days AS INT NO-UNDO.
    /*
    If the settings table has an invoice-terms-routine defined then execute
    that to print the terms.
    */
    IF invoice-terms-routine <> "" THEN DO:
      /* Generate HPGL for the E. & O. E. */
      RUN hpgl-initialize.
      IF output-to-pdf THEN
          RUN VALUE(invoice-terms-routine) ( Invoice.TermsCode, remittance-account, "pdf" ).
      ELSE
          RUN VALUE(invoice-terms-routine) ( Invoice.TermsCode, remittance-account, "hpgl" ).
      RUN hpgl-append( RETURN-VALUE ).
      RUN hpgl-get-codes( YES, YES, OUTPUT remit-out-codes ).
      PUT CONTROL remit-out-codes.

      PUT CONTROL line-printer.
      RUN pcl-moveto( 90, 11 ). RUN carriage-return.
      RUN skip-line(2).
    END.
    ELSE DO:
      PUT CONTROL line-printer.
      RUN pcl-moveto( 90, 11 ). RUN carriage-return.
    
      PUT CONTROL prompt-font.
      PUT UNFORMATTED "E. & O.E. This account is now due.".
      RUN skip-line(2).
      ASSIGN n-days = INT( Invoice.TermsCode ) NO-ERROR.
      IF n-days > 0 THEN 
        PUT UNFORMATTED "Interest may be charged on payments received after " +
          IF late-charge-basis = 'DueDate' AND Invoice.DueDate <> ? 
          THEN STRING(Invoice.DueDate, "99/99/9999") + "." 
          ELSE STRING( Invoice.InvoiceDate
                       + (IF Invoice.TermsCode = "M" THEN 29 ELSE n-days), "99/99/9999" )
                       + ".".
    END.
  END.
      
  PUT CONTROL line-printer.
  RUN skip-line( 1 ).

  IF copy-no = 2 THEN DO:
    PUT CONTROL stamp-font.
    PUT UNFORMATTED time-stamp.
  END.
  
  PUT CONTROL line-printer.
  RUN skip-line( 0.5 ).

  PUT UNFORMATTED FILL( "_ ", 60 ).
  RUN skip-line(2.5).
  PUT CONTROL remittance-title-font.
  PUT UNFORMATTED "Remittance Advice".
  PUT CONTROL remittance-font.
  
  RUN skip-line(1.5).
  PUT UNFORMATTED "Please return this portion with your cheque, payable to:".
  RUN skip-line(1.5).

  PUT CONTROL remittance-font-bold client-name remittance-font.
  RUN skip-line(1).

  DEF VAR i AS INT NO-UNDO.
  DO i = 1 TO 5:
    PUT UNFORMATTED office-address[i]. RUN skip-line(1).
  END.
  
  DEF VAR remittance-codes AS CHAR NO-UNDO.
  amount-due = TRIM( STRING( Invoice.Total + Invoice.TaxAmount, ">,>>>,>>>,>>9.99CR" ) ).

  RUN hpgl-initialize.
  RUN hpgl-moveto( 110, 50 ).
  RUN hpgl-text( remittance-prompt, "Our Ref:" ).
  RUN hpgl-move-relative( 20, 0 ).
  RUN hpgl-text( remittance-font, "T" + STRING( Tenant.TenantCode, "99999" ) + "/" + STRING( Invoice.InvoiceNo ) ).
  RUN hpgl-move-relative( 25, 0 ).
  RUN hpgl-text( remittance-prompt, "Date:" ).
  RUN hpgl-move-relative( 10, 0 ).
  RUN hpgl-text( remittance-font, STRING( Invoice.InvoiceDate, "99/99/9999" ) ).
  RUN hpgl-move-relative( -55, -18 ).
  RUN hpgl-text( remittance-prompt, "From:" ).
  RUN hpgl-move-relative( 20, 0 ).
  RUN hpgl-text( remittance-font, Tenant.Name ).
  RUN hpgl-move-relative( -20, -4 ).
  RUN hpgl-text( remittance-prompt, "Re:" ).
  RUN hpgl-move-relative( 20, 0 ).
  RUN hpgl-text( remittance-font, regarding ).
  RUN hpgl-move-relative( -20, -4 ).
  RUN hpgl-text( remittance-prompt, "Amount Due:" ).
  RUN hpgl-move-relative( 20, 0 ).
  RUN hpgl-text( remittance-font, amount-due ).
  RUN hpgl-move-relative( -20, -6 ).
  RUN hpgl-text( remittance-prompt, "Amount Paid:" ).

  RUN hpgl-get-codes( yes, no, OUTPUT remittance-codes ).
  PUT CONTROL remittance-codes.  

  IF copy-no = 2 THEN RUN print-copy-sign.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-page-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE page-header Procedure 
PROCEDURE page-header :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  RUN reset-page.
  RUN print-title.
  RUN print-address.
  RUN print-header.
  
  PUT CONTROL re-font.

  IF gst-multi-company THEN DO:
    PUT UNFORMATTED "SUPPLIER:".
    RUN carriage-return.
    PUT CONTROL line-printer.
    PUT UNFORMATTED SPACE(14).
    PUT CONTROL re-font.
    PUT UNFORMATTED STRING( client-name, "X(80)" ).
    RUN carriage-return.
    RUN skip-line(2).
    PUT UNFORMATTED "ABN:".
    RUN carriage-return.
    PUT UNFORMATTED SPACE(17).
    PUT UNFORMATTED STRING( gst-number, "X(80)" ).
    RUN carriage-return.
    RUN skip-line(2).
    PUT UNFORMATTED "RE:".
    RUN carriage-return.
    PUT UNFORMATTED SPACE(17).
    PUT UNFORMATTED STRING( regarding, "X(80)" ).
  END.
  ELSE DO:
    PUT UNFORMATTED "RE:".
    RUN carriage-return.
    PUT CONTROL line-printer.
    PUT UNFORMATTED SPACE(11).
    PUT CONTROL re-font.
    PUT UNFORMATTED STRING( regarding, "X(80)" ).
  END.

  RUN skip-line(3).

/*
  PUT CONTROL to-font.
  PUT UNFORMATTED "TO:".
  RUN carriage-return.
  PUT CONTROL line-printer.
  PUT UNFORMATTED SPACE(9).
  PUT CONTROL to-font.
  PUT UNFORMATTED STRING( Invoice.ToDetail, "X(100)" ).
  RUN skip-line(2).
*/

  RUN print-invoice-header.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR i           AS INT NO-UNDO.
DEF VAR token       AS CHAR NO-UNDO.

{inc/showopts.i "report-options"}

  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).
    CASE( ENTRY( 1, token ) ):
      WHEN "OutputPDF" THEN output-to-pdf = YES.
      WHEN "InvoiceRange" THEN ASSIGN
        from-invoice = INT( ENTRY( 2, token ) )
        to-invoice = INT( ENTRY( 3, token ) ).
      WHEN "InvoiceList" THEN ASSIGN
        invoice-list = SUBSTRING( token, INDEX( token, ",") + 1).
      WHEN "CopyOnly" THEN copy-only = Yes.
    END CASE.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-address) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-address Procedure 
PROCEDURE print-address :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEF VAR i AS INT NO-UNDO.
DEF VAR invoice-date AS CHAR NO-UNDO.

  RUN skip-line(6).
  PUT CONTROL tenant-address-font.
  
  PUT UNFORMATTED STRING( tenant-address[1], "X(50)" ).
  RUN carriage-return.
  PUT UNFORMATTED SPACE( 100 ) (IF gst-applies THEN "Tax " ELSE "") + "Invoice No:".
  RUN carriage-return.
  PUT CONTROL num-font.
  PUT UNFORMATTED SPACE( 78 ) STRING( Invoice.InvoiceNo, ">>>>9" ).
  PUT CONTROL tenant-address-font.
  RUN skip-line(2).
  
  PUT UNFORMATTED STRING( tenant-address[2], "X(50)" ). RUN carriage-return.
  IF gst-applies THEN
    PUT UNFORMATTED SPACE( 100 ) gst-number-name.
  RUN carriage-return.

  PUT CONTROL num-font.
  IF gst-applies THEN
    PUT UNFORMATTED SPACE( 83 - LENGTH(gst-number)) gst-number.

  PUT CONTROL tenant-address-font.
  RUN skip-line(2).

  PUT UNFORMATTED STRING( tenant-address[3], "X(50)" ).
  RUN skip-line(2).

  PUT UNFORMATTED STRING( tenant-address[4], "X(50)" ). RUN carriage-return.

  RUN date-to-word( Invoice.InvoiceDate, OUTPUT invoice-date ).
  PUT UNFORMATTED SPACE(118) STRING( invoice-date, "X(20)" ). RUN carriage-return.
  RUN skip-line(2).

  PUT UNFORMATTED STRING( tenant-address[5], "X(50)" ). RUN skip-line(2).
  PUT UNFORMATTED STRING( tenant-address[6], "X(50)" ). RUN skip-line(2).
  PUT UNFORMATTED STRING( tenant-address[7], "X(50)" ). RUN skip-line(1).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-copy-sign) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-copy-sign Procedure 
PROCEDURE print-copy-sign :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR copy-codes AS CHAR NO-UNDO.
  
  RUN hpgl-initialize.
  RUN hpgl-copywatermark.
  RUN hpgl-get-codes( yes, no, OUTPUT copy-codes ).
  PUT CONTROL copy-codes.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-header Procedure 
PROCEDURE print-header :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN skip-line( 2 ).
  RUN pcl-move-relative( 0, 50 ).
  PUT CONTROL title-font.
  PUT UNFORMATTED STRING( IF Invoice.Total < 0 THEN
    "CREDIT NOTE" ELSE IF gst-applies THEN "TAX INVOICE" ELSE "     INVOICE", "X(25)" ).
  RUN carriage-return.
  PUT CONTROL tenant-address-font.
  PUT UNFORMATTED SPACE(138) STRING( "Page " + STRING( page-no ) ).
  PUT CONTROL line-printer.
  RUN skip-line(1).
  PUT CONTROL title-font.
  PUT UNFORMATTED FILL( "__", 60 ).
  RUN skip-line(5).
  PUT CONTROL line-printer.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-invoice-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-invoice-header Procedure 
PROCEDURE print-invoice-header :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR blrb AS CHAR NO-UNDO.

  PUT CONTROL line-printer.

  blrb = WRAP((IF Invoice.Blurb = ? OR TRIM(Invoice.Blurb) = ""
                             THEN Invoice.ToDetail ELSE Invoice.Blurb), 104).

  IF page-no = 1 THEN DO.  
    DEF VAR i AS INT NO-UNDO.
    DO i = 1 TO NUM-ENTRIES( blrb, "~n" ):
      RUN next-line.
      PUT UNFORMATTED (IF i > 1 THEN FILL(" ",9) ELSE "TO:      ")
        ENTRY( i, blrb, "~n" ).
      RUN skip-line(1).
    END.
  END.

  trn-no = trn-no + 3.
  RUN next-line.  
  RUN skip-line(2).
  PUT UNFORMATTED SPACE(9)
    STRING( "Description", "X(50)" ) SPACE(2)
    STRING( "      Amount  ", "X(14)" ) SPACE(6)
    STRING( IF display-percent THEN "Percent" ELSE "", "X(7)" ) SPACE(4)
    STRING( "       Share  ", "X(14)" ).
  RUN skip-line(2).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-title) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-title Procedure 
PROCEDURE print-title :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  RUN hpgl-initialize.
  RUN client-logo( entity-type, entity-code, client-code ).
  RUN client-address( entity-type, entity-code, client-code ).
  RUN hpgl-get-codes( yes, YES, OUTPUT logo-codes ).
  PUT CONTROL logo-codes.

  PUT CONTROL line-printer.
/*  RUN skip-line(0.5). */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reset-page) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reset-page Procedure 
PROCEDURE reset-page :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  PUT CONTROL reset-page.
  trn-no = 0.
  ln = 0.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-skip-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE skip-line Procedure 
PROCEDURE skip-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER n AS DEC NO-UNDO.

  DEF VAR int-part AS INT NO-UNDO.
  DEF VAR dec-part AS DEC NO-UNDO.
  
  int-part = TRUNCATE( n, 0 ).
  IF int-part < 0 THEN RETURN.
  dec-part = n - int-part.
  IF int-part = 0 AND dec-part = 0 THEN RETURN.

  /* Need to have this like the following - do not touch */  
  IF int-part <> 0 THEN PUT CONTROL FILL( CHR(10), int-part ).
  IF dec-part <> 0 THEN PUT CONTROL half-line.
    
  ln = ln + n.

  RUN carriage-return.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-skip-to-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE skip-to-line Procedure 
PROCEDURE skip-to-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF INPUT PARAMETER line-no AS DEC NO-UNDO.

  DEF VAR int-part AS INT NO-UNDO.
  DEF VAR dec-part AS DEC NO-UNDO.
  
  int-part = TRUNCATE( line-no - ln, 0 ).
  IF int-part < 0 THEN RETURN.
  dec-part = ( line-no - ln ) - int-part.
  IF int-part = 0 AND dec-part = 0 THEN RETURN.
  
  IF int-part <> 0 THEN PUT CONTROL FILL( CHR(10), int-part ).
  IF dec-part <> 0 THEN PUT CONTROL half-line.
    
  ln = line-no.

  RUN carriage-return.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-tax-invoices) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE tax-invoices Procedure 
PROCEDURE tax-invoices :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEF VAR i AS INT NO-UNDO.
  
  RUN get-office-details.
  RUN get-control-strings.
  RUN date-to-word( TODAY, OUTPUT date-in-words ).

  IF invoice-list <> ? THEN
  DO i = 1 TO NUM-ENTRIES( invoice-list ):
    FIND Invoice WHERE
      Invoice.InvoiceStatus = "A" AND
      Invoice.InvoiceNo = INT( ENTRY( i, invoice-list ) )
      NO-LOCK NO-ERROR.
    IF AVAILABLE Invoice THEN DO:

      /* If outputting to PDF the do individual files for each invoice */
      IF output-to-pdf THEN DO:
        RUN txtrep-pdf-filename( 'Invoice-' + ENTRY( i, invoice-list ) ).
        OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.
      END.

      RUN each-invoice.

      /* If PDF mode then output will be opened again for the next file */
      IF output-to-pdf THEN
        OUTPUT CLOSE.
    END.
  END.
  
  IF from-invoice <> ? AND to-invoice <> ? THEN
  FOR EACH Invoice NO-LOCK WHERE
    Invoice.InvoiceStatus = "A" AND
    Invoice.InvoiceNo >= from-invoice AND
    Invoice.InvoiceNo <= to-invoice:

    IF output-to-pdf THEN DO:
      RUN txtrep-pdf-filename( 'Invoice-' + STRING( Invoice.InvoiceNo ) ).
      OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.
    END.

    RUN each-invoice.

    IF output-to-pdf THEN
      OUTPUT CLOSE.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-total-line) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE total-line Procedure 
PROCEDURE total-line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  trn-no = trn-no + IF gst-applies THEN 4 ELSE 1.
  RUN next-line.
  
  RUN skip-line(1).
  
  IF gst-applies THEN
  DO:
    PUT UNFORMATTED SPACE(92) FILL( '-', 12 ). RUN skip-line(1).
    PUT UNFORMATTED SPACE(9)
      STRING( "Your total share", "X(50)" ) SPACE(33)
      STRING( Invoice.Total, ">,>>>,>>9.99CR" ). RUN skip-line(1).
    PUT UNFORMATTED SPACE(9)
      STRING( "Plus GST", "X(50)" ) SPACE(33)
      STRING( Invoice.TaxAmount, ">,>>>,>>9.99CR" ). RUN skip-line(1).
  END.
  
  PUT UNFORMATTED SPACE(92) FILL( '-', 12 ).  RUN skip-line(1).
  PUT UNFORMATTED SPACE(9)
    STRING( "Amount now due", "X(50)" ) SPACE(33)
    STRING( Invoice.Total + Invoice.TaxAmount, ">,>>>,>>9.99CR" ). RUN skip-line(1).
  PUT UNFORMATTED SPACE(92) FILL( '=', 12 ).  RUN skip-line(1).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-pdf-file-name) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pdf-file-name Procedure 
FUNCTION pdf-file-name RETURNS CHARACTER
(
    from-num AS CHAR,
    to-num AS CHAR,
    num-list AS CHAR
) :
/*------------------------------------------------------------------------------
  Purpose: Construct the pdf filename based on what invoices are specified 
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR pdf-file-name AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.

IF invoice-list <> ? THEN
    DO i = 1 TO NUM-ENTRIES( num-list ):

    IF pdf-file-name <> "" THEN
        pdf-file-name = pdf-file-name + '-'.
    ELSE
        pdf-file-name = 'Invoice'.
    
    pdf-file-name = pdf-file-name + ENTRY( i, num-list ).
END.
  
IF from-num <> ? AND to-num <> ? THEN DO:
    IF from-num = to-num THEN
        pdf-file-name = 'Invoice-' + from-num.
    ELSE
        pdf-file-name = 'Invoice-' + from-num + '-to-' + to-num.
END.

RETURN pdf-file-name.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

