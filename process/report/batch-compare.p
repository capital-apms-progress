&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/

DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

DEF VAR preview   AS LOGI NO-UNDO INIT No.
DEF VAR posted-batch AS INT NO-UNDO INIT ?.
DEF VAR unposted-batch AS INT NO-UNDO INIT ?.
DEF VAR reverse AS LOGI NO-UNDO INITIAL No.
RUN parse-parameters.


DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
DEF VAR timeStamp AS CHAR FORMAT "X(44)" NO-UNDO.
timeStamp = STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.

DEF VAR line AS CHAR NO-UNDO.

DEF VAR time-font AS CHAR NO-UNDO   INITIAL "helvetica,proportional,point,6,normal".
DEF VAR base-font AS CHAR NO-UNDO  INITIAL "courier,fixed,cpi,16,lpi,9,normal".
DEF VAR header-font AS CHAR NO-UNDO INITIAL "Times,Point,11,lpi,6,Proportional,Normal".


DEFINE TEMP-TABLE MyTran LIKE NewAcctTran
    INDEX XPKMyTran IS PRIMARY EntityType EntityCode AccountCode Amount.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 18.25
         WIDTH              = 51.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}
{inc/date.i}
{inc/null.i}
{inc/convert.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.

RUN pclrep-start( preview, "reset,portrait,tm,2,a4,lm,6,courier,cpi,18,lpi,9").

RUN compare-batches.

OUTPUT CLOSE.

RUN pclrep-finish.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-compare-batches) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE compare-batches Procedure 
PROCEDURE compare-batches :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  RUN load-posted-batch.
  RUN remove-unposted-equal.
  RUN print-differences.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-inst-page-footer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-footer Procedure 
PROCEDURE inst-page-footer :
/*------------------------------------------------------------------------------
  Purpose:  Print any page footer
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-inst-page-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-header Procedure 
PROCEDURE inst-page-header :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/

  line = timeStamp.
  line = line + CHR(13) + SPC(275) + "Page: " + STRING( pclrep-page-number ).
  RUN pclrep-line( time-font, line).
  
  RUN pclrep-down-by(2).
  RUN pclrep-line( header-font, SPC(35) + "Comparing posted batch " + STRING(posted-batch) + " with unposted batch " + STRING(unposted-batch) ).
  RUN pclrep-down-by(1).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-load-posted-batch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE load-posted-batch Procedure 
PROCEDURE load-posted-batch :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FOR EACH AcctTran WHERE BatchCode = posted-batch,
                  FIRST Document OF AcctTran NO-LOCK:
    IF AcctTran.ConsequenceOf > 0 THEN NEXT.
    IF Document.DocumentType = 'INTC' THEN NEXT.
    CREATE MyTran.
    BUFFER-COPY AcctTran TO MyTran.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
{inc/showopts.i "report-options"}

  DEF VAR token AS CHAR NO-UNDO.
  DEF VAR i AS INT NO-UNDO.
  
  DO i = 1 TO NUM-ENTRIES( report-options, "~n" ):
    token = ENTRY( i, report-options, "~n" ).
    
    CASE ENTRY( 1, token ):
      WHEN "Preview" THEN               preview = Yes.
      WHEN "Reverse" THEN               reverse = Yes.
      WHEN "PostedBatch" THEN           posted-batch = INT( ENTRY(2,token ) ).
      WHEN "UnpostedBatch" THEN         unposted-batch = INT( ENTRY(2,token ) ).
    END CASE.
    
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-differences) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-differences Procedure 
PROCEDURE print-differences :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FOR EACH MyTran BY EntityType BY EntityCode BY AccountCode BY Amount:
    line = MyTran.EntityType + " "
         + STRING( MyTran.EntityCode, "99999") + " "
         + STRING( MyTran.AccountCode, "9999.99") + " "
         + STRING( MyTran.Amount, "->>,>>>,>>9.99" ) + "  "
         + STRING( MyTran.BatchCode, ">>9999").
    RUN pclrep-line( base-font, line ).
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-remove-unposted-equal) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remove-unposted-equal Procedure 
PROCEDURE remove-unposted-equal :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FOR EACH NewAcctTran WHERE BatchCode = unposted-batch NO-LOCK:
    FIND FIRST MyTran WHERE MyTran.EntityType = NewAcctTran.EntityType
                      AND MyTran.EntityCode = NewAcctTran.EntityCode
                      AND MyTran.AccountCode = NewAcctTran.AccountCode
                      AND MyTran.Amount = NewAcctTran.Amount NO-ERROR.
    IF AVAILABLE(MyTran) THEN
      DELETE MyTran.
    ELSE DO:
      CREATE MyTran.
      BUFFER-COPY NewAcctTran TO MyTran.
    END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

