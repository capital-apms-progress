
DEF {1} {2} SHARED VAR old_system  AS CHAR FORMAT 'X(4)'   LABEL 'From'  NO-UNDO.
DEF {1} {2} SHARED VAR file_name   AS CHAR FORMAT 'X(15)'  LABEL 'File name'   NO-UNDO.
DEF {1} {2} SHARED VAR messages    AS CHAR FORMAT 'X(60)' LABEL 'Messages'    NO-UNDO.
DEF {1} {2} SHARED VAR import_type AS CHAR FORMAT 'X(20)'  LABEL 'Import type' NO-UNDO.

DEFINE {1} SHARED FRAME default-frame
            import_type old_system file_name messages
            WITH DOWN FONT 8 SCROLL 16 SIZE 107 BY 200 SCROLLABLE
            USE-TEXT NO-BOX.

