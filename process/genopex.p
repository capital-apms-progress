&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------------------------
    File        : process/genRECV.p
    Purpose     : Generate forecast cash flows for opex recoveries
    Author(s)   : Andrew McMillan
  ------------------------------------------------------------------------*/
DEF INPUT PARAMETER process-options AS CHAR NO-UNDO.
DEF VAR scenario-code LIKE Scenario.ScenarioCode NO-UNDO.
DEF VAR debug-mode AS LOGI NO-UNDO  INITIAL No.
RUN parse-options.

{inc/ofc-this.i}
{inc/ofc-set.i "RentCharge-Type" "rent-charge-type" "ERROR"}
{inc/ofc-set.i "AcctGroup-Opex" "recoverable-groups" "ERROR"}

ON WRITE OF CashFlow DO:
  IF NEW CashFlow THEN DO:
    DEF BUFFER LastCashFlow FOR CashFlow.
    FIND LAST LastCashFlow WHERE
      LastCashFlow.ScenarioCode = CashFlow.ScenarioCode AND
      LastCashFlow.EntityType   = CashFlow.EntityType AND
      LastCashFlow.EntityCode   = CashFlow.EntityCode AND
      LastCashFlow.AccountCode  = CashFlow.AccountCode AND
      LastCashFlow.CashFlowType = CashFlow.CashFlowType
      NO-LOCK NO-ERROR.
    ASSIGN CashFlow.Sequence = IF AVAILABLE LastCashFlow THEN
      LastCashFlow.Sequence + 1 ELSE 1.
  END.
END.

/* Selection variables */
DEF VAR entity-list  AS CHAR NO-UNDO.
DEF VAR entity-list-type AS CHAR NO-UNDO.
DEF VAR entity-list-by AS CHAR NO-UNDO.
DEF VAR all-entities AS LOGI NO-UNDO.

DEF VAR service-types  AS CHAR NO-UNDO.
DEF VAR forecast-start AS DATE NO-UNDO.
DEF VAR forecast-end   AS DATE NO-UNDO.
DEF VAR washup-cycle   AS DATE NO-UNDO.

/* parameters affecting generation of RECV */
DEF VAR profiled AS LOGI NO-UNDO    INITIAL No.
DEF VAR recovered-offset AS DEC NO-UNDO    INITIAL 0.2 .

DEF TEMP-TABLE PX NO-UNDO
        FIELD ac LIKE ChartOfAccount.AccountCode
        FIELD mc LIKE Month.MonthCode
        FIELD amt AS DEC
        FIELD d-start AS DATE
        FIELD d-end AS DATE
        INDEX x1 IS UNIQUE PRIMARY ac mc.

DEF WORK-TABLE RX NO-UNDO
        FIELD mc LIKE Month.MonthCode
        FIELD ac LIKE ChartOfAccount.AccountCode
        FIELD amt AS DEC
        FIELD d-start AS DATE
        FIELD d-end AS DATE
        FIELD percent AS DEC
        FIELD r-desc AS CHAR
        FIELD relate AS CHAR.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD check-manual-flow Procedure 
FUNCTION check-manual-flow RETURNS LOGICAL
  ( INPUT man-type AS CHAR, INPUT chg-type AS CHAR, INPUT-OUTPUT d-1 AS DATE, INPUT-OUTPUT d-n AS DATE  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-parameter Procedure 
FUNCTION get-parameter RETURNS CHARACTER
  ( INPUT parameter-name AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD id-code Procedure 
FUNCTION id-code RETURNS CHARACTER
  ( INPUT type AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD include-property Procedure 
FUNCTION include-property RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD is-recoverable Procedure 
FUNCTION is-recoverable RETURNS LOGICAL
  ( INPUT ac AS DEC )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .33
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-debug.i}
{inc/cashflow.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
&SCOP KEEP-DEBUG-MESSAGES KEEP-MESSAGES
{&DEBUG-BEGIN}

FIND Scenario WHERE Scenario.ScenarioCode = scenario-code NO-LOCK.

debug-event("Starting").
RUN get-parameters.
RUN delete-current-cash-flows.
RUN generate-recoveries.

debug-event("Finished").
RUN debug-end.

MESSAGE "Forecast Recovery Regeneration Complete" VIEW-AS ALERT-BOX INFORMATION
            TITLE "Completed".

{&DEBUG-END}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-expense-table Procedure 
PROCEDURE build-expense-table :
/*------------------------------------------------------------------------------
  Purpose:  Build the PX table of expenses for this property
------------------------------------------------------------------------------*/
  /* clear any existing records */
  FOR EACH PX: DELETE PX. END.

DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.
DEF VAR yy AS INT NO-UNDO.
DEF VAR mm AS INT NO-UNDO.

DEF VAR hdrs AS CHAR NO-UNDO.
DEF VAR flows AS CHAR NO-UNDO.
DEF VAR hdr AS CHAR NO-UNDO.
DEF VAR n-mths-per AS INT NO-UNDO.
DEF VAR flow-period-start AS DATE NO-UNDO.

  /* Clear previous ones */
  debug-event( "Clearing expense table" ).
  FOR EACH PX: DELETE PX. END.
  debug-event( "Building expense table" ).

  FOR EACH CashFlow OF Scenario WHERE CashFlow.EntityType = "P"
                                AND CashFlow.EntityCode = Property.PropertyCode:
    IF LOOKUP( CashFlow.CashFlowType, "RENT,MRNT,RECV,MREC") > 0 THEN NEXT.
    IF NOT is-recoverable(CashFlow.AccountCode) THEN NEXT.

    n-mths-per = get-freq-months( CashFlow.FrequencyCode ).
    IF n-mths-per > 1 THEN DO:
      /* this ensures that we have a blank cashflow to cover the whole period */
      flow-period-start = add-months( start-of-bucket( ENTRY(1,hdrs)), 1 - n-mths-per ).
      IF flow-period-start < forecast-start THEN flow-period-start = forecast-start.
      FIND Month WHERE Month.StartDate = flow-period-start NO-LOCK NO-ERROR.
      IF AVAILABLE(Month) THEN DO:
        FIND FIRST PX WHERE PX.ac = CashFlow.AccountCode
                              AND PX.mc = Month.MonthCode NO-ERROR.
        IF NOT AVAILABLE(PX) THEN DO:
          CREATE PX.
          PX.ac  = CashFlow.AccountCode.
          PX.mc  = Month.MonthCode.
          PX.amt = 0.
          PX.d-start = Month.StartDate.
          PX.d-end = Month.EndDate.
        END.
      END.
    END.

    IF n-mths-per > 1 AND NOT(profiled) THEN
      RUN split-cash-flow( forecast-start, forecast-end, "MNTH",
            CashFlow.StartDate, CashFlow.EndDate, "MNTH",
            (CashFlow.Amount / n-mths-per), No, OUTPUT hdrs, OUTPUT flows ).
    ELSE
      RUN split-cash-flow( forecast-start, forecast-end, "MNTH",
            CashFlow.StartDate, CashFlow.EndDate, CashFlow.FrequencyCode,
            CashFlow.Amount, No, OUTPUT hdrs, OUTPUT flows ).

    n = NUM-ENTRIES(hdrs).
    DO i = 1 TO n:
      hdr = ENTRY(i,hdrs).
      FIND Month WHERE Month.StartDate = start-of-bucket(hdr) NO-LOCK NO-ERROR.
      IF NOT AVAILABLE(Month) THEN DO:
        debug-event( "Can't find month record for '" + hdr + "'").
        NEXT.
      END.

      FIND FIRST PX WHERE PX.ac = CashFlow.AccountCode
                            AND PX.mc = Month.MonthCode NO-ERROR.
      IF NOT AVAILABLE(PX) THEN DO:
        CREATE PX.
        PX.ac  = CashFlow.AccountCode.
        PX.mc  = Month.MonthCode.
        PX.amt = 0.
        PX.d-start = Month.StartDate.
        PX.d-end = Month.EndDate.
      END.
      PX.amt = PX.amt + DEC(ENTRY(i,flows)).
    END.
  END.

DEF VAR ths-ac AS DEC NO-UNDO.
DEF VAR first-mc AS INT NO-UNDO.
DEF VAR last-mc AS INT NO-UNDO.

  FIND LAST Month WHERE Month.StartDate <= forecast-start NO-LOCK.
  first-mc = Month.MonthCode.
  FIND LAST Month WHERE Month.StartDate < forecast-end NO-LOCK.
  last-mc = Month.MonthCode.

  FIND FIRST PX NO-ERROR.
  DO WHILE AVAILABLE(PX):
    ths-ac = PX.ac.
    FOR EACH Month WHERE Month.MonthCode >= PX.mc AND Month.MonthCode <= last-mc NO-LOCK:
      IF NOT CAN-FIND(PX WHERE PX.ac = ths-ac AND PX.mc = Month.MonthCode) THEN DO:
        CREATE PX.
        PX.ac  = ths-ac.
        PX.mc  = Month.MonthCode.
        PX.amt = 0.
        PX.d-start = Month.StartDate.
        PX.d-end = Month.EndDate.
      END.
    END.
    FIND FIRST PX WHERE PX.ac > ths-ac NO-ERROR.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clear-rx-records Procedure 
PROCEDURE clear-rx-records :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
  FOR EACH RX: DELETE RX. END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE create-flow Procedure 
PROCEDURE create-flow :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER cf-type AS CHAR NO-UNDO.
DEF INPUT PARAMETER chg-type AS CHAR NO-UNDO.
DEF INPUT PARAMETER account AS DEC NO-UNDO.
DEF INPUT PARAMETER start-date AS DATE NO-UNDO.
DEF INPUT PARAMETER end-date AS DATE NO-UNDO.
DEF INPUT PARAMETER frequency-code AS CHAR NO-UNDO.
DEF INPUT PARAMETER amount AS DEC NO-UNDO.
DEF INPUT PARAMETER related AS CHAR NO-UNDO.
DEF INPUT PARAMETER description AS CHAR NO-UNDO.

DEF VAR manual-flow-type AS CHAR NO-UNDO.

  IF start-date > forecast-end THEN RETURN.

  IF cf-type = "RECV" THEN manual-flow-type = "MREC".
  ELSE IF cf-type = "SCL" THEN manual-flow-type = "MSCL".
  ELSE IF cf-type = "RENT" THEN manual-flow-type = "MRNT".
  ELSE manual-flow-type = "".

  IF manual-flow-type <> "" THEN DO:
    IF NOT check-manual-flow( manual-flow-type, chg-type, 
             INPUT-OUTPUT start-date, INPUT-OUTPUT end-date ) THEN RETURN.
  END.


  CREATE CashFlow.
  ASSIGN  CashFlow.ScenarioCode  = Scenario.ScenarioCode
          CashFlow.EntityType    = "P"
          CashFlow.EntityCode    = Property.PropertyCode
          CashFlow.RelatedKey    = related
          CashFlow.CashFlowType  = cf-type
          CashFlow.CFChangeType  = chg-type
          CashFlow.AccountCode   = account
          CashFlow.StartDate     = start-date
          CashFlow.EndDate       = (IF end-date >= forecast-end THEN ? ELSE end-date)
          CashFlow.FrequencyCode = frequency-code
          CashFlow.Amount        = amount
          CashFlow.Description   = description.

  debug-event( "Created cashflow " + STRING( CashFlow.StartDate, "99/99/9999") + " to " + STRING( end-date, "99/99/9999" ) + ", " + STRING(amount) + ", related: " + related).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delete-current-cash-flows Procedure 
PROCEDURE delete-current-cash-flows :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  debug-event("Deleting current RECV cash flows").
  FOR EACH CashFlow OF Scenario WHERE CashFlow.ScenarioCode = scenario-code
                    AND CashFlow.CashFlowType = "RECV":
    DELETE CashFlow.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE each-tenancy-lease Procedure 
PROCEDURE each-tenancy-lease :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR rental-freq   AS CHAR NO-UNDO.
DEF VAR rental-fract  AS DEC  NO-UNDO.
DEF VAR monthly-lease AS LOGI NO-UNDO.
DEF VAR review-list   AS CHAR NO-UNDO.

  rental-freq = IF CAN-FIND( FIRST FrequencyType WHERE
    FrequencyType.FrequencyCode = TenancyLease.PaymentFrequency ) THEN
    TenancyLease.PaymentFrequency ELSE "MNTH".
  RUN process/calcfreq.p ( rental-freq, OUTPUT rental-fract ).  
  monthly-lease = TenancyLease.LeaseEndDate = ? OR TenancyLease.LeaseEndDate < TODAY.

  review-list = "".
  FOR EACH RentReview NO-LOCK OF TenancyLease WHERE ReviewStatus <> "DONE"
                AND RentReview.DateDue >= forecast-start
                AND RentReview.DateDue <= forecast-end:
    review-list = review-list + STRING( RentReview.DateDue, "99/99/9999") + ",".
  END.

  FOR EACH RentalSpace NO-LOCK OF TenancyLease:
    RUN each-rental-space( rental-freq, rental-fract, monthly-lease,
                TenancyLease.LeaseStartDate, TenancyLease.LeaseEndDate, review-list ).
  END.
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE generate-recoveries Procedure 
PROCEDURE generate-recoveries :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FOR EACH Property NO-LOCK:
    IF NOT include-property() THEN NEXT.

    debug-event( "Opex recoveries for P" + STRING(Property.PropertyCode) + " - " + Property.Name ).
    RUN build-expense-table.
    RUN property-cash-flows.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get-parameters Procedure 
PROCEDURE get-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR parm AS CHAR NO-UNDO.

  entity-list  = get-parameter( "In-Entities" ).
  entity-list-type = ENTRY( 1, entity-list).
  entity-list-by = ENTRY( 2, entity-list).
  entity-list = SUBSTRING( entity-list, LENGTH(entity-list-by) + 3).
  all-entities =  entity-list  = "ALL".

  forecast-start = DATE( get-parameter( "Forecast-Start" ) ).
  IF forecast-start = ? THEN forecast-start = DATE( 1, 1, YEAR(TODAY)).
  forecast-end   = DATE( get-parameter( "Forecast-End" ) ).
  IF forecast-end = ? THEN forecast-end = add-date( TODAY, 11, 0, 0 ).

  washup-cycle = DATE( get-parameter( "Washup-Cycle" ) ).
  IF washup-cycle = ? THEN washup-cycle = DATE( 1, 1, YEAR(TODAY) ) - 1.
  debug-event( "Washup cycle date is: " + STRING( washup-cycle, "99/99/9999") ).

  profiled = (get-parameter( "Profile-Recoveries" ) = "Yes").
  parm = get-parameter( "Recovery-Offset" ).
  IF parm <> "" THEN
    recovered-offset = DEC( parm ).
  ELSE
    recovered-offset = 0.2 .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE make-rx-records Procedure 
PROCEDURE make-rx-records :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER d-1 AS DATE NO-UNDO.
DEF INPUT PARAMETER d-n AS DATE NO-UNDO.
DEF INPUT PARAMETER summarise AS LOGI NO-UNDO.
DEF INPUT PARAMETER relate AS CHAR NO-UNDO.

DEF VAR part-month AS DEC NO-UNDO.
DEF VAR m-1 AS INT NO-UNDO.
DEF VAR m-n AS INT NO-UNDO.

  IF d-1 < forecast-start OR d-1 = ? THEN d-1 = forecast-start.
  IF d-n > forecast-end OR d-n = ? THEN d-n = forecast-end.
  debug-event( "Making RX records from " + STRING( d-1, "99/99/9999")
                + " to " + STRING( d-n, "99/99/9999")
                + ",   All=" + STRING( summarise )
                + ",   Relate=" + relate ).

  IF summarise AND RentalSpace.OutgoingsPercentage <= 0 THEN DO:
    debug-event( "No percentage on rental space: " + relate ).
    RETURN.
  END.

  FIND LAST Month WHERE Month.StartDate <= d-1 NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Month) THEN DO:
    debug-event( "Couldn't find month for " + STRING( d-1, "99/99/9999") ).
    RETURN.
  END.
  m-1 = Month.MonthCode.
  FIND FIRST Month WHERE Month.EndDate >= d-n NO-LOCK NO-ERROR.
  IF NOT AVAILABLE(Month) THEN DO:
    debug-event( "Couldn't find month for " + STRING( d-n, "99/99/9999") ).
    RETURN.
  END.
  m-n = Month.MonthCode.

  IF summarise THEN DO:
    FOR EACH PX WHERE PX.mc >= m-1 AND PX.mc <= m-n :
      IF d-n < PX.d-end OR d-1 > PX.d-start THEN
        part-month = (MIN( PX.d-end, d-n) - MAX(PX.d-start, d-1)) / (PX.d-end - PX.d-start) .
      ELSE
        part-month = 1.0 .

      CREATE RX.
      BUFFER-COPY PX TO RX ASSIGN
                RX.percent = RentalSpace.OutGoingsPercentage * part-month
                RX.amt    = - (PX.amt * (RX.percent / 100))
                RX.relate = relate
                RX.r-desc = "Recover " + TRIM( STRING( RX.percent, "->,>>9.99"))
                          + "% of " + TRIM( STRING( PX.amt, "->>,>>>,>>9.99")).
    END.
  END.
  ELSE DO:
    FOR EACH TenancyOutgoing NO-LOCK OF TenancyLease,
        EACH PX WHERE PX.ac = TenancyOutgoing.AccountCode
                                  AND PX.mc >= m-1 AND PX.mc <= m-n:
      IF d-n < PX.d-end OR d-1 > PX.d-start THEN
        part-month = (MIN( PX.d-end, d-n) - MAX(PX.d-start, d-1)) / (PX.d-end - PX.d-start) .
      ELSE
        part-month = 1.0 .

      CREATE RX.
      BUFFER-COPY PX TO RX ASSIGN
                RX.percent = TenancyOutgoing.Percentage * part-month
                RX.amt    = - (PX.amt * (RX.percent / 100))
                RX.relate = relate
                RX.r-desc = "Recover " + TRIM( STRING( RX.percent, "->,>>9.99"))
                          + "% of " + TRIM( STRING( PX.amt, "->>,>>>,>>9.99")).
    END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE old-property-cash-flows Procedure 
PROCEDURE old-property-cash-flows :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER RentFlow FOR CashFlow.
DEF BUFFER OtherFlow FOR CashFlow.

DEF VAR part-month AS DEC NO-UNDO.
DEF VAR rsp-code AS INT NO-UNDO.
DEF VAR exclude-lease AS LOGICAL NO-UNDO.
DEF VAR lease-list AS CHAR NO-UNDO.
DEF VAR d-1 AS DATE NO-UNDO.
DEF VAR d-n AS DATE NO-UNDO.
DEF VAR last-rsp-code AS INT NO-UNDO       INITIAL ?.

  debug-event( "Processing rental cash flows of property" ).
  FOR EACH RentFlow NO-LOCK OF Scenario WHERE RentFlow.EntityType = "P"
                AND RentFlow.EntityCode = Property.PropertyCode
                AND (RentFlow.CashFlowType = "RENT" OR RentFlow.CashFlowType = "MRNT")
                AND (RentFlow.CFChangeType = "N" OR RentFlow.CFChangeType = "R")
                BY RentFlow.RelatedKey:
    debug-event( "Looking at rental flow for " + RentFlow.RelatedKey ).
    rsp-code = INT(ENTRY( 8, RentFlow.RelatedKey)).
    IF rsp-code <> last-rsp-code THEN DO:
      IF last-rsp-code <> ? THEN DO:
        /* create flows for the RX amounts */
        IF profiled THEN RUN profiled-flows.  ELSE RUN smoothed-flows.
      END.
      last-rsp-code = rsp-code.
      RUN clear-rx-records.
    END.
    FIND RentalSpace NO-LOCK OF Property WHERE RentalSpace.RentalSpaceCode = rsp-code NO-ERROR.
    IF NOT AVAILABLE(RentalSpace) THEN DO:
      debug-event("Rental space '" + RentFlow.RelatedKey + "' not available!").
      NEXT.
    END.

    exclude-lease = RentalSpace.AreaStatus = "V".
    IF NOT exclude-lease THEN DO:
      /* need lease-list to include date identifier */
      IF LOOKUP( STRING(RentalSpace.TenancyLeaseCode) + "-" + STRING(RentFlow.StartDate),
                         lease-list ) > 0 THEN DO:
        debug-event( "Skipping - tenancylease " + STRING(RentalSpace.TenancyLeaseCode)
                         + ",  starting on " + STRING(RentFlow.StartDate) + " already processed").
        exclude-lease = Yes.
      END.
      ELSE DO:
        lease-list = lease-list + STRING(RentalSpace.TenancyLeaseCode) + "-"  + STRING(RentFlow.StartDate) + ",".
      END.
    END.
    IF NOT exclude-lease THEN DO:
      FIND TenancyLease NO-LOCK OF RentalSpace NO-ERROR.
      IF NOT AVAILABLE(TenancyLease) THEN DO:
        debug-event( "Skipping - no tenancylease found for Rental space '" + RentFlow.RelatedKey + "'").
        exclude-lease = Yes.
      END.
      ELSE IF NOT CAN-FIND(FIRST TenancyOutGoing OF TenancyLease WHERE TenancyOutGoing.Percent <> 0) THEN DO:
        debug-event( "Skipping - tenancylease for Rental space '" + RentFlow.RelatedKey + "' has no outgoings.").
        exclude-lease = Yes.
      END.
      ELSE IF TenancyLease.LeaseStatus = "PAST" OR TenancyLease.LeaseStatus = "ASGN" THEN DO:
        debug-event( "Skipping - tenancylease for Rental space '" + RentFlow.RelatedKey + "' has PAST or ASGN status.").
        exclude-lease = Yes.
      END.
      ELSE IF TenancyLease.LeaseEndDate = ? THEN DO:
        FIND FIRST OtherFlow WHERE OtherFlow.EntityType = "P"
                AND OtherFlow.EntityCode = Property.PropertyCode
                AND (OtherFlow.CashFlowType = "RENT" OR OtherFlow.CashFlowType = "MRNT")
                AND OtherFlow.Amount = 0
                AND OtherFlow.RelatedKey = RentFlow.RelatedKey
                AND OtherFlow.StartDate > TenancyLease.LeaseStartDate NO-LOCK NO-ERROR.
        IF AVAILABLE(OtherFlow) AND OtherFlow.StartDate <= RentFlow.StartDate THEN DO:
          debug-event( "Skipping - flow for Rental space '" + RentFlow.RelatedKey + "' "
                            + STRING(RentFlow.StartDate, "99/99/9999") + " appears to be past the end of the existing lease.").
          exclude-lease = Yes.
        END.
      END.
      ELSE IF TenancyLease.LeaseEndDate < RentFlow.StartDate THEN DO:
        debug-event( "Skipping - flow for Rental space '" + RentFlow.RelatedKey + "' "
                            + STRING(RentFlow.StartDate, "99/99/9999") + " is past the end of the existing lease.").
        exclude-lease = yes.
      END.
    END.

    /* Calculate the percentages of all of the PX amounts to create RX amounts */
    IF NOT(exclude-lease) THEN DO:
      d-1 = RentFlow.StartDate.
      d-n = IF RentFlow.EndDate = ? THEN forecast-end ELSE last-of-month( RentFlow.EndDate ).
      IF d-1 > first-of-month( d-1 ) THEN DO:
        d-1 = add-months( first-of-month( d-1 ), 1 ).
        debug-event( RentFlow.RelatedKey + ": Rent flow start after 1st of month " + STRING( RentFlow.StartDate, "99/99/9999")
                        + " - changed to " + STRING( d-1, "99/99/9999") ).
      END.
      IF d-n <> RentFlow.EndDate THEN
        debug-event( RentFlow.RelatedKey + ": Rent flow finish before end of month " + (IF RentFlow.EndDate = ? THEN "?" ELSE STRING(  RentFlow.EndDate, "99/99/9999") )
                        + " - changed to " + STRING( d-n, "99/99/9999") ).

      debug-event( "Making RX records for lease " + STRING(RentalSpace.TenancyLeaseCode)
                         + ",  starting on " + STRING(RentFlow.StartDate) + " already processed").
      RUN make-rx-records( d-1, d-n, No, RentFlow.RelatedKey ).
    END.
  END.

  IF last-rsp-code <> ? THEN DO:
    /* create flows for the RX amounts */
    IF profiled THEN RUN profiled-flows.  ELSE RUN smoothed-flows.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-options Procedure 
PROCEDURE parse-options :
/*------------------------------------------------------------------------------
  Purpose:  Decode the command-line parameters
------------------------------------------------------------------------------*/
DEF VAR i           AS INT NO-UNDO.
DEF VAR token       AS CHAR NO-UNDO.

  scenario-code = INT( ENTRY( 1, process-options , "~n") ).

  DO i = 2 TO NUM-ENTRIES( process-options, "~n" ):
    token = ENTRY( i, process-options, "~n" ).
    CASE( ENTRY( 1, token ) ):
      WHEN "Debug" THEN         debug-mode = Yes.
    END CASE.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE profiled-flows Procedure 
PROCEDURE profiled-flows :
/*------------------------------------------------------------------------------
  Purpose:  Create cashflow records for each month, for each account
------------------------------------------------------------------------------*/

  debug-event( "Creating profiled flows" ).

  FOR EACH RX BY RX.ac BY RX.mc:
    CREATE CashFlow.
    ASSIGN CashFlow.ScenarioCode = Scenario.ScenarioCode
           CashFlow.EntityType   = "P"
           CashFlow.EntityCode   = Property.PropertyCode
           CashFlow.AccountCode  = RX.ac + recovered-offset
           CashFlow.CashFlowType = "RECV"
           CashFlow.StartDate    = rx.d-start
           CashFlow.EndDate      = rx.d-end
           CashFlow.RelatedKey   = rx.relate
           CashFlow.FrequencyCode = "MNTH"
           CashFlow.Amount       = rx.amt
           CashFlow.Description  = rx.r-desc.
    DELETE RX.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE property-cash-flows Procedure 
PROCEDURE property-cash-flows :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER RentFlow FOR CashFlow.

DEF VAR part-month AS DEC NO-UNDO.
DEF VAR rsp-code AS INT NO-UNDO.
DEF VAR lease-list AS CHAR NO-UNDO.
DEF VAR d-1 AS DATE NO-UNDO.
DEF VAR d-n AS DATE NO-UNDO.
DEF VAR last-rsp-code AS INT NO-UNDO       INITIAL ?.
DEF VAR lease-key AS CHAR NO-UNDO.
DEF VAR space-key AS CHAR NO-UNDO.

  debug-event( "Processing current leases of property" ).
  FOR EACH TenancyLease OF Property WHERE TenancyLease.LeaseStatus <> "PAST"
                        AND TenancyLease.LeaseStatus <> "ASGN" NO-LOCK:
    lease-key = id-code( "TLS" ).
    debug-event( "Looking at tenancy lease " + lease-key ).
    RUN clear-rx-records.
    IF CAN-FIND(FIRST TenancyOutGoing OF TenancyLease WHERE TenancyOutGoing.Percent <> 0) THEN DO:
      /* Calculate the percentages of all of the PX amounts to create RX amounts */
      d-1 = TenancyLease.LeaseStartDate.
      d-n = forecast-end.

      FOR EACH RentalSpace OF TenancyLease NO-LOCK:
        space-key = id-code( "RSP" ).
        FIND FIRST RentFlow OF Scenario WHERE RentFlow.RelatedKey = space-key
                            AND RentFlow.StartDate > d-1
                            AND (RentFlow.CFChangeType = "N"
                              OR RentFlow.CFChangeType = "X")
                            NO-LOCK NO-ERROR.
        IF AVAILABLE(RentFlow) THEN DO:
          IF d-n = ? THEN
            d-n = RentFlow.StartDate - 1.
          ELSE
            d-n = MIN( d-n, (RentFlow.StartDate - 1)).
        END.
      END.

      debug-event( "Making RX records for lease " + lease-key ).
      RUN make-rx-records( d-1, d-n, No, lease-key ).
      IF profiled THEN RUN profiled-flows.  ELSE RUN smoothed-flows.
    END.
    ELSE
      debug-event( "Skipping - tenancylease '" + lease-key + "' has no outgoings.").

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE smoothed-flows Procedure 
PROCEDURE smoothed-flows :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR d-1 AS DATE NO-UNDO INITIAL ?.
DEF VAR amt AS DEC NO-UNDO INITIAL 0.

DEF VAR last-ac AS DEC NO-UNDO INITIAL ?.
DEF VAR last-end AS DATE NO-UNDO INITIAL ?.
DEF VAR last-relate AS CHAR NO-UNDO INITIAL ?.
DEF VAR peak-percent AS DEC NO-UNDO INITIAL ?.
DEF VAR n AS INT NO-UNDO.
DEF VAR next-washup AS DATE NO-UNDO.

  debug-event( "Creating smoothed flows" ).
  next-washup = forecast-start - 1.

  FOR EACH RX BY RX.ac BY RX.mc:
    IF RX.ac <> last-ac OR RX.d-start > next-washup THEN DO:
      IF last-ac <> ? THEN
        RUN create-flow( "RECV", "", last-ac + recovered-offset,
                         d-1, last-end, "MNTH", (amt / n), last-relate,
                         "Recovery of " + STRING( last-ac, "9999.99")
                         + " at " + TRIM(STRING(peak-percent,"->,>>9.99"))
                         + "% smoothed over " + STRING(n) + " months" ).

      IF RX.ac <> last-ac THEN
        next-washup = next-date-after( washup-cycle, RX.d-start, "M", 12).
      ELSE
        next-washup = add-months( next-washup, 12).

      ASSIGN last-ac = RX.ac
             d-1     = RX.d-start
             amt     = 0
             n       = 0
             peak-percent = RX.percent.

      debug-event( "Next washup is: " + STRING( next-washup, "99/99/9999") ).
    END.
    debug-event( RX.relate + ": " + STRING( RX.ac, "9999.99" ) + ", "
               + STRING(RX.mc) + ", " + STRING( RX.d-start, "99/99/9999")
               + ",   for " + STRING(RX.amt) ).

    ASSIGN amt          = amt + RX.amt
           last-end     = RX.d-end
           last-relate  = RX.relate
           n            = n + 1
           peak-percent = MAX( peak-percent, RX.percent ).
    DELETE RX.
  END.

  IF last-ac <> ? THEN
    RUN create-flow( "RECV", "", last-ac + recovered-offset, d-1, last-end,
                    "MNTH", (amt / n), last-relate,
                    "Recovery of " + STRING( last-ac, "9999.99")
                    + " at " + TRIM(STRING(peak-percent,"->,>>9.99"))
                    + "% smoothed over " + STRING(n) + " months" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION check-manual-flow Procedure 
FUNCTION check-manual-flow RETURNS LOGICAL
  ( INPUT man-type AS CHAR, INPUT chg-type AS CHAR, INPUT-OUTPUT d-1 AS DATE, INPUT-OUTPUT d-n AS DATE  ) :
/*------------------------------------------------------------------------------
  Purpose:  Check that no manual cash flow occurs between d-1 and d-n
    Notes:  We adjust the dates if necessary, or perhaps we give up entirely
            (i.e. RETURN No) if the whole period is occluded.
------------------------------------------------------------------------------*/
DEF VAR id-cd AS CHAR NO-UNDO.

  /* Can't manually override adjustments, except by specifically overwriting the amounts */
  IF chg-type = "A" THEN DO:
    IF CAN-FIND( FIRST CashFlow WHERE CashFlow.ScenarioCode  = Scenario.ScenarioCode
                AND CashFlow.RelatedKey = id-cd
                AND CashFlow.CashFlowType = man-type
                AND CashFlow.StartDate = d-1
                AND CashFlow.EndDate = d-n
                AND CashFlow.CFChangeType = "A") THEN
      RETURN No.
    ELSE
      RETURN Yes.
  END.

  id-cd = id-code("TLS").
  FOR EACH CashFlow NO-LOCK WHERE CashFlow.ScenarioCode  = Scenario.ScenarioCode
                AND CashFlow.RelatedKey = id-cd
                AND CashFlow.CashFlowType = man-type
                AND (CashFlow.StartDate <= d-n OR CashFlow.StartDate = ?)
                AND (CashFlow.EndDate >= d-1 OR CashFlow.EndDate = ?):

    IF (CashFlow.StartDate <= d-1 OR CashFlow.StartDate = ?)
         AND (CashFlow.EndDate >= d-n OR CashFlow.EndDate = ?) THEN RETURN No.

    IF CashFlow.StartDate > d-1 THEN    d-n = CashFlow.StartDate - 1.
    ELSE IF CashFlow.EndDate < d-n THEN d-1 = CashFlow.EndDate + 1.

  END.

  RETURN (d-1 <= d-n) OR d-1 = ? OR d-n = ?.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-parameter Procedure 
FUNCTION get-parameter RETURNS CHARACTER
  ( INPUT parameter-name AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  IF NOT AVAILABLE Scenario THEN RETURN "".

DEF VAR dv AS CHAR NO-UNDO.

  FIND FIRST ScenarioParameter OF Scenario WHERE
    ScenarioParameter.ParameterID = parameter-name NO-LOCK NO-ERROR.

  dv = ( IF AVAILABLE ScenarioParameter THEN ScenarioParameter.Data ELSE "" ).
  debug-event( "Parameter " + parameter-name + "=" + dv ).

  RETURN dv.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION id-code Procedure 
FUNCTION id-code RETURNS CHARACTER
  ( INPUT type AS CHAR ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  Record appropriate to type must be available 
------------------------------------------------------------------------------*/
  IF type = "TLS" AND AVAILABLE(TenancyLease) THEN
      RETURN "TLS," + STRING( TenancyLease.TenancyLeaseCode, "99999" ).
  ELSE IF AVAILABLE(RentalSpace) THEN
      RETURN "RSP," + STRING( RentalSpace.PropertyCode, "99999" )
            + ",L," + STRING( 50000 + RentalSpace.Level, "99999" )
            + ",S," + STRING( 50000 + RentalSpace.LevelSequence, "99999" )
            + ",R," + STRING( RentalSpace.RentalSpaceCode ).
  ELSE IF AVAILABLE(Contract) THEN
      RETURN "SVC," + STRING( Contract.PropertyCode, "99999" )
            + ",T," + Contract.ServiceType
            + ",C," + STRING( Contract.CreditorCode, "99999" ).
  ELSE
    MESSAGE "Don't know how to make an id code - no appropriate records available".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION include-property Procedure 
FUNCTION include-property RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR include-it AS LOGI NO-UNDO.

  IF all-entities THEN RETURN Property.Active .
  IF entity-list-type <> "P" THEN RETURN No.

  CASE entity-list-by:
    WHEN "Company" THEN
      include-it = LOOKUP( STRING( Property.PropertyCode ), entity-list ) <> 0.

    WHEN "Properties" THEN
      include-it = LOOKUP( STRING( Property.PropertyCode ), entity-list ) <> 0.

    WHEN "Property Managers" THEN
      include-it = LOOKUP( STRING( Property.Administrator ), entity-list ) <> 0.

    WHEN "Portfolios" THEN
      include-it = LOOKUP( STRING( Property.Manager ), entity-list ) <> 0.
 
    WHEN "Regions" THEN
      include-it = LOOKUP( Property.Region, entity-list ) <> 0.
  END CASE.

  RETURN include-it.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION is-recoverable Procedure 
FUNCTION is-recoverable RETURNS LOGICAL
  ( INPUT ac AS DEC ) :
/*------------------------------------------------------------------------------
  Purpose:  Determine if the account 'ac' is recoverable
------------------------------------------------------------------------------*/
DEF BUFFER COA FOR ChartOfAccount.

  FIND COA NO-LOCK WHERE COA.AccountCode = ac NO-ERROR.
  IF NOT AVAILABLE(COA) THEN RETURN No.

  RETURN LOOKUP( COA.AccountGroupCode, recoverable-groups) > 0 .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


