&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
    File        : process/apmsbrun.p
    Purpose     : Run programs which take a parameter except do it from the
                  command line.

    Syntax      : the progress command line should have a -param "<program>,<parameters>"

    Author(s)   : Andrew McMillan
    Created     : 21/6/2000
  ------------------------------------------------------------------------*/

DEF VAR progname AS CHAR NO-UNDO INITIAL "".
DEF VAR parameters AS CHAR NO-UNDO INITIAL "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 7.35
         WIDTH              = 39.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

progname = ENTRY(1, SESSION:PARAMETER).
IF INDEX(SESSION:PARAMETER,',') > 0  THEN DO:
  parameters = SUBSTRING( SESSION:PARAMETER, INDEX(SESSION:PARAMETER,',') + 1).
END.
RUN VALUE( progname ) ( parameters ).

QUIT.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


