&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/
DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

DEF VAR voucher-1 AS INT NO-UNDO   INIT ?.
DEF VAR voucher-n AS INT NO-UNDO   INIT ?.

DEF VAR voucher-date AS DATE NO-UNDO   INIT ?.
DEF VAR due-date     AS DATE NO-UNDO   INIT ?.

RUN parse-parameters.

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}

{inc/ofc-this.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = .25
         WIDTH              = 32.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

RUN copy-vouchers.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-copy-vouchers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copy-vouchers Procedure 
PROCEDURE copy-vouchers :
/*------------------------------------------------------------------------------
  Purpose:     Actually copy the invoices
------------------------------------------------------------------------------*/
DEFINE BUFFER NewVoucher FOR Voucher.
DEFINE BUFFER NewVoucherLine FOR VoucherLine.

FOR EACH Voucher WHERE Voucher.VoucherSeq >= voucher-1 AND Voucher.VoucherSeq <= voucher-n NO-LOCK:
    CREATE NewVoucher.
    BUFFER-COPY Voucher EXCEPT VoucherSeq TO NewVoucher
        ASSIGN NewVoucher.VoucherStatus = "U"
               NewVoucher.BatchCode     = ?
               NewVoucher.Date          = voucher-date
               NewVoucher.DateDue       = due-date.
    FOR EACH VoucherLine OF Voucher NO-LOCK:
        CREATE NewVoucherLine.
        BUFFER-COPY VoucherLine EXCEPT VoucherSeq TO NewVoucherLine
            ASSIGN NewVoucherLine.VoucherSeq = NewVoucher.VoucherSeq.
    END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR token AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.

{inc/showopts.i "report-options"}

  n =  NUM-ENTRIES( report-options, "~n" ).
  DO i = 1 TO n:
    token = ENTRY( i, report-options, "~n" ).
    
    CASE ENTRY( 1, token ):
      WHEN "VoucherDate" THEN voucher-date = DATE(ENTRY(2,TOKEN)).

      WHEN "DueDate"     THEN due-date     = DATE(ENTRY(2,TOKEN)).
      
      WHEN "VoucherRange" THEN ASSIGN
        voucher-1 = INT(ENTRY(2,token))
        voucher-n = INT(ENTRY(3,token)).

    END CASE.
    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

