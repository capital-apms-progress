&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
  ------------------------------------------------------------------------*/
DEF INPUT PARAMETER report-options AS CHAR NO-UNDO.

DEF VAR preview   AS LOGI NO-UNDO INIT Yes.
DEF VAR payment-style AS CHAR NO-UNDO   INIT "".
DEF VAR forced-style AS CHAR NO-UNDO   INIT ?.
DEF VAR bank-account AS CHAR NO-UNDO    INIT "".
DEF VAR due-before AS DATE NO-UNDO.
DEF VAR cheque-date AS DATE NO-UNDO.
DEF VAR cheque-message AS CHAR NO-UNDO  INIT "".
DEF VAR first-cheque-no AS INT NO-UNDO  INIT ?.
DEF VAR enforce-cheque-limit AS LOGI NO-UNDO  INIT No.
DEF VAR test-client-code AS CHAR NO-UNDO INIT ?.
DEF VAR creditor-list AS CHAR NO-UNDO   INIT "".
DEF VAR creditor-1 AS INT NO-UNDO       INIT 0.
DEF VAR creditor-n AS INT NO-UNDO       INIT 999999.
DEF VAR project-1 AS INT NO-UNDO        INIT 0.
DEF VAR project-n AS INT NO-UNDO        INIT 999999.
DEF VAR account-1 AS DEC NO-UNDO        INIT 0.
DEF VAR account-n AS DEC NO-UNDO        INIT 10000.
RUN parse-parameters.

DEF VAR user-name AS CHAR NO-UNDO.
{inc/username.i "user-name"}
DEF VAR timeStamp AS CHAR FORMAT "X(44)" NO-UNDO.
timeStamp = STRING( TODAY, "99/99/9999") + ", " + STRING( TIME, "HH:MM:SS") + " for " + user-name.

DEF VAR interest-font AS CHAR NO-UNDO INITIAL "fixed,courier,cpi,14,lpi,8,bold".
DEF VAR transaction-font AS CHAR NO-UNDO INITIAL "fixed,courier,cpi,16,lpi,9,normal".

{inc/ofc-this.i}
{inc/ofc-acct.i "DEBTORS" "sundry-debtors"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-check-client) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD check-client Procedure 
FUNCTION check-client RETURNS LOGICAL
  ( INPUT et AS CHAR, INPUT ec AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-parent-entity) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD get-parent-entity Procedure 
FUNCTION get-parent-entity RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pay-creditor) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pay-creditor Procedure 
FUNCTION pay-creditor RETURNS LOGICAL
  ( INPUT creditor-code AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pay-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pay-voucher Procedure 
FUNCTION pay-voucher RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-payment) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD print-payment Procedure 
FUNCTION print-payment RETURNS DECIMAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 31.8
         WIDTH              = 32.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{inc/method/m-txtrep.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

RUN build-interest-charges.
RUN charge-interest.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-build-card-payment) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-card-payment Procedure 
PROCEDURE build-card-payment :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF BUFFER CardPayment FOR Payment.
DEF BUFFER CardVchrPaid FOR BeingPaid.

DEF VAR card-creditor AS INT NO-UNDO.

  IF NOT CAN-FIND( FIRST Payment WHERE Payment.PaymentStyle = "CARD") THEN RETURN.
  ASSIGN card-creditor = INT(card-payment-creditor) NO-ERROR.
  IF ERROR-STATUS:ERROR THEN DO:
    MESSAGE "The Office Setting 'Card-Payment-Creditor' needs" SKIP
            "to be set to a Creditor Code before the 'CARD'" SKIP
            "payment style can be used."
            VIEW-AS ALERT-BOX ERROR
            TITLE "Card-Payment-Creditor not set".
    RETURN ERROR "FAIL".
  END.

  FOR EACH Payment WHERE Payment.PaymentStyle = "CARD":
    FIND CardPayment WHERE CardPayment.CreditorCode = card-creditor
                     AND CardPayment.PaymentStyle = {&CARD-PYMT}
                     NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(CardPayment) THEN DO:
      CREATE CardPayment.
      CardPayment.CreditorCode = card-creditor.
      CardPayment.PaymentStyle = {&CARD-PYMT}.
    END.

    /* re-assign the vouchers as being paid by the credit card */
    FOR EACH BeingPaid OF Payment:
      CREATE CardVchrPaid.
      BUFFER-COPY BeingPaid TO CardVchrPaid ASSIGN
              CardVchrPaid.CreditorCode = CardPayment.CreditorCode
              CardVchrPaid.PaymentStyle = CardPayment.PaymentStyle.
    END.

  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-build-interest-info) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE build-interest-info Procedure 
PROCEDURE build-interest-info :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR 
  FOR EACH Tenant WHERE Active NO-LOCK:
  FOR EACH AcctTran WHERE EntityType = "T" AND EntityCode = Tenant.TenantCode
            AND (ClosingGroup = ? OR ClosingGroup = 0) NO-LOCK:
    /* DISPLAY AcctTran. */
  END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-do-card-transfer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE do-card-transfer Procedure 
PROCEDURE do-card-transfer :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER batch-no AS INT NO-UNDO.
DEF INPUT PARAMETER cheque-no AS INT NO-UNDO.

DEF VAR cheque-total AS DEC NO-UNDO.
DEF VAR i            AS INT NO-UNDO.

  FIND Creditor WHERE Creditor.CreditorCode = Payment.CreditorCode NO-LOCK.

  CREATE NewDocument.
  ASSIGN  NewDocument.BatchCode     = batch-no
          NewDocument.DocumentType  = "CARD"
          NewDocument.Description   = Creditor.PayeeName
          NewDocument.Reference     = "To C" + STRING( Creditor.CreditorCode, "99999") .
                                      /* bank-account + STRING(cheque-no, ">999999"). */

  FOR EACH BeingPaid OF Payment, 
      FIRST Voucher WHERE Voucher.VoucherSeq = BeingPaid.VoucherSeq EXCLUSIVE-LOCK:
    Voucher.VoucherStatus        = "P".
    Voucher.BankAccountCode      = bank-account.
    Voucher.ChequeNo             = cheque-no.
    cheque-total = cheque-total + Voucher.GoodsValue + Voucher.TaxValue.
  END.

  /* Debit the creditor */
  CREATE NewAcctTrans.
  ASSIGN  NewAcctTrans.BatchCode      = batch-no
          NewAcctTrans.DocumentCode   = NewDocument.DocumentCode
          NewAcctTrans.EntityType     = "C"
          NewAcctTrans.EntityCode     = Creditor.CreditorCode
          NewAcctTrans.AccountCode    = sundry-creditors
          NewAcctTrans.Amount         = cheque-total
          NewAcctTrans.Date           = cheque-date
          NewAcctTrans.Description    = ""
          NewAcctTrans.Reference      = "".

  CREATE NewAcctTrans.
  ASSIGN  NewAcctTrans.BatchCode      = batch-no 
          NewAcctTrans.DocumentCode   = NewDocument.DocumentCode
          NewAcctTrans.Amount         = cheque-total * -1
          NewAcctTrans.Date           = cheque-date
          NewAcctTrans.Description    = ""
          NewAcctTrans.Reference      = ""
          NewAcctTrans.EntityType      = "C"        /* Credit the card payment creditor */
          NewAcctTrans.EntityCode      = INT(card-payment-creditor)
          NewAcctTrans.AccountCode     = sundry-creditors.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-do-cheque-run) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE do-cheque-run Procedure 
PROCEDURE do-cheque-run :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR batch-no     AS INT NO-UNDO.
DEF VAR cheque-no    AS INT NO-UNDO.
DEF VAR end-no    AS INT NO-UNDO.

  cheque-no = first-cheque-no.
  
cheque-run:
DO TRANSACTION ON ERROR UNDO cheque-run, RETURN ERROR "FAIL":

  /* Create the batch */
  CREATE NewBatch.
  ASSIGN    NewBatch.BatchType   = 'NORM'
            NewBatch.Description = "Cheque Run - " + STRING( TODAY, "99/99/9999" )
                                 + ", " + bank-account + " account".

  batch-no = NewBatch.BatchCode .
  FOR EACH Payment WHERE Payment.PaymentStyle <> "CARD" BY Payment.Name
                    ON ERROR UNDO cheque-run, RETURN ERROR "FAIL":
    RUN do-payment( batch-no, INPUT-OUTPUT cheque-no ).
  END.
  end-no = cheque-no - 1.

  FIND Payment WHERE Payment.PaymentStyle = {&CARD-PYMT} NO-ERROR.
  IF AVAILABLE(Payment) THEN DO:
    cheque-no = Payment.ChequeNo.
    FOR EACH Payment WHERE Payment.PaymentStyle = "CARD" BY Payment.Name
                    ON ERROR UNDO cheque-run, RETURN ERROR "FAIL":
      RUN do-card-transfer( batch-no, cheque-no ).
    END.
  END.

  IF end-no < first-cheque-no THEN DO:
    /* no cheques produced */
    MESSAGE "No cheques to be printed!" 
            VIEW-AS ALERT-BOX INFORMATION
            TITLE "No Cheques to Print" .
    UNDO cheque-run, RETURN ERROR "FAIL".
  END.

DEF VAR start-printing AS LOGI NO-UNDO INITIAL Yes.

  MESSAGE "Ready to print cheques from " + STRING(first-cheque-no, "999999")
                     " to " STRING( end-no, "999999") SKIP
            "Preview a report on the cheques first? " SKIP
            "  - <Yes> to preview a report first" SKIP
            "  - <No> to print the cheques now" SKIP
            "  - <Cancel> to cancel the cheque run"
            VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO-CANCEL
            TITLE "Preview Payment Report?" UPDATE start-printing.

  IF start-printing = Yes THEN DO:
    RUN print-payment-summary( first-cheque-no, end-no ) .
    MESSAGE "Ready to print" STRING(end-no - first-cheque-no + 1)
             "cheques from " + STRING(first-cheque-no, "999999")
                     " to " STRING( end-no, "999999") SKIP
            "Print cheques?" SKIP(1)
            "(the printer will prompt for cheque forms, if required)"
            VIEW-AS ALERT-BOX QUESTION BUTTONS OK-CANCEL
            TITLE "Print Cheques?" UPDATE start-printing.
    IF start-printing <> Yes THEN start-printing = ?.
  END.

  IF start-printing = ? THEN UNDO cheque-run, RETURN ERROR.

END.        /* of transaction block */


report-options = "BankAccount," + BankAccount.BankAccountCode
               + "~nChequeRange," + STRING(first-cheque-no) + "," + STRING(end-no)
               + (IF payment-style = "" THEN "" ELSE "~nPaymentStyle," + payment-style)
               + "~nMessage," + cheque-message.
RUN process/report/chqprt.p( report-options ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-do-payment) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE do-payment Procedure 
PROCEDURE do-payment :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER batch-no AS INT NO-UNDO.
DEF INPUT-OUTPUT PARAMETER cheque-no AS INT NO-UNDO.

DEF VAR cheque-total AS DEC NO-UNDO.
DEF VAR i            AS INT NO-UNDO.
DEF BUFFER Chq FOR Cheque.

    FIND Creditor WHERE Creditor.CreditorCode = Payment.CreditorCode NO-LOCK.

    CREATE NewDocument.
    ASSIGN  NewDocument.BatchCode     = batch-no
            NewDocument.DocumentType  = "CHEQ"
            NewDocument.Description   = Creditor.PayeeName
            NewDocument.Reference     = bank-account + "," + TRIM(STRING(cheque-no, ">999999")).

    FIND FIRST Chq WHERE Chq.BankAccountCode = bank-account
                  AND Chq.ChequeNo = cheque-no NO-LOCK NO-ERROR.
    IF AVAILABLE(Chq) THEN DO:
      MESSAGE "Cheque number " + bank-account + "/" + TRIM(STRING( cheque-no, ">>>999999")) + " is already in use" SKIP(1)
              "Cancelling cheque run"
              VIEW-AS ALERT-BOX ERROR
              TITLE "Attempt to Re-Issue Cheque".
      RETURN ERROR "FAIL".
    END.
      
    CREATE Cheque NO-ERROR.
    ASSIGN  Cheque.BatchCode          = batch-no
            Cheque.DocumentCode       = NewDocument.DocumentCode
            Cheque.BankAccountCode    = bank-account
            Cheque.ChequeNo           = cheque-no
            Cheque.CreditorCode       = Creditor.CreditorCode
            Cheque.Date               = cheque-date
            Cheque.PayeeName          = Creditor.PayeeName
            Payment.ChequeNo          = cheque-no
            cheque-no                 = cheque-no + 1
            NO-ERROR .

    cheque-total = 0.
    FOR EACH BeingPaid OF Payment, 
        FIRST Voucher WHERE Voucher.VoucherSeq = BeingPaid.VoucherSeq EXCLUSIVE-LOCK:
      Voucher.VoucherStatus        = "P".
      Voucher.BankAccountCode      = bank-account.
      Voucher.ChequeNo             = Cheque.ChequeNo.
      cheque-total = cheque-total + Voucher.GoodsValue + Voucher.TaxValue.
    END.

    /* Debit the creditor */
    CREATE NewAcctTrans.
    ASSIGN  NewAcctTrans.BatchCode      = batch-no
            NewAcctTrans.DocumentCode   = NewDocument.DocumentCode
            NewAcctTrans.EntityType     = "C"
            NewAcctTrans.EntityCode     = Creditor.CreditorCode
            NewAcctTrans.AccountCode    = sundry-creditors
            NewAcctTrans.Amount         = cheque-total
            NewAcctTrans.Date           = cheque-date
            NewAcctTrans.Description    = ""
            NewAcctTrans.Reference      = ""
            Cheque.Amount               = cheque-total.

    CREATE NewAcctTrans.
    ASSIGN  NewAcctTrans.BatchCode      = batch-no 
            NewAcctTrans.DocumentCode   = NewDocument.DocumentCode
            NewAcctTrans.Amount         = cheque-total * -1
            NewAcctTrans.Date           = cheque-date
            NewAcctTrans.Description    = ""
            NewAcctTrans.Reference      = ""
            NewAcctTrans.EntityType     = "L"        /* Credit the bank account */
            NewAcctTrans.EntityCode     = BankAccount.CompanyCode
            NewAcctTrans.AccountCode    = BankAccount.AccountCode.

  RETURN "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-inst-page-footer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-footer Procedure 
PROCEDURE inst-page-footer :
/*------------------------------------------------------------------------------
  Purpose:  Print any page footer
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-inst-page-header) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inst-page-header Procedure 
PROCEDURE inst-page-header :
/*------------------------------------------------------------------------------
  Purpose:  Print any page header
------------------------------------------------------------------------------*/

  RUN pclrep-line( "univers,Point,7,bold,Proportional", TimeStamp).
  RUN pclrep-line( "univers,Point,12,bold,Proportional",
    SPC(45) + "Cheque Production Report"
  ).
  RUN pclrep-line( "", "" ).
  
  /* Put any column headers here */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-parse-parameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE parse-parameters Procedure 
PROCEDURE parse-parameters :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF VAR token AS CHAR NO-UNDO.
DEF VAR i AS INT NO-UNDO.
DEF VAR n AS INT NO-UNDO.

{inc/showopts.i "report-options"}

  n =  NUM-ENTRIES( report-options, "~n" ).
  DO i = 1 TO n:
    token = ENTRY( i, report-options, "~n" ).
    
    CASE ENTRY( 1, token ):
      WHEN "Preview" THEN               preview = Yes.
      WHEN "EnforceLimit" THEN          enforce-cheque-limit = Yes.
      WHEN "PaymentStyle" THEN          payment-style = ENTRY(2,token).
      WHEN "ForcePaymentBy" THEN        forced-style = ENTRY(2,token).
      WHEN "DueBefore" THEN             due-before = DATE(ENTRY(2,token)).
      WHEN "ChequeDate" THEN            cheque-date = DATE(ENTRY(2,token)).
      WHEN "FirstCheque" THEN           first-cheque-no = INT( ENTRY(2,token)).
      WHEN "OneClient" THEN             test-client-code = ENTRY(2,token).
      WHEN "BankAccount" THEN           bank-account = ENTRY(2,token).
      WHEN "CreditorList" THEN          creditor-list = SUBSTRING( token, INDEX(token,",") + 1).

      WHEN "Message" THEN               cheque-message = SUBSTRING( token, INDEX(token,",") + 1).

      WHEN "CreditorRange" THEN ASSIGN
        creditor-1 = INT(ENTRY(2,token))
        creditor-n = INT(ENTRY(3,token)).

      WHEN "ProjectRange" THEN ASSIGN
        project-1 = INT(ENTRY(2,token))
        project-n = INT(ENTRY(3,token)).

      WHEN "AccountRange" THEN ASSIGN
        account-1 = INT(ENTRY(2,token))
        account-n = INT(ENTRY(3,token)).


    END CASE.
    
  END.

  /* ensure entries in creditor list are just plain numbers */
  n =  NUM-ENTRIES( creditor-list ).
  DO i = 1 TO n:
    ENTRY(i, creditor-list ) = STRING( INT( TRIM(ENTRY(i,creditor-list)) ) ).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-payment-summary) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE print-payment-summary Procedure 
PROCEDURE print-payment-summary :
/*------------------------------------------------------------------------------
  Purpose:     
------------------------------------------------------------------------------*/
DEF INPUT PARAMETER from-cheque AS INT NO-UNDO.
DEF INPUT PARAMETER to-cheque AS INT NO-UNDO.

DEF VAR control-file-original AS CHAR NO-UNDO.
DEF VAR print-file-original AS CHAR NO-UNDO.
DEF VAR line AS CHAR NO-UNDO.
DEF VAR payment-total AS DEC NO-UNDO.

print-file-original     = txtrep-print-file.
control-file-original   = txtrep-control-file.
RUN txtrep-initialise.
OUTPUT TO VALUE(txtrep-print-file) KEEP-MESSAGES PAGE-SIZE 0.
RUN pclrep-start( preview, "reset,portrait,tm,2,a4,lm,6,courier,cpi,18,lpi,9").

  FOR EACH Payment WHERE Payment.PaymentStyle <> {&CARD-PYMT}
                     AND Payment.PaymentStyle <> "CARD"
                     BY Payment.ChequeNo:
    payment-total = payment-total + print-payment().
  END.
  FOR EACH Payment WHERE Payment.PaymentStyle = {&CARD-PYMT}
                     BY Payment.ChequeNo:
    payment-total = payment-total + print-payment().
  END.
  line = SPC(90) + "==============".
  RUN pclrep-line( voucher-font, line ).
  line = STRING( "Total payments for cheque run", "X(90)") + STRING( payment-total, ">>>,>>>,>>9.99CR" ).
  RUN pclrep-line( voucher-font, line ).
  RUN pclrep-down-by( 2 ).

  IF CAN-FIND( FIRST Payment WHERE Payment.PaymentStyle = "CARD") THEN DO:
    payment-total = 0.
    FOR EACH Payment WHERE Payment.PaymentStyle = "CARD"
                     BY Payment.Name:
      payment-total = payment-total + print-payment().
    END.
    line = SPC(90) + "==============".
    RUN pclrep-line( voucher-font, line ).
    line = STRING( "Total of transfers to credit card", "X(90)") + STRING( payment-total, ">>>,>>>,>>9.99CR" ).
    RUN pclrep-line( voucher-font, line ).
  END.

  OUTPUT CLOSE.
  RUN view-output-file ( preview ).

  txtrep-print-file     = print-file-original.
  txtrep-control-file   = control-file-original.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-check-client) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION check-client Procedure 
FUNCTION check-client RETURNS LOGICAL
  ( INPUT et AS CHAR, INPUT ec AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Decide whether this cheque run is for the client who receives
            expenses for this entity type / entity code.
------------------------------------------------------------------------------*/
DEF VAR entity-code AS CHAR NO-UNDO.
DEF VAR result AS LOGI NO-UNDO.

  DO WHILE et <> "L":
    entity-code = get-parent-entity( et, ec ).
    et = SUBSTRING( entity-code, 1, 1).
    ec = INT( SUBSTRING( entity-code, 2) ).
  END.

  FIND Company WHERE Company.CompanyCode = ec NO-LOCK NO-ERROR.
  result = (Company.ClientCode = test-client-code).

  RETURN result.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-get-parent-entity) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION get-parent-entity Procedure 
FUNCTION get-parent-entity RETURNS CHARACTER
  ( INPUT et AS CHAR, INPUT ec AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR parent-entity AS CHAR NO-UNDO.

  CASE et:
    WHEN "J" THEN DO:
      FIND Project WHERE Project.ProjectCode = ec NO-LOCK NO-ERROR.
      parent-entity = Project.EntityType + STRING( Project.EntityCode, "99999").
    END.
    WHEN "P" THEN DO:
      FIND Property WHERE Property.PropertyCode = ec NO-LOCK NO-ERROR.
      parent-entity = "L" + STRING( Property.CompanyCode, "99999").
    END.
    WHEN "T" THEN DO:
      FIND Tenant WHERE Tenant.TenantCode = ec NO-LOCK NO-ERROR.
      parent-entity = Tenant.EntityType + STRING( Tenant.EntityCode, "99999").
    END.
    WHEN "C" THEN DO:
      FIND Creditor WHERE Creditor.CreditorCode = ec NO-LOCK NO-ERROR.
      parent-entity = "L" + STRING( Creditor.CompanyCode, "99999").
    END.
  END CASE.

  RETURN parent-entity.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pay-creditor) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pay-creditor Procedure 
FUNCTION pay-creditor RETURNS LOGICAL
  ( INPUT creditor-code AS INT ) :
/*------------------------------------------------------------------------------
  Purpose:  Decide whether we pay a particular creditor this time
    Notes:  
------------------------------------------------------------------------------*/
DEF BUFFER RecentCheque FOR Cheque.
DEF BUFFER ThisCreditor FOR Creditor.

DEF VAR cheque-days AS DEC NO-UNDO.

  IF creditor-code < creditor-1 THEN RETURN No.
  IF creditor-code > creditor-n THEN RETURN No.

  IF TRIM(creditor-list) <> "" AND LOOKUP(STRING(creditor-code), creditor-list) = 0 THEN RETURN No.

  IF enforce-cheque-limit THEN DO:
    FIND LAST RecentCheque WHERE RecentCheque.CreditorCode = creditor-code NO-LOCK NO-ERROR.
    IF AVAILABLE(RecentCheque) THEN DO:
      FIND ThisCreditor OF RecentCheque NO-LOCK NO-ERROR.
      IF AVAILABLE(ThisCreditor) THEN DO:
        cheque-days = ThisCreditor.ChequesPerMonth / 30.
        IF (cheque-date - RecentCheque.Date) < INT(cheque-days) THEN RETURN No.
      END.
    END.
  END.

  RETURN Yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pay-voucher) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pay-voucher Procedure 
FUNCTION pay-voucher RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Decide if an approved voucher should be paid in this cheque run
    Notes:  
------------------------------------------------------------------------------*/
  IF Voucher.EntityType = "J" THEN DO:
    IF Voucher.EntityCode < project-1 THEN RETURN No.
    IF Voucher.EntityCode > project-n THEN RETURN No.
  END.

  IF Voucher.AccountCode < account-1 THEN RETURN No.
  IF Voucher.AccountCode > account-n THEN RETURN No.

  IF Voucher.DateDue > due-before AND Voucher.GoodsValue > 0.0 THEN RETURN No.

  IF payment-style <> "" AND Voucher.PaymentStyle <> payment-style THEN RETURN No.

  IF test-client-code <> ? THEN
    RETURN check-client( Voucher.EntityType, Voucher.EntityCode ).

  RETURN Yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-print-payment) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION print-payment Procedure 
FUNCTION print-payment RETURNS DECIMAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
DEF VAR line AS CHAR NO-UNDO.
DEF VAR cheque-total AS DEC NO-UNDO.

  FIND FIRST Creditor WHERE Creditor.CreditorCode = Payment.CreditorCode NO-LOCK NO-ERROR.
  IF Payment.PaymentStyle = "CARD" THEN
    line = "Xfer to card  ".
  ELSE
    line = "Cheque " + STRING( Payment.ChequeNo, ">999999").

  line = line + "  C" + STRING( Creditor.CreditorCode, "99999") + " "
       + Creditor.Payee.

  IF Payment.PaymentStyle <> "CARD" THEN DO:
    FIND PaymentStyle OF Payment NO-LOCK NO-ERROR.
    line = STRING( line, "X(70)") + (IF AVAILABLE(PaymentStyle) THEN PaymentStyle.Description ELSE Payment.PaymentStyle).
  END.
  RUN pclrep-line( payment-font, line ).

  cheque-total = 0.
  FOR EACH BeingPaid OF Payment, FIRST Voucher WHERE Voucher.VoucherSeq = BeingPaid.VoucherSeq NO-LOCK:
    line = SPC(9) + "Voucher" + STRING( Voucher.VoucherSeq, ">>>>>>9") + ",  "
         + STRING( Voucher.Date, "99/99/9999" ) + "   "
         + STRING( Voucher.Description, "X(50)" ) + " "
         + STRING( Voucher.TaxValue + Voucher.GoodsValue, ">>>,>>>,>>9.99CR" ).
    cheque-total = cheque-total + Voucher.TaxValue + Voucher.GoodsValue .
    RUN pclrep-line( voucher-font, line ).
  END.
  line = SPC(90) + "--------------".
  RUN pclrep-line( voucher-font, line ).
  line = SPC(90) + STRING( cheque-total, ">>>,>>>,>>9.99CR" ).
  RUN pclrep-line( voucher-font, line ).
  RUN pclrep-line( ?, ? ).

  RETURN cheque-total.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

